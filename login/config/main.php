<?php
$params = array_merge(
	require( __DIR__ . '/../../common/config/params.php' ),
	require( __DIR__ . '/../../common/config/params-local.php' ),
	require( __DIR__ . '/params.php' )
);
return [
	'id'                  => 'app-login',
	'basePath'            => dirname( __DIR__ ),
	'bootstrap'           => [ 'log' ],
	'controllerNamespace' => 'login\controllers',
//    'modules' => [
//        'v1' => [
//            'basePath' => '@app/modules/v1',
//            'class' => 'api\modules\v1\Module'
//        ]
//    ],
	'components'          => [
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
//		'session'    => [
//			'class'        => 'yii\web\Session',
//			'cookieParams' => [
//				'httponly' => true,
//				'lifetime' => 3600 * 4
//			],
//			'timeout'      => 3600 * 4, //session expire
//			'useCookies'   => true,
//		],
//		'request'    => [
//			'csrfParam' => '_csrf-app',
//		],
//		'user'       => [
//			'identityClass'   => 'common\models\User',
//			'enableAutoLogin' => true,
//			'identityCookie'  => [
//				'name'     => '_identity',
//				'httpOnly' => true,
//				'domain'   => '.bhc',
//				'path'     => '/'
//			],
//		],
//		'log'        => [
//			'traceLevel' => YII_DEBUG ? 3 : 0,
//			'targets'    => [
//				[
//					'class'  => 'yii\log\FileTarget',
//					'levels' => [ 'error', 'warning' ],
//				],
//			],
//		],
//		'urlManager' => [
//			'enablePrettyUrl' => false,
//			'showScriptName'  => false,
//		]
//            'rules' => [
//                [
//                    'class' => 'yii\rest\UrlRule',
//                    'controller' => 'v1/country',
//                    'tokens' => [
//                        '{id}' => '<id:\\w+>'
//                    ]
//
//                ]
//            ],
//        ]
	],
	'params'              => $params,
];



