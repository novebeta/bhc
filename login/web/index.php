<?php
if ( ! isset( $_COOKIE[ '_outlet' ] ) && ( isset( $_COOKIE[ '_identity' ] )  ) ) {
	session_start();
	session_destroy();
	setcookie( 'PHPSESSID', '', time() - 1000, '/' );
	setcookie( '_identity', '', time() - 1000, '/' );
	header( "Refresh:1" );
}
$GLOBALS[ 'timezone' ] = "Asia/Jakarta";
defined('YII_DEBUG') or define('YII_DEBUG', false);
defined('YII_ENV') or define('YII_ENV', 'prod');
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/../../common/config/bootstrap.php';
$config = yii\helpers\ArrayHelper::merge(
	require __DIR__ . '/../../common/config/main.php',
	require __DIR__ . '/../../common/config/main-local.php',
	require __DIR__ . '/../config/main.php',
	require __DIR__ . '/../config/main-local.php'
);
( new yii\web\Application( $config ) )->run();
