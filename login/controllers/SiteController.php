<?php
namespace login\controllers;
use common\components\Custom;
use common\models\LoginForm;
use common\models\User;
use Yii;
use yii\db\Exception;
use yii\web\Controller;
/**
 * Default controller for the `login` module
 */
class SiteController extends Controller {
	protected function logout() {
		Yii::$app->user->logout();
		//if ( isset( $_SERVER['HTTP_COOKIE'] ) ) {
		//	$cookies = explode( ';', $_SERVER['HTTP_COOKIE'] );
			$cookies = $_COOKIE;
			foreach ( $cookies as $name => $cookie ) {
//				$parts = explode( '=', $cookie );
//				$name  = trim( $parts[0] );
				if ( $name == '_csrf-app' || $name == 'XDEBUG_SESSION') {
					continue;
				}
				setcookie( $name, '', time() - 1000 );
				setcookie( $name, '', time() - 1000, '/' );
			}
		//}
//		setcookie( 'PHPSESSID', '', time() - 1000, '/' );
		Yii::$app->session->destroy();
	}
	protected function setSession() {
		Custom::setSession();
	}
	/**
	 * Renders the index view for the module
	 * @return string
	 */
	public function actionIndex() {
		if ( isset( $_COOKIE['_outlet'] )  && ! Yii::$app->user->isGuest) {
			if ( Custom::isAbengkel() ) {
				return $this->redirect( Custom::urlbck( 'site/index' ) );
			} elseif ( Custom::isAunit() ) {
				return $this->redirect( Custom::urlfnt( 'site/index' ) );
			} elseif ( Custom::isFinance() ) {
				return $this->redirect( Custom::urlfinance( 'site/index' ) );
			}
		}
		$model = new LoginForm();
		if ( $model->load( Yii::$app->request->post() ) ) {
			$model->username = $_POST['LoginForm']['username'];
			$model->password = Custom::Encript( $_POST['LoginForm']['password'] );
			if ( $model->validateDatabase() && $model->login() ) {
				self::setSession();
				return $this->redirect( [ 'site/dealer' ] );
			}
		}
		self::logout();
		$this->layout    = '/main-login';
		$model->password = '';
		return $this->render( 'login', [
			'model' => $model,
		] );
	}
	public function actionDealer() {
		if ( Yii::$app->user->isGuest ) {
			return $this->redirect( [ 'site/index' ] );
		}
		if ( isset( $_POST['cabang'] ) ) {
			/** @var User $user */
			$user = User::findOne( Yii::$app->user->id );
			self::setSession();
			if ( Custom::isAbengkel() ) {
				return $this->redirect( Custom::urlbck( 'site/index&oper=flush' ) );
			} elseif ( Custom::isAunit() ) {
				return $this->redirect( Custom::urlfnt( 'site/index&oper=flush' ) );
			} elseif ( Custom::isFinance() ) {
				return $this->redirect( Custom::urlfinance( 'site/index&oper=flush' ) );
			}
		}
		$this->layout = '/main-login';
		return $this->render( 'dealer' );
	}
	/**
	 * Logout action.
	 *
	 * @return string
	 */
	public function actionLogout() {
		self::logout();
		return $this->goHome();
	}
	public function actionFlush() {
//		echo Custom::Decript('_K');
		opcache_reset();
		Yii::$app->cache->flush();
		opcache_get_status();
	}
	public function actionBlank() {
		self::logout();
		return $this->redirect( 'https://www.google.com/' );
	}
	public function actionCabang() {
		if ( Yii::$app->user->isGuest ) {
			return $this->redirect( [ 'site/index' ] );
		}
		$cache = Yii::$app->cache;
		$keyAllCabangTipe   = 'BHC_ALL_CABANG_';
//		$user = $_POST['LoginForm']['username'];
//		$pass = $_POST['LoginForm']['password'];
//		$pass = Custom::Encript( $pass );
		$users = User::findOne( Yii::$app->user->id );
		$user  = $users->UserID;
		$pass  = $users->UserPass;
		switch ( $_POST['tipe'] ) {
			case 1:
				$tipe = 3;
				break;
			case 2 :
				$tipe = 4;
				break;
			case 3 :
				$tipe = 'hfinance';
				break;
		}
		$keyAllCabangTipe .= $tipe;
		if(!empty($_GET['flush']) && $_GET['flush'] == 'true'){
			$cache->delete($keyAllCabangTipe);
		}
		$option = '';
		$cabangAll  = $cache->get($keyAllCabangTipe);
		try {
			$sqlCmd    = "";
			if ($cabangAll === false) {
				$where[] = 'and';
				if ( $tipe == 3 ) {
					$where[] = [ 'length(schema_name)' => $tipe ];
				} elseif ( $tipe == 4 ) {
					$where[] = [ 'and', 'length(schema_name)' => $tipe, 'schema_name like "B%"' ];
				} elseif ( $tipe == 'hfinance' ) {
					$where[] = [ 'schema_name' => $tipe ];
				}
				$where[]   = [ 'not in', 'schema_name', Yii::$app->params['db_exclude'] ];
				$cabangAll = ( new \yii\db\Query() )
					->select( 'SCHEMA_NAME' )
					->from( 'information_schema.schemata' )
					->where( $where )
					->orderBy('SCHEMA_NAME')
					->all();
				$cache->set($keyAllCabangTipe, $cabangAll);
			}
			foreach ( $cabangAll as $ca_cbg ) {
				$cabang = $ca_cbg['SCHEMA_NAME'];
				$sqlTest = "SELECT UPPER('$cabang') AS db,$cabang.tzcompany.PerusahaanNama 
				FROM $cabang.tuser,$cabang.tzcompany
				WHERE $cabang.tuser.UserStatus = 'A' AND $cabang.tuser.UserID = :user AND $cabang.tuser.UserPass = :pass ";
				try {
					$test = Yii::$app->db->createCommand( $sqlTest )
					                     ->bindParam( ':user', $user, \PDO::PARAM_STR )
					                     ->bindParam( ':pass', $pass, \PDO::PARAM_STR )
					                     ->queryScalar();
					if($test != null){
						if ( $sqlCmd != '' ) {
							$sqlCmd .= " UNION ALL ";
						}
						$sqlCmd .= $sqlTest;
					}
				} catch ( Exception $e ) {
				}
			}
			$cmdWithOrder = "SELECT * FROM ($sqlCmd) as cbg ORDER BY PerusahaanNama";
			$s_cbg = Yii::$app->db->createCommand( $cmdWithOrder )
			                      ->bindParam( ':user', $user, \PDO::PARAM_STR )
			                      ->bindParam( ':pass', $pass, \PDO::PARAM_STR )
			                      ->queryAll();
			foreach ( $s_cbg as $cabang ) {
				$option .= '<option value="' . $cabang['db'] . '">' . $cabang['PerusahaanNama'] . '</option>';
			}
		} catch ( Exception $e ) {
			Yii::error( $e->getMessage(), 'login' );
		}
		return $option;
	}
}
