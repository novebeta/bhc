<?php
//use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
$this->title   = 'Sign In';
$fieldOptions1 = [
	'options'       => [ 'class' => 'form-group has-feedback' ],
	'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];
$fieldOptions2 = [
	'options'       => [ 'class' => 'form-group has-feedback' ],
	'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>
    <!--    <div class="Image-1">-->
    <!--    </div>-->
    <div class="login-box">
        <div class="login-logo BHC-Honda-System" style="padding: 15px;margin-bottom: 5px;background-color: #E50000">
            <a href="#" style="color: #ffffff">HSYS</a>
        </div>
        <div class="box box-solid" >
			<?php $form = ActiveForm::begin( [ 'id' => 'login-form', 'enableClientValidation' => false ] ); ?>
            <div class="box-header">
                <p class="box-title">Sign in to start</p>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<?= $form
					->field( $model, 'username', $fieldOptions1 )
					->label( false )
					->textInput( [ 'placeholder' => $model->getAttributeLabel( 'username' ) ] ) ?>

				<?= $form
					->field( $model, 'password', $fieldOptions2 )
					->label( false )
					->passwordInput( [ 'placeholder' => $model->getAttributeLabel( 'password' ) ] ) ?>
                <div class="form-group">
					<?= Html::submitButton( 'Sign in', [
						'class' => 'btn btn-primary btn-block btn-flat',
						'name'  => 'login-button'
					] ) ?>
                </div>
            </div>
			<?php ActiveForm::end(); ?>
        </div>
        <!-- /.login-logo -->
        <!-- /.login-box-body -->
    </div><!-- /.login-box -->
    <style>
        body {
            /*background-image: url("img/background.png");*/
            /*display: block;*/
            /*width: auto;*/
            /*height: 768px;*/
            background-image: url("img/BackgroundLayer1_.jpg");
            background-repeat:no-repeat;
            background-size:cover;
            background-position: 100% 20%;
            /*background-repeat: no-repeat;*/
            /*background-size: cover;*/
            background-attachment: fixed;
            /*background-position: bottom;*/
            /*background-size: contain;*/
        }

        .Rectangle-16 {
            width: 395px;
            height: 40px;
            object-fit: contain;
            border-radius: 9px;
            background-color: #ff0000;
        }

        .Image-1 {
            background-image: url("img/image-1.png");
            width: 177px;
            height: 142px;
            position: absolute;
            /*top: 31px;*/
            /*right: 56px;*/
            top: 5px;
            left: 5px;
            object-fit: contain;
            /*opacity: 0.22;*/
        }

        .Sign-in-to-start {
            /*width: 119px;*/
            height: 23px;
            object-fit: contain;
            /*font-family: OpenSans;*/
            font-size: 17px;
            font-weight: 600;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.35;
            letter-spacing: normal;
            text-align: left;
            color: #707070;
        }

        .BHC-Honda-System {
            /*width: 304px;*/
            /*height: 43px;*/
            /*width: 455px;*/
            object-fit: contain;
            /*font-family: OpenSans; !important;*/
            padding: 6px 12px;
            font-size: 32px;
            font-weight: bolder;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.34;
            letter-spacing: normal;
            text-align: center;
            color: #ff0000;
            background-color: #e2e2e2;
            border-radius: 10px;
        }

        .login-box-body-new {
            background-image: url("img/group-52.png");
            height: 448px;
            width: 455px;
            padding: 20px;
            object-fit: contain;
        }
    </style>