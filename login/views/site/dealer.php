<?php
//use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
$this->title   = 'Sign In';
$fieldOptions1 = [
	'options'       => [ 'class' => 'form-group has-feedback' ],
	'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];
$fieldOptions2 = [
	'options'       => [ 'class' => 'form-group has-feedback' ],
	'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
$typeLoginArr = [];
if(!empty($_COOKIE['_userType'])){
    $typeLoginArr = explode(',', base64_decode($_COOKIE['_userType']));
}
?>
    <!--    <div class="Image-1">-->
    <!--    </div>-->
    <div class="login-box">
        <div class="login-logo BHC-Honda-System" style="margin-bottom: 5px;padding: 15px;background-color: #E50000">
            <a href="#" style="color: #ffffff">BHC Honda System</a>
        </div>
        <div class="box box-solid" style="opacity: 0.9;margin-bottom: 5px">
			<?php $form = ActiveForm::begin( [ 'id' => 'login-form', 'enableClientValidation' => false ] ); ?>
            <div class="box-header">
                <p class="box-title">Menu Pilihan Aplikasi</p>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <select class="form-control" id="tipe" name="tipe">
                        <?php foreach ( $typeLoginArr as $value ) : ?>
                            <?php if($value == 1) : ?>
                                <option value="1" data-imagesrc="
					<?= Yii::getAlias( '@web' ) . '/img/HSYS.png' ?>"
                                data-description="Honda Sales System">HSYS - [UNIT]
                        </option>
                            <?php elseif($value == 2) : ?>
                                <option value="2" data-imagesrc="
					<?= Yii::getAlias( '@web' ) . '/img/HVYS.png' ?>"
                                data-description="Honda Service System">HVYS - [BENGKEL]
                        </option>
                            <?php endif; ?>
                        <?php endforeach; ?>
<!--                        <option value="3" data-imagesrc="-->
<!--					--><?//= Yii::getAlias( '@web' ) . '/img/HFin.png' ?><!--"-->
<!--                                data-description="Honda Finance System">HFIN - [FINANCE]-->
<!--                        </option>-->
                    </select>
                </div>
                <div class="form-group">
					<?= Html::dropDownList( 'cabang', null, [], [
						'id'    => 'loginform-cabang',
						'class' => 'form-control',
					] ) ?>
                </div>
                <div class="form-group">
					<?= Html::submitButton( 'Login', [
						'class' => 'btn btn-primary btn-block btn-flat',
						'name'  => 'login-button'
					] ) ?>
                </div>
            </div>
			<?php ActiveForm::end(); ?>
        </div>
        <div class="box box-solid" style="opacity: 0.8">
            <div class="box-header">
                <p style="font-size: small;text-align: center;margin-bottom: 0">@2020 - Budiardjo Holding Company &
                    co </p>
                <p style="font-size: small;text-align: center">Pre Release Date : 09 November 2019</p>
            </div>
        </div>
        <!-- /.login-logo -->
        <!-- /.login-box-body -->
    </div><!-- /.login-box -->
    <div id="myModal" class="modal modal-danger fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><strong>User</strong> dan <strong>Password</strong> tidak ditemukan!</h4>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <style>
        body {
            /*background-image: url("img/background.png");*/
            /*display: block;*/
            /*width: auto;*/
            /*height: 768px;*/
            background-image: url("img/BackGroundLayer2_.jpg");
            background-repeat:no-repeat;
            background-size:cover;
            background-position:bottom;
        }

        .Rectangle-16 {
            width: 395px;
            height: 40px;
            object-fit: contain;
            border-radius: 9px;
            background-color: #ff0000;
        }

        .Image-1 {
            background-image: url("img/image-1.png");
            width: 177px;
            height: 142px;
            position: absolute;
            /*top: 31px;*/
            /*right: 56px;*/
            top: 5px;
            left: 5px;
            object-fit: contain;
            /*opacity: 0.22;*/
        }

        .Sign-in-to-start {
            /*width: 119px;*/
            height: 23px;
            object-fit: contain;
            /*font-family: OpenSans;*/
            font-size: 17px;
            font-weight: 600;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.35;
            letter-spacing: normal;
            text-align: left;
            color: #707070;
        }

        .BHC-Honda-System {
            /*width: 304px;*/
            /*height: 43px;*/
            /*width: 455px;*/
            object-fit: contain;
            /*font-family: OpenSans; !important;*/
            font-size: 32px;
            font-weight: bold;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.34;
            letter-spacing: normal;
            text-align: center;
            border-radius: 10px;
            color: #ff0000;
        }

        .login-box-body-new {
            background-image: url("img/group-52.png");
            height: 448px;
            width: 455px;
            padding: 20px;
            object-fit: contain;
        }
    </style>
<?php
$jsCabangUrl = "var cabang_url =  '" . Url::toRoute( [ 'site/cabang' ] ) . "';";
$this->registerJs(
	$jsCabangUrl,
	yii\web\View::POS_HEAD,
	'cabang-url-script'
);
$script = <<< JS

function formatRow(t){
    var originalOption = t.element;
    return "<div style='display : inline-block;width: 20%;'><img class='flag' src='" + 
    $(originalOption).data('imagesrc') + "' /></div><div style='display : inline-block;width: 40%;'>"+
    t.text+"</div><div style='display : inline-block;width: 40%;'>" + $(originalOption).data('description')+"</div>";
}

$("#tipe").select2({
templateResult: formatRow,
formatSelection: formatRow,
escapeMarkup: function(m) { return m; }
});

var fillCabang = function(){
    var option = $('#loginform-cabang');
                    option.find('option').remove().end();
    
    if($("#loginform-password").val() != '' && $("#loginform-username").val() != ''){
        // option.append('<option value="AKL" >AKL</option>');  
        $.ajax({
            type: "post",
            url: cabang_url,            
            data: $('#login-form').serialize(),
            success: function(data) {
                if(data == ''){
                 // $('#myModal').modal();
                }else{
                    var option = $('#loginform-cabang');
                    option.find('option').remove().end();
                    option.append(data);
                    option.prop("selectedIndex", 0);
                    option.trigger('change');
                }
                $("#loginform-cabang").select2({});
            }
        });   
    }
}
// $("#loginform-password").focusout(function(){
//     fillCabang();
// });
// $("#loginform-username").focusout(function(){
//     fillCabang();
// });
 $("#tipe").change(function () {
     fillCabang();
 });
 $("#loginform-cabang").change(function () {
    document.cookie = "_outlet="+btoa(this.value.toLowerCase())+";path=/";
 });
 
 fillCabang();
 
JS;
$this->registerJs( $script );
try {
	$this->registerCssFile( '@web/css/select2.min.css' );
	$this->registerJsFile( '@web/jquery/select2.min.js', [
		'depends' => [
			\yii\web\JqueryAsset::className()
		]
	] );
} catch ( \yii\base\InvalidConfigException $e ) {
}
