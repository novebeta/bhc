<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttsohd */

$params                          = '&id=' . $id . '&action=update';
$cancel = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = 'Edit - Order Penjualan: ' . $dsJual['SONoView'];
?>
<div class="ttsohd-update">
    <?= $this->render('_form', [
        'model' => $model,
        'dsJual' => $dsJual,
        'id'     => $id,
        'url'    => [
			'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
			'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttsoit/index','action' => 'update' ] ),
		]
    ]) ?>

</div>
