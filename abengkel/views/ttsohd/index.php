<?php
use abengkel\assets\AppAsset;
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '[SO] Order Penjualan';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'SONo'                     => 'SO No',
		'SINo'                     => 'SI No',
		'PONo'                     => 'PO No',
		'KodeTrans'                => 'Kode Trans',
		'PosKode'                  => 'Pos Kode',
		'KarKode'                  => 'Kar Kode',
		'tdcustomer.MotorNoPolisi' => 'No Polisi',
		'tdcustomer.CusNama'	   => 'Nama Customer',
		'tdcustomer.MotorNama'	   => 'Nama Motor',
		'LokasiKode'               => 'Lokasi Kode',
		'SOKeterangan'             => 'Keterangan',
		'Cetak'                    => 'Cetak',
		'UserID'                   => 'User ID',
	],
	'cmbTgl'    => [
		'SOTgl'                    => 'SO Tgl',
	],
	'cmbNum'    => [
		'SOSubTotal'               => 'Sub Total',
		'SODiscFinal'              => 'Disc Final',
		'SOTotal'                  => 'Total',
		'SOTerbayar'               => 'Terbayar',
	],
	'sortname'  => "SOTgl",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Ttsohd::className(), '[SO] Order Penjualan',
    [
        'url_delete' => Url::toRoute( [ 'ttsohd/delete' ] ),
    ]
), \yii\web\View::POS_READY );
