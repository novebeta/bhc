<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttsohd */

$params                          = '&id=' . $id . '&action=create';
$cancel = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = 'Tambah - Order Penjualan';
?>
<div class="ttsohd-create">
    <?= $this->render('_form', [
        'model' => $model,
        'dsJual' => $dsJual,
        'id'     => $id,
        'url'    => [
			'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
			'cancel' => $cancel,
            'detail'       => Url::toRoute( [ 'ttsoit/index','action' => 'update' ] ),
		]
    ]) ?>

</div>
