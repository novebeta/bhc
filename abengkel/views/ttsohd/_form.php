<?php
use abengkel\components\FormField;
use abengkel\models\Tdkaryawan;
use abengkel\models\Tdlokasi;
use common\components\General;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttsohd */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttsohd-form">
		<?php
		$dsJual[ 'SODiscPersen' ] = 0;
		if ( $dsJual[ 'SOTotal' ] > 0 ) {
			$dsJual[ 'SODiscPersen' ] = round( $dsJual[ 'SODiscFinal' ] / $dsJual[ 'SOTotal' ] * 100 );
		}
		$LokasiKodeCmb = ArrayHelper::map( Tdlokasi::find()->select( [ 'LokasiKode' ] )->where( [ 'LokasiStatus' => 'A', 'LEFT(LokasiNomor,1)' => '7' ] )->orderBy( 'LokasiStatus, LokasiKode' )->asArray()->all(), 'LokasiKode', 'LokasiKode' );
		$KarKodeCmb    = ArrayHelper::map( Tdkaryawan::find()->select( [ 'KarKode', 'KarNama' ] )->where( [ 'KarStatus' => 'A' ] )->orderBy( 'KarStatus,KarKode' )->all(), 'KarKode', 'KarNama' );
		$form          = ActiveForm::begin( [ 'id' => 'form_ttsohd_id', 'action' => $url[ 'update' ] ] );
		\abengkel\components\TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-9",
					      'items' => [
						      [ 'class' => "col-sm-4", 'items' => ":LabelSONo" ],
						      [ 'class' => "col-sm-8", 'items' => ":SONo" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-4", 'items' => ":LabelSOTgl" ],
						      [ 'class' => "col-sm-6", 'items' => ":SOTgl" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-7",
					      'items' => [
						      [ 'class' => "col-sm-4", 'items' => ":LabelSOJam" ],
						      [ 'class' => "col-sm-7", 'items' => ":SOJam" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-3", 'items' => ":LabelSINo" ],
						      [ 'class' => "col-sm-8", 'items' => ":SINo" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-8",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelPONo" ],
						      [ 'class' => "col-sm-6", 'items' => ":PONo" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-5", 'items' => ":LabelStok" ],
						      [ 'class' => "col-sm-9", 'items' => ":Stok" ]
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-9",
					      'items' => [
						      [ 'class' => "col-sm-4", 'items' => ":LabelKonsumen" ],
						      [ 'class' => "col-sm-10", 'items' => ":Konsumen" ],
						      [ 'class' => "col-sm-3", 'items' => ":SearchKonsumen" ],
						      [ 'class' => "col-sm-3", 'items' => ":SearchCus" ],
						      [ 'class' => "col-sm-3", 'items' => ":TambahCus" ],
						      [ 'class' => "col-sm-1" ]
					      ],
					    ],
					    [ 'class' => "form-group col-md-7",
					      'items' => [
						      [ 'class' => "col-sm-4", 'items' => ":LabelType" ],
						      [ 'class' => "col-sm-19", 'items' => ":Type" ],
						      [ 'class' => "col-sm-1" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-8",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelSales" ],
						      [ 'class' => "col-sm-21", 'items' => ":Sales" ]
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-9",
					      'items' => [
						      [ 'class' => "col-sm-4", 'items' => ":LabelNama" ],
						      [ 'class' => "col-sm-19", 'items' => ":Nama" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-7",
					      'items' => [
						      [ 'class' => "col-sm-4", 'items' => ":LabelWarna" ],
						      [ 'class' => "col-sm-19", 'items' => ":Warna" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-8",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelTC" ],
						      [ 'class' => "col-sm-21", 'items' => ":TC" ]
					      ],
					    ],
				    ],
				  ],
				  [
					  'class' => "row col-md-24",
					  'style' => "margin-bottom: 6px;",
					  'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-13",
					      'items' => [
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-4", 'items' => ":KodeBarang", 'style' => 'padding-right:1px;' ],
							        [ 'class' => "col-sm-9", 'items' => ":BarangNama", 'style' => 'padding-right:2px;' ],
							        [ 'class' => "col-sm-4", 'items' => ":Gudang", 'style' => 'padding-right:2px;' ],
							        [ 'class' => "col-sm-4", 'items' => ":Satuan" ],
							        [ 'class' => "col-sm-2", 'items' => ":BtnInfo" ],
							        [ 'class' => "col-sm-1" ],
						        ],
						      ],
						      [ 'class' => "form-group col-md-18",
						        'items' => [
							        [ 'class' => "col-sm-4", 'items' => ":LabelKeterangan" ],
							        [ 'class' => "col-sm-20", 'items' => ":Keterangan" ],
						        ],
						      ],
						      [ 'class' => "form-group col-md-6",
						        'items' => [
							        [ 'class' => "form-group col-md-24",
							          'items' => [
								          [ 'class' => "col-sm-1" ],
								          [ 'class' => "col-sm-4", 'items' => ":BtnBayar" ],
								          [ 'class' => "col-sm-1" ],
							          ],
							        ],
							        [ 'class' => "form-group col-md-24",
							          'items' => [
								          [ 'class' => "col-sm-1" ],
								          [ 'class' => "col-sm-8", 'items' => ":LabelCetak" ],
								          [ 'class' => "col-sm-12", 'items' => ":Cetak" ],
								          [ 'class' => "col-sm-1" ]
							          ],
							        ],
						        ],
						      ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-11",
					      'items' => [
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-4", 'items' => ":LabelTotalBayar" ],
							        [ 'class' => "col-sm-7", 'items' => ":TotalBayar" ],
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-3", 'items' => ":LabelSubTotal" ],
							        [ 'class' => "col-sm-9", 'items' => ":SubTotal" ],
						        ],
						      ],
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-4", 'items' => ":LabelSisaBayar" ],
							        [ 'class' => "col-sm-7", 'items' => ":SisaBayar" ],
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-3", 'items' => ":LabelDisc" ],
							        [ 'class' => "col-sm-2", 'items' => ":Disc" ],
							        [ 'class' => "col-sm-1", 'items' => ":Persen", 'style' => 'padding-left:3px;' ],
							        [ 'class' => "col-sm-6", 'items' => ":Nominal" ],
						        ],
						      ],
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-4", 'items' => ":LabelStatusBayar" ],
							        [ 'class' => "col-sm-5", 'items' => ":StatusBayar" ],
							        [ 'class' => "col-sm-2", 'items' => ":BtnStatus" ],
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-3", 'items' => ":LabelTotal" ],
							        [ 'class' => "col-sm-9", 'items' => ":Total" ]
						        ],
						      ],
					      ],
					    ],
				    ],
				  ],
			  ],
			],
            [ 'class' => "row-no-gutters",
                'items' => [
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'SONo'                     => 'SO No',
                                'SINo'                     => 'SI No',
                                'PONo'                     => 'PO No',
                                'KodeTrans'                => 'Kode Trans',
                                'PosKode'                  => 'Pos Kode',
                                'KarKode'                  => 'Kar Kode',
                                'tdcustomer.MotorNoPolisi' => 'No Polisi',
                                'tdcustomer.CusNama'	   => 'Nama Customer',
                                'tdcustomer.MotorNama'	   => 'Nama Motor',
                                'LokasiKode'               => 'Lokasi Kode',
                                'SOKeterangan'             => 'Keterangan',
                                'Cetak'                    => 'Cetak',
                                'UserID'                   => 'User ID',
                            ],
                        ])
                    ],
			        ['class' => "pull-right", 'items' => ":btnPrint :btnAction"],
                ],
            ],
			[
				":LabelSONo"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor SO</label>',
				":LabelSOTgl"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tanggal SO</label>',
				":LabelSOJam"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jam</label>',
				":LabelSINo"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No SI</label>',
				":LabelPONo"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No PO</label>',
				":LabelStok"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Stok Keluar</label>',
				":LabelKonsumen"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Konsumen</label>',
				":LabelType"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Type</label>',
				":LabelSales"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sales</label>',
				":LabelNama"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama</label>',
				":LabelWarna"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Warna</label>',
				":LabelTC"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">TC</label>',
				":LabelTotalBayar"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total Bayar</label>',
				":LabelSubTotal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sub Total</label>',
				":LabelKeterangan"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelSisaBayar"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sisa Bayar</label>',
				":LabelDisc"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Disc Final</label>',
				":Persen"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">%</label>',
				":LabelCetak"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Cetak</label>',
				":LabelStatusBayar" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status Bayar</label>',
				":LabelTotal"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total</label>',
				":SONo"             => Html::textInput( 'SONoView', $dsJual[ 'SONoView' ], [ 'class' => 'form-control', 'readonly' => 'readonly', 'tabindex' => '1' ] ) .
				                       Html::textInput( 'SONo', $dsJual[ 'SONo' ], [ 'class' => 'hidden' ] ) .
				                       Html::textInput( 'PosKode', $dsJual[ 'PosKode' ], [ 'class' => 'hidden' ] ) .
				                       Html::textInput( 'KodeTrans', $dsJual[ 'KodeTrans' ], [ 'class' => 'hidden' ] ),
				":SOTgl"            => FormField::dateInput( [ 'name' => 'SOTgl', 'config' => [ 'value' => General::asDate( $dsJual[ 'SOTgl' ] ) ] ] ),
				":SOJam"            => FormField::timeInput( [ 'name' => 'SOJam', 'config' => [ 'value' => $dsJual[ 'SOJam' ] ] ] ),
				":SINo"             => Html::textInput( 'SINo', $dsJual[ 'SINo' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":PONo"             => Html::textInput( 'PONo', $dsJual[ 'PONo' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":Stok"             => FormField::combo( 'LokasiKode', [ 'config' => [ 'value' => $dsJual[ 'LokasiKode' ], 'data' => $LokasiKodeCmb, 'pluginEvents' => [ "change" => "function() { $('#LokasiKodeTxt').val(this.value);}", ] ], 'options' => [ 'tabindex' => '3' ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":Konsumen"         => Html::textInput( 'MotorNoPolisi', $dsJual[ 'MotorNoPolisi' ], [ 'class' => 'form-control', 'maxlength' => '12', 'tabindex' => '4' ] ),
				":Type"             => Html::textInput( 'MotorNama', $dsJual[ 'MotorNama' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":Sales"            => FormField::combo( 'KarKode', [ 'config' => [ 'value' => $dsJual[ 'KarKode' ], 'data' => $KarKodeCmb ], 'options' => [ 'tabindex' => '8' ], 'extraOptions' => [ 'altLabel' => [ 'KarNama' ] ] ] ),
				":Nama"             => Html::textInput( 'CusNama', $dsJual[ 'CusNama' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":Warna"            => Html::textInput( 'MotorWarna', $dsJual[ 'MotorWarna' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":TC"               => FormField::combo( 'SOTC', [ 'config' => [ 'value' => $dsJual[ 'KodeTrans' ] ], 'options' => [ 'tabindex' => '9' ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":KodeBarang"       => Html::textInput( 'KodeBarang', '', [ 'id' => 'KodeBarang', 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":BarangNama"       => Html::textInput( 'BarangNama', '', [ 'id' => 'BarangNama', 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":Gudang"           => Html::textInput( 'LokasiKodeTxt', $dsJual[ 'LokasiKode' ], [ 'id' => 'LokasiKodeTxt', 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":Satuan"           => Html::textInput( 'lblStock', 0, [ 'type' => 'number', 'class' => 'form-control', 'style' => 'color: blue; font-weight: bold;', 'readonly' => 'readonly' ] ),
				":TotalBayar"       => FormField::numberInput( [ 'name' => 'TotalBayar', 'config' => [ 'id' => 'TotalBayar', 'value' => $dsJual[ 'TotalBayar' ], 'readonly' => 'readonly' ] ] ),
				":SisaBayar"        => FormField::numberInput( [ 'name' => 'SisaBayar', 'config' => [ 'id' => 'SisaBayar', 'value' => $dsJual[ 'SisaBayar' ], 'readonly' => 'readonly' ] ] ),
				":SubTotal"         => FormField::numberInput( [ 'name' => 'SOSubTotal', 'config' => [ 'id' => 'SOSubTotal', 'value' => $dsJual[ 'SOSubTotal' ], 'readonly' => 'readonly' ] ] ) .
				                       FormField::numberInput( [ 'name' => 'SOTotalPajak', 'config' => [ 'id' => 'SOTotalPajak', 'value' => $dsJual[ 'SOTotalPajak' ], 'displayOptions' => [ 'class' => 'hidden' ] ] ] ),
				":Keterangan"       => Html::textarea( 'SOKeterangan', $dsJual[ 'SOKeterangan' ], [ 'class' => 'form-control', 'maxlength' => '150', 'tabindex' => '10' ] ),
				":Cetak"            => Html::textInput( 'Cetak', $dsJual[ 'Cetak' ], [ 'id' => 'Cetak', 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":Disc"             => FormField::numberInput( [ 'name' => 'SODiscPersen', 'config' => [ 'id' => 'SODiscPersen', 'value' => $dsJual[ 'SODiscPersen' ], 'maskedInputOptions' => [ 'digits' => 0 ] ] ] ),
				":Nominal"          => FormField::numberInput( [ 'name' => 'SODiscFinal', 'config' => [ 'value' => $dsJual[ 'SODiscFinal' ] ] ] ),
				":StatusBayar"      => Html::textInput( 'StatusBayar', $dsJual[ 'StatusBayar' ], [ 'id' => 'StatusBayar', 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":Total"            => FormField::numberInput( [ 'name' => 'SOTotal', 'config' => [ 'id' => 'SOTotal', 'value' => $dsJual[ 'SOTotal' ], 'readonly' => 'readonly' ] ] ) .
				                       FormField::numberInput( [ 'name' => 'UangMuka', 'config' => [ 'id' => 'UangMuka', 'value' => $dsJual[ 'UangMuka' ], 'displayOptions' => [ 'class' => 'hidden' ] ] ] ) .
				                       FormField::numberInput( [ 'name' => 'SOBiayaKirim', 'config' => [ 'id' => 'SOBiayaKirim', 'value' => $dsJual[ 'SOBiayaKirim' ], 'displayOptions' => [ 'class' => 'hidden' ] ] ] ),
				":BtnBayar"         => Html::button( '<i class="fa fa-thumbs-up"></i> Bayar', [ 'class' => 'btn btn-success', 'style' => 'width:120px', 'id' => 'BtnBayar' ] ),
				":BtnInfo"          => Html::button( '<i class="fa fa-info-circle fa-lg"></i>', [ 'class' => 'btn btn-default' ] ),
				":BtnStatus"        => Html::button( '<i class="fa fa-refresh fa-lg"></i>', [ 'class' => 'btn btn-default', 'id' => 'BtnStatus' ] ),
				":SearchKonsumen"   => Html::button( '<i class="glyphicon glyphicon-search"></i>', [ 'class' => 'btn btn-default', 'id' => 'SearchKonsumen', 'tabindex' => '5' ] ),
				":TambahCus"        => Html::button( '<i class="fa fa-user-plus"></i>', [ 'class' => 'btn btn-default', 'id' => 'AddKonsumen', 'tabindex' => '7' ] ),
				":SearchCus"        => Html::button( '<i class="glyphicon glyphicon-user"></i>', [ 'class' => 'btn btn-default', 'id' => 'EditKonsumen', 'tabindex' => '6' ] ),
			],
			[
				'url_main' => 'ttsohd',
                'url_id' => $_GET['id'] ?? '',
                '_akses' => '[SO] Order Penjualan',
			]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlStatusOtomatis       = Url::toRoute( [ 'ttsohd/status-otomatis', 'SONo' => $dsJual[ 'SONo' ], ] );
$urlCustomerCreate       = Url::toRoute( [ 'tdcustomer/create', 'action' => 'create' ] );
$urlCustomerEdit         = Url::toRoute( [ 'tdcustomer/edit' ] );
$urlCheckStock           = Url::toRoute( [ 'site/check-stock-transaksi' ] );
$urlBayarOtomatis        = Url::toRoute( [ 'ttbmhd/bayar-otomatis', 'NominalBayar' => $dsJual[ 'SOTotal' ], ] );
$urlDaftarKonsumenSelect = Url::toRoute( [ 'tdcustomer/select' ] );
$urlKonsumenData         = Url::toRoute( [ 'tdcustomer/td', 'oper' => 'data' ] );
$urlBayar                = Url::toRoute( [ 'bayar/save' ] );
$urlCustomerFindById = Url::toRoute( [ 'tdcustomer/find-by-id' ] );
$urlDetail               = $url[ 'detail' ];
$urlPrint                = $url[ 'print' ];
$urlUpdate               = $url[ 'update' ];
$urlRefresh              = $url[ 'update' ] . '&oper=skip-load&SONoView=' . $dsJual[ 'SONoView' ];
$readOnly                = ( $_GET[ 'action' ] == 'view' ? 'true' : 'false' );
$barang                  = \common\components\General::cCmd(
	"SELECT BrgKode, BrgNama, BrgBarCode, BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual, BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus
                            FROM tdbarang 
                            WHERE BrgStatus = 'A' 
                            UNION
                            SELECT BrgKode, BrgNama, BrgBarCode,  BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual, BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus FROM tdBarang 
                            WHERE BrgKode IN 
                            (SELECT ttsoit.BrgKode FROM ttsoit INNER JOIN tdbarang ON ttsoit.BrgKode = tdbarang.BrgKode 
                             WHERE ttsoit.SONo = :SONo AND tdBarang.BrgStatus = 'N' GROUP BY ttsoit.BrgKode )
                            ORDER BY BrgStatus, BrgKode", [ ':SONo' => $dsJual[ 'SONo' ] ] )->queryAll();
$this->registerJsVar( 'kodeBrang', $barang );
$this->registerJsVar( 'namaBarang', $barang );
$this->registerJsVar( '__idCustomer', $dsJual[ 'MotorNoPolisi' ] );
$urlBarangAjax = Url::toRoute( [ 'tdbarang/ajax-combo' ] );
$resultKas     = \abengkel\models\Ttkmhd::find()->callSP( [ 'Action' => 'Insert' ] );
$resultBank    = \abengkel\models\Ttbmhd::find()->callSP( [ 'Action' => 'Insert' ] );
$this->registerJs( <<< JS
        
        window.BHC_BAYAR_KMNo = '{$resultKas['KMNo']}';
        window.BHC_BAYAR_BMNo = '{$resultBank['BMNo']}';

         function HitungTotal(){
         	let SOTotalPajak = $('#detailGrid').jqGrid('getCol','SOPajak',false,'sum');
         	$('#SOTotalPajak').utilNumberControl().val(SOTotalPajak);
         	let SOSubTotal = $('#detailGrid').jqGrid('getCol','Jumlah',false,'sum');
         	$('#SOSubTotal').utilNumberControl().val(SOSubTotal);
         	HitungBawah();
         }
         window.HitungTotal = HitungTotal;
         
         function HitungBawah(){
         	let SODiscPersen = parseFloat($('#SODiscPersen').val());                 
         	let SOSubTotal = parseFloat($('#SOSubTotal').val());      
         	let SODiscFinal = SODiscPersen * SOSubTotal / 100;                 
         	$('#SODiscFinal').utilNumberControl().val(SODiscFinal);
         	let SOTotalPajak = parseFloat($('#SOTotalPajak').val());                 
         	let SOBiayaKirim = parseFloat($('#SOBiayaKirim').val());                 
         	let UangMuka = parseFloat($('#UangMuka').val());                 
         	let SOTotal = SOSubTotal + SOTotalPajak + SOBiayaKirim - SODiscFinal - UangMuka;
         	$('#SOTotal').utilNumberControl().val(SOTotal);
         	let TotalBayar = parseFloat($('#TotalBayar').val());   
         	let SisaBayar = SOTotal - TotalBayar;
         	$('#SisaBayar').utilNumberControl().val(SisaBayar);
         	if(SisaBayar  > 0 || SOTotal === 0){         	    
         	    $('#StatusBayar').css({"background-color" : "yellow"});
         	    if(TotalBayar === 0) $('#StatusBayar').css({"background-color" : "pink"});
         	} else if(SisaBayar <= 0){
         	    $('#StatusBayar').css({"background-color" : "lime"});
         	}         	
         }         
         window.HitungBawah = HitungBawah;
         
         function checkStok(BrgKode,BarangNama){
           $.ajax({
                url: '$urlCheckStock',
                type: "POST",
                data: { 
                    BrgKode: BrgKode,
                },
                success: function (res) {
                    $('[name=LokasiKode]').val(res.LokasiKode);
                    $('#KodeBarang').val(BrgKode);
                    $('[name=BarangNama]').val(BarangNama);
                    $('[name=lblStock]').val(res.SaldoQty);
                },
                error: function (jXHR, textStatus, errorThrown) {
                },
                async: false
            });
          }
          window.checkStok = checkStok;
         
     $('#detailGrid').utilJqGrid({
        height: 185,
        editurl: '$urlDetail',
        extraParams: {
            SONo: $('input[name="SONo"]').val()
        },
        rownumbers: true,  
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,   
        colModel: [
            { template: 'actions'  },
            {
                name: 'BrgKode',
                label: 'Kode',
                editable: true,
                width: 155,
                editor: {
                    type: 'select2',
                    data: $.map(kodeBrang, function (obj) {
                      obj.id = obj.BrgKode;
                      obj.text = obj.BrgKode;
                      return obj;
                    }),
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'SOHrgJual'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgJual));
                            $('[id="'+rowid+'_'+'SOHrgBeli'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgBeli));
                            $('[id="'+rowid+'_'+'BrgGroup'+'"]').val(data.BrgGroup);
                            window.hitungLine();
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').val(data.BrgNama); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').trigger('change');
                            checkStok($(this).val(),$('[id="'+rowid+'_'+'BrgNama'+'"]').val());
						},
						'change': function(){      
                            let rowid = $(this).attr('rowid');
                            checkStok($(this).val(),$('[id="'+rowid+'_'+'BrgNama'+'"]').val());
						}
                    }                 
                }
            },
            {
                name: 'BrgNama',
                label: 'Nama Barang',
                width: 250,
                editable: true,
                editor: {
                    type: 'select2',
                    data: $.map(namaBarang, function (obj) {
                      obj.id = obj.BrgNama;
                      obj.text = obj.BrgNama;
                      return obj;
                    }),
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'SOHrgJual'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgJual));
                            $('[id="'+rowid+'_'+'SOHrgBeli'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgBeli));
                            $('[id="'+rowid+'_'+'BrgGroup'+'"]').val(data.BrgGroup);
                            window.hitungLine();
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').val(data.BrgKode); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').trigger('change');
						}
                    }              
                }
            },
            {
                name: 'SOQty',
                label: 'Qty',
                width: 75,
                editable: true,     
                template: 'number'           
            },
            {
                name: 'BrgSatuan',
                label: 'Satuan',
                width: 75,
                editable: true,
                editor:{
                    readonly: true
                }
            },
            {
                name: 'SOHrgJual',
                label: 'Harga Part',
                width: 100,
                editable: true,
                template: 'money'
            },
            {
                name: 'SOHrgBeli',
                label: 'Harga Part',
                width: 100,
                editable: true,
                template: 'money',
                hidden: true
            },
            {
                name: 'Disc',
                label: 'Disc %',
                width: 75,
                editable: true,
                template: 'money'
            },            
            {
                name: 'SODiscount',
                label: 'Nilai Disc',
                width: 100,
                editable: true,
                template: 'money'
            },
            {
                name: 'Jumlah',
                label: 'Jumlah',
                width: 100,
                editable: true,
                template: 'money'
            },
            {
                name: 'SOPajak',
                label: 'Pajak',
                editable: true,
                template: 'money',
                hidden: true
            },
            {
                name: 'BrgGroup',
                label: 'Group',
                editable: true,
                hidden: true
            },
            {
                name: 'SINo',
                label: 'SINo',
                editable: true,
                hidden: true
            },
            {
                name: 'PONo',
                label: 'PONo',
                editable: true,
                hidden: true
            },
             {
                name: 'SOAuto',
                label: 'SOAuto',
                editable: true,
                hidden: true,
            },
        ],
        listeners: {
            afterLoad: function (data) {
             HitungTotal();
            }    
        },
        onSelectRow : function(rowid,status,e){ 
            if (window.rowId !== -1) return;
            if (window.rowId_selrow === rowid) return;
            if(rowid.includes('new_row') === true){
                $('[name=KodeBarang]').val('--');
                $('[name=LokasiKode]').val('Kosong');
                $('[name=BarangNama]').val('--');
                $('[name=lblStock]').val(0);
                return ;
            };
            let r = $('#detailGrid').utilJqGrid().getData(rowid).data;
			checkStok(r.BrgKode,r.BrgNama);
			window.rowId_selrow = rowid;
        }
     }).init().fit($('.box-body')).load({url: '$urlDetail'});
     window.rowId_selrow = -1;
     function hitungLine(){
            let Disc = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Disc'+'"]').val()));
            let SOHrgJual = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'SOHrgJual'+'"]').val()));
            let SOQty = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'SOQty'+'"]').val()));
            let SODiscount = Disc*SOHrgJual*SOQty / 100;
            let Jumlah = SOHrgJual * SOQty - SODiscount;
            $('[id="'+window.cmp.rowid+'_'+'SODiscount'+'"]').utilNumberControl().val(SODiscount);
            $('[id="'+window.cmp.rowid+'_'+'Jumlah'+'"]').utilNumberControl().val(Jumlah);
            HitungTotal();
         }          
         window.hitungLine = hitungLine;
        
	 $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
	     window.cmp.rowid = rowid;    
          if(rowid.includes('new_row')){
              var BrgKode = $('[id="'+rowid+'_'+'BrgKode'+'"]');
              var dtBrgKode = BrgKode.select2('data');
              if (dtBrgKode.length > 0){
                  var dt = dtBrgKode[0];
                  BrgKode.val(dt.BrgKode).trigger('change').trigger({
                    type: 'select2:select',
                    params: {
                        data: dt
                    }
                });
              }
            $('[id="'+rowid+'_'+'SOQty'+'"]').val(1);
          }
          
          $('[id="'+window.cmp.rowid+'_'+'SOQty'+'"]').on('keyup', function (event){
              hitungLine();
          });
          $('[id="'+window.cmp.rowid+'_'+'SOHrgJual'+'"]').on('keyup', function (event){
              hitungLine();
          });
          $('[id="'+window.cmp.rowid+'_'+'Disc'+'"]').on('keyup', function (event){
              hitungLine();
          });
         hitungLine();
	 });
	 
	 $('#SearchKonsumen').click(function () {
        window.util.app.dialogListData.show({
                title: 'Daftar Konsumen',
                url: '$urlDaftarKonsumenSelect'
            },
            function (data) {
                console.log(data);
                // $('input[name="SDNo"]').val(data.data.SDNo);
                $.ajax({
                    type: 'POST',
                    url: '$urlKonsumenData',
                    data: {
                        'oper': 'data',
                        'id': btoa(data.id)
                    }
                }).then(function (cusData) {
                   console.log(cusData);
                   Object.keys(cusData).forEach(function(key,index) {
                            let cmp = $('input[name="'+key+'"]');
                            cmp.val(Object.values(cusData)[index]);
                   });
                });
            });
        });
	
	$('#btnSave').click(function (event) {	 
	    let Nominal = parseFloat($('#SOSubTotal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Subtotal tidak boleh 0', size: 'small'});
               return;
           }   
        $('#form_ttsohd_id').attr('action','{$url['update']}');
        $('#form_ttsohd_id').submit();
    });	  
	
	$('#SODiscPersen').change(function (event) {
	  HitungBawah();
	});  
	
	
	$('#BtnBayar').click(function() {	    	    
	    let MotorNoPolisi = $('input[name="MotorNoPolisi"]').val();
	    let CusNama = $('input[name="CusNama"]').val();
	    let SONoView = $('input[name="SONoView"]').val();
	    let SONo = $('input[name="SONo"]').val();
	    let SisaBayar = parseFloat($('#SisaBayar').utilNumberControl().val());
        if(SONo.includes('=')){
             bootbox.alert({message:'Silahkan simpan transaksi ini dulu.',size: 'small'});
	        return;
        }
	    if (SisaBayar <= 0){
	        bootbox.alert({message:'Transaksi ini sudah terlunas',size: 'small'});
	        return;
	    }
	    $('#MyKodeTrans').val('SO');
	    $('#MyKode').val(MotorNoPolisi);
	    $('#MyNama').val(CusNama);
	    $('#MyNoTransaksi').val(SONo);
	    $('#BMMemo').val("Pembayaran [SO] Order Penjualan No: "+SONoView+" - "+MotorNoPolisi+" - "+CusNama);
	    $('#BMNominal').utilNumberControl().val(SisaBayar);
	    $('#BMBayar').utilNumberControl().val(SisaBayar);
        $('#modalBayar').modal('show');
    });
	
	$('#btnSavePopup').click(function(){
	    $.ajax({
            type: 'POST',
            url: '$urlBayar',
            data: $('#form_bayar_id').utilForm().getData(true)
        }).then(function (cusData) {
          location.reload();
        });
	});
	
	function onFocus(){
        window.location.href = "{$urlRefresh}"; 
    };


    let fillCustomer = function(id, data) {
        __idCustomer = id;
        $('[name=MotorNoPolisi]').val(data.MotorNoPolisi);
        $('[name=CusNama]').val(data.CusNama);
        $('[name=SDPembawaMotor]').val(data.CusNama);
        $('[name=MotorWarna]').val(data.MotorWarna);
        $('[name=MotorNama]').val(data.MotorNama);
    }

	$('[name=MotorNoPolisi]').change(function(){
        var MotorNoPolisi = $(this).val();
        $.ajax({
            url: '$urlCustomerFindById',
            type: "GET",
            data: { id: $(this).val() },
            success: function (res) {
                if(res !== null){ 
                    fillCustomer(res.MotorNoPolisi, res); 
                }else{
                    window.openCustomer('--');
                    $("body").removeClass("loading");
                    window._ajaxCounter = 0;
                    $('[name="Tdcustomer[MotorNoPolisi]"]').val(MotorNoPolisi);
                } 
            },
            error: function (jXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            },
            async: false
        });
    });
	
	$('#EditKonsumen').click(function() {
	    let MotorNoPolisi = $('input[name="MotorNoPolisi"]').val();
        window.openCustomer(MotorNoPolisi);
    });

    $('#AddKonsumen').click(function() {
        window.openCustomer('--');
    });		
    
    $('#BtnStatus').click(function() {
        window.util.app.popUpForm.show({
        title: "Daftar Transaksi Keuangan yang melibatkan Nomor SO : "+$('input[name="SONo"]').val(),
        size: 'small',
        url: '$urlStatusOtomatis',
        width: 900,
        height: 320
        },
        function(data) {
            console.log(data);
        });
    });
    
    $('[name=SOTgl]').utilDateControl().cmp().attr('tabindex', '2');
JS
);
echo $this->render( '../_formBayar' );
echo $this->render( '../_formCustomer' );