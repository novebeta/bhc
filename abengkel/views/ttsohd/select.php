<?php
use abengkel\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = $title;
$this->params[ 'breadcrumbs' ][] = $this->title;
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs(\abengkel\models\Ttsohd::class, $title, $options ),	\yii\web\View::POS_READY );
$urlDetail = Url::toRoute( [ 'ttsoit/index' ] );
$this->registerJs( <<< JS
jQuery(function ($) {
    if($('#jqGridDetail').length) {
        $('#jqGridDetail').utilJqGrid({
            url: '$urlDetail',
            height: 240,
            readOnly: true,
            rownumbers: true,
            colModel: [
                {
                    name: 'BrgKode',
                    label: 'BrgKode',
                    width: 100
                },
               	{
	                name: 'BrgNama',
	                label: 'BrgNama',
	                width: 250,
	            },
	            {
	                name: 'SOQty',
	                label: 'Qty',
	                width: 100,
	            },
	            {
	                name: 'BrgSatuan',
	                label: 'Satuan',
	                width: 160,
	            },
	            {
	                name: 'SOHrgJual',
	                label: 'Harga Part',
	                width: 160,
	                hidden: true ,
	            },
                    {
                    name: 'Disc',
                    label: 'Disc %',
                    width: 75,
                    editable: true,
                    template: 'number'
                    },            
                    {
                        name: 'SODiscount',
                        label: 'Nilai Disc',
                        width: 100,
                        editable: true,
                        template: 'money'
                    },
                    {
                        name: 'Jumlah',
                        label: 'Jumlah',
                        width: 100,
                        editable: true,
                        template: 'money'
                    },     
                    {
                        name: 'JenisMat',
                        label: 'JenisMat',
                        width: 100,
                        editable: true,
                        template: 'money'
                    },      
            ],
	        multiselect: gridFn.detail.multiSelect(),
	        onSelectRow: gridFn.detail.onSelectRow,
	        onSelectAll: gridFn.detail.onSelectAll,
	        ondblClickRow: gridFn.detail.ondblClickRow,
	        loadComplete: gridFn.detail.loadComplete,
	        listeners: {
                afterLoad: function(grid, response) {
                    $("#cb_jqGridDetail").trigger('click');
                }
            }
        }).init().setHeight(gridFn.detail.height);
    }
});
JS, \yii\web\View::POS_READY );