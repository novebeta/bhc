<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttkkhd */
$params                          = '&id=' . $id . '&action=create';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = 'Tambah - Kas Keluar Ke Bank';
?>
<div class="ttkkhd-create">
    <?= $this->render('_kas-keluar-ke-bank-form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'id' => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/kas-keluar-ke-bank-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
        ]
    ]) ?>
</div>
