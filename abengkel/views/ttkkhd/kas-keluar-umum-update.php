<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttkkhd */
$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = 'Edit - Kas Keluar Umum : ' . $dsTUang['KKNoView'];
?>
<div class="ttkkhd-update">
    <?= $this->render('_kas-keluar-umum-form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'id'     => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/kas-keluar-umum-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttkkitcoa/index','action' => 'create' ] ),
        ]
    ]) ?>

</div>
