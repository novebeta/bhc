<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttkkhd */

$params                          = '&id=' . $id . '&action=create';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = 'Tambah - Kas Keluar Pengerjaan Luar';
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Kas Keluar Pengerjaan Luar', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="ttkkhd-create">
    <?= $this->render('_kas-keluar-order-pengerjaan-luar', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'id'     => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/kas-keluar-order-pengerjaan-luar-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
        ]
    ]) ?>
</div>
