<?php
use abengkel\components\FormField;
use abengkel\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use common\components\General;
AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttkkhd */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
?>
    <div class="ttkkhd-form">
        <?php
        $form = ActiveForm::begin(['id' => 'form_ttkkhd_id','action' => $url[ 'update' ] ]);
        \abengkel\components\TUi::form(
            ['class' => "row-no-gutters",
                'items' => [
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "form-group col-md-17",
                                'items' => [
                                    ['class' => "col-sm-2",'items' => ":LabelKKNo"],
                                    ['class' => "col-sm-4",'items' => ":KKNo"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-2",'items' => ":LabelKKTgl"],
                                    ['class' => "col-sm-3",'items' => ":KKTgl"],
                                    ['class' => "col-sm-4",'items' => ":KKJam",'style' => 'padding-left:1px;'],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-2",'items' => ":LabelJenis"],
                                    ['class' => "col-sm-3",'items' => ":Jenis"],
                                ],
                            ],
                            ['class' => "form-group col-md-7",
                                'items' => [
                                    ['class' => "col-sm-4",'items' => ":LabelTrans"],
                                    ['class' => "col-sm-20",'items' => ":Trans"],
                                ],
                            ],
                        ],
                    ],
                    [
                        'class' => "row col-md-24",
                        'style' => "margin-bottom: 6px;",
                        'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "form-group col-md-17",
                                'items' => [
                                    ['class' => "col-sm-3",'items' => ":LabelKode"],
                                    ['class' => "col-sm-20",'items' => ":Kode"]
                                ],
                            ],
                            ['class' => "form-group col-md-7",
                                'items' => [
                                    ['class' => "col-sm-7",'items' => ":LabelNominal"],
                                    ['class' => "col-sm-17",'items' => ":Nominal"],
                                ],
                            ],
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "form-group col-md-17",
                                'items' => [
                                    ['class' => "col-sm-3",'items' => ":LabelNoTrans"],
                                    ['class' => "col-sm-5",'items' => ":NoTrans"],
                                    ['class' => "col-sm-2",'items' => ":BtnInfo"],
                                    ['class' => "col-sm-2",'items' => ":LabelSupplier"],
                                    ['class' => "col-sm-11",'items' => ":Supplier"],

                                ],
                            ],
                            ['class' => "form-group col-md-7",
                                'items' => [
                                    ['class' => "col-sm-7",'items' => ":LabelTerima"],
                                    ['class' => "col-sm-17",'items' => ":Terima"],
                                ],
                            ],
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "col-sm-2",'items' => ":LabelKeterangan"],
                            ['class' => "col-sm-22",'items' => ":Keterangan",'style' => 'padding-left:5px;']
                        ],
                    ],
                ],
            ],
            [ 'class' => "row-no-gutters",
                'items' => [
                    ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                        'items'  => [
                            ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":JT"],
                            ['class' => "col-sm-4 pull-right", 'items' => ":LabelJT"],
                        ],
                    ],
                    [ 'class' => "row-no-gutters",
                        'items' => [
                            [
                                'class' => "col-md-12 pull-left",
                                'items' => $this->render( '../_nav', [
                                    'url'=> $_GET['r'],
                                    'options'=> [
                                        'KKNo'      => 'No KK',
                                        'KKJenis'   => 'Jenis',
                                        'KodeTrans' => 'Kode Trans',
                                        'SupKode'   => 'Supplier',
                                        'KasKode'   => 'Kode Kas',
                                        'KKPerson'  => 'Person',
                                        'KKMemo'    => 'Keterangan',
                                        'NoGL'      => 'No Gl',
                                        'Cetak'     => 'Cetak',
                                        'UserID'    => 'User ID',
                                        'PosKode'   => 'Pos Kode',
                                    ],
                                ])
                            ],
                        ]
                    ],
                    ['class' => "col-md-12 pull-right",
                        'items' => [
                            [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
                        ]
                    ]
                ]
            ],
            [
                ":LabelKKNo"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">No KK</label>',
                ":LabelKKTgl"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl KK</label>',
                ":LabelKode"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode</label>',
                ":LabelKeterangan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
                ":LabelTrans"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Trans</label>',
                ":LabelJenis"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
                ":LabelNominal"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
                ":LabelJT"           => \common\components\General::labelGL( $dsTUang[ 'NoGL' ],'JT' ),
                ":LabelTerima"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Diterima Oleh</label>',
                ":LabelNoTrans"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Trans</label>',
                ":LabelSupplier"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Supplier</label>',

                ":KKNo"        => Html::textInput( 'KKNoView', $dsTUang[ 'KKNoView' ], [ 'class' => 'form-control','readonly' => 'readonly' ] ) .
                                  Html::textInput( 'KKNo', $dsTUang[ 'KKNo' ], [ 'class' => 'hidden' ] ),
                ":KKTgl"       => FormField::dateInput( [ 'name' => 'KKTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'KKTgl' ] )] ] ),
                ":KKJam"       => FormField::timeInput( [ 'name' => 'KKJam', 'config' => [ 'value' => $dsTUang[ 'KKTgl' ] ]] ),
                ":Kode"        => FormField::combo( 'NoAccount', [  'name'=> 'KasKode','config' => ['value' => $dsTUang['KasKode'], 'data'=> \abengkel\models\Traccount::find()->kas() ] ] ),
                ":Supplier"    => FormField::combo( 'SupKode', ['config'=> ['value' =>$dsTUang['SupKode'] ],'extraOptions' => ['altLabel'=>[ 'SupNama' ]]] ),
                ":JT"          => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control','readOnly'=>true ] ),
                ":Keterangan"  => Html::textInput( 'KKMemo', $dsTUang[ 'KKMemo' ], [ 'class' => 'form-control', 'maxlength' => '150' ] ),
                ":Nominal"     => FormField::numberInput( [ 'name' => 'KKNominal', 'config' => [ 'value' => $dsTUang[ 'KKNominal' ] ] ] ),
                ":NoTrans"     => Html::textInput( 'KKLink', $dsTUang[ 'KKLink' ], [ 'class' => 'form-control', 'readonly' => true  ] ),
                ":Terima"      => Html::textInput( 'KKPerson', $dsTUang[ 'KKPerson' ], [ 'class' => 'form-control', 'maxlength' => '50' ] ),
                ":Jenis"       => FormField::combo( 'KKJenis', [ 'config' => [ 'value' => $dsTUang[ 'KKJenis' ], 'data' => [
                                    'Tunai'=>	'Tunai',
                                    'DP Kas'=>	'DP Kas',
                                    'Kas Kredit'=>	'Kas Kredit',
                                    'DP Transfer'=> 'DP Transfer',
                                    'Kartu Debit'=>	'Kartu Debit',
                                    'Kartu Kredit'=> 'Kartu Kredit',
                                    'Transfer'=>	'Transfer'] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
                ":Trans"       => FormField::combo( 'KodeTrans', [ 'config' => [ 'value' => $dsTUang[ 'KodeTrans' ], 'data' => [
                                    'UM'=>	'UM - Umum',
                                    'BM'=>	'BM - KK Ke Bank',
                                    'BY'=>	'BY - Biaya',
                                    'PI'=>  'PI - Pembelian',
                                    'SR'=>  'SR - Retur Penjualan',
                                    'PL'=>  'PL -Pengerjaan Luar',
                                    'H1'=>  'H1 - KK Ke H1',] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
                ":BtnInfo"     => '<button type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>',
            ],
            [
                'url_main' => 'ttkkhd',
                'url_id' => $id,
                'url_delete'        => Url::toRoute(['ttkkhd/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
        );
        ActiveForm::end();
        ?>
    </div>
<?php
$urlAdd         = \abengkel\models\Ttkkhd::getRoute($dsTUang[ 'KodeTrans' ],['action'=>'create'] )[ 'create' ];
$urlIndex       = \abengkel\models\Ttkkhd::getRoute($dsTUang[ 'KodeTrans' ],[ 'id' => $id ] )[ 'index' ];
$urlDetail      = $url[ 'detail' ];
$readOnly       = ( $_GET['action'] == 'view' ? 'true' : 'false' );
$urlLoop        = Url::toRoute(['ttkkit/index']);
$urlPopup       = Url::toRoute( [ 'ttkkhd/order', 'KodeTrans' => $dsTUang[ 'KodeTrans' ]] );
switch ($dsTUang[ 'KodeTrans' ]){
    case 'PI':
        $title = 'Daftar Purchase Invoice untuk Kas Keluar Nomor : ';
        break;
    case 'SR':
        $title = 'Daftar Sales Retur untuk Kas Masuk Nomor : ';
        break;
}
$this->registerJsVar('pKKNominal',$dsTUang[ 'KKNominal' ]);
$this->registerJs( <<< JS
     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 225,
        navButtonTambah: false,
        extraParams: {
            KKNo: $('input[name="KKNo"]').val()
        },
        rownumbers: true,  
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,        
        colModel: [
            { template: 'actions'  },
            {
                name: 'KKLink',
                label: 'No',
                width: 133,
                editable: true,
                editor: {
                    type: 'text',
                    readOnly:true
                }
            },
            {
                name: 'TglLink',
                label: 'Tanggal',
                width: 130,
                editable: false,
            },
            {
                name: 'BiayaNama',
                label: 'Kode',
                editable: false,
                width: 120,
            },     
            {
                name: 'NamaTerang',
                label: 'Nama',
                editable: false,
                width: 150,
            },      
            {
                name: 'Total',
                label: 'Total',
                editable: true,
                width: 100,
                template: 'money',
                editor: {
                    readOnly:true
                }
            },   
            {
                name: 'Terbayar',
                label: 'Terbayar',
                editable: false,
                width: 100,
                template: 'money',
                editor: {
                    readOnly:true
                }
            },     
            {
                name: 'KKBayar',
                label: 'Bayar',
                width: 100,
                editable: true,
                template: 'money',
                editrules:{
                    custom:true, 
                    custom_func: function(value) {
                        var d = $('#detailGrid').utilJqGrid().getData(window.cmp.rowid); 
                        var KKBayar = window.util.number.getValueOfEuropeanNumber(value);
                        var Total = window.util.number.getValueOfEuropeanNumber(d.data.Total);
                        var Terbayar = window.util.number.getValueOfEuropeanNumber(d.data.Terbayar);
                        window.hitungLine();                        
                        if (KKBayar === 0) 
                            return [false,"Silahkan mengisi bayar."];  
                        else {                    
                            var sisa = Total - Terbayar;
                            if (KKBayar > sisa){
                                $('[id="'+window.cmp.rowid+'_'+'KKBayar'+'"]').val(sisa);
                               return [false,"Nilai Bayar maksimal : " + window.util.number.format(sisa)];   
                            }else{
                                return [true,""];  
                            }
                        }
                    }
                }
            },
            {
                name: 'Sisa',
                label: 'Sisa',
                width: 100,
                editable: true,
                template: 'money',
                editor: {
                    readOnly:true
                }
            },
        ],
        listeners: {
             afterLoad: function (data) {
                 // let Total = $('#detailGrid').jqGrid('getCol','Total',false,'sum');
                let Total = $('#detailGrid').jqGrid('getCol','KKBayar',false,'sum');
                if(parseFloat(pKKNominal) == 0){
                    $('#KKNominal-disp').utilNumberControl().val(Total);
                }
            }
        },
        onSelectRow : function(rowid,status,e){
			window.rowId_selrow = rowid;
        }      
     }).init().fit($('.box-body')).load({url: '$urlDetail'});

    function hitungLine(){
            let KKBayar = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'KKBayar'+'"]').val()));
            let Total = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Total'+'"]').val()));
            let Sisa = Total - KKBayar;
            $('[id="'+window.cmp.rowid+'_'+'Sisa'+'"]').utilNumberControl().val(Sisa);
        }          
        window.hitungLine = hitungLine;

    $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
        window.cmp.rowid = rowid;  
        $('[id="'+window.cmp.rowid+'_'+'KKBayar'+'"]').on('keyup', function (event){
              hitungLine();
          });
    });

    $('#detailGrid').jqGrid('navButtonAdd',"#detailGrid-pager",{ 
        caption: ' Tambah',
        buttonicon: 'glyphicon glyphicon-plus',
        onClickButton:  function() {
            window.util.app.dialogListData.closeTriggerFromIframe = function(){ 
                var iframe = window.util.app.dialogListData.iframe();
                var mainGrid = iframe.contentWindow.jQuery('#jqGrid').jqGrid();
                window.util.app.dialogListData.gridFn.mainGrid = mainGrid;
                window.util.app.dialogListData.gridFn.var.multiSelect = true;
                var selRowId = window.util.app.dialogListData.gridFn.getSelectedRow();
                $.ajax({
                     type: "POST",
                     url: '$urlLoop',
                     data: {
                         oper: 'batch',
                         data: selRowId,
                         header: {
                             'KKNo': $('[name="KKNo"]').val()
                         }
                     },
                     success: function(data) {
                        mainGrid.jqGrid().trigger('reloadGrid');
                     },
                   });
                return false;
            }
            window.util.app.dialogListData.show({
                title: '$title {$dsTUang[ "KKNoView" ]}',
                url: '$urlPopup'
            },function(){ 
                window.location.href = "{$url['update']}&oper=skip-load&KKNoView={$dsTUang['KKNoView']}"; 
            });
            var iframe = window.util.app.dialogListData.iframe();
            var mainGrid = iframe.contentWindow.jQuery('#jqGrid').jqGrid();
            mainGrid.bind("jqGridAfterSaveCell", function (e, rowid, orgClickEvent) {
                console.log(rowid);
            });
        }, 
        position: "last", 
        title:"", 
        cursor: "pointer"
    });
	
	$('#btnSave').click(function (event) {	    
	    let Nominal = parseFloat($('#KKNominal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Nominal tidak boleh 0', size: 'small'});
               return;
           }
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttkkhd_id').attr('action','{$url['update']}');
        $('#form_ttkkhd_id').submit();
    }); 
	
	$('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  });
	
	$('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
	
	$('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
JS
);



