<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttkkhd */
$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = "Edit - $title : " . $dsTUang['KKNoView'];

?>
<div class="ttkkhd-update">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'id'     => $id,
        'view' => $view,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/' .$view.'-update'. $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttkkit/index','action' => 'create' ] ),
        ]
    ]) ?>

</div>
