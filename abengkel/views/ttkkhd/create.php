<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttkkhd */

$params                          = '&id=' . $id . '&action=create';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = "Tambah - $title";
?>
<div class="ttkkhd-create">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'id'     => $id,
        'view' => $view,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/' .$view.'-update'. $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => '',
        ]
    ]) ?>
</div>
