<?php
use abengkel\assets\AppAsset;
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Kas Keluar Ke H1';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
    'cmbTxt'    => [
        'KKNo'      => 'No KK',
        'KKJenis'   => 'Jenis',
        'KodeTrans' => 'Kode Trans',
        'SupKode'   => 'Supplier',
        'KasKode'   => 'Kode Kas',
        'KKPerson'  => 'Person',
        'KKMemo'    => 'Keterangan',
        'NoGL'      => 'No Gl',
        'Cetak'     => 'Cetak',
        'UserID'    => 'User ID',
        'PosKode'   => 'Pos Kode',
    ],
    'cmbTgl'    => [
        'KKTgl'     => 'Kk Tgl',
    ],
    'cmbNum'    => [
        'KKNominal' => 'Kk Nominal',
    ],
    'sortname'  => "KKTgl",
    'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Ttkkhd::className(), 'Kas Keluar Ke H1' , [
    'url_add'    => \yii\helpers\Url::toRoute([ 'ttkkhd/kas-keluar-ke-h1-create' ,'action'=>'create']),
    'url_update' => \yii\helpers\Url::toRoute([ 'ttkkhd/kas-keluar-ke-h1-update' ,'action'=>'update']),
    'url_delete' => \yii\helpers\Url::toRoute([ 'ttkkhd/delete' ]),
] ), \yii\web\View::POS_READY );
