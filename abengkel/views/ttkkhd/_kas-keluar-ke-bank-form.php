<?php
use abengkel\components\FormField;
use common\components\Custom;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use common\components\General;
\abengkel\assets\AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttkkhd */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
?>
<div class="ttkkhd-form">
        <?php
        $form = ActiveForm::begin(['id' => 'form_ttkkhd_id','action' => $url[ 'update' ] ]);
        \abengkel\components\TUi::form(
            ['class' => "row-no-gutters",
                'items' => [
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "form-group col-md-17",
                                'items' => [
                                    ['class' => "col-sm-3",'items' => ":LabelKKNo"],
                                    ['class' => "col-sm-4",'items' => ":KKNo"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-2",'items' => ":LabelKKTgl"],
                                    ['class' => "col-sm-3",'items' => ":KKTgl"],
                                    ['class' => "col-sm-4",'items' => ":KKJam",'style' => 'padding-left:1px;'],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-2",'items' => ":LabelJenis"],
                                    ['class' => "col-sm-3",'items' => ":Jenis"],
                                ],
                            ],
                            ['class' => "form-group col-md-7",
                                'items' => [
                                    ['class' => "col-sm-7",'items' => ":LabelTrans"],
                                    ['class' => "col-sm-17",'items' => ":Trans"],
                                ],
                            ],
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "form-group col-md-17",
                                'items' => [
                                    ['class' => "col-sm-3",'items' => ":LabelKode"],
                                    ['class' => "col-sm-20",'items' => ":Kode"]
                                ],
                            ],
                            ['class' => "form-group col-md-7",
                                'items' => [
                                    ['class' => "col-sm-7",'items' => ":LabelNominal"],
                                    ['class' => "col-sm-17",'items' => ":Nominal"],
                                ],
                            ],
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "form-group col-md-17",
                                'items' => [
                                    ['class' => "col-sm-3",'items' => ":LabelNoTrans"],
                                    ['class' => "col-sm-5",'items' => ":NoTrans"],
                                    ['class' => "col-sm-2",'items' => ":BtnInfo"],
                                    ['class' => "col-sm-2",'items' => ":LabelSupplier"],
                                    ['class' => "col-sm-11",'items' => ":Supplier"],

                                ],
                            ],
                            ['class' => "form-group col-md-7",
                                'items' => [
                                    ['class' => "col-sm-7",'items' => ":LabelTerima"],
                                    ['class' => "col-sm-17",'items' => ":Terima"],
                                ],
                            ],
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "col-sm-2",'items' => ":LabelKeterangan"],
                            ['class' => "col-sm-22",'items' => ":Keterangan",'style' => 'padding-left:5px;']
                        ],
                    ],
                ],
            ],
            [ 'class' => "row-no-gutters",
                'items' => [
                    ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                        'items'  => [
                            ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":JT"],
                            ['class' => "col-sm-4 pull-right", 'items' => ":LabelJT"],
                        ],
                    ],
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'KKNo'      => 'No KK',
                                'KKJenis'   => 'Jenis',
                                'KodeTrans' => 'Kode Trans',
                                'SupKode'   => 'Supplier',
                                'KasKode'   => 'Kode Kas',
                                'KKPerson'  => 'Person',
                                'KKMemo'    => 'Keterangan',
                                'NoGL'      => 'No Gl',
                                'Cetak'     => 'Cetak',
                                'UserID'    => 'User ID',
                                'PosKode'   => 'Pos Kode',
                            ],
                        ])
                    ],
                    ['class' => "col-md-12 pull-right",
                        'items' => [
                            [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
                        ]
                    ]
                ]
            ],
            [
                ":LabelKKNo"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">No KK</label>',
                ":LabelKKTgl"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl KK</label>',
                ":LabelKode"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode</label>',
                ":LabelKeterangan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
                ":LabelTrans"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Trans</label>',
                ":LabelJenis"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
                ":LabelNominal"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
                ":LabelJT"           => \common\components\General::labelGL( $dsTUang[ 'NoGL' ], 'JT' ),
                ":LabelTerima"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Diterima Oleh</label>',
                ":LabelNoTrans"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Trans</label>',
                ":LabelSupplier"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Supplier</label>',
                ":KKNo"              => Html::textInput( 'KKNoView', $dsTUang[ 'KKNoView' ], [ 'class' => 'form-control','readonly' => 'readonly','tabindex' => '1' ] ) .
                                        Html::textInput( 'KKNo', $dsTUang[ 'KKNo' ], [ 'class' => 'hidden' ] ),
                ":KKTgl"             => FormField::dateInput( [ 'name' => 'KKTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'KKTgl' ] )] ] ),
                ":KKJam"             => FormField::timeInput( [ 'name' => 'KKJam', 'config' => [ 'value' => $dsTUang[ 'KKTgl' ] ]] ),
                ":Kode"              => FormField::combo( 'NoAccount', ['name'=> 'KasKode', 'config' => [ 'value' => $dsTUang['KasKode'] , 'data'=> \abengkel\models\Traccount::find()->kas()]] ),
                ":JT"                => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control' ,'readOnly'=>true] ),
                ":Keterangan"        => Html::textInput( 'KKMemo', $dsTUang[ 'KKMemo' ], [ 'class' => 'form-control','maxlength' => '150' ] ),
                ":Nominal"           => FormField::numberInput( [ 'name' => 'KKNominal', 'config' => [ 'value' => $dsTUang[ 'KKNominal' ] ] ] ),
                ":NoTrans"           => Html::textInput( 'KKLink', $dsTUang[ 'KKLink' ], [ 'class' => 'form-control', 'readonly' => true  ] ),
                ":Terima"            => Html::textInput( 'KKPerson', $dsTUang[ 'KKPerson' ], [ 'class' => 'form-control' ] ),
                ":Supplier"          => FormField::combo( 'DealerKode', ['name' => 'SupKode','config'=> ['value' =>$dsTUang[ 'SupKode' ] ],'extraOptions' => ['altLabel'=>[ 'DealerNama' ], 'altOrder' => 'DealerStatus']] ),
                ":Jenis"             => FormField::combo( 'KKJenis', [ 'config' => [ 'value' => $dsTUang[ 'KKJenis' ], 'data' => [
                                        'Tunai'=>	'Tunai',
                                        'DP Kas'=>	'DP Kas',
                                        'Kas Kredit'=>	'Kas Kredit',
                                        'DP Transfer'=> 'DP Transfer',
                                        'Kartu Debit'=>	'Kartu Debit',
                                        'Kartu Kredit'=> 'Kartu Kredit',
                                        'Transfer'=>	'Transfer'] ], 'options' => ['tabindex'=>'3'],'extraOptions' => [ 'simpleCombo' => true ] ] ),
                ":Trans"             => FormField::combo( 'KodeTrans', [ 'config' => [ 'value' => $dsTUang[ 'KodeTrans' ], 'data' => ['BM'=>	'BM - KK Ke Bank'] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
                ":BtnInfo"           => '<button type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>',
            ],
            [
                '_akses' => 'Kas Keluar Ke Bank',
                'url_main' => 'ttkkhd',
                'url_id' => $id ?? '',
                'url_delete'        => Url::toRoute(['ttkkhd/delete', 'id' => $id ?? ''] ),
            ]
        );
        ActiveForm::end();
        ?>
    </div>
<?php
$urlAdd         = \abengkel\models\Ttkkhd::getRoute('BM',['action'=>'create'] )[ 'create' ];
$urlIndex       = \abengkel\models\Ttkkhd::getRoute('BM',[ 'id' => $id ] )[ 'index' ];
$this->registerJs( <<< JS
	
	$('#btnSave').click(function (event) {	  
	    let Nominal = parseFloat($('#KKNominal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Nominal tidak boleh 0', size: 'small'});
               return;
           }
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttkkhd_id').attr('action','{$url['update']}');
        $('#form_ttkkhd_id').submit();
    });

    $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  });
	
	$('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
	
	$('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
	
	$('[name=KKTgl]').utilDateControl().cmp().attr('tabindex', '2');
JS
);
