<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttkkhd */
$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = 'Edit - Kas Keluar Ke Bank : '. $dsTUang['KKNoView'];
?>
<div class="ttkkhd-update">
    <?= $this->render('_kas-keluar-ke-bank-form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'id'    => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/kas-keluar-ke-bank-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
        ]
    ]) ?>

</div>
