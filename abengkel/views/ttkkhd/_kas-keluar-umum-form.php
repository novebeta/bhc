<?php
use abengkel\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttkkhd */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
?>
<div class="ttkkhd-form">
        <?php
        $form = ActiveForm::begin(['id' => 'form_ttkkhd_id','action' => $url[ 'update' ] ]);
        \abengkel\components\TUi::form(
            ['class' => "row-no-gutters",
                'items' => [
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "form-group col-md-17",
                                'items' => [
                                    ['class' => "col-sm-3",'items' => ":LabelKKNo"],
                                    ['class' => "col-sm-4",'items' => ":KKNo"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-2",'items' => ":LabelKKTgl"],
                                    ['class' => "col-sm-3",'items' => ":KKTgl"],
                                    ['class' => "col-sm-3",'items' => ":KKJam",'style' => 'padding-left:1px;'],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-2",'items' => ":LabelJenis"],
                                    ['class' => "col-sm-4",'items' => ":Jenis"],
                                ],
                            ],
                            ['class' => "form-group col-md-7",
                                'items' => [
                                    ['class' => "col-sm-7",'items' => ":LabelTrans"],
                                    ['class' => "col-sm-17",'items' => ":Trans"],
                                ],
                            ],
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "form-group col-md-17",
                                'items' => [
                                    ['class' => "col-sm-3",'items' => ":LabelKode"],
                                    ['class' => "col-sm-20",'items' => ":Kode"]
                                ],
                            ],
                            ['class' => "form-group col-md-7",
                                'items' => [
                                    ['class' => "col-sm-7",'items' => ":LabelNominal"],
                                    ['class' => "col-sm-17",'items' => ":Nominal"],
                                ],
                            ],
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "form-group col-md-17",
                                'items' => [
                                    ['class' => "col-sm-3",'items' => ":LabelNoTrans"],
                                    ['class' => "col-sm-4",'items' => ":NoTrans"],
                                    ['class' => "col-sm-2",'items' => ":BtnInfo"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-2",'items' => ":LabelSupplier"],
                                    ['class' => "col-sm-11",'items' => ":Supplier"],

                                ],
                            ],
                            ['class' => "form-group col-md-7",
                                'items' => [
                                    ['class' => "col-sm-7",'items' => ":LabelTerima"],
                                    ['class' => "col-sm-17",'items' => ":Terima"],
                                ],
                            ],
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "col-sm-2",'items' => ":LabelKeterangan"],
                            ['class' => "col-sm-22",'items' => ":Keterangan",'style' => 'padding-left:5px;']
                        ],
                    ],
                    [
                        'class' => "row col-md-24",
                        'style' => "margin-bottom: 6px;",
                        'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
                    ],
                ],
            ],
            [ 'class' => "row-no-gutters",
                'items' => [
                    ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                        'items'  => [
                            ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":JT"],
                            ['class' => "col-sm-4 pull-right", 'items' => ":LabelJT"],
                        ],
                    ],
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'KKNo'      => 'No KK',
                                'KKJenis'   => 'Jenis',
                                'KodeTrans' => 'Kode Trans',
                                'SupKode'   => 'Supplier',
                                'KasKode'   => 'Kode Kas',
                                'KKPerson'  => 'Person',
                                'KKMemo'    => 'Keterangan',
                                'NoGL'      => 'No Gl',
                                'Cetak'     => 'Cetak',
                                'UserID'    => 'User ID',
                                'PosKode'   => 'Pos Kode',
                            ],
                        ])
                    ],
                    ['class' => "col-md-12 pull-right",
                        'items' => [
                            [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
                        ]
                    ]
                ]
            ],
            [
                ":LabelKKNo"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">No KK</label>',
                ":LabelKKTgl"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl KK</label>',
                ":LabelKode"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode</label>',
                ":LabelKeterangan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
                ":LabelTrans"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Trans</label>',
                ":LabelJenis"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
                ":LabelNominal"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
                ":LabelJT"           => \common\components\General::labelGL( $dsTUang[ 'NoGL' ], 'JT' ),
                ":LabelTerima"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Diterima Oleh</label>',
                ":LabelNoTrans"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Trans</label>',
                ":LabelSupplier"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Supplier</label>',
                ":KKNo"              => Html::textInput( 'KKNoView', $dsTUang[ 'KKNoView' ], [ 'class' => 'form-control','readonly' => 'readonly', 'tabindex' => '1' ] ) .
                                        Html::textInput( 'KKNo', $dsTUang[ 'KKNo' ], [ 'class' => 'hidden' ] ),
                ":KKTgl"             => FormField::dateInput( [ 'name' => 'KKTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'KKTgl' ] )] ] ),
                ":KKJam"             => FormField::timeInput( [ 'name' => 'KKJam', 'config' => [ 'value' => $dsTUang[ 'KKTgl' ] ]] ),
                ":Kode"              => FormField::combo( 'NoAccount', [ 'name'=> 'KasKode','config' => [ 'value' => $dsTUang['KasKode'] , 'data'=> \abengkel\models\Traccount::find()->kas()]] ),
                ":JT"                => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control','readOnly'=>true ] ),
                ":Keterangan"        => Html::textInput( 'KKMemo', $dsTUang[ 'KKMemo' ], [ 'class' => 'form-control', 'maxlength' => '150' ] ),
                ":Supplier"          => Html::textInput( 'SupKode', $dsTUang[ 'SupKode' ], [ 'class' => 'form-control', 'maxlength' => '75' ] ),
                ":Jenis"             => FormField::combo( 'KKJenis', [ 'config' => [ 'value' => $dsTUang[ 'KKJenis' ], 'data' => [
                                        'Tunai'=>	'Tunai',
                                        'DP Kas'=>	'DP Kas',
                                        'Kas Kredit'=>	'Kas Kredit',
                                        'DP Transfer'=> 'DP Transfer',
                                        'Kartu Debit'=>	'Kartu Debit',
                                        'Kartu Kredit'=> 'Kartu Kredit',
                                        'Transfer'=>	'Transfer'
                                        ] ],'options' => ['tabindex'=>'3'], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
                ":Trans"             => FormField::combo( 'KodeTrans', [ 'config' => [ 'value' => $dsTUang[ 'KodeTrans' ], 'data' => ['UM'=>	'UM - Umum',] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
                ":Nominal"           => FormField::decimalInput( [ 'name' => 'KKNominal', 'config' => [ 'value' => $dsTUang[ 'KKNominal' ], 'id' => 'KKNominal' ] ] ),
                ":NoTrans"           => Html::textInput( 'KKLink', $dsTUang[ 'KKLink' ], [ 'class' => 'form-control', 'readonly' => true  ] ),
                ":Terima"            => Html::textInput( 'KKPerson', $dsTUang[ 'KKPerson' ], [ 'class' => 'form-control', 'maxlength' => '50' ] ),
                ":BtnInfo"           => '<button type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></i></button>',
            ],
            [
                '_akses' => 'Kas Keluar Umum',
                'url_main' => 'ttkkhd',
                'url_id' => $_GET['id'] ?? '',
                'url_delete'        => Url::toRoute(['ttkkhd/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
        );
        ActiveForm::end();
        ?>
    </div>
<?php
$urlAdd         = \abengkel\models\Ttkkhd::getRoute('UM',['action'=>'create'] )[ 'create' ];
$urlIndex       = \abengkel\models\Ttkkhd::getRoute('UM',[ 'id' => $id ] )[ 'index' ];
$urlDetail      = $url[ 'detail' ];
$readOnly       = ( $_GET['action'] == 'view' ? 'true' : 'false' );
$DtAccount      = \common\components\General::cCmd( "SELECT NoAccount, NamaAccount, OpeningBalance, JenisAccount, StatusAccount, NoParent FROM traccount 
WHERE JenisAccount = 'Detail' ORDER BY NoAccount" )->queryAll();
$this->registerJsVar( '__dtAccount', json_encode( $DtAccount )  );
$this->registerJs( <<< JS

     var __NoAccount = JSON.parse(__dtAccount);
     __NoAccount = $.map(__NoAccount, function (obj) {
                      obj.id = obj.NoAccount;
                      obj.text = obj.NoAccount;
                      return obj;
                    });
     var __NamaAccount = JSON.parse(__dtAccount);
     __NamaAccount = $.map(__NamaAccount, function (obj) {
                      obj.id = obj.NamaAccount;
                      obj.text = obj.NamaAccount;
                      return obj;
                    });
     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 220,
        extraParams: {
            KKNo: $('input[name="KKNo"]').val()
        },
        rownumbers: true,  
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,         
        colModel: [
            { template: 'actions'  },
            {
                name: 'NoAccount',
                label: 'Kode',
                editable: true,
                width: 155,
                editor: {
                    type: 'select2',
                    data: __NoAccount,  
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowid = $(this).attr('rowid');
                            $('[id="'+rowid+'_'+'NamaAccount'+'"]').val(data.NamaAccount); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'NamaAccount'+'"]').trigger('change');
						}
                    }                  
                }
            },
            {
                name: 'NamaAccount',
                label: 'Nama',
                width: 250,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __NamaAccount, 
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowid = $(this).attr('rowid');
                            $('[id="'+rowid+'_'+'NoAccount'+'"]').val(data.NoAccount); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'NoAccount'+'"]').trigger('change');
						}
                    }                              
                }
            },
            {
                name: 'KKKeterangan',
                label: 'Keterangan',
                width: 325,
                editable: true,
            },
            {
                name: 'KKBayar',
                label: 'Debit',
                width: 200,
                editable: true,
                template: 'money'
            },
            {
                name: 'KKAutoN',
                label: 'KKAutoN',
                width: 210,
                editable: true,
                hidden: true
            },
        ],     
        listeners: { 
            afterLoad: function(){
                let TotalKKBayar = $('#detailGrid').jqGrid('getCol','KKBayar',false,'sum');
                $("#detailGrid-pager_right").html('<label class="control-label" style="margin-right:10px;">Sub Total COA </label>' +
                 '<label style="width:190px;margin-right:28px;">'+TotalKKBayar.toLocaleString("id-ID", {minimumFractionDigits:2})+'</label>');
            }
        },
     }).init().fit($('.box-body')).load({url: '$urlDetail'}); 
	
	$('#btnSave').click(function (event) {	
	    let TotalKKBayar = $('#detailGrid').jqGrid('getCol','KKBayar',false,'sum');
	    let Nominal = parseFloat($('#KKNominal').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Nominal tidak boleh 0', size: 'small'});
               return;
           }
           if (TotalKKBayar !== Nominal){
            bootbox.alert({ message: "Jumlah nominal dengan subtotal coa berbeda.", size: 'small'});
            return;
            }
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttkkhd_id').attr('action','{$url['update']}');
        $('#form_ttkkhd_id').submit();
    });     
	
	$('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  });
	
	$('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
	
	$('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
	
	$('#detailGrid').bind("jqGridInlineEditRow", function (e, rowId, orgClickEvent) {
        
        if (orgClickEvent.extraparam.oper === 'add'){
            var KKKeterangan = $('[id="'+rowId+'_'+'KKKeterangan'+'"]');
            KKKeterangan.val($('[name="KKMemo"]').val());
        }        
    });
     
	$('[name=KKTgl]').utilDateControl().cmp().attr('tabindex', '2');
JS
);
