<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttkkhd */
$params                          = '&id=' . $id . '&action=UpdateAdd';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/kas-transfer-cancel'.$params );
$this->title                     = 'Tambah - Kas Transfer';
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Kas Transfer', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="ttkkhd-create">
    <?= $this->render('_kas-keluar-transfer-antar-kas-form', [
        'dsTUang'   => $dsTUang,
        'id'        =>$id,
        'url'       => [
            'update' => Custom::url( \Yii::$app->controller->id . '/transfer-antar-kas-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
        ]
    ]) ?>
</div>
