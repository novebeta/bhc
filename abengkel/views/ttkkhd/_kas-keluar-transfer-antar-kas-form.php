<?php
use abengkel\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttkkhd */
/* @var $form yii\widgets\ActiveForm */
$format          = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape          = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
<div class="ttkkhd-form">
	<?php
	$form = ActiveForm::begin( ['id' => 'form_ttkkhd_id','action' => $url[ 'update' ] ] );
	\abengkel\components\TUi::form(
		[ 'class' => "row-no-gutters",
		  'items' => [
			  [ 'class' => "form-group col-md-24",
			    'items' => [
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-4", 'items' => ":LabelKTNo" ],
					      [ 'class' => "col-sm-7", 'items' => ":KTNo" ],
					      [ 'class' => "col-sm-1" ],
					      [ 'class' => "col-sm-2", 'items' => ":LabelKTTgl" ],
					      [ 'class' => "col-sm-4", 'items' => ":KTTgl" ],
					      [ 'class' => "col-sm-5", 'items' => ":KTJam", 'style' => 'padding-left:1px;' ],
				      ],
				    ],
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-3", 'items' => ":LabelKKNo" ],
					      [ 'class' => "col-sm-8", 'items' => ":KKNo" ],
					      [ 'class' => "col-sm-2" ],
					      [ 'class' => "col-sm-3", 'items' => ":LabelKMNo" ],
					      [ 'class' => "col-sm-8", 'items' => ":KMNo" ],
				      ],
				    ],
			    ],
			  ],
			  [ 'class' => "form-group col-md-24",
			    'items' => [
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-4", 'items' => ":LabelJenis" ],
					      [ 'class' => "col-sm-7", 'items' => ":Jenis" ],
					      [ 'class' => "col-sm-1" ],
					      [ 'class' => "col-sm-2", 'items' => ":LabelTrans" ],
					      [ 'class' => "col-sm-9", 'items' => ":Trans" ]
				      ],
				    ],
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-3", 'items' => ":LabelJTKK" ],
					      [ 'class' => "col-sm-8", 'items' => ":JTKK" ],
					      [ 'class' => "col-sm-2", ],
					      [ 'class' => "col-sm-3", 'items' => ":LabelJTKM" ],
					      [ 'class' => "col-sm-8", 'items' => ":JTKM" ],
				      ],
				    ],
			    ],
			  ],
			  [ 'class' => "form-group col-md-24",
			    'items' => [
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-4", 'items' => ":LabelAsal" ],
					      [ 'class' => "col-sm-19", 'items' => ":Asal" ],
					      [ 'class' => "col-sm-1" ],
				      ],
				    ],
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-5", 'items' => ":LabelDiserahkan" ],
					      [ 'class' => "col-sm-19", 'items' => ":Diserahkan" ],
				      ],
				    ],
			    ],
			  ],
			  [ 'class' => "form-group col-md-24",
			    'items' => [
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-4", 'items' => ":LabelTujuan" ],
					      [ 'class' => "col-sm-19", 'items' => ":Tujuan" ],
					      [ 'class' => "col-sm-1" ],
				      ],
				    ],
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-5", 'items' => ":LabelDiterima" ],
					      [ 'class' => "col-sm-19", 'items' => ":Diterima" ],
				      ],
				    ],
			    ],
			  ],
			  [ 'class' => "form-group col-md-24",
			    'items' => [
				    [ 'class' => "col-sm-2", 'items' => ":LabelKeterangan" ],
				    [ 'class' => "col-sm-15", 'items' => ":Keterangan" ],
				    [ 'class' => "col-sm-1" ],
				    [ 'class' => "col-sm-2", 'items' => ":LabelNominal" ],
				    [ 'class' => "col-sm-4", 'items' => ":Nominal" ],
			    ],
			  ],
		  ],
		],
		[ 'class' => "row-no-gutters",
		  'items' => [
              [
                  'class' => "col-md-12 pull-left",
                  'items' => $this->render( '../_nav', [
                      'url'=> $_GET['r'],
                      'options'=> [
                          'KKNo'      => 'No KK',
                          'KKJenis'   => 'Jenis',
                          'KodeTrans' => 'Kode Trans',
                          'SupKode'   => 'Supplier',
                          'KasKode'   => 'Kode Kas',
                          'KKPerson'  => 'Person',
                          'KKMemo'    => 'Keterangan',
                          'NoGL'      => 'No Gl',
                          'Cetak'     => 'Cetak',
                          'UserID'    => 'User ID',
                          'PosKode'   => 'Pos Kode',
                      ],
                  ])
              ],
			  [ 'class' => "col-md-12",
			    'items' => [
				    [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
			    ]
			  ]
		  ]
		],
		[
			":LabelKTNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No KT</label>',
			":LabelKTTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl KT</label>',
			":LabelKKNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No KK</label>',
			":LabelKMNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No KM</label>',
			":LabelJenis"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
			":LabelTrans"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Trans</label>',
			":LabelJTKK"       => \common\components\General::labelGL( $dsTUang[ 'NoGLKK' ],'JT KK' ),
			":LabelJTKM"       => \common\components\General::labelGL( $dsTUang[ 'NoGLKM' ],'JT KM' ),
			":LabelAsal"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kas Asal</label>',
			":LabelTujuan"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kas Tujuan</label>',
			":LabelKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
			":LabelNominal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
			":LabelDiterima"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Diterima Oleh</label>',
			":LabelDiserahkan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Diserahkan Oleh</label>',
			":KTNo"       => Html::textInput( 'KTNo', $dsTUang[ 'KTNo' ], [ 'class' => 'form-control' ] ),
			":KKNo"       => Html::textInput( 'KKNo', $dsTUang[ 'KKNo' ], [ 'class' => 'form-control' , 'readonly' => true] ),
			":KMNo"       => Html::textInput( 'KMNo', $dsTUang[ 'KMNo' ], [ 'class' => 'form-control' , 'readonly' => true] ) .
			                 Html::textInput( 'KodeTrans', 'KT', [ 'class' => 'hidden' ] ) .
			                 Html::textInput( 'KMJam', 'KMJam', [ 'class' => 'hidden' ] ) .
			                 Html::textInput( 'KMTgl', 'KMTgl', [ 'class' => 'hidden' ] ) ,
			":KTTgl"      => FormField::dateInput( [ 'name' => 'KKTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'KKTgl' ] ) ] ] ),
			":KTJam"      => FormField::timeInput( [ 'name' => 'KKJam', 'config' => [ 'value' => $dsTUang[ 'KKTgl' ] ] ] ),
			":Trans"      => Html::textInput( 'Trans', 'KT - Kas Transfer', [ 'class' => 'form-control' ] ),
			":Jenis"      => Html::textInput( 'Jenis', 'Kas Transfer', [ 'class' => 'form-control' ] ),
			":JTKK"       => Html::textInput( 'NoGLKK', $dsTUang[ 'NoGLKK' ], [ 'class' => 'form-control' ] ),
			":JTKM"       => Html::textInput( 'NoGLKM', $dsTUang[ 'NoGLKM' ], [ 'class' => 'form-control' ] ),
			":Diserahkan" => Html::textInput( 'KKPerson', $dsTUang[ 'KKPerson' ], [ 'class' => 'form-control','maxlength' => '50' ] ),
			":Asal"       => FormField::combo( 'NoAccount', [ 'name'=>'KasKodeKK', 'config' => [ 'value' => $dsTUang['KasKodeKK'] , 'data'=> \abengkel\models\Traccount::find()->kas() ]] ),
			":Tujuan"     => FormField::combo( 'NoAccount', [ 'name'=>'KasKodeKM','config' => [ 'value' => $dsTUang['KasKodeKM'] , 'data'=> \abengkel\models\Traccount::find()->kastujuan()  ] ] ),
			":Keterangan" => Html::textInput( 'KKMemo', $dsTUang[ 'KKMemo' ], [ 'class' => 'form-control', 'maxlength' => '150' ] ),
			":Diterima"   => Html::textInput( 'KMPerson', $dsTUang[ 'KMPerson' ], [ 'class' => 'form-control', 'maxlength' => '50' ] ),
			":Nominal"    => FormField::numberInput( [ 'name' => 'KKNominal', 'config' => [ 'value' => $dsTUang[ 'KKNominal' ] ] ] ),
		],
        [
            '_akses' => 'Kas Transfer',
            'url_main' => 'ttkkhd',
            'url_id' => $_GET['id'] ?? '',
            'url_delete'        => Url::toRoute(['ttkkhd/delete', 'id' => $_GET['id'] ?? ''] ),
        ]

	);
	ActiveForm::end();
	?>
</div>
<?php
$urlAdd         = \abengkel\models\Ttkkhd::getRoute('Kas Transfer',['action'=>'create'] )[ 'create' ];
$urlIndex       = \abengkel\models\Ttkkhd::getRoute('Kas Transfer',[ 'id' => $id ] )[ 'index' ];
$urlPrint       = $url[ 'print' ];
$this->registerJs( <<< JS
 	
	$('#btnSave').click(function (event) {	      
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttkkhd_id').attr('action','{$url['update']}');
        $('#form_ttkkhd_id').submit();
    });
    
    $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  });
	
	$('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
	
	$('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
	
	$('#btnCancel').click(function (event) {	      
       $('#form_ttkkhd_id').attr('action','{$url['cancel']}');
       $('#form_ttkkhd_id').submit();
    });
JS
);

