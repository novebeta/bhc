<?php
use abengkel\assets\AppAsset;
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Kas Keluar Pembelian';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
    'cmbTxt'    => [
        'KKNo'      => 'No KK',
        'KKJenis'   => 'Jenis',
        'KodeTrans' => 'Kode Trans',
        'SupKode'   => 'Supplier',
        'KasKode'   => 'Kode Kas',
        'KKPerson'  => 'Person',
        'KKMemo'    => 'Keterangan',
        'NoGL'      => 'No Gl',
        'Cetak'     => 'Cetak',
        'UserID'    => 'User ID',
        'PosKode'   => 'Pos Kode',
    ],
    'cmbTgl'    => [
        'KKTgl'     => 'Kk Tgl',
    ],
    'cmbNum'    => [
        'KKNominal' => 'Kk Nominal',
    ],
    'sortname'  => "KKTgl",
    'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Ttkkhd::className(), 'KK PI - Pembelian' ,
    [
        'url_update' => Url::toRoute( [ 'ttkkhd/kas-keluar-pembelian-update' ,'action'=>'update']),
        'url_add'    => Url::toRoute( [ 'ttkkhd/kas-keluar-pembelian-create', 'action' => 'create' ] ),
        'url_delete' => Url::toRoute( [ 'ttkkhd/delete' ] ),
    ]
), \yii\web\View::POS_READY );
