<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttkkhd */

$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = 'Edit - Kas Keluar Ke H1 : ' . $dsTUang['KKNoView'];
?>
<div class="ttkkhd-update">
    <?= $this->render('_kas-keluar-ke-h1-form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'id' => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/kas-keluar-ke-h1-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
        ]
    ]) ?>

</div>
