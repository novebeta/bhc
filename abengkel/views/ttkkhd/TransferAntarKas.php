<?php
use abengkel\assets\AppAsset;
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Kas Transfer';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
    'cmbTxt'    => [
        'KMLink'      => 'No Transfer',
        'KKNo'   => 'No Kas Keluar',
        'KMNo' => 'No Kas Masuk',
        'KKPerson'   => 'Bayar Ke/Diterima Oleh',
        'KMPerson'   => 'Terima dari/Diserahkan oleh',
        'KKMemo'  => 'Keterangan',
        'KKJenis'    => 'Jenis KK',
        'KKLink'      => 'No Transaksi',
        'UserID'    => 'User',
        'NoGL'      => 'No JT',
    ],
    'cmbTgl'    => [
        'KKTgl'     => 'Kk Tgl',
    ],
    'cmbNum'    => [
        'KKNominal' => 'Kk Nominal',
    ],
    'sortname'  => "KKTgl",
    'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Ttkkhd::className(), 'Transfer Antar Kas', [
    'colGrid'    => \abengkel\models\Ttkkhd::colGridTransferKas(),
    'url_add'    => \yii\helpers\Url::toRoute([ 'ttkkhd/transfer-antar-kas-create' ,'action'=>'create']),
    'url_update' => \yii\helpers\Url::toRoute([ 'ttkkhd/transfer-antar-kas-update' ,'action'=>'update']),
    'url_delete' => \yii\helpers\Url::toRoute([ 'ttkkhd/delete'] ),
] ), \yii\web\View::POS_READY );
