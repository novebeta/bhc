<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tdarea */

$this->title = 'Tambah Area';
$this->params['breadcrumbs'][] = ['label' => 'Area', 'url' => ['tdarea/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdarea-create">
    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdarea/update', 'id' => $id,'action' => 'create' ] ),
            'cancel' => Url::toRoute( [ 'tdarea/cancel', 'id' => $id,'action' => 'create' ] )
        ]
    ]) ?>

</div>
