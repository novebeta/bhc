<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tdarea */

$this->title = 'Edit Area: ' . $model->Provinsi;
$this->params['breadcrumbs'][] = ['label' => 'Area', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="tdarea-update">
    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdarea/update', 'id' => $id ,'action' => 'update'] ),
            'cancel' => Url::toRoute( [ 'tdarea/cancel', 'id' => $id,'action' => 'update' ] )
        ]
    ]) ?>

</div>
