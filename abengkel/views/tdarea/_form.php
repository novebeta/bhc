<?php
use common\components\Custom;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdarea */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tdarea-form">
    <?php
    $form = ActiveForm::begin( ['id' => 'frm_tdarea_id' ,'enableClientValidation'=>false ] );
    \abengkel\components\TUi::form(
        ['class' => "row-no-gutters",
            'items' => [
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelProvinsi"],
                        ['class' => "col-sm-8",'items' => ":Provinsi"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-3",'items' => ":LabelKabupaten"],
                        ['class' => "col-sm-8",'items' => ":Kabupaten"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelKecamatan"],
                        ['class' => "col-sm-8",'items' => ":Kecamatan"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-3",'items' => ":LabelKelurahan"],
                        ['class' => "col-sm-8",'items' => ":Kelurahan"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelKodePos"],
                        ['class' => "col-sm-8",'items' => ":KodePos"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-3",'items' => ":LabelAreaStatus"],
                        ['class' => "col-sm-8",'items' => ":AreaStatus"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelProvAstra"],
                        ['class' => "col-sm-8",'items' => ":ProvAstra"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-3",'items' => ":LabelKabAstra"],
                        ['class' => "col-sm-8",'items' => ":KabAstra"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelKecAstra "],
                        ['class' => "col-sm-8",'items' => ":KecAstra "],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-3",'items' => ":LabelKelAstra"],
                        ['class' => "col-sm-8",'items' => ":KelAstra"],
                    ],
                ],
            ]
        ],
        [
            'class' => "pull-right",
            'items' => ":btnAction"
        ],
        [
            ":LabelProvinsi"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Provinsi</label>',
            ":LabelKabupaten"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kabupaten</label>',
            ":LabelKecamatan"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kecamatan</label>',
            ":LabelKelurahan"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kelurahan</label>',
            ":LabelKodePos"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode POS</label>',
            ":LabelAreaStatus" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Area Status</label>',
            ":LabelProvAstra"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Provinsi Astra</label>',
            ":LabelKabAstra"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kabupaten Astra</label>',
            ":LabelKecAstra"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kecamatan Astra</label>',
            ":LabelKelAstra"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kelurahan Astra</label>',

            ":Provinsi"     => $form->field( $model, 'Provinsi', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '30','autofocus' => 'autofocus' ] ),
            ":Kabupaten"    => $form->field( $model, 'Kabupaten', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '50', ] ),
            ":Kecamatan"    => $form->field( $model, 'Kecamatan', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '50', ] ),
            ":Kelurahan"    => $form->field( $model, 'Kelurahan', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '50', ] ),
            ":KodePos"      => $form->field( $model, 'KodePos', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '7', ] ),
            ":AreaStatus"   => $form->field( $model, 'AreaStatus', [ 'template' => '{input}' ] )
                                ->dropDownList( ['A' => 'Aktif', 'N' => 'Non Aktif', '1' => 'Kabupaten Dealer',], [ 'maxlength' => '1' ] ),
            ":ProvAstra"    => $form->field( $model, 'ProvinsiAstra', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '38', ] ),
            ":KabAstra"     => $form->field( $model, 'KabupatenAstra', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '65', ] ),
            ":KecAstra"     => $form->field( $model, 'KecamatanAstra', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '65', ] ),
            ":KelAstra"     => $form->field( $model, 'KecamatanAstra', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '65', ] ),
        ],
	    [
		    'url_main'   => 'tdarea',
		    'url_id'     => $id,
		    'url_update' => $url[ 'update' ]
	    ]
    );
    ActiveForm::end();
    ?>
</div>
<?php
$urlIndex   = Url::toRoute( [ 'tdarea/index' ] );
$urlAdd     = Url::toRoute( [ 'tdarea/create','id'=>'new' ] );
$this->registerJs( <<< JS
     $('#btnSave').click(function (event) {
       $('#frm_tdarea_id').submit();
    }); 
    $('#btnCancel').click(function (event) {	  
         window.location.href = '{$url['cancel']}';
    });
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
    $('#btnAdd').click(function (event) {	      
        window.location.href = '{$urlAdd}';
	  });
    

JS);


