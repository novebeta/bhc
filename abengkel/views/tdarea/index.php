<?php
/* @var $this yii\web\View */
use abengkel\assets\AppAsset;
$this->title                   = 'Area';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'Provinsi'       => 'Provinsi',
		'Kabupaten'      => 'Kabupaten',
		'Kecamatan'      => 'Kecamatan',
		'Kelurahan'      => 'Kelurahan',
		'KodePos'        => 'Kode Pos',
		'AreaStatus'     => 'Area Status',
		'ProvinsiAstra'  => 'Provinsi Astra',
		'KabupatenAstra' => 'Kabupaten Astra',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
	'sortname'  => "AreaStatus ,Provinsi ,Kabupaten",
	'sortorder' => ''
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Tdarea::className(),'Area' ), \yii\web\View::POS_READY );