<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tzcompany */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-md-12 col-xl-12 col-lg-12">
    <div class="box box-primary ">
		<?php $form = ActiveForm::begin( [
			'class' => 'form-horizontal'
		] ); ?>
        <div class="box-body ">
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'PerusahaanNama' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'PerusahaanNama' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'PerusahaanSlogan' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'PerusahaanSlogan' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'PerusahaanAlamat' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'PerusahaanAlamat' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'PerusahaanTelepon' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'PerusahaanTelepon' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'PrinterView' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'PrinterView' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'PrinterDotMatrix' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'PrinterDotMatrix' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'PrinterInkJet' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'PrinterInkJet' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="pull-right">-
			    <?= Html::submitButton( 'Simpan', [ 'class' => 'btn btn-success' ] ) ?>
                <a href="<?= Url::to( [ 'site/index' ] ) ?>" class="btn btn-warning" role="button">Batal</a>
            </div>
        </div>
	    <?php ActiveForm::end(); ?>
    </div>
</div>