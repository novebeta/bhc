<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdprogramhd */
/*
$this->title = 'Edit Program : ' . $model->PrgNama;
$this->params['breadcrumbs'][] = ['label' => 'Program', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->PrgNama, 'url' => ['view', 'id' => $model->PrgNama]];
$this->params['breadcrumbs'][] = 'Edit';
*/
$params                          = '&id=' . $id . '&action=update';
$cancel = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = 'Edit - Program: ' . $model->PrgNama;
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Program', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = 'Edit';
?>
<div class="tdprogramhd-update">
    <?= $this->render('_form', [
        'model'     => $model,
        'id'        =>$id,
        'dsSetup'   => $dsSetup,
        'url'       => [
            'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'tdprogramit/index','action' => 'create' ] ),
        ]
    ]) ?>

</div>
