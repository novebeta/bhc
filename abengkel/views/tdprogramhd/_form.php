<?php
use abengkel\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdprogramhd */
/* @var $form yii\widgets\ActiveForm */
/*
$url['cancel'] = Url::toRoute('tdprogramhd/index');
$urldetails = Url::toRoute(['tdprogramit/index']);
*/
$format          = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape          = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>

<div class="tdprogramhd-form">
    <?php
    $form = ActiveForm::begin( [ 'id' => 'form_tdprogramhd_id','action' => $url[ 'update' ] ] );
    \abengkel\components\TUi::form(
        [
            'class' => "row-no-gutters",
            'items' => [
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelProgramNama"],
                        ['class' => "col-sm-9",'items' => ":ProgramNama"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelAwal"],
                        ['class' => "col-sm-3",'items' => ":PrgTglAwal"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelAkhir"],
                        ['class' => "col-sm-3",'items' => ":PrgTglAkhir"],

                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelKeterangan"],
                        ['class' => "col-sm-21",'items' => ":PrgMemo"],
                    ],
                ],
                [
	                'class' => "row col-md-24",
	                'style' => "margin-bottom: 6px;",
	                'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
                ],
            ]
        ],
        [
            'class' => "pull-right",
            'items' => ":btnSave :btnCancel"
        ],
        [
            /* label */
            ":LabelProgramNama"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Program</label>',
            ":LabelAwal"            => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Awal</label>',
            ":LabelAkhir"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl AKhir</label>',
            ":LabelKeterangan"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Memo Program</label>',
            /* component */

            ":ProgramNama"          => Html::textInput( 'PrgNamaView', $dsSetup[ 'PrgNamaView' ], [ 'class' => 'form-control','readonly' => 'readonly' ] ) .
                                        Html::textInput( 'PrgNama', $dsSetup[ 'PrgNama' ], [ 'class' => 'hidden' ] ) ,
            ":PrgTglAwal"           => FormField::dateInput( [ 'name' => 'PrgTglMulai', 'config' => [ 'value' => General::asDate( $dsSetup[ 'PrgTglMulai' ] ) ] ] ),
            ":PrgTglAkhir"          => FormField::dateInput( [ 'name' => 'PrgTglAkhir', 'config' => [ 'value' => General::asDate( $dsSetup[ 'PrgTglAkhir' ] ) ] ] ),
            ":PrgMemo"              => Html::textarea( 'PrgMemo', $dsSetup[ 'PrgMemo' ], [ 'class' => 'form-control', 'maxlength' => '150' ] ),
            /* button */
        ],
	    [
		    'url_main'   => 'tdprogramhd',
		    'url_id'     => $id,
		    'url_update' => $url[ 'update' ]
	    ]
    );
    ActiveForm::end();
    ?>
</div>
<?php
$urlDetail = $url[ 'detail' ];
$urlPrint  = $url[ 'print' ];
$readOnly  = ( $_GET['action'] == 'view' ? 'true' : 'false' );
$barang    = \common\components\General::cCmd( "SELECT BrgKode, BrgNama, BrgBarCode, BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual, BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus FROM tdbarang WHERE BrgStatus = 'A' 
              UNION
              SELECT JasaKode , JasaNama, '' AS BrgBarCode, JasaGroup, 'MENIT' AS BrgSatuan, JasaHrgBeli, JasaHrgJual, '--' AS BrgRak1, 0 AS BrgMinStock, 0 AS BrgMaxStock, JasaStatus FROM tdjasa WHERE JasaStatus = 'A'
              ORDER BY BrgStatus, BrgKode" )->queryAll();
$this->registerJsVar( '__Barang', json_encode( $barang ) );
$this->registerJs( <<< JS
     var __BrgKode = JSON.parse(__Barang);
     __BrgKode = $.map(__BrgKode, function (obj) {
                      obj.id = obj.BrgKode;
                      obj.text = obj.BrgKode;
                      return obj;
                    });
     var __BrgNama = JSON.parse(__Barang);
     __BrgNama = $.map(__BrgNama, function (obj) {
                      obj.id = obj.BrgNama;
                      obj.text = obj.BrgNama;
                      return obj;
                    });
     
      var __BrgHrgJual = JSON.parse(__Barang);
     __BrgHrgJual = $.map(__BrgHrgJual, function (obj) {
                      obj.id = obj.BrgHrgJual;
                      obj.text = obj.BrgHrgJual;
                      return obj;
                    });
     // console.log(__BrgHrgJual[0]);
     console.log(__BrgHrgJual[0]['BrgHrgJual']);
     console.log(typeof(__BrgHrgJual[0]['BrgHrgJual']));
     console.log(parseFloat(__BrgHrgJual[0]['BrgHrgJual']));
     console.log(typeof(parseFloat(__BrgHrgJual[0]['BrgHrgJual'])));
     // console.log('batas');
     // console.log(__BrgHrgJual);
     
     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail', 
        height: 290,
        extraParams: {
            PrgNama: $('input[name="PrgNama"]').val()
        },
        rownumbers: true,     
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,      
        colModel: [
            { template: 'actions'   },
            {
                name: 'PrgNama',
                label: 'No',
                width: 147,
                editable: true,
                hidden: true
            },
            {
                name: 'PrgAuto',
                label: 'No',
                width: 147,
                editable: true,
                hidden: true
            },
             {
                name: 'BrgKode',
                label: 'Kode',
                editable: true,
                width: 150,
                // edittype: "select", 
                editor: {
                    type: 'select2',
                    data: __BrgKode,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    // console.log(data);
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
						    console.log(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'PrgHrgJual'+'"]').utilNumberControl().val(data.BrgHrgJual);
                            window.hitungLine();
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').val(data.BrgNama); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').trigger('change');
						}
                    }                 
                }
            },
            {
                name: 'BrgNama',
                label: 'Nama Barang',
                width: 250,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __BrgNama,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'PrgHrgJual'+'"]').utilNumberControl().val(data.BrgHrgJual);
                            window.hitungLine();
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').val(data.BrgKode); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').trigger('change');
						}
                    }              
                }
            },
            {
                name: 'BrgSatuan',
                label: 'Satuan',
                width: 100,
                editable: true,
                editor:{
                    readonly: true,                    
                }
            }, 
            {
                name: 'PrgHrgJual',
                label: 'Harga Jual',
                width: 170,
                editable: true,
                template: 'money'
            },
            {
                name: 'Disc',
                label: 'Disc %',
                width: 100,
                editable: true,
                template: 'money'
            }, 
            {
                name: 'PrgDiscount',
                label: 'Nilai Disc',
                width: 170,
                editable: true,
                template: 'money'
            }       
        ],      
        listeners: {
            afterLoad: function (data) {
             
            }
        },
     onSelectRow : function(rowid,status,e){ 
            if (window.rowId !== -1) return;
            if (window.rowId_selrow === rowid) return;
            if(rowid.includes('new_row') === true){
                $('[name=KodeBarang]').val('--');
                $('[name=BarangNama]').val('--');
                return ;
            };
            let r = $('#detailGrid').getRowData(rowid);
            $('#KodeBarang').val(unescape(r.BrgKode));
            $('#BarangNama').val($("<textarea/>").html(r.BrgNama).text());
            window.rowId_selrow = rowid;
        }    
     }).init().fit($('.box-body')).load({url: '$urlDetail'});
     window.rowId_selrow = -1;
     
     function hitungLine(){
            let Disc        = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Disc'+'"]').val()));
            let PrgHrgJual  = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'PrgHrgJual'+'"]').val()));
            let PrgDiscount = Disc*PrgHrgJual / 100;
            $('[id="'+window.cmp.rowid+'_'+'PrgDiscount'+'"]').val(PrgDiscount);
            // window.HitungTotal();
         }
         window.hitungLine = hitungLine;
     
     $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
	     window.cmp.rowid = rowid;    
          if(rowid.includes('new_row')){              
            $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val('PCS');
            $('[id="'+rowid+'_'+'PrgHrgJual'+'"]').val(parseFloat(__BrgHrgJual[0]['BrgHrgJual']));
          }
         
          $('[id="'+window.cmp.rowid+'_'+'SOHrgJual'+'"]').on('keyup', function (event){
              hitungLine();
          });
          $('[id="'+window.cmp.rowid+'_'+'Disc'+'"]').on('keyup', function (event){
              hitungLine();
          });
         hitungLine();
	 });
     
     $('#btnSave').click(function (event) {
        $('#form_tdprogramhd_id').submit();
	  });  

      $('#btnPrint').click(function (event) {	      
        $('#form_tdprogramhd_id').attr('action','$urlPrint');
        $('#form_tdprogramhd_id').attr('target','_blank');
        $('#form_tdprogramhd_id').submit();
	  }); 
      
      
      
JS
);