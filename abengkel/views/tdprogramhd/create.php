<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdprogramhd */
/*
$this->title = 'Tambah Program';
$this->params['breadcrumbs'][] = ['label' => 'Program', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
*/
$params                          = '&id=' . $id . '&action=create';
$cancel = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = 'Tambah - Program';
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Program', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="tdprogramhd-create">
    <?= $this->render('_form', [
        'model'     => $model,
        'id'        =>$id,
        'dsSetup'   => $dsSetup,
        'url'       => [
            'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'tdprogramit/index','action' => 'create' ] ),
        ]
    ]) ?>

</div>
