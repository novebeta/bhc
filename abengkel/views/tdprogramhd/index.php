<?php
use abengkel\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = 'Program';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'PrgNama' => 'Prg Nama',
		'PrgMemo' => 'Prg Memo',
	],
	'cmbTgl'    => [
		'PrgTglMulai' => 'Prg Tgl Mulai',
		'PrgTglAkhir' => 'Prg Tgl Akhir',
	],
	'cmbNum'    => [
	],
	'sortname'  => "PrgTglMulai",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Tdprogramhd::className(), 'Program', [
	'url_delete' => Url::toRoute( [ 'tdprogramhd/delete' ] ),
] ), \yii\web\View::POS_READY );
