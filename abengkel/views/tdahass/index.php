<?php
use abengkel\assets\AppAsset;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'AHASS';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'AHASSNomor'        => 'AHASS Nomor',
		'AHASSNama'         => 'AHASS Nama',
		'AHASSAlamat'       => 'Alamat',
		'AHASSTelepon'      => 'Telepon',
		'AHASSKontak'       => 'Kontak',
		'AHASSJenisLayanan' => 'Jenis Layanan',
		'AHASSJenisDealer'  => 'Jenis Dealer',
		'AHASSProvinsi'     => 'Provinsi',
		'AHASSKecamatan'    => 'Kecamatan',
		'AHASSKabupaten'    => 'Kabupaten',
		'AHASSMainDealer'   => 'Main Dealer',
		'AHASSStatus'       => 'Status',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
	'sortname'  => "AHASSStatus, AHASSNomor ",
	'sortorder' => ''
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Tdahass::className(),'AHASS' ), \yii\web\View::POS_READY );
