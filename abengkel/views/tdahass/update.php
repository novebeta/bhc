<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdahass */

$this->title = 'Edit AHASS : ' . $model->AHASSNomor;
$this->params['breadcrumbs'][] = ['label' => 'AHASS', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="tdahass-update">
    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdahass/update', 'id' => $id ,'action' => 'update'] ),
            'cancel' => Url::toRoute( [ 'tdahass/cancel', 'id' => $id,'action' => 'update' ] )
        ]
    ]) ?>

</div>
