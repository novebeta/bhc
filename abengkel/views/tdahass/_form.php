<?php
use abengkel\components\FormField;
use common\components\Custom;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdahass */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tdahass-form">
    <?php
    $form = ActiveForm::begin(['id' => 'frm_tdahass_id'  ,'enableClientValidation'=>false]);
    \abengkel\components\TUi::form(
        ['class' => "row-no-gutters",
            'items' => [
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelAHASSNomor"],
                        ['class' => "col-sm-4",'items' => ":AHASSNomor"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelAHASSNama"],
                        ['class' => "col-sm-15",'items' => ":AHASSNama"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelAHASSAlamat"],
                        ['class' => "col-sm-14",'items' => ":AHASSAlamat"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelAHASSTelepon"],
                        ['class' => "col-sm-5",'items' => ":AHASSTelepon"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelAHASSKontak"],
                        ['class' => "col-sm-10",'items' => ":AHASSKontak"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelAHASSMainDealer"],
                        ['class' => "col-sm-9",'items' => ":AHASSMainDealer"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelAHASSProvinsi"],
                        ['class' => "col-sm-10",'items' => ":AHASSProvinsi"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelAHASSStatus"],
                        ['class' => "col-sm-9",'items' => ":AHASSStatus"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelAHASSKabupaten"],
                        ['class' => "col-sm-10",'items' => ":AHASSKabupaten"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelAHASSKecamatan"],
                        ['class' => "col-sm-9",'items' => ":AHASSKecamatan"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelAHASSJenisLayanan"],
                        ['class' => "col-sm-10",'items' => ":AHASSJenisLayanan"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelAHASSJenisDealer"],
                        ['class' => "col-sm-9",'items' => ":AHASSJenisDealer"],
                    ],
                ],
            ]
        ],
        [
            'class' => "pull-right",
//            'items' => "{j :btnJurnal1 :btnJurnal j} {p :btnPickingList :btnPrint p} {a :btnAction a}"
            'items' => ":btnAction"
        ],
        [
            ":LabelAHASSNomor"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">No</label>',
            ":LabelAHASSNama"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama</label>',
            ":LabelAHASSAlamat"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
            ":LabelAHASSKontak"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kontak</label>',
            ":LabelAHASSTelepon"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon</label>',
            ":LabelAHASSJenisLayanan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis layanan</label>',
            ":LabelAHASSJenisDealer"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis Dealer</label>',
            ":LabelAHASSProvinsi"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Provinsi</label>',
            ":LabelAHASSKecamatan"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kecamatan</label>',
            ":LabelAHASSKabupaten"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kabupaten</label>',
            ":LabelAHASSMainDealer"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Main Dealer</label>',
            ":LabelAHASSStatus"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',

            ":AHASSNomor"           => $form->field($model, 'AHASSNomor',['template' => '{input}'])->textInput(['autofocus' => 'autofocus','maxlength' => '10']),
            ":AHASSNama"            => $form->field($model, 'AHASSNama',['template' => '{input}'])->textInput(['maxlength' => '75',]),
            ":AHASSAlamat"          => $form->field($model, 'AHASSAlamat',['template' => '{input}'])->textInput(['maxlength' => '150',]),
            ":AHASSTelepon"         => $form->field($model, 'AHASSTelepon',['template' => '{input}'])->textInput(['maxlength' => '50',]),
            ":AHASSKontak"          => $form->field($model, 'AHASSKontak',['template' => '{input}'])->textInput(['maxlength' => '75',]),


            ":AHASSProvinsi"        => $form->field($model, 'AHASSProvinsi',['template' => '{input}'])->textInput(['maxlength' => '30']),
            ":AHASSKecamatan"       => $form->field($model, 'AHASSKecamatan',['template' => '{input}'])->textInput(['maxlength' => '30',]),
            ":AHASSKabupaten"       => $form->field($model, 'AHASSKabupaten',['template' => '{input}'])->textInput(['maxlength' => '50',]),
            ":AHASSMainDealer"      => $form->field($model, 'AHASSMainDealer',['template' => '{input}'])->textInput(['maxlength' => '75',]),
            ":AHASSStatus"          => $form->field($model, 'AHASSJenisLayanan', ['template' => '{input}'])
                                        ->dropDownList([
                                            'A'=>	'AKTIF',
                                            'N'=>	'NON AKTIF',
                                            '1'=>	'Dealer Utama',
                                        ], ['maxlength' => true]),
            ":AHASSJenisLayanan"    => $form->field($model, 'AHASSJenisLayanan', ['template' => '{input}'])
                                        ->dropDownList([
                                            'H12'=>	'H12',
                                            'H123'=> 'H123',
                                            'H1'=> 'H1',
                                            'H23'=> 'H23',
                                            'H2'=> 'H2',
                                        ], ['maxlength' => true]),
            ":AHASSJenisDealer"     => $form->field($model, 'AHASSJenisLayanan', ['template' => '{input}'])
                                        ->dropDownList([
                                            'Regular'=>	'Regular',
                                            'Wing'=>	'Wing',
                                            'Big Wing'=>	'Big Wing',
                                        ], ['maxlength' => true]),
        ],
	    [
		    'url_main'   => 'tdahass',
		    'url_id'     => $id,
		    'url_update' => $url[ 'update' ]
	    ]
    );
    ActiveForm::end();
    ?>
</div>
<?php
$urlIndex   = Url::toRoute( [ 'tdahass/index' ] );
$urlAdd     = Url::toRoute( [ 'tdahass/create','id'=>'new' ] );
$this->registerJs( <<< JS
     $('#btnSave').click(function (event) {
       $('#frm_tdahass_id').submit();
    }); 
    $('#btnCancel').click(function (event) {	  
         window.location.href = '{$url['cancel']}';
    });
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
    $('#btnAdd').click(function (event) {	      
        window.location.href = '{$urlAdd}';
	  });
    
JS);

