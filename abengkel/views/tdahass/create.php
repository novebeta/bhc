<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdahass */

$this->title = 'Tambah AHASS';
$this->params['breadcrumbs'][] = ['label' => 'AHASS', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdahass-create">
    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdahass/update', 'id' => $id ,'action' => 'create'] ),
            'cancel' => Url::toRoute( [ 'tdahass/cancel', 'id' => $id,'action' => 'create' ] )
        ]
    ]) ?>

</div>
