<?php
use abengkel\assets\AppAsset;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Konsumen';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'MotorNoPolisi'  => 'No Polisi',
		'MotorNoMesin'   => 'No Mesin',
		'MotorNoRangka'  => 'No Rangka',
		'CusKTP'         => 'Cus Ktp',
		'CusNama'        => 'Cus Nama',
		'CusAlamat'      => 'Cus Alamat',
		'CusRT'          => 'Cus Rt',
		'CusRW'          => 'Cus Rw',
		'CusProvinsi'    => 'Cus Provinsi',
		'CusKabupaten'   => 'Cus Kabupaten',
		'CusKecamatan'   => 'Cus Kecamatan',
		'CusKelurahan'   => 'Cus Kelurahan',
		'CusKodePos'     => 'Cus Kode Pos',
		'CusTelepon'     => 'Cus Telepon',
		'CusSex'         => 'Cus Sex',
		'CusTempatLhr'   => 'Cus Tempat Lhr',
		'CusAgama'       => 'Cus Agama',
		'CusPekerjaan'   => 'Cus Pekerjaan',
		'CusEmail'       => 'Cus Email',
		'CusKeterangan'  => 'Cus Keterangan',
		'CusKK'          => 'Cus Kk',
		'CusStatus'      => 'Cus Status',
		'MotorType'      => 'Motor Type',
		'MotorWarna'     => 'Motor Warna',
		'MotorTahun'     => 'Motor Tahun',
		'MotorNama'      => 'Motor Nama',
		'MotorKategori'  => 'Motor Kategori',
		'MotorCC'        => 'Motor Cc',
		'AHASSNomor'     => 'Ahass Nomor',
		'NoBukuServis'   => 'No Buku Servis',
	],
	'cmbTgl' => [
		'TglAkhirServis' => 'Tgl Akhir Servis',
		'TglBeli'        => 'Tgl Beli',
		'CusTglLhr'      => 'Cus Tgl Lhr',
	],
	'cmbNum' => [
	],
	'sortname'  => "CusStatus, MotorNoPolisi ",
	'sortorder' => ''
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Tdcustomer::className(),
    'Konsumen',[
            'mode'=> isset($mode)? $mode : ''
    ] ), \yii\web\View::POS_READY );
