<?php
use abengkel\components\FormField;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdcustomer */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function formattdlokasi(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-6" style="text-align: left">' + result.id + '</div>' +
           '<div class="col-xs-18">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
<div class="modal bootbox fade" id="modalCustomer" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="width: 1000px;height: 550px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konsumen</h5>
            </div>
            <!--            <div class="modal-body">-->
            <div class="customer-form">
				<?php
				$form = ActiveForm::begin( [ 'id' => 'frm_tdcustomer_id' ] );
				\abengkel\components\TUi::form(
					[ 'class' => "row-no-gutters",
					  'items' => [
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-2", 'items' => ":LabelMotorNoPolisi" ],
							    [ 'class' => "col-sm-7", 'items' => ":MotorNoPolisi" ],
							    [ 'class' => "col-sm-1" ],
							    [ 'class' => "col-sm-2", 'items' => ":LabelTglAkhirServis" ],
							    [ 'class' => "col-sm-4", 'items' => ":TglAkhirServis" ],
							    [ 'class' => "col-sm-1" ],
							    [ 'class' => "col-sm-2", 'items' => ":LabelCusProvinsi" ],
							    [ 'class' => "col-sm-5", 'items' => ":CusProvinsi" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-2", 'items' => ":LabelCusNama" ],
							    [ 'class' => "col-sm-7", 'items' => ":CusNama" ],
							    [ 'class' => "col-sm-1" ],
							    [ 'class' => "col-sm-2", 'items' => ":LabelCusKTP" ],
							    [ 'class' => "col-sm-4", 'items' => ":CusKTP" ],
							    [ 'class' => "col-sm-1" ],
							    [ 'class' => "col-sm-2", 'items' => ":LabelCusKabupaten" ],
							    [ 'class' => "col-sm-5", 'items' => ":CusKabupaten" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-2", 'items' => ":LabelCusAlamat" ],
							    [ 'class' => "col-sm-7", 'items' => ":CusAlamat" ],
							    [ 'class' => "col-sm-1" ],
							    [ 'class' => "col-sm-2", 'items' => ":LabelCusRT" ],
							    [ 'class' => "col-sm-2", 'items' => ":CusRT" ],
							    [ 'class' => "col-sm-2", 'style' => 'padding-left:3px', 'items' => ":CusRW" ],
							    [ 'class' => "col-sm-1" ],
							    [ 'class' => "col-sm-2", 'items' => ":LabelCusKecamatan" ],
							    [ 'class' => "col-sm-5", 'items' => ":CusKecamatan" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-2", 'items' => ":LabelCusTelepon" ],
							    [ 'class' => "col-sm-7", 'items' => ":CusTelepon" ],
							    [ 'class' => "col-sm-1" ],
							    [ 'class' => "col-sm-2", 'items' => ":LabelCusKodePos" ],
							    [ 'class' => "col-sm-4", 'items' => ":CusKodePos" ],
							    [ 'class' => "col-sm-1" ],
							    [ 'class' => "col-sm-2", 'items' => ":LabelCusKelurahan" ],
							    [ 'class' => "col-sm-5", 'items' => ":CusKelurahan" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-2", 'items' => ":LabelCusEmail" ],
							    [ 'class' => "col-sm-14", 'items' => ":CusEmail" ],
							    [ 'class' => "col-sm-1" ],
							    [ 'class' => "col-sm-2", 'items' => ":LabelCusStatus" ],
							    [ 'class' => "col-sm-5", 'items' => ":CusStatus" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-2", 'items' => ":LabelCusSex" ],
							    [ 'class' => "col-sm-4", 'items' => ":CusSex" ],
							    [ 'class' => "col-sm-1" ],
							    [ 'class' => "col-sm-3", 'items' => ":LabelCusTempatLhr" ],
							    [ 'class' => "col-sm-3", 'items' => ":CusTempatLhr" ],
							    [ 'class' => "col-sm-3", 'style' => 'padding-left:3px', 'items' => ":CusTglLhr" ],
							    [ 'class' => "col-sm-1" ],
							    [ 'class' => "col-sm-2", 'items' => ":LabelCusAgama" ],
							    [ 'class' => "col-sm-5", 'items' => ":CusAgama" ],
						    ],
						  ],
                          [ 'class' => "form-group col-md-24",
                              'items' => [
                                  [ 'class' => "col-sm-2", 'items' => ":LabelCusPekerjaan" ],
                                  [ 'class' => "col-sm-7", 'items' => ":CusPekerjaan" ],
                                  [ 'class' => "col-sm-1" ],
                                  [ 'class' => "col-sm-2", 'items' => ":LabelCusKeterangan" ],
                                  [ 'class' => "col-sm-4", 'items' => ":CusKeterangan" ],
                                  [ 'class' => "col-sm-1" ],
                                  [ 'class' => "col-sm-2", 'items' => ":LabelCusJenis" ],
                                  [ 'class' => "col-sm-5", 'items' => ":CusJenis" ]
                              ],
                          ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-2", 'items' => ":LabelMotorType" ],
							    [ 'class' => "col-sm-5", 'items' => ":MotorType" ],
							    [ 'class' => "col-sm-2", 'style' => 'padding-left:3px', 'items' => ":MotorTahun" ],
							    [ 'class' => "col-sm-1" ],
							    [ 'class' => "col-sm-2", 'items' => ":LabelMotorWarna" ],
							    [ 'class' => "col-sm-4", 'items' => ":MotorWarna" ],
							    [ 'class' => "col-sm-1" ],
							    [ 'class' => "col-sm-2", 'items' => ":LabelMotorCC" ],
							    [ 'class' => "col-sm-5", 'items' => ":MotorCC" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-2", 'items' => ":LabelMotorNama" ],
							    [ 'class' => "col-sm-7", 'items' => ":MotorNama" ],
							    [ 'class' => "col-sm-1" ],
							    [ 'class' => "col-sm-3", 'items' => ":LabelMesinRangka" ],
							    [ 'class' => "col-sm-3", 'items' => ":MotorNoMesin" ],
							    [ 'class' => "col-sm-3", 'style' => 'padding-left:3px', 'items' => ":MotorNoRangka" ],
							    [ 'class' => "col-sm-2", 'style' => 'text-align:center', 'items' => ":LabelMotorKategori" ],
							    [ 'class' => "col-sm-3", 'items' => ":MotorKategori" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-2", 'items' => ":LabelMotorNoMesin" ],
							    [ 'class' => "col-sm-7", 'items' => ":2MotorNoMesin" ],
							    [ 'class' => "col-sm-1" ],
							    [ 'class' => "col-sm-2", 'items' => ":LabelMotorNoRangka" ],
							    [ 'class' => "col-sm-7", 'items' => ":2MotorNoRangka" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-2", 'items' => ":LabelAHHAS" ],
							    [ 'class' => "col-sm-22", 'items' => ":AHHAS" ],
						    ],
						  ],
					  ]
					],
					[ 'class' => "row-no-gutters",
					  'items' => [
						  [ 'class' => "col-md-24",
						    'items' => [
							    [ 'class' => "pull-left", 'items' => "" ],
							    [ 'class' => "pull-right", 'items' => ":btnSave :btnCancel" ],
						    ]
						  ]
					  ]
					],
					[
						":LabelMotorNoPolisi"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Motor No Polisi</label>',
						":LabelMotorNoMesin"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Mesin</label>',
						":LabelMesinRangka"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Mesin / Rangka</label>',
						":LabelMotorNoRangka"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Rangka</label>',
						":LabelCusKK"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">KK</label>',
						":LabelTglAkhirServis" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Servis</label>',
						":LabelCusNama"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama</label>',
						":LabelCusKTP"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">KTP</label>',
						":LabelCusProvinsi"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Provinsi</label>',
						":LabelCusAlamat"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
						":LabelCusRT"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">RT / RW</label>',
						":LabelCusRW"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">/</label>',
						":LabelCusKabupaten"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kabupaten</label>',
						":LabelCusTelepon"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon</label>',
						":LabelCusKecamatan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kecamatan</label>',
						":LabelCusEmail"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Email</label>',
						":LabelCusKelurahan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kelurahan</label>',
						":LabelCusSex"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis Kelamin</label>',
						":LabelCusTempatLhr"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tempat - Tgl Lahir</label>',
						":LabelCusAgama"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Agama</label>',
						":LabelCusPekerjaan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pekerjaan</label>',
						":LabelCusKeterangan"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
                        ":LabelCusJenis"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
						":LabelMotorWarna"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Warna</label>',
						":LabelMotorCC"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Motor CC</label>',
						":LabelMotorNama"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Motor</label>',
						":LabelCusStatus"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',
						":LabelCusKodePos"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode POS</label>',
						":LabelMotorType"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Type / Tahun</label>',
						":LabelMotorKategori"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kategori</label>',
						":LabelAHHAS"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">AHASS</label>',
						":MotorNoPolisi"       => Html::textInput( 'Tdcustomer[MotorNoPolisi]', '', [ 'class' => 'form-control', 'maxlength' => '12' ] ) .
						                          Html::hiddenInput( 'oper' ),
						":CusKK"               => Html::textInput( 'Tdcustomer[CusKK]', '', [ 'class' => 'form-control', 'maxlength' => '16' ] ),
						":CusNama"             => Html::textInput( 'Tdcustomer[CusNama]', '', [ 'class' => 'form-control', 'maxlength' => '50' ] ),
						":CusKTP"              => Html::textInput( 'Tdcustomer[CusKTP]', '', [ 'class' => 'form-control', 'maxlength' => '18' ] ),
						":CusAlamat"           => Html::textInput( 'Tdcustomer[CusAlamat]', '', [ 'class' => 'form-control', 'maxlength' => '75' ] ),
						":CusRT"               => Html::textInput( 'Tdcustomer[CusRT]', '', [ 'class' => 'form-control', 'maxlength' => '3' ] ),
						":CusRW"               => Html::textInput( 'Tdcustomer[CusRW]', '', [ 'class' => 'form-control', 'maxlength' => '3' ] ),
						":CusTelepon"          => Html::textInput( 'Tdcustomer[CusTelepon]', '', [ 'class' => 'form-control', 'maxlength' => '20' ] ),
						":CusEmail"            => Html::textInput( 'Tdcustomer[CusEmail]', '', [ 'class' => 'form-control', 'maxlength' => '50' ] ),
						":CusTempatLhr"        => Html::textInput( 'Tdcustomer[CusTempatLhr]', '', [ 'class' => 'form-control', 'maxlength' => '30' ] ),
						":CusKeterangan"       => Html::textInput( 'Tdcustomer[CusKeterangan]', '', [ 'class' => 'form-control', 'maxlength' => '100' ] ),
                        ":CusJenis"           => FormField::combo( 'CusJenis', [ 'name' => "Tdcustomer[CusJenis]", 'config' => [ 'id' => 'CusJenis', 'data' => [ 'Reguler' => 'Reguler', 'GC' => 'GC', ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
						":MotorTahun"          => Html::textInput( 'Tdcustomer[MotorTahun]', '', [ 'class' => 'form-control', 'maxlength' => '10' ] ),
						":MotorWarna"          => Html::textInput( 'Tdcustomer[MotorWarna]', '', [ 'class' => 'form-control', 'maxlength' => '35' ] ),
						":MotorKategori"       => Html::textInput( 'Tdcustomer[MotorKategori]', '', [ 'class' => 'form-control', 'maxlength' => '15', 'readonly' => true ] ),
						":MotorCC"             => Html::textInput( 'Tdcustomer[MotorCC]', '', [ 'class' => 'form-control' ] ),
						":TglAkhirServis"      => FormField::dateInput( [ 'name' => 'Tdcustomer[TglAkhirServis]', 'config' => [ 'value' => '1900-01-01' ] ] ),
						":CusTglLhr"           => FormField::dateInput( [ 'name' => 'Tdcustomer[CusTglLhr]', 'config' => [ 'value' => '1900-01-01' ] ] ),
						":MotorNama"           => '<select id="MotorNama" name="Tdcustomer[MotorNama]" style="width: 100%"></select>',
						":MotorType"           => '<select id="MotorType" name="Tdcustomer[MotorType]" style="width: 100%"></select>',
						":MotorNoMesin"        => '<select id="CmbMotorNoMesin" name="Tdcustomer[CmbMotorNoMesin]" style="width: 100%"></select>',
						":MotorNoRangka"       => '<select id="CmbMotorNoRangka" name="Tdcustomer[CmbMotorNoRangka]" style="width: 100%"></select>',
						":CusSex"              => FormField::combo( 'CusSex', [ 'name' => "Tdcustomer[CusSex]", 'config' => [ 'id' => 'CusSex', 'data' => [ 'Perempuan' => 'Perempuan', 'Laki-Laki' => 'Laki-Laki', ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
						":CusAgama"            => FormField::combo( 'CusAgama', [ 'name' => "Tdcustomer[CusAgama]", 'config' => [ 'id' => 'CusAgama', 'data' => [ 'Islam' => 'Islam', 'Kristen' => 'Kristen', 'Katholik' => 'Katholik', 'Hindu' => 'Hindu', 'Budha' => 'Budha', 'Lain-Lain' => 'Lain-Lain', ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
						":CusStatus"           => FormField::combo( 'CusStatus', [ 'name' => "Tdcustomer[CusStatus]", 'config' => [ 'id' => 'CusStatus', 'data' => [ 'A' => 'AKTIF', 'N' => 'NON AKTIF', ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
						":CusPekerjaan"        => FormField::combo( 'CusPekerjaan', [ 'name' => "Tdcustomer[CusPekerjaan]", 'config' => [ 'id' => 'CusPekerjaan', 'data' => [ 'Pegawai Negeri' => 'Pegawai Negeri', 'Pegawai Swasta' => 'Pegawai Swasta', 'Ojek' => 'Ojek', 'Wiraswasta/Pedagang' => 'Wiraswasta/Pedagang', 'Mahasiswa/ Pelajar' => 'Mahasiswa/ Pelajar', 'Guru/Dosen' => 'Guru/Dosen', 'TNI/Polri' => 'TNI/Polri', 'Ibu Rumah Tangga' => 'Ibu Rumah Tangga', 'Petani / Nelayan' => 'Petani / Nelayan', 'Profesional' => 'Profesional', ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
						":AHHAS"               => FormField::combo( 'AHASSNomor', [ 'name' => "Tdcustomer[AHASSNomor]", 'config' => [ 'id' => 'AHASSNomor' ], 'extraOptions' => [ 'altLabel' => [ 'AHASSNama' ] ] ] ),
						":CusProvinsi"         => FormField::combo( 'Provinsi', [ 'name' => 'Tdcustomer[CusProvinsi]', 'config' => [ 'id' => 'CusProvinsi' ], 'extraOptions' => [ 'KabupatenId' => 'CusKabupaten', 'KabupatenValue' => '', 'simpleCombo' => true ] ] ),
						":CusKabupaten"        => FormField::combo( 'Kabupaten', [ 'name' => 'Tdcustomer[CusKabupaten]', 'config' => [ 'id' => 'CusKabupaten' ], 'extraOptions' => [ 'KecamatanId' => 'CusKecamatan', 'KecamatanValue' => '', 'simpleCombo' => true ] ] ),
						":CusKecamatan"        => FormField::combo( 'Kecamatan', [ 'name' => 'Tdcustomer[CusKecamatan]', 'config' => [ 'id' => 'CusKecamatan' ], 'extraOptions' => [ 'KelurahanId' => 'CusKelurahan', 'KelurahanValue' => '', 'simpleCombo' => true ] ] ),
						":CusKelurahan"        => FormField::combo( 'Kelurahan', [ 'name' => 'Tdcustomer[CusKelurahan]', 'config' => [ 'id' => 'CusKelurahan' ], 'extraOptions' => [ 'KodePosId' => 'CusKodePos', 'KodePosValue' => '', 'simpleCombo' => true ] ] ),
						":CusKodePos"          => FormField::combo( 'KodePos', [ 'name' => 'Tdcustomer[CusKodePos]', 'config' => [ 'id' => 'CusKodePos' ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
						":btnSave"             => Html::button( '<i class="glyphicon glyphicon-floppy-disk"></i> Simpan', [ 'class' => 'btn btn-info btn-primary ', 'id' => 'btnSaveCustomer' ] ),
						":btnCancel"           => Html::Button( '<i class="fa fa-ban"></i> Batal', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancelCustomer', 'style' => 'width:80px' ] ),
						":2MotorNoMesin"       => Html::textInput( 'Tdcustomer[MotorNoMesin]', '', [ 'class' => 'form-control', 'maxlength' => '25' ] ),
						":2MotorNoRangka"      => Html::textInput( 'Tdcustomer[MotorNoRangka]', '', [ 'class' => 'form-control', 'maxlength' => '25' ] ),
					],
					[
						'jsBtnCancel' => false,
						'jsBtnAdd'    => false,
						'jsBtnEdit'   => false,
						'jsBtnDaftar' => false,
						'jsBtnPrint'  => false,
						'jsBtnJurnal' => false,
						'jsBtnDelete' => false,
					]
				);
				ActiveForm::end();
				?>
            </div>
            <!--            </div>-->
        </div>
    </div>
</div>