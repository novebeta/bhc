<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdcustomer */

$this->title = 'Edit Konsumen : ' . $model->MotorNoPolisi;
$this->params['breadcrumbs'][] = ['label' => 'Konsumen', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="tdcustomer-update">
    <?= $this->render('_formedit', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdcustomer/update', 'id' => $id ] ),
            'cancel' => Url::toRoute( [ 'tdcustomer/cancel', 'id' => $id,'action' => 'update' ] )
        ]
    ]) ?>

</div>
