<?php
use abengkel\components\FormField;
use abengkel\models\Tdahass;
use abengkel\models\Tdarea;
use common\components\Custom;
use common\components\General;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\web\JsExpression;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdcustomer */
/* @var $form yii\widgets\ActiveForm */
$url[ 'cancel' ] = Url::toRoute( 'tdcustomer/index' );
$format          = <<< SCRIPT
function formattdlokasi(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-6" style="text-align: left">' + result.id + '</div>' +
           '<div class="col-xs-18">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape          = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="tdcustomer-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'frm_tdcustomer_id' ,'enableClientValidation'=>false ] );
		\abengkel\components\TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelMotorNoPolisi" ],
					    [ 'class' => "col-sm-4", 'items' => ":MotorNoPolisi" ],
					    [ 'class' => "col-sm-3", 'items' => ":ZidCustomer" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-1", 'items' => ":LabelCusKK" ],
					    [ 'class' => "col-sm-4", 'items' => ":CusKK" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-3", 'items' => ":LabelTglAkhirServis" ],
					    [ 'class' => "col-sm-5", 'items' => ":TglAkhirServis" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusNama" ],
					    [ 'class' => "col-sm-7", 'items' => ":CusNama" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-1", 'items' => ":LabelCusKTP" ],
					    [ 'class' => "col-sm-4", 'items' => ":CusKTP" ],
					    [ 'class' => "col-sm-2" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusProvinsi" ],
					    [ 'class' => "col-sm-5", 'items' => ":CusProvinsi" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusAlamat" ],
					    [ 'class' => "col-sm-7", 'items' => ":CusAlamat" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusRT" ],
					    [ 'class' => "col-sm-2", 'items' => ":CusRT" ],
					    [ 'class' => "col-sm-2", 'style' => 'padding-left:3px', 'items' => ":CusRW" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusKabupaten" ],
					    [ 'class' => "col-sm-5", 'items' => ":CusKabupaten" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusTelepon" ],
					    [ 'class' => "col-sm-7", 'items' => ":CusTelepon" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusKodePos" ],
					    [ 'class' => "col-sm-4", 'items' => ":CusKodePos" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusKecamatan" ],
					    [ 'class' => "col-sm-5", 'items' => ":CusKecamatan" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusEmail" ],
					    [ 'class' => "col-sm-14", 'items' => ":CusEmail" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusKelurahan" ],
					    [ 'class' => "col-sm-5", 'items' => ":CusKelurahan" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusSex" ],
					    [ 'class' => "col-sm-4", 'items' => ":CusSex" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-3", 'items' => ":LabelCusTempatLhr" ],
					    [ 'class' => "col-sm-3", 'items' => ":CusTempatLhr" ],
					    [ 'class' => "col-sm-3", 'style' => 'padding-left:3px', 'items' => ":CusTglLhr" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusAgama" ],
					    [ 'class' => "col-sm-5", 'items' => ":CusAgama" ],
				    ],
				  ],
                  [ 'class' => "form-group col-md-24",
                      'items' => [
                          [ 'class' => "col-sm-2", 'items' => ":LabelCusPekerjaan" ],
                          [ 'class' => "col-sm-7", 'items' => ":CusPekerjaan" ],
                          [ 'class' => "col-sm-1" ],
                          [ 'class' => "col-sm-2", 'items' => ":LabelCusKeterangan" ],
                          [ 'class' => "col-sm-4", 'items' => ":CusKeterangan" ],
                          [ 'class' => "col-sm-1" ],
                          [ 'class' => "col-sm-2", 'items' => ":LabelCusJenis" ],
                          [ 'class' => "col-sm-5", 'items' => ":CusJenis" ]
                      ],
                  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelMotorType" ],
					    [ 'class' => "col-sm-5", 'items' => ":MotorType" ],
					    [ 'class' => "col-sm-2", 'style' => 'padding-left:3px', 'items' => ":MotorTahun" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelMotorWarna" ],
					    [ 'class' => "col-sm-2", 'items' => ":MotorWarna" ],
					    [ 'class' => "col-sm-2", 'style' => 'padding-left:3px', 'items' => ":MotorCC" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelAHHAS" ],
					    [ 'class' => "col-sm-5", 'items' => ":AHHAS" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelMotorNoMesin" ],
					    [ 'class' => "col-sm-7", 'items' => ":MotorNoMesin" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelMotorNoRangka" ],
					    [ 'class' => "col-sm-12", 'items' => ":MotorNoRangka" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelMotorNama" ],
					    [ 'class' => "col-sm-7", 'items' => ":MotorNama" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelMotorKategori" ],
					    [ 'class' => "col-sm-4", 'items' => ":MotorKategori" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusStatus" ],
					    [ 'class' => "col-sm-5", 'items' => ":CusStatus" ],
				    ],
				  ],
			  ]
			],
			[
				'class' => "pull-right",
				'items' => ":btnAction"
			],
			[
				":LabelZidCustomer"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Z ID Customer</label>',
				":LabelMotorNoPolisi"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode</label>',
				":LabelMotorNoMesin"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Mesin</label>',
				":LabelMotorNoRangka"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Rangka</label>',
				":LabelCusKK"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">KK</label>',
				":LabelTglAkhirServis" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Transaksi Terakhir</label>',
				":LabelCusNama"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama</label>',
				":LabelCusKTP"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">KTP</label>',
				":LabelCusProvinsi"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Provinsi</label>',
				":LabelCusAlamat"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
				":LabelCusRT"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">RT / RW</label>',
				":LabelCusRW"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">/</label>',
				":LabelCusKabupaten"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kabupaten</label>',
				":LabelCusTelepon"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon</label>',
				":LabelCusKecamatan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kecamatan</label>',
				":LabelCusEmail"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Email</label>',
				":LabelCusKelurahan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kelurahan</label>',
				":LabelCusSex"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis Kelamin</label>',
				":LabelCusTempatLhr"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tempat - Tgl Lahir</label>',
				":LabelCusAgama"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Agama</label>',
				":LabelCusPekerjaan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pekerjaan</label>',
				":LabelCusKeterangan"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
                ":LabelCusJenis"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				":LabelMotorWarna"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Warna / CC</label>',
				":LabelMotorNama"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Motor</label>',
				":LabelCusStatus"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',
				":LabelCusKodePos"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode POS</label>',
				":LabelMotorType"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Type / Tahun</label>',
				":LabelMotorKategori"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kategori</label>',
				":LabelAHHAS"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">AHASS</label>',
				":MotorNoPolisi"       => $form->field( $model, 'MotorNoPolisi', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '12', 'autofocus' => 'autofocus' ] ),
				":MotorNoMesin"        => $form->field( $model, 'MotorNoMesin', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '25' ] ),
				":MotorNoRangka"       => $form->field( $model, 'MotorNoRangka', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '25' ] ),
				":CusKK"               => $form->field( $model, 'CusKK', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '16' ] ),
				":TglAkhirServis"      => FormField::dateInput( [ 'name' => 'BMTgl', 'config' => [ 'value' => General::asDate( $model[ 'TglAkhirServis' ] ) ] ] ),
				":CusNama"             => $form->field( $model, 'CusNama', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '50' ] ),
				":CusKTP"              => $form->field( $model, 'CusKTP', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '18' ] ),
				":CusProvinsi"         => $form->field( $model, 'CusProvinsi', [ 'template' => '{input}' ] )->widget( Select2::class, [ 'value' => $model->CusKabupaten, 'theme' => Select2::THEME_DEFAULT, 'pluginOptions' => [ 'highlight' => true ], 'data' => Tdarea::find()->provinsi() ] ),
				":CusAlamat"           => $form->field( $model, 'CusAlamat', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '75' ] ),
				":CusRT"               => $form->field( $model, 'CusRT', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '3' ] ),
				":CusRW"               => $form->field( $model, 'CusRW', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '3' ] ),
				":CusKabupaten"        => $form->field( $model, 'CusKabupaten', [ 'template' => '{input}' ] )->widget( Select2::class, [ 'value' => $model->CusKabupaten, 'theme' => Select2::THEME_DEFAULT, 'pluginOptions' => [ 'highlight' => true ], 'data' => Tdarea::find()->kabupaten() ] ),
				":CusTelepon"          => $form->field( $model, 'CusTelepon', [ 'template' => '{input}' ] )->textInput(),
				":CusKodePos"          => $form->field( $model, 'CusKodePos', [ 'template' => '{input}' ] )->widget( Select2::class, [ 'value' => $model->CusKabupaten, 'theme' => Select2::THEME_DEFAULT, 'pluginOptions' => [ 'highlight' => true ], 'data' => Tdarea::find()->KodePos() ] ),
				":CusKecamatan"        => $form->field( $model, 'CusKecamatan', [ 'template' => '{input}' ] )->widget( Select2::class, [ 'value' => $model->CusKabupaten, 'theme' => Select2::THEME_DEFAULT, 'pluginOptions' => [ 'highlight' => true ], 'data' => Tdarea::find()->kecamatan() ] ),
				":CusEmail"            => $form->field( $model, 'CusEmail', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '50' ] ),
				":CusKelurahan"        => $form->field( $model, 'CusKelurahan', [ 'template' => '{input}' ] )->widget( Select2::class, [ 'value' => $model->CusKabupaten, 'theme' => Select2::THEME_DEFAULT, 'pluginOptions' => [ 'highlight' => true ], 'data' => Tdarea::find()->kelurahan() ] ),
				":CusSex"              => $form->field( $model, 'CusSex', [ 'template' => '{input}' ] )->dropDownList( [ 'Perempuan' => 'Perempuan', 'Laki-Laki' => 'Laki-Laki', ], [ 'maxlength' => true ] ),
				":CusTempatLhr"        => $form->field( $model, 'CusTempatLhr', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '30' ] ),
				":CusTglLhr"           => DatePicker::widget( [ 'name' => 'CusTglLhr', 'type' => DatePicker::TYPE_INPUT, 'value' => $model[ 'CusTglLhr' ], 'pluginOptions' => [ 'autoclose' => true, 'format' => 'dd/mm/yyyy' ] ] ),
//                ":TglAkhirServis"      => FormField::dateInput( [ 'name' => 'BMTgl', 'config' => [ 'value' => General::asDate( $model[ 'TglAkhirServis' ] ) ] ] ),
				":CusAgama"            => $form->field( $model, 'CusAgama', [ 'template' => '{input}' ] )->dropDownList( [ 'Islam' => 'Islam', 'Kristen' => 'Kristen', 'Katholik' => 'Katholik', 'Hindu' => 'Hindu', 'Budha' => 'Budha', 'Lain-Lain' => 'Lain-Lain', ], [ 'maxlength' => true ] ),
				":CusPekerjaan"        => $form->field( $model, 'CusPekerjaan', [ 'template' => '{input}' ] )->dropDownList(
				                            [
                                                'Pegawai Negeri' => 'Pegawai Negeri',
                                                'Pegawai Swasta' => 'Pegawai Swasta',
                                                'Ojek' => 'Ojek',
                                                'Wiraswasta/Pedagang' => 'Wiraswasta/Pedagang',
                                                'Mahasiswa/ Pelajar' => 'Mahasiswa/ Pelajar',
                                                'Guru/Dosen' => 'Guru/Dosen',
                                                'TNI/Polri' => 'TNI/Polri',
                                                'Ibu Rumah Tangga' => 'Ibu Rumah Tangga',
                                                'Petani/Nelayan' => 'Petani/Nelayan',
                                                'Profesional (Dokter/Pengacara, dll)' => 'Profesional (Dokter/Pengacara, dll)',
                                                'Akuntan' => 'Akuntan',
                                                'Anggota BPK' => 'Anggota BPK',
                                                'Anggota DPD' => 'Anggota DPD',
                                                'Anggota DPRD Kabupaten/Kota' => 'Anggota DPRD Kabupaten/Kota',
                                                'Anggota DPRD Provinsi' => 'Anggota DPRD Provinsi',
                                                'Anggota DPR-RI' => 'Anggota DPR-RI',
                                                'Anggota Kabinet/Kementerian' => 'Anggota Kabinet/Kementerian',
                                                'Anggota Mahkamah Konstitusi' => 'Anggota Mahkamah Konstitusi',
                                                'Apoteker' => 'Apoteker',
                                                'Arsitek' => 'Arsitek',
                                                'Belum/Tidak Bekerja' => 'Belum/Tidak Bekerja',
                                                'Biarawati' => 'Biarawati',
                                                'Bidan' => 'Bidan',
                                                'Bupati' => 'Bupati',
                                                'Buruh Harian Lepas' => 'Buruh Harian Lepas',
                                                'Buruh Nelayan/Perikanan' => 'Buruh Nelayan/Perikanan',
                                                'Buruh Peternakan' => 'Buruh Peternakan',
                                                'Buruh Tani/Perkebunan' => 'Buruh Tani/Perkebunan',
                                                'Dokter' => 'Dokter',
                                                'Dosen' => 'Dosen',
                                                'Duta Besar' => 'Duta Besar',
                                                'Gubernur' => 'Gubernur',
                                                'Guru' => 'Guru',
                                                'Imam Mesjid' => 'Imam Mesjid',
                                                'Industri' => 'Industri',
                                                'Juru Masak' => 'Juru Masak',
                                                'Karyawan BUMD' => 'Karyawan BUMD',
                                                'Karyawan BUMN' => 'Karyawan BUMN',
                                                'Karyawan Honorer' => 'Karyawan Honorer',
                                                'Karyawan Swasta' => 'Karyawan Swasta',
                                                'Kepala Desa' => 'Kepala Desa',
                                                'Kepolisian RI' => 'Kepolisian RI',
                                                'Konstruksi' => 'Konstruksi',
                                                'Konsultan' => 'Konsultan',
                                                'Lainnya' => 'Lainnya',
                                                'Mekanik' => 'Mekanik',
                                                'Mengurus Rumah Tangga' => 'Mengurus Rumah Tangga',
                                                'Nelayan/Perikanan' => 'Nelayan/Perikanan',
                                                'Notaris' => 'Notaris',
                                                'Paraji' => 'Paraji',
                                                'Paranormal' => 'Paranormal',
                                                'Pastor' => 'Pastor',
                                                'Pedagang' => 'Pedagang',
                                                'Pegawai Negeri Sipil' => 'Pegawai Negeri Sipil',
                                                'Pelajar/Mahasiswa' => 'Pelajar/Mahasiswa',
                                                'Pelaut' => 'Pelaut',
                                                'Pembantu Rumah Tangga' => 'Pembantu Rumah Tangga',
                                                'Penata Busana' => 'Penata Busana',
                                                'Penata Rambut' => 'Penata Rambut',
                                                'Penata Rias' => 'Penata Rias',
                                                'Pendeta' => 'Pendeta',
                                                'Peneliti' => 'Peneliti',
                                                'Pengacara' => 'Pengacara',
                                                'Pensiunan' => 'Pensiunan',
                                                'Penterjemah' => 'Penterjemah',
                                                'Penyiar Radio' => 'Penyiar Radio',
                                                'Penyiar Televisi' => 'Penyiar Televisi',
                                                'Perancang Busana' => 'Perancang Busana',
                                                'Perangkat Desa' => 'Perangkat Desa',
                                                'Perawat' => 'Perawat',
                                                'Perdagangan' => 'Perdagangan',
                                                'Petani' => 'Petani',
                                                'Pekebun' => 'Pekebun',
                                                'Peternak' => 'Peternak',
                                                'Pialang' => 'Pialang',
                                                'Pilot' => 'Pilot',
                                                'Presiden' => 'Presiden',
                                                'Promotor Acara' => 'Promotor Acara',
                                                'Psikiater' => 'Psikiater',
                                                'Psikolog' => 'Psikolog',
                                                'Seniman' => 'Seniman',
                                                'Sopir' => 'Sopir',
                                                'Tabib' => 'Tabib',
                                                'Tentara Nasional Indonesia' => 'Tentara Nasional Indonesia',
                                                'Transportasi' => 'Transportasi',
                                                'Tukang Batu' => 'Tukang Batu',
                                                'Tukang Cukur' => 'Tukang Cukur',
                                                'Tukang Gigi' => 'Tukang Gigi',
                                                'Tukang Jahit' => 'Tukang Jahit',
                                                'Tukang Kayu' => 'Tukang Kayu',
                                                'Tukang Las/Pandai Besi' => 'Tukang Las/Pandai Besi',
                                                'Tukang Listrik' => 'Tukang Listrik',
                                                'Tukang Sol Sepatu' => 'Tukang Sol Sepatu',
                                                'Ustadz/Mubaligh' => 'Ustadz/Mubaligh',
                                                'Wakil Bupati' => 'Wakil Bupati',
                                                'Wakil Gubernur' => 'Wakil Gubernur',
                                                'Wakil Presiden' => 'Wakil Presiden',
                                                'Wakil Walikota' => 'Wakil Walikota',
                                                'Walikota' => 'Walikota',
                                                'Wartawan' => 'Wartawan',
                                                'Wiraswasta' => 'Wiraswasta'
                                            ], [ 'maxlength' => true ] ),
				":CusKeterangan"       => $form->field( $model, 'CusKeterangan', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '100' ] ),
                ":CusJenis"           => $form->field( $model, 'CusJenis', [ 'template' => '{input}' ] )->dropDownList( [ 'Reguler' => 'Reguler', 'GC' => 'GC', ], [ 'maxlength' => true ] ),
				":MotorType"           => FormField::combo( 'MotorType', [ 'config' => [ 'value' => $model->MotorType ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":MotorTahun"          => $form->field( $model, 'MotorTahun', [ 'template' => '{input}' ] )->textInput(),
				":MotorWarna"          => $form->field( $model, 'MotorWarna', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '35' ] ),
				":MotorNama"           => $form->field( $model, 'MotorNama', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '100', 'readonly' => true ] ),
				":CusStatus"           => $form->field( $model, 'CusStatus', [ 'template' => '{input}' ] )->dropDownList( [ 'A' => 'AKTIF', 'N' => 'NON AKTIF', ], [ 'maxlength' => true ] ),
				":MotorCC"             => $form->field( $model, 'MotorCC', [ 'template' => '{input}' ] )->textInput(),
				":MotorKategori"       => $form->field( $model, 'MotorKategori', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '15', 'readonly' => true ] ),
				":ZidCustomer"       => $form->field( $model, 'ZidCustomer', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '15', 'readonly' => true ] ),
				":AHHAS"               => $form->field( $model, 'AHASSNomor', [ 'template' => '{input}' ] )
				                               ->widget( Select2::class, [
					                               'value'         => $model->AHASSNomor,
					                               'theme'         => Select2::THEME_DEFAULT,
					                               'data'          => Tdahass::find()->ahass(),
					                               'pluginOptions' => [
						                               'dropdownAutoWidth' => true,
						                               'templateResult'    => new JsExpression( 'formattdlokasi' ),
						                               'templateSelection' => new JsExpression( 'formattdlokasi' ),
						                               'matcher'           => new JsExpression( 'matchCustom' ),
						                               'escapeMarkup'      => $escape ] ] ),
			],
			[
				'url_main'   => 'tdcustomer',
				'url_id'     => $id,
				'url_update' => $url[ 'update' ]
			]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlTdArea = Url::toRoute( 'tdarea/find' );
$this->registerJs( <<< JS
    let CusProvinsi = $('#tdcustomer-cusprovinsi'),
        CusKabupaten = $('#tdcustomer-cuskabupaten'),
        CusKecamatan = $('#tdcustomer-cuskecamatan'),
        CusKelurahan = $('#tdcustomer-cuskelurahan');
    CusProvinsi.on("change", function (e) { console.log()
        CusKabupaten.utilSelect2().loadRemote('$urlTdArea', { mode: 'combo', field: 'Kabupaten', Provinsi: this.value }, true, function(response) {
            CusKabupaten.val(null).trigger('change');
            CusKabupaten.val(null).trigger('change');
        });
    });

    CusKabupaten.on("change", function (e) {
        CusKecamatan.utilSelect2().loadRemote('$urlTdArea', { mode: 'combo', field: 'Kecamatan', Kabupaten: this.value }, true, function(response) {
            CusKecamatan.val(null).trigger('change');
        });
    });
    
    CusKecamatan.on("change", function (e) {
        CusKelurahan.utilSelect2().loadRemote('$urlTdArea', { mode: 'combo', field: 'Kelurahan', Kecamatan: this.value }, true, function(response) {
            CusKelurahan.val(null).trigger('change');
        });
    });
    
    $('#btnSave').click(function (event) {
        let MotorNoMesin = $('#tdcustomer-motornomesin').val();
        let MotorNoRangka = $('#tdcustomer-motornorangka').val();
        if ( MotorNoMesin.length < 12 ){
               bootbox.alert({message:'No Mesin yang kurang dari 12 karakter', size: 'small'});
               return;
           } 
        if ( MotorNoRangka < 16){
               bootbox.alert({message:'No Rangka yang kurang dari 16 karakter', size: 'small'});
               return;
           } 
        $('#frm_tdcustomer_id').attr('action','{$url['update']}');
       $('#frm_tdcustomer_id').submit();
    }); 
    
    
    
    
JS
);
