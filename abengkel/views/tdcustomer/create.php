<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdcustomer */

$this->title = 'Tambah Konsumen';
$this->params['breadcrumbs'][] = ['label' => 'Konsumen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdcustomer-create">
    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdcustomer/create','action' => 'create' ] ),
            'cancel' => Url::toRoute( [ 'tdcustomer/cancel', 'id' => $id,'action' => 'create' ] )
        ]
    ]) ?>

</div>
