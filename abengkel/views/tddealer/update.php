<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tddealer */
$this->title                   = 'Edit Dealer : ' . $model->DealerKode;
$this->params['breadcrumbs'][] = [ 'label' => 'Dealer', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tddealer-update">
	<?= $this->render( '_form', [
		'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tddealer/update', 'id' => $id,'action' => 'update' ] ),
            'cancel' => Url::toRoute( [ 'tddealer/cancel', 'id' => $id,'action' => 'update' ] )
        ]
	] ) ?>
</div>
