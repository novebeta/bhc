<?php
use abengkel\components\FormField;
use common\components\Custom;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tddealer */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="tddealer-form">
    <?php
    $form = ActiveForm::begin(['id' => 'frm_tddealer_id'  ,'enableClientValidation'=>false]);
    \abengkel\components\TUi::form(
        ['class' => "row-no-gutters",
            'items' => [
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelDealerKode"],
                        ['class' => "col-sm-4",'items' => ":DealerKode"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelDmain"],
                        ['class' => "col-sm-2",'items' => ":Dmain"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelDastra"],
                        ['class' => "col-sm-3",'items' => ":Dastra"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelDealerStatus"],
                        ['class' => "col-sm-4",'items' => ":DealerStatus"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelDealerNama"],
                        ['class' => "col-sm-12",'items' => ":DealerNama"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelDealerContact"],
                        ['class' => "col-sm-7",'items' => ":DealerContact"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelDealerAlamat"],
                        ['class' => "col-sm-12",'items' => ":DealerAlamat"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelDealerKota"],
                        ['class' => "col-sm-7",'items' => ":DealerKota"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelDealerTelepon"],
                        ['class' => "col-sm-5",'items' => ":DealerTelepon"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-1",'items' => ":LabelDealerFax"],
                        ['class' => "col-sm-5",'items' => ":DealerFax"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelDealerProvinsi"],
                        ['class' => "col-sm-7",'items' => ":DealerProvinsi"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelDealerKeterangan"],
                        ['class' => "col-sm-12",'items' => ":DealerKeterangan"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelDealerEmail"],
                        ['class' => "col-sm-7",'items' => ":DealerEmail"],
                    ],
                ],

            ]
        ],
        [
            'class' => "pull-right",
            'items' => ":btnSave :btnCancel"
        ],
        [
            ":LabelDealerKode"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode Dealer</label>',
            ":LabelDmain"               => '<label class="control-label" style="margin: 0; padding: 6px 0;">Main Dealer</label>',
            ":LabelDastra"              => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode Astra</label>',
            ":LabelDealerStatus"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',
            ":LabelDealerNama"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Dealer</label>',
            ":LabelDealerContact"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kontak</label>',
            ":LabelDealerAlamat"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
            ":LabelDealerKota"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kota</label>',
            ":LabelDealerTelepon"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon</label>',
            ":LabelDealerFax"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">Fax</label>',
            ":LabelDealerProvinsi"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Provinsi</label>',
            ":LabelDealerKeterangan"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
            ":LabelDealerEmail"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Email</label>',

            ":DealerKode"         => $form->field($model, 'DealerKode',['template' => '{input}'])->textInput(['autofocus' => 'autofocus', 'maxlength' => '10']),
            ":Dmain"              => $form->field($model, 'DealerKodeDMain',['template' => '{input}'])->textInput(['maxlength' => '3',]),
            ":Dastra"             => $form->field($model, 'DealerKodeDAstra',['template' => '{input}'])->textInput(['maxlength' => '5',]),
            ":DealerStatus"       => FormField::combo( 'DealerStatus',[  'extraOptions' => ['simpleCombo' => true]] ),
            ":DealerNama"         => $form->field($model, 'DealerNama',['template' => '{input}'])->textInput(['maxlength' => '50',]),
            ":DealerContact"      => $form->field($model, 'DealerContact',['template' => '{input}'])->textInput(['maxlength' => '50',]),
            ":DealerAlamat"       => $form->field($model, 'DealerAlamat',['template' => '{input}'])->textInput(['maxlength' => '75',]),
            ":DealerKota"         => $form->field($model, 'DealerKota',['template' => '{input}'])->textInput(['maxlength' => '50',]),
            ":DealerTelepon"      => $form->field($model, 'DealerTelepon',['template' => '{input}'])->textInput(['maxlength' => '75',]),
            ":DealerFax"          => $form->field($model, 'DealerFax',['template' => '{input}'])->textInput(['maxlength' => '50',]),
            ":DealerProvinsi"     => $form->field($model, 'DealerProvinsi',['template' => '{input}'])->textInput(['maxlength' => '50',]),
            ":DealerKeterangan"   => $form->field($model, 'DealerKeterangan',['template' => '{input}'])->textInput(['maxlength' => '100',]),
            ":DealerEmail"        => $form->field($model, 'DealerEmail',['template' => '{input}'])->textInput(['maxlength' => '25',]),

        ],
	    [
		    'url_main'   => 'tddealer',
		    'url_id'     => $id,
		    'url_update' => $url[ 'update' ]
	    ]
    );
    ActiveForm::end();
    ?>
</div>
<?php
$urlIndex   = Url::toRoute( [ 'tddealer/index' ] );
$urlAdd     = Url::toRoute( [ 'tddealer/create','id'=>'new' ] );
$this->registerJs( <<< JS
     $('#btnSave').click(function (event) {
       $('#frm_tddealer_id').submit();
    }); 
    $('#btnCancel').click(function (event) {	  
         window.location.href = '{$url['cancel']}';
    });
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
    $('#btnAdd').click(function (event) {	      
        window.location.href = '{$urlAdd}';
	  });
    
    


JS);
