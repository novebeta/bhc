<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tddealer */
$this->title                   = 'Tambah Dealer';
$this->params['breadcrumbs'][] = [ 'label' => 'Dealer', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tddealer-create">
	<?= $this->render( '_form', [
		'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tddealer/update', 'id' => $id ,'action' => 'create'] ),
            'cancel' => Url::toRoute( [ 'tddealer/cancel', 'id' => $id,'action' => 'create' ] )
        ]
	] ) ?>
</div>
