<?php
use abengkel\assets\AppAsset;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
AppAsset::register($this);
$this->title = 'Dealer';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'DealerKode'       => 'Kode',
		'DealerNama'       => 'Nama',
		'DealerContact'    => 'Kontak',
		'DealerAlamat'     => 'Alamat',
		'DealerKota'       => 'Kota',
		'DealerProvinsi'   => 'Provinsi',
		'DealerTelepon'    => 'Telepon',
		'DealerFax'        => 'Fax',
		'DealerEmail'      => 'Email',
		'DealerKeterangan' => 'Keterangan',
		'DealerStatus'     => 'Status',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
	'sortname'  => "DealerStatus, DealerKode",
	'sortorder' => ''
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Tddealer::className() ,'Dealer'), \yii\web\View::POS_READY );