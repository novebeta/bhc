<?php
use abengkel\components\FormField;
use abengkel\components\Menu;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
$PosKode = Menu::getUserLokasi()['PosKode'];
$defCOA = \common\components\General::cCmd("SELECT NoAccount  FROM tdpos WHERE PosKode = :PosKode",[':PosKode' => $PosKode])->queryScalar();
?>
    <div class="modal bootbox fade" id="modalBayar" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document" style="width: 450px;height: 550px;">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bayar-form">
						<?php
						$form = ActiveForm::begin( [ 'id' => 'form_bayar_id' ] );
						\abengkel\components\TUi::form(
							[ 'class' => "row-no-gutters",
							  'items' => [
								  [ 'class' => "form-group col-lg-24",
								    'items' => [
									    [ 'class' => "col-lg-8" ],
									    [ 'class' => "col-lg-16", 'items' => ":LabelPembayaran" ],
								    ],
								  ],
								  [ 'class' => "form-group col-lg-24",
								    'items' => [
									    [ 'class' => "col-md-5 col-sm-5 col-lg-5 col-xs-5", 'items' => ":LabelNomor" ],
									    [ 'class' => "col-md-8 col-sm-8 col-lg-8 col-xs-8", 'items' => ":Nomor" ],
									    [ 'class' => "col-md-1 col-sm-1 col-lg-1 col-xs-1" ],
									    [ 'class' => "col-md-4 col-sm-4 col-lg-4 col-xs-4", 'items' => ":LabelTgl" ],
									    [ 'class' => "col-md-6 col-sm-6 col-lg-6 col-xs-6", 'items' => ":Tgl" ],
								    ],
								  ],
								  [ 'class' => "form-group col-lg-24",
								    'items' => [
									    [ 'class' => "col-lg-24 col-xs-24", 'items' => "<p>" ],
								    ]
								  ],
								  [ 'class' => "form-group col-lg-24",
								    'items' => [
									    [ 'class' => "col-md-5 col-sm-5 col-lg-5 col-xs-5", 'items' => ":LabelCaraBayar" ],
									    [ 'class' => "col-md-8 col-sm-8 col-lg-8 col-xs-8", 'items' => ":CaraBayar" ],
									    [ 'class' => "col-md-1 col-sm-1 col-lg-1 col-xs-1" ],
									    [ 'class' => "col-md-4 col-sm-4 col-lg-4 col-xs-4", 'items' => ":LabelJam" ],
									    [ 'class' => "col-md-6 col-sm-6 col-lg-6 col-xs-6", 'items' => ":Jam" ],
								    ],
								  ],
								  [ 'class' => "form-group col-md-24",
								    'items' => [
									    [ 'class' => "col-lg-24 col-xs-24", 'items' => "<p>" ],
								    ]
								  ],
								  [ 'class' => "form-group col-md-24",
								    'items' => [
									    [ 'class' => "col-md-5 col-sm-5 col-lg-5 col-xs-5", 'items' => ":LabelCOA" ],
									    [ 'class' => "col-md-19 col-sm-19 col-lg-19 col-xs-19", 'items' => ":COA" ],
								    ],
								  ],
								  [ 'class' => "form-group col-md-24",
								    'items' => [
									    [ 'class' => "col-lg-24 col-xs-24", 'items' => "<p>" ],
								    ]
								  ],
								  [ 'class' => "form-group col-md-24",
								    'items' => [
									    [ 'class' => "col-md-5 col-sm-5 col-lg-5 col-xs-5", 'items' => ":LabelKartu" ],
									    [ 'class' => "col-md-19 col-sm-19 col-lg-19 col-xs-19", 'items' => ":Kartu" ],
								    ],
								  ],
								  [ 'class' => "form-group col-md-24",
								    'items' => [
									    [ 'class' => "col-lg-24 col-xs-24", 'items' => "<p>" ],
								    ]
								  ],
								  [ 'class' => "form-group col-md-24",
								    'items' => [
									    [ 'class' => "col-md-5 col-sm-5 col-lg-5 col-xs-5", 'items' => ":LabelKeterangan" ],
									    [ 'class' => "col-md-19 col-sm-19 col-lg-19 col-xs-19", 'items' => ":Keterangan" ],
								    ],
								  ],
								  [ 'class' => "form-group col-md-24",
								    'items' => [
									    [ 'class' => "col-lg-24 col-xs-24", 'items' => "<p>" ],
								    ]
								  ],
								  [ 'class' => "form-group col-md-24",
								    'items' => [
									    [ 'class' => "col-md-5 col-sm-5 col-lg-5 col-xs-5", 'items' => ":LabelNominal" ],
									    [ 'class' => "col-md-19 col-sm-19 col-lg-19 col-xs-19", 'items' => ":Nominal" ],
								    ],
								  ],
								  [ 'class' => "form-group col-md-24",
								    'items' => [
									    [ 'class' => "col-lg-24 col-xs-24", 'items' => "<p>" ],
								    ]
								  ],
								  [ 'class' => "form-group col-md-24",
								    'items' => [
									    [ 'class' => "col-md-5 col-sm-5 col-lg-5 col-xs-5", 'items' => ":LabelByr" ],
									    [ 'class' => "col-md-19 col-sm-19 col-lg-19 col-xs-19", 'items' => ":Byr" ],
								    ],
								  ],
								  [ 'class' => "form-group col-md-24",
								    'items' => [
									    [ 'class' => "col-lg-24 col-xs-24", 'items' => "<p>" ],
								    ]
								  ],
								  [ 'class' => "form-group col-md-24",
								    'items' => [
									    [ 'class' => "col-md-5 col-sm-5 col-lg-5 col-xs-5", 'items' => ":LabelKembali" ],
									    [ 'class' => "col-md-19 col-sm-19 col-lg-19 col-xs-19", 'items' => ":Kembali" ],
								    ],
								  ],
							  ]
							],
							[ 'class' => "row-no-gutters",
							  'items' => [
								  [ 'class' => "col-md-24",
								    'items' => [
									    [ 'class' => "pull-left", 'items' => ":btnPrint " ],
									    [ 'class' => "pull-right", 'items' => ":btnSave :btnCancel" ],
								    ]
								  ]
							  ]
							],
							[
								":LabelPembayaran" => '<label class="control-label" style="margin: 0; padding: 8px 0;font-size: 24px;text-align: justify-all">Pembayaran</label>',
								":LabelNomor"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor</label>',
								":LabelTgl"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tanggal</label>',
								":LabelJam"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jam</label>',
								":LabelCaraBayar"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Cara Bayar</label>',
								":LabelCOA"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode</label>',
								":LabelKartu"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Kartu</label>',
								":LabelKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
								":LabelNominal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
								":LabelByr"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bayar</label>',
								":LabelKembali"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kembali</label>',
								":Padding"         => '<label class="control-label" style="margin: 0; padding: -6px 0;"> </label>',
								":Nomor"           => Html::textInput( 'BMNoView', '', [ 'id' => 'BMNoView', 'class' => 'form-control', 'readonly' => 'readonly' ] ) .
								                      Html::hiddenInput( 'MyKode', '', [ 'id' => 'MyKode' ] ).
								                      Html::hiddenInput( 'MyNama', '', [ 'id' => 'MyNama' ] ).
								                      Html::hiddenInput( 'MyNoTransaksi', '', [ 'id' => 'MyNoTransaksi' ] ).
								                      Html::hiddenInput( 'MyKodeTrans', '', [ 'id' => 'MyKodeTrans' ] ),
								":CaraBayar"       => FormField::combo( 'CaraBayar', [ 'config' => [ 'id' => 'CaraBayar', 'value' => 'Tunai' ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
								":COA"             => FormField::combo( 'NoAccount', [ 'config' => [ 'id' => 'BMNoAccount','value' => $defCOA, 'data' => \aunit\models\Traccount::find()->kas() ] ] ),
								":Kartu"           => Html::textInput( 'NoKartu', '', [ 'id' => 'BMNoKartu', 'class' => 'form-control', 'readOnly' => true ] ),
								":Tgl"             => Html::textInput( 'BMTglView', date( 'd/m/Y' ), [ 'id' => 'BMTglView', 'class' => 'form-control', 'readOnly' => true ] ) .
								                      Html::hiddenInput( 'BMTgl', date( 'Y-m-d' ), [ 'id' => 'BMTgl', 'class' => 'form-control' ] ),
								":Jam"             => Html::textInput( 'BMJam', date( 'H:i:s' ), [ 'id' => 'BMJam', 'class' => 'form-control', 'readOnly' => true ] ),
								":Keterangan"      => Html::textarea( 'Memo', '', [ 'id' => 'BMMemo', 'class' => 'form-control' ] ),
								":Nominal"         => FormField::numberInput( [ 'name' => 'BMNominal', 'config' => [ 'id' => 'BMNominal', 'value' => 0, 'readonly' => true ] ] ),
								":Byr"             => FormField::numberInput( [ 'name' => 'BMBayar', 'config' => [ 'id' => 'BMBayar', 'value' => 0 ] ] ),
								":Kembali"         => FormField::numberInput( [ 'name' => 'BMKembali', 'config' => [ 'id' => 'BMKembali', 'value' => 0, 'readonly' => 'readonly' ] ] ),
								":btnSave"         => Html::button( '<i class="glyphicon glyphicon-floppy-disk"></i> Simpan', [ 'class' => 'btn btn-info btn-primary ', 'id' => 'btnSavePopup' ] ),
								":btnCancel"       => Html::Button( '<i class="fa fa-ban"></i> Batal', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancelPopup', 'style' => 'width:80px' ] ),
								":btnPrint"        => Html::button( '<i class="glyphicon glyphicon-print"></i>  Print ', [ 'class' => 'btn btn-warning ', 'id' => 'btnPrintPopup', 'style' => 'width:80px' ] ),
								":btnAdd"          => '',
								":btnEdit"         => '',
								":btnDaftar"       => '',
								":btnDelete"       => '',
							],
							[
								'jsBtnCancel' => false,
								'jsBtnAdd'    => false,
								'jsBtnEdit'   => false,
								'jsBtnDaftar' => false,
								'jsBtnPrint'  => false,
								'jsBtnJurnal' => false,
								'jsBtnDelete' => false,
							]
						);
						ActiveForm::end();
						?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
$this->registerJsVar( '__kas', \aunit\models\Traccount::find()->kas( false ) );
$this->registerJsVar( '__bank', \aunit\models\Traccount::find()->bank( false ) );
$this->registerJsVar( '__posKode', $defCOA );
$this->registerJs( <<< JS
   
   $("#BMNoView").val(BHC_BAYAR_KMNo);
        
        function inputBayar() {
            let BMNominal = parseFloat($('#BMNominal').val());      
            let BMBayar = parseFloat($('#BMBayar').val());                 
            let BMKembali = BMBayar - BMNominal;
            if (BMKembali < 0) BMKembali = 0; 
            $('#BMKembali').utilNumberControl().val(BMKembali);
        }

        $('#BMBayar-disp').on('keyup blur change', function (){
            setTimeout(inputBayar, 100);
        });
        
        $('#BMBayar-disp').inputmask({
            onKeyDown: function (){
                setTimeout(inputBayar, 100);
            }
        });
        
        $('#CaraBayar').change(function (event) {
             let CaraBayar = $('#CaraBayar').val();
			var data = __bank;
			var indexKasBank = 0;
			$('#BMNoKartu').prop('readonly', false);
				$("#BMNoView").val(BHC_BAYAR_BMNo);
			if (CaraBayar === 'Tunai' || CaraBayar === 'Kas Kredit'){
				data = __kas;
				$('#BMNoKartu').prop('readonly', true);
					$("#BMNoView").val(BHC_BAYAR_KMNo);
			}
			var element = $('#BMNoAccount');
			element.val("");
			element.find('option').remove();
			$.each(data, function(index, value) {
				element.append('<option value="' + value.id + '">' + value.text  + '</option>');
				if(value.id == __posKode){
					indexKasBank = index;
				}
			});
			element.prop("selectedIndex", indexKasBank).trigger('change');
          });
        
        $('#btnCancelPopup').click(function (event) {
            $('#modalBayar').modal('hide');
        }); 
        
        $('#modalBayar').on('shown.bs.modal', function (e) {
          $('#BMJam').val((new Date(BHC_dtNow)).toLocaleTimeString('en-GB', {hour: '2-digit', minute:'2-digit', second:'2-digit'}));
		  $('#CaraBayar').trigger('change');
        })
    
        
JS
);