<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdjasagroup */

$this->title = 'Edit Jasa Group: ' . $model->JasaGroup;
$this->params['breadcrumbs'][] = ['label' => 'Jasa Group', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="tdjasagroup-update">
    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdjasagroup/update', 'id' => $id ,'action' => 'update'] ),
            'cancel' => Url::toRoute( [ 'tdjasagroup/cancel', 'id' => $id,'action' => 'update' ] )
        ]
    ]) ?>

</div>
