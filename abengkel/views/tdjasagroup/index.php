<?php
use yii\grid\GridView;
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tdjasagroups';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdjasagroup-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Tdjasagroup', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'JasaGroup',
            'JasaGroupNama',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
