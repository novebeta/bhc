<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdjasagroup */

$this->title = 'Tambah Jasa Group';
$this->params['breadcrumbs'][] = ['label' => 'Jasa Group', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdjasagroup-create">
    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdjasagroup/update', 'id' => $id ,'action' => 'create'] ),
            'cancel' => Url::toRoute( [ 'tdjasagroup/cancel', 'id' => $id,'action' => 'create' ] )
        ]
    ]) ?>

</div>
