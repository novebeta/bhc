<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/** @var \abengkel\models\Tdjasagroup $model */
$url['cancel'] = Url::toRoute('tdjasa/index');
?>

<div class="tdjasa-form">
	<?php
	$form = ActiveForm::begin(['id' => 'frm_tdjasa_id' ,'enableClientValidation'=>false]);
	\abengkel\components\TUi::form(
		[
			'class' => "row-no-gutters",
			'items' => [
				['class' => "form-group col-md-24",
				 'items' => [
					 ['class' => "col-sm-2",'items' => [['items' => ":LabelJasaKode"],]],
					 ['class' => "col-sm-5",'items' => [['items' => ":JasaKode"],]],
				 ],
				],
				['class' => "form-group col-md-24",
				 'items' => [
					 ['class' => "col-sm-2",'items' => [['items' => ":LabelJasaNama"],]],
					 ['class' => "col-sm-14",'items' => [['items' => ":JasaNama"],]],
				 ],
				],
			]
		],
		[
			'class' => "pull-right",
			'items' => ":btnAction"
		],
		[
			":LabelJasaKode"       => '<div class="form-group"> <label class="control-label" >Jasa Grup</label></div>',
			":LabelJasaNama"       => '<div class="form-group"> <label class="control-label" >Jasa Grup Nama</label></div>',

			":JasaKode"            => $form->field($model, 'JasaGroup', ['template' => '{input}'])->textInput(['maxlength' => true,'autofocus' => 'autofocus' ]),
			":JasaNama"            => $form->field($model, 'JasaGroupNama', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
		],
		[
			'url_main'   => 'tdjasagroup',
			'url_id'     => $id,
			'url_update' => $url[ 'update' ],
			'url_index' => Url::toRoute(['tdjasa/index']),
		]
	);
	ActiveForm::end();
    ?>
</div>
<?php
$this->registerJs( <<< JS
     $('#btnSave').click(function (event) {
       $('#frm_tdjasa_id').submit();
    }); 

    
    
JS);
