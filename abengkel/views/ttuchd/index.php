<?php
use abengkel\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Update Standar Cost';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'UCNo'      => 'Uc No',
		'KodeTrans' => 'Kode Trans',
		'PosKode'   => 'Pos Kode',
		'NoGL'      => 'No Gl',
		'Cetak'     => 'Cetak',
		'UserID'    => 'User ID',
	],
	'cmbTgl'    => [
		'UCTgl'     => 'Uc Tgl',
	],
	'cmbNum'    => [
		'UCHrgLama' => 'Uc Hrg Lama',
		'UCHrgBaru' => 'Uc Hrg Baru',
		'UCSelisih' => 'Uc Selisih',
	],
	'sortname'  => "UCTgl",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Ttuchd::className(), 'Update Standar Cost',
    [
        'url_delete' => Url::toRoute( [ 'ttuchd/delete' ] ),
    ]), \yii\web\View::POS_READY );
