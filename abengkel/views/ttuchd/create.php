<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttuchd */
$params                          = '&id=' . $id . '&action=create';
$cancel = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = 'Tambah - Standard Cost';
?>
<div class="ttuchd-create">
    <?= $this->render('_form', [
        'model'     => $model,
        'id'        =>$id,
        'dsSetup'   => $dsSetup,
        'url'       => [
            'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttucit/index','action' => 'create' ] ),
        ]
    ]) ?>

</div>
