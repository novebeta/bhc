<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttuchd */
$params                          = '&id=' . $id . '&action=update';
$cancel = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = 'Edit - Standard Cost: ' . $dsSetup['UCNoView'];
?>
<div class="ttuchd-update">
    <?= $this->render('_form', [
        'model' => $model,
        'dsSetup' => $dsSetup,
        'id'=>$id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttucit/index','action' => 'update' ] ),
                /*
                'print'        => Url::toRoute( [ \Yii::$app->controller->id . '/print', 'id' => $id, 'action' => 'print' ] ),
                */
        ]
    ]) ?>

</div>
