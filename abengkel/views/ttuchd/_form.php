<?php
use abengkel\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttuchd */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttuchd-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_ttuchd_id', 'action' => $url[ 'update' ] ] );
		\abengkel\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelNumber" ],
						  [ 'class' => "col-sm-4", 'items' => ":Number" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelTgl" ],
						  [ 'class' => "col-sm-2", 'items' => ":Tgl" ],
						  [ 'class' => "col-sm-3", 'items' => ":Jam" ],
						  [ 'class' => "col-sm-5" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelTC" ],
						  [ 'class' => "col-sm-5", 'items' => ":TC" ],
					  ],
					],
					[
						'class' => "row col-md-24",
						'style' => "margin-bottom: 6px;",
						'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
					],
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelUCKeterangan" ],
						  [ 'class' => "col-sm-13", 'items' => ":UCKeterangan" ],
						  [ 'class' => "col-sm-3", 'style' => "padding-left:3px;", 'items' => ":UCHrgLama" ],
						  [ 'class' => "col-sm-3", 'style' => "padding-left:3px;", 'items' => ":UCHrgBaru" ],
						  [ 'class' => "col-sm-3", 'style' => "padding-left:3px;", 'items' => ":UCSelisih" ],
					  ],
					],
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelStatus" ],
						  [ 'class' => "col-sm-2", 'items' => ":Status" ],
						  [ 'class' => "col-sm-11" ],
						  [ 'class' => "col-sm-3", 'style' => "padding-left:3px;", 'items' => ":LabelUCHrgLama" ],
						  [ 'class' => "col-sm-3", 'style' => "padding-left:3px;", 'items' => ":LabelUCHrgBaru" ],
						  [ 'class' => "col-sm-3", 'style' => "padding-left:3px;", 'items' => ":LabelUCSelisih" ],
					  ],
					],
				]
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-16",
				    'items' => [
					    [ 'class' => "col-sm-1", 'items' => ":LabelJT" ],
					    [ 'class' => "col-sm-4", 'items' => ":JT" ],
				    ],
				  ],
				  [ 'class' => "col-md-8",
				    'items' => [
					    [ 'class' => "pull-right", 'items' => ":btnPrint :btnAction" ]
				    ]
				  ]
			  ]
			],
			[
				/* label */
				":LabelNumber"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor UC</label>',
				":LabelTgl"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl UC</label>',
				":LabelTC"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">TC</label>',
				":LabelUCKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelStatus"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status Cetak</label>',
				":LabelUCHrgLama"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">&nbsp&nbsp Jumlah Harga Lama</label>',
				":LabelUCHrgBaru"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">&nbsp&nbsp Jumlah Harga Baru</label>',
				":LabelUCSelisih"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">&nbsp&nbsp Selisih</label>',
				":LabelJT"           => \common\components\General::labelGL( $model->NoGL ),
				":Number"       => Html::textInput( 'UCNoView', $dsSetup[ 'UCNoView' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ) .
				                   Html::textInput( 'UCNo', $dsSetup[ 'UCNo' ], [ 'class' => 'hidden' ] ),
				":UCKeterangan" => Html::textarea( 'UCKeterangan', $dsSetup[ 'UCKeterangan' ], [ 'class' => 'form-control', 'maxlength' => '150' ] ),
				":Tgl"          => FormField::dateInput( [ 'name' => 'UCTgl', 'config' => [ 'value' => General::asDate( $dsSetup[ 'UCTgl' ] ) ] ] ),
				":Jam"          => FormField::timeInput( [ 'name' => 'UCJam', 'config' => [ 'value' => $dsSetup[ 'UCTgl' ] ] ] ),
				":TC"           => FormField::combo( 'KodeTrans', [ 'name'   => "KodeTrans", 'config' => [ 'value' => $dsSetup[ 'KodeTrans' ], 'data' => [
					'UC' => 'UC - Update Standard Cost', ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":UCHrgLama"    => FormField::decimalInput( [ 'name' => 'UCHrgLama', 'config' => [ 'value' => $dsSetup[ 'UCHrgLama' ], 'readonly' => 'readonly' ] ] ),
				":UCHrgBaru"    => FormField::decimalInput( [ 'name' => 'UCHrgBaru', 'config' => [ 'value' => $dsSetup[ 'UCHrgBaru' ], 'readonly' => 'readonly' ] ] ),
				":UCSelisih"    => FormField::decimalInput( [ 'name' => 'UCSelisih', 'config' => [ 'value' => $dsSetup[ 'UCSelisih' ], 'readonly' => 'readonly' ] ] ),
				":Status"       => Html::textInput( 'Cetak', $dsSetup[ 'Cetak' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":JT"           => $form->field( $model, 'NoGL', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true ] ),
			],
			[
				'url_main' => 'ttuchd',
				'url_id'   => $id,
			]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlDetail = $url[ 'detail' ];
$urlPrint  = $url[ 'print' ];
$urlSaldoQty = Url::toRoute(['tdbarang/get-saldo-qty']);
$readOnly  = ( $_GET[ 'action' ] == 'view' ? 'true' : 'false' );
$barang    = \common\components\General::cCmd(
	"SELECT BrgKode, BrgNama, BrgBarCode, BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual, BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus 
FROM tdbarang 
WHERE BrgStatus = 'A' 
UNION
SELECT BrgKode, BrgNama, BrgBarCode,  BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual, BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus FROM tdBarang 
WHERE BrgKode IN 
(SELECT ttucit.BrgKode FROM ttucit INNER JOIN tdbarang ON ttucit.BrgKode = tdbarang.BrgKode 
 WHERE ttucit.UCNo = :UCNo AND tdBarang.BrgStatus = 'N' GROUP BY ttucit.BrgKode )
ORDER BY BrgStatus, BrgKode", [ ':UCNo' => $dsSetup[ 'UCNo' ] ] )->queryAll();
$this->registerJsVar( '__Barang', json_encode( $barang ) );
$this->registerJs( <<< JS
     var __BrgKode = JSON.parse(__Barang);
     __BrgKode = $.map(__BrgKode, function (obj) {
                      obj.id = obj.BrgKode;
                      obj.text = obj.BrgKode;
                      return obj;
                    });
     var __BrgNama = JSON.parse(__Barang);
     __BrgNama = $.map(__BrgNama, function (obj) {
                      obj.id = obj.BrgNama;
                      obj.text = obj.BrgNama;
                      return obj;
                    });
      function HitungBawah(){         
         }         
         window.HitungBawah = HitungBawah;
      function HitungTotal(){ 
          let JumHargaBeliLama = $('#detailGrid').jqGrid('getCol','JumHargaBeliLama',false,'sum');
          $('#UCHrgLama').utilNumberControl().val(JumHargaBeliLama);
          let JumHargaBeliBaru = $('#detailGrid').jqGrid('getCol','JumHargaBeliBaru',false,'sum');          
          $('#UCHrgBaru').utilNumberControl().val(JumHargaBeliBaru);
          let SelisihHargaBeli = $('#detailGrid').jqGrid('getCol','SelisihHargaBeli',false,'sum');
          $('#UCSelisih').utilNumberControl().val(SelisihHargaBeli);
         	HitungBawah();
         }
         window.HitungTotal = HitungTotal;
        
     $('#detailGrid').utilJqGrid({    
     editurl: '$urlDetail',    
        height: 255,
        extraParams: {
            UCNo: '$model->UCNo'
        },
        postData: {
            UCNo: '$model->UCNo'
        },
        rownumbers: true,  
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,           
        colModel: [
            { template: 'actions'  },
            {
                name: 'BrgKode',
                label: 'Kode',
                width: 115,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __BrgKode,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'UCHrgJualLama'+'"]').inputmask('setvalue',parseFloat(data.BrgHrgJual));
                            $('[id="'+rowid+'_'+'UCHrgJualBaru'+'"]').inputmask('setvalue',parseFloat(data.BrgHrgJual));
                            $('[id="'+rowid+'_'+'UCHrgBeliLama'+'"]').inputmask('setvalue',parseFloat(data.BrgHrgBeli));
                            $('[id="'+rowid+'_'+'UCHrgBeliBaru'+'"]').inputmask('setvalue',parseFloat(data.BrgHrgBeli));
                            $('[id="'+rowid+'_'+'BrgGroup'+'"]').val(data.BrgGroup);
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').val(data.BrgNama); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').trigger('change');
                            window.hitungLine();
						}
                    }                 
                }
            },
            {
                name: 'BrgNama',
                label: 'Nama Barang',
                width: 150,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __BrgNama,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    console.log(data);
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'UCHrgJualLama'+'"]').inputmask('setvalue',parseFloat(data.BrgHrgJual));
                            $('[id="'+rowid+'_'+'UCHrgJualBaru'+'"]').inputmask('setvalue',parseFloat(data.BrgHrgJual));
                            $('[id="'+rowid+'_'+'UCHrgBeliLama'+'"]').inputmask('setvalue',parseFloat(data.BrgHrgBeli));
                            $('[id="'+rowid+'_'+'UCHrgBeliBaru'+'"]').inputmask('setvalue',parseFloat(data.BrgHrgBeli));
                            $('[id="'+rowid+'_'+'BrgGroup'+'"]').val(data.BrgGroup);
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').val(data.BrgNama); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').trigger('change');
                            window.hitungLine();
						}
                    }              
                }
            },
            {
                name: 'UCQty',
                label: 'Qty',
                editable: true,
                width: 75,
                template: 'number',
                editoptions:{
                    readOnly:true,
                }, 
            },     
            {
                name: 'BrgSatuan',
                label: 'Satuan',
                editable: true,
                width: 100,
            },      
            {
                name: 'UCHrgBeliLama',
                label: 'Hrg Beli Lama',
                editable: true,
                width: 100,
                template: 'money',
                editoptions:{
                    readOnly:true,
                    maxlength: 48
                }, 
            },    
            {
                name: 'UCHrgBeliBaru',
                label: 'Hrg Beli Baru',
                editable: true,
                width: 100,
                template: 'money'
            }, 
            {
                name: 'JumHargaBeliLama',
                label: 'Jml Hrg Lama',
                editable: true,
                width: 100,
                template: 'money',
                editoptions:{
                    readOnly:true,
                }, 
            },     
            {
                name: 'JumHargaBeliBaru',
                label: 'Jml Hrg Baru',
                width: 100,
                editable: true,
                template: 'money',
                editoptions:{
                    readOnly:true,
                }, 
            },
            {
                name: 'SelisihHargaBeli',
                label: 'Selisih Hrg Beli',
                width: 100,
                editable: true,
                template: 'money',
                editoptions:{
                    readOnly:true,
                }, 
            },
        ],
        listeners: {
            afterLoad: function (data) {
             window.HitungTotal();
            }    
        },
       
        onSelectRow : function(rowid,status,e){ 
            if (window.rowId !== -1) return;
            if (window.rowId_selrow === rowid) return;
            if(rowid.includes('new_row') === true){
                $('[name=KodeBarang]').val('--');
                $('[name=BarangNama]').val('--');
                return ;
            };
            let r = $('#detailGrid').getRowData(rowid);
            $('#KodeBarang').val(unescape(r.BrgKode));
            $('#BarangNama').val($("<textarea/>").html(r.BrgNama).text());
            // console.log(r);
            window.rowId_selrow = rowid;
        }    
     }).init().fit($('.box-body')).load({url: '$urlDetail'});
       window.rowId_selrow = -1;
     $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
             window.cmp.rowid = rowid;    
              // if(rowid === "new_row"){
              //   $('[id="'+rowid+'_'+'UCQty'+'"]').val(0);
              // }
             function hitungLine(){
                 let BrgKode = $('[id="'+window.cmp.rowid+'_'+'BrgKode'+'"]').val();
                 $.get( "{$urlSaldoQty}&BrgKode="+BrgKode, function( data ) {
                     $('[id="'+window.cmp.rowid+'_'+'UCQty'+'"]').inputmask('setvalue',parseFloat(data));
                    let UCHrgBeliLama       = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'UCHrgBeliLama'+'"]').val()));
                    let UCHrgBeliBaru       = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'UCHrgBeliBaru'+'"]').val()));
                    let SelisihHargaBeli    = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'SelisihHargaBeli'+'"]').val()));
                    let UCQty               = parseFloat(data);
                    $('[id="'+window.cmp.rowid+'_'+'JumHargaBeliLama'+'"]').utilNumberControl().val(UCHrgBeliLama * UCQty);
                    $('[id="'+window.cmp.rowid+'_'+'JumHargaBeliBaru'+'"]').utilNumberControl().val(UCHrgBeliBaru * UCQty);
                    $('[id="'+window.cmp.rowid+'_'+'SelisihHargaBeli'+'"]').utilNumberControl().val(UCHrgBeliLama - UCHrgBeliBaru);
                    window.HitungTotal();
                });                
             }
             window.hitungLine = hitungLine;
              $('[id="'+window.cmp.rowid+'_'+'UCQty'+'"]').on('change', function (event){
                  hitungLine();
              });
              $('[id="'+window.cmp.rowid+'_'+'UCHrgBeliBaru'+'"]').on('change', function (event){
                  hitungLine();
              });
               $('[id="'+window.cmp.rowid+'_'+'UCHrgBeliLama'+'"]').on('change', function (event){
                  hitungLine();
              });
             hitungLine();
         });
       
      $('#btnCancel').click(function (event) {	      
       $('#form_ttuchd_id').attr('action','{$url['cancel']}');
       $('#form_ttuchd_id').submit();
    });
	
	$('#btnSave').click(function (event) {	      
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttuchd_id').attr('action','{$url['update']}');
        $('#form_ttuchd_id').submit();
    });  

      $('#btnPrint').click(function (event) {	      
        $('#form_ttuchd_id').attr('action','$urlPrint');
        $('#form_ttuchd_id').attr('target','_blank');
        $('#form_ttuchd_id').submit();
	  }); 
JS
);


