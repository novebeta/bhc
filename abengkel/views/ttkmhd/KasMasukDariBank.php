<?php
use abengkel\assets\AppAsset;
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Kas Masuk Dari Bank';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
    'cmbTxt'    => [
        'KMNo'          => 'No KM',
        'KMJenis'       => 'Jenis',
        'KodeTrans'     => 'Kode Trans',
        'PosKode'       => 'Pos Kode',
        'MotorNoPolisi' => 'No Polisi',
        'KMPerson'      => 'Km Person',
        'KMMemo'        => 'Km Memo',
        'NoGL'          => 'No Gl',
        'Cetak'         => 'Cetak',
        'UserID'        => 'User ID',
        'KasKode'       => 'Kas Kode',
    ],
    'cmbTgl'    => [
        'KMTgl'         => 'Tanggal KM',
    ],
    'cmbNum'    => [
        'KMNominal'     => 'Nominal',
        'KMBayarTunai'  => 'Bayar Tunai',
    ],
    'sortname'  => "KMTgl",
    'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Ttkmhd::className(), 'Kas Masuk Dari Bank' , [
    'url_add'    => \yii\helpers\Url::toRoute( [ 'ttkmhd/kas-masuk-dari-bank-create','action'=>'create'] ),
    'url_update' => \yii\helpers\Url::toRoute( [ 'ttkmhd/kas-masuk-dari-bank-update','action'=>'update'] ),
    'url_print' => \yii\helpers\Url::toRoute( [ 'ttkmhd/kas-masuk-dari-bank-print','action'=>'print'] ),
    'url_delete' => \yii\helpers\Url::toRoute( [ 'ttkmhd/delete' ]),
] ), \yii\web\View::POS_READY );
