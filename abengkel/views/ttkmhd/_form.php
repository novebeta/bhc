<?php
use abengkel\assets\AppAsset;
use abengkel\components\FormField;
use abengkel\components\TUi;
use abengkel\models\Traccount;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttkmhd */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
<div class="ttkmhd-form">
		<?php
		$form = ActiveForm::begin( ['id' => 'form_ttkmhd_id','action' => $url[ 'update' ] ] );
		TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-17",
					      'items' => [
						      [ 'class' => "col-sm-2", 'items' => ":LabelKMNo" ],
						      [ 'class' => "col-sm-4", 'items' => ":KMNo" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelKMTgl" ],
						      [ 'class' => "col-sm-3", 'items' => ":KMTgl" ],
						      [ 'class' => "col-sm-3", 'items' => ":KMJam", 'style' => 'padding-left:1px;' ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelJenis" ],
						      [ 'class' => "col-sm-3", 'items' => ":Jenis" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-7",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelTrans" ],
						      [ 'class' => "col-sm-21", 'items' => ":Trans" ],
					      ],
					    ],
				    ],
				  ],
				  [
					  'class' => "row col-md-24",
					  'style' => "margin-bottom: 6px;",
					  'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-17",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelKode" ],
						      [ 'class' => "col-sm-10", 'items' => ":Kode" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-3", 'items' => ":LabelTotal" ],
						      [ 'class' => "col-sm-6", 'items' => ":Total" ]
					      ],
					    ],
					    [ 'class' => "form-group col-md-7",
					      'items' => [
						      [ 'class' => "col-sm-6", 'items' => ":LabelNominal" ],
						      [ 'class' => "col-sm-18", 'items' => ":Nominal" ],
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-17",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelNoTrans" ],
						      [ 'class' => "col-sm-8", 'items' => ":NoTrans" ],
						      [ 'class' => "col-sm-2", 'items' => ":BtnInfo" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-3", 'items' => ":LabelNoPol" ],
						      [ 'class' => "col-sm-6", 'items' => ":NoPol" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-7",
					      'items' => [
						      [ 'class' => "col-sm-6", 'items' => ":LabelTerima" ],
						      [ 'class' => "col-sm-18", 'items' => ":Terima" ],
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelKeterangan" ],
					    [ 'class' => "col-sm-22", 'items' => ":Keterangan", 'style' => 'padding-left:5px;' ]
				    ],
				  ],
			  ],
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
                  ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                      'items'  => [
                          ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":JT"],
                          ['class' => "col-sm-4 pull-right", 'items' => ":LabelJT"],
                      ],
                  ],
                  [
                      'class' => "col-md-12 pull-left",
                      'items' => $this->render( '../_nav', [
                          'url'=> $_GET['r'],
                          'options'=> [
                              'KMNo'          => 'No KM',
                              'KMJenis'       => 'Jenis',
                              'KodeTrans'     => 'Kode Trans',
                              'PosKode'       => 'Pos Kode',
                              'MotorNoPolisi' => 'No Polisi',
                              'KMPerson'      => 'Km Person',
                              'KMMemo'        => 'Km Memo',
                              'NoGL'          => 'No Gl',
                              'Cetak'         => 'Cetak',
                              'UserID'        => 'User ID',
                              'KasKode'       => 'Kas Kode',
                          ],
                      ])
                  ],
                  ['class' => "col-md-12 pull-right",
				    'items' => [
					    [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
				    ]
				  ]
			  ]
			],
			[
				":LabelKMNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No KM</label>',
				":LabelKMTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl KM</label>',
				":LabelKode"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode</label>',
				":LabelKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelTrans"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Trans</label>',
				":LabelJenis"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				":LabelNominal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
				":LabelJT"         => \common\components\General::labelGL( $dsTUang[ 'NoGL' ],'JT' ),
				":LabelTerima"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Diterima dari</label>',
				":LabelNoTrans"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Trans</label>',
				":LabelNoPol"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Polisi</label>',
				":LabelTotal"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total</label>',
				":KMNo"            => Html::textInput( 'KMNoView', $dsTUang[ 'KMNoView' ], [ 'class' => 'form-control','readonly' => 'readonly','tabindex' => '1' ] ) .
                                      Html::textInput( 'KMNo', $dsTUang[ 'KMNo' ], [ 'class' => 'hidden' ] ),
                ":KMTgl"           => FormField::dateInput( [ 'name' => 'KMTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'KMTgl' ] ) ] ] ),
                ":KMJam"           => FormField::timeInput( [ 'name' => 'KMJam', 'config' => [ 'value' => $dsTUang[ 'KMTgl' ] ] ] ),
				":JT"              => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control','readOnly'=>true ] ),
				":Terima"          => Html::textInput( 'KMPerson', $dsTUang[ 'KMPerson' ], [ 'class' => 'form-control', 'maxlength' => '50' ] ),
				":Keterangan"      => Html::textInput( 'KMMemo', $dsTUang[ 'KMMemo' ], [ 'class' => 'form-control', 'maxlength' => '150' ] ),
				":NoPol"           => Html::textInput( 'MotorNoPolisi', $dsTUang[ 'MotorNoPolisi' ], [ 'class' => 'form-control', 'maxlength' => '12' ] ),
				":Nominal"         => FormField::numberInput( [ 'name' => 'KMNominal', 'config' => [ 'value' => $dsTUang[ 'KMNominal' ] ] ] ),
				":Kode"            => FormField::combo( 'NoAccount', [ 'name' => 'KasKode', 'config' => [  'value' => $dsTUang[ 'KasKode'], 'data'=> \abengkel\models\Traccount::find()->kas()  ]] ),
				":Trans"           => FormField::combo( 'KodeTrans', [ 'config' => [ 'value' => $dsTUang[ 'KodeTrans' ],
                                        'data' => [
                                        'UM' => 'UM - Umum',
                                        'BK' => 'BK - KM Dari Bank',
                                        'SV' => 'SV - Servis',
                                        'SE' => 'SE - Estimati Biaya Servis',
                                        'SI' => 'SI - Penjualan',
                                        'SO' => 'SO - Order Penjualan',
                                        'PR' => 'PR - Retur Pembelian',
                                        'PL' => 'PL - Pengerjaan Luar' ]
                                        ], 'extraOptions'=> [ 'simpleCombo' => true ] ] ),
				":Jenis"           => FormField::combo( 'KMJenis', [ 'config' => [ 'value' => $dsTUang[ 'KMJenis' ], 'data' => [
                                        'Tunai'        => 'Tunai',
                                        'Kartu Debit'  => 'Kartu Debit',
                                        'Kartu Kredit' => 'Kartu Kredit',
                                        'Transfer'     => 'Transfer', ] ],'options' => ['tabindex'=>'3'], 'extraOptions'         => [ 'simpleCombo' => true ] ] ),
				":NoTrans"         => Html::textInput( 'KMLink', $dsTUang[ 'KMLink' ], [ 'class' => 'form-control', 'readonly' => true  ] ),
				":Total"           => FormField::numberInput( [ 'name' => 'Total', 'config' => [ 'value' => $dsTUang[ 'Total' ] ] ] ),
				":BtnInfo"         => '<button type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></i></button>',
			],
            [
                'url_main' => 'ttkmhd',
                'url_id' => $id,
                'url_delete'        => Url::toRoute(['ttkmhd/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php

$urlAdd         = \abengkel\models\Ttkmhd::getRoute($dsTUang[ 'KodeTrans' ],['action'=>'create'] )[ 'create' ];
$urlIndex       = \abengkel\models\Ttkmhd::getRoute($KodeTrans,[ 'id' => $id ] )[ 'index' ];
$urlDetail = $url[ 'detail' ];
$readOnly  = ( $_GET['action'] == 'view' ? 'true' : 'false' );
$urlLoop   = Url::toRoute(['ttkmit/index']);
$urlPopup  = Url::toRoute( [ 'ttkmhd/order', 'KodeTrans' => $dsTUang[ 'KodeTrans' ]] );
$KMLink = '';
switch ($dsTUang[ 'KodeTrans' ]){
    case 'SV':
        $title = 'Daftar Servis Invoice untuk Kas Masuk Nomor : ';
        break;
    case 'SI':
        $title = 'Daftar Sales Invoice untuk Kas Masuk Nomor : ';
        break;
    case 'SE':
        $title = 'Daftar Service Estimation untuk Kas Masuk Nomor : ';
        break;
    case 'SO':
        $title = 'Daftar Sales Order untuk Kas Masuk Nomor : ';
        break;
    case 'PR':
        $title = 'Daftar Purchase Retur untuk Kas Masuk Nomor : ';
        break;
}
$this->registerJs( <<< JS
     $('#detailGrid').utilJqGrid({
        height: 225,
        editurl: '$urlDetail',
        navButtonTambah: false,
        extraParams: {
            KMNo: $('input[name="KMNo"]').val()
        },
        rownumbers: true,  
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,     
        colModel: [
            { template: 'actions'  },
            {
                name: 'KMLink',
                label: 'No',
                width: 120,
                editable: true,
                editor: {
                    type: 'text',
                    readOnly:true
                }
            },
            {
                name: 'TglLink',
                label: 'Tanggal',
                width: 130,
                editable: false,
            },
            {
                name: 'KMNo',
                label: 'Kode',
                editable: false,
                width: 140,
            },     
            {
                name: 'BiayaNama',
                label: 'Nama',
                editable: false,
                width: 140,
            },      
            {
                name: 'Total',
                label: 'Total',
                editable: true,
                width: 100,
                template: 'money',
                editor: {
                    readOnly:true
                }
            },   
            {
                name: 'Terbayar',
                label: 'Terbayar',
                editable: false,
                width: 100,
                template: 'money',
                editor: {
                    readOnly:true
                }
            },     
            {
                name: 'KMBayar',
                label: 'Bayar',
                width: 100,
                editable: true,
                template: 'money',
                editrules:{
                    custom:true, 
                    custom_func: function(value) {
                        var KMBayar = parseFloat(window.util.number.getValueOfEuropeanNumber(value));
                        var Total = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Total'+'"]').val()));
                        var Terbayar = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Terbayar'+'"]').val()));
                        window.hitungLine();
                        if (KMBayar === 0) 
                            return [false,"Silahkan mengisi bayar."];  
                        else {   
                            var sisa = Total - Terbayar;
                            if (KMBayar > sisa){
                                $('[id="'+window.cmp.rowid+'_'+'KMBayar'+'"]').val(sisa);
                               return [false,"Nilai Bayar maksimal : " + window.util.number.format(sisa)];   
                            }else{
                                return [true,""];  
                            }
                        }
                        
                    }
                }
            },
            {
                name: 'Sisa',
                label: 'Sisa',
                width: 100,
                editable: true,
                template: 'money',
                editor: {
                    readOnly:true
                }
            },
        ],  
        listeners: {
             afterLoad: function (data) {
                let Total = $('#detailGrid').jqGrid('getCol','KMBayar',false,'sum');
               $('#Total-disp').utilNumberControl().val(Total);
            }
        },
        onSelectRow : function(rowid,status,e){
			window.rowId_selrow = rowid;
        }
     }).init().fit($('.box-body')).load({url: '$urlDetail'}); 

        function hitungLine(){
            let KMBayar = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'KMBayar'+'"]').val()));
            let Total = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Total'+'"]').val()));
            let Sisa = Total - KMBayar;
            $('[id="'+window.cmp.rowid+'_'+'Sisa'+'"]').utilNumberControl().val(Sisa);
        }          
        window.hitungLine = hitungLine;

    $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
        window.cmp.rowid = rowid;   
        $('[id="'+window.cmp.rowid+'_'+'KMBayar'+'"]').on('keyup', function (event){
              hitungLine();
          });
    });
    
    $('#detailGrid').jqGrid('navButtonAdd',"#detailGrid-pager",{ 
        caption: ' Tambah',
        buttonicon: 'glyphicon glyphicon-plus',
        onClickButton:  function() {
            window.util.app.dialogListData.closeTriggerFromIframe = function(){ 
                var iframe = window.util.app.dialogListData.iframe();
                var mainGrid = iframe.contentWindow.jQuery('#jqGrid').jqGrid();
                window.util.app.dialogListData.gridFn.mainGrid = mainGrid;
                window.util.app.dialogListData.gridFn.var.multiSelect = true;
                var selRowId = window.util.app.dialogListData.gridFn.getSelectedRow();
                console.log(selRowId);
                $.ajax({
                     type: "POST",
                     url: '$urlLoop',
                     data: {
                         oper: 'batch',
                         data: selRowId,
                         header: {
                             'KMNo': $('[name="KMNo"]').val()
                         }
                     },
                     success: function(data) {
                        mainGrid.jqGrid().trigger('reloadGrid');
                     },
                   });
                return false;
            }
            window.util.app.dialogListData.show({
                title: '$title {$dsTUang[ "KMNoView" ]}',
                url: '$urlPopup'
            },function(){                 
                window.location.href = "{$url['update']}&oper=skip-load&KMNoView={$dsTUang['KMNoView']}"; 
            });
        }, 
        position: "last", 
        title:"", 
        cursor: "pointer"
    });
    
	$('#btnSave').click(function (event) {	    
	     let Nominal = parseFloat($('#KMNominal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Nominal tidak boleh 0', size: 'small'});
               return;
           }
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttkmhd_id').attr('action','{$url['update']}');
        $('#form_ttkmhd_id').submit();
    });
	
	$('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  });
	
	$('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
	
	$('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	});
	
	$('#btnCancel').click(function (event) {	      
       $('#form_ttkmhd_id').attr('action','{$url['cancel']}');
       $('#form_ttkmhd_id').submit();
    });
	
	$('[name=KMTgl]').utilDateControl().cmp().attr('tabindex', '2');
JS
);


