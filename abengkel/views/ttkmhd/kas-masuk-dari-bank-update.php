<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttkmhd */
$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = 'Edit - Kas Masuk Dari Bank : ' . $dsTUang['KMNoView'];
?>
<div class="ttkmhd-update">
    <?= $this->render('_kas-masuk-dari-bank-form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'id'    => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/kas-masuk-dari-bank-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
        ]
    ]) ?>

</div>
