<?php
use abengkel\assets\AppAsset;
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Kas Masuk Service Invoice';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
    'cmbTxt'    => [
        'KMNo'          => 'No KM',
        'KMJenis'       => 'Jenis',
        'KodeTrans'     => 'Kode Trans',
        'PosKode'       => 'Pos Kode',
        'MotorNoPolisi' => 'No Polisi',
        'KMPerson'      => 'Km Person',
        'KMMemo'        => 'Km Memo',
        'NoGL'          => 'No Gl',
        'Cetak'         => 'Cetak',
        'UserID'        => 'User ID',
        'KasKode'       => 'Kas Kode',
    ],
    'cmbTgl'    => [
        'KMTgl'         => 'Tanggal KM',
    ],
    'cmbNum'    => [
        'KMNominal'     => 'Nominal',
        'KMBayarTunai'  => 'Bayar Tunai',
    ],
    'sortname'  => "KMTgl",
    'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Ttkmhd::className(), 'KM SV - Servis' ,
    [
        'url_update' => Url::toRoute( [ 'ttkmhd/kas-masuk-service-invoice-update' , 'action' => 'update'] ),
        'url_add'    => Url::toRoute( [ 'ttkmhd/kas-masuk-service-invoice-create', 'action' => 'create' ] ),
        'url_delete' => Url::toRoute( [ 'ttkmhd/delete' ] ),
    ]
), \yii\web\View::POS_READY );
