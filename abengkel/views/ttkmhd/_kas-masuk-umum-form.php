<?php
use abengkel\components\FormField;
use common\components\General;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
\abengkel\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttkmhd */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttkmhd-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_ttkmhd_id', 'action' => $url[ 'update' ] ] );
		\abengkel\components\TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-17",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelKMNo" ],
						      [ 'class' => "col-sm-4", 'items' => ":KMNo" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelKMTgl" ],
						      [ 'class' => "col-sm-3", 'items' => ":KMTgl" ],
						      [ 'class' => "col-sm-4", 'items' => ":KMJam", 'style' => 'padding-left:1px;' ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelJenis" ],
						      [ 'class' => "col-sm-3", 'items' => ":Jenis" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-7",
					      'items' => [
						      [ 'class' => "col-sm-6", 'items' => ":LabelTrans" ],
						      [ 'class' => "col-sm-18", 'items' => ":Trans" ],
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-17",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelKode" ],
						      [ 'class' => "col-sm-20", 'items' => ":Kode" ]
					      ],
					    ],
					    [ 'class' => "form-group col-md-7",
					      'items' => [
						      [ 'class' => "col-sm-6", 'items' => ":LabelNominal" ],
						      [ 'class' => "col-sm-18", 'items' => ":Nominal" ],
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-17",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelNoTrans" ],
						      [ 'class' => "col-sm-8", 'items' => ":NoTrans" ],
						      [ 'class' => "col-sm-2", 'items' => ":BtnInfo" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-3", 'items' => ":LabelNoPol" ],
						      [ 'class' => "col-sm-6", 'items' => ":NoPol" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-7",
					      'items' => [
						      [ 'class' => "col-sm-6", 'items' => ":LabelTerima" ],
						      [ 'class' => "col-sm-18", 'items' => ":Terima" ],
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelKeterangan" ],
					    [ 'class' => "col-sm-22", 'items' => ":Keterangan", 'style' => 'padding-left:5px;' ]
				    ],
				  ],
				  [
					  'class' => "row col-md-24",
					  'style' => "margin-bottom: 6px;",
					  'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
				  ],
			  ],
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
                  ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                      'items'  => [
                          ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":JT"],
                          ['class' => "col-sm-4 pull-right", 'items' => ":LabelJT"],
                      ],
                  ],
                  [
                      'class' => "col-md-12 pull-left",
                      'items' => $this->render( '../_nav', [
                          'url'=> $_GET['r'],
                          'options'=> [
                              'KMNo'          => 'No KM',
                              'KMJenis'       => 'Jenis',
                              'KodeTrans'     => 'Kode Trans',
                              'PosKode'       => 'Pos Kode',
                              'MotorNoPolisi' => 'No Polisi',
                              'KMPerson'      => 'Km Person',
                              'KMMemo'        => 'Km Memo',
                              'NoGL'          => 'No Gl',
                              'Cetak'         => 'Cetak',
                              'UserID'        => 'User ID',
                              'KasKode'       => 'Kas Kode',
                          ],
                      ])
                  ],
                  ['class' => "col-md-12 pull-right",
				    'items' => [
					    [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
				    ]
				  ]
			  ]
			],
			[
				":LabelKMNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No KM</label>',
				":LabelKMTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl KM</label>',
				":LabelKode"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode</label>',
				":LabelKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelTrans"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Trans</label>',
				":LabelJenis"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				":LabelNominal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
				":LabelJT"         => \common\components\General::labelGL( $dsTUang[ 'NoGL' ] ,'JT'),
				":LabelTerima"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Diterima dari</label>',
				":LabelNoTrans"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Trans</label>',
				":LabelNoPol"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Polisi</label>',
				":KMNo"            => Html::textInput( 'KMNoView', $dsTUang[ 'KMNoView' ], [ 'class' => 'form-control', 'readonly' => 'readonly', 'tabindex' => '1' ] ) .
				                      Html::textInput( 'KMNo', $dsTUang[ 'KMNo' ], [ 'class' => 'hidden' ] ),
				":KMTgl"           => FormField::dateInput( [ 'name' => 'KMTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'KMTgl' ] ) ] ] ),
				":KMJam"           => FormField::timeInput( [ 'name' => 'KMJam', 'config' => [ 'value' => $dsTUang[ 'KMTgl' ] ] ] ),
				":JT"              => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control', 'readOnly' => true ] ),
				":Terima"          => Html::textInput( 'KMPerson', $dsTUang[ 'KMPerson' ], [ 'class' => 'form-control', 'maxlength' => '50' ] ),
				":Keterangan"      => Html::textInput( 'KMMemo', $dsTUang[ 'KMMemo' ], [ 'class' => 'form-control', 'maxlength' => '150' ] ),
				":NoPol"           => Html::textInput( 'MotorNoPolisi', $dsTUang[ 'MotorNoPolisi' ], [ 'class' => 'form-control', 'maxlength' => '12' ] ),
				":Nominal"         => FormField::numberInput( [ 'name' => 'KMNominal', 'config' => [ 'value' => $dsTUang[ 'KMNominal' ] ] ] ),
				":Kode"            => FormField::combo( 'NoAccount', ['name'=> 'KasKode', 'config' => [ 'value' => $dsTUang[ 'KasKode' ], 'data' => \abengkel\models\Traccount::find()->kas() ] ] ),
				":Trans"           => FormField::combo( '', ['name'=> 'KodeTrans',  'config' => [ 'value' => $dsTUang[ 'KodeTrans' ], 'data' => [
					'UM' => 'UM - Umum',
				] ], 'extraOptions'                                             => [ 'simpleCombo' => true ] ] ),
				":Jenis"           => FormField::combo( 'KMJenis', [ 'config' => [ 'value' => $dsTUang[ 'KMJenis' ], 'data' => [
					'Tunai'        => 'Tunai',
					'Kartu Debit'  => 'Kartu Debit',
					'Kartu Kredit' => 'Kartu Kredit',
					'Transfer'     => 'Transfer', ] ], 'options' => ['tabindex'=>'3'], 'extraOptions'         => [ 'simpleCombo' => true ] ] ),
				":NoTrans"         => Html::textInput( 'KMLink', $dsTUang[ 'KMLink' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":BtnInfo"         => '<button type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>',
			],
			[
				'url_main' => 'ttkmhd',
				'url_id'   => $id,
                'url_delete'        => Url::toRoute(['ttkmhd/delete', 'id' => $_GET['id'] ?? ''] ),
			]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlAdd    = \abengkel\models\Ttkmhd::getRoute( 'UM', [ 'action' => 'create' ] )[ 'create' ];
$urlIndex  = \abengkel\models\Ttkmhd::getRoute( 'UM', [ 'id' => $id ] )[ 'index' ];
$urlDetail = $url[ 'detail' ];
$readOnly  = ( $_GET[ 'action' ] == 'view' ? 'true' : 'false' );
$DtAccount = \common\components\General::cCmd( "SELECT NoAccount, NamaAccount, OpeningBalance, JenisAccount, StatusAccount, NoParent FROM traccount 
WHERE JenisAccount = 'Detail' ORDER BY NoAccount" )->queryAll();
$this->registerJsVar( '__dtAccount', json_encode( $DtAccount ) );
$this->registerJs( <<< JS
     var __NoAccount = JSON.parse(__dtAccount);
     __NoAccount = $.map(__NoAccount, function (obj) {
                      obj.id = obj.NoAccount;
                      obj.text = obj.NoAccount;
                      return obj;
                    });
     var __NamaAccount = JSON.parse(__dtAccount);
     __NamaAccount = $.map(__NamaAccount, function (obj) {
                      obj.id = obj.NamaAccount;
                      obj.text = obj.NamaAccount;
                      return obj;
                    });
     $('#detailGrid').utilJqGrid({
        height: 210,
        editurl: '$urlDetail',        
        extraParams: {
            KMNo: $('input[name="KMNo"]').val()
        },
        rownumbers: true,  
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,         
        colModel: [
            { template: 'actions'  },
            {
                name: 'NoAccount',
                label: 'Kode',
                editable: true,
                width: 140,
                editor: {
                    type: 'select2',
                    data: __NoAccount,  
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowid = $(this).attr('rowid');
                            $('[id="'+rowid+'_'+'NamaAccount'+'"]').val(data.NamaAccount); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'NamaAccount'+'"]').trigger('change');
						}
                    }                  
                }
            },
            {
                name: 'NamaAccount',
                label: 'Nama',
                width: 250,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __NamaAccount, 
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    
						    let rowid = $(this).attr('rowid');
                            $('[id="'+rowid+'_'+'NoAccount'+'"]').val(data.NoAccount); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'NoAccount'+'"]').trigger('change');
						}
                    }                              
                }
            },
            {
                name: 'KMKeterangan',
                label: 'Keterangan',
                width: 330,
                editable: true,
            },
            {
                name: 'KMBayar',
                label: 'Kredit',
                width: 210,
                editable: true,
                template: 'money'
            },
            {
                name: 'KMAutoN',
                label: 'KMAutoN',
                width: 210,
                editable: true,
                hidden: true
            },
        ],
        listeners: {
            afterLoad: function(){
                let TotalKMBayar = $('#detailGrid').jqGrid('getCol','KMBayar',false,'sum');
                $("#detailGrid-pager_right").html('<label class="control-label" style="margin-right:10px;">Sub Total COA </label>' +
                 '<label style="width:190px;margin-right:25px;">'+TotalKMBayar.toLocaleString("id-ID", {minimumFractionDigits:2})+'</label>');
            }
        }, 
     }).init().fit($('.box-body')).load({url: '$urlDetail'});
     
     
     $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowId, orgClickEvent) {
        
        if (orgClickEvent.extraparam.oper === 'add'){
            var KMKeterangan = $('[id="'+rowId+'_'+'KMKeterangan'+'"]');
            KMKeterangan.val($('[name="KMMemo"]').val());
        }        
    });
	
	$('#btnSave').click(function (event) {	      
	    let Nominal = parseFloat($('#KMNominal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Nominal tidak boleh 0', size: 'small'});
               return;
           }  
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttkmhd_id').attr('action','{$url['update']}');
        $('#form_ttkmhd_id').submit();
    });  
	
	$('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  });
	
	$('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
	
	$('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
	
	$('[name=KMTgl]').utilDateControl().cmp().attr('tabindex', '2');
JS
);



