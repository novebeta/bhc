<?php
use abengkel\assets\AppAsset;
use abengkel\models\Ttkmhd;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Kas Masuk Dari Bank';
$this->params['breadcrumbs'][] = $this->title;
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Ttkmhd::className(), '' ,
    $options), \yii\web\View::POS_READY );
