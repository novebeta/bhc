<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttkmhd */

$params                          = '&id=' . $id . '&action=create&jenis=kas';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = "Tambah - $title";

?>
<div class="ttkmhd-create">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'KodeTrans' => $KodeTrans,
        'view' => $view,
        'id'    => $id,
        'title' => $title,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/' .$view.'-update'. $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttkmit/index','action' => 'update' ] ),
        ]
    ]) ?>
</div>
