<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttkmhd */

$params                          = '&id=' . $id . '&action=update&jenis=kas';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = "Edit - $title : " . $dsTUang['KMNoView'];
?>
<div class="ttkmhd-update">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'KodeTrans' => $KodeTrans,
        'id'     => $id,
        'view' => $view,
        'title' => $title,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/' .$view.'-update'. $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail'       => Url::toRoute( [ 'ttkmit/index','action' => 'update' ] ),
        ]
    ]) ?>
</div>
