<?php
use abengkel\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Update Harga Jasa';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'UJNo'			=> 'No UJ',
		'UJKeterangan'  => 'Keterangan',
		'PosKode'       => 'Pos Kode',
		'KodeTrans'     => 'Kode Trans [TC]',
		'Cetak'         => 'Cetak',
		'UserID'        => 'User ID',
	],
	'cmbTgl'    => [
		'UJTgl'			=> 'Tanggal UJ',
	],
	'cmbNum'    => [
	],
	'sortname'  => "UJTgl",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Ttujhd::className(), 'Update Harga Jasa',
    [
        'url_delete' => Url::toRoute( [ 'ttujhd/delete' ] ),
    ]), \yii\web\View::POS_READY );
