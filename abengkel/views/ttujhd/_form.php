<?php
use abengkel\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttujhd */
/* @var $form yii\widgets\ActiveForm */

$format          = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape          = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttujhd-form">
		<?php
        $form = ActiveForm::begin( [ 'id' => 'form_ttujhd_id','action' => $url[ 'update' ] ] );
		\abengkel\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelNumber" ],
						  [ 'class' => "col-sm-3", 'items' => ":Number" ],
						  [ 'class' => "col-sm-1"],
						  [ 'class' => "col-sm-1",'items' => ":LabelTgl" ],
						  [ 'class' => "col-sm-2", 'items' => ":Tgl" ],
						  [ 'class' => "col-sm-2", 'items' => ":Jam" ],
						  [ 'class' => "col-sm-7" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelTC" ],
						  [ 'class' => "col-sm-5", 'items' => ":TC" ],
					  ],
					],
					[
						'class' => "row col-md-24",
						'style' => "margin-bottom: 6px;",
						'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
					],
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelUJKeterangan" ],
						  [ 'class' => "col-sm-13", 'items' => ":UJKeterangan" ],
						  [ 'class' => "col-sm-1"],
						  [ 'class' => "col-sm-2", 'items' => ":LabelStatus" ],
						  [ 'class' => "col-sm-2", 'items' => ":Status" ],
					  ],
					],
				]
			],
			[ 'class' => "pull-right", 'items' => ":btnPrint :btnAction" ],
			[
				/* label */
				":LabelNumber"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor UJ</label>',
				":LabelTgl"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl UJ</label>',
				":LabelTC"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">TC</label>',
				":LabelUJKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelStatus"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status Cetak</label>',
				/* component */
                ":Number"           => Html::textInput( 'UJNoView', $dsSetup[ 'UJNoView' ], [ 'class' => 'form-control','readonly' => 'readonly' ] ) .
                                        Html::textInput( 'UJNo', $dsSetup[ 'UJNo' ], [ 'class' => 'hidden' ] ) ,
                ":UJKeterangan"     => Html::textarea( 'UJKeterangan', $dsSetup[ 'UJKeterangan' ], [ 'class' => 'form-control', 'maxlength' => '150' ] ),
                ":Tgl"              => FormField::dateInput( [ 'name' => 'UJTgl', 'config' => [ 'value' => General::asDate( $dsSetup[ 'UJTgl' ] ) ] ] ),
                ":Jam"              => FormField::timeInput( [ 'name' => 'UJJam', 'config' => [ 'value' => $dsSetup[ 'UJTgl' ] ] ] ),
                ":TC"               => FormField::combo( 'KodeTrans', [  'name'=> "KodeTrans",'config' => ['value' => $dsSetup[ 'KodeTrans' ], 'data' => [
                                        'UJ' => 'UJ - Update Harga Jasa',] ], 'extraOptions'  => [ 'simpleCombo' => true ] ] ),
                ":Status"           => Html::textInput( 'Cetak', $dsSetup[ 'Cetak' ], [ 'class' => 'form-control','readonly' => 'readonly' ] ),
			],
            [
                'url_main' => 'ttujhd',
                'url_id' => $id,
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlDetail = $url[ 'detail' ];
$urlPrint  = $url[ 'print' ];
$readOnly  = ( $_GET['action'] == 'view' ? 'true' : 'false' );
$barang    = \common\components\General::cCmd(
        "SELECT JasaKode, JasaNama, JasaGroup, JasaHrgBeli, JasaHrgJual, JasaWaktuKerja, JasaWaktuSatuan, JasaWaktuKerjaMenit, JasaStatus, JasaKeterangan FROM tdjasa
WHERE JasaStatus = 'A' 
UNION
SELECT JasaKode, JasaNama, JasaGroup, JasaHrgBeli, JasaHrgJual, JasaWaktuKerja, JasaWaktuSatuan, JasaWaktuKerjaMenit, JasaStatus, JasaKeterangan FROM tdjasa
WHERE JasaKode IN 
(SELECT ttUJit.JasaKode FROM ttUJit INNER JOIN tdjasa ON ttUJit.JasaKode = tdjasa.JasaKode 
 WHERE ttUJit.UJNo = :UJNo AND tdjasa.JasaStatus = 'N' GROUP BY ttUJit.JasaKode )
ORDER BY JasaStatus, JasaKode",[':UJNo' => $dsSetup['UJNo'] ] )->queryAll();

$this->registerJsVar( '__Barang', json_encode( $barang ) );
$this->registerJs( <<< JS
     var __BrgKode = JSON.parse(__Barang);
     __BrgKode = $.map(__BrgKode, function (obj) {
                      obj.id = obj.JasaKode;
                      obj.text = obj.JasaKode;
                      return obj;
                    });
     var __BrgNama = JSON.parse(__Barang);
     __BrgNama = $.map(__BrgNama, function (obj) {
                      obj.id = obj.JasaNama;
                      obj.text = obj.JasaNama;
                      return obj;
                    });
    
        
     $('#detailGrid').utilJqGrid({        
        editurl: '$urlDetail',       
        height: 300,
        extraParams: {
            UJNo: $('input[name="UJNo"]').val()
        },
        rownumbers: true,  
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,         
        colModel: [
            { template: 'actions' },
            {
                name: 'JasaKode',
                label: 'Kode',
                width: 150,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __BrgKode,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    // console.log(data);
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.JasaWaktuSatuan);
                            $('[id="'+rowid+'_'+'UJHrgJualLama'+'"]').utilNumberControl().val(data.JasaHrgJual);
                            $('[id="'+rowid+'_'+'UJHrgJualBaru'+'"]').utilNumberControl().val(data.JasaHrgJual);
                            $('[id="'+rowid+'_'+'UJHrgBeliLama'+'"]').utilNumberControl().val(data.JasaHrgBeli);
                            $('[id="'+rowid+'_'+'UJHrgBeliBaru'+'"]').utilNumberControl().val(data.JasaHrgBeli);
                            $('[id="'+rowid+'_'+'JasaGroup'+'"]').val(data.JasaGroup);
                            $('[id="'+rowid+'_'+'JasaNama'+'"]').val(data.JasaNama); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'JasaNama'+'"]').trigger('change');
                            window.hitungLine();
						}
                    }                 
                }
            },
            {
                name: 'JasaNama',
                label: 'Nama Jasa',
                width: 230,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __BrgNama,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    console.log(data);
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.JasaWaktuSatuan);
                            $('[id="'+rowid+'_'+'UJHrgJualLama'+'"]').utilNumberControl().val(data.JasaHrgJual);
                            $('[id="'+rowid+'_'+'UJHrgJualBaru'+'"]').utilNumberControl().val(data.JasaHrgJual);
                            $('[id="'+rowid+'_'+'UJHrgBeliLama'+'"]').utilNumberControl().val(data.JasaHrgBeli);
                            $('[id="'+rowid+'_'+'UJHrgBeliBaru'+'"]').utilNumberControl().val(data.JasaHrgBeli);
                            $('[id="'+rowid+'_'+'JasaGroup'+'"]').val(data.JasaGroup);
                            $('[id="'+rowid+'_'+'JasaKode'+'"]').val(data.JasaKode); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'JasaKode'+'"]').trigger('change');
                            window.hitungLine();
						}
                    }              
                }
            },
            {
                name: 'JasaGroup',
                label: 'Group',
                editable: true,
                width: 120,
            },      
            {
                name: 'UJHrgBeliLama',
                label: 'Hrg Beli Lama',
                editable: true,
                width: 110,
                template: 'money',
                editoptions:{
                    readOnly:true,
                }, 
            },    
            {
                name: 'UJHrgBeliBaru',
                label: 'Hrg Beli Baru',
                editable: true,
                width: 110,
                template: 'money'
            },
            {
                name: 'UJHrgJualLama',
                label: 'Hrg Jual Lama',
                editable: true,
                width: 110,
                template: 'money',
                editoptions:{
                    readOnly:true,
                }, 
            },    
            {
                name: 'UJHrgJualBaru',
                label: 'Hrg Jual Baru',
                editable: true,
                width: 110,
                template: 'money'
            }, 
        ], 
     }).init().fit($('.box-body')).load({url: '$urlDetail'});
      
         $('#btnCancel').click(function (event) {	      
           $('#form_ttujhd_id').attr('action','{$url['cancel']}');
           $('#form_ttujhd_id').submit();
        });
	
        $('#btnSave').click(function (event) {	      
            $('input[name="SaveMode"]').val('ALL');
            $('#form_ttujhd_id').attr('action','{$url['update']}');
            $('#form_ttujhd_id').submit();
        }); 

       $('#btnPrint').click(function (event) {	      
        $('#form_ttujhd_id').attr('action','$urlPrint');
        $('#form_ttujhd_id').attr('target','_blank');
        $('#form_ttujhd_id').submit();
	  });   
JS
);
