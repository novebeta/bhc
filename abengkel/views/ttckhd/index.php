<?php
use abengkel\assets\AppAsset;
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '[CK] Klaim KPB';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'CKNo'        => 'No CK',
		'CKNoKlaim'   => 'No Klaim',
		'CKMemo'      => 'Memo',
		'NoGL'        => 'No GL',
		'Cetak'       => 'Cetak',
		'UserID'      => 'User ID',
	],
	'cmbTgl'    => [
		'CKTgl'       => 'Tgl CK',
	],
	'cmbNum'    => [
		'CKTotalPart' => 'Total Part',
		'CKTotalJasa' => 'Total Jasa',
	],
	'sortname'  => "CKTgl",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Ttckhd::className(), '[CK] Klaim KPB',
    [
        'url_delete' => Url::toRoute( [ 'ttckhd/delete' ] ),
    ]
), \yii\web\View::POS_READY );
