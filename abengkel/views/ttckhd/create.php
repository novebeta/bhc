<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttckhd */
$params                          = '&id=' . $id . '&action=create';
$cancel                          = Custom::url(\Yii::$app->controller->id.'/cancel'.$params);
$this->title                     = 'Tambah - Klaim KPB';
?>
<div class="ttckhd-create">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTServis' => $dsTServis,
        'id' => $id,
        'url'    => [
			'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
			'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttckit/index','action' => 'update' ] ),
		]
    ]) ?>

</div>
