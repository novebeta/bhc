<?php
use abengkel\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttckhd */
/* @var $form yii\widgets\ActiveForm */

$format          = <<< SCRIPT
function matchCustom(params, data) {
if ($.trim(params.term) === '') {
return data;
}
if (typeof data.text === 'undefined') {
return null;
}
if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
var modifiedData = $.extend({}, data, true);
return modifiedData;
}
return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttckhd-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_ttckhd_id','action' => $url['update'] ] );
		\abengkel\components\TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-1", 'items' => ":LabelCKNo" ],
					    [ 'class' => "col-sm-3", 'items' => ":CKNo" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-1", 'items' => ":LabelCKTgl" ],
					    [ 'class' => "col-sm-2", 'items' => ":CKTgl" ],
					    [ 'class' => "col-sm-2", 'items' => ":CKJam" ],
					    [ 'class' => "col-sm-7" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelNoKlaimKPB" ],
					    [ 'class' => "col-sm-5", 'items' => ":NoKlaimKPB" ],
				    ],
				  ],
				  [
					  'class' => "row col-md-24",
					  'style' => "margin-bottom: 6px;",
					  'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-8" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelTotalJasa" ],
					    [ 'class' => "col-sm-3", 'items' => ":TotalJasa" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelTotalPart" ],
					    [ 'class' => "col-sm-3", 'items' => ":TotalPart" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-1", 'items' => ":LabelTotal" ],
					    [ 'class' => "col-sm-3", 'items' => ":Total" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelKeterangan" ],
					    [ 'class' => "col-sm-17", 'items' => ":Keterangan" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-1", 'items' => ":LabelCetak" ],
					    [ 'class' => "col-sm-3", 'items' => ":Cetak" ]
				    ],
				  ],
			  ]
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
                  ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                      'items'  => [
                          ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":JT"],
                          ['class' => "col-sm-4 pull-right", 'items' => ":LabelJT"],
                      ],
                  ],
                  [
                      'class' => "col-md-12 pull-left",
                      'items' => $this->render( '../_nav', [
                          'url'=> $_GET['r'],
                          'options'=> [
                              'CKNo'        => 'No CK',
                              'CKNoKlaim'   => 'No Klaim',
                              'CKMemo'      => 'Memo',
                              'NoGL'        => 'No GL',
                              'Cetak'       => 'Cetak',
                              'UserID'      => 'User ID',
                          ],
                      ])
                  ],
                  ['class' => "col-md-12 pull-right",
				    'items' => [
					    [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
				    ]
				  ]
			  ]
			],
			[
				":LabelCKNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No CK</label>',
				":LabelCKTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl CK</label>',
				":LabelNoKlaimKPB" => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Klaim KPB</label>',
				":LabelTotalJasa"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total Jasa</label>',
				":LabelTotalPart"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total Part</label>',
				":LabelTotal"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total</label>',
				":LabelKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelCetak"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Cetak</label>',
				":LabelJT"         => \common\components\General::labelGL( $dsTServis[ 'NoGL' ], 'JT' ),

                ":CKNo"       => Html::textInput( 'CKNoView', $dsTServis[ 'CKNoView' ], [ 'class' => 'form-control', 'readonly'=> 'readonly'  ] ) .
                                 Html::hiddenInput( 'CKNo', $dsTServis[ 'CKNo' ]) .
                                 Html::textInput( 'SaveMode', 'ALL', [ 'class' => 'hidden' ] ),
				":NoKlaimKPB" => Html::textInput( 'CKNoKlaim', $dsTServis[ 'CKNoKlaim' ], [ 'class' => 'form-control', 'maxlength' => '35' ] ),
				":CKTgl"      => FormField::dateInput( [ 'name' => 'CKTgl', 'config' => [ 'value' => General::asDate( $dsTServis[ 'CKTgl' ] ) ] ] ),
				":CKJam"      => FormField::timeInput( [ 'name' => 'CKJam', 'config' => [ 'value' => $dsTServis[ 'CKTgl' ] ] ] ),
				":Keterangan" => Html::textInput( 'CKMemo', $dsTServis[ 'CKMemo' ], [ 'class' => 'form-control', 'maxlength' => '150' ] ),
				":Cetak"      => Html::textInput( 'Cetak', $dsTServis[ 'Cetak' ], [ 'class' => 'form-control', 'readonly'=> 'readonly' ] ),
				":JT"         => Html::textInput( 'NoGL', $dsTServis[ 'NoGL' ], [ 'class' => 'form-control', 'readonly'=> 'readonly' ] ),
				":TotalJasa"  => FormField::numberInput( [ 'name' => 'CKTotalJasa', 'config' => [ 'value' => $dsTServis[ 'CKTotalJasa' ], 'readonly'=> 'readonly'  ] ] ),
				":TotalPart"  => FormField::numberInput( [ 'name' => 'CKTotalPart', 'config' => [ 'value' => $dsTServis[ 'CKTotalPart' ], 'readonly'=> 'readonly'  ] ] ),
				":Total"      => FormField::numberInput( [ 'name' => 'CKTotal', 'config' => [ 'value' => $dsTServis[ 'CKTotal' ], 'readonly'=> 'readonly' ] ] ),
			], [
				'url_main' => 'ttckhd',
				'url_id' => $_GET['id'] ?? '',
                '_akses' => '[CK] Klaim KPB',
			]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlDetail      = $url[ 'detail' ];
$readOnly       = ( $_GET['action'] == 'view' ? 'true' : 'false' );
$urlPopup       = Url::toRoute( [ 'ttckhd/order'] );
$urlLoop        = Url::toRoute(['ttckit/index']);
$this->registerJs( <<< JS
        
        function HitungTotal(){
         	let Total = $('#detailGrid').jqGrid('getCol','CKTotal',false,'sum');
         	$('#Total-disp').val(Total);
         }
         window.HitungTotal = HitungTotal;
        
        $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 250,
        navButtonTambah: false,
        extraParams: {
            CKNo: $('input[name="CKNo"]').val()
        },
        rownumbers: true,  
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,        
        colModel: [
            { template: 'actions'  },
            {
                name: 'SDNo',
                label: 'No',
                width: 80,
                editable: true,
                editor : {
                    type: 'text',
                    readOnly: true
                }
            },
            {
                name: 'SDTgl',
                label: 'Tanggal',
                width: 120,
                editable: true,
                editor : {
                    type: 'text',
                    readOnly: true
                }
            },
            {
                name: 'MotorNoPolisi',
                label: 'NoPolisi',
                editable: true,
                width: 80,
                editor : {
                    type: 'text',
                    readOnly: true
                }
            },     
            {
                name: 'CusNama',
                label: 'Nama',
                editable: true,
                width: 190,
                editor : {
                    type: 'text',
                    readOnly: true
                }
            },    
            {
                name: 'MotorType',
                label: 'MotorType',
                editable: true,
                width: 140,
                editor : {
                    type: 'text',
                    readOnly: true
                }
            },     
            {
                name: 'CKPart',
                label: 'Part',
                editable: true,
                width: 120,
                template: 'money',
                editor : {
                    readOnly: true
                }
            },      
            {
                name: 'CKJasa',
                label: 'Jasa',
                editable: true,
                width: 100,
                template: 'money'
            },      
            {
                name: 'CKTotal',
                label: 'Total',
                editable: true,
                width: 100,
                template: 'money',
                editor : {
                    readOnly: true
                }
            },   
        ],      
        listeners: {
             afterLoad: function (data) {
                window.HitungTotal();
            }
        }
     }).init().fit($('.box-body')).load({url: '$urlDetail'});
    
    $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
        window.cmp.rowid = rowid;    
    });
    
    $('#detailGrid').jqGrid('navButtonAdd',"#detailGrid-pager",{ 
        caption: ' Tambah',
        buttonicon: 'glyphicon glyphicon-plus',
        onClickButton:  function() {
            window.util.app.dialogListData.closeTriggerFromIframe = function(){ 
                var iframe = window.util.app.dialogListData.iframe();
                // var mainGrid = iframe.contentWindow.jQuery('#jqGrid').jqGrid();
                // window.util.app.dialogListData.gridFn.mainGrid = mainGrid;
                // window.util.app.dialogListData.gridFn.var.multiSelect = true;
                var selRowId = iframe.contentWindow.util.app.dialogListData.gridFn.getSelectedRow(true);
                console.log(selRowId);
                $.ajax({
                     type: "POST",
                     url: '$urlLoop',
                     data: {
                         oper: 'batch',
                         data: selRowId,
                         header: {
                             'CKNo': $('[name="CKNo"]').val()
                         }
                     },
                     success: function(data) {
                        iframe.contentWindow.jQuery('#jqGrid').jqGrid().trigger('reloadGrid');
                     },
                   });
                return false;
            }
            window.util.app.dialogListData.show({
                title: "Daftar Servis KPB dari Klaim KPB Nomor : {$dsTServis['CKNoView']}",
                url: '$urlPopup'
            },function(){                 
                window.location.href = "{$url['update']}&oper=skip-load&CKNoView={$dsTServis['CKNoView']}"; 
            });
        }, 
        position: "last", 
        title:"", 
        cursor: "pointer"
    });
    	
	$('#btnSave').click(function (event) {	 	 
//	    let Nominal = parseFloat($('#CKTotal-disp').inputmask('unmaskedvalue'));
//           if (Nominal === 0 ){
//               bootbox.alert({message:'Total tidak boleh 0', size: 'small'});
//               return;
//           }
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttckhd_id').attr('action','{$url['update']}');
        $('#form_ttckhd_id').submit();
    }); 
JS
);