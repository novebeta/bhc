<?php

use abengkel\models\Ttsdhd;
use yii\helpers\Html;
use abengkel\assets\AppAsset;
use yii\widgets\ActiveForm;
use abengkel\components\FormField;
use kartik\datecontrol\DateControl;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttgeneralledgerhd */
/* @var $form yii\widgets\ActiveForm */
AppAsset::register( $this );
$this->title                     = 'PUSH INV2';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr = [
    'cmbTxt' => [
        'noShippingList' =>	'No Shipping List',
        'poId' =>	'Po Id',
        'idSpk' =>	'Id Spk',
    ],
    'cmbTgl' => [],
    'cmbNum' => [],
    'sortname' => "noShippingList",
    'sortorder' => 'desc'
];
$this->registerJsVar('setcmb', $arr);
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render('../_search'); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
            <table id="jqGridDetail"></table>
            <table id="scnjqGridDetail"></table>
            <div class="col-md-24">
                <div class="form-group">
                    <button type="submit" formtarget="_blank" class="btn btn-primary fas fa-upload pull-right"> PUSH INV2</button>
                </div>
            </div>
        </div>
    </div>

<?php
$this->registerJs(<<< JS
    window.gridParam_jqGrid = {
        readOnly: true,
        rownumbers: true,
        rowNum: 10000,
        pgbuttons: false,
        pginput: false,
        viewrecords: false,
        rowList: [],
        pgtext: "",
        height: 120,
    }
    $('#filter-tgl-val1').change(function() {
        let _tgl2 = new Date($(this).val());      
        _tgl2.setDate(_tgl2.getDate() + 7);
        $('[name=tgl2]').val(_tgl2.toISOString().slice(0, 10));
	    $('#filter-tgl-val2-disp').datepicker('update', _tgl2);
	    $('#filter-tgl-val2-disp').trigger('change');
    });
JS
);
$this->registerJs( \abengkel\components\TdUi::mainGridJs(\abengkel\models\Ttsdhd::class,
    '',[
        'colGrid' => Ttsdhd::colGridPushInv2(),
        'url' => Url::toRoute( [ 'site/details' ] ),
        'mode' => isset($mode) ? $mode : '',

    ] ),	\yii\web\View::POS_READY );
$urlJasa = '';
$urlParts = '';
$this->registerJs( <<< JS
jQuery(function ($) {
    if($('#jqGridDetail').length) {
        $('#jqGridDetail').utilJqGrid({
            url: '{$urlJasa}',
            height: 75,
            readOnly: true,
            rownumbers: true,
            colModel: [
                {
                    name: 'idJob',
                    label: 'Id Job',
                    width: 150
                },
                {
                    name: 'hargaServis',
                    label: 'Harga Servis',
                    width: 150
                },
               	{
	                name: 'promoIdJasa',
	                label: 'Promo Id Jasa',
	                width: 100,
	            },
                {
                    name: 'discServiceAmount',
                    label: 'Disc Service Amount',
                    width: 150,
                    editable: true,
                },
               	{
	                name: 'discServicePercentage',
	                label: 'Disc Service Percentage',
	                width: 150,
	            },
               	{
	                name: 'totalHargaServis',
	                label: 'Total Harga Servis',
	                width: 150,
	            },
               	{
	                name: 'createdTime',
	                label: 'Created Time',
	                width: 150,
	            }       
            ],
	        multiselect: gridFn.detail.multiSelect(),
	        onSelectRow: gridFn.detail.onSelectRow,
	        onSelectAll: gridFn.detail.onSelectAll,
	        ondblClickRow: gridFn.detail.ondblClickRow,
	        loadComplete: gridFn.detail.loadComplete,
	        listeners: {
                afterLoad: function(grid, response) {
                    $("#cb_jqGridDetail").trigger('click');
                }
            }
        }).init();
        $('#scnjqGridDetail').utilJqGrid({
            url: '{$urlParts}',
            height: 75,
            readOnly: true,
            rownumbers: true,
            colModel: [
                {
                    name: 'idJob',
                    label: 'Id Job',
                    width: 150
                },
               	{
	                name: 'partsNumber',
	                label: 'Parts Number',
	                width: 250,
	            },
                {
                    name: 'kuantitas',
                    label: 'Kuantitas',
                    width: 150
                },
                {
                    name: 'hargaParts',
                    label: 'Harga Parts',
                    width: 150,
                    editable: true,
                },
               	{
	                name: 'promoIDParts',
	                label: 'Promo ID Parts',
	                width: 150,
	            },
               	{
	                name: 'discPartsAmount',
	                label: 'Disc Parts Amount',
	                width: 150,
	            },
               	{
	                name: 'discPartsPercentage',
	                label: 'Disc Parts Percentage',
	                width: 150,
	            },
               	{
	                name: 'ppn',
	                label: 'Ppn',
	                width: 150,
	            },
               	{
	                name: 'totalHargaParts',
	                label: 'Total Harga Parts',
	                width: 150,
	            },
               	{
	                name: 'UangMuka',
	                label: 'Uang Muka',
	                width: 150,
	            },
               	{
	                name: 'createdTime',
	                label: 'Created Time',
	                width: 150,
	            }
            ],
	        multiselect: gridFn.detail.multiSelect(),
	        onSelectRow: gridFn.detail.onSelectRow,
	        onSelectAll: gridFn.detail.onSelectAll,
	        ondblClickRow: gridFn.detail.ondblClickRow,
	        loadComplete: gridFn.detail.loadComplete,
	        listeners: {
                afterLoad: function(grid, response) {
                    $("#cb_scnjqGridDetail").trigger('click');
                }
            }
        }).init();
    }
});

// $("#jqGrid").bind("jqGridSelectRow", function (e, rowid, orgClickEvent) {
//     var data = $(this).getRowData(rowid);
//     $('#scnjqGridDetail').utilJqGrid().load({param: data});
// });
//
// $("#jqGrid").bind("jqGridSelectRow", function (e, rowid, orgClickEvent) {
//     var data = $(this).getRowData(rowid);
//     $('#jqGridDetail').utilJqGrid().load({param: data});
// });
JS, \yii\web\View::POS_READY );
