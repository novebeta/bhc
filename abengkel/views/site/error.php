<?php
use abengkel\components\Menu;
use yii\helpers\Html;
use yii\helpers\Json;
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */
$this->title = $name;
?>
<section class="content">
    <div class="error-content">
        <p>
			<?=$message; ?>
        </p>
        <p>
            The above error occurred while the Web server was processing your request.
            Please contact us if you think this is a server error. Thank you.
            Meanwhile, you may <a href='<?= Yii::$app->homeUrl ?>'>return to dashboard</a>.
        </p>
        <p>
            <a data-toggle="collapse" href="#trace" role="button" aria-expanded="false">
                Tracing Data
            </a>Or
            <a data-toggle="collapse" href="#input" role="button" aria-expanded="false">
                Input Data
            </a>
        </p>
        <div class="collapse" id="trace">
            <div class="card card-body">
                <pre><?=$exception->getTraceAsString();?></pre>
            </div>
        </div>
        <div class="collapse" id="input">
            <div class="card card-body">
                    <pre>PROFILE : <?=JSON::encode(Menu::getUserLokasi())."\n"?>POST    : <?=JSON::encode($_POST)."\n"?>GET     : <?=JSON::encode($_GET)."\n"?>
                    </pre>
            </div>
        </div>
    </div>
</section>
