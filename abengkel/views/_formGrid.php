<?php
use abengkel\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = $title;
$this->params[ 'breadcrumbs' ][] = $this->title;
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render( '_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs(\abengkel\models\Ttsdhd::className(), $title, $options ),	\yii\web\View::POS_READY );
$this->registerJs( <<< JS
jQuery(function ($) {
    if($('#jqGridDetail').length) {
        $('#jqGridDetail').utilJqGrid({
            url: '',
            height: 240,
            readOnly: true,
            rownumbers: true,
            colModel: [
                {
                    name: 'JasaKode',
                    label: 'Kode',
                    width: 100
                },
               	{
	                name: 'JasaNama',
	                label: 'Nama Jasa',
	                width: 250,
	            },
	            {
	                name: 'SDWaktuKerja',
	                label: 'Waktu .',
	                width: 100,
	            },
	            {
	                name: 'SDWaktuSatuan',
	                label: 'Satuan',
	                width: 160,
	            },
                    {
                    name: 'SDHrgJual',
                    label: 'Harga Jasa .',
                    width: 75,
                    editable: true,
                    },            
                    {
                        name: 'Disc',
                        label: 'Disc %',
                        width: 100,
                        editable: true,
                    },
                    {
                        name: 'SDJualDisc',
                        label: 'Discount',
                        width: 100,
                        editable: true,
                        template: 'money'
                    },
                    {
                        name: 'Jumlah',
                        label: 'Jumlah',
                        width: 100,
                        editable: true,
                        template: 'money'
                    },          
            ],
	        multiselect: gridFn.detail.multiSelect(),
	        onSelectRow: gridFn.detail.onSelectRow,
	        onSelectAll: gridFn.detail.onSelectAll,
	        ondblClickRow: gridFn.detail.ondblClickRow,
	        loadComplete: gridFn.detail.loadComplete,
	        listeners: {
                afterLoad: function(grid, response) {
                    $("#cb_jqGridDetail").trigger('click');
                }
            }
        }).init().setHeight(gridFn.detail.height);
        $('#scnjqGridDetail').utilJqGrid({
            url: '',
            height: 240,
            readOnly: true,
            rownumbers: true,
            colModel: [
                {
                    name: 'BrgKode',
                    label: 'Kode',
                    width: 100
                },
               	{
	                name: 'BrgNama',
	                label: 'Nama Barang',
	                width: 250,
	            },
	            {
	                name: 'SDQty',
	                label: 'Qty .',
	                width: 100,
	            },
	            {
	                name: 'BrgSatuan',
	                label: 'Satuan',
	                width: 160,
	            },
                    {
                    name: 'SDHrgJual',
                    label: 'Harga Part',
                    width: 75,
                    editable: true,
                    },            
                    {
                        name: 'Disc',
                        label: 'Disc %',
                        width: 100,
                        editable: true,
                    },
                    {
                        name: 'SDJualDisc',
                        label: 'Nilai Disc.',
                        width: 100,
                        editable: true,
                        template: 'money'
                    },
                    {
                        name: 'Jumlah',
                        label: 'Jumlah',
                        width: 100,
                        editable: true,
                        template: 'money'
                    },          
            ],
	        multiselect: gridFn.detail.multiSelect(),
	        // onSelectRow: gridFn.detail.onSelectRow,
	        // onSelectAll: gridFn.detail.onSelectAll,
	        // ondblClickRow: gridFn.detail.ondblClickRow,
	        loadComplete:  function() {
                    console.log('test');
                    $("#cb_scnjqGridDetail").trigger('click');
                },
	        listeners: {
                afterLoad: function(grid, response) {
                    console.log('test');
                    $("#cb_scnjqGridDetail").trigger('click');
                }
            }
        }).init().setHeight(gridFn.detail.height);
    }
    $("#jqGrid").bind("jqGridSelectRow", function (e, rowid, orgClickEvent) {
    var data = $(this).getRowData(rowid);
    $('#scnjqGridDetail').utilJqGrid().load({param: data});
});
JS, \yii\web\View::POS_READY );