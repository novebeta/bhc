<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdpos */

$this->title = 'Tambah POS';
$this->params['breadcrumbs'][] = ['label' => 'POS', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdpo-create">
    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdpos/update', 'id' => $id ,'action' => 'create'] ),
            'cancel' => Url::toRoute( [ 'tdpos/cancel', 'id' => $id,'action' => 'create' ] )
        ]
    ]) ?>

</div>
