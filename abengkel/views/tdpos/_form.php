<?php
use abengkel\components\FormField;
use abengkel\models\Traccount;
use kartik\number\NumberControl;
use common\components\Custom;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\web\View;
use abengkel\models\Tdkaryawan;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdpos */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function formattdlokasi(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-6" style="text-align: left">' + result.id + '</div>' +
           '<div class="col-xs-18">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
?>

<div class="tdpos-form">
    <?php
    $form = ActiveForm::begin(['id' => 'frm_tdpos_id' ,'enableClientValidation'=>false ]);
    \abengkel\components\TUi::form(
        ['class' => "row-no-gutters",
            'items' => [
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelPosKode"],
                        ['class' => "col-sm-5",'items' => ":PosKode"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelPosNama"],
                        ['class' => "col-sm-6",'items' => ":PosNama"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelPosNomor"],
                        ['class' => "col-sm-5",'items' => ":PosNomor"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelPosAlamat"],
                        ['class' => "col-sm-14",'items' => ":PosAlamat"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelPosJenis"],
                        ['class' => "col-sm-5",'items' => ":PosJenis"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelKarKode"],
                        ['class' => "col-sm-14",'items' => ":KarKode"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelPosTelepon"],
                        ['class' => "col-sm-5",'items' => ":PosTelepon"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelNoAccount"],
                        ['class' => "col-sm-14",'items' => ":NoAccount"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelPosStatus"],
                        ['class' => "col-sm-5",'items' => ":PosStatus"],
                    ],
                ],
            ]
        ],
        [
            'class' => "pull-right",
            'items' => ":btnAction"
        ],
        [
            ":LabelPosKode"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode</label>',
            ":LabelPosNama"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama</label>',
            ":LabelKarKode"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Petugas</label>',
            ":LabelPosAlamat"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
            ":LabelPosTelepon"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon</label>',
            ":LabelPosNomor"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor</label>',
            ":LabelPosStatus"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',
            ":LabelPosJenis"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
            ":LabelNoAccount"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Account</label>',

            ":PosKode"       => $form->field($model, 'PosKode',['template' => '{input}'])->textInput(['autofocus' => 'autofocus', 'maxlength' => '20']),
            ":PosNama"       => $form->field($model, 'PosNama',['template' => '{input}'])->textInput(['maxlength' => '75',]),
            ":PosAlamat"     => $form->field($model, 'PosAlamat',['template' => '{input}'])->textInput(['maxlength' => '150',]),
            ":PosTelepon"    => $form->field($model, 'PosTelepon',['template' => '{input}'])->textInput(['maxlength' => '30',]),
            ":PosNomor"      => $form->field($model, 'PosNomor',['template' => '{input}'])->textInput(['maxlength' => '2',]),
            ":PosStatus"     => FormField::combo( 'LokasiStatus',[  'extraOptions' => ['simpleCombo' => true]] ),
            ":PosJenis"      => FormField::combo( 'PosJenis',[  'extraOptions' => ['simpleCombo' => true]] ),

            ":NoAccount"       => $form->field($model, 'NoAccount', ['template' => '{input}'])
                                ->widget(Select2::classname(), [
                                    'value' => $model->NoAccount,
                                    'theme' => Select2::THEME_DEFAULT,
                                    'data' => Traccount::find()->Account(),
                                    'pluginOptions' => [
                                        'dropdownAutoWidth' => true,
                                        'templateResult' => new JsExpression('formattdlokasi'),
                                        'templateSelection' => new JsExpression('formattdlokasi'),
                                        'matcher' => new JsExpression('matchCustom'),
                                        'escapeMarkup' => $escape]]),

            ":KarKode"        => $form->field($model, 'KarKode', ['template' => '{input}'])
                                ->widget(Select2::classname(), [
                                    'value' => $model->KarKode,
                                    'theme' => Select2::THEME_DEFAULT,
                                    'data' => Tdkaryawan::find()->KarKode(),
                                    'pluginOptions' => [
                                        'dropdownAutoWidth' => true,
                                        'templateResult' => new JsExpression('formattdlokasi'),
                                        'templateSelection' => new JsExpression('formattdlokasi'),
                                        'matcher' => new JsExpression('matchCustom'),
                                        'escapeMarkup' => $escape]]),
        ],
	    [
		    'url_main'   => 'tdpos',
		    'url_id'     => $id,
		    'url_update' => $url[ 'update' ]
	    ]
    );
    ActiveForm::end();
    ?>
</div>
<?php
$urlIndex   = Url::toRoute( [ 'tdpos/index' ] );
$urlAdd     = Url::toRoute( [ 'tdpos/create','id'=>'new' ] );
$this->registerJs( <<< JS
     $('#btnSave').click(function (event) {
       $('#frm_tdpos_id').submit();
    }); 
    $('#btnCancel').click(function (event) {	  
         window.location.href = '{$url['cancel']}';
    });
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
    $('#btnAdd').click(function (event) {	      
        window.location.href = '{$urlAdd}';
	  });
    
    


JS);


