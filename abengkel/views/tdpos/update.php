<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdpos */

$this->title = 'Edit POS : ' . $model->PosKode;
$this->params['breadcrumbs'][] = ['label' => 'POS', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="tdpo-update">
    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdpos/update', 'id' => $id ,'action' => 'update'] ),
            'cancel' => Url::toRoute( [ 'tdpos/cancel', 'id' => $id,'action' => 'update' ] )
        ]
    ]) ?>

</div>
