<?php
use abengkel\assets\AppAsset;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'POS';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'PosKode'       => 'Kode POS',
		'PosNama'       => 'Nama POS',
		'PosAlamat'     => 'Alamat',
		'PosTelepon'    => 'Telepon',
//		'KarKode'       => 'Karyawan',
		'PosNomor'      => 'Nomor',
		'PosStatus'     => 'Status',
		'PosJenis'      => 'Jenis',
		'NoAccount'     => 'NoAccount',
	],
	'cmbTgl'    => [
	],
	'cmbNum'    => [
	],
	'sortname'  => "PosStatus, PosKode",
	'sortorder' => ''
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Tdpos::className(), 'POS' ), \yii\web\View::POS_READY );
