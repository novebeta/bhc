<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
$this->title                   = 'Daftar [PS] Penerimaan Barang';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;

$sqlSupplier                           = Yii::$app->db
	->createCommand( "SELECT '%' AS value, 'Semua' AS label, 0 AS SupStatus, 0 AS SupKode
		UNION
		SELECT SupKode AS value, CONCAT(SupNama,' ', SupKode) AS label, SupStatus, SupKode FROM tdsupplier 
		WHERE SupStatus = 'A' OR SupStatus = 1 
		ORDER BY SupStatus, SupKode" )
	->queryAll();
$cmbSupplier                           = ArrayHelper::map( $sqlSupplier, 'value', 'label' );
//$cmbSupplier                           = json::encode( [ 'results' => $sql ] );
//$sqlSupplier                           = [ 'results' => [ 'id' => 1, 'text' => 'text1' ] ] ;
$format = <<< SCRIPT
function formatBank(result) {
    return '<div class="row">' +
           '<div class="col-md-6">' + result.id + '</div>' +
           '<div class="col-md-18">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );

?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Header1' => 'Header1',
									'Item1' => 'Item1',
									'Item2' => 'Item2',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-footer">
				<div class="container-fluid">
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-sm-4">Tanggal</label>
							<div class="col-md-10">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
							<div class="col-md-10">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="form-group">
							<label class="control-label col-sm-4">Supplier</label>
							<div class="col-sm-20">
								<? echo Select2::widget( [
									'name'          => 'supplier',
									'data'          => $cmbSupplier,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatBank' ),
										'templateSelection' => new JsExpression( 'formatBank' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-sm-4">TC</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'TC', null, [
								""     => "Semua",
								"TC01" => "TC01 - Penerimaan dari Supplier MD",
								"TC02" => "TC02 - Penerimaan dari Supplier Lokal",
								"TC03" => "TC03 - Penerimaan Part Robbing",
								"TC05" => "TC05 - Penerimaan dari Dealer Lain",
								"TC06" => "TC06 - Penerimaan Olie KPB",
								"TC07" => "TC07 - Penerimaan Part HO",
								"TC08" => "TC08 - Penerimaan Part Claim",
								"TC09" => "TC09 - Penerimaan Kekurangan Part/Oli",
								"TC10" => "TC10 - Penerimaan Lain Lain",
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="form-group">
							<label class="control-label col-sm-4">Sort</label>
							<div class="col-sm-13">
								<?= Html::dropDownList( 'Sort', null, [
									"ttpshd.PSNo"             => "No Penerimaan [PS]",
									"ttpshd.PSTgl"            => "Tanggal Penerimaan [PS]",
									"ttpshd.PSNoRef"          => "No Faktur [NoRef]",
									"ttpshd.PINo"             => "No Pembelian Inv [PI]",
									"ttpshd.PONo"             => "No Order [PO]",
									"ttpshd.TINo"             => "No Transfer [TI]",
									"ttpshd.NoGL"             => "No JT",
									"ttpshd.KodeTrans"        => "TC",
									"ttpshd.SupKode"          => "Kode Supplier",
									"tdSupplier.SupNama"      => "Nama Supplier",
									"ttpshd.DealerKode"       => "Kode Dealer",
									"tdDealer.DealerNama"     => "Nama Dealer",
									"ttpshd.PSKeterangan"     => "Keterangan",
									"ttpshd.Cetak"            => "Cetak",
									"ttpshd.LokasiKode"       => "Lokasi",
									"ttpshd.PosKode"          => "POS",
									"ttpshd.UserID"           => "User",
									"DATE(ttpshd.PSTgl)"      => "Tgl Beli",
									"DATE(ttpshd.PSTglTempo)" => "Tgl Tempo",
									"ttpshd.PSSubTotal"       => "Sub Total",
									"ttpshd.PSDiscFinal"      => "Discount Final",
									"ttpshd.PSTotalPajak"     => "Total Pajak",
									"ttpshd.PSBiayaKirim"     => "Biaya Kirim",
									"ttpshd.PSTotal"          => "Total Jual",
									"ttpshd.PSTerm"           => "Term",                 
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
							<div class="col-sm-7">
								<?= Html::dropDownList( 'order', null, [
								"ASC" => 'ASC',
								'DESC' => 'DESC',
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Status</label>
							<div class="col-sm-12">
								<?= Html::dropDownList( 'status', null, [
								""                          => "Semua",
								" AND ttpshd.PINo <> '--' " => "Sudah PI",
								" AND ttpshd.PINo = '--' "  => "Belum PI",
								" AND ttpshd.TINo <> '--' " => "Sudah TI",
								" AND ttpshd.TINo = '--' "  => "Belum TI",
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
				</div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
