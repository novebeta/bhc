<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
$this->title                   = 'Daftar Kas';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;
$sqlBank                           = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, 0 AS StatusAccount,  '' AS NoAccount
	UNION
	SELECT NoAccount AS value, NamaAccount AS label, StatusAccount, NoAccount AS NoAccount FROM traccount WHERE NoParent IN ('11010000','11010300') AND StatusAccount = 'A' AND JenisAccount = 'Detail' 
	ORDER BY StatusAccount, NoAccount" )
	->queryAll();
$cmbBank                           = ArrayHelper::map( $sqlBank, 'value', 'label' );

$format = <<< SCRIPT
function formatBank(result) {
    return '<div class="row">' +
           '<div class="col-md-3">' + result.id + '</div>' +
           '<div class="col-md-21">' + result.text + '</div>' +
           '</div>';
}

function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );

?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Kas Masuk' => 'Kas Masuk',
									'Kas Keluar' => 'Kas Keluar',
									'Kas Saldo' => 'Kas Saldo',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-footer">
				<div class="container-fluid">
					<div class="col-md-2">
						<div class="form-group">
							<label class="control-label col-sm-24">Tanggal</label>
						</div>
					</div>
					<div class="col-md-8">
						<div class="form-group">
							<div class="col-md-10">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format' => 'dd/M/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
							<div class="col-md-10">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="form-group">
							<label class="control-label col-sm-5">Kode Trans</label>
							<div class="col-sm-19">
								<?= Html::dropDownList( 'TC', null, [
								"" => "All - Semua",
								"UM"  => "UM - Umum",
								"BK"  => "BK - KM Dari Bank",
								"SV"  => "SV - Servis",
								"SE"  => "SE - Estimati Biaya Servis",
								"SI"  => "SI - Penjualan",
								"SO"  => "SO - Order Penjualan",
								"PR"  => "PR - Retur Pembelian",
								"PL"  => "PL - Pengerjaan Luar",
								"All" => "All-Semua",
								"UM"  => "UM-Umum",
								"BM"  => "BM-KK Ke Bank",
								"PI"  => "PI-Pembelian",
								"SR"  => "SR-Retur Penjualan",
								"PL"  => "PL-Pengerjaan Luar",
								"H1"  => "H1-KK Ke H1",
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<div class="col-md-2">
						<div class="form-group">
							<label class="control-label col-sm-24">Kas</label>
						</div>
					</div>
					<div class="col-md-17">
						<div class="form-group">
							<div class="col-sm-24">
								<? echo Select2::widget( [
									'name'          => 'kas',
									'data'          => $cmbBank,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatBank' ),
										'templateSelection' => new JsExpression( 'formatBank' ),
										'matcher'           => new JsExpression( 'matchCustom' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<div class="col-md-2">
						<div class="form-group">
							<label class="control-label col-sm-24">Jenis</label>
						</div>
					</div>
					<div class="col-md-8">
						<div class="form-group">
							<div class="col-sm-20">
								<?= Html::dropDownList( 'jenis', null, [
								""               => "Semua",
								"Tunai"          => "Tunai",
								"Kas Kredit"     => "Kas Kredit",
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="form-group">
							<label class="control-label col-sm-5">Sort</label>
							<div class="col-sm-14">
								<?= Html::dropDownList( 'Sort', null, [],['id'=>'sort-id', 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
							<div class="col-sm-5">
								<?= Html::dropDownList( 'order', null, [
								"ASC" => 'ASC',
								'DESC' => 'DESC',
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
				</div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php

$arr = [
	'Kas Masuk'  => [
		'cmb'  => [
			"KMTgl"         => "Tanggal",
			"KMNo"          => "No KM",
			"KMJenis"       => "Jenis",
			"KMNominal"     => "Nominal",
			"KMPerson"      => "Person",
			"KasKode"       => "Kode Kas",
			"KodeTrans"     => "Kode Trans",
			"MotorNoPolisi" => "No Polisi",
			"KMMemo"        => "Keterangan",
			"UserID"        => "UserID",
			"KMBayarTunai"  => "Bayar Tunai",
        ],
	],
	'Kas Keluar' => [
		'cmb'  => [
			"KKTgl"         => "Tanggal",
			"KKNo"          => "No KK",
			"KKJenis"       => "Jenis",
			"KMNominal"     => "Nominal",
			"KKPerson"      => "Person",
			"KasKode"       => "Kode Kas",
			"KodeTrans"     => "Kode Trans",
			"SupKode"       => "Supplier",
			"KKMemo"        => "Keterangan",
			"UserID"        => "UserID",
        ],
	],
	'Kas Saldo' => [
		'cmb'  => [
			"Tgl"           => "Tanggal",
			"No"            => "Nomor",
			"Jenis"         => "Jenis",
			"Nominal"       => "Nominal",
			"Person"        => "Person",
			"KasKode"       => "Kode Kas",
			"KodeTrans"     => "Kode Trans",
			"MotorNoPolisi" => "Motor No Polisi",
			"Memo"          => "Keterangan",
        ],
	],
];
$this->registerJsVar( 'setcmb', $arr );
$this->registerJs( $this->render( 'DaftarKas.js' ) );
