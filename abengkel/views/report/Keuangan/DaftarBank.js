$("#tipe-id").change(function () {
    var tipe = this.value;
    var option = $('#sort-id');
    option.find('option').remove().end();
    for (let val in setcmb[tipe].cmb) {
        option.append('<option value="' + val + '">' + setcmb[tipe].cmb[val] + '</option>');
    }
    option.prop("selectedIndex", 0);
});

$("#tipe-id").trigger('change');