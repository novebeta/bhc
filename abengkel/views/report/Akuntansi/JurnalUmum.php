<?php
use kartik\checkbox\CheckboxX;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
$this->title                   = 'Laporan Jurnal Umum';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;

$sqlBank = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, 0 AS StatusAccount,  '' AS NoAccount
	UNION
	SELECT NoAccount AS value, NamaAccount AS label, StatusAccount, NoAccount AS NoAccount FROM traccount 
	WHERE JenisAccount = 'Detail' 
	ORDER BY StatusAccount, NoAccount" )
	->queryAll();
$cmbBank = ArrayHelper::map( $sqlBank, 'value', 'label' );

$format = <<< SCRIPT
function formatBank(result) {
    return '<div class="row">' +
           '<div class="col-md-3">' + result.id + '</div>' +
           '<div class="col-md-21">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );

?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
            <div class="box-body">
                <div class="container-fluid">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Laporan</label>
                            <div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Jurnal Umum'          => 'Jurnal Umum',
									'Jurnal Perkiraan'     => 'Jurnal Perkiraan',
									'Jurnal Header'        => 'Jurnal Header',
									'Ledger Detail'        => 'Ledger Detail', //hide
									'Ledger Harian'        => 'Ledger Harian', //hide
									'Jurnal Tidak Balance' => 'Jurnal Tidak Balance', //hide
									'Belum Terposting'     => 'Belum Terposting',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Format</label>
                            <div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="container-fluid">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Tanggal</label>
                        </div>
                    </div>
                    <div class="col-md-14">
                        <div class="form-group">
                            <div class="col-md-6">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'id'            => 'tgl1_id',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
										'pluginOptions' => [
											'autoclose'      => true,
											'format'         => 'dd/mm/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                            <div class="col-md-6">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'id'            => 'tgl2_id',
										'type'          => DatePicker::TYPE_INPUT,
                                        'value'         => date('t/m/Y',strtotime("today")),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                            <label class="control-label col-sm-2">Bulan</label>
                            <div class="col-md-6">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'TglKu',
										'id'            => 'TglKu_id',
										'attribute2'	=>'to_date',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','MMMM  yyyy' ),
										'pluginOptions' => [
											'startView'=>'year',
											'minViewMode'=>'months',
											'format' => 'MM  yyyy'
                                        ],
										'pluginEvents'  => [
											"change" => new JsExpression("function(e) {
											 let d = new Date(Date.parse('01 ' + $(this).val()));											 
											 var tgl1 = $('#tgl1_id');
											 var tgl2 = $('#tgl2_id');
											 let d2 = tgl2.kvDatepicker('getDate');
											  tgl1.kvDatepicker('update', d);
											  tgl2.kvDatepicker('update',new Date(d.getFullYear(), d.getMonth()+1, 0));
											}")
										],
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
							<div class="col-sm-4">
								<? try {
									echo CheckboxX::widget([
										'name'=>'ringkas',
										'options'=>['id'=>'ringkas'],
										'pluginOptions'=>['threeState'=>false, 'size'=>'lg'],
									]);
									echo '<label class="cbx-label" for="ringkas">Ringkas</label>';
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24 jurnalHide">Transaksi 1</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-sm-24">
								<?= Html::dropDownList( 'trans1', null, [
									"01SD" => "[SD] Daftar Servis",
									"02SV" => "[SV] Invoice Servis ",
									"03SI" => "[SI] Invoice Penjualan",
									"04SR" => "[SR] Retur Penjualan",
									"05PS" => "[PS] Penerimaan Barang",
									"06PI" => "[PI] Invoice Pembelian",
									"07PR" => "[PR] Retur Pembelian",
									"08TI" => "[TI] Transfer Item",
									"09TA" => "[TA] Transfer Adjustment",
									"10KM" => "[KM] Kas Masuk",
									"11KK" => "[KK] Kas Keluar",
									"12BM" => "[BM] Bank Masuk",
									"13BK" => "[BK] Bank Keluar",
									"14CK" => "[CK] Klaim KPB",
									"15CC" => "[CC] Klaim C2",
									"16UC" => "[UC] Update Standard Cost",
									"17GL" => "[MM] Memorial",
									"18JP" => "[JP] Pemindah Bukuan",
								], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Perkiraan 1</label>
                        </div>
                    </div>
                    <div class="col-md-14">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? echo Select2::widget( [
									'name'          => 'perkiraan1',
									'data'          => $cmbBank,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatBank' ),
										'templateSelection' => new JsExpression( 'formatBank' ),
										'matcher'           => new JsExpression( 'matchCustom' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24 jurnalHide">Transaksi 2</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-sm-24">
								<?= Html::dropDownList( 'trans2', '17GL', [
									"01SD" => "[SD] Daftar Servis",
									"02SV" => "[SV] Invoice Servis ",
									"03SI" => "[SI] Invoice Penjualan",
									"04SR" => "[SR] Retur Penjualan",
									"05PS" => "[PS] Penerimaan Barang",
									"06PI" => "[PI] Invoice Pembelian",
									"07PR" => "[PR] Retur Pembelian",
									"08TI" => "[TI] Transfer Item",
									"09TA" => "[TA] Transfer Adjustment",
									"10KM" => "[KM] Kas Masuk",
									"11KK" => "[KK] Kas Keluar",
									"12BM" => "[BM] Bank Masuk",
									"13BK" => "[BK] Bank Keluar",
									"14CK" => "[CK] Klaim KPB",
									"15CC" => "[CC] Klaim C2",
									"16UC" => "[UC] Update Standard Cost",
									"17GL" => "[MM] Memorial",
									"18JP" => "[JP] Pemindah Bukuan",
								], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Perkiraan 2</label>
                        </div>
                    </div>
                    <div class="col-md-14">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? echo Select2::widget( [
									'name'          => 'perkiraan2',
									'data'          => $cmbBank,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatBank' ),
										'templateSelection' => new JsExpression( 'formatBank' ),
										'matcher'           => new JsExpression( 'matchCustom' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24 jurnalHide">Sort</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-sm-8">
								<?= Html::dropDownList( 'sort', null, [
									"Jam" => "Jam",
									"No"  => "No",
								], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
                            <label class="control-label col-sm-5">Validasi</label>
                            <div class="col-sm-11">
								<?= Html::dropDownList( 'valid', null, [
									""      => "Semua",
									"Sudah" => "Sudah",
									"Belum" => "Belum",
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
$this->registerJs( $this->render( 'JurnalUmum.js' ) );


