<?php
use kartik\checkbox\CheckboxX;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\web\JsExpression;
$this->title                   = 'Laporan Neraca Rugi Laba';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;
?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Aktiva'             => 'Aktiva',
									'Pasiva'             => 'Pasiva',
									'Laba-Rugi'          => 'Laba-Rugi',
									'Pendapatan'         => 'Pendapatan',
									'Neraca T'           => 'Neraca T',
									'Laba-Rugi Tri'      => 'Laba-Rugi Tri',
									'Pendapatan Tri'     => 'Pendapatan Tri',
									'Laba-Rugi Semester' => 'Laba-Rugi Semester',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-footer">
				<div class="container-fluid">
					<div class="col-md-2">
						<div class="form-group">
							<label class="control-label col-sm-24">Tanggal</label>
						</div>
					</div>
					<div class="col-md-22">
						<div class="form-group">
							<div class="col-md-6">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'id'            => 'tgl1_id',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format' => 'dd/mm/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
							<div class="col-md-6">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'id'            => 'tgl2_id',
										'type'          => DatePicker::TYPE_INPUT,
                                        'value'         => date('t/m/Y',strtotime("today")),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
							<label class="control-label col-sm-2">Bulan</label>
							<div class="col-md-6">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'TglKu',
										'id'            => 'TglKu_id',
										'attribute2'    =>'to_date',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => date('t/m/Y',strtotime("today")),
										'pluginOptions' => [
											'startView'=>'year',
											'minViewMode'=>'months',
											'format' => 'MM  yyyy'
                                        ],
										'pluginEvents'  => [
											"change" => new JsExpression("function(e) {
											 let d = new Date(Date.parse('01 ' + $(this).val()));											 
											 var tgl1 = $('#tgl1_id');
											 var tgl2 = $('#tgl2_id');
											  tgl1.kvDatepicker('update', d);
											  tgl2.kvDatepicker('update',new Date(d.getFullYear(), d.getMonth()+1, 0));
											}")
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
							<div class="col-sm-3">
								<? try {
									echo CheckboxX::widget([
										'name'=>'eom',
										'options'=>['id'=>'eom'],
                                        'value'=>true,
										'pluginOptions'=>['threeState'=>false, 'size'=>'lg'],
										'pluginEvents'  => [
											"change" => new JsExpression("function(e) {
											 var today = new Date();
											 var tgl2 = $('#tgl2_id');
											 var TglKu = $('#TglKu_id');
											  let tglku = TglKu.kvDatepicker('getDate');
											 if($(this).val() == 1){
											    var lastDayOfMonth = new Date(tglku.getFullYear(), tglku.getMonth()+1, 0);
											    tgl2.kvDatepicker('update', lastDayOfMonth)
											 }else{
											    tgl2.kvDatepicker('update', new Date(today.getFullYear(), today.getMonth(), today.getDate()));
											 }
											}")
										]
									]);
									echo '<label class="cbx-label" for="eom">EOM</label>';
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<div class="col-md-2">
						<div class="form-group">
							<label class="control-label col-sm-24 jurnalHide">Jenis</label>
						</div>
					</div>
					<div class="col-md-22">
						<div class="form-group">
							<div class="col-sm-6">
								<?= Html::dropDownList( 'jenis', null, [], [ 'id' => 'jenis-id', 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
							</div>
						</div>
					</div>
				</div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php

$arr = [
	'Aktiva'   => [
		'sort' => [
		'Year To Date'                     => 'Year To Date',
		'Prev Year to Date & Year to Date' => 'Prev Year to Date & Year to Date',
		],
	],
	'Pasiva'   => [
		'sort' => [
		'Year To Date'                     => 'Year To Date',
		'Prev Year to Date & Year to Date' => 'Prev Year to Date & Year to Date',
		],
	],
	'Laba-Rugi' => [
		'sort' => [
        'Prev Month & Month To Date'       => 'Prev Month & Month To Date',
        'Month To Date'                    => 'Month To Date',
		'Year To Date'                     => 'Year To Date',
		'Prev Year to Date & Year to Date' => 'Prev Year to Date & Year to Date',
		'Month To Date & Year to Date'     => 'Month To Date & Year to Date',
		],
	],
	'Pendapatan'  => [
		'sort' => [
        'Prev Month & Month To Date'       => 'Prev Month & Month To Date',
		'Month To Date'                    => 'Month To Date',
		'Year To Date'                     => 'Year To Date',
		'Prev Year to Date & Year to Date' => 'Prev Year to Date & Year to Date',
		'Month To Date & Year to Date'     => 'Month To Date & Year to Date',
		],
	],
	'Neraca T' => [
		'sort' => [
        'Prev Month & Month To Date'       => 'Prev Month & Month To Date',
        'Month To Date'                    => 'Month To Date',
		'Year To Date'                     => 'Year To Date',
		'Prev Year to Date & Year to Date' => 'Prev Year to Date & Year to Date',
		'Month To Date & Year to Date'     => 'Month To Date & Year to Date',
		],
	],        
	'Laba-Rugi Tri'  => [
		'sort' => [
        'Prev Month & Month To Date'       => 'Prev Month & Month To Date',
		'Month To Date'                    => 'Month To Date',
		'Year To Date'                     => 'Year To Date',
		'Prev Year to Date & Year to Date' => 'Prev Year to Date & Year to Date',
		'Month To Date & Year to Date'     => 'Month To Date & Year to Date',
		],
	],
	'Pendapatan Tri'  => [
		'sort' => [
        'Prev Month & Month To Date'       => 'Prev Month & Month To Date',
		'Month To Date'                    => 'Month To Date',
		'Year To Date'                     => 'Year To Date',
		'Prev Year to Date & Year to Date' => 'Prev Year to Date & Year to Date',
		'Month To Date & Year to Date'     => 'Month To Date & Year to Date',
		],
	],
	'Laba-Rugi Semester'       => [
		'sort' => [
        'Prev Month & Month To Date'       => 'Prev Month & Month To Date',
		'Month To Date'                    => 'Month To Date',
		'Year To Date'                     => 'Year To Date',
		'Prev Year to Date & Year to Date' => 'Prev Year to Date & Year to Date',
		'Month To Date & Year to Date'     => 'Month To Date & Year to Date',
		],
	],
];
$this->registerJsVar( 'setcmb', $arr );
$this->registerJs( $this->render( 'NeracaRugiLaba.js' ) );



		