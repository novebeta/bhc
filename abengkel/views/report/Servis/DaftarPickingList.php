<?php
use kartik\date\DatePicker;
use yii\helpers\Html;
$this->title                   = 'Daftar [PL] Picking List';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;
?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
        <div class="box-body">
            <div class="container-fluid">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="control-label col-sm-4">Laporan</label>
                        <div class="col-sm-20">
							<?= Html::dropDownList( 'tipe', null, [
								'Header1' => 'Header1',
								'Item1' => 'Item1',
							], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label class="control-label col-sm-4">Format</label>
                        <div class="col-sm-20">
							<?= Html::dropDownList( 'filetype', null, [
								'pdf'  => 'PDF',
								'xlsr' => 'EXCEL RAW',
								'xls'  => 'EXCEL',
								'doc'  => 'WORD',
								'rtf'  => 'RTF',
							], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
			<div class="container-fluid">
				<div class="col-md-10">
					<div class="form-group">
						<label class="control-label col-sm-4">Tanggal</label>
						<div class="col-sm-8">
							<? try {
								echo DatePicker::widget( [
									'name'          => 'tgl1',
									'type'          => DatePicker::TYPE_INPUT,
									'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
									'pluginOptions' => [
										'autoclose' => true,
										'format'    => 'dd/mm/yyyy'
									]
								] );
							} catch ( \yii\base\InvalidConfigException $e ) {
							} ?>
						</div>
						<label class="control-label col-sm-4">s/d</label>
						<div class="col-sm-8">
							<? try {
								echo DatePicker::widget( [
									'name'          => 'tgl2',
									'type'          => DatePicker::TYPE_INPUT,
									'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
									'pluginOptions' => [
										'autoclose' => true,
										'format'    => 'dd/mm/yyyy'
									]
								] );
							} catch ( \yii\base\InvalidConfigException $e ) {
							} ?>
						</div>
					</div>
				</div>
				<div class="col-md-10">
					<div class="form-group">
						<label class="control-label col-sm-4">Status</label>
						<div class="col-sm-7">
							<?= Html::dropDownList( 'status', null, [
							"" => 'Semua',
							'Menunggu' => 'Menunggu',
							'Diproses' => 'Diproses',
							'Istirahat' => 'Istirahat',
							'Selesai' => 'Selesai',
							'Lunas' => 'Lunas',
							], [ 'text' => '', 'class' => 'form-control' ] ) ?>
						</div>
						<label class="control-label col-sm-6">Cetak</label>
						<div class="col-sm-7">
							<?= Html::dropDownList( 'cetak', null, [
							"" => 'Semua',
							'Sudah' => 'Sudah',
							'Belum' => 'Belum'
							], [ 'text' => '', 'class' => 'form-control' ] ) ?>
						</div>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<div class="col-md-10">
					<div class="form-group">
						<label class="control-label col-sm-4">TC</label>
						<div class="col-sm-12">
							<?= Html::dropDownList( 'TC', null, [
							 ""      =>    "ALL",                          
							 "TC"    =>    "SD-TC11,13,14 - Daftar Servis",  
							 "TC12"  =>    "SI-TC12 - Penjualan Counter",    
							 "TC11"  =>    "TC11 - Penjualan Servis",        
							 "TC13"  =>    "TC13 - Pengeluaran eks Claim C2",
							 "TC14"  =>    "TC14 - Pengeluaran eks KPB",     
							], [ 'text' => '', 'class' => 'form-control' ] ) ?>
						</div>
						<div class="col-sm-8">
							<?= Html::dropDownList( 'ASS', null, [
							""        => "ALL",
							"ASS1"    => "ASS1",
							"ASS2"    => "ASS2",
							"ASS3"    => "ASS3",
							"ASS4"    => "ASS4",
							"CS"      => "CS",
							"HR"      => "HR",
							"LS"      => "LS",
							"LR"      => "LR",
							"JR"      => "JR",
							"CLAIMC2" => "CLAIMC2",
							"OPL"     => "OPL",
							"OR+"     => "OR+",
							"Other"   => "Other",
							], [ 'text' => '', 'class' => 'form-control' ] ) ?>
						</div>
					</div>
				</div>
				<div class="col-md-10">
					<div class="form-group">
						<label class="control-label col-sm-4">Sort</label>
						<div class="col-sm-13">
							<?= Html::dropDownList( 'Sort', null, [
							"SDPickingNo"              =>    "No Picking List", 
							"SDPickingDate"            =>    "Tgl Picking List", 
							"Cetak"                    =>    "Cetak",           
							"SDNo"                     =>    "No Service [SD]", 
							"SDTgl"                    =>    "Tgl Daftar [SD]", 
							"MotorNoPolisi"            =>    "No Polisi",       
							"CusNama"                  =>    "Pemilik",         
							"PKBNo"                    =>    "No PKB",          
							"MotorNoMesin"             =>    "No Mesin",  
							], [ 'text' => '', 'class' => 'form-control' ] ) ?>
						</div>
						<div class="col-sm-7">
							<?= Html::dropDownList( 'order', null, [
							"ASC" => 'ASC',
							'DESC' => 'DESC',
							], [ 'text' => '', 'class' => 'form-control' ] ) ?>
						</div>
					</div>
				</div>
			</div>
        </div>
		<?= Html::endForm(); ?>
        </div>
    </div>
<?php


