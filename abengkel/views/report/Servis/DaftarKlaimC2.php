<?php
use kartik\date\DatePicker;
use yii\helpers\Html;
$this->title                   = 'Daftar [CC] Klaim C2';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;
?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
        <div class="box-body">
            <div class="container-fluid">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="control-label col-sm-4">Laporan</label>
                        <div class="col-sm-20">
							<?= Html::dropDownList( 'tipe', null, [
								'Header1' => 'Header1',
								'Item1' => 'Item1',
							], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label class="control-label col-sm-4">Format</label>
                        <div class="col-sm-20">
							<?= Html::dropDownList( 'filetype', null, [
								'pdf'  => 'PDF',
								'xlsr' => 'EXCEL RAW',
								'xls'  => 'EXCEL',
								'doc'  => 'WORD',
								'rtf'  => 'RTF',
							], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
			<div class="col-md-10">
				<div class="form-group">
					<label class="control-label col-sm-4">Tanggal</label>
					<div class="col-sm-8">
						<? try {
							echo DatePicker::widget( [
								'name'          => 'tgl1',
								'type'          => DatePicker::TYPE_INPUT,
								'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
								'pluginOptions' => [
									'autoclose' => true,
									'format'    => 'dd/mm/yyyy'
								]
							] );
						} catch ( \yii\base\InvalidConfigException $e ) {
						} ?>
					</div>
					<label class="control-label col-sm-4">s/d</label>
					<div class="col-sm-8">
						<? try {
							echo DatePicker::widget( [
								'name'          => 'tgl2',
								'type'          => DatePicker::TYPE_INPUT,
								'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
								'pluginOptions' => [
									'autoclose' => true,
									'format'    => 'dd/mm/yyyy'
								]
							] );
						} catch ( \yii\base\InvalidConfigException $e ) {
						} ?>
					</div>
				</div>
			</div>
			<div class="col-md-14">
				<div class="form-group">
					<label class="control-label col-sm-2">Sort</label>
					<div class="col-sm-8">
						<?= Html::dropDownList( 'Sort', null, [
							"ttcchd.CCNo"          =>      "No Klaim C2",
							"ttcchd.CCTgl"         =>      "Tgl Klaim",  
							"ttcchd.CCNoKlaim"     =>      "No Klaim",   
							"ttcchd.NoGL"          =>      "No JT",      
							"ttcchd.CCMemo"        =>      "Keterangan", 
							"ttcchd.Cetak"         =>      "Cetak",      
							"Date(ttcchd.CCTgl)"   =>      "Tgl Klaim",  
							"CCTotalPart"          =>      "Total Part", 
							"CCTotalJasa"          =>      "Total Jasa", 
							"ttcchd.UserID"        =>      "User",                                                  
						], [ 'text' => '', 'class' => 'form-control' ] ) ?>
					</div>
					<div class="col-sm-4">
						<?= Html::dropDownList( 'order', null, [
						"ASC" => 'ASC',
						'DESC' => 'DESC',
						], [ 'text' => '', 'class' => 'form-control' ] ) ?>
					</div>
				</div>
			</div>
        </div>
		<?= Html::endForm(); ?>
        </div>
    </div>
<?php

