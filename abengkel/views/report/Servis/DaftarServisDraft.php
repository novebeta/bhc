<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
$this->title                   = 'Daftar [SD] Servis Draft';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;

$sqlPos                           = Yii::$app->db
	->createCommand( "	SELECT 'Semua' as label, '' as value, 0 as PosStatus, 0 As PosNomor FROM tdpos 
	UNION SELECT PosKode as label, PosKode as value, PosStatus, PosNomor FROM tdpos 
	ORDER BY PosStatus, PosNomor " )
	->queryAll();
$cmbPos                           = ArrayHelper::map( $sqlPos, 'value', 'label' );

?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Header1' => 'Header1',
									'Konsumen1' => 'Konsumen1',
									'Konsumen2' => 'Konsumen2',
									'Konsumen3' => 'Konsumen3',
									'Motor1' => 'Motor1',
									'Item1' => 'Item1',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<div class="container-fluid">
					<div class="col-md-9">
						<div class="form-group">
							<label class="control-label col-sm-4">Tanggal</label>
							<div class="col-sm-8">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
							<label class="control-label col-sm-4">s/d</label>
							<div class="col-sm-8">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
						</div>
					</div>
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Status</label>
							<div class="col-sm-7">
								<?= Html::dropDownList( 'status', null, [
								"'Menunggu','Diproses','Istirahat','Selesai','Lunas'" => 'Semua',
								'Selesai' => 'Selesai',
								'Lunas' => 'Lunas',
								"'Menunggu','Diproses','Istirahat'" => 'Outstanding',
								'Menunggu' => 'Menunggu',
								'Diproses' => 'Diproses',
								'Istirahat' => 'Istirahat'
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
							<div class="col-sm-1"> </div>
							<label class="control-label col-sm-5">Servis Invoice</label>
							<div class="col-sm-7">
								<?= Html::dropDownList( 'serviceinvoice', null, [
								"" => 'Semua',
								'SV' => 'Ada SV',
								'--' => 'Belum SV'
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<label class="control-label col-sm-4">Dump</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'dump', null, [
								"Semua" => 'Semua',
								'Not Dump' => '6-600 menit',
								'Dump' => 'Dump'
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<div class="col-md-9">
						<div class="form-group">
							<label class="control-label col-sm-4">TC</label>
							<div class="col-sm-12">
								<?= Html::dropDownList( 'TC', null, [
								"" => "ALL", 
								"TC11" => "TC11 - Penjualan Servis", 
								"TC13" => "TC13 - Pengeluaran eks Claim C2", 
								"TC14" => "TC14 - Pengeluaran eks KPB" 
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
							<div class="col-sm-8">
								<?= Html::dropDownList( 'ASS', null, [
								""        => "ALL",
								"ASS1"    => "ASS1",
								"ASS2"    => "ASS2",
								"ASS3"    => "ASS3",
								"ASS4"    => "ASS4",
								"CS"      => "CS",
								"HR"      => "HR",
								"LS"      => "LS",
								"LR"      => "LR",
								"JR"      => "JR",
								"CLAIMC2" => "CLAIMC2",
								"OPL"     => "OPL",
								"OR+"     => "OR+",
								"Other"   => "Other",
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Sort</label>
							<div class="col-sm-13">
								<?= Html::dropDownList( 'Sort', null, [
									"ttsdhd.SDNo"						=> "No Service [SD]" ,
									"ttsdhd.SDTgl"                     => "Tgl Daftar [SD]" ,
									"ttsdhd.SVNo"                      => "No Invoice [SV]",
									"ttsdhd.SVTgl"						=> "Tgl Invoice [SV]" ,
									"ttsdhd.MotorNoPolisi"             => "No Polisi",       
									"tdcustomer.CusNama"               =>   "Pemilik",         
									"ttsdhd.PKBNo"                     =>   "No PKB",          
									"tdcustomer.MotorNoMesin"          =>   "No Mesin",        
									"ttsdhd.SDPembawaMotor"            =>   "Pembawa Motor",   
									"ttsdhd.SDNoUrut"                  =>   "No Urut",         
									"ttsdhd.SENo"                      =>   "No Estimasi",     
									"ttsdhd.NoGLSD"                    =>   "No JT [SD]",      
									"ttsdhd.NoGLSV"                    =>   "No JT [SV]",      
									"ttsdhd.KodeTrans"                 =>   "TC",              
									"ttsdhd.SDStatus"                  =>   "Status",          
									"ttsdhd.SDJenis"                   =>   "Jenis",           
									"ttsdhd.ASS"                       =>   "KPB / ASS",       
									"ttsdhd.PrgNama"                   =>   "Program",         
									"ttsdhd.KarKode"                   =>   "Kode Karyawan",   
									"ttsdhd.SDStatus"                  =>   "Status",          
									"ttsdhd.SDKeluhan"                 =>   "Keluhan",         
									"ttsdhd.SDKeterangan"              =>   "Keterangan",      
									"ttsdhd.SDHubunganPembawa"         =>   "Hubungan Pembawa",
									"ttsdhd.SDAlasanServis"            =>   "Alasan Servis",   
									"ttsdhd.Cetak"                     =>   "Cetak",           
									"ttsdhd.KlaimKPB"                  =>   "KlaimKPB",        
									"ttsdhd.KlaimC2"                   =>   "KlaimC2",         
									"ttsdhd.UserID"                    =>   "User",            
									"ttsdhd.SDNoUrut"                  =>   "No Urut",         
									"ttsdhd.SDKmSekarang"              =>   "Km Sekarang",     
									"ttsdhd.SDKmBerikut"               =>   "Km Berikut",      
									"ttsdhd.SDTotalWaktu"              =>   "Total Waktu",     
									"ttsdhd.SDTotalJasa"               =>   "Total Jasa",      
									"ttsdhd.SDTotalQty"                =>   "Total Qty",       
									"ttsdhd.SDTotalPart"               =>   "Total Part",      
									"ttsdhd.SDTotalGratis"             =>   "Total Gratis",    
									"ttsdhd.SDTotalNonGratis"          =>   "Total NonGratis", 
									"ttsdhd.SDDiscFinal"               =>   "Disc Final",      
									"ttsdhd.SDTotalPajak"              =>   "Total Pajak",     
									"ttsdhd.SDTotalBiaya"              =>   "Total Biaya",     
									"ttsdhd.SDDurasi"                  =>   "Durasi",          
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
							<div class="col-sm-7">
								<?= Html::dropDownList( 'order', null, [
								"ASC" => 'ASC',
								'DESC' => 'DESC',
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<label class="control-label col-sm-4">POS</label>
							<div class="col-sm-20">
								<? echo Select2::widget( [
									'name'          => 'pos',
									'data'          => $cmbPos,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
								] ); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php

 