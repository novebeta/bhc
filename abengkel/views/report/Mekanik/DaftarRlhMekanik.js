jQuery(function ($) {
    $("#tipe-id").change(function () {
        var tipe = this.value;

        if (tipe === "SV SI SR Mekanik") {
            $(".jenisHide").show();
        } else {
            $(".jenisHide").hide();
        }

    });
    $('#tipe-id').trigger('change');

});

