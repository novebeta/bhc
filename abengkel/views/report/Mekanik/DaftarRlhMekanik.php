<?php
use kartik\date\DatePicker;
use yii\helpers\Html;
$this->title                   = 'Daftar Rugi Laba Harian (RLH) Mekanik';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;
?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'RLH Mekanik'       => 'RLH Mekanik',
									'RLH Mekanik Rekap' => 'RLH Mekanik Rekap',
									'SD OutStanding'    => 'SD OutStanding',
									'SV SI SR Mekanik'  => 'SV SI SR Mekanik',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-footer">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Tanggal</label>
							<div class="col-md-10">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format' => 'dd/mm/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
							<div class="col-md-10">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
						</div>
					</div>
					<div class="col-md-7 jenisHide">
						<div class="form-group">
							<label class="control-label col-sm-6">Jenis Transaksi</label>
							<div class="col-md-18">
								<?= Html::dropDownList( 'jenis', null, [
									'SV'	=> 'SV - Invoice Servis"',
									'SI'	=> 'SI - Invoice Penjualan',
									'SR'    => 'SR - Retur Penjualan',
									'%'		=> 'Semua',
								], [ 'id' => 'jenis-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
				</div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
$this->registerJs( $this->render( 'DaftarRlhMekanik.js' ) );