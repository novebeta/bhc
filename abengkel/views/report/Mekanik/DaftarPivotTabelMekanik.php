<?php
use kartik\checkbox\CheckboxX;
use kartik\date\DatePicker;
use yii\helpers\Html;
$this->title                   = 'Daftar Pivot Tabel Mekanik';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;
?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Mekanik Group Jasa' => 'Mekanik Group Jasa',
									'Mekanik Group Part' => 'Mekanik Group Part',
									'Sales Group Part' => 'Sales Group Part',
									'Mekanik Part' => 'Mekanik Part',
									'Mekanik Jasa' => 'Mekanik Jasa',
									'Sales Part' => 'Sales Part',
									'Mekanik Type Motor' => 'Mekanik Type Motor',
									'Mekanik Kategori Motor' => 'Mekanik Kategori Motor',
									'Jasa Type Motor' => 'Jasa Type Motor',
									'Jasa Kategori Motor' => 'Jasa Kategori Motor',
									'Mekanik Laba Harian' => 'Mekanik Laba Harian',
									'Mekanik Jasa Harian' => 'Mekanik Jasa Harian',
									'Mekanik Part Harian' => 'Mekanik Part Harian',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-footer">
				<div class="col-md-10">
					<div class="form-group">
						<label class="control-label col-sm-4">Tanggal</label>
						<div class="col-md-10">
							<? try {
								echo DatePicker::widget( [
									'name'          => 'tgl1',
									'type'          => DatePicker::TYPE_INPUT,
									'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
									'pluginOptions' => [
										'autoclose' => true,
										'format' => 'dd/mm/yyyy',
										'todayHighlight' => true
									]
								] );
							} catch ( \yii\base\InvalidConfigException $e ) {
							} ?>
						</div>
						<div class="col-md-10">
							<? try {
								echo DatePicker::widget( [
									'name'          => 'tgl2',
									'type'          => DatePicker::TYPE_INPUT,
									'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
									'pluginOptions' => [
										'autoclose' => true,
										'format'    => 'dd/mm/yyyy'
									]
								] );
							} catch ( \yii\base\InvalidConfigException $e ) {
							} ?>
						</div>
					</div>
				</div>
				<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-8">Jenis Transaksi</label>
							<div class="col-sm-16">
								<?= Html::dropDownList( 'jenis', null, [
									'%'  => 'Semua',
									'SV' => 'SV - Service Invoice',
									'SI'  => 'SI - Sales Invoice',
									'SR'  => 'SR - Sales Retur',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
				</div>
				<div class="col-sm-6">
					<? try {
						echo CheckboxX::widget([
							'name'=>'unitentry',
							'value' => true,
							'options'=>['id'=>'unitentry'],
							'pluginOptions'=>['threeState'=>false, 'size'=>'lg']
						]);
						echo '<label class="cbx-label" for="unitentry">Unit Entry</label>';
					} catch ( \yii\base\InvalidConfigException $e ) {
					} ?>
				</div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
