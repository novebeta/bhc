<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
$this->title                   = 'Daftar Start Stop Mekanik';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;

$sqlBank                           = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, 0 AS KarStatus, 0 as KarKode
	UNION
	SELECT KarKode AS value, KarNama AS label, KarStatus, KarKode FROM tdkaryawan 
	WHERE KarStatus = 'A' AND KarJabatan = 'Mekanik' 
	ORDER BY KarStatus, KarKode" )
	->queryAll();
$cmbBank                           = ArrayHelper::map( $sqlBank, 'value', 'label' );
//$cmbBank                           = json::encode( [ 'results' => $sql ] );
//$sqlBank                           = [ 'results' => [ 'id' => 1, 'text' => 'text1' ] ] ;

$format = <<< SCRIPT
function formatBank(result) {
    return '<div class="row">' +
           '<div class="col-md-3">' + result.id + '</div>' +
           '<div class="col-md-21">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );

?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Header1' => 'Header1',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-footer">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Tanggal</label>
							<div class="col-md-10">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format' => 'dd/mm/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
							<div class="col-md-10">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-sm-4">Mekanik</label>
							<div class="col-sm-20">
								<? echo Select2::widget( [
										'name'          => 'mekanik',
										'data'          => $cmbBank,
										'options'       => [ 'class' => 'form-control' ],
										'theme'         => Select2::THEME_DEFAULT,
										'pluginOptions' => [
											'dropdownAutoWidth' => true,
											'templateResult'    => new JsExpression( 'formatBank' ),
											'templateSelection' => new JsExpression( 'formatBank' ),
											'escapeMarkup'      => $escape,
										],
								] ); ?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-sm-4">Status</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'status', null, [
								"" => 'Semua',
								"Sudah" => 'Sudah',
								"Belum" => 'Belum'
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
				</div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
