$("#tipe-id").change(function () {
    var tipe = this.value;
    var option = $('#sort-id');
    option.find('option').remove().end();
    for (let val in setcmb[tipe].sort) {
        option.append('<option value="' + val + '">' + setcmb[tipe].sort[val] + '</option>');
    }
    option.prop("selectedIndex", 0);
    var option1 = $('#kode1-id');
    var option2 = $('#kode2-id');
    option1.find('option').remove().end();
    option2.find('option').remove().end();
    option1.append('<option value="">Semua</option>');
    option2.append('<option value="">Semua</option>');
    for (let val in setcmb[tipe].cmb) {
        option1.append('<option value="' + setcmb[tipe].cmb[val].value + '">' + setcmb[tipe].cmb[val].label + '</option>');
        option2.append('<option value="' + setcmb[tipe].cmb[val].value + '">' + setcmb[tipe].cmb[val].label + '</option>');
    }
    option1.prop("selectedIndex", 0);
    option2.prop("selectedIndex", 0);
    fillKonsumen();
});

function fillKonsumen() {
    let tipe = $("#tipe-id").val();
    if( tipe !== 'Konsumen1' && tipe !== 'Konsumen2' && tipe !== 'Konsumen3') return;
    $.ajax({
        type: 'POST',
        url: '<?=$urlCombo?>',
        data: {
            'from': $('#tgl1').kvDatepicker('getDate').toISOString().slice(0, 10),
            'to': $('#tgl2').kvDatepicker('getDate').toISOString().slice(0, 10),
        }
    }).then(function (cusData) {
        var option1 = $('#kode1-id');
        var option2 = $('#kode2-id');
        option1.empty();
        option2.empty();
        option1.append('<option value="">Semua</option>');
        option2.append('<option value="">Semua</option>');
        cusData.forEach(function(key,index) {
            option1.append('<option value="'+cusData[index].value+'">'+cusData[index].label+'</option>');
            option2.append('<option value="'+cusData[index].value+'">'+cusData[index].label+'</option>');
        });
    });
}
window.fillKonsumen = fillKonsumen;
$("#tipe-id").trigger('change');