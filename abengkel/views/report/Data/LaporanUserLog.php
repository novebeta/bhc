<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
$this->title                   = 'User Log';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;

$sqlUser = Yii::$app->db
	->createCommand( "
	SELECT 'Semua' AS label, '%' AS value ,  '' AS UserID
	UNION
	SELECT UserNama As label,  UserID AS value, UserID FROM tuser  
	ORDER BY UserID " )
	->queryAll();
$cmbUser = ArrayHelper::map( $sqlUser, 'value', 'label' );

$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
function formatDual(result) {
    return '<div class="row">' +
           '<div class="col-md-8">' + result.id + '</div>' +
           '<div class="col-md-16">' + result.text + '</div>' +
           '</div>';
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );

?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'User Log'  => 'User Log',
									'User Log DB'  => 'User Log DB',
									'Login'  => 'Login',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-footer">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-5">Tanggal</label>
							<div class="col-sm-8">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
							<label class="control-label col-sm-3">s/d</label>
							<div class="col-sm-8">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="form-group">
							<label class="control-label col-sm-4">Jenis</label>
							<div class="col-sm-13">
								<?= Html::dropDownList( 'jenis', null, [
								""		 => "Semua",  
								"Tambah" => "Tambah", 
								"Edit"   => "Edit",   
								"Hapus"  => "Hapus",  
								"Log"    => "Login",  
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-5">Nama Trans</label>
							<div class="col-sm-19">
								<?= Html::dropDownList( 'namatrans', null, [
									""							=>  "Semua",                     
									"ttsd"                      =>  "SD] Daftar Servis",        
									"ttsd"                      =>  "[SV] Invoice Servis ",      
									"ttsi"                      =>  "[SI] Invoice Penjualan",    
									"ttsr"                      =>  "[SR] Retur Penjualan",      
									"ttps"                      =>  "[PS] Penerimaan Barang",    
									"ttpi"                      =>  "[PI] Invoice Pembelian",    
									"ttpr"                      =>  "[PR] Retur Pembelian",      
									"ttti"                      =>  "[TI] Transfer Item",        
									"ttta"                      =>  "[TA] Transfer Adjustment",  
									"ttkm"                      =>  "[KM] Kas Masuk",            
									"ttkk"                      =>  "[KK] Kas Keluar",           
									"ttbm"                      =>  "[BM] Bank Masuk",           
									"ttbk"                      =>  "[BK] Bank Keluar",          
									"ttck"                      =>  "[CK] Klaim KPB",            
									"ttcc"                      =>  "[CC] Klaim C2",             
									"ttuc"                      =>  "[UC] Update Standard Cost", 
									"ttgeneralledger"           =>  "Jurnal",                    
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="form-group">
							<label class="control-label col-sm-4">No Trans</label>
                            <div class="col-sm-20">
								<input type="text" id="notrans" name="notrans" class="form-control"> </input>
							</div>
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-5">User ID</label>
                             <div class="col-sm-19">
								<? echo Select2::widget( [
									'name'          => 'userid',
									'data'          => $cmbUser,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatDual' ),
										'templateSelection' => new JsExpression( 'formatDual' ),
										'matcher'           => new JsExpression( 'matchCustom' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
                            </div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="form-group">
							<label class="control-label col-sm-4">Sort</label>
							<div class="col-sm-13">
								<?= Html::dropDownList( 'sort', null, [
								"LogTglJam" => "Tanggal",        
								"UserID"    => "UserID",         
								"LogJenis"  => "Jenis Proses",   
								"LogNama"   => "Nama Transaksi", 
								"NoTrans"   => "No Transaksi",   
								"LogTabel"  => "Tabel Database", 
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
							<div class="col-sm-7">
								<?= Html::dropDownList( 'order', null, [
								"ASC" => 'ASC',
								'DESC' => 'DESC',
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php


