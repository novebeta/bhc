<?php
use abengkel\models\Tdahass;
use abengkel\models\Tdarea;
use abengkel\models\Tdcustomer;
use abengkel\models\Tddealer;
use abengkel\models\Tdhass;
use abengkel\models\Tdjasa;
use abengkel\models\Tdjasagroup;
use abengkel\models\Tdkaryawan;
use abengkel\models\Tdlokasi;
use abengkel\models\Tdmotortype;
use abengkel\models\Tdpos;
use abengkel\models\Tdsupplier;
use abengkel\models\Tuakses;
use abengkel\models\Tuser;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\web\JsExpression;
$this->title                     = 'Laporan Data';
$this->params[ 'breadcrumbs' ][] = $this->title;
$formatter                       = \Yii::$app->formatter;
$from                            = Yii::$app->formatter->asDate( 'now', '01/MM/yyyy' );
$to                              = Yii::$app->formatter->asDate( 'now', 'dd/MM/yyyy' );
?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
            <div class="box-body">
                <div class="container-fluid">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Laporan</label>
                            <div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Type Motor' => 'Type Motor',
									'Supplier'   => 'Supplier',
									'Dealer'     => 'Dealer',
									'Area'       => 'Area',
									'User'       => 'User',
									'AHASS'      => 'AHASS',
									'Hak Akses'  => 'Hak Akses',
									'Jasa'       => 'Jasa',
									'Jasa Group' => 'Jasa Group',
									'Lokasi'     => 'Lokasi',
									'POS'        => 'POS',
									'Karyawan1'  => 'Karyawan1',
									'Karyawan2'  => 'Karyawan2',
									'Konsumen1'  => 'Konsumen1',
									'Konsumen2'  => 'Konsumen2',
									'Konsumen3'  => 'Konsumen3',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Format</label>
                            <div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="container-fluid">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Kode 1</label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="col-sm-24">
								<?= Html::dropDownList( 'kode1', null, [],
									[ 'id' => 'kode1-id', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-sm-8">Tanggal</label>
                            <div class="col-sm-16">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'id'            => 'tgl1',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => $from,
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										],
										'pluginEvents'  => [
											"change" => new JsExpression("function(e) {
											 window.fillKonsumen();
											}")
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="col-md-2">
                            </div>
                            <label class="control-label col-sm-6">s/d</label>
                            <div class="col-sm-16">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'id'            => 'tgl2',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => $to,
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										],
										'pluginEvents'  => [
											"change" => new JsExpression("function(e) {
											 window.fillKonsumen();
											}")
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-sm-6">Status</label>
                            <div class="col-sm-18">
								<?= Html::dropDownList( 'status', null, [
									''  => 'Semua',
									'A' => 'Aktif',
									'N' => 'Non Aktif',
								], [ 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Kode 2</label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="col-sm-24">
								<?= Html::dropDownList( 'kode2', null, [],
									[ 'id' => 'kode2-id', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Sort</label>
                            <div class="col-sm-14">
								<?= Html::dropDownList( 'sort', null, [], [ 'id' => 'sort-id', 'text' => '', 'class' => 'form-control' ] ) ?>
                            </div>
                            <div class="col-sm-6">
								<?= Html::dropDownList( 'order', null, [
									"ASC"  => 'ASC',
									'DESC' => 'DESC',
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
$arr      = [
	'Type Motor' => [
		'cmb'  => Tdmotortype::find()->combo(),
		'sort' => [
			"MotorType"     => "Type",
			"MotorNama"     => "Nama",
			"MotorKategori" => "Kategori",
			"MotorCC"       => "CC",
			"MotorStatus"   => "Status",
			"MotorNoMesin"  => "No Mesin",
			"MotorNoRangka" => "No Rangka",
		],
	],
	'Supplier'   => [
		'cmb'  => Tdsupplier::find()->combo(),
		'sort' => [
			"SupKode"       => "Kode",
			"SupNama"       => "Nama",
			"SupContact"    => "Kontak",
			"SupAlamat"     => "Alamat",
			"SupKota"       => "Kota",
			"SupTelepon"    => "Telepon",
			"SupFax"        => "Fax",
			"SupEmail"      => "Email",
			"SupKeterangan" => "Keterangan",
			"SupStatus"     => "Status",
		],
	],
	'Dealer'     => [
		'cmb'  => Tddealer::find()->combo(),
		'sort' => [
			"DealerKode"       => "Kode",
			"DealerNama"       => "Nama",
			"DealerContact"    => "Kontak",
			"DealerAlamat"     => "Alamat",
			"DealerKota"       => "Kota",
			"DealerProvinsi"   => "Provinsi",
			"DealerTelepon"    => "Telepon",
			"DealerFax"        => "Fax",
			"DealerEmail"      => "Email",
			"DealerKeterangan" => "Keterangan",
			"DealerStatus"     => "Status",
			"DealerKodeDAstra" => "Kode Astra",
			"DealerKodeDMain"  => "Main Dealer",
		],
	],
	'Area'       => [
		'cmb'  => Tdarea::find()->combo(),
		'sort' => [
			"Provinsi"       => "Provinsi",
			"Kabupaten"      => "Kabupaten",
			"Kecamatan"      => "Kecamatan",
			"Kelurahan"      => "Kelurahan",
			"KodePos"        => "KodePos",
			"AreaStatus"     => "Status",
			"ProvinsiAstra"  => "Provinsi Astra",
			"KabupatenAstra" => "Kabupaten Astra",
		],
	],
	'User'       => [
		'cmb'  => Tuser::find()->combo(),
		'sort' => [
			"UserID"         => "User ID",
			"UserPass"       => "Pasword",
			"KodeAkses"      => "Kode Akses",
			"UserNama"       => "User Nama",
			"UserAlamat"     => "Alamat",
			"UserTelepon"    => "Telepon",
			"UserKeterangan" => "Keterangan",
			"LokasiKode"     => "Lokasi",
		],
	],
	'AHASS'      => [
		'cmb'  => Tdahass::find()->combo(),
		'sort' => [
			"AHASSNomor"        => "AHASSNomor",
			"AHASSNama"         => "Nama",
			"AHASSAlamat"       => "Alamat",
			"AHASSTelepon"      => "Telepon",
			"AHASSKontak"       => "Kontak",
			"AHASSJenisLayanan" => "Jenis Layanan",
			"AHASSJenisDealer"  => "Jenis Dealer",
			"AHASSProvinsi"     => "Provinsi",
			"AHASSKecamatan"    => "Kecamatan",
			"AHASSKabupaten"    => "Kabupaten",
			"AHASSMainDealer"   => "Main Dealer",
			"AHASSStatus"       => "Status",
		],
	],
	'Hak Akses'  => [
		'cmb'  => Tuakses::find()->combo(),
		'sort' => [
			"KodeAkses"  => "KodeAkses",
			"MenuInduk"  => "Indek",
			"MenuText"   => "Text",
			"MenuNo"     => "No",
			"MenuStatus" => "Status",
			"MenuTambah" => "Tambah",
			"MenuEdit"   => "Edit",
			"MenuHapus"  => "Hapus",
			"MenuCetak"  => "Cetak",
			"MenuView"   => "View",
		],
	],
	'Jasa'       => [
		'cmb'  => Tdjasa::find()->combo(),
		'sort' => [
			"JasaKode"            => "Kode",
			"JasaNama"            => "Nama",
			"JasaGroup"           => "Group",
			"JasaHrgBeli"         => "Harga Beli",
			"JasaHrgJual"         => "Harga Jual",
			"JasaWaktuKerja"      => "Waktu Kerja",
			"JasaWaktuSatuan"     => "Satuan Waktu",
			"JasaWaktuKerjaMenit" => "Waktu Menit",
			"JasaStatus"          => "Status",
			"JasaKeterangan"      => "Keterangan",
		],
	],
	'Jasa Group' => [
		'cmb'  => Tdjasagroup::find()->combo(),
		'sort' => [
			"JasaGroup"     => "Group",
			"JasaGroupNama" => "Nama",
		],
	],
	'Lokasi'     => [
		'cmb'  => Tdlokasi::find()->combo(),
		'sort' => [
			"LokasiNomor"   => "Nomor",
			"LokasiKode"    => "Kode",
			"LokasiNama"    => "Nama",
			"KarKode"       => "Karyawan",
			"LokasiAlamat"  => "Alamat",
			"LokasiStatus"  => "Status",
			"LokasiTelepon" => "Telepon",
			"LokasiJenis"   => "Jenis",
			"LokasiInduk"   => "Induk",
		],
	],
	'POS'        => [
		'cmb'  => Tdpos::find()->combo(),
		'sort' => [
			"PosNomor"   => "Nomor",
			"PosKode"    => "Kode",
			"PosNama"    => "Nama",
			"PosAlamat"  => "Alamat",
			"PosTelepon" => "Telepon",
			"KarKode"    => "Karyawan",
			"PosStatus"  => "Status",
			"PosJenis"   => "Jenis",
			"NoAccount"  => "No Account",
		],
	],
	'Karyawan1'  => [
		'cmb'  => Tdkaryawan::find()->combo(),
		'sort' => [
			"KarKode"      => "Kode",
			"KarNama"      => "Nama",
			"KarAlamat"    => "Alamat",
			"KarKTP"       => "KTP",
			"KarRT"        => "RT",
			"KarRW"        => "RW",
			"KarProvinsi"  => "Provinsi",
			"KarKabupaten" => "Kabupaten",
			"KarKecamatan" => "Kecamatan",
			"KarKelurahan" => "Kelurahan",
			"KarKodePos"   => "Kode POS",
			"KarTelepon"   => "Telepon",
			"KarEmail"     => "Email",
			"KarSex"       => "Sex",
			"KarTempatLhr" => "Tempat Lahir",
			"KarTglLhr"    => "Tanggal Lahir",
			"KarTglMasuk"  => "Tanggal Masuk",
			"KarTglKeluar" => "Tanggal Keluar",
			"KarAgama"     => "Agama",
			"KarJabatan"   => "Jabatan",
			"KarStatus"    => "Status",
			"KarPassword"  => "Password",
		],
	],
	'Karyawan2'  => [
		'cmb'  => Tdkaryawan::find()->combo(),
		'sort' => [
			"KarKode"      => "Kode",
			"KarNama"      => "Nama",
			"KarAlamat"    => "Alamat",
			"KarKTP"       => "KTP",
			"KarRT"        => "RT",
			"KarRW"        => "RW",
			"KarProvinsi"  => "Provinsi",
			"KarKabupaten" => "Kabupaten",
			"KarKecamatan" => "Kecamatan",
			"KarKelurahan" => "Kelurahan",
			"KarKodePos"   => "Kode POS",
			"KarTelepon"   => "Telepon",
			"KarEmail"     => "Email",
			"KarSex"       => "Sex",
			"KarTempatLhr" => "Tempat Lahir",
			"KarTglLhr"    => "Tanggal Lahir",
			"KarTglMasuk"  => "Tanggal Masuk",
			"KarTglKeluar" => "Tanggal Keluar",
			"KarAgama"     => "Agama",
			"KarJabatan"   => "Jabatan",
			"KarStatus"    => "Status",
			"KarPassword"  => "Password",
		],
	],
	'Konsumen1'  => [
		'cmb'  => Tdcustomer::find()->combo( [ 'from' => $from, 'to' => $to ] ),
		'sort' => [
			"MotorNoPolisi"  => "NoPolisi",
			"MotorNoMesin"   => "NoMesin",
			"MotorNoRangka"  => "NoRangka",
			"CusKTP"         => "KTP",
			"CusNama"        => "Nama",
			"CusAlamat"      => "Alamat",
			"CusRT"          => "RT",
			"CusRW"          => "RW",
			"CusProvinsi"    => "Provinsi",
			"CusKabupaten"   => "Kabupaten",
			"CusKecamatan"   => "Kecamatan",
			"CusKelurahan"   => "Kelurahan",
			"CusKodePos"     => "Kode Pos",
			"CusTelepon"     => "Telepon",
			"CusSex"         => "Sec",
			"CusTempatLhr"   => "Tempat Lahir",
			"CusTglLhr"      => "Tanggal Lahir",
			"CusAgama"       => "Agama",
			"CusPekerjaan"   => "Pekerjaan",
			"CusEmail"       => "Email",
			"CusKeterangan"  => "Keterangan",
			"CusKK"          => "KK",
			"CusStatus"      => "Status",
			"MotorType"      => "Type Motor",
			"MotorWarna"     => "Warna Motor",
			"MotorTahun"     => "Tahun Motor",
			"MotorNama"      => "Nama Motor",
			"MotorKategori"  => "Kategori Motor",
			"MotorCC"        => "CC",
			"TglAkhirServis" => "Tanggal Akhir Servis",
			"AHASSNomor"     => "No AHASS",
			"TglBeli"        => "Tanggal Beli",
			"NoBukuServis"   => "Buku Servis",
		],
	],
	'Konsumen2'  => [
		'cmb'  => Tdcustomer::find()->combo( [ 'from' => $from, 'to' => $to ] ),
		'sort' => [
			"MotorNoPolisi"  => "NoPolisi",
			"MotorNoMesin"   => "NoMesin",
			"MotorNoRangka"  => "NoRangka",
			"CusKTP"         => "KTP",
			"CusNama"        => "Nama",
			"CusAlamat"      => "Alamat",
			"CusRT"          => "RT",
			"CusRW"          => "RW",
			"CusProvinsi"    => "Provinsi",
			"CusKabupaten"   => "Kabupaten",
			"CusKecamatan"   => "Kecamatan",
			"CusKelurahan"   => "Kelurahan",
			"CusKodePos"     => "Kode Pos",
			"CusTelepon"     => "Telepon",
			"CusSex"         => "Sec",
			"CusTempatLhr"   => "Tempat Lahir",
			"CusTglLhr"      => "Tanggal Lahir",
			"CusAgama"       => "Agama",
			"CusPekerjaan"   => "Pekerjaan",
			"CusEmail"       => "Email",
			"CusKeterangan"  => "Keterangan",
			"CusKK"          => "KK",
			"CusStatus"      => "Status",
			"MotorType"      => "Type Motor",
			"MotorWarna"     => "Warna Motor",
			"MotorTahun"     => "Tahun Motor",
			"MotorNama"      => "Nama Motor",
			"MotorKategori"  => "Kategori Motor",
			"MotorCC"        => "CC",
			"TglAkhirServis" => "Tanggal Akhir Servis",
			"AHASSNomor"     => "No AHASS",
			"TglBeli"        => "Tanggal Beli",
			"NoBukuServis"   => "Buku Servis",
		],
	],
	'Konsumen3'  => [
		'cmb'  => Tdcustomer::find()->combo( [ 'from' => $from, 'to' => $to ] ),
		'sort' => [
			"MotorNoPolisi"  => "NoPolisi",
			"MotorNoMesin"   => "NoMesin",
			"MotorNoRangka"  => "NoRangka",
			"CusKTP"         => "KTP",
			"CusNama"        => "Nama",
			"CusAlamat"      => "Alamat",
			"CusRT"          => "RT",
			"CusRW"          => "RW",
			"CusProvinsi"    => "Provinsi",
			"CusKabupaten"   => "Kabupaten",
			"CusKecamatan"   => "Kecamatan",
			"CusKelurahan"   => "Kelurahan",
			"CusKodePos"     => "Kode Pos",
			"CusTelepon"     => "Telepon",
			"CusSex"         => "Sec",
			"CusTempatLhr"   => "Tempat Lahir",
			"CusTglLhr"      => "Tanggal Lahir",
			"CusAgama"       => "Agama",
			"CusPekerjaan"   => "Pekerjaan",
			"CusEmail"       => "Email",
			"CusKeterangan"  => "Keterangan",
			"CusKK"          => "KK",
			"CusStatus"      => "Status",
			"MotorType"      => "Type Motor",
			"MotorWarna"     => "Warna Motor",
			"MotorTahun"     => "Tahun Motor",
			"MotorNama"      => "Nama Motor",
			"MotorKategori"  => "Kategori Motor",
			"MotorCC"        => "CC",
			"TglAkhirServis" => "Tanggal Akhir Servis",
			"AHASSNomor"     => "No AHASS",
			"TglBeli"        => "Tanggal Beli",
			"NoBukuServis"   => "Buku Servis",
		],
	],
];
$urlCombo = \yii\helpers\Url::toRoute( [ 'tdcustomer/find-combo' ] );
$this->registerJsVar( 'setcmb', $arr );
$this->registerJs( $this->render( 'LaporanData.js', [
	'urlCombo' => $urlCombo
] ) );