<?php

use abengkel\components\Menu;
use yii\helpers\Html;
\abengkel\assets\AppAsset::register( $this );
\abengkel\assets\JqwidgetsAsset::register( $this );
$this->title                     = 'Laporan Query';
$this->params[ 'breadcrumbs' ][] = $this->title;
$formatter                       = \Yii::$app->formatter;
$this->registerCss( ".jqx-widget-content { font-size: 12px; }" );
?>
    <div class="laporan-query col-md-24" style="padding-left: 0;">
        <div class="box box-default">
			<?= Html::beginForm(); ?>
            <div class="box-body" id='jqxWidget'>
                <div id="jqxgrid">
                </div>
            </div>
            <div class="box-footer">
                <div class="container-fluid">
                    <div class="form-group col-sm-18">
                        <textarea class="form-control" id="sqlraw" rows="4">SELECT * FROM ttsdhd WHERE SDTgl BETWEEN DATE_ADD(NOW(), INTERVAL -30 DAY) AND NOW();
						</textarea>
                    </div>
                    <div class="form-group col-sm-6">
                        <input type="file" id="file-input" class="hidden"/>
                        <button class="btn btn-primary" id='btnLoad' type="button" style="width: 80px;height: 64px"><i class="fa fa-folder-open fa-lg"></i>&nbsp Load</button>
                        <button class="btn btn-danger" id='btnTampil' type="button" style="width: 80px;height: 64px"><i class="fa fa-th fa-lg"></i>&nbsp Tampil</button>
                        <button class="btn btn-success" id='btnExcel' type="button" style="width: 80px;height: 64px"><i class="fa fa-file-excel-o fa-lg"></i>&nbsp Excel</button>
                        <button class="btn btn-success" id='btnTsv' type="button" style="width: 80px;height: 64px"><i class="fa fa-file-excel-o fa-lg"></i>&nbsp TSV</button>
                    </div>
                </div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
$urlData = \yii\helpers\Url::toRoute( [ 'report/query' ] );
$this->registerJsVar('isAdminOrAuditor',Menu::showOnlyHakAkses(['Admin','Auditor','GM','Korwil']));
$this->registerJs( <<< JS
jQuery(function ($) {
     var json = '[{ "columns": [] }, {"rows" : []}]';
            var obj = $.parseJSON(json);
            var columns = obj[0].columns;
            var datafields = new Array();
            for (var i = 0; i < columns.length; i++) {
                datafields.push({ name: columns[i].datafield });
            }
            var rows = obj[1].rows;
            // prepare the data
            var source =
            {
                datatype: "json",
                datafields: datafields,
                id: 'id',
                localdata: rows
            };
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#jqxgrid").jqxGrid(            {
                width: '100%',
                rowsheight: 21,
                height: 300,
                source: dataAdapter,
                columnsresize: true,
                columns: columns
            });
            function readSingleFile(e) {
              //  console.log(e);
              var file = e.target.files[0];
              if (!file) {
                return;
              }
              var reader = new FileReader();
              reader.onload = function(e) {
                var contents = e.target.result;
                $('#sqlraw').val(contents);
              };
              reader.readAsText(file);
            }
            document.getElementById('file-input').addEventListener('change', readSingleFile, false);
            $('#btnLoad').click(function() {
                $('#file-input').trigger('click');
            });
            $('#btnTampil').click(function() {
                var sqlraw = $('#sqlraw').val();
                //SELECT atau CALL di bebaskan saja
                if(!isAdminOrAuditor){
                //     if(!sqlraw.toLowerCase().includes("SELECT") || !sqlraw.toLowerCase().includes("CALL")){
                //         alert('Query harus mengandung kata SELECT atau CALL');
                //         return;
                //     }
                    if(sqlraw.toLowerCase().includes("update") || sqlraw.toLowerCase().includes("delete")){
                        alert('Anda tidak dapat  menggunakan perintah query tersebut');
                        return;
                    }
                }
                $.ajax({
                        type: 'POST',
                        url: '$urlData',
                        data: {
                            q:btoa($('#sqlraw').val())
                        }
                    }).then(function (json) {
                        // var json = '[{ "columns": [] }, {"rows" : []}]';
                        var obj = json;
                        var columns = obj.columns;
                        var datafields = new Array();
                        columns.unshift({
                              text: '#', sortable: false, filterable: false, editable: false,
                              groupable: false, draggable: false, resizable: false,
                              datafields: '', columntype: 'number', width: 50,
                              cellsrenderer: function (row, column, value) {
                                  return "<div style='margin:4px;'>" + (value + 1) + "</div>";
                              }
                          });
                        datafields.push({name:'rowindex'});
                        for (var i = 0; i < columns.length; i++) {
                            datafields.push({ name: columns[i].datafield });
                        }
                        var rows = obj.rows;
                        // prepare the data
                        var source =
                        {
                            datatype: "json",
                            datafields: datafields,
                            id: 'id',
                            localdata: rows
                        };
                        var dataAdapter = new $.jqx.dataAdapter(source);
                        $("#jqxgrid").jqxGrid(                        {
                            width: '100%',
                            rowsheight: 21,
                            height: 330,
                            source: dataAdapter,
                            columnsresize: true,
                            columns: columns
                        });
                         $("#jqxgrid").jqxGrid('autoresizecolumns');
                    });
            });
             $("#btnExcel").click(function () {
                $("#jqxgrid").jqxGrid('exportdata', 'xlsx', 'LaporanQuery');
            });
             $("#btnTsv").click(function () {
                $("#jqxgrid").jqxGrid('exportdata', 'tsv', 'LaporanQuery');
            });
});
JS
);

