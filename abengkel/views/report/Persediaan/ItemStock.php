<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
$this->title                     = 'Item Stock';
$this->params[ 'breadcrumbs' ][] = $this->title;
$formatter                       = \Yii::$app->formatter;
$sqlBank = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, 0 AS StatusAccount,  '' AS NoAccount
	UNION
	SELECT NoAccount AS value, NamaAccount AS label, StatusAccount, NoAccount AS NoAccount FROM traccount 
	WHERE StatusAccount = 'A' AND JenisAccount = 'Detail' 
	ORDER BY StatusAccount, NoAccount" )
	->queryAll();
$cmbBank = ArrayHelper::map( $sqlBank, 'value', 'label' );
$sqlBrgGroup = Yii::$app->db
	->createCommand( "
	SELECT 'Semua' AS label, '%' AS value, '' AS BrgGroup
	UNION
	SELECT 'Non Oli' AS label, 'r' AS value, 'z' AS BrgGroup
	UNION
	SELECT BrgGroup AS label, BrgGroup AS value, BrgGroup FROM tdbarang 
	GROUP BY BrgGroup
	ORDER BY BrgGroup " )
	->queryAll();
$cmbBrgGroup = ArrayHelper::map( $sqlBrgGroup, 'value', 'label' );
$sqlLokasi = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, 0 AS LokasiStatus,  '0' AS LokasiNomor, '1' AS LokasiKode
	UNION
	SELECT LokasiKode AS value, LokasiNama AS label, LokasiStatus, LokasiNomor, LokasiKode AS LokasiKode FROM tdlokasi 
	ORDER BY LokasiStatus, LokasiNomor, LokasiKode" )
	->queryAll();
$cmbLokasi = ArrayHelper::map( $sqlLokasi, 'value', 'label' );
$sqlBarang = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, '' AS BrgStatus,  '' AS BrgKode
	UNION
	SELECT BrgKode AS value, BrgNama AS label, BrgStatus, BrgKode FROM tdbarang 
	ORDER BY BrgStatus, BrgKode" )
	->queryAll();
$cmbBarang = ArrayHelper::map( $sqlBarang, 'value', 'label' );
$format = <<< SCRIPT
function formatBank(result) {
    return '<div class="row">' +
           '<div class="col-md-8">' + result.id + '</div>' +
           '<div class="col-md-16">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
            <div class="box-body">
                <div class="container-fluid">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Laporan</label>
                            <div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Stock Ringkas'        => 'Stock Ringkas',
									'Stock Ringkas Nilai'  => 'Stock Ringkas Nilai',
									'Stock Detail'         => 'Stock Detail',
									'Stock Lokasi Ringkas' => 'Stock Lokasi Ringkas',
									'Stock Lokasi Detail'  => 'Stock Lokasi Detail',
									'Stock Lokasi Nilai'   => 'Stock Lokasi Nilai',
                                    'Stock Akuntansi'      => 'Stock Akuntansi',
                                    'Stock FIFO'            => 'Stock FIFO',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Format</label>
                            <div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="container-fluid">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Lokasi</label>
                        </div>
                    </div>
                    <div class="col-md-17">
                        <div class="form-group">
                            <div class="col-sm-12">
								<? echo Select2::widget( [
									'name'          => 'lokasi',
									'data'          => $cmbLokasi,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatBank' ),
										'templateSelection' => new JsExpression( 'formatBank' ),
										'escapeMarkup'      => $escape,
										'matcher'           => new JsExpression( 'matchCustom' ),
									],
								] ); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Tanggal</label>
                        </div>
                    </div>
                    <div class="col-md-17">
                        <div class="col-md-6">
							<? try {
								echo DatePicker::widget( [
									'name'          => 'tgl1',
									'type'          => DatePicker::TYPE_INPUT,
									'value'         => Yii::$app->formatter->asDate( 'now', 'dd/MM/yyyy' ),
									'pluginOptions' => [
										'autoclose'      => true,
										'format'         => 'dd/mm/yyyy',
										'todayHighlight' => true
									]
								] );
							} catch ( \yii\base\InvalidConfigException $e ) {
							} ?>
                        </div>
                        <div class="col-md-6">
							<? try {
								echo DatePicker::widget( [
									'name'          => 'tgl2',
									'type'          => DatePicker::TYPE_INPUT,
									'value'         => Yii::$app->formatter->asDate( 'now', 'dd/MM/yyyy' ),
									'pluginOptions' => [
										'autoclose' => true,
										'format'    => 'dd/mm/yyyy'
									]
								] );
							} catch ( \yii\base\InvalidConfigException $e ) {
							} ?>
                        </div>
                        <label class="control-label col-sm-2">Sort</label>
                        <div class="col-sm-7">
							<?= Html::dropDownList( 'sort', null, [
								"BrgKode" => "Kode Barang",
								"BrgNama" => "Nama Barang",
								"IN"      => "IN",
								"OUT"     => "OUT",
							], [ 'text' => '', 'class' => 'form-control' ] ) ?>
                        </div>
                        <div class="col-sm-3">
							<?= Html::dropDownList( 'order', null, [
								"ASC"  => 'ASC',
								'DESC' => 'DESC',
							], [ 'text' => '', 'class' => 'form-control' ] ) ?>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label col-sm-6">Status</label>
                            <div class="col-sm-18">
								<?= Html::dropDownList( 'status', null, [
									"A" => "Aktif",
									"N" => "Non Aktif",
									""  => "Semua",
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Jenis</label>
                        </div>
                    </div>
                    <div class="col-md-17">
                        <div class="form-group">
                            <div class="col-sm-9">
								<? echo Select2::widget( [
									'name'         => 'BrgGroup',
									'data'         => $cmbBrgGroup,
									'options'      => [ 'class' => 'form-control' ],
									'theme'        => Select2::THEME_DEFAULT,
									'pluginEvents' => [
										"change" => new JsExpression( "function(e) {
										    var where = 'BrgGroup = :BrgGroup';
										    var grup = this.value;	
										    if(this.value === 'r') {
										        where = 'BrgGroup <> :BrgGroup';
										        grup = 'Oli';
										    } 
											$('#barang').utilSelect2().loadRemote('" . \yii\helpers\Url::toRoute(['tdbarang/find']) . "', 
											{  mode: 'combo', value: 'BrgKode', label: ['BrgNama'], condition : where, params: {':BrgGroup':grup}, allOption:true }, true);
											}" )
									]
								] ); ?>
                            </div>
                            <label class="control-label col-sm-3">Barang</label>
                            <div class="col-sm-12">
								<? echo Select2::widget( [
									'name'          => 'barang',
									'id'            => 'barang',
									'data'          => $cmbBarang,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatBank' ),
										'templateSelection' => new JsExpression( 'formatBank' ),
										'matcher'           => new JsExpression( 'matchCustom' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label col-sm-6">Stock</label>
                            <div class="col-sm-18">
								<?= Html::dropDownList( 'stock', null, [
									"Semua"     => "Semua",
									"Transaksi" => "Transaksi",
									"Idle"      => "Idle",
									"Nol"       => "Nol",
									"IN"        => "IN",
									"OUT"       => "OUT",
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
