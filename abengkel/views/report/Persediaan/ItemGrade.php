<?php
use kartik\date\DatePicker;
use kartik\checkbox\CheckboxX;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
$this->title                   = 'Item Grade';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;


$sqlBrgGroup = Yii::$app->db
	->createCommand( "
	SELECT 'Semua' AS label, '%' AS value, '0' AS BrgGroup
	UNION
	SELECT BrgGroup AS label, BrgGroup AS value, BrgGroup FROM tdbarang 
	GROUP BY BrgGroup
	UNION
	SELECT 'Non Oli' AS label, 'r' AS value, 'z' AS BrgGroup
	ORDER BY BrgGroup " )
	->queryAll();
$cmbBrgGroup = ArrayHelper::map( $sqlBrgGroup, 'value', 'label' );

$sqlLokasi   = Yii::$app->db
	->createCommand( "
	SELECT LokasiKode AS value, LokasiNama AS label, LokasiStatus, LokasiNomor, LokasiKode AS LokasiKode FROM tdlokasi 
	UNION
	SELECT '%' AS value, 'Semua' AS label, 'x' AS LokasiStatus,  'x' AS LokasiNomor, 'x' AS LokasiKode
	ORDER BY LokasiStatus, LokasiNomor, LokasiKode" )
	->queryAll();
$cmbLokasi   = ArrayHelper::map( $sqlLokasi, 'value', 'label' );

$sqlBarang   = Yii::$app->db
	->createCommand( "
	SELECT BrgKode AS value, BrgNama AS label, BrgStatus, BrgKode FROM tdbarang 
	UNION
	SELECT '%' AS value, 'Semua' AS label, 'z' AS BrgStatus,  'z' AS BrgKode
	ORDER BY BrgStatus, BrgKode" )
	->queryAll();
$cmbBarang   = ArrayHelper::map( $sqlBarang, 'value', 'label' );

$format = <<< SCRIPT
function formatBank(result) {
    return '<div class="row">' +
           '<div class="col-md-8">' + result.id + '</div>' +
           '<div class="col-md-16">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );

?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'MonitorGrade' => 'Monitor Grade',
									'MonitorGradePart' => 'Monitor Grade Part',
									'MonitorGradeOli' => 'Monitor Grade Oli',
									'Grade12' => 'Grade 12',
									'Level12' => 'Level 12',
									'Grade06' => 'Grade 06',
									'Level06' => 'Level 06',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>

                <div class="col-md-24">
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-sm-24">
                                <label class="control-label col-sm-4">Tanggal</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <div class="col-md-12">
                                <? try {
                                    echo DatePicker::widget( [
                                        'name'          => 'tgl1',
                                        'id'            => 'tgl1_id',
                                        'type'          => DatePicker::TYPE_INPUT,
                                        'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
                                        'pluginOptions' => [
                                            'autoclose'      => true,
                                            'format'         => 'dd/mm/yyyy',
                                            'todayHighlight' => true
                                        ]
                                    ] );
                                } catch ( \yii\base\InvalidConfigException $e ) {
                                } ?>
                            </div>
                            <div class="col-md-12">
                                <? try {
                                    echo DatePicker::widget( [
                                        'name'          => 'tgl2',
                                        'id'            => 'tgl2_id',
                                        'type'          => DatePicker::TYPE_INPUT,
                                        'value'         => date('t/m/Y',strtotime("today")),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format'    => 'dd/mm/yyyy',
                                        ],
                                    ] );
                                } catch ( \yii\base\InvalidConfigException $e ) {
                                } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <? try {
                            echo CheckboxX::widget([
                                'name'=>'eom',
                                'value'         => true,
                                'options'=>['id'=>'eom'],
                                'pluginOptions'=>['threeState'=>false, 'size'=>'lg'],
                                'pluginEvents'  => [
                                    "change" => new JsExpression("function(e) {
                                                 var today = new Date();
                                                 var tgl2 = $('#tgl2_id');
                                                 let tglku = tgl2.kvDatepicker('getDate');
                                                 var lastDayOfMonth = new Date(tglku.getFullYear(), tglku.getMonth()+1, 0);
                                                if($(this).val() == 1){
                                                    tgl2.kvDatepicker('update', lastDayOfMonth)
                                                 }else{
                                                    tgl2.kvDatepicker('update', new Date(today.getFullYear(), today.getMonth(), today.getDate()));
                                                 }
                                                }")
                                ]
                            ]);
                            echo '<label class="cbx-label" for="eom">EOM</label>';
                        } catch ( \yii\base\InvalidConfigException $e ) {
                        } ?>
                    </div>
                </div>
			</div>

			<?= Html::endForm(); ?>
        </div>
    </div>
<?php

