<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\checkbox\CheckboxX;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
$this->title                   = 'Dashboard Marketing';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;


$format = <<< SCRIPT
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );

?>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">

                <div class="col-md-24">
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-sm-24">
                                <label class="control-label col-sm-4">Tanggal</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <div class="col-md-12">
                                <? try {
                                    echo DatePicker::widget( [
                                        'name'          => 'tgl1',
                                        'id'            => 'tgl1_id',
                                        'type'          => DatePicker::TYPE_INPUT,
                                        'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
                                        'pluginOptions' => [
                                            'autoclose'      => true,
                                            'format'         => 'dd/mm/yyyy',
                                            'todayHighlight' => true
                                        ]
                                    ] );
                                } catch ( \yii\base\InvalidConfigException $e ) {
                                } ?>
                            </div>
                            <div class="col-md-12">
                                <? try {
                                    echo DatePicker::widget( [
                                        'name'          => 'tgl2',
                                        'id'            => 'tgl2_id',
                                        'type'          => DatePicker::TYPE_INPUT,
                                        'value'         => date('t/m/Y',strtotime("today")),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format'    => 'dd/mm/yyyy',
                                        ],
                                    ] );
                                } catch ( \yii\base\InvalidConfigException $e ) {
                                } ?>
                            </div>
                        </div>
                    </div>
                    <label class="control-label col-sm-1">Bulan</label>
                    <div class="col-md-3">
                        <? try {
                            echo DatePicker::widget( [
                                'name'          => 'TglKu',
                                'attribute2'=>'to_date',
                                'type'          => DatePicker::TYPE_INPUT,
                                'value'         => Yii::$app->formatter->asDate( 'now','MMMM  yyyy' ),
                                'pluginOptions' => [
                                    'startView'=>'year',
                                    'minViewMode'=>'months',
                                    'format' => 'MM  yyyy'
                                ],
                                'pluginEvents'  => [
                                    "change" => new JsExpression("function(e) {
                                                 let d = new Date(Date.parse('01 ' + $(this).val()));											 
                                                 var tgl1 = $('#tgl1_id');
                                                 var tgl2 = $('#tgl2_id');
                                                  tgl1.kvDatepicker('update', d);
                                                  tgl2.kvDatepicker('update',new Date(d.getFullYear(), d.getMonth()+1, 0));
                                                }")
                                ]
                            ] );
                        } catch ( \yii\base\InvalidConfigException $e ) {
                        } ?>
                    </div>
                    <div class="col-sm-6">
                        <? try {
                            echo CheckboxX::widget([
                                'name'=>'eom',
                                'value'         => true,
                                'options'=>['id'=>'eom'],
                                'pluginOptions'=>['threeState'=>false, 'size'=>'lg'],
                                'pluginEvents'  => [
                                    "change" => new JsExpression("function(e) {
                                                 var today = new Date();
                                                 var tgl2 = $('#tgl2_id');
                                                 let tglku = tgl2.kvDatepicker('getDate');
                                                 var lastDayOfMonth = new Date(tglku.getFullYear(), tglku.getMonth()+1, 0);
                                                if($(this).val() == 1){
                                                    tgl2.kvDatepicker('update', lastDayOfMonth)
                                                 }else{
                                                    tgl2.kvDatepicker('update', new Date(today.getFullYear(), today.getMonth(), today.getDate()));
                                                 }
                                                }")
                                ]
                            ]);
                            echo '<label class="cbx-label" for="eom">EOM</label>';
                        } catch ( \yii\base\InvalidConfigException $e ) {
                        } ?>
                    </div>
                </div>

                <div class="col-md-24"><br><br></div>


                <!--                DASHBOARD BENGKEL-->
                <div class="col-md-8">
                    <!-- /.box 1-->
                    <div class="row">
                        <div class="col-md-20">
                            <div class="box box-warning direct-chat direct-chat-warning">
                                <div class="box-header with-border">
                                    <h3 class="box-title">UNIT ENTRY</h3>
                                    <div class="box-tools pull-right">
                                        <!-- BUTTON-->
                                        <button class="btn"><i class="fa fa-bar-chart"></i></button>
                                        <button class="btn"><i class="fa fa-download"></i></button>
                                        <button class="btn"><i class="fa fa-file-text"></i></button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">

                                    <!-- CHARTJS 1-->
                                    <div>
                                        <canvas id="myChart"></canvas>
                                    </div>
                                    <script>
                                        var ctx = document.getElementById('myChart').getContext('2d');
                                        var myChart = new Chart(ctx, {
                                            type: 'bar',
                                            data: {
                                                labels: ["21", "22", "23","24","25","26"],
                                                datasets: [{
                                                    label: [],
                                                    //isi
                                                    data: [1200, 1900, 300, 5000, 2000, 3000],
                                                    //warna
                                                    backgroundColor: [
                                                        'rgba(0,0,255)',
                                                        'rgba(0,0,255)',
                                                        'rgba(0,0,255)',
                                                        'rgba(0,0,255)',
                                                        'rgba(0,0,255)',
                                                        'rgba(0,0,255)',
                                                    ],
                                                    borderColor: [],
                                                    // borderWidth: 1
                                                }]
                                            },
                                            options: {
                                                scales: {
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }]
                                                },
                                            }
                                        });
                                    </script>



                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer"></div>
                                <!-- /.box-footer-->
                            </div>
                            <!--/.direct-chat -->
                        </div>
                        <!-- /.row -->

                        <!-- /.box 2-->
                        <div class="col-md-20">
                            <div class="box box-warning direct-chat direct-chat-warning">
                                <div class="box-header with-border">
                                    <h3 class="box-title">AKUMULASI UE</h3>
                                    <div class="box-tools pull-right">
                                        <!-- BUTTON-->
                                        <button class="btn"><i class="fa fa-bar-chart"></i></button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">

                                    <canvas id="myChart3"></canvas>

                                    <script>

                                        var barChartData = {
                                            labels: ["21", "22", "23","24","25","26", "27"],
                                            datasets: [
                                                {
                                                    label: "M-1",
                                                    backgroundColor: "blue",
                                                    borderColor: "blue",
                                                    borderWidth: 1,
                                                    data: [30, 5, 6, 7,3, 5, 6]
                                                },
                                                {
                                                    label: "M",
                                                    backgroundColor: "green",
                                                    borderColor: "green",
                                                    borderWidth: 1,
                                                    data: [4, 7, 3, 6, 10,7,4]
                                                },
                                            ]
                                        };

                                        var chartOptions = {
                                            responsive: true,
                                            legend: {
                                                position: "top"
                                            },
                                            title: {
                                                display: true,
                                                text: "Chart.js Bar Chart"
                                            },
                                            scales: {
                                                yAxes: [{
                                                    ticks: {
                                                        beginAtZero: true
                                                    }
                                                }]
                                            }
                                        }

                                        var ctx = document.getElementById("myChart3").getContext("2d");
                                        window.myBar = new Chart(ctx, {
                                            type: "bar",
                                            data: barChartData,
                                            options: chartOptions
                                        });

                                    </script>



                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer"></div>
                                <!-- /.box-footer-->
                            </div>
                            <!--/.direct-chat -->
                        </div>
                    </div>

                    <!-- /.box 3-->
                    <div class="col-md-20">
                        <!-- DAILY DISTRIBUTION TREND -->
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">PART</h3>
                                <div class="box-tools pull-right">
                                    <!-- BUTTON-->
                                    <button class="btn"><i class="fa fa-bar-chart"></i></button>
                                    <button class="btn"><i class="fa fa-download"></i></button>
                                    <button class="btn"><i class="fa fa-file-text"></i></button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <!-- CHARTJS 1-->
                                <div>
                                    <canvas id="myChart2"></canvas>
                                </div>
                                <script>
                                    var ctx = document.getElementById('myChart2').getContext('2d');
                                    var myChart2 = new Chart(ctx, {
                                        type: 'bar',
                                        data: {
                                            labels: ["21", "22", "23","24","25","26"],
                                            datasets: [{
                                                label: [],
                                                //isi
                                                data: [1200, 1900, 300, 5000, 2000, 3000],
                                                //warna
                                                backgroundColor: [
                                                    'rgba(0,0,255)',
                                                    'rgba(0,0,255)',
                                                    'rgba(0,0,255)',
                                                    'rgba(0,0,255)',
                                                    'rgba(0,0,255)',
                                                    'rgba(0,0,255)',
                                                ],
                                                borderColor: [],
                                                // borderWidth: 1
                                            }]
                                        },
                                        options: {
                                            scales: {
                                                yAxes: [{
                                                    ticks: {
                                                        beginAtZero: true
                                                    }
                                                }]
                                            },
                                        }
                                    });
                                </script>



                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>


                    <!-- /.row -->
                    <!-- /.box 4-->
                    <div class="col-md-20">
                        <!-- DAILY DISTRIBUTION TREND -->
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">AKUMULASI PART</h3>
                                <div class="box-tools pull-right">
                                    <!-- BUTTON-->
                                    <button class="btn"><i class="fa fa-bar-chart"></i></button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <canvas id="myChart4"></canvas>

                                <script>

                                    var barChartData = {
                                        labels: ["21", "22", "23","24","25","26", "27"],
                                        datasets: [
                                            {
                                                label: "M-1",
                                                backgroundColor: "blue",
                                                borderColor: "blue",
                                                borderWidth: 1,
                                                data: [30, 5, 6, 7,3, 5, 6]
                                            },
                                            {
                                                label: "M",
                                                backgroundColor: "green",
                                                borderColor: "green",
                                                borderWidth: 1,
                                                data: [4, 7, 3, 6, 10,7,4]
                                            },
                                        ]
                                    };

                                    var chartOptions = {
                                        responsive: true,
                                        legend: {
                                            position: "top"
                                        },
                                        title: {
                                            display: true,
                                            text: "Chart.js Bar Chart"
                                        },
                                        scales: {
                                            yAxes: [{
                                                ticks: {
                                                    beginAtZero: true
                                                }
                                            }]
                                        }
                                    }

                                    var ctx = document.getElementById("myChart4").getContext("2d");
                                    window.myBar = new Chart(ctx, {
                                        type: "bar",
                                        data: barChartData,
                                        options: chartOptions
                                    });

                                </script>



                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                </div>

                <div class="col-md-16">

                    <div class="col-md-24">
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="col-md-24"><br><br><br></div>
                        <table style="border-spacing: 10px">
                            <tr>
                                <td style="padding: 5px;background-color: #b9def0">Part</td>
                                <td style="padding: 10px;"></td>
                                <td style="padding: 5px;background-color: #b9def0">Oli</td>
                                <td style="padding: 10px;"></td>
                                <td style="padding: 5px;background-color: #b9def0">Jasa</td>
                                <td style="padding: 10px;"></td>
                                <td style="padding: 5px;background-color: #b9def0">Revenue</td>
                                <td style="padding: 10px;"></td>
                                <td style="padding: 5px;background-color: #b9def0">Provit</td>
                            </tr>
                            <tr>
                                <td style="padding: 5px;background-color:#b9def0;font-size:x-large">150.000.00</td>
                                <td style="padding: 5px;"></td>
                                <td style="padding: 5px;background-color:#b9def0;font-size:x-large">35.000.00</td>
                                <td style="padding: 5px;"></td>
                                <td style="padding: 5px;background-color:#b9def0;font-size:x-large">35.000.00</td>
                                <td style="padding: 5px;"></td>
                                <td style="padding: 5px;background-color:#b9def0;font-size:x-large">220.000.00</td>
                                <td style="padding: 5px;"></td>
                                <td style="padding: 5px;background-color:#b9def0;font-size:x-large">35.000.00</td>
                            </tr>
                            <tr>
                                <td style="padding: 10px;font-size:xx-large"></td>
                            </tr>
                            <tr>
                                <td style="padding: 5px;background-color: #b9def0">Part/Unit(Rupiah)</td>
                                <td style="padding: 10px;"></td>
                                <td style="padding: 5px;background-color: #b9def0">Oli/Unit(Rupiah)</td>
                                <td style="padding: 10px;"></td>
                                <td style="padding: 5px;background-color: #b9def0">Jasa/Unit(Rupiah)</td>
                                <td style="padding: 10px;"></td>
                                <td style="padding: 5px;background-color: #b9def0">Revenue/Unit(Rupiah)</td>
                                <td style="padding: 10px;"></td>
                                <td style="padding: 5px;background-color: #b9def0">Provit/Unit(Rupiah)</td>
                            </tr>
                            <tr>
                                <td style="padding: 5px;background-color:#b9def0;font-size:x-large">50.000</td>
                                <td style="padding: 5px;"></td>
                                <td style="padding: 5px;background-color:#b9def0;font-size:x-large">50.000</td>
                                <td style="padding: 5px;"></td>
                                <td style="padding: 5px;background-color:#b9def0;font-size:x-large">50.000</td>
                                <td style="padding: 5px;"></td>
                                <td style="padding: 5px;background-color:#b9def0;font-size:x-large">50.000</td>
                                <td style="padding: 5px;"></td>
                                <td style="padding: 5px;background-color:#b9def0;font-size:x-large">50.000</td>
                            </tr>

                        </table>
                    </div>
                    </div>
                    <div class="col-md-24"><br><br><br></div>
                    <div class="col-md-24">
                        <div class="col-md-12">
                            <div class="box box-warning direct-chat direct-chat-warning">
                            <br><h4 class="box-title" style="text-align: center;">UE by jam kedatangan</h4>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <canvas id="speedChart" width="700" height="500"></canvas>
                                <script>
                                    var speedCanvas = document.getElementById("speedChart");

                                    var speedData = {
                                        labels: ["1.039.900", "2.893.849", "1.039.900", "2.893.849", "1.039.900", "2.893.849", "1.039.900"],
                                        datasets: [{
                                            label: "",
                                            data: [26, 30, 5, 20, 20, 55, 40],
                                            backgroundColor: 'rgb(54, 162, 235)',
                                            pointBackgroundColor: 'rgb(54, 162, 235)',
                                            fill: true,
                                        }]
                                    };

                                    var chartOptions = {
                                        legend: {
                                            display: false,
                                            position: 'top',
                                            labels: {
                                                boxWidth: 80,
                                                fontColor: 'black'
                                            }
                                        }
                                    };

                                    var lineChart = new Chart(speedCanvas, {
                                        type: 'line',
                                        data: speedData,
                                        options: chartOptions
                                    });
                                </script>

                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <!-- DAILY DISTRIBUTION TREND -->
                            <div class="box box-warning direct-chat direct-chat-warning">
                                <div class="box-header with-border">
                                    <h3 class="box-title">UE By Hari</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn"><i class="fa fa-download"></i></button>
                                        <button class="btn"><i class="fa fa-file-text"></i></button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">

                                    <!-- CHARTJS 1-->
                                    <div>
                                        <canvas id="myChartUH"></canvas>
                                    </div>
                                    <script>
                                        var ctx = document.getElementById('myChartUH').getContext('2d');
                                        var myChartUH = new Chart(ctx, {
                                            type: 'bar',
                                            data: {
                                                labels: ["21", "22", "23","24","25","26"],
                                                datasets: [{
                                                    label: [],
                                                    //isi
                                                    data: [1200, 1900, 300, 5000, 2000, 3000],
                                                    //warna
                                                    backgroundColor: [
                                                        'rgba(0,0,255)',
                                                        'rgba(0,0,255)',
                                                        'rgba(0,0,255)',
                                                        'rgba(0,0,255)',
                                                        'rgba(0,0,255)',
                                                        'rgba(0,0,255)',
                                                    ],
                                                    borderColor: [],
                                                    // borderWidth: 1
                                                }]
                                            },
                                            options: {
                                                indexAxis: 'y',
                                            }
                                        });
                                    </script>



                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer"></div>
                                <!-- /.box-footer-->
                            </div>
                            <!--/.direct-chat -->
                        </div>
                    </div>
                    <div class="col-md-24"><br><br></div>
                    <div class="col-md-24">
                    <div class="col-md-12">
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">UE vs Target</h3>
                                <div class="box-tools pull-right">
                                    <!-- BUTTON-->
                                    <button class="btn"><i class="fa fa-bar-chart"></i></button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <canvas id="myChartUVT"></canvas>
                                <script>

                                    var barChartData = {
                                        labels: ["21", "22", "23","24","25","26", "27"],
                                        datasets: [
                                            {
                                                label: "M-1",
                                                backgroundColor: "blue",
                                                borderColor: "blue",
                                                borderWidth: 1,
                                                data: [30, 5, 6, 7,3, 5, 6]
                                            },
                                            {
                                                label: "M",
                                                backgroundColor: "green",
                                                borderColor: "green",
                                                borderWidth: 1,
                                                data: [4, 7, 3, 6, 10,7,4]
                                            },
                                        ]
                                    };

                                    var chartOptions = {
                                        responsive: true,
                                        legend: {
                                            position: "top"
                                        },
                                        title: {
                                            display: true,
                                            text: "Chart.js Bar Chart"
                                        },
                                        scales: {
                                            yAxes: [{
                                                ticks: {
                                                    beginAtZero: true
                                                }
                                            }]
                                        }
                                    }

                                    var ctx = document.getElementById("myChartUVT").getContext("2d");
                                    window.myBar = new Chart(ctx, {
                                        type: "bar",
                                        data: barChartData,
                                        options: chartOptions
                                    });

                                </script>



                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                    <div class="col-md-12">
                            <!-- DAILY DISTRIBUTION TREND -->
                            <div class="box box-warning direct-chat direct-chat-warning">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Laba & Unit Entry per Mekanik</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn"><i class="fa fa-download"></i></button>
                                        <button class="btn"><i class="fa fa-file-text"></i></button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">

                                    <!-- CHARTJS 1-->
                                    <div>
                                        <canvas id="myChartLUE"></canvas>
                                    </div>
                                    <script>
                                        var ctx = document.getElementById('myChartLUE').getContext('2d');
                                        var myChartLUE = new Chart(ctx, {
                                            type: 'bar',
                                            data: {
                                                labels: ["21", "22", "23","24","25","26"],
                                                datasets: [{
                                                    label: [],
                                                    //isi
                                                    data: [1200, 1900, 300, 5000, 2000, 3000],
                                                    //warna
                                                    backgroundColor: [
                                                        'rgba(0,0,255)',
                                                        'rgba(0,0,255)',
                                                        'rgba(0,0,255)',
                                                        'rgba(0,0,255)',
                                                        'rgba(0,0,255)',
                                                        'rgba(0,0,255)',
                                                    ],
                                                    borderColor: [],
                                                    // borderWidth: 1
                                                }]
                                            },
                                            options: {
                                                scales: {
                                                    xAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }]
                                                },
                                            }
                                        });
                                    </script>



                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer"></div>
                                <!-- /.box-footer-->
                            </div>
                            <!--/.direct-chat -->
                        </div>
                    </div>
                </div>
                <!--                DASHBOARD BENGKEL-->


                <div class="col-md-24">
<!--                    <div class="col-md-8">-->
<!--                        <div class="box box-warning direct-chat direct-chat-warning">-->
<!--                            <div class="box-header with-border">-->
<!--                                <h3 class="box-title"></h3>-->
<!--                                <div class="box-tools pull-right">-->

<!--                                </div>-->
<!--                            </div>-->

<!--                            <div class="box-body">-->
<!---->

<!--                                <div>-->
<!--                                    <canvas id="unitEntry"></canvas>-->
<!--                                </div>-->
<!--                                <script>-->
<!--                                    var ctx = document.getElementById('unitEntry').getContext('2d');-->
<!--                                    var myChart = new Chart(ctx, {-->
<!--                                        type: 'bar',-->
<!--                                        data: {-->
<!--                                            labels: ['21', '22', '23', '24', '25'],-->
<!--                                            datasets: [{-->
<!--                                                label: 'UNIT ENTRY',-->
<!--                                                //isi-->
<!--                                                data: [29, 75, 0, 123, 32, 43],-->
<!--                                                //warna-->
<!--                                                backgroundColor: [-->
<!--                                                    'rgba(255, 206, 86, 1)',-->
<!--                                                    'rgba(255, 206, 86, 1)',-->
<!--                                                    'rgba(255, 206, 86, 1)',-->
<!--                                                    'rgba(255, 206, 86, 1)',-->
<!--                                                    'rgba(255, 206, 86, 1)',-->
<!--                                                    'rgba(255, 206, 86, 1)',-->
<!--                                                ],-->
<!--                                                borderColor: [-->
<!--                                                    'rgba(255, 206, 86, 1)',-->
<!--                                                    'rgba(255, 206, 86, 1)',-->
<!--                                                    'rgba(255, 206, 86, 1)',-->
<!--                                                    'rgba(255, 159, 64, 1)',-->
<!--                                                    'rgba(255, 159, 64, 1)',-->
<!--                                                    'rgba(255, 159, 64, 1)',-->
<!--                                                ],-->
<!--                                                borderWidth: 1-->
<!--                                            }]-->
<!--                                        },-->
<!--                                        options: {-->
<!--                                            scales: {-->
<!--                                                yAxes: [{-->
<!--                                                    ticks: {-->
<!--                                                        beginAtZero: true-->
<!--                                                    }-->
<!--                                                }]-->
<!--                                            }-->
<!--                                        }-->
<!--                                    });-->
<!--                                </script>-->
<!---->
<!--                                <div class="col-md-24"><br><br><br></div>-->
<!---->

<!--                                <div>-->
<!--                                    <br><h4 class="box-title" style="text-align: center;">Akumulasi UE</h4>-->
<!--                                    <canvas id="akumulasiUe"></canvas>-->
<!--                                </div>-->
<!--                                <script>-->
<!---->
<!--                                    var barChartData = {-->
<!--                                        labels: [-->
<!--                                            "21",-->
<!--                                            "22",-->
<!--                                            "23",-->
<!--                                            "24",-->
<!--                                            "25",-->
<!--                                            "26",-->
<!--                                            "27"-->
<!--                                        ],-->
<!---->
<!--                                        datasets: [-->
<!--                                            {-->
<!--                                                label: "M-1",-->
<!--                                                backgroundColor: "blue",-->
<!--                                                borderColor: "blue",-->
<!--                                                borderWidth: 1,-->
<!--                                                data: [14, 5, 6, 7,3, 5, 6]-->
<!--                                            },-->
<!--                                            {-->
<!--                                                label: "M",-->
<!--                                                backgroundColor: "green",-->
<!--                                                borderColor: "green",-->
<!--                                                borderWidth: 1,-->
<!--                                                data: [4, 7, 3, 6, 10,7,4]-->
<!--                                            },-->
<!--                                        ]-->
<!--                                    };-->
<!---->
<!--                                    var chartOptions = {-->
<!--                                        responsive: true,-->
<!--                                        legend: {-->
<!--                                            position: "top"-->
<!--                                        },-->
<!--                                        title: {-->
<!--                                            display: true,-->
<!--                                            text: "Chart.js Bar Chart"-->
<!--                                        },-->
<!--                                        scales: {-->
<!--                                            yAxes: [{-->
<!--                                                ticks: {-->
<!--                                                    beginAtZero: true-->
<!--                                                }-->
<!--                                            }]-->
<!--                                        }-->
<!--                                    }-->
<!---->
<!--                                    var ctx = document.getElementById("akumulasiUe").getContext("2d");-->
<!--                                    window.myBar = new Chart(ctx, {-->
<!--                                        type: "bar",-->
<!--                                        data: barChartData,-->
<!--                                        options: chartOptions-->
<!--                                    });-->
<!---->
<!--                                </script>-->
<!---->
<!--                                <div class="col-md-24"><br><br><br></div>-->
<!---->

<!--                                <div>-->
<!--                                    <canvas id="part"></canvas>-->
<!--                                </div>-->
<!--                                <script>-->
<!--                                    var ctx = document.getElementById('part').getContext('2d');-->
<!--                                    var myChart = new Chart(ctx, {-->
<!--                                        type: 'bar',-->
<!--                                        data: {-->
<!--                                            labels: ['21', '22', '23', '24', '25'],-->
<!--                                            datasets: [{-->
<!--                                                label: 'PART',-->
<!--                                                //isi-->
<!--                                                data: [2900, 7500, 20000, 12300, 3200, 4300],-->
<!--                                                //warna-->
<!--                                                backgroundColor: [-->
<!--                                                    'rgba(255, 206, 86, 1)',-->
<!--                                                    'rgba(255, 206, 86, 1)',-->
<!--                                                    'rgba(255, 206, 86, 1)',-->
<!--                                                    'rgba(255, 206, 86, 1)',-->
<!--                                                    'rgba(255, 206, 86, 1)',-->
<!--                                                    'rgba(255, 206, 86, 1)',-->
<!--                                                ],-->
<!--                                                borderColor: [-->
<!--                                                    'rgba(255, 206, 86, 1)',-->
<!--                                                    'rgba(255, 206, 86, 1)',-->
<!--                                                    'rgba(255, 206, 86, 1)',-->
<!--                                                    'rgba(255, 159, 64, 1)',-->
<!--                                                    'rgba(255, 159, 64, 1)',-->
<!--                                                    'rgba(255, 159, 64, 1)',-->
<!--                                                ],-->
<!--                                                borderWidth: 1-->
<!--                                            }]-->
<!--                                        },-->
<!--                                        options: {-->
<!--                                            scales: {-->
<!--                                                yAxes: [{-->
<!--                                                    ticks: {-->
<!--                                                        beginAtZero: true-->
<!--                                                    }-->
<!--                                                }]-->
<!--                                            }-->
<!--                                        }-->
<!--                                    });-->
<!--                                </script>-->
<!---->
<!--                                <div class="col-md-24"><br><br><br></div>-->
<!---->

<!--                                <div>-->
<!--                                    <br><h4 class="box-title" style="text-align: center;">Akumulasi Part</h4>-->
<!--                                    <canvas id="akumulasiPart"></canvas>-->
<!--                                </div>-->
<!--                                <script>-->
<!---->
<!--                                    var barChartData = {-->
<!--                                        labels: [-->
<!--                                            "21",-->
<!--                                            "22",-->
<!--                                            "23",-->
<!--                                            "24",-->
<!--                                            "25",-->
<!--                                            "26",-->
<!--                                            "27"-->
<!--                                        ],-->
<!---->
<!--                                        datasets: [-->
<!--                                            {-->
<!--                                                label: "M-1",-->
<!--                                                backgroundColor: "blue",-->
<!--                                                borderColor: "blue",-->
<!--                                                borderWidth: 1,-->
<!--                                                data: [14, 5, 6, 7,3, 5, 6]-->
<!--                                            },-->
<!--                                            {-->
<!--                                                label: "M",-->
<!--                                                backgroundColor: "green",-->
<!--                                                borderColor: "green",-->
<!--                                                borderWidth: 1,-->
<!--                                                data: [4, 7, 3, 6, 10,7,4]-->
<!--                                            },-->
<!--                                        ]-->
<!--                                    };-->
<!---->
<!--                                    var chartOptions = {-->
<!--                                        responsive: true,-->
<!--                                        legend: {-->
<!--                                            position: "top"-->
<!--                                        },-->
<!--                                        title: {-->
<!--                                            display: true,-->
<!--                                            text: "Chart.js Bar Chart"-->
<!--                                        },-->
<!--                                        scales: {-->
<!--                                            yAxes: [{-->
<!--                                                ticks: {-->
<!--                                                    beginAtZero: true-->
<!--                                                }-->
<!--                                            }]-->
<!--                                        }-->
<!--                                    }-->
<!---->
<!--                                    var ctx = document.getElementById("akumulasiPart").getContext("2d");-->
<!--                                    window.myBar = new Chart(ctx, {-->
<!--                                        type: "bar",-->
<!--                                        data: barChartData,-->
<!--                                        options: chartOptions-->
<!--                                    });-->
<!---->
<!--                                </script>-->
<!---->
<!---->
<!---->
<!--                            </div>-->

<!--                            <div class="box-footer"></div>-->

<!--                        </div>-->
<!---->
<!--                    </div>-->
<!--                    <div class="col-md-16">-->
<!--                        <div class="col-md-24">-->
<!--                            <style>-->
<!--                                /*table, td, th, tr {}*/-->
<!--                                table {-->
<!--                                    /*background-color: #ADD8E6;*/-->
<!--                                    width: 100%;-->
<!--                                    height: 70px;-->
<!--                                    text-align: right;-->
<!--                                }-->
<!--                                th{-->
<!--                                    font-size: 10px;-->
<!--                                }-->
<!--                                td{-->
<!--                                    font-size: 14px;-->
<!--                                    font-family: "Arial Black";-->
<!--                                }-->
<!--                                .col-md-4{-->
<!--                                    background-color: #DCDCDC-->
<!--                                }-->
<!--                            </style>-->
<!--                            <div class="col-md-24">-->
<!--                                <div class="col-md-4">-->
<!--                                    <table>-->
<!--                                        <tr><th>Part</th></tr>-->
<!--                                        <tr><td>150.000.000</td>-->
<!--                                        </tr>-->
<!--                                    </table>-->
<!--                                </div>-->
<!--                                <div class="col-md-1"></div>-->
<!--                                <div class="col-md-4">-->
<!--                                    <table>-->
<!--                                        <tr><th>Oli</th></tr>-->
<!--                                        <tr><td>350.000.000</td>-->
<!--                                        </tr>-->
<!--                                    </table>-->
<!--                                </div>-->
<!--                                <div class="col-md-1"></div>-->
<!--                                <div class="col-md-4">-->
<!--                                    <table>-->
<!--                                        <tr><th>Jasa</th></tr>-->
<!--                                        <tr><td>350.000.000</td>-->
<!--                                        </tr>-->
<!--                                    </table>-->
<!--                                </div>-->
<!--                                <div class="col-md-1"></div>-->
<!--                                <div class="col-md-4">-->
<!--                                    <table>-->
<!--                                        <tr><th>Revenue</th></tr>-->
<!--                                        <tr><td>220.000.000</td>-->
<!--                                        </tr>-->
<!--                                    </table>-->
<!--                                </div>-->
<!--                                <div class="col-md-1"></div>-->
<!--                                <div class="col-md-4">-->
<!--                                    <table>-->
<!--                                        <tr><th>Profit</th></tr>-->
<!--                                        <tr><td>35.000.000</td>-->
<!--                                        </tr>-->
<!--                                    </table>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-md-24"><br><br><br>-->
<!--                                <div class="col-md-24">-->
<!--                                    <div class="col-md-4">-->
<!--                                        <table>-->
<!--                                            <tr><th>Part/Unit(Rupiah)</th></tr>-->
<!--                                            <tr><td>50.000</td>-->
<!--                                            </tr>-->
<!--                                        </table>-->
<!--                                    </div>-->
<!--                                    <div class="col-md-1"></div>-->
<!--                                    <div class="col-md-4">-->
<!--                                        <table>-->
<!--                                            <tr><th>Oli/Unit(Rupiah)</th></tr>-->
<!--                                            <tr><td>50.000</td>-->
<!--                                            </tr>-->
<!--                                        </table>-->
<!--                                    </div>-->
<!--                                    <div class="col-md-1"></div>-->
<!--                                    <div class="col-md-4">-->
<!--                                        <table>-->
<!--                                            <tr><th>Jasa/Unit(Rupiah)</th></tr>-->
<!--                                            <tr><td>50.000</td>-->
<!--                                            </tr>-->
<!--                                        </table>-->
<!--                                    </div>-->
<!--                                    <div class="col-md-1"></div>-->
<!--                                    <div class="col-md-4">-->
<!--                                        <table>-->
<!--                                            <tr><th>Revenue/Unit(Rupiah)</th></tr>-->
<!--                                            <tr><td>50.000</td>-->
<!--                                            </tr>-->
<!--                                        </table>-->
<!--                                    </div>-->
<!--                                    <div class="col-md-1"></div>-->
<!--                                    <div class="col-md-4">-->
<!--                                        <table>-->
<!--                                            <tr><th>Profit/Unit(Rupiah)</th></tr>-->
<!--                                            <tr><td>50.000</td>-->
<!--                                            </tr>-->
<!--                                        </table>-->
<!--                                    </div>-->
<!--                                </div>-->
<!---->
<!--                            </div>-->
<!---->
<!---->
<!--                        </div>-->
<!--                        <div class="col-md-24">-->
<!--                            <div class="col-md-12">-->
<!--                                <br><h4 class="box-title" style="text-align: center;">UE by jam kedatangan</h4>-->
<!--                                <div class="box-body">-->
<!--                                    <canvas id="speedChart" width="600" height="400"></canvas>-->
<!--                                    <script>-->
<!--                                        var speedCanvas = document.getElementById("speedChart");-->
<!---->
<!--                                        var speedData = {-->
<!--                                            labels: ["1.039.900", "2.893.849", "1.039.900", "2.893.849", "1.039.900", "2.893.849", "1.039.900"],-->
<!--                                            datasets: [{-->
<!--                                                label: "",-->
<!--                                                data: [26, 30, 5, 20, 20, 55, 40],-->
<!--                                                backgroundColor: 'rgb(54, 162, 235)',-->
<!--                                                pointBackgroundColor: 'rgb(54, 162, 235)',-->
<!--                                                fill: true,-->
<!--                                            }]-->
<!--                                        };-->
<!---->
<!--                                        var chartOptions = {-->
<!--                                            legend: {-->
<!--                                                display: false,-->
<!--                                                position: 'top',-->
<!--                                                labels: {-->
<!--                                                    boxWidth: 80,-->
<!--                                                    fontColor: 'black'-->
<!--                                                }-->
<!--                                            }-->
<!--                                        };-->
<!---->
<!--                                        var lineChart = new Chart(speedCanvas, {-->
<!--                                            type: 'line',-->
<!--                                            data: speedData,-->
<!--                                            options: chartOptions-->
<!--                                        });-->
<!--                                    </script>-->
<!---->
<!--                                </div>-->
<!--                                <div class="box-footer"></div>-->
<!--                            </div>-->
<!--                            <div class="col-md-12">-->

<!--                                <br><h4 class="box-title" style="text-align: center;">UE by hari</h4>-->

<!--                                <div class="box-body">-->
<!--                                    <script>-->
<!--                                    </script>-->
<!---->
<!--                                </div>-->

<!--                                <div class="box-footer"></div>-->

<!--                            </div>-->
<!--                        </div>-->


<!--                        <div class="col-md-24">-->
<!--                            <div class="col-md-12">-->

<!--                                <br><h4 style="text-align: center;">UE vs Target</h4>-->

<!--                                <div>-->
<!--                                    <canvas id="uevstarget"></canvas>-->
<!--                                </div>-->
<!--                                <script>-->
<!---->
<!--                                    var barChartData = {-->
<!--                                        labels: [-->
<!--                                            "21",-->
<!--                                            "22",-->
<!--                                            "23",-->
<!--                                            "24",-->
<!--                                            "25",-->
<!--                                            "26",-->
<!--                                            "27"-->
<!--                                        ],-->
<!---->
<!--                                        datasets: [-->
<!--                                            {-->
<!--                                                label: "M-1",-->
<!--                                                backgroundColor: "blue",-->
<!--                                                borderColor: "blue",-->
<!--                                                borderWidth: 1,-->
<!--                                                data: [14, 5, 6, 7,3, 5, 6]-->
<!--                                            },-->
<!--                                            {-->
<!--                                                label: "M",-->
<!--                                                backgroundColor: "green",-->
<!--                                                borderColor: "green",-->
<!--                                                borderWidth: 1,-->
<!--                                                data: [4, 7, 3, 6, 10,7,4]-->
<!--                                            },-->
<!--                                        ]-->
<!--                                    };-->
<!---->
<!--                                    var chartOptions = {-->
<!--                                        responsive: true,-->
<!--                                        legend: {-->
<!--                                            position: "top"-->
<!--                                        },-->
<!--                                        title: {-->
<!--                                            display: true,-->
<!--                                            text: "Chart.js Bar Chart"-->
<!--                                        },-->
<!--                                        scales: {-->
<!--                                            yAxes: [{-->
<!--                                                ticks: {-->
<!--                                                    beginAtZero: true-->
<!--                                                }-->
<!--                                            }]-->
<!--                                        }-->
<!--                                    }-->
<!---->
<!--                                    var ctx = document.getElementById("uevstarget").getContext("2d");-->
<!--                                    window.myBar = new Chart(ctx, {-->
<!--                                        type: "bar",-->
<!--                                        data: barChartData,-->
<!--                                        options: chartOptions-->
<!--                                    });-->
<!---->
<!--                                </script>-->
<!--                            </div>-->
<!--                            <div class="col-md-12">-->
<!--                                <br><h4 style="text-align: center;">Laba & Unit Entry Per Mekanik</h4>-->
<!--                                <div class="box-body">-->
<!---->
<!--                                    <script>-->
<!--                                    </script>-->
<!---->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
                    </div></div>





            <div class="box-footer"></div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
