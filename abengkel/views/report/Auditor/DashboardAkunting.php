<?php

use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\checkbox\CheckboxX;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;

$this->title = 'Dashboard Akuntansi ( x 10^6 )';
$this->params['breadcrumbs'][] = $this->title;
$formatter = \Yii::$app->formatter;

$urlAkudashacct = Url::toRoute(['report/akudashacct']);
$format = <<< SCRIPT
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
$this->registerJs(<<< JS
     
    
    
    
    Chart.register(ChartDataLabels);
function reloadGrid1(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
        $('#ajaxspinner1').show();
        $('#ajaxspinner2').show();
        $('#ajaxspinner3').show();
        $('#ajaxspinner4').show();
                
        $.ajax({
            type: 'POST',
            url: '$urlAkudashacct',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            success:function(response) {
                
                $('#TotalAssets').text(response['TotalAssets']);
                $('#CashBank').text(response['CashBank']);
                $('#Piutang').text(response['Piutang']);
                $('#Persediaan').text(response['Persediaan']);
                $('#Depositor').text(response['Depositor']);
                $('#AktivaTetap').text(response['AktivaTetap']);
                $('#TotalLiabilities').text(response['TotalLiabilities']);
                $('#Hutang').text(response['Hutang']);
                $('#Titipan').text(response['Titipan']);
                $('#Modal').text(response['Modal']);
                $('#TotalRugiLaba').text(response['TotalRugiLaba']);
                                
                $('#myChartLabaRugi').remove();
                $('.5').append('<canvas id="myChartLabaRugi"><canvas>');
                $('#myChartWorkingCapital').remove();
                $('.2').append('<canvas id="myChartWorkingCapital"><canvas>');
                $('#myChartTotalEquity').remove();
                $('.3').append('<canvas id="myChartTotalEquity"><canvas>');
                $('#myChartOpex').remove();
                $('.4').append('<canvas id="myChartOpex"><canvas>');
                $('#myChartNeraca').remove();
                $('.1').append('<canvas id="myChartNeraca"><canvas>');
                
                $('#myChartTreemap').remove();
                $('.6').append('<canvas id="myChartTreemap"><canvas>');
                
                
                //Working Capital
                ctx2 = document.getElementById("myChartWorkingCapital").getContext("2d");
                myChartWorkingCapital = new Chart(ctx2, {
                        type: "bar",
                        data: {
                            labels: ['Current Asset', 'Current Liabilities', 'Working Capital'],
                            renderText: ['Current Asset', 'Current Liabilities', 'Working Capital'],
                            
                            datasets: [
                                    {
                                    label: "Working Capital",
                                    data: [
                                        [parseInt(response['CurrentAsset']*-1)],
                                        [0,response['WorkingCapital']],
                                        [response['WorkingCapital'],parseInt(response['CurrentAsset']*-1)]                         
                                    ],
                                    backgroundColor: ["dodgerblue","coral","coral"],   
                                    renderText: ['Current Asset', 'Current Liabilities', 'Working Capital'],                                                           
                                    },
                                ]
                        },
                        options: {
                             plugins: {
                                 datalabels: {
                                     // display: false
                                     color: 'black',
                                     formatter: (val, ctx) => (ctx.dataset.renderText[ctx.dataIndex])
                                 }
                                 },
                        }
                    });
                
                //Total Equity
                ctx3 = document.getElementById("myChartTotalEquity").getContext("2d");
                myChartTotalEquity = new Chart(ctx3, {
                    type: "bar",
                        data: {
                            labels: ['Total Asset', 'Total Liabilities', 'Total Equity'],
                            datasets: [
                                    {
                                    label: "Total Equity",
                                    data: [
                                        [parseInt(response['TotalAsset'])*-1],
                                        [0,response['TotalLiabilites']],
                                        [response['TotalLiabilites'],parseInt(response['TotalAsset'])*-1]                         
                                    ],
                                    backgroundColor: ["dodgerblue","coral","coral"],
                                    renderText: ['Total Asset', 'Total Liabilities', 'Total Equity'],                             
                                    }
                                ]
                        },
                        options: {
                             plugins: {
                                 datalabels: {
                                     // display: false
                                     color: 'black',
                                     formatter: (val, ctx) => (ctx.dataset.renderText[ctx.dataIndex])
                                 }
                                 },
                        }
                    });
                
                
                 //LABA RUGI
                ctx5 = document.getElementById("myChartLabaRugi").getContext("2d");
                myChartLabaRugi = new Chart(ctx5, {
                                                data: {
                                                    labels: response['LabaRugiLabel'],                                                    
                                                    datasets: [
                                                        {
                                                            type: "bar",
                                                            label: "LabaRugiLabel",
                                                            backgroundColor: "dodgerblue",
                                                            borderColor: "dodgerblue",
                                                            borderWidth: 1,
                                                            data: response['LabaRugi']
                                                        },
                                                    ]
                                                },
                                                plugins: [ChartDataLabels],
                                                options: {
                                                    responsive: true,
                                                    legend: {
                                                        position: "top"
                                                    },
                                                    title: {
                                                        display: true,
                                                        text: "Chart.js Bar Chart"
                                                    },
                                                    // scales: {
                                                    //     yAxes: [{
                                                    //         ticks: {
                                                    //             beginAtZero: true
                                                    //         }
                                                    //     }]
                                                    // },
                                                }
                                            });
                
                //NERACA
                
                ctx1 = document.getElementById("myChartNeraca").getContext("2d");
                myChartNeraca = new Chart(ctx1,{
                                 type: 'pie',
                                  data: {
                                    
                                    datasets: [{
                                        label: 'Pie',
                                        data:  [
                                            response['Hutang1'],response['Titipan1'],response['Modal1'],response['TotalRugiLaba1'],
                                            response['CashBank1'],response['Piutang1'],response['Persediaan1'],response['Depositor1'],response['AktivaTetap1']
                                            ],  
                                        backgroundColor: ["coral","orange","lightcoral","sandybrown",
                                            "dodgerblue","lightblue","lightskyblue","deepskyblue","cornflowerblue"],
                                        renderText: ["Hutang","Titipan","Modal","Total Rugi Laba",
                                            "Cash Bank","Piutang","Persediaan","Depositor, Adv & Prepay","Aktiva Tetap & Lain2"]
                                      },
                                      {
                                        label: 'Pasiva & Aktiva',
                                        data: [response['TotalLiabilities'],response['TotalAssets']],
                                        backgroundColor: ["saddlebrown","royalblue"],
                                        renderText: [ "Pasiva","Aktiva"]
                                      },
                                    ]
                                  },
                                  options: {
                                    plugins: {
                                      datalabels: {
                                        color: 'black',
                                        formatter: (val, ctx) => (ctx.dataset.renderText[ctx.dataIndex])
                                      }
                                    }
                                  }
                                
                            });
                                
                //OPEX              
                const DATA = [
                      {
                        what: 'Gaji & Tunj',
                        value: response['Gaji'],
                        color: 'dodgerblue'
                      },
                      {
                        what: 'Insentif',
                        value: response['Insentif'],
                        color: 'coral'
                      },
                      {
                        what: 'By Pemasaran',
                        value: response['Pemasaran'],
                        color: 'lightgrey'
                      },
                      {
                        what: 'Penyusutan Asuransi',
                        value: response['Asuransi'],
                        color: 'yellow'
                      },
                      {
                        what: 'Penyusutan Asset',
                        value: response['Asset'],
                        color: 'lightskyblue'
                      },
                      {
                        what: 'Opex Lainnya',
                        value: response['Opex'],
                        color: 'mediumspringgreen'
                      }
                    ];
                
                ctx = document.getElementById("myChartOpex").getContext("2d");
                myChartOpex = new Chart(ctx, {
                        type: "treemap",
                        data: {
                            datasets: [
                                {
                                    tree: DATA,
                                    key: 'value',
                                    // renderText: ['Gaji','Insentif','Pemasaran','Asuransi','Asset','Opex'],
                                    backgroundColor(ctx) {
                                        // console.log(['red','green','blue']);
                                      if (ctx.type !== 'data') {
                                        return;
                                      }
                                       return ctx.raw._data.color;
                                    },
                                    labels: {
                                      align: 'left',
                                      display: true,
                                      formatter(ctx) {
                                        if (ctx.type !== 'data') {
                                          return;
                                        }
                                        return [ctx.raw._data.what+', '+ctx.raw.v, ' '];
                                      },
                                      color: ['black', 'black'],
                                      font: [{size: 12  , weight: 'bold'}, {size: 12}],
                                      position: 'top'
                                    }
                                    
                                  }                                
                            ],
                        },
                        options: {
                        events: [],
                        plugins: {
                            datalabels: {
                                                          display: false
                                                        },
                          title: {
                            display: true,
                            text: 'OPEX'
                          },
                          legend: {
                            display: false
                          },
                          tooltip: {
                            enabled: false
                          }
                        }
                      }
                });            
                               
                
                                                     
                
                $('#ajaxspinner1').hide();
                $('#ajaxspinner2').hide();
                $('#ajaxspinner3').hide();
                $('#ajaxspinner4').hide();
            }
        })
    }    

   $("#tampil").click(function() {
        reloadGrid1();
   }); 

JS
);

?>

    <script src="https://cdn.jsdelivr.net/npm/chart.js@4.4.1/dist/chart.umd.min.js"></script>
    <!--    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.0/dist/chart.min.js"></script>-->
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-chart-treemap@2.3.0/dist/chartjs-chart-treemap.min.js">

    </script>



    <style>
        .tombol {
            background: #2C97DF;
            color: white;
            border-top: 0;
            border-left: 0;
            border-right: 0;
            border-bottom: 0;
            padding: 5px 10px;
            text-decoration: none;
            font-family: sans-serif;
            font-size: 11pt;
            border-radius: 4px;
        }

        .outer {
            overflow-y: auto;
            height: 100px;
        }

        .outer table {
            width: 100%;
            table-layout: fixed;
            border: 1px solid black;
            border-spacing: 1px;
        }

        .outer table th {
            text-align: left;
            top: 0;
            position: sticky;
            background-color: white;
        }
    </style>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
            <?= Html::beginForm(); ?>
            <div class="box-body">
                <div class="col-md-24">
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-sm-24">
                                <label class="control-label col-sm-4">Tanggal</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <div class="col-md-12">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'tgl1',
                                        'id' => 'tgl1',
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => Yii::$app->formatter->asDate('now', '01/MM/yyyy'),
//                                                'value' => '01/02/2021',
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                            'todayHighlight' => true
                                        ],
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                            <div class="col-md-12">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'tgl2',
                                        'id' => 'tgl2',
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => date('t/m/Y', strtotime("today")),
//                                                'value' => '27/02/2021',
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                        ],
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                        </div>
                    </div>
                    <label class="control-label col-sm-1">Bulan</label>
                    <div class="col-md-3">
                        <? try {
                            echo DatePicker::widget([
                                'name' => 'TglKu',
                                'id' => 'TglKu',
                                'attribute2' => 'to_date',
                                'type' => DatePicker::TYPE_INPUT,
                                'value' => Yii::$app->formatter->asDate('now', 'MMMM  yyyy'),
                                'pluginOptions' => [
                                    'startView' => 'year',
                                    'minViewMode' => 'months',
                                    'format' => 'MM  yyyy'
                                ],
                                'pluginEvents' => [
                                    "change" => new JsExpression("
                                                function(e) {
                                                 let d = new Date(Date.parse('01 ' + $(this).val()));											 
                                                 var tgl1 = $('#tgl1');
                                                 var tgl2 = $('#tgl2');
                                                  tgl1.kvDatepicker('update', d);
                                                  tgl2.kvDatepicker('update',new Date(d.getFullYear(), d.getMonth()+1, 0));     
                                                }                                                                                            
                                                "),
                                ]
                            ]);
                        } catch (\yii\base\InvalidConfigException $e) {
                        } ?>
                    </div>
                    <div class="col-sm-2">
                        <? try {
                            echo CheckboxX::widget([
                                'name' => 'eom',
                                'value' => true,
                                'options' => ['id' => 'eom'],
                                'pluginOptions' => ['threeState' => false, 'size' => 'lg'],
                                'pluginEvents' => [
                                    "change" => new JsExpression("function(e) {
                                                 var today = new Date();
                                                 var tgl2 = $('#tgl2');
                                                 let tglku = tgl2.kvDatepicker('getDate');
                                                 var lastDayOfMonth = new Date(tglku.getFullYear(), tglku.getMonth()+1, 0);
                                                if($(this).val() == 1){
                                                    tgl2.kvDatepicker('update', lastDayOfMonth)
                                                 }else{
                                                    tgl2.kvDatepicker('update', new Date(today.getFullYear(), today.getMonth(), today.getDate()));
                                                 }
                                                }")
                                ]
                            ]);
                            echo '<label class="cbx-label" for="eom">EOM</label>';
                        } catch (\yii\base\InvalidConfigException $e) {
                        } ?>
                    </div>
                    <label class="control-label col-sm-1">Level</label>
                    <div class="col-sm-3">
                        <?= Html::dropDownList('level', null, [
                            'Dealer' => 'Dealer',
                            'Korwil' => 'Korwil',
                            'Nasional' => 'Nasional',
                            'Anper' => 'Anper',
                            'BHC' => 'BHC',
                        ], ['id' => 'level', 'text' => 'Pilih tipe laporan', 'class' => 'form-control']) ?>

                    </div>
                    <div class="col-sm-5" style="height: 50px" id="tampil">
                        <label class="control-label tombol"><i class="glyphicon glyphicon-new-window">
                                Tampil</i></label>
                    </div>

                </div>

                <div class="col-md-24"><br><br></div>
                <div class="row">
                    <!-- /.box 2-->
                    <div class="col-md-12">
                        <!-- NERACA -->
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Neraca</h3>
                            </div>
                            <i id="ajaxspinner1" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-4"></div>
                                <div class="col-md-16">
                                    <div class="1">
                                        <canvas id="myChartNeraca" ></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer"></div>
                    </div>
                    <div class="col-md-12">
                        <!-- ASSET -->
                        <div class="box box-primary direct-chat direct-chat-warning">
                            <i id="ajaxspinner2" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <!-- /.box-header -->
                            <div class="box-body"><div class="col-md-2"></div>
                                <div class="col-md-20" style="align-content: center">
                                    <table style = "overflow-y: auto;margin-top: 20px" class="table table-striped mb-0" id="table2">
                                        <thead style="position: sticky;top: 0;">
                                        <tr style="background-color: lightskyblue">
                                            <th colspan="2" style="text-align: left"><h2><strong>Total Assets</strong></h2></th>
                                            <th colspan="2" style="text-align: right"><h3><label id="TotalAssets"></label></h3></th>
                                        </tr>
                                        <tr style="background-color: lightblue;font-size: 16px">
                                            <th style="text-align: left">Cash & Bank</th>
                                            <th style="text-align: right"><label id="CashBank"></label></th>
                                            <th style="padding-left: 30px">Depositor, Adv & Prepay</th>
                                            <th style="text-align: right"><label id="Depositor"></label></th>
                                        </tr>
                                        <tr style="background-color: lightblue;font-size: 16px">
                                            <th style="text-align: left">Piutang</th>
                                            <th style="text-align: right"><label id="Piutang"></label></th>
                                            <th style="padding-left: 30px">Aktiva Tetap & Lain2</th>
                                            <th style="text-align: right"><label id="AktivaTetap"></label></th>
                                        </tr>
                                        <tr style="background-color: lightblue;font-size: 16px">
                                            <th style="text-align: left">Persediaan</th>
                                            <th style="text-align: right"><label id="Persediaan"></label></th>
                                            <th></th><th></th>
                                        </tr>
                                        </thead>
                                        <tbody id="asset"></tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="box-body"><div class="col-md-2"></div>
                                <div class="col-md-20" style="align-content: center">
                                    <table style = "overflow-y: auto;margin-top: 20px" class="table table-striped mb-0" id="table2">
                                        <thead style="position: sticky;top: 0;">
                                        <tr style="background-color: coral">
                                            <th colspan="2" style="text-align: left"><h2><strong>Total Liabilities</strong></h2></th>
                                            <th colspan="2" style="text-align: right"><h3><label id="TotalLiabilities"></label></h3></th>
                                        </tr>
                                        <tr style="background-color: lightcoral;font-size: 16px">
                                            <th style="text-align: left">Hutang</th>
                                            <th style="text-align: right"><label id="Hutang"></label></th>
                                            <th style="padding-left: 30px">Modal</th>
                                            <th style="text-align: right"><label id="Modal"></label></th>
                                        </tr>
                                        <tr style="background-color: lightcoral;font-size: 16px">
                                            <th style="text-align: left">Titipan</th>
                                            <th style="text-align: right"><label id="Titipan"></label></th>
                                            <th style="padding-left: 30px">Total Rugi Laba</th>
                                            <th style="text-align: right"><label id="TotalRugiLaba"></label></th>
                                        </tr>
                                        </thead>
                                        <tbody id="asset"></tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                </div>

                <div class="col-md-24"><br><br></div>
                <div class="row">
                    <!-- Working Capital -->
                    <div class="col-md-12">
                        <div class="box box-primary direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Working Capital</h3>
                            </div>
                            <i id="ajaxspinner3" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="2">
                                    <canvas id="myChartWorkingCapital"></canvas>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                    <!-- Total Equity -->
                    <div class="col-md-12">
                        <div class="box box-primary direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Total Equity</h3>
                            </div>
                            <i id="ajaxspinner4" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="3">
                                    <canvas id="myChartTotalEquity"></canvas>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                </div>

                <div class="row">
                    <!-- OPEX -->
                    <div class="col-md-12">
                        <div class="box box-primary direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">OPEX</h3>
                            </div>
                            <i id="ajaxspinner5" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-4"></div>
                                <div class="col-md-16">
                                <div class="4">
                                    <canvas id="myChartOpex"></canvas>
                                </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                    <!-- LABA RUGIy -->
                    <div class="col-md-12">
                        <div class="box box-primary direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Laba Rugi</h3>
                            </div>
                            <i id="ajaxspinner6" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="5">
                                    <canvas id="myChartLabaRugi"></canvas>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                </div>


            </div>
            <div class="box-footer">

            </div>
            <?= Html::endForm(); ?>
        </div>
    </div>
<?php
