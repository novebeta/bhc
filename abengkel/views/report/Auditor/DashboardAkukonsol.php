<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\checkbox\CheckboxX;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\helpers\Url;

$this->title = 'Dashboard Akuntansi Konsolidasi';
$this->params['breadcrumbs'][] = $this->title;
$formatter = \Yii::$app->formatter;


$format = <<< SCRIPT

SCRIPT;

$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
$urlProfit = Url::toRoute(['report/akukonsolprofit']);

$this->registerJs(<<< JS
    
    Chart.register(ChartDataLabels);
    function reloadGrid1(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
        // $('#ajaxspinner1').show();
        // $('#ajaxspinner2').show();
        // $('#ajaxspinner3').show();
                
        $.ajax({
            type: 'POST',
            url: '$urlProfit',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            success:function(response) {
                console.log(response['profit']);
                console.log(response['kota']);
                console.log(response['profitperunit']);
                console.log(response['penjualanpart']);
                console.log(response['penjualanoli']);
                console.log(response['penjualanjasa']);                
                
                
                $('#myChartProfitOperasional').remove();
                $('.2').append('<canvas id="myChartProfitOperasional"><canvas>');
                $('#myChartProfitnpbt').remove();
                $('.4').append('<canvas id="myChartProfitnpbt"><canvas>');
                $('#myChartBagan').remove();
                $('.8').append('<canvas id="myChartBagan"><canvas>');
                $('#myChartGajiTunjangan').remove();
                $('.9').append('<canvas id="myChartGajiTunjangan"><canvas>');
                $('#myChartInsentif').remove();
                $('.10').append('<canvas id="myChartInsentif"><canvas>');
                $('#myChartBiayaPemasaran').remove();
                $('.11').append('<canvas id="myChartBiayaPemasaran"><canvas>');
                //PROFIT OPS
                ctx1 = document.getElementById("myChartProfitOperasional").getContext("2d");
                myChartProfitOperasional = new Chart(ctx1, {
                                                data: {
                                                    labels: response['kota'],
                                                    datasets: [
                                                        {
                                                            type: "bar",
                                                            label: "profit",
                                                            backgroundColor: "coral",
                                                            borderColor: "coral",
                                                            borderWidth: 1,
                                                            borderRadius: 25,
                                                            data: response['profit']
                                                        },
                                                    ]
                                                },
                                                plugins: [ChartDataLabels],
                                                options: {
                                                     plugins: {
                                                        datalabels: {
                                                          display: false
                                                        }
                                                      },
                                                    responsive: true,
                                                    legend: {
                                                        position: "top"
                                                    },
                                                    title: {
                                                        display: true,
                                                        text: "Chart.js Bar Chart"
                                                    },
                                                    scales: {
                                                        yAxes: [{
                                                            ticks: {
                                                                beginAtZero: true
                                                            }
                                                        }]
                                                    },
                                                }
                                            });                
                //Profit npbt
                ctx3 = document.getElementById("myChartProfitnpbt").getContext("2d");
                myChartProfitnpbt = new Chart(ctx3, {
                                                data: {
                                                    labels: response['kota'],
                                                    datasets: [
                                                        {
                                                            type: "bar",
                                                            label: "ProfitPerUnit",
                                                            backgroundColor: "dodgerblue",
                                                            borderColor: "dodgerblue",
                                                            borderWidth: 1,
                                                            borderRadius: 25,
                                                            data: response['profitperunit']
                                                        },
                                                    ]
                                                },
                                                plugins: [ChartDataLabels],
                                                options: {                                                    
                                                     plugins: {
                                                        datalabels: {
                                                          display: false
                                                        }
                                                      },
                                                    responsive: true,
                                                    legend: {
                                                        position: "top"
                                                    },
                                                    title: {
                                                        display: true,
                                                        text: "Chart.js Bar Chart"
                                                    },
                                                    scales: {
                                                        yAxes: [{
                                                            ticks: {
                                                                beginAtZero: true
                                                            }
                                                        }]
                                                    },
                                                }
                                            });                
                
                //NPBT BAGAN
                ctx7 = document.getElementById("myChartBagan").getContext("2d");
                myChartBagan = new Chart(ctx7, {
                                                data: {
                                                    labels: response['kota'],
                                                    datasets: [
                                                        {
                                                            type: "bar",
                                                            label: "PenjualanPart",
                                                            backgroundColor: "coral",
                                                            borderColor: "coral",
                                                            borderWidth: 1,
                                                            borderRadius: 25,
                                                            data: response['penjualanpart']
                                                        },
                                                        {
                                                            type: "bar",
                                                            label: "PenjualanOli",
                                                            backgroundColor: "dodgerblue",
                                                            borderColor: "dodgerblue",
                                                            borderWidth: 1,
                                                            borderRadius: 25,
                                                            data: response['penjualanoli']
                                                        },
                                                        {
                                                            type: "bar",
                                                            label: "PenjualanJasa",
                                                            backgroundColor: "indianred",
                                                            borderColor: "indianred",
                                                            borderWidth: 1,
                                                            borderRadius: 25,
                                                            data: response['penjualanjasa']
                                                        },
                                                    ]
                                                },
                                                plugins: [ChartDataLabels],
                                                options: {
                                                    plugins: {
                                                        datalabels: {
                                                          display: false
                                                        }
                                                      },
                                                    responsive: true,
                                                    legend: {
                                                        position: "top"
                                                    },
                                                    title: {
                                                        display: true,
                                                        text: "Chart.js Bar Chart"
                                                    },
                                                    scales: {
                                                        yAxes: [{
                                                            ticks: {
                                                                beginAtZero: true
                                                            }
                                                        }]
                                                    },
                                                }
                                            });      
                
                
//                $('#ajaxspinner1').hide();
//                $('#ajaxspinner2').hide();
//                $('#ajaxspinner3').hide();
                
            }
        })
    }    
   $("#tampil").click(function() {
        reloadGrid1();
   }); 

JS
);



?>
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.0/dist/chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
<style>
    .tombol {
        background: #2C97DF;
        color: white;
        border-top: 0;
        border-left: 0;
        border-right: 0;
        border-bottom: 0;
        padding: 5px 10px;
        text-decoration: none;
        font-family: sans-serif;
        font-size: 11pt;
        border-radius: 4px;
    }

    .outer {
        overflow-y: auto;
        height: 100px;
    }

    .outer table {
        width: 100%;
        table-layout: fixed;
        border: 1px solid black;
        border-spacing: 1px;
    }

    .outer table th {
        text-align: left;
        top: 0;
        position: sticky;
        background-color: white;
    }
</style>
<div class="box box-primary">
    <?= Html::beginForm(); ?>
    <div class="box-body">
        <form id="tgl_query">
            <div class="form-group">
                <div class="box-body">
                    <div class="col-md-24">
                        <label class="control-label col-sm-1">Tanggal</label>
                        <div class="col-md-5">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <? try {
                                        echo DatePicker::widget([
                                            'name' => 'tgl1',
                                            'id' => 'tgl1',
                                            'type' => DatePicker::TYPE_INPUT,
                                            'value' => Yii::$app->formatter->asDate('now', '01/MM/yyyy'),
                                            'pluginOptions' => [
                                                'autoclose' => true,
                                                'format' => 'dd/mm/yyyy',
                                                'todayHighlight' => true
                                            ],
                                        ]);
                                    } catch (\yii\base\InvalidConfigException $e) {
                                    } ?>
                                </div>
                                <div class="col-md-12">
                                    <? try {
                                        echo DatePicker::widget([
                                            'name' => 'tgl2',
                                            'id' => 'tgl2',
                                            'type' => DatePicker::TYPE_INPUT,
                                            'value' => date('t/m/Y', strtotime("today")),
                                            'pluginOptions' => [
                                                'autoclose' => true,
                                                'format' => 'dd/mm/yyyy',
                                            ],

                                        ]);
                                    } catch (\yii\base\InvalidConfigException $e) {
                                    } ?>
                                </div>
                            </div>
                        </div>
                        <label class="control-label col-sm-1">Bulan</label>
                        <div class="col-md-3" style="margin-left: 5px">
                            <? try {
                                echo DatePicker::widget([
                                    'name' => 'TglKu',
                                    'id' => 'TglKu',
                                    'attribute2' => 'to_date',
                                    'type' => DatePicker::TYPE_INPUT,
                                    'value' => Yii::$app->formatter->asDate('now', 'MMMM  yyyy'),
                                    'pluginOptions' => [
                                        'startView' => 'year',
                                        'minViewMode' => 'months',
                                        'format' => 'MM  yyyy'
                                    ],
                                    'pluginEvents' => [
                                        "change" => new JsExpression("
                                                function(e) {
                                                 let d = new Date(Date.parse('01 ' + $(this).val()));											 
                                                 var tgl1 = $('#tgl1');
                                                 var tgl2 = $('#tgl2');                                               
                                                  tgl1.kvDatepicker('update', d);
                                                  tgl2.kvDatepicker('update',new Date(d.getFullYear(), d.getMonth()+1, 0)); 
                                                }                                                                                            
                                                "),
                                    ]
                                ]);
                            } catch (\yii\base\InvalidConfigException $e) {
                            } ?>
                        </div>
                        <div class="col-sm-2">
                            <? try {
                                echo CheckboxX::widget([
                                    'name' => 'eom',
                                    'value' => true,
                                    'options' => ['id' => 'eom'],
                                    'pluginOptions' => ['threeState' => false, 'size' => 'lg'],
                                    'pluginEvents' => [
                                        "change" => new JsExpression("function(e) {
                                                 var today = new Date();
                                                 var tgl2 = $('#tgl2');
                                                 let tglku = tgl2.kvDatepicker('getDate');
                                                 var lastDayOfMonth = new Date(tglku.getFullYear(), tglku.getMonth()+1, 0);
                                                if($(this).val() == 1){
                                                    tgl2.kvDatepicker('update', lastDayOfMonth)
                                                 }else{
                                                    tgl2.kvDatepicker('update', new Date(today.getFullYear(), today.getMonth(), today.getDate()));
                                                 }
                                                }")
                                    ]
                                ]);
                                echo '<label class="cbx-label" for="eom">EOM</label>';
                            } catch (\yii\base\InvalidConfigException $e) {
                            } ?>
                        </div>
                        <label class="control-label col-sm-1">Level</label>
                        <div class="col-sm-3">
                            <?= Html::dropDownList('level', null, [
                                'Nasional' => 'Nasional',
                                'BHC' => 'BHC',
                                'Korwil' => 'Korwil',
                                'Anper' => 'Anper',
                                'Dealer' => 'Dealer',
                            ], ['id' => 'level', 'text' => 'Pilih tipe laporan', 'class' => 'form-control']) ?>

                        </div>
                        <div class="col-sm-5" style="height: 50px" id="tampil">
                            <label class="control-label tombol"><i class="glyphicon glyphicon-new-window">
                                    Tampil</i></label>
                        </div>
                    </div>
                </div>
        </form>

        <!--JUDUL BAGAN-->
        <div class="col-md-24">
            <!--PROFIT OPERSIONAL-->
            <div class="box box-warning direct-chat direct-chat-warning">
                <div class="col-md-24">
                    <div class="box-header with-border">
<!--                        <i id="ajaxspinner1" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>-->
                        <div class="box-header with-border" style="text-align: center;">
                            <h3 class="box-title">PROFIT</h3><br>
                        </div>
                    </div>
                    <div class="box-body">
                        <div>
                            <div class="2">
                                <canvas id="myChartProfitOperasional"></canvas>
                            </div>
                        </div>
                        <script>
                        </script>
                    </div>
                    <div class="box-footer"></div>
                </div>
            </div>
        </div>

        <!--JUDUL BAGAN-->
        <div class="col-md-24">
            <div class="box box-warning direct-chat direct-chat-warning">
                <div class="col-md-24">
                    <div class="box-header with-border">
<!--                        <i id="ajaxspinner2" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>-->
                        <div class="box-header with-border" style="text-align: center;">
                            <h3 class="box-title">PROFIT PER UNIT</h3><br>
                        </div>
                    </div>
                    <div class="box-body">

                        <div>
                            <div class="4">
                                <canvas id="myChartProfitnpbt"></canvas>
                            </div>
                        </div>
                        <script>
                        </script>
                    </div>
                    <div class="box-footer"></div>
                </div>
            </div>
        </div>

        <!--JUDUL BAGAN-->
        <div class="col-md-24">
            <div class="box box-warning direct-chat direct-chat-warning">
                <div class="col-md-24">
                    <div class="box-header with-border">
<!--                        <i id="ajaxspinner4" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none;"></i>-->
                        <div class="box-header with-border" style="text-align: center;">
                            <h3 class="box-title">PENJUALAN</h3><br>
                        </div>
                    </div>
                    <div class="box-body">

                        <div>
                            <div class="8">
                                <canvas id="myChartBagan"></canvas>
                            </div>
                        </div>
                        <script>
                        </script>
                    </div>
                    <div class="box-footer"></div>
                </div>
            </div>
        </div>

        <?= Html::endForm(); ?>
    </div>
</div>
<?php
?>








