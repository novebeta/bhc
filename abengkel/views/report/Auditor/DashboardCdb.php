<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\checkbox\CheckboxX;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\helpers\Url;


$this->title = 'Dashboard Marketing';
$this->params['breadcrumbs'][] = $this->title;
$formatter = \Yii::$app->formatter;


$format = <<< SCRIPT

SCRIPT;

$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
$urlQueryLaporanDashboard = Url::toRoute(['report/lapdashboard']);
$urlJasaOli = Url::toRoute(['report/lapdashboard-jasaoli']);
$urlProduktifitas = Url::toRoute(['report/lapdashboard-produktifitas']);
$urlJasaGroup = Url::toRoute(['report/lapdashboard-jasagroup']);
$urlSalesTarget = Url::toRoute(['report/lapdashboard-salestarget']);
$urlDailyTarget = Url::toRoute(['report/lapdashboard-dailytarget']);
$urlBiayaoperasional = Url::toRoute(['report/lapdashboard-biayaoperasional']);
$urlDetailbiaya = Url::toRoute(['report/lapdashboard-detailbiaya']);
$urlPeformance = Url::toRoute(['report/lapdashboard-peformance']);


$url = Url::toRoute(['report/querycombo']);
$urlcmbkwl = Url::toRoute(['report/querycombokwl']);
$urlcmbmd = Url::toRoute(['report/querycombomd']);
$urlcmbdealer = Url::toRoute(['report/querycombodealer']);
$urlkwl = Url::toRoute(['report/getdatakwl']);

$this->registerJs(<<< JS
    
    
    

    Chart.register(ChartDataLabels);
    
    //Jasa Oli
    function reloadGrid1(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
        $('#ajaxspinner1').show();
        
        $.ajax({
            type: 'POST',
            url: '$urlJasaOli',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            global : false,
            success:function(response) {
                var jasaolihead = '';                
                jasaolihead = '<tr style="background-color: white">' +
                        '<th scope="col" style="text-align: center">Tipe</th>' +
                         '<th scope="col" style="text-align: center">'+response['ago']+'</th>' +
                          '<th scope="col" style="text-align: center">'+response['before']+'</th>' +
                           '<th scope="col" style="text-align: center">'+response['now']+'</th></tr>' 
                $('#jasaolihead').html(jasaolihead);
                
                var jasaoli = '';
                    var i;
                    for(i=0; i<response['jasaoli'].length; i++){
                       var ago = response['jasaoli'][i].JasaAgo.split('.00');	
                       var	ago_string = ago.toString(),
                           agosisa 	    = ago_string.length % 3,
                           agorupiah 	= ago_string.substr(0, agosisa),
                           agoribuan 	= ago_string.substr(agosisa).match(/\d{3}/g);                            
                       if (agoribuan) {
                           agoseparator = agosisa ? '.' : '';
                           agorupiah += agoseparator + agoribuan.join('.');
                       }
                       
                       var before = response['jasaoli'][i].JasaBefore.split('.00');	
                       var	before_string = before.toString(),
                           beforesisa 	    = before_string.length % 3,
                           beforerupiah 	= before_string.substr(0, beforesisa),
                           beforeribuan 	= before_string.substr(beforesisa).match(/\d{3}/g);                            
                       if (beforeribuan) {
                           beforeseparator = beforesisa ? '.' : '';
                           beforerupiah += beforeseparator + beforeribuan.join('.');
                       }
                       
                       var now = response['jasaoli'][i].JasaNow.split('.00');	
                       var	now_string = now.toString(),
                           nowsisa 	    = now_string.length % 3,
                           nowrupiah 	= now_string.substr(0, nowsisa),
                           nowribuan 	= now_string.substr(nowsisa).match(/\d{3}/g);                            
                       if (nowribuan) {
                           nowseparator = nowsisa ? '.' : '';
                           nowrupiah += nowseparator + nowribuan.join('.');
                       }
                   
                        jasaoli += '<tr align="right">'+
                                '<td align="left">'+response['jasaoli'][i].Tipe+'</td>'+
                                '<td>'+agorupiah+'</td>'+
                                '<td>'+beforerupiah+'</td>'+
                                '<td>'+nowrupiah+'</td>'+
                                '</tr>';
                     }
                    $('#jasaoli').html(jasaoli);
                    $('#ajaxspinner1').hide();
            }
            })
        
             
    }
    
    //Produktifitas
    function reloadGrid2(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
        $('#ajaxspinner2').show();
        
        $.ajax({
            type: 'POST',
            url: '$urlProduktifitas',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            global : false,
            success:function(response) {
                var produkhead = '';
                produkhead = '<tr style="background-color: white">' +
                        '<th scope="col" style="text-align: center">KarKode</th>' +
                         '<th scope="col" style="text-align: center">'+response['ago']+'</th>' +
                          '<th scope="col" style="text-align: center">'+response['before']+'</th>' +
                           '<th scope="col" style="text-align: center">'+response['now']+'</th></tr>' 
                $('#produkhead').html(produkhead);
                
                var produk = '';
                    var j;
                    for(j=0; j<response['produk'].length; j++){
                        produk += '<tr align="right">'+
                                '<td align="left">'+response['produk'][j].KarKode+'</td>'+
                                '<td>'+response['produk'][j].BlnAgo+'</td>'+
                                '<td>'+response['produk'][j].BlnBefore+'</td>'+
                                '<td>'+response['produk'][j].BlnNow+'</td>'+
                                '</tr>';
                    }
                    $('#produk').html(produk);
                    $('#ajaxspinner2').hide();
            }
            })
        
             
    }
    
    //Jasa Group
    function reloadGrid3(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
        $('#ajaxspinner3').show();
        
        $.ajax({
            type: 'POST',
            url: '$urlJasaGroup',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            global : false,
            success:function(response) {
                //JASA GROUP
                var jasagrouphead = '';
                
                jasagrouphead = '<tr style="background-color: white">' +
                        '<th scope="col" style="text-align: center">JasaGroup</th>' +
                         '<th scope="col" style="text-align: center">Unit '+response['ago']+'</th>' +
                         '<th scope="col" style="text-align: center">Nominal '+response['ago']+'</th>' +
                          '<th scope="col" style="text-align: center">Unit '+response['before']+'</th>' +
                          '<th scope="col" style="text-align: center">Nominal '+response['before']+'</th>' +
                           '<th scope="col" style="text-align: center">Unit '+response['now']+'</th>'+
                           '<th scope="col" style="text-align: center">Nominal '+response['now']+'</th></tr>' 
                $('#jasagrouphead').html(jasagrouphead);
                
                var jasagroup = '';
                    var k;
                    for(k=0; k<response['jasagroup'].length; k++){
                       var nago = response['jasagroup'][k].NominalAgo;	
                       var	nagostring = nago.toString(),
                            nago_string = nagostring.split('.')[0];
                           nagosisa 	= nago_string.length % 3,
                           nagorupiah 	= nago_string.substr(0, nagosisa),
                           nagoribuan 	= nago_string.substr(nagosisa).match(/\d{3}/g);                            
                       if (nagoribuan) {
                           nagoseparator = nagosisa ? '.' : '';
                           nagorupiah += nagoseparator + nagoribuan.join('.');
                       }
                       
                       var nbefore = response['jasagroup'][k].NominalBefore;	
                       var nstring = nbefore.toString(),
                           nbefore_string = nstring.split('.')[0];
                           nbeforesisa 	    = nbefore_string.length % 3,
                           nbeforerupiah 	= nbefore_string.substr(0, nbeforesisa),
                           nbeforeribuan 	= nbefore_string.substr(nbeforesisa).match(/\d{3}/g);                            
                       if (nbeforeribuan) {
                           nbeforeseparator = nbeforesisa ? '.' : '';
                           nbeforerupiah += nbeforeseparator + nbeforeribuan.join('.');
                       }
                       
                       var nnow = response['jasagroup'][k].NominalNow;	
                       var	nnowstring = nnow.toString(),
                            nnow_string = nnowstring.split('.')[0];
                           nnowsisa 	= nnow_string.length % 3,
                           nnowrupiah 	= nnow_string.substr(0, nnowsisa),
                           nnowribuan 	= nnow_string.substr(nnowsisa).match(/\d{3}/g);                            
                       if (nnowribuan) {
                           nnowseparator = nnowsisa ? '.' : '';
                           nnowrupiah += nnowseparator + nnowribuan.join('.');
                       }
                        
                        jasagroup += '<tr align="right">'+
                                '<td align="left">'+response['jasagroup'][k].JasaGroup+'</td>'+
                                '<td>'+response['jasagroup'][k].UnitAgo+'</td>'+
                                '<td>'+nagorupiah+'</td>'+
                                '<td>'+response['jasagroup'][k].UnitBefore+'</td>'+
                                '<td>'+nbeforerupiah+'</td>'+
                                '<td>'+response['jasagroup'][k].UnitNow+'</td>'+
                                '<td>'+nnowrupiah+'</td>'+
                                '</tr>';
                    }
                    $('#jasagroup').html(jasagroup);
                    $('#ajaxspinner3').hide();
            }
            })
        
             
    }
    
    //Sales VS Target
    function reloadGrid4(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
        $('#ajaxspinner4').show();
        
        $.ajax({
            type: 'POST',
            url: '$urlSalesTarget',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            global : false,
            success:function(response) {
                //SALES VS TARGET
                var sales = '';
                    var l;
                    for(l=0; l<response['sales'].length; l++){
                        sales += '<tr align="right">'+
                                '<td align="left">'+response['sales'][l].KarKode+'</td>'+
                                '<td>'+response['sales'][l].KarNama+'</td>'+
                                '<td>'+response['sales'][l].Target+'</td>'+
                                '<td>'+response['sales'][l].UnitEntry+'</td>'+
                                '<td>'+response['sales'][l].Persen+'</td>'+
                                '<td>'+response['sales'][l].Selisih+'</td>'+
                                '</tr>';
                    }
                    $('#sales').html(sales);
                    $('#ajaxspinner4').hide();
            }
            })
        
             
    }
    
    //Daily Target
    function reloadGrid5(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
        $('#ajaxspinner5').show();
        
        $.ajax({
            type: 'POST',
            url: '$urlDailyTarget',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            global : false,
            success:function(response) {
                $('#ChartDaily').remove();
                $('.1').append('<canvas id="ChartDaily"><canvas>');
                ctx1 = document.getElementById("ChartDaily").getContext("2d");
                ChartDaily = new Chart(ctx1, {
                                            data: {
                                                labels: response['labeldaily'],
                                                datasets: [
                                                    {
                                                        type: "bar",
                                                        label: "Actual",
                                                        backgroundColor: "orange",
                                                        borderColor: "orange",
                                                        borderWidth: 1,
                                                        data: response['actualdaily']
                                                    },
                                                    {
                                                        type: "line",
                                                        label: "Daily Target",
                                                        backgroundColor: "Aquamarine",
                                                        borderColor: "Aquamarine",
                                                        borderWidth: 5,
                                                        data: response['targetdaily']                                                      
                                                    },
                                                ]
                                            },
                                            plugins: [ChartDataLabels],
                                            options: {
                                                responsive: true,
                                                legend: {
                                                    position: "top"
                                                },
                                                title: {
                                                    display: true,
                                                    text: "Chart.js Bar Chart"
                                                },
                                                scales: {
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }]
                                                },
                                            }
                                        });
                $('#ajaxspinner5').hide();
            }
            })
        
             
    }
    
    //Biaya Operasional
    function reloadGrid6(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
        $('#ajaxspinner6').show();
        
        $.ajax({
            type: 'POST',
            url: '$urlBiayaoperasional',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            global : false,
            success:function(response) {
                 $('#ChartBiaya').remove(); 
                    $('.2').append('<canvas id="ChartBiaya"><canvas>');
                
                //BIAYA OPERASIONAL
                ctx2 = document.getElementById("ChartBiaya").getContext("2d");
                ChartBiaya = new Chart(ctx2, {
                                            data: {
                                                labels: response['labelbiaya'],
                                                datasets: [
                                                    {
                                                        type: "bar",
                                                        label: " ",
                                                        backgroundColor: "purple",
                                                        borderColor: "purple",
                                                        borderWidth: 1,
                                                        data: response['nilaibiaya']
                                                    },
                                                ]
                                            },
                                            plugins: [ChartDataLabels],
                                            options: {
                                                responsive: true,
                                                legend: {
                                                    position: "top"
                                                },
                                                title: {
                                                    display: true,
                                                    text: "Chart.js Bar Chart"
                                                },
                                                scales: {
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }]
                                                },
                                            }
                                        }); 
                $('#ajaxspinner6').hide();
            }
            })
        
             
    }
    
    //Detail Biaya Operasional
    function reloadGrid7(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
        $('#ajaxspinner7').show();
        
        $.ajax({
            type: 'POST',
            url: '$urlDetailbiaya',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            global : false,
            success:function(response) {
                 //DETAIL BIAYA OPERASIONAL
                var dtbiayahead = '';
                
                dtbiayahead = '<tr style="background-color: white">' +
                        '<th scope="col" style="text-align: center">NamaAccount</th>' +
                         '<th scope="col" style="text-align: center">'+response['ago']+'</th>' +
                          '<th scope="col" style="text-align: center">'+response['before']+'</th>' +
                           '<th scope="col" style="text-align: center">'+response['now']+'</th></tr>' 
                $('#dtbiayahead').html(dtbiayahead);
                           
                var dtbiaya = '';
                    var m;
                    for(m=0; m<3; m++){
                        var bago = response['dtbiaya'][m].BlnAgo;	
                       var	bagostring = bago.toString(),
                            bago_string = bagostring.split('.')[0];
                           bagosisa 	= bago_string.length % 3,
                           bagorupiah 	= bago_string.substr(0, bagosisa),
                           bagoribuan 	= bago_string.substr(bagosisa).match(/\d{3}/g);                            
                       if (bagoribuan) {
                           bagoseparator = bagosisa ? '.' : '';
                           bagorupiah += bagoseparator + bagoribuan.join('.');
                       }
                       
                       var bbefore = response['dtbiaya'][m].BlnBefore;	
                       var	bbeforestring = bbefore.toString(),
                       bbefore_string = bbeforestring.split('.')[0];
                           bbeforesisa 	    = bbefore_string.length % 3,
                           bbeforerupiah 	= bbefore_string.substr(0, bbeforesisa),
                           bbeforeribuan 	= bbefore_string.substr(bbeforesisa).match(/\d{3}/g);                            
                       if (bbeforeribuan) {
                           bbeforeseparator = bbeforesisa ? '.' : '';
                           bbeforerupiah += bbeforeseparator + bbeforeribuan.join('.');
                       }
                       
                       var bnow = response['dtbiaya'][m].BlnNow;	
                       var	bnowstring = bnow.toString(),
                       bnow_string = bnowstring.split('.')[0];
                           bnowsisa 	= bnow_string.length % 3,
                           bnowrupiah 	= bnow_string.substr(0, bnowsisa),
                           bnowribuan 	= bnow_string.substr(bnowsisa).match(/\d{3}/g);                            
                       if (bnowribuan) {
                           bnowseparator = bnowsisa ? '.' : '';
                           bnowrupiah += bnowseparator + bnowribuan.join('.');
                       }
                       
                       
                       
                       
                        dtbiaya += 
                           '<tr align="right">'+
                                '<td align="left">'+response['dtbiaya'][m].NamaAccount+'</td>'+
                                '<td>'+bagorupiah+'</td>'+
                                '<td>'+bbeforerupiah+'</td>'+
                                '<td>'+response['dtbiaya'][m].BlnNow+'</td>'+
                                '</tr>';
                    }
                    $('#dtbiaya').html(dtbiaya);
                $('#ajaxspinner7').hide();
            }
            })
        
             
    }
    
    //Peformance
    function reloadGrid8(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
        $('#ajaxspinner8').show();
        
        $.ajax({
            type: 'POST',
            url: '$urlPeformance',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            global : false,
            success:function(response) {     
                //PEFORMANCE
                var peformance = '';
                    var n;
                    for(n=0; n<response['peformance'].length; n++){
                        
                       var gp = response['peformance'][n].GP.split('.00');		
                       var	gp_string = gp.toString(),
                           gpsisa 	= gp_string.length % 3,
                           gprupiah 	= gp_string.substr(0, gpsisa),
                           gpribuan 	= gp_string.substr(gpsisa).match(/\d{3}/g);                            
                       if (gpribuan) {
                           gpseparator = gpsisa ? '.' : '';
                           gprupiah += gpseparator + gpribuan.join('.');
                       }
                       
                       var OliCounter = response['peformance'][n].OliCounter.split('.00');	
                       var	OliCounter_string = OliCounter.toString(),
                           OliCountersisa 	= OliCounter_string.length % 3,
                           OliCounterrupiah 	= OliCounter_string.substr(0, OliCountersisa),
                           OliCounterribuan 	= OliCounter_string.substr(OliCountersisa).match(/\d{3}/g);                            
                       if (OliCounterribuan) {
                           OliCounterseparator = OliCountersisa ? '.' : '';
                           OliCounterrupiah += OliCounterseparator + OliCounterribuan.join('.');
                       }
                       
                       var OliSD = response['peformance'][n].OliSD.split('.00');	
                       var	OliSD_string = OliSD.toString(),
                           OliSDsisa 	= OliSD_string.length % 3,
                           OliSDrupiah 	= OliSD_string.substr(0, OliSDsisa),
                           OliSDribuan 	= OliSD_string.substr(OliSDsisa).match(/\d{3}/g);                            
                       if (OliSDribuan) {
                           OliSDseparator = OliSDsisa ? '.' : '';
                           OliSDrupiah += OliSDseparator + OliSDribuan.join('.');
                       }
                       
                       var PartCounter = response['peformance'][n].PartCounter.split('.00');	
                       var	PartCounter_string = PartCounter.toString(),
                           PartCountersisa 	= PartCounter_string.length % 3,
                           PartCounterrupiah 	= PartCounter_string.substr(0, PartCountersisa),
                           PartCounterribuan 	= PartCounter_string.substr(PartCountersisa).match(/\d{3}/g);                            
                       if (PartCounterribuan) {
                           PartCounterseparator = PartCountersisa ? '.' : '';
                           PartCounterrupiah += PartCounterseparator + PartCounterribuan.join('.');
                       }
                       
                       var PartSD = response['peformance'][n].PartSD.split('.00');	
                       var	PartSD_string = PartSD.toString(),
                           PartSDsisa 	= PartSD_string.length % 3,
                           PartSDrupiah 	= PartSD_string.substr(0, PartSDsisa),
                           PartSDribuan 	= PartSD_string.substr(PartSDsisa).match(/\d{3}/g);                            
                       if (PartSDribuan) {
                           PartSDseparator = PartSDsisa ? '.' : '';
                           PartSDrupiah += PartSDseparator + PartSDribuan.join('.');
                       }
                       
                       var Jasa = response['peformance'][n].Jasa.split('.00');	
                       var	Jasa_string = Jasa.toString(),
                           Jasasisa 	= Jasa_string.length % 3,
                           Jasarupiah 	= Jasa_string.substr(0, Jasasisa),
                           Jasaribuan 	= Jasa_string.substr(Jasasisa).match(/\d{3}/g);                            
                       if (Jasaribuan) {
                           Jasaseparator = Jasasisa ? '.' : '';
                           Jasarupiah += Jasaseparator + Jasaribuan.join('.');
                       }
                       
                        
                        peformance += '<tr align="right">'+
                                '<td align="left">'+response['peformance'][n].Tanggal+'</td>'+
                                '<td>'+response['peformance'][n].HariKerja+'</td>'+
                                '<td>'+response['peformance'][n].UnitEntry+'</td>'+
                                '<td>'+response['peformance'][n].KPB1+'</td>'+
                                '<td>'+response['peformance'][n].KPB234+'</td>'+
                                '<td>'+response['peformance'][n].KPBPersen+'</td>'+
                                '<td>'+Jasarupiah+'</td>'+
                                '<td>'+PartSDrupiah+'</td>'+
                                '<td>'+PartCounterrupiah+'</td>'+
                                '<td>'+OliSDrupiah+'</td>'+
                                '<td>'+OliCounterrupiah+'</td>'+
                                '<td>'+gprupiah+'</td>'+
                                '</tr>';
                    }
                    $('#peformance').html(peformance);   
                $('#ajaxspinner8').hide();
            }
            })
        
             
    }
    
   $("#tampil").click(function() {
        reloadGrid1();
        reloadGrid2();
        reloadGrid3();
        reloadGrid4();
        reloadGrid5();
        reloadGrid6();
        reloadGrid7();
        reloadGrid8();
   });
   
   
            
JS
);

$sqlMd = $datamd;
$cmbMd = ArrayHelper::map($sqlMd, 'MD', 'MD');

$sqlKorwil = $data;
$cmbKorwil = ArrayHelper::map($sqlKorwil, 'Korwil', 'Korwil');

$sqlDealer = $datadealer;
$cmbDealer = ArrayHelper::map($sqlDealer, 'Dealer', 'Dealer');
?>
    <!--<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>-->
    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.0/dist/chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
    <style>
        .tombol {
            background: #2C97DF;
            color: white;
            border-top: 0;
            border-left: 0;
            border-right: 0;
            border-bottom: 0;
            padding: 5px 10px;
            text-decoration: none;
            font-family: sans-serif;
            font-size: 11pt;
            border-radius: 4px;
        }

        .outer {
            overflow-y: auto;
            height: 100px;
        }

        .outer table {
            width: 100%;
            table-layout: fixed;
            border: 1px solid black;
            border-spacing: 1px;
        }

        .outer table th {
            text-align: left;
            top: 0;
            position: sticky;
            background-color: white;
        }
    </style>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
            <?= Html::beginForm(); ?>

            <div class="box-body">
                <form id="tgl_query">
                    <div class="form-group">
                        <div class="box-body">
                            <div class="col-md-24">
                                <label class="control-label col-sm-1">Tanggal</label>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <? try {
                                                echo DatePicker::widget([
                                                    'name' => 'tgl1',
                                                    'id' => 'tgl1',
                                                    'type' => DatePicker::TYPE_INPUT,
                                                    'value' => Yii::$app->formatter->asDate('now', '01/MM/yyyy'),
                                                    'pluginOptions' => [
                                                        'autoclose' => true,
                                                        'format' => 'dd/mm/yyyy',
                                                        'todayHighlight' => true
                                                    ],
                                                ]);
                                            } catch (\yii\base\InvalidConfigException $e) {
                                            } ?>
                                        </div>
                                        <div class="col-md-12">
                                            <? try {
                                                echo DatePicker::widget([
                                                    'name' => 'tgl2',
                                                    'id' => 'tgl2',
                                                    'type' => DatePicker::TYPE_INPUT,
                                                    'value' => date('t/m/Y', strtotime("today")),
                                                    'pluginOptions' => [
                                                        'autoclose' => true,
                                                        'format' => 'dd/mm/yyyy',
                                                    ],

                                                ]);
                                            } catch (\yii\base\InvalidConfigException $e) {
                                            } ?>
                                        </div>
                                    </div>
                                </div>
                                <label class="control-label col-sm-1">Bulan</label>
                                <div class="col-md-3" style="margin-left: 5px">
                                    <? try {
                                        echo DatePicker::widget([
                                            'name' => 'TglKu',
                                            'id' => 'TglKu',
                                            'attribute2' => 'to_date',
                                            'type' => DatePicker::TYPE_INPUT,
                                            'value' => Yii::$app->formatter->asDate('now', 'MMMM  yyyy'),
                                            'pluginOptions' => [
                                                'startView' => 'year',
                                                'minViewMode' => 'months',
                                                'format' => 'MM  yyyy'
                                            ],
                                            'pluginEvents' => [
                                                "change" => new JsExpression("
                                                function(e) {
                                                 let d = new Date(Date.parse('01 ' + $(this).val()));											 
                                                 var tgl1 = $('#tgl1');
                                                 var tgl2 = $('#tgl2');                                               
                                                  tgl1.kvDatepicker('update', d);
                                                  tgl2.kvDatepicker('update',new Date(d.getFullYear(), d.getMonth()+1, 0)); 
                                                }                                                                                            
                                                "),
                                            ]
                                        ]);
                                    } catch (\yii\base\InvalidConfigException $e) {
                                    } ?>
                                </div>
                                <div class="col-sm-2">
                                    <? try {
                                        echo CheckboxX::widget([
                                            'name' => 'eom',
                                            'value' => true,
                                            'options' => ['id' => 'eom'],
                                            'pluginOptions' => ['threeState' => false, 'size' => 'lg'],
                                            'pluginEvents' => [
                                                "change" => new JsExpression("function(e) {
                                                 var today = new Date();
                                                 var tgl2 = $('#tgl2');
                                                 let tglku = tgl2.kvDatepicker('getDate');
                                                 var lastDayOfMonth = new Date(tglku.getFullYear(), tglku.getMonth()+1, 0);
                                                if($(this).val() == 1){
                                                    tgl2.kvDatepicker('update', lastDayOfMonth)
                                                 }else{
                                                    tgl2.kvDatepicker('update', new Date(today.getFullYear(), today.getMonth(), today.getDate()));
                                                 }
                                                }")
                                            ]
                                        ]);
                                        echo '<label class="cbx-label" for="eom">EOM</label>';
                                    } catch (\yii\base\InvalidConfigException $e) {
                                    } ?>
                                </div>

                                <label class="control-label col-sm-1">Level</label>
                                <div class="col-sm-5">
                                    <?= Html::dropDownList('level', null, [
                                        'Dealer' => 'Dealer',
                                        'Korwil' => 'Korwil',
                                        'Nasional' => 'Nasional',
                                        'Anper' => 'Anper',
                                        'BHC' => 'BHC',
                                    ], ['id' => 'level', 'text' => 'Pilih tipe laporan', 'class' => 'form-control']) ?>

                                </div>
                                <div class="col-sm-5" style="height: 50px" id="tampil">
                                    <label class="control-label tombol"><i class="glyphicon glyphicon-new-window">
                                            Tampil</i></label>
                                </div>
                            </div>
                        </div>
                </form>


                <div class="col-md-24">
                    <div class="col-md-12">
                        <div class="col-md-24">
                            <div class="box box-warning direct-chat direct-chat-warning">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Jasa Oli Part per Unit</h3>

                                    <div class="box-tools pull-right">
                                        <button class="btn" id="exceljasaoli" type="button" formtarget="_blank "><i
                                                    class="fas fa-download"></i></button>
                                        <button class="btn" id="showjasaoli" type="button" formtarget="_blank "><i class="fa fa-file-text"></i></button>
                                    </div>
                                    <i id="ajaxspinner1" class="fas fa-spinner fa-spin fa-3x fa-fw pull-right" style="display:none"></i>
                                </div>
                                <div class="box-body">
                                    <div style="position: relative;height: 150px;overflow: auto;display: block;">
                                        <table class="table table-bordered table-striped mb-0" id="tablejasaoli">
                                            <thead id="jasaolihead">

                                            </thead>
                                            <tbody id="jasaoli"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-24">
                            <div class="box-header with-border">
                                <h3 class="box-title">Produktifitas</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn" id="excelproduktifitas" type="button" formtarget="_blank "><i
                                                class="fas fa-download"></i></button>
                                    <button class="btn" id="showproduktifitas" type="button" formtarget="_blank "><i
                                                class="fa fa-file-text"></i></button>
                                </div>
                                <i id="ajaxspinner2" class="fas fa-spinner fa-spin fa-3x fa-fw pull-right" style="display:none"></i>
                            </div>
                            <div class="box-body">
                                <div style="position: relative;height: 150px;overflow: auto;display: block;">
                                    <table class="table table-bordered table-striped mb-0" id="tableproduktifitas">
                                        <thead id="produkhead">

                                        </thead>
                                        <tbody id="produk"></tbody>
                                    </table>

                                </div>


                            </div>
                            <div class="box-footer"></div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Jasa Group</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn" id="exceljasagroup" type="button" formtarget="_blank "><i
                                                class="fas fa-download"></i></button>
                                    <button class="btn" id="showjasagroup" type="button" formtarget="_blank "><i
                                                class="fa fa-file-text"></i></button>
                                </div>
                                <i id="ajaxspinner3" class="fas fa-spinner fa-spin fa-3x fa-fw pull-right" style="display:none"></i>
                            </div>
                            <div class="box-body">
                                <div style="position: relative;height: 300px;overflow: auto;display: block;">
                                    <table style="overflow-y: auto;" class="table table-bordered table-striped mb-0"
                                           id="tablejasagroup">
                                        <thead style="position: sticky;top: 0;" id="jasagrouphead">

                                        </thead>
                                        <tbody id="jasagroup"></tbody>
                                    </table>

                                </div>
                            </div>
                            <div class="box-footer"></div>
                        </div>

                    </div>
                </div>

                <div class="col-md-24">
                    <div class="col-md-12">
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Sales VS Target</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn" id="excelsales" type="button" formtarget="_blank "><i
                                                class="fas fa-download"></i></button>
                                    <button class="btn" id="showsales" type="button" formtarget="_blank "><i
                                                class="fa fa-file-text"></i></button>
                                </div>
                                <i id="ajaxspinner4" class="fas fa-spinner fa-spin fa-3x fa-fw pull-right" style="display:none"></i>
                            </div>
                            <div class="box-body">
                                <div style="position: relative;height: 300px;overflow: auto;display: block;">
                                    <table style="overflow-y: auto;" class="table table-bordered table-striped mb-0"
                                           id="tablesales">
                                        <thead style="position: sticky;top: 0;">
                                        <tr style="background-color: white">
                                            <th scope="col" style="text-align: center">KarKode</th>
                                            <th scope="col" style="text-align: center">KarNama</th>
                                            <th scope="col" style="text-align: center">Target</th>
                                            <th scope="col" style="text-align: center">UnitEntry</th>
                                            <th scope="col" style="text-align: center">Persen</th>
                                            <th scope="col" style="text-align: center">Selisih</th>
                                        </tr>
                                        </thead>
                                        <tbody id="sales"></tbody>
                                    </table>

                                </div>
                            </div>
                            <div class="box-footer"></div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Daily Target</h3>
                                <div class="box-tools pull-right">
                                   <button class="btn" id="showdaily" type="button" formtarget="_blank "><i
                                                class="fa fa-file-text"></i></button>
                                </div>
                                <i id="ajaxspinner5" class="fas fa-spinner fa-spin fa-3x fa-fw pull-right" style="display:none"></i>
                            </div>
                            <div class="box-body">
                                <div>
                                    <div class="1">
                                        <canvas id="ChartDaily"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer"></div>
                        </div>

                    </div>
                </div>

                <div class="col-md-24">
                    <div class="col-md-12">
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Biaya Operasional</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn" id="showbiaya" type="button" formtarget="_blank "><i
                                                class="fa fa-file-text"></i></button>
                                </div>
                                <i id="ajaxspinner6" class="fas fa-spinner fa-spin fa-3x fa-fw pull-right" style="display:none"></i>
                            </div>
                            <div class="box-body">
                                <div>
                                    <div class="2">
                                        <canvas id="ChartBiaya"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer"></div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Detail Biaya Operasional</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn" id="exceldetail" type="button" formtarget="_blank "><i
                                                class="fas fa-download"></i></button>
                                    <button class="btn" id="showdetail" type="button" formtarget="_blank "><i
                                                class="fa fa-file-text"></i></button>
                                </div>
                                <i id="ajaxspinner7" class="fas fa-spinner fa-spin fa-3x fa-fw pull-right" style="display:none"></i>
                            </div>
                            <div class="box-body">
                                <div style="position: relative;height: 300px;overflow: auto;display: block;">
                                    <table style="overflow-y: auto;" class="table table-bordered table-striped mb-0"
                                           id="tabledetail">
                                        <thead style="position: sticky;top: 0;" id="dtbiayahead">
                                        </thead>
                                        <tbody id="dtbiaya"></tbody>
                                    </table>

                                </div>
                            </div>
                            <div class="box-footer"></div>
                        </div>

                    </div>
                </div>

                <div class="col-md-24">
                    <div class="col-md-24">
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Peformance</h3>
                                <div class="pull-right">
                                    <button class="btn" id="excelpeformance" type="button" formtarget="_blank "><i
                                                class="fas fa-download"></i></button>
                                    <button class="btn" id="showpeformance" type="button" formtarget="_blank "><i
                                                class="fa fa-file-text"></i></button>
                                </div>
                                <i id="ajaxspinner8" class="fas fa-spinner fa-spin fa-3x fa-fw pull-right" style="display:none"></i>
                            </div>
                            <div class="box-body">
                                <div style="position: relative;height: 200px;overflow: auto;display: block;">
                                    <table style="overflow-y: auto;" class="table table-bordered table-striped mb-0"
                                           id="tablepeformance">
                                        <thead style="position: sticky;top: 0;">
                                        <tr style="background-color: white">
                                            <th scope="col" style="text-align: center">Tanggal</th>
                                            <th scope="col" style="text-align: center">HariKerja</th>
                                            <th scope="col" style="text-align: center">UnitEntry</th>
                                            <th scope="col" style="text-align: center">KPB1</th>
                                            <th scope="col" style="text-align: center">KPB234</th>
                                            <th scope="col" style="text-align: center">%KPB</th>
                                            <th scope="col" style="text-align: center">Jasa</th>
                                            <th scope="col" style="text-align: center">PartSD</th>
                                            <th scope="col" style="text-align: center">PartCounter</th>
                                            <th scope="col" style="text-align: center">OliSD</th>
                                            <th scope="col" style="text-align: center">OliCounter</th>
                                            <th scope="col" style="text-align: center">GP</th>
                                        </tr>
                                        </thead>
                                        <tbody id="peformance"></tbody>
                                    </table>

                                </div>
                            </div>
                            <div class="box-footer"></div>
                        </div>

                    </div>
                </div>
                <?= Html::endForm(); ?>
            </div>
        </div>
<?php
$urljasaoli = Url::toRoute(['report/show-detail', 'param' => '"JPOPerUnit"']);
$urlproduktifitas = Url::toRoute(['report/show-detail', 'param' => '"Produktivitas"']);
$urljasagroup = Url::toRoute(['report/show-detail', 'param' => '"JasaGroup"']);
$urlsales = Url::toRoute(['report/show-detail', 'param' => '"Level12"']);
$urldetail = Url::toRoute(['report/show-detail', 'param' => '"Grade12"']);
$urlpeformance = Url::toRoute(['report/show-detail', 'param' => '"Performance"']);
$urlbiaya = Url::toRoute(['report/show-detail', 'param' => '"Grade06"']);
$urldaily = Url::toRoute(['report/show-detail', 'param' => '"Level06"']);


$user = $_SESSION['__id'];
$this->registerJsVar('userid', $user);

$this->registerJs(<<< JS
        
        $('#showjasaoli').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            var level = $('#level').val();
            var eom = $('#eom').val();
            window.open('{$urljasaoli}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","","",""', '_blank');
        });
        $('#showproduktifitas').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            var level = $('#level').val();
            var eom = $('#eom').val();
            window.open('{$urlproduktifitas}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","","",""', '_blank');
        });
        $('#showjasagroup').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            var level = $('#level').val();
            var eom = $('#eom').val();
            window.open('{$urljasagroup}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","","",""', '_blank');
        });
        $('#showsales').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            var level = $('#level').val();
            var eom = $('#eom').val();
            window.open('{$urlsales}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","","",""', '_blank');
        });
        $('#showdetail').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            var level = $('#level').val();
            var eom = $('#eom').val();
            window.open('{$urldetail}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","","",""', '_blank');
        });         
        $('#showpeformance').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            var level = $('#level').val();
            var eom = $('#eom').val();
            window.open('{$urlpeformance}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","","",""', '_blank');
        });         
        $('#showbiaya').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            var level = $('#level').val();
            var eom = $('#eom').val();
            window.open('{$urlbiaya}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","","",""', '_blank');
        });         
        $('#showdaily').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            var level = $('#level').val();
            var eom = $('#eom').val();
            window.open('{$urldaily}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","","",""', '_blank');
        });      
        
        
        $(document).ready(function () {
      $("#exceljasaoli").click(function(){
        TableToExcel.convert(document.getElementById("tablejasaoli"), {
            name: "Jasa Oli Part per Unit.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
      $("#excelproduktifitas").click(function(){
        TableToExcel.convert(document.getElementById("tableproduktifitas"), {
            name: "Produktifitas.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });      
      $("#exceljasagroup").click(function(){
        TableToExcel.convert(document.getElementById("tablejasagroup"), {
            name: "Jasa Group.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
      $("#excelsales").click(function(){
        TableToExcel.convert(document.getElementById("tablesales"), {
            name: "Sales vs Target.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
      $("#exceldetail").click(function(){
        TableToExcel.convert(document.getElementById("tabledetail"), {
            name: "Detail Biaya Operasional.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
      $("#excelpeformance").click(function(){
        TableToExcel.convert(document.getElementById("tablepeformance"), {
            name: "Peformance.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
      
  });

JS
);











