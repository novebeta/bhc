jQuery(function ($) {
    $("#tipe-id").change(function () {
        var tipe = this.value;
        if (tipe === "Saldo Piutang" || tipe === "Umur Piutang - Curent" || tipe === "Umur Piutang - Overdue") {
            $(".tgl1Hide").hide();
        } else {
            $(".tgl1Hide").show();
        }
        if (tipe === "Saldo Piutang") {
            $(".lunasHide").show();
        } else {
            $(".lunasHide").hide();
        }
        if (tipe === "Saldo Piutang" || tipe === "Belum Di Link") {
            $(".cbsensitifHide").show();
        } else {
            $(".cbsensitifHide").hide();
        }
        if (tipe === "Riwayat Piutang") {
            $(".nobuktiHide").show();
        } else {
            $(".nobuktiHide").hide();
        }
        if (tipe === "Umur Piutang - Curent" || tipe === "Umur Piutang - Overdue") {
            $(".lblRangeHide").show();
        } else {
            $(".lblRangeHide").hide();
        }
        if (tipe === "Umur Piutang - Overdue") {
            $(".numRangeHide").show();
        } else {
            $(".numRangeHide").hide();
        }
        if (tipe === "Umur Piutang - Curent") {
            $(".RangetglHide").show();
            $('#tgl2').trigger('change');
        } else {
            $(".RangetglHide").hide();
        }
    });
    $('#tipe-id').trigger('change');
    $('#r1b').change(function () {
        $('#r2a').utilNumberControl().val(parseInt($(this).val()) + 1);
    });
    $('#r2b').change(function () {
        $('#r3a').utilNumberControl().val(parseInt($(this).val()) + 1);
    });
});

