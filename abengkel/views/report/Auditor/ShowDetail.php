<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use kartik\checkbox\CheckboxX;
use yii\web\JsExpression;

\abengkel\assets\AppAsset::register($this);
\abengkel\assets\JqwidgetsAsset::register($this);
$this->title = $title;
$formatter = \Yii::$app->formatter;
$this->registerCss(".jqx-widget-content { font-size: 12px; }");
$this->registerJs(<<< JS
 
JS
);

?>

<head>
    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
    <style>
        .table td{
            border: lightgrey solid 1px !important;
        }
        .table th{
            border: lightgrey solid 1px !important;
        }
    </style>
</head>
    <div class="laporan-query col-md-24" style="padding-left: 0;">
        <div class="box box-default">

            <button class="btn pull-right" id='btnExcel' type="button" style="background-color:limegreen;width: 40px;height: 30px;margin: 10px;position: absolute;top: -45px;right: 10px"><i class="fa fa-file-excel-o fa-lg"></i></button>
            
            <div class="box-body">
                <div class="col-md-24">

                    <div>
                        <div class="box-body">

                            <div style="position: relative;height: 650px;overflow: auto;width: auto">
                                    <?php
                                    switch ($no) {
                                        case "1":
                                            echo '
                                            <table class="table table-bordered table-striped mb-0" id="table1">
                                                <thead><tr><th style="text-align: center">Tipe</th>';
                                            echo '<th style="text-align: center">' . $ago . '</th>';
                                            echo '<th style="text-align: center;">' . $before . '</th>';
                                            echo '<th style="text-align: center;">' . $now . '</th></tr></thead><tbody>';
                                            foreach ($data as $key) {
                                                echo '<tr><td style="text-align: left">' . $key['Tipe'] . '</td>';
                                                echo '<td style="text-align: right">' . number_format($key['JasaAgo'], 0, ',', '.') . '</td>';
                                                echo '<td style="text-align: right;">' . number_format($key['JasaBefore'], 0, ',', '.') . '</td>';
                                                echo '<td style="text-align: right;">' . number_format($key['JasaNow'], 0, ',', '.') . '</td></tr>';
                                            }
                                            echo '</tbody></table>';
                                            break;
                                        case "2":
                                            echo '<table class="table table-bordered table-striped mb-0" id="table1">
                                                <thead><tr><th style="text-align: center">Karkode</th>';
                                            echo '<th style="text-align: center">'.$ago.'</th>';
                                            echo '<th style="text-align: center;">'.$before.'</th>';
                                            echo '<th style="text-align: center;">'.$now.'</th></tr></thead><tbody>';
                                            foreach ($data as $key){
                                                echo '<tr><td style="text-align: left">'.$key['KarKode'].'</td>';
                                                echo '<td style="text-align: right">'.number_format($key['BlnAgo'],0,',','.').'</td>';
                                                echo '<td style="text-align: right;">'.number_format($key['BlnBefore'],0,',','.').'</td>';
                                                echo '<td style="text-align: right;">'.number_format($key['BlnNow'],0,',','.').'</td></tr>';
                                            }
                                            echo '</tbody></table>';
                                            break;
                                        case "3":
                                            echo '<table class="table table-bordered table-striped mb-0" id="table1">
                                                <thead><tr><th style="text-align: center">JasaGroup</th>';
                                            echo '<th style="text-align: center">Unit'.$ago.'</th>';
                                            echo '<th style="text-align: center">Nominal'.$ago.'</th>';
                                            echo '<th style="text-align: center">Unit'.$before.'</th>';
                                            echo '<th style="text-align: center">Nominal'.$before.'</th>';
                                            echo '<th style="text-align: center">Unit'.$now.'</th>';
                                            echo '<th style="text-align: center">Nominal'.$now.'</th></tr></thead><tbody>';
                                            foreach ($data as $key){
                                                echo '<tr><td style="text-align: left">'.$key['JasaGroup'].'</td>';
                                                echo '<td style="text-align: right">'.$key['UnitAgo'].'</td>';
                                                echo '<td style="text-align: right;">'.number_format($key['NominalAgo'],0,',','.').'</td>';
                                                echo '<td style="text-align: right">'.$key['UnitBefore'].'</td>';
                                                echo '<td style="text-align: right;">'.number_format($key['NominalBefore'],0,',','.').'</td>';
                                                echo '<td style="text-align: right;">'.$key['UnitNow'].'</td>';
                                                echo '<td style="text-align: right;">'.number_format($key['NominalNow'],0,',','.').'</td></tr>';
                                            }
                                            echo '</tbody></table>';
                                            break;
                                        case "4":
                                            echo '<table class="table table-bordered table-striped mb-0" id="table1">
                                                <thead><tr><th style="text-align: center">Dealer</th>';
                                            echo '<th style="text-align: center">Kota</th>';
                                            echo '<th style="text-align: center;">CV</th>';
                                            echo '<th style="text-align: center;">BgrKode</th>';
                                            echo '<th style="text-align: center;">BgrNama</th>';
                                            echo '<th style="text-align: center;">SaldoAKhir</th>';
                                            echo '<th style="text-align: center;">P01</th>';
                                            echo '<th style="text-align: center;">P02</th>';
                                            echo '<th style="text-align: center;">P03</th>';
                                            echo '<th style="text-align: center;">P04</th>';
                                            echo '<th style="text-align: center;">P05</th>';
                                            echo '<th style="text-align: center;">P06</th>';
                                            echo '<th style="text-align: center;">P07</th>';
                                            echo '<th style="text-align: center;">P08</th>';
                                            echo '<th style="text-align: center;">P09</th>';
                                            echo '<th style="text-align: center;">P10</th>';
                                            echo '<th style="text-align: center;">P11</th>';
                                            echo '<th style="text-align: center;">P12</th>';
                                            echo '<th style="text-align: center;">AVG12</th>';
                                            echo '<th style="text-align: center;">SL12</th></tr></thead><tbody>';
                                            foreach ($data as $key){
                                                echo '<tr><td style="text-align: left">'.$key['Dealer'].'</td>';
                                                echo '<td style="text-align: left">'.$key['Kota'].'</td>';
                                                echo '<td style="text-align: left">'.$key['CV'].'</td>';
                                                echo '<td style="text-align: left">'.$key['BrgKode'].'</td>';
                                                echo '<td style="text-align: left">'.$key['BrgNama'].'</td>';
                                                echo '<td style="text-align: left">'.$key['SaldoAkhir'].'</td>';
                                                echo '<td style="text-align: left">'.$key['P01'].'</td>';
                                                echo '<td style="text-align: left">'.$key['P02'].'</td>';
                                                echo '<td style="text-align: left">'.$key['P03'].'</td>';
                                                echo '<td style="text-align: left">'.$key['P04'].'</td>';
                                                echo '<td style="text-align: left">'.$key['P05'].'</td>';
                                                echo '<td style="text-align: left">'.$key['P06'].'</td>';
                                                echo '<td style="text-align: left">'.$key['P07'].'</td>';
                                                echo '<td style="text-align: left">'.$key['P08'].'</td>';
                                                echo '<td style="text-align: left">'.$key['P09'].'</td>';
                                                echo '<td style="text-align: left">'.$key['P10'].'</td>';
                                                echo '<td style="text-align: left">'.$key['P11'].'</td>';
                                                echo '<td style="text-align: left">'.$key['P12'].'</td>';
                                                echo '<td style="text-align: left">'.$key['AVG12'].'</td>';
                                                echo '<td style="text-align: right;">'.$key['SL12'].'</td></tr>';
                                            }
                                            echo '</tbody></table>';
                                            break;
                                        case "5":
                                            echo '<table class="table table-bordered table-striped mb-0" id="table1">
                                                <thead><tr><th style="text-align: center">Dealer</th>';
                                            echo '<th style="text-align: center">Kota</th>';
                                            echo '<th style="text-align: center;">CV</th>';
                                            echo '<th style="text-align: center;">BgrKode</th>';
                                            echo '<th style="text-align: center;">BgrNama</th>';
                                            echo '<th style="text-align: center;">AvgHrgJual</th>';
                                            echo '<th style="text-align: center;">AVDHD</th>';
                                            echo '<th style="text-align: center;">NilaiDemand</th>';
                                            echo '<th style="text-align: center;">AkumulasiNilai</th>';
                                            echo '<th style="text-align: center;">PersenAkumulasi</th>';
                                            echo '<th style="text-align: center;">Grade</th></tr></thead><tbody>';
                                            foreach ($data as $key){
                                                echo '<tr><td style="text-align: left">'.$key['Dealer'].'</td>';
                                                echo '<td style="text-align: left">'.$key['Kota'].'</td>';
                                                echo '<td style="text-align: left">'.$key['CV'].'</td>';
                                                echo '<td style="text-align: left">'.$key['BrgKode'].'</td>';
                                                echo '<td style="text-align: left">'.$key['BrgNama'].'</td>';
                                                echo '<td style="text-align: right"></td>';
                                                echo '<td style="text-align: right"></td>';
                                                echo '<td style="text-align: right"></td>';
                                                echo '<td style="text-align: right">'.number_format($key['BrgHrgBeli'],0,',','.').'</td>';
                                                echo '<td style="text-align: right">'.$key['SaldoAkhir'].'</td>';
                                                echo '<td style="text-align: right">'.number_format($key['Qty'],0,',','.').'</td>';
                                                echo '<td style="text-align: right">'.number_format($key['AkumulasiNilai'],0,',','.').'</td>';
                                                echo '<td style="text-align: right">'.$key['Persen'].'</td>';
                                                echo '<td style="text-align: right;">'.$key['Grade'].'</td></tr>';
                                            }
                                            echo '</tbody></table>';
                                            break;
                                        case "6":
                                            echo '<button class="btn pull-right" id="btnExcel1" type="button" style="margin: 10px;"><i class="fa fa-file-excel-o fa-lg"></i></button>
                                                <table class="table table-bordered table-striped mb-0" id="table1">
                                                <thead><tr><th style="text-align: center">Tanggal</th>';
                                            echo '<th style="text-align: center">HariKerja</th>';
                                            echo '<th style="text-align: center">UnitEntry</th>';
                                            echo '<th style="text-align: center">KPB1</th>';
                                            echo '<th style="text-align: center">KPB234</th>';
                                            echo '<th style="text-align: center">%KPB</th>';
                                            echo '<th style="text-align: center">Jasa</th>';
                                            echo '<th style="text-align: center">PartSD</th>';
                                            echo '<th style="text-align: center">PartCounter</th>';
                                            echo '<th style="text-align: center">OliSD</th>';
                                            echo '<th style="text-align: center">OliCounter</th>';
                                            echo '<th style="text-align: center">GP</th></tr></thead><tbody>';


                                            $data1 = $data['data1'];
                                            foreach ($data1 as $valarray => $key) {
                                                echo '<tr><td style="text-align: left">' . $key['Tanggal'] . '</td>';
                                                echo '<td style="text-align: right">' . $key['HariKerja'] . '</td>';
                                                echo '<td style="text-align: right;">' . $key['UnitEntry'] . '</td>';
                                                echo '<td style="text-align: right">' . $key['KPB1'] . '</td>';
                                                echo '<td style="text-align: right">' . $key['KPB234'] . '</td>';
                                                echo '<td style="text-align: right;">' . $key['KPBPersen'] . '</td>';
                                                echo '<td style="text-align: right;">' . $key['Jasa'] . '</td>';
                                                echo '<td style="text-align: right;">' . $key['PartSD'] . '</td>';
                                                echo '<td style="text-align: right;">' . $key['PartCounter'] . '</td>';
                                                echo '<td style="text-align: right;">' . $key['OliSD'] . '</td>';
                                                echo '<td style="text-align: right;">' . $key['OliCounter'] . '</td>';
                                                echo '<td style="text-align: right;">' . number_format($key['GP'], 0, ',', '.') . '</td></tr>';

                                            }
                                            echo '</tbody></table>';

                                            echo '<br><br><button class="btn pull-right" id="btnExcel2" type="button" style="margin: 10px;"><i class="fa fa-file-excel-o fa-lg"></i></button><table class="table table-bordered table-striped mb-0" id="table2">
                                                <thead><tr><th style="text-align: center">Tanggal</th>';
                                            echo '<th style="text-align: center">HariKerja</th>';
                                            echo '<th style="text-align: center">UnitEntry</th>';
                                            echo '<th style="text-align: center">KPB1</th>';
                                            echo '<th style="text-align: center">KPB234</th>';
                                            echo '<th style="text-align: center">%KPB</th>';
                                            echo '<th style="text-align: center">Jasa</th>';
                                            echo '<th style="text-align: center">PartSD</th>';
                                            echo '<th style="text-align: center">PartCounter</th>';
                                            echo '<th style="text-align: center">OliSD</th>';
                                            echo '<th style="text-align: center">OliCounter</th>';
                                            echo '<th style="text-align: center">GP</th></tr></thead><tbody>';


                                            $data2 = $data['data2'];
                                            foreach ($data2 as $valarray2 => $key2) {
                                                echo '<tr><td style="text-align: left">' . $key2['Tanggal'] . '</td>';
                                                echo '<td style="text-align: right">' . $key2['HariKerja'] . '</td>';
                                                echo '<td style="text-align: right;">' . $key2['UnitEntry'] . '</td>';
                                                echo '<td style="text-align: right">' . $key2['KPB1'] . '</td>';
                                                echo '<td style="text-align: right">' . $key2['KPB234'] . '</td>';
                                                echo '<td style="text-align: right;">' . $key2['KPBPersen'] . '</td>';
                                                echo '<td style="text-align: right;">' . $key2['Jasa'] . '</td>';
                                                echo '<td style="text-align: right;">' . $key2['PartSD'] . '</td>';
                                                echo '<td style="text-align: right;">' . $key2['PartCounter'] . '</td>';
                                                echo '<td style="text-align: right;">' . $key2['OliSD'] . '</td>';
                                                echo '<td style="text-align: right;">' . $key2['OliCounter'] . '</td>';
                                                echo '<td style="text-align: right;">' . number_format($key2['GP'], 0, ',', '.') . '</td></tr>';

                                            }
                                            echo '</tbody></table>';


                                            echo '<br><br><button class="btn pull-right" id="btnExcel3" type="button" style="margin: 10px;"><i class="fa fa-file-excel-o fa-lg"></i></button><table class="table table-bordered table-striped mb-0" id="table3">
                                                <thead><tr><th style="text-align: center">Tanggal</th>';
                                            echo '<th style="text-align: center">HariKerja</th>';
                                            echo '<th style="text-align: center">UnitEntry</th>';
                                            echo '<th style="text-align: center">KPB1</th>';
                                            echo '<th style="text-align: center">KPB234</th>';
                                            echo '<th style="text-align: center">%KPB</th>';
                                            echo '<th style="text-align: center">Jasa</th>';
                                            echo '<th style="text-align: center">PartSD</th>';
                                            echo '<th style="text-align: center">PartCounter</th>';
                                            echo '<th style="text-align: center">OliSD</th>';
                                            echo '<th style="text-align: center">OliCounter</th>';
                                            echo '<th style="text-align: center">GP</th></tr></thead><tbody>';


                                            $data3 = $data['data3'];
                                            foreach ($data3 as $valarray3 => $key3) {
                                                echo '<tr><td style="text-align: left">' . $key3['Tanggal'] . '</td>';
                                                echo '<td style="text-align: right">' . $key3['HariKerja'] . '</td>';
                                                echo '<td style="text-align: right;">' . $key3['UnitEntry'] . '</td>';
                                                echo '<td style="text-align: right">' . $key3['KPB1'] . '</td>';
                                                echo '<td style="text-align: right">' . $key3['KPB234'] . '</td>';
                                                echo '<td style="text-align: right;">' . $key3['KPBPersen'] . '</td>';
                                                echo '<td style="text-align: right;">' . $key3['Jasa'] . '</td>';
                                                echo '<td style="text-align: right;">' . $key3['PartSD'] . '</td>';
                                                echo '<td style="text-align: right;">' . $key3['PartCounter'] . '</td>';
                                                echo '<td style="text-align: right;">' . $key3['OliSD'] . '</td>';
                                                echo '<td style="text-align: right;">' . $key3['OliCounter'] . '</td>';
                                                echo '<td style="text-align: right;">' . number_format($key3['GP'], 0, ',', '.') . '</td></tr>';

                                            }
                                            echo '</tbody></table>';


                                            break;
                                        case "7":
                                            echo '<table class="table table-bordered table-striped mb-0" id="table1">
                                                <thead><tr><th style="text-align: center">Dealer</th>';
                                            echo '<th style="text-align: center">Kota</th>';
                                            echo '<th style="text-align: center;">CV</th>';
                                            echo '<th style="text-align: center;">BgrKode</th>';
                                            echo '<th style="text-align: center;">BgrNama</th>';
                                            echo '<th style="text-align: center;">AvgHrgJual</th>';
                                            echo '<th style="text-align: center;">AVDHD</th>';
                                            echo '<th style="text-align: center;">NilaiDemand</th>';
                                            echo '<th style="text-align: center;">AkumulasiNilai</th>';
                                            echo '<th style="text-align: center;">PersenAkumulasi</th>';
                                            echo '<th style="text-align: center;">Grade</th></tr></thead><tbody>';
                                            foreach ($data as $key){
                                                echo '<tr><td style="text-align: left">'.$key['Dealer'].'</td>';
                                                echo '<td style="text-align: left">'.$key['Kota'].'</td>';
                                                echo '<td style="text-align: left">'.$key['CV'].'</td>';
                                                echo '<td style="text-align: left">'.$key['BrgKode'].'</td>';
                                                echo '<td style="text-align: left">'.$key['BrgNama'].'</td>';
                                                echo '<td style="text-align: right">'.number_format($key['BrgHrgBeli'],0,',','.').'</td>';
                                                echo '<td style="text-align: right">'.$key['SaldoAkhir'].'</td>';
                                                echo '<td style="text-align: right">'.number_format($key['Qty'],0,',','.').'</td>';
                                                echo '<td style="text-align: right">'.number_format($key['Akumulasi'],0,',','.').'</td>';
                                                echo '<td style="text-align: right">'.$key['Persen'].'</td>';
                                                echo '<td style="text-align: right;">'.$key['Grade'].'</td></tr>';
                                            }
                                            echo '</tbody></table>';
                                            break;
                                        case "8":
                                            echo '<table class="table table-bordered table-striped mb-0" id="table1">
                                                <thead><tr><th style="text-align: center">Dealer</th>';
                                            echo '<th style="text-align: center">Kota</th>';
                                            echo '<th style="text-align: center;">CV</th>';
                                            echo '<th style="text-align: center;">BgrKode</th>';
                                            echo '<th style="text-align: center;">BgrNama</th>';
                                            echo '<th style="text-align: center;">SaldoAKhir</th>';
                                            echo '<th style="text-align: center;">P01</th>';
                                            echo '<th style="text-align: center;">P02</th>';
                                            echo '<th style="text-align: center;">P03</th>';
                                            echo '<th style="text-align: center;">P04</th>';
                                            echo '<th style="text-align: center;">P05</th>';
                                            echo '<th style="text-align: center;">P06</th>';
                                            echo '<th style="text-align: center;">AVG12</th>';
                                            echo '<th style="text-align: center;">SL06</th></tr></thead><tbody>';
                                            foreach ($data as $key){
                                                echo '<tr><td style="text-align: left">'.$key['Dealer'].'</td>';
                                                echo '<td style="text-align: left">'.$key['Kota'].'</td>';
                                                echo '<td style="text-align: left">'.$key['CV'].'</td>';
                                                echo '<td style="text-align: left">'.$key['BrgKode'].'</td>';
                                                echo '<td style="text-align: left">'.$key['BrgNama'].'</td>';
                                                echo '<td style="text-align: left">'.$key['SaldoAkhir'].'</td>';
                                                echo '<td style="text-align: left">'.$key['P01'].'</td>';
                                                echo '<td style="text-align: left">'.$key['P02'].'</td>';
                                                echo '<td style="text-align: left">'.$key['P03'].'</td>';
                                                echo '<td style="text-align: left">'.$key['P04'].'</td>';
                                                echo '<td style="text-align: left">'.$key['P05'].'</td>';
                                                echo '<td style="text-align: left">'.$key['P06'].'</td>';
                                                echo '<td style="text-align: left">'.$key['AVG12'].'</td>';
                                                echo '<td style="text-align: right;">'.$key['SL06'].'</td></tr>';
                                            }
                                            echo '</tbody></table>';
                                            break;
                                    }
                                    ?>
                            </div><br>
                        </div>
                        <div class="box-footer"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php

$this->registerJsVar('no', $no);
$this->registerJs(<<< JS

 $(document).ready(function () {
     if( no == "6"){
        $("#btnExcel").hide();
     }else {
        $("#btnExcel").show();    
     }
     
      $("#btnExcel").click(function(){
        TableToExcel.convert(document.getElementById("table1"), {
            name: "DetailDataExcel.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
      
      $("#btnExcel1").click(function(){
        TableToExcel.convert(document.getElementById("table1"), {
            name: "DetailPeformanceNow.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
      
      $("#btnExcel2").click(function(){
        TableToExcel.convert(document.getElementById("table1"), {
            name: "DetailPeformanceBefore.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
      
      $("#btnExcel3").click(function(){
        TableToExcel.convert(document.getElementById("table1"), {
            name: "DetailPeformanceAgo.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
      
        
     
  });

JS
);

