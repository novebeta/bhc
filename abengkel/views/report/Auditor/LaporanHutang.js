jQuery(function ($) {
    $("#tipe-id").change(function () {
        var tipe = this.value;
        if (tipe === "Saldo Hutang Supplier" || tipe === "Saldo Hutang Non Supplier" || tipe === "Umur Hutang - Curent" || tipe === "Umur Hutang - Overdue") {
            $(".tgl1Hide").hide();
        } else {
            $(".tgl1Hide").show();
        }

        if (tipe === "Saldo Hutang Supplier" || tipe === "Saldo Hutang Non Supplier") {
            $(".lunasHide").show();
        } else {
            $(".lunasHide").hide();
        }
        if (tipe === "Saldo Hutang Supplier" || tipe === "Saldo Hutang Non Supplier" || tipe === "Belum Di Link") {
            // if(tipe === "Belum Di Link"){
            //     $("#cek").prop("checked", true);
            // }else{
            //     $("#cek").prop("checked", false);
            // }
            $(".cbsensitifHide").show();
        } else {
            $(".cbsensitifHide").hide();
        }
        if (tipe === "Riwayat Hutang Supplier" || tipe === "Riwayat Hutang Non Supplier") {
            $(".nobuktiHide").show();
        } else {
            $(".nobuktiHide").hide();
        }

        if (tipe === "Umur Hutang - Curent" || tipe === "Umur Hutang - Overdue") {
            $(".lblRangeHide").show();
        } else {
            $(".lblRangeHide").hide();
        }
        if (tipe === "Umur Hutang - Overdue") {
            $(".numRangeHide").show();
        } else {
            $(".numRangeHide").hide();
        }
        if (tipe === "Umur Hutang - Curent") {
            $(".RangetglHide").show();
            $('#tgl2').trigger('change');
        } else {
            $(".RangetglHide").hide();
        }

        if (tipe === "Saldo Hutang Supplier" || tipe === "Umur Hutang - Curent" || tipe === "Umur Hutang - Overdue") {
            $(".jenisHide").show();
        } else {
            $(".jenisHide").hide();
        }

    });
    $('#tipe-id').trigger('change');
    $('#r1b').change(function () {
        $('#r2a').utilNumberControl().val(parseInt($(this).val()) + 1);
    });
    $('#r2b').change(function () {
        $('#r3a').utilNumberControl().val(parseInt($(this).val()) + 1);
    });
});

