<?php
use kartik\date\DatePicker;
use yii\helpers\Html;
$this->title                   = 'Laporan Bulanan';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;
?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Rugi Laba Bulanan' => 'Rugi Laba Bulanan',
									'Grafik Rugi Laba'  => 'Grafik Rugi Laba',
									'Grafik Jasa'       => 'Grafik Jasa',
									'Grafik Part'       => 'Grafik Part',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-footer">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Tanggal</label>
							<div class="col-md-10">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'attribute2'=>'to_date',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','MMMM  yyyy' ),
										'pluginOptions' => [
											'startView'=>'year',
											'minViewMode'=>'months',
											'format' => 'MM  yyyy'										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
							<div class="col-md-10">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'attribute2'=>'to_date',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','MMMM  yyyy' ),
										'pluginOptions' => [
											'startView'=>'year',
											'minViewMode'=>'months',
											'format' => 'MM  yyyy'										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
						</div>
					</div>
				</div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
