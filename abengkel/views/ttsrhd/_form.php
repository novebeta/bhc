<?php
use abengkel\components\FormField;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttsrhd */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
    function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
    return data;
    }
    if (typeof data.text === 'undefined') {
    return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
    var modifiedData = $.extend({}, data, true);
    return modifiedData;
    }
    return null;
    }
    SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
?>
    <div class="ttsrhd-form">
		<?php
$dsTJual['SRDiscPersen'] = 0;
if ($dsTJual['SRTotal'] > 0) {
    $dsTJual['SRDiscPersen'] = round($dsTJual['SRDiscFinal'] / $dsTJual['SRTotal'] * 100);
}
$form = ActiveForm::begin(['id' => 'form_ttsrhd_id', 'action' => $url['update']]);
\abengkel\components\TUi::form(
    ['class' => "row-no-gutters",
        'items'  => [
            ['class' => "form-group col-md-24",
                'items'  => [
                    ['class' => "form-group col-md-8",
                        'items'  => [
                            ['class' => "col-sm-5", 'items' => ":LabelSRNo"],
                            ['class' => "col-sm-6", 'items' => ":SRNo"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-5", 'items' => ":LabelSRTgl"],
                            ['class' => "col-sm-6", 'items' => ":SRTgl"],
                            ['class' => "col-sm-1"],
                        ],
                    ],
                    ['class' => "form-group col-md-8",
                        'items'  => [
                            ['class' => "col-sm-3", 'items' => ":LabelSRJam"],
                            ['class' => "col-sm-5", 'items' => ":SRJam"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-3", 'items' => ":LabelSINo"],
                            ['class' => "col-sm-9", 'items' => ":SINo"],
                            ['class' => "col-sm-2", 'items' => ":SearchKonsumen"],
                        ],
                    ],
                    ['class' => "form-group col-md-8",
                        'items'  => [
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-5", 'items' => ":LabelStok"],
                            ['class' => "col-sm-18", 'items' => ":Stok"],
                        ],
                    ],
                ],
            ],
            ['class' => "form-group col-md-24",
                'items'  => [
                    ['class' => "form-group col-md-8",
                        'items'  => [
                            ['class' => "col-sm-5", 'items' => ":LabelKonsumen"],
                            ['class' => "col-sm-9", 'items' => ":Konsumen"],
                            ['class' => "col-sm-3", 'items' => ":BtnCariMotor"],
                            ['class' => "col-sm-3", 'items' => ":SearchCus"],
                            ['class' => "col-sm-3", 'items' => ":TambahCus"],
                            ['class' => "col-sm-1"],
                        ],
                    ],
                    ['class' => "form-group col-md-8",
                        'items'  => [
                            ['class' => "col-sm-3", 'items' => ":LabelType"],
                            ['class' => "col-sm-21", 'items' => ":Type"],
                        ],
                    ],
                    ['class' => "form-group col-md-8",
                        'items'  => [
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-5", 'items' => ":LabelSales"],
                            ['class' => "col-sm-18", 'items' => ":Sales"],
                        ],
                    ],
                ],
            ],
            ['class' => "form-group col-md-24",
                'items'  => [
                    ['class' => "form-group col-md-8",
                        'items'  => [
                            ['class' => "col-sm-5", 'items' => ":LabelNama"],
                            ['class' => "col-sm-18", 'items' => ":Nama"],
                        ],
                    ],
                    ['class' => "form-group col-md-8",
                        'items'  => [
                            ['class' => "col-sm-3", 'items' => ":LabelWarna"],
                            ['class' => "col-sm-21", 'items' => ":Warna"],
                        ],
                    ],
                    ['class' => "form-group col-md-8",
                        'items'  => [
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-5", 'items' => ":LabelTC"],
                            ['class' => "col-sm-18", 'items' => ":TC"],
                        ],
                    ],
                ],
            ],
            [
                'class' => "row col-md-24",
                'style' => "margin-bottom: 6px;",
                'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>',
            ],
            ['class' => "form-group col-md-24",
                'items'  => [
                    ['class' => "form-group col-md-13",
                        'items'  => [
                            ['class' => "form-group col-md-24",
                                'items'  => [
                                    ['class' => "col-sm-4", 'items' => ":KodeBarang"],
                                    ['class' => "col-sm-10", 'items' => ":BarangNama", 'style' => 'padding-left:2px;'],
                                    ['class' => "col-sm-4", 'items' => ":Gudang", 'style' => 'padding-left:2px;'],
                                    ['class' => "col-sm-4", 'items' => ":Satuan", 'style' => 'padding-left:2px;'],
                                    ['class' => "col-sm-2", 'items' => ":BtnInfo"],
                                ],
                            ],
                            ['class' => "form-group col-md-18",
                                'items'  => [
                                    ['class' => "col-sm-4", 'items' => ":LabelKeterangan"],
                                    ['class' => "col-sm-20", 'items' => ":Keterangan"],
                                ],
                            ],
                            ['class' => "form-group col-md-6",
                                'items'  => [
                                    ['class' => "form-group col-md-24",
                                        'items'  => [
                                            ['class' => "col-sm-1"],
                                            ['class' => "col-sm-4", 'items' => ":BtnBayar"],
                                            ['class' => "col-sm-1"],
                                        ],
                                    ],
                                    ['class' => "form-group col-md-24",
                                        'items'  => [
                                            ['class' => "col-sm-1"],
                                            ['class' => "col-sm-8", 'items' => ":LabelCetak"],
                                            ['class' => "col-sm-12", 'items' => ":Cetak"],
                                            ['class' => "col-sm-1"],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                    ['class' => "col-sm-1"],
                    ['class' => "form-group col-md-11",
                        'items'  => [
                            ['class' => "form-group col-md-24",
                                'items'  => [
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-4", 'items' => ":LabelTotalBayar"],
                                    ['class' => "col-sm-6", 'items' => ":TotalBayar"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-3", 'items' => ":LabelSubTotal"],
                                    ['class' => "col-sm-9", 'items' => ":SubTotal"],
                                ],
                            ],
                            ['class' => "form-group col-md-24",
                                'items'  => [
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-4", 'items' => ":LabelSisaBayar"],
                                    ['class' => "col-sm-6", 'items' => ":SisaBayar"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-3", 'items' => ":LabelDisc"],
                                    ['class' => "col-sm-2", 'items' => ":Disc"],
                                    ['class' => "col-sm-1", 'items' => ":Persen", 'style' => 'padding-left:3px;'],
                                    ['class' => "col-sm-6", 'items' => ":Nominal"],
                                ],
                            ],
                            ['class' => "form-group col-md-24",
                                'items'  => [
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-4", 'items' => ":LabelStatusBayar"],
                                    ['class' => "col-sm-4", 'items' => ":StatusBayar"],
                                    ['class' => "col-sm-2", 'items' => ":BtnStatus"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-3", 'items' => ":LabelTotal"],
                                    ['class' => "col-sm-9", 'items' => ":Total"],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    ['class' => "row-no-gutters",
        'items'  => [
            [
                'class' => "col-md-12 pull-left",
                'items' => $this->render( '../_nav', [
                    'url'=> $_GET['r'],
                    'options'=> [
                        'SRNo'          => 'SR No',
                        'SINo'          => 'SI No',
                        'KodeTrans'     => 'Kode Trans',
                        'LokasiKode'    => 'Lokasi Kode',
                        'PosKode'       => 'Pos Kode',
                        'KarKode'       => 'Kar Kode',
                        'MotorNoPolisi' => 'No Polisi',
                        'SRKeterangan'  => 'SR Keterangan',
                        'NoGL'          => 'No Gl',
                        'Cetak'         => 'Cetak',
                        'UserID'        => 'User ID',
                    ],
                ])
            ],
            ['class' => "form-group col-md-6", 'style' => "margin-left:-70px",
                'items'  => [
                    ['class' => "col-sm-3", 'items' => ":LabelPOS"],
                    ['class' => "col-sm-12", 'items' => ":POS"],
                ],
            ],
            ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                'items'  => [
                    ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":JT"],
                    ['class' => "col-sm-4 pull-right", 'items' => ":LabelJT"],
                ],
            ],
            ['class' => "col-md-12 pull-right",
                'items'  => [
                    ['class' => "pull-right", 'items' => ":btnPrint :btnAction"],
                ],
            ],
        ],
    ],
    [
        ":LabelSRNo"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor SR</label>',
        ":LabelSRTgl"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tanggal SR</label>',
        ":LabelSRJam"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jam</label>',
        ":LabelSINo"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No SI</label>',
        ":LabelPONo"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No PO</label>',
        ":LabelStok"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Stok Masuk</label>',
        ":LabelKonsumen"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Konsumen</label>',
        ":LabelType"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Type</label>',
        ":LabelSales"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sales</label>',
        ":LabelNama"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama</label>',
        ":LabelWarna"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Warna</label>',
        ":LabelTC"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">TC</label>',
        ":LabelTotalBayar"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total Bayar</label>',
        ":LabelSubTotal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sub Total</label>',
        ":LabelKeterangan"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
        ":LabelSisaBayar"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sisa Bayar</label>',
        ":LabelDisc"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Disc Final</label>',
        ":Persen"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">%</label>',
        ":LabelCetak"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Cetak</label>',
        ":LabelStatusBayar" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status Bayar</label>',
        ":LabelTotal"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total</label>',
        ":LabelPOS"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">POS</label>',
        ":LabelJT"          => \common\components\General::labelGL($dsTJual['NoGL'], 'JT'),
        ":SRNo"             => Html::textInput('SRNoView', $dsTJual['SRNoView'], ['class' => 'form-control', 'readonly' => 'readonly','tabindex' => '1']) .
        Html::textInput('SRNo', $dsTJual['SRNo'], ['class' => 'hidden']) .
        FormField::numberInput(['name' => 'SRBiayaKirim', 'config' => ['id' => 'SRBiayaKirim', 'value' => $dsTJual['SRBiayaKirim'], 'displayOptions' => ['class' => 'hidden']]]) .
        FormField::numberInput(['name' => 'SRTotalPajak', 'config' => ['id' => 'SRTotalPajak', 'value' => $dsTJual['SRTotalPajak'], 'displayOptions' => ['class' => 'hidden']]]),
        ":SRTgl"            => FormField::dateInput(['name' => 'SRTgl', 'config' => ['value' => General::asDate($dsTJual['SRTgl']), 'options' => ['tabindex' => '1']]]),
        ":SRJam"            => FormField::timeInput(['name' => 'SRJam', 'config' => ['value' => $dsTJual['SRTgl'], 'options' => ['tabindex' => '2']]]),
        ":SINo"             => Html::textInput('SINo', $dsTJual['SINo'], ['class' => 'form-control', 'readonly' => 'readonly']),
        ":JT"               => Html::textInput('NoGL', $dsTJual['NoGL'], ['class' => 'form-control', 'readOnly' => true]),
        ":Konsumen"         => Html::textInput('MotorNoPolisi', $dsTJual['MotorNoPolisi'], ['class' => 'form-control', 'maxlength' => '12','tabindex' => '5']),
        ":Type"             => Html::textInput('MotorNama', $dsTJual['MotorNama'], ['class' => 'form-control', 'readonly' => 'readonly']),
        ":Sales"            => FormField::combo('KarKode', ['config' => ['value' => $dsTJual['KarKode']], 'options' => [ 'tabindex' => '9' ],'extraOptions' => ['altLabel' => ['KarNama']]]),
        ":Nama"             => Html::textInput('CusNama', $dsTJual['CusNama'], ['class' => 'form-control', 'readonly' => 'readonly']),
        ":Warna"            => Html::textInput('MotorWarna', $dsTJual['MotorWarna'], ['class' => 'form-control', 'readonly' => 'readonly']),
        ":Keterangan"       => Html::textarea('SRKeterangan', $dsTJual['SRKeterangan'], ['class' => 'form-control', 'maxlength' => '150','tabindex' => '11']),
        ":TC"               => FormField::combo('SRTC', ['name' => 'KodeTrans', 'options' => [ 'tabindex' => '10' ],'extraOptions' => ['simpleCombo' => true]]),
        ":SubTotal"         => FormField::numberInput(['name' => 'SRSubTotal', 'config' => ['id' => 'SRSubTotal', 'value' => $dsTJual['SRSubTotal'], 'readonly' => 'readonly']]),
        ":Cetak"            => Html::textInput('Cetak', $dsTJual['Cetak'], ['class' => 'form-control', 'readonly' => 'readonly']),
        ":TotalBayar"       => FormField::decimalInput(['name' => 'SRTerbayar', 'config' => ['id' => 'SRTerbayar', 'value' => $dsTJual['SRTerbayar'], 'readonly' => 'readonly']]),
        ":POS"              => FormField::combo('PosKode', ['config' => ['value' => $dsTJual['PosKode']], 'extraOptions' => ['simpleCombo' => true]]),
        ":Total"            => FormField::decimalInput(['name' => 'SRTotal', 'config' => ['id' => 'SRTotal', 'value' => $dsTJual['SRTotal'], 'readonly' => 'readonly']]),
        ":Nominal"          => FormField::numberInput(['name' => 'SRDiscFinal', 'config' => ['value' => $dsTJual['SRDiscFinal']],'tabindex' => '13']),
        ":KodeBarang"       => Html::textInput('KodeBarang', '', ['id' => 'KodeBarang', 'class' => 'form-control', 'readonly' => 'readonly']),
        ":BarangNama"       => Html::textInput('BarangNama', '', ['id' => 'BarangNama', 'class' => 'form-control', 'readonly' => 'readonly']),
        ":Gudang"           => Html::textInput('LokasiKodeTxt', $dsTJual['LokasiKode'], ['class' => 'form-control', 'readonly' => 'readonly']),
        ":Satuan"           => Html::textInput('lblStock', 0, ['type' => 'number', 'class' => 'form-control', 'style' => 'color: blue; font-weight: bold;', 'readonly' => 'readonly']),
        ":SisaBayar"        => FormField::decimalInput(['name' => 'SisaBayar', 'config' => ['id' => 'SisaBayar', 'value' => 0, 'readonly' => 'readonly']]),
        ":Disc"             => FormField::integerInput(['name' => 'SRDiscPersen', 'config' => ['id' => 'SRDiscPersen', 'value' => 0], 'tabindex' => '12']),
        ":StatusBayar"      => Html::textInput('StatusBayar', $dsTJual['StatusBayar'], ['id' => 'StatusBayar', 'class' => 'form-control', 'readonly' => 'readonly']),
        ":Stok"             => FormField::combo('LokasiKode', ['config' => ['value' => $dsTJual['LokasiKode'], 'options' => [ 'tabindex' => '4' ], 'data' => ['Retur' => 'Retur']]]),
        ":BtnBayar"         => Html::button('<i class="fa fa-thumbs-up"></i> Bayar', ['class' => 'btn btn-success', 'style' => 'width:120px', 'id' => 'BtnBayar']),
        ":BtnInfo"          => '<button type="button" class="btn btn-default"><i class="fa fa-info-circle fa-lg"></i></button>',
        ":BtnStatus"        => Html::button('<i class="fa fa-refresh fa-lg"></i>', ['class' => 'btn btn-default', 'id' => 'BtnStatus']),
//        ":SearchKonsumen"   => '<button id="BtnSearch"  type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>',
        ":SearchKonsumen"   => Html::button( '<i class="glyphicon glyphicon-search"></i>', [ 'class' => 'btn btn-default', 'id' => 'BtnSearch', 'tabindex' => '3' ] ),
        ":TambahCus"        => Html::button('<i class="fa fa-user-plus"></i>', ['class' => 'btn btn-default', 'id' => 'AddKonsumen', 'tabindex' => '8']),
        ":SearchCus"        => Html::button('<i class="glyphicon glyphicon-user"></i>', ['class' => 'btn btn-default', 'id' => 'EditKonsumen','tabindex' => '7']),
        ":BtnCariMotor"     => Html::button( '<i class="glyphicon glyphicon-search"></i>', [ 'class' => 'btn btn-default', 'id' => 'BtnCariMotor', 'tabindex' => '6' ] ),
    ],
    [
        'url_main' => 'ttsrhd',
        'url_id' => $_GET['id'] ?? '',
        '_akses' => '[SR] Retur Penjualan',
        'url_delete'        => Url::toRoute(['ttsrhd/delete', 'id' => $_GET['id'] ?? ''] ),
    ]
);
ActiveForm::end();
?>
    </div>
<?php
$urlCustomer             = Url::toRoute( [ 'tdcustomer/select' , 'tgl1'=> '1900-01-01'] );
$urlCustomerFindById = Url::toRoute( [ 'tdcustomer/find-by-id' ] );
$urlStatusOtomatis       = Url::toRoute(['ttsrhd/status-otomatis', 'SRNo' => $dsTJual['SRNo']]);
$urlCustomerCreate       = Url::toRoute(['tdcustomer/create', 'action' => 'create']);
$urlCustomerEdit         = Url::toRoute(['tdcustomer/edit']);
$urlCheckStock           = Url::toRoute(['site/check-stock-transaksi']);
$urlBayarOtomatis        = Url::toRoute(['ttbmhd/bayar-otomatis', 'NominalBayar' => $dsTJual['SRTotal']]);
$urlDaftarKonsumenSelect = Url::toRoute(['tdcustomer/select']);
$urlKonsumenData         = Url::toRoute(['tdcustomer/td', 'oper' => 'data']);
$urlBarangAjax           = Url::toRoute(['tdbarang/ajax-combo']);
$urlSI                   = Url::toRoute(['ttsihd/select']);
$urlDetail               = $url['detail'];
$urlPrint                = $url['print'];
$readOnly                = ($_GET['action'] == 'view' ? 'true' : 'false');
$urlBayar                = Url::toRoute(['bayar/save']);
$urlRefresh              = $url['update'] . '&oper=skip-load&SRNoView=' . $dsTJual['SRNoView'];
$resultKas               = \abengkel\models\Ttkkhd::find()->callSP(['Action' => 'Insert']);
$resultBank              = \abengkel\models\Ttbkhd::find()->callSP(['Action' => 'Insert']);
$barang                  = \common\components\General::cCmd(
    "SELECT BrgKode, BrgNama, BrgBarCode, BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual, BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus
                            FROM tdbarang
                            WHERE BrgStatus = 'A'
                            UNION
                            SELECT BrgKode, BrgNama, BrgBarCode,  BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual, BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus FROM tdBarang
                            WHERE BrgKode IN
                            (SELECT ttsrit.BrgKode  FROM ttsrit INNER JOIN tdbarang ON ttsrit.BrgKode = tdbarang.BrgKode
                             WHERE ttsrit.srNo = '--' AND tdBarang.BrgStatus = 'N'  GROUP BY ttsrit.BrgKode )
                            ORDER BY BrgStatus, BrgKode")->queryAll();

$this->registerJsVar('kodeBrang', $barang);
$this->registerJsVar('namaBarang', $barang);
$this->registerJsVar('__urlSI', $urlSI);
$this->registerJsVar('__model', $dsTJual);
$this->registerJsVar('__idCustomer', $dsTJual['MotorNoPolisi']);
$this->registerJs(<<< JS

         window.BHC_BAYAR_KMNo = '{$resultKas['KKNo']}';
        window.BHC_BAYAR_BMNo = '{$resultBank['BKNo']}';

         window.getUrlSI = function(){
            let SINo = $('input[name="SINo"]').val();
            return __urlSI + ((SINo === '' || SINo === '--') ? '':'&check=off&cmbTxt1=SINo&txt1='+SINo);
        }

        $('#BtnSearch').click(function(){
        window.util.app.dialogListData.show({
            title: 'Daftar Sales Invoice / Invoice Penjualan',
            url: getUrlSI()
        },
        function (data) {
             console.log(data);
             if (!Array.isArray(data)){
                 return;
             }
             var itemData = [];
             data.forEach(function(itm,ind) {
                let cmp = $('input[name="SINo"]');
                cmp.val(itm.header.data.SINo);
                itm.data.SINo = itm.header.data.SINo;
                itm.data.SRNo = __model.SRNo;
                itm.data.SRHrgJual = itm.data.SIHrgJual;
                itm.data.SRQty = itm.data.SIQty;
                itm.data.SRDiscount = itm.data.SIDiscount;
                itemData.push(itm.data);
            });
            $.ajax({
             type: "POST",
             url: '$urlDetail',
             data: {
                 oper: 'batch',
                 data: itemData,
                 header: btoa($('#form_ttsrhd_id').serialize())
             },
             success: function(data) {
               window.location.href = "{$url['update']}&oper=skip-load&SRNoView={$dsTJual['SRNoView']}";
             },
           });
        })
    });

         function HitungBawah(){
         	let SRDiscPersen = parseFloat($('#SRDiscPersen').val());
         	let SRSubTotal = parseFloat($('#SRSubTotal').val());
         	let SRDiscFinal = SRDiscPersen * SRSubTotal / 100;
         	$('#SRDiscFinal').utilNumberControl().val(SRDiscFinal);
         	let SRTotalPajak = parseFloat($('#SRTotalPajak').val());
         	let SRBiayaKirim = parseFloat($('#SRBiayaKirim').val());
         	let SRTotal = SRSubTotal + SRTotalPajak + SRBiayaKirim - SRDiscFinal;
         	$('#SRTotal').utilNumberControl().val(SRTotal);
         	let TotalBayar = parseFloat($('#SRTerbayar').val());
         	let SisaBayar = SRTotal - TotalBayar;
         	$('#SisaBayar').utilNumberControl().val(SisaBayar);
         	if(SisaBayar  > 0 || SRTotal === 0){
         	    $('#StatusBayar').css({"background-color" : "yellow"});
         	    if(TotalBayar === 0) $('#StatusBayar').css({"background-color" : "pink"});
         	} else if(SisaBayar <= 0){
         	    $('#StatusBayar').css({"background-color" : "lime"});
         	}
         }
         window.HitungBawah = HitungBawah;

         function checkStok(BrgKode,BarangNama){
           $.ajax({
                url: '$urlCheckStock',
                type: "POST",
                data: {
                    BrgKode: BrgKode,
                },
                success: function (res) {
                    $('[name=LokasiKode]').val(res.LokasiKode);
                    $('#KodeBarang').val(BrgKode);
                    $('[name=BarangNama]').val(BarangNama);
                    $('[name=lblStock]').val(res.SaldoQty);
                },
                error: function (jXHR, textStatus, errorThrown) {

                },
                async: false
            });
          }
          window.checkStok = checkStok;

        function HitungTotal(){
         	let SRTotalPajak = $('#detailGrid').jqGrid('getCol','SRPajak',false,'sum');
         	$('#SRTotalPajak').utilNumberControl().val(SRTotalPajak);
         	let SRSubTotal = $('#detailGrid').jqGrid('getCol','Jumlah',false,'sum');
         	$('#SRSubTotal').utilNumberControl().val(SRSubTotal);
         	HitungBawah();
         }
         window.HitungTotal = HitungTotal;

     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 160,
        rowNum: 1000,
        extraParams: {
            SRNo: $('input[name="SRNo"]').val()
        },
        rownumbers: true,
        loadonce:true,
        readOnly: $readOnly,
        colModel: [
            { template: 'actions'  },
            {
                name: 'BrgKode',
                label: 'Kode',
                editable: true,
                width: 165,
                editor: {
                    type: 'select2',
                    data: $.map(kodeBrang, function (obj) {
                      obj.id = obj.BrgKode;
                      obj.text = obj.BrgKode;
                      return obj;
                    }),
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;

						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'SRHrgJual'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgJual));
                            $('[id="'+rowid+'_'+'SRHrgBeli'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgBeli));
                            $('[id="'+rowid+'_'+'BrgGroup'+'"]').val(data.BrgGroup);
                            window.hitungLine();
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').val(data.BrgNama); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').trigger('change');
                            checkStok($(this).val(),$('[id="'+rowid+'_'+'BrgNama'+'"]').val());
						},
						'change': function(){
                            let rowid = $(this).attr('rowid');
                            checkStok($(this).val(),$('[id="'+rowid+'_'+'BrgNama'+'"]').val());
						}
                    }
                }
            },
            {
                name: 'BrgNama',
                label: 'Nama Barang',
                width: 240,
                editable: true,
                editor: {
                    type: 'select2',
                    data: $.map(namaBarang, function (obj) {
                      obj.id = obj.BrgNama;
                      obj.text = obj.BrgNama;
                      return obj;
                    }),
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    console.log(data);
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'SRHrgJual'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgJual));
                            $('[id="'+rowid+'_'+'SRHrgBeli'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgBeli));
                            $('[id="'+rowid+'_'+'BrgGroup'+'"]').val(data.BrgGroup);
                            window.hitungLine();
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').val(data.BrgKode); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').trigger('change');
						}
                    }
                }
            },

            {
                name: 'SRQty',
                label: 'Qty',
                width: 75,
                editable: true,
                template: 'number'
            },
            {
                name: 'BrgSatuan',
                label: 'Satuan',
                width: 75,
                editable: true,
                editor:{
                    readonly: true
                }
            },
            {
                name: 'SRHrgJual',
                label: 'Harga Part',
                width: 100,
                editable: true,
                template: 'money'
            },
            {
                name: 'SRHrgBeli',
                label: 'Harga Part',
                width: 100,
                editable: true,
                template: 'money',
                hidden: true
            },
            {
                name: 'Disc',
                label: 'Disc %',
                width: 75,
                editable: true,
                template: 'number'
            },
            {
                name: 'SRDiscount',
                label: 'Nilai Disc',
                width: 100,
                editable: true,
                template: 'money'
            },
            {
                name: 'Jumlah',
                label: 'Jumlah',
                width: 100,
                editable: true,
                template: 'money'
            },
            {
                name: 'SRPajak',
                label: 'Pajak',
                editable: true,
                template: 'money',
                hidden: true
            },
            {
                name: 'BrgGroup',
                label: 'Group',
                editable: true,
                hidden: true
            },
            {
                name: 'LokasiKode',
                label: 'LokasiKode',
                editable: true,
                hidden: true
            },
            {
                name: 'SRAuto',
                label: 'SRAuto',
                editable: true,
                hidden: true
            },
        ],

        listeners: {
            afterLoad: function (data) {
             window.HitungTotal();
            }
        },
        onSelectRow : function(rowid,status,e){
            if (window.rowId !== -1) return;
            if (window.rowId_selrow === rowid) return;
            if(rowid.includes('new_row') === true){
                $('[name=KodeBarang]').val('--');
                $('[name=LokasiKode]').val('Kosong');
                $('[name=BarangNama]').val('--');
                $('[name=lblStock]').val(0);
                return ;
            };
            let r = $('#detailGrid').utilJqGrid().getData(rowid).data;
			checkStok(r.BrgKode,r.BrgNama);
			window.rowId_selrow = rowid;
        }
     }).init().fit($('.box-body')).load({url: '$urlDetail'});
     window.rowId_selrow = -1;
     function hitungLine(){

            let Disc = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Disc'+'"]').val()));
            let SRHrgJual = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'SRHrgJual'+'"]').val()));
            let SRQty = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'SRQty'+'"]').val()));
            let SRDiscount = Disc*SRHrgJual*SRQty / 100;
            $('[id="'+window.cmp.rowid+'_'+'SRDiscount'+'"]').utilNumberControl().val(SRDiscount);
            $('[id="'+window.cmp.rowid+'_'+'Jumlah'+'"]').utilNumberControl().val(SRHrgJual * SRQty - SRDiscount);
            window.HitungTotal();
         }
         window.hitungLine = hitungLine;

	 $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
	     window.cmp.rowid = rowid;

          if(rowid.includes('new_row')){
              var BrgKode = $('[id="'+rowid+'_'+'BrgKode'+'"]');
              var dtBrgKode = BrgKode.select2('data');
              if (dtBrgKode.length > 0){
                  var dt = dtBrgKode[0];
                  BrgKode.val(dt.BrgKode).trigger('change').trigger({
                    type: 'select2:select',
                    params: {
                        data: dt
                    }
                });
              }
            $('[id="'+rowid+'_'+'SRQty'+'"]').val(1);
          }

          $('[id="'+window.cmp.rowid+'_'+'SRQty'+'"]').on('keyup', function (event){
              hitungLine();
          });
          $('[id="'+window.cmp.rowid+'_'+'SRHrgJual'+'"]').on('keyup', function (event){
              hitungLine();
          });
          $('[id="'+window.cmp.rowid+'_'+'Disc'+'"]').on('keyup', function (event){
              hitungLine();
          });
         hitungLine();
	 });

	 $('#SearchKonsumen').click(function () {
        window.util.app.dialogListData.show({
                title: 'Daftar Konsumen',
                url: '$urlDaftarKonsumenSelect'
            },
            function (data) {
                console.log(data);

                $.ajax({
                    type: 'POST',
                    url: '$urlKonsumenData',
                    data: {
                        'oper': 'data',
                        'id': btoa(data.id)
                    }
                }).then(function (cusData) {

                   console.log(cusData);
                   Object.keys(cusData).forEach(function(key,index) {
                            let cmp = $('input[name="'+key+'"]');
                            cmp.val(Object.values(cusData)[index]);

                   });
                });
            });
        });

	$('#btnSave').click(function (event) {
	    let Nominal = parseFloat($('#SRSubTotal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Subtotal tidak boleh 0', size: 'small'});
               return;
           }
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttsrhd_id').attr('action','{$url['update']}');
        $('#form_ttsrhd_id').submit();
    });

	  $('#SRDiscPersen').change(function (event) {
	      HitungBawah();
	  });

	  $('#BtnBayar').click(function() {
	    let MotorNoPolisi = $('input[name="MotorNoPolisi"]').val();
	    let CusNama = $('input[name="CusNama"]').val();
	    let SRNoView = $('input[name="SRNoView"]').val();
	    let SRNo = $('input[name="SRNo"]').val();
        let SisaBayar = parseFloat($('#SisaBayar').utilNumberControl().val());
        if(SRNo.includes('=')){
             bootbox.alert({message:'Silahkan simpan transaksi ini dulu.',size: 'small'});
	        return;
        }
	    if (SisaBayar <= 0){
	        bootbox.alert({message:'Transaksi ini sudah terlunas',size: 'small'});
	        return;
	    }
	    $('#MyKodeTrans').val('SR');
	    $('#MyKode').val(MotorNoPolisi);
	    $('#MyNama').val(CusNama);
	    $('#MyNoTransaksi').val(SRNo);
	    $('#BMMemo').val("Pembayaran [SR] Retur Penjualan No: "+SRNoView+" - "+MotorNoPolisi+" - "+CusNama);
	    $('#BMNominal').utilNumberControl().val(SisaBayar);
	    $('#BMBayar').utilNumberControl().val(SisaBayar);
        $('#modalBayar').modal('show');
    });

	  $('#btnSavePopup').click(function(){
	    $.ajax({
            type: 'POST',
            url: '$urlBayar',
            data: $('#form_bayar_id').utilForm().getData(true)
        }).then(function (cusData) {
          location.reload();
        });
	});

	function onFocus(){
        window.location.href = "{$urlRefresh}";
    };


	$('#EditKonsumen').click(function() {
	    let MotorNoPolisi = $('input[name="MotorNoPolisi"]').val();
        window.openCustomer(MotorNoPolisi);
    });

    $('#AddKonsumen').click(function() {
        window.openCustomer('--');
    });

    $('#BtnStatus').click(function() {
        window.util.app.popUpForm.show({
        title: "Daftar Transaksi Keuangan yang melibatkan Nomor SR : "+$('input[name="SRNo"]').val(),
        size: 'small',
        url: '$urlStatusOtomatis',
        width: 900,
        height: 320
        },
        function(data) {
            console.log(data);
        });
    });
    
    $('#BtnCariMotor').click(function() {
        window.util.app.dialogListData.show({
            title: 'Daftar Konsumen',
            url: '$urlCustomer'
        },
        function (data) {
            console.log(data);
            if(data.data === undefined){
                return ;
            }
            __idCustomer = data.id;
            let MotorNoMesin = data.data.MotorNoMesin;
            let MotorType = data.data.MotorType;
            let CusTlp = data.data.CusTelepon;
            let MotorNoPolisi = data.data.MotorNoPolisi;
            $('[name=MotorNoMesin]').val(data.data.MotorNoMesin).trigger('change');
            $('[name=MotorType]').val(data.data.MotorType).trigger('change');
            $('[name=CusTlp]').val(data.data.CusTelepon).trigger('change');            
            $('[name=MotorNoPolisi]').val(data.data.MotorNoPolisi).trigger('change');            
        })
    });
    
    let fillCustomer = function(id, data) {
        __idCustomer = id;
        $('[name=MotorNoPolisi]').val(data.MotorNoPolisi);
        $('[name=CusNama]').val(data.CusNama);
        $('[name=SDPembawaMotor]').val(data.CusNama);
        $('[name=MotorWarna]').val(data.MotorWarna);
        $('[name=MotorNama]').val(data.MotorNama);
    }
    
    $('[name=MotorNoPolisi]').change(function(){
        var MotorNoPolisi = $(this).val();
        $.ajax({
            url: '$urlCustomerFindById',
            type: "GET",
            data: { id: $(this).val() },
            success: function (res) {
                if(res !== null){ 
                    fillCustomer(res.MotorNoPolisi, res); 
                }else{
                    window.openCustomer('--');
                    $("body").removeClass("loading");
                    window._ajaxCounter = 0;
                    $('[name="Tdcustomer[MotorNoPolisi]"]').val(MotorNoPolisi);
                } 
            },
            error: function (jXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            },
            async: false
        });
    });

JS
);
echo $this->render('../_formBayar');
echo $this->render('../_formCustomer');
