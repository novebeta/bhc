<?php
use abengkel\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '[PO] Order Pembelian';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'PONo'         => 'No PO',
		'PSNo'         => 'No PS',
		'SONo'         => 'No SO',
		'KodeTrans'    => 'Kode Trans',
		'PosKode'      => 'Pos Kode',
		'SupKode'      => 'Sup Kode',
		'LokasiKode'   => 'Lokasi Kode',
		'POKeterangan' => 'Keterangan',
		'Cetak'        => 'Cetak',
		'UserID'       => 'User ID',
	],
	'cmbTgl'    => [
		'POTgl'        => 'Tgl PO',
	],
	'cmbNum'    => [
		'POSubTotal'   => 'Sub Total',
		'PODiscFinal'  => 'Disc Final',
		'POTotal'      => 'Total',
		'POTotalPajak' => 'Total Pajak',
		'POBiayaKirim' => 'Biaya Kirim',
	],
	'sortname'  => "POTgl",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Ttpohd::className(), '[PO] Order Pembelian',    [
        'url_delete' => Url::toRoute( [ 'ttpohd/delete' ] ),
    ]
), \yii\web\View::POS_READY );
