<?php
use abengkel\components\FormField;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttpohd */
/* @var $form yii\widgets\ActiveForm */

$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
?>
    <div class="ttpohd-form">
		<?php
$altConditionLokasiKode  = "LokasiStatus = 'A' AND LEFT(LokasiNomor,1) = '7'";
$altConditionSupKode     = "Supstatus = 'A' OR Supstatus = 1";
$dsTBeli['PODiscPersen'] = 0;
if ($dsTBeli['POTotal'] > 0) {
 $dsTBeli['PODiscPersen'] = round($dsTBeli['PODiscFinal'] / $dsTBeli['POTotal'] * 100);
}
$form = ActiveForm::begin(['id' => 'form_ttpohd_id', 'action' => $url['update']]);
\abengkel\components\TUi::form(
 ['class' => "row-no-gutters",
  'items'  => [
   ['class' => "form-group col-md-24",
    'items'  => [
     ['class' => "form-group col-md-10",
      'items'  => [
       ['class' => "col-sm-3", 'items' => ":LabelPONo"],
       ['class' => "col-sm-7", 'items' => ":PONo"],
       ['class' => "col-sm-1"],
       ['class' => "col-sm-3", 'items' => ":LabelPOTgl"],
       ['class' => "col-sm-5", 'items' => ":POTgl"],
       ['class' => "col-sm-5", 'items' => ":POJam", 'style' => 'padding-right:10px;'],
      ],
     ],
     ['class' => "form-group col-md-14",
      'items'  => [
//       ['class' => "col-sm-3", 'items' => ":LabelLokasi"],
//       ['class' => "col-sm-6", 'items' => ":Lokasi"],
//       ['class' => "col-sm-1"],
       ['class' => "col-sm-2", 'items' => ":LabelSupplier"],
       ['class' => "col-sm-12", 'items' => ":Supplier"],
      ],
     ],
    ],
   ],
   ['class' => "form-group col-md-24",
    'items'  => [
     ['class' => "form-group col-md-10",
      'items'  => [
       ['class' => "col-sm-3", 'items' => ":LabelSONo"],
       ['class' => "col-sm-5", 'items' => ":SONo"],
       ['class' => "col-sm-2", 'items' => ":BtnSearch"],
       ['class' => "col-sm-1"],
       ['class' => "col-sm-3", 'items' => ":LabelSOTgl"],
       ['class' => "col-sm-5", 'items' => ":SOTgl"],
       ['class' => "col-sm-5", 'items' => ":SOJam", 'style' => 'padding-right:10px;'],
      ],
     ],
     ['class' => "form-group col-md-14",
      'items'  => [
        ['class' => "col-sm-3", 'items' => ":LabelTglAcc"],
        ['class' => "col-sm-3", 'items' => ":TglAcc"],
        ['class' => "col-sm-3", 'items' => ":AccJam"],
        ['class' => "col-sm-1"],
        ['class' => "col-sm-2", 'items' => ":LabelPOAcc", 'style' => 'padding-left:5px;'],
       ['class' => "col-sm-4", 'items' => ":POAcc"],
       ['class' => "col-sm-1", 'items' => ":LabelTC", 'style' => 'padding-left:5px;'],
       ['class' => "col-sm-7", 'items' => ":TC"],
      ],
     ],
    ],
   ],
   ['class' => "form-group col-md-24",
    'items'  => [
     ['class' => "form-group col-md-10",
      'items'  => [
       ['class' => "col-sm-3", 'items' => ":LabelPSNo"],
       ['class' => "col-sm-5", 'items' => ":PSNo"],
       ['class' => "col-sm-2", 'items' => ":BtnInfo"],
       ['class' => "col-sm-1"],
       ['class' => "col-sm-3", 'items' => ":LabelPSTgl"],
       ['class' => "col-sm-5", 'items' => ":PSTgl"],
       ['class' => "col-sm-5", 'items' => ":PSJam", 'style' => 'padding-right:10px;'],
      ],
     ],
     ['class' => "form-group col-md-14",
      'items'  => [
        ['class' => "col-sm-3", 'items' => ":LabelTglRelease"],
        ['class' => "col-sm-3", 'items' => ":TglRelease"],
        ['class' => "col-sm-3", 'items' => ":ReleaseJam"],
        ['class' => "col-sm-1"],
        ['class' => "col-sm-2", 'items' => ":LabelPOStatus", 'style' => 'padding-left:5px;'],
        ['class' => "col-sm-4", 'items' => ":POStatus"],
       ['class' => "col-sm-4", 'items' => ":Acc", 'style' => 'display: flex;justify-content: center;'],
       ['class' => "col-sm-4", 'items' => ":Tolak", 'style' => 'display: flex;justify-content: center;'],
      ],
     ],
    ],
   ],
   [
    'class' => "row col-md-24",
    'style' => "margin-bottom: 6px;",
    'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>',
   ],
   ['class' => "form-group col-md-24",
    'items'  => [
     ['class' => "form-group col-md-16",
      'items'  => [
       ['class' => "form-group col-md-24",
        'items'  => [
         ['class' => "col-sm-4", 'items' => ":KodeBarang"],
         ['class' => "col-sm-10", 'items' => ":BarangNama", 'style' => 'padding-left:3px;'],
         ['class' => "col-sm-4", 'items' => ":Gudang", 'style' => 'padding-left:3px;'],
         ['class' => "col-sm-4", 'items' => ":Satuan", 'style' => 'padding-left:3px;'],
         ['class' => "col-sm-2", 'items' => ":BtnInfo"],
        ],
       ],
       ['class' => "form-group col-md-24",
        'items'  => [
         ['class' => "col-sm-3", 'items' => ":LabelKeterangan"],
         ['class' => "col-sm-14", 'items' => ":Keterangan"],
         ['class' => "col-sm-1"],
         ['class' => "col-sm-2", 'items' => ":LabelCetak"],
         ['class' => "col-sm-3", 'items' => ":Cetak"],
        ],
       ],
      ],
     ],
     ['class' => "col-sm-1"],
     ['class' => "form-group col-md-7",
      'items'  => [
       ['class' => "form-group col-md-24",
        'items'  => [
         ['class' => "col-sm-6", 'items' => ":LabelSubTotal"],
         ['class' => "col-sm-17", 'items' => ":SubTotal"],
        ],
       ],
       ['class' => "form-group col-md-24",
        'items'  => [
         ['class' => "col-sm-6", 'items' => ":LabelDisc"],
         ['class' => "col-sm-4", 'items' => ":Disc"],
         ['class' => "col-sm-1", 'items' => ":Persen", 'style' => 'padding-left:1px;'],
         ['class' => "col-sm-12", 'items' => ":Nominal"],
        ],
       ],
       ['class' => "form-group col-md-24",
        'items'  => [
         ['class' => "col-sm-6", 'items' => ":LabelTotal"],
         ['class' => "col-sm-17   ", 'items' => ":Total"],
        ],
       ],
      ],
     ],
    ],
   ],
  ],
 ],
 ['class' => "row-no-gutters",
  'items'  => [
   ['class' => "col-md-24",
    'items'  => [
        [
            'class' => "col-md-12 pull-left",
            'items' => $this->render( '../_nav', [
                'url'=> $_GET['r'],
                'options'=> [
                    'PONo'         => 'No PO',
                    'PSNo'         => 'No PS',
                    'SONo'         => 'No SO',
                    'KodeTrans'    => 'Kode Trans',
                    'PosKode'      => 'Pos Kode',
                    'SupKode'      => 'Sup Kode',
                    'LokasiKode'   => 'Lokasi Kode',
                    'POKeterangan' => 'Keterangan',
                    'Cetak'        => 'Cetak',
                    'UserID'       => 'User ID',
                ],
            ])
        ],
     ['class' => "pull-right", 'items' => ":btnPrint :btnImport :btnAction"],
    ],
   ],
  ],
 ],
 [
  ":LabelPONo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No PO</label>',
  ":LabelPOTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl PO</label>',
  ":LabelSONo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No SO</label>',
  ":LabelSOTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl SO</label>',
  ":LabelPSNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor PS</label>',
  ":LabelPSTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl PS</label>',
  ":LabelTC"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">TC</label>',
  ":LabelSubTotal"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sub Total</label>',
  ":LabelKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
  ":LabelDisc"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Disc Final</label>',
  ":Persen"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">%</label>',
  ":LabelCetak"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Cetak</label>',
  ":LabelTotal"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total</label>',
  ":LabelSupplier"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Supplier</label>',
  ":LabelLokasi"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Lokasi Masuk</label>',
  ":LabelTglAcc"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tanggal Acc</label>',
  ":LabelTglRelease" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Release</label>',
  ":LabelPOAcc"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Acc</label>',
  ":LabelPOStatus"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',
  ":LabelJT"         => \common\components\General::labelGL($dsTBeli['NoGL'], 'JT'),
  ":PONo"            => Html::textInput('PONoView', $dsTBeli['PONoView'], ['class' => 'form-control', 'readonly' => 'readonly']) .
  Html::textInput('PONo', $dsTBeli['PONo'], ['class' => 'hidden']),
  ":SONo"            => Html::textInput('SONo', $dsTBeli['SONo'], ['class' => 'form-control', 'maxlength' => '10']),
  ":SOTgl"           => FormField::dateInput(['name' => 'SOTgl', 'config' => ['value' => General::asDate($dsTBeli['SOTgl'])]]),
  ":POTgl"           => FormField::dateInput(['name' => 'POTgl', 'config' => ['value' => General::asDate($dsTBeli['POTgl'])]]),
  ":PSTgl"           => FormField::dateInput(['name' => 'PSTgl', 'config' => ['value' => General::asDate($dsTBeli['PSTgl'])]]),
  ":TglAcc"          => FormField::dateInput(['name' => 'TglAcc', 'config' => ['value' => General::asDate($dsTBeli['TglAcc']), 'disabled'=> !\abengkel\components\Menu::showOnlyHakAkses(['Admin','Auditor'])]]),
  ":TglRelease"      => FormField::dateInput(['name' => 'TglRelease', 'config' => ['value' => General::asDate($dsTBeli['TglRelease'])]]),
  ":SOJam"           => FormField::timeInput(['name' => 'SOJam', 'config' => ['value' => $dsTBeli['SOTgl']]]),
  ":POJam"           => FormField::timeInput(['name' => 'POJam', 'config' => ['value' => $dsTBeli['POTgl']]]),
  ":PSJam"           => FormField::timeInput(['name' => 'PSJam', 'config' => ['value' => $dsTBeli['PSTgl']]]),
  ":AccJam"           => FormField::timeInput(['name' => 'AccJam', 'config' => ['value' => $dsTBeli['TglAcc'], 'disabled'=> !\abengkel\components\Menu::showOnlyHakAkses(['Admin','Auditor'])]]),
  ":ReleaseJam"           => FormField::timeInput(['name' => 'ReleaseJam', 'config' => ['value' => $dsTBeli['TglRelease']]]),
  ":PSNo"            => Html::textInput('PSNo', $dsTBeli['PSNo'], ['class' => 'form-control', 'maxlength' => '10', 'readonly' => true]),
  ":Supplier"        => FormField::combo('SupKode', ['config' => ['value' => $dsTBeli['SupKode']], 'extraOptions' => ['altLabel' => ['SupNama'], 'altCondition' => $altConditionSupKode]]),
  ":Lokasi"          => FormField::combo('LokasiKode', ['config' => ['value' => $dsTBeli['LokasiKode']], 'extraOptions' => ['simpleCombo' => true, 'altCondition' => $altConditionLokasiKode]]),
  ":TC"              => FormField::combo('KodeTrans', ['config' => ['value' => $dsTBeli['KodeTrans'], 'data' => [
    'POHO' => 'POHO - Purchase Order Hotline', 
    'PORG' => 'PORG - Purchase Order Reguler', 
    'POFX' => 'POFX - Purchase Order Fixed',
    'POPS' => 'POPS - Purchase Order Plus',
    ]], 'extraOptions' => ['simpleCombo' => true]]),
  ":POStatus"              => FormField::combo('POStatus', ['config' => ['disabled'=> !\abengkel\components\Menu::showOnlyHakAkses(['Admin','Auditor']),'value' => $dsTBeli['POStatus'], 'data' => [
    'Aju' => 'Aju', 'Terkirim' => 'Terkirim', 'Acc' => 'Acc', 'Acc.' => 'Acc.','Ditolak' => 'Ditolak','Ditolak.' => 'Ditolak.','Expired' => 'Expired'
  ]], 'extraOptions' => ['simpleCombo' => true]]),
  ":Acc"             => Html::checkbox('Acc', $dsTBeli['Acc'], ['label'=> 'Acc','class'=>'acctolak','disabled'=> !\abengkel\components\Menu::showOnlyHakAkses(['Admin','Auditor'])]),
  ":Tolak"           => Html::checkbox('Tolak', $dsTBeli['Tolak'], ['label'=> 'Tolak','class'=>'acctolak','disabled'=> !\abengkel\components\Menu::showOnlyHakAkses(['Admin','Auditor'])]),
  ":Keterangan"      => Html::textarea('POKeterangan', $dsTBeli['POKeterangan'], ['class' => 'form-control', 'maxlength' => '150']),
  ":POAcc"           => Html::textInput('POAcc', $dsTBeli['POAcc'], ['class' => 'form-control', 'readonly' => 'readonly']),
  ":Cetak"           => Html::textInput('Cetak', $dsTBeli['Cetak'], ['class' => 'form-control', 'readonly' => 'readonly']),
  ":Disc"            => FormField::integerInput(['name' => 'PODiscPersen', 'config' => ['id' => 'PODiscPersen', 'value' => 0]]),
  ":Nominal"         => FormField::numberInput(['name' => 'PODiscFinal', 'config' => ['value' => $dsTBeli['PODiscFinal']]]),
  ":SubTotal"        => FormField::decimalInput(['name' => 'POSubTotal', 'config' => ['id' => 'POSubTotal', 'value' => $dsTBeli['POSubTotal'], 'readonly' => 'readonly']]),
  ":Total"           => FormField::decimalInput(['name' => 'POTotal', 'config' => ['id' => 'POTotal', 'value' => $dsTBeli['POTotal'], 'readonly' => 'readonly']]),
  ":JT"              => '<input type="text"  class="form-control" name="">',
  ":KodeBarang"      => Html::textInput('KodeBarang', '', ['id' => 'KodeBarang', 'class' => 'form-control', 'readonly' => 'readonly']),
  ":BarangNama"      => Html::textInput('BarangNama', '', ['id' => 'BarangNama', 'class' => 'form-control', 'readonly' => 'readonly']),
  ":Gudang"          => Html::textInput('LokasiKodeTxt', '', ['class' => 'form-control', 'readonly' => 'readonly']),
  ":btnImport"       => Html::button('<i class="fa fa-arrow-circle-down"><br><br><br>Import</i>', ['class' => 'btn bg-navy ', 'id' => 'btnImport', 'style' => 'width:60px;height:60px']),

  ":Satuan"          => Html::textInput('lblStock', 0, ['type' => 'number', 'class' => 'form-control', 'style' => 'color: blue; font-weight: bold;', 'readonly' => 'readonly']),
  ":BtnInfo"         => '<button type="button" class="btn btn-default"><i class="fa fa-info-circle fa-lg"></i></button>',
  ":BtnSearch"       => '<button id="BtnSearch" type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>',
 ], [
        'url_main' => 'ttpohd',
        'url_id' => $_GET['id'] ?? '',
        '_akses' => '[PO] Order Pembelian',
 ]
);
ActiveForm::end();
?>
    </div>
<?php
$urlCheckStock = Url::toRoute(['site/check-stock-transaksi']);
$urlDetail     = $url['detail'];
$urlPrint      = $url['print'];
//$urlImport     = Url::toRoute(['ttpohd/import', 'tgl1' => Yii::$app->formatter->asDate('-5 day', 'yyyy-MM-dd')]);
$urlImport     = Url::toRoute(['ttpohd/import', 'tgl1' => Yii::$app->formatter->asDate('now', 'yyyy-MM-dd')]);
$urlDetailImport     = Url::toRoute(['ttpohd/import-loop']);
$readOnly      = ($_GET['action'] == 'view' ? 'true' : 'false');
$barang        = \common\components\General::cCmd(
 "SELECT BrgKode, BrgNama, BrgBarCode, BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual, BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus
                    FROM tdbarang
                    WHERE BrgStatus = 'A'
                    UNION
                    SELECT BrgKode, BrgNama, BrgBarCode,  BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual, BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus FROM tdBarang
                    WHERE BrgKode IN
                    (SELECT ttpoit.BrgKode FROM ttpoit INNER JOIN tdbarang ON ttpoit.BrgKode = tdbarang.BrgKode
                    WHERE ttpoit.poNo = :PONo AND tdBarang.BrgStatus = 'N' GROUP BY ttpoit.BrgKode)
                    ORDER BY BrgStatus, BrgKode", [':PONo' => $dsTBeli['PONo']])->queryAll();
$this->registerJsVar('__Barang', json_encode($barang));
$this->registerJsVar('__model', $dsTBeli);
$this->registerJs(<<< JS
     var __BrgKode = JSON.parse(__Barang);
     __BrgKode = $.map(__BrgKode, function (obj) {
                      obj.id = obj.BrgKode;
                      obj.text = obj.BrgKode;
                      return obj;
                    });
     var __BrgNama = JSON.parse(__Barang);
     __BrgNama = $.map(__BrgNama, function (obj) {
                      obj.id = obj.BrgNama;
                      obj.text = obj.BrgNama;
                      return obj;
                    });
     function HitungTotal(){
         	let POSubTotal = $('#detailGrid').jqGrid('getCol','Jumlah',false,'sum');
         	$('#POSubTotal').utilNumberControl().val(POSubTotal);
         	HitungBawah();
         }
         window.HitungTotal = HitungTotal;
         function HitungBawah(){
         	let PODiscPersen = parseFloat($('#PODiscPersen').val());
         	let POSubTotal = parseFloat($('#POSubTotal').val());
         	let PODiscFinal = PODiscPersen * POSubTotal / 100;
         	$('#PODiscFinal').utilNumberControl().val(PODiscFinal);
         	let POTotal = POSubTotal - PODiscFinal;
         	$('#POTotal').utilNumberControl().val(POTotal);
         }
         window.HitungBawah = HitungBawah;
         function checkStok(BrgKode,BarangNama){
           $.ajax({
                url: '$urlCheckStock',
                type: "POST",
                data: {
                    BrgKode: BrgKode,
                },
                success: function (res) {
                    $('[name=LokasiKodeTxt]').val(res.LokasiKode);
                    $('#KodeBarang').val(BrgKode);
                    $('[name=BarangNama]').val(BarangNama);
                    $('[name=lblStock]').val(res.SaldoQty);
                },
                error: function (jXHR, textStatus, errorThrown) {
                },
                async: false
            });
          }
          window.checkStok = checkStok;
     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 220,
        extraParams: {
            PONo: $('input[name="PONo"]').val()
        },
        rownumbers: true,
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,
        rownumbers: true,
        colModel: [
            { template: 'actions'  },
            {
                name: 'BrgKode',
                label: 'Kode',
                width: 115,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __BrgKode,
                    events:{
                        
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
						    $('[id="'+rowid+'_'+'POHrgJual'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgBeli));
                            $('[id="'+rowid+'_'+'POHrgBeli'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgBeli));
                            $('[id="'+rowid+'_'+'BrgGroup'+'"]').val(data.BrgGroup);
                            $('[id="'+rowid+'_'+'POQtyAju'+'"]').val(1);
                            $('[id="'+rowid+'_'+'POQty'+'"]').val(1);
                            $('[id="'+rowid+'_'+'POSaldo'+'"]').val(data.BrgMinStock);
                            window.hitungLine();
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').val(data.BrgNama); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').trigger('change');
                            checkStok($(this).val(),$('[id="'+rowid+'_'+'BrgNama'+'"]').val());
						},
						'change': function(){
                            let rowid = $(this).attr('rowid');
                            checkStok($(this).val(),$('[id="'+rowid+'_'+'BrgNama'+'"]').val());
						}
                    }
                }
            },
            {
                name: 'BrgNama',
                label: 'Nama Barang',
                width: 190,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __BrgNama,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;

						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
						    $('[id="'+rowid+'_'+'POHrgJual'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgBeli));
                            $('[id="'+rowid+'_'+'POHrgBeli'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgBeli));
                            $('[id="'+rowid+'_'+'BrgGroup'+'"]').val(data.BrgGroup);
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').val(data.BrgKode); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').trigger('change');
                            window.hitungLine();
						}
                    }
                }
            },
            {
                name: 'POSaldo',
                label: 'Saldo',
                editable: true,
                width: 75,
                template: 'number'
            },
            {
                name: 'POQtyAju',
                label: 'QtyAju',
                editable: true,
                width: 75,
                template: 'number',
                editor: {
                    events:{
                        'change': function(e) { 
                            let rowid = $(this).attr('rowid');
                            let QtyAjuVal = $('[id="'+rowid+'_'+'POQtyAju'+'"]').val();
                            console.log(QtyAjuVal);
                            $('[id="'+rowid+'_'+'POQty'+'"]').val(QtyAjuVal); 
                            $('[id="'+rowid+'_'+'POQty'+'"]').trigger('change'); 
                        }
                    }                              
                }
            },
            {
                name: 'POQty',
                label: 'QtyAcc',
                editable: true,
                width: 75,
                template: 'number'
            },
            {
                name: 'BrgSatuan',
                label: 'Satuan',
                editable: true,
                width: 90,
            },
            {
                name: 'POHrgJual',
                label: 'Harga Part',
                width: 120,
                editable: true,
                template: 'money'
            },
            {
                name: 'POHrgBeli',
                label: 'Harga Part',
                width: 120,
                editable: true,
                template: 'money',
                hidden: true
            },
            
            {
                name: 'Disc',
                label: 'Disc %',
                editable: true,
                width: 100,
                template: 'number',
                hidden: true
            },
            {
                name: 'PODiscount',
                label: 'Nilai Disc',
                width: 120,
                editable: true,
                template: 'money',
                hidden: true
            },
            
            {
                name: 'Jumlah',
                label: 'Jumlah',
                width: 120,
                editable: true,
                template: 'money'
            },
            {
                name: 'POSLAwal',
                label: 'SL Awal',
                editable: true,
                width: 75,
                template: 'number'
            },
            {
                name: 'POSLNow',
                label: 'SL Now',
                editable: true,
                width: 75,
                template: 'number'
            },
            {
                name: 'POPajak',
                label: 'Pajak',
                editable: true,
                template: 'money',
                hidden: true
            },
            {
                name: 'SONo',
                label: 'SONo',
                editable: true,
                hidden: true
            },
            {
                name: 'POAuto',
                label: 'POAuto',
                editable: true,
                hidden: true
            },
            {
                name: 'PSNo',
                label: 'PSNo',
                editable: true,
                hidden: true
            },
            {
                name: 'PSQtyS',
                label: 'PSQtyS',
                editable: true,
                hidden: true,
                template: 'money',
                
            },
        ],
        listeners: {
            afterLoad: function (data) {
             window.HitungTotal();
            }
        },
        onSelectRow : function(rowid,status,e){
            if (window.rowId !== -1) return;
            if (window.rowId_selrow === rowid) return;
            if(rowid.includes('new_row') === true){
                $('[name=KodeBarang]').val('--');
                $('[name=LokasiKodeTxt]').val('Kosong');
                $('[name=BarangNama]').val('--');
                $('[name=lblStock]').val(0);
                return ;
            };
            let r = $('#detailGrid').utilJqGrid().getData(rowid).data;
            checkStok(r.BrgKode,r.BrgNama);
            window.rowId_selrow = rowid;
        }
    }).init().fit($('.box-body')).load({url: '$urlDetail'});
    window.rowId_selrow = -1;

     function hitungLine(){

            let Disc = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Disc'+'"]').val()));
            let POHrgJual = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'POHrgJual'+'"]').val()));
            let POQty = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'POQty'+'"]').val()));
            let PODiscount = Disc*POHrgJual*POQty / 100;
            $('[id="'+window.cmp.rowid+'_'+'PODiscount'+'"]').utilNumberControl().val(PODiscount);
            $('[id="'+window.cmp.rowid+'_'+'Jumlah'+'"]').utilNumberControl().val(POHrgJual * POQty - PODiscount);
            window.HitungTotal();
         }
         window.hitungLine = hitungLine;

         $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
	     window.cmp.rowid = rowid;

          if(rowid.includes('new_row')){
              var BrgKode = $('[id="'+rowid+'_'+'BrgKode'+'"]');
              var dtBrgKode = BrgKode.select2('data');
              if (dtBrgKode.length > 0){
                  var dt = dtBrgKode[0];
                  BrgKode.val(dt.BrgKode).trigger('change').trigger({
                    type: 'select2:select',
                    params: {
                        data: dt
                    }
                });
              }
            $('[id="'+rowid+'_'+'POQty'+'"]').val(1);
          }

          $('[id="'+window.cmp.rowid+'_'+'POQty'+'"]').on('keyup', function (event){
              hitungLine();
          });
          $('[id="'+window.cmp.rowid+'_'+'POHrgJual'+'"]').on('keyup', function (event){
              hitungLine();
          });
          $('[id="'+window.cmp.rowid+'_'+'Disc'+'"]').on('keyup', function (event){
              hitungLine();
          });
         hitungLine();
	 });

      $('#btnSave').click(function (event) {
          
          let Nominal = parseFloat($('#POSubTotal-disp').inputmask('unmaskedvalue'));
          //dimatikan req 09012025
          //  if (Nominal === 0 ){
          //      bootbox.alert({message:'Subtotal tidak boleh 0', size: 'small'});
          //      return;
          //  }
        $('input[name="SaveMode"]').val('ALL');
        $('[name="TglAcc"]').prop("disabled", false);
        $('[name="AccJam"]').prop("disabled", false);
        $('[name="POStatus"]').prop("disabled", false);
        $('#form_ttpohd_id').attr('action','{$url['update']}');
        $('#form_ttpohd_id').submit();
	  });

	  $('#PODiscPersen').change(function (event) {
	      HitungBawah();
	  });
JS
);
$urlSO = Url::toRoute(['ttsohd/select','loop'=>'PO']);
$this->registerJsVar('__urlSO', $urlSO);
$this->registerJsVar('__userId', Yii::$app->session->get( '__id' ));
$this->registerJs(<<< JS

    window.getUrlSO = function(){
        let SONo = $('input[name="SONo"]').val();
        return __urlSO + ((SONo === '' || SONo === '--') ? '':'&check=off&cmbTxt1=SONo&txt1='+SONo);
    }

    $('#BtnSearch').click(function()
     {
        window.util.app.dialogListData.show({
            title: 'Daftar Sales Order / Order Penjualan',
            url: getUrlSO()
        },
        function (data) {
             console.log(data);
              if (!Array.isArray(data)){
                 return;
             }
             var itemData = [];
             data.forEach(function(itm,ind) {
                let cmp = $('input[name="SONo"]');
                cmp.val(itm.header.data.SONo);
                itm.data.SONo = itm.header.data.SONo;
                itm.data.PONo = __model.PONo;
                itm.data.POHrgJual = itm.data.SOHrgJual;
                itm.data.POQty = itm.data.SOQty;
                itm.data.PODiscount = itm.data.SODiscount;
                itemData.push(itm.data);
            });
            $.ajax({
             type: "POST",
             url: '$urlDetail',
             data: {
                 oper: 'batch',
                 data: itemData,
                 header: btoa($('#form_ttpohd_id').serialize())
             },
             success: function(data) {
               window.location.href = "{$url['update']}&oper=skip-load&PONoView={$dsTBeli['PONoView']}";
             },
           });
        })
    });

     $('#btnImport').click(function (event) {
        window.util.app.dialogListData.show({
            title: 'Manage Parts Sales - PRSL - SI',
            url: '{$urlImport}'
        },
        function (data) {
            $.ajax({
				type: "POST",
				url: '$urlDetailImport',
				data: {
					oper: 'import',
					data: data,
					header: btoa($('#form_ttpohd_id').serialize())
				},
				success: function(data) {
                    window.location.href = "{$url['update']}&oper=skip-load&PONoView={$dsTBeli['PONoView']}";
				},
			});
        });
    });

    $('.acctolak').click(function (event) {
        if(!event.isTrigger){
            var accChecked = $('[name="Acc"]').is(':checked');
            var tolakChecked = $('[name="Tolak"]').is(':checked');
            if(!accChecked && !tolakChecked){
                $('[name="POAcc"]').val('--');
                $('[name="POStatus"]').val('Terkirim').trigger('change');
                return;
            }
            if($(this).attr('name') == 'Acc' && accChecked){
                tolakChecked = false;
                $('[name="Tolak"]').prop('checked', false);
            }
            if($(this).attr('name') == 'Tolak' && tolakChecked){
                accChecked = false;
                $('[name="Acc"]').prop('checked', false);
            }
            if(accChecked && !tolakChecked){
                $('[name="POAcc"]').val(__userId);
                $('[name="POStatus"]').val('Acc').trigger('change');
            }
            if(!accChecked && tolakChecked){
                $('[name="POAcc"]').val(__userId);
                $('[name="POStatus"]').val('Ditolak').trigger('change');
            }
        }
    });

    $('[name="POStatus"]').change(function (event) {
        const approve = ["Acc", "Acc."];
        const reject = ["Ditolak", "Ditolak."];
        var thisValue = $(this).val();
        if(reject.includes(thisValue)){
            $('[name="Tolak"]').prop('checked', true);
            $('[name="Acc"]').prop('checked', false);
        }
        if(approve.includes(thisValue)){
            $('[name="Acc"]').prop('checked', true);
            $('[name="Tolak"]').prop('checked', false);
        }
    });

    $('[name="POStatus"]').trigger('change');
JS
);
