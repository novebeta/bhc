<?php
use abengkel\components\FormField;
use yii\bootstrap\Html;
use yii\helpers\Url;
if(!$_GET['action'] || (isset($_GET['action']) && $_GET['action'] != 'display')){
    return;
}
$urlNav = Url::toRoute(['site/navigation']);
$_nav = Yii::$app->session['_navSearch'];
$cmbSearch = null;
$inSearch = null;
$linkBack = null;
$linkNext = null;
$current = 0;
$total = 0;
if(!isset($_nav['url']) || $_nav['url'] !== $url){
    $_nav = [
        'url'=> $url,
        'cmbSearch'=> null,
        'inSearch'=> null,
        'data'=> null,
        'linkBack'=> null,
        'linkNext'=> null,
        'current'=> null,
        'total'=> null,
    ];
    Yii::$app->session['_navSearch'] = $_nav;
}else{
    $cmbSearch = $_nav['cmbSearch'];
    $inSearch = $_nav['inSearch'];
    $linkBack = $_nav['linkBack'];
    $linkNext = $_nav['linkNext'];
    $current = $_nav['current'];
    $total = $_nav['total'];
}
$current++;
?>
<style>
    .wd-auto {
        width: auto;
    }
</style>
<form id="filter-bottom-form">
    <div class=" col-sm-8" style="margin-bottom: 2px;">
        <?php echo FormField::combo( 'comboSearch', [ 'config' => [ 'data' => $options,'value'=> $cmbSearch ], 'options' => ['tabindex'=>'3'], 'extraOptions' => [ 'simpleCombo' => true ] ] ) ?>
    </div>
    <div class=" col-sm-12">
        <?php echo Html::textInput( 'inputSearch', $inSearch, [ 'class' => 'form-control', 'style'=> 'margin-left:2px;' ] ) ?>
    </div>
    <div class=" col-sm-24">
        <div class=" col-sm-3">
            <?php echo Html::button( '<i class="fas fa-fast-backward"></i>', ['id'=>'btnFirst', 'class' => 'btn-default btn bg-grey btn-block','disabled'=>($total<=0)] ); ?>
        </div>
        <div class=" col-sm-3">
            <?php echo Html::button( '<i class="fas fa-backward"></i>', [ 'id'=>'btnBack', 'class' => 'btn-default btn bg-grey btn-block' ,'disabled'=> ($linkBack<0)] ); ?>
        </div>
        <div class=" col-sm-7" style="margin-left:2px;">
            <div class=" col-sm-12">
                <?php echo FormField::numberInput( [ 'name' => 'navCurrent', 'config' => [ 'value' => $current,'maskedInputOptions'=> [ 'groupSeparator' => '', 'radixPoint' => ',', 'digits' => 0, 'digitsOptional' => true ],'disabled'=>($total <= 0) ] ] ); ?>
            </div>
            <div class=" col-sm-5" style="margin-left:10px;">
                <?php echo '<label class="control-label" >/</label>'; ?>
            </div>
            <div class=" col-sm-3">
                <?php echo '<label class="control-label" id="navTotal">'.$total.'</label>'; ?>
            </div>
        </div>
        <div class=" col-sm-3" style="margin-left: 21px;">
            <?php echo Html::button( '<i class="fas fa-forward"></i>', [ 'id'=>'btnNext', 'class' => 'btn-default btn bg-grey btn-block','disabled'=>($current == $total)] ); ?>
        </div>
        <div class=" col-sm-3">
            <?php echo Html::button( '<i class="fas fa-fast-forward"></i>', [ 'id'=>'btnLast','class' => 'btn-default btn bg-grey btn-block' ,'disabled'=>($total<=0)] ); ?>
        </div>
    </div>
</form>
<?php $this->registerJs(<<< JS
    
    $('[name=inputSearch]').on('keypress', function (e) {
        if(e.which === 13){
            $('[name=inputSearch]').attr("disabled", "disabled");
            $.ajax({
                type: "POST",
                url: '$urlNav',
                data: {
                    oper: 'search',
                    url: '$url',
                    cmbSearch: $('[name=comboSearch]').val(),
                    inSearch: $('[name=inputSearch]').val()
                },
                success: function(data) {
                    updateNav(data);
                    redirectNewPage(data['currentUrl']);
                },
                complete: function(){
                    $('[name=inputSearch]').removeAttr("disabled");
                }
            });
        }
    });

    $('#btnFirst,#btnBack,#btnNext,#btnLast').on('click', function (e) {
        var whichID = $(this).attr('id');
        console.log(whichID);
        var session = Lockr.get('_storeNav');
        var newValue = 1;
        switch (whichID) {
            case 'btnBack':
                newValue = parseInt(session['current']);
                break;
            case 'btnNext':
                newValue = parseInt(session['current']) + 2;
                break;
            case 'btnLast':
                newValue = parseInt(session['total']);
                break;
        }
        // $('#navCurrent-disp').attr("disabled", "disabled");
        var linkNext = newValue;
        newValue--;
        var current = newValue;
        newValue--;
        var linkBack = newValue;
        $.ajax({
            type: "POST",
            url: '$urlNav',
            data: {
                oper: 'update',
                linkBack,
                linkNext,
                current
            },
            success: function(data) {
                updateNav(data);
                redirectNewPage(data['currentUrl']);
            },
            complete: function(){
                // $('#navCurrent-disp').removeAttr("disabled");
            }
        });
    });

    $('#navCurrent-disp').on('keypress', function (e) {
        if(e.which === 13){
            var newValue = $('[name=navCurrent]').utilNumberControl().val();
            var session = Lockr.get('_storeNav');
            if(newValue < 0 || newValue > parseInt(session['total'])){
                $('[name=navCurrent]').utilNumberControl().val(parseInt(session['current'])+1);
                return;
            }
            $('#navCurrent-disp').attr("disabled", "disabled");
            var linkNext = newValue;
            newValue--;
            var current = newValue;
            newValue--;
            var linkBack = newValue;
            $.ajax({
                type: "POST",
                url: '$urlNav',
                data: {
                    oper: 'update',
                    linkBack,
                    linkNext,
                    current
                },
                success: function(data) {
                    updateNav(data);
                    redirectNewPage(data['currentUrl']);
                },
                complete: function(){
                    $('#navCurrent-disp').removeAttr("disabled");
                }
            });
        }
    });

    function updateNav(session){
        Lockr.set('_storeNav', session);
        (session['linkBack'] == '-1') ? $('#btnBack').attr('disabled',true) : $('#btnBack').removeAttr("disabled");
        (session['current'] == session['total']) ? $('#btnNext').attr('disabled',true) : $('#btnNext').removeAttr("disabled");
        $('#navTotal').html(session['total']);
        $('[name=navCurrent]').utilNumberControl().val(parseInt(session['current'])+1);
        if(parseFloat(session['total']) <= 0){
            $('#btnFirst').attr('disabled',true);
            $('#btnLast').attr('disabled',true);
            $('#navCurrent-disp').attr('disabled',true);
        }else{
            $('#btnFirst').removeAttr("disabled");
            $('#btnLast').removeAttr("disabled");
            $('#navCurrent-disp').removeAttr("disabled");
        }
    }

    function redirectNewPage(id) {
        var url = new URL(window.location);
        (url.searchParams.has('id') ? url.searchParams.set('id', id) : url.searchParams.append('id', id));
        url.search = url.searchParams;
        url        = url.toString();
        window.location = url;
    }

JS
);