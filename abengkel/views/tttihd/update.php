<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tttihd */
$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = 'Edit - Transfer: ' . $dsTMutasi['TINoView'];
?>
<div class="tttihd-update">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTMutasi' => $dsTMutasi,
        'id'     => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail'       => Url::toRoute( [ 'tttiit/index','action' => 'update' ] ),
        ]
    ]) ?>

</div>
