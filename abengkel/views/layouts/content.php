<?php
use yii\widgets\Breadcrumbs;
?>
<div class="content-wrapper">
    <section class="content-header">
		<?php if ( isset( $this->blocks[ 'content-header' ] ) ) { ?>
            <h1><?= $this->blocks[ 'content-header' ] ?></h1>
		<?php } else { ?>
            <h1>
				<?php
				if ( $this->title !== null ) {
					echo \yii\helpers\Html::encode( $this->title );
				} else {
					echo \yii\helpers\Inflector::camel2words(
						\yii\helpers\Inflector::id2camel( $this->context->module->id )
					);
					echo ( $this->context->module->id !== \Yii::$app->id ) ? '<small>Module</small>' : '';
				} ?>
            </h1>
		<?php } ?>

		<?=
		Breadcrumbs::widget( [
			'homeLink' => [
				'label'  => '<span class="fa fa-home"></span> Home',
				'url'    => \yii\helpers\Url::home(),
				'encode' => false,
			],
			'links'    => isset( $this->params[ 'breadcrumbs' ] ) ? $this->params[ 'breadcrumbs' ] : [],
		] ) ?>
    </section>
    <section class="content">
		<? //=Alert::widget() ?>
		<?= $content ?>
    </section>
</div>
<footer class="main-footer" style="padding: 2px 15px 2px 15px">
    <div id="timer" class="pull-right hidden-xs">
    </div>
    BHC Honda System Release Date 01 November 2020 | Login :
	<?= Yii::$app->formatter->asDatetime( Yii::$app->session->get( '_loginTime' ), 'dd-MM-yyyy HH:mm:ss' ); ?> | Durasi : <span id="duration"></span>
</footer>