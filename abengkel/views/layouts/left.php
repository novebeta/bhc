<aside class="main-sidebar">
    <section class="sidebar">
        <!-- search form (Optional) -->
        <form method="get" class="sidebar-form" id="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" style="background-color: #e2e2e2"
                       placeholder="Search..." id="search-input">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"
                            style="background-color: #e2e2e2">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <ul class="sidebar-menu" data-widget="tree">
			<?= Yii::$app->session->get( '_nav' ); ?>
        </ul>
    </section>
</aside>
