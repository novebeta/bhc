<?php
use abengkel\components\FormField;
use kartik\checkbox\CheckboxX;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttgeneralledgerhd */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttgeneralledgerhd-form">
		<?php
		$GLValid_option = [
			'onclick' => "$(this).parent().prev().val(this.checked? 'Sudah' : 'Belum')"
		];
		if ( $model->GLValid === 'Sudah' ) {
			$GLValid_option[ 'checked' ] = 1;
		}
		$form = ActiveForm::begin( [ 'id' => 'form_ttgeneralledgerhd_id', 'action' => $url[ 'update' ] ] );
		\abengkel\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "form-group col-md-12",
						    'items' => [
							    [ 'class' => "form-group col-md-24",
							      'items' => [
								      [ 'class' => "col-sm-4", 'items' => ":LabelGL" ],
								      [ 'class' => "col-sm-14", 'items' => ":NoGL" ],
								      [ 'class' => "col-sm-1" ],
								      [ 'class' => "col-sm-5", 'items' => ":TglGL" ],
							      ],
							    ],
							    [ 'class' => "form-group col-md-24",
							      'items' => [
								      [ 'class' => "col-sm-4", 'items' => ":LabelKeterangan" ],
								      [ 'class' => "col-sm-20", 'items' => ":GLKeterangan" ],
							      ],
							    ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-12",
						    'items' => [
							    [ 'class' => "form-group col-md-24",
							      'items' => [
								      [ 'class' => "col-sm-9" ],
								      [ 'class' => "col-sm-5", 'items' => ":LabelValidasi" ],
								      [ 'class' => "col-sm-8", 'items' => ":Validasi", 'style' => 'padding-right:2px;' ],
								      [ 'class' => "col-sm-2", 'items' => ":Check" ],
							      ],
							    ],
							    [ 'class' => "form-group col-md-24",
							      'items' => [
								      [ 'class' => "col-sm-9" ],
								      [ 'class' => "col-sm-5", 'items' => ":LabelTransaksi" ],
								      [ 'class' => "col-sm-8", 'items' => ":Transaksi" ],
								      [ 'class' => "col-sm-2", 'items' => ":SearchTrans" ],
							      ],
							    ],
							    [ 'class' => "form-group col-md-24",
							      'items' => [
								      [ 'class' => "col-sm-9" ],
								      [ 'class' => "col-sm-5", 'items' => ":LabelHutang" ],
								      [ 'class' => "col-sm-2", 'items' => ":btnInfoLink" ],
								      [ 'class' => "col-sm-6", 'items' => ":Hutang" ],
								      [ 'class' => "col-sm-2", 'items' => ":SearchHtg" ],
							      ],
							    ],
						    ],
						  ],
					  ],
					],
					[
						'class' => "row col-md-24",
						'style' => "margin-bottom: 3px;",
						'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
					],
					[
						'class' => "form-inline pull-right",
						'items' => ":LabelSelisih :Selisih :LabelTotal :TotalDebetGL :TotalKreditGL"
					]
				]
			],
            [ 'class' => "row-no-gutters",
                'items' => [
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'NoGL'    => 'No. Jurnal',
                                'MemoGL'  => 'Memo Jurnal',
                                'GLLink'  => 'Link Transaksi',
                                'HPLink'  => 'Link Hutang-Piutang',
                                'NoAccount'  => 'No. Perkiraan',
                                'NamaAccount'  => 'Nama Perkiraan',
                                'KeteranganGL'  => 'Keterangan',
                            ],
                        ])
                    ],
                    [
                        'class' => "pull-right",
                        'items' => ":btnPrint :btnAction"
                    ],
                ]
            ],
			[
				":LabelGL"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Jurnal</label>',
				":LabelKeterangan"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Memo</label>',
				":LabelValidasi"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Validasi Jurnal</label>',
				":LabelTransaksi"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Link Transaksi</label>',
				":LabelHutang"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Hutang - Piutang</label>',
				":LabelSelisih"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Selisih</label>',
				":LabelTotal"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total</label>',
                ":NoGL"            => Html::textInput( 'NoGL', $dsJurnal[ 'NoGL' ], [ 'class' => 'hidden' ] ) .
                    Html::textInput( 'KodeTrans', $dsJurnal[ 'KodeTrans' ], [ 'class' => 'hidden' ] ) .
                    Html::textInput( 'NoGLView', $dsJurnal[ 'NoGLView' ], [ 'class' => 'form-control', 'readonly' => 'readonly', 'tabindex' => '1' ] ),
                ":GLKeterangan"    => Html::textarea( 'MemoGL', $dsJurnal[ 'MemoGL' ], [ 'class' => 'form-control',  'tabindex' => '3' ] ),
                ":Validasi"        => Html::textInput( 'GLValid', $dsJurnal[ 'GLValid' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
                ":Transaksi"       => Html::textInput( 'GLLink', $dsJurnal[ 'GLLink' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
                ":Hutang"          => Html::textInput( 'HPLink', $dsJurnal[ 'HPLink' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
                ":TglGL"           => FormField::dateInput( [ 'name' => 'TglGL', 'config' => [ 'value' => $dsJurnal[ 'TglGL' ] ] ] ),
                ":SearchTrans"     => Html::button( '<i class="glyphicon glyphicon-search"></i>', [ 'class' => 'btn btn-default', 'id' => 'SearchTrans', 'tabindex' => '4' ] ),
                ":SearchHtg"       => Html::button( '<i class="glyphicon glyphicon-search"></i>', [ 'class' => 'btn btn-default', 'id' => 'SearchHtg', 'tabindex' => '6' ] ),
                ":btnInfoLink"     => Html::button( '<i class="fa fa-info-circle fa-lg"></i>', [ 'class' => 'btn btn-default', 'id' => 'BtnInfoLink' , 'tabindex' => '5'] ),
                ":Check"           => CheckboxX::widget( [ 'id' => 'check', 'value' => ( $dsJurnal[ 'GLValid' ] === 'Sudah' ), 'name' => 'check', 'readonly' => !\abengkel\components\Menu::showOnlyHakAkses(['Admin','Auditor']),
                    'pluginOptions' => [ 'threeState' => false, 'size' => 'lg', ], ] ),
                ":Selisih"         => FormField::numberInput( [ 'name' => 'Selisih', 'config' => [ 'id' => 'Selisih', 'value' => 0 ] ] ),
                ":TotalDebetGL"    => FormField::numberInput( [ 'name' => 'TotalDebetGL', 'config' => [ 'id' => 'TotalDebetGL', 'value' => $dsJurnal[ 'TotalDebetGL' ] ] ] ),
                ":TotalKreditGL"   => FormField::numberInput( [ 'name' => 'TotalKreditGL', 'config' => [ 'id' => 'TotalKreditGL', 'value' => $dsJurnal[ 'TotalKreditGL' ] ] ] ),
                ":btnSave"       => Html::button( '<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i> ', [ 'class' => 'btn btn-info btn-primary ', 'id' => 'btnSave','style' => 'width:60px;height:60px' ] ),
                ":btnPrint"        => Html::Button( '<i class="glyphicon glyphicon-print"> <br><br> Print</i> ', [ 'class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px', ] ),
                ":btnCancel"       => Html::Button( '<i class="fa fa-ban"><br><br>Batal</i>', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel','style' => 'width:60px;height:60px', ] ),
			],
            [
                'url_main'         => 'ttgeneralledgerhd',
                'url_delete'        => Url::toRoute(['ttgeneralledgerhd/delete', 'id' => $_GET['id'] ?? '','type' => $view ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlAdd               = Url::toRoute( [ 'ttgeneralledgerhd/' . $view . '-create', 'action' => 'create' ] );
$urlIndex             = Url::toRoute( [ 'ttgeneralledgerhd/' . $view ] );
$urlDetail            = $url[ 'detail' ];
$urlPrint             = $url[ 'print' ];
$urlHutang            = Url::toRoute( [ 'ttgeneralledgerhd/select-mm-hutang', 'NoGL' => $dsJurnal[ 'NoGL' ] ] );
$urlPiutangHutang     = Url::toRoute( [ 'ttgeneralledgerhd/td' ] );
$urlPiutangHutangAjax = Url::toRoute( [ 'ttgeneralledgerhd/link-piutang-hutang' ] );
$urlHutangPiutang     = Url::toRoute( [ 'ttgeneralledgerhd/show-hutang-piutang', 'NoGL' => $dsJurnal[ 'NoGL' ] ] );
$urlSearchTrans       = Url::toRoute( [ 'ttgeneralledgerhd/search-trans', 'NoGL' => $dsJurnal[ 'NoGL' ], 'TglGL' => $dsJurnal[ 'TglGL' ] ] );
$urlSearchTransAfter  = Url::toRoute( [ 'ttgeneralledgerhd/search-trans-after', 'NoGL' => $dsJurnal[ 'NoGL' ], 'TglGL' => $dsJurnal[ 'TglGL' ] ] );
$readOnly             = ( ( $_GET[ 'action' ] == 'view' || $_GET[ 'action' ] == 'display' ) ? 'true' : 'false' );
$sqlComm              = "SELECT NoAccount, NamaAccount, OpeningBalance, JenisAccount, StatusAccount, NoParent FROM traccount 
WHERE JenisAccount = 'Detail' ORDER BY NoAccount";
switch ( $dsJurnal[ 'KodeTrans' ] ) {
    case 'JT':
        $sqlComm = "SELECT NoAccount, NamaAccount, StatusAccount FROM TRAccount 
            WHERE JenisAccount = 'Detail' AND NoAccount <> '26040000'
            ORDER BY StatusAccount, NoAccount , StatusAccount";
        break;
    case 'GL':
        $sqlComm = "SELECT NoAccount, NamaAccount, StatusAccount FROM TRAccount 
            WHERE JenisAccount = 'Detail' AND NoAccount <> '26040000' 
            AND LEFT(NoAccount,4) NOT IN ('1101','1102','1103','1104','1105') 
            UNION SELECT NoAccount, NamaAccount, StatusAccount FROM TRAccount 
            WHERE NoAccount = '11010200'
            ORDER BY StatusAccount, NoAccount";
        break;
    case 'JA':
        $sqlComm = "SELECT NoAccount, NamaAccount 
            FROM TRAccount 
            WHERE (NoAccount IN ('11060100', '11070400', '11110100', '11110200', '11150100', '11160100', '11170300', '11170400', '11170500', '24030000', '24040100', '24040300', '24040400', '24040600', '24040700', '24050100', '24050100', '30010000', '30030000', '30040000', '40040300', '40990000', '50000000', '60020000', '60050000', '60060000', '60080000', '60130000', '60140000', '60380000') 
                OR NoParent = '11090000' 
                OR NoParent = '40010000' 
                OR NoParent = '11080000') 
              AND JenisAccount = 'Detail' AND NoAccount <> '26040000' 
            ORDER BY StatusAccount, NoAccount, StatusAccount";
        break;
    case 'JP':
        $sqlComm = "SELECT NoAccount, NamaAccount FROM TRAccount 
                        WHERE (LEFT(NoAccount,6) = '240205' 
                                 OR LEFT(NoAccount,4) = '2406' 
                                 OR LEFT(NoAccount,1) = '4' 
                                 OR (LEFT(NoAccount,4) BETWEEN '1106' AND '1113')
                                 OR NoAccount IN ('12030000','11110400','24020100','24020200','24030000')
                                 OR NoParent = '11080000'
                                 OR NoParent = '24020300'
                                 OR NoParent = '24050000'
                                 OR NoParent = '24050600'
                                 OR NoParent = '25010000'
                                 OR NoAccount = '24050100' 
                                 OR NoAccount = '24010102'
                                 OR NoAccount = '24010200'
                                 OR NoAccount = '24010101' ) 
                        AND JenisAccount = 'Detail' AND NoAccount <> '26040000'
            ORDER BY StatusAccount, NoAccount , StatusAccount";
        break;
}
$DtAccount = \common\components\General::cCmd( $sqlComm )->queryAll();
$this->registerJsVar( '__dtAccount', json_encode( $DtAccount ) );
$this->registerJsVar( '__urlPiutangHutang', $urlPiutangHutang );
$arr                           = [
	'cmbTxt'    => [
		'GLLink'  => 'Link Transaksi',
		'NoGL'    => 'No. Jurnal',
		'HPLink'  => 'Link Hutang-Piutang',
		'NoAccount'  => 'No. Perkiraan',
		'NamaAccount'  => 'Nama Perkiraan',
		'KeteranganGL'  => 'Keterangan',
		//		'GLValid' => 'Validasi',
		'MemoGL'  => 'Memo Jurnal',
	],
	'cmbTgl'    => [
		'TglGL' => 'Tgl Jurnal',
	],
	'cmbNum'    => [
		'TotalDebetGL'  => 'Debet',
		'TotalKreditGL' => 'Kredit',
	],
	'sortname'  => "TglGL",
	'sortorder' => 'asc',
];
$this->registerJsVar( 'setcmb', $arr );
$this->registerJsVar('__msgHutangPiutang', $this->render( '../_search' ));
$this->registerJs( <<< JS

    var __NoAccount = JSON.parse(__dtAccount);
     __NoAccount = $.map(__NoAccount, function (obj) {
                      obj.id = obj.NoAccount;
                      obj.text = obj.NoAccount;
                      return obj;
                    });
     var __NamaAccount = JSON.parse(__dtAccount);
     __NamaAccount = $.map(__NamaAccount, function (obj) {
                      obj.id = obj.NamaAccount;
                      obj.text = obj.NamaAccount;
                      return obj;
                    });
     $('#detailGrid').utilJqGrid({     
        editurl: '$urlDetail',
        height: 255,
        extraParams: {
            NoGL: $('input[name="NoGL"]').val(),
            TglGL: $('input[name="TglGL"]').val(),
        },
        rownumbers: true,
        loadonce:true,
        rowNum: 1000,  
        pgbuttons: false,
        pginput: false,
        pgtext: "",
        readOnly: $readOnly,         
        colModel: [
            { template: 'actions' },
            {
                name: 'NoAccount',
                label: 'Nomor',
                editable: true,
                width: 100,
                editor: {
                    type: 'select2',
                    data: __NoAccount,  
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    // console.log(data);
						    let rowid = $(this).attr('rowid');
                            $('[id="'+rowid+'_'+'NamaAccount'+'"]').val(data.NamaAccount); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'NamaAccount'+'"]').trigger('change');
						}
                    }                  
                }
            },
            {
                name: 'NamaAccount',
                label: 'Nama',
                width: 250,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __NamaAccount, 
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    // console.log(data);
						    let rowid = $(this).attr('rowid');
                            $('[id="'+rowid+'_'+'NoAccount'+'"]').val(data.NoAccount); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'NoAccount'+'"]').trigger('change');
						}
                    }                              
                }
            },
            {
                name: 'KeteranganGL',
                label: 'Keterangan',
                width: 380,
                editable: true,
            },
            {
                name: 'GLAutoN',
                label: 'GLAutoN',
                width: 380,
                editable: true,
                hidden: true
            },
            {
                name: 'DebetGL',
                label: 'Debet',
                width: 100,
                editable: true,
                template: 'money',
                editor: {
                    events:{
                        'change': function(e) { 
                            let kreditGL = $('[id="'+$(this).attr('rowid')+'_'+'KreditGL'+'"]'); 
                            if(parseFloat(window.util.number.getValueOfEuropeanNumber(this.value)) !== parseFloat(0)) kreditGL.val('0'); 
                        }
                    }                              
                }
            },
            {
                name: 'KreditGL',
                label: 'Kredit',
                width: 100,
                editable: true,
                template: 'money',
                editor: {
                    events:{
                        'change': function(e) { 
                            let debetGL = $('[id="'+$(this).attr('rowid')+'_'+'DebetGL'+'"]'); 
                            if(parseFloat(window.util.number.getValueOfEuropeanNumber(this.value)) !== parseFloat(0)) debetGL.val('0'); 
                        }
                    }                              
                }
            },
        ],   
        listeners: {
            afterLoad: function(grid, response) {
                let TotalDebetGL = $('#detailGrid').jqGrid('getCol','DebetGL',false,'sum');
                let TotalKreditGL = $('#detailGrid').jqGrid('getCol','KreditGL',false,'sum');
                $('#TotalDebetGL').utilNumberControl().val(TotalDebetGL);  
                $('#TotalKreditGL').utilNumberControl().val(TotalKreditGL);  
                $('#Selisih').utilNumberControl().val(Math.abs(TotalDebetGL.toFixed(2) -TotalKreditGL.toFixed(2))); 
            }
        }     
     }).init().fit($('.box-body')).load({url: '$urlDetail'});
     
      $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowId, orgClickEvent) {
        // console.log(orgClickEvent);
        if (orgClickEvent.extraparam.oper === 'add'){
            var KeteranganGL = $('[id="'+rowId+'_'+'KeteranganGL'+'"]');
            KeteranganGL.val($('[name="MemoGL"]').val());
        }        
    });
        
      function showDialog(a){
          if (a === undefined) return;
          var dataHutangPiutang = [];
          var colModelPiutang = [
                  { name: 'GLLink',key:true, index: 'GLLink', width: 120, label: 'No Trans',editable: true,editoptions:{readOnly:true,},   },
                  { name: 'TglGL', index: 'TglGL', width: 70, label: 'Tgl Jurnal',editable: true,editoptions:{readOnly:true,}, },
                  { name: 'NoAccount', index: 'NoAccount', width: 90, label: 'No Account',editable: true, hidden:true},
                  { name: 'NamaAccount', index: 'NamaAccount', width: 70, label: 'Nama Account',editable: true,hidden:true},
                  { name: 'MemoGL', index: 'MemoGL', width: 280, label: 'Memo',editable: true,editoptions:{readOnly:true,}, },
                  { name: 'DebetGL', index: 'DebetGL', width: 90, label: 'Total',editable: true,editoptions:{readOnly:true,}, 'formatter':'number','align':'right'},
                  { name: 'PiutangLunas', index: 'PiutangLunas', width: 90, label: 'PiutangLunas',editable: true,editoptions:{readOnly:true,}, 'formatter':'number','align':'right'},
                  { name: 'Bayar', index: 'Bayar', width: 90, label: 'Pelunasan',editable: true, 'formatter':'number','align':'right',  edittype: 'number', editoptions: { 
                      dataInit: function (elem) {
                       setTimeout(function(){
                           $(elem).inputmask({alias:"numeric",digits:0,groupSeparator:".",radixPoint:",",autoGroup:true,autoUnmask:true});
                       }, 100);
                                   }
                    }},
                  { name: 'PiutangSisa', index: 'PiutangSisa', width: 90, label: 'PiutangSisa',editable: true,editoptions:{readOnly:true,},'formatter':'number','align':'right'},
                  { name: 'NoGL', index: 'NoGL', width: 70, label: 'No Jurnal',editable: true,editoptions:{readOnly:true,},},
                ];
          var colModelHutang = [
                  { name: 'GLLink',key:true, index: 'GLLink', width: 120, label: 'No Trans',editable: true,editoptions:{readOnly:true,}, },
                  { name: 'TglGL', index: 'TglGL', width: 70, label: 'Tgl Jurnal',editable: true,editoptions:{readOnly:true,}, },
                  { name: 'NoAccount', index: 'NoAccount', width: 90, label: 'No Account',editable: true, hidden:true},
                  { name: 'NamaAccount', index: 'NamaAccount', width: 70, label: 'Nama Account',editable: true, hidden:true},
                  { name: 'MemoGL', index: 'MemoGL', width: 280, label: 'Memo',editable: true,editoptions:{readOnly:true,}, },
                  { name: 'KreditGL', index: 'KreditGL', width: 90, label: 'Total',editable: true,editoptions:{readOnly:true,}, 'formatter':'number','align':'right'},
                  { name: 'HutangLunas', index: 'HutangLunas', width: 90, label: 'HutangLunas',editable: true,editoptions:{readOnly:true,}, 'formatter':'number','align':'right'},
                  { name: 'Bayar', index: 'Bayar', width: 90, label: 'Pelunasan',editable: true, 'formatter':'number','align':'right',  edittype: 'number', editoptions: { 
                      dataInit: function (elem) {
                       setTimeout(function(){
                           $(elem).inputmask({alias:"numeric",digits:0,groupSeparator:".",radixPoint:",",autoGroup:true,autoUnmask:true});
                       }, 100);
                                   }
                    }},
                  { name: 'HutangSisa', index: 'HutangSisa', width: 90, label: 'HutangSisa',editable: true,editoptions:{readOnly:true,}, 'formatter':'number','align':'right'},
                  { name: 'NoGL', index: 'NoGL', width: 70, label: 'No Jurnal',editable: true,editoptions:{readOnly:true,},},
                ];
          function HitungBawah(){
              var grid = $("#detailGridHp");              
              var i, selRowIds = grid.jqGrid("getGridParam", "selarrrow"), n, rowData,sum;
              window.BHC_selectRow =[];
                sum = 0;
                for (i = 0, n = selRowIds.length; i < n; i++) {
                    rowData = grid.getRowData(selRowIds[i]);
                    window.BHC_selectRow.push({
                        NoGL: $('input[name="NoGL"]').val(),
                        TglGL: $('input[name="TglGL"]').val(),
                        GLLink: rowData.GLLink,
                        NoAccount: rowData.NoAccount,
                        HPJenis: a[0],
                        HPNilai: rowData.Bayar
                    });          
                    sum += parseFloat(rowData.Bayar);
                }
                 $('#totpelunasan').inputmask('setvalue',sum);
                window.BHC_totpelunasan = sum;
          }
          window.HitungBawah = HitungBawah;
          this.dialog =  bootbox.dialog({
                    title: 'Daftar '+a[0],
                    message: __msgHutangPiutang + '<table id="detailGridHp"></table><div id="detailGridPagerHp"></div>',
                    size: 'large',
                    onEscape: false,
                    backdrop: true,
                    onHidden: function(e) {
                         var x = window.BHC_itemApar.pop();  
                         if (x === undefined){
                             window.location.href = "{$url['update']}&oper=skip-load&NoGLView={$dsJurnal['NoGLView']}"; 
                         }else{                             
                            showDialog(x);
                         }
                    },
                    buttons: {
                        ok: {
                            label: '<i class="glyphicon glyphicon-ok"></i>&nbsp&nbsp Ok &nbsp&nbsp&nbsp&nbsp&nbsp',
                            className: 'btn-success',
                            callback: function(){
                                if (window.BHC_maxpelunasan.toFixed(2) !== window.BHC_totpelunasan.toFixed(2)){
                                    bootbox.alert({message:'Jumlah Max Pelunasan tidak sama dengan Total Pelunasan',size:'small'});
                                    return false;
                                }
                                $.ajax({
                                     type: "POST",
                                     url: '$urlPiutangHutangAjax',
                                     data: {
                                         oper: 'add',
                                          data: window.BHC_selectRow,
                                          header: btoa($('#form_ttgeneralledgerhd_id').serialize())
                                     },
                                     success: function(response) {
                                         console.log(response);
                                     }
                                });
                            }
                        },
                        cancel: {
                            label: '<i class="fa fa-ban"></i>&nbsp&nbsp Batal &nbsp',
                            className: 'btn-danger',
                            callback: function(){
                                // console.log('Cancel');
                            }
                        }
                    }
                });
                this.dialog.find('.modal-dialog').width(1050); 
                
                this.dialog.on("shown.bs.modal", function() {
                    $('#filter-tgl-val1').remove();
                    $('#filter-tgl-val1-disp').attr('name','tgl1').kvDatepicker({format:'dd/mm/yyyy',autoclose: true});
                    $('#filter-tgl-val2').remove();
                    $('#filter-tgl-val2-disp').attr('name','tgl2').kvDatepicker({format:'dd/mm/yyyy',autoclose: true});
                    var tgl1 = $('[name=tgl1]');
                    var tgl2 = $('[name=tgl2]');
                    tgl1.change(function(){
                        $('#filter-bottom-form').submit();
                    })
                    tgl2.change(function(){
                        $('#filter-bottom-form').submit();
                    })
                    $("#checked-filter").change(function() {
                        if(this.checked){
                            $("#span-checked-filter").text("Filter #2 Aktif");    
                            $(".inline-filter2").show();
                        }else{
                            $("#span-checked-filter").text("Filter #2 Non Aktif");
                            $(".inline-filter2").hide();
                        }
                    });    
                    if (/Mobi/i.test(navigator.userAgent) || /Android/i.test(navigator.userAgent)) {
                        $("#checked-filter").prop("checked", false);  
                        $("#span-checked-filter").text("Filter #2 Non Aktif");
                        $(".inline-filter2").hide();
                    }
                    var option = $('.cmbTxt');
                    option.find('option').remove().end();
                    for (let val in setcmb['cmbTxt']) {
                        option.append('<option value="' + val + '">' + setcmb['cmbTxt'][val] + '</option>');
                    }
                    $('#filter-text-cmb2').prop("selectedIndex", 1);
                    if(Object.keys(setcmb['cmbTgl']).length > 0){
                        var option = $('#filter-tgl-cmb1');
                        option.find('option').remove().end();
                        for (let val in setcmb['cmbTgl']) {
                            option.append('<option value="' + val + '">' + setcmb['cmbTgl'][val] + '</option>');
                        }
                    }
                    if(Object.keys(setcmb['cmbNum']).length > 0){
                        var option = $('#filter-num-cmb1');
                        option.find('option').remove().end();
                        for (let val in setcmb['cmbNum']) {
                            option.append('<option value="' + val + '">' + setcmb['cmbNum'][val] + '</option>');
                        }
                    }
                    
                    function submitFilter(){
                        var url_string = $(location).attr('href');
                        var url = new URL(url_string);
                        var c = 'ttgeneralledgerhd/select-piutang-hutang';
                        var pageKey = c;
                        var _searchCookie = Lockr.get('F.'+pageKey)||1;
                        var tgl1arr = $('[name=tgl1]').val().split('/');
                        var tgl2arr = $('[name=tgl2]').val().split('/');
                        const search={
                            cmbTxt1: $('#filter-text-cmb1').val(),
                            txt1 : $('#filter-text-val1').val(),
                            cmbTxt2 : $('#filter-text-cmb2').val(),
                            txt2 : $('#filter-text-val2').val(),
                            check : $('#checked-filter').is(":checked") ? 'on' : 'off',
                            cmbTgl1 : $('#filter-tgl-cmb1').val(),
                            tgl1 : tgl1arr[2]+'-'+tgl1arr[1]+'-'+tgl1arr[0],
                            tgl2 : tgl2arr[2]+'-'+tgl2arr[1]+'-'+tgl2arr[0],
                            cmbNum1 : $('#filter-num-cmb1').val(),
                            cmbNum2 : $('#filter-num-cmb2').val(),
                            num1 : $('#filter-num-val').val(),
                            r:'ttgeneralledgerhd/select-piutang-hutang',
                            urlParams:{
                                r:'ttgeneralledgerhd/select-piutang-hutang',
                                JenisHP: a[0],
                                MyNoAccountHP:a[1]                            
                            }
                        };
                        if(_searchCookie !== 1){
                            if(_.isEqual(_searchCookie, search)){
                                return;
                            }
                        }
                        var query = JSON.stringify(search);
                         // $('#jqGrid')
                         // .jqGrid('setGridParam',{
                         //     page: 1,
                         //     sortname: setcmb.sortname,
                         //     sortorder: setcmb.sortorder,
                         //     postData: {
                         //         query: query
                         //     }
                         // }).trigger("reloadGrid");
                         var grid = $("#detailGridHp");
                         $("body").addClass("loading");
                         $.ajax({
                             type: "POST",
                             url: '$urlPiutangHutang',
                             data: {
                                 query: query,
                                 _search:true,
                                    nd:"1601533557381",
                                    rows:"10000",
                                    page:"1",
                                    sidx:"TglGL",
                                    sord:"asc"
                             },
                             success: function(response) {
                                 dataHutangPiutang = response.rows; 
                                 grid.jqGrid("clearGridData").jqGrid('setGridParam',{ 
                                     datatype: 'local', 
                                     rowNum: 1000,
                                     data: dataHutangPiutang }).trigger('reloadGrid');
                                 $("body").removeClass("loading");
                             },
                         });
                         $('#gbox_grid .s-ico').css('display','none');
                         $('#gbox_grid #jqgh_grid_id .s-ico').css('display','');
                         $('#gbox_grid #jqgh_grid_id .s-ico .ui-icon-triangle-1-s').removeClass('ui-state-disabled');
                    }
                    window.submitFilter = submitFilter;
                    function findString (back) {
                        var str = $('#txt-find-id').val();
                         if (parseInt(navigator.appVersion)<4) return;
                         var strFound;
                         if (window.find) {
                        
                          // CODE FOR BROWSERS THAT SUPPORT window.find
                        
                          strFound=self.find(str,0,back);
                          // if (!strFound) {
                          //  strFound=self.find(str,0,1);
                          //  while (self.find(str,0,1)) continue;
                          // }
                         }
                         else if (navigator.appName.indexOf("Microsoft")!=-1) {
                        
                          // EXPLORER-SPECIFIC CODE
                        
                          if (TRange!=null) {
                           TRange.collapse(false);
                           strFound=TRange.findText(str);
                           if (strFound) TRange.select();
                          }
                          if (TRange==null || strFound==0) {
                           TRange=self.document.body.createTextRange();
                           strFound=TRange.findText(str);
                           if (strFound) TRange.select();
                          }
                         }
                         else if (navigator.appName=="Opera") {
                          alert ("Opera browsers not supported, sorry...")
                          return;
                         }
                         if (!strFound) alert ("String '"+str+"' not found!")
                         return;
                    }
                    
                    $('#filter-bottom-form').on('submit', function(e){
                        e.preventDefault();
                        submitFilter();
                    });
                    $('#btn-find-id').on('click', function(e){
                        e.preventDefault();
                        $('#findModal').modal();
                    });
                    $('#find-up').on('click', function(e){
                        e.preventDefault();
                        findString(1);
                    });
                    $('#find-down').on('click', function(e){
                        e.preventDefault();
                        findString(0);
                    });
                    
                    $('.blur').on('blur', function(e){
                        submitFilter();
                    });
                    
                    $('.chg').on('change', function(e){
                        setTimeout(submitFilter(),500);
                    });
                    $('.blur').keydown(function (e){
                        if(e.keyCode == 13){
                            submitFilter();
                        }
                    });                    
                    $("#checked-filter").trigger('change');
                });
                 var formset = "<div class='form-group col-md-20'>" +
                                "<div class='col-md-2'><label>Account</label></div>"+    
                                "<div class='col-md-8'><input id='NoAccountPopup' type='text' class='form-control' readOnly=true></div>"+
                                "<div class='col-md-3'><label>Max Pelunasan</label></div>"+            
                                "<div class='col-md-4'><input id='maxpelunasan' type='text' class='form-control'readOnly=true ></div>"+
                                "<div class='col-md-3'><label>Tot Pelunasan</label></div>"+            
                                "<div class='col-md-4'><input id='totpelunasan' type='text' class='form-control' readOnly=true></div>"+        
                               "</div>";
                this.dialog.find('.modal-footer').prepend(formset);    
                $('#maxpelunasan,#totpelunasan').inputmask({alias:"numeric",digits:2,groupSeparator:".",radixPoint:",",autoGroup:true,autoUnmask:true});
                $('#NoAccountPopup').val(a[1]+' - '+a[2]);
                $('#maxpelunasan').inputmask('setvalue',a[3]);
                window.BHC_maxpelunasan = a[3];
                $('#totpelunasan').inputmask('setvalue',0);
                var grid = $("#detailGridHp"); 
                grid.jqGrid({
                datatype: 'local',
                // editurl:'clientArray' ,
                rowNum: 1000,  
                cellEdit : true,                
                loadonce:true,
                cellsubmit:'clientArray',
                data: dataHutangPiutang,
                rownumbers: true,
                colModel: (a[0] === 'Piutang') ? colModelPiutang : colModelHutang,
                multiSort: true,
                search: false,
                altclass: 'row-zebra',
                altRows: true,
                multiselect:true,  
                // pager: "#detailGridPagerHp",
                // viewrecords: true,
                //rowList: [1000],       //
                pgbuttons: false,
                pginput: false,
                pgtext: "",
                width: 1000,
                height: 320,
                autowidth:false, 
                shrinkToFit:false,
                styleUI : 'Bootstrap',
                afterSaveCell: function (id,name,val,iRow,iCol){
                   HitungBawah();
                },
                beforeSaveCell: function (rowid, cellname, value, iRow, iCol){
                   // console.log(rowid+' '+cellname+' '+value+' '+iRow+' '+iCol);
                   let newvalue = parseFloat($("#"+iRow+"_"+cellname).val());
                   var col1 = a[0] === 'Piutang' ? 'DebetGL' : 'KreditGL' ;
                   var col2 = a[0] === 'Piutang' ? 'PiutangSisa' : 'HutangSisa' ;
                   let awal = parseFloat(grid.jqGrid("getCell", rowid, col1));
                   if(newvalue > awal){
                       newvalue = awal;
                   }
                   let sisa = awal - newvalue; 
                   grid.jqGrid("setCell", rowid, col2, sisa);
                   return  newvalue;
                },
                onSelectRow: function(){
                    HitungBawah();
                },
                }).navGrid('#detailGridPagerHp',{
                    edit:false,add:false,del:false,search:false,refresh:false
                }).bindKeys();   
                        
      }
      
      function getAccountHP(){
          window.BHC_itemApar = [];
           var allRows= $('#detailGrid').getRowData();
          allRows.forEach( a => {
              if(a.NoAccount !== undefined){ 
                  var accPiu = a.NoAccount.substr(0, 4);
                  var accHut = a.NoAccount.substr(0, 2);
                  var DebetGL = parseFloat(a.DebetGL);
                  var KreditGL = parseFloat(a.KreditGL);
                  if ((accPiu === '1106' || accPiu === '1107'|| accPiu === '1108'|| accPiu === '1109'|| 
                  accPiu === '1110'|| accPiu === '1111'|| accPiu === '1112'|| accPiu === '1113'|| accPiu === '1114') &&
                  KreditGL > 0){                  
                    window.BHC_itemApar.push(['Piutang',a.NoAccount,a.NamaAccount,KreditGL]);
                  }
                  if ((accHut === '24' || accHut === '25') && DebetGL > 0){
                    window.BHC_itemApar.push(['Hutang',a.NoAccount,a.NamaAccount,DebetGL]);
                  }
              }
            });
      }
      
      $('#SearchHtg').click(function() {
          if($('input[name="GLValid"]').val() === 'Sudah'){
              bootbox.alert({message:'Jurnal sudah tervalidasi',size: 'small'});
              return;
          }
          bootbox.confirm({message: "Apakah anda akan melakukan proses link hutang piutang?", size: 'small', callback: function(result){ 
            if(result){
                $( document).unbind( "ajaxStart ajaxStop" );
                $("body").addClass("loading");
                $('input[name="HPLink"]').val('--');
                $.ajax({
                     type: "POST",
                     url: '$urlPiutangHutangAjax',
                     data: {
                         oper: 'del',
                         NoGL: $('input[name="NoGL"]').val(),
                         header: btoa($('#form_ttgeneralledgerhd_id').serialize())
                     },
                     success: function(response) {
                         window.BHC_preselect = response.msg;   
                         getAccountHP();
                         $("body").removeClass("loading");
                          if (window.BHC_itemApar.length === 0){
                              bootbox.alert({message:"Tidak ada account Hutang '24'-'25' Debet - Piutang '1106'-'1114' Kredit",size:'small'});
                              return false;
                          }          
                          var x = window.BHC_itemApar.pop();                          
                          showDialog(x);
                     },
                 });       
                      
            }
            }
        });         
      });
      
      $('#SearchTrans').click(function(){
           if($('input[name="GLValid"]').val() === 'Sudah'){
              bootbox.alert({message:'Jurnal sudah tervalidasi',size: 'small'});
              return;
          }
        window.util.app.dialogListData.show({
            title: 'Daftar Data Konsumen',
            url: '$urlSearchTrans'
        },function(){
            var iframe = window.util.app.dialogListData.iframe();
            var selRowId = iframe.contentWindow.util.app.dialogListData.gridFn.getSelectedRow(true);
            $.ajax({
                 type: "POST",
                 url: '$urlSearchTransAfter',
                 data: {
                     oper: 'batch',
                     data: selRowId,
                     jenis: iframe.contentWindow.jQuery("#filter-text-cmb2 :selected").val(),
                     header: btoa($('#form_ttgeneralledgerhd_id').serialize())
                 },
                 success: function(data) {                    
                    window.location.href = "{$url['update']}&oper=skip-load&BKNoView={$dsJurnal['NoGLView']}"; 
                 },
               }); 
        });
      });
    
    $('#BtnInfoLink').click(function (event) {	
        window.util.app.dialogListData.show({
            title: 'Daftar Transaksi Hutang - Piutang yang terlunasi oleh jurnal ini',
            url: '$urlHutangPiutang'
        },
        function (data) {
            console.log(data);
        })
    }); 
      
      $('#check').on('change', function(){
          if (parseInt(this.value) === 1){
              $('input[name="GLValid"]').val('Sudah');
          }else{
               $('input[name="GLValid"]').val('Belum');
          }
      })
     
    $('#btnPrint').click(function (event) {	     
        $('#form_ttgeneralledgerhd_id').attr('action','$urlPrint');
        $('#form_ttgeneralledgerhd_id').attr('target','_blank');
        $('#form_ttgeneralledgerhd_id').submit();
	  }); 
    
      
    $('#btnCancel').click(function (event) {	      
       $('#form_ttgeneralledgerhd_id').attr('action','{$url['cancel']}');
       $('#form_ttgeneralledgerhd_id').submit();
    });
    
    $('#btnAdd').click(function (event) {	      
        window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });     
    
    $('#btnSave').click(function (event) {
        getAccountHP();
        var HPLink = $('[name=HPLink]').val().trim();
        if ((HPLink === '--' || HPLink === '') && window.BHC_itemApar.length !== 0){
            var msg = "Silahkan melakukan link pelunasan hutang piutang berikut ini : \\n ";
             window.BHC_itemApar.forEach( a => {
                msg += "* "+a[0]+" - "+a[1]+" - "+a[2]+" ("+(a[0]==='Piutang'?'K':'D')+") -> "+parseFloat(a[3]).toLocaleString('id-ID')+" \\n ";
             });
              bootbox.dialog({message:"<pre>"+msg+"</pre>",size:'medium',closeButton: false,
              buttons: {                  
                    ok: {
                        label: "OK",
                        className: 'btn-info',
                        callback: function(){                           
                        }
                    },
                    cancel: {
                        label: "Ignore",
                        className: 'btn-info',
                        callback: function(){
                            let selisih = $('#Selisih').utilNumberControl().val();
                            if (selisih !== 0){
                                 bootbox.alert({ message: "Total Debet dan Total Kredit harus sama.", size: 'small'});
                                return;
                            }
                            $('#form_ttgeneralledgerhd_id').attr('action','{$url['update']}');
                            $('#form_ttgeneralledgerhd_id').submit();
                        }
                    }
                }
              });
          } else{
            let selisih = $('#Selisih').utilNumberControl().val();
            if (selisih !== 0){
                 bootbox.alert({ message: "Total Debet dan Total Kredit harus sama.", size: 'small'});
                return;
            }
            $('#form_ttgeneralledgerhd_id').attr('action','{$url['update']}');
            $('#form_ttgeneralledgerhd_id').submit();
          }
        
    }); 
    
    $('[name=TglGL]').utilDateControl().cmp().attr('tabindex', '2');
    $('[name=Selisih').utilNumberControl().cmp().attr('tabindex', '7');
    $('[name=TotalDebetGL').utilNumberControl().cmp().attr('tabindex', '8');
    $('[name=TotalKreditGL').utilNumberControl().cmp().attr('tabindex', '9');
    $('#BtnInfoLink').attr('disabled',false);
JS
);
