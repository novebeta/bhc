<?php
use abengkel\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
AppAsset::register( $this );
$this->title                   = 'Jurnal Adjustment';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
    'cmbTxt'    => [
        'NoGL'    => 'No. Jurnal',
        'MemoGL'  => 'Memo Jurnal',
        'GLLink'  => 'Link Transaksi',
        'HPLink'  => 'Link Hutang-Piutang',
        'NoAccount'  => 'No. Perkiraan',
        'NamaAccount'  => 'Nama Perkiraan',
        'KeteranganGL'  => 'Keterangan',
//		'GLValid' => 'Validasi',
    ],
    'cmbTgl'    => [
        'TglGL' => 'Tgl Jurnal',
    ],
    'cmbNum'    => [
        'TotalDebetGL'  => 'Debet',
        'TotalKreditGL' => 'Kredit',
    ],
    'sortname'  => "TglGL",
    'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Ttgeneralledgerhd::className(), 'Jurnal Adjustment', [
    'url_add'       => Url::toRoute( [ 'ttgeneralledgerhd/adjustment-create' , 'action' => 'create' ] ),
    'url_update'    => Url::toRoute( [ 'ttgeneralledgerhd/adjustment-update'  ,'action'=>'update']),
    'url_delete'    => Url::toRoute( [ 'ttgeneralledgerhd/delete','type' => '' ] ),
] ), \yii\web\View::POS_READY );
