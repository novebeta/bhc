<?php
use kartik\datecontrol\DateControl;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttgeneralledgerhd */
/* @var $form yii\widgets\ActiveForm */

\abengkel\assets\AppAsset::register($this);
?>

    <div class="ttgeneralledgerhd-form">
        <?php
        $GLValid_option = [
            'onclick' => "$(this).parent().prev().val(this.checked? 'Sudah' : 'Belum')",
            'uncheck' => 'Belum',
            'checked' => 'Sudah'
        ];
        $model->GLValid = true;

        $form = ActiveForm::begin(['id'=>'frm_ttgeneralledgerhd_id','action' => $url['update']]);
        \abengkel\components\TUi::form(
            [
                'class' => "row-no-gutters",
                'items' => [
                    ['class' => "form-inline col-md-14",
                        'items' => [
                            [ 'style' => "margin-bottom: 3px;",'items' => ":NoGL :TglGL"],
                            [ 'style' => "margin-bottom: 3px;",'items' => ":MemoGL"]
                        ]
                    ],
                    ['class' => "form-inline col-md-10",
                        'items' => [
                            [ 'style' => "margin-bottom: 3px;",'items' => ":GLValid"],
                            [ 'style' => "margin-bottom: 3px;",'items' => ":GLLink :btn_GLLink"],
                            [ 'style' => "margin-bottom: 3px;",'items' => ":HutangPiutang"]
                        ]
                    ],
                    ['class' => "col-md-24", 'style' => "margin-bottom: 3px;", 'items' => '<table id="detailGrid"></table>' ],
                    ['class' => "form-inline pull-right", 'items' => ":Selisih :TotalDebetGL :TotalKreditGL" ]
                ]
            ],
            ['class' => "pull-right", 'items' => ":btnSave :btnPrint :btnCancel"],
            [
                ":NoGL"         => $form->field($model, 'NoGL', ['template' => '{label}{input}'])
                                        ->textInput(['maxlength' => true, 'placeholder' => 'No Jurnal', 'readonly'=>''])
                                        ->label('No Jurnal', ['style' => 'margin: 0; padding: 6px 0; width: 60px;']),
                ":TglGL"        => $form->field($model, 'TglGL', ['template' => '{label}{input}'])
                                        ->widget(DateControl::className(), [
                                            'type'          => DateControl::FORMAT_DATE,
                                            'pluginOptions' => [ 'autoclose' => true ]
                                        ])->label('Tanggal Jurnal', ['style' => 'margin: 0; padding: 6px 0; width: 85px;']),
                ":MemoGL"       => $form->field($model, 'MemoGL', ['template' => '{label}{input}'])
                                        ->textarea(['maxlength' => true, 'placeholder' => 'Memo Jurnal', 'cols' => 80, 'rows' => 3])
                                        ->label('Memo', ['style' => 'margin: 0; padding: 6px 0; width: 60px;']),
                ":GLValid"      => $form->field($model, 'GLValid', ['template' => '{label}<div class="input-group"><input type="text" class="form-control" value="'.($model->GLValid).'" readonly><span class="input-group-addon">{input}</span></div>'])
                                        ->checkbox($GLValid_option, false)
                                        ->label('Validasi Jurnal', ['style' => 'margin: 0; padding: 6px 0; width: 100px;']),
                ":GLLink"       => $form->field($model, 'GLLink', ['template' => '{label}{input}'])
                                        ->textInput(['maxlength' => true, 'readonly'=>''])
                                        ->label('Link Transaksi', ['style' => 'margin: 0; padding: 6px 0; width: 100px;']),
                ":HutangPiutang" => '<div class="form-group">
                                        <label class="control-label" style="margin: 0; padding: 6px 0; width: 100px;">Hutang - Piutang</label>
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-info-sign"></i></button>
                                            </span>
                                            <input type="text" class="form-control" placeholder="...">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i></button>
                                                <button class="btn btn-default" type="button" id="btnTesPopUpForm"><i class="glyphicon glyphicon-search"></i></button>
                                            </span>
                                        </div>
                                        <div class="help-block"></div>
                                    </div>',
                ":Selisih"        => '<div class="form-group">
                                        <label class="control-label" for="selisih" style="margin: 0; padding: 6px 0; width: 40px;">Selisih</label>'.
                                        \kartik\number\NumberControl::widget([
                                            'id' => 'selisih',
                                            'name' => 'selisih',
                                            'value' => 0,
                                            'readonly' => true,
                                            'maskedInputOptions' => [
                                                'groupSeparator' => '.',
                                                'radixPoint' => ','
                                            ]
                                        ])
                                        .'<div class="help-block"></div>
                                      </div>',
                ":TotalDebetGL"   => $form->field($model, 'TotalDebetGL')
                                    ->widget(\kartik\number\NumberControl::className(), [
                                        'value' => 0,
                                        'readonly' => true,
                                        'maskedInputOptions' => [
                                            'groupSeparator' => '.',
                                            'radixPoint' => ','
                                        ],
                                        'displayOptions' => ['class' => 'form-control']
                                    ])->label('Total', ['style' => 'margin: 0; padding: 6px 0 6px 2px; width: 30px;']),
                ":TotalKreditGL" => $form->field($model, 'TotalKreditGL')
                                    ->widget(\kartik\number\NumberControl::className(), [
                                        'value' => 0,
                                        'readonly' => true,
                                        'maskedInputOptions' => [
                                            'groupSeparator' => '.',
                                            'radixPoint' => ','
                                        ],
                                        'displayOptions' => ['class' => 'form-control']
                                    ])->label(false),
                ":btn_GLLink"   => Html::button( '<i class="glyphicon glyphicon glyphicon-search"></i>', ['class' => 'btn btn-default ', 'id' => 'btn_GLLink']),
                ":btnSave"      => Html::button( '<i class="glyphicon glyphicon-floppy-disk"></i> Simpan', ['class' => 'btn btn-info btn-primary ', 'id' => 'btnSave']),
                ":btnPrint"     => '<a href="'.$url['print'].'"class="btn btn-danger btn-warning" role="button">&nbsp<i class="glyphicon glyphicon-print"></i>&nbsp&nbsp Print &nbsp</a>',
                ":btnCancel"    => Html::Button( '<i class="fa fa-ban"></i>&nbspBatal&nbsp&nbsp', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel' ] ),

            ]
        );
        ActiveForm::end();
        ?>
    </div>

<?php
$dataCOA        = json_encode(\abengkel\models\Traccount::find()->comboSelect2());
$urlValidate    = $url['validation'];
$urlDetail      = $url['detail'];
$readOnly       = ( $_GET['action'] == 'view' ? 'true' : 'false' );
$DtAccount      = \common\components\General::cCmd(
                "SELECT NoAccount, NamaAccount, OpeningBalance, JenisAccount, StatusAccount, NoParent FROM traccount 
                     WHERE JenisAccount = 'Detail' ORDER BY NoAccount" )->queryAll();
$this->registerJsVar( '__dtAccount', json_encode( $DtAccount ) );
$this->registerJs(<<< JS
    var kodeTrans = '$kodeTrans',
        btn_GLLink = $('#btn_GLLink');

    var __NoAccount = JSON.parse(__dtAccount);
     __NoAccount = $.map(__NoAccount, function (obj) {
                      obj.id = obj.NoAccount;
                      obj.text = obj.NoAccount;
                      return obj;
                    });
     var __NamaAccount = JSON.parse(__dtAccount);
     __NamaAccount = $.map(__NamaAccount, function (obj) {
                      obj.id = obj.NamaAccount;
                      obj.text = obj.NamaAccount;
                      return obj;
                    });

    $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 260,
        extraParams: {
            NoGL: '$model->NoGL',
            TglGL: '$model->TglGL'
        },
        rownumbers: true,
        rowNum: 1000,
        readOnly: $readOnly,
        colModel: [
            { template: 'actions'  },
            {
                name: 'NoAccount',
                label: 'Nomor',
                editable: true,
                editor: {
                    type: 'select2',
                    data: $dataCOA,
                    valueField: 'NoAccount',
                    displayField: 'NoAccount',
                    templateResult: function(data) {
                        return $('<span style="display: inline-block; width: 70px;">'+data.NoAccount+'</span><span style="display: inline-block;">'+data.NamaAccount+'</span>');
                    },
                    searchFields: [ 'NoAccount', 'NamaAccount' ],
                    onchange: function(e) {
                        let namaAccount = $('[id="'+$(this).attr('rowid')+'_'+'NamaAccount'+'"]'),
                            data = $(this).utilSelect2().getSelectedData();
                        namaAccount.val(data[0].NamaAccount);
                    }
                }
            },
            {
                name: 'NamaAccount',
                label: 'Nama Perkiraan',
                width: 200,
                editable: true,
                editor: {
                    type: 'text',
                    readOnly: true
                }
            },
            {
                name: 'KeteranganGL',
                label: 'Keterangan',
                width: 282,
                editable: true
            },
            {
                name: 'DebetGL',
                label: 'Debet',
                width: 140,
                editable: true,
                template: 'money',
                editor: {
                    type: 'numbercontrol',
                    events:{
                        'change': function(e) { 
                            let kreditGL = $('[id="'+$(this).attr('rowid')+'_'+'KreditGL'+'"]'); 
                            if(parseFloat(window.util.number.getValueOfEuropeanNumber(this.value)) !== parseFloat(0)) kreditGL.val('0'); 
                        }
                    }     
                }
            },
            {
                name: 'KreditGL',
                label: 'Kredit',
                width: 140,
                editable: true,
                template: 'money',
                editor: {
                     type: 'numbercontrol',
                    events:{
                        'change': function(e) { 
                            let debetGL = $('[id="'+$(this).attr('rowid')+'_'+'DebetGL'+'"]'); 
                            if(parseFloat(window.util.number.getValueOfEuropeanNumber(this.value)) !== parseFloat(0)) debetGL.val('0'); 
                        }
                    }     
                }
            }
        ],
        listeners: {
            afterLoad: function(grid, response) {
                let TotalDebetGL = $('#detailGrid').jqGrid('getCol','DebetGL',false,'sum');
                let TotalKreditGL = $('#detailGrid').jqGrid('getCol','KreditGL',false,'sum');
                $('#ttgeneralledgerhd-totaldebetgl').utilNumberControl().val(TotalDebetGL);  
                $('#ttgeneralledgerhd-totalkreditgl').utilNumberControl().val(TotalKreditGL);  
                $('#Selisih').utilNumberControl().val(Math.abs(TotalDebetGL-TotalKreditGL));  
            },
        }
    }).init().fit($('.box-body')).load({url: '$urlDetail'});
    
    $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowId, orgClickEvent) {
        // console.log(orgClickEvent);
        if (orgClickEvent.extraparam.oper === 'add'){
            var KeteranganGL = $('[id="'+rowId+'_'+'KeteranganGL'+'"]');
            KeteranganGL.val($('[name="MemoGL"]').val());
        }        
    });
    
    $('#btnCancel').click(function (event) {	      
       $('#frm_ttgeneralledgerhd_id').attr('action','{$url['cancel']}');
       $('#frm_ttgeneralledgerhd_id').submit();
    });
    
    $('#btnSave').click(function (event) {	
        let selisih = $('#Selisih').utilNumberControl().val();
        if (selisih !== 0){
             bootbox.alert({ message: "Total Debet dan Total Kredit harus sama.", size: 'small'});
            return;
        }
        $('#frm_ttgeneralledgerhd_id').attr('action','{$url['update']}');
        $('#frm_ttgeneralledgerhd_id').submit();
    }); 
   
JS
);