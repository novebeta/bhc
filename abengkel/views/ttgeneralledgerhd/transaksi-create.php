<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttgeneralledgerhd */

$this->title = 'Tambah - Jurnal Transaksi';
$this->params['breadcrumbs'][] = ['label' => 'Jurnal Transaksi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$params = '&id='.$id.'&action=create';
?>
<div class="ttgeneralledgerhd-create">
    <?= $this->render('memorial-form', [
        'model'     => $model,
        'dsJurnal'  => $dsJurnal,
        'view'      => $view,
        'url' => [
            'update'    => Custom::url(\Yii::$app->controller->id.'/transaksi-update'.$params ),
            'print'     => Custom::url(\Yii::$app->controller->id.'/print'.$params ),
            'cancel'    => Custom::url(\Yii::$app->controller->id.'/transaksi-cancel'.$params ),
            'detail'    => Custom::url('ttgeneralledgerit/index'.$params )
        ]
    ]) ?>

</div>
