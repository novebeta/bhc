<?php
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttgeneralledgerhd */

$this->title = "Edit - $title: " . $model->NoGL;
$this->params['breadcrumbs'][] = ['label' => $title, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NoGL, 'url' => ['view', 'id' => $id]];
$this->params['breadcrumbs'][] = 'Edit';
$action = 'update';
?>
<div class="ttgeneralledgerhd-update">

    <?= $this->render('_form', [
        'model'     => $model,
        'kodeTrans' => $kodeTrans,
        'url'       => [
                    'update'        => Url::toRoute([ \Yii::$app->controller->id."/$actionPrefix-update", 'id' => $id, 'action' => $action] ),
                    'print'         => Url::toRoute([ \Yii::$app->controller->id."/$actionPrefix-print", 'id' => $id, 'action' => $action] ),
                    'cancel'        => Url::toRoute([ \Yii::$app->controller->id."/$actionPrefix-cancel", 'id' => $id, 'action' => $action] ),
                    'detail'        => Url::toRoute([ 'ttgeneralledgerit/index', 'id' => $id, 'action' => $action] ),
                    'validation'    => Url::toRoute([ \Yii::$app->controller->id.'/validate', 'id' => $id, 'action' => $action] ),
        ]
    ]) ?>

</div>
