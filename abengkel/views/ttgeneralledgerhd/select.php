<?php
use abengkel\assets\AppAsset;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = $title;
$this->params[ 'breadcrumbs' ][] = $this->title;
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
$hidden = ($arr['hideFilter'] ?? false);
?>
    <div id="searchPanel" class="panel panel-default">
        <div class="panel-body">
			<? echo $hidden ? '': $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs(
	\abengkel\models\Ttgeneralledgerhd::class,
	$title, $options ), \yii\web\View::POS_READY );