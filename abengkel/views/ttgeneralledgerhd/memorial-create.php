<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttgeneralledgerhd */

$params                          = '&id=' . $id . '&action=create';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = 'Tambah - Jurnal Memorial';
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Jurnal Memorial', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = $this->title;
$dsJurnal['GLLink'] = $dsJurnal[ 'NoGLView' ];
?>
<div class="ttgeneralledgerhd-create">
    <?= $this->render('memorial-form', [
        'model'     => $model,
        'dsJurnal'  => $dsJurnal,
        'view'      => $view,
        'id'      => $id,
        'url'       => [
                'update'    => Custom::url(\Yii::$app->controller->id.'/memorial-update'.$params ),
                'print'     => Custom::url(\Yii::$app->controller->id.'/memorial-print'.$params ),
                'cancel'    => Custom::url(\Yii::$app->controller->id.'/memorial-cancel'.$params ),
                'detail'    => Custom::url('ttgeneralledgerit/index'.$params )
        ]
    ]) ?>
</div>
