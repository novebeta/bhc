<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttgeneralledgerhd */

$this->title = str_replace('Menu','',\abengkel\components\TUi::$actionMode).' - Jurnal Transaksi: ' . $dsJurnal[ 'NoGLView' ];
$this->params['breadcrumbs'][] = ['label' => 'Jurnal Transaksi', 'url' => ['index']];
$this->params['breadcrumbs'][] = str_replace('Menu','',\abengkel\components\TUi::$actionMode);
$params = '&id='.$id.'&action=update';
?>
<div class="ttgeneralledgerhd-update">
    <?= $this->render('memorial-form', [
        'model'     => $model,
        'dsJurnal'  => $dsJurnal,
        'view'      => $view,
        'id'      => $id,
        'url'       => [
            'update'    => Custom::url(Yii::$app->controller->id.'/transaksi-update'.$params ),
            'print'     => Custom::url(Yii::$app->controller->id.'/print'.$params ),
            'cancel'    => Custom::url(Yii::$app->controller->id.'/transaksi-cancel'.$params ),
            'detail'    => Custom::url('ttgeneralledgerit/index'.$params )
        ]
    ]) ?>

</div>
