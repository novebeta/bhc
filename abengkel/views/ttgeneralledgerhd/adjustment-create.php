<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttgeneralledgerhd */

$this->title = 'Tambah - Jurnal Adjustment';
$this->params['breadcrumbs'][] = ['label' => 'Jurnal Adjustment', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$params = '&id='.$id.'&action=create';
?>
<div class="ttgeneralledgerhd-create">

    <?= $this->render('memorial-form', [
        'model'     => $model,
        'dsJurnal'  => $dsJurnal,
        'view'      =>$view,
        'url'       => [
            'update'    => Custom::url(\Yii::$app->controller->id.'/adjustment-update'.$params ),
            'print'     => Custom::url(\Yii::$app->controller->id.'/print'.$params ),
            'cancel'    => Custom::url(\Yii::$app->controller->id.'/adjustment-cancel'.$params ),
            'detail'    => Custom::url('ttgeneralledgerit/index'.$params )
        ]
    ]) ?>

</div>
