<?php
use abengkel\assets\AppAsset;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Type Motor';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'MotorType'     => 'Motor Type',
		'MotorNama'     => 'Motor Nama',
		'MotorKategori' => 'Motor Kategori',
		'MotorCC'       => 'Motor Cc',
		'MotorStatus'   => 'Motor Status',
		'MotorNoMesin'  => 'Motor No Mesin',
		'MotorNoRangka' => 'Motor No Rangka',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
	'sortname'  => "MotorStatus",
	'sortorder' => 'asc'
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Tdmotortype::className(),'Type Motor' ), \yii\web\View::POS_READY );
