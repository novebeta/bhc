<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdmotortype */

$this->title = 'Edit Type Motor : ' . $model->MotorType;
$this->params['breadcrumbs'][] = ['label' => 'Type Motor', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tdmotortype-update">
    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdmotortype/update', 'id' => $id ,'action' => 'update'] ),
            'cancel' => Url::toRoute( [ 'tdmotortype/cancel', 'id' => $id,'action' => 'update' ] )
        ]
    ]) ?>

</div>
