<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdmotortype */

$this->title = 'Tambah Type Motor';
$this->params['breadcrumbs'][] = ['label' => 'Type Motor', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdmotortype-create">
    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdmotortype/update', 'id' => $id,'action' => 'create' ] ),
            'cancel' => Url::toRoute( [ 'tdmotortype/cancel', 'id' => $id,'action' => 'create' ] )
        ]
    ]) ?>

</div>
