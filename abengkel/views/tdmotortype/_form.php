<?php
use abengkel\components\FormField;
use common\components\Custom;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdmotortype */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="tdmotortype-form">
    <?php
    $form = ActiveForm::begin(['id' => 'frm_tdmotortype_id'  ,'enableClientValidation'=>false]);
    \abengkel\components\TUi::form(
        [
            'class' => "row-no-gutters",
            'items' => [
                ['class' => "form-group col-md-24", 'style' => "",
                    'items' => [
                        ['class' => "col-sm-2", 'items' => ":LabelMotorType"],
                        ['class' => "col-sm-6", 'items' => ":MotorType"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' =>":LabelMotorNama"],
                        ['class' => "col-sm-13",'items' => ":MotorNama"],
                    ],
                ],
                ['class' => "form-group col-md-24",'style' => "",
                    'items' => [
                        ['class' => "col-sm-2", 'items' => ":LabelMotorKategori"],
                        ['class' => "col-sm-6",'items' => ":MotorKategori"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelMotorCC"],
                        ['class' => "col-sm-4",'items' => ":MotorCC"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelTypeStatus"],
                        ['class' => "col-sm-6",'items' => ":TypeStatus"],
                    ],
                ],
            ]
        ],
        [
            'class' => "pull-right",
            'items' => ":btnAction"
        ],

        [
            /* label */
            ":LabelMotorType"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Type</label>',
            ":LabelMotorKategori"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kategori</label>',
            ":LabelMotorNama"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Motor</label>',
            ":LabelMotorCC"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Motor CC</label>',
            ":LabelTypeStatus"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',

            /* component */
            ":MotorType"        => $form->field($model, 'MotorType', ['template' => '{input}'])->textInput(['maxlength' => '50','autofocus' => 'autofocus']),
            ":MotorNama"        => $form->field($model, 'MotorNama', ['template' => '{input}'])->textInput(['maxlength' => '100']),
            ":MotorCC"          => FormField::combo( 'MotorCC',[  'extraOptions' => ['simpleCombo' => true]] ),
            ":TypeStatus"       => $form->field($model, 'MotorStatus', ['template' => '{input}'])->dropDownList(['A' => 'AKTIF', 'N' => 'NON AKTIF'], ['maxlength' => true,]),
            ":MotorKategori"    => $form->field($model, 'MotorKategori', ['template' => '{input}'])
                                    ->dropDownList([
                                        'Cub Low' => 'Cub Low',
                                        'Cub Mid' => 'Cub Mid',
                                        'Cub High' => 'Cub High',
                                        'AT Low' => 'AT Low',
                                        'AT Mid' => 'AT Mid',
                                        'AT High' => 'AT High',
                                        'Sport Low' => 'Sport Low',
                                        'Sport Mid' => 'Sport Mid',
                                        'Sport High' => 'Sport High',
                                    ],['maxlength' => true]),
        ],
	    [
		    'url_main'   => 'tdmotortype',
		    'url_id'     => $id,
		    'url_update' => $url[ 'update' ]
	    ]
    );
    ActiveForm::end();
    ?>
</div>
<?php
$urlIndex   = Url::toRoute( [ 'tdmotortype/index' ] );
$urlAdd     = Url::toRoute( [ 'tdmotortype/create','id'=>'new' ] );
$this->registerJs( <<< JS
     $('#btnSave').click(function (event) {
       $('#frm_tdmotortype_id').submit();
    }); 
    $('#btnCancel').click(function (event) {	  
         window.location.href = '{$url['cancel']}';
    });
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
    $('#btnAdd').click(function (event) {	      
        window.location.href = '{$urlAdd}';
	  });
    
    

    
JS);
