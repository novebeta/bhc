<?php
use abengkel\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '[TA] Penyesuaian';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'TANo'         => 'No TA',
		'KodeTrans'    => 'Kode Trans',
		'PosKode'      => 'Pos Kode',
		'LokasiKode'   => 'Lokasi Kode',
		'TAKeterangan' => 'Keterangan',
		'NoGL'         => 'No Gl',
		'Cetak'        => 'Cetak',
		'UserID'       => 'User ID',
	],
	'cmbTgl'    => [
		'TATgl'        => 'Tanggal TA',
	],
	'cmbNum'    => [
		'TATotal'      => 'Total',
	],
	'sortname'  => "TATgl",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Tttahd::className(), '[TA] Penyesuaian',
    [
        'url_delete' => Url::toRoute( [ 'tttahd/delete' ] ),
    ]
), \yii\web\View::POS_READY );
