<?php
/* @var $this yii\web\View */
/* @var $model aunit\models\Tuser */
$this->title                   = 'User Profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tuser-update">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
