<?php
use aunit\assets\AppAsset;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Password';
$this->params['breadcrumbs'][] = $this->title;
$arr = [
	'cmbTxt'    => [
            'UserID'         => 'User ID',
            'UserPass'       => 'User Pass',
            'KodeAkses'      => 'Kode Akses',
            'UserNama'       => 'User Nama',
            'UserAlamat'     => 'User Alamat',
            'UserTelepon'    => 'User Telepon',
            'UserKeterangan' => 'User Keterangan',
            'UserStatus'     => 'User Status',
            'LokasiKode'     => 'Lokasi Kode',
	],
	'cmbTgl'    => [
	],
	'cmbNum'    => [
	],
	'sortname'  => "UserStatus",
	'sortorder' => 'asc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Tuser::className(),
	'User Password' ), \yii\web\View::POS_READY );