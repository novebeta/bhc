<?php
use aunit\models\Tdlokasi;
use aunit\models\Tuakses;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tuser */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-md-12 col-xl-12 col-lg-12">
    <div class="box box-primary ">
		<?php $form = ActiveForm::begin( [
			'class' => 'form-horizontal'
		] ); ?>
        <div class="box-body ">
            <div class="form-group row">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'UserID' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'UserID' )->textInput( [
						'maxlength' => true,
						'disabled'  => ! $model->isNewRecord
					] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'KodeAkses' ) ?>:</label>
                <div class="col-sm-10">
					<?= $form->field( $model, 'KodeAkses' )->widget( Select2::classname(), [
						'value'         => $model->KodeAkses,
						'options'       => [
							'placeholder' => 'Filter as you type ...',
							'class'       => 'form-control'
						],
						'theme'         => Select2::THEME_DEFAULT,
						'pluginOptions' => [ 'highlight' => true ],
						'data'          => Tuakses::find()->select2(),
						'disabled'      => ! $model->isNewRecord
					] )->label( false ) ?>
                </div>
                <label class="control-label col-sm-2"><?= $model->getAttributeLabel( 'UserStatus' ) ?>:</label>
                <div class="col-sm-6">
					<?= $form->field( $model, 'UserStatus' )->dropDownList( [
						'A' => 'AKTIF',
						'N' => 'NON AKTIF'
					],
						[
							'maxlength' => true,
							'disabled'  => ! $model->isNewRecord
						] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'LokasiKode' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'LokasiKode' )->widget( Select2::classname(), [
						'value'         => $model->LokasiKode,
						'options'       => [
							'placeholder' => 'Filter as you type ...',
							'class'       => 'form-control'
						],
						'theme'         => Select2::THEME_DEFAULT,
						'pluginOptions' => [ 'highlight' => true ],
						'data'          => ArrayHelper::map( \abengkel\models\Tdpos::find()->select2('PosKode'), 'label', 'value' ),
						'disabled'      => ! $model->isNewRecord
					] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'UserNama' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'UserNama' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'UserAlamat' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'UserAlamat' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'UserTelepon' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'UserTelepon' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'UserKeterangan' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'UserKeterangan' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6">Password Lama:</label>
                <div class="col-sm-18">
                    <div class="form-group">
						<?= Html::passwordInput( 'old_pass', null, [ 'class' => 'form-control' ] ); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6">Password Baru:</label>
                <div class="col-sm-18">
                    <div class="form-group">
						<?= Html::passwordInput( 'new_pass', null, [ 'class' => 'form-control' ] ); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6">Konfirmasi Password:</label>
                <div class="col-sm-18">
                    <div class="form-group">
						<?= Html::passwordInput( 'confirm_pass', null, [ 'class' => 'form-control' ] ); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="pull-right">-
				<?= Html::submitButton( 'Simpan', [ 'class' => 'btn btn-success' ] ) ?>
                <a href="<?= Url::to( [ 'site/index' ] ) ?>" class="btn btn-warning" role="button">Batal</a>
            </div>
        </div>
		<?php ActiveForm::end(); ?>
    </div>
</div>
