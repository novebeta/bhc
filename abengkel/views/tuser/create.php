<?php
/* @var $this yii\web\View */
/* @var $model aunit\models\Tuser */
$this->title                   = 'User';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tuser-create">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
