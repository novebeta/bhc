<?php
/* @var $this yii\web\View */
/* @var $model abengkel\models\Traccount */
$this->title                   = 'Edit Data Perkiraan: ' . $model->NoAccount;
$this->params['breadcrumbs'][] = [ 'label' => 'Data Perkiraan', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->NoAccount, 'url' => [ 'view', 'id' => $model->NoAccount ] ];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="traccount-update">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
