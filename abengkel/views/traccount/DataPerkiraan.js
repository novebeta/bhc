jQuery(function ($) {
    var searchTree = {
        trees: [],
        foundItems: [],
        selectedIndex: null,
        search: function () {
            let searchField = $('#searchField').val(),
                searchOperand = $('#searchOperand').val(),
                searchKeyword = $('#searchKeyword').val();

            switch (searchOperand) {
                case 'Left': searchPattern = new RegExp('^'+searchKeyword, 'i'); break;
                case 'Right': searchPattern = new RegExp(searchKeyword+'$', 'i'); break;
                case 'Exactly': searchPattern = new RegExp('^'+searchKeyword+'$', 'i'); break;
                case 'Not': searchPattern = new RegExp('.*[^('+searchKeyword+')].*', 'i'); break;
                default : searchPattern = new RegExp(searchKeyword, 'i');
            }

            this.selectedIndex = null;
            this.foundItems = [];
            $('#searchSelectedItem').prop('readonly', true) .val(0).attr('min', 0).attr('max', 0);

            this.trees.forEach(function (tree_id, index) {
                let id = '#'+tree_id,
                    items = $(id).jqxTree('getItems');
                // console.log(items);
                items.forEach(function (item, index) {
                    let value = JSON.parse(item.value);
                    if(value.JenisAccount === "Detail" && typeof(value[searchField]) !== "undefined" && searchPattern.test(value[searchField]))
                        searchTree.foundItems.push({ tree_id: id, item_id: item.id });
                })

            });

            $('#searchFoundItems').text('of '+this.foundItems.length);
            if(this.foundItems.length) {
                this.firstItem();
                $('#searchSelectedItem').prop('readonly', false).attr('min', 1).attr('max', this.foundItems.length);
            }
        },
        selectItem: function (idx) { // console.log(idx);
            if(!(idx > 0 && idx <= this.foundItems.length)) {
                $('#searchSelectedItem').val(this.selectedIndex);
                return false;
            }
            this.selectedIndex = idx;
            let index = idx -1,
                tree = $(this.foundItems[index].tree_id),
                item = $("li#"+this.foundItems[index].item_id),
                isFirst = this.selectedIndex === 1,
                isLast = this.selectedIndex === this.foundItems.length;

            $('#searchBtnFirst').prop('disabled', isFirst);
            $('#searchBtnPrev').prop('disabled', isFirst);
            $('#searchSelectedItem').val(this.selectedIndex);
            $('#searchBtnNext').prop('disabled', isLast);
            $('#searchBtnLast').prop('disabled', isLast);

            $('a[href="#'+tree.closest('.tab-pane').attr('id')+'"]').tab('show');

            tree.jqxTree('collapseAll');
            tree.jqxTree('selectItem', item[0]);
            tree.jqxTree('ensureVisible', item[0]);
            tree.jqxTree('expandItem', item.closest('li[role=treeitem]')[0]);
            // tree.scrollTop(item[0].offsetTop - tree[0].offsetTop);
        },
        firstItem: function () { // console.log('firstItem : '+1);
            this.selectItem(1);
        },
        prevItem: function () { // console.log('prevItem : '+(this.selectedIndex-1));
            this.selectItem(this.selectedIndex-1);
        },
        nextItem: function () { // console.log('nextItem : '+(this.selectedIndex+1));
            this.selectItem(this.selectedIndex+1);
        },
        lastItem: function () { // console.log('lastItem : '+this.foundItems.length);
            this.selectItem(this.foundItems.length);
        }
    };

    // var url = document.location.toString();
    // if (url.match('#')) {
    //     $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    // }
    function tree(event) {
        var args = event.args;
        // console.log(args.owner.itemMapping[0].item.id);
        var item = $(this).jqxTree('getItem', args.element);
        var d = JSON.parse(item.value);
        $('#NoAccount-old').val(d.NoAccount);
        $('#NoAccount-root').val(args.owner.itemMapping[0].item.id);
        $('#NoAccount').val(d.NoAccount);
        $('#NamaAccount').val(d.NamaAccount);
        $('#JenisAccount').val(d.JenisAccount);
        $('#StatusAccount').val(d.StatusAccount);
        $("#NoParent").val(d.NoParent).trigger('change.select2');
    }

    function createTree(tree_id, NoAccount, data) {
        searchTree.trees.push(tree_id);

        var source =
            {
                datatype: "json",
                datafields: [
                    {name: 'id'},
                    {name: 'NoAccount'},
                    {name: 'NamaAccount'},
                    {name: 'OpeningBalance'},
                    {name: 'JenisAccount'},
                    {name: 'StatusAccount'},
                    {name: 'NoParent'},
                    {name: 'LevelAccount'},
                    {name: 'label'},
                    {name: 'value'},
                    {name: 'icon'}
                ],
                id: 'id',
            };
        source.localdata = data;
        const dataAdapter = new $.jqx.dataAdapter(source);
        dataAdapter.dataBind();
        let record = dataAdapter.getRecordsHierarchy('id', 'NoParent', 'items');
        let treeId = $('#' + tree_id);
        treeId.jqxTree({source: record, width: '99%', height: '290px'}); //290px
        treeId.jqxTree('expandItem', $("#" + NoAccount)[0]);
        treeId.on('select', tree);
        treeId.on('itemClick', tree);
    }

    createTree('jqxWidget_10000000', '10000000', dt_aktiva);
    createTree('jqxWidget_20000000', '20000000', dt_pasiva);
    createTree('jqxWidget_30000000', '30000000', dt_sales);
    createTree('jqxWidget_40000000', '40000000', dt_pend_ope);
    createTree('jqxWidget_50000000', '50000000', dt_hpp);
    createTree('jqxWidget_60000000', '60000000', dt_biaya_ope);
    createTree('jqxWidget_80000000', '80000000', dt_biaya_non);
    createTree('jqxWidget_90000000', '90000000', dt_pend_non_ope);

    function readMode() {
        $('#data-perkiraan-input').addClass("disabledbutton");
        $('#tab_content').removeClass("disabledbutton");
        $('#delete-perkiraan').text('Hapus');
    }

    function writeMode() {
        $('#data-perkiraan-input').removeClass("disabledbutton");
        $('#tab_content').addClass("disabledbutton");
        $('#delete-perkiraan').text('Batal');
    }

    $('#add-perkiraan').click(function () {
        // window.location.href = "index.php?r=traccount/data-perkiraan-create";
        if ($(this).text() === ' Tambah') {
            writeMode();
            $(this).text('Simpan');
            $('#update-perkiraan').addClass('disabledbutton');
            // $('#NoAccount').val("");
            $('#NamaAccount').val("");
            $('#JenisAccount').val("Detail");
            $('#StatusAccount').val("A");
        } else {
            $.ajax({
                type: "post",
                url: 'index.php?r=traccount/data-perkiraan-create',
                data: $('#form-data-perkiraan').serialize(),
                success: function (data) {
                    // var obj = JSON.parse(data);
                    bootbox.alert({size: "small", message: data.msg});
                    if (data.status) {
                        readMode();
                        $(this).text('Tambah');
                        $('#update-perkiraan').removeClass('disabledbutton');
                        let rootid = $('#NoAccount-root').val();
                        createTree('jqxWidget_' + rootid, rootid, data.record);
                    }
                }
            });
            window.location.href = "index.php?r=traccount/data-perkiraan";
        }
    });
    $('#update-perkiraan').click(function () {
        // window.location.href = "index.php?r=traccount/data-perkiraan-update&id=" + btoa($('#NoAccount').val());
        // $("#tab_header").prop('disabled',true);
        if ($(this).text() === ' Edit') {
            writeMode();
            $(this).text('Simpan');
            $('#add-perkiraan').addClass('disabledbutton');
        } else {
            $.ajax({
                type: "post",
                url: 'index.php?r=traccount/data-perkiraan-update',
                data: $('#form-data-perkiraan').serialize(),
                success: function (data) {
                    // var obj = JSON.parse(data);
                    bootbox.alert({size: "small", message: data.msg});
                    if (data.status) {
                        readMode();
                        $(this).text('Edit');
                        $('#add-perkiraan').removeClass('disabledbutton');
                        let rootid = $('#NoAccount-root').val();
                        createTree('jqxWidget_' + rootid, rootid, data.record);
                    }
                }
            });
        }
    });
    $('#delete-perkiraan').click(function () {
        // var href = $(this).attr('href');
        // var rowId = $(this).attr('rowId');
        if ($(this).text() === 'Hapus') {
            var noAccount = $('#NoAccount').val();
            var rootid = $('#NoAccount-root').val();
            bootbox.confirm({
                size: "small",
                message: "Are you sure?",
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            type: "post",
                            url: 'index.php?r=traccount/data-perkiraan-delete',
                            data: {
                                'NoAccount': noAccount,
                                'RootID': rootid,
                            },
                            success: function (data) {
                                bootbox.alert({size: "small", message: data.msg});
                                if (data.status) {
                                    let rootid = $('#NoAccount-root').val();
                                    createTree('jqxWidget_' + rootid, rootid, data.record);
                                }
                            }
                        });
                    }
                }
            })
        } else {
            if ($('#update-perkiraan').text() === 'Simpan') {
                $('#update-perkiraan').text('Edit');
                $('#add-perkiraan').removeClass('disabledbutton');
            } else {
                $('#add-perkiraan').text('Tambah');
                $('#update-perkiraan').removeClass('disabledbutton');
            }
            readMode();
            $(this).text('Hapus');
        }
    });

    $('#searchBtn').click(function () { searchTree.search(); });
    $('#searchBtnFirst').click(function () { searchTree.firstItem(); });
    $('#searchBtnPrev').click(function () { searchTree.prevItem(); });
    $('#searchBtnNext').click(function () { searchTree.nextItem(); });
    $('#searchBtnLast').click(function () { searchTree.lastItem(); });
    $('#searchSelectedItem').keyup(function (e) {
        if (e.keyCode === 13) searchTree.selectItem(this.value);
    });
    $('#searchKeyword').keyup(function (e) {
        if (e.keyCode === 13) searchTree.search();
    });

});