<?php
use abengkel\assets\JqwidgetsAsset;
use kartik\datecontrol\DateControl;
use kartik\number\NumberControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $searchModel abengkel\models\TraccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
JqwidgetsAsset::register( $this );
$this->title                   = 'Neraca Awal';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsVar( 'dt_aktiva', $aktiva, \yii\web\View::POS_READY );
$this->registerJsVar( 'dt_pasiva', $pasiva, \yii\web\View::POS_READY );
$this->registerJsVar( 'dt_sales', $sales, \yii\web\View::POS_READY );
$this->registerJsVar( 'dt_pend_ope', $pend_ope, \yii\web\View::POS_READY );
$this->registerJsVar( 'dt_hpp', $hpp, \yii\web\View::POS_READY );
$this->registerJsVar( 'dt_biaya_ope', $biaya_ope, \yii\web\View::POS_READY );
$this->registerJsVar( 'dt_biaya_non', $biaya_non, \yii\web\View::POS_READY );
$this->registerJsVar( 'dt_pend_non_ope', $pend_non_ope, \yii\web\View::POS_READY );
$sqlBank = Yii::$app->db
	->createCommand( "
	SELECT NoAccount AS value, NamaAccount AS label, StatusAccount, NoAccount AS NoAccount FROM traccount 
	WHERE StatusAccount = 'A' AND JenisAccount = 'Header' 
	ORDER BY StatusAccount, NoAccount" )
	->queryAll();
$cmbBank = ArrayHelper::map( $sqlBank, 'value', 'label' );
$format  = <<< SCRIPT
function formatBank(result) {
    return '<div class="row">' +
           '<div class="col-xs-3">' + result.id + '</div>' +
           '<div class="col-xs-21">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape  = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
$dispOptions = [ 'class' => 'form-control kv-monospace' ];
//$saveOptions = [
//	'type' => 'text',
//	'label'=>'<label>Saved Value: </label>',
//	'class' => 'kv-saved',
//	'readonly' => true,
//	'tabindex' => 1000
//];
$saveCont = [ 'class' => 'kv-saved-cont' ];
?>
    <div class="panel panel-default" style="height: 525px">
        <div class="box-body no-padding col-sm-14">
            <div class="nav-tabs-custom" style="box-shadow: none;">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs ">
                    <li class="active"><a href="#aktiva" data-toggle="tab">Aktiva</a></li>
                    <li><a href="#pasiva" data-toggle="tab">Pasiva</a></li>
                    <li><a href="#pendapatan-sales" data-toggle="tab">Penjualan</a></li>
                    <li><a href="#pendapatan-operasional" data-toggle="tab">Pendapatan Operasional</a></li>
                    <li><a href="#hpp" data-toggle="tab">HPP</a></li>
                    <li><a href="#biaya-operasional" data-toggle="tab">Biaya Operasional</a></li>
                </ul>
                <div class="tab-content">
                    <div class="chart tab-pane active" id="aktiva">
                        <div id='jqxWidget_10000000'>
                        </div>
                    </div>
                    <div class="chart tab-pane" id="pasiva">
                        <div id='jqxWidget_20000000'>
                        </div>
                    </div>
                    <div class="chart tab-pane" id="pendapatan-sales">
                        <div id='jqxWidget_30000000'>
                        </div>
                    </div>
                    <div class="chart tab-pane" id="pendapatan-operasional">
                        <div id='jqxWidget_40000000'>
                        </div>
                    </div>
                    <div class="chart tab-pane" id="hpp">
                        <div id='jqxWidget_50000000'>
                        </div>
                    </div>
                    <div class="chart tab-pane" id="biaya-operasional">
                        <div id='jqxWidget_60000000'>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-body no-padding col-sm-10">
            <div class="box-body">
                <div class="form-group row">
                    <label class="control-label col-sm-6">Tanggal Awal</label>
                    <div class="col-sm-9">
						<? try {
							echo DateControl::widget( [
								'name'          => 'tgl1',
								'type'          => DateControl::FORMAT_DATE,
								'value'         => $tglAwal,
								'options'       => [
									'id' => 'filter-tgl-val1',
								],
								'pluginOptions' => [
									'autoclose' => true,
								],
							] );
						} catch ( \yii\base\InvalidConfigException $e ) {
						} ?>
                    </div>
                    <label class="control-label col-sm-9">&nbsp;</label>
                </div>
                <div class="form-group row">
                    <label class="control-label col-sm-6">No Jurnal</label>
                    <div class="col-sm-9">
						<?= Html::textInput( 'NoGL', ( $tglAwal == null ? '' : '00NA00000' ), [ 'id' => 'NoGL', 'class' => 'form-control' ] ) ?>
                    </div>
                    <label class="control-label col-sm-3">No Item</label>
                    <div class="col-sm-6">
						<?= NumberControl::widget( [
							'name'           => 'tot_item',
//							'options'            => $saveOptions,
							'displayOptions' => $dispOptions,
//							'saveInputContainer' => $saveCont
						] ); ?>
                    </div>
                </div>
                <hr/>
                <div class="form-group row">
                    <label class="control-label col-sm-6">Nomor Perkiraan</label>
                    <div class="col-sm-9">
						<?= Html::textInput( 'NoAccount', '', [ 'id' => 'NoAccount', 'class' => 'form-control' ] ) ?>
                    </div>
                    <label class="control-label col-sm-3">Jenis</label>
                    <div class="col-sm-6" style="margin-bottom: 5px">
						<?= Html::dropDownList( 'JenisAccount', null, [
							'Header' => 'Header',
							'Detail' => 'Detail',
						],
							[
								'id'    => 'JenisAccount',
								'class' => 'form-control'
							] ) ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-sm-6">Nama Perkiraan</label>
                    <div class="col-sm-18">
						<?= Html::textInput( 'NamaAccount', '', [ 'id' => 'NamaAccount', 'class' => 'form-control' ] ) ?>
                    </div>
                    <!--                    <label class="control-label col-sm-4 invisible">Status:</label>-->
                    <!--                    <div class="col-sm-11 invisible"    >-->
                    <!--						--><? //= Html::dropDownList( 'StatusAccount', null, [
					//							'A' => 'AKTIF',
					//							'N' => 'NON AKTIF',
					//						],
					//							[
					//								'id'    => 'StatusAccount',
					//								'class' => 'form-control',
					//								'style' => 'width:200px;'
					//							] ) ?>
                    <!--                    </div>-->
                </div>
                <hr/>
                <div class="form-group row">
                    <label class="control-label col-sm-6">Debet</label>
                    <div class="col-sm-18">
						<?= NumberControl::widget( [
							'name'           => 'debet',
//							'options'            => $saveOptions,
							'displayOptions' => $dispOptions,
//							'saveInputContainer' => $saveCont
						] ); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-sm-6">Kredit</label>
                    <div class="col-sm-18">
						<?= NumberControl::widget( [
							'name'           => 'kredit',
//							'options'            => $saveOptions,
							'displayOptions' => $dispOptions,
//							'saveInputContainer' => $saveCont
						] ); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-sm-6">Keterangan</label>
                    <div class="col-sm-18">
						<?= Html::textInput( 'Note', '', [ 'id' => 'note_id', 'class' => 'form-control' ] ) ?>
                    </div>
                </div>
                <hr/>
                <div class="form-group row">
                    <label class="control-label col-sm-12 text-center">Total Debet</label>
                    <label class="control-label col-sm-12 text-center">Total Kredit</label>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
						<?= NumberControl::widget( [
							'name'           => 'tot_kredit',
//							'options'            => $saveOptions,
							'displayOptions' => $dispOptions,
//							'saveInputContainer' => $saveCont
						] ); ?>
                    </div>
                    <div class="col-sm-12">
						<?= NumberControl::widget( [
							'name'           => 'tot_kredit',
//							'options'            => $saveOptions,
							'displayOptions' => $dispOptions,
//							'saveInputContainer' => $saveCont
						] ); ?>
                    </div>
                </div>
                <!--                <div class="form-group invisible">-->
                <!--                    <label class="control-label col-sm-4">Nomor Induk:</label>-->
                <!--                    <div class="col-sm-20">-->
                <!--						--><? // echo Select2::widget( [
				//							'name'          => 'NoParent',
				//							'data'          => $cmbBank,
				//							'options'       => [ 'id' => 'NoParent', 'class' => 'form-control' ],
				//							'theme'         => Select2::THEME_DEFAULT,
				//							'pluginOptions' => [
				//								'dropdownAutoWidth' => true,
				//								'templateResult'    => new JsExpression( 'formatBank' ),
				//								'templateSelection' => new JsExpression( 'formatBank' ),
				//								'matcher'           => new JsExpression( 'matchCustom' ),
				//								'escapeMarkup'      => $escape,
				//							],
				//						] ); ?>
                <!--                    </div>-->
                <!--                </div>-->
            </div>
            <div class="box-footer">
                <div class="pull-right">
                    <?= Html::button( '<i class="glyphicon glyphicon-floppy-disk"></i> Tambah', [ 'class' => 'btn btn-info btn-primary', 'id' => 'add-perkiraan' ] ) ?>
                    <?= Html::button( '<i class="glyphicon glyphicon-print"></i>&nbsp&nbsp Ubah&nbsp', [ 'class' => 'btn btn-danger btn-warning', 'id' => 'update-perkiraan' ] ) ?>
                    <?= Html::button( '<i class="fa fa-ban"></i> &nbsp&nbspHapus&nbsp', [ 'class' => 'btn btn-danger', 'id' => 'delete-perkiraan' ] ) ?>
                </div>
            </div>
        </div>
    </div>
    <div id="dataConfirmModal" class="modal bootstrap-dialog type-warning fade size-normal in" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="bootstrap-dialog-header">
                        <div class="bootstrap-dialog-close-button" style="display: none;">
                            <button class="close" data-dismiss="modal" aria-label="close">×</button>
                        </div>
                        <div class="bootstrap-dialog-title" id="w3_title">Confirmation</div>
                    </div>
                </div>
                <form action="index.php?r=traccount/data-perkiraan-delete" id="delete-form" method="post">
                    <div class="modal-body"></div>
                    <input type="hidden" name="NoAccount"/>
                    <div class="modal-footer">
                        <div class="bootstrap-dialog-footer">
                            <div class="bootstrap-dialog-footer-buttons">
                                <button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><span class="fa fa-ban"></span> Cancel</button>
                                <button class="btn btn-warning" type="submit"><span class="glyphicon glyphicon-ok"></span> Ok</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
$this->registerJs( $this->render( 'NeracaAwal.js' ) );
