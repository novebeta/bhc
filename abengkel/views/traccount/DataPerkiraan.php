<?php
use abengkel\assets\JqwidgetsAsset;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $searchModel abengkel\models\TraccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
JqwidgetsAsset::register( $this );
$this->title                   = 'Data Perkiraan';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsVar( 'dt_aktiva', $aktiva, \yii\web\View::POS_READY );
$this->registerJsVar( 'dt_pasiva', $pasiva, \yii\web\View::POS_READY );
$this->registerJsVar( 'dt_sales', $sales, \yii\web\View::POS_READY );
$this->registerJsVar( 'dt_pend_ope', $pend_ope, \yii\web\View::POS_READY );
$this->registerJsVar( 'dt_hpp', $hpp, \yii\web\View::POS_READY );
$this->registerJsVar( 'dt_biaya_ope', $biaya_ope, \yii\web\View::POS_READY );
$this->registerJsVar( 'dt_biaya_non', $biaya_non, \yii\web\View::POS_READY );
$this->registerJsVar( 'dt_pend_non_ope', $pend_non_ope, \yii\web\View::POS_READY );
$sqlBank = Yii::$app->db
	->createCommand( "
	SELECT NoAccount AS value, NamaAccount AS label, StatusAccount, NoAccount AS NoAccount FROM traccount 
	WHERE StatusAccount = 'A' AND JenisAccount = 'Header' 
	ORDER BY StatusAccount, NoAccount" )
	->queryAll();
$cmbBank = ArrayHelper::map( $sqlBank, 'value', 'label' );
$format  = <<< SCRIPT
function formatBank(result) {
    return '<div class="row">' +
           '<div class="col-xs-3">' + result.id + '</div>' +
           '<div class="col-xs-21">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape  = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="panel panel-default" style="height: 525px">
        <div class="box-body no-padding">
            <div class="nav-tabs-custom" style="box-shadow: none;">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs" id="tab_header">
                    <li class="active"><a href="#aktiva" data-toggle="tab">Aktiva</a></li>
                    <li><a href="#pasiva" data-toggle="tab">Pasiva</a></li>
                    <li><a href="#pendapatan-sales" data-toggle="tab">Pendapatan Sales</a></li>
                    <li><a href="#pendapatan-operasional" data-toggle="tab">Pendapatan Operasional</a></li>
                    <li><a href="#hpp" data-toggle="tab">HPP</a></li>
                    <li><a href="#biaya-operasional" data-toggle="tab">Biaya Operasional</a></li>
                    <li><a href="#biaya-non-operasional" data-toggle="tab">Biaya Non Operasional</a></li>
                    <li><a href="#pendapatan-non-operasional" data-toggle="tab">Pendapatan Non Operasional</a></li>
                </ul>
                <div class="tab-content" id="tab_content">
                    <!-- Morris chart - Sales -->
                    <div class="chart tab-pane active" id="aktiva">
                        <div id='jqxWidget_10000000'>
                        </div>
                    </div>
                    <div class="chart tab-pane" id="pasiva">
                        <div id='jqxWidget_20000000'>
                        </div>
                    </div>
                    <div class="chart tab-pane" id="pendapatan-sales">
                        <div id='jqxWidget_30000000'>
                        </div>
                    </div>
                    <div class="chart tab-pane" id="pendapatan-operasional">
                        <div id='jqxWidget_40000000'>
                        </div>
                    </div>
                    <div class="chart tab-pane" id="hpp">
                        <div id='jqxWidget_50000000'>
                        </div>
                    </div>
                    <div class="chart tab-pane" id="biaya-operasional">
                        <div id='jqxWidget_60000000'>
                        </div>
                    </div>
                    <div class="chart tab-pane" id="biaya-non-operasional">
                        <div id='jqxWidget_80000000'>
                        </div>
                    </div>
                    <div class="chart tab-pane" id="pendapatan-non-operasional">
                        <div id='jqxWidget_90000000'>
                        </div>
                    </div>
                </div>
                <div class="box-body disabledbutton" style="padding-top: 0px;" id="data-perkiraan-input">
                    <form method="post" id="form-data-perkiraan">
                        <input type="hidden" id="NoAccount-old" name="NoAccountOld">
                        <input type="hidden" id="NoAccount-root" name="RootID">
                        <div class="form-group">
                            <label class="control-label col-sm-4">No Perkiraan:</label>
                            <div class="col-sm-5">
								<?= Html::textInput( 'Traccount[NoAccount]', '', [ 'id' => 'NoAccount', 'class' => 'form-control' ] ) ?>
                            </div>
                            <label class="control-label col-sm-4">Nama Perkiraan:</label>
                            <div class="col-sm-11" style="margin-bottom: 5px">
								<?= Html::textInput( 'Traccount[NamaAccount]', '', [ 'id' => 'NamaAccount', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4">Jenis Perkiraan:</label>
                            <div class="col-sm-5" style="margin-bottom: 5px">
								<?= Html::dropDownList( 'Traccount[JenisAccount]', null, [
									'Header' => 'Header',
									'Detail' => 'Detail',
								],
									[
										'id'    => 'JenisAccount',
										'class' => 'form-control'
									] ) ?>
                            </div>
                            <label class="control-label col-sm-4">Status:</label>
                            <div class="col-sm-11" style="margin-bottom: 5px">
								<?= Html::dropDownList( 'Traccount[StatusAccount]', null, [
									'A' => 'AKTIF',
									'N' => 'NON AKTIF',
								],
									[
										'id'    => 'StatusAccount',
										'class' => 'form-control',
										'style' => 'width:200px;'
									] ) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4">Nomor Induk:</label>
                            <div class="col-sm-20">
								<? echo Select2::widget( [
									'name'          => 'Traccount[NoParent]',
									'data'          => $cmbBank,
									'options'       => [ 'id' => 'NoParent', 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatBank' ),
										'templateSelection' => new JsExpression( 'formatBank' ),
										'matcher'           => new JsExpression( 'matchCustom' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="box-footer">
                    <div class=" row-no-gutters">
                        <div class=" form-inline col-md-16">
                            <div class="form-group">
                                <div class="col-sm-5">
                                    <?= Html::dropDownList( '', null, [
                                        'NamaAccount'   => 'Nama Perkiraan',
                                        'NoAccount'     => 'Nomor Perkiraan',
                                        'NoParent'      => 'Nomor Induk Perkiraan',
                                    ],
                                        [
                                            'id'    => 'searchField',
                                            'class' => 'form-control'
                                        ] ) ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-5">
                                    <?= Html::dropDownList( '', null, [
                                        'Both'      => 'Both',
                                        'Left'      => 'Left',
                                        'Right'     => 'Right',
                                        'Exactly'   => 'Exactly',
                                        'Not'       => 'Not',
                                    ],
                                        [
                                            'id'    => 'searchOperand',
                                            'class' => 'form-control'
                                        ] ) ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-5">
                                    <div class="input-group">
                                        <input type="text" id="searchKeyword" class="form-control" style="width: 150px;">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" id="searchBtn" type="button"><i class="glyphicon glyphicon-search"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" id="searchBtnFirst" type="button" disabled><i class="glyphicon glyphicon-step-backward"></i></button>
                                    <button class="btn btn-default" id="searchBtnPrev" type="button" disabled><i class="glyphicon glyphicon-chevron-left"></i></button>
                                </span>
                                <input type="number" class="form-control" id="searchSelectedItem" placeholder="0" style="width: 50px;" min="0" max="0" readonly>
                                <span class="input-group-addon" id="searchFoundItems">of 0</span>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" id="searchBtnNext" type="button" disabled><i class="glyphicon glyphicon-chevron-right"></i></button>
                                    <button class="btn btn-default" id="searchBtnLast" type="button" disabled><i class="glyphicon glyphicon-step-forward"></i></button>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="row-fluid pull-right">
                                <!--						--><?//= Html::button( 'Simpan', [ 'class' => 'btn btn-success', 'id' => 'save-perkiraan' ] ) ?>
                                <!--						--><?//= Html::button( 'Batal', [ 'class' => 'btn btn-warning ', 'id' => 'batal-perkiraan' ] ) ?>
                                <?= Html::button( '<i class="glyphicon glyphicon-floppy-disk"></i> Tambah', [ 'class' => 'btn btn-info btn-primary', 'id' => 'add-perkiraan' ] ) ?>
                                <?= Html::button( '<i class="glyphicon glyphicon-print"></i>&nbsp&nbsp Ubah', [ 'class' => 'btn btn-danger btn-warning', 'id' => 'update-perkiraan' ] ) ?>
                                <?= Html::button( '<i class="fa fa-ban"></i> &nbsp&nbspHapus&nbsp', [ 'class' => 'btn btn-danger', 'id' => 'delete-perkiraan' ] ) ?>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div id="dataConfirmModal" class="modal bootstrap-dialog type-warning fade size-normal in" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="bootstrap-dialog-header">
                        <div class="bootstrap-dialog-close-button" style="display: none;">
                            <button class="close" data-dismiss="modal" aria-label="close">×</button>
                        </div>
                        <div class="bootstrap-dialog-title" id="w3_title">Confirmation</div>
                    </div>
                </div>
                <form action="index.php?r=traccount/data-perkiraan-delete" id="delete-form" method="post">
                    <div class="modal-body"></div>
                    <input type="hidden" name="NoAccount"/>
                    <div class="modal-footer">
                        <div class="bootstrap-dialog-footer">
                            <div class="bootstrap-dialog-footer-buttons">
                                <button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><span class="fa fa-ban"></span> Cancel</button>
                                <button class="btn btn-warning" type="submit"><span class="glyphicon glyphicon-ok"></span> Ok</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
$this->registerJs( $this->render( 'DataPerkiraan.js' ) );
