<?php
/* @var $this yii\web\View */
/* @var $model abengkel\models\Traccount */
$this->title                   = 'Ubah Data Perkiraan: ' . $model->NoAccount;
$this->params['breadcrumbs'][] = [ 'label' => 'Data Perkiraan', 'url' => [ 'data-perkiraan' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->NoAccount, 'url' => [ 'data-perkiraan-update', 'id' => base64_encode($model->NoAccount) ] ];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="traccount-update">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
