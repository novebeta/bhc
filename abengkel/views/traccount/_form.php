<?php
use abengkel\models\Traccount;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Traccount */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function formattraccount(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-8" style="text-align: left">' + result.id + '</div>' +
           '<div class="col-xs-16">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
$url['cancel'] = Url::toRoute('traccount/index');
?>



<div class="traccount-form">
    <?php
    $form = ActiveForm::begin([]);
    \abengkel\components\TUi::form(
        [
            'class' => "row-no-gutters",
            'items' => [
                [
                    'class' => "form-group col-md-24",
                    'style' => "margin-bottom: -2px;",
                    'items' => [
                        [
                            'class' => "col-sm-3",
                            'items' => [
                                [
                                    'items' => ":LabelNomorPerkiraan"
                                ],
                            ]
                        ],
                        [
                            'class' => "col-sm-8",
                            'items' => [
                                [
                                    'items' => ":NomorPerkiraan"
                                ],
                            ]
                        ],
                        ['class' => "col-sm-1"],
                        [
                            'class' => "col-sm-3",
                            'items' => [
                                [
                                    'items' => ":LabelNamaPerkiraan"
                                ],
                            ]
                        ],
                        [
                            'class' => "col-sm-8",
                            'items' => [
                                [
                                    'items' => ":NamaPerkiraan"
                                ],
                            ]
                        ],
                    ],
                ],
                [
                    'class' => "form-group col-md-24",
                    'style' => "margin-bottom: -2px;",
                    'items' => [
                        [
                            'class' => "col-sm-3",
                            'items' => [
                                [
                                    'items' => ":LabelJenisPerkiraan"
                                ],
                            ]
                        ],
                        [
                            'class' => "col-sm-8",
                            'items' => [
                                [
                                    'items' => ":JenisPerkiraan"
                                ],
                            ]
                        ],
                        ['class' => "col-sm-1"],
                        [
                            'class' => "col-sm-3",
                            'items' => [
                                [
                                    'items' => ":LabelStatus"
                                ],
                            ]
                        ],
                        [
                            'class' => "col-sm-4",
                            'items' => [
                                [
                                    'items' => ":Status"
                                ],
                            ]
                        ],
                    ],
                ],
                [
                    'class' => "form-group col-md-24",
                    'style' => "margin-bottom: -2px;",
                    'items' => [
                        [
                            'class' => "col-sm-3",
                            'items' => [
                                [
                                    'items' => ":LabelNomorInduk"
                                ],
                            ]
                        ],
                        [
                            'class' => "col-sm-8",
                            'items' => [
                                [
                                    'items' => ":NomorInduk"
                                ],
                            ]
                        ],

                    ],
                ],
            ]
        ],
        [
            'class' => "pull-right",
            'items' => ":btnSave :btnCancel"
        ],
        [
            ":LabelNomorPerkiraan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor Perkiraan</label>',
            ":LabelNamaPerkiraan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Perkiraan</label>',
            ":LabelJenisPerkiraan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis Perkiraan</label>',
            ":LabelStatus" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',
            ":LabelNomorInduk" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor Induk</label>',


            ":NomorPerkiraan" => $form->field($model, 'NoAccount', ['template' => '{input}'])
                ->textInput(['maxlength' => true,]),
            ":NamaPerkiraan" => $form->field($model, 'NamaAccount', ['template' => '{input}'])
                ->textInput(['maxlength' => true,]),
            ":JenisPerkiraan" => $form->field($model, 'JenisAccount', ['template' => '{input}'])
                ->dropDownList(
                    [
                        'Header' => 'Header',
                        'Detail' => 'Detail'
                    ],
                    [
                        'maxlength' => true,
                    ]),
            ":Status" => $form->field($model, 'StatusAccount', ['template' => '{input}'])
                ->dropDownList(
                    [
                        'A' => 'AKTIF',
                        'N' => 'NON AKTIF'
                    ],
                    [
                        'maxlength' => true,
                    ]),
            ":NomorInduk" => $form->field($model, 'NoParent', ['template' => '{input}'])
                ->widget(Select2::classname(), [
                    'value' => $model->NoParent,
                    'theme' => Select2::THEME_DEFAULT,
                    'data'          => Traccount::find()->select2(),
                    'pluginOptions' => [
                        'dropdownAutoWidth' => true,
                        'templateResult' => new JsExpression('formattraccount'),
                        'templateSelection' => new JsExpression('formattraccount'),
                        'matcher' => new JsExpression('matchCustom'),
                        'escapeMarkup' => $escape,
                    ],
                ]),


            ":btnSave" => Html::submitButton('<i class="glyphicon glyphicon-floppy-disk"></i> Simpan', ['class' => 'btn btn-info btn-primary ', 'id' => 'btnSave']),
            ":btnCancel"      => Html::Button( '<i class="fa fa-ban"></i>&nbspBatal&nbsp&nbsp', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel' ] ),
        ]
    );
    ActiveForm::end();
    ?>
</div>


<!--<div class="col-md-12 col-xl-12 col-lg-12">-->
<!--    <div class="box box-primary ">-->
<!--		--><?php //$form = ActiveForm::begin( [
//			'class' => 'form-horizontal'
//		] ); ?>
<!--        <div class="box-body ">-->
<!--            <div class="form-group">-->
<!--                <label class="control-label col-sm-6">--><?//= $model->getAttributeLabel( 'NoAccount' ) ?><!--:</label>-->
<!--                <div class="col-sm-18">-->
<!--					--><?//= $form->field( $model, 'NoAccount' )->textInput( [
//						'maxlength' => true,
//					] )->label( false) ?>
<!--                </div>-->
<!--            </div>-->
<!--            <div class="form-group">-->
<!--                <label class="control-label col-sm-6">--><?//= $model->getAttributeLabel( 'NamaAccount' ) ?><!--:</label>-->
<!--                <div class="col-sm-18">-->
<!--					--><?//= $form->field( $model, 'NamaAccount' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
<!--                </div>-->
<!--            </div>-->
<!--            <div class="form-group">-->
<!--                <label class="control-label col-sm-6">--><?//= $model->getAttributeLabel( 'JenisAccount' ) ?><!--:</label>-->
<!--                <div class="col-sm-18">-->
<!--					--><?//= $form->field( $model, 'JenisAccount' )->dropDownList( [
//						'Header' => 'Header',
//						'Detail' => 'Detail',
//					], [ 'maxlength' => true ] )->label( false ) ?>
<!--                </div>-->
<!--            </div>-->
<!--            <div class="form-group">-->
<!--                <label class="control-label col-sm-6">--><?//= $model->getAttributeLabel( 'StatusAccount' ) ?><!--:</label>-->
<!--                <div class="col-sm-18">-->
<!--					--><?//= $form->field( $model, 'StatusAccount' )->dropDownList( [
//						'A' => 'AKTIF',
//						'N' => 'NON AKTIF',
//					], [ 'maxlength' => true ] )->label( false ) ?>
<!--                </div>-->
<!--            </div>-->
<!--            <div class="form-group">-->
<!--                <label class="control-label col-sm-6">--><?//= $model->getAttributeLabel( 'NoParent' ) ?><!--:</label>-->
<!--                <div class="col-sm-18">-->
<!--					--><?//= $form->field( $model, 'NoParent' )->widget( Select2::classname(), [
//						'value'         => $model->NoParent,
//						'options'       => [ 'placeholder' => 'Filter as you type ...' ],
//						'pluginOptions' => [ 'highlight' => true ],
//						'data'          => Traccount::find()->header()
//					] )->label( false ) ?>
<!--                </div>-->
<!--            </div>-->
<!--            <div class="box-footer">-->
<!--                <div class="pull-right">-->
<!--					--><?//= Html::submitButton( '<i class="glyphicon glyphicon-floppy-disk"></i> Simpan', [ 'class' => 'btn btn-info btn-primary' ] ) ?>
<!--                    <a href="--><?//= Url::to( [ 'traccount/index' ] ) ?><!--"class="btn btn-danger btn-primary" role="button"><i class="glyphicon glyphicon-ban-circle"></i>&nbsp&nbsp Batal &nbsp</a>-->
<!--                </div>-->
<!--            </div>-->
<!--			--><?php //ActiveForm::end(); ?>
<!--        </div>-->
<!--    </div>-->
