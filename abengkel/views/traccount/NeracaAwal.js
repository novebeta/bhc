jQuery(function ($) {
    // var url = document.location.toString();
    // if (url.match('#')) {
    //     $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    // }
    function tree(event) {
        var args = event.args;
        // console.log(args.owner.itemMapping[0].item.id);
        var item = $(this).jqxTree('getItem', args.element);
        var d = JSON.parse(item.value);
        $('#NoAccount-old').val(d.NoAccount);
        $('#NoAccount-root').val(args.owner.itemMapping[0].item.id);
        $('#NoAccount').val(d.NoAccount);
        $('#NamaAccount').val(d.NamaAccount);
        $('#JenisAccount').val(d.JenisAccount);
        $('#StatusAccount').val(d.StatusAccount);
        $("#NoParent").val(d.NoParent).trigger('change.select2');
    }

    function createTree(tree_id, NoAccount, data) {
        var source =
            {
                datatype: "json",
                datafields: [
                    {name: 'id'},
                    {name: 'NoAccount'},
                    {name: 'NamaAccount'},
                    {name: 'OpeningBalance'},
                    {name: 'JenisAccount'},
                    {name: 'StatusAccount'},
                    {name: 'NoParent'},
                    {name: 'LevelAccount'},
                    {name: 'label'},
                    {name: 'value'},
                    {name: 'icon'}
                ],
                id: 'id',
            };
        source.localdata = data;
        const dataAdapter = new $.jqx.dataAdapter(source);
        dataAdapter.dataBind();
        let record = dataAdapter.getRecordsHierarchy('id', 'NoParent', 'items');
        let treeId = $('#' + tree_id);
        treeId.jqxTree({source: record, width: '99%', height: '450px'}); //290px
        treeId.jqxTree('expandItem', $("#" + NoAccount)[0]);
        treeId.on('select', tree);
        treeId.on('itemClick', tree);
    }

    createTree('jqxWidget_10000000', '10000000', dt_aktiva);
    createTree('jqxWidget_20000000', '20000000', dt_pasiva);
    createTree('jqxWidget_30000000', '30000000', dt_sales);
    createTree('jqxWidget_40000000', '40000000', dt_pend_ope);
    createTree('jqxWidget_50000000', '50000000', dt_hpp);
    createTree('jqxWidget_60000000', '60000000', dt_biaya_ope);

    function readMode() {
        $('#data-perkiraan-input').addClass("disabledbutton");
        $('#tab_content').removeClass("disabledbutton");
        $('#delete-perkiraan').text('Hapus');
    }

    function writeMode() {
        $('#data-perkiraan-input').removeClass("disabledbutton");
        $('#tab_content').addClass("disabledbutton");
        $('#delete-perkiraan').text('Batal');
    }

    $('#add-perkiraan').click(function () {
        // window.location.href = "index.php?r=traccount/data-perkiraan-create";
        if ($(this).text() === 'Tambah') {
            writeMode();
            $(this).text('Simpan');
            $('#update-perkiraan').addClass('disabledbutton');
            $('#NoAccount').val("");
            $('#NamaAccount').val("");
            $('#JenisAccount').val("Detail");
            $('#StatusAccount').val("A");
        } else {
            $.ajax({
                type: "post",
                url: 'index.php?r=traccount/data-perkiraan-create',
                data: $('#form-data-perkiraan').serialize(),
                success: function (data) {
                    // var obj = JSON.parse(data);
                    bootbox.alert({size: "small", message: data.msg});
                    if (data.status) {
                        readMode();
                        $(this).text('Tambah');
                        $('#update-perkiraan').removeClass('disabledbutton');
                        let rootid = $('#NoAccount-root').val();
                        createTree('jqxWidget_' + rootid, rootid, data.record);
                    }
                }
            });
        }
    });
    $('#update-perkiraan').click(function () {
        // window.location.href = "index.php?r=traccount/data-perkiraan-update&id=" + btoa($('#NoAccount').val());
        // $("#tab_header").prop('disabled',true);
        if ($(this).text() === 'Edit') {
            writeMode();
            $(this).text('Simpan');
            $('#add-perkiraan').addClass('disabledbutton');
        } else {
            $.ajax({
                type: "post",
                url: 'index.php?r=traccount/data-perkiraan-update',
                data: $('#form-data-perkiraan').serialize(),
                success: function (data) {
                    // var obj = JSON.parse(data);
                    bootbox.alert({size: "small", message: data.msg});
                    if (data.status) {
                        readMode();
                        $(this).text('Edit');
                        $('#add-perkiraan').removeClass('disabledbutton');
                        let rootid = $('#NoAccount-root').val();
                        createTree('jqxWidget_' + rootid, rootid, data.record);
                    }
                }
            });
        }
    });
    $('#delete-perkiraan').click(function () {
        // var href = $(this).attr('href');
        // var rowId = $(this).attr('rowId');
        if ($(this).text() === 'Hapus') {
            var noAccount = $('#NoAccount').val();
            var rootid = $('#NoAccount-root').val();
            bootbox.confirm({
                size: "small",
                message: "Are you sure?",
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            type: "post",
                            url: 'index.php?r=traccount/data-perkiraan-delete',
                            data: {
                                'NoAccount': noAccount,
                                'RootID': rootid,
                            },
                            success: function (data) {
                                bootbox.alert({size: "small", message: data.msg});
                                if (data.status) {
                                    let rootid = $('#NoAccount-root').val();
                                    createTree('jqxWidget_' + rootid, rootid, data.record);
                                }
                            }
                        });
                    }
                }
            })
        } else {
            if ($('#update-perkiraan').text() === 'Simpan') {
                $('#update-perkiraan').text('Edit');
                $('#add-perkiraan').removeClass('disabledbutton');
            } else {
                $('#add-perkiraan').text('Tambah');
                $('#update-perkiraan').removeClass('disabledbutton');
            }
            readMode();
            $(this).text('Hapus');
        }
    });
});