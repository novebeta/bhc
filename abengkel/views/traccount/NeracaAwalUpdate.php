<?php
/* @var $this yii\web\View */
/* @var $model abengkel\models\Traccount */
$this->title                   = 'Ubah Neraca Awal: ' . $model->NoAccount;
$this->params['breadcrumbs'][] = [ 'label' => 'Neraca Awal', 'url' => [ 'neraca-awal' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->NoAccount, 'url' => [ 'neraca-awal-update', 'id' => base64_encode($model->NoAccount) ] ];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="traccount-update">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
