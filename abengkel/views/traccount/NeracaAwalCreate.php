<?php
/* @var $this yii\web\View */
/* @var $model abengkel\models\Traccount */
$this->title                   = 'Tambah Neraca Awal';
$this->params['breadcrumbs'][] = [ 'label' => 'Neraca Awal', 'url' => [ 'neraca-awal' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="traccount-create">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
