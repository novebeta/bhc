<?php
use yii\grid\GridView;
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel abengkel\models\TtgeneralledgeritSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ttgeneralledgerits';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttgeneralledgerit-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ttgeneralledgerit', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'NoGL',
            'TglGL',
            'GLAutoN',
            'NoAccount',
            'KeteranganGL',
            //'DebetGL',
            //'KreditGL',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
