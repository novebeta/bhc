<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttgeneralledgerit */

$this->title = 'Update Ttgeneralledgerit: ' . $model->NoGL;
$this->params['breadcrumbs'][] = ['label' => 'Ttgeneralledgerits', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NoGL, 'url' => ['view', 'NoGL' => $model->NoGL, 'TglGL' => $model->TglGL, 'GLAutoN' => $model->GLAutoN]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ttgeneralledgerit-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
