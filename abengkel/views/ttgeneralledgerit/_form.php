<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttgeneralledgerit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ttgeneralledgerit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'NoGL')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TglGL')->textInput() ?>

    <?= $form->field($model, 'GLAutoN')->textInput() ?>

    <?= $form->field($model, 'NoAccount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'KeteranganGL')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DebetGL')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'KreditGL')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
