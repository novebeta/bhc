<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttgeneralledgerit */

$this->title = 'Create Ttgeneralledgerit';
$this->params['breadcrumbs'][] = ['label' => 'Ttgeneralledgerits', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttgeneralledgerit-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
