<?php
use abengkel\assets\AppAsset;
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '[CC] Klaim C2';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'CCNo'        => 'No CC',
		'CCNoKlaim'   => 'No Klaim',
		'CCMemo'      => 'Memo',
		'NoGL'        => 'No GL',
		'Cetak'       => 'Cetak',
		'UserID'      => 'User ID',
	],
	'cmbTgl'    => [
		'CCTgl'       => 'Tgl CC',
	],
	'cmbNum'    => [
		'CCTotalPart' => 'Total Part',
		'CCTotalJasa' => 'Total Jasa',
	],
	'sortname'  => "CCTgl",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Ttcchd::className(), '[CC] Klaim C2',
    [
        'url_delete' => Url::toRoute( [ 'ttcchd/delete' ] ),
    ]), \yii\web\View::POS_READY );
