<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttcchd */

$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = 'Edit - Klaim C2 : ' . $dsTServis['CCNoView'];
?>
<div class="ttcchd-update">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTServis' => $dsTServis,
        'id'=> $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttccit/index','action' => 'update' ] ),
        ]
    ]) ?>

</div>
