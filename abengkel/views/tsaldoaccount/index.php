<?php
use abengkel\assets\AppAsset;
/* @var $this yii\web\View */
/* @var $searchModel abengkel\models\TsaldoaccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tutup Buku';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [],
	'cmbTgl'    => [],
	'cmbNum'    => [],
	'sortname'  => "TglOpen",
	'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Tsaldoaccount::className(),
	'Tutup Buku' ), \yii\web\View::POS_READY );
