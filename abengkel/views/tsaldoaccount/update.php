<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tsaldoaccount */

$this->title = 'Tutup Buku';
$this->params['breadcrumbs'][] = ['label' => 'Tutup Buku', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (DateTime::createFromFormat('Y-m-d', $model->TglOpen))->format('d/m/Y'), 'url' => ['view', 'NoAccount' => $model->NoAccount, 'TglSaldo' => $model->TglSaldo]];
$this->params['breadcrumbs'][] = 'Edit';
$params = '&id='.$id.'&action=update';
?>
<div class="tsaldoaccount-update">
    <?= $this->render('_form', [
        'model' => $model,
        'url'   => [
            'update'    => Custom::url(\Yii::$app->controller->id.'/update'.$params ),
            'print'     => Custom::url(\Yii::$app->controller->id.'/print'.$params ),
            'cancel'    => Custom::url(\Yii::$app->controller->id.'/cancel'.$params ),
            'detail'    => Custom::url(\Yii::$app->controller->id.'/detail' )
        ]
    ]) ?>
</div>
