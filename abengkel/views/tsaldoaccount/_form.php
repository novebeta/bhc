<?php
use abengkel\components\TUi;
use kartik\datecontrol\DateControl;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tsaldoaccount */
/* @var $form yii\widgets\ActiveForm */
\abengkel\assets\AppAsset::register($this);
?>
<div class="tsaldoaccount-form">
    <?php
        $form = ActiveForm::begin( [ 'id' => 'frm_tsaldoaccount_id', 'action' => $url[ 'create' ] ] );
        TUi::form(
            [
                'class' => "row",
                'items' => [
                    [
                        'class' => "row col-md-20 gridContainer",
                        'style' => "margin-bottom: 3px;",
                        'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
                    ],
                    [
                        'class' => "form-inline col-md-4",
                        'items' => [
                            [
                                'style' => "margin-bottom: 3px;",
                                'items' => ":TglDari"
                            ],
                            [
                                'style' => "margin-bottom: 3px;",
                                'items' => ":TglSampai"
                            ],
                            [
                                'style' => "margin-bottom: 3px;",
                                'items' => ":DaftarTglTutupBuku"
                            ]
                        ]
                    ]
                ]
            ],
            [
                'class' => "row-no-gutters",
                'items' => [
                    [
                        'class' => "form-inline col-md-12",
                        'items' => ":TglSaldo"
                    ],
                    [
                        'class' => "col-md-12",
                        'items' => [
                            [
                                'class' => "pull-right",
                                'items' => ":btnCreate :btnDelete"
                            ]
                        ]
                    ]
                ]
            ],
            [
                ":TglDari"            => '<div class="form-group">' .
                    '<label class="control-label" style="margin: 0; padding: 0; width: 60px;" for="TglDari">Periode</label>' .
                    DateControl::widget( [
                        'id'   => 'TglDari',
                        'name' => 'TglDari',
                        'value'         => Yii::$app->formatter->asDate( '-5 years', 'yyyy-01-01' ),
                        'type' => DateControl::FORMAT_DATE
                    ] )
                    . '</div>',
                ":TglSampai"          => '<div class="form-group">' .
                    '<label class="control-label" style="margin: 0; padding: 0; width: 0px;" for="TglSampai"></label>' .
                    DateControl::widget( [
                        'id'   => 'TglSampai',
                        'name' => 'TglSampai',
                        'value'         => Yii::$app->formatter->asDate( 'now', 'yyyy-MM-dd' ),
                        'type' => DateControl::FORMAT_DATE
                    ] )
                    . '</div>',
                ":TglSaldo"           => $form->field( $model, 'TglSaldo', [ 'template' => '{label}{input}' ] )
                    ->widget( DateControl::class, [
                        'type' => DateControl::FORMAT_DATE
                    ] )->label( 'Saldo Awal Sampai Tanggal', [ 'style' => 'margin: 0; padding: 6px 0; width: 150px;' ] ),
                ":DaftarTglTutupBuku" => '<label class="control-label" style="margin: 0; padding: 0;" for="list-group">Tanggal</label>' .
                    '<div id="DaftarTglTutupBuku" class="list-group" style="max-height: 360px;"></div>',
                ":btnCreate"          => '<a id="btnCreate" class="btn btn-info" role="button"> <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp&nbspTambah</a>',
                ":btnDelete"          => '<a href="#" id="btnDelete" class="btn btn-danger" role="button"><i class="fa fa-ban"></i>&nbsp&nbsp Hapus &nbsp&nbsp</a>',
                ":btnSave"            => '<a href="#" id="btnSave" class="btn btn-info" role="button"> <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp&nbspSimpan</a>',
                ":btnCancel"          => Html::Button( '<i class="fa fa-ban"></i>&nbspBatal&nbsp&nbsp', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel' ] ),
            ]
        );
        ActiveForm::end();
        ?>
</div>
<?php
$urlListTutupBuku = $url[ 'list' ];
$urlSaldoAccount  = $url[ 'detail' ];
$this->registerJs( <<< JS

    $('#detailGrid').utilJqGrid({  
        height: 410,
        rownumbers: false,  
        loadonce:true,
        rowNum: 10000,  
        readOnly: true,    
        shrinkToFit: true,    
        autowidth: true,    
        navButtonTambah: false,        
        colModel: [
            {
                name: 'NoAccount',
                label: 'Nomor',
                width: 60
            },
            {
                name: 'NamaAccount',
                label: 'Nama Perkiraan',
                width: 300
            },
            {
                name: 'MyOpening',
                label: 'Open',
                width: 120,
                template: 'money',
            },
            {
                name: 'Debet',
                label: 'Debet',
                width: 120,
                template: 'money',
            },
            {
                name: 'Kredit',
                label: 'Kredit',
                width: 120,
                template: 'money',
            },
            {
                name: 'SaldoAccount',
                label: 'SaldoAccount',
                width: 120,
                template: 'money',
            }
        ],  
     }).init().fit($('.gridContainer'));
        

        btnCreate = $('#btnCreate'),
        btnDelete = $('#btnDelete'),

        funcLoadListTutupBuku = function(a, b, c) {
            if($('#TglDari').val() && $('#TglSampai').val())
                $.ajax({
                    url: '$urlListTutupBuku',
                    method:"POST",
                    data: {
                        TglDari: $('#TglDari').val(),
                        TglSampai: $('#TglSampai').val()
                    },
                    success: function(response) {
                        //console.log(response);
                        let elem = $('#DaftarTglTutupBuku') .empty();
                        
                        for(i=0; i < response.rows.length; i++) {
                            elem.append('<a href="javascript:void(0);" class="list-group-item item-tglsaldo" style="padding:3px 15px;" TglSaldo="'+response.rows[i]['TglSaldo']+'">'+response.rows[i]['TglSaldoFormatted']+'</a>');
                        }
                        
                        $('.item-tglsaldo').click(function() {
                            funcLoadSaldoAccount($(this).attr('TglSaldo'), 'read');
                            $('.item-tglsaldo').removeClass('active')
                            $(this).addClass('active');
                            $('#tsaldoaccount-tglsaldo').utilDateControl().val($(this).attr('TglSaldo'));
                        });
                        $('.item-tglsaldo').last().trigger('click');
                    }
                });
        },
        funcLoadSaldoAccount = function (TglSaldo, oper) {
            $.ajax({
                url: '$urlSaldoAccount',
                method:"POST",
                data: { oper: oper, Tanggal: TglSaldo },
                success: function(response) {
                    $('#detailGrid').jqGrid('clearGridData')
                        .jqGrid('setGridParam', { datatype: 'local', data: response.rows })
                        .trigger('reloadGrid')
                        .jqGrid('setGridParam', { datatype: 'json' });
                }
            });
        };
    
    
    $('#TglDari').change(funcLoadListTutupBuku);
    $('#TglSampai').change(funcLoadListTutupBuku);
    
    btnCreate.click(function() {
        var TglSaldo = $('#tsaldoaccount-tglsaldo').val();
       $.ajax({
            url: '$urlSaldoAccount',
            method:"POST",
            data: { oper: 'add', Tanggal: TglSaldo },
            success: function(response) {
                $.notify({message: response.msg},{type: response.status == '0' ? 'success':'warning'});
                $('#detailGrid').jqGrid('clearGridData')
                    .jqGrid('setGridParam', { datatype: 'local', data: response.rows })
                    .trigger('reloadGrid')
                    .jqGrid('setGridParam', { datatype: 'json' });
                funcLoadListTutupBuku();
            }
        });
    })
    
    btnDelete.click(function() {
        var TglSaldo = $(".list-group a.active").attr('TglSaldo');
        if (TglSaldo === undefined) return;
        $.ajax({
            url: '$urlSaldoAccount',
            method:"POST",
            data: { oper: 'del', Tanggal: TglSaldo },
            success: function(response) {                
                $.notify({message: response.msg},{type: response.status == '0' ? 'success':'warning'});
                $('#detailGrid').jqGrid('clearGridData')
                    .jqGrid('setGridParam', { datatype: 'local', data: response.rows })
                    .trigger('reloadGrid')
                    .jqGrid('setGridParam', { datatype: 'json' });
                funcLoadListTutupBuku();
            }
        });
    })
    
    funcLoadListTutupBuku();
    
    $('[name=TglDari]').utilDateControl().cmp().attr('tabindex', '1');
    $('[name=TglSampai]').utilDateControl().cmp().attr('tabindex', '2');
    
JS
);