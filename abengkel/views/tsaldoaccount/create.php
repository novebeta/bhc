<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tsaldoaccount */

$this->title = 'Tutup Buku';
$this->params['breadcrumbs'][] = $this->title;
$params = '&id='.$id.'&action=create';
?>
<div class="tsaldoaccount-create">
    <?= $this->render('_form', [
        'model' => $model,
        'url'   => [
            'create'    => Custom::url(\Yii::$app->controller->id.'/create'.$params ),
            'delete'    => Custom::url(\Yii::$app->controller->id.'/delete'.$params ),
            'detail'    => Custom::url(\Yii::$app->controller->id.'/detail' ),
            'list'      => Custom::url(\Yii::$app->controller->id.'/list' )
        ]
    ]) ?>
</div>
