<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttbmhd */

$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = 'Edit - Bank Masuk Dari Kas : ' . $dsTUang['BMNoView'];
?>
<div class="ttbmhd-update">
    <?= $this->render('_bank-masuk-dari-kas-form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'id' => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/bank-masuk-dari-kas-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
        ]
    ]) ?>
</div>
