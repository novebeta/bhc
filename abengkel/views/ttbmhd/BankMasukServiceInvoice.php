<?php
use abengkel\assets\AppAsset;
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Bank Masuk - Servis';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
    'cmbTxt'    => [
        'BMNo'          => 'No BM',
        'BMJenis'       => 'Jenis',
        'KodeTrans'     => 'Kode Trans',
        'PosKode'       => 'Pos Kode',
        'MotorNoPolisi' => 'No Polisi',
        'BankKode'      => 'Bank Kode',
        'BMPerson'      => 'Person',
        'BMCekNo'       => 'No Cek',
        'BMCekTempo'    => 'Cek Tempo',
        'BMAC'          => 'AC',
        'BMMemo'        => 'Keterangan',
        'NoGL'          => 'No GL',
        'Cetak'         => 'Cetak',
        'UserID'        => 'UserID',
    ],
    'cmbTgl'    => [
        'BMTgl'         => 'Tanggal BM',
    ],
    'cmbNum'    => [
        'BMNominal'     => 'Nominal',
    ],
    'sortname'  => "BMTgl",
    'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Ttbmhd::className(), 'BM SV - Servis', [
    'url_update' => Url::toRoute( [ 'ttbmhd/bank-masuk-service-invoice-update','action'=>'update']),
    'url_add'    => Url::toRoute( [ 'ttbmhd/bank-masuk-service-invoice-create', 'action' => 'create' ] ),
    'url_delete' => Url::toRoute( [ 'ttbmhd/delete' ] ),
] ), \yii\web\View::POS_READY );
