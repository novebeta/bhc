<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttbmhd */
$params                          = '&id=' . $id . '&action=create';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = 'Tambah - Bank Masuk Umum';

?>
<div class="ttbmhd-create">
    <?= $this->render('_bank-masuk-umum-form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'id'     => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/bank-masuk-umum-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail'       => Url::toRoute( [ 'ttbmitcoa/index','action' => 'create' ] ),
        ]
    ]) ?>
</div>
