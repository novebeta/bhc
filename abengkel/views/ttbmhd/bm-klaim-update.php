<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttbmhd */
$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = "Edit - $title : " . $model->BMNo;
?>
<div class="ttbmhd-update">
    <?= $this->render('_bm-klaim-form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'id'     => $id,
        'view' => $view,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/'.$view.'-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail'       => Url::toRoute( [ 'ttbmit/index','action' => 'create' ] ),
        ]
    ]) ?>
</div>
