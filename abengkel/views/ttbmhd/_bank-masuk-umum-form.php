<?php
use abengkel\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttbmhd */
/* @var $form yii\widgets\ActiveForm */
$format          = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape          = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
<div class="ttbmhd-form">
		<?php
		$form = ActiveForm::begin( ['id' => 'form_ttbmhd_id','action' => $url[ 'update' ] ] );
		\abengkel\components\TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-4", 'items' => ":LabelBMNo" ],
						      [ 'class' => "col-sm-7", 'items' => ":BMNo" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelBMTgl" ],
						      [ 'class' => "col-sm-5", 'items' => ":BMTgl" ],
						      [ 'class' => "col-sm-4", 'items' => ":BMJam", 'style' => 'padding-left:1px;' ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-2", 'items' => ":LabelJenis" ],
						      [ 'class' => "col-sm-9", 'items' => ":Jenis" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-4", 'items' => ":LabelTrans" ],
						      [ 'class' => "col-sm-8", 'items' => ":Trans" ]
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelKode" ],
					    [ 'class' => "col-sm-16", 'items' => ":Kode" ,'style' => 'padding-right:20px;'],
					    [ 'class' => "col-sm-2", 'items' => ":LabelNominal" ],
					    [ 'class' => "col-sm-4", 'items' => ":Nominal" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
						      [ 'class' => "col-sm-2", 'items' => ":LabelNoTrans" ],
						      [ 'class' => "col-sm-6", 'items' => ":NoTrans" ],
						      [ 'class' => "col-sm-2", 'items' => ":BtnSearch" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelPolisi" ],
						      [ 'class' => "col-sm-6", 'items' => ":Polisi",'style' => 'padding-right:20px;'],
						      [ 'class' => "col-sm-2", 'items' => ":LabelDiterima" ],
						      [ 'class' => "col-sm-4", 'items' => ":Diterima" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelAC" ],
					    [ 'class' => "col-sm-7", 'items' => ":AC" ],
                        [ 'class' => "col-sm-1" ],
                        [ 'class' => "col-sm-2", 'items' => ":LabelCek" ],
					    [ 'class' => "col-sm-6", 'items' => ":Cek",'style' => 'padding-right:20px;'],
					    [ 'class' => "col-sm-2", 'items' => ":LabelTglCair" ],
					    [ 'class' => "col-sm-4", 'items' => ":TglCair" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelKeterangan" ],
					    [ 'class' => "col-sm-22", 'items' => ":Keterangan" ],
				    ],
				  ],
				  [
					  'class' => "row col-md-24",
					  'style' => "margin-bottom: 6px;",
					  'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
				  ],
			  ],
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
                  ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                      'items'  => [
                          ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":JT"],
                          ['class' => "col-sm-4 pull-right", 'items' => ":LabelJT"],
                      ],
                  ],
                  [
                      'class' => "col-md-12 pull-left",
                      'items' => $this->render( '../_nav', [
                          'url'=> $_GET['r'],
                          'options'=> [
                              'BMNo'          => 'No BM',
                              'BMJenis'       => 'Jenis',
                              'KodeTrans'     => 'Kode Trans',
                              'PosKode'       => 'Pos Kode',
                              'MotorNoPolisi' => 'No Polisi',
                              'BankKode'      => 'Bank Kode',
                              'BMPerson'      => 'Person',
                              'BMCekNo'       => 'No Cek',
                              'BMCekTempo'    => 'Cek Tempo',
                              'BMAC'          => 'AC',
                              'BMMemo'        => 'Keterangan',
                              'NoGL'          => 'No GL',
                              'Cetak'         => 'Cetak',
                              'UserID'        => 'UserID',
                          ],
                      ])
                  ],
                  ['class' => "col-md-12 pull-right",
				    'items' => [
					    [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
				    ]
				  ]
			  ]
			],
			[
				":LabelBMNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No BM</label>',
				":LabelBMTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl BM</label>',
				":LabelJenis"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				":LabelTrans"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Trans</label>',
				":LabelKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelNominal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
				":LabelDiterima"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Diterima Oleh</label>',
				":LabelKode"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode</label>',
				":LabelNoTrans"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Trans</label>',
				":LabelAC"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">A/C</label>',
				":LabelPolisi"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Polisi</label>',
				":LabelCek"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Cek</label>',
				":LabelTglCair"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Cair</label>',
				":LabelJT"         => \common\components\General::labelGL( $dsTUang[ 'NoGL' ], 'JT' ),
                ":BMNo"            => Html::textInput( 'BMNoView', $dsTUang[ 'BMNoView' ], [ 'class' => 'form-control','readonly' => 'readonly', 'tabindex' => '1' ] ) .
                                      Html::textInput( 'BMNo', $dsTUang[ 'BMNo' ], [ 'class' => 'hidden' ] ),
				":BMTgl"           => FormField::dateInput( [ 'name' => 'BMTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BMTgl' ] ) ] ] ),
				":TglCair"         => FormField::dateInput( [ 'name' => 'BMCekTempo', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BMCekTempo' ] ) ] ] ),
				":BMJam"           => FormField::timeInput( [ 'name' => 'BMJam', 'config' => [ 'value' => $dsTUang[ 'BMTgl' ] ] ] ),
				":JT"              => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control','readOnly'=>true ] ),
				":Keterangan"      => Html::textInput( 'BMMemo', $dsTUang[ 'BMMemo' ], [ 'class' => 'form-control', 'maxlength' => '150' ] ),
				":Diterima"        => Html::textInput( 'BMPerson', $dsTUang[ 'BMPerson' ], [ 'class' => 'form-control', 'maxlength' => '50' ] ),
				":Nominal"         => FormField::numberInput( [ 'name' => 'BMNominal', 'config' => [ 'id' => 'BMNominal','value' => $dsTUang[ 'BMNominal' ] ] ] ),
				":Polisi"          => Html::textInput( 'MotorNoPolisi', $dsTUang[ 'MotorNoPolisi' ], [ 'class' => 'form-control', 'maxlength' => '12' ] ),
				":Cek"             => Html::textInput( 'BMCekNo', $dsTUang[ 'BMCekNo' ], [ 'class' => 'form-control', 'maxlength' => '25' ] ),
				":AC"              => Html::textInput( 'BMAC', $dsTUang[ 'BMAC' ], [ 'class' => 'form-control', 'maxlength' => '25' ] ),
				":NoTrans"         => Html::textInput( 'BMLink', $dsTUang[ 'BMLink' ], [ 'class' => 'form-control', 'readonly' => true  ] ),
				":Kode"            => FormField::combo( 'NoAccount', [ 'name' => 'BankKode', 'config' => [ 'id' => 'NoAccount','value' => $dsTUang[ 'BankKode' ], 'data'=> \abengkel\models\Traccount::find()->bank() ]] ),
				":Jenis"           => FormField::combo( 'BMJenis', [ 'config' => [ 'value' => $dsTUang[ 'BMJenis' ], 'data' => [
                                        'Tunai'        => 'Tunai',
                                        'Transfer'     => 'Transfer',
                                        'DP Transfer'  => 'DP Transfer',
                                        'Kartu Debit'  => 'Kartu Debit',
                                        'Kartu Kredit' => 'Kartu Kredit' ] ],'options' => ['tabindex'=>'3'], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":Trans"           => FormField::combo( 'KodeTrans', [ 'config' => [ 'value' => $dsTUang[ 'KodeTrans' ], 'data' => ['UM' => 'UM - Umum',] ], 'extraOptions'         => [ 'simpleCombo' => true ] ] ),
				":BtnSearch"       => '<button type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>',
			],
            [
                'url_main'         => 'ttbmhd',
                'url_id'           => $id,
                'url_delete'        => Url::toRoute(['ttbmhd/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlAdd         = \abengkel\models\Ttbmhd::getRoute('UM',['action'=>'create'] )[ 'create' ];
$urlIndex       = \abengkel\models\Ttbmhd::getRoute('UM',[ 'id' => $id ] )[ 'index' ];
$urlDetail          = $url[ 'detail' ];
$readOnly           = ( $_GET['action'] == 'view' ? 'true' : 'false' );
$DtAccount          = \common\components\General::cCmd( "SELECT NoAccount, NamaAccount, OpeningBalance, JenisAccount, StatusAccount, NoParent FROM traccount 
WHERE JenisAccount  = 'Detail' ORDER BY NoAccount" )->queryAll();
$this->registerJsVar( '__dtAccount', json_encode( $DtAccount )  );
$this->registerJsVar('__update', $_GET['action']);
$this->registerJs( <<< JS
     var __NoAccount = JSON.parse(__dtAccount);
     __NoAccount = $.map(__NoAccount, function (obj) {
                      obj.id = obj.NoAccount;
                      obj.text = obj.NoAccount;
                      return obj;
                    });
     var __NamaAccount = JSON.parse(__dtAccount);
     __NamaAccount = $.map(__NamaAccount, function (obj) {
                      obj.id = obj.NamaAccount;
                      obj.text = obj.NamaAccount;
                      return obj;
                    });
     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 190,
        extraParams: {
            BMNo: $('input[name="BMNo"]').val()
        },
        postData: {
            BMNo: $('input[name="BMNo"]').val()
        },
        rownumbers: true,  
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,
        colModel: [
            { template: 'actions'  },
            {
                name: 'NoAccount',
                label: 'Kode',
                editable: true,
                width: 170,
                editor: {
                    type: 'select2',
                    data: __NoAccount,  
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowid = $(this).attr('rowid');
                            $('[id="'+rowid+'_'+'NamaAccount'+'"]').val(data.NamaAccount); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'NamaAccount'+'"]').trigger('change');
						}
                    }                  
                }
            },
            {
                name: 'NamaAccount',
                label: 'Nama',
                width: 250,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __NamaAccount, 
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowid = $(this).attr('rowid');
                            $('[id="'+rowid+'_'+'NoAccount'+'"]').val(data.NoAccount); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'NoAccount'+'"]').trigger('change');
						}
                    }                              
                }
            },
            {
                name: 'BMKeterangan',
                label: 'Keterangan',
                width: 310,
                editable: true,
            },
            {
                name: 'BMBayar',
                label: 'Kredit',
                width: 200,
                editable: true,
                template: 'money'
            },
            {
                name: 'BMAutoN',
                label: 'BMAutoN',
                width: 210,
                editable: true,
                hidden: true
            },
        ],  
        listeners: {
            afterLoad: function(){
                let TotalBMBayar = $('#detailGrid').jqGrid('getCol','BMBayar',false,'sum');
                $("#detailGrid-pager_right").html('<label class="control-label" style="margin-right:10px;">Sub Total COA </label>' +
                 '<label style="width:190px;margin-right:28px;">'+TotalBMBayar.toLocaleString("id-ID", {minimumFractionDigits:2})+'</label>');
            }
        },             
     }).init().fit($('.box-body')).load({url: '$urlDetail'});
     
	$('#btnSave').click(function (event) {	    
	    let TotalBMBayar = numUtils.round($('#detailGrid').jqGrid('getCol','BMBayar',false,'sum'),2);
	    // let Nominal = parseFloat($('#BMNominal-disp').inputmask('unmaskedvalue'));
	    let Nominal = numUtils.round($('#BMNominal').utilNumberControl().val(),2);
           if (Nominal === 0 ){
               bootbox.alert({message:'Nominal tidak boleh 0', size: 'small'});
               return;
        }
        if (TotalBMBayar !== Nominal){
            bootbox.alert({ message: "Jumlah nominal dengan subtotal coa berbeda.", size: 'small'});
            return;
            }
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttbmhd_id').attr('action','{$url['update']}');
        $('#form_ttbmhd_id').submit();
    }); 
	$('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  });
	
	$('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
	
	$('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
	
	$('#detailGrid').bind("jqGridInlineEditRow", function (e, rowId, orgClickEvent) {
        
        if (orgClickEvent.extraparam.oper === 'add'){
            var BMKeterangan = $('[id="'+rowId+'_'+'BMKeterangan'+'"]');
            BMKeterangan.val($('[name="BMMemo"]').val());
        }        
    });

	$('[name=BMTgl]').utilDateControl().cmp().attr('tabindex', '2');
	
	//enable kode saat edit request 07/06/2024
	/*$( document ).ready(function() {
        if(__update == 'update'){
            $('#NoAccount').attr('disabled',true);
        }
    });*/
	
JS
);
