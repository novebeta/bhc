<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttbmhd */
$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/'.$view );
$this->title                     =  "Edit - $title : " . $dsTUang['BMNoView'];
?>
<div class="ttbmhd-update">
    <?= $this->render('_form', [
        'model'     => $model,
        'dsTUang'   => $dsTUang,
        'id'        => $id,
        'view'      => $view,
        'title'     => $title,
        'url'       => [
                        'update' => Custom::url( \Yii::$app->controller->id . '/'.$view .'-update'. $params ),
                        'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
                        'cancel' => $cancel,
                        'detail' => Url::toRoute( [ 'ttbmit/index','action' => 'create' ] ),
        ]
    ]) ?>
</div>
