<?php
use abengkel\assets\AppAsset;
use abengkel\components\FormField;
use aunit\components\TUi;
use common\components\Custom;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttbmhd */
/* @var $form yii\widgets\ActiveForm */
$url[ 'cancel' ] = Url::toRoute( 'ttbkhd/transfer-antar-bank' );
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttpihd-form">
		<?php
		$form = ActiveForm::begin( ['id' => 'form_ttbmhd_id','action' => $url[ 'update' ],   ] );
		TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-4", 'items' => ":LabelPINo" ],
						      [ 'class' => "col-sm-7", 'items' => ":PINo" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelPITgl" ],
						      [ 'class' => "col-sm-4", 'items' => ":PITgl" ],
						      [ 'class' => "col-sm-5", 'items' => ":PIJam", 'style' => 'padding-left:1px;' ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-2", 'items' => ":LabelJenis" ],
						      [ 'class' => "col-sm-9", 'items' => ":Jenis" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelTrans" ],
						      [ 'class' => "col-sm-10", 'items' => ":Trans" ]
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelKode" ],
					    [ 'class' => "col-sm-14", 'items' => ":Kode" ],
					    [ 'class' => "col-sm-2", ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelNominal" ],
					    [ 'class' => "col-sm-4", 'items' => ":Nominal" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-4", 'items' => ":LabelNoTrans" ],
						      [ 'class' => "col-sm-8", 'items' => ":NoTrans" ],
						      [ 'class' => "col-sm-2", 'items' => ":BtnSearch" ],
						      [ 'class' => "col-sm-3", 'items' => ":LabelPolisi" ],
						      [ 'class' => "col-sm-7", 'items' => ":Polisi" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-4", 'items' => ":LabelDiterima" ],
						      [ 'class' => "col-sm-19", 'items' => ":Diterima" ],
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelAC" ],
					    [ 'class' => "col-sm-7", 'items' => ":AC" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-1", 'items' => ":LabelCek" ],
					    [ 'class' => "col-sm-7", 'items' => ":Cek" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelTglCair" ],
					    [ 'class' => "col-sm-3", 'items' => ":TglCair" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelKeterangan" ],
					    [ 'class' => "col-sm-22", 'items' => ":Keterangan" ],
				    ],
				  ],
			  ],
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
                  ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                      'items'  => [
                          ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":JT"],
                          ['class' => "col-sm-4 pull-right", 'items' => ":LabelJT"],
                      ],
                  ],
                  [
                      'class' => "col-md-12 pull-left",
                      'items' => $this->render( '../_nav', [
                          'url'=> $_GET['r'],
                          'options'=> [
                              'BMNo'          => 'No BM',
                              'BMJenis'       => 'Jenis',
                              'KodeTrans'     => 'Kode Trans',
                              'PosKode'       => 'Pos Kode',
                              'MotorNoPolisi' => 'No Polisi',
                              'BankKode'      => 'Bank Kode',
                              'BMPerson'      => 'Person',
                              'BMCekNo'       => 'No Cek',
                              'BMCekTempo'    => 'Cek Tempo',
                              'BMAC'          => 'AC',
                              'BMMemo'        => 'Keterangan',
                              'NoGL'          => 'No GL',
                              'Cetak'         => 'Cetak',
                              'UserID'        => 'UserID',
                          ],
                      ])
                  ],
                  ['class' => "col-md-12 pull-right",
				    'items' => [
					    [ 'class' => "pull-right", 'items' => ":btnJurnal :btnSave :btnPrint :btnCancel" ]
				    ]
				  ]
			  ]
			],
			[
				":LabelPINo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No BM</label>',
				":LabelPITgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl BM</label>',
				":LabelKK"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">No KK</label>',
				":LabelKM"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">No KM</label>',
				":LabelJenis"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				":LabelTrans"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Trans</label>',
				":LabelJTKK"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">JT KK</label>',
				":LabelJTKM"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">JT KM</label>',
				":LabelAsal"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kas Asal</label>',
				":LabelTujuan"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kas Tujuan</label>',
				":LabelKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelNominal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
				":LabelDiterima"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Diterima Dari</label>',
				":LabelDiserahkan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Diserahkan Oleh</label>',
				":LabelKode"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode</label>',
				":LabelNoTrans"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Transaksi</label>',
				":LabelAC"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">A/C</label>',
				":LabelPolisi"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Polisi</label>',
				":LabelCek"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Cek</label>',
				":LabelTglCair"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Cair</label>',
				":LabelJT"         => \common\components\General::labelGL( $dsBeli[ 'NoGL' ], 'JT' ),
				":LabelCOA"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Chart Of Account</label>',
				":LabelSubCOA"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sub Total COA</label>',
				":PINo"            => '<input type="text"  class="form-control" name="">',
				":KK"              => '<input type="text"  class="form-control" name="">',
				":KM"              => '<input type="text"  class="form-control" name="">',
				":PITgl"           => FormField::dateInput( [ 'name' => 'SOTgl', 'config' => [ 'value' => Yii::$app->formatter->asDate( 'now' ) ] ] ),
				":TglCair"         => FormField::dateInput( [ 'name' => 'TglCair', 'config' => [ 'value' => Yii::$app->formatter->asDate( 'now' ) ] ] ),
				":PIJam"           => FormField::timeInput( [ 'name' => 'SOJam' ] ),
				":Trans"           => '<input type="text"  class="form-control" name="">',
				":Jenis"           => '<input type="text"  class="form-control" name="">',
				":JTKK"            => '<input type="text"  class="form-control" name="">',
				":JTKM"            => '<input type="text"  class="form-control" name="">',
				":Diserahkan"      => '<input type="text"  class="form-control" name="">',
				":Asal"            => FormField::combo( 'NoAccount', [ 'config' => [ 'value' => 'NoAccount' ], 'extraOptions' => [ 'altLabel' => [ 'NamaAccount' ] ] ] ),
				":Tujuan"          => FormField::combo( 'NoAccount', [ 'config' => [ 'value' => 'NoAccount' ], 'extraOptions' => [ 'altLabel' => [ 'NamaAccount' ] ] ] ),
				":Keterangan"      => '<input type="text"  class="form-control" name="">',
				":Diterima"        => '<input type="text"  class="form-control" name="">',
				":Nominal"         => '<input type="text"  class="form-control" name="">',
				":Kode"            => FormField::combo( 'NoAccount', [ 'config' => [ 'value' => 'NoAccount' ], 'extraOptions' => [ 'altLabel' => [ 'NamaAccount' ] ] ] ),
				":NoTrans"         => '<input type="text"  class="form-control" name="">',
				":AC"              => '<input type="text"  class="form-control" name="">',
				":Polisi"          => '<input type="text"  class="form-control" name="">',
				":Cek"             => '<input type="text"  class="form-control" name="">',
				":JT"              => '<input type="text"  class="form-control" name="">',
				":SubCOA"          => '<input type="text"  class="form-control" name="">',
				":BtnSearch"       => '<button type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>',
				":BtnInfo"         => '<button type="button" class="btn btn-default"><i class="fa fa-refresh fa-lg"></i></button>',
				":btnSave"    => Html::button( '<i class="glyphicon glyphicon-floppy-disk"></i> Simpan', [ 'class' => 'btn btn-info btn-primary ', 'id' => 'btnSave' . Custom::viewClass() ] ),
				":btnCancel"  => Html::Button( '<i class="fa fa-ban"></i> Batal', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:80px' ] ),
				":btnAdd"     => Html::button( '<i class="fa fa-plus-circle"></i> Tambah', [ 'class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:80px' ] ),
				":btnEdit"    => Html::button( '<i class="fa fa-edit"></i> Edit', [ 'class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:80px' ] ),
				":btnDaftar"  => Html::Button( '<i class="fa fa-list"></i> Daftar ', [ 'class' => 'btn btn-primary ', 'id' => 'btnDaftar', 'style' => 'width:80px' ] ),
				":btnPrint"   => Html::button( '<i class="glyphicon glyphicon-print"></i>  Print ', [ 'class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:80px' ] ),
				":btnJurnal"  => Html::button( '<i class="fa fa-balance-scale"></i>  Jurnal ', [ 'class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:80px' ] ),
			]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlPrint                = $url[ 'print' ];
$this->registerJs( <<< JS
    $('#btnPrint').click(function (event) {	      
        $('#form_ttbmhd_id').attr('action','$urlPrint');
        $('#form_ttbmhd_id').attr('target','_blank');
        $('#form_ttbmhd_id').submit();
	  });

    $('#btnCancel').click(function (event) {	      
       $('#form_ttbmhd_id').attr('action','{$url['cancel']}');
       $('#form_ttbmhd_id').submit();
    });
	
	$('#btnSave').click(function (event) {	      
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttbmhd_id').attr('action','{$url['update']}');
        $('#form_ttbmhd_id').submit();
    }); 

JS
);

