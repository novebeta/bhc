<?php
use abengkel\components\FormField;
use yii\bootstrap\Html;
use common\components\Custom;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\widgets\TimePicker;
use common\components\General;
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="bayar-form">
    <?php
    $altCondition = "LEFT(NoAccount,5) = '11010'  AND JenisAccount = 'Detail'";
    $form = ActiveForm::begin( [  ] );
    \abengkel\components\TUi::form(
        [ 'class' => "row-no-gutters",
            'items' => [
                [ 'class' => "form-group col-md-24",
                    'items' => [
                        [ 'class' => "col-xs-8"],
                        [ 'class' => "col-xs-16", 'items' => ":LabelPembayaran" ],
                    ],
                ],
                [ 'class' => "form-group col-md-24",
                    'items' => [
                        [ 'class' => "col-md-5 col-sm-5 col-lg-5 col-xs-5", 'items' => ":LabelNomor" ],
                        [ 'class' => "col-md-8 col-sm-8 col-lg-8 col-xs-8", 'items' => ":Nomor" ],
                        [ 'class' => "col-md-1 col-sm-1 col-lg-2 col-xs-1"],
                        [ 'class' => "col-md-4 col-sm-4 col-lg-4 col-xs-4", 'items' => ":LabelTgl" ],
                        [ 'class' => "col-md-6 col-sm-6 col-lg-6 col-xs-6", 'items' => ":Tgl" ],
                    ],
                ],
                [ 'class' => "form-group col-md-24",
                    'items' => [
                        [ 'class' => "col-lg-24 col-xs-24", 'items' => "<p>"],
                    ]
                ],

                [ 'class' => "form-group col-md-24",
                    'items' => [
                        [ 'class' => "col-md-5 col-sm-5 col-lg-5 col-xs-5", 'items' => ":LabelCaraBayar" ],
                        [ 'class' => "col-md-8 col-sm-8 col-lg-8 col-xs-8", 'items' => ":CaraBayar" ],
                        [ 'class' => "col-md-1 col-sm-1 col-lg-2 col-xs-1"],
                        [ 'class' => "col-md-4 col-sm-4 col-lg-4 col-xs-4", 'items' => ":LabelJam" ],
                        [ 'class' => "col-md-6 col-sm-6 col-lg-6 col-xs-6", 'items' => ":Jam" ],
                    ],
                ],
                [ 'class' => "form-group col-md-24",
                    'items' => [
                        [ 'class' => "col-lg-24 col-xs-24", 'items' => "<p>"],
                    ]
                ],
                [ 'class' => "form-group col-md-24",
                    'items' => [
                        [ 'class' => "col-md-5 col-sm-5 col-lg-5 col-xs-5", 'items' => ":LabelCOA" ],
                        [ 'class' => "col-md-19 col-sm-19 col-lg-19 col-xs-19", 'items' => ":COA" ],
                    ],
                ],
                [ 'class' => "form-group col-md-24",
                    'items' => [
                        [ 'class' => "col-lg-24 col-xs-24", 'items' => "<p>"],
                    ]
                ],
                [ 'class' => "form-group col-md-24",
                    'items' => [
                        [ 'class' => "col-md-5 col-sm-5 col-lg-5 col-xs-5", 'items' => ":LabelKartu" ],
                        [ 'class' => "col-md-19 col-sm-19 col-lg-19 col-xs-19", 'items' => ":Kartu" ],
                    ],
                ],
                [ 'class' => "form-group col-md-24",
                    'items' => [
                        [ 'class' => "col-lg-24 col-xs-24", 'items' => "<p>"],
                    ]
                ],
                [ 'class' => "form-group col-md-24",
                    'items' => [
                        [ 'class' => "col-md-5 col-sm-5 col-lg-5 col-xs-5", 'items' => ":LabelKeterangan" ],
                        [ 'class' => "col-md-19 col-sm-19 col-lg-19 col-xs-19", 'items' => ":Keterangan" ],
                    ],
                ],
                [ 'class' => "form-group col-md-24",
                    'items' => [
                        [ 'class' => "col-lg-24 col-xs-24", 'items' => "<p>"],
                    ]
                ],
                [ 'class' => "form-group col-md-24",
                    'items' => [
                        [ 'class' => "col-md-5 col-sm-5 col-lg-5 col-xs-5", 'items' => ":LabelNominal" ],
                        [ 'class' => "col-md-19 col-sm-19 col-lg-19 col-xs-19", 'items' => ":Nominal" ],
                    ],
                ],
                [ 'class' => "form-group col-md-24",
                    'items' => [
                        [ 'class' => "col-lg-24 col-xs-24", 'items' => "<p>"],
                    ]
                ],
                [ 'class' => "form-group col-md-24",
                    'items' => [
                        [ 'class' => "col-md-5 col-sm-5 col-lg-5 col-xs-5", 'items' => ":LabelByr" ],
                        [ 'class' => "col-md-19 col-sm-19 col-lg-19 col-xs-19", 'items' => ":Byr" ],
                    ],
                ],
                [ 'class' => "form-group col-md-24",
                    'items' => [
                        [ 'class' => "col-lg-24 col-xs-24", 'items' => "<p>"],
                    ]
                ],
                [ 'class' => "form-group col-md-24",
                    'items' => [
                        [ 'class' => "col-md-5 col-sm-5 col-lg-5 col-xs-5", 'items' => ":LabelKembali" ],
                        [ 'class' => "col-md-19 col-sm-19 col-lg-19 col-xs-19", 'items' => ":Kembali" ],
                    ],
                ],
            ]
        ],
        [ 'class' => "row-no-gutters",
            'items' => [
                [ 'class' => "col-md-24",
                    'items' => [
                        [ 'class' => "pull-left", 'items' => ":btnPrint " ],
                        [ 'class' => "pull-right", 'items' => ":btnSave :btnCancel" ],
                    ]
                ]
            ]
        ],
        [
            ":LabelPembayaran"  => '<label class="control-label" style="margin: 0; padding: 8px 0;font-size: 24px;text-align: justify-all">Pembayaran</label>',
            ":LabelNomor"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor</label>',
            ":LabelTgl"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tanggal</label>',
            ":LabelJam"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jam</label>',
            ":LabelCaraBayar"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Cara Bayar</label>',
            ":LabelCOA"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode</label>',
            ":LabelKartu"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Kartu</label>',
            ":LabelKeterangan"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
            ":LabelNominal"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
            ":LabelByr"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bayar</label>',
            ":LabelKembali"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kembali</label>',
            ":Padding"          => '<label class="control-label" style="margin: 0; padding: -6px 0;"> </label>',

            ":Nomor"            => Html::textInput( 'BMNoView', $dsTUang[ 'BMNoView' ], [ 'class' => 'form-control','readonly' => 'readonly' ] ) .
                                    Html::textInput( 'BMNo', $dsTUang[ 'BMNo' ], [ 'class' => 'hidden' ] ),
            ":CaraBayar"        => FormField::combo( 'CaraBayar', [ 'config' => [ 'id' => 'CaraBayar', 'value' => $dsTUang[ 'CaraBayar' ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
            ":COA"              => FormField::combo( 'NoAccount', [ 'config' => [ 'value' => $dsTUang[ 'BankKode' ] ], 'extraOptions' => [ 'altLabel' => [ 'NamaAccount' ], 'altCondition'=> $altCondition,] ] ),

            ":Kartu"            => Html::textInput( 'NoKartu',  $dsTUang[ 'NoKartu' ], [ 'class' => 'form-control' ] ),
            ":Keterangan"       => Html::textarea( 'Keterangan', '', [ 'class' => 'form-control' ] ),
            ":Tgl"              => FormField::dateInput( [ 'name' => 'BMTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BMTgl' ] ) ] ] ),
            ":Jam"              => FormField::timeInput( [ 'name' => 'BMJam', 'config' => [ 'value' => $dsTUang[ 'BMTgl' ] ] ] ),


            ":Nominal"          => FormField::numberInput( [ 'name' => 'BMNominal', 'config' => [ 'id' => 'BMNominal','value' => $dsTUang[ 'BMNominal' ], 'readonly' => true ] ] ),
            ":Byr"              => FormField::numberInput( [ 'name' => 'Bayar', 'config' => [ 'id' => 'BMBayar', ] ] ),
            ":Kembali"          => FormField::numberInput( [ 'name' => 'Kembali', 'config' => [ 'id' => 'BMKembali','readonly' => 'readonly' ] ] ),

            ":btnSave"    => Html::button( '<i class="glyphicon glyphicon-floppy-disk"></i> Simpan', [ 'class' => 'btn btn-info btn-primary ', 'id' => 'btnSave' . Custom::viewClass() ] ),
            ":btnCancel"  => Html::Button( '<i class="fa fa-ban"></i> Batal', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:80px' ] ),
            ":btnAdd"     => Html::button( '<i class="fa fa-plus-circle"></i> Tambah', [ 'class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:80px' ] ),
            ":btnEdit"    => Html::button( '<i class="fa fa-edit"></i> Edit', [ 'class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:80px' ] ),
            ":btnDaftar"  => Html::Button( '<i class="fa fa-list"></i> Daftar ', [ 'class' => 'btn btn-primary ', 'id' => 'btnDaftar', 'style' => 'width:80px' ] ),
            ":btnPrint"   => Html::button( '<i class="glyphicon glyphicon-print"></i>  Print ', [ 'class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:80px' ] ),
            ":btnJurnal"  => Html::button( '<i class="fa fa-balance-scale"></i>  Jurnal ', [ 'class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:80px' ] ),

        ]
    );
    ActiveForm::end();
    ?>
</div>
<?php
if ( $dsTUang[ 'CaraBayar' ] != 'Tunai')
{
    $altCondition = " LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'";
}
$this->registerJs( <<< JS
    function ChangeCaraBayar(){
         	let CaraBayar = parseFloat($('#CaraBayar').val());                
         	$('#CaraBayar').utilNumberControl().val(CaraBayar);
         }         
         window.ChangeCaraBayar = ChangeCaraBayar; 

    function HitungBawah(){
         	let BMBayar = parseFloat($('#BMBayar').val());                 
         	let BMNominal = parseFloat($('#BMNominal').val());      
         	let BMKembali = BMBayar - BMNominal;                 
         	$('#BMKembali').utilNumberControl().val(BMKembali);
         }         
         window.HitungBawah = HitungBawah;

    $('#BMBayar').change(function (event) {
	      HitungBawah();
	  }); 
    
    $('#CaraBayar').change(function (event) {
	      ChangeCaraBayar();
	  }); 
    
JS
);