<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttbmhd */

$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = 'Edit - Bank Masuk Dari H1 : ' . $dsTUang['BMNoView'];
?>
<div class="ttbmhd-update">
    <?= $this->render('_bank-masuk-dari-h1-form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'id' => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/bank-masuk-dari-h1-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
        ]
    ]) ?>
</div>
