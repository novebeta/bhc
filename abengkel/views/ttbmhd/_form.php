<?php
use abengkel\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttbmhd */
/* @var $form yii\widgets\ActiveForm */
$format          = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape          = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttbmhd-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_ttbmhd_id','action' => $url[ 'update' ] ] );
		\abengkel\components\TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-2", 'items' => ":LabelBMNo" ],
						      [ 'class' => "col-sm-7", 'items' => ":BMNo" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelBMTgl" ],
						      [ 'class' => "col-sm-4", 'items' => ":BMTgl" ],
						      [ 'class' => "col-sm-4", 'items' => ":BMJam", 'style' => 'padding-left:1px;' ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-2", 'items' => ":LabelJenis" ],
						      [ 'class' => "col-sm-9", 'items' => ":Jenis" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelTrans" ],
						      [ 'class' => "col-sm-10", 'items' => ":Trans" ]
					      ],
					    ],
				    ],
				  ],
				  [
					  'class' => "row col-md-24",
					  'style' => "margin-bottom: 6px;",
					  'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelKode" ],
					    [ 'class' => "col-sm-14", 'items' => ":Kode" ],
					    [ 'class' => "col-sm-1", ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelNominal" ],
					    [ 'class' => "col-sm-5", 'items' => ":Nominal" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
						      [ 'class' => "col-sm-2", 'items' => ":LabelNoTrans" ],
						      [ 'class' => "col-sm-5", 'items' => ":NoTrans" ],
						      [ 'class' => "col-sm-2", 'items' => ":BtnSearch" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelPolisi" ],
						      [ 'class' => "col-sm-5", 'items' => ":Polisi" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelDiterima" ],
						      [ 'class' => "col-sm-5", 'items' => ":Diterima" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelAC" ],
					    [ 'class' => "col-sm-6", 'items' => ":AC" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelCek" ],
					    [ 'class' => "col-sm-5", 'items' => ":Cek" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelTglCair" ],
					    [ 'class' => "col-sm-5", 'items' => ":TglCair" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelKeterangan" ],
					    [ 'class' => "col-sm-22", 'items' => ":Keterangan" ],
				    ],
				  ],
			  ],
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
                  ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                      'items'  => [
                          ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":JT"],
                          ['class' => "col-sm-4 pull-right", 'items' => ":LabelJT"],
                      ],
                  ],
                  [
                      'class' => "col-md-12 pull-left",
                      'items' => $this->render( '../_nav', [
                          'url'=> $_GET['r'],
                          'options'=> [
                              'BMNo'          => 'No BM',
                              'BMJenis'       => 'Jenis',
                              'KodeTrans'     => 'Kode Trans',
                              'PosKode'       => 'Pos Kode',
                              'MotorNoPolisi' => 'No Polisi',
                              'BankKode'      => 'Bank Kode',
                              'BMPerson'      => 'Person',
                              'BMCekNo'       => 'No Cek',
                              'BMCekTempo'    => 'Cek Tempo',
                              'BMAC'          => 'AC',
                              'BMMemo'        => 'Keterangan',
                              'NoGL'          => 'No GL',
                              'Cetak'         => 'Cetak',
                              'UserID'        => 'UserID',
                          ],
                      ])
                  ],
                  ['class' => "col-md-12 pull-right",
				    'items' => [
					    [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
				    ]
				  ]
			  ]
			],
			[
				":LabelBMNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No BM</label>',
				":LabelBMTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl BM</label>',
				":LabelJenis"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				":LabelNominal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
				":LabelDiterima"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Diterima Oleh</label>',
				":LabelKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelJT"         => \common\components\General::labelGL( $dsTUang[ 'NoGL' ], 'JT' ),
				":LabelKode"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode</label>',
				":LabelAC"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">A/C</label>',
				":LabelPolisi"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Polisi</label>',
				":LabelNoTrans"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Transaksi</label>',
				":LabelTrans"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Trans</label>',
				":LabelCek"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Cek</label>',
				":LabelTglCair"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Cair</label>',

                ":BMNo"            => Html::textInput( 'BMNoView', $dsTUang[ 'BMNoView' ], [ 'class' => 'form-control','readonly' => 'readonly', 'tabindex' => '1' ] ) .
                                      Html::textInput( 'BMNo', $dsTUang[ 'BMNo' ], [ 'class' => 'hidden' ] ),
				":BMTgl"           => FormField::dateInput( [ 'name' => 'BMTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BMTgl' ] ) ] ] ),
				":TglCair"         => FormField::dateInput( [ 'name' => 'BMCekTempo', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BMCekTempo' ] ) ] ] ),
				":BMJam"           => FormField::timeInput( [ 'name' => 'BMJam', 'config' => [ 'value' => $dsTUang[ 'BMTgl' ] ] ] ),
				":JT"              => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control' ,'readOnly'=>true] ),
				":Keterangan"      => Html::textInput( 'BMMemo', $dsTUang[ 'BMMemo' ], [ 'class' => 'form-control', 'maxlength' => '150' ] ),
				":Diterima"        => Html::textInput( 'BMPerson', $dsTUang[ 'BMPerson' ], [ 'class' => 'form-control', 'maxlength' => '50' ] ),
				":Nominal"         => FormField::numberInput( [ 'name' => 'BMNominal', 'config' => [ 'value' => $dsTUang[ 'BMNominal' ] ] ] ),
				":Polisi"          => Html::textInput( 'MotorNoPolisi', $dsTUang[ 'MotorNoPolisi' ], [ 'class' => 'form-control', 'maxlength' => '12' ] ),
				":Cek"             => Html::textInput( 'BMCekNo', $dsTUang[ 'BMCekNo' ], [ 'class' => 'form-control', 'maxlength' => '25' ] ),
				":AC"              => Html::textInput( 'BMAC', $dsTUang[ 'BMAC' ], [ 'class' => 'form-control', 'maxlength' => '25' ] ),
				":NoTrans"         => Html::textInput( 'BMLink', $dsTUang[ 'BMLink' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Kode"            => FormField::combo( 'NoAccount', [ 'name' => 'BankKode','config' => [  'id' => 'NoAccount','value' => $dsTUang[ 'BankKode' ], 'data'=> \abengkel\models\Traccount::find()->bank() ] ] ),
				":Jenis"           => FormField::combo( 'BMJenis', [ 'config' => [ 'value' => $dsTUang[ 'BMJenis' ], 'data' => [
                                        'Tunai'        => 'Tunai',
                                        'Transfer'     => 'Transfer',
                                        'DP Transfer'  => 'DP Transfer',
                                        'Kartu Debit'  => 'Kartu Debit',
                                        'Kartu Kredit' => 'Kartu Kredit' ] ],'options' => ['tabindex'=>'3'], 'extraOptions'      => [ 'simpleCombo' => true ] ] ),
				":Trans"           => FormField::combo( 'KodeTrans', [ 'config' => [ 'value' => $dsTUang[ 'KodeTrans' ], 'data' => [
                                        'UM' => 'UM - Umum',
                                        'KK' => 'KK - BM Dari Kas',
                                        'SV' => 'SV - Servis',
                                        'SO' => 'SO - Order Penjualan',
                                        'SI' => 'SI - Penjualan',
                                        'PR' => 'PR - Retur Pembelian',
                                        'CK' => 'CK - Klaim KPB',
                                        'CC' => 'CC  - Klaim C2',
                                        'H1' => 'H1  - BM Dari H1',
                                        'SE' => 'SE - Service Estimation'
                ] ], 'extraOptions'              => [ 'simpleCombo' => true ] ] ),
				":BtnSearch"       => '<button type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>',
			],
            [
                'url_main'         => 'ttbmhd',
                'url_id'           => $id,
                'url_delete'        => Url::toRoute(['ttbmhd/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlAdd         = \abengkel\models\Ttbmhd::getRoute($dsTUang[ 'KodeTrans' ],['action'=>'create'] )[ 'create' ];
$urlIndex       = \abengkel\models\Ttbmhd::getRoute($dsTUang[ 'KodeTrans' ],[ 'id' => $id ] )[ 'index' ];
$urlDetail      = $url[ 'detail' ];
$readOnly       = ( $_GET['action'] == 'view' ? 'true' : 'false' );
$urlLoop        = Url::toRoute(['ttbmit/index']);
$urlPopup       = Url::toRoute( [ 'ttbmhd/order', 'KodeTrans' => $dsTUang[ 'KodeTrans' ]] );
switch ($dsTUang[ 'KodeTrans' ]){
    case 'SV':
        $title = 'Daftar Servis Invoice untuk Bank Masuk Nomor : ';
        break;
    case 'SI':
        $title = 'Daftar Sales Invoice untuk Bank Masuk Nomor : ';
        break;
    case 'SE':
        $title = 'Daftar Service Estimation untuk Kas Masuk Nomor : ';
        break;
    case 'SO':
        $title = 'Daftar Sales Order untuk Bank Masuk Nomor : ';
        break;
    case 'PR':
        $title = 'Daftar Purchase Retur untuk Bank Masuk Nomor : ';
        break;
}
$this->registerJsVar('__update', $_GET['action']);
$this->registerJs( <<< JS
     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 225,
        navButtonTambah: false,
        extraParams: {
            BMNo: $('input[name="BMNo"]').val()
        },
        postData: {
            BMNo: $('input[name="BMNo"]').val()
        },
        rownumbers: true,  
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,      
        colModel: [
            { template: 'actions'  },
            {
                name: 'BMLink',
                label: 'No',
                width: 100,
                editable: true,
                editor: {
                    type: 'text',
                    readOnly:true
                }
            },
            {
                name: 'TglLink',
                label: 'Tanggal',
                width: 130,
                editable: false,
            },
            {
                name: 'BiayaNama',
                label: 'Kode',
                editable: false,
                width: 150,
            },     
            {
                name: 'CusNama',
                label: 'Nama',
                editable: false,
                width: 150,
            },      
            {
                name: 'Total',
                label: 'Total',
                editable: false,
                width: 100,
                template: 'money',
                editor: {
                    readOnly:true
                }
            },   
            {
                name: 'Terbayar',
                label: 'Terbayar',
                editable: false,
                width: 100,
                template: 'money',
                editor: {
                    readOnly:true
                }
            },     
            {
                name: 'BMBayar',
                label: 'Bayar',
                width: 100,
                editable: true,
                template: 'money',
                editrules:{
                    custom:true, 
                    custom_func: function(value) {
                        var BMBayar = parseFloat(window.util.number.getValueOfEuropeanNumber(value));
                        var Total = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Total'+'"]').val()));
                        var Terbayar = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Terbayar'+'"]').val()));
                        if (BMBayar === 0) 
                            return [false,"Silahkan mengisi bayar."];  
                        else {          
                            var sisa = Total - Terbayar;
                            if (BMBayar > sisa){
                                $('[id="'+window.cmp.rowid+'_'+'BMBayar'+'"]').val(sisa);
                               return [false,"Nilai Bayar maksimal : " + window.util.number.format(sisa)];   
                            }else{
                                return [true,""];  
                            }
                        }
                    }
                }
            },
            {
                name: 'Sisa',
                label: 'Sisa',
                width: 100,
                editable: true,
                template: 'money',
                editor: {
                    readOnly:true
                }
            },
        ],      
     }).init().fit($('.box-body')).load({url: '$urlDetail'}); 

     $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
        window.cmp.rowid = rowid;    
    });

    $('#detailGrid').jqGrid('navButtonAdd',"#detailGrid-pager",{ 
        caption: ' Tambah',
        buttonicon: 'glyphicon glyphicon-plus',
        onClickButton:  function() {
            window.util.app.dialogListData.closeTriggerFromIframe = function(){ 
                var iframe = window.util.app.dialogListData.iframe();
                var mainGrid = iframe.contentWindow.jQuery('#jqGrid').jqGrid();
                window.util.app.dialogListData.gridFn.mainGrid = mainGrid;
                window.util.app.dialogListData.gridFn.var.multiSelect = true;
                var selRowId = window.util.app.dialogListData.gridFn.getSelectedRow();
                console.log(selRowId);    
                $.ajax({
                     type: "POST",
                     url: '$urlLoop',
                     data: {
                         oper: 'batch',
                         data: selRowId,
                         header: btoa($('#form_ttbmhd_id').serialize())
                     },
                     success: function(data) {
                        mainGrid.jqGrid().trigger('reloadGrid');
                     },
                   });
                return false;
            }
            window.util.app.dialogListData.show({
                title: '$title {$dsTUang[ "BMNoView" ]}',
                url: '$urlPopup'
            },function(){ 
                window.location.href = "{$url['update']}&oper=skip-load&BMNoView={$dsTUang['BMNoView']}"; 
            });
        }, 
        position: "last", 
        title:"", 
        cursor: "pointer"
    });
	
	$('#btnSave').click(function (event) {	 
	    let Nominal = parseFloat($('#BMNominal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Nominal tidak boleh 0', size: 'small'});
               return;
        }
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttbmhd_id').attr('action','{$url['update']}');
        $('#NoAccount').attr('disabled',false);
        $('#form_ttbmhd_id').submit();
    });  
	
	$('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  });
	
	$('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
	
	$('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
	
	$('[name=BMTgl]').utilDateControl().cmp().attr('tabindex', '2');
	//enable kode saat edit request 07/06/2024
	/*
	$( document ).ready(function() {
        if(__update == 'update'){
            $('#NoAccount').attr('disabled',true);
        }
    });*/
JS
);