<?php
use abengkel\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttbmhd */
/* @var $form yii\widgets\ActiveForm */
$format          = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape          = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
<div class="ttbmhd-form">
	<?php
	$form = ActiveForm::begin( ['id' => 'form_ttbmhd_id','action' => $url[ 'update' ] ] );
	\abengkel\components\TUi::form(
		[ 'class' => "row-no-gutters",
		  'items' => [
			  [ 'class' => "form-group col-md-24",
			    'items' => [
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-4", 'items' => ":LabelBMNo" ],
					      [ 'class' => "col-sm-6", 'items' => ":BMNo" ],
					      [ 'class' => "col-sm-1" ],
					      [ 'class' => "col-sm-2", 'items' => ":LabelBMTgl" ],
					      [ 'class' => "col-sm-4", 'items' => ":BMTgl" ],
					      [ 'class' => "col-sm-6", 'items' => ":BMJam", 'style' => 'padding-left:1px;' ],
				      ],
				    ],
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-2", 'items' => ":LabelJenis" ],
					      [ 'class' => "col-sm-8", 'items' => ":Jenis" ],
					      [ 'class' => "col-sm-2" ],
					      [ 'class' => "col-sm-4", 'items' => ":LabelTrans" ],
					      [ 'class' => "col-sm-8", 'items' => ":Trans" ]
				      ],
				    ],
			    ],
			  ],
			  [ 'class' => "form-group col-md-24",
			    'items' => [
				    [ 'class' => "col-sm-2", 'items' => ":LabelKode" ],
				    [ 'class' => "col-sm-15", 'items' => ":Kode" ],
				    [ 'class' => "col-sm-1", ],
				    [ 'class' => "col-sm-2", 'items' => ":LabelNominal" ],
				    [ 'class' => "col-sm-4", 'items' => ":Nominal" ],
			    ],
			  ],
			  [ 'class' => "form-group col-md-24",
			    'items' => [
					      [ 'class' => "col-sm-2", 'items' => ":LabelNoTrans" ],
					      [ 'class' => "col-sm-5", 'items' => ":NoTrans" ],
					      [ 'class' => "col-sm-2", 'items' => ":BtnSearch" ],
					      [ 'class' => "col-sm-2", 'items' => ":LabelPolisi" ],
					      [ 'class' => "col-sm-6", 'items' => ":Polisi" ],
					      [ 'class' => "col-sm-1" ],
					      [ 'class' => "col-sm-2", 'items' => ":LabelDiterima" ],
					      [ 'class' => "col-sm-4", 'items' => ":Diterima" ],
			    ],
			  ],
			  [ 'class' => "form-group col-md-24",
			    'items' => [
				    [ 'class' => "col-sm-2", 'items' => ":LabelAC" ],
				    [ 'class' => "col-sm-6", 'items' => ":AC" ],
				    [ 'class' => "col-sm-1" ],
				    [ 'class' => "col-sm-2", 'items' => ":LabelCek" ],
				    [ 'class' => "col-sm-6", 'items' => ":Cek" ],
				    [ 'class' => "col-sm-1" ],
				    [ 'class' => "col-sm-2", 'items' => ":LabelTglCair" ],
				    [ 'class' => "col-sm-4", 'items' => ":TglCair" ],
			    ],
			  ],
			  [ 'class' => "form-group col-md-24",
			    'items' => [
				    [ 'class' => "col-sm-2", 'items' => ":LabelKeterangan" ],
				    [ 'class' => "col-sm-22", 'items' => ":Keterangan" ],
			    ],
			  ],
		  ],
		],
		[ 'class' => "row-no-gutters",
		  'items' => [
              ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                  'items'  => [
                      ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":JT"],
                      ['class' => "col-sm-4 pull-right", 'items' => ":LabelJT"],
                  ],
              ],
              [
                  'class' => "col-md-12 pull-left",
                  'items' => $this->render( '../_nav', [
                      'url'=> $_GET['r'],
                      'options'=> [
                          'BMNo'          => 'No BM',
                          'BMJenis'       => 'Jenis',
                          'KodeTrans'     => 'Kode Trans',
                          'PosKode'       => 'Pos Kode',
                          'MotorNoPolisi' => 'No Polisi',
                          'BankKode'      => 'Bank Kode',
                          'BMPerson'      => 'Person',
                          'BMCekNo'       => 'No Cek',
                          'BMCekTempo'    => 'Cek Tempo',
                          'BMAC'          => 'AC',
                          'BMMemo'        => 'Keterangan',
                          'NoGL'          => 'No GL',
                          'Cetak'         => 'Cetak',
                          'UserID'        => 'UserID',
                      ],
                  ])
              ],
              ['class' => "col-md-12 pull-right",
			    'items' => [
				    [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
			    ]
			  ]
		  ]
		],
		[
			":LabelBMNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No BM</label>',
			":LabelBMTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl BM</label>',
			":LabelJenis"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
			":LabelNominal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
			":LabelDiterima"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Diterima Oleh</label>',
			":LabelKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
			":LabelJT"         => \common\components\General::labelGL( $dsTUang[ 'NoGL' ], 'JT' ),
			":LabelKode"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode</label>',
			":LabelAC"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">A/C</label>',
			":LabelPolisi"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Polisi</label>',
			":LabelNoTrans"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Transaksi</label>',
			":LabelTrans"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Trans</label>',
			":LabelCek"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Cek</label>',
			":LabelTglCair"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Cair</label>',
            ":BMNo"            => Html::textInput( 'BMNoView', $dsTUang[ 'BMNoView' ], [ 'class' => 'form-control','readonly' => 'readonly' , 'tabindex' => '1'] ) .
                                  Html::textInput( 'BMNo', $dsTUang[ 'BMNo' ], [ 'class' => 'hidden' ] ),
			":BMTgl"           => FormField::dateInput( [ 'name' => 'BMTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BMTgl' ] ) ] ] ),
			":BMJam"           => FormField::timeInput( [ 'name' => 'BMJam', 'config' => [ 'value' => $dsTUang[ 'BMTgl' ] ] ] ),
			":JT"              => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control','readOnly'=>true ] ),
			":Keterangan"      => Html::textInput( 'BMMemo', $dsTUang[ 'BMMemo' ], [ 'class' => 'form-control', 'maxlength' => '150' ] ),
			":Diterima"        => Html::textInput( 'BMPerson', $dsTUang[ 'BMPerson' ], [ 'class' => 'form-control', 'maxlength' => '50' ] ),
			":Nominal"         => FormField::numberInput( [ 'name' => 'BMNominal', 'config' => [ 'value' => $dsTUang[ 'BMNominal' ] ] ] ),
			":Polisi"          => Html::textInput( 'MotorNoPolisi', $dsTUang[ 'MotorNoPolisi' ], [ 'class' => 'form-control', 'maxlength' => '12' ] ),
			":Cek"             => Html::textInput( 'BMCekNo', $dsTUang[ 'BMCekNo' ], [ 'class' => 'form-control', 'maxlength' => '25' ] ),
			":AC"              => Html::textInput( 'BMAC', $dsTUang[ 'BMAC' ], [ 'class' => 'form-control', 'maxlength' => '25' ] ),
			":NoTrans"         => Html::textInput( 'BMLink', $dsTUang[ 'BMLink' ], [ 'class' => 'form-control', 'readonly' => true  ] ),
			":TglCair"         => FormField::dateInput( [ 'name' => 'BMCekTempo', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BMCekTempo' ] ) ] ] ),
			":Kode"            => FormField::combo( 'NoAccount', [ 'name' => 'BankKode', 'config' => [ 'id' => 'NoAccount','value' => $dsTUang[ 'BankKode' ], 'data'=> \abengkel\models\Traccount::find()->bank() ]] ),
			":Jenis"           => FormField::combo( 'BMJenis', [ 'config' => [ 'value' => $dsTUang[ 'BMJenis' ], 'data' => [
                                    'Tunai'        => 'Tunai',
                                    'Transfer'     => 'Transfer',
                                    'DP Transfer'  => 'DP Transfer',
                                    'Kartu Debit'  => 'Kartu Debit',
                                    'Kartu Kredit' => 'Kartu Kredit' ] ], 'options' => ['tabindex'=>'3'],'extraOptions'      => [ 'simpleCombo' => true ] ] ),
			":Trans"           => FormField::combo( 'KodeTrans', [ 'config' => [ 'value' => $dsTUang[ 'KodeTrans' ], 'data' => ['KK' => 'KK - BM Dari Kas',] ], 'extraOptions'              => [ 'simpleCombo' => true ] ] ),
			":BtnSearch"       => '<button id="BtnSearch" type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>',
		],
        [
            'url_main'         => 'ttbmhd',
            'url_id'           => $id,
            'url_delete'        => Url::toRoute(['ttbmhd/delete', 'id' => $_GET['id'] ?? ''] ),
        ]
	);
	ActiveForm::end();
	?>
</div>
<?php
$urlAdd         = \abengkel\models\Ttbmhd::getRoute('KK',['action'=>'create'] )[ 'create' ];
$urlIndex       = \abengkel\models\Ttbmhd::getRoute('KK',[ 'id' => $id ] )[ 'index' ];
$urlPrint       = $url[ 'print' ];
$urlLoop        = Url::toRoute( [ 'ttbmhd/loop-bank-masuk' ] );
$urlDataKas     = Url::toRoute( [ 'ttkkhd/select-kk-kas','BMNo'=>$dsTUang['BMNo']] );
//$BMLink         = 'BMNo';
$this->registerJsVar('__update', $_GET['action']);
$this->registerJs(<<< JS
$('#BtnSearch').click(function () {
        
        window.util.app.dialogListData.show({
                title: 'Daftar Kas Keluar',
                url: '$urlDataKas'
            },
            function (data) {
                if (data.data === undefined) return;
                console.log(data);
                $('[name=BMLink]').val(data.data.No);
                $('[name=MotorNoPolisi]').val(data.data.KasKode);
                $('[name=BMNominal]').utilNumberControl().val(data.data.KKNominal);
                $.ajax({
                    type: 'POST',
                    url: '$urlLoop',
                    data: $('#form_ttbmhd_id').serialize()
                }).then(function (cusData) { // console.log(data);
                    window.location.href = "{$url['update']}&oper=skip-load&BMNoView={$dsTUang['BMNoView']}"; 
                });
            })
    });
	
	$('#btnSave').click(function (event) {	  
	    let Nominal = parseFloat($('#BMNominal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Nominal tidak boleh 0', size: 'small'});
               return;
        }
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttbmhd_id').attr('action','{$url['update']}');
        $('#form_ttbmhd_id').submit();
    }); 
	$('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  });
	
	$('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
	
	$('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
    
	$('[name=BMTgl]').utilDateControl().cmp().attr('tabindex', '2');
	//enable kode saat edit request 07/06/2024
	/*
	$( document ).ready(function() {
        if(__update == 'update'){
            $('#NoAccount').attr('disabled',true);
        }
    });*/
JS
);
