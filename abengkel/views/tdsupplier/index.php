<?php
use abengkel\assets\AppAsset;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Supplier';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
        'SupKode'       => 'Sup Kode',
        'SupNama'       => 'Sup Nama',
        'SupContact'    => 'Sup Contact',
        'SupAlamat'     => 'Sup Alamat',
        'SupKota'       => 'Sup Kota',
        'SupTelepon'    => 'Sup Telepon',
        'SupFax'        => 'Sup Fax',
        'SupEmail'      => 'Sup Email',
        'SupKeterangan' => 'Sup Keterangan',
        'SupStatus'     => 'Sup Status',
	],
	'cmbTgl'    => [
	],
	'cmbNum'    => [
	],
	'sortname'  => "SupStatus, SupKode",
	'sortorder' => ''
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Tdsupplier::className(), 'Supplier' ), \yii\web\View::POS_READY );
