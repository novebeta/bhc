<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdsupplier */

$this->title = 'Edit Supplier : ' . $model->SupKode;
$this->params['breadcrumbs'][] = ['label' => 'Supplier', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="tdsupplier-update">
    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdsupplier/update', 'id' => $id,'action' => 'update' ] ),
            'cancel' => Url::toRoute( [ 'tdsupplier/cancel', 'id' => $id,'action' => 'update' ] )
        ]
    ]) ?>
</div>
