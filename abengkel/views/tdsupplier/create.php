<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdsupplier */

$this->title = 'Tambah Supplier';
$this->params['breadcrumbs'][] = ['label' => 'Supplier', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdsupplier-create">
    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdsupplier/update', 'id' => $id,'action' => 'create' ] ),
            'cancel' => Url::toRoute( [ 'tdsupplier/cancel', 'id' => $id,'action' => 'create' ] )
        ]
    ]) ?>

</div>
