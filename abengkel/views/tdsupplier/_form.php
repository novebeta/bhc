<?php
use common\components\Custom;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdsupplier */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="tdsupplier-form">
    <?php
    $form = ActiveForm::begin(['id' => 'frm_tdsupplier_id'  ,'enableClientValidation'=>false]);
    \abengkel\components\TUi::form(
        ['class' => "row-no-gutters",
            'items' => [
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelSupKode"],
                        ['class' => "col-sm-5",'items' => ":SupKode"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelSupNama"],
                        ['class' => "col-sm-10",'items' => ":SupNama"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelSupContact"],
                        ['class' => "col-sm-8",'items' => ":SupContact"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelSupAlamat"],
                        ['class' => "col-sm-10",'items' => ":SupAlamat"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelSupKota"],
                        ['class' => "col-sm-8",'items' => ":SupKota"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelSupTelepon"],
                        ['class' => "col-sm-6",'items' => ":SupTelepon"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-1",'items' => ":LabelSupFax"],
                        ['class' => "col-sm-5",'items' => ":SupFax"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-1",'items' => ":LabelSupEmail"],
                        ['class' => "col-sm-6",'items' => ":SupEmail"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelSupKeterangan"],
                        ['class' => "col-sm-13",'items' => ":SupKeterangan"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-1",'items' => ":LabelSupStatus"],
                        ['class' => "col-sm-6",'items' => ":SupStatus"],
                    ],
                ],
            ]
        ],
        [
            'class' => "pull-right",
            'items' => ":btnAction"
        ],
        [
            ":LabelSupKode"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode Supplier</label>',
            ":LabelSupNama"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Supplier</label>',
            ":LabelSupContact"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kontak</label>',
            ":LabelSupAlamat"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
            ":LabelSupKota"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kota</label>',
            ":LabelSupTelepon"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon</label>',
            ":LabelSupFax"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">Fax</label>',
            ":LabelSupEmail"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Email</label>',
            ":LabelSupKeterangan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
            ":LabelSupStatus"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',

            ":SupKode"          => $form->field($model, 'SupKode', ['template' => '{input}'])->textInput(['maxlength' => '15','autofocus' => 'autofocus' ]),
            ":SupNama"          => $form->field($model, 'SupNama', ['template' => '{input}'])->textInput(['maxlength' => '50', ]),
            ":SupContact"       => $form->field($model, 'SupContact', ['template' => '{input}'])->textInput(['maxlength' => '50', ]),
            ":SupAlamat"        => $form->field($model, 'SupAlamat', ['template' => '{input}'])->textInput(['maxlength' => '75', ]),
            ":SupKota"          => $form->field($model, 'SupKota', ['template' => '{input}'])->textInput(['maxlength' => '25', ]),
            ":SupTelepon"       => $form->field($model, 'SupTelepon', ['template' => '{input}'])->textInput(['maxlength' => '75', ]),
            ":SupFax"           => $form->field($model, 'SupFax', ['template' => '{input}'])->textInput(['maxlength' => '50', ]),
            ":SupEmail"         => $form->field($model, 'SupEmail', ['template' => '{input}'])->textInput(['maxlength' => '25', ]),
            ":SupKeterangan"    => $form->field($model, 'SupKeterangan', ['template' => '{input}'])->textInput(['maxlength' => '100', ]),
            ":SupStatus"        => $form->field($model, 'SupStatus', ['template' => '{input}'])
                ->dropDownList([
                    'A' => 'AKTIF',
                    'N' => 'NON AKTIF',
                    '1' => 'Supplier Utama',
                ], ['maxlength' => true]),
//            ":btnSave"          => Html::submitButton('<i class="glyphicon glyphicon-floppy-disk"></i> Simpan', [ 'class' => 'btn btn-info btn-primary' . Custom::viewClass(), 'id' => 'btnSave']),
//            ":btnCancel"        => '<a href="' . $url['cancel'] . '" class="btn btn-danger btn-primary" role="button"><i class="fa fa-ban"></i>&nbsp&nbsp Batal &nbsp&nbsp</a>'
        ],
	    [
		    'url_main'   => 'tdsupplier',
		    'url_id'     => $id,
		    'url_update' => $url[ 'update' ]
	    ]
    );
    ActiveForm::end();
    ?>
</div>
<?php
$urlIndex   = Url::toRoute( [ 'tdsupplier/index' ] );
$urlAdd     = Url::toRoute( [ 'tdsupplier/create','id'=>'new' ] );
$this->registerJs( <<< JS
     $('#btnSave').click(function (event) {
       $('#frm_tdsupplier_id').submit();
    }); 
    $('#btnCancel').click(function (event) {	  
         window.location.href = '{$url['cancel']}';
    });
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
    $('#btnAdd').click(function (event) {	      
        window.location.href = '{$urlAdd}';
	  });
    


JS);


