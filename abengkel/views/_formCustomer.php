<?php
use abengkel\models\Tdmotortype;
use yii\helpers\Url;
echo $this->render( 'tdcustomer/_formedit' );
$urlLoadCustomer = Url::toRoute( [ 'tdcustomer/load-customer' ] );
$urlSaveCustomer = Url::toRoute( [ 'tdcustomer/save-customer' ] );
$urlTdArea       = Url::toRoute( 'tdarea/find' );
$__dtMotor       = Tdmotortype::find()->all();
$this->registerJsVar( '__dtMotor', $__dtMotor );
$this->registerJs( <<< JS

    $('[id="MotorType"]').select2({
        data: $.map(__dtMotor, function (obj) {
            obj.id = obj.MotorType;
            obj.text = obj.MotorType;
            return obj;
        }),
    }).on('select2:select', function (e) {
        var data = e.params.data;
        if(data === undefined) return ;
        console.log(data);        
        $('[name="Tdcustomer[MotorNama]"]').val(data.MotorNama).trigger('change');
        $('[name="Tdcustomer[MotorKategori]"]').val(data.MotorKategori);
        $('[name="Tdcustomer[MotorCC]"]').val(data.MotorCC);
        $('[name="Tdcustomer[MotorWarna]"]').val(data.MotorWarna);        
        $('[name="Tdcustomer[CmbMotorNoMesin]"]').val(data.MotorNoMesin).trigger('change');
        $('[name="Tdcustomer[CmbMotorNoRangka]"]').val(data.MotorNoRangka).trigger('change');
       	let MotorNoMesin = window.MotorNoMesin;
        if (MotorNoMesin === '--' || MotorNoMesin === ''){
        	$('[name="Tdcustomer[MotorNoMesin]"]').val(data.MotorNoMesin);
        }
        let MotorNoRangka = window.MotorNoRangka;
        if (MotorNoRangka === '--' || MotorNoRangka === ''){
        	$('[name="Tdcustomer[MotorNoRangka]"]').val(data.MotorNoRangka);
        }
    });

    $('[id="MotorNama"]').select2({
        data: $.map(__dtMotor, function (obj) {
            obj.id = obj.MotorNama;
            obj.text = obj.MotorNama;
            return obj;
        }),
    }).on('select2:select', function (e) {
        var data = e.params.data;
        if(data === undefined) return ;
        $('[name="Tdcustomer[MotorType]"]').val(data.MotorType).trigger('change');
        $('[name="Tdcustomer[MotorKategori]"]').val(data.MotorKategori);
        $('[name="Tdcustomer[MotorCC]"]').val(data.MotorCC);
        $('[name="Tdcustomer[MotorWarna]"]').val(data.MotorWarna);        
        $('[name="Tdcustomer[CmbMotorNoMesin]"]').val(data.MotorNoMesin).trigger('change');
        $('[name="Tdcustomer[CmbMotorNoRangka]"]').val(data.MotorNoRangka).trigger('change');
        let MotorNoMesin = window.MotorNoMesin;
        if (MotorNoMesin === '--' || MotorNoMesin === ''){
        	$('[name="Tdcustomer[MotorNoMesin]"]').val(data.MotorNoMesin);
        }
        let MotorNoRangka = window.MotorNoRangka;
        if (MotorNoRangka === '--' || MotorNoRangka === ''){
        	$('[name="Tdcustomer[MotorNoRangka]"]').val(data.MotorNoRangka);
        }
    });


    $('[id="CmbMotorNoRangka"]').select2({
        data: $.map(__dtMotor, function (obj) {
            obj.id = obj.MotorNoRangka;
            obj.text = obj.MotorNoRangka;
            return obj;
        }),
    }).on('select2:select', function (e) {
        var data = e.params.data;
        if(data === undefined) return ;
        $('[name="Tdcustomer[MotorType]"]').val(data.MotorType).trigger('change');
        $('[name="Tdcustomer[MotorNama]"]').val(data.MotorNama).trigger('change');
        $('[name="Tdcustomer[MotorKategori]"]').val(data.MotorKategori);
        $('[name="Tdcustomer[MotorCC]"]').val(data.MotorCC);
        $('[name="Tdcustomer[MotorWarna]"]').val(data.MotorWarna);        
        $('[name="Tdcustomer[CmbMotorNoMesin]"]').val(data.MotorNoMesin).trigger('change');
        let MotorNoMesin = window.MotorNoMesin;
        if (MotorNoMesin === '--' || MotorNoMesin === ''){
        	$('[name="Tdcustomer[MotorNoMesin]"]').val(data.MotorNoMesin);
        }
        let MotorNoRangka = window.MotorNoRangka;
        if (MotorNoRangka === '--' || MotorNoRangka === ''){
        	$('[name="Tdcustomer[MotorNoRangka]"]').val(data.MotorNoRangka);
        }
    });
    
    $('[id="CmbMotorNoMesin"]').select2({
        data: $.map(__dtMotor, function (obj) {
            obj.id = obj.MotorNoMesin;
            obj.text = obj.MotorNoMesin;
            return obj;
        }),
    }).on('select2:select', function (e) {
        var data = e.params.data;
        if(data === undefined) return ;
        $('[name="Tdcustomer[MotorType]"]').val(data.MotorType).trigger('change');
        $('[name="Tdcustomer[MotorNama]"]').val(data.MotorNama).trigger('change');
        $('[name="Tdcustomer[MotorKategori]"]').val(data.MotorKategori);
        $('[name="Tdcustomer[MotorCC]"]').val(data.MotorCC);
        $('[name="Tdcustomer[MotorWarna]"]').val(data.MotorWarna);        
        $('[name="Tdcustomer[CmbMotorNoRangka]"]').val(data.MotorNoRangka).trigger('change');
        let MotorNoMesin = window.MotorNoMesin;
        if (MotorNoMesin === '--' || MotorNoMesin === ''){
        	$('[name="Tdcustomer[MotorNoMesin]"]').val(data.MotorNoMesin);
        }
        let MotorNoRangka = window.MotorNoRangka;
        if (MotorNoRangka === '--' || MotorNoRangka === ''){
        	$('[name="Tdcustomer[MotorNoRangka]"]').val(data.MotorNoRangka);
        }
    });

	$('#btnCancelCustomer').click(function (event) {
        $('#modalCustomer').modal('hide');        
    });
	
	$('#modalCustomer').on('hidden.bs.modal', function (e) {
    	$( document).bind( "ajaxStart ajaxStop" );
	});

	$('#btnSaveCustomer').click(function (event) {
	    let MotorNoMesin = $('[name="Tdcustomer[MotorNoMesin]"]').val();
	    let MotorNoRangka = $('[name="Tdcustomer[MotorNoRangka]"]').val();
	    if (MotorNoMesin === "" || MotorNoMesin === "--" || MotorNoMesin.length < 10 || MotorNoRangka === "--" || MotorNoRangka === "" || MotorNoRangka.length < 10) {
            bootbox.alert({message: "Anda belum memasukkan data konsumen dengan lengkap.", size: 'small'});
            return '';
        }
	    if (MotorNoMesin.length < 12){
               bootbox.alert({message:'No Mesin yang kurang dari 12 karakter', size: 'small'});
               return;
           } 
	    $.ajax({
            type: 'POST',
            url: '$urlSaveCustomer',
            data: $('#frm_tdcustomer_id').serialize()
        }).then(function (rData) {
           	console.log(rData);
           	 $('[name=MotorNoPolisi]').val($('[name="Tdcustomer[MotorNoPolisi]"]').val()).trigger('change');
        	$('#modalCustomer').modal('hide');  
        });
    });
	
	function fillFormCustomer(oCust){
	    window.MotorNoMesin = oCust.MotorNoMesin;
	    window.MotorNoRangka = oCust.MotorNoRangka;
	    $('[name=oper]').val(oCust.oper);
	    $('[name="Tdcustomer[MotorType]"]').val(oCust.MotorType).trigger('change');
	    var data = $('[name="Tdcustomer[MotorType]"]').select2('data');
	    $('[name="Tdcustomer[MotorType]"]').trigger({
		    type: 'select2:select',
		    params: {
		        data: data[0]
		    }
		});
	    $('[name="Tdcustomer[MotorNoPolisi]"]').val(oCust.MotorNoPolisi);
	    $('[name="Tdcustomer[MotorNama]"]').val(oCust.MotorNama);
	    $('[name="Tdcustomer[MotorNoRangka]"]').val(oCust.MotorNoRangka);
	    $('[name="Tdcustomer[MotorNoMesin]"]').val(oCust.MotorNoMesin);
	    $('[name="Tdcustomer[CusKK]"]').val(oCust.CusKK);
	    $('[name="Tdcustomer[CusNama]"]').val(oCust.CusNama);
	    $('[name="Tdcustomer[CusKTP]"]').val(oCust.CusKTP);
	    $('[name="Tdcustomer[CusAlamat]"]').val(oCust.CusAlamat);
	    $('[name="Tdcustomer[CusRT]"]').val(oCust.CusRT);
	    $('[name="Tdcustomer[CusRW]"]').val(oCust.CusRW);
	    $('[name="Tdcustomer[CusTelepon]"]').val(oCust.CusTelepon);
	    $('[name="Tdcustomer[CusEmail]"]').val(oCust.CusEmail);
	    $('[name="Tdcustomer[CusTempatLhr]"]').val(oCust.CusTempatLhr);
	    $('[name="Tdcustomer[CusKeterangan]"]').val(oCust.CusKeterangan);
	    $('[name="Tdcustomer[MotorTahun]"]').val(oCust.MotorTahun);
	    $('[name="Tdcustomer[MotorWarna]"]').val(oCust.MotorWarna);
	    $('[name="Tdcustomer[MotorKategori]"]').val(oCust.MotorKategori);
	    $('[name="Tdcustomer[TglAkhirServis]"]').utilDateControl().val(oCust.TglAkhirServis);
	    $('[name="Tdcustomer[CusTglLhr]"]').utilDateControl().val(oCust.CusTglLhr);
	    $('[name="Tdcustomer[MotorCC]"]').val(oCust.MotorCC);	    
	    $('[name="Tdcustomer[CusSex]"]').val(oCust.CusSex).trigger('change');
	    $('[name="Tdcustomer[CusAgama]"]').val(oCust.CusAgama).trigger('change');
	    $('[name="Tdcustomer[CusStatus]"]').val(oCust.CusStatus).trigger('change');
	    $('[name="Tdcustomer[CusPekerjaan]"]').val(oCust.CusPekerjaan).trigger('change');
	    $('[name="Tdcustomer[AHASSNomor]"]').val(oCust.AHASSNomor).trigger('change');
	     window.CusKabupaten = oCust.CusKabupaten;
	     window.CusKecamatan = oCust.CusKecamatan;
	     window.CusKelurahan = oCust.CusKelurahan;
	     window.CusKodePos = oCust.CusKodePos;
	     window.afterLoadKabupaten = function (){
	         console.log(window.CusKabupaten);
	         $('[id="CusKabupaten"]').val(window.CusKabupaten); 
	         window.afterLoadKabupaten = undefined;
	     }
	     window.afterLoadKecamatan = function (){
	         console.log(window.CusKecamatan); 
	         $('[id="CusKecamatan"]').val(window.CusKecamatan);
	         window.afterLoadKecamatan = undefined;
	     }
	     window.afterLoadKelurahan = function (){ 
	         console.log(window.CusKelurahan);
	         $('[id="CusKelurahan"]').val(window.CusKelurahan); 
	         window.afterLoadKelurahan = undefined;
	     }
	     window.afterLoadKodePos = function (){ 
	         console.log(window.CusKodePos);
	         $('[id="CusKodePos"]').val(window.CusKodePos);
	         window.afterLoadKodePos = undefined;
	     }
	     $('[id="CusProvinsi"').val(oCust.CusProvinsi).trigger('change');
	     // window.ajaxStop();
	     // window.ajaxStop();
	     // window.ajaxStop();
	     // window.ajaxStop();
	     // window.ajaxStop();
	}

	function openCustomer(MotorNoPolisi){	    
        $( document).unbind( "ajaxStart ajaxStop" );
	    if (MotorNoPolisi === '--'){
	        fillFormCustomer({
	        	MotorNoPolisi:'--',
	        	oper:'Add',
	        	MotorNama:'',
	        	MotorNoRangka:'',
	        	MotorNoMesin:'',
	        	CusKK:'',
	        	CusNama:'',
	        	CusKTP:'',
	        	CusAlamat:'',
	        	CusRT:'',
	        	CusRW:'',
	        	CusTelepon:'',
	        	CusEmail:'',
	        	CusTempatLhr:'',
	        	CusKeterangan:'',
	        	MotorTahun: (new Date()).getFullYear(),
	        	MotorWarna:'',
	        	MotorKategori:'',
	        	MotorCC:'',
	        	TglAkhirServis:'1900-01-01',
	        	CusTglLhr:'1900-01-01',
	        	MotorType:'',
	        	CusSex:'Laki-Laki',
	        	CusAgama:'Islam',
	        	CusStatus:'AKTIF',
	        	CusPekerjaan:'Pegawai Negeri',
	        	AHASSNomor:'',
	        	CusProvinsi:'Jawa Tengah',
	        	CusKabupaten:'Banjarnegara',
	        	CusKecamatan:'Banjarmangu',
	        	CusKelurahan:'Banjarkulon',
	        	CusKodePos:'53452'
	        });
	        $('#modalCustomer').modal('show');
	    }else{
	        $.ajax({
	            type: 'POST',
	            url: '$urlLoadCustomer',
	            data: {
	                'Tdcustomer[MotorNoPolisi]':MotorNoPolisi
	            }
	        }).then(function (rData) {
	            console.log(rData);
	            rData.oper = 'Edit';
	            fillFormCustomer(rData);
		        $('#modalCustomer').modal('show');
	        });
	    }	    
	}
	window.openCustomer = openCustomer;
	
JS
);
