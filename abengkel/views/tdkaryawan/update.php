<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdkaryawan */

$this->title = 'Edit Karyawan : ' . $model->KarKode;
$this->params['breadcrumbs'][] = ['label' => 'Karyawan', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->KarKode, 'url' => ['view', 'id' => $model->KarKode]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="tdkaryawan-update">
    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
	        'update'   => Url::toRoute( [ 'tdkaryawan/update', 'id' => $id ,'action' => 'update'] ),
	        'cancel' => Url::toRoute( [ 'tdkaryawan/cancel', 'id' => $id,'action' => 'update' ] )
        ]
    ]) ?>

</div>
