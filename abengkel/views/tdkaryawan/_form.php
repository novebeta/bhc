<?php
use aunit\models\Tdarea;
use common\components\Custom;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdkaryawan */
/* @var $form yii\widgets\ActiveForm */
$urlTdArea = Url::toRoute( 'tdarea/find' );
?>
    <div class="tdkaryawan-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'frm_tdkaryawan_id' ,'enableClientValidation'=>false] );
		\abengkel\components\TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelKarKode" ],
					    [ 'class' => "col-sm-7", 'items' => ":KarKode" ],
					    [ 'class' => "col-sm-8" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelKarPassword" ],
					    [ 'class' => "col-sm-5", 'items' => ":KarPassword" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelKarNama" ],
					    [ 'class' => "col-sm-7", 'items' => ":KarNama" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelKarKTP" ],
					    [ 'class' => "col-sm-4", 'items' => ":KarKTP" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelKarProvinsi" ],
					    [ 'class' => "col-sm-5", 'items' => ":KarProvinsi" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelKarAlamat" ],
					    [ 'class' => "col-sm-7", 'items' => ":KarAlamat" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelKarRT" ],
					    [ 'class' => "col-sm-2", 'items' => ":KarRT" ],
					    [ 'class' => "col-sm-2", 'style' => 'padding-left:3px', 'items' => ":KarRW" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelKarKabupaten" ],
					    [ 'class' => "col-sm-5", 'items' => ":KarKabupaten" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelKarTelepon" ],
					    [ 'class' => "col-sm-7", 'items' => ":KarTelepon" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelKarKodePos" ],
					    [ 'class' => "col-sm-4", 'items' => ":KarKodePos" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelKcmt" ],
					    [ 'class' => "col-sm-5", 'items' => ":KarKecamatan" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelKarEmail" ],
					    [ 'class' => "col-sm-14", 'items' => ":KarEmail" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelKarKelurahan" ],
					    [ 'class' => "col-sm-5", 'items' => ":KarKelurahan" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelKarJK" ],
					    [ 'class' => "col-sm-3", 'items' => ":KarJK" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-3", 'items' => ":LabelKarTmpLahir" ],
					    [ 'class' => "col-sm-4", 'items' => ":KarTmpLahir" ],
					    [ 'class' => "col-sm-3", 'style' => 'padding-left:3px', 'items' => ":KarTglLahir" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelKarAgama" ],
					    [ 'class' => "col-sm-5", 'items' => ":KarAgama" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelKarJabatan" ],
					    [ 'class' => "col-sm-4", 'items' => ":KarJabatan" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelKarMasuk" ],
					    [ 'class' => "col-sm-2", 'items' => ":KarMasuk" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelKarKeluar" ],
					    [ 'class' => "col-sm-2", 'items' => ":KarKeluar" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelKarStatus" ],
					    [ 'class' => "col-sm-5", 'items' => ":KarStatus" ],
				    ],
				  ],
			  ]
			],
			[
				'class' => "pull-right",
				'items' => ":btnAction"
			],
			[
				":LabelKarKodePos"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode POS</label>',
				":LabelKarKode"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode</label>',
				":LabelKarPassword"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Password</label>',
				":LabelKarNama"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama</label>',
				":LabelKarKTP"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">KTP</label>',
				":LabelKarProvinsi"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Provinsi</label>',
				":LabelKarAlamat"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
				":LabelKarRT"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">RT/RW</label>',
				":LabelKarRW"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">/</label>',
				":LabelKarTelepon"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon</label>',
				":LabelKcmt"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kecamatan</label>',
				":LabelKarEmail"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Email</label>',
				":LabelKarKelurahan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kelurahan</label>',
				":LabelKarJK"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis Kelamin</label>',
				":LabelKarTmpLahir"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tempat - Tgl Lahir</label>',
				":LabelKarTglLahir"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">-</label>',
				":LabelKarAgama"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Agama</label>',
				":LabelKarJabatan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jabatan</label>',
				":LabelKarMasuk"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Masuk</label>',
				":LabelKarKeluar"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Keluar</label>',
				":LabelKarStatus"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',
				":LabelKarKabupaten" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kabupaten</label>',
				":KarNama"      => $form->field( $model, 'KarNama', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '35', ] ),
				":KarAlamat"    => $form->field( $model, 'KarAlamat', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '75', ] ),
				":KarKTP"       => $form->field( $model, 'KarKTP', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '25', ] ),
				":KarRT"        => $form->field( $model, 'KarRT', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '3', ] ),
				":KarRW"        => $form->field( $model, 'KarRW', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '3', ] ),
				":KarProvinsi"  => $form->field( $model, 'KarProvinsi', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '30', ] ),
				":KarTelepon"   => $form->field( $model, 'KarTelepon', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '30', ] ),
				":KarEmail"     => $form->field( $model, 'KarEmail', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '50', ] ),
				":KarPassword"  => $form->field( $model, 'KarPassword', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '35', ] ),
				":KarTmpLahir"  => $form->field( $model, 'KarTempatLhr', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '30', ] ),
				":KarKodePos"   => $form->field( $model, 'KarKodePos', [ 'template' => '{input}' ] )
				                        ->widget( Select2::class, [ 'value' => $model->KarKodePos, 'theme' => Select2::THEME_DEFAULT, 'pluginOptions' => [ 'highlight' => true ], 'data' => Tdarea::find()->KodePos() ] ),
				":KarKode"      => $form->field( $model, 'KarKode', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '10', 'autofocus' => 'autofocus' ] ),
				":KarKecamatan" => $form->field( $model, 'KarKecamatan', [ 'template' => '{input}' ] )
				                        ->widget( Select2::class, [ 'value' => $model->KarKecamatan, 'theme' => Select2::THEME_DEFAULT, 'pluginOptions' => [ 'highlight' => true ], 'data' => Tdarea::find()->kecamatan() ] ),
				":KarKelurahan" => $form->field( $model, 'KarKelurahan', [ 'template' => '{input}' ] )
				                        ->widget( Select2::class, [ 'value' => $model->KarKelurahan, 'theme' => Select2::THEME_DEFAULT, 'pluginOptions' => [ 'highlight' => true ], 'data' => Tdarea::find()->kelurahan() ] ),
				":KarJK"        => $form->field( $model, 'KarSex', [ 'template' => '{input}' ] )->dropDownList( [ 'Perempuan' => 'Perempuan', 'Laki-Laki' => 'Laki-Laki', ], [ 'maxlength' => true ] ),
				":KarTglLahir"  => DatePicker::widget( [ 'name' => 'KarTglLahir', 'type' => DatePicker::TYPE_INPUT, 'value' => '01/01/1900', 'pluginOptions' => [ 'autoclose' => true, 'format' => 'dd/mm/yyyy' ] ] ),
				":KarMasuk"     => DatePicker::widget( [ 'name' => 'KarTglMasuk', 'type' => DatePicker::TYPE_INPUT, 'value' => Yii::$app->formatter->asDate( 'now', 'dd/mm/yyyy' ), 'pluginOptions' => [ 'autoclose' => true, 'format' => 'dd/mm/yyyy' ] ] ),
				":KarKeluar"    => DatePicker::widget( [ 'name' => 'KarTglKeluar', 'type' => DatePicker::TYPE_INPUT, 'value' => Yii::$app->formatter->asDate( 'now', 'dd/mm/yyyy' ), 'pluginOptions' => [ 'autoclose' => true, 'format' => 'dd/mm/yyyy' ] ] ),
				":KarStatus"    => $form->field( $model, 'KarStatus', [ 'template' => '{input}' ] )->dropDownList( [ 'A' => 'AKTIF', 'N' => 'NON AKTIF', ], [ 'maxlength' => true ] ),
				":KarKabupaten" => $form->field( $model, 'KarKabupaten', [ 'template' => '{input}' ] )
				                        ->widget( Select2::class, [ 'value' => $model->KarKabupaten, 'theme' => Select2::THEME_DEFAULT, 'pluginOptions' => [ 'highlight' => true ], 'data' => Tdarea::find()->kabupaten() ] ),
				":KarAgama"     => $form->field( $model, 'KarAgama', [ 'template' => '{input}' ] )
				                        ->dropDownList( [
					                        'Islam'     => 'Islam',
					                        'Kristen'   => 'Kristen',
					                        'Katholik'  => 'Katholik',
					                        'Hindu'     => 'Hindu',
					                        'Budha'     => 'Budha',
					                        'Lain-Lain' => 'Lain-Lain',
				                        ], [ 'maxlength' => true ] ),
				":KarJabatan"   => $form->field( $model, 'KarJabatan', [ 'template' => '{input}' ] )
				                        ->dropDownList( [
					                        'Akunting'          => 'Akunting',
					                        'Ass Mekanik'       => 'Ass Mekanik',
					                        'Counter Part'      => 'Counter Part',
					                        'CRM'               => 'CRM',
					                        'Front Desk'        => 'Front Desk',
					                        'Kepala Bengkel'    => 'Kepala Bengkel',
					                        'Kasir'             => 'Kasir',
					                        'Kepala Mekanik'    => 'Kepala Mekanik',
					                        'Mekanik Final Cek' => 'Mekanik Final Cek',
					                        'Mekanik'           => 'Mekanik',
					                        'PIC Part'          => 'PIC Part',
					                        'Office Boy'        => 'Office Boy',
					                        'Servis Advisor'    => 'Servis Advisor',
				                        ], [ 'maxlength' => true ] ),
			],
			[
				'url_main'   => 'tdkaryawan',
				'url_id'     => $id,
				'url_update' => $url[ 'update' ]
			]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$this->registerJs( <<< JS
    let KarKabupaten = $('#tdkaryawan-karkabupaten'),
        KarKecamatan = $('#tdkaryawan-karkecamatan'),
        KarKelurahan = $('#tdkaryawan-karkelurahan');
    KarKabupaten.on("change", function (e) { console.log()
        KarKecamatan.utilSelect2().loadRemote('$urlTdArea', { mode: 'combo', field: 'Kecamatan', Kabupaten: this.value }, true, function(response) {
            KarKecamatan.val(null).trigger('change');
            KarKelurahan.val(null).trigger('change');
        });
    });

    KarKecamatan.on("change", function (e) {
        KarKelurahan.utilSelect2().loadRemote('$urlTdArea', { mode: 'combo', field: 'Kelurahan', Kecamatan: this.value }, true, function(response) {
            KarKelurahan.val(null).trigger('change');
        });
    });
    
    $('#btnSave').click(function (event) {
       $('#frm_tdkaryawan_id').submit();
    }); 
    
    
    
JS
);


