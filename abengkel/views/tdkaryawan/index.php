<?php
use abengkel\assets\AppAsset;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Karyawan';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'KarKode'          => 'Kode',
		'KarNama'          => 'Nama',
		'KarAlamat'        => 'Alamat',
		'KarKTP'           => 'KTP',
		'KarRT'            => 'RT',
		'KarRW'            => 'RW',
		'KarTempatLhr'     => 'Tempat Lhr',
		'KarProvinsi'      => 'Provinsi',
		'KarKabupaten'     => 'Kabupaten',
		'KarKecamatan'     => 'Kecamatan',
		'KarKelurahan'     => 'Kelurahan',
		'KarKodePos'       => 'Kode Pos',
		'KarTelepon'       => 'Telepon',
		'KarEmail'         => 'Email',
		'KarSex'           => 'Sex',
		'KarAgama'         => 'Agama',
		'KarJabatan'       => 'Jabatan',
		'KarStatus'        => 'Status',
		'KarPassword'      => 'Password',
		'KarTglLhr'        => 'Tgl Lhr',
		'KarTglMasuk'      => 'Tgl Masuk',
		'KarTglKeluar'     => 'Tgl Keluar',
	],
	'cmbTgl' => [

	],
	'cmbNum' => [
	],
	'sortname'  => "KarStatus, KarKode ",
	'sortorder' => ''
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Tdkaryawan::className(),'Karyawan' ), \yii\web\View::POS_READY );
