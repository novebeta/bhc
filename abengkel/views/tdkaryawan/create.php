<?php
use yii\helpers\Url;
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdkaryawan */

$this->title = 'Tambah Karyawan';
$this->params['breadcrumbs'][] = ['label' => 'Karyawan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdkaryawan-create">
    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url' => [
            'update'    => Url::toRoute( [ 'tdkaryawan/update', 'id' => $id ,'action' => 'create'] ),
            'cancel' => Url::toRoute( [ 'tdkaryawan/cancel', 'id' => $id,'action' => 'create' ] )
        ]
    ]) ?>

</div>
