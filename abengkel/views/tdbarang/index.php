<?php
use abengkel\assets\AppAsset;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Spare Part';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'BrgKode'       => 'Kode Barang',
		'BrgNama'       => 'Nama Barang',
		'BrgGroup'      => 'Group',
		'BrgSatuan'     => 'Satuan',
		'BrgStatus'     => 'Status',
		'BrgMemo'       => 'Memo',
		'BrgBarCode'    => 'BarKode',
		'BrgNamaPasar1' => 'Nama Pasar1',
		'BrgNamaPasar2' => 'Nama Pasar2',
		'BrgRak1'       => 'Rak1',
		'BrgRak2'       => 'Rak2',
		'BrgRak3'       => 'Rak3',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
		'BrgHrgBeli'  => 'Harga Beli',
		'BrgHrgJual'  => 'Harga Jual',
		'BrgMinStock' => 'Minimal Stock',
		'BrgMaxStock' => 'Maximal Stock',
	],
	'sortname'  => "BrgStatus,BrgGroup, BrgKode",
	'sortorder' => ''
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Tdbarang::className(),'Spare Part' ), \yii\web\View::POS_READY );
