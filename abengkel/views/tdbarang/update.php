<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdbarang */

$this->title = 'Edit Spare Part: ' . $model->BrgKode;
$this->params['breadcrumbs'][] = ['label' => 'Spare Part', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="tdbarang-update">
    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdbarang/update', 'id' => $id ,'action' => 'update'] ),
            'cancel' => Url::toRoute( [ 'tdbarang/cancel', 'id' => $id,'action' => 'update' ] )
        ]
    ]) ?>

</div>
