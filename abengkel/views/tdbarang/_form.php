<?php
use abengkel\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\number\NumberControl;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdbarang */
/* @var $form yii\widgets\ActiveForm */
?>
    <div class="tdbarang-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'frm_tdbarang_id', 'enableClientValidation' => false ] );
		\abengkel\components\TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelBrgKode" ],
					    [ 'class' => "col-sm-8", 'items' => ":BrgKode" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelBrgBarCode" ],
					    [ 'class' => "col-sm-7", 'items' => ":BrgBarCode" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-1", 'items' => ":btnPhoto" ]
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelBrgNama" ],
					    [ 'class' => "col-sm-22", 'items' => ":BrgNama" ]
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelBrgGroup" ],
					    [ 'class' => "col-sm-10", 'items' => ":BrgGroup" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelBrgSatuan" ],
					    [ 'class' => "col-sm-9", 'items' => ":BrgSatuan" ]
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelBrgHrgBeli" ],
					    [ 'class' => "col-sm-6", 'items' => ":BrgHrgBeli" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelBrgMinStock" ],
					    [ 'class' => "col-sm-6", 'items' => ":BrgMinStock" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelTglUpdate" ],
					    [ 'class' => "col-sm-4", 'items' => ":TglUpdate" ]
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelBrgHrgJual" ],
					    [ 'class' => "col-sm-6", 'items' => ":BrgHrgJual" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelBrgMaxStock" ],
					    [ 'class' => "col-sm-6", 'items' => ":BrgMaxStock" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelBrgStatus" ],
					    [ 'class' => "col-sm-4", 'items' => ":BrgStatus" ]
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelBrgRak1" ],
					    [ 'class' => "col-sm-6", 'items' => ":1BrgRak" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelBrgRak2" ],
					    [ 'class' => "col-sm-6", 'items' => ":2BrgRak" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelBrgRak3" ],
					    [ 'class' => "col-sm-4", 'items' => ":3BrgRak" ]
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelBrgMemo" ],
					    [ 'class' => "col-sm-22", 'items' => ":BrgMemo" ]
				    ],
				  ],
			  ]
			],
			[
				'class' => "pull-right",
				'items' => ":btnAction"
			],
			[
				":LabelBrgKode"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode</label>',
				":LabelBrgBarCode"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bar Code</label>',
				":LabelBrgNama"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama</label>',
				":LabelBrgGroup"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Group</label>',
				":LabelBrgSatuan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Satuan</label>',
				":LabelBrgHrgBeli"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Hrg Beli</label>',
				":LabelBrgMinStock" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Min Stock</label>',
				":LabelTglUpdate"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Update</label>',
				":LabelBrgHrgJual"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Hrg Jual</label>',
				":LabelBrgMaxStock" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Max Stock</label>',
				":LabelBrgStatus"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',
				":LabelBrgRak1"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Rak 1</label>',
				":LabelBrgRak2"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Rak 2</label>',
				":LabelBrgRak3"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Rak 3</label>',
				":LabelBrgMemo"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Memo</label>',
				":BrgKode"          => $form->field( $model, 'BrgKode', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '20', 'autofocus' => 'autofocus','onkeyup'=>'this.value = this.value.toUpperCase();', 'style' => 'text-transform: uppercase' ] ),
				":BrgBarCode"       => $form->field( $model, 'BrgBarCode', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '50', ] ),
				":BrgNama"          => $form->field( $model, 'BrgNama', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '75', 'onkeyup'=>'this.value = this.value.toUpperCase();', 'style' => 'text-transform: uppercase'] ),
				":BrgSatuan"        => FormField::combo( 'BrgSatuan', ['name' =>'Tdbarang[BrgSatuan]', 'config' => [ 'value' => $model->BrgSatuan ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":TglUpdate"        => FormField::dateInput( [ 'name' => 'Tdbarang[TglUpdate]', 'config' => [ 'value' => General::asDate( $model->TglUpdate ) ] ] ),
				":BrgHrgBeli"       => $form->field( $model, 'BrgHrgBeli', [ 'template' => '{input}' ] )->widget( NumberControl::class, [ 'maskedInputOptions' => [ 'allowMinus' => false ], 'displayOptions' => [ 'class' => 'form-control', 'placeholder' => '0', ], 'readonly' => ! $model->isNewRecord ] ),
				":BrgMinStock"      => $form->field( $model, 'BrgMinStock', [ 'template' => '{input}' ] )->widget( NumberControl::class, [ 'maskedInputOptions' => [ 'allowMinus' => false ], 'displayOptions' => [ 'class' => 'form-control', 'placeholder' => '0', ] ] ),
				":BrgHrgJual"       => $form->field( $model, 'BrgHrgJual', [ 'template' => '{input}' ] )->widget( NumberControl::class, [ 'maskedInputOptions' => [ 'allowMinus' => false ], 'displayOptions' => [ 'class' => 'form-control', 'placeholder' => '0', ], 'readonly' => ! $model->isNewRecord ] ),
				":BrgMaxStock"      => $form->field( $model, 'BrgMaxStock', [ 'template' => '{input}' ] )->widget( NumberControl::class, [ 'maskedInputOptions' => [ 'allowMinus' => false ], 'displayOptions' => [ 'class' => 'form-control', 'placeholder' => '0', ] ] ),
				":BrgStatus"        => FormField::combo( 'BrgStatus', ['name' =>'Tdbarang[BrgStatus]', 'config' => [ 'value' => $model->BrgStatus ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":1BrgRak"          => $form->field( $model, 'BrgRak1', [ 'template' => '{input}' ] )->textInput( [ 'value' => $model->BrgRak1,'maxlength' => '15', ] ),
				":2BrgRak"          => $form->field( $model, 'BrgRak2', [ 'template' => '{input}' ] )->textInput( [ 'value' => $model->BrgRak2,'maxlength' => '15', ] ),
				":3BrgRak"          => $form->field( $model, 'BrgRak3', [ 'template' => '{input}' ] )->textInput( [ 'value' => $model->BrgRak3,'maxlength' => '15', ] ),
				":BrgMemo"          => $form->field( $model, 'BrgMemo', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => '150', ] ),
				":BrgGroup"         => $form->field( $model, 'BrgGroup', [ 'template' => '{input}' ] )
				                            ->dropDownList( [
					                            'Accesories'    => 'Accesories',
					                            'Battery'       => 'Battery',
					                            'Oli'           => 'Oli',
					                            'Part'          => 'Honda Genuine Part',
					                            'Plastic Part'  => 'Plastic Part',
					                            'Special Tools' => 'Special Tools',
					                            'Tire'          => 'Ban',
					                            'XLokal'        => 'Lokal',
				                            ], [ 'maxlength' => true ] ),
				":btnPhoto"         => Html::submitButton( '<i class="glyphicon glyphicon-camera fa-lg"></i>&nbsp Load Foto', [ 'class' => 'btn btn-primary' ] ),
			],
			[
				'url_main'   => 'tdbarang',
				'url_id'     => $id,
				'url_update' => $url[ 'update' ]
			]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlIndex = Url::toRoute( [ 'tdbarang/index' ] );
$urlAdd   = Url::toRoute( [ 'tdbarang/create', 'id' => 'new' ] );
$this->registerJs( <<< JS
     $('#btnSave').click(function (event) {
       $('#frm_tdbarang_id').submit();
    }); 
    $('#btnCancel').click(function (event) {	  
         window.location.href = '{$url['cancel']}';
    });
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
    $('#btnAdd').click(function (event) {	      
        window.location.href = '{$urlAdd}';
	  });
    
    

JS
);


