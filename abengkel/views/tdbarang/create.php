<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdbarang */

$this->title = 'Tambah Spare Part';
$this->params['breadcrumbs'][] = ['label' => 'Spare Part', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdbarang-create">
    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdbarang/update', 'id' => $id,'action' => 'create' ] ),
            'cancel' => Url::toRoute( [ 'tdbarang/cancel', 'id' => $id,'action' => 'create' ] )
        ]
    ]) ?>
</div>
