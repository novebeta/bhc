<?php

\abengkel\assets\AppAsset::register($this);

?>
    <div class="panel panel-default">
        <div class="panel-body">
            <table id="headerGridPL"></table><div id="headerGridPagerPL"></div>
            <table id="detailGridPL"></table><div id="detailGridPagerPL"></div>
        </div>
    </div>
<?php
$Lokasi        = \common\components\General::cCmd( "SELECT * FROM tdLokasi Order By LokasiStatus, LokasiNomor" )->queryAll();
$this->registerJsVar( '__Lokasi', json_encode( $Lokasi ) );

$this->registerJs(<<< JS
    
    var __LokasiKode = JSON.parse(__Lokasi);
     __LokasiKode = $.map(__LokasiKode, function (obj) {
                      obj.id = obj.LokasiKode;
                      obj.text = obj.LokasiKode;
                      return obj;
                    });
    
    var headerGrid = $('#headerGridPL');
     headerGrid.utilJqGrid({
        height: 150,
        url: '{$urlHeaderGrid}',
        extraParams: {
            id: '$id'
        },
        postData: {
            id: '$id'
        },
        readOnly: true,
        rownumbers: true,
        colModel: [
            {
                name: 'SIPickingNo',
                label: 'No Picking',
                width: 100
            },
            {
                name: 'SIPickingDate',
                label: 'Tgl Picking',
                width: 100
            },
            {
                name: 'SINo',
                label: 'No SI',
                width: 100
            },
            {
                name: 'SITgl',
                label: 'Tgl SI',
                width: 100
            }, 
            {
                name: 'Cetak',
                label: 'Cetak',
                width: 100
            }
        ],
        onSelectRow: function(rowId) {
             detailGrid.utilJqGrid().load({ param: { id: rowId } });
             parent.window.util.app.dialogPrint.selectedItem = { id: rowId, data: headerGrid.getRowData(rowId) };
        }
     }).init().fit($('.panel-body')).load({url: '{$urlHeaderGrid}'});
     
    var detailGrid = $('#detailGridPL');
     detailGrid.utilJqGrid({
        url: '{$urlDetailGrid}',
        editurl: '{$urlDetailGrid}',
        height: 170,
        // readOnly: true, DATA BISA DIEDIT - REQUEST 25/6/2024
        rownumbers: true, 
        navButtonTambah: false,
        data: {
           oper: 'edit',             
           },
        saveRowParams:{
            aftersavefunc:function (rowid, response, options) {
                $.notify({message: response.responseJSON.message},{type: response.responseJSON.success ? 'success' : 'warning'});
                $('#detailGridPL').utilJqGrid().load(window.detailGridPLParams);
            },
        },      
        colModel: [
            { template: 'actions' },
            {
                name: 'SIAuto',
                label: 'SIAuto',
                width: 100,
                editable: true,
                hidden:true
            },
            {
                name: 'SINo',
                label: 'SINo',
                width: 100,
                editable: true,
                hidden:true
            },
            {
                name: 'BrgKode',
                label: 'BrgKode',
                editable: true,
                editor: { type:'text', readOnly: true },
                width: 115
            },
            {
                name: 'BrgNama',
                label: 'BrgNama',
                width: 222
            },
            {
                name: 'BrgGroup',
                label: 'Group',
                width: 100
            },
            {
                name: 'SIQty',
                label: 'Qty.',
                template: 'number',
                width: 60
            }, 
            {
                name: 'BrgSatuan',
                label: 'Satuan',
                width: 80
            },
            {
                name: 'BrgRak1',
                label: 'Bin',
                width: 110
            },
            {
                name: 'LokasiKode',
                label: 'Lokasi',
                width: 110,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __LokasiKode,
                }
            },
            // {
            //     name: 'LokasiKode',
            //     label: 'Lokasi',
            //     width: 110
            // },
            {
                name: 'SaldoQty',
                label: 'Saldo',
                template: 'number',
                width: 110
            }
        ]  ,
        onSelectRow: function() {
           var d = $('#detailGridPL').utilJqGrid().getData();
               console.log(d);
        },
        
     }).init().fit($('.box-body')).load({url: '{$urlDetailGrid}'});
     // }).init().fit($('.panel-body'));
JS
);
