<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttsihd */
$params                          = '&id=' . $id . '&action=update&jenis='.$dsTJual['SINoView'];
$cancel                          = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$print                          = Custom::url(\Yii::$app->controller->id.'/print'.$params );
$this->title                     = "Edit - $title : " . $dsTJual['SINoView'];
?>
<div class="ttsihd-update">
	<?= $this->render( '_form', [
		'model'   => $model,
		'dsTJual' => $dsTJual,
		'id'      => $id,
        'jenis' => $jenis,
        'title' => $title,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . "/$view-update" . $params ),
            'print'  => $print,
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttsiit/index','action' => 'create', 'jenis' => $jenis ] ),
        ]
	] ) ?>
</div>
