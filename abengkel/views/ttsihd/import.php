<?php
/* @var $this yii\web\View */

/* @var $dataProvider yii\data\ActiveDataProvider */

use abengkel\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
$this->title = 'Manage Parts Sales';
$this->params['breadcrumbs'][] = $this->title;
$arr = [
    'cmbTxt' => [
        'noShippingList' => 'No Shipping List',
        'noSO' => 'No SO',
    ],
    'cmbTgl' => [
        'tanggalTerima' => 'Tanggal'
    ],
    'cmbNum' => [
    ],
    'sortname' => "noShippingList",
    'sortorder' => 'desc'
];
$this->registerJsVar('setcmb', $arr);
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render('../_search'); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs(<<< JS
    window.gridParam_jqGrid = {
        readOnly: true,
        rownumbers: true,
        rowNum: 10000,
        pgbuttons: false,
        pginput: false,
        viewrecords: false,
        rowList: [],
        pgtext: ""
    }
    // $('#filter-tgl-val1').change(function() {
    //     let _tgl1 = new Date($(this).val());      
    //     _tgl1.setDate(_tgl1.getDate() + 5);
	//     $('#filter-tgl-val2-disp').datepicker('update', _tgl1);
	//     $('#filter-tgl-val2-disp').trigger('change');
    // });
JS
);

$this->registerJs(\abengkel\components\TdUi::mainGridJs(\abengkel\models\Ttsihd::class,
    'Import Parts Sales', [
        'colGrid' => $colGrid,
        'url' => Url::toRoute(['ttsihd/import-items']),
        'mode' => isset($mode) ? $mode : '',
    ]), \yii\web\View::POS_READY);
$urlDetail = Url::toRoute(['ttsihd/import-details']);
$this->registerJs(<<< JS
jQuery(function ($) {
    if($('#jqGridDetail').length) {
        $('#jqGridDetail').utilJqGrid({
            url: '$urlDetail',
            height: 240,
            readOnly: true,
            rownumbers: true,
            rowNum: 1000,
            colModel: [
               	{
	                name: 'partsNumber',
	                label: 'Parts Number',
	                width: 100,
	            },
                {
                    name: 'kuantitas',
                    label: 'Kuantitas',
                    width: 80,
                    align : 'right'
                },
	            {
	                name: 'hargaParts',
	                label: 'Harga Parts',
	                width: 90,
	                template: 'money',
	            },
	            {
	                name: 'discAmount',
	                label: 'Disc Amount',
	                width: 90,
	                template: 'money',
	            },
	            {
	                name: 'discPercentage',
	                label: 'Disc Percentage',
	                width: 100,
	                align : 'right'
	            },
	            {
	                name: 'ppn',
	                label: 'Ppn',
	                width: 80,
	                template: 'money',
	            },
	            {
	                name: 'totalHargaParts',
	                label: 'Total Harga Parts',
	                width: 120,
	                template: 'money',
	            },
	            {
	                name: 'uangMuka',
	                label: 'Uang Muka',
	                width: 90,
	                template: 'money',
	            },
	            {
	                name: 'promoIdParts',
	                label: 'Promo Id Parts',
	                width: 100,
	            },
	            {
	                name: 'bookingIdReference',
	                label: 'Booking Id Reference',
	                width: 130,
	            },
	            {
	                name: 'createdTime',
	                label: 'Created Time',
	                width: 130,
	            },
            ],
	        multiselect: gridFn.detail.multiSelect(),
	        onSelectRow: gridFn.detail.onSelectRow,
	        onSelectAll: gridFn.detail.onSelectAll,
	        ondblClickRow: gridFn.detail.ondblClickRow,
	        loadComplete: gridFn.detail.loadComplete,
	        listeners: {
                afterLoad: function(grid, response) {
                    $("#cb_jqGridDetail").trigger('click');
                }
            }
        }).init().setHeight(gridFn.detail.height);
    }
});
JS, \yii\web\View::POS_READY);