<?php
use abengkel\components\FormField;
use abengkel\components\Menu;
use abengkel\models\Tdlokasi;
use abengkel\models\Ttsihd;
use common\components\Custom;
use common\components\General;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttsihd */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$altCondition = "KarStatus = 'A'";
$PenjualanInternal = $title;//Penjualan Internal
$isInternal = in_array($dsTJual['KodeTrans'], ['TC15', 'TC18', 'TC41', 'TC99','TC17']);
$escape     = new JsExpression("function(m) { return m; }");
if($PenjualanInternal == 'Penjualan Internal'){
    $jenis = 'penjualanInternal';
}else if ($PenjualanInternal == 'Invoice Penjualan'){
    $jenis = 'invoicePenjualan';
}else{
    $jenis = 'mutasiInternal';
}
//$jenis = $PenjualanInternal == 'Penjualan Internal' ? 'penjualanInternal' : 'invoicePenjualan';
$this->registerJs($format, View::POS_HEAD);
$this->registerJsVar('__isInternal', $isInternal);
$this->registerJsVar('__PenjualanInternal', $PenjualanInternal);
?>
    <div class="ttsihd-form">
		<?php
$dsTJual['SIDiscPersen'] = 0;
if ($dsTJual['SITotal'] > 0) {
    $dsTJual['SIDiscPersen'] = round($dsTJual['SIDiscFinal'] / $dsTJual['SITotal'] * 100);
}
$LokasiKodeCmb = ArrayHelper::map(Tdlokasi::find()->select(['LokasiKode'])->where(['LokasiStatus' => 'A', 'LEFT(LokasiNomor,1)' => '7'])->orderBy('LokasiStatus, LokasiKode')->asArray()->all(), 'LokasiKode', 'LokasiKode');
$form          = ActiveForm::begin(['id' => 'form_ttsihd_id', 'action' => $url['update']]);
\abengkel\components\TUi::form(
    ['class' => "row-no-gutters",
        'items'  => [
            ['class' => "form-group col-md-24",
                'items'  => [
                    ['class' => "form-group col-md-10",
                        'items'  => [
                            ['class' => "col-sm-4", 'items' => ":LabelSINo"],
                            ['class' => "col-sm-5", 'items' => ":SINo"],
                            ['class' => "col-sm-2", 'items' => ":LabelSITgl", 'style' => 'padding-left:3px;'],
                            ['class' => "col-sm-5", 'items' => ":SITgl", 'style' => 'padding-left:3px;'],
                            ['class' => "col-sm-3", 'items' => ":LabelSIJam", 'style' => 'padding-left:7px;'],
                            ['class' => "col-sm-5", 'items' => ":SIJam", 'style' => 'padding-right:7px;'],
                        ],
                    ],
                    ['class' => "form-group col-md-9",
                        'items'  => [
                            ['class' => "col-sm-3", 'items' => ":LabelSONo"],
                            ['class' => "col-sm-8", 'items' => ":SONo"],
                            ['class' => "col-sm-6", 'items' => ":SOTgl", 'style' => 'padding-left:2px;'],
                            ['class' => "col-sm-5", 'items' => ":SOJam"],
                            ['class' => "col-sm-2", 'items' => ":BtnSearch"],
                        ],
                    ],
                    ['class' => "form-group col-md-5",
                        'items'  => [
                            ['class' => "col-sm-2"],
                            ['class' => "col-sm-5", 'items' => ":LabelSDNo"],
                            ['class' => "col-sm-13", 'items' => ":SDNo"],
                            ['class' => "col-sm-2", 'items' => ":SDBtnSearch"],
                        ],
                    ],
                ],
            ],
            ['class' => "form-group col-md-24",
                'items'  => [
                    ['class' => "form-group col-md-10",
                        'items'  => [
                            ['class' => "col-sm-4", 'items' => ":LabelKonsumen"],
                            ['class' => "col-sm-5", 'items' => ":Konsumen"],
                            ['class' => "col-sm-2", 'items' => ":SearchKonsumen"],
                            ['class' => "col-sm-2", 'items' => ":SearchCus"],
                            ['class' => "col-sm-2", 'items' => ":TambahCus"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-2", 'items' => ":LabelType"],
                            ['class' => "col-sm-6", 'items' => ":Type", 'style' => 'padding-right:7px;padding-left:5px;'],
                        ],
                    ],
                    ['class' => "form-group col-md-7",
                        'items'  => [
                            ['class' => "col-sm-4", 'items' => ":LabelSales"],
                            ['class' => "col-sm-19", 'items' => ":Sales"],
                        ],
                    ],
                    ['class' => "form-group col-md-7",
                        'items'  => [
                            ['class' => "col-sm-5", 'items' => ":LabelProgram"],
                            ['class' => "col-sm-19", 'items' => ":Program"],
                        ],
                    ],
                ],
            ],
            ['class' => "form-group col-md-24",
                'items'  => [
                    ['class' => "form-group col-md-10",
                        'items'  => [
                            ['class' => "col-sm-4", 'items' => ":LabelNama"],
                            ['class' => "col-sm-11", 'items' => ":Nama"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-2", 'items' => ":LabelWarna"],
                            ['class' => "col-sm-6", 'items' => ":Warna", 'style' => 'padding-right:7px;padding-left:5px;'],
                        ],
                    ],
                    ['class' => "form-group col-md-7",
                        'items'  => [
                            ['class' => "col-sm-4", 'items' => ":LabelDealer"],
                            ['class' => "col-sm-19", 'items' => ":Dealer"],
                        ],
                    ],
                    ['class' => "form-group col-md-7",
                        'items'  => [
                            ['class' => "col-sm-5", 'items' => ":LabelTC"],
                            ['class' => "col-sm-19", 'items' => ":TC"],
                        ],
                    ],
                ],
            ],
            [
                'class' => "row col-md-24",
                'style' => "margin-bottom: 6px;",
                'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>',
            ],
            ['class' => "form-group col-md-24",
                'items'  => [
                    ['class' => "form-group col-md-12",
                        'items'  => [
                            ['class' => "col-sm-4", 'items' => ":KodeBarang"],
                            ['class' => "col-sm-10", 'items' => ":BarangNama", 'style' => 'padding-left:2px;'],
                            ['class' => "col-sm-4", 'items' => ":Gudang", 'style' => 'padding-left:2px;'],
                            ['class' => "col-sm-4", 'items' => ":Satuan", 'style' => 'padding-left:2px;'],
                            ['class' => "col-sm-2", 'items' => ":Info"],
                        ],
                    ],
                    ['class' => "form-group col-md-12",
                        'items'  => [
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-2", 'items' => ":LabelJenis"],
                            ['class' => "col-sm-4", 'items' => ":Jenis"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-1", 'items' => ":LabelTOP"],
                            ['class' => "col-sm-2", 'items' => ":TOP", 'style' => 'padding-left:4px;'],
                            ['class' => "col-sm-1", 'items' => ":LabelHari", 'style' => 'padding-left:2px;'],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-3", 'items' => ":LabelSubTotal"],
                            ['class' => "col-sm-8", 'items' => ":SubTotal"],
                        ],
                    ],
                ],
            ],
            ['class' => "form-group col-md-24",
                'items'  => [
                    ['class' => "form-group col-md-12",
                        'items'  => [
                            ['class' => "form-group col-md-24",
                                'items'  => [
                                    ['class' => "col-sm-4", 'items' => ":LabelKeterangan"],
                                    ['class' => "col-sm-13", 'items' => ":Keterangan"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-6", 'items' => ":BtnBayar"],
                                ],
                            ],
                            ['class' => "form-group col-md-24",
                                'items'  => [
                                    ['class' => "col-sm-4", 'items' => ":LabelCetak"],
                                    ['class' => "col-sm-3", 'items' => ":Cetak"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-2", 'items' => ":LabelNPWP"],
                                    ['class' => "col-sm-5", 'items' => ":NPWP"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-2", 'items' => ":LabelZnoSO"],
                                    ['class' => "col-sm-5", 'items' => ":ZnoSO"]
                                ],
                            ],
                        ],
                    ],
                    ['class' => "form-group col-md-12",
                        'items'  => [
                            ['class' => "form-group col-md-24",
                                'items'  => [
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-4", 'items' => ":LabelTotalBayar"],
                                    ['class' => "col-sm-7", 'items' => ":TotalBayar"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-3", 'items' => ":LabelDisc"],
                                    ['class' => "col-sm-2", 'items' => ":Disc"],
                                    ['class' => "col-sm-1", 'items' => ":Persen", 'style' => 'padding-left:5px;'],
                                    ['class' => "col-sm-5", 'items' => ":Nominal"],
                                ],
                            ],
                            ['class' => "form-group col-md-24",
                                'items'  => [
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-4", 'items' => ":LabelSisaBayar"],
                                    ['class' => "col-sm-7", 'items' => ":SisaBayar"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-3", 'items' => ":LabelUangMuka"],
                                    ['class' => "col-sm-8", 'items' => ":UangMuka"],
                                ],
                            ],
                            ['class' => "form-group col-md-24",
                                'items'  => [
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-4", 'items' => ":LabelStatusBayar"],
                                    ['class' => "col-sm-5", 'items' => ":StatusBayar"],
                                    ['class' => "col-sm-2", 'items' => ":BtnInfo"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-3", 'items' => ":LabelTotal"],
                                    ['class' => "col-sm-8", 'items' => ":Total"],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    ['class' => "row-no-gutters",
        'items'  => [
            [
                'class' => "col-md-12 pull-left",
                'items' => $this->render( '../_nav', [
                    'url'=> $_GET['r'].'?'.$jenis,
                    'options'=> [
                        'SINo'          => 'No SI',
                        'SONo'          => 'No SO',
                        'SDNo'          => 'No SD',
                        'SIJenis'       => 'Jenis',
                        'PosKode'       => 'Pos Kode',
                        'KodeTrans'     => 'Kode Trans',
                        'KarKode'       => 'Karyawan',
                        'PrgNama'       => 'Prg Nama',
                        'MotorNoPolisi' => 'No Polisi',
                        'MotorNoMesin' => 'No Mesin',
                        'LokasiKode'    => 'Lokasi Kode',
                        'DealerKode'    => 'Dealer Kode',
                        'SIKeterangan'  => 'Keterangan',
                        'NoGL'          => 'No Gl',
                        'Cetak'         => 'Cetak',
                        'UserID'        => 'UserID',
                    ],
                ])
            ],
            ['class' => "form-group col-md-4", 'style' => "margin-left:-70px",
                'items'  => [
                    ['class' => "col-sm-3", 'items' => ":LabelPOS"],
                    ['class' => "col-sm-12", 'items' => ":POS"],
                ],
            ],
            ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                'items'  => [
                    ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":JT"],
                    ['class' => "col-sm-4 pull-right", 'items' => ":LabelJT"],
                ],
            ],
            ['class' => "col-md-9 pull-right",
                'items'  => [
                    ['class' => "pull-right", 'items' => ":btnImport :btnPrint :btnPickingList :btnAction"],
                ],
            ],
        ],
    ],
    [
        ":LabelZnoSO"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No SO</label>',
        ":LabelSINo"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor SI</label>',
        ":LabelSITgl"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl SI</label>',
        ":LabelSIJam"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jam SI</label>',
        ":LabelSDNo"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No SD</label>',
        ":LabelKonsumen"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Konsumen</label>',
        ":LabelType"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Type</label>',
        ":LabelSales"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sales</label>',
        ":LabelNama"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama</label>',
        ":LabelWarna"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Warna</label>',
        ":LabelDealer"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Dealer</label>',
        ":LabelTC"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">TC</label>',
        ":LabelJenis"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
        ":LabelTOP"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">TOP</label>',
        ":LabelHari"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Hari</label>',
        ":LabelSubTotal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sub Total</label>',
        ":LabelTotalBayar"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total Bayar</label>',
        ":LabelKeterangan"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
        ":LabelSisaBayar"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sisa Bayar</label>',
        ":LabelStatusBayar" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status Bayar</label>',
        ":LabelDisc"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Disc Final</label>',
        ":Persen"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">%</label>',
        ":LabelTotal"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total</label>',
        ":LabelCetak"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Cetak</label>',
        ":LabelSONo"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No SO</label>',
        ":LabelProgram"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Program</label>',
        ":LabelJT"          => \common\components\General::labelGL($dsTJual['NoGL'], 'JT'),
        ":LabelPOS"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">POS</label>',
        ":LabelUangMuka"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Uang Muka</label>',
        ":SINo"             => Html::textInput('SINoView', $dsTJual['SINoView'], ['class' => 'form-control', 'readonly' => 'readonly']) .
        Html::textInput('SINo', $dsTJual['SINo'], ['class' => 'hidden']) .
        Html::hiddenInput('SIPickingNo', null, ['id' => 'SIPickingNo']) .
        FormField::decimalInput(['name' => 'SIBiayaKirim', 'config' => ['id' => 'SIBiayaKirim', 'value' => $dsTJual['SIBiayaKirim'], 'displayOptions' => ['class' => 'hidden']]]) .
        FormField::decimalInput(['name' => 'SITotalPajak', 'config' => ['id' => 'SITotalPajak', 'value' => $dsTJual['SITotalPajak'], 'displayOptions' => ['class' => 'hidden']]]),
        ":SITgl"            => FormField::dateInput(['name' => 'SITgl', 'config' => ['value' => General::asDate($dsTJual['SITgl'])]]),
        ":SIJam"            => FormField::timeInput(['name' => 'SIJam', 'config' => ['value' => $dsTJual['SITgl']]]),
        ":SOTgl"            => FormField::dateInput(['name' => 'SOTgl', 'config' => ['value' => General::asDate($dsTJual['SOTgl'])]]),
        ":SOJam"            => FormField::timeInput(['name' => 'SOJam', 'config' => ['value' => $dsTJual['SOTgl']]]),
        ":SDNo"             => Html::textInput('SDNo', $dsTJual['SDNo'], ['class' => 'form-control', 'maxlength' => '25']),
        ":Konsumen"         => Html::textInput('MotorNoPolisi', $dsTJual['MotorNoPolisi'], ['class' => 'form-control', 'maxlength' => '12']),
        ":Keterangan"       => Html::textarea('SIKeterangan', $dsTJual['SIKeterangan'], ['class' => 'form-control', 'maxlength' => '150']),
        ":Cetak"            => Html::textInput('Cetak', $dsTJual['Cetak'], ['class' => 'form-control', 'readonly' => 'readonly']),
        ":JT"               => Html::textInput('NoGL', $dsTJual['NoGL'], ['class' => 'form-control', 'readOnly' => true]),
        ":SONo"             => Html::textInput('SONo', $dsTJual['SONo'], ['class' => 'form-control', 'readonly' => 'readonly']),
        ":TOP"              => Html::textInput('SITOP', $dsTJual['SITOP'], ['class' => 'form-control']),
        ":SubTotal"         => FormField::decimalInput(['name' => 'SISubTotal', 'config' => ['id' => 'SISubTotal', 'value' => $dsTJual['SISubTotal'], 'readonly' => 'readonly']]),
        ":Disc"             => FormField::integerInput(['name' => 'SIDiscPersen', 'config' => ['id' => 'SIDiscPersen', 'value' => $dsTJual['SIDiscPersen']]]),
        ":TotalBayar"       => FormField::decimalInput(['name' => 'SITerbayar', 'config' => ['id' => 'SITerbayar', 'value' => $dsTJual['SITerbayar'], 'readonly' => 'readonly']]),
        ":Total"            => FormField::decimalInput(['name' => 'SITotal', 'config' => ['id' => 'SITotal', 'value' => $dsTJual['SITotal'], 'readonly' => 'readonly']]),
        ":Nama"             => Html::textInput('CusNama', $dsTJual['CusNama'], ['class' => 'form-control', 'readonly' => 'readonly']),
        ":Jenis"            => Html::textInput('SIJenis', $dsTJual['SIJenis'], ['class' => 'form-control', 'maxlength' => '6']),
        ":UangMuka"         => FormField::decimalInput(['name' => 'SIUangMuka', 'config' => ['id' => 'SIUangMuka', 'value' => $dsTJual['SIUangMuka'], 'readonly' => 'readonly']]),
        ":Sales"            => FormField::combo('KarKode', ['config' => ['value' => $dsTJual['KarKode']], 'extraOptions' => ['altLabel' => ['KarNama'],'altCondition'=> $altCondition,]]),
        ":Dealer"           => FormField::combo('DealerKode', ['config' => ['value' => $dsTJual['DealerKode']], 'extraOptions' => ['altLabel' => ['DealerNama']]]),
        ":POS"              => Menu::showOnlyHakAkses(['Admin', 'Auditor']) ? FormField::combo('PosKode', ['config' => ['value' => $dsTJual['PosKode']], 'extraOptions' => ['simpleCombo' => true]]) :
        Html::textInput('PosKode', $dsTJual['PosKode'], ['class' => 'form-control', 'readonly' => 'readonly']),
//        ":TC"               => FormField::combo($isInternal ? 'PenjualanTC' : 'KodeTrans', ['name' => 'KodeTrans', 'config' => ['id'=> 'KodeTrans', 'value' => $dsTJual['KodeTrans']], 'extraOptions' => ['simpleCombo' => true]]),
//        ":TC"               => FormField::combo($isInternal ? 'PenjualanTCPenjualanInternal' : 'KodeTransInvoicePenjualan', ['name' => 'KodeTrans', 'config' => ['id'=> 'KodeTrans', 'value' => $dsTJual['KodeTrans']], 'extraOptions' => ['simpleCombo' => true]]),
        ":TC"               => (($jenis == 'penjualanInternal') ?
                                  FormField::combo('PenjualanTCPenjualanInternal', ['name' => 'KodeTrans', 'config' => ['id'=> 'KodeTrans', 'value' => $dsTJual['KodeTrans']], 'extraOptions' => ['simpleCombo' => true]])
                                : (($jenis == 'invoicePenjualan') ?
                                    FormField::combo('KodeTransInvoicePenjualan', ['name' => 'KodeTrans', 'config' => ['id'=> 'KodeTrans', 'value' => $dsTJual['KodeTrans']], 'extraOptions' => ['simpleCombo' => true]])
                                : FormField::combo('KodeTransPenjualanInternal', ['name' => 'KodeTrans', 'config' => ['id'=> 'KodeTrans', 'value' => $dsTJual['KodeTrans']], 'extraOptions' => ['simpleCombo' => true]])
        )),
        ":Program"          => FormField::combo('Program', ['config' => ['id'=> 'PrgNama', 'value' => $dsTJual['PrgNama']], 'extraOptions' => ['altLabel' => ['PrgNama'], 'simpleCombo' => true]]),
        ":Type"             => Html::textInput('MotorNama', $dsTJual['MotorNama'], ['class' => 'form-control', 'readonly' => 'readonly']),
        ":Warna"            => Html::textInput('MotorWarna', $dsTJual['MotorWarna'], ['class' => 'form-control', 'readonly' => 'readonly']),
        ":KodeBarang"       => Html::textInput('KodeBarang', '', ['id' => 'KodeBarang', 'class' => 'form-control', 'readonly' => 'readonly']),
        ":BarangNama"       => Html::textInput('BarangNama', '', ['id' => 'BarangNama', 'class' => 'form-control', 'readonly' => 'readonly']),
        ":Gudang"           => Html::textInput('LokasiKode', '', ['class' => 'form-control', 'readonly' => 'readonly']),
        ":Satuan"           => Html::textInput('lblStock', 0, ['type' => 'number', 'class' => 'form-control', 'style' => 'color: blue; font-weight: bold;', 'readonly' => 'readonly']),
        ":SisaBayar"        => FormField::decimalInput(['name' => 'SisaBayar', 'config' => ['id' => 'SisaBayar', 'value' => 0, 'readonly' => 'readonly']]),
        ":StatusBayar"      => Html::textInput('StatusBayar', $dsTJual['StatusBayar'], ['id' => 'StatusBayar', 'class' => 'form-control', 'style' => 'font-weight: bold;', 'readonly' => 'readonly']),
        ":Nominal"          => FormField::numberInput(['name' => 'SIDiscFinal', 'config' => ['value' => $dsTJual['SIDiscFinal']]]),
        ":BtnBayar"         => Html::button('<i class="fa fa-thumbs-up"></i> Bayar', ['class' => 'btn btn-success', 'style' => 'width:120px', 'id' => 'BtnBayar']),
        ":BtnInfo"          => Html::button('<i class="fa fa-refresh fa-lg"></i>', ['class' => 'btn btn-default', 'id' => 'BtnStatus']),
        ":btnImport"       => $PenjualanInternal == 'Penjualan Internal' ? '' : Html::button('<i class="fa fa-arrow-circle-down"><br><br>Import</i>', ['class' => 'btn bg-navy ', 'id' => 'btnImport', 'style' => 'width:60px;height:60px']),
        ":SearchKonsumen"   => Html::button('<i class="glyphicon glyphicon-search"></i>', ['class' => 'btn btn-default', 'id' => 'BtnCariMotor']),
        ":TambahCus"        => Html::button('<i class="fa fa-user-plus"></i>', ['class' => 'btn btn-default', 'id' => 'AddKonsumen']),
        ":SearchCus"        => Html::button('<i class="glyphicon glyphicon-user"></i>', ['class' => 'btn btn-default', 'id' => 'EditKonsumen']),
        ":BtnSearch"        => '<button id="BtnSearch" type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>',
        ":SDBtnSearch"      => '<button id="SDBtnSearch" type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>',
        ":btnPickingList"   => Html::button('<i class="glyphicon glyphicon-print"><br><br> Pick List</i>', ['class' => 'btn btn-warning ' . Custom::viewClass(), 'id' => 'btnPickingList', 'style' => 'height:60px']),
        ":Info"             => Html::button('<i class="fa fa-info-circle fa-lg"></i>', ['class' => 'btn btn-default']),
        ":ZnoSO"            => Html::textInput('ZnoSO', $dsTJual['ZnoSO'], [ 'class' => 'form-control',]),
        ":LabelNPWP"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">NPWP</label>',
        ":NPWP"            => Html::textInput('NPWP', $dsTJual['NPWP'], [ 'class' => 'form-control',]),
    ],
    [
        'url_main'   => 'ttsihd',
        'url_id'     => $_GET['id'] ?? '',
        'jsBtnPrint' => false,
        'url_add'    => Ttsihd::getRoute($model->KodeTrans, ['action' => 'create'])['create'],
        'url_index'  => Ttsihd::getRoute($model->KodeTrans)['index'],
        'url_update' => Ttsihd::getRoute($model->KodeTrans, ['id' => $id, 'action' => 'update'])['update'],
        'url_delete' => Url::toRoute(['ttsihd/delete', 'id' => $_GET['id'] ?? ''] ),
    ]
);
ActiveForm::end();
?>
    </div>
<?php
$urlCustomer             = Url::toRoute(['tdcustomer/select', 'tgl1' => '1900-01-01']);
$urlCustomerCreate       = Url::toRoute(['tdcustomer/create', 'action' => 'create']);
$urlCustomerEdit         = Url::toRoute(['tdcustomer/edit']);
$urlCheckStock           = Url::toRoute(['site/check-stock-transaksi']);
$urlDaftarKonsumenSelect = Url::toRoute(['tdcustomer/select']);
$urlKonsumenData         = Url::toRoute(['tdcustomer/td', 'oper' => 'data']);
$urlDetail               = $url['detail'];
if ($dsTJual['KodeTrans'] == 'TC12' || $dsTJual['KodeTrans'] == 'TC17') {
    $barang = \common\components\General::cCmd(
        "SELECT tdbarang.BrgKode AS BrgKode, tdbarang.BrgNama, tdbarang.BrgBarCode, tdbarang.BrgGroup, tdbarang.BrgSatuan, tdbarang.BrgHrgBeli, tdbarang.BrgHrgJual, tdbarang.BrgRak1, tdbarang.BrgMinStock, tdbarang.BrgMaxStock, tdbarang.BrgStatus, SUM(SaldoQty) AS SaldoQty, tdbaranglokasi.LokasiKode
                            FROM tdbarang
                            INNER JOIN tdbaranglokasi ON tdbaranglokasi.BrgKode = tdbarang.BrgKode
                            WHERE LokasiKode NOT IN ('Retur', 'Klaim', 'Kosong', 'Retur', 'Robbing', 'Rusak', 'Penerimaan1', 'Penerimaan2')
                            GROUP BY tdbarang.BrgKode HAVING SUM(SaldoQty) > 0
                            UNION
                    SELECT
                       tdbarang.BrgKode AS BrgKode, tdbarang.BrgNama, tdbarang.BrgBarCode, tdbarang.BrgGroup, tdbarang.BrgSatuan, tdbarang.BrgHrgBeli, tdbarang.BrgHrgJual, tdbarang.BrgRak1, tdbarang.BrgMinStock, tdbarang.BrgMaxStock, tdbarang.BrgStatus, SUM(SaldoQty) AS SaldoQty, tdbaranglokasi.LokasiKode
                    FROM
                       tdbarang
                       INNER JOIN tdbaranglokasi
                          ON tdbaranglokasi.BrgKode = tdbarang.BrgKode
                       INNER JOIN ttsiit
                          ON ttsiit.BrgKode = tdbarang.BrgKode
                    WHERE ttsiit.siNo = :SINo
                    GROUP BY tdbarang.BrgKode
                    ORDER BY BrgStatus, BrgKode", [':SINo' => $dsTJual['SINo']])->queryAll();
} else {
    $barang = \common\components\General::cCmd(
        "SELECT tdbarang.BrgKode, tdbarang.BrgNama, tdbarang.BrgBarCode, tdbarang.BrgGroup, tdbarang.BrgSatuan, tdbarang.BrgHrgBeli, tdbarang.BrgHrgBeli AS BrgHrgJual, tdbarang.BrgRak1, tdbarang.BrgMinStock, tdbarang.BrgMaxStock, tdbarang.BrgStatus, SUM(SaldoQty) AS SaldoQty
                            FROM tdbarang
                            INNER JOIN tdbaranglokasi
                            ON tdbaranglokasi.BrgKode = tdbarang.BrgKode
                            WHERE LokasiKode NOT IN ('Retur', 'Klaim', 'Kosong', 'Retur', 'Robbing', 'Rusak', 'Penerimaan1', 'Penerimaan2')
                            GROUP BY tdbarang.BrgKode
                            HAVING SUM(SaldoQty) > 0
                            UNION
                            SELECT tdbarang.BrgKode, tdbarang.BrgNama, tdbarang.BrgBarCode, tdbarang.BrgGroup, tdbarang.BrgSatuan, tdbarang.BrgHrgBeli, tdbarang.BrgHrgBeli AS BrgHrgJual, tdbarang.BrgRak1, tdbarang.BrgMinStock, tdbarang.BrgMaxStock, tdbarang.BrgStatus ,SUM(SaldoQty)
                            FROM
                            tdbarang
                            INNER JOIN tdbaranglokasi
                            ON tdbaranglokasi.BrgKode = tdbarang.BrgKode
                            INNER JOIN ttsiit
                            ON ttsiit.BrgKode = tdbarang.BrgKode
                            WHERE ttsiit.siNo = :SINo
                            GROUP BY tdbarang.BrgKode
                            ORDER BY BrgStatus, BrgKode", [':SINo' => $dsTJual['SINo']])->queryAll();
}
$urlBarangAjax       = Url::toRoute(['tdbarang/ajax-combo']);
$urlSO               = Url::toRoute(['ttsohd/select','loop'=>'SI']);
$urlSD               = Url::toRoute(['ttsdhd/select']);
$urlBayar            = Url::toRoute(['bayar/save']);
$urlDetailImport     = Url::toRoute(['ttsihd/import-loop']);
$urlPrint           = $url[ 'print' ];
//$urlImport           = Url::toRoute(['ttsihd/import', 'tgl1' => Yii::$app->formatter->asDate('-5 day', 'yyyy-MM-dd')]);
$urlImport           = Url::toRoute(['ttsihd/import', 'tgl1' => Yii::$app->formatter->asDate('now', 'yyyy-MM-dd')]);
$urlRefresh          = $url['update'] . '&oper=skip-load&SINoView=' . $dsTJual['SINoView'];
$resultKas           = \abengkel\models\Ttkmhd::find()->callSP(['Action' => 'Insert']);
$resultBank          = \abengkel\models\Ttbmhd::find()->callSP(['Action' => 'Insert']);
$urlStatusOtomatis   = Url::toRoute(['ttsihd/status-otomatis', 'SINo' => $dsTJual['SINo']]);
$urlCustomerFindById = Url::toRoute(['tdcustomer/find-by-id']);
$this->registerJsVar('kodeBrang', $barang);
$this->registerJsVar('namaBarang', $barang);
$this->registerJsVar('__urlSO', $urlSO);
$this->registerJsVar('__urlSD', $urlSD);
$this->registerJsVar('__model', $dsTJual);
$this->registerJsVar('__idCustomer', $dsTJual['MotorNoPolisi']);
$urlPickingList      = Url::toRoute(['ttsihd/picking-list', 'id' => base64_encode($dsTJual['SINo'])]);
$urlPrintPickingList = Url::toRoute(['ttsihd/print-picking-list']);
$hideHargaBeli       = Menu::showOnlyHakAkses(['akuntansi', 'admin']) ? 'false' : 'true';
$this->registerJs(<<< JS

        window.BHC_BAYAR_KMNo = '{$resultKas['KMNo']}';
        window.BHC_BAYAR_BMNo = '{$resultBank['BMNo']}';
        
        $('#btnPrint').click(function (event) {
        $('#form_ttsihd_id').utilForm().submit("{$urlPrint}","POST","_blank");
        });
        
      function HitungTotal(){
         	let SITotalPajak = $('#detailGrid').jqGrid('getCol','SIPajak',false,'sum');
         	$('#SITotalPajak').utilNumberControl().val(SITotalPajak);
         	let SISubTotal = $('#detailGrid').jqGrid('getCol','Jumlah',false,'sum');
         	$('#SISubTotal').utilNumberControl().val(SISubTotal);
         	HitungBawah();
         }
      window.HitungTotal = HitungTotal;

      function HitungBawah(){
         	let SIDiscPersen = parseFloat($('#SIDiscPersen').val());
         	let SIDiscFinal = parseFloat($('#SIDiscFinal').val());
         	let SISubTotal = parseFloat($('#SISubTotal').val());
         	if(SIDiscFinal === 0){
         	SIDiscFinal = SIDiscPersen * SISubTotal / 100;
         	$('#SIDiscFinal').utilNumberControl().val(SIDiscFinal);
         	}
         	let SITotalPajak = parseFloat($('#SITotalPajak').val());
         	let SIBiayaKirim = parseFloat($('#SIBiayaKirim').val());
         	let UangMuka = parseFloat($('#SIUangMuka').val());
         	let SITotal = SISubTotal + SITotalPajak + SIBiayaKirim - SIDiscFinal - UangMuka;
         	$('#SITotal').utilNumberControl().val(SITotal);
         	let TotalBayar = parseFloat($('#SITerbayar').val());
         	let SisaBayar = SITotal - TotalBayar;
         	$('#SisaBayar').utilNumberControl().val(SisaBayar);
         	if(SisaBayar  > 0 || SITotal === 0){
         	    $('#StatusBayar').css({"background-color" : "yellow"});
         	    if(TotalBayar === 0) $('#StatusBayar').css({"background-color" : "pink"});
         	} else if(SisaBayar <= 0){
         	    $('#StatusBayar').css({"background-color" : "lime"});
         	}
         }
      window.HitungBawah = HitungBawah;

      function checkStok(BrgKode,BarangNama){
           $.ajax({
                url: '$urlCheckStock',
                type: "POST",
                data: {
                    BrgKode: BrgKode,
                },
                success: function (res) {
                    $('[name=LokasiKode]').val(res.LokasiKode);
                    $('#KodeBarang').val(BrgKode);
                    $('[name=BarangNama]').val(BarangNama);
                    $('[name=lblStock]').val(res.SaldoQty);
                },
                error: function (jXHR, textStatus, errorThrown) {

                },
                async: false
            });
          }
      window.checkStok = checkStok;
      window.useHargaBeli = function() {
        return (__isInternal && $.inArray( $('select[name=KodeTrans]').select2('data')[0].id, ['TC15','TC18','TC99','TC17'] ) > -1);
      };
     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 150,
        rowNum: 1000,
        extraParams: {
            SINo: $('input[name="SINo"]').val(),
        },
        rownumbers: true,
        loadonce:true,
        colModel: [
            { template: 'actions'  },
            {
                name: 'BrgKode',
                label: 'Kode',
                editable: true,
                width: 125,
                editor: {
                    type: 'select2',
                    data: $.map(kodeBrang, function (obj) {
                      obj.id = obj.BrgKode;
                      obj.text = obj.BrgKode;
                      return obj;
                    }),
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    console.log(data);
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            var hargaDefault = (useHargaBeli() ? parseFloat(data.BrgHrgBeli) : parseFloat(data.BrgHrgJual));
                            $('[id="'+rowid+'_'+'SIHrgJual'+'"]').utilNumberControl().val(hargaDefault);
                            $('[id="'+rowid+'_'+'SIHrgBeli'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgBeli));
                            $('[id="'+rowid+'_'+'BrgGroup'+'"]').val(data.BrgGroup);
                            window.hitungLine();
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').val(data.BrgNama); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').trigger('change');
                            $('[id="'+rowid+'_'+'SIQty'+'"]').val(1);
                            $('[id="'+rowid+'_'+'SIQty'+'"]').trigger('change');
                            checkStok($(this).val(),$('[id="'+rowid+'_'+'BrgNama'+'"]').val());
						},
						'change': function(){
                            let rowid = $(this).attr('rowid');
                            checkStok($(this).val(),$('[id="'+rowid+'_'+'BrgNama'+'"]').val());
						}
                    }
                }
            },
            {
                name: 'BrgNama',
                label: 'Nama Barang',
                width: 220,
                editable: true,
                editor: {
                    type: 'select2',
                    data: $.map(namaBarang, function (obj) {
                      obj.id = obj.BrgNama;
                      obj.text = obj.BrgNama;
                      return obj;
                    }),
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    // console.log(data);
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            var hargaDefault = (useHargaBeli() ? parseFloat(data.BrgHrgBeli) : parseFloat(data.BrgHrgJual));
                            $('[id="'+rowid+'_'+'SIHrgJual'+'"]').utilNumberControl().val(hargaDefault);
                            $('[id="'+rowid+'_'+'SIHrgBeli'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgBeli));
                            $('[id="'+rowid+'_'+'BrgGroup'+'"]').val(data.BrgGroup);
                            window.hitungLine();
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').val(data.BrgKode); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').trigger('change');
						}
                    }
                }
            },
            {
                name: 'SIQty',
                label: 'Qty',
                width: 50,
                editable: true,
                template: 'number'
            },
            {
                name: 'BrgSatuan',
                label: 'Satuan',
                width: 50,
                editable: true,
                editor:{
                    readonly: true
                }
            },
            {
                name: 'SIHrgJual',
                label: 'Harga Part',
                width: 100,
                editable: true,
                template: 'money',
                // editor: { readOnly: __isInternal }
            },
            {
                name: 'Disc',
                label: 'Disc %',
                width: 75,
                editable: true,
                template: 'number'
            },
            {
                name: 'SIDiscount',
                label: 'Nilai Disc',
                width: 100,
                editable: true,
                template: 'money'
            },
            {
                name: 'Jumlah',
                label: 'Jumlah',
                width: 120,
                editable: true,
                template: 'money',
                editor: { readOnly: true }
            },
            {
                name: 'SIHrgBeli',
                label: 'Harga Beli',
                width: 65,
                editable: true,
                template: 'money',
                hidden: {$hideHargaBeli},
                editor: { readOnly: true }
            },
            {
                name: 'SIPajak',
                label: 'Pajak',
                editable: true,
                template: 'money',
                hidden: true
            },
            {
                name: 'BrgGroup',
                label: 'Group',
                editable: true,
                hidden: true
            },
            {
                name: 'LokasiKode',
                label: 'LokasiKode',
                editable: true,
                hidden: true
            },
            {
                name: 'BrgRak1',
                label: 'BrgRak1',
                editable: true,
                hidden: true
            },
            {
                name: 'SIAuto',
                label: 'SIAuto',
                editable: true,
                hidden: true
            },
            {
                name: 'SINo',
                label: 'SINo',
                editable: true,
                hidden: true
            },
            {
                name: 'SIPickingNo',
                label: 'SIPickingNo',
                editable: true,
                hidden: true
            },
            {
                name: 'SONo',
                label: 'SONo',
                editable: true,
                hidden: true
            },
            {
                name: 'KodeTrans',
                label: 'KodeTrans',
                editable: true,
                hidden: true
            },
            {
                name: 'PrgNama',
                label: 'PrgNama',
                editable: true,
                hidden: true
            },
        ],
        listeners: {
            afterLoad: function (data) {
             window.HitungTotal();
            }
        },
        onSelectRow : function(rowid,status,e){
            if (window.rowId !== -1) return;
            if (window.rowId_selrow === rowid) return;
            if(rowid.includes('new_row') === true){
                $('[name=KodeBarang]').val('--');
                $('[name=LokasiKode]').val('Kosong');
                $('[name=BarangNama]').val('--');
                $('[name=lblStock]').val(0);
                return ;
            };
            let r = $('#detailGrid').utilJqGrid().getData(rowid).data;
			checkStok(r.BrgKode,r.BrgNama);
			window.rowId_selrow = rowid;
        }
     }).init().fit($('.box-body')).load({url: '$urlDetail'});
     window.rowId_selrow = -1;
     function hitungLine(){
            let Disc = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Disc'+'"]').val()));
            let SIHrgJual = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'SIHrgJual'+'"]').val()));
            let SIQty = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'SIQty'+'"]').val()));
            let SIDiscount = Disc*SIHrgJual*SIQty / 100;
            $('[id="'+window.cmp.rowid+'_'+'SIDiscount'+'"]').utilNumberControl().val(SIDiscount);
            $('[id="'+window.cmp.rowid+'_'+'Jumlah'+'"]').utilNumberControl().val(SIHrgJual * SIQty - SIDiscount);
            window.HitungTotal();
         }
     window.hitungLine = hitungLine;

	 $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
	     window.cmp.rowid = rowid;

	     if(rowid.includes('new_row')){
              var BrgKode = $('[id="'+rowid+'_'+'BrgKode'+'"]');
              var dtBrgKode = BrgKode.select2('data');
              if (dtBrgKode.length > 0){
                  var dt = dtBrgKode[0];
                  BrgKode.val(dt.BrgKode).trigger('change').trigger({
                    type: 'select2:select',
                    params: {
                        data: dt
                    }
                });
              }
            $('[id="'+rowid+'_'+'SIQty'+'"]').val(1);
          }

          $('[id="'+window.cmp.rowid+'_'+'SIQty'+'"]').on('keyup', function (event){
              hitungLine();
          });
          $('[id="'+window.cmp.rowid+'_'+'SIHrgJual'+'"]').on('keyup', function (event){
              hitungLine();
          });
          $('[id="'+window.cmp.rowid+'_'+'Disc'+'"]').on('keyup', function (event){
              hitungLine();
          });
         hitungLine();
        $('[id="'+rowid+'_'+'KodeTrans'+'"]').val($('#KodeTrans').val());
        $('[id="'+rowid+'_'+'PrgNama'+'"]').val($('#PrgNama').val());
	 });

	 function onFocus(){
         $('#form_ttsihd_id').attr('target','');
         var iframe = window.util.app.dialogListData.iframe();
         var mainGrid = iframe.contentWindow.jQuery('#headerGridPL').jqGrid();
         mainGrid.jqGrid().trigger('reloadGrid');
         $( document ).unbind("focus");
    }

	 $('#btnPickingList').click(function (event) {
        window.util.app.dialogListData.show({
            title: 'Picking List',
            url: '$urlPickingList',
            button: {
                ok: {
                    label: '<i class="glyphicon glyphicon-print"></i>&nbsp&nbspPrint&nbsp&nbsp',
                    className: 'btn-warning',
                    callback: function(){
                        var iframe = window.util.app.dialogListData.iframe();
                        var mainGrid = iframe.contentWindow.jQuery('#headerGridPL').jqGrid();
                        window.util.app.dialogListData.gridFn.mainGrid = mainGrid;
                        window.util.app.dialogListData.gridFn.var.multiSelect = false;
                        var selRowId = window.util.app.dialogListData.gridFn.getSelectedRow();

                        let data = selRowId.data;
                        console.log(data);
                        $('#form_ttsihd_id').attr('action','$urlPrintPickingList');
                        $('#form_ttsihd_id').attr('target','_blank');
                        $('#SIPickingNo').val(data.SIPickingNo);
                        $('#form_ttsihd_id').submit();
                        $( document ).focus(function() {
                          setTimeout(onFocus, 100);
                        });
                        return false;
                    }
                },
                cancel: {
                    label: '<i class="fa fa-ban"></i>&nbsp&nbsp Batal &nbsp',
                    className: 'btn-danger',
                    callback: function (data) {
                        window.util.app.dialogListData.dialog.modal('hide');
                    }
                }
            }
        },
        function (data) {
             $( document ).unbind("focus");
        })
    });


	 $('#SearchKonsumen').click(function () {
        window.util.app.dialogListData.show({
                title: 'Daftar Konsumen',
                url: '$urlDaftarKonsumenSelect'
            },
            function (data) {
                console.log(data);

                $.ajax({
                    type: 'POST',
                    url: '$urlKonsumenData',
                    data: {
                        'oper': 'data',
                        'id': btoa(data.id)
                    }
                }).then(function (cusData) {

                   console.log(cusData);
                   Object.keys(cusData).forEach(function(key,index) {
                            let cmp = $('input[name="'+key+'"]');
                            cmp.val(Object.values(cusData)[index]);

                   });
                });
            });
        });


	$('#btnSave').click(function (event) {
	    
	    
	    // SUBTOTAL BOLEH 0 - 01/11/2024
	    // let Nominal = parseFloat($('#SISubTotal-disp').inputmask('unmaskedvalue'));
        //    if (Nominal === 0 ){
        //        bootbox.alert({message:'Subtotal tidak boleh 0', size: 'small'});
        //        return;
        //    }
	     
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttsihd_id').attr('action','{$url['update']}');
        $('#form_ttsihd_id').submit();
    });

	  $('#SIDiscPersen').change(function (event) {
	      HitungBawah();
	  });

	   window.getUrlSO = function(){
        let SONo = $('input[name="SONo"]').val();
        return __urlSO + ((SONo === '' || SONo === '--') ? '':'&loop=SI&check=off&cmbTxt1=SONo&txt1='+SONo);
        }

        window.getUrlSD = function(){
        let SDNo = $('input[name="SDNo"]').val();
        return __urlSD + ((SDNo === '' || SDNo === '--') ? '':'&check=off&cmbTxt1=SDNo&txt1='+SDNo);
        }

    $('#BtnSearch').click(function(){
        window.util.app.dialogListData.show({
            title: 'Daftar Sales Order / Order Penjualan',
            url: getUrlSO()
        },
        function (data) {
             console.log(data);
             if (!Array.isArray(data)){
                 return;
             }
             var itemData = [];
             data.forEach(function(itm,ind) {
                let cmp = $('input[name="SONo"]');
                cmp.val(itm.header.data.SONo);
                itm.data.SONo = itm.header.data.SONo;
                itm.data.SINo = __model.SINo;
                itm.data.SIHrgJual = itm.data.SOHrgJual;
                itm.data.SIQty = itm.data.SOQty;
                itm.data.SIDiscount = itm.data.SODiscount;
                itemData.push(itm.data);
            });
            $.ajax({
             type: "POST",
             url: '$urlDetail',
             data: {
                 oper: 'batchSO',
                 data: itemData,
                 header: btoa($('#form_ttsihd_id').serialize())
             },
             success: function(data) {
               window.location.href = "{$urlRefresh}";
             }
           });
        })
    });

	   $('#SDBtnSearch').click(function(){
        window.util.app.dialogListData.show({
            title: 'Daftar Servis',
            url: getUrlSD()
        },
        function (data) {
             console.log(data);
             if (!Array.isArray(data)){
                 return;
             }
             var itemData = [];
             data.forEach(function(itm,ind) {
                let cmp = $('input[name="SDNo"]');
                cmp.val(itm.header.data.SDNo);
                itm.data.SDNo = itm.header.data.SDNo;
                itm.data.SINo = __model.SINo;
                itm.data.SIHrgJual = itm.data.SDHrgJual;
                itm.data.SIQty = itm.data.SDQty;
                itm.data.SIDiscount = itm.data.SDDiscount;
                itemData.push(itm.data);
            });
            $.ajax({
             type: "POST",
             url: '$urlDetail',
             data: {
                 oper: 'batchSD',
                 data: itemData,
                 header: btoa($('#form_ttsihd_id').serialize())
             },
             success: function(data) {
               window.location.href = "{$urlRefresh}";
             }
           });
        })
    });


    $('#BtnBayar').click(function() {
	    let SINo = $('input[name="SINo"]').val();
	    let MotorNoPolisi = $('input[name="MotorNoPolisi"]').val();
	    let CusNama = $('input[name="CusNama"]').val();
	    let SINoView = $('input[name="SINoView"]').val();
        let SisaBayar = parseFloat($('#SisaBayar').utilNumberControl().val());
        if(SINo.includes('=')){
             bootbox.alert({message:'Silahkan simpan transaksi ini dulu.',size: 'small'});
	        return;
        }
	    if (SisaBayar <= 0){
	        bootbox.alert({message:'Transaksi ini sudah terlunas',size: 'small'});
	        return;
	    }
	    $('#MyKodeTrans').val('SI');
	    $('#MyKode').val(MotorNoPolisi);
	    $('#MyNama').val(CusNama);
	    $('#MyNoTransaksi').val(SINo);
	    $('#BMMemo').val("Pembayaran [SI] Invoice Penjualan No: "+SINoView+" - "+MotorNoPolisi+" - "+CusNama);
	    $('#BMNominal').utilNumberControl().val(SisaBayar);
	    $('#BMBayar').utilNumberControl().val(SisaBayar);
        $('#modalBayar').modal('show');
    });

	$('#btnSavePopup').click(function(){
	    $.ajax({
            type: 'POST',
            url: '$urlBayar',
            data: $('#form_bayar_id').utilForm().getData(true)
        }).then(function (cusData) {
            location.reload();
        });
	});

	$('#EditKonsumen').click(function() {
	    let MotorNoPolisi = $('input[name="MotorNoPolisi"]').val();
        window.openCustomer(MotorNoPolisi);
    });

    $('#AddKonsumen').click(function() {
        window.openCustomer('--');
    });

    $('#BtnStatus').click(function() {
        window.util.app.popUpForm.show({
        title: "Daftar Transaksi Keuangan yang melibatkan Nomor SI : "+$('input[name="SINo"]').val(),
        size: 'small',
        url: '$urlStatusOtomatis',
        width: 900,
        height: 320
        },
        function(data) {
            console.log(data);
        });
    });

    let fillCustomer = function(id, data) {
        __idCustomer = id;
        $('[name=MotorNoPolisi]').val(data.MotorNoPolisi);
        $('[name=CusNama]').val(data.CusNama);
        $('[name=SDPembawaMotor]').val(data.CusNama);
        $('[name=MotorWarna]').val(data.MotorWarna);
        $('[name=MotorNama]').val(data.MotorNama);
    }

    $('[name=MotorNoPolisi]').change(function(){
        $.ajax({
            url: '$urlCustomerFindById',
            type: "GET",
            data: { id: $(this).val() },
            success: function (res) {
                //console.log(res);
                fillCustomer(res.MotorNoPolisi, res);
            },
            error: function (jXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            },
            async: false
        });
    });

    $('#BtnCariMotor').click(function() {
        window.util.app.dialogListData.show({
            title: 'Daftar Konsumen',
            url: '$urlCustomer'
        },
        function (data) {
            console.log(data);
            if(data.data === undefined){
                return ;
            }
            __idCustomer = data.id;
            $('[name=MotorNoPolisi]').val(data.data.MotorNoPolisi).trigger('change');
        })
    });

    $( document ).ready(function() {
        if(__model['KodeTrans'] == 'TC15'|| __model['KodeTrans'] == 'TC18'|| __model['KodeTrans'] == 'TC99'){
            $('#BtnBayar').attr('disabled',true);
            $('#BtnOPL').attr('disabled',true);
        }
    });

    $('#btnImport').click(function (event) {
        window.util.app.dialogListData.show({
            title: 'Manage Parts Sales - PRSL - SI',
            url: '{$urlImport}'
        },
        function (data) {
			if(!Array.isArray(data)) return;
            $.ajax({
				type: "POST",
				url: '$urlDetailImport',
				data: {
					oper: 'import',
					data: data,
					header: btoa($('#form_ttsihd_id').serialize())
				},
				success: function(data) {
                    window.location.href = "{$url['update']}&oper=skip-load&SINoView={$dsTJual['SINoView']}";
				},
			});
        });
    });
    

JS
);
echo $this->render('../_formBayar');
echo $this->render('../_formCustomer');
