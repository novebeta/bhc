<?php
use abengkel\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = '[SI] Penjualan Internal';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'SINo'          => 'No SI',
		'SONo'          => 'No SO',
		'SDNo'          => 'No SD',
		'SIJenis'       => 'Jenis',
		'PosKode'       => 'Pos Kode',
		'KodeTrans'     => 'Kode Trans',
		'KarKode'       => 'Karyawan',
		'PrgNama'       => 'Prg Nama',
		'MotorNoPolisi' => 'No Polisi',
        'MotorNoMesin' => 'No Mesin',
		'LokasiKode'    => 'Lokasi Kode',
		'DealerKode'    => 'Dealer Kode',
		'SIKeterangan'  => 'Keterangan',
		'NoGL'          => 'No Gl',
		'Cetak'         => 'Cetak',
		'UserID'        => 'UserID',
	],
	'cmbTgl'    => [
		'SITgl' => 'Tanggal SI',
	],
	'cmbNum'    => [
		'SISubTotal'  => 'Sub Total',
		'SIDiscFinal' => 'Disc Final',
		'SIUangMuka'  => 'Uang Muka',
		'SITotal'     => 'Total',
		'SITOP'       => 'top',
		'SITerbayar'  => 'Terbayar',
	],
	'sortname'  => "SITgl",
	'sortorder' => 'desc'
];
if ( \abengkel\components\Menu::showOnlyHakAkses( [ 'warehouse', 'gudang' ] ) ) {
	$arr[ 'cmbTxt' ][ 'PLCetak' ] = 'PLCetak';
}
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Ttsihd::class, '[SI] Penjualan Internal', [
	'url_add'    => Url::toRoute( [ 'ttsihd/penjualan-internal-create', 'action' => 'create' ] ),
	'url_update' => Url::toRoute( [ 'ttsihd/penjualan-internal-update', 'action' => 'update' ] ),
	'url_view'   => Url::toRoute( [ 'ttsihd/penjualan-internal-update', 'action' => 'view' ] ),
] ), \yii\web\View::POS_READY );
