<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttsehd */
$params                          = '&id=' . $id . '&action=create';
$cancel                          = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = 'Tambah - Estimasi Servis';
?>
<div class="ttsehd-create">
    <?= $this->render('_form', [
        'model'     => $model,
        'dsTServis' => $dsTServis,
        'id'        => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttseitbarang/index','action' => 'create'] ),
        ]
    ]) ?>

</div>
