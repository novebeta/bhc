<?php
use abengkel\components\FormField;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttsehd */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
if ($.trim(params.term) === '') {
return data;
}
if (typeof data.text === 'undefined') {
return null;
}
if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
var modifiedData = $.extend({}, data, true);
return modifiedData;
}
return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttsehd-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_ttsehd_id', 'action' => $url[ 'update' ] ] );
		\abengkel\components\TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-4", 'items' => ":LabelNoEstimasi" ],
						      [ 'class' => "col-sm-10", 'items' => ":NoEstimasi" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelSETgl", 'style' => 'padding-left:5px;' ],
						      [ 'class' => "col-sm-4", 'items' => ":SETgl" ],
						      [ 'class' => "col-sm-4", 'items' => ":SEJam" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelSDNo" ],
						      [ 'class' => "col-sm-4", 'items' => ":SDNo" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelRiwayat", 'style' => 'padding-left:3px;' ],
						      [ 'class' => "col-sm-1", 'items' => ":SearchRiwayat", 'style' => 'padding-left:6px;' ],
						      [ 'class' => "col-sm-2" ],
						      [ 'class' => "col-sm-3", 'items' => ":LabelTC" ],
						      [ 'class' => "col-sm-9", 'items' => ":TC" ]
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-4", 'items' => ":LabelNoPolisi" ],
						      [ 'class' => "col-sm-4", 'items' => ":NoPolisi" ],
						      [ 'class' => "col-sm-2", 'items' => ":BtnCariMotor" ],
						      [ 'class' => "col-sm-2", 'items' => ":BtnEditMotor" ],
						      [ 'class' => "col-sm-2", 'items' => ":BtnNewMotor" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelNama", 'style' => 'padding-left:5px;' ],
						      [ 'class' => "col-sm-8", 'items' => ":Nama" ]
					      ],
					    ],
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelTypeMotor" ],
						      [ 'class' => "col-sm-8", 'items' => ":TypeMotor" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-3", 'items' => ":LabelWarnaMotor" ],
						      [ 'class' => "col-sm-9", 'items' => ":WarnaMotor" ],
					      ],
					    ],
				    ],
				  ],
				  [
					  'class' => "col-md-24",
					  'style' => "margin-bottom: 6px;",
					  'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-14" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelTotalWaktu" ],
					    [ 'class' => "col-sm-2", 'items' => ":TotalWaktu:StringTotalWaktu" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelTotalJasa" ],
					    [ 'class' => "col-sm-3", 'items' => ":TotalJasa" ],
				    ],
				  ],
				  [
					  'class' => "col-md-24",
					  'style' => "margin-bottom: 6px;",
					  'items' => '<table id="scnDetailGrid"></table><div id="scnDetailGridPager"></div>'
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-13",
					      'items' => [
						      [ 'class' => "col-sm-5", 'items' => ":lblMyBrgKode", 'style' => 'padding-right:2px;' ],
						      [ 'class' => "col-sm-8", 'items' => ":lblBrgNama", 'style' => 'padding-right:2px;' ],
						      [ 'class' => "col-sm-5", 'items' => ":lblLokasiKode", 'style' => 'padding-right:2px;' ],
						      [ 'class' => "col-sm-4", 'items' => ":lblStock", 'style' => 'padding-right:2px;' ],
						      [ 'class' => "col-sm-1", 'items' => ":BtnInfo" ]
					      ],
					    ],
					    [ 'class' => "form-group col-md-11",
					      'items' => [
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-4", 'items' => ":LabelCetak" ],
						      [ 'class' => "col-sm-3", 'items' => ":Cetak" ],
						      [ 'class' => "col-sm-3", 'items' => ":LabelQtyTotal", 'style' => 'padding-left:5px;' ],
						      [ 'class' => "col-sm-2", 'items' => ":QtyTotal" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-4", 'items' => ":LabelPartTotal" ],
						      [ 'class' => "col-sm-6", 'items' => ":PartTotal" ],
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-13",
				    'items' => [
					    [ 'class' => "col-sm-4", 'items' => ":LabelSales" ],
					    [ 'class' => "col-sm-16", 'items' => ":Sales" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-3", 'items' => ":BtnBayar" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-11",
				    'items' => [
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-4", 'items' => ":LabelTotalBayar" ],
					    [ 'class' => "col-sm-8", 'items' => ":TotalBayar" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-4", 'items' => ":LabelEstimasiBiaya" ],
					    [ 'class' => "col-sm-6", 'items' => ":EstimasiBiaya" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-13",
				    'items' => [
					    [ 'class' => "col-sm-4", 'items' => ":LabelKeterangan" ],
					    [ 'class' => "col-sm-16", 'items' => ":Keterangan" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-11",
				    'items' => [
					    [ 'class' => "form-group col-md-24",
					      'items' => [
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-4", 'items' => ":LabelSisaBayar" ],
						      [ 'class' => "col-sm-8", 'items' => ":SisaBayar" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-24",
					      'items' => [
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-4", 'items' => ":LabelStatusBayar" ],
						      [ 'class' => "col-sm-6", 'items' => ":StatusBayar" ],
						      [ 'class' => "col-sm-1", 'items' => ":BtnRefresh" ],
					      ],
					    ],
				    ],
				  ],
			  ]
			],
            [ 'class' => "row-no-gutters",
                'items' => [
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'SENo'          => 'No SE',
                                'SDNo'          => 'No SD',
                                'KodeTrans'     => 'Kode Trans',
                                'MotorNoPolisi' => 'No Polisi',
                                'KarKode'       => 'Karyawan',
                                'SEKeterangan'  => 'Keterangan',
                                'Cetak'         => 'Cetak',
                                'UserID'        => 'UserID',
                            ],
                        ])
                    ],
			        ['class' => "pull-right", 'items' => ":btnPrint :btnAction"],
                ]
            ],
			[
				":LabelNoEstimasi"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Estimasi</label>',
				":LabelSETgl"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl SE</label>',
				":LabelSDNo"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">No SD</label>',
				":LabelRiwayat"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Riwayat</label>',
				":LabelTC"            => '<label class="control-label" style="margin: 0; padding: 6px 0;">TC</label>',
				":LabelNoPolisi"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">No. Polisi</label>',
				":LabelNama"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama</label>',
				":LabelTypeMotor"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Type</label>',
				":LabelWarnaMotor"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Warna</label>',
				":LabelTotalWaktu"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total Waktu</label>',
				":LabelTotalJasa"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total Jasa</label>',
				":LabelCetak"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Cetak</label>',
				":LabelQtyTotal"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total Qty</label>',
				":LabelPartTotal"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total Part</label>',
				":LabelSales"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sales</label>',
				":LabelTotalBayar"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total Bayar</label>',
				":LabelEstimasiBiaya" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Estimasi Biaya</label>',
				":LabelKeterangan"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelSisaBayar"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sisa Bayar</label>',
				":LabelStatusBayar"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status Bayar</label>',
				":NoEstimasi" => Html::textInput( 'SENoView', $dsTServis[ 'SENoView' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ) .
				                 Html::hiddenInput( 'SENo', $dsTServis[ 'SENo' ] ),
				":SETgl"      => FormField::dateInput( [ 'name' => 'SETgl', 'config' => [ 'value' => General::asDate( $dsTServis[ 'SETgl' ] ) ] ] ),
				":SEJam"      => FormField::timeInput( [ 'name' => 'SEJam', 'config' => [ 'value' => $dsTServis[ 'SETgl' ] ] ] ),
				":NoPolisi"   => Html::textInput( 'MotorNoPolisi', $dsTServis[ 'MotorNoPolisi' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":SDNo"       => Html::textInput( 'SDNo', $dsTServis[ 'SDNo' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":TC"         => FormField::combo( 'SETC', [ 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":Nama"       => Html::textInput( 'CusNama', $dsTServis[ 'CusNama' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":TypeMotor"  => Html::textInput( 'MotorType', $dsTServis[ 'MotorType' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":WarnaMotor" => Html::textInput( 'MotorWarna', $dsTServis[ 'MotorWarna' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":TotalWaktu"       => Html::hiddenInput( 'SETotalWaktu', $dsTServis[ 'SETotalWaktu' ], [ 'class' => 'form-control', 'id' => 'SETotalWaktu' ] ),
				":StringTotalWaktu" => Html::textInput( 'StringTotalWaktu', $dsTServis[ 'SETotalWaktu' ], [ 'class' => 'form-control', 'id' => 'StringTotalWaktu', 'readonly' => 'readonly' ] ),
				":TotalJasa"        => FormField::numberInput( [ 'name' => 'SETotalJasa', 'config' => [ 'value' => $dsTServis[ 'SETotalJasa' ], 'readonly' => 'readonly' ] ] ),
				":lblMyBrgKode"  => Html::textInput( 'KodeBarang', '--', [ 'class' => 'form-control', 'style' => 'color: blue;', 'readonly' => 'readonly' ] ),
				":lblBrgNama"    => Html::textInput( 'BarangNama', '--', [ 'class' => 'form-control', 'style' => 'color: blue;', 'readonly' => 'readonly' ] ),
				":lblLokasiKode" => Html::textInput( 'LokasiKode', 'Kosong', [ 'class' => 'form-control', 'style' => 'color: blue;', 'readonly' => 'readonly' ] ),
				":lblStock"      => Html::textInput( 'lblStock', 0, [ 'type' => 'number', 'class' => 'form-control', 'style' => 'color: blue; font-weight: bold;', 'readonly' => 'readonly' ] ),
				":Cetak"         => Html::textInput( 'Cetak', $dsTServis[ 'Cetak' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":PartTotal"     => FormField::numberInput( [ 'name' => 'SETotalPart', 'config' => [ 'value' => $dsTServis[ 'SETotalPart' ], 'readonly' => 'readonly' ] ] ),
				":Sales"         => FormField::combo( 'KarKode', [ 'config' => [ 'value' => 'KarKode' ], 'extraOptions' => [ 'altLabel' => [ 'KarNama' ] ] ] ),
				":EstimasiBiaya" => FormField::numberInput( [ 'name' => 'SETotalBiaya', 'config' => [ 'value' => $dsTServis[ 'SETotalBiaya' ] ] ] ),
				":Keterangan"    => Html::textarea( 'SEKeterangan', $dsTServis[ 'SEKeterangan' ], [ 'class' => 'form-control', 'maxlength' => '150' ] ),
				":TotalBayar"    => FormField::numberInput( [ 'name' => 'SETerbayar', 'config' => [ 'value' => $dsTServis['SETerbayar'], 'readonly' => 'readonly','id'=>'SETerbayar' ] ] ),
				":QtyTotal"      => Html::textInput( 'QtyTotal', 0, [ 'id' => 'QtyTotal', 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":SisaBayar"     => FormField::numberInput( [ 'name' => 'SisaBayar', 'config' => [ 'value' => 0.00, 'readonly' => 'readonly','id'=>'SisaBayar' ] ] ),
				":StatusBayar"   => Html::textInput( 'StatusBayar', 'Belum', [ 'class' => 'form-control', 'readonly' => 'readonly','id'=>'StatusBayar' ] ),
				":BtnCariMotor"  => Html::button( '<i class="glyphicon glyphicon-search"></i>', [ 'class' => 'btn btn-default', 'id' => 'BtnCariMotor' ] ),
				":BtnEditMotor"  => Html::button( '<i class="glyphicon glyphicon-user"></i>', [ 'class' => 'btn btn-default', 'id' => 'BtnEditMotor' ] ),
				":BtnNewMotor"   => Html::button( '<i class="fa fa-user-plus"></i>', [ 'class' => 'btn btn-default', 'id' => 'BtnNewMotor' ] ),
				":BtnBayar"      => Html::button( '<i class="fa fa-thumbs-up"></i> Bayar', [ 'class' => 'btn btn-success', 'id' => 'BtnBayar' ] ),
				":BtnInfo"       => '<button type="button" class="btn btn-default"><i class="fa fa-info-circle fa-lg"></i></button>',
				":BtnRefresh"    => '<button type="button" class="btn btn-default"><i class="fa fa-refresh fa-lg"></i></button>',
				":SearchRiwayat" => '<button type="button" class="btn btn-default"><i class="fa fa-clock-o fa-lg"></i></button>',
			],
			[
				'url_main' => 'ttsehd',
                'url_id' => $_GET['id'] ?? '',
                '_akses' => '[SE] Estimasi Servis',
			]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlCheckStock = Url::toRoute( [ 'site/check-stock-transaksi' ] );
$jasa = \common\components\General::cCmd(
        "SELECT JasaKode, JasaNama, JasaGroup, JasaHrgBeli, JasaHrgJual, JasaWaktuKerja, JasaWaktuSatuan, JasaWaktuKerjaMenit, JasaStatus, JasaKeterangan FROM tdjasa
WHERE JasaStatus = 'A' 
UNION
SELECT JasaKode, JasaNama, JasaGroup, JasaHrgBeli, JasaHrgJual, JasaWaktuKerja, JasaWaktuSatuan, JasaWaktuKerjaMenit, JasaStatus, JasaKeterangan FROM tdjasa
WHERE JasaKode IN 
(SELECT ttSEitjasa.JasaKode FROM ttSEitjasa INNER JOIN tdjasa ON ttSEitjasa.JasaKode = tdjasa.JasaKode 
 WHERE ttSEitjasa.SENo = :SENo AND tdjasa.JasaStatus = 'N' GROUP BY ttSEitjasa.JasaKode )
ORDER BY JasaStatus, JasaKode", [ ':SENo' => $dsTServis[ 'SENo' ]])->queryAll();
$this->registerJsVar( '__Jasa', json_encode( $jasa ) );
$this->registerJsVar( '__urlDetailJasa', Url::toRoute( [ 'ttseitjasa/index' ] ) );
$barang = \common\components\General::cCmd(
        "SELECT BrgKode, BrgNama, BrgBarCode, BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual, BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus 
FROM tdbarang 
WHERE BrgStatus = 'A' 
UNION
SELECT BrgKode, BrgNama, BrgBarCode,  BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual, BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus FROM tdBarang 
WHERE BrgKode IN 
(SELECT ttseitBarang.BrgKode FROM ttseitbarang INNER JOIN tdbarang ON ttseitbarang.BrgKode = tdbarang.BrgKode  
 WHERE ttseitBarang.SENo = :SENo AND tdBarang.BrgStatus = 'N' GROUP BY ttseitBarang.BrgKode )
ORDER BY BrgStatus, BrgKode", [ ':SENo' => $dsTServis[ 'SENo' ]] )->queryAll();
$this->registerJsVar( '__Barang', json_encode( $barang ) );
$this->registerJsVar( '__urlDetailBarang', Url::toRoute( [ 'ttseitbarang/index' ] ) );
$readOnly   = ( $_GET[ 'action' ] == 'view' ? 'true' : 'false' );
$urlBayar   = Url::toRoute( [ 'bayar/save' ] );
$urlRefresh = $url[ 'update' ] . '&oper=skip-load&SENoView=' . $dsTServis[ 'SENoView' ];
$resultKas  = \abengkel\models\Ttkmhd::find()->callSP( [ 'Action' => 'Insert' ] );
$resultBank = \abengkel\models\Ttbmhd::find()->callSP( [ 'Action' => 'Insert' ] );
$this->registerJs( <<< JS
/*--JASA--*/

      window.BHC_BAYAR_KMNo = '{$resultKas['KMNo']}';
      window.BHC_BAYAR_BMNo = '{$resultBank['BMNo']}';

     window.HitungBawah = function (){
            let TotalJasaPart = parseFloat($('#SETotalPart').val()) + parseFloat($('#SETotalJasa').val());
            let result = window.util.calculatePrice({
                qty: 1,
                price: TotalJasaPart,
                disc: $('#SEDiscPersen').val(),
                discRp: $('#SEDiscFinal').val()
            });                 
            // $('#TotalJasaPart').utilNumberControl().val(TotalJasaPart);               
            //  $('#SEDiscPersen').utilNumberControl().val(result.disc);
            //  $('#SEDiscFinal').utilNumberControl().val(result.discRp);
            /*
            let SEUangMuka = parseFloat($('#SEUangMuka').val());                
            let SETotalBiaya = result.total - SEUangMuka;
            */
            let SETotalBiaya = TotalJasaPart;
            $('#SETotalBiaya').utilNumberControl().val(SETotalBiaya);
            
            let TotalBayar = parseFloat($('#SETerbayar').val());   
            let SisaBayar = SETotalBiaya - TotalBayar;
            $('#SisaBayar').utilNumberControl().val(SisaBayar);
            if(SisaBayar  > 0 || SETotalBiaya === 0){         	    
         	    $('#StatusBayar').css({"background-color" : "yellow"});
         	    if(TotalBayar === 0) $('#StatusBayar').css({"background-color" : "pink"});
         	} else if(SisaBayar <= 0){
         	    $('#StatusBayar').css({"background-color" : "lime"});
         	}  
        }   
        
     window.hitungLineJasa = function (){
        //console.log('hitung');   
        let result = window.util.calculatePrice({
            qty: 1,
            price: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Jasa+'_'+'SEHrgJual'+'"]').val()),
            disc: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Jasa+'_'+'Disc'+'"]').val()),
            discRp: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Jasa+'_'+'SEJualDisc'+'"]').val())
        })
        $('[id="'+window.cmp.rowid_Jasa+'_'+'Disc'+'"]').val(result.disc);
        $('[id="'+window.cmp.rowid_Jasa+'_'+'SEJualDisc'+'"]').val(result.discRp);
        $('[id="'+window.cmp.rowid_Jasa+'_'+'Jumlah'+'"]').val(result.total);
        window.HitungTotalJasa();
    };
    
     window.HitungTotalJasa = function (){
        let SETotalWaktu = dgJasa.jqGrid('getCol','SEWaktuKerja',false,'sum');
        let SETotalJasa = dgJasa.jqGrid('getCol','Jumlah',false,'sum');
        $('#SETotalWaktu').utilNumberControl().val(SETotalWaktu);
        $('#StringTotalWaktu').val(window.util.minuteToString(SETotalWaktu));
        $('#SETotalJasa').utilNumberControl().val(SETotalJasa);
        window.HitungBawah();
    };
    
 
    var __JasaKode = JSON.parse(__Jasa);
    __JasaKode = $.map(__JasaKode, function (obj) {
                  obj.id = obj.JasaKode;
                  obj.text = obj.JasaKode;
                  return obj;
                });
    var __JasaNama = JSON.parse(__Jasa);
    __JasaNama = $.map(__JasaNama, function (obj) {
                  obj.id = obj.JasaNama;
                  obj.text = obj.JasaNama;
                  return obj;
                });
    var dgJasa = $('#detailGrid');
     dgJasa.utilJqGrid({
        editurl: __urlDetailJasa,
        height: 120,
        extraParams: {
            SENo: '$model->SENo'
        },
        rownumbers: true,      
        colModel: [
            { template: 'actions'},
            {
                name: 'JasaKode',
                label: 'Kode',
                width: 115,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __JasaKode,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    // console.log(data);
						    let rowid = $(this).attr('rowid');
                            $('[id="'+rowid+'_'+'SEWaktuKerja'+'"]').val(data.JasaWaktuKerja);
						    $('[id="'+rowid+'_'+'SEWaktuSatuan'+'"]').val(data.JasaWaktuSatuan);
                            $('[id="'+rowid+'_'+'SEHrgJual'+'"]').utilNumberControl().val(data.JasaHrgJual);
                            $('[id="'+rowid+'_'+'JasaNama'+'"]').val(data.JasaNama).trigger('change');
                            window.hitungLineJasa();
						}
                    }                 
                }
            },
            {
                name: 'JasaNama',
                label: 'Nama Jasa',
                width: 232,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __JasaNama,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    // console.log(data);
						    let rowid = $(this).attr('rowid');
                            $('[id="'+rowid+'_'+'SEWaktuKerja'+'"]').val(data.JasaWaktuKerja);
						    $('[id="'+rowid+'_'+'SEWaktuSatuan'+'"]').val(data.JasaWaktuSatuan);
                            $('[id="'+rowid+'_'+'SEHrgJual'+'"]').utilNumberControl().val(data.JasaHrgJual);
                            $('[id="'+rowid+'_'+'JasaKode'+'"]').val(data.JasaKode).trigger('change');
                            window.hitungLineJasa();
						}
                    }              
                }
            },
            {
                name: 'SEWaktuKerja',
                label: 'Waktu',
                editable: true,
                template: 'number',
                width: 100,
                editor: { readOnly: true }
            },     
            {
                name: 'SEWaktuSatuan',
                label: 'Satuan',
                editable: true,
                width: 80,
                editor: { type: 'text', readOnly: true }
            },      
            {
                name: 'SEHrgJual',
                label: 'Harga Jasa',
                editable: true,
                width: 110,
                template: 'money',
                editor: {
                    events:{
                        'change': function (e) {window.hitungLineJasa();}
                    }              
                }
            },   
            {
                name: 'Disc',
                label: 'Disc %.',
                editable: true,
                template: 'number',
                width: 70,
                editor: {
                    events:{
                        'change': function (e) {
                            let result = window.util.calculatePrice({
                                qty: 1,
                                price: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Jasa+'_'+'SEHrgJual'+'"]').val()),
                                disc: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Jasa+'_'+'Disc'+'"]').val()),
                            })
                            $('[id="'+window.cmp.rowid_Jasa+'_'+'SEJualDisc'+'"]').utilNumberControl().val(result.discRp);
                            $('[id="'+window.cmp.rowid_Jasa+'_'+'Jumlah'+'"]').utilNumberControl().val(result.total);
                            window.hitungLineJasa();
                        }
                    }              
                }
            },     
            {
                name: 'SEJualDisc',
                label: 'Discount',
                width: 110,
                editable: true,
                template: 'money',
                editor: {
                    events:{
                        'change': function (e) {                            
                            let result = window.util.calculatePrice({
                                qty: 1,
                                price: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Jasa+'_'+'SEHrgJual'+'"]').val()),
                                discRp: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Jasa+'_'+'SEJualDisc'+'"]').val()),
                            })
                            $('[id="'+window.cmp.rowid_Jasa+'_'+'Disc'+'"]').utilNumberControl().val(result.disc);
                            $('[id="'+window.cmp.rowid_Jasa+'_'+'Jumlah'+'"]').utilNumberControl().val(result.total);
                            window.hitungLineJasa();
                        }
                    }              
                }
            },
            {
                name: 'Jumlah',
                label: 'Jumlah',
                width: 110,
                editable: true,
                template: 'money',
                editor: { readOnly: true }
            }
        ],
        listeners: {
            afterLoad: function (data) {
                window.HitungTotalJasa();
            }     
        }  
     }).init().fit($('.box-body')).load({url: __urlDetailJasa});
     
    dgJasa.bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
        window.cmp.rowid_Jasa = rowid;    
        if(rowid === "new_row"){
            $('[id="'+rowid+'_'+'SEWaktuKerja'+'"]').val(0);
            $('[id="'+rowid+'_'+'SEWaktuSatuan'+'"]').val('MENIT');
            $('[id="'+rowid+'_'+'Disc'+'"]').val(0.00);
        }
    });
    
   
     
/*--BARANG--*/
 window.hitungLineBarang = function (){
        //console.log('hitung');   
        let result = window.util.calculatePrice({
            qty: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Barang+'_'+'SEQty'+'"]').val()),
            price: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Barang+'_'+'SEHrgJual'+'"]').val()),
            disc: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Barang+'_'+'Disc'+'"]').val()),
            discRp: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Barang+'_'+'SEJualDisc'+'"]').val()),
        })
        $('[id="'+window.cmp.rowid_Barang+'_'+'Disc'+'"]').utilNumberControl().val(result.disc);
        $('[id="'+window.cmp.rowid_Barang+'_'+'SEJualDisc'+'"]').utilNumberControl().val(result.discRp);
        $('[id="'+window.cmp.rowid_Barang+'_'+'Jumlah'+'"]').val(result.total);
        window.HitungTotalBarang();
    };
     
    window.HitungTotalBarang = function (){
        let SETotalQty = dgBarang.jqGrid('getCol','SEQty',false,'sum');
        let SETotalPart = dgBarang.jqGrid('getCol','Jumlah',false,'sum');
        $('#QtyTotal').utilNumberControl().val(SETotalQty);
        $('#SETotalPart').utilNumberControl().val(SETotalPart);
        window.HitungBawah();
    }
    var __BrgKode = JSON.parse(__Barang);
    __BrgKode = $.map(__BrgKode, function (obj) {
                  obj.id = obj.BrgKode;
                  obj.text = obj.BrgKode;
                  return obj;
                });
    var __BrgNama = JSON.parse(__Barang);
    __BrgNama = $.map(__BrgNama, function (obj) {
                  obj.id = obj.BrgNama;
                  obj.text = obj.BrgNama;
                  return obj;
                });
    var dgBarang = $('#scnDetailGrid');
     dgBarang.utilJqGrid({
        editurl: __urlDetailBarang,
        height: 120,
        extraParams: {
            SENo: '$model->SENo'
        },
        rownumbers: true,        
        loadonce:true,
        rowNum: 1000,
         
        colModel: [
            { template: 'actions' },
            {
                name: 'BrgKode',
                label: 'Kode',
                width: 115,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __BrgKode,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    // console.log(data);
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'SEHrgJual'+'"]').utilNumberControl().val(data.BrgHrgJual);
                            $('[id="'+rowid+'_'+'SEHrgBeli'+'"]').utilNumberControl().val(data.BrgHrgBeli);
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').val(data.BrgNama); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').trigger('change');
                            window.hitungLineBarang();
						}
                    }                 
                }
            },
            {
                name: 'BrgNama',
                label: 'Nama Barang',
                width: 232,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __BrgNama,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    // console.log(data);
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'SEHrgJual'+'"]').utilNumberControl().val(data.BrgHrgJual);
                            $('[id="'+rowid+'_'+'SEHrgBeli'+'"]').utilNumberControl().val(data.BrgHrgBeli);
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').val(data.BrgKode); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').trigger('change');
                            window.hitungLineBarang();
						}
                    }              
                }
            },
            {
                name: 'SEQty',
                label: 'Qty.',
                editable: true,
                template: 'number',
                width: 100,
                editor: {
                    events:{
                        'change': function (e) {window.hitungLineBarang();}
                    }              
                }
            },     
            {
                name: 'BrgSatuan',
                label: 'Satuan',
                editable: true,
                width: 80,
                editor: { type: 'text', readOnly: true }
            },      
            {
                name: 'SEHrgJual',
                label: 'Harga Part',
                editable: true,
                template: 'money',
                width: 110,
                editor: {
                    events:{
                        'change': function (e) {window.hitungLineBarang();}
                    }              
                }
            },   
            {
                name: 'Disc',
                label: 'Disc %.',
                editable: true,
                template: 'number',
                width: 70,
                editor: {
                    events:{
                        'change': function (e) {                            
                            let result = window.util.calculatePrice({
                                qty: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Barang+'_'+'SEQty'+'"]').val()),
                                price: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Barang+'_'+'SEHrgJual'+'"]').val()),
                                disc: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Barang+'_'+'Disc'+'"]').val()),
                            })
                            $('[id="'+window.cmp.rowid_Barang+'_'+'SEJualDisc'+'"]').utilNumberControl().val(result.discRp);
                            $('[id="'+window.cmp.rowid_Barang+'_'+'Jumlah'+'"]').utilNumberControl().val(result.total);
                            window.hitungLineBarang();
                        }
                    }              
                }
            },     
            {
                name: 'SEJualDisc',
                label: 'Nilai Disc.',
                width: 110,
                editable: true,
                template: 'money',
                editor: {
                    events:{
                        'change': function (e) {                            
                            let result = window.util.calculatePrice({
                                qty: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Barang+'_'+'SEQty'+'"]').val()),
                                price: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Barang+'_'+'SEHrgJual'+'"]').val()),
                                discRp: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Barang+'_'+'SEJualDisc'+'"]').val()),
                            })
                            $('[id="'+window.cmp.rowid_Barang+'_'+'Disc'+'"]').utilNumberControl().val(result.disc);
                            $('[id="'+window.cmp.rowid_Barang+'_'+'Jumlah'+'"]').utilNumberControl().val(result.total);
                            window.hitungLineBarang();
                        }
                    }              
                }
            },
            {
                name: 'Jumlah',
                label: 'Jumlah',
                width: 110,
                editable: true,
                template: 'money',
                editor: { readOnly: true }
            },      
            {
                name: 'SEHrgBeli',
                label: 'Harga Beli',
                editable: true,
                template: 'money',
                width: 110,
                editor: { readOnly: true }
            }
        ],
        listeners: {
            afterLoad: function (data) {
            window.HitungTotalBarang();
            }    
        },
        /*
        loadComplete: function (data) {
            window.HitungTotalBarang();
        },*/
        onSelectRow : function(rowid,status,e){
            if (window.rowId !== -1) return;
            if (window.rowId_selrow === rowid) return;
            if(rowid.includes('new_row') === true){
                $('[name=KodeBarang]').val('--');
                $('[name=LokasiKode]').val('Kosong');
                $('[name=BarangNama]').val('--');
                $('[name=lblStock]').val(0);
                return ;
            };
            let r = dgBarang.getRowData(rowid);
            $('[name=KodeBarang]').val(unescape(r.BrgKode));
            $('[name=LokasiKode]').val(r.LokasiKode);
            $('[name=BarangNama]').val($("<textarea/>").html(r.BrgNama).text());
            
            // check stok
            $.ajax({
                url: '$urlCheckStock',
                type: "GET",
                data: { 
                    BrgKode: r.BrgKode,
                    LokasiKode: r.LokasiKode
                    },
                success: function (res) {
                    $('[name=LokasiKode]').val(res.LokasiKode);
                    $('[name=lblStock]').val(res.SaldoQtyReal);
                },
                error: function (jXHR, textStatus, errorThrown) {
                    // console.log(errorThrown);
                },
                async: false
            });
            window.rowId_selrow = rowid;
        }        
     }).init().fit($('.box-body')).load({url: __urlDetailBarang});
     window.rowId_selrow = -1;
    dgBarang.bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
        window.cmp.rowid_Barang = rowid;    
        if(rowid === "new_row"){
            $('[id="'+rowid+'_'+'SEQty'+'"]').utilNumberControl().val(0);
            $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val('');
            $('[id="'+rowid+'_'+'Disc'+'"]').utilNumberControl().val(0.00);
            $('[id="'+rowid+'_'+'SEJualDisc'+'"]').utilNumberControl().val(0.00);
            $('[id="'+rowid+'_'+'Jumlah'+'"]').utilNumberControl().val(0.00);
        }
    });
    
    $('#SEDiscPersen').change(function () {
        $('#SEDiscFinal').utilNumberControl().val(0);
        window.HitungBawah();
    });
    
    $('#SEDiscFinal').change(function () {
        $('#SEDiscPersen').utilNumberControl().val(0);
        window.HitungBawah();
    });
JS
);
$urlCustomer         = Url::toRoute( [ 'tdcustomer/select' ] );
$urlCustomerEdit     = Url::toRoute( [ 'tdcustomer/edit' ] );
$urlCustomerCreate   = Url::toRoute( [ 'tdcustomer/create' ] );
$urlCustomerFindById = Url::toRoute( [ 'tdcustomer/find-by-id' ] );
$this->registerJsVar( '__idCustomer', $dsTServis[ 'MotorNoPolisi' ] );
$this->registerJs( <<< JS
    let fillCustomer = function(id, data) {
        __idCustomer = id;
        $('[name=MotorNoPolisi]').val(data.MotorNoPolisi);
        $('[name=CusNama]').val(data.CusNama);
        $('[name=MotorType]').val(data.MotorType);
        $('[name=MotorWarna]').val(data.MotorWarna);
    }
    
    $('#BtnCariMotor').click(function() {
        window.util.app.dialogListData.show({
            title: 'Daftar Konsumen',
            url: '$urlCustomer'
        },
        function (data) {
            // console.log(data);
            __idCustomer = data.id;
            $('[name=MotorNoPolisi]').val(data.data.MotorNoPolisi);
            $.ajax({
                url: '$urlCustomerFindById',
                type: "GET",
                data: { id: data.data.MotorNoPolisi },
                success: function (res) {
                    //console.log(res);
                    fillCustomer(res.MotorNoPolisi, res);
                },
                error: function (jXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                },
                async: false
            });
        })
    });
    
    $('#BtnEditMotor').click(function() {
        let MotorNoPolisi = $('input[name="MotorNoPolisi"]').val();
        window.openCustomer(MotorNoPolisi);
    });

    $('#BtnNewMotor').click(function() {
        window.openCustomer('--');
    });
    
    $('#btnSave').click(function (event) {	      
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttsehd_id').attr('action','{$url['update']}');
        $('#form_ttsehd_id').submit();
    });    
    
    $('#BtnBayar').click(function() {	
	    let MotorNoPolisi = $('input[name="MotorNoPolisi"]').val();
	    let CusNama = $('input[name="CusNama"]').val();
	    let SENoView = $('input[name="SENoView"]').val();
	    let SENo = $('input[name="SENo"]').val();
        let SisaBayar = parseFloat($('#SisaBayar').utilNumberControl().val());        
        if(SENo.includes('=')){
             bootbox.alert({message:'Silahkan simpan transaksi ini dulu.',size: 'small'});
	        return;
        }
	    if (SisaBayar <= 0){
	        bootbox.alert({message:'Transaksi ini sudah terlunas',size: 'small'});
	        return;
	    }
	    $('#MyKodeTrans').val('SE');
	    $('#MyKode').val(MotorNoPolisi);
	    $('#MyNama').val(CusNama);
	    $('#MyNoTransaksi').val(SENo);
	    $('#BMMemo').val("Pembayaran [SE] Etimasi Biaya Servis No: "+SENoView+" - "+MotorNoPolisi+" - "+CusNama);
	    $('#BMNominal').utilNumberControl().val(SisaBayar);
	    $('#BMBayar').utilNumberControl().val(SisaBayar);
        $('#modalBayar').modal('show');
    });
	
	$('#btnSavePopup').click(function(){
	    $.ajax({
            type: 'POST',
            url: '$urlBayar',
            data: $('#form_bayar_id').utilForm().getData(true)
        }).then(function (cusData) {
            location.reload(); 
        });
	});
	
	function onFocus(){
        window.location.href = "{$urlRefresh}"; 
    };
	
JS
);
echo $this->render( '../_formBayar' );
echo $this->render( '../_formCustomer' );
