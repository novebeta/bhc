<?php
use abengkel\assets\AppAsset;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = '[SE] Estimasi Servis';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'SENo'          => 'No SE',
		'SDNo'          => 'No SD',
		'KodeTrans'     => 'Kode Trans',
		'MotorNoPolisi' => 'No Polisi',
		'KarKode'       => 'Karyawan',
		'SEKeterangan'  => 'Keterangan',
		'Cetak'         => 'Cetak',
		'UserID'        => 'UserID',
	],
	'cmbTgl'    => [
		'SETgl' => 'Tanggal SE',
	],
	'cmbNum'    => [
		'SETotalBiaya' => 'Total Biaya',
		'SETotalJasa'  => 'Total Jasa',
		'SETotalPart'  => 'Total Part',
		'SETerbayar'   => 'Terbayar',
		'SETotalWaktu' => 'Total Waktu',
	],
	'sortname'  => "SETgl",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Ttsehd::class, '[SE] Estimasi Servis'
), \yii\web\View::POS_READY );
