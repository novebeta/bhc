<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttsehd */
$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = 'Edit - Estimasi Servis : ' . $dsTServis['SENoView'];
?>
<div class="ttsehd-update">
    <?= $this->render('_form', [
        'model'     => $model,
        'dsTServis' => $dsTServis,
        'id'        => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttseitbarang/index','action' => 'update'] ),
        ]
    ]) ?>

</div>
