<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Trmp */
$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = str_replace('Menu','',\abengkel\components\TUi::$actionMode).' - Memorial Penyusutan : ' . $dsMp['MPNoView'];
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Memorial Penyusutan', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = 'Edit';
?>
<div class="trmp-update">
    <?= $this->render('_form', [
        'model' => $model,
        'dsMp' => $dsMp,
        'id'     => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail'    => Custom::url('trmp/detail'.$params )
        ]
    ]) ?>

</div>
