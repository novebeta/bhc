<?php
use abengkel\assets\AppAsset;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Memorial Penyusutan';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
    'cmbTxt'    => [
        'MPNo'         => 'Mp No',
        'MPTgl'        => 'Mp Tgl',
        'MPMemo'       => 'Mp Memo',
        'MPSaldoAwal'  => 'Saldo Awal',
        'MPUmur'       => 'Umur',
        'MPAwal'       => 'MP Awal',
        'MPNext'       => 'MP Selanjutnya',
        'MPCOADebet'   => 'MP COA Debet',
        'MPCOAKredit'  => 'MP COA Kredit',
        'MPTglMulai'   => 'MP Tgl Mulai',
        'MPSaldoNow'   => 'MP Saldo Sekarang',
        'MPStatus'     => 'MP Status',
        'UserID'       => 'USer ID',
    ],
    'cmbTgl'    => [
        'trmp.MPTgl'        => 'Tgl MP',
    ],
    'cmbNum'    => [
        'trmp.MPAwal'       => 'MP Awal',
        'trmp.MPNext'       => 'MP Selanjutnya',
        'trmp.MPCOADebet'   => 'MP COA Debet',
        'trmp.MPCOAKredit'  => 'MP COA Kredit',
        'trmp.MPSaldoNow'   => 'MP Saldo Sekarang',
    ],
    'sortname'  => "MPTgl",
    'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Trmp::className(), 'Memorial Penyusutan'), \yii\web\View::POS_READY );