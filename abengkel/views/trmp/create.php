<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\components\Custom;

/* @var $this yii\web\View */
/* @var $model abengkel\models\Trmp */
$params                          = '&id=' . $id . '&action=create';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = 'Tambah - Memorial Penyusutan';
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Memorial Penyusutan', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = $this->title;

?>
<div class="trmp-create">
    <?= $this->render('_form', [
        'model' => $model,
        'dsMp' => $dsMp,
        'id'     => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail'    => Custom::url('trmp/detail'.$params )
        ]
    ]) ?>

</div>
