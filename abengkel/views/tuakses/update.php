<?php
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tuser */
$this->title                   = 'Edit Hak Akses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tuakses-update">
	<?= $this->render( '_form', [
		'model' => $model,
        'rArr' => $rArr,
	] ) ?>
</div>
