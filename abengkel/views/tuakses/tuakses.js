jQuery(function ($) {
    Lockr.prefix = 'tuakses_';
    // Lockr.set('records', rTuakses);
    Lockr.set('records', rArr);
    window.rArr = rArr;
    function treeFn(event) {
        var args = event.args;
        // console.log(args);
        var item = $(this).jqxTree('getItem', args.element);
        // var d = JSON.parse(item.value);
        // let d = JSON.parse(r[item.id]);
        // console.log(item);
        var object = Lockr.get('records');
        // var objIndex = _.findIndex(object, function(o) { return o.Id == item.label; });
        var objIndex = _.findIndex(object, function(o) {
            if (args.element.innerText == '-'){
                return o.Id == args.element.innerText && o.MenuNo == $('#' + args.element.id + ' > div > span').attr('menuno') && o.MenuInduk == $('#' + args.element.id + ' > div > span').attr('menuinduk');
            }
            return o.Id == args.element.innerText;
        });
        var d = object[objIndex];
        // console.log(d);
        $('#recordId').val(item.label);
        $('#MenuText').val(item.label);
        $('#MenuTambah').val(d.MenuTambah);
        $('#MenuTambah').checkboxX('refresh');
        $('#MenuEdit').val(d.MenuEdit);
        $('#MenuEdit').checkboxX('refresh');
        $('#MenuHapus').val(d.MenuHapus);
        $('#MenuHapus').checkboxX('refresh');
        $('#MenuCetak').val(d.MenuCetak);
        $('#MenuCetak').checkboxX('refresh');
        $('#MenuView').val(d.MenuView);
        $('#MenuView').checkboxX('refresh');
    }

    function itemChange(event) {
        var args = event.args;
        // var element = args.element;
        // var checked = args.checked;
        // console.log(args);
        let records = Lockr.get('records');
        var objIndex = _.findIndex(records, function(o) {
            if (args.element.innerText == '-'){
                return o.Id == args.element.innerText && o.MenuNo == $('#' + args.element.id + ' > div > span').attr('menuno') && o.MenuInduk == $('#' + args.element.id + ' > div > span').attr('menuinduk');
            }
            return o.Id == args.element.innerText;
        });
        records[objIndex].MenuStatus = args.checked ? '1' : '0';
        // let d = records[args.element.innerText];
        // let thisId = 'MenuStatus';
        // d.MenuStatus = args.checked ? '1' : '0';
        // records[args.element.innerText] = d;
        Lockr.set('records', records);
    }

    function ope() {
        let id = $('#recordId').val();
        let records = Lockr.get('records');
        var objIndex = _.findIndex(records, function(o) { return o.Id == id; });
        var d = records[objIndex];
        let thisId = $(this).attr('name');
        d[thisId] = $(this).val();
        records[id] = d;
        Lockr.set('records', records);
    }

    function createTree(obj, induk, elmt){
        var tree = $('#jqxTree');
        let items = _.remove(obj, function (element) {
            return element.MenuInduk === induk;
        });
        if (items.length === 0) return;
        // console.log(items);
        tree.jqxTree('addTo', items, elmt);
        items.forEach(function (d,idxD){
            let idx = $('li').filter(function() {
                if (d.Id == '-'){
                    var menuno = $(this).children().children('span').attr('menuno');
                    var menuinduk = $(this).children().children('span').attr('menuinduk');
                    return menuno == d.MenuNo && menuinduk == d.MenuInduk;
                }else{
                    return this.innerText === d.Id;
                }
            }).prop('id');
            if(idx !== ''){
                let id = "#" + idx;
                tree.jqxTree('checkItem', $(id)[0], d.MenuStatus == 1);
                createTree(object, d.Id, $(id)[0]);
            }

        });
    }
    $('#MenuTambah').on('change', ope);
    $('#MenuEdit').on('change', ope);
    $('#MenuHapus').on('change', ope);
    $('#MenuCetak').on('change', ope);
    $('#MenuView').on('change', ope);
    var tree = $('#jqxTree');
    tree.jqxTree({hasThreeStates: false, checkboxes: true, width: '100%', height: '450px'});
    tree.css('visibility', 'visible');
    var object = Lockr.get('records');
    var items = _.remove(object, function (element) {
        return element.MenuInduk == 'MainMenu1';
    });
    if (items.length === 0) return;
    tree.jqxTree('addTo', items);
    items.forEach(function (d,idxD){
        let idx = $('li').filter(function() { return this.innerText === d.Id}).prop('id')
        let id = "#" + idx;
        tree.jqxTree('checkItem', $(id)[0], d.MenuStatus == 1);
        createTree(object, d.Id, $(id)[0]);
    });
    // for (var property in object) {
    //     if (object.hasOwnProperty(property)) {
    //         let d = object[property];
    //         let idx = $('li','[role=treeitem]').filter(function() { return this.textContent === d.Id}).prop('id')
    //         let id = "#" + idx;
    //         tree.jqxTree('checkItem', $(id)[0], d.MenuStatus == 1);
    //     }
    // }

    // tree.jqxTree('checkItem', $("#System5")[0], true);
    tree.on('itemClick', treeFn);
    tree.on('checkChange', itemChange);
    $("#tuakses_form").submit(function (event) {
        var object = Lockr.get('records');
        $("#records").val(JSON.stringify(object));
        $("#KodeAksesId").val($("#KodeAkses").val());
    });
});