<?php
use abengkel\assets\AppAsset;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hak Akses';
$this->params['breadcrumbs'][] = $this->title;
$arr = [
	'cmbTxt'    => [
		'KodeAkses' => 'KodeAkses',
	],
	'cmbTgl'    => [],
	'cmbNum'    => [],
	'sortname'  => "KodeAkses",
	'sortorder' => 'asc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Tuakses::class,
	'Hak Akses' ), \yii\web\View::POS_READY );
