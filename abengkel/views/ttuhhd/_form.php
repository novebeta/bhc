<?php
use abengkel\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttuhhd */
/* @var $form yii\widgets\ActiveForm */

$format          = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape          = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>

<div class="ttuhhd-form">
    <?php
    $form = ActiveForm::begin( [ 'id' => 'form_ttuhhd_id','action' => $url[ 'update' ] ] );
    \abengkel\components\TUi::form(
	    [
		    'class' => "row-no-gutters",
		    'items' => [
			    [ 'class' => "form-group col-md-24",
			      'items' => [
				      [ 'class' => "col-sm-2", 'items' => ":LabelNumber" ],
				      [ 'class' => "col-sm-3", 'items' => ":Number" ],
				      [ 'class' => "col-sm-1" ],
				      [ 'class' => "col-sm-1",'items' => ":LabelTgl" ],
				      [ 'class' => "col-sm-2", 'items' => ":Tgl" ],
				      [ 'class' => "col-sm-2", 'items' => ":Jam" ],
				      [ 'class' => "col-sm-7" ],
				      [ 'class' => "col-sm-1", 'items' => ":LabelTC" ],
				      [ 'class' => "col-sm-5", 'items' => ":TC" ],
			      ],
			    ],
			    [
				    'class' => "row col-md-24",
				    'style' => "margin-bottom: 6px;",
				    'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
			    ],
			    [ 'class' => "form-group col-md-24", 'style' => "",
			      'items' => [
				      [ 'class' => "col-sm-2", 'items' => ":LabelUHKeterangan" ],
				      [ 'class' => "col-sm-13", 'items' => ":UHKeterangan" ],
                      [ 'class' => "col-sm-1"],
                      [ 'class' => "col-sm-2", 'items' => ":LabelStatus" ],
				      [ 'class' => "col-sm-2", 'items' => ":Status" ],
			      ],
			    ],
		    ]
	    ],
	    [ 'class' => "pull-right", 'items' => ":btnPrint :btnAction" ],
	    [
		    /* label */
		    ":LabelNumber"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor UH</label>',
		    ":LabelTgl"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl UH</label>',
		    ":LabelTC"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">TC</label>',
		    ":LabelUHKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
		    ":LabelStatus"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status Cetak</label>',
		    /* component */
            ":Number"           => Html::textInput( 'UHNoView', $dsSetup[ 'UHNoView' ], [ 'class' => 'form-control','readonly' => 'readonly' ] ) .
                                    Html::textInput( 'UHNo', $dsSetup[ 'UHNo' ], [ 'class' => 'hidden' ] ) ,
            ":UHKeterangan"     => Html::textarea( 'UHKeterangan', $dsSetup[ 'UHKeterangan' ], [ 'class' => 'form-control', 'maxlength' => '150' ] ),
            ":Tgl"              => FormField::dateInput( [ 'name' => 'UHTgl', 'config' => [ 'value' => General::asDate( $dsSetup[ 'UHTgl' ] ) ] ] ),
            ":Jam"              => FormField::timeInput( [ 'name' => 'UHJam', 'config' => [ 'value' => $dsSetup[ 'UHTgl' ] ] ] ),
            ":TC"               => FormField::combo( 'KodeTrans', [  'name'=> "KodeTrans",'config' => ['value' => $dsSetup[ 'KodeTrans' ], 'data' => [
                                    'UH' => 'UH - Update Harga Jual / HET',] ], 'extraOptions'  => [ 'simpleCombo' => true ] ] ),
            ":Status"           => Html::textInput( 'Cetak', $dsSetup[ 'Cetak' ], [ 'class' => 'form-control','readonly' => 'readonly' ] ),
        ],
        [
            'url_main' => 'ttuhhd',
            'url_id' => $id,
        ]
    );
    ActiveForm::end();
    ?>
</div>
<?php
$urlDetail = $url[ 'detail' ];
$urlPrint  = $url[ 'print' ];
$readOnly  = ( $_GET['action'] == 'view' ? 'true' : 'false' );
$barang    = \common\components\General::cCmd(
        "SELECT BrgKode, BrgNama, BrgBarCode, BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual, BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus 
FROM tdbarang 
WHERE BrgStatus = 'A' 
UNION
SELECT BrgKode, BrgNama, BrgBarCode,  BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual, BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus FROM tdBarang 
WHERE BrgKode IN 
(SELECT ttUHit.BrgKode FROM ttUHit INNER JOIN tdbarang ON ttUHit.BrgKode = tdbarang.BrgKode 
 WHERE ttUHit.UHNo = :UHNo AND tdBarang.BrgStatus = 'N' GROUP BY ttUHit.BrgKode )
ORDER BY BrgStatus, BrgKode",[':UHNo' => $dsSetup['UHNo']] )->queryAll();
$this->registerJsVar( '__Barang', json_encode( $barang ) );
$this->registerJs( <<< JS
     var __BrgKode = JSON.parse(__Barang);
     __BrgKode = $.map(__BrgKode, function (obj) {
                      obj.id = obj.BrgKode;
                      obj.text = obj.BrgKode;
                      return obj;
                    });
     var __BrgNama = JSON.parse(__Barang);
     __BrgNama = $.map(__BrgNama, function (obj) {
                      obj.id = obj.BrgNama;
                      obj.text = obj.BrgNama;
                      return obj;
                    });
    
        
     $('#detailGrid').utilJqGrid({     
     editurl: '$urlDetail',       
        height: 300,
        extraParams: {
             UHNo: $('input[name="UHNo"]').val()
        },
        rownumbers: true,  
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,       
        colModel: [
            { template: 'actions' },
            {
                name: 'BrgKode',
                label: 'Kode',
                width: 200,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __BrgKode,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    // console.log(data);
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'UHHrgJualLama'+'"]').utilNumberControl().val(data.BrgHrgJual);
                            $('[id="'+rowid+'_'+'UHHrgJualBaru'+'"]').utilNumberControl().val(data.BrgHrgJual);
                            $('[id="'+rowid+'_'+'UHHrgBeliLama'+'"]').utilNumberControl().val(data.BrgHrgBeli);
                            $('[id="'+rowid+'_'+'UHHrgBeliBaru'+'"]').utilNumberControl().val(data.BrgHrgBeli);
                            $('[id="'+rowid+'_'+'BrgGroup'+'"]').val(data.BrgGroup);
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').val(data.BrgNama); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').trigger('change');
                            window.hitungLine();
						}
                    }                 
                }
            },
            {
                name: 'BrgNama',
                label: 'Nama Barang',
                width: 300,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __BrgNama,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    console.log(data);
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);                            
                            $('[id="'+rowid+'_'+'UHHrgJualLama'+'"]').utilNumberControl().val(data.BrgHrgJual);
                            $('[id="'+rowid+'_'+'UHHrgJualBaru'+'"]').utilNumberControl().val(data.BrgHrgJual);
                            $('[id="'+rowid+'_'+'UHHrgBeliLama'+'"]').utilNumberControl().val(data.BrgHrgBeli);
                            $('[id="'+rowid+'_'+'UHHrgBeliBaru'+'"]').utilNumberControl().val(data.BrgHrgBeli);
                            $('[id="'+rowid+'_'+'BrgGroup'+'"]').val(data.BrgGroup);
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').val(data.BrgKode); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').trigger('change');
                            window.hitungLine();
						}
                    }              
                }
            },
            {
                name: 'BrgSatuan',
                label: 'Satuan',
                editable: true,
                width: 100,
            },      
            {
                name: 'UHHrgJualLama',
                label: 'Hrg Jual Lama',
                editable: true,
                width: 160,
                template: 'money',
                editoptions:{
                    readOnly:true,
                }, 
            },    
            {
                name: 'UHHrgJualBaru',
                label: 'Hrg Jual Baru',
                editable: true,
                width: 160,
                template: 'money'
            }, 
        ], 
     }).init().fit($('.box-body')).load({url: '$urlDetail'});
     
    $('#btnCancel').click(function (event) {	      
       $('#form_ttuhhd_id').attr('action','{$url['cancel']}');
       $('#form_ttuhhd_id').submit();
    });
	
	$('#btnSave').click(function (event) {	      
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttuhhd_id').attr('action','{$url['update']}');
        $('#form_ttuhhd_id').submit();
    }); 

       $('#btnPrint').click(function (event) {	      
        $('#form_ttuhhd_id').attr('action','$urlPrint');
        $('#form_ttuhhd_id').attr('target','_blank');
        $('#form_ttuhhd_id').submit();
	  }); 
JS
);

