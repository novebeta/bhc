<?php
use abengkel\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Update Harga Jual (HET)';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'UHNo'			=> 'No UH',
		'UHKeterangan'  => 'Keterangan',
		'PosKode'       => 'Pos Kode',
		'KodeTrans'     => 'Kode Trans [TC]',
		'Cetak'         => 'Cetak',
		'UserID'        => 'User ID',
	],
	'cmbTgl'    => [
		'UHTgl'			=> 'Tanggal UH',
	],
	'cmbNum'    => [
	],
	'sortname'  => "UHTgl",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Ttuhhd::className(), 'Update Harga Jual (HET)',
    [
        'url_delete' => Url::toRoute( [ 'ttuhhd/delete' ] ),
    ]), \yii\web\View::POS_READY );
