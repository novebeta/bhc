<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttuhhd */
$params                          = '&id=' . $id . '&action=create';
$cancel = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = 'Tambah - Harga Jual / HET';
?>
<div class="ttuhhd-create">
    <?= $this->render('_form', [
        'model'     => $model,
        'id'        =>$id,
        'dsSetup'   => $dsSetup,
        'url'       => [
            'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttuhit/index','action' => 'create' ] ),
        ]
    ]) ?>

</div>
