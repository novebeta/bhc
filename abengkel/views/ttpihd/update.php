<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttpihd */

$params                          = '&id=' . $id . '&action=update';
$cancel = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = 'Edit - Invoice Pembelian: ' . $dsTBeli['PINoView'];
?>
<div class="ttpihd-update">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTBeli' => $dsTBeli,
        'id'     => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail'       => Url::toRoute( [ 'ttpiit/index','action' => 'update' ] ),
        ]
    ]) ?>

</div>
