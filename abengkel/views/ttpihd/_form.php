<?php
use abengkel\components\FormField;
use abengkel\models\Tdsupplier;
use common\components\General;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use abengkel\models\Tdlokasi;
\abengkel\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttpihd */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttpihd-form">
		<?php
		$dsTBeli[ 'PIDiscPersen' ] = 0;
		if ( $dsTBeli[ 'PITotal' ] > 0 ) {
			$dsTBeli[ 'PIDiscPersen' ] = round( $dsTBeli[ 'PIDiscFinal' ] / $dsTBeli[ 'PISubTotal' ] * 100 );
		}
		$altConditionLokasiKode = "LokasiStatus = 'A' AND (LEFT(LokasiNomor,1) = '7' OR LEFT(LokasiNomor,1) = '8')";
		$LokasiKodeCmb          = ArrayHelper::map( Tdlokasi::find()->select( [ 'LokasiKode' ] )->where( [ 'LokasiStatus' => 'A', 'LEFT(LokasiNomor,1)' => '7' ] )->orderBy( 'LokasiStatus, LokasiKode' )->asArray()->all(), 'LokasiKode', 'LokasiKode' );
		$SupplierKodeCmb        = ArrayHelper::map( Tdsupplier::find()->select( [ 'SupKode', 'SupNama' ] )->where("SupStatus = 'A' OR SupStatus = 1 OR SupStatus = 'D'")->orderBy( 'SupStatus,SupKode' )->all(), 'SupKode', 'SupNama' );
		$form                   = ActiveForm::begin( [ 'id' => 'form_ttpihd_id', 'action' => $url[ 'update' ] ] );
		\abengkel\components\TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-4", 'items' => ":LabelPINo" ],
						      [ 'class' => "col-sm-7", 'items' => ":PINo" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelPITgl" ],
						      [ 'class' => "col-sm-4", 'items' => ":PITgl" ],
						      [ 'class' => "col-sm-5", 'items' => ":PIJam", 'style' => 'padding-left:2px;' ],
						      [ 'class' => "col-sm-1" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "form-group col-md-10",
						        'items' => [
							        [ 'class' => "col-sm-8", 'items' => ":LabelStok" ],
							        [ 'class' => "col-sm-16", 'items' => ":Stok" ],
						        ],
						      ],
						      [ 'class' => "form-group col-md-14",
						        'items' => [
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-5", 'items' => ":LabelSupplier" ],
							        [ 'class' => "col-sm-18", 'items' => ":Supplier" ],
						        ],
						      ],
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-4", 'items' => ":LabelPSNo" ],
						      [ 'class' => "col-sm-5", 'items' => ":PSNo" ],
						      [ 'class' => "col-sm-2", 'items' => ":BtnSearch" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelPSTgl" ],
						      [ 'class' => "col-sm-4", 'items' => ":PSTgl" ],
						      [ 'class' => "col-sm-5", 'items' => ":PSJam", 'style' => 'padding-left:2px;' ],
						      [ 'class' => "col-sm-1" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "form-group col-md-10",
						        'items' => [
							        [ 'class' => "col-sm-5", 'items' => ":LabelJenis" ],
							        [ 'class' => "col-sm-8", 'items' => ":Jenis" ],
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-4", 'items' => ":LabelTerm" ],
							        [ 'class' => "col-sm-6", 'items' => ":Term" ],
						        ],
						      ],
						      [ 'class' => "form-group col-md-14",
						        'items' => [
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-5", 'items' => ":LabelTempo" ],
							        [ 'class' => "col-sm-7", 'items' => ":Tempo" ],
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-2", 'items' => ":LabelTC" ],
							        [ 'class' => "col-sm-8", 'items' => ":TC" ]
						        ],
						      ],
					      ],
					    ],
				    ],
				  ],
				  [
					  'class' => "row col-md-24",
					  'style' => "margin-bottom: 6px;",
					  'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-13",
					      'items' => [
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-4", 'items' => ":KodeBarang", 'style' => 'padding-right:1px;' ],
							        [ 'class' => "col-sm-9", 'items' => ":BarangNama", 'style' => 'padding-right:2px;' ],
							        [ 'class' => "col-sm-4", 'items' => ":Gudang", 'style' => 'padding-right:2px;' ],
							        [ 'class' => "col-sm-4", 'items' => ":Satuan" ],
							        [ 'class' => "col-sm-2", 'items' => ":BtnInfo" ],
							        [ 'class' => "col-sm-1" ],
						        ],
						      ],
						      [ 'class' => "form-group col-md-18",
						        'items' => [
							        [ 'class' => "col-sm-4", 'items' => ":LabelKeterangan" ],
							        [ 'class' => "col-sm-20", 'items' => ":Keterangan" ],
						        ],
						      ],
						      [ 'class' => "form-group col-md-6",
						        'items' => [
							        [ 'class' => "form-group col-md-24",
							          'items' => [
								          [ 'class' => "col-sm-1" ],
								          [ 'class' => "col-sm-4", 'items' => ":BtnBayar" ],
								          [ 'class' => "col-sm-1" ],
							          ],
							        ],
							        [ 'class' => "form-group col-md-24",
							          'items' => [
								          [ 'class' => "col-sm-1" ],
								          [ 'class' => "col-sm-8", 'items' => ":LabelCetak" ],
								          [ 'class' => "col-sm-12", 'items' => ":Cetak" ],
								          [ 'class' => "col-sm-1" ]
							          ],
							        ],
						        ],
						      ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-11",
					      'items' => [
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-4", 'items' => ":LabelRefNo" ],
							        [ 'class' => "col-sm-6", 'items' => ":RefNo" ],
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-3", 'items' => ":LabelSubTotal" ],
							        [ 'class' => "col-sm-9", 'items' => ":SubTotal" ],
						        ],
						      ],
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-4", 'items' => ":LabelBayar" ],
							        [ 'class' => "col-sm-6", 'items' => ":Bayar" ],
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-3", 'items' => ":LabelDisc" ],
							        [ 'class' => "col-sm-2", 'items' => ":Disc" ],
							        [ 'class' => "col-sm-1", 'items' => ":Persen", 'style' => 'padding-left:4px;' ],
							        [ 'class' => "col-sm-6", 'items' => ":Nominal" ],
						        ],
						      ],
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-4", 'items' => ":LabelSisa" ],
							        [ 'class' => "col-sm-6", 'items' => ":Sisa" ],
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-3", 'items' => ":LabelTotal" ],
							        [ 'class' => "col-sm-9", 'items' => ":Total" ]
						        ],
						      ],
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-4", 'items' => ":LabelByrStatus" ],
							        [ 'class' => "col-sm-4", 'items' => ":Status" ],
							        [ 'class' => "col-sm-2", 'items' => ":BtnStatus" ],
                                    [ 'class' => "col-sm-1" ],
                                    [ 'class' => "col-sm-3", 'items' => ":LabelPPNPI" ],
                                    [ 'class' => "col-sm-9", 'items' => ":PPNPI" ]
						        ],
						      ],
					      ],
					    ],
				    ],
				  ],
                  ['class' => "col-md-24",
                      'items'  => [
                          ['class' => "col-md-2", 'items' => ":LabelZnoPenerimaan"],
                          ['class' => "col-md-4", 'items' => ":ZnoPenerimaan"],
                          ['class' => "col-md-1"],
                          ['class' => "col-md-3", 'items' => ":LabelZnoShippingList"],
                          ['class' => "col-md-4", 'items' => ":ZnoShippingList"],
                      ],
                  ],
			  ],
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
                  ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                      'items'  => [
                          ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":JT"],
                          ['class' => "col-sm-4 pull-right", 'items' => ":LabelJT"],
                      ],
                  ],
                  [
                      'class' => "col-md-12 pull-left",
                      'items' => $this->render( '../_nav', [
                          'url'=> $_GET['r'],
                          'options'=> [
                              'PINo'         => 'No PI',
                              'PIJenis'      => 'Jenis',
                              'PSNo'         => 'No PS',
                              'KodeTrans'    => 'Kode Trans/TC',
                              'PosKode'      => 'POS',
                              'SupKode'      => 'Supplier',
                              'LokasiKode'   => 'Lokasi',
                              'PINoRef'      => 'No Ref',
                              'PIKeterangan' => 'Keterangan',
                              'NoGL'         => 'No GL',
                              'Cetak'        => 'Cetak',
                              'UserID'       => 'UserID',
                          ],
                      ])
                  ],
                  ['class' => "col-md-12 pull-right",
				    'items' => [
					    [ 'class' => "pull-right", 'items' => ":btnImport  :btnJurnal :btnPrint :btnAction" ]
				    ]
				  ]
			  ]
			],
			[
				":LabelPINo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor PI</label>',
				":LabelPITgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl PI</label>',
				":LabelPSTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl PS</label>',
				":LabelPSNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor PS</label>',
				":LabelTC"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">TC</label>',
				":LabelSubTotal"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sub Total</label>',
				":LabelKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelDisc"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Disc Final</label>',
				":Persen"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">%</label>',
				":LabelCetak"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Cetak</label>',
				":LabelTotal"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total</label>',
				":LabelSupplier"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Supplier</label>',
				":LabelStok"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Stok Masuk</label>',
				":LabelJenis"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				":LabelTerm"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Term</label>',
				":LabelTempo"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Tempo</label>',
				":LabelRefNo"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor Ref#</label>',
				":LabelSisa"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sisa Bayar</label>',
				":LabelByrStatus"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status Bayar</label>',
				":LabelBayar"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total Bayar</label>',
				":LabelJT"         => \common\components\General::labelGL( $dsTBeli[ 'NoGL' ], 'JT' ),
				":JT"              => Html::textInput( 'NoGL', $dsTBeli[ 'NoGL' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":PINo"            => Html::textInput( 'PINoView', $dsTBeli[ 'PINoView' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ) .
				                      Html::textInput( 'PINo', $dsTBeli[ 'PINo' ], [ 'class' => 'hidden' ] ) .
				                      Html::textInput( 'PosKode', $dsTBeli[ 'PosKode' ], [ 'class' => 'hidden' ] ) .
				                      Html::textInput( 'StokAll', 0, [ 'class' => 'hidden' ] ) .
				                      Html::textInput( 'PITotalPajak', 0, [ 'id' => 'PITotalPajak', 'class' => 'hidden' ] ) .
				                      Html::textInput( 'PIBiayaKirim', 0, [ 'id' => 'PIBiayaKirim', 'class' => 'hidden' ] ),
				":PSNo"            => Html::textInput( 'PSNo', $dsTBeli[ 'PSNo' ], [ 'class' => 'form-control', 'maxlength' => '10' ] ),
				":Cetak"           => Html::textInput( 'Cetak', $dsTBeli[ 'Cetak' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":Stok"            => Html::textInput( 'LokasiKode', $dsTBeli[ 'LokasiKode' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":Keterangan"      => Html::textarea( 'PIKeterangan', $dsTBeli[ 'PIKeterangan' ], [ 'class' => 'form-control', 'maxlength' => '150' ] ),
				":RefNo"           => Html::textInput( 'PINoRef', $dsTBeli[ 'PINoRef' ], [ 'class' => 'form-control' ] ),
				":Term"            => FormField::integerInput( [ 'name' => 'PITerm', 'config' => [ 'id' => 'PITerm', 'value' => $dsTBeli[ 'PITerm' ] ] ] ),

				":Jenis"           => FormField::combo( 'PIJenis', [ 'config' => [ 'value' => $dsTBeli[ 'PIJenis' ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":Supplier"        => FormField::combo( 'SupKode', [ 'config' => [ 'value' => $dsTBeli[ 'SupKode' ], 'data' => $SupplierKodeCmb ], 'extraOptions' => [ 'altLabel' => [ 'SupNama' ] ] ] ),
				":TC"              => FormField::combo( 'KodeTrans', [ 'config' => [ 'value' => $dsTBeli[ 'KodeTrans' ], 'data' => [
					'PIPR' => 'PIPR',
					'PIOL' => 'PIOL',
				] ], 'extraOptions'                                             => [ 'simpleCombo' => true ] ] ),
				":Disc"            => FormField::integerInput( [ 'name' => 'PIDiscPersen', 'config' => [ 'id' => 'PIDiscPersen', 'value' => $dsTBeli[ 'PIDiscPersen' ] ] ] ),
				":PITgl"           => FormField::dateInput( [ 'name' => 'PITgl', 'config' => [ 'id' => 'PITgl', 'value' => General::asDate( $dsTBeli[ 'PITgl' ] ) ] ] ),
				":PSTgl"           => FormField::dateInput( [ 'name' => 'PSTgl', 'config' => [ 'id' => 'PSTgl', 'value' => General::asDate( $dsTBeli[ 'PSTgl' ] ) ] ] ),
				":Tempo"           => FormField::dateInput( [ 'name' => 'PITglTempo', 'config' => [ 'id' => 'PITglTempo', 'value' => $dsTBeli[ 'PITglTempo' ] ] ] ),
				":PIJam"           => FormField::timeInput( [ 'name' => 'PIJam', 'config' => [ 'value' => $dsTBeli[ 'PITgl' ] ] ] ),
				":PSJam"           => FormField::timeInput( [ 'name' => 'PSJam', 'config' => [ 'id' => 'PSJam', 'value' => $dsTBeli[ 'PSTgl' ] ] ] ),
				":KodeBarang"      => Html::textInput( 'KodeBarang', '', [ 'id' => 'KodeBarang', 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":BarangNama"      => Html::textInput( 'BarangNama', '', [ 'id' => 'BarangNama', 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":Gudang"          => Html::textInput( 'LokasiKodeTxt', $dsTBeli[ 'LokasiKode' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":Satuan"          => Html::textInput( 'lblStock', 0, [ 'type' => 'number', 'class' => 'form-control', 'style' => 'color: blue; font-weight: bold;', 'readonly' => 'readonly' ] ),
				":Status"          => Html::textInput( 'StatusBayar', $dsTBeli[ 'StatusBayar' ], [ 'id' => 'StatusBayar', 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":SubTotal"        => FormField::numberInput( [ 'name' => 'PISubTotal', 'config' => [ 'id' => 'PISubTotal', 'value' => $dsTBeli[ 'PISubTotal' ], 'readonly' => 'readonly' ] ] ),
				":Total"           => FormField::numberInput( [ 'name' => 'PITotal', 'config' => [ 'id' => 'PITotal', 'value' => $dsTBeli[ 'PITotal' ], 'readonly' => 'readonly' ] ] ),
				":Bayar"           => FormField::numberInput( [ 'name' => 'PITerbayar', 'config' => [ 'id' => 'PITerbayar', 'value' => $dsTBeli[ 'PITerbayar' ], 'readonly' => 'readonly' ] ] ),
				":Nominal"         => FormField::numberInput( [ 'name' => 'PIDiscFinal', 'config' => [ 'value' => $dsTBeli[ 'PIDiscFinal' ] ] ] ),
				":Sisa"            => FormField::numberInput( [ 'name' => 'PISisa', 'config' => [ 'id' => 'SisaBayar', 'value' => 0, 'readonly' => 'readonly' ] ] ),
				":BtnInfo"         => '<button type="button" class="btn btn-default"><i class="fa fa-info-circle fa-lg"></i></button>',
				":BtnSearch"       => '<button id="BtnSearch" type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>',
				":BtnBayar"        => Html::button( '<i class="fa fa-thumbs-up"></i> Bayar', [ 'class' => 'btn btn-success', 'style' => 'width:120px', 'id' => 'BtnBayar' ] ),
                ":btnImport" => Html::button( '<i class="fa fa-arrow-circle-down"><br><br>Import</i>', [ 'class' => 'btn bg-navy ', 'id' => 'btnImport', 'style' => 'width:60px;height:60px' ] ),
                ":BtnStatus"       => Html::button( '<i class="fa fa-refresh fa-lg"></i>', [ 'class' => 'btn btn-default', 'id' => 'BtnStatus' ] ),

                ":LabelZnoPenerimaan"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Penerima</label>',
                ":LabelZnoShippingList"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Shipping List</label>',
                ":ZnoPenerimaan"           => Html::textInput('ZnoPenerimaan', '--', ['class' => 'form-control']),
                ":ZnoShippingList"           => Html::textInput('ZnoShippingList', '--', ['class' => 'form-control']),
                ":LabelPPNPI"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">PPN</label>',
                ":PPNPI"              => FormField::numberInput( [ 'name' => 'PPNPI', 'config' => [ 'value' => $dsTBeli[ 'PPNPI' ], 'readonly' => 'readonly' ] ] ),
			],
            [
				'url_main' => 'ttpihd',
				'url_id' => $_GET['id'] ?? '',
                '_akses' => '[PI] Invoice Pembelian',
			]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$barang            = \common\components\General::cCmd(
	"SELECT BrgKode, BrgNama, BrgBarCode, BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual, BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus
                            FROM tdbarang 
                            WHERE BrgStatus = 'A' 
                            UNION
                            SELECT BrgKode, BrgNama, BrgBarCode,  BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual, BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus FROM tdBarang 
                            WHERE BrgKode IN 
                            (SELECT ttpiit.BrgKode FROM ttpiit INNER JOIN tdbarang ON ttpiit.BrgKode = tdbarang.BrgKode 
                             WHERE ttpiit.PINo = :PINo AND tdBarang.BrgStatus = 'N' GROUP BY ttpiit.BrgKode )
                            ORDER BY BrgStatus, BrgKode", [ ':PINo' => $dsTBeli[ 'PINo' ] ] )->queryAll();
$urlStatusOtomatis = Url::toRoute( [ 'ttpihd/status-otomatis', 'PINo' => $dsTBeli[ 'PINo' ], ] );
$urlCheckStock     = Url::toRoute( [ 'site/check-stock-transaksi' ] );
$urlBayarOtomatis  = Url::toRoute( [ 'ttbmhd/bayar-otomatis', 'NominalBayar' => $dsTBeli[ 'PITotal' ], ] );
$urlPS             = Url::toRoute( [ 'ttpshd/select' ] );
$urlBayar          = Url::toRoute( [ 'bayar/save' ] );
$urlRefresh        = $url[ 'update' ] . '&oper=skip-load&PINoView=' . $dsTBeli[ 'PINoView' ];
//$urlImport        = Url::toRoute( [ 'ttpihd/import','tgl1'    => Yii::$app->formatter->asDate( '-5 day', 'yyyy-MM-dd') ] );
$urlImport        = Url::toRoute( [ 'ttpihd/import','tgl1'    => Yii::$app->formatter->asDate( 'now', 'yyyy-MM-dd') ] );
$urlDetailImport     = Url::toRoute(['ttpihd/import-loop']);
$resultKas         = \abengkel\models\Ttkkhd::find()->callSP( [ 'Action' => 'Insert' ] );
$resultBank        = \abengkel\models\Ttbkhd::find()->callSP( [ 'Action' => 'Insert' ] );
$urlDetail         = $url[ 'detail' ];
$urlPrint          = $url[ 'print' ];
$readOnly          = ( $_GET[ 'action' ] == 'view' ? 'true' : 'false' );
$this->registerJsVar( '__Barang', json_encode( $barang ) );
$this->registerJsVar( '__model', $dsTBeli );
$this->registerJsVar( '__urlPS', $urlPS );
$this->registerJs( <<< JS

    window.BHC_BAYAR_KMNo = '{$resultKas['KKNo']}';
        window.BHC_BAYAR_BMNo = '{$resultBank['BKNo']}';

    var __BrgKode = JSON.parse(__Barang);
     __BrgKode = $.map(__BrgKode, function (obj) {
                      obj.id = obj.BrgKode;
                      obj.text = obj.BrgKode;
                      return obj;
                    });
     var __BrgNama = JSON.parse(__Barang);
     __BrgNama = $.map(__BrgNama, function (obj) {
                      obj.id = obj.BrgNama;
                      obj.text = obj.BrgNama;
                      return obj;
                    });
     function HitungTotal(){
         	let PITotalPajak = $('#detailGrid').jqGrid('getCol','PIPajak',false,'sum');
         	$('#PITotalPajak').utilNumberControl().val(PITotalPajak);
         	let PISubTotal = $('#detailGrid').jqGrid('getCol','Jumlah',false,'sum');
         	$('#PISubTotal').utilNumberControl().val(PISubTotal);
         	console.log(PISubTotal);
         	HitungBawah();
         }
         window.HitungTotal = HitungTotal;
         function HitungBawah(){
         	let PISubTotal = parseFloat($('#PISubTotal').val());           	
         	let PIDiscFinal = parseFloat($('#PIDiscFinal').val());
         	let PIDiscPersen = PIDiscFinal / PISubTotal * 100 ;   
         	$('#PIDiscPersen').utilNumberControl().val(PIDiscPersen);
         	let PITotalPajak = parseFloat($('#PITotalPajak').val());                 
         	let PIBiayaKirim = parseFloat($('#PIBiayaKirim').val());                 
         	let PITotal = PISubTotal + PITotalPajak + PIBiayaKirim - PIDiscFinal;
         	$('#PITotal').utilNumberControl().val(PITotal);
         	let TotalBayar = parseFloat($('#PITerbayar').val());   
         	let SisaBayar = PITotal - TotalBayar;
         	$('#SisaBayar').utilNumberControl().val(SisaBayar);
         	if(SisaBayar  > 0 || PITotal === 0){         	    
         	    $('#StatusBayar').css({"background-color" : "yellow"});
         	    if(TotalBayar === 0) $('#StatusBayar').css({"background-color" : "pink"});
         	} else if(SisaBayar <= 0){
         	    $('#StatusBayar').css({"background-color" : "lime"});
         	}  
         }         
         window.HitungBawah = HitungBawah;
         
         function checkStok(BrgKode,BarangNama){
           $.ajax({
                url: '$urlCheckStock',
                type: "POST",
                data: { 
                    BrgKode: BrgKode,
                },
                success: function (res) {
                    $('[name=LokasiKodeTxt]').val(res.LokasiKode);
                    $('#KodeBarang').val(BrgKode);
                    $('[name=BarangNama]').val(BarangNama);
                    $('[name=lblStock]').val(res.SaldoQty);
                },
                error: function (jXHR, textStatus, errorThrown) {
                },
                async: false
            });
          }
          window.checkStok = checkStok;	
          
     $('#detailGrid').utilJqGrid({
     editurl: '$urlDetail',
        height: 160,
        extraParams: {
            PINo: $('input[name="PINo"]').val()
        },
        rownumbers: true,  
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,     
        colModel: [
            { template: 'actions'  },
            {
                name: 'BrgKode',
                label: 'Kode',
                width: 130,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __BrgKode,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'PIHrgJual'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgJual));
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').val(data.BrgNama); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').trigger('change');
                            checkStok($(this).val(),$('[id="'+rowid+'_'+'BrgNama'+'"]').val());
                            window.hitungLine();
						},
						'change': function(){      
                            let rowid = $(this).attr('rowid');
                            checkStok($(this).val(),$('[id="'+rowid+'_'+'BrgNama'+'"]').val());
						}
                    }                 
                }
            },
            {
                name: 'BrgNama',
                label: 'Nama Barang',
                width: 183,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __BrgNama,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    console.log(data);
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'PIHrgJual'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgJual));
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').val(data.BrgKode); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').trigger('change');
                            window.hitungLine();
						}
                    }              
                }
            },
            {
                name: 'PIQty',
                label: 'Qty',
                editable: true,
                template: 'number',
                width: 100, 
                editoptions:{
                    readOnly:true,
                }, 
            },     
            {
                name: 'BrgSatuan',
                label: 'Satuan',
                editable: true,
                width: 100,
            },      
            {
                name: 'PIHrgJual',
                label: 'Harga Part',
                editable: true,
                width: 100,
                template: 'money',
            },   
            {
                name: 'Disc',
                label: 'Disc %',
                editable: true,
                template: 'money',
                width: 100,
            },     
            {
                name: 'PIDiscount',
                label: 'Nilai Disc',
                width: 100,
                editable: true,
                template: 'money'
            },
            {
                name: 'Jumlah',
                label: 'Jumlah',
                width: 120,
                editable: true,
                template: 'money'
            },
            {
                name: 'PIPajak',
                label: 'PIPajak',
                width: 100,
                editable: true,
                hidden: true
            },
            {
                name: 'PIAuto',
                label: 'PIAuto',
                width: 100,
                editable: true,
                hidden: true
            },
            {
                name: 'PSNo',
                label: 'PSNo',
                width: 100,
                editable: true,
                hidden: true
            },
        ],
        listeners: {
            afterLoad: function (data) {
             window.HitungTotal();
            }    
        },
        
        onSelectRow : function(rowid,status,e){
            if (window.rowId !== -1) return;
            if (window.rowId_selrow === rowid) return;
            if(rowid.includes('new_row') === true){
                $('[name=KodeBarang]').val('--');
                $('[name=LokasiKodeTxt]').val('Kosong');
                $('[name=BarangNama]').val('--');
                $('[name=lblStock]').val(0);
                return ;
            };
            let r = $('#detailGrid').utilJqGrid().getData(rowid).data;
			checkStok(r.BrgKode,r.BrgNama);
			window.rowId_selrow = rowid;
        }        
     }).init().fit($('.box-body')).load({url: '$urlDetail'});    
     window.rowId_selrow = -1;	
     function hitungLine(){
            let Disc = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Disc'+'"]').val()));
            let PIHrgJual = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'PIHrgJual'+'"]').val()));
            let PIQty = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'PIQty'+'"]').val()));
            let PIDiscount = Disc*PIHrgJual*PIQty / 100;
            $('[id="'+window.cmp.rowid+'_'+'PIDiscount'+'"]').utilNumberControl().val(PIDiscount);
            $('[id="'+window.cmp.rowid+'_'+'Jumlah'+'"]').utilNumberControl().val(PIHrgJual * PIQty - PIDiscount);
            window.HitungTotal();
         }
         
         window.hitungLine = hitungLine;
     
     function hitungTerm(){
         var tgl1 = $('#PITgl');
         var tgl2 = $('#PITglTempo').utilDateControl().cmp();
         var term = parseInt($('#PITerm').val());
         
         let d = tgl1.utilDateControl().getDate();
         let d2 = d.getTime() + (term * 24*60*60*1000);
          tgl2.kvDatepicker('update',new Date(d2));
     }
     window.hitungTerm = hitungTerm;
     $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
	     window.cmp.rowid = rowid;    
          if(rowid.includes('new_row')){
              var BrgKode = $('[id="'+rowid+'_'+'BrgKode'+'"]');
              var dtBrgKode = BrgKode.select2('data');
              if (dtBrgKode.length > 0){
                  var dt = dtBrgKode[0];
                  BrgKode.val(dt.BrgKode).trigger('change').trigger({
                    type: 'select2:select',
                    params: {
                        data: dt
                    }
                });
              }
            $('[id="'+rowid+'_'+'PIQty'+'"]').val(1);
            $('[id="'+rowid+'_'+'Disc'+'"]').val(0);
          }
         
          $('[id="'+window.cmp.rowid+'_'+'PIQty'+'"]').on('keyup', function (event){
              hitungLine();
          });
          $('[id="'+window.cmp.rowid+'_'+'PIHrgJual'+'"]').on('keyup', function (event){
              hitungLine();
          });
          $('[id="'+window.cmp.rowid+'_'+'Disc'+'"]').on('keyup', function (event){
              hitungLine();
          });
         hitungLine();
	 });
         
      $('#btnSave').click(function (event) {
          let Nominal = parseFloat($('#PISubTotal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Subtotal tidak boleh 0', size: 'small'});
               return;
           }  
          let NoRef = $('[name=PINoRef]').val();
           if ( NoRef === "" || NoRef === "--" ){
               bootbox.alert({message:'No Ref# harus diisi', size: 'small'});
               return;
           } 
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttpihd_id').attr('action','{$url['update']}');
        $('#form_ttpihd_id').submit();
	  });  
	  
	  $('#PIDiscPersen').change(function (event) {
	      HitungBawah();
	  });
	  
	  $('#PITerm-disp').inputmask({
	    onKeyDown: function(event, buffer, caretPos, opts){
	        hitungTerm();
	    }
	  });
	  
	  $('#PITgl').change(function (){
	      hitungTerm();
	  });
   
    window.getUrlPS = function(){
        let PSNo = $('input[name="PSNo"]').val();
        return __urlPS + ((PSNo === '' || PSNo === '--') ? '':'&check=off&cmbTxt1=PSNo&txt1='+PSNo);
    } 
    
    $('#BtnSearch').click(function()     {
        window.util.app.dialogListData.show({
            title: 'Daftar Purchase Sheet / Penerimaan Barang',
            url: getUrlPS()
        },
        function (data) {
            // console.log(data);
             if (!Array.isArray(data)){
                 return;
             }
             var itemData = [];
             data.forEach(function(itm,ind) {
                $('[name="LokasiKode"]').val(itm.header.data.LokasiKode);
                $('[name="PINoRef"]').val(itm.header.data.PSNoRef);
                $('[name="SupKode"]').utilSelect2().setSelection(itm.header.data.SupKode);
                let cmp = $('[name="PSNo"]');
                cmp.val(itm.header.data.PSNo);
                itm.data.PSNo = itm.header.data.PSNo; 
                itm.data.PINo = __model.PINo; 
                itm.data.PIHrgJual = itm.data.PSHrgJual; 
                itm.data.PIQty = itm.data.PSQty; 
                itm.data.PIDiscount = itm.data.PSDiscount; 
                itemData.push(itm.data);
            });
            $.ajax({
             type: "POST",
             url: '$urlDetail',
             data: {
                 oper: 'batch',
                 data: itemData,
                 header: btoa($('#form_ttpihd_id').serialize())
             },
             success: function(data) {
                window.location.href = "{$url['update']}&oper=skip-load&PINoView={$dsTBeli['PINoView']}"; 
             },
           });
        })
    });
    
    $('#BtnBayar').click(function() {	
	    let SupKode = $('[name="SupKode"]').val();
	    let PINoRef = $('[name="PINoRef"]').val();
	    let PINoView = $('[name="PINoView"]').val();
	    let PINo = $('[name="PINo"]').val();
        let SisaBayar = parseFloat($('#SisaBayar').utilNumberControl().val());
        if(PINo.includes('=')){
             bootbox.alert({message:'Silahkan simpan transaksi ini dulu.',size: 'small'});
	        return;
        }
	    if (SisaBayar <= 0){
	        bootbox.alert({message:'Transaksi ini sudah terlunas',size: 'small'});
	        return;
	    }
	    $('#MyKodeTrans').val('PI');
	    $('#MyKode').val(SupKode);
	    $('#MyNama').val(PINoRef);
	    $('#MyNoTransaksi').val(PINo);
	    $('#BMMemo').val("Pembayaran [PI] Invoice Pembelian No: "+PINoView+" - "+SupKode+" - "+PINoRef);
	    $('#BMNominal').utilNumberControl().val(SisaBayar);
	    $('#BMBayar').utilNumberControl().val(SisaBayar);
        $('#modalBayar').modal('show');
    });
	  
	  $('#btnSavePopup').click(function(){
	    $.ajax({
            type: 'POST',
            url: '$urlBayar',
            data: $('#form_bayar_id').utilForm().getData(true)
        }).then(function (cusData) {
           location.reload();
        });
	});
	
	function onFocus(){
        window.location.href = "{$urlRefresh}"; 
    };
	
	$('#BtnStatus').click(function() {
        window.util.app.popUpForm.show({
        title: "Daftar Penerimaan Barang yang melibatkan Nomor PI : "+$('input[name="PINo"]').val(),
        size: 'small',
        url: '$urlStatusOtomatis',
        width: 900,
        height: 320
        },
        function(data) {
            console.log(data);
        });
    });
    
     $('#btnImport').click(function (event) {	      
        window.util.app.dialogListData.show({
            title: 'Manage Parts Inbound - PINB - PI',
            url: '{$urlImport}'
        },
        function (data) {
			if(!Array.isArray(data)) return;
              $.ajax({
                type: "POST",
                url: '$urlDetailImport',
                data: {
                  oper: 'import',
                  data: data,
                  header: btoa($('#form_ttpihd_id').serialize())
                },
                success: function(data) {
                  window.location.href = "{$url['update']}&oper=skip-load&PINoView={$dsTBeli['PINoView']}"; 
                },
              });
        });
    });   
    
JS
);
echo $this->render( '../_formBayar' );


