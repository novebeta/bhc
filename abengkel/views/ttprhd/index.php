<?php
use abengkel\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '[PR] Retur Pembelian';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'PRNo'         => 'No PR',
		'PINo'         => 'No PI',
		'KodeTrans'    => 'Kode Trans',
		'PosKode'      => 'Pos',
		'SupKode'      => 'Supplier',
		'LokasiKode'   => 'Lokasi',
		'PRKeterangan' => 'Keterangan',
		'NoGL'         => 'No GL',
		'Cetak'        => 'Cetak',
		'UserID'       => 'User ID',
	],
	'cmbTgl'    => [
		'PRTgl'        => 'Tanggal PR',
	],
	'cmbNum'    => [
		'PRSubTotal'   => 'Sub Total',
		'PRDiscFinal'  => 'Disc Final',
		'PRTotal'      => 'Total',
		'PRTerbayar'   => 'Terbayar',
	],
	'sortname'  => "PRTgl",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Ttprhd::className(), '[PR] Retur Pembelian',    [
        'url_delete' => Url::toRoute( [ 'ttprhd/delete' ] ),
    ]
), \yii\web\View::POS_READY );
