<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttprhd */
$params                          = '&id=' . $id . '&action=create';
$cancel = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = 'Tambah - Retur Pembelian';
?>
<div class="ttprhd-create">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTBeli' => $dsTBeli,
        'id'     => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail'       => Url::toRoute( [ 'ttprit/index','action' => 'update' ] ),
        ]
    ]) ?>

</div>
