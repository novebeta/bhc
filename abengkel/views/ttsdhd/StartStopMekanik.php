<?php
use abengkel\components\FormField;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttsdhd */
/* @var $form yii\widgets\ActiveForm */
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
\abengkel\assets\AppAsset::register( $this );
$this->title                     = 'Start Stop Mekanik';
$this->params[ 'breadcrumbs' ][] = $this->title;
$urlCancel = Url::toRoute( 'ttsdhd/daftar-servis' );
$urlDetail = Url::toRoute( [ 'ttsdhd/start-stop-mekanik-list' ] );
$format          = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape          = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="panggil-antrian-form">
		<?php
		$form = ActiveForm::begin( [ 'options' => [ 'method' => 'post' ] ] );
		\abengkel\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-10",
					  'items' => [
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-6", 'items' => ":LabelMekanik" ],
							    [ 'class' => "col-sm-17", 'items' => ":Mekanik" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-6", 'items' => ":LabelPassword" ],
							    [ 'class' => "col-sm-17", 'items' => ":Password" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-6", 'items' => ":LabelNoSD" ],
							    [ 'class' => "col-sm-10", 'items' => ":NoSD" ],
							    [ 'class' => "col-sm-1" ],
							    [ 'class' => "col-sm-6", 'items' => ":Tgl" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-6", 'items' => ":LabelNoPKB" ],
							    [ 'class' => "col-sm-17", 'items' => ":NoPKB" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-6", 'items' => ":LabelNoPol" ],
							    [ 'class' => "col-sm-17", 'items' => ":NoPol" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-6", 'items' => ":LabelNama" ],
							    [ 'class' => "col-sm-17", 'items' => ":Nama" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-6", 'items' => ":LabelTypeMotor" ],
							    [ 'class' => "col-sm-17", 'items' => ":TypeMotor" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-6", 'items' => ":LabelWarna" ],
							    [ 'class' => "col-sm-17", 'items' => ":Warna" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-6", 'items' => ":LabelWaktu" ],
							    [ 'class' => "col-sm-5", 'items' => ":Waktu" ],
							    [ 'class' => "col-sm-1" ],
							    [ 'class' => "col-sm-3", 'items' => ":LabelMenit" ],
							    [ 'class' => "col-sm-5", 'items' => ":2Waktu" ],
							    [ 'class' => "col-sm-1" ],
							    [ 'class' => "col-sm-2", 'items' => ":LabelMenit" ],
						    ],
						  ],
					  ],
					],
					[ 'class' => "form-group col-md-10",
					  'items' => [
						  [
							  'class' => "row col-md-24",
							  'style' => "margin-bottom: 6px;",
							  'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
						  ],
					  ],
					],
				]
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-10",
				    'items' => [
					    [ 'class' => "col-sm-8", 'items' => ":btnStart" ],
					    [ 'class' => "col-sm-8", 'items' => ":btnStop" ],
					    [ 'class' => "col-sm-8", 'items' => ":btnFinish" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-10",
				    'items' => [
					    [ 'class' => "col-sm-24", 'items' => ":DateTime" ],
				    ],
				  ],
				  [ 'class' => "col-md-4",
				    'items' => [
					    [ 'class' => "pull-right", 'items' => ":btnClose" ]
				    ]
				  ]
			  ]
			],
			[
				/* label */
				":LabelMekanik"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Mekanik</label>',
				":LabelPassword"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Password</label>',
				":LabelNoSD"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">No SD</label>',
				":LabelNoPKB"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">No PKB</label>',
				":LabelNoPol"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Polisi</label>',
				":LabelNama"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama</label>',
				":LabelTypeMotor" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Type Motor</label>',
				":LabelWarna"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Warna</label>',
				":LabelWaktu"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Waktu</label>',
				":LabelMenit"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Menit</label>',
				":Mekanik"        => FormField::combo( 'KarKode', [ 'config' => [ 'data' => \abengkel\models\Tdkaryawan::find()->KaryawanStartStop() ] ] ),
				":Password"       => '<input type="password" id="paassid" class="form-control" name="">',
				":NoSD"           => '<select id="SDNo" style="width: 100%" disabled></select>',
				":Tgl"            => '<input type="text" id="SDTgl" class="form-control" readonly value="">',
				":NoPKB"          => '<select id="PKBNo" style="width: 100%" disabled></select>',
				":NoPol"          => '<select id="MotorNoPolisi" style="width: 100%" disabled></select>',
				":Nama"           => '<select id="SDPembawaMotor" style="width: 100%" disabled></select>',
				":TypeMotor"      => '<select id="MotorNama" style="width: 100%" disabled></select>',
				":Warna"          => '<select id="MotorWarna" style="width: 100%" disabled></select>',
				":Waktu"          => '<input type="text" id="SDTotalWaktu" class="form-control" readonly value="45">',
				":2Waktu"         => '<input type="text" id="lblDurasi" class="form-control" readonly value="0">',
				":DateTime"       => '<input type="text" readonly id="datetimepanel" class="form-control" style="font-size: 4rem;height: 80px;text-align: center;font-weight: bold;">',
				":btnStart"       => Html::button( '<i class="glyphicon glyphicon-play-circle fa-2x"> </i><br> Start', [ 'id'=>'btnStart', 'class' => 'btn btn-success', 'style' => 'width:125px;height:90px;font-size:18px', 'disabled'=>true ] ),
				":btnStop"        => Html::button( '<i class="fa fa-pause-circle fa-2x"> </i><br> Stop', [ 'id'=>'btnStop', 'class' => 'btn btn-success', 'style' => 'width:125px;height:90px;font-size:18px', 'disabled'=>true ] ),
				":btnFinish"      => Html::button( '<i class="fa fa-stop-circle fa-2x"> </i><br> Finish', [ 'id'=>'btnFinish', 'class' => 'btn btn-success', 'style' => 'width:125px;height:90px;font-size:18px', 'disabled'=>true ] ),
				":btnClose"       => Html::button( '<i class="fas fa-window-close fa-2x"> </i><br> Keluar', [ 'class' => 'btn btn-success', 'style' => 'width:125px;height:90px;font-size:18px', 'disabled'=>true ] ),
			]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlCekPassword = Url::toRoute( [ 'tdkaryawan/cek-password' ] );
$urlDurasi      = Url::toRoute( [ 'ttsdhd/start-stop-mekanik-durasi' ] );
$urlCekKarKode = Url::toRoute(['ttsdhd/cek-kar-kode-diproses']);
$urlUpdateList = Url::toRoute(['ttsdhd/start-stop-mekanik-list-update']);
$this->registerJs( <<<JS
    $('#SDNo').select2();
    $('#PKBNo').select2();
    $('#MotorNoPolisi').select2();
    $('#SDPembawaMotor').select2();
    $('#MotorNama').select2();
    $('#MotorWarna').select2();
    function setDurasi(){
        $.ajax({
            url: '$urlDurasi',
            type: "POST",
            data: {                
                SDNo: ($('#SDNo').select2('data').length > 0) ? $('#SDNo').select2('data')[0].SDNo : ''
            },
            success: function (res) {
                $('#lblDurasi').val(res.rows);
            }
        });
    }
    var grid = $('#detailGrid');
     grid.jqGrid({
        url:'$urlDetail',
        styleUI : 'Bootstrap',
        responsive: false,
        height: 250,
        datatype: 'json',
	    mtype: 'POST',
        navButtonTambah: false,
        colModel: [
            {
                name: 'Nomor',
                label: 'No',
                editable: true,
                width: 50,
                editor: { readOnly: true }
            },  
            {
                name: 'KarNama',
                label: 'Mekanik',
                editable: true,
                width: 145,
                editor: { readOnly: true }
            },  
            {
                name: 'START',
                label: 'Start',
                editable: true,
                width: 145,
                editor: { readOnly: true }
            },  
            {
                name: 'FINISH',
                label: 'Finish',
                editable: true,
                width: 145,
                editor: { readOnly: true }
            },  
            {
                name: 'STATUS',
                label: 'Status',
                editable: true,
                width: 145,
                editor: { readOnly: true }
            },     
        ],
        loadComplete: function (data) {
            // console.log(data);
            var rows = data.rows;
            var rowsCount = parseInt(data.rowsCount);
            if (rowsCount > 0){
                var lastData = rows[rowsCount - 1];
                window.BHC_lastData = lastData; 
                if (lastData.STATUS === 'Selesai'){
                    $('#btnFinish').prop('disabled',true);
                    $('#btnStop').prop('disabled',true);
                    $('#btnStart').prop('disabled',true);
                }else if(lastData.STATUS === 'Istirahat'){
                    $('#btnFinish').prop('disabled',true);
                    $('#btnStop').prop('disabled',true);
                    $('#btnStart').prop('disabled',false);
                }else if(lastData.STATUS === 'Diproses'){
                    $('#btnFinish').prop('disabled',false);
                    $('#btnStop').prop('disabled',false);
                    $('#btnStart').prop('disabled',true);
                }
            }else{
                window.BHC_lastData = {
                    Nomor: '',
                    KarNama: '',
                    START: '',
                    FINISH: '',
                    status: '',
                };
                $('#btnFinish').prop('disabled',true);
                $('#btnStop').prop('disabled',true);
                $('#btnStart').prop('disabled',false);
            }
        }  
     });

   setInterval(function () {
       let dtNow = new Date(window.BHC_dtNow);        
       $("#datetimepanel").val( ('00'+dtNow.getDate()).slice(-2)+'/'+('00'+(dtNow.getMonth()+1)).slice(-2)+'/'+dtNow.getFullYear()+' '+
       ('00'+dtNow.getHours()).slice(-2)+':'+('00'+dtNow.getMinutes()).slice(-2)+':'+('00'+dtNow.getSeconds()).slice(-2));
   },1000);

   setInterval(setDurasi,60000);
   
    function setPropDisabled(disabled){
        $('#SDNo').prop('disabled',disabled);
        $('#PKBNo').prop('disabled',disabled);
        $('#MotorNoPolisi').prop('disabled',disabled);
        $('#SDPembawaMotor').prop('disabled',disabled);
        $('#MotorNama').prop('disabled',disabled);
        $('#MotorWarna').prop('disabled',disabled);
    }
    
    $('[name=KarKode]').on('change',function (){
        setPropDisabled(true);
        $('#btnFinish').prop('disabled',true);
        $('#btnStop').prop('disabled',true);
        $('#btnStart').prop('disabled',true);
    });
    
    $('#paassid').blur(function (){
         $.ajax({
            url: '$urlCekPassword',
            type: "POST",
            data: {
                'KarKode': $('[name=KarKode]').val(),
                'KarPassword': $(this).val()
            },
            success: function (res) {
                if (res.msg !== 'false'){
                    setPropDisabled(false);
                     $('#SDNo').select2({
                        data: $.map(res.data, function (obj) {
                            obj.id = obj.SDNo;
                            obj.text = obj.SDNo;
                            return obj;
                        }),
                    }).on('select2:select', function (e) {
                        var data = e.params.data;                      
                        $('#PKBNo').val(data.PKBNo).trigger('change');
                        $('#MotorNoPolisi').val(data.MotorNoPolisi).trigger('change');
                        $('#SDPembawaMotor').val(data.SDPembawaMotor).trigger('change');
                        $('#MotorNama').val(data.MotorNama).trigger('change');
                        $('#MotorWarna').val(data.MotorWarna).trigger('change');
                        let d = new Date(data.SDTgl); 
                        $('#SDTgl').val( ('00'+d.getDate()).slice(-2)+'/'+('00'+(d.getMonth()+1)).slice(-2)+'/'+d.getFullYear());
                        $('#SDTotalWaktu').val(data.SDTotalWaktu);
                    }).on('change',function (){
                         grid.jqGrid('setGridParam',{       
                           postData: {
                               SDNo: ($('#SDNo').select2('data').length > 0) ? $('#SDNo').select2('data')[0].SDNo : '',
                               KarKode: $('[name=KarKode]').val()
                           },
                       }).trigger('reloadGrid');
                        setDurasi();
                    }); 
                     
                     $('#PKBNo').select2({
                        data: $.map(res.data, function (obj) {
                            obj.id = obj.PKBNo;
                            obj.text = obj.PKBNo;
                            return obj;
                        }),
                    }).on('select2:select', function (e) {
                        var data = e.params.data;
                        $('#SDNo').val(data.SDNo).trigger('change');
                        $('#MotorNoPolisi').val(data.MotorNoPolisi).trigger('change');
                        $('#SDPembawaMotor').val(data.SDPembawaMotor).trigger('change');
                        $('#MotorNama').val(data.MotorNama).trigger('change');
                        $('#MotorWarna').val(data.MotorWarna).trigger('change');
                        let d = new Date(data.SDTgl); 
                        $('#SDTgl').val( ('00'+d.getDate()).slice(-2)+'/'+('00'+(d.getMonth()+1)).slice(-2)+'/'+d.getFullYear());
                        $('#SDTotalWaktu').val(data.SDTotalWaktu);
                    }); 
                    
                     $('#MotorNoPolisi').select2({
                        data: $.map(res.data, function (obj) {
                            obj.id = obj.MotorNoPolisi;
                            obj.text = obj.MotorNoPolisi;
                            return obj;
                        }),
                    }).on('select2:select', function (e) {
                        var data = e.params.data;
                        $('#SDNo').val(data.SDNo).trigger('change');
                        $('#PKBNo').val(data.PKBNo).trigger('change');
                        $('#SDPembawaMotor').val(data.SDPembawaMotor).trigger('change');
                        $('#MotorNama').val(data.MotorNama).trigger('change');
                        $('#MotorWarna').val(data.MotorWarna).trigger('change');
                        let d = new Date(data.SDTgl); 
                        $('#SDTgl').val( ('00'+d.getDate()).slice(-2)+'/'+('00'+(d.getMonth()+1)).slice(-2)+'/'+d.getFullYear());
                        $('#SDTotalWaktu').val(data.SDTotalWaktu);
                    }); 
                    
                     $('#SDPembawaMotor').select2({
                        data: $.map(res.data, function (obj) {
                            obj.id = obj.SDPembawaMotor;
                            obj.text = obj.SDPembawaMotor;
                            return obj;
                        }),
                    }).on('select2:select', function (e) {
                        var data = e.params.data;
                        $('#SDNo').val(data.SDNo).trigger('change');
                        $('#PKBNo').val(data.PKBNo).trigger('change');
                        $('#MotorNoPolisi').val(data.MotorNoPolisi).trigger('change');
                        $('#MotorNama').val(data.MotorNama).trigger('change');
                        $('#MotorWarna').val(data.MotorWarna).trigger('change');
                        let d = new Date(data.SDTgl); 
                        $('#SDTgl').val( ('00'+d.getDate()).slice(-2)+'/'+('00'+(d.getMonth()+1)).slice(-2)+'/'+d.getFullYear());
                        $('#SDTotalWaktu').val(data.SDTotalWaktu);
                    });  
                    
                     $('#MotorNama').select2({
                        data: $.map(res.data, function (obj) {
                            obj.id = obj.MotorNama;
                            obj.text = obj.MotorNama;
                            return obj;
                        }),
                    }).on('select2:select', function (e) {
                        var data = e.params.data;
                        $('#SDNo').val(data.SDNo).trigger('change');
                        $('#PKBNo').val(data.PKBNo).trigger('change');
                        $('#MotorNoPolisi').val(data.MotorNoPolisi).trigger('change');
                        $('#SDPembawaMotor').val(data.SDPembawaMotor).trigger('change');
                        $('#MotorWarna').val(data.MotorWarna).trigger('change');
                        let d = new Date(data.SDTgl); 
                        $('#SDTgl').val( ('00'+d.getDate()).slice(-2)+'/'+('00'+(d.getMonth()+1)).slice(-2)+'/'+d.getFullYear());
                        $('#SDTotalWaktu').val(data.SDTotalWaktu);
                    });   
                     
                     $('#MotorWarna').select2({
                        data: $.map(res.data, function (obj) {
                            obj.id = obj.MotorWarna;
                            obj.text = obj.MotorWarna;
                            return obj;
                        }),
                    }).on('select2:select', function (e) {
                        var data = e.params.data;
                        $('#SDNo').val(data.SDNo).trigger('change');
                        $('#PKBNo').val(data.PKBNo).trigger('change');
                        $('#MotorNoPolisi').val(data.MotorNoPolisi).trigger('change');
                        $('#SDPembawaMotor').val(data.SDPembawaMotor).trigger('change');
                        $('#MotorNama').val(data.MotorNama).trigger('change');
                        let d = new Date(data.SDTgl); 
                        $('#SDTgl').val( ('00'+d.getDate()).slice(-2)+'/'+('00'+(d.getMonth()+1)).slice(-2)+'/'+d.getFullYear());
                        $('#SDTotalWaktu').val(data.SDTotalWaktu);
                    });  
                      grid.jqGrid('setGridParam',{       
                           postData: {
                               SDNo: ($('#SDNo').select2('data').length > 0) ? $('#SDNo').select2('data')[0].SDNo : '',
                               KarKode: $('[name=KarKode]').val()
                           },
                       }).trigger('reloadGrid');
                        setDurasi();
                }else{
                    bootbox.alert({message:'Password Salah',size: 'small'});
                }
            },
            error: function (jXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
         });
    });

    function CekKarKodeDiproses(callback){
        var Hasil = false;
        var A = null;
        var KarKode = $('[name=KarKode]').val();
        if (window.BHC_lastData !== undefined){
            if (window.BHC_lastData.KarKode === KarKode && window.BHC_lastData.STATUS === 'Diproses'){
                Hasil = false;
                A = "Benar";
            }
        }
        if (A === null){
            $.ajax({
                url: '$urlCekKarKode',
                type: "POST",
                data: {                
                    KarKode: KarKode
                },
                success: function (res) {
                    A = res.rows;
                    if (A !== '' && A !== '--'){
                        bootbox.alert({message:KarKode+' sedang menangani SD Nomor : '+A, size: 'small'});
                        Hasil = true;
                    }
                    if (Hasil) return;
                    callback();
                }
            });
        }else{
            if (Hasil) return;
            callback();
        }
    }
    
     $('#btnStart').click(function (event) {
         // console.log(window.BHC_lastData);
          if (window.BHC_lastData.STATUS === 'Selesai' || window.BHC_lastData.STATUS === 'Diproses'){
              return ;
          }
          CekKarKodeDiproses(function (){
              var KarKode = $('[name=KarKode]').val();
            $.ajax({
                url: '$urlUpdateList',
                type: "POST",
                data: {                
                    oper: 'start',
                    SDNo: $('#SDNo').select2('data')[0].SDNo,
                    KarKode: KarKode
                },
                success: function (res) {
                    grid.jqGrid('setGridParam',{       
                           postData: {
                               SDNo: ($('#SDNo').select2('data').length > 0) ? $('#SDNo').select2('data')[0].SDNo : ''
                           },
                       }).trigger('reloadGrid');
                }
            });         
          });
    });
     $('#btnStop').click(function (event) {
         // console.log(window.BHC_lastData);
          if (window.BHC_lastData.STATUS === 'Selesai' || window.BHC_lastData.STATUS === 'Istirahat'){
              return ;
          }
          CekKarKodeDiproses(function (){
              var KarKode = $('[name=KarKode]').val();
              if (window.BHC_lastData.KarKode !== KarKode){
                   bootbox.alert({message:'Seharusnya yang menunda servis ini adalah ' + window.BHC_lastData.KarKode + 
                  ' bukan '+ KarKode, size: 'small'});
              }else{
                  $.ajax({
                    url: '$urlUpdateList',
                    type: "POST",
                    data: {                
                        oper: 'stop',
                        SDNo: $('#SDNo').select2('data')[0].SDNo,
                        KarKode: KarKode
                    },
                    success: function (res) {
                        grid.jqGrid('setGridParam',{       
                               postData: {
                                   SDNo: ($('#SDNo').select2('data').length > 0) ? $('#SDNo').select2('data')[0].SDNo : ''
                               },
                           }).trigger('reloadGrid');
                    }
                }); 
              }
          });
    });
     $('#btnFinish').click(function (event) {
         // console.log(window.BHC_lastData);
          if (window.BHC_lastData.STATUS === 'Selesai' || window.BHC_lastData.STATUS === 'Istirahat'){
              return ;
          }
          CekKarKodeDiproses(function (){
              var KarKode = $('[name=KarKode]').val();
              if (window.BHC_lastData.KarKode !== KarKode){
                  bootbox.alert({message:'Seharusnya yang menyelesaikan servis ini adalah ' + window.BHC_lastData.KarKode + 
                  ' bukan '+ KarKode, size: 'small'});
              }else{
                  $.ajax({
                    url: '$urlUpdateList',
                    type: "POST",
                    data: {                
                        oper: 'finish',
                        SDNo: $('#SDNo').select2('data')[0].SDNo,
                        KarKode: KarKode
                    },
                    success: function (res) {
                        grid.jqGrid('setGridParam',{       
                               postData: {
                                   SDNo: ($('#SDNo').select2('data').length > 0) ? $('#SDNo').select2('data')[0].SDNo : ''
                               },
                           }).trigger('reloadGrid');
                    }
                }); 
              } 
          });
    });
JS
);
