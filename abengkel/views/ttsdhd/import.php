<?php
use abengkel\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = $title;
$this->params[ 'breadcrumbs' ][] = $this->title;
AppAsset::register( $this );
$arr = [
    'cmbTxt' => [
            'noWorkOrder'	=>	'No Work Order',
    ],
    'cmbTxt2' => [
        'noWorkOrder' => 'No Work Order',
        'noSAForm' => 'No SA Form',
        'tanggalServis' => 'Tanggal Servis',
        'waktuPKB' => 'Waktu PKB',
        'noPolisi' => 'No Polisi',
        'noRangka' => 'No Rangka',
        'noMesin' => 'No Mesin',
        'kodeTipeUnit' => 'Kode Tipe Unit',
        'tahunMotor' => 'Tahun Motor',
        'informasiBensin' => 'Informasi Bensin',
        'kmTerakhir' => 'Km Terakhir',
        'tipeComingCustomer' => 'Tipe Coming Customer',
        'namaPemilik' => 'Nama Pemilik',
        'alamatPemilik' => 'Alamat Pemilik',
        'kodePropinsiPemilik' => 'Kode Propinsi Pemilik',
        'kodeKotaPemilik' => 'Kode Kota Pemilik',
        'kodeKecamatanPemilik' => 'Kode Kecamatan Pemilik',
        'kodeKelurahanPemilik' => 'Kode Kelurahan Pemilik',
        'kodePosPemilik' => 'Kode Pos Pemilik',
        'alamatPembawa' => 'Alamat Pembawa',
        'kodePropinsiPembawa' => 'Kode Propinsi Pembawa',
        'kodeKotaPembawa' => 'Kode Kota Pembawa',
        'kodeKecamatanPembawa' => 'Kode Kecamatan Pembawa',
        'kodeKelurahanPembawa' => 'Kode Kelurahan Pembawa',
        'kodePosPembawa' => 'Kode Pos Pembawa',
        'namaPembawa' => 'Nama Pembawa',
        'noTelpPembawa' => 'No Telp Pembawa',
        'hubunganDenganPemilik' => 'Hubungan Dengan Pemilik',
        'keluhanKonsumen' => 'Keluhan Konsumen',
        'rekomendasiSA' => 'Rekomendasi SA',
        'hondaIdSA' => 'Honda Id SA',
        'hondaIdMekanik' => 'Honda Id Mekanik',
        'saranMekanik' => 'Saran Mekanik',
        'asalUnitEntry' => 'Asal Unit Entry',
        'idPIT' => 'Id PIT',
        'jenisPIT' => 'Jenis PIT',
        'waktuPendaftaran' => 'Waktu Pendaftaran',
        'waktuSelesai' => 'Waktu Selesai',
        'totalFRT' => 'Total FRT',
        'setUpPembayaran' => 'Set Up Pembayaran',
        'catatanTambahan' => 'Catatan Tambahan',
        'konfirmasiPekerjaanTambahan' => 'Konfirmasi Pekerjaan Tambahan',
        'noBukuClaimC => ' => 'No Buku Claim C',
        'noWorkOrderJobReturn' => 'No Work Order Job Return',
        'totalBiayaService' => 'Total Biaya Service',
        'waktuPekerjaan' => 'Waktu Pekerjaan',
        'statusWorkOrder' => 'Status Work Order',

    ],
    'cmbTgl' => [],
    'cmbNum' => [],
    'sortname' => "noWorkOrder",
    'sortorder' => 'desc'
];
$this->registerJsVar('setcmb', $arr);
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render('../_search'); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php

$this->registerJs(<<< JS
    window.gridParam_jqGrid = {
        readOnly: true,
        rownumbers: true,
        rowNum: 10000,
        pgbuttons: false,
        pginput: false,
        viewrecords: false,
        rowList: [],
        pgtext: ""
    }
    // $('#filter-tgl-val1').change(function() {
    //     let _tgl1 = new Date($(this).val());      
    //     _tgl1.setDate(_tgl1.getDate() + 5);
	//     $('#filter-tgl-val2-disp').datepicker('update', _tgl1);
	//     $('#filter-tgl-val2-disp').trigger('change');
    // });
JS
);
$this->registerJs( \abengkel\components\TdUi::mainGridJs(\abengkel\models\Ttsdhd::class, 
$title,[
    'colGrid' => $colGrid,
    'url' => Url::toRoute(['ttsdhd/import-items']),
    'mode' => isset($mode) ? $mode : '',
] ),	\yii\web\View::POS_READY );
$urlJasa = Url::toRoute(['ttsdhd/import-jasa']);
$urlParts = Url::toRoute(['ttsdhd/import-part']);
$this->registerJs( <<< JS
jQuery(function ($) {
    if($('#jqGridDetail').length) {
        $('#jqGridDetail').utilJqGrid({
            url: '{$urlJasa}',
            height: 75,
            readOnly: true,
            rownumbers: true,
            rowNum: 1000,
            colModel: [
                {
                    name: 'idJob',
                    label: 'Id Job',
                    width: 60
                },
                {
                    name: 'jenisPekerjaan',
                    label: 'Jenis Pekerjaan',
                    width: 90
                },
               	{
	                name: 'namaPekerjaan',
	                label: 'Nama Pekerjaan',
	                width: 300,
	            },
                {
                    name: 'biayaService',
                    label: 'Biaya Service',
                    width: 120,
                    editable: true,
                    template: 'money',
                },
               	{
	                name: 'createdTime',
	                label: 'Created Time',
	                width: 130,
	            }         
            ],
	        multiselect: true,
	        onSelectRow: gridFn.detail.onSelectRow,
	        onSelectAll: gridFn.detail.onSelectAll,
	        ondblClickRow: gridFn.detail.ondblClickRow,
	        loadComplete: gridFn.detail.loadComplete,
	        listeners: {
                afterLoad: function(grid, response) {
                    $("#cb_jqGridDetail").trigger('click');
                }
            }
        }).init();
        $('#scnjqGridDetail').utilJqGrid({
            url: '{$urlParts}',
            height: 75,
            readOnly: true,
            rownumbers: true,
            colModel: [
                {
                    name: 'idJob',
                    label: 'Id Job',
                    width: 60
                },
               	{
	                name: 'partsNumber',
	                label: 'Parts Number',
	                width: 150,
	            },
                {
                    name: 'kuantitas',
                    label: 'Kuantitas',
                    width: 80,
                     align: 'right'
                },
                {
                    name: 'hargaParts',
                    label: 'Harga Parts',
                    width: 120,
                    editable: true,
                    template: 'money',
                },
               	{
	                name: 'createdTime',
	                label: 'Created Time',
	                width: 130,
	            }
            ],
	        multiselect: true,
	        onSelectRow: gridFn.detail.onSelectRow,
	        onSelectAll: gridFn.detail.onSelectAll,
	        ondblClickRow: gridFn.detail.ondblClickRow,
	        loadComplete: gridFn.detail.loadComplete,
	        listeners: {
                afterLoad: function(grid, response) {
                    $("#cb_scnjqGridDetail").trigger('click');
                }
            }
        }).init();
    }
});

$("#jqGrid").bind("jqGridSelectRow", function (e, rowid, orgClickEvent) {
    var data = $(this).getRowData(rowid);
    $('#scnjqGridDetail').utilJqGrid().load({param: data});
});

JS, \yii\web\View::POS_READY );