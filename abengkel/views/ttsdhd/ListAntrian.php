<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttsdhd */
/* @var $form yii\widgets\ActiveForm */
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
\abengkel\assets\AppAsset::register($this);

$this->title = 'List Antrian';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
    'cmbTxt'    => [
        'SDNo'              => 'No SD',
    ],
    'cmbTgl'    => [
        'SDTGL'           => 'Tgl SD',
    ],
    'cmbNum'    => [
        'SDTotalBiaya'      => 'Total Biaya',
    ],
    'sortname'  => "SDTgl",
    'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
$url['cancel'] = Url::toRoute('ttsdhd/daftar-servis');
$url['detail'] = '';
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
?>
<div class="panggil-antrian-form">
        <?php
        $form = ActiveForm::begin([]);
        \abengkel\components\TUi::form(
            [
                'class' => "row-no-gutters",
                'items' => [
                    ['class' => "col-md-24",
                        'items' => [
                            ['class' => "col-md-7",
                                'items' => ":Panggilan"
                            ],
                            ['class' => "col-md-8",
                                'items' => [
                                    ['class' => "col-md-24",
                                        'items' => ":Kasir",
                                    ],
                                    ['class' => "col-md-24",
                                        'items' => ":Bengkel",
                                    ],
                                ],
                            ],
                            ['class' => "col-md-9",
                                'items' => [
                                    ['class' => "col-md-12",
                                        'items' => ':LogoBengkel',
                                    ],
                                    ['class' => "col-md-12",
                                        'items' => [
                                            ['class' => "col-sm-24",'items' => ":Jam"],
                                            ['class' => "col-sm-24",'items' => ":Tgl"],
                                        ],
                                    ],
                                    ['class' => "col-md-24",
                                        'items' => ':InfoBengkel',
                                    ],
                                ],
                            ],

                        ],
                    ],
                    ['class' => "col-md-24",
                        'style' => "margin-bottom: 6px;",
                        'items' => '<table id="detailGrid"></table>'
                    ],

                ]
            ],
            [],
            [
                /* label */
                ":Panggilan"      => '<div class="well" style="height: 200px; line-height: 25px; padding: 15px 5px; margin: 0 4px 4px 0; width: auto; display: block; text-align: center; overflow: hidden;">'.
                                    '<span id="Pgl-nourut" style="font-size: 4rem; font-weight: bold; color: darkred; display: inline-block; white-space:nowrap;">000</span><br>'.
                                    '<span id="Pgl-nopol" style="font-size: 4rem; font-weight: bold; color: darkred; display: inline-block; white-space:nowrap;">XX 000 XX</span><br>'.
                                    '<span id="Pgl-nama" style="font-size: 2rem; font-weight: bold; color: black; display: inline-block; white-space:nowrap;">Nama customer</span><br>'.
                                    '<span style="font-size: 2rem; font-weight: bold; color: darkgreen; display: inline-block; padding-top: 10px;">Silahkan Menuju Ke</span><br>'.
                                    '<span id="Pgl-tujuan" style="font-size: 4rem; font-weight: bold; color: darkblue; display: inline-block; white-space:nowrap;">Kasir Bengkel</span>'.
                                 '</div>',
                ":Kasir"      => '<div class="well" style="height: 98px; width: auto; display: block; margin-bottom: 2px; padding: 5px;">'.
                                    '<div style="float:left; width: 120px; height: 100%; text-align: center;">'.
                                        '<span style="font-size: 26px; display: inline-block; font-weight: bold; color: darkblue; vertical-align: middle;">Kasir<br>Bengkel</span>'.
                                    '</div>'.
                                    '<div style="width: 100%; height: 100%; text-align: center;">'.
                                        '<span id="Kasir-nourut" style="font-size: 22px; display: inline-block; font-weight: bold; color: black;">000</span><br>'.
                                        '<span id="Kasir-nopol" style="font-size: 22px; display: inline-block; font-weight: bold; color: black;">XX 000 XX</span><br>'.
                                        '<span id="Kasir-nama" style="font-size: 14px; display: inline-block; font-weight: bold; color: black; word-wrap: break-spaces;">Nama Customer</span>'.
                                    '</div>'.
                                 '</div>',
                ":Bengkel"    => '<div class="well" style="height: 98px; width: auto; display: block; margin: 2px 0 4px 0; padding: 5px;">'.
                                    '<div style="float:left; width: 120px; height: 100%; text-align: center;">'.
                                        '<span style="font-size: 26px; display: inline-block; font-weight: bold; color: darkblue; vertical-align: middle;">Mekanik<br>S A</span>'.
                                    '</div>'.
                                    '<div style="width: 100%; height: 100%; text-align: center;">'.
                                        '<span id="Mekanik-nourut" style="font-size: 22px; display: inline-block; font-weight: bold; color: black;">000</span><br>'.
                                        '<span id="Mekanik-nopol" style="font-size: 22px; display: inline-block; font-weight: bold; color: black;">XX 000 XX</span><br>'.
                                        '<span id="Mekanik-nama" style="font-size: 14px; display: inline-block; font-weight: bold; color: black; word-wrap: break-spaces;">Nama Customer</span>'.
                                    '</div>'.
                                 '</div>',
                ":LogoBengkel" => Html::img('@web/img/ahass.png', ['alt'=>'Logo AHASS', 'style'=>'max-width: 100%; height: auto; max-height: 100px; padding: 3px;', ]),
                ":Jam"        => '<div style="height: 50px; line-height: 50px; width: auto; display: block; background-color: black; text-align: center;">'.
                                    '<span id="ListAntrian-jam" style="font-size: 35px; display: inline-block; font-weight: bold; color: white; vertical-align: middle;">00:00:00</span>'.
                                 '</div>',
                ":Tgl"        => '<div style="height: 30px; line-height: 25px; width: auto; display: block; background-color: black; text-align: center; margin-top: 3px;">'.
                                    '<span id="ListAntrian-tgl" style="font-size: 20px; display: inline-block; font-weight: bold; color: white; vertical-align: middle;">dd mmm yyyy</span>'.
                                 '</div>',
                ":InfoBengkel" => '<div style="padding: 5px; width: auto; overflow: hidden;">'.
                    '<span style="font-size: 20px; color: darkblue; white-space:nowrap;">'.$dataCompany['PerusahaanNama'].'</span><br>'.
                    '<span style="font-size: 16px; color: darkred; white-space:nowrap;">'.$dataCompany['PerusahaanSlogan'].'</span><br>'.
                    '<span style="font-size: 14px; color: black; word-wrap: break-spaces">'.$dataCompany['PerusahaanAlamat'].'</span><br>'.
                    '<span style="font-size: 14px; color: black; white-space:nowrap;">'.$dataCompany['PerusahaanTelepon'].'</span><br>'.
                    '</div>',
            ]
        );
        ActiveForm::end();
        ?>
    </div>

<?php
$urlLoadHeader = Url::toRoute( [ 'ttsdhd/list-antrian-load-header' ] );
$this->registerJsVar( 'urlDetailGrid', Url::toRoute( [ 'ttsdhd/list-antrian-load-antrian' ] ) );
$this->registerJs( <<< JS
    var isLoadingHeader = 0, isLoadingAntrian = 0, setOffAjaxSpinner = 0,
        grid = $('#detailGrid');

    grid.utilJqGrid({
        url: urlDetailGrid,
        height: 250,      
        ajaxSpinner: false,
        showNotification: false,
        readOnly: true,
        colModel: [
            {"name": "SDNoUrut", "label": "No.", "width": 50},
            {"name": "MotorNoPolisi", "label": "No Polisi", "width": 150},
            {"name": "SDPembawaMotor", "label": "Nama Konsumen", "width": 200},
            {"name": "SDNo", "label": "No SD", "width": 100},
            {"name": "KarNama", "label": "Nama Mekanik", "width": 200},
            {"name": "StatusAntrian", "label": "Status", "width": 100},
            {"name": "Durasi", "label": "Durasi", "width": 100},
            {"name": "Persen", "label": "%", "width": 62},
            {"name": "ProgressBar", "label": "Waktu", "width": 100}
        ],
        listeners: {
            afterLoad: function(grid, response) {
                isLoadingAntrian = 0;
            }
        }
     }).init().fit($('.box-body'));

    function updateJam() {
        var today = new Date();
            
            var hour=today.getHours();
            var minute=today.getMinutes();
            var second=today.getSeconds();
            if(hour <10 ){hour='0'+hour;}
            if(minute <10 ) {minute='0' + minute; }
            if(second<10){second='0' + second;}

            $('#ListAntrian-jam').text(hour+':'+minute+':'+second);
            $('#ListAntrian-tgl').text(today.getDate()+' '+bulan[today.getMonth()]+' '+today.getFullYear());
    }
    function loadHeader(data) {
        $('#Pgl-nourut').text(data.Pgl.SDNoUrut);
        $('#Pgl-nopol').text(data.Pgl.MotorNoPolisi);
        $('#Pgl-nama').text(data.Pgl.SDPembawaMotor);
        $('#Pgl-tujuan').text(data.Pgl.QSTujuan);
        
        $('#Kasir-nourut').text(data.Kasir.SDNoUrut);
        $('#Kasir-nopol').text(data.Kasir.MotorNoPolisi);
        $('#Kasir-nama').text(data.Kasir.SDPembawaMotor);
        
        $('#Mekanik-nourut').text(data.Mekanik.SDNoUrut);
        $('#Mekanik-nopol').text(data.Mekanik.MotorNoPolisi);
        $('#Mekanik-nama').text(data.Mekanik.SDPembawaMotor);
    }
    
    function getHeaderData() {
        if(!setOffAjaxSpinner) {
            $(document).off( 'ajaxStart' );
            $(document).off( 'ajaxStop' );
            setOffAjaxSpinner = 1;
        }
    
        if(!isLoadingHeader) {
            isLoadingHeader = 1;
            $.ajax({
                url: '$urlLoadHeader',
                type: "POST",
                data: {},
                success: function (res) {
                    //console.log(res);
                    isLoadingHeader = 0;
                    loadHeader(res);
                },
                error: function (jXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                    isLoadingHeader = 0;
                }
            });
        }
        
        if(!isLoadingAntrian) {
            isLoadingAntrian = 1;
            grid.utilJqGrid().load();
        }
    }
    
    var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
        timer = setInterval(function() {
            updateJam();
            getHeaderData();
        }, 1000);
    
    loadHeader({
        "Pgl": { "SDNoUrut": "--", "MotorNoPolisi": "--", "SDPembawaMotor": "--", "QSTujuan": "--" },
        "Kasir": { "SDNoUrut": "--", "MotorNoPolisi": "--", "SDPembawaMotor": "--" },
        "Mekanik": { "SDNoUrut": "--", "MotorNoPolisi": "--", "SDPembawaMotor": "--" }
    });
JS);
