<?php
use abengkel\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
<div class="history-form">
    <?php
        $form = ActiveForm::begin( [ 'id' => 'history_form_id' ] );
        \abengkel\components\TUi::form(
            [
                'class' => "row-no-gutters",
                'items' => [
                    // [ 'class' => "form-group col-md-24", 'items' => ":LabelPageTitle" ],
                    [ 'class' => "form-group col-md-24",
                        'items' => [
                            [ 'class' => "col-sm-4", 'items' => ":LabelNamaKonsumen" ],
                            [ 'class' => "col-sm-8", 'items' => ":NamaKonsumen" ],
                            [ 'class' => "col-sm-4", 'items' => ":LabelJumlahService" ],
                            [ 'class' => "col-sm-8", 'items' => ":JumlahService" ],
                        ],
                    ],
                    [ 'class' => "form-group col-md-24",
                        'items' => [
                            [ 'class' => "col-sm-4", 'items' => ":LabelAlamat" ],
                            [ 'class' => "col-sm-8", 'items' => ":Alamat" ],
                            [ 'class' => "col-sm-4", 'items' => ":LabelStatus" ],
                            [ 'class' => "col-sm-8", 'items' => ":Status" ],
                        ],
                    ],
                    [ 'class' => "form-group col-md-24",
                        'items' => [
                            [ 'class' => "col-sm-4", 'items' => ":LabelNoTelp" ],
                            [ 'class' => "col-sm-8", 'items' => ":NoTelp" ],
                            [ 'class' => "col-sm-4", 'items' => ":LabelRataJasa" ],
                            [ 'class' => "col-sm-8", 'items' => ":RataJasa" ],
                        ],
                    ],
                    [ 'class' => "form-group col-md-24",
                        'items' => [
                            [ 'class' => "col-sm-4", 'items' => ":LabelPlat" ],
                            [ 'class' => "col-sm-8", 'items' => ":Plat" ],
                            [ 'class' => "col-sm-4", 'items' => ":LabelMaxJasa" ],
                            [ 'class' => "col-sm-8", 'items' => ":MaxJasa" ],
                        ],
                    ],
                    [ 'class' => "form-group col-md-24",
                        'items' => [
                            [ 'class' => "col-sm-4", 'items' => ":LabelTipeMotor" ],
                            [ 'class' => "col-sm-8", 'items' => ":TipeMotor" ],
                            [ 'class' => "col-sm-4", 'items' => ":LabelSiklusServis" ],
                            [ 'class' => "col-sm-8", 'items' => ":SiklusServis" ],
                        ],
                    ],
                    [ 'class' => "form-group col-md-24",
                        'items' => [
                            [ 'class' => "col-sm-4", 'items' => ":LabelNoRangka" ],
                            [ 'class' => "col-sm-8", 'items' => ":NoRangka" ],
                            [ 'class' => "col-sm-4", 'items' => ":LabelProgram" ],
                            [ 'class' => "col-sm-8", 'items' => ":Program" ],
                        ],
                    ],
                    [ 'class' => "form-group col-md-24",
                        'items' => [
                            [ 'class' => "col-sm-4", 'items' => ":LabelNoMesin" ],
                            [ 'class' => "col-sm-8", 'items' => ":NoMesin" ],
                        ],
                    ],
                    [ 'class' => "row form-group col-md-24",],
                    [ 'class' => "row col-md-24",
                        'items' => [
                            [ 'class' => "col-sm-12", 'items' => ":GridDetailServices" ],
                            [ 'class' => "col-sm-12", 'items' => ":GridDetailPartServices" ],
                        ],
                    ],
                    [ 'class' => "row col-md-24", 'items' => ":GridRiwayatService" ],
                ]
            ],
            [],
            [
                // ':LabelPageTitle'=>'<h1>Kartu Konsumen</h1>',
                ':LabelNamaKonsumen'=>'<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Konsumen</label>',
                ':LabelJumlahService'=>'<label class="control-label" style="margin: 0; padding: 6px 5px;">Jumlah Servis</label>',
                ':LabelAlamat'=>'<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
                ':LabelStatus'=>'<label class="control-label" style="margin: 0; padding: 6px 5px;">Status</label>',
                ':LabelNoTelp'=>'<label class="control-label" style="margin: 0; padding: 6px 0;">No Telpon</label>',
                ':LabelRataJasa'=>'<label class="control-label" style="margin: 0; padding: 6px 5px;">Rata Rata Jasa</label>',
                ':LabelPlat'=>'<label class="control-label" style="margin: 0; padding: 6px 0;">Plat Nomor</label>',
                ':LabelMaxJasa'=>'<label class="control-label" style="margin: 0; padding: 6px 5px;">Jasa Tertinggi</label>',
                ':LabelTipeMotor'=>'<label class="control-label" style="margin: 0; padding: 6px 0;">Type Motor</label>',
                ':LabelSiklusServis'=>'<label class="control-label" style="margin: 0; padding: 6px 5px;">Siklus Servis</label>',
                ':LabelNoRangka'=>'<label class="control-label" style="margin: 0; padding: 6px 0;">No Rangka</label>',
                ':LabelProgram'=>'<label class="control-label" style="margin: 0; padding: 6px 5px;">Program Berjalan</label>',
                ':LabelNoMesin'=>'<label class="control-label" style="margin: 0; padding: 6px 0;">No Mesin</label>',
                ':NamaKonsumen'=>Html::textInput( 'CusNama', $data['CusNama'], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
                ':JumlahService'=>FormField::numberInput( [ 'name' => 'JumlahServis', 'config' => [ 'value' => $data['JumlahServis'], 'readonly' => 'readonly', 'maskedInputOptions' => ['digits'=>0]] ] ),
                ':Alamat'=>Html::textInput( 'CusAlamat', $data['CusAlamat'], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
                ':Status'=>Html::textInput( 'StatusPeringkat', $data['StatusPeringkat'], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
                ':NoTelp'=>Html::textInput( 'CusTelepon', $data['CusTelepon'], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
                ':RataJasa'=>FormField::numberInput( [ 'name' => 'RataRataJasa', 'config' => [ 'value' => $data['RataRataJasa'], 'readonly' => 'readonly'] ] ),
                ':Plat'=>Html::textInput( 'MotorNoPolisi', $data['MotorNoPolisi'], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
                ':MaxJasa'=>FormField::numberInput( [ 'name' => 'MaxJasa', 'config' => [ 'value' => $data['MaxJasa'], 'readonly' => 'readonly'] ] ),
                ':TipeMotor'=>Html::textInput( 'MotorType', $data['MotorType'], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
                ':SiklusServis'=>FormField::numberInput( [ 'name' => 'Siklus', 'config' => [ 'value' => $data['Siklus'], 'readonly' => 'readonly'] ] ),
                ':NoRangka'=>Html::textInput( 'MotorNoRangka', $data['MotorNoRangka'], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
                ':Program'=>Html::textInput( 'PrgNama', $data['PrgNama'], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
                ':NoMesin'=>Html::textInput( 'MotorNoMesin', $data['MotorNoMesin'], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
                ":GridDetailServices" => '<table id="detailGridServices"></table><div id="detailGridServicesPager"></div>',
                ":GridDetailPartServices" => '<table id="detailGridPartServices"></table><div id="detailGridPartServicesPager"></div>',
                ":GridRiwayatService" => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
            ],
            []
        );
        ActiveForm::end();
    ?>
</div>
<?php
$urlDetail = Url::toRoute( [ 'ttsdhd/history-otomatis-item', 'MotorNoPolisi' => $data[ 'MotorNoPolisi' ] ] );
$url = Url::toRoute( [ 'ttsdhd/history-otomatis-item' ] );
$this->registerJs( <<< JS
    $('#detailGridServices').utilJqGrid({
        height: 100,
        rownumbers: true,  
        navButtonTambah: false,
        colModel: [
            {
                name: 'JasaNama',
                label: 'Jasa Nama',
                width: 220,
            },
            {
                name: 'SDHrgJual',
                label: 'Jasa',
                width: 100,
                template: 'number'
            },
        ],
     }).init();
    $('#detailGridPartServices').utilJqGrid({
        height: 100,
        rownumbers: true,  
        navButtonTambah: false,
        colModel: [
            {
                name: 'BrgNama',
                label: 'Part Nama',
                width: 220,
            },
            {
                name: 'SDHrgJual',
                label: 'Part',
                width: 100,
                template: 'number'
            },
        ],
     }).init();
     $('#detailGrid').utilJqGrid({
        editurl: '{$urlDetail}',
        height: 155,
        rownumbers: true,  
        navButtonTambah: false,
        colModel: [
            {
                name: 'SDNo',
                label: 'No SD',
                width: 120,
            },
            {
                name: 'SDTgl',
                label: 'Tanggal',
                width: 120,
                formatter: 'date',
                formatoptions: { srcformat: "ISO8601Long", newformat: "m/d/Y H:i:s" }
            },
            {
                name: 'SDTotalJasa',
                label: 'Jasa',
                width: 100,
                template: 'number'
            },
            {
                name: 'SDTotalPart',
                label: 'Part',
                width: 100,
                template: 'number'
            },
            {
                name: 'ASS',
                label: 'Jenis Servis',
                width: 100,
            },
        ],
        onSelectRow : function(rowid,status,e){
            let r = $('#detailGrid').utilJqGrid().getData(rowid).data;
            let url = '{$url}';
            $('#detailGridServices').utilJqGrid().load({url: url+'&SDNoService='+r.SDNo});
            $('#detailGridPartServices').utilJqGrid().load({url: url+'&SDNoPart='+r.SDNo});
        },
     }).init().fit($('.box-body')).load({url: '{$urlDetail}'});
JS
);