<?php
use abengkel\components\FormField;
use abengkel\components\Menu;
use common\components\Custom;
use common\components\General;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttsdhd */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
$dsTServis[ 'SDTglOld' ] = $dsTServis[ 'SDTgl' ];
$timestamp               = strtotime( $dsTServis[ 'SDTglOld' ] );
$dsTServis[ 'SDJam' ]    = date( 'H:i:s', $timestamp );
$dsTServis[ 'SDTgl' ]    = date( 'Y-m-d', $timestamp );
$dsTServis[ 'SVTglOld' ] = $dsTServis[ 'SVTgl' ];
$timestamp               = strtotime( $dsTServis[ 'SVTglOld' ] );
$dsTServis[ 'SVJam' ]    = date( 'H:i:s', $timestamp );
$dsTServis[ 'SVTgl' ]    = date( 'Y-m-d', $timestamp );
$SDHargaBeliHide         = \abengkel\components\Menu::showOnlyHakAkses( [ 'Auditor', 'Admin', 'Akuntansi' ], 'false', 'true' );
$this->registerJsVar('GLValid',(!empty($dsTServis[ 'GLValid' ]) ? $dsTServis[ 'GLValid' ] : false));
$this->registerJsVar('CusJenis',$dsTServis[ 'CusJenis' ]);
?>
    <div class="ttsdhd-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_ttsdhd_id', 'action' => $url[ 'update' ] ] );
		\abengkel\components\TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelSDNo" ],
						      [ 'class' => "col-sm-5", 'items' => ":SDNo" ],
						      [ 'class' => "col-sm-5", 'items' => ":SVNo" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelTgl", 'style' => 'padding-left:5px;' ],
						      [ 'class' => "col-sm-4", 'items' => ":SDTgl:SVTgl" ],
						      [ 'class' => "col-sm-4", 'items' => ":SDJam:SVJam" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-4", 'items' => ":LabelSENo" ],
						      [ 'class' => "col-sm-4", 'items' => ":SENo" ],
						      [ 'class' => "col-sm-1", 'items' => ":BtnInfo" ],
						      [ 'class' => "col-sm-2" ],
						      [ 'class' => "col-sm-3", 'items' => ":LabelKodeTrans" ],
						      [ 'class' => "col-sm-7", 'items' => ":KodeTrans" ],
						      [ 'class' => "col-sm-3", 'items' => ":ASS" ],
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelMotorNoPolisi" ],
						      [ 'class' => "col-sm-4", 'items' => ":MotorNoPolisi" ],
						      [ 'class' => "col-sm-2", 'items' => ":BtnCariMotor" ],
						      [ 'class' => "col-sm-2", 'items' => ":BtnEditMotor", 'style' => 'padding-right:10px;' ],
						      [ 'class' => "col-sm-2", 'items' => ":BtnNewMotor", 'style' => 'padding-right:10px;' ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelMotorType", 'style' => 'padding-left:5px;' ],
						      [ 'class' => "col-sm-8", 'items' => ":MotorType" ],
						      [ 'class' => "col-sm-1" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-4", 'items' => ":LabelSDKmSekarang" ],
						      [ 'class' => "col-sm-6", 'items' => ":SDKmSekarang" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-3", 'items' => ":LabelPrgNama" ],
						      [ 'class' => "col-sm-10", 'items' => ":PrgNama" ],
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelCusNama" ],
						      [ 'class' => "col-sm-10", 'items' => ":CusNama" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelMotorWarna", 'style' => 'padding-left:5px;' ],
						      [ 'class' => "col-sm-8", 'items' => ":MotorWarna" ],
						      [ 'class' => "col-sm-1" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-4", 'items' => ":LabelSDKmBerikut" ],
						      [ 'class' => "col-sm-6", 'items' => ":SDKmBerikut" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-3", 'items' => ":LabelPKBNo" ],
						      [ 'class' => "col-sm-6", 'items' => ":PKBNo" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-1", 'items' => ":LabelNoUrut" ],
						      [ 'class' => "col-sm-2", 'items' => ":NoUrut" ],
					      ],
					    ],
				    ],
				  ],
				  [
					  'class' => "col-md-24",
					  'style' => "margin-bottom: 6px;",
					  'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelSDPembawaMotor" ],
					    [ 'class' => "col-sm-3", 'items' => ":SDPembawaMotor" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-1", 'items' => ":LabelSDHubunganPembawa" ],
					    [ 'class' => "col-sm-3", 'items' => ":SDHubunganPembawa", 'style' => 'padding-left:20px;' ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-1", 'items' => ":LabelSDAlasanServis" ],
					    [ 'class' => "col-sm-4", 'items' => ":SDAlasanServis" ],
					    //                            ['class' => "col-sm-1"],
					    [ 'class' => "col-sm-2", 'items' => ":LabelSDTotalWaktu", 'style' => 'padding-left:20px;' ],
					    [ 'class' => "col-sm-2", 'items' => ":StringTotalWaktu:SDTotalWaktu" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelSDTotalJasa", 'style' => 'padding-left:20px;' ],
					    [ 'class' => "col-sm-2", 'items' => ":SDTotalJasa" ],
				    ],
				  ],
				  [ 'class' => "col-sm-24", 'items' => "<hr>" ],
				  [
					  'class' => "col-md-24",
					  'style' => "margin-bottom: 6px;",
					  'items' => '<table id="scnDetailGrid"></table><div id="scnDetailGridPager"></div>'
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-13",
					      'items' => [
						      [ 'class' => "col-sm-5", 'items' => ":lblMyBrgKode", 'style' => 'padding-right:2px;' ],
						      [ 'class' => "col-sm-9", 'items' => ":lblBrgNama", 'style' => 'padding-right:2px;' ],
						      [ 'class' => "col-sm-5", 'items' => ":lblLokasiKode", 'style' => 'padding-right:2px;' ],
						      [ 'class' => "col-sm-3", 'items' => ":lblStock", 'style' => 'padding-right:2px;' ],
						      [ 'class' => "col-sm-2", 'items' => ":BtnGdg" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-11",
					      'items' => [
						      [ 'class' => "col-sm-2", 'items' => ":LabelCetak", 'style' => 'padding-left:5px;' ],
						      [ 'class' => "col-sm-3", 'items' => ":Cetak" ],
						      [ 'class' => "col-sm-4", 'items' => ":LabelSDTotalQty", 'style' => 'padding-left:15px;' ],
						      [ 'class' => "col-sm-2", 'items' => ":SDTotalQty" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-5", 'items' => ":LabelSDTotalPart" ],
						      [ 'class' => "col-sm-7", 'items' => ":SDTotalPart" ],
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-13",
				    'items' => [
					    [ 'class' => "form-group col-md-24",
					      'items' => [
						      [ 'class' => "col-sm-4", 'items' => ":LabelMekanik" ],
						      [ 'class' => "col-sm-10", 'items' => ":Mekanik" ],
                              ['class' => "col-md-4", 'items' => ":KarKodeSA"],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelJenis" ],
						      [ 'class' => "col-sm-3", 'items' => ":Jenis" ]
					      ],
					    ],
					    [ 'class' => "form-group col-md-24",
					      'items' => [
						      [ 'class' => "col-sm-4", 'items' => ":LabelStatus_" ],
						      [ 'class' => "col-sm-4", 'items' => ":Status_" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelDurasi" ],
						      [ 'class' => "col-sm-3", 'items' => ":Durasi", 'style' => 'padding-right:5px;' ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelMenit" ],
						      [ 'class' => "col-sm-1", 'items' => ":StartStopMekanik" ],
						      [ 'class' => "col-sm-2" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelTOP" ],
						      [ 'class' => "col-sm-2", 'items' => ":TOP" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-1", 'items' => ":LabelHari", 'style' => 'padding-left:5px;' ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-24",
					      'items' => [
						      [ 'class' => "col-sm-4", 'items' => ":LabelKeterangan" ],
						      [ 'class' => "col-sm-10", 'items' => ":Keterangan" ],
                              [ 'class' => "col-sm-1" ],
                              [ 'class' => "col-sm-2", 'items' => ":LabelNPWP" ],
                              [ 'class' => "col-sm-4", 'items' => ":NPWP", 'style' => 'padding-right:4px;' ],
//						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-3", 'items' => ":BtnOPL" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-24",
					      'items' => [

						      [ 'class' => "col-sm-4", 'items' => ":LabelKeluhan" ],
						      [ 'class' => "col-sm-10", 'items' => ":Keluhan" ],
						      [ 'class' => "col-sm-1" ],
                              ['class' => "col-md-2", 'items' => ":LabelZnoWorkOrder"],
                              ['class' => "col-md-4", 'items' => ":ZnoWorkOrder", 'style' => 'padding-right:4px;'],
//                              [ 'class' => "col-sm-1" ],
                              [ 'class' => "col-sm-3", 'items' => ":BtnBayar" ],
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-11",
				    'items' => [
					    [ 'class' => "form-group col-md-24",
					      'items' => [
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-4", 'items' => ":LabelOPL" ],
						      [ 'class' => "col-sm-6", 'items' => ":OPL" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-5", 'items' => ":LabelTotalJasa" ],
						      [ 'class' => "col-sm-7", 'items' => ":TotalJasaPart" ]
					      ],
					    ],
					    [ 'class' => "form-group col-md-24",
					      'items' => [
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-4", 'items' => ":LabelTotalBayar" ],
						      [ 'class' => "col-sm-6", 'items' => ":TotalBayar" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-5", 'items' => ":LabelDisc" ],
						      [ 'class' => "col-sm-2", 'items' => ":SDDiscPersen" ],
						      [ 'class' => "col-sm-1", 'items' => ":LabelPersen", 'style' => 'padding-left:4px;' ],
						      [ 'class' => "col-sm-4", 'items' => ":SDDiscFinal" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-24",
					      'items' => [
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-4", 'items' => ":LabelSisaBayar" ],
						      [ 'class' => "col-sm-6", 'items' => ":SisaBayar" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-5", 'items' => ":LabelSDUangMuka" ],
						      [ 'class' => "col-sm-7", 'items' => ":SDUangMuka" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-24",
					      'items' => [
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-4", 'items' => ":LabelStatusBayar" ],
						      [ 'class' => "col-sm-4", 'items' => ":StatusBayar" ],
						      [ 'class' => "col-sm-2", 'items' => ":BtnReload" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-5", 'items' => ":LabelEstimasi" ],
						      [ 'class' => "col-sm-7", 'items' => ":Estimasi" ],
					      ],
					    ],
				    ],
				  ],
			  ]
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-12", 'style' => 'position: absolute;top: -35px;right:0px;',
				    'items' => [
					    [ 'class' => "col-sm-12"],
					    [ 'class' => "col-sm-2", 'items' => ":LabelJTSD" ],
					    [ 'class' => "col-sm-4", 'items' => ":JTSD" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelJTSV" ],
					    [ 'class' => "col-sm-4", 'items' => ":JTSV" ],
				    ],
				  ],
                  [ 'class' => "form-group col-md-16",
                      'items' => [
                              [
                              'class' => "col-md-12 pull-left",
                              'items' => $this->render( '../_nav', [
                                  'url'=> $_GET['r'],
                                  'options'=> [
                                      'SDNo'              => 'No SD',
                                      'SVNo'              => 'No SV',
                                      'SENo'              => 'No SE',
                                      'NoGLSD'            => 'No GL SD',
                                      'NoGLSV'            => 'No GL SV',
                                      'KodeTrans'         => 'Kode Trans',
                                      'ASS'               => 'ASS',
                                      'PosKode'           => 'Pos Kode',
                                      'KarKode'           => 'Kar Kode',
                                      'PrgNama'           => 'Prg Nama',
                                      'PKBNo'             => 'Pkb No',
                                      'ttsdhd.MotorNoPolisi'     => 'Motor No Polisi',
                                      'tdcustomer.MotorNoMesin'     => 'Nomor Mesin',
                                      'LokasiKode'        => 'Lokasi Kode',
                                      'SDNoUrut'          => 'No Urut',
                                      'SDJenis'           => 'Jenis',
                                      'SDStatus'          => 'Status',
                                      'SDKeluhan'         => 'Keluhan',
                                      'SDKeterangan'      => 'Keterangan',
                                      'SDPembawaMotor'    => 'Pembawa Motor',
                                      'SDHubunganPembawa' => 'Hubungan Pembawa',
                                      'SDAlasanSErvis'    => 'Alasan SErvis',
                                      'KlaimKPB'          => 'Klaim Kpb',
                                      'KlaimC2'           => 'Klaim C2',
                                      'Cetak'             => 'Cetak',
                                      'UserID'            => 'User ID',
                                  ],
                              ])
                          ],
                              [ 'class' => "col-sm-1", 'items' => ":LabelPOS" ],
                              [ 'class' => "col-sm-5", 'items' => ":Riwayat" ],
                              [ 'class' => "col-sm-2", 'items' => ":LabelRiwayat" ],
                              [ 'class' => "col-sm-1", 'items' => ":SearchRiwayat" ],
                              [ 'class' => "col-sm-2", 'items' => ":CusPeringkat", 'style' => 'margin-left:15px;' ],
                          ],
				  ],
				  [ 'class' => "col-md-8",
				    'items' => [
					    [ 'class' => "pull-right", 'items' => ":btnImport :btnJurnal  :btnPrint :BtnPrint2 :btnAction " ]
				    ]
				  ],
			  ]
			],
			[
				":LabelSDNo"              => $Jenis == 'SD' ? '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor SD</label>' : '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor SV</label>',
				":LabelCusNama"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama</label>',
				":LabelTgl"               => "<label class=\"control-label\" style=\"margin: 0; padding: 6px 0;\">Tgl $Jenis</label>",
				":LabelSENo"              => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Estimasi</label>',
				":LabelKodeTrans"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">TC</label>',
				":LabelMotorNoPolisi"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">No. Polisi</label>',
				":LabelMotorType"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Type</label>',
				":LabelSDKmSekarang"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Km. Sekarang</label>',
				":LabelPrgNama"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">Program</label>',
				":LabelSDPembawaMotor"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pembawa</label>',
				":LabelMotorWarna"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Warna</label>',
				":LabelSDKmBerikut"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Km. Berikutnya</label>',
				":LabelPKBNo"             => '<label class="control-label" style="margin: 0; padding: 6px 0;">No PKB</label>',
				":LabelNoUrut"            => '<label class="control-label" style="margin: 0; padding: 6px 0;">No</label>',
				":LabelSDHubunganPembawa" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Hubungan</label>',
				":LabelSDAlasanServis"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alasan</label>',
				":LabelSDTotalWaktu"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total Waktu</label>',
				":LabelSDTotalJasa"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total Jasa</label>',
				":LabelMekanik"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">Mekanik / SA</label>',
				":LabelJenis"             => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				":LabelOPL"               => '<label class="control-label" style="margin: 0; padding: 6px 0;">OPL</label>',
				":LabelTotalJasa"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total Jasa Part</label>',
				":LabelStatusBayar"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status Bayar</label>',
				":LabelStatus_"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',
				":LabelDurasi"            => '<label class="control-label" style="margin: 0; padding: 6px 0;">Durasi</label>',
				":LabelMenit"             => '<label class="control-label" style="margin: 0; padding: 6px 0;">Menit</label>',
				":LabelTOP"               => '<label class="control-label" style="margin: 0; padding: 6px 0;">TOP</label>',
				":LabelTotalBayar"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total Bayar</label>',
				":LabelDisc"              => '<label class="control-label" style="margin: 0; padding: 6px 0;">Disc Final</label>',
				":LabelPersen"            => '<label class="control-label" style="margin: 0; padding: 6px 0;">%</label>',
				":LabelKeterangan"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelKeluhan"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keluhan</label>',
				":LabelSisaBayar"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sisa Bayar</label>',
				":LabelSDUangMuka"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Uang Muka</label>',
				":LabelEstimasi"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">Estimasi Biaya</label>',
				":LabelHari"              => '<label class="control-label" style="margin: 0; padding: 6px 0;">Hari</label>',
				":LabelSDTotalPart"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total Part</label>',
				":LabelCetak"             => '<label class="control-label" style="margin: 0; padding: 6px 0;">Cetak</label>',
				":LabelSDTotalQty"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total Qty</label>',
				":LabelRiwayat"           => '<label class="control-label" style="margin: 0; padding: 6px 0;"> &nbsp&nbsp&nbspRiwayat </label>',
				":LabelJTSD"              => \common\components\General::labelGL( $dsTServis[ 'NoGLSD' ], ' &nbspJT - SD' ),
				":LabelJTSV"              => \common\components\General::labelGL( $dsTServis[ 'NoGLSV' ], ' &nbspJT - SV' ),
				":LabelPOS"               => '<label class="control-label" style="margin: 0; padding: 6px 0;">POS</label>',
				":SDNo"                   => Html::textInput( 'SDNoView', $dsTServis[ 'SDNoView' ], [ 'class' => 'form-control', 'readonly' => 'readonly', 'tabindex' => '1' ] ) .
				                             Html::hiddenInput( 'SDNo', $dsTServis[ 'SDNo' ] ) .
				                             Html::hiddenInput( 'StatPrint', null, [ 'id' => 'StatPrint' ] ) .
				                             Html::hiddenInput( 'SDPickingNo', null, [ 'id' => 'SDPickingNo' ] ),
				":SVNo"                   => Html::textInput( 'SVNo', $dsTServis[ 'SVNo' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":SDTgl"                  => $Jenis == 'SD' ? FormField::dateInput( [ 'name' => 'SDTgl', 'config' => [ 'value' => $dsTServis[ 'SDTgl' ] ] ] ) : Html::hiddenInput( 'SDTgl', $dsTServis[ 'SDTgl' ] ),
				":SDJam"                  => $Jenis == 'SD' ? FormField::timeInput( [ 'name' => 'SDJam', 'config' => [ 'value' => $dsTServis[ 'SDJam' ] ] ] ) : Html::hiddenInput( 'SDJam', $dsTServis[ 'SDJam' ] ),
				":SVTgl"                  => $Jenis == 'SV' ? FormField::dateInput( [ 'name' => 'SVTgl', 'config' => [ 'value' => $dsTServis[ 'SVTgl' ] ] ] ) : Html::hiddenInput( 'SVTgl', $dsTServis[ 'SVTgl' ] ),
				":SVJam"                  => $Jenis == 'SV' ? FormField::timeInput( [ 'name' => 'SVJam', 'config' => [ 'value' => $dsTServis[ 'SVJam' ] ] ] ) : Html::hiddenInput( 'SVJam', $dsTServis[ 'SVJam' ] ),
				":MotorNoPolisi"          => Html::textInput( 'MotorNoPolisi', $dsTServis[ 'MotorNoPolisi' ], [ 'class' => 'form-control', 'maxlength' => '12', 'tabindex' => '5' ] ).
                                             Html::textInput( 'MotorNoMesin', '', [ 'class' => 'hidden' ] ).
                                             Html::textInput( 'MotorType', '', [ 'class' => 'hidden' ] ).
                                             Html::textInput( 'CusTlp', '', [ 'class' => 'hidden' ] ),
				":CusNama"                => Html::textInput( 'CusNama', $dsTServis[ 'CusNama' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":MotorWarna"             => Html::textInput( 'MotorWarna', $dsTServis[ 'MotorWarna' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":MotorType"              => Html::textInput( 'MotorNama', $dsTServis[ 'MotorNama' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":SENo"                   => Html::textInput( 'SENo', $dsTServis[ 'SENo' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":KodeTrans"              => FormField::combo( 'SDTC', [ 'name' => 'KodeTrans', 'config' => [ 'id'=> 'KodeTrans', 'value' => $dsTServis[ 'KodeTrans' ] ], 'options' => [ 'tabindex' => '4' ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":ASS"                    => FormField::combo( 'ASS', [ 'config' => [ 'value' => $dsTServis[ 'ASS' ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":SDKmSekarang"           => Html::textInput( 'SDKmSekarang', $dsTServis[ 'SDKmSekarang' ], [ 'id' => 'SDKmSekarang', 'class' => 'form-control', 'style' => 'text-align:right;', 'maxlength' => '6', 'tabindex' => '9' ] ),
				":SDKmBerikut"            => Html::textInput( 'SDKmBerikut', $dsTServis[ 'SDKmBerikut' ], [ 'id' => 'SDKmBerikut','class' => 'form-control', 'style' => 'text-align:right;', 'maxlength' => '6', 'tabindex' => '11' ] ),
				":PrgNama"                => FormField::combo( 'Program', [ 'name' => 'PrgNama', 'config' => [ 'value' => $dsTServis[ 'PrgNama' ] ], 'options' => [ 'id' => 'PrgNama', 'tabindex' => '10' ], 'extraOptions' => [ 'simpleCombo' => true,'altCondition'=> ":SDTgl BETWEEN PrgTglMulai  AND PrgTglAkhir",'altParams'=>   [':SDTgl'=>$dsTServis[ 'SDTgl'] ]] ] ),
				":PKBNo"                  => Html::textInput( 'PKBNo', $dsTServis[ 'PKBNo' ], [ 'class' => 'form-control', 'maxlength' => '20', 'tabindex' => '12' ] ),
				":NoUrut"                 => Html::textInput( 'SDNoUrut', $dsTServis[ 'SDNoUrut' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":SDPembawaMotor"         => Html::textInput( 'SDPembawaMotor', $dsTServis[ 'SDPembawaMotor' ], [ 'class' => 'form-control', 'maxlength' => '75', 'tabindex' => '13' ] ),
				":SDHubunganPembawa"      => FormField::combo( 'HubunganPembawaMotor', [ 'name' => 'SDHubunganPembawa', 'config' => [ 'value' => $dsTServis[ 'SDHubunganPembawa' ] ], 'options' => [ 'tabindex' => '14' ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":SDAlasanServis"         => FormField::combo( 'SDAlasanServis', [ 'config' => [ 'value' => $dsTServis[ 'SDAlasanServis' ] ], 'options' => [ 'tabindex' => '15' ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":SDTotalWaktu"           => Html::hiddenInput( 'SDTotalWaktu', $dsTServis[ 'SDTotalWaktu' ], [ 'class' => 'form-control', 'id' => 'SDTotalWaktu' ] ),
				":StringTotalWaktu"       => Html::textInput( 'StringTotalWaktu', $dsTServis[ 'SDTotalWaktu' ], [ 'class' => 'form-control', 'id' => 'StringTotalWaktu', 'readonly' => 'readonly' ] ),
				":SDTotalJasa"            => FormField::numberInput( [ 'name' => 'SDTotalJasa', 'config' => [ 'value' => $dsTServis[ 'SDTotalJasa' ], 'readonly' => 'readonly', 'id' => 'SDTotalJasa' ] ] ),
				":lblMyBrgKode"           => Html::textInput( 'KodeBarang', '--', [ 'id' => 'KodeBarang', 'class' => 'form-control', 'style' => 'color: blue;', 'readonly' => 'readonly' ] ),
				":lblBrgNama"             => Html::textInput( 'BarangNama', '--', [ 'id' => 'BarangNama', 'class' => 'form-control', 'style' => 'color: blue;', 'readonly' => 'readonly' ] ),
				":lblLokasiKode"          => Html::textInput( 'LokasiKode', 'Kosong', [ 'class' => 'form-control', 'style' => 'color: blue;', 'readonly' => 'readonly' ] ),
				":lblStock"               => Html::textInput( 'lblStock', 0, [ 'type' => 'number', 'class' => 'form-control', 'style' => 'color: blue; font-weight: bold;', 'readonly' => 'readonly' ] ),
				":Cetak"                  => Html::textInput( 'Cetak', $dsTServis[ 'Cetak' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":SDTotalQty"             => Html::textInput( 'SDTotalQty', $dsTServis[ 'SDTotalQty' ], [ 'class' => 'form-control', 'readonly' => 'readonly', 'id' => 'SDTotalQty' ] ),
				":SDTotalPart"            => FormField::numberInput( [ 'name' => 'SDTotalPart', 'config' => [ 'value' => $dsTServis[ 'SDTotalPart' ], 'readonly' => 'readonly', 'id' => 'SDTotalPart' ] ] ),
				":Mekanik"                => FormField::combo( 'KarKode', [ 'config' => [ 'value' => $dsTServis[ 'KarKode' ] ], 'options' => [ 'tabindex' => '16','disabled'=>'disabled' ], 'extraOptions' => [ 'altLabel' => [ 'KarNama' ] ] ] ),
				":Jenis"                  => FormField::combo( 'JenisServis', [ 'name' => 'SDJenis', 'config' => [ 'value' => $dsTServis[ 'SDJenis' ] ], 'options' => [ 'tabindex' => '17' ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":Status_"                => FormField::combo( 'ServisStatus', [ 'name' => 'SDStatus', 'config' => [ 'value' => $dsTServis[ 'SDStatus' ] ], 'options' => [ 'tabindex' => '18' ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":Durasi"                 => Html::textInput( 'SDDurasi', $dsTServis[ 'SDDurasi' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":TOP"                    => Html::textInput( 'SDTOP', $dsTServis[ 'SDTOP' ], [ 'class' => 'form-control', 'maxlength' => '3' ] ),
				":Keterangan"             => Html::textInput( 'SDKeterangan', $dsTServis[ 'SDKeterangan' ], [ 'class' => 'form-control', 'maxlength' => '150' ] ),
				":Keluhan"                => Html::textInput( 'SDKeluhan', $dsTServis[ 'SDKeluhan' ], [ 'class' => 'form-control', 'maxlength' => '150' ] ),
				":OPL"                    => FormField::numberInput( [ 'name' => 'SDKasKeluar', 'config' => [ 'value' => $dsTServis[ 'SDKasKeluar' ], 'readonly' => 'readonly' ] ] ),
				":TotalBayar"             => FormField::numberInput( [ 'name' => 'SDTerbayar', 'config' => [ 'value' => $dsTServis[ 'SDTerbayar' ], 'readonly' => 'readonly' ] ] ),
				":SisaBayar"              => FormField::numberInput( [ 'name' => 'SisaBayar', 'config' => [ 'value' => 0.00, 'readonly' => 'readonly' ] ] ),
				":StatusBayar"            => Html::textInput( 'StatusBayar', $dsTServis[ 'StatusBayar' ], [ 'class' => 'form-control', 'style' => 'background-color: pink;', 'readonly' => 'readonly' ] ),
				":TotalJasaPart"          => FormField::numberInput( [ 'name' => 'TotalJasaPart', 'config' => [ 'value' => 0.00, 'readonly' => 'readonly' ] ] ),
				":SDDiscPersen"           => Html::textInput( 'SDDiscPersen', 0, [ 'id' => 'SDDiscPersen', 'class' => 'form-control', 'maxlength' => '2' ] ),
				":SDDiscFinal"            => FormField::numberInput( [ 'name' => 'SDDiscFinal', 'config' => [ 'value' => $dsTServis[ 'SDDiscFinal' ] ] ] ),
				":SDUangMuka"             => FormField::numberInput( [ 'name' => 'SDUangMuka', 'config' => [ 'value' => $dsTServis[ 'SDUangMuka' ], 'readonly' => 'readonly' ] ] ),
				":Estimasi"               => FormField::numberInput( [ 'name' => 'SDTotalBiaya', 'config' => [ 'value' => $dsTServis[ 'SDTotalBiaya' ], 'readonly' => 'readonly' ] ] ),
//				":Riwayat"                => Menu::showOnlyHakAkses( [ 'Admin', 'Auditor' ] ) ? FormField::combo( 'PosKode', [ 'config' => [ 'value' => $dsTServis[ 'PosKode' ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ) :
//					Html::textInput( 'PosKode', $dsTServis[ 'PosKode' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
                ":Riwayat"                => FormField::combo( 'PosKode', [ 'config' => [ 'value' => $dsTServis[ 'PosKode' ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":JTSV"                   => Html::textInput( 'NoGLSV', $dsTServis[ 'NoGLSV' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":JTSD"                   => Html::textInput( 'NoGLSD', $dsTServis[ 'NoGLSD' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":BtnCariMotor"           => Html::button( '<i class="glyphicon glyphicon-search"></i>', [ 'class' => 'btn btn-default', 'id' => 'BtnCariMotor', 'tabindex' => '6' ] ),
				":BtnEditMotor"           => Html::button( '<i class="glyphicon glyphicon-user"></i>', [ 'class' => 'btn btn-default', 'id' => 'BtnEditMotor', 'tabindex' => '7' ] ),
				":BtnNewMotor"            => Html::button( '<i class="fa fa-user-plus"></i>', [ 'class' => 'btn btn-default', 'id' => 'BtnNewMotor', 'tabindex' => '8' ] ),
				":BtnBayar"               => Html::button( '<i class="fa fa-thumbs-up"></i> Bayar', [ 'class' => 'btn btn-success', 'style' => 'width:76px', 'id' => 'BtnBayar' ] ),
				":BtnOPL"                 => Html::button( '<i class="fas fa-tools fa-lg"></i> OPL', [ 'class' => 'btn btn-primary', 'id' => 'BtnOPL', 'style' => 'width:76px' ] ),
				":StartStopMekanik"       => Html::button( '<i class="fa fa-clock-o fa-lg"></i>', [ 'class' => 'btn btn-default', 'id' => 'BtnMekanik' ] ),
				":SearchRiwayat"          => Html::button( '<i class="fa fa-clock-o fa-lg"></i>', [ 'class' => 'btn btn-default', 'id' => 'BtnRiwayat' ] ),
				":BtnInfo"                => Html::button( '<i class="fa fa-info-circle fa-lg"></i>', [ 'class' => 'btn btn-default', 'id' => 'BtnFindSE', 'tabindex' => '3' ] ),
				":BtnGdg"                 => Html::button( '<i class="fa fa-info-circle fa-lg"></i>', [ 'class' => 'btn btn-default' ] ),
				":BtnReload"              => Html::button( '<i class="fa fa-refresh fa-lg"></i>', [ 'class' => 'btn btn-default', 'id' => 'BtnStatus' ] ),
				":BtnSearchFB"            => Html::button( '<i class="glyphicon glyphicon-search"></i>', [ 'class' => 'btn btn-default', 'id' => 'BtnSearchFB' ] ),
				":BtnPrint2"              => Html::button( '<i class="glyphicon glyphicon-print"><br><br>Pick-L</i> ', [ 'class' => 'btn bg-fuchsia', 'id' => 'btnPrintPickingList','style' => 'height:60px' ] ),
//				":btnAdd"                 => Html::button( '<i class="fa fa-plus-circle"></i> ' . ( $Jenis == 'SD' ? 'Tambah' : 'Konfirm' ), [ 'class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:80px' ] ),
                ":btnImport"       => Html::button('<i class="fa fa-arrow-circle-down"><br><br>Import</i>', ['class' => 'btn bg-navy', 'id' => 'btnImport', 'style' => 'width:60px;height:60px']),
           //     ":btnGrid"                => Html::button( '<i class="fa fa-info-circle fa-lg"></i>', [ 'class' => 'btn btn-default', 'id' => 'btnGrid' ] ),

                ":LabelZnoWorkOrder"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">WO</label>',
                ":ZnoWorkOrder"           => Html::textInput('ZnoWorkOrder', $dsTServis[ 'ZnoWorkOrder' ], ['class' => 'form-control']),
                ":LabelKarKodeSA"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kar Kode SA</label>',
//                ":KarKodeSA"           => Html::textInput('KarKodeSA', $dsTServis[ 'KarKodeSA' ], ['class' => 'form-control']),
                ":KarKodeSA"           => FormField::combo( 'DaftarKarKodeSA', [ 'name' => 'KarKodeSA', 'config' => [ 'value' => $dsTServis[ 'KarKodeSA' ] ], 'extraOptions' => [ 'simpleCombo' => true,'altCondition'=> "KarKode <> '--' AND KarStatus = 'A' AND (KarJabatan = 'Servis Advisor' OR KarJabatan = 'Kepala Bengkel')", ] ] ),

                ":LabelNPWP"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">NPWP</label>',
                ":NPWP"           => Html::textInput('NPWP', $dsTServis[ 'NPWP' ], ['class' => 'form-control']),
                ":CusPeringkat"   => Html::textInput('CusPeringkat', $dsTServis[ 'CusPeringkat' ], ['class' => 'form-control']),
			],
			[
				'url_main'   => 'ttsdhd',
                'url_id' => $_GET['id'] ?? '',
                '_akses' => '',
				'jsBtnAdd'   => false,
				'jsBtnPrint' => false,
				'jsBtnJurnal' => false,
			]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlDetail     = $url[ 'detail' ];
$urlUpdate     = $url[ 'update' ];
$urlPrint      = $url[ 'print' ];
$urlBayar      = Url::toRoute( [ 'bayar/save' ] );
$urlCheckStock = Url::toRoute( [ 'site/check-stock-transaksi' ] );
$urlJasa       = Url::toRoute( [ 'ttsdhd/get-jasa-barang' ] );
$this->registerJsVar( '__Jenis', $Jenis );
$this->registerJsVar( '__model', $dsTServis );
$this->registerJsVar( '__urlDetailJasa', Url::toRoute( [ 'ttsditjasa/index' ] ) );
$this->registerJsVar( '__urlDetailBarang', Url::toRoute( [ 'ttsditbarang/index' ] ) );
$urlJurnal           = Url::toRoute( [ 'ttsdhd/' . ( $Jenis == 'SD' ? 'daftar-servis-jurnal' : 'invoice-servis-jurnal' ),'id' => $id, ] );
$urlIndex            = Url::toRoute( [ 'ttsdhd/' . ( $Jenis == 'SD' ? 'daftar-servis' : 'invoice-servis' ) ] );
$urlCancel           = Url::toRoute( [ 'ttsdhd/' . ( $Jenis == 'SD' ? 'daftar-servis-cancel' : 'invoice-servis-cancel' ), 'id' => $id, 'action' => 'create' ] );
$urlDelete           = Url::toRoute( [ 'ttsdhd/' . ( $Jenis == 'SD' ? 'daftar-servis-delete' : 'invoice-servis-delete' ), 'id' => $id, 'action' => 'create' ] );
$urlAdd              = Url::toRoute( [ 'ttsdhd/' . ( $Jenis == 'SD' ? 'daftar-servis-create' : 'konfirm' ), 'action' => 'create' ] );
$urlCustomer         = Url::toRoute( [ 'tdcustomer/select' , 'tgl1'=> '1900-01-01'] );
$urlCustomerEdit     = Url::toRoute( [ 'tdcustomer/edit' ] );
$urlCustomerCreate   = Url::toRoute( [ 'tdcustomer/create', 'action' => 'create' ] );
$urlCustomerFindById = Url::toRoute( [ 'tdcustomer/find-by-id' ] );
$urlPickingList      = Url::toRoute( [ 'ttsdhd/picking-list', 'id' => base64_encode( $dsTServis[ 'SDNo' ] ) ] );
$urlPrintPickingList = Url::toRoute( [ 'ttsdhd/print-picking-list' ] );
$urlLoop             = Url::toRoute( [ 'ttsdhd/loop' ] );
$urlRefresh          = $url[ 'update' ] . "&oper=skip-load&SDNoView=" . $dsTServis[ 'SDNoView' ];
$urlHistoryOtomatis  = Url::toRoute( [ 'ttsdhd/history-otomatis' ] );
//$urlImport           = Url::toRoute(['ttsdhd/import', 'tgl1' => Yii::$app->formatter->asDate('-5 day', 'yyyy-MM-dd')]);
$urlImport           = Url::toRoute(['ttsdhd/import', 'tgl1' => Yii::$app->formatter->asDate('now', 'yyyy-MM-dd')]);
$urlGrid             = Url::toRoute( [ 'ttsdhd/grid' ] );
$urlMekanikOtomatis  = Url::toRoute( [ 'ttsdhd/mekanik-otomatis', 'SDNo' => $dsTServis[ 'SDNo' ] ] );
$urlStatusOtomatis   = Url::toRoute( [ 'ttsdhd/status-otomatis', 'SDNo' => $dsTServis[ 'SDNo' ], 'SVNo' => $dsTServis[ 'SVNo' ] ] );
$urlBayarOtomatis    = Url::toRoute( [ 'ttbmhd/bayar-otomatis', 'NominalBayar' => $dsTServis[ 'SDTotalBiaya' ], ] );
$urlFindSE           = Url::to( [ 'ttsehd/select-sd' ] );
$urlSE               = Url::to( [ 'ttsehd/select-sd' ] );
$urlImportLoop 			= Url::to( [ 'ttsdhd/import-loop' ] );
$urlProgramIt 			= Url::to( [ 'tdprogramit/index' ] );
$this->registerJsVar( '__urlSE', $urlSE );
$this->registerJsVar( '__idCustomer', $dsTServis[ 'MotorNoPolisi' ] );
$this->registerJsVar( 'akses', Menu::getUserLokasi()[ 'KodeAkses' ] );
$resultKas     = \abengkel\models\Ttkmhd::find()->callSP( [ 'Action' => 'Insert' ] );
$resultBank    = \abengkel\models\Ttbmhd::find()->callSP( [ 'Action' => 'Insert' ] );
$resultKasOut  = \abengkel\models\Ttkkhd::find()->callSP( [ 'Action' => 'Insert' ] );
$resultBankOut = \abengkel\models\Ttbkhd::find()->callSP( [ 'Action' => 'Insert' ] );
$this->registerJs( <<< JS
/*--JASA--*/
    window.__JasaKode = [];
    window.__JasaNama = [];
    window.__BrgKode = [];
    window.__BrgNama = [];
    window.__Program = [];
    
    function setJasa(){
        
        $.ajax({
            url: '$urlJasa',
            type: "POST",
            data: { 
                SDTC: $('[name=KodeTrans]').val(),
                SDNo: $('[name=SDNo]').val(),
            },
            success: function (__Jasa) {
                // console.log(__Jasa);
                
                __JasaKode = __Jasa.jasa;
                __JasaNama = __Jasa.jasa;
                __BrgKode = __Jasa.barang;
                __BrgNama = __Jasa.barang;
                __Jasa = null;
                // var __JasaKode = JSON.parse(__Jasa.rows);
                __JasaKode = $.map(__JasaKode, function (obj) {
                  obj.id = obj.JasaKode;
                  obj.text = obj.JasaKode;
                  return obj;
                });
        
                window.__JasaKode = __JasaKode;
                // var __JasaNama = JSON.parse(__Jasa.rows);
                __JasaNama = $.map(__JasaNama, function (obj) {
                  obj.id = obj.JasaNama;
                  obj.text = obj.JasaNama;
                  return obj;
                });
                window.__JasaNama = __JasaNama;
                
                __BrgKode = $.map(__BrgKode, function (obj) {
                  obj.id = obj.BrgKode;
                  obj.text = obj.BrgKode;
                  return obj;
                });
                window.__BrgKode = __BrgKode;
                __BrgNama = $.map(__BrgNama, function (obj) {
                  obj.id = obj.BrgNama;
                  obj.text = obj.BrgNama;
                  return obj;
                });                
                window.__BrgNama = __BrgNama;
            },
        });       
    }   
    
    $('#SDKmSekarang').change(function() {
        let skrg = $(this).utilNumberControl().val();
        let next = parseInt(skrg) + 2000;
        $('#SDKmBerikut').val(next);
    });

    $('#PrgNama').change(function() {
        $.ajax({
            url: '$urlProgramIt',
            type: "POST",
            data: { 
                PrgNama: $(this).val(),
            },
            success: function (res) {
                console.log(res);
            	window.__Program = {};
            	$.each( res.rows, function( key, value ) {
							  window.__Program[value.BrgKode] = value;
							});
            },
            error: function (jXHR, textStatus, errorThrown) {
            },
        });
    });
    
    $('#KodeTrans').change(function() {
        setJasa();        
        $('#detailGrid').utilJqGrid().load({url: __urlDetailJasa, param: { SDNo: '$model->SDNo', SVNo: '$model->SVNo'} });
        $('#scnDetailGrid').utilJqGrid().load({url: __urlDetailBarang, param: { SDNo: '$model->SDNo', SVNo: '$model->SVNo',KodeTrans: $('#KodeTrans').val() } });
        let searchParams = new URLSearchParams(window.location.search);
        if (searchParams.get('action') !== 'view' && searchParams.get('action') !== 'display'){
            $('#BtnBayar').attr('disabled',$(this).val() != 'TC11');
            $('#BtnOPL').attr('disabled',$(this).val() != 'TC11');
        }
    		// var dataIDs = $('#scnDetailGrid').getDataIDs(); 
				// for(i = 0; i < dataIDs.length; i++)
				// {
				//     var rowData = $('#scnDetailGrid').jqGrid ('getRowData', dataIDs[i]);
							 // rowData.Val1= '12321'; 
							 // $('#scnDetailGrid').jqGrid('setRowData', dataIDs[i], rowData)
				//     console.log(rowData);
				// }
    });

    $('[name="ASS"]').change(function() {
        let ass = $(this).val();
        if (ass === 'ASS2' || ass === 'ASS3' || ass === 'ASS4'){
            $('#scnDetailGrid').closest('.ui-jqgrid').block({message: null,overlayCSS:{opacity:0.2}});
        }else{
            $('#scnDetailGrid').closest('.ui-jqgrid').unblock();
        }
    });
    
     window.HitungBawah = function (){
        let TotalJasaPart = parseFloat($('#SDTotalPart').val()) + parseFloat($('#SDTotalJasa').val());
        let result = window.util.calculatePrice({
            qty: 1,
            price: TotalJasaPart,
            disc: $('#SDDiscPersen').val(),
            discRp: $('#SDDiscFinal').val()
        });                 
        $('#TotalJasaPart').utilNumberControl().val(TotalJasaPart);               
        $('#SDDiscPersen').utilNumberControl().val(result.disc);
        $('#SDDiscFinal').utilNumberControl().val(result.discRp);
        let SDUangMuka = parseFloat($('#SDUangMuka').val());                
        let SDTotalBiaya = result.total - SDUangMuka;
        $('#SDTotalBiaya').utilNumberControl().val(SDTotalBiaya);
        
        let TotalBayar = parseFloat($('#SDTerbayar').val());   
        let SisaBayar = SDTotalBiaya - TotalBayar;
        $('#SisaBayar').utilNumberControl().val(SisaBayar);
        
        if(SisaBayar  > 0 || SDTotalBiaya === 0){         	    
            $('[name=StatusBayar]').css({"background-color" : "yellow"});
            if(TotalBayar === 0) $('[name=StatusBayar]').css({"background-color" : "pink"});
        } else if(SisaBayar <= 0){
            $('[name=StatusBayar]').css({"background-color" : "lime"});
        }   
        let StatusBayar = $('[name=StatusBayar]').val();
        if(StatusBayar === 'BELUM'){
            $('[name=StatusBayar]').css({"background-color" : "yellow"});
        }
    }
    
     window.hitungLineJasa = function (){
        //console.log('hitung');   
        let result = window.util.calculatePrice({
            qty: 1,
            price: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Jasa+'_'+'SDHrgJual'+'"]').val()),
            disc: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Jasa+'_'+'Disc'+'"]').val()),
            discRp: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Jasa+'_'+'SDJualDisc'+'"]').val())
        })
        $('[id="'+window.cmp.rowid_Jasa+'_'+'Disc'+'"]').val(result.disc);
        $('[id="'+window.cmp.rowid_Jasa+'_'+'SDJualDisc'+'"]').val(result.discRp);
        $('[id="'+window.cmp.rowid_Jasa+'_'+'Jumlah'+'"]').val(result.total);
        window.HitungTotalJasa();
    };
    
    window.HitungTotalJasa = function (){
        let SDTotalWaktu = dgJasa.jqGrid('getCol','SDWaktuKerja',false,'sum');
        let SDTotalJasa = dgJasa.jqGrid('getCol','Jumlah',false,'sum');
        $('#SDTotalWaktu').utilNumberControl().val(SDTotalWaktu);
        $('#StringTotalWaktu').val(window.util.minuteToString(SDTotalWaktu));
        $('#SDTotalJasa').utilNumberControl().val(SDTotalJasa);
        window.HitungBawah();
    };
    
    var dgJasa = $('#detailGrid');
     dgJasa.utilJqGrid({
        editurl: __urlDetailJasa,
        height: 120,
        extraParams: {
            SDNo: '$model->SDNo',
            SVNo: '$model->SVNo'
        },
        rownumbers: true,
        rowNum: 1000,
        colModel: [
            { template: 'actions' },
            {
                name: 'JasaKode',
                label: 'Kode',
                width: 115,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __JasaKode,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    // console.log(data);
						    let rowid = $(this).attr('rowid');
                            $('[id="'+rowid+'_'+'SDWaktuKerja'+'"]').val(data.JasaWaktuKerja);
						    $('[id="'+rowid+'_'+'SDWaktuSatuan'+'"]').val(data.JasaWaktuSatuan);
                            $('[id="'+rowid+'_'+'SDHrgJual'+'"]').utilNumberControl().val(data.JasaHrgJual);
                            $('[id="'+rowid+'_'+'JasaNama'+'"]').val(data.JasaNama); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'JasaNama'+'"]').trigger('change');
                            window.hitungLineJasa();
						}
                    }                 
                }
            },
            {
                name: 'JasaNama',
                label: 'Nama Jasa',
                width: 222,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __JasaNama,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    // console.log(data);
						    let rowid = $(this).attr('rowid');
                            $('[id="'+rowid+'_'+'SDWaktuKerja'+'"]').val(data.JasaWaktuKerja);
						    $('[id="'+rowid+'_'+'SDWaktuSatuan'+'"]').val(data.JasaWaktuSatuan);
                            $('[id="'+rowid+'_'+'SDHrgJual'+'"]').utilNumberControl().val(data.JasaHrgJual);
                            $('[id="'+rowid+'_'+'JasaKode'+'"]').val(data.JasaKode); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'JasaKode'+'"]').trigger('change');
                            window.hitungLineJasa();
						}
                    }              
                }
            },
            {
                name: 'SDWaktuKerja',
                label: 'Waktu',
                editable: true,
                template: 'number',
                width: 100,
                editor: { readOnly: true }
            },     
            {
                name: 'SDWaktuSatuan',
                label: 'Satuan',
                editable: true,
                width: 80,
                editor: { type: 'text', readOnly: true }
            },      
            {
                name: 'SDHrgJual',
                label: 'Harga Jasa',
                editable: true,
                width: 110,
                template: 'money',
                editoptions:{
                    maxlength: 48
                },
                editor: {
                    events:{
                        'change': function (e) {window.hitungLineJasa();}
                    }              
                }
            },   
            {
                name: 'Disc',
                label: 'Disc %.',
                editable: true,
                template: 'number',
                editoptions:{
                    maxlength: 5
                },
                width: 70,
                editor: {
                    events:{
                        'change': function (e) {
                           let result = window.util.calculatePrice({
                                qty: 1,
                                price: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Jasa+'_'+'SDHrgJual'+'"]').val()),
                                disc: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Jasa+'_'+'Disc'+'"]').val())
                            })
                            $('[id="'+window.cmp.rowid_Jasa+'_'+'SDJualDisc'+'"]').val(result.discRp);
                            $('[id="'+window.cmp.rowid_Jasa+'_'+'Jumlah'+'"]').val(result.total);
                            window.HitungTotalJasa();
                        }
                    }              
                }
            },     
            {
                name: 'SDJualDisc',
                label: 'Discount',
                width: 110,
                editable: true,
                template: 'money',
                editoptions:{
                    maxlength: 48
                },
                editor: {
                    events:{
                        'change': function (e) {
                            let result = window.util.calculatePrice({
                                qty: 1,
                                price: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Jasa+'_'+'SDHrgJual'+'"]').val()),
                                discRp: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Jasa+'_'+'SDJualDisc'+'"]').val())
                            })
                            $('[id="'+window.cmp.rowid_Jasa+'_'+'Disc'+'"]').val(result.disc);
                            $('[id="'+window.cmp.rowid_Jasa+'_'+'Jumlah'+'"]').val(result.total);
                            window.HitungTotalJasa();
                        }
                    }              
                }
            },
            {
                name: 'Jumlah',
                label: 'Jumlah',
                width: 110,
                editable: true,
                template: 'money',
                editor: { readOnly: true }
            },
            {
                name: 'SDAuto',
                // label: 'Jumlah',
                width: 100,
                editable: true,
                // template: 'money',
                // editor: { readOnly: true }
                hidden:true
            },
            {
                name: 'KodeTransJasa',
                editable: true,
                hidden:true
            },
            {
                name: 'PrgNamaJasa',
                editable: true,
                hidden:true
            },
        ],
        listeners: {
            afterLoad: function (data) { 
                let jQty = $('#detailGrid').jqGrid('getGridParam', 'records');
                let bQty = $('#scnDetailGrid').jqGrid('getGridParam', 'records');
                let param = ['Admin', 'Auditor'];
                if(!param.includes(akses)){
                    if ((jQty + bQty) > 0 && GLValid){
                        $("#KodeTrans").attr('disabled', true);
                        $("#KodeTransHidden").remove();
                        $("#form_ttsdhd_id").append("<input id='KodeTransHidden' name='KodeTrans' type='hidden' value='"+$("[id=KodeTrans]").val()+"'>");
                    }else{
                        $("#KodeTrans").attr('disabled', false);
                        $("#KodeTransHidden").remove();
                    }
                }
                window.HitungTotalJasa();
            }     
        }
     }).init().fit($('.box-body'));
     
    dgJasa.bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
        window.cmp.rowid_Jasa = rowid;    
        var JasaKode = $('[id="'+rowid+'_'+'JasaKode'+'"]');
        JasaKode.select2('destroy').empty().select2({
            data: $.map(window.__JasaKode, function (obj) {
                      obj.id = obj.JasaKode;
                      obj.text = obj.JasaKode;
                      return obj;
                    })
            });    
        var JasaNama = $('[id="'+rowid+'_'+'JasaNama'+'"]');
        JasaNama.select2('destroy').empty().select2({
            data: $.map(window.__JasaNama, function (obj) {
                      obj.id = obj.JasaNama;
                      obj.text = obj.JasaNama;
                      return obj;
                    })
            });
        if(rowid.includes("new_row")){
            $('[id="'+rowid+'_'+'SDWaktuKerja'+'"]').val(0);
            $('[id="'+rowid+'_'+'SDWaktuSatuan'+'"]').val('MENIT');
            $('[id="'+rowid+'_'+'Disc'+'"]').utilNumberControl().val(0.00);
            $('[id="'+rowid+'_'+'JasaKode'+'"]').trigger({
                type: 'select2:select',
                params: {
                    data: window.__JasaKode[0]
                }
            });
        }
        $('[id="'+rowid+'_'+'KodeTransJasa'+'"]').val($('#KodeTrans').val());
        $('[id="'+rowid+'_'+'PrgNamaJasa'+'"]').val($('#PrgNama').val());
    });
    
   
     
/*--BARANG--*/

    window.hitungLineBarang = function (){
        if($('[name=KodeTrans]').val() != 'TC14'){
            var SDHrgJual =  window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Barang+'_'+'SDHrgJual'+'"]').val());
            var BrgHrgJual = parseFloat(dgBarang.getRowData(window.cmp.rowid_Barang)['BrgHrgJual']);
            // console.log(SDHrgJual,BrgHrgJual);
            if(SDHrgJual < BrgHrgJual){
                $("[id='jCancelButton_scnDetailGrid_"+window.cmp.rowid_Barang+"']").click();
                bootbox.alert({message:'Harga Part minimal '+BrgHrgJual+'',size: 'small'});
                return;
            }
        }
        let result = window.util.calculatePrice({
            qty: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Barang+'_'+'SDQty'+'"]').val()),
            price: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Barang+'_'+'SDHrgJual'+'"]').val()),
            disc: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Barang+'_'+'Disc'+'"]').val()),
            discRp: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Barang+'_'+'SDJualDisc'+'"]').val()),
        })
        $('[id="'+window.cmp.rowid_Barang+'_'+'Disc'+'"]').utilNumberControl().val(result.disc);
        $('[id="'+window.cmp.rowid_Barang+'_'+'SDJualDisc'+'"]').utilNumberControl().val(result.discRp);
        $('[id="'+window.cmp.rowid_Barang+'_'+'Jumlah'+'"]').utilNumberControl().val(result.total);
        window.HitungTotalBarang();
    };
     
    window.HitungTotalBarang = function (){
        let SDTotalQty = dgBarang.jqGrid('getCol','SDQty',false,'sum');
        let SDTotalPart = dgBarang.jqGrid('getCol','Jumlah',false,'sum');
        $('#SDTotalQty').utilNumberControl().val(SDTotalQty);
        $('#SDTotalPart').utilNumberControl().val(SDTotalPart);
        window.HitungBawah();
    }
    
    function checkStok(BrgKode,BarangNama){
           $.ajax({
                url: '$urlCheckStock',
                type: "POST",
                data: { 
                    BrgKode: BrgKode,
                },
                success: function (res) {
                    $('[name=LokasiKode]').val(res.LokasiKode);
                    $('#KodeBarang').val(BrgKode);
                    $('[name=BarangNama]').val(BarangNama);
                    $('[name=lblStock]').val(res.SaldoQty);
                },
                error: function (jXHR, textStatus, errorThrown) {
                },
                async: false
            });
          }
    window.checkStok = checkStok;    
    var dgBarang = $('#scnDetailGrid');
     dgBarang.utilJqGrid({
        editurl: __urlDetailBarang,
        height: 120,
        extraParams: {
            SDNo: '$model->SDNo',
            SVNo: '$model->SVNo',
        },
        rownumbers: true,
        rowNum: 1000,
        colModel: [
            { template: 'actions' },
            {
                name: 'BrgKode',
                label: 'Kode',
                width: 115,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __BrgKode,
                    events:{
                        'select2:select': function (e) {
													    var data = e.params.data;
													    // console.log(window.__Program[data.BrgKode]);
													    let rowid = $(this).attr('rowid');
													    let isTC11 = $('[name=KodeTrans]').val() === 'TC11';
													    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
	                            $('[id="'+rowid+'_'+'SDHrgJual'+'"]').utilNumberControl().val(isTC11 ? parseFloat(data.BrgHrgJual) : parseFloat(data.BrgHrgBeli));
	                            $('[id="'+rowid+'_'+'SDHrgBeli'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgBeli));
	                            $('[id="'+rowid+'_'+'BrgNama'+'"]').val(data.BrgNama); // Select the option with a value of '1'
	                            $('[id="'+rowid+'_'+'BrgNama'+'"]').trigger('change');
	                            if(window.__Program[data.BrgKode] !== undefined){	                                
	                                var disc = window.__Program[data.BrgKode].Disc;
	                                // console.log(window.__Program[data.BrgKode]);
	                                // console.log(disc);
	                                // console.log(typeof(disc));
	                            	$('[id="'+rowid+'_'+'Disc'+'"]').val(disc);
	                            	$('[id="'+rowid+'_'+'Disc'+'"]').trigger('change');
	                            }
	                            checkStok($(this).val(),$('[id="'+rowid+'_'+'BrgNama'+'"]').val());
	                            window.hitungLineBarang();
													},
													'change': function(){
	                            let rowid = $(this).attr('rowid');
	                            checkStok($(this).val(),$('[id="'+rowid+'_'+'BrgNama'+'"]').val());
													}
                    }
                }
            },
            {
                name: 'BrgNama',
                label: 'Nama Barang',
                width: 190,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __BrgNama,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    // console.log(data);
						    let rowid = $(this).attr('rowid');
						    let isTC11 = $('[name=KodeTrans]').val() === 'TC11';
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'SDHrgJual'+'"]').utilNumberControl().val(isTC11 ? parseFloat(data.BrgHrgJual) : parseFloat(data.BrgHrgBeli));
                            $('[id="'+rowid+'_'+'SDHrgBeli'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgBeli));
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').val(data.BrgKode); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').trigger('change');
                            window.hitungLineBarang();
						}
                    }
                }
            },
            {
                name: 'SDQty',
                label: 'Qty.',
                editable: true,
                template: 'number',
                width: 60,
                editoptions:{
                    maxlength: 11,
                },
                editor: {
                    'onchange': function (e) {window.hitungLineBarang();}
                }
            },
            {
                name: 'BrgSatuan',
                label: 'Satuan',
                editable: true,
                width: 70,
                editor: { type: 'text', readOnly: true }
            },
            {
                name: 'BrgHrgJual',
                label: 'Harga Beli',
                editable: false,
                hidden: true,
            },
            {
                name: 'SDHrgJual',
                label: 'Harga Part',
                editable: true,
                template: 'money',
                width: 110,
                editoptions:{
                    maxlength:48
                },
                editor: {
                    events:{
                        'change': function (e) {window.hitungLineBarang();}
                    }
                }
            },
            {
                name: 'Disc',
                label: 'Disc %.',
                editable: true,
                template: 'number',
                width: 70,
                editoptions:{
                    maxlength:5
                },   
                editor: {
                    events:{
                        'change': function (e) {
                            let result = window.util.calculatePrice({
                                qty: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Barang+'_'+'SDQty'+'"]').val()),
                                price: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Barang+'_'+'SDHrgJual'+'"]').val()),
                                disc: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Barang+'_'+'Disc'+'"]').val()),
                            })
                            $('[id="'+window.cmp.rowid_Barang+'_'+'SDJualDisc'+'"]').utilNumberControl().val(result.discRp);
                            $('[id="'+window.cmp.rowid_Barang+'_'+'Jumlah'+'"]').utilNumberControl().val(result.total);
                            window.HitungTotalBarang();
                        }
                    }              
                }
            },     
            {
                name: 'SDJualDisc',
                label: 'Nilai Disc.',
                width: 100,
                editable: true,
                template: 'money',
                editoptions:{
                    maxlength:48
                },   
                editor: {
                    events:{
                        'change': function (e) {
                           let result = window.util.calculatePrice({
                                qty: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Barang+'_'+'SDQty'+'"]').val()),
                                price: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Barang+'_'+'SDHrgJual'+'"]').val()),
                                discRp: window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid_Barang+'_'+'SDJualDisc'+'"]').val()),
                            })
                            $('[id="'+window.cmp.rowid_Barang+'_'+'Disc'+'"]').utilNumberControl().val(result.disc);
                            $('[id="'+window.cmp.rowid_Barang+'_'+'Jumlah'+'"]').utilNumberControl().val(result.total);
                            window.HitungTotalBarang();
                        }
                    }              
                }
            },
            {
                name: 'Jumlah',
                label: 'Jumlah',
                width: 100,
                editable: true,
                template: 'money',
                editor: { readOnly: true }
            },
            {
                name: 'SDAuto',
                // label: 'Jumlah',
                width: 100,
                editable: true,
                // template: 'money',
                // editor: { readOnly: true }
                hidden:true
            },    
            {
                name: 'SDPickingDate',
                // label: 'Jumlah',
                width: 100,
                editable: true,
                // template: 'money',
                // editor: { readOnly: true }
                hidden:true
            },   
            {
                name: 'SDPickingNo',
                // label: 'Jumlah',
                width: 100,
                editable: true,
                // template: 'money',
                // editor: { readOnly: true }
                hidden:true
            },  
            {
                name: 'Cetak',
                // label: 'Jumlah',
                width: 100,
                editable: true,
                // template: 'money',
                // editor: { readOnly: true }
                hidden:true
            },  
            {
                name: 'SDHrgBeli',
                label: 'Harga Beli',
                editable: true,
                template: 'money',
                hidden: {$SDHargaBeliHide},
                width: 100,
                editor: { readOnly: true }
            },
            {
                name: 'KodeTransBarang',
                editable: true,
                hidden:true
            },
            {
                name: 'PrgNamaBarang',
                editable: true,
                hidden:true
            },
        ],
        listeners: {
            afterLoad: function (data) {
                let jQty = $('#detailGrid').jqGrid('getGridParam', 'records');
                let bQty = $('#scnDetailGrid').jqGrid('getGridParam', 'records');
                let param = ['Admin', 'Auditor'];
                if(!param.includes(akses)){
                    if ((jQty + bQty) > 0 && GLValid){
                        $("#KodeTrans").attr('disabled', true);
                        $("#KodeTransHidden").remove();
                        $("#form_ttsdhd_id").append("<input id='KodeTransHidden' name='KodeTrans' type='hidden' value='"+$("[id=KodeTrans]").val()+"'>");
                    }else{
                        $("#KodeTrans").attr('disabled', false);
                        $("#KodeTransHidden").remove();
                    }
                }
                window.HitungTotalBarang();
            }    
        },
        
        onSelectRow : function(rowid,status,e){
            if (window.rowId !== -1) return;
            if (window.rowId_selrow === rowid) return;
            if(rowid.includes('new_row') === true){
                $('[name=KodeBarang]').val('--');
                $('[name=LokasiKode]').val('Kosong');
                $('[name=BarangNama]').val('--');
                $('[name=lblStock]').val(0);
                return ;
            };
            let r = $('#scnDetailGrid').utilJqGrid().getData(rowid).data;            
            // check stok
            checkStok(r.BrgKode,r.BrgNama);
            window.rowId_selrow = rowid;
        }        
     }).init().fit($('.box-body'));     
     window.rowId_selrow = -1;
    dgBarang.bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
        window.cmp.rowid_Barang = rowid;    
        var BrgKode = $('[id="'+rowid+'_'+'BrgKode'+'"]');
        BrgKode.select2('destroy').empty().select2({
            data: $.map(window.__BrgKode, function (obj) {
                      obj.id = obj.BrgKode;
                      obj.text = obj.BrgKode;
                      return obj;
                    })
            });    
        var BrgNama = $('[id="'+rowid+'_'+'BrgNama'+'"]');
        BrgNama.select2('destroy').empty().select2({
            data: $.map(window.__BrgNama, function (obj) {
                      obj.id = obj.BrgNama;
                      obj.text = obj.BrgNama;
                      return obj;
                    })
            });
        if(rowid.includes("new_row")){
            $('[id="'+rowid+'_'+'SDQty'+'"]').utilNumberControl().val(1);
            $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val('');
            // $('[id="'+rowid+'_'+'Disc'+'"]').utilNumberControl().val(10.00);
            $('[id="'+rowid+'_'+'SDJualDisc'+'"]').utilNumberControl().val(0.00);
            $('[id="'+rowid+'_'+'Jumlah'+'"]').utilNumberControl().val(0.00);
            $('[id="'+rowid+'_'+'BrgKode'+'"]').trigger({
                type: 'select2:select',
                params: {
                    data: window.__BrgKode[0]
                }
            });
        }
        $('[id="'+rowid+'_'+'KodeTransBarang'+'"]').val($('#KodeTrans').val());
        $('[id="'+rowid+'_'+'PrgNamaBarang'+'"]').val($('#PrgNama').val());
    });
    
    $('#SDDiscPersen').change(function () {
        $('#SDDiscFinal').utilNumberControl().val(0);
        window.HitungBawah();
    });
    $('#SDDiscFinal').change(function () {
        $('#SDDiscPersen').utilNumberControl().val(0);
        window.HitungBawah();
    });

    window.getUrlSE = function(){
        let SENo = $('input[name="SENo"]').val();
        return __urlSE + ((SENo === '' || SENo === '--') ? '':'&check=off&cmbTxt1=SENo&txt1='+SENo);
    } 
    let fillCustomer = function(id, data) {
        __idCustomer = id;
        $('[name=MotorNoPolisi]').val(data.MotorNoPolisi);
        $('[name=CusNama]').val(data.CusNama);
        $('[name=SDPembawaMotor]').val(data.CusNama);
        $('[name=MotorWarna]').val(data.MotorWarna);
        $('[name=MotorNama]').val(data.MotorNama);
    }
    
    $('[name=MotorNoPolisi]').change(function(){
        var MotorNoPolisi = $(this).val();
        $.ajax({
            url: '$urlCustomerFindById',
            type: "GET",
            data: { id: $(this).val() },
            success: function (res) {
                if(res !== null){ 
                    fillCustomer(res.MotorNoPolisi, res); 
                }else{
                    window.openCustomer('--');
                    $("body").removeClass("loading");
                    window._ajaxCounter = 0;
                    $('[name="Tdcustomer[MotorNoPolisi]"]').val(MotorNoPolisi);
                } 
            },
            error: function (jXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            },
            async: false
        });
    });
    
    $('#BtnCariMotor').click(function() {
        window.util.app.dialogListData.show({
            title: 'Daftar Konsumen',
            url: '$urlCustomer'
        },
        function (data) {
            console.log(data);
            if(data.data === undefined){
                return ;
            }
            __idCustomer = data.id;
            let MotorNoMesin = data.data.MotorNoMesin;
            let MotorType = data.data.MotorType;
            let CusTlp = data.data.CusTelepon;
            let MotorNoPolisi = data.data.MotorNoPolisi;
            $('[name=MotorNoMesin]').val(data.data.MotorNoMesin).trigger('change');
            $('[name=MotorType]').val(data.data.MotorType).trigger('change');
            $('[name=CusTlp]').val(data.data.CusTelepon).trigger('change');            
            $('[name=MotorNoPolisi]').val(data.data.MotorNoPolisi).trigger('change');            
        })
    });
    
    $('#BtnEditMotor').click(function() {
        let MotorNoPolisi = $('input[name="MotorNoPolisi"]').val();
        window.openCustomer(MotorNoPolisi);
    });

    $('#BtnNewMotor').click(function() {
        window.openCustomer('--');
    });

    $('#btnSave').click(function (event) {        
        let PKBNo = $('[name=PKBNo]').val();
        let MotorNoPolisi = $('[name=MotorNoPolisi]').val();
        let MotorNoMesin = $('[name=MotorNoMesin]').val();
        let MotorType = $('[name=MotorType]').val();
        let CusTlp = $('[name=CusTlp]').val();
        if (PKBNo == null || PKBNo == '--' || PKBNo == '') {
            bootbox.alert({message: "PKBNo harus diisi", size: 'small'});
            return '';
        }
        else{
            $('input[name="SaveMode"]').val('ALL');
            $('#form_ttsdhd_id').attr('action','{$url['update']}');
            $('#form_ttsdhd_id').submit();
            }
    });
    
    
    function submitPrint(mode){
        // let id = (mode === 1)? '#detailGrid':'#scnDetailGrid';
        // let jQty = jQuery(id).jqGrid('getGridParam', 'records');
        let kodeTrans = $('#KodeTrans').val();
        let statusBayar = $('[name="StatusBayar"]').val();
        if($.inArray(kodeTrans, ['TC13','TC14']) == -1 && CusJenis != 'GC' && statusBayar.toLowerCase() != 'lunas'){
            bootbox.alert({message: "Inovice tidak bisa dicetak karena belum lunas.", size: 'small'});
            // return '';
        }
        // if(statusBayar.toLowerCase() == 'lunas' && __Jenis == 'SV' ){
        //     bootbox.alert({message: "Invoice tidak bisa dicetak karena sudah lunas.", size: 'small'});
        // }else{
        // if (jQty > 0){ //unnecessary qty checking
            $('#StatPrint').val(mode);               
 	        $('#form_ttsdhd_id').utilForm().submit("{$urlPrint}","POST","_blank");
 	        // }
        // }
    }
    $('#btnPrint').click(function (event) {
        if(__Jenis !== 'SD' ){
            submitPrint(1);
            // setTimeout(function() {submitPrint(2);},5000);
        }else{            
 	        $('#form_ttsdhd_id').utilForm().submit("{$urlPrint}","POST","_blank");        
        }       
    });
    
    
    function onFocus(){
         $('#form_ttsdhd_id').attr('target','');
         var iframe = window.util.app.dialogListData.iframe();
         var mainGrid = iframe.contentWindow.jQuery('#headerGridPL').jqGrid();
         mainGrid.jqGrid().trigger('reloadGrid');
         $( document ).unbind("focus");    
    }    
    
    $('#btnPrintPickingList').click(function (event) {
        window.util.app.dialogListData.show({
            title: 'Picking List',
            url: '$urlPickingList',
            button: {
                ok: {
                    label: '<i class="glyphicon glyphicon-print"></i>&nbsp&nbspPrint&nbsp&nbsp',
                    className: 'btn-warning',
                    callback: function(){ 
                        var iframe = window.util.app.dialogListData.iframe();
                        var mainGrid = iframe.contentWindow.jQuery('#headerGridPL').jqGrid();
                        window.util.app.dialogListData.gridFn.mainGrid = mainGrid;
                        window.util.app.dialogListData.gridFn.var.multiSelect = false;
                        var selRowId = window.util.app.dialogListData.gridFn.getSelectedRow();
                        // console.log(selRowId);
                        let data = selRowId.data;                        
                        // $('#form_ttsdhd_id').attr('action','$urlPrintPickingList');
                        // $('#form_ttsdhd_id').attr('target','_blank');
                        $('#SDPickingNo').val(data.SDPickingNo);     
                        // $('#form_ttsdhd_id').submit();
                        util.app.submit('$urlPrintPickingList','_blank',$('#form_ttsdhd_id').utilForm().getData());
                        $( document ).focus(function() {
                          setTimeout(onFocus, 100);
                        });
                        return false;
                    }
                },
                cancel: {
                    label: '<i class="fa fa-ban"></i>&nbsp&nbsp Batal &nbsp',
                    className: 'btn-danger',
                    callback: function (data) {
                        window.util.app.dialogListData.dialog.modal('hide');
                    }
                }
            }
        },
        function (data) {
            $( document ).unbind("focus");
        })
    });
        
    $('#BtnRiwayat').click(function() {
        window.util.app.popUpForm.show({
            title: 'Kartu Konsumen : '+$('input[name="CusNama"]').val()+' - '+$('input[name="MotorNoPolisi"]').val(),
            size: 'small',
            url: '$urlHistoryOtomatis'+'&MotorNoPolisi='+$('input[name="MotorNoPolisi"]').val(),
            width: 800,
            height: 550,
            button:{
                print:{
                    label: '<i class="glyphicon glyphicon-print"></i>&nbsp&nbsp Print &nbsp',
                    className: 'btn btn-warning',
                    callback: this.callback
                },
                cancel: {
                    label: '<i class="fa fa-ban"></i>&nbsp&nbsp Keluar &nbsp',
                    className: 'btn-danger'
                }
            }
        },
        function(data) {
            console.log(data);
        });
    });
    
    $('#BtnMekanik').click(function() {
    		window.util.app.popUpForm.callback = function(){
    			location.reload();
    		}
        window.util.app.popUpForm.show({
            title: 'Start Stop Mekanik',
            size: 'small',
            url: '$urlMekanikOtomatis',
            width: 900,
            height: 300
        },
        function(data) {
        });
    });
    
    $('#BtnStatus').click(function() {
        window.util.app.popUpForm.show({
        title: "Daftar Transaksi Keuangan yang melibatkan Nomor SV : "+$('input[name="SVNo"]').val(),
        size: 'small',
        url: '$urlStatusOtomatis',
        width: 900,
        height: 320
        },
        function(data) {
            console.log(data);
        });
    });
    
    $('#BtnFindSE').click(function (){
        window.util.app.dialogListData.show({
            title: 'Daftar Service Estimation / Estimasi Biaya Servis',
            url: getUrlSE()
        },
        function (data) {
             console.log(data);
              if (!Array.isArray(data)){
                 return;
             }
             var itemData = [];
             data.forEach(function(itm,ind) {
                let cmp = $('input[name="SENo"]');
                cmp.val(itm.header.data.SENo);
                 $('input[name="MotorNoPolisi"]').val(itm.header.data.MotorNoPolisi);
                 $('input[name="CusNama"]').val(itm.header.data.CusNama);
                itm.data.SENo = itm.header.data.SENo; 
                itm.data.SDNo = __model.SDNo; 
                itm.data.SDHrgJual = itm.data.SEHrgJual; 
                itm.data.SDQty = itm.data.SEQty; 
                itm.data.SDDiscount = itm.data.SEDiscount; 
                itemData.push(itm.data);
            });
              $.ajax({
             type: "POST",
             url: '$urlDetail',
             data: {
                 oper: 'batch',
                 data: itemData,
                 header: btoa($('#form_ttsdhd_id').serialize())
             },
             success: function(data) {
               window.location.href = "{$url['update']}&oper=skip-load&SDNoView={$dsTServis['SDNoView']}";
             },
           });
        })
    });
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });  
    
    $('#btnCancel').click(function (event) {
        $('#form_ttsdhd_id').attr('action','{$urlCancel}');
        $('#form_ttsdhd_id').submit();
	});
    
    $('#btnJurnal').click(function (event) {	
 	    var form = $(this).parents('form:first');   
 	    bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
 	    			form.utilForm().submit("{$urlJurnal}","POST","");        
                }
            }
        });
	  });    
    
    $('#btnAdd').click(function (event) {
        if( '{$Jenis}' === 'SV' && $('[name=SDStatus]').val().toLowerCase() !== 'selesai'){
             bootbox.alert({message:'Status service belum selesai.',size: 'small'});
             return; 
         }
 	    $('#form_ttsdhd_id').utilForm().submit("{$urlAdd}","POST","");
	}); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
    
    $('#btnDelete').click(function (event) {
        $('#form_ttsdhd_id').attr('action','{$urlDelete}');
        $('#form_ttsdhd_id').submit();
	  });
    
    window.BHC_BAYAR_KMNo = '{$resultKas['KMNo']}';
    window.BHC_BAYAR_BMNo = '{$resultBank['BMNo']}';
    
     $('#BtnBayar').click(function() {	
         window.BHC_BAYAR_KMNo = '{$resultKas['KMNo']}';
         window.BHC_BAYAR_BMNo = '{$resultBank['BMNo']}';
         
         if('{$dsTServis["KodeTrans"]}' == '13' || '{$dsTServis["KodeTrans"]}' == '14' || '{$dsTServis["KodeTrans"]}' == '99'){
            bootbox.alert({message:'Transaksi dengan TC {$dsTServis["KodeTrans"]} tidak perlu dilakukan pembayaran',size: 'small'});
	        return; 
         }
         if('{$Jenis}' === 'SD' ){
            bootbox.alert({message:'Transaksi dengan Jenis SD tidak dapat dilakukan pembayaran',size: 'small'});
	        return; 
         }else{
             if($('[name=SDStatus]').val().toLowerCase() !== 'selesai'){
                 bootbox.alert({message:'Status service belum selesai.',size: 'small'});
                 return; 
             }
             if($('[name=SVNo]').val() === '' || $('[name=SVNo]').val() === '--'){
                 bootbox.alert({message:'SVNo belum terbentuk.',size: 'small'});
                 return; 
             }
         }
        let SisaBayar = parseFloat($('#SisaBayar').utilNumberControl().val());
	    if (SisaBayar <= 0){
	        bootbox.alert({message:'Transaksi ini sudah terlunas',size: 'small'});
	        return;
	    }
	    let MotorNoPolisi = $('input[name="MotorNoPolisi"]').val();
	    let CusNama = $('input[name="CusNama"]').val();
	    let SVNo = $('input[name="SVNo"]').val();
	    $('#MyKodeTrans').val('SV');
	    $('#MyKode').val(MotorNoPolisi);
	    $('#MyNama').val(CusNama);
	    $('#MyNoTransaksi').val(SVNo);
	    $('#BMMemo').val("Pembayaran [SV] Invoice Servis No: "+SVNo+" - "+MotorNoPolisi+" - "+CusNama);
	    $('#BMNominal').utilNumberControl().val(SisaBayar);
	    $('#BMBayar').utilNumberControl().val(SisaBayar);
        $('#modalBayar').modal('show');
    });
    
     $('#BtnOPL').click(function() {
         window.BHC_BAYAR_KMNo = '{$resultKasOut['KKNo']}';
         window.BHC_BAYAR_BMNo = '{$resultBankOut['BKNo']}';
         
         if('{$dsTServis["KodeTrans"]}' == '13' || '{$dsTServis["KodeTrans"]}' == '14' || '{$dsTServis["KodeTrans"]}' == '99'){
            bootbox.alert({message:'Transaksi dengan TC {$dsTServis["KodeTrans"]} tidak perlu dilakukan pembayaran',size: 'small'});
	        return; 
         }
        //DISABLE ALERT 11/08/2023 (LUNAS MASIH BISA TAMBAH OPL)
        //let SisaBayar = parseFloat($('#SisaBayar').utilNumberControl().val());
	    // if (SisaBayar <= 0){
	    //    bootbox.alert({message:'Transaksi ini sudah terlunas',size: 'small'});
	    //    return;
	    //}	    
	    let MotorNoPolisi = $('input[name="MotorNoPolisi"]').val();
	    let CusNama = $('input[name="CusNama"]').val();
	    let SDNo = $('input[name="SDNo"]').val();
	    let SVNo = $('input[name="SVNo"]').val();
	    if(SDNo.includes('=') || SVNo.includes('=') ){
            bootbox.alert({message:'NoTrans Masih Menggunakan = , <br> SDNo : '+SDNo+' <br> SVNo : '+ SVNo,size: 'small'});
	        return;             
         }
	        $('#MyKodeTrans').val('PL');
            $('#MyKode').val(MotorNoPolisi);
            $('#MyNama').val(CusNama);
            // $('#MyNoTransaksi').val(SVNo);
            // $('#BMMemo').val("Pembayaran [SV] Invoice Servis No: "+SVNo+" - "+MotorNoPolisi+" - "+CusNama);
            // $('#BMNominal').utilNumberControl().val(SisaBayar);
            // $('#BMBayar').utilNumberControl().val(SisaBayar);
            $('#MyNoTransaksi').val(SDNo);
            $('#BMMemo').val("Pembayaran Pengerjaan Luar");
            $('#BMNominal').utilNumberControl().val(0);
            $('#BMBayar').utilNumberControl().val(0);
            $('#modalBayar').modal('show');
    });
	
	$('#btnSavePopup').click(function(){
	    $.ajax({
            type: 'POST',
            url: '$urlBayar',
            data: $('#form_bayar_id').utilForm().getData(true)
        }).then(function (cusData) {
            location.reload();
        });
	});
	
	$('[name=SDTgl]').utilDateControl().cmp().attr('tabindex', '2');
	$('[id=KodeTrans]').trigger('change');
	
	
	$('#btnImport').click(function (event) {
        window.util.app.dialogListData.show({
            title: 'Manage Work Order - PKB - SD',
            url: '{$urlImport}'
        },
        function (data) {
            //console.log(data);
			// if(!Array.isArray(data)) return;
			if(data.data === undefined) return;
            let iframe = window.util.app.dialogListData.iframe();
            let gridJasa = iframe.contentWindow.$('#jqGridDetail');
			let ids = gridJasa.jqGrid('getGridParam', 'selarrrow');
            var dtGridJasa = [];
			ids.forEach(elmt => {
				dtGridJasa.push(gridJasa.jqGrid('getRowData',elmt));
			});             
			let gridBrg = iframe.contentWindow.$('#scnjqGridDetail');
			ids = gridBrg.jqGrid('getGridParam', 'selarrrow');
			var dtGridBrg = [];
			ids.forEach(elmt => {
				dtGridBrg.push(gridBrg.jqGrid('getRowData',elmt));
			}); 
            $.ajax({
				type: "POST",
				url: '$urlImportLoop',
				data: {
					oper: 'import',
					data: data,
					dtGrid1: dtGridJasa,
					dtGrid2: dtGridBrg,
					header: btoa($('#form_ttsdhd_id').serialize())
				},
				success: function(data) {
                    // console.log(data);
                    var href = new URL("{$url['update']}");
                    href.searchParams.set('id', data.id);
					window.location.href = href.toString();
                },
			});
        });
    });
    $( document ).ready(function() {
        if(__model['KodeTrans'] == 'TC13' || __model['KodeTrans'] == 'TC14'){
            $('#BtnBayar').attr('disabled',true);
            $('#BtnOPL').attr('disabled',true);
        }
    });
	
JS
);
echo $this->render( '../_formBayar' );
echo $this->render( '../_formCustomer' );
