<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttsdhd */

if($Jenis === 'SD') { /* SD */
    $actionPrefix = 'daftar-servis';
    $title        = 'Surat Perintah Kerja Bengkel';
} else { /* SV */
    $actionPrefix = 'invoice-servis';
    $title        = 'Invoice Service';
}

$params                          = '&id=' . $id . '&action=create';
$cancel                          = Custom::url(\Yii::$app->controller->id.'/'.$actionPrefix.'-cancel'.$params );
$this->title                     = "Tambah - $title";
?>
<div class="ttsdhd-create">
    <?= $this->render('_form', [
        'model'     => $model,
        'dsTServis' => $dsTServis,
        'id'        => $id,
        'Jenis'     => $Jenis,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . "/$actionPrefix-update" . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttsditbarang/index','action' => 'create'] ),

        ]
    ]) ?>

</div>
