<?php
use abengkel\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
$url[ 'cancel' ] = '';
$format          = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape          = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="mekanik-form">
        <?php
        $form = ActiveForm::begin( [ 'id' => 'form_mekanik' ] );
        \aunit\components\TUi::form(
            [
                'class' => "row-no-gutters",
                'items' => [
                    [
                        'class' => "row col-md-24",
                        'style' => "margin-bottom: 6px;",
                        'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
                    ],
                ]
            ],
            [ 'class' => "row-no-gutters",
                'items' => []
            ],
            [],
        );
        ActiveForm::end();
        ?>
    </div>
<?php
$urlDetail = Url::toRoute( [ 'ttsdhd/mekanik-otomatis-item', 'SDNo' => $SDNo ] );
$this->registerJsVar( '__karKodeOri', $KarKode );
$this->registerJs( <<< JS
    __KarKode = $.map(__karKodeOri, function (obj) {
          obj.id = obj.KarKode;
          obj.text = obj.KarKode;
          return obj;
        });
     $('#detailGrid').utilJqGrid({
        editurl: '{$urlDetail}',
        height: 210,
        rownumbers: true,  
        loadonce:true,
        // navButtonTambah: false,
        rowNum: 1000,
        colModel: [     
            { template: 'actions' },
            {
                name: 'SDNo',
                label: 'SDNo',
                width: 150,
                hidden: true,
                editable:true
            },
            {
                name: 'Nomor',
                label: 'Nomor',
                width: 150,
                hidden: true,
                editable:true
            },
             {
                name: 'KarKode',
                label: 'KarKode',
                width: 250,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __KarKode,
                }
            },
            {
                name: 'Tgl',
                label: 'Tgl',
                hidden: true,
                width: 150,
                editable: true,
                editor: {
                    readOnly:true
                },
            },
            {
                name: 'Start',
                label: 'Start',
                width: 150,
                editable: true,
                editor: {
                    readOnly:true
                },
            },
            {
                name: 'Finish',
                label: 'Finish',
                width: 150,
                editable: true,
                editor: {
                    readOnly:true
                }
            },
            {
                name: 'Status',
                label: 'Status',
                width: 200,
                editable: true,
                editor: {
                    type: 'select2',
                    data: [{id:"Selesai",name:"Selesai"},{id:"Istirahat",name:"Istirahat"}],
                }
            },                     
        ], 
         onSelectRow : function(){
            let date_view = new Date().toLocaleString([['en-GB']]);
            let date = new Date().toLocaleString([['sv-SE']]);
            console.log('date->',date);
            $('[name=Tgl]').val(date);
            $('[name=Start]').val(date_view);
            $('[name=Finish]').val(date_view);
            $('[name=Nomor]').val(1);        
            
        },
        listeners: {
            afterLoad: function (data) {
             
            }    
        },  
     }).init().fit($('.box-body')).load({url: '{$urlDetail}'});        

     // $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
     //     window.cmp.rowid = rowid;
     //     let r = $('#detailGrid').utilJqGrid().getData(rowid).data;
     //    $('[id="'+rowid+'_'+'KarKode'+'"]').val(r.KarKode).trigger('change');
     //    $('[id="'+rowid+'_'+'Status'+'"]').val(r.Status).trigger('change');
     // });
JS
);