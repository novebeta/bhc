<?php
use abengkel\assets\AppAsset;
use abengkel\components\Menu;
use common\components\Custom;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
AppAsset::register( $this );
$this->title = 'Jasa';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'JasaKode'            => 'Kode',
		'JasaNama'            => 'Nama',
		'JasaGroup'           => 'Group',
		'JasaKeterangan'      => 'Keterangan',
		'JasaStatus'          => 'Status',
		'JasaWaktuSatuan'     => 'Satuan',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
		'JasaHrgBeli'                => 'Harga Beli',
		'JasaHrgJual'                => 'Harga Jual',
		'JasaWaktuKerja'             => 'Waktu Kerja',
		'JasaWaktuKerjaMenit'        => 'Waktu Kerja dlm Menit',
	],
	'sortname'  => "JasaStatus asc, JasaKode asc",
	'sortorder' => ''
];
$this->registerJsVar( 'setcmb', $arr );
$arrGroup                           = [
	'cmbTxt' => [
		'JasaGroup'           => 'Group',
		'JasaGroupNama'           => 'Group Nama',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
	'sortname'  => "JasaGroupNama",
	//	'sortorder' => 'ASC'
];
$this->registerJsVar( 'setcmb_grup', $arrGroup );
?>
    <div class="panel panel-default">
        <div class="panel-body no-padding">
            <div class="nav-tabs-custom">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#Jasa" data-toggle="tab">Jasa</a></li>
                    <li><a href="#grup" data-toggle="tab">Grup</a></li>
                </ul>
                <div class="tab-content">
                    <!-- Morris chart - Sales -->
                    <div class="chart tab-pane active" id="Jasa">
                        <div id="findModal" class="modal modal-danger fade" tabindex="-1" role="dialog">
                            <div class="modal-dialog modal-sm" role="document">
                                <div class="modal-content">
                                    <div class="form-inline" id="top-menu">
                                        <div class="input-group">
                                            <input id="txt-find-id" type="search" class="form-control" style="width: 235px"
                                                   aria-describedby="basic-addon1">
                                            <span class="input-group-btn">
                                                <button id="find-up" class="btn btn-default" type="button">
                                                    <span class="fa fa-angle-up" aria-hidden="true"></span>
                                                </button>
                                                <button id="find-down" class="btn btn-default" type="button">
                                                    <span class="fa fa-angle-down" aria-hidden="true"></span>
                                                </button>
                                                <button class="btn btn-default" type="button" data-dismiss="modal">
                                                    <span class="fa fa-close" aria-hidden="true"></span>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div>
                        <form id="filter-bottom-form">
                            <div class="form-inline" style="margin-bottom: 3px" id="top-menu">
                                <select id="filter-text-cmb1" class="form-control cmbTxt chg">
                                </select>
                                <input type="text" id="filter-text-val1" class="form-control blur" aria-label="Search">
                                <input type="text" readonly class="form-control" value="Dan" style="width: 50px">
                                <select id="filter-text-cmb2" class="form-control cmbTxt chg">
                                </select>
                                <input type="text" id="filter-text-val2" class="form-control blur" aria-label="Search">
                                <div class="material-switch form-control">
                                    <input id="checked-filter" type="checkbox" class="chg" checked/>
                                    <label for="checked-filter" class="label-success"></label>
                                    <span id="span-checked-filter" style="margin-left: 5px">Filter #2 Aktif</span>
                                </div>
                                <button id="lbl-find-id" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                            <div class="form-inline inline-filter2" style="margin-bottom: 3px">
                                <select id="filter-tgl-cmb1" class="form-control filter2 chg">
                                    <option value="">Tanggal</option>
                                </select>
								<? try {
									echo DateControl::widget( [
										'name'          => 'tgl1',
										'type'          => DateControl::FORMAT_DATE,
										'value'         => Yii::$app->formatter->asDate( '-90 day' ),
										'options'       => [
											'id'    => 'filter-tgl-val1',
											'class' => 'filter2 dt',
										],
										'pluginOptions' => [
											'autoclose' => true,
										],
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                                -
								<? try {
									echo DateControl::widget( [
										'name'          => 'tgl2',
										'type'          => DateControl::FORMAT_DATE,
										'value'         => Yii::$app->formatter->asDate( 'now' ),
										'options'       => [
											'id'    => 'filter-tgl-val2',
											'class' => 'filter2 dt',
										],
										'pluginOptions' => [
											'autoclose' => true,
										],
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                                <select id="filter-num-cmb1" class="form-control filter2 chg">
                                    <option value="" selected>Jumlah</option>
                                </select>
                                <select id="filter-num-cmb2" class="form-control filter2 chg" style="width: 60px;">
                                    <option value=">">&gt;</option>
                                    <option value="=">&gt;=</option>
                                    <option value="<">&lt;</option>
                                    <option value="<=">&lt;=</option>
                                    <option value="=">=</option>
                                    <option value="<>">&lt;&gt;</option>
                                </select>
                                <input id="filter-num-val" type="number" class="form-control blur" placeholder="Nominal" value="0">
                                <button id="btn-find-id" class="btn btn-default" type="button">
                                    <span class="fa fa-binoculars"></span>
                                </button>
                            </div>
                        </form>
                        <table id="jqGrid_jasa"></table>
                        <div id="jqGridPager_jasa"></div>
                    </div>
                    <div class="chart tab-pane" id="grup">
                        <div id="findModal_grup" class="modal modal-danger fade" tabindex="-1" role="dialog">
                            <div class="modal-dialog modal-sm" role="document">
                                <div class="modal-content">
                                    <div class="form-inline" id="top-menu_grup">
                                        <div class="input-group">
                                            <input id="txt-find-id_grup" type="search" class="form-control" style="width: 235px"
                                                   aria-describedby="basic-addon1">
                                            <span class="input-group-btn">
                                                <button id="find-up_grup" class="btn btn-default" type="button">
                                                    <span class="fa fa-angle-up" aria-hidden="true"></span>
                                                </button>
                                                <button id="find-down_grup" class="btn btn-default" type="button">
                                                    <span class="fa fa-angle-down" aria-hidden="true"></span>
                                                </button>
                                                <button class="btn btn-default" type="button" data-dismiss="modal">
                                                    <span class="fa fa-close" aria-hidden="true"></span>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div>
                        <form id="filter-bottom-form_grup">
                            <div class="form-inline" style="margin-bottom: 3px" id="top-menu_grup">
                                <select id="filter-text-cmb1_grup" class="form-control cmbTxt_grup chg_grup">
                                </select>
                                <input type="text" id="filter-text-val1_grup" class="form-control blur_grup" aria-label="Search">
                                <input type="text" readonly class="form-control" value="Dan" style="width: 50px">
                                <select id="filter-text-cmb2_grup" class="form-control cmbTxt_grup chg_grup">
                                </select>
                                <input type="text" id="filter-text-val2_grup" class="form-control blur_grup" aria-label="Search">
                                <div class="material-switch form-control">
                                    <input id="checked-filter_grup" type="checkbox" class="chg_grup" checked/>
                                    <label for="checked-filter_grup" class="label-success"></label>
                                    <span id="span-checked-filter_grup" style="margin-left: 5px">Filter #2 Aktif</span>
                                </div>
                                <button id="lbl-find-id_grup" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                            <div class="form-inline inline-filter2" style="margin-bottom: 3px">
                                <select id="filter-tgl-cmb1_grup" class="form-control filter2 chg_grup">
                                    <option value="">Tanggal</option>
                                </select>
								<? try {
									echo DateControl::widget( [
										'name'          => 'tgl1',
										'type'          => DateControl::FORMAT_DATE,
										'value'         => Yii::$app->formatter->asDate( '-90 day' ),
										'options'       => [
											'id'    => 'filter-tgl-val1_grup',
											'class' => 'filter2 dt',
										],
										'pluginOptions' => [
											'autoclose' => true,
										],
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                                -
								<? try {
									echo DateControl::widget( [
										'name'          => 'tgl2',
										'type'          => DateControl::FORMAT_DATE,
										'value'         => Yii::$app->formatter->asDate( 'now' ),
										'options'       => [
											'id'    => 'filter-tgl-val2_grup',
											'class' => 'filter2 dt',
										],
										'pluginOptions' => [
											'autoclose' => true,
										],
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                                <select id="filter-num-cmb1_grup" class="form-control filter2 chg_grup">
                                    <option value="" selected>Jumlah</option>
                                </select>
                                <select id="filter-num-cmb2_grup" class="form-control filter2 chg_grup" style="width: 60px;">
                                    <option value=">">&gt;</option>
                                    <option value="=">&gt;=</option>
                                    <option value="<">&lt;</option>
                                    <option value="<=">&lt;=</option>
                                    <option value="=">=</option>
                                    <option value="<>">&lt;&gt;</option>
                                </select>
                                <input id="filter-num-val_grup" type="number" class="form-control blur" placeholder="Nominal" value="0">
                                <button id="btn-find-id_grup" class="btn btn-default" type="button">
                                    <span class="fa fa-binoculars"></span>
                                </button>
                            </div>
                        </form>
                        <table id="jqGrid_grup"></table>
                        <div id="jqGridPager_grup"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
$menuText       = $url_update = $url_delete = $url_view = $url_print = '';
$url            = Custom::url( 'tdjasa/td' );
$url_add        = Custom::url( 'tdjasa/create&action=create' );
$url_update     = Custom::url( 'tdjasa/update&action=update' );
$url_view       = Custom::url( 'tdjasa/view&action=view' );
$url_print      = Custom::url( 'tdjasa/print' );
$url_delete     = $url;
$menuText       = 'Jasa';
$button_add_jasa     = Menu::akses( $menuText, Menu::ADD ) ? "grid.navButtonAdd('#jqGridPager_jasa',{
		   caption:' Tambah', 
		   buttonicon:'glyphicon glyphicon-plus', 
		   onClickButton: function(){ 
		      window.location = '$url_add';
		   }, 
	        position:'last'
			})" : '';
$button_update  = Menu::akses( $menuText, Menu::UPDATE ) ?
	"<a href=\"$url_update&id='+id+'&action=update\" title=\"Edit\" aria-label=\"Update\" data-pjax=\"0\"><span class=\"glyphicon glyphicon-edit\"></span></a>" : "";
$button_delete  = Menu::akses( $menuText, Menu::DELETE ) ?
	"<a href=\"$url_delete\" rowId=\"'+id+'\" title=\"Hapus\" aria-label=\"Delete\" data-pjax=\"0\" data-confirm=\"Are you sure you want to delete this item?\" data-method=\"post\"><span class=\"glyphicon glyphicon-trash\"></span></a>" : "";
$button_view    = Menu::akses( $menuText, Menu::VIEW ) ?
	"<a href=\"$url_view&id='+id+'&action=view\" title=\"View\" aria-label=\"View\" data-pjax=\"0\"><span class=\"fa fa-eye\"></span></a>" : "";
$cols           = [];
$gridColsProses = '';
if ( $button_update != '' || $button_delete != '' ) {
	$gridColsProses = " {
                 label: 'Proses', 
                 name: 'actions', 
                 sortable: false,
                 width: 75, 
                 align: 'center',
                 formatter: function(cellvalue, options, rowObject) {
                     let id = btoa(options.rowId);
                   return '$button_update $button_delete';
                 }
             },";
}
foreach ( \abengkel\models\Tdjasa::colGrid() as $filed => $label ) {
	$is_array = is_array( $label );
	$col      = [];
	if ( $is_array ) {
		$col = $label;
	} else {
		$col['name']  = $filed;
		$col['label'] = $label;
	}
	$cols[] = json_encode( $col );
}
$gridCols = implode( ', ', $cols );
$gridCols = preg_replace( '/[\'"](func\w+)[\'"]/', '$1', $gridCols );
$cols_jasa = $gridColsProses . $gridCols;
//$this->registerJsVar( 'cols', '[' . $gridColsProses . $gridCols . ']' );

$url            = Custom::url( 'tdjasagroup/td' );
$url_add        = Custom::url( 'tdjasagroup/create' );
$url_update     = Custom::url( 'tdjasagroup/update' );
$url_view       = Custom::url( 'tdjasagroup/view' );
$url_print      = Custom::url( 'tdjasagroup/print' );
$url_delete     = $url;
$button_add_grup     = Menu::akses( $menuText, Menu::ADD ) ? "grid.navButtonAdd('#jqGridPager_grup',{
		   caption:' Tambah', 
		   buttonicon:'glyphicon glyphicon-plus', 
		   onClickButton: function(){ 
		      window.location = '$url_add';
		   }, 
	        position:'last'
			})" : '';
$button_update  = Menu::akses( $menuText, Menu::UPDATE ) ?
	"<a href=\"$url_update&id='+id+'&action=update\" title=\"Edit\" aria-label=\"Update\" data-pjax=\"0\"><span class=\"glyphicon glyphicon-edit\"></span></a>" : "";
$button_delete  = Menu::akses( $menuText, Menu::DELETE ) ?
	"<a href=\"$url_delete\" rowId=\"'+id+'\" title=\"Hapus\" aria-label=\"Delete\" data-pjax=\"0\" data-confirm=\"Are you sure you want to delete this item?\" data-method=\"post\"><span class=\"glyphicon glyphicon-trash\"></span></a>" : "";
$button_view    = Menu::akses( $menuText, Menu::VIEW ) ?
	"<a href=\"$url_view&id='+id+'\" title=\"Lihat\" aria-label=\"View\" data-pjax=\"0\"><span class=\"glyphicon glyphicon-view\"></span></a>" : "";
$cols           = [];
$gridColsProses = '';
if ( $button_update != '' || $button_delete != '' ) {
	$gridColsProses = " {
                 label: 'Proses', 
                 name: 'actions', 
                 sortable: false,
                 width: 75, 
                 align: 'center',
                 formatter: function(cellvalue, options, rowObject) {
                     let id = btoa(options.rowId);
                   return '$button_update $button_delete';
                 }
             },";
}
foreach ( \abengkel\models\Tdjasagroup::colGrid() as $filed => $label ) {
	$is_array = is_array( $label );
	$col      = [];
	if ( $is_array ) {
		$col = $label;
	} else {
		$col['name']  = $filed;
		$col['label'] = $label;
	}
	$cols[] = json_encode( $col );
}
$gridCols = implode( ', ', $cols );
$gridCols = preg_replace( '/[\'"](func\w+)[\'"]/', '$1', $gridCols );
//$this->registerJsVar( 'cols_warna', '[' . $gridColsProses . $gridCols . ']' );
$cols_grup = $gridColsProses . $gridCols;
$this->registerJs( <<< JS

    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    }

    window.submitFilter = function() {
        // console.log($('#checked-filter').is(":checked"));
        var url_string = $(location).attr('href');
        var url = new URL(url_string);
        var c = decodeURIComponent(url.searchParams.get("r"));
        const search = {
            cmbTxt1: $('#filter-text-cmb1').val(),
            txt1: $('#filter-text-val1').val(),
            cmbTxt2: $('#filter-text-cmb2').val(),
            txt2: $('#filter-text-val2').val(),
            check: $('#checked-filter').is(":checked") ? 'on' : 'off',
            cmbTgl1: $('#filter-tgl-cmb1').val(),
            tgl1: $('#filter-tgl-val1').val(),
            tgl2: $('#filter-tgl-val2').val(),
            cmbNum1: $('#filter-num-cmb1').val(),
            cmbNum2: $('#filter-num-cmb2').val(),
            num1: $('#filter-num-val').val(),
            r: c
        };
        $('#jqGrid_jasa')
            .jqGrid('setGridParam', {
                sortname: setcmb.sortname,
                sortorder: setcmb.sortorder,
                postData: {
                    query: JSON.stringify(search)
                }
            }).trigger("reloadGrid");
        $('#gbox_grid .s-ico').css('display', 'none');
        $('#gbox_grid #jqgh_grid_id .s-ico').css('display', '');
        $('#gbox_grid #jqgh_grid_id .s-ico .ui-icon-triangle-1-s').removeClass('ui-state-disabled');
    }

    function findString(back) {
        var str = $('#txt-find-id').val();
        if (parseInt(navigator.appVersion) < 4) return;
        var strFound;
        if (window.find) {
            // CODE FOR BROWSERS THAT SUPPORT window.find
            strFound = self.find(str, 0, back);
            // if (!strFound) {
            //  strFound=self.find(str,0,1);
            //  while (self.find(str,0,1)) continue;
            // }
        } else if (navigator.appName.indexOf("Microsoft") != -1) {
            // EXPLORER-SPECIFIC CODE
            if (TRange != null) {
                TRange.collapse(false);
                strFound = TRange.findText(str);
                if (strFound) TRange.select();
            }
            if (TRange == null || strFound == 0) {
                TRange = self.document.body.createTextRange();
                strFound = TRange.findText(str);
                if (strFound) TRange.select();
            }
        } else if (navigator.appName == "Opera") {
            alert("Opera browsers not supported, sorry...")
            return;
        }
        if (!strFound) alert("String '" + str + "' not found!")
        return;
    }
    window.submitFilterGrup = function() {
        console.log($('#checked-filter').is(":checked"));
        var url_string = $(location).attr('href');
        var url = new URL(url_string);
        var c = decodeURIComponent(url.searchParams.get("r"));
        const search = {
            cmbTxt1: $('#filter-text-cmb1_grup').val(),
            txt1: $('#filter-text-val1_grup').val(),
            cmbTxt2: $('#filter-text-cmb2_grup').val(),
            txt2: $('#filter-text-val2_grup').val(),
            check: $('#checked-filter_grup').is(":checked") ? 'on' : 'off',
            cmbTgl1: $('#filter-tgl-cmb1_grup').val(),
            tgl1: $('#filter-tgl-val1_grup').val(),
            tgl2: $('#filter-tgl-val2_grup').val(),
            cmbNum1: $('#filter-num-cmb1_grup').val(),
            cmbNum2: $('#filter-num-cmb2_grup').val(),
            num1: $('#filter-num-val_grup').val(),
            r: c
        };
        var query = JSON.stringify(search);
        $('#jqGrid_grup')
        .jqGrid('setGridParam',{
            page: 1,
            sortname: setcmb_grup.sortname,
            sortorder: setcmb_grup.sortorder,
            postData: {
                query: query
            }
        }).trigger("reloadGrid");
        $('#gbox_grid .s-ico').css('display', 'none');
        $('#gbox_grid #jqgh_grid_id .s-ico').css('display', '');
        $('#gbox_grid #jqgh_grid_id .s-ico .ui-icon-triangle-1-s').removeClass('ui-state-disabled');
    }

    function findStringgrup(back) {
        var str = $('#txt-find-id_grup').val();
        if (parseInt(navigator.appVersion) < 4) return;
        var strFound;
        if (window.find) {
            // CODE FOR BROWSERS THAT SUPPORT window.find
            strFound = self.find(str, 0, back);
            // if (!strFound) {
            //  strFound=self.find(str,0,1);
            //  while (self.find(str,0,1)) continue;
            // }
        } else if (navigator.appName.indexOf("Microsoft") != -1) {
            // EXPLORER-SPECIFIC CODE
            if (TRange != null) {
                TRange.collapse(false);
                strFound = TRange.findText(str);
                if (strFound) TRange.select();
            }
            if (TRange == null || strFound == 0) {
                TRange = self.document.body.createTextRange();
                strFound = TRange.findText(str);
                if (strFound) TRange.select();
            }
        } else if (navigator.appName == "Opera") {
            alert("Opera browsers not supported, sorry...")
            return;
        }
        if (!strFound) alert("String '" + str + "' not found!")
        return;
    }

    var ajaxRowOpt = {
        success: function (response, status, error) {
            $("#jqGrid_jasa").trigger("reloadGrid");
            $.notify(
                {
                    message: response.message
                },
                {
                    type: response.success ? 'success' : 'warning',
                    mouse_over: 'pause',
                    newest_on_top: true,
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    }
                }
            );
        },
        error: function (response, status, error) {
            $.notify(
                {
                    message: error + '<br> ' + response.responseJSON.message
                },
                {
                    type: 'danger',
                    mouse_over: 'pause',
                    newest_on_top: true,
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    }
                }
            );
        }
    };
    $.jgrid.defaults.styleUI = 'Bootstrap';
    $.jgrid.defaults.pgtext = "{0} of {1}";
    var url_string = $(location).attr('href');
    var url = new URL(url_string);
    var c = decodeURIComponent(url.searchParams.get("r"));
    var pageKey = c;
    var grid = $("#jqGrid_jasa");
    grid.jqGrid({
        url: 'index?r=tdjasa/td',
        mtype: "POST",
        datatype: "json",
        jsonReader: {
            root: 'rows',
            page: 'currentPage',
            total: 'totalPages',
            records: 'rowsCount',
            id: 'rowId',
            repeatitems: false
        },
        rownumbers: true,
        colModel: [
            $cols_jasa
        ],
        multiSort: true,
        sortname: setcmb.sortname,
        sortorder: setcmb.sortorder,
        search: true,
        postData: {
            query: function () {
                const search = {
                    cmbTxt1: '',
                    txt1: '',
                    cmbTxt2: '',
                    txt2: '',
                    check: 'on',
                    cmbTgl1: $('#filter-tgl-cmb1').val(),
                    tgl1: $('#filter-tgl-val1').val(),
                    tgl2: $('#filter-tgl-val2').val(),
                    cmbNum1: '',
                    cmbNum2: '',
                    num1: '',
                    r: c
                };
                return JSON.stringify(search);
            }
        },
        altclass: 'row-zebra',
        altRows: true,
        pager: "#jqGridPager_jasa",
        viewrecords: true,
        rowNum: 15,
        rowList: [15, 30, 45, 60, 75],
        width: 700,
        height: 320,
        autowidth: false,
        shrinkToFit: false,
        styleUI: 'Bootstrap',
        // toppager:true,
        ajaxRowOptions: ajaxRowOpt,
        page: (document.cookie.match(new RegExp("(?:.*;)?\s*" + pageKey + "\s*=\s*([^;]+)(?:.*)?$")) || [, 1])[1],
        gridComplete: function () {
            $('a[data-confirm]').on('click', function (ev) {
                var href = $(this).attr('href');
                var rowId = $(this).attr('rowId');
                var dataConfirmModal = $('#dataConfirmModal');
                if (!dataConfirmModal.length) {
                    $('body').append('<div id="dataConfirmModal" class="modal bootstrap-dialog type-warning fade size-normal in" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true">' +
                        '<div class="modal-dialog"><div class="modal-content">' +
                        '<div class="modal-header"><div class="bootstrap-dialog-header"><div class="bootstrap-dialog-close-button" style="display: none;"><button class="close" data-dismiss="modal" aria-label="close">×</button></div><div class="bootstrap-dialog-title" id="w3_title">Confirmation</div></div></div>' +
                        '<form id="delete-form" method="post"><div class="modal-body"><div class="bootstrap-dialog-body"><div class="bootstrap-dialog-message">Are you sure you want to delete this item?</div></div></div>' +
                        '<div class="modal-footer"><div class="bootstrap-dialog-footer"><div class="bootstrap-dialog-footer-buttons"><button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><span class="fa fa-ban"></span>  Cancel</button><button class="btn btn-warning" type="submit" ><span class="glyphicon glyphicon-ok"></span> Ok</button></div></div></div></form></div></div></div>');
                }
                dataConfirmModal.find('.modal-body').text($(this).attr('data-confirm'));
                // $('#dataConfirmOK').attr('href', href);
                dataConfirmModal.modal({show: true});
                $('#delete-form').on('submit', function (e) {
                    e.preventDefault();
                    // $.notifyClose();
                    $.ajax({
                        url: href, //this is the submit URL
                        type: 'POST', //or POST
                        data: {
                            'oper': 'del',
                            'id': rowId
                        },
                        success: function (data) {
                            $.notify({
                                    message: data.message
                                },
                                {
                                    type: data.success ? 'success' : 'warning',
                                    allow_duplicates: false,
                                    mouse_over: 'pause',
                                    newest_on_top: true,
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    animate: {
                                        enter: 'animated fadeInDown',
                                        exit: 'animated fadeOutUp'
                                    }
                                });
                            $('#jqGrid_jasa').trigger('reloadGrid');
                        }
                    });
                    $('#dataConfirmModal').modal('hide');
                });
                return false;
            });
        }
    }).navGrid('#jqGridPager_jasa', {
        edit: false, add: false, del: false, search: false, refresh: false
    }).bindKeys();
    grid.jqGrid("setLabel", "rn", "No", {'text-align': 'center'});
    grid.jqGrid("setLabel", "actions", "Proses", {'text-align': 'center'});
    // grid grup
    var grid = $("#jqGrid_grup");
    grid.jqGrid({
        url: 'index?r=tdjasagroup/td',
        mtype: "POST",
        datatype: "json",
        jsonReader: {
            root: 'rows',
            page: 'currentPage',
            total: 'totalPages',
            records: 'rowsCount',
            id: 'rowId',
            repeatitems: false
        },
        rownumbers: true,
        colModel: [
            $cols_grup
        ],
        multiSort: true,
        sortname: setcmb_grup.sortname,
        sortorder: setcmb_grup.sortorder,
        search: true,
        postData: {
            query: function () {
                const search = {
                    cmbTxt1: '',
                    txt1: '',
                    cmbTxt2: '',
                    txt2: '',
                    check: 'on',
                    cmbTgl1: $('#filter-tgl-cmb1_grup').val(),
                    tgl1: $('#filter-tgl-val1_grup').val(),
                    tgl2: $('#filter-tgl-val2_grup').val(),
                    cmbNum1: '',
                    cmbNum2: '',
                    num1: '',
                    r: c
                };
                return JSON.stringify(search);
            }
        },
        altclass: 'row-zebra',
        altRows: true,
        pager: "#jqGridPager_grup",
        viewrecords: true,
        rowNum: 15,
        rowList: [15, 30, 45, 60, 75],
        width: 700,
        height: 320,
        autowidth: false,
        shrinkToFit: false,
        styleUI: 'Bootstrap',
        // toppager:true,
        ajaxRowOptions: ajaxRowOpt,
        page: (document.cookie.match(new RegExp("(?:.*;)?\s*" + pageKey + "\s*=\s*([^;]+)(?:.*)?$")) || [, 1])[1],
        gridComplete: function () {
            $('a[data-confirm]').on('click', function (ev) {
                var href = $(this).attr('href');
                var rowId = $(this).attr('rowId');
                var dataConfirmModal = $('#dataConfirmModal');
                if (!dataConfirmModal.length) {
                    $('body').append('<div id="dataConfirmModal" class="modal bootstrap-dialog type-warning fade size-normal in" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true">' +
                        '<div class="modal-dialog"><div class="modal-content">' +
                        '<div class="modal-header"><div class="bootstrap-dialog-header"><div class="bootstrap-dialog-close-button" style="display: none;"><button class="close" data-dismiss="modal" aria-label="close">×</button></div><div class="bootstrap-dialog-title" id="w3_title">Confirmation</div></div></div>' +
                        '<form id="delete-form" method="post"><div class="modal-body"><div class="bootstrap-dialog-body"><div class="bootstrap-dialog-message">Are you sure you want to delete this item?</div></div></div>' +
                        '<div class="modal-footer"><div class="bootstrap-dialog-footer"><div class="bootstrap-dialog-footer-buttons"><button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><span class="fa fa-ban"></span>  Cancel</button><button class="btn btn-warning" type="submit" ><span class="glyphicon glyphicon-ok"></span> Ok</button></div></div></div></form></div></div></div>');
                }
                dataConfirmModal.find('.modal-body').text($(this).attr('data-confirm'));
                // $('#dataConfirmOK').attr('href', href);
                dataConfirmModal.modal({show: true});
                $('#delete-form').on('submit', function (e) {
                    e.preventDefault();
                    // $.notifyClose();
                    $.ajax({
                        url: href, //this is the submit URL
                        type: 'POST', //or POST
                        data: {
                            'oper': 'del',
                            'id': rowId
                        },
                        success: function (data) {
                            $.notify({
                                    message: data.message
                                },
                                {
                                    type: data.success ? 'success' : 'warning',
                                    allow_duplicates: false,
                                    mouse_over: 'pause',
                                    newest_on_top: true,
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    animate: {
                                        enter: 'animated fadeInDown',
                                        exit: 'animated fadeOutUp'
                                    }
                                });
                            $('#jqGrid_jasa').trigger('reloadGrid');
                        }
                    });
                    $('#dataConfirmModal').modal('hide');
                });
                return false;
            });
        }
    }).navGrid('#jqGridPager_grup', {
        edit: false, add: false, del: false, search: false, refresh: false
    }).bindKeys();
    grid.jqGrid("setLabel", "rn", "No", {'text-align': 'center'});
    grid.jqGrid("setLabel", "actions", "Proses", {'text-align': 'center'});

    $button_add_jasa

    $button_add_grup

    function resizeGrid() {
        $('#jqGrid_jasa')
            .setGridWidth($('.panel-body').width()-23, false);
        $('#jqGrid_grup')
            .setGridWidth($('.panel-body').width()-23, false);
    }

    resizeGrid();
    $('.panel-body').resize(resizeGrid);
    var tgl1 = $('#filter-tgl-val1');
    var tgl2 = $('#filter-tgl-val2');
    tgl1.change(function () {
        $('#filter-bottom-form').submit();
    })
    tgl2.change(function () {
        $('#filter-bottom-form').submit();
    })
    $("#checked-filter").change(function () {
        if (this.checked) {
            $("#span-checked-filter").text("Filter #2 Aktif");
            $(".inline-filter2").show();
        } else {
            $("#span-checked-filter").text("Filter #2 Non Aktif");
            $(".inline-filter2").hide();
        }
        // $(".filter2").prop('disabled', !this.checked);
    });
    var option = $('.cmbTxt');
    option.find('option').remove().end();
    for (let val in setcmb['cmbTxt']) {
        option.append('<option value="' + val + '">' + setcmb['cmbTxt'][val] + '</option>');
    }
    $('#filter-text-cmb2').prop("selectedIndex", 1);
    if (Object.keys(setcmb['cmbTgl']).length > 0) {
        var option = $('#filter-tgl-cmb1');
        option.find('option').remove().end();
        for (let val in setcmb['cmbTgl']) {
            option.append('<option value="' + val + '">' + setcmb['cmbTgl'][val] + '</option>');
        }
    }
    if (Object.keys(setcmb['cmbNum']).length > 0) {
        var option = $('#filter-num-cmb1');
        option.find('option').remove().end();
        for (let val in setcmb['cmbNum']) {
            option.append('<option value="' + val + '">' + setcmb['cmbNum'][val] + '</option>');
        }
    }

    $('#filter-bottom-form').on('submit', function (e) {
        e.preventDefault();
        submitFilter();
    });
    $('#btn-find-id').on('click', function (e) {
        e.preventDefault();
        $('#findModal').modal();
    });
    $('#find-up').on('click', function (e) {
        e.preventDefault();
        findString(1);
    });
    $('#find-down').on('click', function (e) {
        e.preventDefault();
        findString(0);
    });
    $('.blur').on('blur', function (e) {
        submitFilter();
    });
    $('.chg').on('change', function (e) {
        submitFilter();
        // console.log(e);
        // setTimeout(, 500);
    });
    $('.blur').keydown(function (e) {
        if (e.keyCode == 13) {
            submitFilter();
        }
    });
    var tgl1 = $('#filter-tgl-val1_grup');
    var tgl2 = $('#filter-tgl-val2_grup');
    tgl1.change(function () {
        $('#filter-bottom-form_grup').submit();
    });
    tgl2.change(function () {
        $('#filter-bottom-form_grup').submit();
    });
    $("#checked-filter_grup").change(function () {
        if (this.checked) {
            $("#span-checked-filter_grup").text("Filter #2 Aktif");
            $(".inline-filter2").show();
        } else {
            $("#span-checked-filter_grup").text("Filter #2 Non Aktif");
            $(".inline-filter2").hide();
        }
        // $(".filter2").prop('disabled', !this.checked);
    });
    var option = $('.cmbTxt_grup');
    option.find('option').remove().end();
    for (let val in setcmb_grup['cmbTxt']) {
        option.append('<option value="' + val + '">' + setcmb_grup['cmbTxt'][val] + '</option>');
    }
    $('#filter-text-cmb2_grup').prop("selectedIndex", 1);
    if (Object.keys(setcmb_grup['cmbTgl']).length > 0) {
        var option = $('#filter-tgl-cmb1_grup');
        option.find('option').remove().end();
        for (let val in setcmb_grup['cmbTgl']) {
            option.append('<option value="' + val + '">' + setcmb_grup['cmbTgl'][val] + '</option>');
        }
    }
    if (Object.keys(setcmb_grup['cmbNum']).length > 0) {
        var option = $('#filter-num-cmb1');
        option.find('option').remove().end();
        for (let val in setcmb_grup['cmbNum']) {
            option.append('<option value="' + val + '">' + setcmb_grup['cmbNum'][val] + '</option>');
        }
    }

    $('#filter-bottom-form_grup').on('submit', function (e) {
        e.preventDefault();
        submitFilterGrup();
    });
    $('#btn-find-id_grup').on('click', function (e) {
        e.preventDefault();
        $('#findModal_grup').modal();
    });
    $('#find-up_grup').on('click', function (e) {
        e.preventDefault();
        findStringgrup(1);
    });
    $('#find-down_grup').on('click', function (e) {
        e.preventDefault();
        findStringgrup(0);
    });
    $('.blur_grup').on('blur', function (e) {
        submitFilterGrup();
    });
    $('.chg_grup').on('change', function (e) {
        setTimeout(submitFilterGrup(), 500);
    });
    $('.blur_grup').keydown(function (e) {
        if (e.keyCode == 13) {
            submitFilterGrup();
        }
    });

JS);