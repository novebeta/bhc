<?php
use yii\helpers\Url;
use common\components\Custom;
$this->title                   = 'Tambah Jasa';
$this->params['breadcrumbs'][] = ['label' => 'Jasa', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdjasa-create">
	<?= $this->render( '_form', [
		'model' => $model,
		'id'    => $id,
        'url' => [
            'update'    => Custom::url(\Yii::$app->controller->id.'/create' ),
            'cancel'    => Custom::url(\Yii::$app->controller->id.'/' ),
        ]
	] ) ?>
</div>

