<?php
use abengkel\components\FormField;
use abengkel\models\Tdjasagroup;
use kartik\number\NumberControl;
use common\components\Custom;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdkaryawan */
/* @var $form yii\widgets\ActiveForm */
/** @var \abengkel\models\Tdjasa $model */
$url['cancel'] = Url::toRoute('tdjasa/index');
$format = <<< SCRIPT
function formattdlokasi(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-6" style="text-align: left">' + result.id + '</div>' +
           '<div class="col-xs-18">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
?>

<div class="tdjasa-form">
    <?php 
    $form = ActiveForm::begin(['id' => 'frm_tdjasa_id' ,'enableClientValidation'=>false]);
	\abengkel\components\TUi::form(
        [
            'class' => "row-no-gutters",
            'items' => [
                ['class' => "form-group col-md-24",
                    'items' => [
						['class' => "col-sm-2",'items' => [['items' => ":LabelJasaKode"],]],
						['class' => "col-sm-5",'items' => [['items' => ":JasaKode"],]],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => [['items' => ":LabelZjasaId"],]],
                        ['class' => "col-sm-6",'items' => [['items' => ":ZjasaId"],]],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-3",'items' => [['items' => ":LabelZnamaPekerjaan"],]],
                        ['class' => "col-sm-4",'items' => [['items' => ":ZnamaPekerjaan"],]],
                     ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
						['class' => "col-sm-2",'items' => [['items' => ":LabelJasaNama"],]],
						['class' => "col-sm-14",'items' => [['items' => ":JasaNama"],]],
						['class' => "col-sm-1"],
						['class' => "col-sm-3",'items' => [['items' => ":LabelHrgBeli"],]],
						['class' => "col-sm-4",'items' => [['items' => ":JasaHrgBeli"],]],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
						['class' => "col-sm-2",'items' => [['items' => ":LabelGroup"],]],
						['class' => "col-sm-14",'items' => [['items' => ":JasaGroup"],]],
						['class' => "col-sm-1"],
						['class' => "col-sm-3",'items' => [['items' => ":LabelHrgJual"],]],
						['class' => "col-sm-4",'items' => [['items' => ":JasaHrgJual"],]],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
						['class' => "col-sm-2",'items' => [['items' => ":LabelWaktuKerja"],]],
						['class' => "col-sm-3",'items' => [['items' => ":JasaWaktuKerja"],]],
						['class' => "col-sm-1"],
						['class' => "col-sm-3",'items' => [['items' => ":JasaWaktuSatuan"],]],
						['class' => "col-sm-1"],
						['class' => "col-sm-1",'items' => [['items' => ":Labelsmdgn"],]],
						['class' => "col-sm-3",'items' => [['items' => ":JasaWaktuKerjaMenit"],]],
						['class' => "col-sm-1"],
						['class' => "col-sm-1",'items' => [['items' => ":LabelMenit"],]],
						['class' => "col-sm-1"],
						['class' => "col-sm-3",'items' => [['items' => ":LabelStatus"],]],
						['class' => "col-sm-4",'items' => [['items' => ":JasaStatus"],]],
                    ],
                ],
            ]
        ],
        [
            'class' => "pull-right",
            'items' => ":btnAction"
        ],
        [
			":LabelZjasaId"       => '<div class="form-group"> <label class="control-label" style="margin: 0; padding: 6px 0;">Jasa ID</label></div>',
			":LabelZnamaPekerjaan"       => '<div class="form-group"> <label class="control-label" style="margin: 0; padding: 6px 0;">Nama Pekerjaan</label></div>',
			":LabelJasaKode"       => '<div class="form-group"> <label class="control-label" style="margin: 0; padding: 6px 0;">Kode Jasa</label></div>',
			":LabelJasaNama"       => '<div class="form-group"> <label class="control-label" style="margin: 0; padding: 6px 0;">Nama Jasa</label></div>',
			":LabelHrgBeli"        => '<div class="form-group"> <label class="control-label" style="margin: 0; padding: 6px 0;">Harga Beli</label></div>',
			":LabelGroup"          => '<div class="form-group"> <label class="control-label" style="margin: 0; padding: 6px 0;">Group</label></div>',
			":LabelHrgJual"        => '<div class="form-group"> <label class="control-label" style="margin: 0; padding: 6px 0;">Harga Jual</label></div>',
			":LabelWaktuKerja"     => '<div class="form-group"> <label class="control-label" style="margin: 0; padding: 6px 0;">Waktu Kerja</label></div>',
			":Labelsmdgn"          => '<div class="form-group"> <label class="control-label" style="margin: 0; padding: 6px 0;">  =</label></div>',
			":LabelMenit"          => '<div class="form-group"> <label class="control-label" style="margin: 0; padding: 6px 0;">Menit</label></div>',
			":LabelStatus"         => '<div class="form-group"> <label class="control-label" style="margin: 0; padding: 6px 0;">Status</label></div>',

			":JasaKode"             => $form->field($model, 'JasaKode', ['template' => '{input}'])->textInput(['maxlength' => '10','autofocus' => 'autofocus', 'onkeyup'=>'this.value = this.value.toUpperCase();','style' => 'text-transform: uppercase']),
			":JasaNama"             => $form->field($model, 'JasaNama', ['template' => '{input}'])->textInput(['maxlength' => '50', 'onkeyup'=>'this.value = this.value.toUpperCase();','style' => 'text-transform: uppercase' ]),
			":JasaHrgBeli"          => $form->field($model, 'JasaHrgBeli', ['template' => '{input}'])->widget(NumberControl::classname(), ['maskedInputOptions' => ['allowMinus' => false], 'displayOptions' => ['class' => 'form-control', 'placeholder' => '0',]]),
			":JasaHrgJual"          => $form->field($model, 'JasaHrgJual', ['template' => '{input}'])->widget(NumberControl::classname(), ['maskedInputOptions' => ['allowMinus' => false], 'displayOptions' => ['class' => 'form-control', 'placeholder' => '0',]]),
			":JasaWaktuSatuan"      => $form->field($model, 'JasaWaktuSatuan', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
			":JasaWaktuKerjaMenit"  => $form->field($model, 'JasaWaktuKerjaMenit', ['template' => '{input}'])->widget(NumberControl::classname(), ['maskedInputOptions' => ['allowMinus' => false], 'displayOptions' => ['class' => 'form-control', 'placeholder' => '0',]]),
			":JasaWaktuKerja"       => $form->field($model, 'JasaWaktuKerja', ['template' => '{input}'])->widget(NumberControl::classname(), ['maskedInputOptions' => ['allowMinus' => false], 'displayOptions' => ['class' => 'form-control', 'placeholder' => '0',]]),
			":JasaStatus"           => $form->field($model, 'JasaStatus', ['template' => '{input}'])->dropDownList(['A'         => 'AKTIF', 'N'         => 'NON AKTIF',], ['maxlength' => true]),

            ":ZjasaId"             => $form->field($model, 'ZjasaId', ['template' => '{input}'])->textInput(['autofocus' => 'autofocus', ]),
            ":ZnamaPekerjaan"             => $form->field($model, 'ZnamaPekerjaan', ['template' => '{input}'])->textInput([]),

            ":JasaGroup"            => $form->field($model, 'JasaGroup', ['template' => '{input}'])
                                        ->widget(Select2::classname(), [
	                                        'value' => $model->JasaGroup,
	                                        'theme' => Select2::THEME_DEFAULT,
	                                        'data' => Tdjasagroup::find()->Group(),
	                                        'pluginOptions' => [
                                                'dropdownAutoWidth' => true,
                                                'templateResult' => new JsExpression('formattdlokasi'),
                                                'templateSelection' => new JsExpression('formattdlokasi'),
                                                'matcher' => new JsExpression('matchCustom'),
                                                'escapeMarkup' => $escape]]),
        ],
        [
			'url_main' => 'tdjasa',
            'url_id'   => $id,
			'url_update' => $url[ 'update' ]
		]
    );
    ActiveForm::end();
    ?>
</div>
<?php
$this->registerJs( <<< JS
     $('#btnSave').click(function (event) {
       $('#frm_tdjasa_id').submit();
    }); 


JS);
