<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdjasa */
$this->title                   = 'Edit Jasa: ' . $model->JasaKode;
$this->params['breadcrumbs'][] = [ 'label' => 'Jasa', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="tdjasa-update">
	<?= $this->render( '_form', [
		'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdjasa/update', 'id' => $id,'action' => 'update' ] ),
            'cancel' => Url::toRoute( [ 'tdjasa/cancel', 'id' => $id,'action' => 'update' ] )
        ]
	] ) ?>
</div>
