<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdlokasi */

$this->title = 'Edit Lokasi Gudang : ' . $model->LokasiKode;
$this->params['breadcrumbs'][] = ['label' => 'Lokasi Gudang', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="tdlokasi-update">
    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdlokasi/update', 'id' => $id ,'action' => 'update'] ),
            'cancel' => Url::toRoute( [ 'tdlokasi/cancel', 'id' => $id,'action' => 'update' ] )
        ]
    ]) ?>

</div>
