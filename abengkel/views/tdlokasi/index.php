<?php
use abengkel\assets\AppAsset;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gudang / Lokasi Penyimpanan';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'LokasiKode'    => 'Kode',
		'LokasiNama'    => 'Nama',
		'KarKode'       => 'Karyawan',
		'LokasiAlamat'  => 'Alamat',
		'LokasiStatus'  => 'Status',
		'LokasiTelepon' => 'Telepon',
		'LokasiNomor'   => 'Nomor',
		'LokasiJenis'   => 'Jenis',
		'LokasiInduk'   => 'Induk',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
	'sortname'  => "LokasiStatus, LokasiNomor, LokasiKode",
	'sortorder' => ''
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Tdlokasi::className(),'Gudang' ), \yii\web\View::POS_READY );
