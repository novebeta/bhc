<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdlokasi */

$this->title = 'Tambah Lokasi Gudang';
$this->params['breadcrumbs'][] = ['label' => 'Lokasi Gudang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdlokasi-create">
    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'    => Url::toRoute( [ 'tdlokasi/update', 'id' => $id ,'action' => 'create'] ),
            'cancel'    => Url::toRoute( [ 'tdlokasi/cancel', 'id' => $id,'action' => 'create' ] )
        ]
    ]) ?>

</div>
