<?php
use abengkel\components\FormField;
use abengkel\models\Tdkaryawan;
use kartik\number\NumberControl;
use common\components\Custom;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Tdlokasi */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function formattdlokasi(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-6" style="text-align: left">' + result.id + '</div>' +
           '<div class="col-xs-18">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
?>

<div class="tdlokasi-form">
    <?php
    $form = ActiveForm::begin(['id' => 'frm_tdlokasi_id'  ,'enableClientValidation'=>false]);
    \abengkel\components\TUi::form(
        ['class' => "row-no-gutters",
            'items' => [
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelLokasiKode"],
                        ['class' => "col-sm-4",'items' => ":LokasiKode"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelLokasiNomor"],
                        ['class' => "col-sm-2",'items' => ":LokasiNomor"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelLokasiJenis"],
                        ['class' => "col-sm-3",'items' => ":LokasiJenis"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelLokasiInduk"],
                        ['class' => "col-sm-4",'items' => ":LokasiInduk"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelLokasiNama"],
                        ['class' => "col-sm-9",'items' => ":LokasiNama"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelKarKode"],
                        ['class' => "col-sm-10",'items' => ":KarKode"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelLokasiAlamat"],
                        ['class' => "col-sm-9",'items' => ":LokasiAlamat"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelLokasiTelepon"],
                        ['class' => "col-sm-3",'items' => ":LokasiTelepon"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelLokasiStatus"],
                        ['class' => "col-sm-4",'items' => ":LokasiStatus"],
                    ],
                ],

            ]
        ],
        [
            'class' => "pull-right",
            'items' => ":btnAction"
        ],
        [
            ":LabelLokasiKode"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode</label>',
            ":LabelLokasiNama"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama</label>',
            ":LabelKarKode"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Petugas</label>',
            ":LabelLokasiAlamat"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
            ":LabelLokasiStatus"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',
            ":LabelLokasiTelepon" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon</label>',
            ":LabelLokasiNomor"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor</label>',
            ":LabelLokasiJenis"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
            ":LabelLokasiInduk"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Induk</label>',

            ":LokasiKode"         => $form->field($model, 'LokasiKode',['template' => '{input}'])->textInput(['autofocus' => 'autofocus']),
            ":LokasiNama"         => $form->field($model, 'LokasiNama',['template' => '{input}'])->textInput(['maxlength' => '15',]),
            ":LokasiAlamat"       => $form->field($model, 'LokasiAlamat',['template' => '{input}'])->textInput(['maxlength' => '75',]),
            ":LokasiStatus"       => FormField::combo( 'LokasiStatus',[  'extraOptions' => ['simpleCombo' => true]] ),
            ":LokasiTelepon"      => $form->field($model, 'LokasiTelepon',['template' => '{input}'])->textInput(['maxlength' => '30',]),
            ":LokasiNomor"        => $form->field($model, 'LokasiNomor',['template' => '{input}'])->textInput(['maxlength' => '3',]),
            ":LokasiJenis"        => FormField::combo( 'LokasiJenis',[  'extraOptions' => ['simpleCombo' => true]] ),
            ":LokasiInduk"        => FormField::combo( 'LokasiInduk',[  'extraOptions' => ['simpleCombo' => true]] ),
            ":KarKode"            => $form->field($model, 'KarKode', ['template' => '{input}'])
                                    ->widget(Select2::classname(), [
                                        'value' => $model->KarKode,
                                        'theme' => Select2::THEME_DEFAULT,
                                        'data' => Tdkaryawan::find()->KarKode(),
                                        'pluginOptions' => [
                                            'dropdownAutoWidth' => true,
                                            'templateResult' => new JsExpression('formattdlokasi'),
                                            'templateSelection' => new JsExpression('formattdlokasi'),
                                            'matcher' => new JsExpression('matchCustom'),
                                            'escapeMarkup' => $escape]]),
        ],
	    [
		    'url_main'   => 'tdlokasi',
		    'url_id'     => $id,
		    'url_update' => $url[ 'update' ]
	    ]
    );
    ActiveForm::end();
    ?>
</div>
<?php
$urlIndex   = Url::toRoute( [ 'tdlokasi/index' ] );
$urlAdd     = Url::toRoute( [ 'tdlokasi/create','id'=>'new' ] );
$this->registerJs( <<< JS
     $('#btnSave').click(function (event) {
       $('#frm_tdlokasi_id').submit();
    }); 
    $('#btnCancel').click(function (event) {	  
         window.location.href = '{$url['cancel']}';
    });
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
    $('#btnAdd').click(function (event) {	      
        window.location.href = '{$urlAdd}';
	  }); 
    
    
    
JS);


