<?php
/* @var $this yii\web\View */

/* @var $dataProvider yii\data\ActiveDataProvider */

use abengkel\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
$this->title = 'Manage Parts Inbound';
$this->params['breadcrumbs'][] = $this->title;
$arr = [
    'cmbTxt' => [
        'noPO' => 'No PO',
    ],
    'cmbTxt2' => [
        'noPenerimaan' => 'No Penerimaan',
        'tglPenerimaan' => 'Tgl Penerimaan',
        'noShippingList' => 'No Shipping List',
    ],
    'cmbTgl' => [],
    'cmbNum' => [],
    'sortname' => "noPO",
    'sortorder' => 'desc'
];
$this->registerJsVar('setcmb', $arr);
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render('../_search'); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs(<<< JS
    window.gridParam_jqGrid = {
        readOnly: true,
        rownumbers: true,
        rowNum: 10000,
        pgbuttons: false,
        pginput: false,
        viewrecords: false,
        rowList: [],
        pgtext: ""
    }
    // $('#filter-tgl-val1').change(function() {
    //     let _tgl1 = new Date($(this).val());      
    //     _tgl1.setDate(_tgl1.getDate() + 5);
	//     $('#filter-tgl-val2-disp').datepicker('update', _tgl1);
	//     $('#filter-tgl-val2-disp').trigger('change');
    // });
JS
);

$this->registerJs(\abengkel\components\TdUi::mainGridJs(\abengkel\models\Ttpshd::class,
    'Import Parts Inbound', [
        'colGrid' => $colGrid,
        'url' => Url::toRoute(['ttpshd/import-items']),
        'mode' => isset($mode) ? $mode : '',
    ]), \yii\web\View::POS_READY);
$urlDetail = Url::toRoute(['ttpshd/import-details']);
$this->registerJs(<<< JS
jQuery(function ($) {
    if($('#jqGridDetail').length) {
        $('#jqGridDetail').utilJqGrid({
            url: '$urlDetail',
            height: 240,
            readOnly: true,
            rownumbers: true,
            rowNum: 1000,
            colModel: [
               	{
	                name: 'noPO',
	                label: 'No PO',
	                width: 150,
	            },
                {
                    name: 'jenisOrder',
                    label: 'Jenis Order',
                    width: 90
                },
	            {
	                name: 'idWarehouse',
	                label: 'Id Warehouse',
	                width: 90,
	            },
	            {
	                name: 'partsNumber',
	                label: 'Parts Number',
	                width: 100,
	            },
	            {
	                name: 'kuantitas',
	                label: 'Kuantitas',
	                width: 70,
	                align: 'right'
	            },
	            {
	                name: 'uom',
	                label: 'Uom',
	                width: 50,
	            },
	            {
	                name: 'createdTime',
	                label: 'Created Time',
	                width: 130,
	            },
            ],
	        multiselect: gridFn.detail.multiSelect(),
	        onSelectRow: gridFn.detail.onSelectRow,
	        onSelectAll: gridFn.detail.onSelectAll,
	        ondblClickRow: gridFn.detail.ondblClickRow,
	        loadComplete: gridFn.detail.loadComplete,
	        listeners: {
                afterLoad: function(grid, response) {
                    $("#cb_jqGridDetail").trigger('click');
                }
            }
        }).init().setHeight(gridFn.detail.height);
    }
});
JS, \yii\web\View::POS_READY);