<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttpshd */
$params = '&id=' . $id . '&action=create';
$cancel = Custom::url(\Yii::$app->controller->id . '/cancel' . $params);
$this->title = 'Tambah - Penerimaan Barang Dari Supplier';
?>
<div class="ttpshd-create">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTBeli' => $dsTBeli,
        'id' => $id,
        'url' => [
            'update' => Custom::url(\Yii::$app->controller->id . '/update' . $params),
            'print' => Custom::url(\Yii::$app->controller->id . '/print' . $params),
            'cancel' => $cancel,
            'detail' => Url::toRoute(['ttpsit/index', 'action' => 'create']),
        ]
    ]) ?>

</div>
