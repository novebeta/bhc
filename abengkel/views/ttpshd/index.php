<?php
use abengkel\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '[PS] Penerimaan Barang';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'PSNo'         => 'No PS',
		'PINo'         => 'No PI',
		'PONo'         => 'No PO',
		'TINo'         => 'No TI',
		'KodeTrans'    => 'Kode Trans',
		'SupKode'      => 'Kode Sup',
		'DealerKode'   => 'Dealer ',
		'LokasiKode'   => 'Lokasi',
		'PSNoRef'      => 'No Ref',
		'PSKeterangan' => 'Keterangan',
		'NoGL'         => 'No GL',
		'Cetak'        => 'Cetak',
		'UserID'       => 'User ID',
	],
	'cmbTgl'    => [
		'PSTgl'        => 'Tanggal PS',
	],
	'cmbNum'    => [
		'PSSubTotal'   => 'Sub Total',
		'PSDiscFinal'  => 'Disc Final',
		'PSTotal'      => 'Total',
		'PSTerbayar'   => 'Terbayar',
	],
	'sortname'  => "PSTgl",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Ttpshd::className(), '[PS] Penerimaan Barang',    [
        'url_delete' => Url::toRoute( [ 'ttpshd/delete' ] ),
        'mode'=> isset($mode)? $mode : '',
    ]
), \yii\web\View::POS_READY );
