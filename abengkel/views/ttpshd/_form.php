<?php
use abengkel\components\FormField;
use abengkel\models\Tdlokasi;
use abengkel\models\Tdsupplier;
use common\components\General;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttpshd */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttpshd-form">
		<?php
		$dsTBeli[ 'PSDiscPersen' ] = 0;
		if ( $dsTBeli[ 'PSTotal' ] > 0 ) {
			$dsTBeli[ 'PSDiscPersen' ] = round( $dsTBeli[ 'PSDiscFinal' ] / $dsTBeli[ 'PSTotal' ] * 100 );
		}
		$altConditionLokasiKode = "LokasiStatus = 'A' AND (LEFT(LokasiNomor,1) = '7' OR LEFT(LokasiNomor,1) = '8')";
		$SupplierKodeCmb        = ArrayHelper::map( Tdsupplier::find()->select( [ 'SupKode', 'SupNama' ] )->where( "Supstatus = 'A' OR Supstatus = 1" )->orderBy( 'SupStatus,SupKode' )->all(), 'SupKode', 'SupNama' );
		$LokasiKodeCmb          = ArrayHelper::map( Tdlokasi::find()->select( [ 'LokasiKode' ] )->where( "LokasiStatus = 'A' AND (LEFT(LokasiNomor,1) = '7' OR LEFT(LokasiNomor,1) = '8')" )->orderBy( 'LokasiStatus, LokasiNomor, LokasiKode' )->all(), 'LokasiKode', 'LokasiKode' );
		$form                   = ActiveForm::begin( [ 'id' => 'form_ttpshd_id', 'action' => $url[ 'update' ] ] );
		\abengkel\components\TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-10",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelPSNo" ],
						      [ 'class' => "col-sm-8", 'items' => ":PSNo" ],
						      [ 'class' => "col-sm-3", 'items' => ":LabelPSTgl", 'style' => 'padding-left:5px;' ],
						      [ 'class' => "col-sm-5", 'items' => ":PSTgl" ],
						      [ 'class' => "col-sm-4", 'items' => ":PSJam" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-14",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelPINo" ],
						      [ 'class' => "col-sm-4", 'items' => ":PINo" ],
						      [ 'class' => "col-sm-4", 'items' => ":PITgl", 'style' => 'padding-left:2px;' ],
						      [ 'class' => "col-sm-3", 'items' => ":PIJam", 'style' => 'padding-left:2px;' ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelTC", 'style' => 'padding-left:4px;' ],
						      [ 'class' => "col-sm-8", 'items' => ":TC" ],
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-10",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelPONo" ],
						      [ 'class' => "col-sm-5", 'items' => ":PONo" ],
						      [ 'class' => "col-sm-3", 'items' => ":BtnSearch" ],
						      [ 'class' => "col-sm-3", 'items' => ":LabelPOTgl", 'style' => 'padding-left:5px;' ],
						      [ 'class' => "col-sm-5", 'items' => ":POTgl" ],
						      [ 'class' => "col-sm-4", 'items' => ":POJam" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-14",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelSupplier" ],
						      [ 'class' => "col-sm-11", 'items' => ":Supplier" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelLokasi", 'style' => 'padding-left:5px;' ],
						      [ 'class' => "col-sm-8", 'items' => ":Lokasi" ]
					      ],
					    ],
				    ],
				  ],
				  [
					  'class' => "row col-md-24",
					  'style' => "margin-bottom: 6px;",
					  'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-17",
					      'items' => [
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-4", 'items' => ":KodeBarang" ],
							        [ 'class' => "col-sm-10", 'items' => ":BarangNama", 'style' => 'padding-left:3px;' ],
							        [ 'class' => "col-sm-4", 'items' => ":Gudang", 'style' => 'padding-left:3px;' ],
							        [ 'class' => "col-sm-4", 'items' => ":Satuan", 'style' => 'padding-left:3px;' ],
							        [ 'class' => "col-sm-2", 'items' => ":BtnInfo" ],
						        ],
						      ],
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "form-group col-md-4",
                                        'items' => [
                                            [ 'class' => "form-group col-md-24",
                                                'items' => [
                                                    [ 'class' => "col-sm-3", 'items' => ":LabelKeterangan" ],
                                                ],
                                            ],
                                            [ 'class' => "form-group col-md-24",
                                                'items' => [
                                                    [ 'class' => "col-sm-7", 'items' => ":LabelCetak" ],
                                                    [ 'class' => "col-sm-14", 'items' => ":Cetak" ]
                                                ],
                                            ],
                                        ],
						            ],
                                    [ 'class' => "form-group col-md-13",
                                        'items' => [
                                            [ 'class' => "col-sm-23", 'items' => ":Keterangan" ],
                                        ],
                                      ],
                                    [ 'class' => "form-group col-md-7",
                                        'items' => [
                                            [ 'class' => "form-group col-md-24",
                                                'items' => [
                                                    [ 'class' => "col-sm-8", 'items' => ":LabelTINo" ],
                                                    [ 'class' => "col-sm-14", 'items' => ":TINo" ],
                                                ],
                                            ],
                                            [ 'class' => "form-group col-md-24",
                                                'items' => [
                                                    [ 'class' => "col-sm-8", 'items' => ":LabelZnoPenerimaan" ],
                                                    [ 'class' => "col-sm-14", 'items' => ":ZnoPenerimaan" ],
                                                ],
                                            ],
                                        ],
                                    ]
						        ],
						      ]
					      ],
					    ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "form-group col-md-6",
					      'items' => [
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-10", 'items' => ":LabelSubTotal" ],
							        [ 'class' => "col-sm-14", 'items' => ":SubTotal" ],
						        ],
						      ],
						      [ 'class' => "form-group col-md-24",
						        'items' => [
                                    [ 'class' => "col-sm-10", 'items' => ":LabelRefNo" ],
                                    [ 'class' => "col-sm-14", 'items' => ":RefNo" ],
						        ],
						      ],

						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-10", 'items' => ":LabelZnoShippingList" ],
							        [ 'class' => "col-sm-14", 'items' => ":ZnoShippingList" ],
						        ],
						      ],
					      ],
					    ],
				    ],
				  ],
			  ],
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
                  ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                      'items'  => [
                          ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":JT"],
                          ['class' => "col-sm-4 pull-right", 'items' => ":LabelJT"],
                      ],
                  ],
                  [
                      'class' => "col-md-12 pull-left",
                      'items' => $this->render( '../_nav', [
                          'url'=> $_GET['r'],
                          'options'=> [
                              'PSNo'         => 'No PS',
                              'PINo'         => 'No PI',
                              'PONo'         => 'No PO',
                              'TINo'         => 'No TI',
                              'KodeTrans'    => 'Kode Trans',
                              'SupKode'      => 'Kode Sup',
                              'DealerKode'   => 'Dealer ',
                              'LokasiKode'   => 'Lokasi',
                              'PSNoRef'      => 'No Ref',
                              'PSKeterangan' => 'Keterangan',
                              'NoGL'         => 'No GL',
                              'Cetak'        => 'Cetak',
                              'UserID'       => 'User ID',
                          ],
                      ])
                  ],
                  ['class' => "col-md-12 pull-right",
				    'items' => [
					    [ 'class' => "pull-right", 'items' => ":btnImport :btnJurnal :btnPrint :btnAction" ]
				    ]
				  ]
			  ]
			],
			[
				":LabelPONo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No PO</label>',
				":LabelPOTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl PO</label>',
				":LabelPSTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl PS</label>',
				":LabelPSNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No PS</label>',
				":LabelPINo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No PI</label>',
				":LabelTC"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">TC</label>',
				":LabelSubTotal"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sub Total</label>',
				":LabelKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelCetak"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Cetak</label>',
				":LabelSupplier"   => '<label id="lblSupp" class="control-label" style="margin: 0; padding: 6px 0;">Supplier</label>',
				":LabelLokasi"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Lokasi</label>',
				":LabelTINo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No TI</label>',
				":LabelRefNo"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Ref#</label>',
				":LabelJT"         => \common\components\General::labelGL( $dsTBeli[ 'NoGL' ], 'JT' ),
				":PSNo"            => Html::textInput( 'PSNoView', $dsTBeli[ 'PSNoView' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ) .
				                      Html::textInput( 'PSNo', $dsTBeli[ 'PSNo' ], [ 'class' => 'hidden' ] ) .
				                      Html::textInput( 'PosKode', $dsTBeli[ 'PosKode' ], [ 'class' => 'hidden' ] ) .
				                      Html::textInput( 'StokAll', 0, [ 'class' => 'hidden' ] ) .
				                      Html::textInput( 'PSTotalPajak', 0, [ 'id' => 'PSTotalPajak', 'class' => 'hidden' ] ) .
				                      Html::textInput( 'PSBiayaKirim', 0, [ 'id' => 'PSBiayaKirim', 'class' => 'hidden' ] ),
				":PINo"            => Html::textInput( 'PINo', $dsTBeli[ 'PINo' ], [ 'class' => 'form-control', 'maxlength' => '10', 'readonly' => true ] ),
				":PONo"            => Html::textInput( 'PONo', $dsTBeli[ 'PONo' ], [ 'class' => 'form-control', 'maxlength' => '10', 'readonly' => true ] ),
				":PSTgl"           => FormField::dateInput( [ 'name' => 'PSTgl', 'config' => [ 'value' => General::asDate( $dsTBeli[ 'PSTgl' ] ) ] ] ),
				":PITgl"           => FormField::dateInput( [ 'name' => 'PITgl', 'config' => [ 'value' => General::asDate( $dsTBeli[ 'PITgl' ] ) ] ] ),
				":POTgl"           => FormField::dateInput( [ 'name' => 'POTgl', 'config' => [ 'value' => General::asDate( $dsTBeli[ 'POTgl' ] ) ] ] ),
				":PIJam"           => FormField::timeInput( [ 'name' => 'PIJam', 'config' => [ 'value' => $dsTBeli[ 'PITgl' ] ] ] ),
				":POJam"           => FormField::timeInput( [ 'name' => 'POJam', 'config' => [ 'value' => $dsTBeli[ 'POTgl' ] ] ] ),
				":PSJam"           => FormField::timeInput( [ 'name' => 'PSJam', 'config' => [ 'value' => $dsTBeli[ 'PSTgl' ] ] ] ),
				":JT"              => Html::textInput( 'NoGL', $dsTBeli[ 'NoGL' ], [ 'class' => 'form-control', 'readOnly' => true ] ),
				":Cetak"           => Html::textInput( 'Cetak', $dsTBeli[ 'Cetak' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":Keterangan"      => Html::textarea( 'PSKeterangan', $dsTBeli[ 'PSKeterangan' ], [ 'class' => 'form-control','rows' => '3', 'maxlength' => '150' ] ),
				":RefNo"           => Html::textInput( 'PSNoRef', $dsTBeli[ 'PSNoRef' ], [ 'id' => 'PSNoRef', 'class' => 'form-control', 'maxlength' => '35' ] ),

				":TINo"            => Html::textInput( 'TINo', $dsTBeli[ 'TINo' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":SubTotal"        => FormField::numberInput( [ 'name' => 'PSSubTotal', 'config' => [ 'value' => $dsTBeli[ 'PSSubTotal' ] ] ] ),
				":TC"              => FormField::combo( 'KodeTrans', [ 'config' => [ 'id' => 'KodeTrans', 'value' => $dsTBeli[ 'KodeTrans' ], 'data' => [
					'TC01' => 'TC01 - Penerimaan dari Supplier MD',
					'TC02' => 'TC02 - Penerimaan dari Supplier Lokal',
					'TC03' => 'TC03 - Penerimaan Part Robbing',
					'TC05' => 'TC05 - Penerimaan dari Dealer Lain',
					'TC06' => 'TC06 - Penerimaan Olie KPB',
					'TC07' => 'TC07 - Penerimaan Part HO',
					'TC08' => 'TC08 - Penerimaan Part Claim',
					'TC09' => 'TC09 - Penerimaan Kekurangan Part/Oli',
					'TC10' => 'TC10 - Penerimaan Lain Lain',
					'TC41' => 'TC41 - Pemindahan part dari H1-Modal',
				] ], 'extraOptions'                                             => [ 'simpleCombo' => true ] ] ),

				":Supplier"        => FormField::combo( 'SupKode', [ 'config' => [ 'id' => 'SupKode', 'value' => $dsTBeli[ 'SupKode' ], 'data' => $SupplierKodeCmb ], 'extraOptions' => [ 'altLabel' => [ 'SupNama' ] ] ] ).
				                      FormField::combo( 'DealerKode', [ 'config' => [ 'id' => 'DealerKode', 'value' => $dsTBeli[ 'DealerKode' ],'class' => 'hidden' ], 'extraOptions' => [ 'altLabel' => [ 'DealerNama' ] ] ] ),
				":KodeBarang"      => Html::textInput( 'KodeBarang', '', [ 'id' => 'KodeBarang', 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":BarangNama"      => Html::textInput( 'BarangNama', '', [ 'id' => 'BarangNama', 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":Gudang"          => Html::textInput( 'LokasiKodeTxt', '', [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
                ":Lokasi"          => FormField::combo( 'LokasiKode', [ 'config' => [ 'id' => 'LokasiKode', 'value' => $dsTBeli[ 'LokasiKode' ], 'data' => $LokasiKodeCmb ], 'extraOptions' => [ 'simpleCombo' => true ], 'altCondition' => $altConditionLokasiKode ] ),
				":Satuan"          => Html::textInput( 'lblStock', 0, [ 'type' => 'number', 'class' => 'form-control', 'style' => 'color: blue; font-weight: bold;', 'readonly' => 'readonly' ] ),
				":BtnInfo"         => '<button type="button" class="btn btn-default"><i class="fa fa-info-circle fa-lg"></i></button>',
				":BtnSearch"       => '<button id="BtnSearch" type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>',
                ":btnImport"       => Html::button( '<i class="fa fa-arrow-circle-down"><br><br>Import</i>', [ 'class' => 'btn bg-navy ', 'id' => 'btnImport', 'style' => 'width:60px;height:60px' ] ),

                ":LabelZnoPenerimaan"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Penerima</label>',
                ":LabelZnoShippingList"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Shipping List</label>',
                ":ZnoPenerimaan"           => Html::textInput('ZnoPenerimaan', $dsTBeli[ 'ZnoPenerimaan' ], ['class' => 'form-control']),
                ":ZnoShippingList"           => Html::textInput('ZnoShippingList', $dsTBeli[ 'ZnoShippingList' ], ['class' => 'form-control']),
			],
			[
				'url_main' => 'ttpshd',
				'url_id' => $_GET['id'] ?? '',
                '_akses' => '[PS] Penerimaan Barang',
			]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlCheckStock           = Url::toRoute( [ 'site/check-stock-transaksi' ] );
$urlDaftarKonsumenSelect = Url::toRoute( [ 'tdcustomer/select' ] );
$urlKonsumenData         = Url::toRoute( [ 'tdcustomer/td', 'oper' => 'data' ] );
$urlDetail               = $url[ 'detail' ];
$urlDetailImport     = Url::toRoute(['ttpshd/import-loop']);
$urlPrint                = $url[ 'print' ];
$urlPO                   = Url::toRoute( [ 'ttpohd/select','tgl1'=> General::asDate('-90 days') ] );
//$urlImport        = Url::toRoute( [ 'ttpshd/import','tgl1'    => Yii::$app->formatter->asDate( '-5 day', 'yyyy-MM-dd') ] );
$urlImport        = Url::toRoute( [ 'ttpshd/import','tgl1'    => Yii::$app->formatter->asDate( 'now', 'yyyy-MM-dd') ] );
$readOnly                = ( $_GET[ 'action' ] == 'view' ? 'true' : 'false' );
$barang                  = \common\components\General::cCmd(
	"SELECT BrgKode, BrgNama, BrgBarCode, BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual, BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus
            FROM tdbarang 
            WHERE BrgStatus = 'A' 
            UNION
            SELECT BrgKode, BrgNama, BrgBarCode,  BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual, BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus FROM tdBarang 
            WHERE BrgKode IN 
            (	SELECT ttpsit.BrgKode FROM ttpsit INNER JOIN tdbarang ON ttpsit.BrgKode = tdbarang.BrgKode 
                WHERE ttpsit.PSNo = :PSNo AND tdBarang.BrgStatus = 'N' GROUP BY ttpsit.BrgKode )
            ORDER BY BrgStatus, BrgKode", [ ':PSNo' => $dsTBeli[ 'PSNo' ] ] )->queryAll();
$this->registerJsVar( '__Barang', json_encode( $barang ) );
$this->registerJsVar( '__model', $dsTBeli );
$this->registerJsVar( '__urlPO', $urlPO );
$this->registerJs( <<< JS
     var __BrgKode = JSON.parse(__Barang);
     __BrgKode = $.map(__BrgKode, function (obj) {
                      obj.id = obj.BrgKode;
                      obj.text = obj.BrgKode;
                      return obj;
                    });
     var __BrgNama = JSON.parse(__Barang);
     __BrgNama = $.map(__BrgNama, function (obj) {
                      obj.id = obj.BrgNama;
                      obj.text = obj.BrgNama;
                      return obj;
                    });
        function HitungTotal(){
         	// let PSTotalPajak = $('#detailGrid').jqGrid('getCol','PSPajak',false,'sum');
         	// $('#PSTotalPajak').utilNumberControl().val(PSTotalPajak);
         	let PSSubTotal = $('#detailGrid').jqGrid('getCol','Jumlah',false,'sum');
         	$('#PSSubTotal').utilNumberControl().val(PSSubTotal);
         	HitungBawah();
         }
         window.HitungTotal = HitungTotal;
         function HitungBawah(){
         	let PSSubTotal = parseFloat($('#PSSubTotal').utilNumberControl().val());
         	// let PSTotalPajak = parseFloat($('#PSTotalPajak').utilNumberControl().val());                 
         	// let PSBiayaKirim = parseFloat($('#PSBiayaKirim').utilNumberControl().val());
         	// let PSTotal = PSSubTotal;
         	// $('#PSTotal').utilNumberControl().val(PSTotal);
         }         
         window.HitungBawah = HitungBawah;
         
         function checkStok(BrgKode,BarangNama){
           $.ajax({
                url: '$urlCheckStock',
                type: "POST",
                data: { 
                    BrgKode: BrgKode,
                },
                success: function (res) {
                    $('[name=LokasiKodeTxt]').val(res.LokasiKode);
                    $('#KodeBarang').val(BrgKode);
                    $('[name=BarangNama]').val(BarangNama);
                    $('[name=lblStock]').val(res.SaldoQty);
                },
                error: function (jXHR, textStatus, errorThrown) {
                    
                },
                async: false
            });
          }
          window.checkStok = checkStok;	
         
     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 220,
        extraParams: {
            PSNo: $('input[name="PSNo"]').val()
        },
        rownumbers: true,  
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,     
        colModel: [
            { template: 'actions'  },
            {
                name: 'BrgKode',
                label: 'Kode',
                editable: true,
                width: 150,
                editor: {
                    type: 'select2',
                    data: __BrgKode,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    console.log(data);
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'PSHrgJual'+'"]').inputmask('setvalue',parseFloat(data.BrgHrgBeli));
                            $('[id="'+rowid+'_'+'BrgGroup'+'"]').val(data.BrgGroup);
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').val(data.BrgNama); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').trigger('change');  
                            checkStok($(this).val(),$('[id="'+rowid+'_'+'BrgNama'+'"]').val());
                            window.hitungLine();
						},
						'change': function(){      
                            let rowid = $(this).attr('rowid');
                            checkStok($(this).val(),$('[id="'+rowid+'_'+'BrgNama'+'"]').val());
						}
                    }                 
                }
            },
            {
                name: 'BrgNama',
                label: 'Nama Barang',
                width: 260,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __BrgNama,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'PSHrgJual'+'"]').inputmask('setvalue',parseFloat(data.BrgHrgBeli));
                            $('[id="'+rowid+'_'+'BrgGroup'+'"]').val(data.BrgGroup);
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').val(data.BrgKode); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').trigger('change');
                            window.hitungLine();
						}
                    }              
                }
            },
            {
                name: 'PSQty',
                label: 'Qty',
                width: 75,
                editable: true,
                template: 'number'
            },
            {
                name: 'BrgSatuan',
                label: 'Satuan',
                editable: true,
                width: 75,
            },  
            {
                name: 'PSHrgJual',
                label: 'Harga Part',
                width: 100,
                editable: true,
                template: 'money',
                editor: { readOnly: true }
            },
            {
                name: 'Disc',
                label: 'Disc %',
                width: 75,
                editable: true,
                template: 'number'
            },            
            {
                name: 'PSDiscount',
                label: 'Nilai Disc',
                width: 100,
                editable: true,
                template: 'money'
            },
            {
                name: 'Jumlah',
                label: 'Jumlah',
                width: 100,
                editable: true,
                template: 'money',
                editor: { readOnly: true }
            },          
            {
                name: 'PSPajak',
                label: 'Pajak',
                editable: true,
                template: 'money',
                hidden: true
            },
            {
                name: 'PONo',
                label: 'PONo',
                editable: true,
                hidden: true
            },
            {
                name: 'PINo',
                label: 'PINo',
                editable: true,
                hidden: true
            },
            {
                name: 'PSAuto',
                label: 'PSAuto',
                editable: true,
                hidden: true
            },            
        ],
        listeners: {
            afterLoad: function (data) {
             window.HitungTotal();
            }    
        },
        onSelectRow : function(rowid,status,e){ 
            if (window.rowId !== -1) return;
            if (window.rowId_selrow === rowid) return;
            if(rowid.includes('new_row') === true){
                $('[name=KodeBarang]').val('--');
                $('[name=LokasiKodeTxt]').val('Kosong');
                $('[name=BarangNama]').val('--');
                $('[name=lblStock]').val(0);
                return ;
            };
            let r = $('#detailGrid').utilJqGrid().getData(rowid).data;
			checkStok(r.BrgKode,r.BrgNama);
			window.rowId_selrow = rowid;
        }
     }).init().fit($('.box-body')).load({url: '$urlDetail'});  
     window.rowId_selrow = -1;
     $('#KodeTrans').on('change', function (e) {
        var KodeTrans = $(this).val();
        showSupDealer(KodeTrans);
        switch (KodeTrans){
            case 'TC01':
            case 'TC05':
            case 'TC06':
            case 'TC09':
            case 'TC10':
                $('#LokasiKode').val('Penerimaan1').trigger('change');
                $('#SupKode').prop('selectedIndex', 0).change();
                break;
            case 'TC02':
                $('#LokasiKode').val('Lokal').trigger('change');
                $('#SupKode').prop('selectedIndex', 1).change();
                break;
            case 'TC03':
                $('#LokasiKode').val('Robbing').trigger('change');
                $('#SupKode').prop('selectedIndex', 0).change();
                break;
            case 'TC07':
                $('#LokasiKode').val('HotLine').trigger('change');
                $('#SupKode').prop('selectedIndex', 0).change();
                break;
            case 'TC08':
                $('#LokasiKode').val('Klaim').trigger('change');                
                $('#SupKode').prop('selectedIndex', 0).change();
                break;
        }
    });

    function showSupDealer(KodeTrans){
        var LokasiKode = $('#LokasiKode');
        var SupKode = $('#SupKode'); 
        var DealerKode = $('#DealerKode'); 
        var lblSupp = $('#lblSupp');        
        if (KodeTrans !== 'TC05'){
             lblSupp.text('Supplier');
             SupKode.next(".select2-container").show();
             SupKode.attr('disabled', false);
             DealerKode.next(".select2-container").hide();
             DealerKode.attr('disabled', true);
        }else{            
             lblSupp.text('Dealer');
             DealerKode.next(".select2-container").show();
             DealerKode.attr('disabled', false);
             SupKode.next(".select2-container").hide();
             SupKode.attr('disabled', true);
        }
    }
     
     function hitungLine(){
            
            let Disc = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Disc'+'"]').val()));
            let PSHrgJual = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'PSHrgJual'+'"]').val()));
            let PSQty = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'PSQty'+'"]').val()));
            let PSDiscount = Disc*PSHrgJual*PSQty / 100;
            $('[id="'+window.cmp.rowid+'_'+'PSDiscount'+'"]').utilNumberControl().val(PSDiscount);
            let jumlah = PSHrgJual * PSQty - PSDiscount;
            $('[id="'+window.cmp.rowid+'_'+'Jumlah'+'"]').inputmask('setvalue',parseFloat(jumlah));
            window.HitungTotal();
         }
         window.hitungLine = hitungLine;
    
	 $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
	     window.cmp.rowid = rowid;    
          
	     if(rowid.includes('new_row')){
              var BrgKode = $('[id="'+rowid+'_'+'BrgKode'+'"]');
              var dtBrgKode = BrgKode.select2('data');
              if (dtBrgKode.length > 0){
                  var dt = dtBrgKode[0];
                  BrgKode.val(dt.BrgKode).trigger('change').trigger({
                    type: 'select2:select',
                    params: {
                        data: dt
                    }
                });
              }
            $('[id="'+rowid+'_'+'PSQty'+'"]').val(1);
          }
         
          $('[id="'+window.cmp.rowid+'_'+'PSQty'+'"]').on('keyup', function (event){
              hitungLine();
          });
          $('[id="'+window.cmp.rowid+'_'+'PSHrgJual'+'"]').on('keyup', function (event){
              hitungLine();
          });
          $('[id="'+window.cmp.rowid+'_'+'Disc'+'"]').on('keyup', function (event){
              hitungLine();
          });
         hitungLine();
	 });
	 
	 $('#SearchKonsumen').click(function () {
        window.util.app.dialogListData.show({
                title: 'Daftar Konsumen',
                url: '$urlDaftarKonsumenSelect'
            },
            function (data) {
                console.log(data);
                $.ajax({
                    type: 'POST',
                    url: '$urlKonsumenData',
                    data: {
                        'oper': 'data',
                        'id': btoa(data.id)
                    }
                }).then(function (cusData) {
                   console.log(cusData);
                   Object.keys(cusData).forEach(function(key,index) {
                            let cmp = $('input[name="'+key+'"]');
                            cmp.val(Object.values(cusData)[index]);
                   });
                });
            });
        });
	  
	 $('#btnSave').click(function (event) {
	     let Nominal = parseFloat($('#PSSubTotal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Subtotal tidak boleh 0', size: 'small'});
               return;
           }  
           let NoRef = $('#PSNoRef').val();
           if ( NoRef === "" || NoRef === "--" ){
               bootbox.alert({message:'No Ref# harus diisi', size: 'small'});
               return;
           } 
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttpshd_id').attr('action','{$url['update']}');
        $('#form_ttpshd_id').submit();
      });  
	 
	  window.getUrlPO = function(){
        let PONo = $('input[name="PONo"]').val();
        return __urlPO + ((PONo === '' || PONo === '--') ? '':'&check=off&cmbTxt1=PONo&txt1='+PONo);
    } 
    
    function sendDataLoop(itemData, XCetak){
        $.ajax({
             type: "POST",
             url: '$urlDetail',
             data: {
                 oper: 'batch',
                 data: itemData,
                 header: btoa($('#form_ttpshd_id').serialize()),
                 XCetak: XCetak
             },
             success: function(data) {
               window.location.href = "{$url['update']}&oper=skip-load&PSNoView={$dsTBeli['PSNoView']}"; 
             },
         });
    }
    
    $('#BtnSearch').click(function(){
        window.util.app.dialogListData.show({
            title: 'Daftar Purchase Order / Order Pembelian',
            url: getUrlPO()
        },
        function (data) {
             console.log(data);
             if (!Array.isArray(data)){
                 return;
             }
             $.ajax({
                 type: "POST",
                 url: '$urlDetail',
                 data: {
                     oper: 'cekpops',
                     PONo: data[0].header.id,
                     PSNo: '{$dsTBeli['PSNo']}',
                 },
                 success: function(cekpops) {
                     console.log(cekpops);
                     var itemData = [];
                     data.forEach(function(itm,ind) {
                        let cmp = $('input[name="PONo"]');
                        cmp.val(itm.header.data.PONo);
                        itm.data.PONo = itm.header.data.PONo; 
                        itm.data.PSNo = __model.PSNo; 
                        itm.data.PSHrgJual = itm.data.POHrgJual; 
                        itm.data.PSQty = itm.data.POQty; 
                        itm.data.PSDiscount = itm.data.PODiscount; 
                        itemData.push(itm.data);
                    });
                     if (parseInt(cekpops.rowsCount) > 0){
                         bootbox.confirm("Purchase Sheet {$dsTBeli['PSNo']} telah memiliki Data Barang,<br>" +
                          "Apakah Data Barang pada No Packing Order "+data[0].header.id+" akan ditambahkan ke dalam Purchase Sheet?", function(result){ 
                            sendDataLoop(itemData, result ? 'AdaYes':'AdaNo'); 
                        });
                     }else{
                        sendDataLoop(itemData, 'TidakAda');
                     }
                 },
               });
             
        })
    });
	  
	  
	
    let searchParams = new URLSearchParams(window.location.search);
    if (searchParams.get('action') === 'create'){
        $('#KodeTrans').trigger('change');
    }else{
        var KodeTrans = $(this).val();
        var LokasiKode = $('#LokasiKode');
        var SupKode = $('#SupKode'); 
        var DealerKode = $('#DealerKode'); 
        var lblSupp = $('#lblSupp');        
        if (KodeTrans !== 'TC05'){
             lblSupp.text('Supplier');
             SupKode.next(".select2-container").show();
             SupKode.attr('disabled', false);
             DealerKode.next(".select2-container").hide();
             DealerKode.attr('disabled', true);
        }else{            
             lblSupp.text('Dealer');
             DealerKode.next(".select2-container").show();
             DealerKode.attr('disabled', false);
             SupKode.next(".select2-container").hide();
             SupKode.attr('disabled', true);
        }
    }
    
    
    $('#btnImport').click(function (event) {	      
        window.util.app.dialogListData.show({
            title: 'Manage Parts Inbound - PINB - PS',
            url: '{$urlImport}'
        },
        function (data) {
			if(!Array.isArray(data)) return;
              $.ajax({
                type: "POST",
                url: '$urlDetailImport',
                data: {
                  oper: 'import',
                  data: data,
                  header: btoa($('#form_ttpshd_id').serialize())
                },
                success: function(data) {
                  window.location.href = "{$url['update']}&oper=skip-load&PSNoView={$dsTBeli['PSNoView']}"; 
                },
              });
        });
    });   
    
    $(function() {
        var KodeTrans = $('#KodeTrans').val();
        showSupDealer(KodeTrans);
    });
     
JS
);