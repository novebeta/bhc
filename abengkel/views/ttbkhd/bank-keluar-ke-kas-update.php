<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttbkhd */
$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = 'Edit - Bank Keluar Ke Kas : ' . $model->BKNo;
?>
<div class="ttbkhd-update">
    <?= $this->render('_bank-keluar-ke-kas-form', [
        'model'     => $model,
        'dsTUang'   => $dsTUang,
        'id'        => $id,
        'url'       => [
            'update' => Custom::url( \Yii::$app->controller->id . '/bank-keluar-ke-kas-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
        ]
    ]) ?>

</div>
