<?php
use abengkel\assets\AppAsset;
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Bank Keluar Order Pengerjaan Luar';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
    'cmbTxt'    => [
        'BKNo'       => 'No BK',
        'BKJenis'    => 'Jenis',
        'KodeTrans'  => 'Kode Trans',
        'PosKode'    => 'Pos Kode',
        'SupKode'    => 'Sup Kode',
        'BankKode'   => 'Bank Kode',
        'BKAC'       => 'AC',
        'BKPerson'   => 'Person',
        'BKCekNo'    => 'No Cek',
        'BKMemo'     => 'Memo',
        'NoGL'       => 'No GL',
        'Cetak'      => 'Cetak',
        'UserID'     => 'User ID',
    ],
    'cmbTgl'    => [
        'BKTgl'      => 'Tanggal BK',
        'BKCekTempo' => 'Cek Tempo',
    ],
    'cmbNum'    => [
        'BKNominal'  => 'Nominal',
    ],
    'sortname'  => "BKTgl",
    'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Ttbkhd::className(), 'BK PL - Order Pengerjaan Luar',
    [
    'url_update' => Url::toRoute( [ 'ttbkhd/bank-keluar-order-pengerjaan-luar-update' ,'action'=>'update']),
    'url_add'    => Url::toRoute( [ 'ttbkhd/bank-keluar-order-pengerjaan-luar-create', 'action' => 'create' ] ),
    'url_delete' => Url::toRoute( [ 'ttbkhd/delete' ] ),
    ] ), \yii\web\View::POS_READY );
