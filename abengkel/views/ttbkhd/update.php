<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttbkhd */
$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     =  "Edit - $title : " . $model->BKNo;
?>
<div class="ttbkhd-update">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'id'     => $id,
        'view' => $view,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/'.$view .'-update'. $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttbkit/index','action' => 'create' ] ),
        ]
    ]) ?>
</div>