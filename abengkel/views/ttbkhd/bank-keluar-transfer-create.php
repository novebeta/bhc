<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttbkhd */
$params                          = '&id=' . $id . '&action=UpdateAdd';
$cancel                          = Custom::url( 'ttbkhd/bank-transfer-cancel'.$params );
$this->title                     = 'Tambah - Bank Transfer';
?>
<div class="ttbkhd-create">
    <?= $this->render('_bank-keluar-transfer-form', [
        'dsTUang' => $dsTUang,
        'id'     => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/transfer-antar-bank-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttbkit/index','action' => 'create' ] ),
        ]
    ]) ?>
</div>
