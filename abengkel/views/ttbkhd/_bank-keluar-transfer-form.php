<?php
use abengkel\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttbkhd */
/* @var $form yii\widgets\ActiveForm */
$format          = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape          = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
<div class="ttpihd-form">
	<?php
	$form = ActiveForm::begin( ['id' => 'form_ttbkhd_id', 'action' => $url[ 'update' ] ] );
	\abengkel\components\TUi::form(
		[ 'class' => "row-no-gutters",
		  'items' => [
			  [ 'class' => "form-group col-md-24",
			    'items' => [
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-4", 'items' => ":LabelPINo" ],
					      [ 'class' => "col-sm-7", 'items' => ":PINo" ],
					      [ 'class' => "col-sm-1" ],
					      [ 'class' => "col-sm-2", 'items' => ":LabelPITgl" ],
					      [ 'class' => "col-sm-4", 'items' => ":PITgl" ],
					      [ 'class' => "col-sm-5", 'items' => ":PIJam", 'style' => 'padding-left:1px;' ],
				      ],
				    ],
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-3", 'items' => ":LabelKK" ],
					      [ 'class' => "col-sm-8", 'items' => ":KK" ],
					      [ 'class' => "col-sm-2" ],
					      [ 'class' => "col-sm-3", 'items' => ":LabelKM" ],
					      [ 'class' => "col-sm-8", 'items' => ":KM" ],
				      ],
				    ],
			    ],
			  ],
			  [ 'class' => "form-group col-md-24",
			    'items' => [
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-4", 'items' => ":LabelJenis" ],
					      [ 'class' => "col-sm-7", 'items' => ":Jenis" ],
					      [ 'class' => "col-sm-1" ],
				      ],
				    ],
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-3", 'items' => ":LabelJTKK" ],
					      [ 'class' => "col-sm-8", 'items' => ":JTKK" ],
					      [ 'class' => "col-sm-2", ],
					      [ 'class' => "col-sm-3", 'items' => ":LabelJTKM" ],
					      [ 'class' => "col-sm-8", 'items' => ":JTKM" ],
				      ],
				    ],
			    ],
			  ],
			  [ 'class' => "form-group col-md-24",
			    'items' => [
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-4", 'items' => ":LabelAsal" ],
					      [ 'class' => "col-sm-19", 'items' => ":Asal" ],
					      [ 'class' => "col-sm-1" ],
				      ],
				    ],
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-5", 'items' => ":LabelDiserahkan" ],
					      [ 'class' => "col-sm-19", 'items' => ":Diserahkan" ],
				      ],
				    ],
			    ],
			  ],
			  [ 'class' => "form-group col-md-24",
			    'items' => [
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-4", 'items' => ":LabelAC" ],
					      [ 'class' => "col-sm-8", 'items' => ":ACBK" ],
					      [ 'class' => "col-sm-1" ],
					      [ 'class' => "col-sm-2", 'items' => ":LabelCek" ],
					      [ 'class' => "col-sm-8", 'items' => ":CekBK" ],
				      ],
				    ],
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-5", 'items' => ":LabelTglCair" ],
					      [ 'class' => "col-sm-5", 'items' => ":TglCairBK" ],
				      ],
				    ],
			    ],
			  ],
			  [ 'class' => "form-group col-md-24",
			    'items' => [
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-4", 'items' => ":LabelTujuan" ],
					      [ 'class' => "col-sm-19", 'items' => ":Tujuan" ],
					      [ 'class' => "col-sm-1" ],
				      ],
				    ],
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-5", 'items' => ":LabelDiterima" ],
					      [ 'class' => "col-sm-19", 'items' => ":Diterima" ],
				      ],
				    ],
			    ],
			  ],
			  [ 'class' => "form-group col-md-24",
			    'items' => [
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-4", 'items' => ":LabelAC" ],
					      [ 'class' => "col-sm-8", 'items' => ":ACBM" ],
					      [ 'class' => "col-sm-1" ],
					      [ 'class' => "col-sm-2", 'items' => ":LabelCek" ],
					      [ 'class' => "col-sm-8", 'items' => ":CekBM" ],
				      ],
				    ],
				    [ 'class' => "form-group col-md-12",
				      'items' => [
					      [ 'class' => "col-sm-5", 'items' => ":LabelTglCair" ],
					      [ 'class' => "col-sm-5", 'items' => ":TglCairBM" ],
				      ],
				    ],
			    ],
			  ],
			  [ 'class' => "form-group col-md-24",
			    'items' => [
				    [ 'class' => "col-sm-2", 'items' => ":LabelKeterangan" ],
				    [ 'class' => "col-sm-15", 'items' => ":Keterangan" ],
				    [ 'class' => "col-sm-1" ],
				    [ 'class' => "col-sm-2", 'items' => ":LabelNominal" ],
				    [ 'class' => "col-sm-4", 'items' => ":Nominal" ],
			    ],
			  ],
		  ],
		],
		[ 'class' => "row-no-gutters",
		  'items' => [
			  [ 'class' => "col-md-24",
			    'items' => [
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'BKNo'       => 'No BK',
                                'BKJenis'    => 'Jenis',
                                'KodeTrans'  => 'Kode Trans',
                                'PosKode'    => 'Pos Kode',
                                'SupKode'    => 'Sup Kode',
                                'BankKode'   => 'Bank Kode',
                                'BKAC'       => 'AC',
                                'BKPerson'   => 'Person',
                                'BKCekNo'    => 'No Cek',
                                'BKMemo'     => 'Memo',
                                'NoGL'       => 'No GL',
                                'Cetak'      => 'Cetak',
                                'UserID'     => 'User ID',
                            ],
                        ])
                    ],
				    [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
			    ]
			  ]
		  ]
		],
		[
			":LabelPINo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No BT</label>',
			":LabelPITgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl BT</label>',
			":LabelKK"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">No BK</label>',
			":LabelKM"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">No BM</label>',
			":LabelJenis"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
			":LabelTrans"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Trans</label>',
			":LabelJTKK"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">JT BK</label>',
			":LabelJTKM"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">JT BM</label>',
			":LabelAsal"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bank Keluar</label>',
			":LabelTujuan"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bank Masuk</label>',
			":LabelKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
			":LabelNominal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
			":LabelDiterima"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Terima Dari</label>',
			":LabelDiserahkan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bayar Ke</label>',
			":LabelAC"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">A/C</label>',
			":LabelCek"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Cek</label>',
			":LabelTglCair"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Cair</label>',
			":PINo"            => Html::textInput( 'BTNoView', str_replace( '=', '-', $dsTUang[ 'BTNo' ] ), [ 'class' => 'form-control', 'readonly' => true ] ) .
			                      Html::textInput( 'BTNo', $dsTUang[ 'BTNo' ], [ 'class' => 'hidden' ] ),
			":KK"              => Html::textInput( 'BKNoView', str_replace( '=', '-', $dsTUang[ 'BKNo' ] ), [ 'class' => 'form-control' ] ) .
			                      Html::textInput( 'BKNo', $dsTUang[ 'BKNo' ], [ 'class' => 'hidden' ] ),
			":KM"              => Html::textInput( 'BMNoView', str_replace( '=', '-', $dsTUang[ 'BMNo' ] ), [ 'class' => 'form-control' ] ) .
			                      Html::textInput( 'BMNo', $dsTUang[ 'BMNo' ], [ 'class' => 'hidden' ] ).
			                      Html::textInput( 'KodeTrans', 'BT', [ 'class' => 'hidden' ] ),
			":PITgl"           => FormField::dateInput( [ 'name' => 'BTTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BKTgl' ] ) ] ] ),
			":TglCairBK"       => FormField::dateInput( [ 'name' => 'BKCekTempo', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BKCekTempo' ] ) ] ] ),
			":TglCairBM"       => FormField::dateInput( [ 'name' => 'BMCekTempo', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BMCekTempo' ] ) ] ] ),
			":PIJam"           => FormField::timeInput( [ 'name' => 'BTJam', 'config' => [ 'value' => $dsTUang[ 'BKTgl' ] ] ] ),
			":Jenis"           => Html::textInput( 'BKJenis', $dsTUang[ 'BKJenis' ], [ 'class' => 'form-control' ] ),
			":JTKK"            => Html::textInput( 'NoGLBK', $dsTUang[ 'NoGLBK' ], [ 'class' => 'form-control' ] ),
			":JTKM"            => Html::textInput( 'NoGLBM', $dsTUang[ 'NoGLBM' ], [ 'class' => 'form-control' ] ),
			":Diserahkan"      => Html::textInput( 'BKPerson', $dsTUang[ 'BKPerson' ], [ 'class' => 'form-control', 'maxlength' => '50' ] ),
			":Asal"            => FormField::combo( 'NoAccount', ['name' => 'NoAccountBK', 'config' => [ 'value' => $dsTUang[ 'NoAccountBK' ], 'data'=> \abengkel\models\Traccount::find()->bank() ] ] ),
			":Tujuan"          => FormField::combo( 'NoAccount', ['name' => 'NoAccountBM',  'config' => ['value' => $dsTUang[ 'NoAccountBM' ], 'data'=> \abengkel\models\Traccount::find()->bank() ] ] ),
			":Keterangan"      => Html::textInput( 'BKMemo', $dsTUang[ 'BKMemo' ], [ 'class' => 'form-control', 'maxlength' => '150' ] ),
			":Diterima"        => Html::textInput( 'BMPerson', $dsTUang[ 'BMPerson' ], [ 'class' => 'form-control', 'maxlength' => '50' ] ),
			":Nominal"         => FormField::numberInput( [ 'name' => 'BKNominal', 'config' => [ 'value' => $dsTUang[ 'BKNominal' ] ] ] ),
			":ACBK"            => Html::textInput( 'BKAC', $dsTUang[ 'BKAC' ], [ 'class' => 'form-control' ] ),
			":CekBK"           => Html::textInput( 'BKCekNo', $dsTUang[ 'BKCekNo' ], [ 'class' => 'form-control' ] ),
			":ACBM"            => Html::textInput( 'BMAC', $dsTUang[ 'BMAC' ], [ 'class' => 'form-control' ] ),
			":CekBM"           => Html::textInput( 'BMCekNo', $dsTUang[ 'BMCekNo' ], [ 'class' => 'form-control' ] ),
			":BtnInfo"         => '<button type="button" class="btn btn-default"><i class="fa fa-refresh fa-lg"></i></button>',
			":BtnBayar"        => Html::button( '<i class="fa fa-dollar fa-lg"></i> Bayar', [ 'class' => 'btn btn-primary', 'style' => 'width:120px' ] ),
		],
        [
            'url_main'         => 'ttbkhd',
            'url_id'           => $id,
            'url_delete'        => Url::toRoute(['ttbkhd/delete', 'id' => $_GET['id'] ?? ''] ),
        ]
	);
	ActiveForm::end();
	?>
</div>
<?php
$urlAdd   = \abengkel\models\Ttbkhd::getRoute( 'BT',['action'=>'create'])[ 'create' ];
$urlIndex = \abengkel\models\Ttbkhd::getRoute( 'BT', [ 'id' => $id ] )[ 'index' ];
$this->registerJs( <<< JS
	$('#btnSave').click(function (event) {	      
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttbkhd_id').attr('action','{$url['update']}');
        $('#form_ttbkhd_id').submit();
    }); 

    $('#btnCancel').click(function (event) {	      
       $('#form_ttbkhd_id').attr('action','{$url['cancel']}');
       $('#form_ttbkhd_id').submit();
    });
    
    $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });    

JS
);

