<?php
use abengkel\components\FormField;
use abengkel\models\Tdsupplier;
use common\components\Custom;
use common\components\General;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\abengkel\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttbkhd */
/* @var $form yii\widgets\ActiveForm */
$format          = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape          = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttpihd-form">
		<?php
		$SupplierKodeCmb = ArrayHelper::map( Tdsupplier::find()->select( [ 'SupKode', 'SupNama' ] )->where( [ 'SupStatus' => 'A' or 'D' ] )->orderBy( 'SupStatus,SupKode' )->all(), 'SupKode', 'SupNama' );
		$form            = ActiveForm::begin( ['id' => 'form_ttbkhd_id','action' => $url[ 'update' ],   ] );
		\abengkel\components\TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-16",
					      'items' => [
						      [ 'class' => "col-sm-2", 'items' => ":LabelBKNo" ],
						      [ 'class' => "col-sm-4", 'items' => ":BKNo" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelBKTgl" ],
						      [ 'class' => "col-sm-3", 'items' => ":BKTgl" ],
						      [ 'class' => "col-sm-4", 'items' => ":BKJam", 'style' => 'padding-left:1px;' ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelJenis" ],
						      [ 'class' => "col-sm-4", 'items' => ":Jenis" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-8",
					      'items' => [
						      [ 'class' => "col-sm-2" ],
						      [ 'class' => "col-sm-4", 'items' => ":LabelTrans" ],
						      [ 'class' => "col-sm-18", 'items' => ":Trans" ]
					      ],
					    ],
				    ],
				  ],
				  [
					  'class' => "row col-md-24",
					  'style' => "margin-bottom: 6px;",
					  'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelKode" ],
						      [ 'class' => "col-sm-21", 'items' => ":Kode" ],
					      ],
					    ],
                        [ 'class' => "form-group col-md-6",
                            'items' => [
                                [ 'class' => "col-sm-2" ],
                                [ 'class' => "col-sm-6", 'items' => ":LabelTotal" ],
                                [ 'class' => "col-sm-16", 'items' => ":Total" ],
                            ],
					    ],
					    [ 'class' => "form-group col-md-6",
					      'items' => [
						      [ 'class' => "col-sm-2" ],
						      [ 'class' => "col-sm-6", 'items' => ":LabelNominal" ],
						      [ 'class' => "col-sm-16", 'items' => ":Nominal" ],
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-16",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelNoTrans" ],
						      [ 'class' => "col-sm-7", 'items' => ":NoTrans" ],
						      [ 'class' => "col-sm-2", 'items' => ":BtnSearch" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelPolisi" ],
						      [ 'class' => "col-sm-10", 'items' => ":SupKode" ]
					      ],
					    ],
					    [ 'class' => "form-group col-md-8",
					      'items' => [
						      [ 'class' => "col-sm-2" ],
						      [ 'class' => "col-sm-6", 'items' => ":LabelDiterima" ],
						      [ 'class' => "col-sm-16", 'items' => ":Diterima" ],
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-16",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelAC" ],
						      [ 'class' => "col-sm-8", 'items' => ":AC" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelCek" ],
						      [ 'class' => "col-sm-10", 'items' => ":Cek" ]
					      ],
					    ],
					    [ 'class' => "form-group col-md-8",
					      'items' => [
						      [ 'class' => "col-sm-2" ],
						      [ 'class' => "col-sm-6", 'items' => ":LabelTglCair" ],
						      [ 'class' => "col-sm-16", 'items' => ":TglCair" ],
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelKeterangan" ],
					    [ 'class' => "col-sm-22", 'items' => ":Keterangan" ],
				    ],
				  ],
			  ],
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
                  ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                      'items'  => [
                          ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":JT"],
                          ['class' => "col-sm-4 pull-right", 'items' => ":LabelJT"],
                      ],
                  ],
                  [
                      'class' => "col-md-12 pull-left",
                      'items' => $this->render( '../_nav', [
                          'url'=> $_GET['r'],
                          'options'=> [
                              'BKNo'       => 'No BK',
                              'BKJenis'    => 'Jenis',
                              'KodeTrans'  => 'Kode Trans',
                              'PosKode'    => 'Pos Kode',
                              'SupKode'    => 'Sup Kode',
                              'BankKode'   => 'Bank Kode',
                              'BKAC'       => 'AC',
                              'BKPerson'   => 'Person',
                              'BKCekNo'    => 'No Cek',
                              'BKMemo'     => 'Memo',
                              'NoGL'       => 'No GL',
                              'Cetak'      => 'Cetak',
                              'UserID'     => 'User ID',
                          ],
                      ])
                  ],
                  ['class' => "col-md-12 pull-right",
				    'items' => [
					    [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
				    ]
				  ]
			  ]
			],
			[
				":LabelBKNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No BK</label>',
				":LabelBKTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl BK</label>',
				":LabelJenis"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				":LabelTrans"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Trans</label>',
				":LabelKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelNominal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
				":LabelTotal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total</label>',
				":LabelDiterima"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Diterima Oleh</label>',
				":LabelKode"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode</label>',
				":LabelNoTrans" => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Transaksi</label>',
				":LabelAC"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">A/C</label>',
				":LabelPolisi"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Supplier</label>',
				":LabelCek"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Cek</label>',
				":LabelTglCair" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Cair</label>',
				":LabelJT"      => \common\components\General::labelGL( $dsTUang[ 'NoGL' ],'JT' ),
                ":BKNo"         => Html::textInput( 'BKNoView', $dsTUang[ 'BKNoView' ], [ 'class' => 'form-control','readonly' => 'readonly', 'tabindex' => '1' ] ) .
                                     Html::textInput( 'BKNo', $dsTUang[ 'BKNo' ], [ 'class' => 'hidden' ] ),
				":BKTgl"        => FormField::dateInput( [ 'name' => 'BKTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BKTgl' ] ) ] ] ),
				":BKJam"        => FormField::timeInput( [ 'name' => 'BKJam', 'config' => [ 'value' => $dsTUang[ 'BKTgl' ] ] ] ),
				":TglCair"      => FormField::dateInput( [ 'name' => 'BKCekTempo', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BKCekTempo' ] ) ] ] ),
				":Kode"         => FormField::combo( 'NoAccount', [ 'name' => 'BankKode', 'config' => ['id' => 'NoAccount', 'value' => $dsTUang[ 'BankKode' ], 'data'=> \abengkel\models\Traccount::find()->bank() ] ] ),
				":Keterangan"   => Html::textInput( 'BKMemo', $dsTUang[ 'BKMemo' ], [ 'class' => 'form-control', 'maxlength' => '150' ] ),
				":Diterima"     => Html::textInput( 'BKPerson', $dsTUang[ 'BKPerson' ], [ 'class' => 'form-control', 'maxlength' => '50' ] ),
				":Nominal"      => FormField::numberInput( [ 'name' => 'BKNominal', 'config' => [ 'value' => $dsTUang[ 'BKNominal' ] ] ] ),
				":Total"        => FormField::numberInput( [ 'name' => 'BKTotal', 'config' => [ 'id' => 'BKTotal', ] ] ),
				":Cek"          => Html::textInput( 'BKCekNo', $dsTUang[ 'BKCekNo' ], [ 'class' => 'form-control' ] ),
				":AC"           => Html::textInput( 'BKAC', $dsTUang[ 'BKAC' ], [ 'class' => 'form-control' ] ),
				":JT"           => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control','readOnly'=>true ] ),
				":NoTrans"      => Html::textInput( 'BKLink', $dsTUang[ 'BKLink' ], [ 'class' => 'form-control', 'readonly' => true  ] ),
				":SupKode"      => FormField::combo( 'SupKode', [ 'config' => [ 'value' => $dsTUang[ 'SupKode' ], 'data' => $SupplierKodeCmb ], 'extraOptions' => [ 'altLabel' => [ 'SupNama' ] ] ] ),
				":Jenis"        => FormField::combo( 'BKJenis', [ 'config' => [ 'value' => $dsTUang[ 'BKJenis' ], 'data' => [
                                    'Tunai'        => 'Tunai',
                                    'Kartu Debit'  => 'Kartu Debit',
                                    'Kartu Kredit' => 'Kartu Kredit',
                                    'Transfer'     => 'Transfer', ] ],'options' => ['tabindex'=>'3'],  'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":Trans"        => FormField::combo( 'KodeTrans', [ 'config' => [ 'value' => $dsTUang[ 'KodeTrans' ],
                                    'data' => [
                                    'PI' => 'PI - Pembelian',
                                    'SR' => 'SR - Retur Penjualan', ]
                                    ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":BtnSearch"    => '<button type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>',
			],
            [
                'url_main'         => 'ttbkhd',
                'url_id'           => $id,
                'url_delete'        => Url::toRoute(['ttbkhd/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlAdd         = \abengkel\models\Ttbkhd::getRoute($dsTUang[ 'KodeTrans' ],['action'=>'create'] )[ 'create' ];
$urlIndex       = \abengkel\models\Ttbkhd::getRoute($dsTUang[ 'KodeTrans' ],[ 'id' => $id ] )[ 'index' ];
$urlDetail    = $url[ 'detail' ];
$readOnly     = ( $_GET['action'] == 'view' ? 'true' : 'false' );
$urlLoop      = Url::toRoute(['ttbkit/index']);
$urlPopup     = Url::toRoute( [ 'ttbkhd/order', 'KodeTrans' => $dsTUang[ 'KodeTrans' ]] );
switch ($dsTUang[ 'KodeTrans' ]){
    case 'PI':
        $title = 'Daftar Purchase Invoice untuk Bank Keluar Nomor : ';
        break;
    case 'SR':
        $title = 'Daftar Sales Retur untuk Bank Masuk Nomor : ';
        break;
}
$this->registerJsVar('__update', $_GET['action']);
$this->registerJs( <<< JS
     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 190,
        navButtonTambah: false,
        extraParams: {
            BKNo: $('input[name="BKNo"]').val()
        },
        postData: {
            BKNo: $('input[name="BKNo"]').val()
        },
        rownumbers: true,  
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,          
        colModel: [
            { template: 'actions'  },
            {
                name: 'BKLink',
                label: 'No',
                width: 135,
                editable: true,
                editor: {
                    type: 'text',
                    readOnly:true
                }
            },
            {
                name: 'TglLink',
                label: 'Tanggal',
                width: 100,
                editable: false,
            },
            {
                name: 'BKNo',
                label: 'Kode',
                editable: false,
                width: 150,
            },     
            {
                name: 'BiayaNama',
                label: 'Nama',
                editable: false,
                width: 150,
            },      
            {
                name: 'Total',
                label: 'Total',
                editable: false,
                width: 100,
                template: 'money',
                editor: {
                    readOnly:true
                }
            },   
            {
                name: 'Terbayar',
                label: 'Terbayar',
                editable: false,
                width: 100,
                template: 'money',
                editor: {
                    readOnly:true
                }
            },     
            {
                name: 'BKBayar',
                label: 'Bayar',
                width: 100,
                editable: true,
                template: 'money',
                editrules:{
                    custom:true, 
                    custom_func: function(value) {
                        var BKBayar = parseFloat(window.util.number.getValueOfEuropeanNumber(value));
                        var Total = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Total'+'"]').val()));
                        var Terbayar = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Terbayar'+'"]').val()));
                        if (BKBayar === 0) 
                            return [false,"Silahkan mengisi bayar."];  
                        else {       
                            var sisa = Total - Terbayar;
                            if (BKBayar > sisa){
                                $('[id="'+window.cmp.rowid+'_'+'BKBayar'+'"]').val(sisa);
                               return [false,"Nilai Bayar maksimal : " + window.util.number.format(sisa)];   
                            }else{
                                return [true,""];  
                            }
                        }
                    }
                }
            },
            {
                name: 'Sisa',
                label: 'Sisa',
                width: 100,
                editable: true,
                template: 'money',
                editor: {
                    readOnly:true
                }
            },
        ],      
        listeners: {
            afterLoad: function(grid, response) {
                let TotalBKBayar = $('#detailGrid').jqGrid('getCol','BKBayar',false,'sum');
                $('#BKTotal-disp').utilNumberControl().val(TotalBKBayar);
            },
        } 
      }).init().fit($('.box-body')).load({url: '$urlDetail'}); 

    $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
        window.cmp.rowid = rowid;    
    });

    $('#detailGrid').jqGrid('navButtonAdd',"#detailGrid-pager",{ 
        caption: ' Tambah',
        buttonicon: 'glyphicon glyphicon-plus',
        onClickButton:  function() {
            window.util.app.dialogListData.closeTriggerFromIframe = function(){ 
                var iframe = window.util.app.dialogListData.iframe();
                var mainGrid = iframe.contentWindow.jQuery('#jqGrid').jqGrid();
                window.util.app.dialogListData.gridFn.mainGrid = mainGrid;
                window.util.app.dialogListData.gridFn.var.multiSelect = true;
                var selRowId = window.util.app.dialogListData.gridFn.getSelectedRow();
                $.ajax({
                     type: "POST",
                     url: '$urlLoop',
                     data: {
                         oper: 'batch',
                         data: selRowId,
                         header: btoa($('#form_ttbkhd_id').serialize())
                     },
                     success: function(data) {
                        mainGrid.jqGrid().trigger('reloadGrid');
                     },
                   });
                return false;
            }
            window.util.app.dialogListData.show({
                title: '$title {$dsTUang[ "BKNoView" ]}',
                url: '$urlPopup'
            },function(){ 
                window.location.href = "{$url['update']}&oper=skip-load&BKNoView={$dsTUang['BKNoView']}";  
            });
        }, 
        position: "last", 
        title:"", 
        cursor: "pointer"
    });

	$('#btnSave').click(function (event) {	 
	    let Nominal = parseFloat($('#BKNominal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Nominal tidak boleh 0', size: 'small'});
               return;
        }
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttbkhd_id').attr('action','{$url['update']}');
        $('#form_ttbkhd_id').submit();
    });
	
	$('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  });
	
	$('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
	
	$('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  });
    
	$('[name=BKTgl]').utilDateControl().cmp().attr('tabindex', '2');
	
	//enable kode saat edit request 07/06/2024
	/*
	$( document ).ready(function() {
        if(__update == 'update'){
            $('#NoAccount').attr('disabled',true);
        }
    });*/
	
JS
);