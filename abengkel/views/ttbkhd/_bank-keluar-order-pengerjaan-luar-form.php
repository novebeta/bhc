<?php
use abengkel\assets\AppAsset;
use abengkel\components\FormField;
use abengkel\models\Tdsupplier;
use aunit\components\TUi;
use common\components\Custom;
use common\components\General;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttbkhd */
/* @var $form yii\widgets\ActiveForm */
$format          = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape          = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttbkhd-form">
		<?php
		$SupplierKodeCmb = ArrayHelper::map( Tdsupplier::find()->select( [ 'SupKode', 'SupNama' ] )->where( [ 'SupStatus' => 'A' or 'D' ] )->orderBy( 'SupStatus,SupKode' )->all(), 'SupKode', 'SupNama' );
		$form            = ActiveForm::begin( ['id' => 'form_ttbkhd_id','action' => $url[ 'update' ] ] );
        \abengkel\components\TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-16",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelBKNo" ],
						      [ 'class' => "col-sm-4", 'items' => ":BKNo" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelBKTgl" ],
						      [ 'class' => "col-sm-3", 'items' => ":BKTgl" ],
						      [ 'class' => "col-sm-4", 'items' => ":BKJam", 'style' => 'padding-left:1px;' ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelJenis" ],
						      [ 'class' => "col-sm-4", 'items' => ":Jenis" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-8",
					      'items' => [
						      [ 'class' => "col-sm-2" ],
						      [ 'class' => "col-sm-6", 'items' => ":LabelTrans" ],
						      [ 'class' => "col-sm-16", 'items' => ":Trans" ]
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-16",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelKode" ],
						      [ 'class' => "col-sm-21", 'items' => ":Kode" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-8",
					      'items' => [
						      [ 'class' => "col-sm-2" ],
						      [ 'class' => "col-sm-6", 'items' => ":LabelNominal" ],
						      [ 'class' => "col-sm-16", 'items' => ":Nominal" ],
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-16",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelNoTrans" ],
						      [ 'class' => "col-sm-8", 'items' => ":NoTrans" ],
						      [ 'class' => "col-sm-2", 'items' => ":BtnSearch" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelPolisi" ],
						      [ 'class' => "col-sm-9", 'items' => ":SupKode" ]
					      ],
					    ],
					    [ 'class' => "form-group col-md-8",
					      'items' => [
						      [ 'class' => "col-sm-2" ],
						      [ 'class' => "col-sm-6", 'items' => ":LabelDiterima" ],
						      [ 'class' => "col-sm-16", 'items' => ":Diterima" ],
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-16",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelAC" ],
						      [ 'class' => "col-sm-9", 'items' => ":AC" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelCek" ],
						      [ 'class' => "col-sm-9", 'items' => ":Cek" ]
					      ],
					    ],
					    [ 'class' => "form-group col-md-8",
					      'items' => [
						      [ 'class' => "col-sm-2" ],
						      [ 'class' => "col-sm-6", 'items' => ":LabelTglCair" ],
						      [ 'class' => "col-sm-16", 'items' => ":TglCair" ],
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelKeterangan" ],
					    [ 'class' => "col-sm-22", 'items' => ":Keterangan" ],
				    ],
				  ],
			  ],
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
                  ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                      'items'  => [
                          ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":JT"],
                          ['class' => "col-sm-4 pull-right", 'items' => ":LabelJT"],
                      ],
                  ],
                  [
                      'class' => "col-md-12 pull-left",
                      'items' => $this->render( '../_nav', [
                          'url'=> $_GET['r'],
                          'options'=> [
                              'BKNo'       => 'No BK',
                              'BKJenis'    => 'Jenis',
                              'KodeTrans'  => 'Kode Trans',
                              'PosKode'    => 'Pos Kode',
                              'SupKode'    => 'Sup Kode',
                              'BankKode'   => 'Bank Kode',
                              'BKAC'       => 'AC',
                              'BKPerson'   => 'Person',
                              'BKCekNo'    => 'No Cek',
                              'BKMemo'     => 'Memo',
                              'NoGL'       => 'No GL',
                              'Cetak'      => 'Cetak',
                              'UserID'     => 'User ID',
                          ],
                      ])
                  ],
                  ['class' => "col-md-12 pull-right",
				    'items' => [
					    [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
				    ]
				  ]
			  ]
			],
			[
				":LabelBKNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No BK</label>',
				":LabelBKTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl BK</label>',
				":LabelJenis"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				":LabelTrans"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Trans</label>',
				":LabelKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelNominal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
				":LabelDiterima"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Diterima Oleh</label>',
				":LabelTotal"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total</label>',
				":LabelKode"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode</label>',
				":LabelNoTrans"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Transaksi</label>',
				":LabelAC"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">A/C</label>',
				":LabelPolisi"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Supplier</label>',
				":LabelCek"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Cek</label>',
				":LabelTglCair"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Cair</label>',
				":LabelJT"         => \common\components\General::labelGL( $dsTUang[ 'NoGL' ], 'JT'),
				":BKNo"            => Html::textInput( 'BKNoView', $dsTUang[ 'BKNoView' ], [ 'class' => 'form-control','readonly' => 'readonly' ] ) .
                                      Html::textInput( 'BKNo', $dsTUang[ 'BKNo' ], [ 'class' => 'hidden' ] ),
				":BKTgl"           => FormField::dateInput( [ 'name' => 'BKTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BKTgl' ] ) ] ] ),
				":BKJam"           => FormField::timeInput( [ 'name' => 'BKJam', 'config' => [ 'value' => $dsTUang[ 'BKTgl' ] ] ] ),
				":TglCair"         => FormField::dateInput( [ 'name' => 'BKCekTempo', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BKCekTempo' ] ) ] ] ),
				":Kode"            => FormField::combo( 'NoAccount', [ 'name' => 'BankKode', 'config' => [ 'id' => 'NoAccount','value' => $dsTUang[ 'BankKode' ] , 'data'=> \abengkel\models\Traccount::find()->bank()]] ),
				":Keterangan"      => Html::textInput( 'BKMemo', $dsTUang[ 'BKMemo' ], [ 'class' => 'form-control', 'maxlength' => '150' ] ),
				":Diterima"        => Html::textInput( 'BKPerson', $dsTUang[ 'BKPerson' ], [ 'class' => 'form-control', 'maxlength' => '50' ] ),
				":Nominal"         => FormField::numberInput( [ 'name' => 'BKNominal', 'config' => [ 'value' => $dsTUang[ 'BKNominal' ] ] ] ),
				":Cek"             => Html::textInput( 'BKCekNo', $dsTUang[ 'BKCekNo' ], [ 'class' => 'form-control', 'maxlength' => '25' ] ),
				":AC"              => Html::textInput( 'BKAC', $dsTUang[ 'BKAC' ], [ 'class' => 'form-control', 'maxlength' => '25' ] ),
				":JT"              => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control','readOnly'=>true ] ),
				":NoTrans"         => Html::textInput( 'BKLink', $dsTUang[ 'BKLink' ], [ 'class' => 'form-control', 'readonly' => true  ] ),
				":SupKode"         => FormField::combo( 'SupKode', [ 'config' => [ 'value' => $dsTUang[ 'SupKode' ], 'data' => $SupplierKodeCmb ], 'extraOptions' => [ 'altLabel' => [ 'SupNama' ] ] ] ),
				":Jenis"           => FormField::combo( 'BKJenis', [ 'config' => [ 'value' => $dsTUang[ 'BKJenis' ], 'data' => [
                                        'Tunai'        => 'Tunai',
                                        'Kartu Debit'  => 'Kartu Debit',
                                        'Kartu Kredit' => 'Kartu Kredit',
                                        'Transfer'     => 'Transfer', ] ], 'extraOptions'         => [ 'simpleCombo' => true ] ] ),
				":Trans"           => FormField::combo( 'KodeTrans', [ 'config' => [ 'value' => $dsTUang[ 'KodeTrans' ], 'data' => ['PL' => 'PL - Pengerjaan Luar', ] ], 'extraOptions'              => [ 'simpleCombo' => true ] ] ),
				":BtnSearch"       => '<button type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>',
			],
            [
                'url_main'         => 'ttbkhd',
                'url_id'           => $id,
                'url_delete'        => Url::toRoute(['ttbkhd/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlAdd         = \abengkel\models\Ttbkhd::getRoute('PL',['action'=>'create'] )[ 'create' ];
$urlIndex       = \abengkel\models\Ttbkhd::getRoute('PL',[ 'id' => $id ] )[ 'index' ];
$this->registerJsVar('__update', $_GET['action']);
$this->registerJs( <<< JS
	
	$('#btnSave').click(function (event) {	      
	    let Nominal = parseFloat($('#BKNominal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Nominal tidak boleh 0', size: 'small'});
               return;
        }
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttbkhd_id').attr('action','{$url['update']}');
        $('#form_ttbkhd_id').submit();
    }); 

    $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  });
	
	$('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
	
	$('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  });
	//enable kode saat edit request 07/06/2024
	/*
	$( document ).ready(function() {
        if(__update == 'update'){
            $('#NoAccount').attr('disabled',true);
        }
    });*/

JS
);

