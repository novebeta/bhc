<?php
use abengkel\assets\AppAsset;
use abengkel\models\Ttbkhd;
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = 'Transfer Antar Bank';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'BKNo'      => 'No BK',
		'BKJenis'   => 'Jenis',
		'KodeTrans' => 'Kode Trans',
		'PosKode'   => 'Pos Kode',
		'SupKode'   => 'Sup Kode',
		'BankKode'  => 'Bank Kode',
		'BKAC'      => 'AC',
		'BKPerson'  => 'Person',
		'BKCekNo'   => 'No Cek',
		'BKMemo'    => 'Memo',
		'NoGL'      => 'No GL',
		'Cetak'     => 'Cetak',
		'UserID'    => 'User ID',
	],
	'cmbTgl'    => [
		'BKTgl'      => 'Tanggal BK',
		'BKCekTempo' => 'Cek Tempo',
	],
	'cmbNum'    => [
		'BKNominal' => 'Nominal',
	],
	'sortname'  => "BKTgl",
	'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \abengkel\components\TdUi::mainGridJs( \abengkel\models\Ttbkhd::className(), 'Transfer Antar Bank', [
	'url_add'    => \yii\helpers\Url::toRoute([ 'ttbkhd/transfer-antar-bank-create','action'=>'UpdateAdd']),
	'url_update' => \yii\helpers\Url::toRoute([ 'ttbkhd/transfer-antar-bank-update','action'=>'UpdateEdit']),
	'url_delete' => \yii\helpers\Url::toRoute(['ttbkhd/delete' ]),
	'colGrid'    => Ttbkhd::colGridTransferBank()
] ), \yii\web\View::POS_READY );
