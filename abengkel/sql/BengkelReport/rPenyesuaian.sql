DELIMITER $$
DROP PROCEDURE IF EXISTS `rPenyesuaian`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rPenyesuaian`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,
IN xTC VARCHAR(100),
IN xSort VARCHAR(150),IN xOrder VARCHAR(10))
BEGIN
/*CALL rPenyesuaian('Header1','2020-01-01','2020-05-31','','TITgl','ASC');*/
	IF xTipe = "Header1" THEN
		SET @MyQuery = CONCAT("
		SELECT Cetak, KodeTrans, LokasiKode, NoGL, PosKode, TAKeterangan, TANo, TATgl, TATotal, UserID
      FROM  tttahd
      WHERE DATE(tttahd.TATgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery," AND tttahd.KodeTrans LIKE '%" , xTC , "' ");
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
	END IF;
	IF xTipe = "Item1" THEN
		SET @MyQuery = CONCAT("
		SELECT tttait.TANo, tttait.TAAuto, tttait.BrgKode, tttait.TAQty, tttait.TAHrgBeli, tdbarang.BrgNama, tdbarang.BrgSatuan, tttait.TAQty * tttait.TAHrgBeli AS Jumlah, tttait.LokasiKode, tdbarang.BrgGroup, tdbarang.BrgRak1, tttahd.TATgl
      FROM tttait 
      INNER JOIN tdbarang ON tttait.BrgKode = tdbarang.BrgKode 
      INNER JOIN tttahd ON tttait.TANo = tttahd.TANo
      WHERE DATE(tttahd.TATgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery," AND tttahd.KodeTrans LIKE '%" , xTC , "' ");
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY tttait.TANo, tttait.TAAuto, " ,xSort," ",xOrder );
	END IF;
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$
DELIMITER ;

