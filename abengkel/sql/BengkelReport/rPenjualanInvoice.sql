DELIMITER $$
DROP PROCEDURE IF EXISTS `rPenjualanInvoice`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rPenjualanInvoice`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,IN xTC VARCHAR(30),IN xPos VARCHAR(30),IN xStatus VARCHAR(250),IN xSort VARCHAR(20),IN xOrder VARCHAR(10))
BEGIN
/*
	Cara Akses SP :
	CALL rPenjualanInvoice('Header1','2020-01-01','2020-01-31','','','','SITgl','ASC');
*/
	   SET xTC = CONCAT("%",xTC);
	   SET xPOS = CONCAT("%",xPOS);
	   IF xTipe = "Header1" THEN
	      SET @MyQuery = CONCAT("SELECT ttsihd.SINo, ttsihd.SITgl, ttsihd.SONo, ttsihd.SIJenis, ttsihd.PosKode, ttsihd.KodeTrans, ttsihd.KarKode, ttsihd.PrgNama, ttsihd.MotorNoPolisi, ttsihd.LokasiKode, ttsihd.SISubTotal, ttsihd.SIDiscFinal, ttsihd.SITotalPajak, 
                         ttsihd.SIBiayaKirim, ttsihd.SIUangMuka, ttsihd.SITotal, ttsihd.SITOP, ttsihd.SITerbayar, ttsihd.SIKeterangan, ttsihd.NoGL, ttsihd.Cetak, ttsihd.UserID, tdkaryawan.KarNama, tdcustomer.CusNama, tdcustomer.MotorWarna, 
                         tdmotortype.MotorNama, IFNULL(ttsohd.SOTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SOTgl, ttsihd.DealerKode, tddealer.DealerNama,ttsihd.KarKode 
								 FROM ttsihd INNER JOIN
                         tdkaryawan ON ttsihd.KarKode = tdkaryawan.KarKode INNER JOIN
                         tddealer ON ttsihd.DealerKode = tddealer.DealerKode LEFT OUTER JOIN
                         ttsohd ON ttsihd.SONo = ttsohd.SONo LEFT OUTER JOIN
                         tdcustomer ON ttsihd.MotorNoPolisi = tdcustomer.MotorNoPolisi LEFT OUTER JOIN
                         tdmotortype ON tdcustomer.MotorType = tdmotortype.MotorType
                         WHERE ttsihd.SITgl BETWEEN '",xTgl1,"' AND '",xTgl2,"'",
                         "AND ttsihd.KodeTrans LIKE '",xTC,"' ",
                         "AND ttsihd.PosKode LIKE '",xPOS,"' ", xStatus,
                         "ORDER BY ",xSort," ",xOrder);
		END IF;
	   IF xTipe = "Item1" THEN
	      SET @MyQuery = CONCAT("SELECT ttsiit.SINo, ttsiit.SIAuto, ttsiit.BrgKode, ttsiit.SIQty, ttsiit.SIQty * ttsiit.SIHrgBeli AS SIHrgBeli, ttsiit.SIHrgJual, ttsiit.SIDiscount, ttsiit.SIPajak, tdbarang.BrgNama, tdbarang.BrgSatuan, ttsiit.SIDiscount / (ttsiit.SIHrgJual * ttsiit.SIQty) * 100 AS Disc, 
                         ttsiit.SIQty * ttsiit.SIHrgJual  AS Jumlah, ttsiit.LokasiKode, tdbarang.BrgGroup, ttsiit.SIPickingNo, ttsiit.SIPickingDate, ttsiit.SONo, ttsiit.Cetak, tdbarang.BrgRak1, ttsihd.SITgl, ttsihd.KarKode, 
                         tdcustomer.MotorNoPolisi, tdcustomer.CusNama, ttsiit.SIQty * (ttsiit.SIHrgJual - ttsiit.SIHrgBeli) - ttsiit.SIDiscount AS Laba
								 FROM ttsiit INNER JOIN
                         tdbarang ON ttsiit.BrgKode = tdbarang.BrgKode INNER JOIN
                         ttsihd ON ttsiit.SINo = ttsihd.SINo INNER JOIN
                         tdcustomer ON ttsihd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                         WHERE ttsihd.SITgl BETWEEN '",xTgl1,"' AND '",xTgl2,"'",
                         "AND ttsihd.KodeTrans LIKE '",xTC,"' ",
                         "AND ttsihd.PosKode LIKE '",xPOS,"' ", xStatus,
                         "ORDER BY ",xSort," ",xOrder);
	   END IF;
	   IF xTipe = "Item2" THEN
	      SET @MyQuery = CONCAT("SELECT ttsiit.SINo, ttsiit.SIAuto, ttsiit.BrgKode, ttsiit.SIQty, ttsiit.SIQty * ttsiit.SIHrgBeli AS SIHrgBeli, ttsiit.SIHrgJual, ttsiit.SIDiscount, ttsiit.SIPajak, tdbarang.BrgNama, tdbarang.BrgSatuan, ttsiit.SIDiscount / (ttsiit.SIHrgJual * ttsiit.SIQty) * 100 AS Disc, 
                         ttsiit.SIQty * ttsiit.SIHrgJual AS Jumlah, ttsiit.LokasiKode, tdbarang.BrgGroup, ttsiit.SIPickingNo, ttsiit.SIPickingDate, ttsiit.SONo, ttsiit.Cetak, tdbarang.BrgRak1, ttsihd.SITgl, ttsihd.KarKode, 
                         tdcustomer.MotorNoPolisi, tdcustomer.CusNama, ttsiit.SIQty * (ttsiit.SIHrgJual - ttsiit.SIHrgBeli) - ttsiit.SIDiscount AS Laba
								 FROM ttsiit INNER JOIN
                         tdbarang ON ttsiit.BrgKode = tdbarang.BrgKode INNER JOIN
                         ttsihd ON ttsiit.SINo = ttsihd.SINo INNER JOIN
                         tdcustomer ON ttsihd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                         WHERE ttsihd.SITgl BETWEEN '",xTgl1,"' AND '",xTgl2,"'",
                         "AND ttsihd.KodeTrans LIKE '",xTC,"' ",
                         "AND ttsihd.PosKode LIKE '",xPOS,"' ", xStatus,
                         "ORDER BY ",xSort," ",xOrder);
	   END IF;
	   IF xTipe = "Item3" THEN
	      SET @MyQuery = CONCAT("SELECT ttsiit.SINo, ttsiit.SIAuto, ttsiit.BrgKode, ttsiit.SIQty, ttsiit.SIQty * ttsiit.SIHrgBeli AS SIHrgBeli, ttsiit.SIHrgJual, ttsiit.SIDiscount, ttsiit.SIPajak, tdbarang.BrgNama, tdbarang.BrgSatuan, ttsiit.SIDiscount / (ttsiit.SIHrgJual * ttsiit.SIQty) * 100 AS Disc, 
                         ttsiit.SIQty * ttsiit.SIHrgJual AS Jumlah, ttsiit.LokasiKode, tdbarang.BrgGroup, ttsiit.SIPickingNo, ttsiit.SIPickingDate, ttsiit.SONo, ttsiit.Cetak, tdbarang.BrgRak1, ttsihd.SITgl, ttsihd.KarKode, 
                         tdcustomer.MotorNoPolisi, tdcustomer.CusNama, ttsiit.SIQty * (ttsiit.SIHrgJual - ttsiit.SIHrgBeli) - ttsiit.SIDiscount AS Laba
								 FROM ttsiit INNER JOIN
                         tdbarang ON ttsiit.BrgKode = tdbarang.BrgKode INNER JOIN
                         ttsihd ON ttsiit.SINo = ttsihd.SINo INNER JOIN
                         tdcustomer ON ttsihd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                         WHERE ttsihd.SITgl BETWEEN '",xTgl1,"' AND '",xTgl2,"'",
                         "AND ttsihd.KodeTrans LIKE '",xTC,"' ",
                         "AND ttsihd.PosKode LIKE '",xPOS,"' ", xStatus,
                         "ORDER BY ",xSort," ",xOrder);
      END IF;
      PREPARE STMT FROM @MyQuery;
      EXECUTE Stmt;
	END$$
DELIMITER ;

