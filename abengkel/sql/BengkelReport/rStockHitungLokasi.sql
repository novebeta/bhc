DELIMITER $$
DROP PROCEDURE IF EXISTS `rStockHitungLokasi`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rStockHitungLokasi`(IN MyBrgKode VARCHAR(20), IN MyBrgStatus VARCHAR(1), IN MyBrgGroup VARCHAR(15), IN MyLokasi VARCHAR(15),
IN  TglAwal DATE, IN TglAkhir DATE )
BEGIN
DECLARE TglAwalSaldo DATE;
DECLARE TglAwalSaldoTransaksi DATE;
DECLARE TglAkhirSaldo DATE;
DECLARE Tanggal DATE;
SET Tanggal = '1900-01-01';
SELECT IFNULL(TglSaldo, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) INTO Tanggal FROM tsaldostock WHERE TglSaldo < TglAwal  ORDER BY TglSaldo DESC ;
SET TglAwalSaldo = Tanggal;
SET TglAwalSaldoTransaksi = DATE_ADD(Tanggal, INTERVAL 1 DAY);
SET TglAkhirSaldo = DATE_ADD(TglAwal, INTERVAL -1 DAY);
DROP TEMPORARY TABLE IF EXISTS `StockBarang`;
CREATE TEMPORARY TABLE IF NOT EXISTS `StockBarang` AS
SELECT tdbarang.BrgKode,LokasiKode, BrgBarCode, BrgNama, BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual, BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus,
0 AS SD, 0 AS SI, 0 AS SR, 0 AS PS, 0 AS PR, 0 AS TIPlus, 0 AS TIMin, 0 AS TA, 0 AS SaldoAwal, 0 AS SaldoAkhir
FROM tdbaranglokasi LEFT OUTER JOIN tdbarang ON tdbaranglokasi.BrgKode = tdbarang.BrgKode
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND
BrgStatus LIKE CONCAT('%',MyBrgStatus) AND
BrgGroup LIKE CONCAT('%',MyBrgGroup) AND
LokasiKode LIKE CONCAT('%',MyLokasi) 
ORDER BY BrgGroup,BrgKode,tdbaranglokasi.LokasiKode;

DROP TEMPORARY TABLE IF EXISTS `SAwal`;
CREATE TEMPORARY TABLE IF NOT EXISTS `SAwal` AS
SELECT tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode,  IFNULL(MyGroup.MySUM, 0) AS MySum FROM tdbaranglokasi
LEFT OUTER JOIN
(SELECT BrgKode, LokasiKode, Saldo AS MySum FROM tsaldostock
WHERE LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(TglSaldo) BETWEEN TglAwalSaldo AND TglAwalSaldo) MyGroup
ON tdbaranglokasi.BrgKode = MyGroup.BrgKode  AND tdbaranglokasi.LokasiKode = MyGroup.LokasiKode
LEFT OUTER  JOIN tdbarang ON tdbarang.BrgKode = tdbaranglokasi.BrgKode
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus) AND tdbaranglokasi.LokasiKode LIKE CONCAT('%',MyLokasi) 
ORDER BY tdbarang.BrgGroup, tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode;
CREATE INDEX BrgKode ON SAwal(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `tvsumbrglokasi`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrglokasi` AS
SELECT IFNULL(ttsditbarang.BrgKode,'NULL') AS BrgKode,ttsditbarang.LokasiKode, SUM(ttsditbarang.SDQty) AS MySum FROM ttsdhd
INNER JOIN ttsditbarang ON ttsdhd.SDNo = ttsditbarang.SDNo
WHERE ttsditbarang.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(SDTgl) BETWEEN TglAwalSaldoTransaksi AND TglAkhirSaldo
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttsditbarang.BrgKode,'NULL'), ttsditbarang.LokasiKode;
CREATE INDEX BrgKode ON tvsumbrglokasi(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `SD`;
CREATE TEMPORARY TABLE IF NOT EXISTS `SD` AS
SELECT tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode, IFNULL(tvsumbrglokasi.MySUM, 0) AS MySum FROM tdbaranglokasi
LEFT OUTER JOIN tvsumbrglokasi
ON tdbaranglokasi.BrgKode = tvsumbrglokasi.BrgKode AND tdbaranglokasi.LokasiKode = tvsumbrglokasi.LokasiKode
LEFT OUTER  JOIN tdbarang ON tdbarang.BrgKode = tdbaranglokasi.BrgKode
WHERE tdbaranglokasi.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus) AND tdbaranglokasi.LokasiKode LIKE CONCAT('%',MyLokasi)
ORDER BY tdbarang.BrgGroup, tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode;
CREATE INDEX BrgKode ON SD(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `tvsumbrglokasi`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrglokasi` AS
SELECT IFNULL(ttsiit.BrgKode,'NULL') AS BrgKode,ttsiit.LokasiKode, SUM(ttsiit.SIQty) AS MySum FROM ttsihd
INNER JOIN ttsiit ON ttsihd.SINo = ttsiit.SINo
WHERE ttsiit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(SITgl) BETWEEN TglAwalSaldoTransaksi AND TglAkhirSaldo
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttsiit.BrgKode,'NULL'),ttsiit.LokasiKode;
CREATE INDEX BrgKode ON tvsumbrglokasi(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `SI`;
CREATE TEMPORARY TABLE IF NOT EXISTS `SI` AS
SELECT tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode, IFNULL(tvsumbrglokasi.MySUM, 0) AS MySum FROM tdbaranglokasi
LEFT OUTER JOIN tvsumbrglokasi
ON tdbaranglokasi.BrgKode = tvsumbrglokasi.BrgKode AND tdbaranglokasi.LokasiKode = tvsumbrglokasi.LokasiKode
LEFT OUTER  JOIN tdbarang ON tdbarang.BrgKode = tdbaranglokasi.BrgKode
WHERE tdbaranglokasi.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus) AND tdbaranglokasi.LokasiKode LIKE CONCAT('%',MyLokasi)
ORDER BY tdbarang.BrgGroup, tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode;
CREATE INDEX BrgKode ON SI(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `tvsumbrglokasi`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrglokasi` AS
SELECT IFNULL(ttPRit.BrgKode,'NULL') AS BrgKode,ttPRit.LokasiKode, SUM(ttPRit.PRQty) AS MySum FROM ttPRhd
INNER JOIN ttPRit ON ttPRhd.PRNo = ttPRit.PRNo
WHERE ttPRit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(PRTgl) BETWEEN TglAwalSaldoTransaksi AND TglAkhirSaldo
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttPRit.BrgKode,'NULL'),ttPRit.LokasiKode;
CREATE INDEX BrgKode ON tvsumbrglokasi(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `PR`;
CREATE TEMPORARY TABLE IF NOT EXISTS `PR` AS
SELECT tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode, IFNULL(tvsumbrglokasi.MySUM, 0) AS MySum FROM tdbaranglokasi
LEFT OUTER JOIN tvsumbrglokasi
ON tdbaranglokasi.BrgKode = tvsumbrglokasi.BrgKode AND tdbaranglokasi.LokasiKode = tvsumbrglokasi.LokasiKode
LEFT OUTER  JOIN tdbarang ON tdbarang.BrgKode = tdbaranglokasi.BrgKode
WHERE tdbaranglokasi.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus) AND tdbaranglokasi.LokasiKode LIKE CONCAT('%',MyLokasi)
ORDER BY tdbarang.BrgGroup, tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode;
CREATE INDEX BrgKode ON PR(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `tvsumbrglokasi`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrglokasi` AS
SELECT IFNULL(ttpsit.BrgKode,'NULL') AS BrgKode,ttpsit.LokasiKode, SUM(ttpsit.psQty) AS MySum FROM ttpshd
INNER JOIN ttpsit ON ttpshd.psNo = ttpsit.psNo
WHERE ttpsit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(psTgl) BETWEEN TglAwalSaldoTransaksi AND TglAkhirSaldo
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttpsit.BrgKode,'NULL'),ttpsit.LokasiKode;
CREATE INDEX BrgKode ON tvsumbrglokasi(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `PS`;
CREATE TEMPORARY TABLE IF NOT EXISTS `PS` AS
SELECT tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode, IFNULL(tvsumbrglokasi.MySUM, 0) AS MySum FROM tdbaranglokasi
LEFT OUTER JOIN tvsumbrglokasi
ON tdbaranglokasi.BrgKode = tvsumbrglokasi.BrgKode AND tdbaranglokasi.LokasiKode = tvsumbrglokasi.LokasiKode
LEFT OUTER  JOIN tdbarang ON tdbarang.BrgKode = tdbaranglokasi.BrgKode
WHERE tdbaranglokasi.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus) AND tdbaranglokasi.LokasiKode LIKE CONCAT('%',MyLokasi)
ORDER BY tdbarang.BrgGroup, tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode;
CREATE INDEX BrgKode ON PS(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `tvsumbrglokasi`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrglokasi` AS
SELECT IFNULL(ttSRit.BrgKode,'NULL') AS BrgKode, ttSRit.LokasiKode, SUM(ttSRit.SRQty) AS MySum FROM ttSRhd
INNER JOIN ttSRit ON ttSRhd.SRNo = ttSRit.SRNo
WHERE ttSRit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(SRTgl) BETWEEN TglAwalSaldoTransaksi AND TglAkhirSaldo
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttSRit.BrgKode,'NULL'), ttSRit.LokasiKode;
CREATE INDEX BrgKode ON tvsumbrglokasi(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `SR`;
CREATE TEMPORARY TABLE IF NOT EXISTS `SR` AS
SELECT tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode, IFNULL(tvsumbrglokasi.MySUM, 0) AS MySum FROM tdbaranglokasi
LEFT OUTER JOIN tvsumbrglokasi
ON tdbaranglokasi.BrgKode = tvsumbrglokasi.BrgKode AND tdbaranglokasi.LokasiKode = tvsumbrglokasi.LokasiKode
LEFT OUTER  JOIN tdbarang ON tdbarang.BrgKode = tdbaranglokasi.BrgKode
WHERE tdbaranglokasi.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus) AND tdbaranglokasi.LokasiKode LIKE CONCAT('%',MyLokasi)
ORDER BY tdbarang.BrgGroup, tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode;
CREATE INDEX BrgKode ON SR(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `tvsumbrglokasi`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrglokasi` AS
SELECT IFNULL(ttTIit.BrgKode,'NULL') AS BrgKode,ttTIit.LokasiTujuan AS LokasiKode,  SUM(ttTIit.TIQty) AS MySum FROM ttTIhd
INNER JOIN ttTIit ON ttTIhd.TINo = ttTIit.TINo
WHERE ttTIit.LokasiTujuan LIKE CONCAT('%',MyLokasi) AND DATE(TITgl) BETWEEN TglAwalSaldoTransaksi AND TglAkhirSaldo
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttTIit.BrgKode,'NULL'),ttTIit.LokasiTujuan;
CREATE INDEX BrgKode ON tvsumbrglokasi(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `TIPlus`;
CREATE TEMPORARY TABLE IF NOT EXISTS `TIPlus` AS
SELECT tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode, IFNULL(tvsumbrglokasi.MySUM, 0) AS MySum FROM tdbaranglokasi
LEFT OUTER JOIN tvsumbrglokasi
ON tdbaranglokasi.BrgKode = tvsumbrglokasi.BrgKode AND tdbaranglokasi.LokasiKode = tvsumbrglokasi.LokasiKode
LEFT OUTER  JOIN tdbarang ON tdbarang.BrgKode = tdbaranglokasi.BrgKode
WHERE tdbaranglokasi.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus) AND tdbaranglokasi.LokasiKode LIKE CONCAT('%',MyLokasi)
ORDER BY tdbarang.BrgGroup, tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode;
CREATE INDEX BrgKode ON TIPlus(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `tvsumbrglokasi`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrglokasi` AS
SELECT IFNULL(ttTIit.BrgKode,'NULL') AS BrgKode,ttTIit.LokasiAsal AS LokasiKode, SUM(ttTIit.TIQty) AS MySum FROM ttTIhd
INNER JOIN ttTIit ON ttTIhd.TINo = ttTIit.TINo
WHERE ttTIit.LokasiAsal LIKE CONCAT('%',MyLokasi) AND DATE(TITgl) BETWEEN TglAwalSaldoTransaksi AND TglAkhirSaldo
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttTIit.BrgKode,'NULL'),ttTIit.LokasiAsal;
CREATE INDEX BrgKode ON tvsumbrglokasi(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `TIMin`;
CREATE TEMPORARY TABLE IF NOT EXISTS `TIMin` AS
SELECT tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode, IFNULL(tvsumbrglokasi.MySUM, 0) AS MySum FROM tdbaranglokasi
LEFT OUTER JOIN tvsumbrglokasi
ON tdbaranglokasi.BrgKode = tvsumbrglokasi.BrgKode AND tdbaranglokasi.LokasiKode = tvsumbrglokasi.LokasiKode
LEFT OUTER  JOIN tdbarang ON tdbarang.BrgKode = tdbaranglokasi.BrgKode
WHERE tdbaranglokasi.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus) AND tdbaranglokasi.LokasiKode LIKE CONCAT('%',MyLokasi)
ORDER BY tdbarang.BrgGroup, tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode;
CREATE INDEX BrgKode ON TIMin(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `tvsumbrglokasi`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrglokasi` AS
SELECT IFNULL(ttTAit.BrgKode,'NULL') AS BrgKode, ttTAit.LokasiKode, SUM(ttTAit.TAQty) AS MySum FROM ttTAhd
INNER JOIN ttTAit ON ttTAhd.TANo = ttTAit.TANo
WHERE ttTAit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(TATgl) BETWEEN TglAwalSaldoTransaksi AND TglAkhirSaldo
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttTAit.BrgKode,'NULL'), ttTAit.LokasiKode;
CREATE INDEX BrgKode ON tvsumbrglokasi(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `TA`;
CREATE TEMPORARY TABLE IF NOT EXISTS `TA` AS
SELECT tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode, IFNULL(tvsumbrglokasi.MySUM, 0) AS MySum FROM tdbaranglokasi
LEFT OUTER JOIN tvsumbrglokasi
ON tdbaranglokasi.BrgKode = tvsumbrglokasi.BrgKode AND tdbaranglokasi.LokasiKode = tvsumbrglokasi.LokasiKode
LEFT OUTER  JOIN tdbarang ON tdbarang.BrgKode = tdbaranglokasi.BrgKode
WHERE tdbaranglokasi.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus) AND tdbaranglokasi.LokasiKode LIKE CONCAT('%',MyLokasi)
ORDER BY tdbarang.BrgGroup, tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode;
CREATE INDEX BrgKode ON TA(BrgKode);
UPDATE StockBarang INNER JOIN SAwal ON SAwal.BrgKode = StockBarang.BrgKode AND SAwal.LokasiKode = StockBarang.LokasiKode SET  SaldoAwal = MySum ;
UPDATE StockBarang INNER JOIN SD ON SD.BrgKode = StockBarang.BrgKode AND SD.LokasiKode = StockBarang.LokasiKode SET  SD = MySum ;
UPDATE StockBarang INNER JOIN SI ON SI.BrgKode = StockBarang.BrgKode AND SI.LokasiKode = StockBarang.LokasiKode SET  SI = MySum ;
UPDATE StockBarang INNER JOIN SR ON SR.BrgKode = StockBarang.BrgKode AND SR.LokasiKode = StockBarang.LokasiKode SET  SR = MySum ;
UPDATE StockBarang INNER JOIN PS ON PS.BrgKode = StockBarang.BrgKode AND PS.LokasiKode = StockBarang.LokasiKode SET  PS = MySum ;
UPDATE StockBarang INNER JOIN PR ON PR.BrgKode = StockBarang.BrgKode AND PR.LokasiKode = StockBarang.LokasiKode SET  PR = MySum ;
UPDATE StockBarang INNER JOIN TA ON TA.BrgKode = StockBarang.BrgKode AND TA.LokasiKode = StockBarang.LokasiKode SET  TA = MySum ;
UPDATE StockBarang INNER JOIN TIPlus ON TIPlus.BrgKode = StockBarang.BrgKode AND TIPlus.LokasiKode = StockBarang.LokasiKode SET  TIPlus = MySum ;
UPDATE StockBarang INNER JOIN TIMin ON TIMin.BrgKode = StockBarang.BrgKode AND TIMin.LokasiKode = StockBarang.LokasiKode SET  TIMin = MySum ;
UPDATE StockBarang SET SaldoAwal = SaldoAwal + PS + SR + TIPlus + TA - SD - SI - PR - TIMin;

DROP TEMPORARY TABLE IF EXISTS `tvsumbrglokasi`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrglokasi` AS
SELECT IFNULL(ttsditbarang.BrgKode,'NULL') AS BrgKode,ttsditbarang.LokasiKode, SUM(ttsditbarang.SDQty) AS MySum FROM ttsdhd
INNER JOIN ttsditbarang ON ttsdhd.SDNo = ttsditbarang.SDNo
WHERE ttsditbarang.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(SDTgl) BETWEEN TglAwal AND TglAkhir
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttsditbarang.BrgKode,'NULL'), ttsditbarang.LokasiKode;
CREATE INDEX BrgKode ON tvsumbrglokasi(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `SD`;
CREATE TEMPORARY TABLE IF NOT EXISTS `SD` AS
SELECT tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode, IFNULL(tvsumbrglokasi.MySUM, 0) AS MySum FROM tdbaranglokasi
LEFT OUTER JOIN tvsumbrglokasi
ON tdbaranglokasi.BrgKode = tvsumbrglokasi.BrgKode AND tdbaranglokasi.LokasiKode = tvsumbrglokasi.LokasiKode
LEFT OUTER  JOIN tdbarang ON tdbarang.BrgKode = tdbaranglokasi.BrgKode
WHERE tdbaranglokasi.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus) AND tdbaranglokasi.LokasiKode LIKE CONCAT('%',MyLokasi)
ORDER BY tdbarang.BrgGroup, tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode;
CREATE INDEX BrgKode ON SD(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `tvsumbrglokasi`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrglokasi` AS
SELECT IFNULL(ttsiit.BrgKode,'NULL') AS BrgKode,ttsiit.LokasiKode, SUM(ttsiit.SIQty) AS MySum FROM ttsihd
INNER JOIN ttsiit ON ttsihd.SINo = ttsiit.SINo
WHERE ttsiit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(SITgl) BETWEEN TglAwal AND TglAkhir
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttsiit.BrgKode,'NULL'),ttsiit.LokasiKode;
CREATE INDEX BrgKode ON tvsumbrglokasi(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `SI`;
CREATE TEMPORARY TABLE IF NOT EXISTS `SI` AS
SELECT tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode, IFNULL(tvsumbrglokasi.MySUM, 0) AS MySum FROM tdbaranglokasi
LEFT OUTER JOIN tvsumbrglokasi
ON tdbaranglokasi.BrgKode = tvsumbrglokasi.BrgKode AND tdbaranglokasi.LokasiKode = tvsumbrglokasi.LokasiKode
LEFT OUTER  JOIN tdbarang ON tdbarang.BrgKode = tdbaranglokasi.BrgKode
WHERE tdbaranglokasi.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus) AND tdbaranglokasi.LokasiKode LIKE CONCAT('%',MyLokasi)
ORDER BY tdbarang.BrgGroup, tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode;
CREATE INDEX BrgKode ON SI(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `tvsumbrglokasi`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrglokasi` AS
SELECT IFNULL(ttPRit.BrgKode,'NULL') AS BrgKode,ttPRit.LokasiKode, SUM(ttPRit.PRQty) AS MySum FROM ttPRhd
INNER JOIN ttPRit ON ttPRhd.PRNo = ttPRit.PRNo
WHERE ttPRit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(PRTgl) BETWEEN TglAwal AND TglAkhir
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttPRit.BrgKode,'NULL'),ttPRit.LokasiKode;
CREATE INDEX BrgKode ON tvsumbrglokasi(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `PR`;
CREATE TEMPORARY TABLE IF NOT EXISTS `PR` AS
SELECT tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode, IFNULL(tvsumbrglokasi.MySUM, 0) AS MySum FROM tdbaranglokasi
LEFT OUTER JOIN tvsumbrglokasi
ON tdbaranglokasi.BrgKode = tvsumbrglokasi.BrgKode AND tdbaranglokasi.LokasiKode = tvsumbrglokasi.LokasiKode
LEFT OUTER  JOIN tdbarang ON tdbarang.BrgKode = tdbaranglokasi.BrgKode
WHERE tdbaranglokasi.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus) AND tdbaranglokasi.LokasiKode LIKE CONCAT('%',MyLokasi)
ORDER BY tdbarang.BrgGroup, tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode;
CREATE INDEX BrgKode ON PR(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `tvsumbrglokasi`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrglokasi` AS
SELECT IFNULL(ttpsit.BrgKode,'NULL') AS BrgKode,ttpsit.LokasiKode, SUM(ttpsit.psQty) AS MySum FROM ttpshd
INNER JOIN ttpsit ON ttpshd.psNo = ttpsit.psNo
WHERE ttpsit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(psTgl) BETWEEN TglAwal AND TglAkhir
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttpsit.BrgKode,'NULL'),ttpsit.LokasiKode;
CREATE INDEX BrgKode ON tvsumbrglokasi(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `PS`;
CREATE TEMPORARY TABLE IF NOT EXISTS `PS` AS
SELECT tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode, IFNULL(tvsumbrglokasi.MySUM, 0) AS MySum FROM tdbaranglokasi
LEFT OUTER JOIN tvsumbrglokasi
ON tdbaranglokasi.BrgKode = tvsumbrglokasi.BrgKode AND tdbaranglokasi.LokasiKode = tvsumbrglokasi.LokasiKode
LEFT OUTER  JOIN tdbarang ON tdbarang.BrgKode = tdbaranglokasi.BrgKode
WHERE tdbaranglokasi.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus) AND tdbaranglokasi.LokasiKode LIKE CONCAT('%',MyLokasi)
ORDER BY tdbarang.BrgGroup, tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode;
CREATE INDEX BrgKode ON PS(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `tvsumbrglokasi`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrglokasi` AS
SELECT IFNULL(ttSRit.BrgKode,'NULL') AS BrgKode, ttSRit.LokasiKode, SUM(ttSRit.SRQty) AS MySum FROM ttSRhd
INNER JOIN ttSRit ON ttSRhd.SRNo = ttSRit.SRNo
WHERE ttSRit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(SRTgl) BETWEEN TglAwal AND TglAkhir
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttSRit.BrgKode,'NULL'), ttSRit.LokasiKode;
CREATE INDEX BrgKode ON tvsumbrglokasi(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `SR`;
CREATE TEMPORARY TABLE IF NOT EXISTS `SR` AS
SELECT tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode, IFNULL(tvsumbrglokasi.MySUM, 0) AS MySum FROM tdbaranglokasi
LEFT OUTER JOIN tvsumbrglokasi
ON tdbaranglokasi.BrgKode = tvsumbrglokasi.BrgKode AND tdbaranglokasi.LokasiKode = tvsumbrglokasi.LokasiKode
LEFT OUTER  JOIN tdbarang ON tdbarang.BrgKode = tdbaranglokasi.BrgKode
WHERE tdbaranglokasi.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus) AND tdbaranglokasi.LokasiKode LIKE CONCAT('%',MyLokasi)
ORDER BY tdbarang.BrgGroup, tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode;
CREATE INDEX BrgKode ON SR(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `tvsumbrglokasi`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrglokasi` AS
SELECT IFNULL(ttTIit.BrgKode,'NULL') AS BrgKode,ttTIit.LokasiTujuan AS LokasiKode,  SUM(ttTIit.TIQty) AS MySum FROM ttTIhd
INNER JOIN ttTIit ON ttTIhd.TINo = ttTIit.TINo
WHERE ttTIit.LokasiTujuan LIKE CONCAT('%',MyLokasi) AND DATE(TITgl) BETWEEN TglAwal AND TglAkhir
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttTIit.BrgKode,'NULL'),ttTIit.LokasiTujuan;
CREATE INDEX BrgKode ON tvsumbrglokasi(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `TIPlus`;
CREATE TEMPORARY TABLE IF NOT EXISTS `TIPlus` AS
SELECT tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode, IFNULL(tvsumbrglokasi.MySUM, 0) AS MySum FROM tdbaranglokasi
LEFT OUTER JOIN tvsumbrglokasi
ON tdbaranglokasi.BrgKode = tvsumbrglokasi.BrgKode AND tdbaranglokasi.LokasiKode = tvsumbrglokasi.LokasiKode
LEFT OUTER  JOIN tdbarang ON tdbarang.BrgKode = tdbaranglokasi.BrgKode
WHERE tdbaranglokasi.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus) AND tdbaranglokasi.LokasiKode LIKE CONCAT('%',MyLokasi)
ORDER BY tdbarang.BrgGroup, tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode;
CREATE INDEX BrgKode ON TIPlus(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `tvsumbrglokasi`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrglokasi` AS
SELECT IFNULL(ttTIit.BrgKode,'NULL') AS BrgKode,ttTIit.LokasiAsal AS LokasiKode, SUM(ttTIit.TIQty) AS MySum FROM ttTIhd
INNER JOIN ttTIit ON ttTIhd.TINo = ttTIit.TINo
WHERE ttTIit.LokasiAsal LIKE CONCAT('%',MyLokasi) AND DATE(TITgl) BETWEEN TglAwal AND TglAkhir
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttTIit.BrgKode,'NULL'),ttTIit.LokasiAsal;
CREATE INDEX BrgKode ON tvsumbrglokasi(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `TIMin`;
CREATE TEMPORARY TABLE IF NOT EXISTS `TIMin` AS
SELECT tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode, IFNULL(tvsumbrglokasi.MySUM, 0) AS MySum FROM tdbaranglokasi
LEFT OUTER JOIN tvsumbrglokasi
ON tdbaranglokasi.BrgKode = tvsumbrglokasi.BrgKode AND tdbaranglokasi.LokasiKode = tvsumbrglokasi.LokasiKode
LEFT OUTER  JOIN tdbarang ON tdbarang.BrgKode = tdbaranglokasi.BrgKode
WHERE tdbaranglokasi.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus) AND tdbaranglokasi.LokasiKode LIKE CONCAT('%',MyLokasi)
ORDER BY tdbarang.BrgGroup, tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode;
CREATE INDEX BrgKode ON TIMin(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `tvsumbrglokasi`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrglokasi` AS
SELECT IFNULL(ttTAit.BrgKode,'NULL') AS BrgKode, ttTAit.LokasiKode, SUM(ttTAit.TAQty) AS MySum FROM ttTAhd
INNER JOIN ttTAit ON ttTAhd.TANo = ttTAit.TANo
WHERE ttTAit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(TATgl) BETWEEN TglAwal AND TglAkhir
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttTAit.BrgKode,'NULL'), ttTAit.LokasiKode;
CREATE INDEX BrgKode ON tvsumbrglokasi(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `TA`;
CREATE TEMPORARY TABLE IF NOT EXISTS `TA` AS
SELECT tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode, IFNULL(tvsumbrglokasi.MySUM, 0) AS MySum FROM tdbaranglokasi
LEFT OUTER JOIN tvsumbrglokasi
ON tdbaranglokasi.BrgKode = tvsumbrglokasi.BrgKode AND tdbaranglokasi.LokasiKode = tvsumbrglokasi.LokasiKode
LEFT OUTER  JOIN tdbarang ON tdbarang.BrgKode = tdbaranglokasi.BrgKode
WHERE tdbaranglokasi.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus) AND tdbaranglokasi.LokasiKode LIKE CONCAT('%',MyLokasi)
ORDER BY tdbarang.BrgGroup, tdbaranglokasi.BrgKode, tdbaranglokasi.LokasiKode;
CREATE INDEX BrgKode ON TA(BrgKode);
UPDATE StockBarang SET PS =0, SR =0, TIPlus = 0, TA = 0, SD = 0, SI =0, PR = 0, TIMin = 0;
UPDATE StockBarang INNER JOIN SD ON SD.BrgKode = StockBarang.BrgKode AND SD.LokasiKode = StockBarang.LokasiKode SET  SD = MySum ;
UPDATE StockBarang INNER JOIN SI ON SI.BrgKode = StockBarang.BrgKode AND SI.LokasiKode = StockBarang.LokasiKode SET  SI = MySum ;
UPDATE StockBarang INNER JOIN SR ON SR.BrgKode = StockBarang.BrgKode AND SR.LokasiKode = StockBarang.LokasiKode SET  SR = MySum ;
UPDATE StockBarang INNER JOIN PS ON PS.BrgKode = StockBarang.BrgKode AND PS.LokasiKode = StockBarang.LokasiKode SET  PS = MySum ;
UPDATE StockBarang INNER JOIN PR ON PR.BrgKode = StockBarang.BrgKode AND PR.LokasiKode = StockBarang.LokasiKode SET  PR = MySum ;
UPDATE StockBarang INNER JOIN TA ON TA.BrgKode = StockBarang.BrgKode AND TA.LokasiKode = StockBarang.LokasiKode SET  TA = MySum ;
UPDATE StockBarang INNER JOIN TIPlus ON TIPlus.BrgKode = StockBarang.BrgKode AND TIPlus.LokasiKode = StockBarang.LokasiKode SET  TIPlus = MySum ;
UPDATE StockBarang INNER JOIN TIMin ON TIMin.BrgKode = StockBarang.BrgKode AND TIMin.LokasiKode = StockBarang.LokasiKode SET  TIMin = MySum ;
UPDATE StockBarang SET SaldoAkhir = SaldoAwal + PS + SR + TIPlus + TA - SD - SI - PR - TIMin;
END$$
DELIMITER ;

