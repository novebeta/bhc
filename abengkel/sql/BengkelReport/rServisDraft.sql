DELIMITER $$
DROP PROCEDURE IF EXISTS `rServisDraft`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rServisDraft`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,
IN xTC VARCHAR(30),IN xPOS VARCHAR(30),IN xASS VARCHAR(250),IN xServiceinvoice VARCHAR(20),
IN xStatus VARCHAR(250),IN xDump VARCHAR(10),IN xSort VARCHAR(150),IN xOrder VARCHAR(10),
OUT JumKonsTidakMenunggu SMALLINT,
OUT JumKonsMenunggu SMALLINT,
OUT SumWaktuTunggu DOUBLE,
OUT RataRataWaktuTunggu DOUBLE,
OUT Dump SMALLINT,
OUT PengerjaanTepatWaktu SMALLINT,
OUT PengerjaanLebihCepat SMALLINT,
OUT PengerjaanLebihLambat SMALLINT,
OUT RataRataLebihCepat DOUBLE ,
OUT RataRataLebihLambat DOUBLE 
)
BEGIN
/*
	CALL rServisDraft('Header1','2020-01-01','2020-01-31','','','','','!Selesai!','','SDTgl','ASC',@A,@B,@C,@D,@E,@F,@G,@H,@I,@J);
*/
	SET JumKonsTidakMenunggu =0;
	SET JumKonsMenunggu =0;
	SET SumWaktuTunggu =0;
	SET RataRataWaktuTunggu =0;
	SET Dump =0;
	SET PengerjaanTepatWaktu =0;
	SET PengerjaanLebihCepat =0;
	SET PengerjaanLebihLambat =0;
	SET RataRataLebihCepat =0;
	SET RataRataLebihLambat =0; 

	UPDATE ttsdhd
   INNER JOIN (SELECT SDNo,Finish, SUM(TIMESTAMPDIFF(MINUTE, START, Finish)) AS Durasiku FROM  ttsdittime GROUP BY SDNo) ttsdittime ON ttsdittime.SDNo = ttsdhd.SDNo
   SET SDDurasi = Durasiku, SDJamSelesai= FInish
   WHERE SDDUrasi = 0 AND Durasiku < 5000 AND DATE(ttsdhd.SVTgl) BETWEEN xTgl1 AND xTgl2;

   UPDATE ttsdhd  SET SDDurasi = TIMESTAMPDIFF(MINUTE, SDJamDikerjakan, SDJamSelesai)WHERE SDDurasi <= 0 AND DATE(ttsdhd.SVTgl) BETWEEN xTgl1 AND xTgl2;
   UPDATE ttsdhd  SET SDJamDikerjakan =  SDJamSelesai  WHERE SDDurasi < 0 AND DATE(ttsdhd.SVTgl) BETWEEN xTgl1 AND xTgl2;

	SET xTC = CONCAT("%",xTC);
	SET xPOS = CONCAT("%",xPOS);
	SET xASS = CONCAT("%",xASS,"%");
	SET xServiceinvoice = CONCAT("%",xServiceinvoice,"%");

	SET xStatus = REPLACE(xStatus,"!","'");

	IF xTipe = "Header1" OR xTipe = "Konsumen1" OR xTipe = "Konsumen2" OR xTipe = "Konsumen3" OR xTipe = "Motor1" THEN
		DROP TEMPORARY TABLE IF EXISTS ttsdhd;
		SET @MyQuery = CONCAT("
  		CREATE TEMPORARY TABLE IF NOT EXISTS ttsdhd AS
		SELECT ttsdhd.SDNo, ttsdhd.SDTgl, ttsdhd.SDJamMasuk, ttsdhd.SDNoUrut, ttsdhd.SDStatus, ttsdhd.SDKmSekarang, ttsdhd.SDKmBerikut, ttsdhd.SDKeluhan, ttsdhd.MotorNoPolisi, ttsdhd.KarKode, ttsdhd.LokasiKode,
		ttsdhd.SDJamDikerjakan, ttsdhd.SDJamSelesai, ttsdhd.SDTotalWaktu, ttsdhd.SDTotalJasa, ttsdhd.SDTotalQty, ttsdhd.SDTotalPart, ttsdhd.SDTotalGratis, ttsdhd.SDTotalNonGratis, ttsdhd.SDDiscFinal, ttsdhd.SDTotalPajak,
      ttsdhd.SDTotalBiaya, ttsdhd.SVNo, ttsdhd.SVTgl, ttsdhd.UserID, IFNULL(tdkaryawan.KarNama, '--') AS KarNama, IFNULL(tdcustomer.CusNama, '--') AS CusNama, IFNULL(tdcustomer.MotorType, '--') AS MotorType,
      IFNULL(tdcustomer.MotorWarna, '--') AS MotorWarna, IFNULL(tdmotortype.MotorNama, '--') AS MotorNama, fWaktu(ttsdhd.SDTotalWaktu) AS SDWaktu, tdcustomer.CusTelepon, tdcustomer.MotorNoMesin,
      tdcustomer.MotorNoRangka, ttsdhd.SDPembawaMotor, ttsdhd.SDHubunganPembawa, ttsdhd.SDAlasanServis, ttsdhd.SDKasKeluar, ttsdhd.NoGLSD, ttsdhd.NoGLSV, ttsdhd.KodeTrans, ttsdhd.PKBNo, ttsdhd.PosKode,
      ttsdhd.Cetak, ttsdhd.PrgNama, ttsdhd.SDUangMuka, ttsdhd.SDKeterangan, ttsdhd.SDJenis, ttsdhd.SDTOP, ttsdhd.SDDurasi, ttsdhd.SENo, ttsdhd.ASS, ttsdhd.SDTerbayar, ttsdhd.KlaimKPB, ttsdhd.KlaimC2,
      IFNULL(TIMESTAMPDIFF(MINUTE, ttsdhd.SDJamMasuk, ttsdhd.SDJamDikerjakan),0) AS WTunggu, tdcustomer.CusKTP, tdcustomer.CusEmail, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusProvinsi,
      tdcustomer.CusKabupaten, tdcustomer.CusKecamatan, tdcustomer.CusKelurahan, tdcustomer.CusKodePos, tdcustomer.CusSex, tdcustomer.CusTempatLhr, tdcustomer.CusTglLhr, tdcustomer.CusAgama,
      tdcustomer.CusPekerjaan, tdcustomer.CusKeterangan, tdcustomer.CusKK, tdcustomer.MotorTahun, tdcustomer.MotorKategori, tdcustomer.MotorCC, tdcustomer.TglAkhirServis, tdcustomer.AHASSNomor, tdahass.AHASSNama,
      tdahass.AHASSKecamatan
		FROM ttsdhd LEFT OUTER JOIN
      tdkaryawan ON ttsdhd.KarKode = tdkaryawan.KarKode LEFT OUTER JOIN
      tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi LEFT OUTER JOIN
      tdmotortype ON tdcustomer.MotorType = tdmotortype.MotorType LEFT OUTER JOIN
      tdahass ON tdahass.AHASSNomor = tdcustomer.AHASSNomor
      WHERE DATE(ttsdhd.SDTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'",
      " AND KodeTrans LIKE '",xTC,"' ",
      " AND ASS LIKE  '",xASS,"' ",
      " AND ttsdhd.PosKode LIKE '",xPOS,"' ",
      " AND ttsdhd.SVNo LIKE '" , xServiceinvoice , "' ");
		IF xDump = "Dump" THEN
			SET @MyQuery = CONCAT(@MyQuery," ","AND (ttsdhd.SDDurasi <= 0 OR ttsdhd.SDDurasi > 600)");
		ELSEIF xDump = "6-600 menit" THEN
			SET @MyQuery = CONCAT(@MyQuery," "," AND (ttsdhd.SDDurasi > 0  AND ttsdhd.SDDurasi < 600) ");
		ELSEIF xDump = "Semua" THEN
			SET xDump = " ";
		END IF;
      SET @MyQuery = CONCAT(@MyQuery," AND ttsdhd.SDStatus IN (",xStatus,")");
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
      PREPARE STMT FROM @MyQuery;
		EXECUTE Stmt;
		
		SELECT IFNULL(COUNT(SDNo),0) INTO JumKonsTidakMenunggu FROM ttsdhd WHERE WTunggu = 0 AND SDDurasi > 0 AND SDDurasi < 600;
		SELECT IFNULL(COUNT(SDNo),0) INTO JumKonsMenunggu FROM ttsdhd WHERE WTunggu > 0 AND SDDurasi > 0 AND SDDurasi < 600;
		SELECT IFNULL(SUM(WTunggu),0) INTO SumWaktuTunggu FROM ttsdhd WHERE WTunggu > 0 AND SDDurasi > 0 AND SDDurasi < 600;
		IF JumKonsMenunggu > 0 THEN
			SET RataRataWaktuTunggu = SumWaktuTunggu / JumKonsMenunggu;
		END IF;
		SELECT IFNULL(COUNT(SDNo),0) INTO Dump FROM ttsdhd WHERE  SDDurasi <= 0 AND SDDurasi > 600;
		SELECT IFNULL(COUNT(SDNo),0) INTO PengerjaanTepatWaktu FROM ttsdhd WHERE SDTotalWaktu = SDDurasi;
		SELECT IFNULL(COUNT(SDNo),0) INTO PengerjaanLebihLambat FROM ttsdhd WHERE SDTotalWaktu < SDDurasi AND SDDurasi > 0 AND SDDurasi < 600 ;
		SELECT IFNULL(COUNT(SDNo),0) INTO PengerjaanLebihCepat FROM ttsdhd WHERE SDTotalWaktu > SDDurasi AND SDDurasi > 0 AND SDDurasi < 600 ;
		SELECT IFNULL(SUM(SDTotalWaktu - SDDurasi),0) INTO RataRataLebihCepat FROM ttsdhd WHERE SDTotalWaktu > SDDurasi AND SDDurasi > 0 AND SDDurasi < 600;
		IF PengerjaanLebihCepat > 0 THEN 
			SET RataRataLebihCepat = RataRataLebihCepat / PengerjaanLebihCepat;
		END IF;
		SELECT IFNULL(SUM(SDDurasi - SDTotalWaktu),0) INTO RataRataLebihLambat FROM ttsdhd  WHERE SDTotalWaktu < SDDurasi AND SDDurasi > 0 AND SDDurasi < 600;
		IF PengerjaanLebihLambat > 0 THEN 
			SET RataRataLebihLambat = RataRataLebihLambat / PengerjaanLebihLambat;
		END IF;
            
		SET @MyQuery = CONCAT("SELECT * FROM ttsdhd;");
	END IF;
	IF xTipe = "Item1" THEN
			SET @MyQuery = CONCAT("
		SELECT ttsditjasa.SDNo, ttsditjasa.SDAuto, ttsditjasa.JasaKode AS BrgKode, ttsditjasa.SDWaktuKerja AS SDQty, ttsditjasa.SDWaktuSatuan AS BrgSatuan, ttsditjasa.SDHrgJual, 
		ttsditjasa.SDJualDisc, tdjasa.JasaNama AS BrgNama, ttsditjasa.SDJualDisc / ttsditjasa.SDHrgJual * 100 AS Disc, 
		ttsditjasa.SDHrgJual - ttsditjasa.SDJualDisc AS Jumlah, 'Jasa' AS Jenis,  ttsdhd.SDTgl, ttsdhd.KodeTrans , ttsdhd.ASS, ttsdhd.SDStatus, tdjasa.JasaGroup AS BrgGroup
		FROM ttsditjasa INNER JOIN tdjasa ON ttsditjasa.JasaKode = tdjasa.JasaKode 
		INNER JOIN ttsdhd ON ttsditjasa.SDNo = ttsdhd.SDNo
      WHERE DATE(ttsdhd.SDTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'",
      " AND KodeTrans LIKE '",xTC,"' ",
      " AND ASS LIKE  '",xASS,"' ",
      " AND ttsdhd.PosKode LIKE '",xPOS,"' ",
      " AND ttsdhd.SVNo LIKE '" , xServiceinvoice , "' ");
		IF xDump = "Dump" THEN
			SET @MyQuery = CONCAT(@MyQuery," ","AND (ttsdhd.SDDurasi <= 0 OR ttsdhd.SDDurasi > 600)");
		ELSEIF xDump = "6-600 menit" THEN
			SET @MyQuery = CONCAT(@MyQuery," "," AND (ttsdhd.SDDurasi > 0  AND ttsdhd.SDDurasi < 600) ");
		ELSEIF xDump = "Semua" THEN
			SET xDump = " ";
		END IF;
      SET @MyQuery = CONCAT(@MyQuery," AND ttsdhd.SDStatus IN (",xStatus,")");
      SET @MyQuery = CONCAT(@MyQuery," UNION 
      SELECT ttsditbarang.SDNo, ttsditbarang.SDAuto, ttsditbarang.BrgKode, ttsditbarang.SDQty, tdbarang.BrgSatuan, ttsditbarang.SDHrgJual, 
		ttsditbarang.SDJualDisc, tdbarang.BrgNama , ttsditbarang.SDJualDisc / ttsditbarang.SDHrgJual * 100 AS Disc, 
		ttsditbarang.SDQty * ttsditbarang.SDHrgJual - ttsditbarang.SDJualDisc AS Jumlah, 'Barang' AS Jenis, ttsdhd.SDTgl , ttsdhd.KodeTrans, ttsdhd.ASS, ttsdhd.SDStatus, tdbarang.BrgGroup
		FROM ttsditbarang INNER JOIN tdbarang ON ttsditbarang.BrgKode = tdbarang.BrgKode
		INNER JOIN ttsdhd ON ttsditbarang.SDNo = ttsdhd.SDNo
      WHERE DATE(ttsdhd.SDTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'",
      " AND KodeTrans LIKE '",xTC,"' ",
      " AND ASS LIKE  '",xASS,"' ",
      " AND ttsdhd.PosKode LIKE '",xPOS,"' ",
      " AND ttsdhd.SVNo LIKE '" , xServiceinvoice , "' ");
		IF xDump = "Dump" THEN
			SET @MyQuery = CONCAT(@MyQuery," ","AND (ttsdhd.SDDurasi <= 0 OR ttsdhd.SDDurasi > 600)");
		ELSEIF xDump = "6-600 menit" THEN
			SET @MyQuery = CONCAT(@MyQuery," "," AND (ttsdhd.SDDurasi > 0  AND ttsdhd.SDDurasi < 600) ");
		ELSEIF xDump = "Semua" THEN
			SET xDump = " ";
		END IF;
      SET @MyQuery = CONCAT(@MyQuery," AND ttsdhd.SDStatus IN (",xStatus,")");
	END IF;
	
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$
DELIMITER ;

