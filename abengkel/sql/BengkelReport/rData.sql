DELIMITER $$

DROP PROCEDURE IF EXISTS `rData`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rData`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,
IN xKode1 VARCHAR(250), IN xKode2 VARCHAR(250),
IN xStatus VARCHAR(250), IN xSort VARCHAR(150),IN xOrder VARCHAR(10))
BEGIN
/*
	CALL rData('Header1','2020-01-01','2020-05-31','','','','MotorType','ASC');
*/
       
	IF xTipe = "Type Motor" THEN
		IF xKode1 = "" OR xKode2 = "" THEN
			SET @MyQuery = CONCAT("SELECT tdmotortype.*, 'A' AS TypeStatus FROM tdmotortype "); 
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		ELSE
			SET @MyQuery = CONCAT("SELECT tdmotortype.*, 'A' AS TypeStatus  FROM tdmotortype ");                      
			SET @MyQuery = CONCAT(@MyQuery," WHERE (MotorType BETWEEN '" , xKode1 , "' AND '" , xKode2 , "') ");                     
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		END IF;
	END IF;

	IF xTipe = "Supplier" THEN
		IF xKode1 = "" OR xKode2 = "" THEN
			SET @MyQuery = CONCAT("SELECT * FROM tdSupplier "); 
			SET @MyQuery = CONCAT(@MyQuery," WHERE SupStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		ELSE
			SET @MyQuery = CONCAT("SELECT * FROM tdSupplier ");                      
			SET @MyQuery = CONCAT(@MyQuery," WHERE SupStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," AND (SupKode BETWEEN '" , xKode1 , "' AND '" , xKode2 , "') ");                     
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		END IF;
	END IF;
	
	IF xTipe = "Dealer" THEN
		IF xKode1 = "" OR xKode2 = "" THEN
			SET @MyQuery = CONCAT("SELECT * FROM tddealer "); 
			SET @MyQuery = CONCAT(@MyQuery," WHERE DealerStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		ELSE
			SET @MyQuery = CONCAT("SELECT * FROM tddealer ");                      
			SET @MyQuery = CONCAT(@MyQuery," WHERE DealerStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," AND (DealerKode BETWEEN '" , xKode1 , "' AND '" , xKode2 , "') ");                     
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		END IF;
	END IF;

	IF xTipe = "Area" THEN
		IF xKode1 = "" OR xKode2 = "" THEN
			SET @MyQuery = CONCAT("SELECT * FROM tdarea "); 
			SET @MyQuery = CONCAT(@MyQuery," WHERE AreaStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		ELSE
			SET @MyQuery = CONCAT("SELECT * FROM tdarea ");                      
			SET @MyQuery = CONCAT(@MyQuery," WHERE AreaStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," AND (Kabupaten BETWEEN '" , xKode1 , "' AND '" , xKode2 , "') ");                     
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		END IF;
	END IF;

	IF xTipe = "User" THEN
		IF xKode1 = "" OR xKode2 = "" THEN
			SET @MyQuery = CONCAT("SELECT tuser.* FROM tuser "); 
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		ELSE
			SET @MyQuery = CONCAT("SELECT tuser.* FROM tuser ");                      
			SET @MyQuery = CONCAT(@MyQuery," WHERE (UserID BETWEEN '" , xKode1 , "' AND '" , xKode2 , "') ");                     
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		END IF;
	END IF;

	IF xTipe = "AHASS" THEN
		IF xKode1 = "" OR xKode2 = "" THEN
			SET @MyQuery = CONCAT("SELECT * FROM tdahass "); 
			SET @MyQuery = CONCAT(@MyQuery," WHERE AHASSStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		ELSE
			SET @MyQuery = CONCAT("SELECT * FROM tdahass ");                      
			SET @MyQuery = CONCAT(@MyQuery," WHERE AHASSStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," AND (AHASSNomor BETWEEN '" , xKode1 , "' AND '" , xKode2 , "') ");                     
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		END IF;
	END IF;

	IF xTipe = "Hak Akses" THEN
		IF xKode1 = "" OR xKode2 = "" THEN
			SET @MyQuery = CONCAT("SELECT KodeAkses AS tKodeAkses, MenuInduk AS tMenuInduk, MenuText AS tMenuText, MenuNo AS tMenuNo,
			MenuStatus AS tMenuStatus, MenuTambah AS tMenuTambah, MenuEdit AS tMenuEdit, MenuHapus AS tMenuHapus, MenuCetak AS tMenuCetak, MenuView AS tMenuView FROM tuakses "); 
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY KodeAkses, MenuInduk, MenuNo, " ,xSort," ",xOrder );
		ELSE
			SET @MyQuery = CONCAT("SELECT KodeAkses, MenuInduk, MenuText, MenuNo, MenuStatus, MenuTambah, MenuEdit, MenuHapus, MenuCetak, MenuView FROM tuakses ");                      
			SET @MyQuery = CONCAT(@MyQuery," WHERE (KodeAkses BETWEEN '" , xKode1 , "' AND '" , xKode2 , "') ");                     
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY KodeAkses, MenuInduk, MenuNo, " ,xSort," ",xOrder );
		END IF;
	END IF;

	IF xTipe = "Jasa" THEN
		IF xKode1 = "" OR xKode2 = "" THEN
			SET @MyQuery = CONCAT("SELECT * FROM tdjasa "); 
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		ELSE
			SET @MyQuery = CONCAT("SELECT *  FROM tdjasa ");                      
			SET @MyQuery = CONCAT(@MyQuery," WHERE (JasaKode BETWEEN '" , xKode1 , "' AND '" , xKode2 , "') ");                     
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		END IF;
	END IF;

	IF xTipe = "Jasa Group" THEN
		IF xKode1 = "" OR xKode2 = "" THEN
			SET @MyQuery = CONCAT("SELECT * FROM tdjasagroup "); 
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		ELSE
			SET @MyQuery = CONCAT("SELECT *  FROM tdjasagroup ");                      
			SET @MyQuery = CONCAT(@MyQuery," WHERE (JasaGroup BETWEEN '" , xKode1 , "' AND '" , xKode2 , "') ");                     
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		END IF;
	END IF;

	IF xTipe = "Lokasi" THEN
		IF xKode1 = "" OR xKode2 = "" THEN
			SET @MyQuery = CONCAT("SELECT * FROM tdlokasi "); 
			SET @MyQuery = CONCAT(@MyQuery," WHERE LokasiStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY  " ,xSort," ",xOrder );
		ELSE
			SET @MyQuery = CONCAT("SELECT * FROM tdlokasi ");                      
			SET @MyQuery = CONCAT(@MyQuery," WHERE LokasiStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," AND (LokasiKode BETWEEN '" , xKode1 , "' AND '" , xKode2 , "') ");                     
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		END IF;	
	END IF;

	IF xTipe = "POS" THEN
		IF xKode1 = "" OR xKode2 = "" THEN
			SET @MyQuery = CONCAT("SELECT * FROM tdPOS "); 
			SET @MyQuery = CONCAT(@MyQuery," WHERE PosStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY  " ,xSort," ",xOrder );
		ELSE
			SET @MyQuery = CONCAT("SELECT * FROM tdPOS ");                      
			SET @MyQuery = CONCAT(@MyQuery," WHERE PosStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," AND (PosKode BETWEEN '" , xKode1 , "' AND '" , xKode2 , "') ");                     
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		END IF;
	END IF;
	
	IF xTipe = "Karyawan1" THEN
		IF xKode1 = "" OR xKode2 = "" THEN
			SET @MyQuery = CONCAT("SELECT * FROM tdkaryawan "); 
			SET @MyQuery = CONCAT(@MyQuery," WHERE KarStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY  " ,xSort," ",xOrder );
		ELSE
			SET @MyQuery = CONCAT("SELECT * FROM tdkaryawan ");                      
			SET @MyQuery = CONCAT(@MyQuery," WHERE KarStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," AND (KarKode BETWEEN '" , xKode1 , "' AND '" , xKode2 , "') ");                     
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		END IF;
	END IF;

	IF xTipe = "Karyawan2" THEN
		IF xKode1 = "" OR xKode2 = "" THEN
			SET @MyQuery = CONCAT("SELECT * FROM tdkaryawan "); 
			SET @MyQuery = CONCAT(@MyQuery," WHERE KarStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY  " ,xSort," ",xOrder );
		ELSE
			SET @MyQuery = CONCAT("SELECT * FROM tdkaryawan ");                      
			SET @MyQuery = CONCAT(@MyQuery," WHERE KarStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," AND (KarKode BETWEEN '" , xKode1 , "' AND '" , xKode2 , "') ");                     
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		END IF;
	END IF;

	IF xTipe = "Konsumen1" THEN
		IF xKode1 = "" OR xKode2 = "" THEN
			SET @MyQuery = CONCAT("SELECT * FROM tdcustomer "); 
			SET @MyQuery = CONCAT(@MyQuery," WHERE CusStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," AND (TglAkhirServis BETWEEN '",xTgl1,"' AND '",xTgl2,"')");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY  " ,xSort," ",xOrder );
		ELSE
			SET @MyQuery = CONCAT("SELECT * FROM tdcustomer ");                      
			SET @MyQuery = CONCAT(@MyQuery," WHERE CusStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," AND (MotorNoPolisi BETWEEN '" , xKode1 , "' AND '" , xKode2 , "') ");                     
			SET @MyQuery = CONCAT(@MyQuery," AND (TglAkhirServis BETWEEN '",xTgl1,"' AND '",xTgl2,"')");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		END IF;	
	END IF;

	IF xTipe = "Konsumen2" THEN
		IF xKode1 = "" OR xKode2 = "" THEN
			SET @MyQuery = CONCAT("SELECT * FROM tdcustomer "); 
			SET @MyQuery = CONCAT(@MyQuery," WHERE CusStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," AND (TglAkhirServis BETWEEN '",xTgl1,"' AND '",xTgl2,"')");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY  " ,xSort," ",xOrder );
		ELSE
			SET @MyQuery = CONCAT("SELECT * FROM tdcustomer ");                      
			SET @MyQuery = CONCAT(@MyQuery," WHERE CusStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," AND (MotorNoPolisi BETWEEN '" , xKode1 , "' AND '" , xKode2 , "') ");                     
			SET @MyQuery = CONCAT(@MyQuery," AND (TglAkhirServis BETWEEN '",xTgl1,"' AND '",xTgl2,"')");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		END IF;	
	END IF;

	IF xTipe = "Konsumen3" THEN
		IF xKode1 = "" OR xKode2 = "" THEN
			SET @MyQuery = CONCAT("SELECT * FROM tdcustomer "); 
			SET @MyQuery = CONCAT(@MyQuery," WHERE CusStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," AND (TglAkhirServis BETWEEN '",xTgl1,"' AND '",xTgl2,"')");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY  " ,xSort," ",xOrder );
		ELSE
			SET @MyQuery = CONCAT("SELECT * FROM tdcustomer ");                      
			SET @MyQuery = CONCAT(@MyQuery," WHERE CusStatus LIKE '%" , xStatus ,"'");                     
			SET @MyQuery = CONCAT(@MyQuery," AND (MotorNoPolisi BETWEEN '" , xKode1 , "' AND '" , xKode2 , "') ");                     
			SET @MyQuery = CONCAT(@MyQuery," AND (TglAkhirServis BETWEEN '",xTgl1,"' AND '",xTgl2,"')");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
		END IF;	
	END IF;

	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$

DELIMITER ;

