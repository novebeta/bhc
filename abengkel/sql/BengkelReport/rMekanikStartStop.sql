DELIMITER $$
DROP PROCEDURE IF EXISTS `rMekanikStartStop`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rMekanikStartStop`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,
IN xMekanik VARCHAR(100),IN xStatus VARCHAR(100))
BEGIN
/*
		CALL rMekanikStartStop('Header1','2020-01-01','2020-05-31','','');
*/
	IF xTipe = "Header1" THEN
		SET @MyQuery = CONCAT("
		SELECT ttsdittime.SDNo, ttsdhd.SDTgl, ttsdittime.KarKode, ttsdittime.Nomor, ttsdittime.Start, ttsdittime.Finish, ttsdittime.Status, 
      IF(TIMESTAMPDIFF(MINUTE, ttsdittime.Start, ttsdittime.Finish)>1000,0,TIMESTAMPDIFF(MINUTE, ttsdittime.Start, ttsdittime.Finish)) AS Durasi, ttsdhd.SDDurasi, 
      ttsdhd.MotorNoPolisi, tdcustomer.CusNama, tdcustomer.MotorType, tdcustomer.MotorWarna, tdcustomer.MotorTahun, tdcustomer.MotorNama, tdcustomer.MotorKategori, tdcustomer.MotorCC
      FROM  ttsdittime 
      INNER JOIN ttsdhd ON ttsdhd.SDNo = ttsdittime.SDNo 
      INNER JOIN tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi
      WHERE DATE(ttsdhd.SDTGl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery," AND ttsdittime.KarKode LIKE '%" , xMekanik , "' ");
      SET @MyQuery = CONCAT(@MyQuery," AND ttsdittime.Status LIKE  '%" , xStatus , "' ");
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY ttsdittime.KarKode, ttsdittime.Start ");
	END IF;
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$
DELIMITER ;

