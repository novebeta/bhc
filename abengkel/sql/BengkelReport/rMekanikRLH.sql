DELIMITER $$
DROP PROCEDURE IF EXISTS `rMekanikRLH`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rMekanikRLH`(IN xTipe VARCHAR(50),IN xTgl1 DATE,IN xTgl2 DATE,IN xJenis VARCHAR(5),
OUT BiayaOp DECIMAL(18,2), OUT Pendapatan DECIMAL(18,2), OUT PendapatanJasaKPBdanC2 DECIMAL(18,2),OUT SaldoAwal DECIMAL(18,2), 
OUT KoreksiPenjualan DECIMAL(18,2),OUT KoreksiHPP DECIMAL(18,2),OUT BiayaNonOp DECIMAL(18,2),OUT PendapatanNonOp DECIMAL(18,2))
BEGIN
/* CALL rMekanikRLH('RLH Mekanik Rekap','2020-02-01','2020-02-29','',@BiayaOp , @Pendapatan , @PendapatanJasaKPBdanC2 ,@SaldoAwal , @KoreksiPenjualan ,@KoreksiHPP ,@BiayaNonOp ,@PendapatanNonOp ); */
   DECLARE  xTgl0, xTglx DATE;
   DECLARE SumPenjualan, SumHPP DOUBLE;
	SET SaldoAwal =0;
	SET BiayaOp = 0;
	SET Pendapatan = 0;
	SET PendapatanJasaKPBdanC2 = 0;
	SET KoreksiPenjualan = 0;
	SET KoreksiHPP = 0;
	SET BiayaNonOp = 0;
	SET PendapatanNonOp = 0;
	SET SumPenjualan = 0;
	SET SumHPP = 0;
  							 
	IF (xTipe = "RLH Mekanik") THEN
		DROP TEMPORARY TABLE IF EXISTS RLHDetail;
		CREATE TEMPORARY TABLE IF NOT EXISTS RLHDetail AS
		SELECT ttsdhd.PosKode, ttsdhd.SVNo,ttsdhd.SVTgl,PKBNo,KodeTrans, ASS,ttsdhd.MotorNoPolisi, tdcustomer.CusNama,ttsdhd.KarKode,
		000000000000000000.00 AS JasaASS,
		000000000000000000.00 AS JasaC2,
		000000000000000000.00 AS JasaReguler,
		000000000000000000.00 AS JasaTotal,
		000000000000000000.00 AS JasaDisc,
		000000000000000000.00 AS JasaBersih,
		SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) AS PartJual,
		SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) AS OliJual,
		SUM(IF(BrgGroup ='Oli' AND KodeTrans = 'TC14',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) AS OliJualKPB, 
		SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS PartBeli,
		SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS OliBeli,
		SUM(IF(BrgGroup ='Oli' AND KodeTrans = 'TC14',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS OliBeliKPB,
		SUM(IFNULL(ttsditbarang.SDJualDisc,000000000000000000.00)) AS DiscPart,
		SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00))- SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS PartLaba,
		SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) - SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS OliLaba,
		SUM(IFNULL(ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00))- SUM(IFNULL(ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,0)) - SUM(IFNULL(ttsditbarang.SDJualDisc,000000000000000000.00)) AS PartOliLaba,
		ttsdhd.SDDiscFinal AS DiscFinal,
		000000000000000000.00 AS LabaTotal
		FROM ttsdhd
		LEFT OUTER JOIN ttsditbarang ON ttsdhd.SDNo = ttsditbarang.SDNo
		LEFT OUTER JOIN tdbarang ON tdbarang.BrgKode = ttsditbarang.BrgKode
		LEFT OUTER JOIN tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi
		WHERE DATE(ttsdhd.SVTgl) BETWEEN xTgl1 AND xTgl2
		GROUP BY ttsdhd.SDNo ORDER BY SDTgl;
		CREATE INDEX SVNo ON RLHDetail(SVNo);

		DROP TEMPORARY TABLE IF EXISTS RLHDetailJasa;
		CREATE TEMPORARY TABLE IF NOT EXISTS RLHDetailJasa AS
		SELECT ttsdhd.PosKode, ttsdhd.SVNo,ttsdhd.SVTgl,PKBNo,KodeTrans,ASS,ttsdhd.MotorNoPolisi, '--' AS CusNama,ttsdhd.KarKode,
		SUM(IF(LEFT(JasaGroup,3)='ASS',ttsditjasa.SDHrgJual,000000000000000000.00)) AS JasaASS,
		SUM(IF(JasaGroup='CLAIMC2',ttsditjasa.SDHrgJual,000000000000000000.00)) AS JasaC2,
		SUM(IF(LEFT(JasaGroup,3)<>'ASS' AND JasaGroup<>'CLAIMC2',ttsditjasa.SDHrgJual,0)) AS JasaReguler,
		IFNULL(SUM(ttsditjasa.SDHrgJual),000000000000000000.00) AS JasaTotal,
		IFNULL(SUM(ttsditjasa.SDJualDisc),000000000000000000.00) AS JasaDisc,
		IFNULL(SUM(ttsditjasa.SDHrgJual),000000000000000000.00)-IFNULL(SUM(ttsditjasa.SDJualDisc),000000000000000000.00) AS JasaBersih
		FROM ttsdhd
		LEFT OUTER JOIN ttsditjasa ON ttsdhd.SDNo = ttsditjasa.SDNo
		LEFT OUTER JOIN tdjasa ON tdjasa.JasaKode = ttsditjasa.JasaKode
		WHERE DATE(ttsdhd.SVTgl) BETWEEN xTgl1 AND xTgl2
		GROUP BY ttsdhd.SDNo ORDER BY SDTgl;
		CREATE INDEX SVNo ON RLHDetailJasa(SVNo);

		UPDATE RLHDetail INNER JOIN RLHDetailJasa ON RLHDetail.SVNo = RLHDetailJasa.SVNo SET
		RLHDetail.JasaASS = RLHDetailJasa.JasaASS,
		RLHDetail.JasaC2 = RLHDetailJasa.JasaC2,
		RLHDetail.JasaReguler = RLHDetailJasa.JasaReguler,
		RLHDetail.JasaTotal = RLHDetailJasa.JasaTotal,
		RLHDetail.JasaDisc = RLHDetailJasa.JasaDisc,
		RLHDetail.JasaBersih = RLHDetailJasa.JasaBersih,
		RLHDetail.LabaTotal =  RLHDetail.PartOliLaba + RLHDetailJasa.JasaBersih - RLHDetail.DiscFinal;

		INSERT INTO RLHDetail
		SELECT ttSIhd.PosKode, ttSIhd.SINo AS SVNo, ttSIhd.SITgl AS SVTgl, ttSIhd.SONo AS PKBNo, KodeTrans, SIJenis AS ASS, ttsihd.MotorNoPolisi, '--' AS CusNama,ttsihd.KarKode,
		000000000000000000.00 AS JasaASS,
		000000000000000000.00 AS JasaC2,
		000000000000000000.00 AS JasaReguler,
		000000000000000000.00 AS JasaTotal,
		000000000000000000.00 AS JasaDisc,
		000000000000000000.00 AS JasaBersih,
		SUM(IF(BrgGroup <>'Oli',ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00)) AS PartJual,
		SUM(IF(BrgGroup ='Oli',ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00)) AS OliJual,
		000000000000000000.00 AS OliJualKPB,
		SUM(IF(BrgGroup <>'Oli',ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) AS PartBeli,
		SUM(IF(BrgGroup ='Oli',ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) AS OliBeli,
		000000000000000000.00 AS OliBeliKPB,
		SUM(IFNULL(ttsiit.SIDiscount,000000000000000000.00)) AS DiscPart,
		SUM(IF(BrgGroup <>'Oli',ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00))- SUM(IF(BrgGroup <>'Oli',ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) AS PartLaba,
		SUM(IF(BrgGroup ='Oli',ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00)) - SUM(IF(BrgGroup ='Oli',ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) AS OliLaba,
		SUM(IFNULL(ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00))- SUM(IFNULL(ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) - SUM(IFNULL(ttsiit.SIDiscount,000000000000000000.00)) AS PartOliLaba,
		ttSIhd.SIDiscFinal AS DiscFinal,
		SUM(IFNULL(ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00))- SUM(IFNULL(ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) - SUM(IFNULL(ttsiit.SIDiscount,000000000000000000.00))-ttSIhd.SIDiscFinal AS LabaTotal
		FROM ttSIhd
		LEFT OUTER JOIN ttsiit ON ttSIhd.SINo = ttsiit.SINo
		LEFT OUTER JOIN tdbarang ON tdbarang.BrgKode = ttsiit.BrgKode
		WHERE DATE(ttSIhd.SITgl) BETWEEN xTgl1 AND xTgl2
		GROUP BY ttSIhd.SINO ORDER BY SITgl;

		INSERT INTO RLHDetail
		SELECT ttsrhd.PosKode, ttsrhd.SRNo AS SVNo, ttsrhd.srTgl AS SVTgl,'--' AS PKBNo, KodeTrans,'SR' AS ASS, ttsrhd.MotorNoPolisi, '--' AS CusNama,ttsrhd.KarKode,
		000000000000000000.00 AS JasaASS,
		000000000000000000.00 AS JasaC2,
		000000000000000000.00 AS JasaReguler,
		000000000000000000.00 AS JasaTotal,
		000000000000000000.00 AS JasaDisc,
		000000000000000000.00 AS JasaBersih,
		-SUM(IF(BrgGroup <>'Oli',ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00)) AS PartJual,
		-SUM(IF(BrgGroup ='Oli',ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00)) AS OliJual,
		000000000000000000.00 AS OliJualKPB,
		-SUM(IF(BrgGroup <>'Oli',ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) AS PartBeli,
		-SUM(IF(BrgGroup ='Oli',ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) AS OliBeli,
		000000000000000000.00 AS OliBeliKPB,
		-SUM(IFNULL(ttsrit.srDiscount,000000000000000000.00)) AS DiscPart,
		-SUM(IF(BrgGroup <>'Oli',ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00))- SUM(IF(BrgGroup <>'Oli',ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) AS PartLaba,
		-SUM(IF(BrgGroup ='Oli',ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00)) - SUM(IF(BrgGroup ='Oli',ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) AS OliLaba,
		-(SUM(IFNULL(ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00))- SUM(IFNULL(ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) - SUM(IFNULL(ttsrit.srDiscount,000000000000000000.00))) AS PartOliLaba,
		-ttsrhd.srDiscFinal AS DiscFinal,
		-(SUM(IFNULL(ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00))- SUM(IFNULL(ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) - SUM(IFNULL(ttsrit.srDiscount,000000000000000000.00))-ttsrhd.srDiscFinal) AS LabaTotal
		FROM ttsrhd
		LEFT OUTER JOIN ttsrit ON ttsrhd.srNo = ttsrit.srNo
		LEFT OUTER JOIN tdbarang ON tdbarang.BrgKode = ttsrit.BrgKode
		WHERE DATE(ttSRhd.SRTgl) BETWEEN xTgl1 AND xTgl2
		GROUP BY ttsrhd.SRNO  ORDER BY SRTgl;


		SET xTgl0 = DATE_ADD(LAST_DAY(DATE_ADD(xTgl1, INTERVAL -1 MONTH)), INTERVAL 1 DAY);
		SET xTglx = DATE_ADD(xTgl1, INTERVAL -1 DAY);
		IF xTgl1 = DATE_ADD(LAST_DAY(DATE_ADD(xTgl1, INTERVAL -1 MONTH)), INTERVAL 1 DAY) THEN
			SET xTglx = DATE_ADD(LAST_DAY(DATE_ADD(xTgl1, INTERVAL -1 MONTH)), INTERVAL 1 DAY);
		END IF;
		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO  BiayaNonOp
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '80'
		AND ttgeneralledgerit.TglGL BETWEEN xTgl0 AND xTglx;
		SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO PendapatanNonOp
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '90'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl0 AND xTglx;
		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO BiayaOp
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '60'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl0 AND xTglx;
		SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO Pendapatan
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '40'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl0 AND xTglx;
		SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO KoreksiPenjualan
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '30' AND LEFT(ttgeneralledgerit.NoGL,2) <> 'JT'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl0 AND xTglx;
		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO KoreksiHPP
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE ((LEFT(NoAccount,2) = '50' AND LEFT(ttgeneralledgerit.NoGL,2) <> 'JT') OR NoAccount = '50050000')
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl0 AND xTglx;
		SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO SumPenjualan
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '30' AND LEFT(ttgeneralledgerit.NoGL,2) = 'JT'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl0 AND xTglx;
		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO SumHPP
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE (LEFT(NoAccount,2) = '50' AND LEFT(ttgeneralledgerit.NoGL,2) = 'JT' AND NoAccount <> '50050000' )
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl0 AND xTglx;
		SET SaldoAwal = SumPenjualan - SumHPP + Pendapatan - BiayaOp + KoreksiPenjualan - KoreksiHPP - BiayaNonOp + PendapatanNonOp;
		IF MONTH(DATE_ADD(xTgl1, INTERVAL -1 DAY)) <> MONTH(xTgl1) THEN
         SET SaldoAwal = 0;
		END IF;

		SET BiayaOp = 0;
		SET Pendapatan = 0;
		SET PendapatanJasaKPBdanC2 = 0;
		SET KoreksiPenjualan = 0;
		SET KoreksiHPP = 0;
		SET BiayaNonOp = 0;
		SET PendapatanNonOp = 0;
		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO  BiayaNonOp
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '80'
		AND ttgeneralledgerit.TglGL BETWEEN xTgl1 AND xTgl2;
		SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO PendapatanNonOp
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '90'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl1 AND xTgl2;
		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO BiayaOp
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '60'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl1 AND xTgl2;
		SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO Pendapatan
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '40'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl1 AND xTgl2;
		SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO KoreksiPenjualan
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '30' AND LEFT(ttgeneralledgerit.NoGL,2) <> 'JT'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl1 AND xTgl2;
		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO KoreksiHPP
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE ((LEFT(NoAccount,2) = '50' AND LEFT(ttgeneralledgerit.NoGL,2) <> 'JT') OR NoAccount = '50050000')
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl1 AND xTgl2;	
		SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO  PendapatanJasaKPBdanC2
      FROM ttgeneralledgerit 
      INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL  
      WHERE (NoAccount = '40050000' OR NoAccount = '40060000') AND LEFT(ttgeneralledgerit.NoGL,2) = 'JT'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl1 AND xTgl2;
		
		SET @MyQuery = CONCAT("SELECT * FROM RLHDetail;");
	END IF;

	IF xTipe = "RLH Mekanik Rekap" THEN
		DROP TEMPORARY TABLE IF EXISTS RLHDetail;
		CREATE TEMPORARY TABLE IF NOT EXISTS RLHDetail AS
		SELECT ttsdhd.PosKode, MAX(ttsdhd.SVNo) AS SVNo,ttsdhd.SVTgl,PKBNo,KodeTrans, ASS,ttsdhd.MotorNoPolisi, tdcustomer.CusNama,ttsdhd.KarKode,
		000000000000000000.00 AS JasaASS,
		000000000000000000.00 AS JasaC2,
		000000000000000000.00 AS JasaReguler,
		000000000000000000.00 AS JasaTotal,
		000000000000000000.00 AS JasaDisc,
		000000000000000000.00 AS JasaBersih,
		SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) AS PartJual,
		SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) AS OliJual,
		SUM(IF(BrgGroup ='Oli' AND KodeTrans = 'TC14',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) AS OliJualKPB, 	
		SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS PartBeli,
		SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS OliBeli,
		SUM(IF(BrgGroup ='Oli' AND KodeTrans = 'TC14',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS OliBeliKPB,	
		SUM(IFNULL(ttsditbarang.SDJualDisc,000000000000000000.00)) AS DiscPart,
		SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00))- SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS PartLaba,
		SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) - SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS OliLaba,
		SUM(IFNULL(ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00))- SUM(IFNULL(ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,0)) - SUM(IFNULL(ttsditbarang.SDJualDisc,000000000000000000.00)) AS PartOliLaba,
		ttsdhd.SDDiscFinal AS DiscFinal,
		000000000000000000.00 AS LabaTotal,
		000000000000000000.00 AS JumJasaASS,
		000000000000000000.00 AS JumJasaC2,
		000000000000000000.00 AS JumJasaReguler,
		000000000000000000.00 AS JumJasaASS1,
		000000000000000000.00 AS JumJasaASS2,
		000000000000000000.00 AS JumJasaASS3,
		000000000000000000.00 AS JumJasaASS4
		FROM ttsdhd
		LEFT OUTER JOIN ttsditbarang ON ttsdhd.SDNo = ttsditbarang.SDNo
		LEFT OUTER JOIN tdbarang ON tdbarang.BrgKode = ttsditbarang.BrgKode
		LEFT OUTER JOIN tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi
		WHERE DATE(ttsdhd.SVTgl) BETWEEN xTgl1 AND xTgl2
		GROUP BY  DATE(ttsdhd.SVTgl),ttsdhd.KarKode;
		CREATE INDEX SVNo ON RLHDetail(SVNo);

		/* Servis Jasa */
		DROP TEMPORARY TABLE IF EXISTS RLHDetailJasa;
		CREATE TEMPORARY TABLE IF NOT EXISTS RLHDetailJasa AS
		SELECT ttsdhd.PosKode, MAX(ttsdhd.SVNo) AS SVNo,ttsdhd.SVTgl,PKBNo,KodeTrans,ASS,ttsdhd.MotorNoPolisi, '--' AS CusNama,ttsdhd.KarKode,
		SUM(IF(LEFT(JasaGroup,3)='ASS',ttsditjasa.SDHrgJual,000000000000000000.00)) AS JasaASS,
		SUM(IF(JasaGroup='CLAIMC2',ttsditjasa.SDHrgJual,000000000000000000.00)) AS JasaC2,
		SUM(IF(LEFT(JasaGroup,3)<>'ASS' AND JasaGroup<>'CLAIMC2',ttsditjasa.SDHrgJual,0)) AS JasaReguler,
		IFNULL(SUM(ttsditjasa.SDHrgJual),000000000000000000.00) AS JasaTotal,
		IFNULL(SUM(ttsditjasa.SDJualDisc),000000000000000000.00) AS JasaDisc,
		IFNULL(SUM(ttsditjasa.SDHrgJual),000000000000000000.00)-IFNULL(SUM(ttsditjasa.SDJualDisc),000000000000000000.00) AS JasaBersih
		FROM ttsdhd
		LEFT OUTER JOIN ttsditjasa ON ttsdhd.SDNo = ttsditjasa.SDNo
		LEFT OUTER JOIN tdjasa ON tdjasa.JasaKode = ttsditjasa.JasaKode
		WHERE DATE(ttsdhd.SVTgl) BETWEEN xTgl1 AND xTgl2
      GROUP BY DATE(ttsdhd.SVTgl), ttsdhd.KarKode;
		CREATE INDEX SVNo ON RLHDetailJasa(SVNo);

		UPDATE RLHDetail INNER JOIN RLHDetailJasa ON RLHDetail.SVNo = RLHDetailJasa.SVNo SET
		RLHDetail.JasaASS = RLHDetailJasa.JasaASS,
		RLHDetail.JasaC2 = RLHDetailJasa.JasaC2,
		RLHDetail.JasaReguler = RLHDetailJasa.JasaReguler,
		RLHDetail.JasaTotal = RLHDetailJasa.JasaTotal,
		RLHDetail.JasaDisc = RLHDetailJasa.JasaDisc,
		RLHDetail.JasaBersih = RLHDetailJasa.JasaBersih,
		RLHDetail.LabaTotal =  RLHDetail.PartOliLaba + RLHDetailJasa.JasaBersih - RLHDetail.DiscFinal;

		/* Servis Jumlah Jasa */
		DROP TEMPORARY TABLE IF EXISTS RLHDetailJasa;
		CREATE TEMPORARY TABLE IF NOT EXISTS RLHDetailJasa AS
		SELECT MAX(ttsdhd.SVNo) AS SVNo,ttsdhd.SVTgl,PKBNo,KodeTrans,ASS,ttsdhd.MotorNoPolisi, '--' AS CusNama,ttsdhd.KarKode,
		SUM(IF(KodeTrans ='TC14' AND ASS = 'ASS1',1,0)) AS JumJasaASS1,
		SUM(IF(KodeTrans ='TC14' AND ASS = 'ASS2',1,0)) AS JumJasaASS2,
		SUM(IF(KodeTrans ='TC14' AND ASS = 'ASS3',1,0)) AS JumJasaASS3,
		SUM(IF(KodeTrans ='TC14' AND ASS = 'ASS4',1,0)) AS JumJasaASS4,
		SUM(IF(KodeTrans ='TC14' ,1,0)) AS JumJasaASS,
		SUM(IF(KodeTrans='TC13',1,0)) AS JumJasaC2,
		SUM(IF(KodeTrans='TC11',1,0)) AS JumJasaReguler
		FROM ttsdhd
		WHERE DATE(ttsdhd.SVTgl) BETWEEN xTgl1 AND xTgl2
		GROUP BY DATE(ttsdhd.SVTgl),ttsdhd.KarKode;
		CREATE INDEX SVNo ON RLHDetailJasa(SVNo);

		UPDATE RLHDetail INNER JOIN RLHDetailJasa ON RLHDetail.SVNo = RLHDetailJasa.SVNo SET
		RLHDetail.JumJasaASS = RLHDetailJasa.JumJasaASS,
		RLHDetail.JumJasaC2 = RLHDetailJasa.JumJasaC2,
		RLHDetail.JumJasaReguler = RLHDetailJasa.JumJasaReguler,
		RLHDetail.JumJasaASS1 = RLHDetailJasa.JumJasaASS1,
		RLHDetail.JumJasaASS2 = RLHDetailJasa.JumJasaASS2,
		RLHDetail.JumJasaASS3 = RLHDetailJasa.JumJasaASS3,
		RLHDetail.JumJasaASS4 = RLHDetailJasa.JumJasaASS4;

		/* Sales Counter */
		INSERT INTO RLHDetail
		SELECT ttSIhd.PosKode, MAX(ttSIhd.SINo) AS SVNo, ttSIhd.SITgl AS SVTgl, ttSIhd.SONo AS PKBNo, KodeTrans, SIJenis AS ASS, ttsihd.MotorNoPolisi, '--' AS CusNama,ttsihd.KarKode,
		000000000000000000.00 AS JasaASS,
		000000000000000000.00 AS JasaC2,
		000000000000000000.00 AS JasaReguler,
		000000000000000000.00 AS JasaTotal,
		000000000000000000.00 AS JasaDisc,
		000000000000000000.00 AS JasaBersih,
		SUM(IF(BrgGroup <>'Oli',ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00)) AS PartJual,
		SUM(IF(BrgGroup ='Oli',ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00)) AS OliJual,
		000000000000000000.00 AS OliJualKPB,
		SUM(IF(BrgGroup <>'Oli',ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) AS PartBeli,
		SUM(IF(BrgGroup ='Oli',ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) AS OliBeli,
		000000000000000000.00 AS OliBeliKPB,
		SUM(IFNULL(ttsiit.SIDiscount,000000000000000000.00)) AS DiscPart,
		SUM(IF(BrgGroup <>'Oli',ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00))- SUM(IF(BrgGroup <>'Oli',ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) AS PartLaba,
		SUM(IF(BrgGroup ='Oli',ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00)) - SUM(IF(BrgGroup ='Oli',ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) AS OliLaba,
		SUM(IFNULL(ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00))- SUM(IFNULL(ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) - SUM(IFNULL(ttsiit.SIDiscount,000000000000000000.00)) AS PartOliLaba,
		ttSIhd.SIDiscFinal AS DiscFinal,
		SUM(IFNULL(ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00))- SUM(IFNULL(ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) - SUM(IFNULL(ttsiit.SIDiscount,000000000000000000.00))-ttSIhd.SIDiscFinal AS LabaTotal,
		000000000000000000.00 AS JumJasaASS,
		000000000000000000.00 AS JumJasaC2,
		000000000000000000.00 AS JumJasaReguler,
		000000000000000000.00 AS JumJasaASS1,
		000000000000000000.00 AS JumJasaASS2,
		000000000000000000.00 AS JumJasaASS3,
		000000000000000000.00 AS JumJasaASS4
		FROM ttSIhd
		LEFT OUTER JOIN ttsiit ON ttSIhd.SINo = ttsiit.SINo
		LEFT OUTER JOIN tdbarang ON tdbarang.BrgKode = ttsiit.BrgKode
		WHERE DATE(ttSIhd.SITgl) BETWEEN xTgl1 AND xTgl2
		GROUP BY DATE(ttSIhd.SITgl),ttSIhd.KarKode;

		/* Retur Penjualan */
		INSERT INTO RLHDetail
		SELECT ttsrhd.PosKode, MAX(ttsrhd.SRNo) AS SVNo, ttsrhd.srTgl AS SVTgl,'--' AS PKBNo, KodeTrans,'SR' AS ASS, ttsrhd.MotorNoPolisi, '--' AS CusNama,ttsrhd.KarKode,
		000000000000000000.00 AS JasaASS,
		000000000000000000.00 AS JasaC2,
		000000000000000000.00 AS JasaReguler,
		000000000000000000.00 AS JasaTotal,
		000000000000000000.00 AS JasaDisc,
		000000000000000000.00 AS JasaBersih,
		-SUM(IF(BrgGroup <>'Oli',ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00)) AS PartJual,
		-SUM(IF(BrgGroup ='Oli',ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00)) AS OliJual,
		000000000000000000.00 AS OliJualKPB,
		-SUM(IF(BrgGroup <>'Oli',ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) AS PartBeli,
		-SUM(IF(BrgGroup ='Oli',ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) AS OliBeli,
		000000000000000000.00 AS OliBeliKPB,
		-SUM(IFNULL(ttsrit.srDiscount,000000000000000000.00)) AS DiscPart,
		-SUM(IF(BrgGroup <>'Oli',ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00))- SUM(IF(BrgGroup <>'Oli',ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) AS PartLaba,
		-SUM(IF(BrgGroup ='Oli',ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00)) - SUM(IF(BrgGroup ='Oli',ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) AS OliLaba,
		-(SUM(IFNULL(ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00))- SUM(IFNULL(ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) - SUM(IFNULL(ttsrit.srDiscount,000000000000000000.00))) AS PartOliLaba,
		-ttsrhd.srDiscFinal AS DiscFinal,
		-(SUM(IFNULL(ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00))- SUM(IFNULL(ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) - SUM(IFNULL(ttsrit.srDiscount,000000000000000000.00))-ttsrhd.srDiscFinal) AS LabaTotal,
		000000000000000000.00 AS JumJasaASS,
		000000000000000000.00 AS JumJasaC2,
		000000000000000000.00 AS JumJasaReguler,
		000000000000000000.00 AS JumJasaASS1,
		000000000000000000.00 AS JumJasaASS2,
		000000000000000000.00 AS JumJasaASS3,
		000000000000000000.00 AS JumJasaASS4
		FROM ttsrhd
		LEFT OUTER JOIN ttsrit ON ttsrhd.srNo = ttsrit.srNo
		LEFT OUTER JOIN tdbarang ON tdbarang.BrgKode = ttsrit.BrgKode
		WHERE DATE(ttSRhd.SRTgl) BETWEEN xTgl1 AND xTgl2
		GROUP BY DATE(ttSRhd.SRTgl),ttsrhd.KarKode;


		SET xTgl0 = DATE_ADD(LAST_DAY(DATE_ADD(xTgl1, INTERVAL -1 MONTH)), INTERVAL 1 DAY);
		SET xTglx = DATE_ADD(xTgl1, INTERVAL -1 DAY);
		IF xTgl1 = DATE_ADD(LAST_DAY(DATE_ADD(xTgl1, INTERVAL -1 MONTH)), INTERVAL 1 DAY) THEN
		  SET xTglx = DATE_ADD(LAST_DAY(DATE_ADD(xTgl1, INTERVAL -1 MONTH)), INTERVAL 1 DAY);
		END IF;
		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO  BiayaOp
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '80'
		AND ttgeneralledgerit.TglGL BETWEEN xTgl0 AND xTglx;
		SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO PendapatanNonOp
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '90'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl0 AND xTglx;
		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO BiayaOp
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '60'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl0 AND xTglx;
		SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO Pendapatan
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '40'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl0 AND xTglx;
		SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO KoreksiPenjualan
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '30' AND LEFT(ttgeneralledgerit.NoGL,2) <> 'JT'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl0 AND xTglx;
		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO KoreksiHPP
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE ((LEFT(NoAccount,2) = '50' AND LEFT(ttgeneralledgerit.NoGL,2) <> 'JT') OR NoAccount = '50050000')
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl0 AND xTglx;
		SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO SumPenjualan
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '30' AND LEFT(ttgeneralledgerit.NoGL,2) = 'JT'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl0 AND xTglx;
		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO SumHPP
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE (LEFT(NoAccount,2) = '50' AND LEFT(ttgeneralledgerit.NoGL,2) = 'JT' AND NoAccount <> '50050000' )
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl0 AND xTglx;
		SET SaldoAwal = SumPenjualan - SumHPP + Pendapatan - BiayaOp + KoreksiPenjualan - KoreksiHPP - BiayaNonOp + PendapatanNonOp;
		IF MONTH(DATE_ADD(xTgl1, INTERVAL -1 DAY)) <> MONTH(xTgl1) THEN
			 SET SaldoAwal = 0;
		END IF;

		SET BiayaOp = 0;
		SET Pendapatan = 0;
		SET PendapatanJasaKPBdanC2 = 0;
		SET KoreksiPenjualan = 0;
		SET KoreksiHPP = 0;
		SET BiayaNonOp = 0;
		SET PendapatanNonOp = 0;
		SET SumPenjualan = 0;
		SET SumHPP = 0;
		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO  BiayaOp
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '80'
		AND ttgeneralledgerit.TglGL BETWEEN xTgl1 AND xTgl2;
		SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO PendapatanNonOp
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '90'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl1 AND xTgl2;
		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO BiayaOp
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '60'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl1 AND xTgl2;
		SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO Pendapatan
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '40'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl1 AND xTgl2;
		SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO KoreksiPenjualan
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '30' AND LEFT(ttgeneralledgerit.NoGL,2) <> 'JT'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl1 AND xTgl2;
		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO KoreksiHPP
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE ((LEFT(NoAccount,2) = '50' AND LEFT(ttgeneralledgerit.NoGL,2) <> 'JT') OR NoAccount = '50050000')
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl1 AND xTgl2;
		SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO SumPenjualan
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '30' AND LEFT(ttgeneralledgerit.NoGL,2) = 'JT'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl1 AND xTgl2;
		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO SumHPP
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE (LEFT(NoAccount,2) = '50' AND LEFT(ttgeneralledgerit.NoGL,2) = 'JT' AND NoAccount <> '50050000' )
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl1 AND xTgl2;
		SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO  PendapatanJasaKPBdanC2
      FROM ttgeneralledgerit 
      INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL  
      WHERE (NoAccount = '40050000' OR NoAccount = '40060000') AND LEFT(ttgeneralledgerit.NoGL,2) = 'JT'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl1 AND xTgl2;
		
		SET @MyQuery = CONCAT("SELECT * FROM RLHDetail;");
	END IF;

	IF xTipe = "SD OutStanding" THEN
		DROP TEMPORARY TABLE IF EXISTS RLHDetail;
		CREATE TEMPORARY TABLE IF NOT EXISTS RLHDetail AS
		SELECT ttsdhd.PosKode, MAX(ttsdhd.SDNo) AS SVNo,ttsdhd.SDTgl AS SVTgl,PKBNo,KodeTrans, ASS,ttsdhd.MotorNoPolisi, tdcustomer.CusNama,ttsdhd.KarKode,
		000000000000000000.00 AS JasaASS,
		000000000000000000.00 AS JasaC2,
		000000000000000000.00 AS JasaReguler,
		000000000000000000.00 AS JasaTotal,
		000000000000000000.00 AS JasaDisc,
		000000000000000000.00 AS JasaBersih,
		SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) AS PartJual,
		SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) AS OliJual,
		SUM(IF(BrgGroup ='Oli' AND KodeTrans = 'TC14',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) AS OliJualKPB,
		SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS PartBeli,
		SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS OliBeli,
		SUM(IF(BrgGroup ='Oli' AND KodeTrans = 'TC14',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS OliBeliKPB,
		SUM(IFNULL(ttsditbarang.SDJualDisc,000000000000000000.00)) AS DiscPart,
		SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00))- SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS PartLaba,
		SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) - SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS OliLaba,
		SUM(IFNULL(ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00))- SUM(IFNULL(ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,0)) - SUM(IFNULL(ttsditbarang.SDJualDisc,000000000000000000.00)) AS PartOliLaba,
		ttsdhd.SDDiscFinal AS DiscFinal,
		000000000000000000.00 AS LabaTotal,
		000000000000000000.00 AS JumJasaASS,
		000000000000000000.00 AS JumJasaC2,
		000000000000000000.00 AS JumJasaReguler,
		000000000000000000.00 AS JumJasaASS1,
		000000000000000000.00 AS JumJasaASS2,
		000000000000000000.00 AS JumJasaASS3,
		000000000000000000.00 AS JumJasaASS4
		FROM ttsdhd
		LEFT OUTER JOIN ttsditbarang ON ttsdhd.SDNo = ttsditbarang.SDNo
		LEFT OUTER JOIN tdbarang ON tdbarang.BrgKode = ttsditbarang.BrgKode
		LEFT OUTER JOIN tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi
		WHERE DATE(ttsdhd.SDTgl) BETWEEN xTgl1 AND xTgl2  AND ttsdhd.SVNo = '--'
		GROUP BY ttsdhd.SDNo;
		CREATE INDEX SVNo ON RLHDetail(SVNo);

		/* Servis Jasa */
		DROP TEMPORARY TABLE IF EXISTS RLHDetailJasa;
		CREATE TEMPORARY TABLE IF NOT EXISTS RLHDetailJasa AS
		SELECT ttsdhd.PosKode, MAX(ttsdhd.SDNo) AS SVNo,ttsdhd.SVTgl,PKBNo,KodeTrans,ASS,ttsdhd.MotorNoPolisi, '--' AS CusNama,ttsdhd.KarKode,
		SUM(IF(LEFT(JasaGroup,3)='ASS',ttsditjasa.SDHrgJual,000000000000000000.00)) AS JasaASS,
		SUM(IF(JasaGroup='CLAIMC2',ttsditjasa.SDHrgJual,000000000000000000.00)) AS JasaC2,
		SUM(IF(LEFT(JasaGroup,3)<>'ASS' AND JasaGroup<>'CLAIMC2',ttsditjasa.SDHrgJual,0)) AS JasaReguler,
		IFNULL(SUM(ttsditjasa.SDHrgJual),000000000000000000.00) AS JasaTotal,
		IFNULL(SUM(ttsditjasa.SDJualDisc),000000000000000000.00) AS JasaDisc,
		IFNULL(SUM(ttsditjasa.SDHrgJual),000000000000000000.00)-IFNULL(SUM(ttsditjasa.SDJualDisc),000000000000000000.00) AS JasaBersih
		FROM ttsdhd
		LEFT OUTER JOIN ttsditjasa ON ttsdhd.SDNo = ttsditjasa.SDNo
		LEFT OUTER JOIN tdjasa ON tdjasa.JasaKode = ttsditjasa.JasaKode
		WHERE DATE(ttsdhd.SDTgl) BETWEEN xTgl1 AND xTgl2   AND ttsdhd.SVNo = '--'
		GROUP BY ttsdhd.SDNo;
		CREATE INDEX SVNo ON RLHDetailJasa(SVNo);

		UPDATE RLHDetail INNER JOIN RLHDetailJasa ON RLHDetail.SVNo = RLHDetailJasa.SVNo SET
		RLHDetail.JasaASS = RLHDetailJasa.JasaASS,
		RLHDetail.JasaC2 = RLHDetailJasa.JasaC2,
		RLHDetail.JasaReguler = RLHDetailJasa.JasaReguler,
		RLHDetail.JasaTotal = RLHDetailJasa.JasaTotal,
		RLHDetail.JasaDisc = RLHDetailJasa.JasaDisc,
		RLHDetail.JasaBersih = RLHDetailJasa.JasaBersih,
		RLHDetail.LabaTotal =  RLHDetail.PartOliLaba + RLHDetailJasa.JasaBersih - RLHDetail.DiscFinal;

		DROP TEMPORARY TABLE IF EXISTS RLHDetailJasa;
		CREATE TEMPORARY TABLE IF NOT EXISTS RLHDetailJasa AS
		SELECT MAX(ttsdhd.SDNo) AS SVNo,ttsdhd.SVTgl,PKBNo,KodeTrans,ASS,ttsdhd.MotorNoPolisi, '--' AS CusNama,ttsdhd.KarKode,
		SUM(IF(KodeTrans ='TC14' AND ASS = 'ASS1',1,0)) AS JumJasaASS1,
		SUM(IF(KodeTrans ='TC14' AND ASS = 'ASS2',1,0)) AS JumJasaASS2,
		SUM(IF(KodeTrans ='TC14' AND ASS = 'ASS3',1,0)) AS JumJasaASS3,
		SUM(IF(KodeTrans ='TC14' AND ASS = 'ASS4',1,0)) AS JumJasaASS4,
		SUM(IF(KodeTrans ='TC14' ,1,0)) AS JumJasaASS,
		SUM(IF(KodeTrans='TC13',1,0)) AS JumJasaC2,
		SUM(IF(KodeTrans='TC11',1,0)) AS JumJasaReguler
		FROM ttsdhd
		WHERE DATE(ttsdhd.SDTgl) BETWEEN xTgl1 AND xTgl2 AND ttsdhd.SVNo = '--'
		GROUP BY ttsdhd.SDNo;
		CREATE INDEX SVNo ON RLHDetailJasa(SVNo);

		UPDATE RLHDetail INNER JOIN RLHDetailJasa ON RLHDetail.SVNo = RLHDetailJasa.SVNo SET
		RLHDetail.JumJasaASS = RLHDetailJasa.JumJasaASS,
		RLHDetail.JumJasaC2 = RLHDetailJasa.JumJasaC2,
		RLHDetail.JumJasaReguler = RLHDetailJasa.JumJasaReguler,
		RLHDetail.JumJasaASS1 = RLHDetailJasa.JumJasaASS1,
		RLHDetail.JumJasaASS2 = RLHDetailJasa.JumJasaASS2,
		RLHDetail.JumJasaASS3 = RLHDetailJasa.JumJasaASS3,
		RLHDetail.JumJasaASS4 = RLHDetailJasa.JumJasaASS4;
				
		SET @MyQuery = CONCAT("SELECT * FROM RLHDetail;");
	END IF;

	IF xTipe = "SV SI SR Mekanik" THEN
		DROP TEMPORARY TABLE IF EXISTS RLHDetail;
		CREATE TEMPORARY TABLE IF NOT EXISTS RLHDetail AS
		SELECT ttsdhd.PosKode, MAX(ttsdhd.SVNo) AS SVNo,ttsdhd.SVTgl,PKBNo,KodeTrans, ASS,ttsdhd.MotorNoPolisi, tdcustomer.CusNama,ttsdhd.KarKode,
		000000000000000000.00 AS JasaASS,
		000000000000000000.00 AS JasaC2,
		000000000000000000.00 AS JasaReguler,
		000000000000000000.00 AS JasaTotal,
		000000000000000000.00 AS JasaDisc,
		000000000000000000.00 AS JasaBersih,
		SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) AS PartJual,
		SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) AS OliJual,
		SUM(IF(BrgGroup ='Oli' AND KodeTrans = 'TC14',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) AS OliJualKPB,
		SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS PartBeli,
		SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS OliBeli,
		SUM(IF(BrgGroup ='Oli' AND KodeTrans = 'TC14',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS OliBeliKPB,
		SUM(IFNULL(ttsditbarang.SDJualDisc,000000000000000000.00)) AS DiscPart,
		SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00))- SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS PartLaba,
		SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) - SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS OliLaba,
		SUM(IFNULL(ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00))- SUM(IFNULL(ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,0)) - SUM(IFNULL(ttsditbarang.SDJualDisc,000000000000000000.00)) AS PartOliLaba,
		ttsdhd.SDDiscFinal AS DiscFinal,
		000000000000000000.00 AS LabaTotal,
		000000000000000000.00 AS JumJasaASS,
		000000000000000000.00 AS JumJasaC2,
		000000000000000000.00 AS JumJasaReguler,
		000000000000000000.00 AS JumJasaASS1,
		000000000000000000.00 AS JumJasaASS2,
		000000000000000000.00 AS JumJasaASS3,
		000000000000000000.00 AS JumJasaASS4
		FROM ttsdhd
		LEFT OUTER JOIN ttsditbarang ON ttsdhd.SDNo = ttsditbarang.SDNo
		LEFT OUTER JOIN tdbarang ON tdbarang.BrgKode = ttsditbarang.BrgKode
		LEFT OUTER JOIN tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi
		WHERE ttsdhd.SVTgl BETWEEN DATE_ADD(CURDATE(), INTERVAL -5 DAY) AND CURDATE();
		CREATE INDEX SVNo ON RLHDetail(SVNo);
		DELETE FROM RLHDetail;

		IF xJenis = "%" OR xJenis = "SV" THEN
			INSERT INTO RLHDetail
			SELECT ttsdhd.PosKode, ttsdhd.SVNo,ttsdhd.SVTgl,PKBNo,KodeTrans, ASS,ttsdhd.MotorNoPolisi, tdcustomer.CusNama,ttsdhd.KarKode,
			000000000000000000.00 AS JasaASS,
			000000000000000000.00 AS JasaC2,
			000000000000000000.00 AS JasaReguler,
			000000000000000000.00 AS JasaTotal,
			000000000000000000.00 AS JasaDisc,
			000000000000000000.00 AS JasaBersih,
			SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) AS PartJual,
			SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) AS OliJual,
			SUM(IF(BrgGroup ='Oli' AND KodeTrans = 'TC14',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) AS OliJualKPB,
			SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS PartBeli,
			SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS OliBeli,
			SUM(IF(BrgGroup ='Oli' AND KodeTrans = 'TC14',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS OliBeliKPB,
			SUM(IFNULL(ttsditbarang.SDJualDisc,000000000000000000.00)) AS DiscPart,
			SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00))- SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS PartLaba,
			SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) - SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS OliLaba,
			SUM(IFNULL(ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00))- SUM(IFNULL(ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,0)) - SUM(IFNULL(ttsditbarang.SDJualDisc,000000000000000000.00)) AS PartOliLaba,
			ttsdhd.SDDiscFinal AS DiscFinal,
			000000000000000000.00 AS LabaTotal,
			000000000000000000.00 AS JumJasaASS,
			000000000000000000.00 AS JumJasaC2,
			000000000000000000.00 AS JumJasaReguler,
			000000000000000000.00 AS JumJasaASS1,
			000000000000000000.00 AS JumJasaASS2,
			000000000000000000.00 AS JumJasaASS3,
			000000000000000000.00 AS JumJasaASS4
			FROM ttsdhd
			LEFT OUTER JOIN ttsditbarang ON ttsdhd.SDNo = ttsditbarang.SDNo
			LEFT OUTER JOIN tdbarang ON tdbarang.BrgKode = ttsditbarang.BrgKode
			LEFT OUTER JOIN tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi
			WHERE DATE(ttsdhd.SVTgl) BETWEEN xTgl1 AND xTgl2
			GROUP BY ttsdhd.SDNo ORDER BY ttsdhd.SDNo;

			DROP TEMPORARY TABLE IF EXISTS RLHDetailJasa;
			CREATE TEMPORARY TABLE IF NOT EXISTS RLHDetailJasa AS
			SELECT ttsdhd.PosKode, ttsdhd.SVNo,ttsdhd.SVTgl,PKBNo,KodeTrans,ASS,ttsdhd.MotorNoPolisi, '--' AS CusNama,ttsdhd.KarKode,
			SUM(IF(LEFT(JasaGroup,3)='ASS',ttsditjasa.SDHrgJual,000000000000000000.00)) AS JasaASS,
			SUM(IF(JasaGroup='CLAIMC2',ttsditjasa.SDHrgJual,000000000000000000.00)) AS JasaC2,
			SUM(IF(LEFT(JasaGroup,3)<>'ASS' AND JasaGroup<>'CLAIMC2',ttsditjasa.SDHrgJual,0)) AS JasaReguler,
			IFNULL(SUM(ttsditjasa.SDHrgJual),000000000000000000.00) AS JasaTotal,
			IFNULL(SUM(ttsditjasa.SDJualDisc),000000000000000000.00) AS JasaDisc,
			IFNULL(SUM(ttsditjasa.SDHrgJual),000000000000000000.00)-IFNULL(SUM(ttsditjasa.SDJualDisc),000000000000000000.00) AS JasaBersih
			FROM ttsdhd
			LEFT OUTER JOIN ttsditjasa ON ttsdhd.SDNo = ttsditjasa.SDNo
			LEFT OUTER JOIN tdjasa ON tdjasa.JasaKode = ttsditjasa.JasaKode
			WHERE DATE(ttsdhd.SVTgl) BETWEEN xTgl1 AND xTgl2
			GROUP BY ttsdhd.SDNo ORDER BY ttsdhd.SDNo;
			CREATE INDEX SVNo ON RLHDetailJasa(SVNo);

			UPDATE RLHDetail INNER JOIN RLHDetailJasa ON RLHDetail.SVNo = RLHDetailJasa.SVNo SET
			RLHDetail.JasaASS = RLHDetailJasa.JasaASS,
			RLHDetail.JasaC2 = RLHDetailJasa.JasaC2,
			RLHDetail.JasaReguler = RLHDetailJasa.JasaReguler,
			RLHDetail.JasaTotal = RLHDetailJasa.JasaTotal,
			RLHDetail.JasaDisc = RLHDetailJasa.JasaDisc,
			RLHDetail.JasaBersih = RLHDetailJasa.JasaBersih,
			RLHDetail.LabaTotal =  RLHDetail.PartOliLaba + RLHDetailJasa.JasaBersih - RLHDetail.DiscFinal;
		END IF;

		IF xJenis = "%" OR xJenis = "SI" THEN
			INSERT INTO RLHDetail
			SELECT ttSIhd.PosKode, MAX(ttSIhd.SINo) AS SVNo, ttSIhd.SITgl AS SVTgl, ttSIhd.SONo AS PKBNo, KodeTrans, SIJenis AS ASS, ttsihd.MotorNoPolisi, '--' AS CusNama,ttsihd.KarKode,
			000000000000000000.00 AS JasaASS,
			000000000000000000.00 AS JasaC2,
			000000000000000000.00 AS JasaReguler,
			000000000000000000.00 AS JasaTotal,
			000000000000000000.00 AS JasaDisc,
			000000000000000000.00 AS JasaBersih,
			SUM(IF(BrgGroup <>'Oli',ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00)) AS PartJual,
			SUM(IF(BrgGroup ='Oli',ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00)) AS OliJual,
			000000000000000000.00 AS OliJualKPB,
			SUM(IF(BrgGroup <>'Oli',ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) AS PartBeli,
			SUM(IF(BrgGroup ='Oli',ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) AS OliBeli,
			000000000000000000.00 AS OliBeliKPB,
			SUM(IFNULL(ttsiit.SIDiscount,000000000000000000.00)) AS DiscPart,
			SUM(IF(BrgGroup <>'Oli',ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00))- SUM(IF(BrgGroup <>'Oli',ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) AS PartLaba,
			SUM(IF(BrgGroup ='Oli',ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00)) - SUM(IF(BrgGroup ='Oli',ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) AS OliLaba,
			SUM(IFNULL(ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00))- SUM(IFNULL(ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) - SUM(IFNULL(ttsiit.SIDiscount,000000000000000000.00)) AS PartOliLaba,
			ttSIhd.SIDiscFinal AS DiscFinal,
			SUM(IFNULL(ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00))- SUM(IFNULL(ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) - SUM(IFNULL(ttsiit.SIDiscount,000000000000000000.00))-ttSIhd.SIDiscFinal AS LabaTotal,
			000000000000000000.00 AS JumJasaASS,
			000000000000000000.00 AS JumJasaC2,
			000000000000000000.00 AS JumJasaReguler,
			000000000000000000.00 AS JumJasaASS1,
			000000000000000000.00 AS JumJasaASS2,
			000000000000000000.00 AS JumJasaASS3,
			000000000000000000.00 AS JumJasaASS4
			FROM ttSIhd
			LEFT OUTER JOIN ttsiit ON ttSIhd.SINo = ttsiit.SINo
			LEFT OUTER JOIN tdbarang ON tdbarang.BrgKode = ttsiit.BrgKode
			WHERE DATE(ttSIhd.SITgl) BETWEEN xTgl1 AND xTgl2
			GROUP BY ttSIhd.SINo;
		END IF;

		IF xJenis = "%" OR xJenis = "SR" THEN
			INSERT INTO RLHDetail
			SELECT ttsrhd.PosKode, MAX(ttsrhd.SRNo) AS SVNo, ttsrhd.srTgl AS SVTgl,'--' AS PKBNo, KodeTrans,'SR' AS ASS, ttsrhd.MotorNoPolisi, '--' AS CusNama,ttsrhd.KarKode,
			000000000000000000.00 AS JasaASS,
			000000000000000000.00 AS JasaC2,
			000000000000000000.00 AS JasaReguler,
			000000000000000000.00 AS JasaTotal,
			000000000000000000.00 AS JasaDisc,
			000000000000000000.00 AS JasaBersih,
			-SUM(IF(BrgGroup <>'Oli',ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00)) AS PartJual,
			-SUM(IF(BrgGroup ='Oli',ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00)) AS OliJual,
			000000000000000000.00 AS OliJualKPB,
			-SUM(IF(BrgGroup <>'Oli',ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) AS PartBeli,
			-SUM(IF(BrgGroup ='Oli',ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) AS OliBeli,
			000000000000000000.00 AS OliBeliKPB,
			-SUM(IFNULL(ttsrit.srDiscount,000000000000000000.00)) AS DiscPart,
			-SUM(IF(BrgGroup <>'Oli',ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00))- SUM(IF(BrgGroup <>'Oli',ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) AS PartLaba,
			-SUM(IF(BrgGroup ='Oli',ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00)) - SUM(IF(BrgGroup ='Oli',ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) AS OliLaba,
			-(SUM(IFNULL(ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00))- SUM(IFNULL(ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) - SUM(IFNULL(ttsrit.srDiscount,000000000000000000.00))) AS PartOliLaba,
			-ttsrhd.srDiscFinal AS DiscFinal,
			-(SUM(IFNULL(ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00))- SUM(IFNULL(ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) - SUM(IFNULL(ttsrit.srDiscount,000000000000000000.00))-ttsrhd.srDiscFinal) AS LabaTotal,
			000000000000000000.00 AS JumJasaASS,
			000000000000000000.00 AS JumJasaC2,
			000000000000000000.00 AS JumJasaReguler,
			000000000000000000.00 AS JumJasaASS1,
			000000000000000000.00 AS JumJasaASS2,
			000000000000000000.00 AS JumJasaASS3,
			000000000000000000.00 AS JumJasaASS4
			FROM ttsrhd
			LEFT OUTER JOIN ttsrit ON ttsrhd.srNo = ttsrit.srNo
			LEFT OUTER JOIN tdbarang ON tdbarang.BrgKode = ttsrit.BrgKode
			WHERE DATE(ttSRhd.SRTgl) BETWEEN xTgl1 AND xTgl2
			GROUP BY ttSRhd.SRNo;
 		END IF;
		SET @MyQuery = CONCAT("SELECT * FROM RLHDetail;");
	END IF;

	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;  
END$$
DELIMITER ;

