DELIMITER $$
DROP PROCEDURE IF EXISTS `rPenerimaanBarang`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rPenerimaanBarang`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,
IN xTC VARCHAR(100),IN xSupplier VARCHAR(100),IN xStatus VARCHAR(100),
IN xSort VARCHAR(150),IN xOrder VARCHAR(10))
BEGIN
/*
		CALL rPenerimaanBarang('Header1','2020-01-01','2020-05-31','','','','PSTgl','ASC');
*/
	IF xTipe = "Header1" THEN
		SET @MyQuery = CONCAT("
		SELECT ttpshd.PSNo, ttpshd.PSTgl, ttpshd.SupKode, ttpshd.LokasiKode, ttpshd.PSKeterangan, ttpshd.PSSubTotal, ttpshd.PSDiscFinal, ttpshd.PSTotalPajak, ttpshd.PSBiayaKirim, ttpshd.PSTotal, ttpshd.UserID, ttpshd.NoGL, 
		ttpshd.KodeTrans, tdsupplier.SupNama, ttpshd.PINo, IFNULL(ttpihd.PITgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS PITgl, ttpshd.PONo, IFNULL(ttpohd.POTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS POTgl, 
		ttpshd.PosKode, ttpshd.Cetak, ttpshd.PSNoRef, ttpshd.DealerKode, ttpshd.PSTerbayar, IFNULL(tddealer.DealerNama, '--') AS DealerNama, ttpshd.TINo
      FROM ttpshd INNER JOIN
      tdsupplier ON ttpshd.SupKode = tdsupplier.SupKode LEFT OUTER JOIN
      tddealer ON ttpshd.DealerKode = tddealer.DealerKode LEFT OUTER JOIN
      ttpihd ON ttpihd.PSNo = ttpshd.PSNo OR ttpihd.PINo = ttpshd.PINo LEFT OUTER JOIN
      ttpohd ON ttpohd.PONo = ttpohd.PONo
      WHERE DATE(ttpshd.PSTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'") ;
      SET @MyQuery = CONCAT(@MyQuery,"  AND ttpshd.KodeTrans LIKE '%", xTC ,"' ");
      SET @MyQuery = CONCAT(@MyQuery,"  AND ttpshd.SupKode LIKE '%", xSupplier ,"' ");
      SET @MyQuery = CONCAT(@MyQuery, xStatus );
      SET @MyQuery = CONCAT(@MyQuery, " GROUP BY ttpshd.PSNo " );
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
	END IF;
	IF xTipe = "Item1" OR xTipe = "Item2" THEN
		SET @MyQuery = CONCAT("
		SELECT  ttpsit.PSNo, ttpsit.PSAuto, ttpsit.BrgKode, ttpsit.PSQty, ttpsit.PSHrgBeli, ttpsit.PSHrgJual, ttpsit.PSDiscount, ttpsit.PSPajak, 
      tdbarang.BrgNama, tdbarang.BrgSatuan, 0 AS Disc, ttpsit.PSQty * ttpsit.PSHrgJual - ttpsit.PSDiscount AS Jumlah, 
      ttpsit.LokasiKode, tdbarang.BrgGroup, ttpsit.PONo, ttpsit.PINo,PIQtyS, ttpihd.PITgl, ttpshd.PSTgl
      FROM ttpsit 
      INNER JOIN tdbarang ON ttpsit.BrgKode = tdbarang.BrgKode
      INNER JOIN ttpshd ON ttpsit.PSNo = ttpshd.PSNo
      LEFT OUTER JOIN ttpihd ON ttpsit.PINo = ttpihd.PINo
		WHERE DATE(ttpshd.PSTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery,"  AND ttpshd.KodeTrans LIKE '%", xTC ,"' ");
      SET @MyQuery = CONCAT(@MyQuery,"  AND ttpshd.SupKode LIKE '%", xSupplier ,"' ");
      SET @MyQuery = CONCAT(@MyQuery, xStatus );
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY ttpsit.PSNo, ttpsit.PSAuto," ,xSort," ",xOrder );    
	END IF;
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$
DELIMITER ;

