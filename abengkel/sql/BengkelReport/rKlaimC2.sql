DELIMITER $$
DROP PROCEDURE IF EXISTS `rKlaimC2`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rKlaimC2`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,
IN xSort VARCHAR(150),IN xOrder VARCHAR(10))
BEGIN
/*	CALL rKlaimC2('Header1','2020-01-01','2020-05-31','CCTgl','ASC'); */
	IF xTipe = "Header1" THEN
		SET @MyQuery = CONCAT("
		SELECT CCNo, CCTgl, CCTotalPart, CCTotalJasa, CCTotalPart + CCTotalJasa AS CCTotal, CCNoKlaim, CCMemo, NoGL, Cetak, UserID FROM ttcchd                       
      WHERE DATE(ttcchd.CCTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
	END IF;
	IF xTipe = "Item1" THEN
		SET @MyQuery = CONCAT("
		SELECT ttccit.CCNo, ttccit.SDNo, ttccit.CCPart, ttccit.CCJasa, ttsdhd.SDTgl, ttsdhd.MotorNoPolisi, 
         ttccit.CCPart + ttccit.CCJasa AS CCTotal, tdcustomer.MotorType, tdcustomer.MotorNama, ttsdhd.SVNo, tdcustomer.CusNama, ttcchd.CCTgl
         FROM  ttccit INNER JOIN
         ttsdhd ON ttccit.SDNo = ttsdhd.SDNo INNER JOIN
         tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi INNER JOIN
         ttcchd ON ttccit.CCNo = ttcchd.CCNo
      WHERE DATE(ttcchd.CCTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
	END IF;

	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$
DELIMITER ;

