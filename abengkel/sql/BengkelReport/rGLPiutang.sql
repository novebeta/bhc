DELIMITER $$
DROP PROCEDURE IF EXISTS `rGLPiutang`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rGLPiutang`(IN xTipe VARCHAR(50),IN xTgl1 DATE,IN xTgl2 DATE,
IN xPerkiraan1 VARCHAR(10),IN xPerkiraan2 VARCHAR(10), IN xStatus VARCHAR(5), IN xSensitif VARCHAR(1), IN xNoBukti VARCHAR(30))
BEGIN
/*
	CALL rGLPiutang('Saldo Piutang','2020-01-01','2020-02-29','%','%','Belum','0','');
*/
	IF xTipe = "Saldo Piutang" THEN
		IF xSensitif = "1" THEN
			DELETE FROM tvpiutangdebet ;
			INSERT INTO tvpiutangdebet  SELECT * FROM vpiutangdebet ; 
		END IF;
		DROP TEMPORARY TABLE IF EXISTS tvpiutangbayar0 ;
		CREATE TEMPORARY TABLE IF NOT EXISTS tvpiutangbayar0 AS
		SELECT GLLink AS HPLink, NoAccount, SUM(HPNilai) AS SumKreditGL FROM ttglhpit
		WHERE (NOAccount BETWEEN '11060000' AND '11150000') AND HPJenis = 'Piutang' AND TglGL <= xTgl2
		GROUP BY HPLink, NoAccount ;
		CREATE INDEX HPLink ON tvpiutangbayar0(HPLink);
		CREATE INDEX NoAccount ON tvpiutangbayar0(NoAccount);

    CASE xStatus
			WHEN "Belum" THEN
				SET @LunasScript = " tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) <> 0 AND ";
			WHEN "Lunas" THEN
				SET @LunasScript = " tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) = 0 AND ";
			ELSE
				SET @LunasScript = " ";
		END CASE;

		DROP TEMPORARY TABLE IF EXISTS tPiutangNonLease;
    IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS tPiutangNonLease AS
			SELECT tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount,
			tvpiutangdebet.NamaAccount, tvpiutangdebet.KeteranganGL, tvpiutangdebet.DebetGL,
			IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangLunas, tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangSisa
			FROM tvpiutangdebet
			LEFT OUTER JOIN tvpiutangbayar0  ON tvpiutangdebet.GLLink = tvpiutangbayar0.HPLink AND tvpiutangdebet.NoAccount = tvpiutangbayar0.NoAccount
			WHERE ",@LunasScript);
			SET @MyQuery = CONCAT(@MyQuery," (tvpiutangdebet.TglGL <= '",xTgl2,"') ");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY tvpiutangdebet.NoAccount, tvpiutangdebet.TglGL, tvpiutangdebet.GLLink  ");
		ELSE
			SET @MyQuery = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS tPiutangNonLease AS
			SELECT tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount,
			tvpiutangdebet.NamaAccount, tvpiutangdebet.KeteranganGL, tvpiutangdebet.DebetGL,
			IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangLunas, tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangSisa
			FROM tvpiutangdebet
			LEFT OUTER JOIN tvpiutangbayar0  ON tvpiutangdebet.GLLink = tvpiutangbayar0.HPLink AND tvpiutangdebet.NoAccount = tvpiutangbayar0.NoAccount
			WHERE ",@LunasScript);
			SET @MyQuery = CONCAT(@MyQuery," (tvpiutangdebet.TglGL <= '",xTgl2,"') ");
			SET @MyQuery = CONCAT(@MyQuery," AND tvpiutangdebet.NoAccount BETWEEN '" , xPerkiraan1 , "' AND '", xPerkiraan2, "' ");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY tvpiutangdebet.NoAccount, tvpiutangdebet.TglGL, tvpiutangdebet.GLLink  ");
		END IF;
		PREPARE STMT FROM @MyQuery;
		EXECUTE Stmt;
		SET @MyQuery = "SELECT * FROM tPiutangNonLease;";
	END IF;

	IF xTipe = "Mutasi Debet Piutang" THEN
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT("SELECT vglall.* FROM vglall
			WHERE vglall.KodeTrans <> 'NA' AND (vglall.NoAccount BETWEEN '11060000' AND '11150000') AND vglall.DebetGL > 0");
			SET @MyQuery = CONCAT(@MyQuery," AND vglall.TglGL BETWEEN '",xTgl1,"' AND '", xTgl2 , "'");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY   NoAccount , GLLink ");
		ELSE
			SET @MyQuery = CONCAT("SELECT vglall.* FROM vglall
			WHERE vglall.KodeTrans <> 'NA' AND (vglall.NoAccount BETWEEN '11060000' AND '11150000') AND vglall.DebetGL > 0");
			SET @MyQuery = CONCAT(@MyQuery," AND vglall.NoAccount BETWEEN '" , xPerkiraan1 , "' AND '", xPerkiraan2, "' ");
			SET @MyQuery = CONCAT(@MyQuery," AND vglall.TglGL BETWEEN '",xTgl1,"' AND '", xTgl2 , "'");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY   NoAccount , GLLink ");
		END IF;
	END IF;
	
	IF xTipe = "Mutasi Kredit Piutang" THEN
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT("SELECT vglall.* FROM vglall
			WHERE vglall.KodeTrans <> 'NA' AND (vglall.NoAccount BETWEEN '11060000' AND '11150000') AND vglall.KreditGL > 0 ");
			SET @MyQuery = CONCAT(@MyQuery," AND vglall.TglGL BETWEEN '",xTgl1,"' AND '", xTgl2 , "'");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY   NoAccount , GLLink ");
		ELSE
			SET @MyQuery = CONCAT("SELECT vglall.* FROM vglall
			WHERE vglall.KodeTrans <> 'NA' AND (vglall.NoAccount BETWEEN '11060000' AND '11150000') AND vglall.KreditGL > 0  ");
			SET @MyQuery = CONCAT(@MyQuery," AND vglall.NoAccount BETWEEN '" , xPerkiraan1 , "' AND '", xPerkiraan2, "' ");
			SET @MyQuery = CONCAT(@MyQuery," AND vglall.TglGL BETWEEN '",xTgl1,"' AND '", xTgl2 , "'");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY   NoAccount , GLLink ");
		END IF;
	END IF;

	IF xTipe = "Umur Piutang - Curent" THEN
		IF xSensitif = "1" THEN
			DELETE FROM tvpiutangdebet ;
			INSERT INTO tvpiutangdebet  SELECT * FROM vpiutangdebet ;
		END IF;
		DROP TEMPORARY TABLE IF EXISTS tvpiutangbayar0 ;
		CREATE TEMPORARY TABLE IF NOT EXISTS tvpiutangbayar0 AS
		SELECT GLLink AS HPLink, NoAccount, SUM(HPNilai) AS SumKreditGL FROM ttglhpit
		WHERE (NOAccount BETWEEN '11060000' AND '11150000') AND HPJenis = 'Piutang' AND TglGL <= xTgl2
		GROUP BY HPLink, NoAccount ;
		CREATE INDEX HPLink ON tvpiutangbayar0(HPLink);
		CREATE INDEX NoAccount ON tvpiutangbayar0(NoAccount);

      DROP TEMPORARY TABLE IF EXISTS MyClient;
      CREATE TEMPORARY TABLE IF NOT EXISTS MyClient AS
      SELECT SVNo AS SKNo, SVTgl AS SKTgl, DATE_ADD(SVTgl, INTERVAL SDTOP DAY) AS TglTempo, CusNama, MotorType, ttsdhd.MotorNoPolisi AS SalesNama
      FROM ttsdhd INNER JOIN tdcustomer ON tdcustomer.MotorNoPolisi = ttsdhd.MotorNoPolisi
      UNION
      SELECT SINo AS SKNo, SITgl AS SKTgl, DATE_ADD(SITgl, INTERVAL SITOP DAY) AS TglTempo, CusNama, MotorType, ttsihd.MotorNoPolisi AS SalesNama
      FROM ttsihd INNER JOIN tdcustomer ON tdcustomer.MotorNoPolisi = ttsihd.MotorNoPolisi;
		CREATE INDEX SKNo ON MyClient(SKNo);

      DROP TEMPORARY TABLE IF EXISTS Piutang;
      CREATE TEMPORARY TABLE IF NOT EXISTS Piutang AS
      SELECT tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount, tvpiutangdebet.NamaAccount, tvpiutangdebet.KeteranganGL, tvpiutangdebet.DebetGL,
      IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangLunas, tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangSisa, 0 AS OD, 0 AS CR, 0 AS R1, 0 AS R2, 0 AS R3, 0 AS R4
      FROM tvpiutangdebet
      LEFT OUTER JOIN tvpiutangbayar0 ON tvpiutangdebet.GLLink = tvpiutangbayar0.HPLink AND tvpiutangdebet.NoAccount = tvpiutangbayar0.NoAccount
      WHERE tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) > 0 AND tvpiutangdebet.KodeTrans LIKE '%';
 		CREATE INDEX GLLink ON Piutang(GLLink);

		DROP TEMPORARY TABLE IF EXISTS tPiutangLease;
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT("SELECT *
			FROM Piutang INNER JOIN MyClient ON Piutang.GLLink = MyClient.SKNo ");
			SET @MyQuery = CONCAT(@MyQuery,"WHERE (TglGL <= '",xTgl2,"') ");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY NoAccount, TglGL ");
		ELSE
			SET @MyQuery = CONCAT("SELECT *
			FROM Piutang INNER JOIN MyClient ON Piutang.GLLink = MyClient.SKNo ");
			SET @MyQuery = CONCAT(@MyQuery,"WHERE (TglGL <= '",xTgl2,"') ");
			SET @MyQuery = CONCAT(@MyQuery," AND NoAccount BETWEEN '" , xPerkiraan1 , "' AND '", xPerkiraan2, "' ");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY NoAccount, TglGL ");
		END IF;
		
	END IF;

	IF xTipe = "Umur Piutang - Overdue" THEN
		IF xSensitif = "1" THEN
			DELETE FROM tvpiutangdebet ;
			INSERT INTO tvpiutangdebet  SELECT * FROM vpiutangdebet ;
		END IF;
		DROP TEMPORARY TABLE IF EXISTS tvpiutangbayar0 ;
		CREATE TEMPORARY TABLE IF NOT EXISTS tvpiutangbayar0 AS
		SELECT GLLink AS HPLink, NoAccount, SUM(HPNilai) AS SumKreditGL FROM ttglhpit
		WHERE (NOAccount BETWEEN '11060000' AND '11150000') AND HPJenis = 'Piutang' AND TglGL <= xTgl2
		GROUP BY HPLink, NoAccount ;
		CREATE INDEX HPLink ON tvpiutangbayar0(HPLink);
		CREATE INDEX NoAccount ON tvpiutangbayar0(NoAccount);

      DROP TEMPORARY TABLE IF EXISTS MyClient;
      CREATE TEMPORARY TABLE IF NOT EXISTS MyClient AS
      SELECT SVNo AS SKNo, SVTgl AS SKTgl, DATE_ADD(SVTgl, INTERVAL SDTOP DAY) AS TglTempo, CusNama, MotorType, ttsdhd.MotorNoPolisi AS SalesNama
      FROM ttsdhd INNER JOIN tdcustomer ON tdcustomer.MotorNoPolisi = ttsdhd.MotorNoPolisi
      UNION
      SELECT SINo AS SKNo, SITgl AS SKTgl, DATE_ADD(SITgl, INTERVAL SITOP DAY) AS TglTempo, CusNama, MotorType, ttsihd.MotorNoPolisi AS SalesNama
      FROM ttsihd INNER JOIN tdcustomer ON tdcustomer.MotorNoPolisi = ttsihd.MotorNoPolisi;
		CREATE INDEX SKNo ON MyClient(SKNo);

      DROP TEMPORARY TABLE IF EXISTS Piutang;
      CREATE TEMPORARY TABLE IF NOT EXISTS Piutang AS
      SELECT tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount, tvpiutangdebet.NamaAccount, tvpiutangdebet.KeteranganGL, tvpiutangdebet.DebetGL,
      IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangLunas, tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangSisa, 0 AS OD, 0 AS CR, 0 AS R1, 0 AS R2, 0 AS R3, 0 AS R4
      FROM tvpiutangdebet
      LEFT OUTER JOIN tvpiutangbayar0 ON tvpiutangdebet.GLLink = tvpiutangbayar0.HPLink AND tvpiutangdebet.NoAccount = tvpiutangbayar0.NoAccount
      WHERE tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) > 0 AND tvpiutangdebet.KodeTrans LIKE '%';
 		CREATE INDEX GLLink ON Piutang(GLLink);

		DROP TEMPORARY TABLE IF EXISTS tPiutangLease;
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT("SELECT * 
			FROM Piutang INNER JOIN MyClient ON Piutang.GLLink = MyClient.SKNo ");
			SET @MyQuery = CONCAT(@MyQuery,"WHERE (TglGL <= '",xTgl2,"') ");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY NoAccount, TglGL ");
		ELSE
  		SET @MyQuery = CONCAT("SELECT *
			FROM Piutang INNER JOIN MyClient ON Piutang.GLLink = MyClient.SKNo ");
			SET @MyQuery = CONCAT(@MyQuery,"WHERE (TglGL <= '",xTgl2,"') ");
			SET @MyQuery = CONCAT(@MyQuery," AND NoAccount BETWEEN '" , xPerkiraan1 , "' AND '", xPerkiraan2, "' ");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY NoAccount, TglGL ");
		END IF;
	END IF;
	
	IF xTipe = "Riwayat Piutang" THEN
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			DROP TEMPORARY TABLE IF EXISTS vpiutangbayar;
			CREATE TEMPORARY TABLE IF NOT EXISTS vpiutangbayar AS
			SELECT ttgeneralledgerhd.GLLink AS NoPelunasan, ttglhpit.TglGL, ttglhpit.GLLink AS HPLink, NoAccount, HPNilai AS KreditGL, MemoGL 
         FROM ttglhpit  
               INNER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.NoGL = ttglhpit.NoGL AND ttgeneralledgerhd.TglGL = ttglhpit.TglGL 
					WHERE (NOAccount BETWEEN '11060000' AND '11150000') 
					AND HPJenis = 'Piutang' AND ttglhpit.TglGL BETWEEN xTgl1 AND xTgl2;
			CREATE INDEX HPLink ON vpiutangbayar(HPLink);
			CREATE INDEX NoAccount ON vpiutangbayar(NoAccount);
	
			SET @MyQuery = CONCAT("SELECT tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount, tvpiutangdebet.NamaAccount, 
			tvpiutangdebet.KeteranganGL, tvpiutangdebet.DebetGL, IFNULL(vpiutangbayar.NoPelunasan,'--') AS NoPelunasan, vpiutangbayar.TglGL AS TglPelunasan, 
			IFNULL(vpiutangbayar.MemoGL,'--') AS MemoPelunasan, IFNULL(vpiutangbayar.KreditGL,0) AS KreditGL 
         FROM tvpiutangdebet 
         LEFT OUTER JOIN vpiutangbayar ",
         "ON tvpiutangdebet.GLLink = vpiutangbayar.HPLink AND tvpiutangdebet.NoAccount = vpiutangbayar.NoAccount ");							
			SET @MyQuery = CONCAT(@MyQuery," WHERE tvpiutangdebet.TglGL  BETWEEN '",xTgl1,"' AND '", xTgl2 , "'");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY NoAccount, TglGL ");
		ELSE
			DROP TEMPORARY TABLE IF EXISTS vpiutangbayar;
			CREATE TEMPORARY TABLE IF NOT EXISTS vpiutangbayar AS
			SELECT ttgeneralledgerhd.GLLink AS NoPelunasan, ttglhpit.TglGL, ttglhpit.GLLink AS HPLink, NoAccount, HPNilai AS KreditGL, MemoGL 
         FROM ttglhpit  
               INNER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.NoGL = ttglhpit.NoGL AND ttgeneralledgerhd.TglGL = ttglhpit.TglGL 
					WHERE (NOAccount BETWEEN '11060000' AND '11150000') 
					AND HPJenis = 'Piutang' AND ttglhpit.TglGL BETWEEN xTgl1 AND xTgl2;
			CREATE INDEX HPLink ON vpiutangbayar(HPLink);
			CREATE INDEX NoAccount ON vpiutangbayar(NoAccount);

			SET @MyQuery = CONCAT("SELECT tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount, tvpiutangdebet.NamaAccount, 
			tvpiutangdebet.KeteranganGL, tvpiutangdebet.DebetGL, IFNULL(vpiutangbayar.NoPelunasan,'--') AS NoPelunasan, vpiutangbayar.TglGL AS TglPelunasan, 
			IFNULL(vpiutangbayar.MemoGL,'--') AS MemoPelunasan, IFNULL(vpiutangbayar.KreditGL,0) AS KreditGL 
         FROM tvpiutangdebet 
         LEFT OUTER JOIN vpiutangbayar ",
			"ON tvpiutangdebet.GLLink = vpiutangbayar.HPLink AND tvpiutangdebet.NoAccount = vpiutangbayar.NoAccount ");							
			SET @MyQuery = CONCAT(@MyQuery," WHERE tvpiutangdebet.TglGL  BETWEEN '",xTgl1,"' AND '", xTgl2 , "'");
			SET @MyQuery = CONCAT(@MyQuery," AND tvpiutangdebet.NoAccount BETWEEN '" , xPerkiraan1 , "' AND '", xPerkiraan2, "' ");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY NoAccount, TglGL ");
		END IF;
		IF xNoBukti <> "" AND LENGTH(xNoBukti) >= 10 THEN
		SET @MyQuery = CONCAT("SELECT vpiutangdebet.GLLink, vpiutangdebet.NoGL, vpiutangdebet.TglGL, vpiutangdebet.NoAccount, vpiutangdebet.NamaAccount, 
			vpiutangdebet.KeteranganGL, vpiutangdebet.DebetGL, IFNULL(vpiutangbayar.NoPelunasan,'--') AS NoPelunasan, vpiutangbayar.TglGL AS TglPelunasan, 
			IFNULL(vpiutangbayar.MemoGL,'--') AS MemoPelunasan, IFNULL(vpiutangbayar.KreditGL,0) AS KreditGL 
         FROM vpiutangdebet 
         LEFT OUTER JOIN 
             (SELECT ttgeneralledgerhd.GLLink AS NoPelunasan, ttglhpit.TglGL, ttglhpit.GLLink AS HPLink, NoAccount, HPNilai AS KreditGL, MemoGL 
               FROM ttglhpit  
               INNER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.NoGL = ttglhpit.NoGL AND ttgeneralledgerhd.TglGL = ttglhpit.TglGL 
					WHERE (NOAccount BETWEEN '11060000' AND '11150000') 
					AND HPJenis = 'Piutang' AND ttglhpit.GLLink = '" , xNoBukti , "') vpiutangbayar ",
					"ON vpiutangdebet.GLLink = vpiutangbayar.HPLink AND vpiutangdebet.NoAccount = vpiutangbayar.NoAccount ");							
			SET @MyQuery = CONCAT(@MyQuery," WHERE vpiutangdebet.GLLink = '" , xNoBukti , "'");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY NoAccount, TglGL ");
		END IF;
	END IF;

	IF xTipe = "Perincian Pelunasan"  THEN
		SET @MyQuery = CONCAT("SELECT ttglhpit.NoGL, ttglhpit.TglGL, ttglhpit.GLLink AS HutangPiutang,  ttgeneralledgerhd.GLLink AS Pelunasan, ttglhpit.NoAccount, 
		ttgeneralledgerhd.MemoGL AS NamaAccount, HPJenis, HPNilai
      FROM ttglhpit INNER JOIN ttgeneralledgerhd ON ttglhpit.NoGL = ttgeneralledgerhd.NoGL
      WHERE HPJenis = 'Piutang'
      AND ttglhpit.TglGL  BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT(@MyQuery," ");	
		ELSE
			SET @MyQuery = CONCAT(@MyQuery," AND (ttglhpit.NoAccount BETWEEN '",xPerkiraan1,"' AND '",xPerkiraan2,"')");	
		END IF;
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY ttglhpit.NoGL, ttglhpit.TglGL, ttgeneralledgerhd.GLLink ");
	END IF;

	IF xTipe = "Belum Di Link" THEN
		IF xSensitif <> "1" THEN
			UPDATE ttgeneralledgerhd
			INNER JOIN (SELECT ttgeneralledgerhd.NoGL, HPLink, ttglhpit.GLLink
			FROM ttgeneralledgerhd
			INNER JOIN ttglhpit ON ttglhpit.NoGL =  ttgeneralledgerhd.NoGL
			WHERE HPLink = '--' OR HPLink = ''
			GROUP BY ttgeneralledgerhd.NoGL HAVING COUNT(ttglhpit.NoGL) > 1) ttglhpit ON ttglhpit.NoGL =  ttgeneralledgerhd.NoGL
			SET ttgeneralledgerhd.HPLink = 'Many'
			WHERE ttgeneralledgerhd.HPLink = '--' OR ttgeneralledgerhd.HPLink = '';
		
			UPDATE ttgeneralledgerhd
			INNER JOIN (SELECT ttgeneralledgerhd.NoGL, HPLink, ttglhpit.GLLink
			FROM ttgeneralledgerhd
			INNER JOIN ttglhpit ON ttglhpit.NoGL =  ttgeneralledgerhd.NoGL
			WHERE HPLink = '--' OR HPLink = ''
			GROUP BY ttgeneralledgerhd.NoGL HAVING COUNT(ttglhpit.NoGL) = 1) ttglhpit ON ttglhpit.NoGL =  ttgeneralledgerhd.NoGL
			SET ttgeneralledgerhd.HPLink = ttglhpit.GLLink
			WHERE ttgeneralledgerhd.HPLink = '--' OR ttgeneralledgerhd.HPLink = '';
			
			SET @MyQuery = CONCAT("SELECT ttgeneralledgerit.NoGL, ttgeneralledgerit.TglGL, ttgeneralledgerhd.GLLink, 
			ttgeneralledgerit.NoAccount, ttgeneralledgerit.KeteranganGL, ttgeneralledgerit.KreditGL As Nilai
         FROM ttgeneralledgerit 
         INNER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.NoGL = ttgeneralledgerit.NoGL 
         WHERE ttgeneralledgerit.KreditGL > 0 
         AND (ttgeneralledgerit.NoAccount BETWEEN '11060000' AND '11150000') AND ttgeneralledgerhd.HPLink = '--'
			AND ttgeneralledgerit.TglGL   BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
			IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
				SET @MyQuery = CONCAT(@MyQuery," ");	
			ELSE
				SET @MyQuery = CONCAT(@MyQuery," AND (ttgeneralledgerit.NoAccount BETWEEN '",xPerkiraan1,"' AND '",xPerkiraan2,"')");	
			END IF;
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY ttgeneralledgerit.NoGL, ttgeneralledgerit.TglGL, ttgeneralledgerhd.GLLink ");		
		END IF;
		
		IF xSensitif = "1" THEN
			DROP TEMPORARY TABLE IF EXISTS tvpiutangkredit;
			IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
				CREATE TEMPORARY TABLE IF NOT EXISTS tvpiutangkredit AS  SELECT * FROM vpiutangkredit WHERE  TglGL  BETWEEN Tgl1 AND Tgl2 ;
			ELSE
				CREATE TEMPORARY TABLE IF NOT EXISTS tvpiutangkredit AS  SELECT * FROM vpiutangkredit WHERE  TglGL  BETWEEN Tgl1 AND Tgl2  AND (vpiutangkredit.NoAccount BETWEEN xPerkiraan1 AND xPerkiraan2);
			END IF;
			CREATE INDEX NoGL ON tvpiutangkredit(NoGL);
			CREATE INDEX NoAccount ON tvpiutangkredit(NoAccount);
			
			
			SET @MyQuery = CONCAT("
			 SELECT tvpiutangkredit.NoGL, tvpiutangkredit.TglGL, tvpiutangkredit.GLLink, tvpiutangkredit.NoAccount, tvpiutangkredit.KeteranganGL, 
            tvpiutangkredit.KreditGL-IFNULL(ttglhpit.HPNilai,0) AS Nilai, ttglhpit.*
            FROM tvpiutangkredit 
            LEFT OUTER JOIN 
            (SELECT NoGL, NoAccount, IFNULL(SUM(HPNilai),0) AS HPNilai FROM ttglhpit WHERE (ttglhpit.NoAccount BETWEEN '11060000' AND '11150000') AND ttglhpit.HPJenis = 'Piutang' GROUP BY NOGL, NOaccount) ttglhpit 
            ON tvpiutangkredit.NoGL = ttglhpit.NoGL AND tvpiutangkredit.NoAccount = ttglhpit.NoAccount 
            WHERE  ttglhpit.NoGL IS NULL OR ((tvpiutangkredit.KreditGL- ttglhpit.HPNilai) >0 AND KodeTrans <> 'JA')
          UNION
          SELECT tvpiutangkredit.NoGL, tvpiutangkredit.TglGL, tvpiutangkredit.GLLink, tvpiutangkredit.NoAccount, tvpiutangkredit.KeteranganGL, tvpiutangkredit.KreditGL - IFNULL(ttglhpit.HPNilai, 0) AS Nilai, ttglhpit.* 
            FROM tvpiutangkredit 
            LEFT OUTER JOIN 
            (SELECT GLLink, NoAccount, IFNULL(SUM(HPNilai), 0) AS HPNilai FROM ttglhpit 
             WHERE (ttglhpit.NoAccount BETWEEN '11060000'AND '11150000') AND ttglhpit.HPJenis = 'Piutang' 
             GROUP BY GLLink, NOaccount) ttglhpit 
            ON tvpiutangkredit.HPlink = ttglhpit.GLLink AND tvpiutangkredit.NoAccount = ttglhpit.NoAccount 
            WHERE (KodeTrans = 'JA') AND (tvpiutangkredit.KreditGL- ttglhpit.HPNilai) > 0
			");
		END IF;
	END IF;
	
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;  
END$$
DELIMITER ;

