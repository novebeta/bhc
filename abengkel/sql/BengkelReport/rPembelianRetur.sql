DELIMITER $$
DROP PROCEDURE IF EXISTS `rPembelianRetur`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rPembelianRetur`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,
IN xStatus VARCHAR(100),
IN xSort VARCHAR(150),IN xOrder VARCHAR(10))
BEGIN
/*
		CALL rPembelianRetur('Header1','2020-01-01','2020-05-31','','PRTgl','ASC');
*/
	IF xTipe = "Header1" THEN
		SET @MyQuery = CONCAT("
		SELECT ttprhd.PRNo, ttprhd.PRTgl, ttprhd.SupKode, ttprhd.LokasiKode, ttprhd.PRKeterangan, ttprhd.PRSubTotal, ttprhd.PRDiscFinal, ttprhd.PRTotalPajak, ttprhd.PRBiayaKirim, ttprhd.PRTotal, ttprhd.UserID, tdsupplier.SupNama, 
      ttprhd.NoGL, ttprhd.KodeTrans, ttprhd.PINo, ttprhd.PosKode, ttprhd.Cetak, ttprhd.PRTerbayar
      FROM  ttprhd INNER JOIN  tdsupplier ON ttprhd.SupKode = tdsupplier.SupKode
      WHERE DATE(ttprhd.PRTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery,xStatus);
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
	END IF;
	IF xTipe = "Item1" THEN
		SET @MyQuery = CONCAT("
		SELECT ttprit.PRNo, ttprit.PRAuto, ttprit.BrgKode, ttprit.PRQty, ttprit.PRHrgBeli, ttprit.PRHrgJual, ttprit.PRDiscount, ttprit.PRPajak, ttprit.PRDiscount / ttprit.PRHrgJual * 100 AS Disc, 
      ttprit.PRQty * ttprit.PRHrgJual - ttprit.PRDiscount AS Jumlah, tdbarang.BrgNama, tdbarang.BrgSatuan, ttprit.LokasiKode, tdbarang.BrgGroup, ttprhd.PRTgl
      FROM ttprit 
      INNER JOIN tdbarang ON ttprit.BrgKode = tdbarang.BrgKode 
      INNER JOIN ttprhd ON ttprit.PRNo = ttprhd.PRNo
      WHERE DATE(ttprhd.PrTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery,xStatus);
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY ttprit.PRNo, ttprit.PRAuto, " ,xSort," ",xOrder );    
	END IF;
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$
DELIMITER ;

