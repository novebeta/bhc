DELIMITER $$
DROP PROCEDURE IF EXISTS `rGLNeracaView`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rGLNeracaView`(IN  MyTgl1 DATE, MyTgl2 DATE , MyTglSaldo DATE)
BEGIN
	CALL rglNeraca(MyTgl1,MyTgl2,MyTglSaldo);
	SELECT * FROM traccountsaldo;
END$$
DELIMITER ;

