DELIMITER $$
DROP PROCEDURE IF EXISTS `rStockItem`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rStockItem`(IN xTipe VARCHAR(50),IN xTgl1 DATE,IN xTgl2 DATE,
IN xLokasi VARCHAR(50), IN xBrgGroup VARCHAR(50), IN xBrgKode VARCHAR(50),IN xStatus VARCHAR(50), 
IN xStock VARCHAR(50),IN xSort VARCHAR(50),IN xOrder VARCHAR(50))
BEGIN
/* CALL rStockItem('Stock Lokasi Detail','2020-04-01','2020-04-17','%','%','%','A','OUT','BrgKode','ASC'); */
  							 
	IF xTipe = "Stock Ringkas" OR xTipe = "Stock Ringkas Nilai" OR xTipe = "Stock Detail" THEN
		CALL rStockHitung(xBrgKode,xStatus,xBrgGroup,xLokasi,xTgl1,xTgl2);
		CASE xStock
			WHEN "Transaksi" THEN 
				/*FillHilangStockNonTransaksi() */
				SET @MyQuery = CONCAT("SELECT * FROM StockBarang 
				WHERE (SD <> 0 OR SI <> 0 OR SR <> 0 OR PS <> 0 OR PR <> 0 OR TA <> 0 OR TIMin <> 0 OR TIPlus <> 0) 
				ORDER BY ",xSort," ",xOrder);
			WHEN "Idle" THEN 
				/*FillHilangStockTransaksi() */
				SET @MyQuery = CONCAT("SELECT * FROM StockBarang 
				WHERE (SD = 0 AND SI = 0 AND SR = 0 AND PS = 0 AND PR = 0 AND TA = 0 AND TIMin = 0 OR TIPlus = 0) 
				ORDER BY ",xSort," ",xOrder);
			WHEN "Nol" THEN 
				/*FillHilangStockNotNol() */
				SET @MyQuery = CONCAT("SELECT * FROM StockBarang 
				WHERE SaldoAkhir = 0
				ORDER BY ",xSort," ",xOrder);
			WHEN "IN" THEN 
				/*FillHilangStockNotIn()*/
				SET @MyQuery = CONCAT("SELECT * FROM StockBarang 
				WHERE (PS <> 0 OR SR <> 0 AND TIMin <> 0)			
				ORDER BY ",xSort," ",xOrder);
			WHEN "OUT" THEN 	
				/*FillHilangStockNotOUT()*/
				SET @MyQuery = CONCAT("SELECT * FROM StockBarang 
				WHERE (SD <> 0 OR SI <> 0 AND PR <> 0 AND TA <> 0 AND TIPlus <> 0)			
				ORDER BY ",xSort," ",xOrder);
			ELSE	
				SET @MyQuery = CONCAT("SELECT * FROM StockBarang ORDER BY ",xSort," ",xOrder);
		END CASE;
	END IF;

	IF xTipe = "Stock Lokasi Ringkas" OR xTipe = "Stock Lokasi Detail" OR xTipe = "Stock Lokasi Nilai" THEN
		CALL rStockHitungLokasi(xBrgKode,xStatus,xBrgGroup,xLokasi,xTgl1,xTgl2);
		CASE xStock
			WHEN "Transaksi" THEN 
				/*FillHilangStockNonTransaksi() */
				SET @MyQuery = CONCAT("SELECT * FROM StockBarang 
				WHERE (SD <> 0 OR SI <> 0 OR SR <> 0 OR PS <> 0 OR PR <> 0 OR TA <> 0 OR TIMin <> 0 OR TIPlus <> 0) 
				ORDER BY ",xSort," ",xOrder);
			WHEN "Idle" THEN 
				/*FillHilangStockTransaksi() */
				SET @MyQuery = CONCAT("SELECT * FROM StockBarang 
				WHERE (SD = 0 AND SI = 0 AND SR = 0 AND PS = 0 AND PR = 0 AND TA = 0 AND TIMin = 0 OR TIPlus = 0) 
				ORDER BY ",xSort," ",xOrder);
			WHEN "Nol" THEN 
				/*FillHilangStockNotNol() */
				SET @MyQuery = CONCAT("SELECT * FROM StockBarang 
				WHERE SaldoAkhir = 0
				ORDER BY ",xSort," ",xOrder);
			WHEN "IN" THEN 
				/*FillHilangStockNotIn()*/
				SET @MyQuery = CONCAT("SELECT * FROM StockBarang 
				WHERE (PS <> 0 OR SR <> 0 AND TIMin <> 0)			
				ORDER BY ",xSort," ",xOrder);
			WHEN "OUT" THEN 	
				/*FillHilangStockNotOUT()*/
				SET @MyQuery = CONCAT("SELECT * FROM StockBarang 
				WHERE (SD <> 0 OR SI <> 0 AND PR <> 0 AND TA <> 0 AND TIPlus <> 0)			
				ORDER BY ",xSort," ",xOrder);
			ELSE	
				SET @MyQuery = CONCAT("SELECT * FROM StockBarang ORDER BY ",xSort," ",xOrder);
		END CASE;
	END IF;

	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;  
END$$
DELIMITER ;

