DELIMITER $$
DROP PROCEDURE IF EXISTS `rStockHitung`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rStockHitung`(IN MyBrgKode VARCHAR(20), IN MyBrgStatus VARCHAR(1), IN MyBrgGroup VARCHAR(15), IN MyLokasi VARCHAR(15),
IN  TglAwal DATE, IN TglAkhir DATE )
BEGIN
DECLARE TglAwalSaldo DATE;
DECLARE TglAwalSaldoTransaksi DATE;
DECLARE TglAkhirSaldo DATE;
DECLARE Tanggal DATE;
SET Tanggal = '1900-01-01';
SELECT IFNULL(TglSaldo, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) INTO Tanggal FROM tsaldostock WHERE TglSaldo < TglAwal  ORDER BY TglSaldo DESC ;

SET TglAwalSaldo = Tanggal;
SET TglAwalSaldoTransaksi = DATE_ADD(Tanggal, INTERVAL 1 DAY);
SET TglAkhirSaldo = DATE_ADD(TglAwal, INTERVAL -1 DAY);

DROP TEMPORARY TABLE IF EXISTS `StockBarang`;
CREATE TEMPORARY TABLE IF NOT EXISTS `StockBarang` AS
SELECT BrgKode, BrgBarCode, BrgNama, BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual, BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus,
0 AS SD, 0 AS SI, 0 AS SR, 0 AS PS, 0 AS PR, 0 AS TIPlus, 0 AS TIMin, 0 AS TA, 0 AS SaldoAwal, 0 AS SaldoAkhir FROM tdbarang
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus)
ORDER BY tdbarang.BrgGroup, tdbarang.BrgKode;
CREATE INDEX BrgKode ON StockBarang(BrgKode);


DROP TEMPORARY TABLE IF EXISTS `SAwal`;
CREATE TEMPORARY TABLE IF NOT EXISTS `SAwal` AS
SELECT tdbarang.BrgKode, IFNULL(MyGroup.MySUM, 0) AS MySum FROM tdbarang
LEFT OUTER JOIN
  (SELECT BrgKode, Saldo AS MySum FROM tsaldostock WHERE LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(TglSaldo) BETWEEN TglAwalSaldo AND TglAwalSaldo) MyGroup
ON tdbarang.BrgKode = MyGroup.BrgKode
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus)
ORDER BY tdbarang.BrgGroup, tdbarang.BrgKode;
CREATE INDEX BrgKode ON SAwal(BrgKode);

DROP TEMPORARY TABLE IF EXISTS `tvsumbrg`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrg` AS
SELECT IFNULL(ttsditbarang.BrgKode,'NULL') AS BrgKode, SUM(ttsditbarang.SDQty) AS MySum FROM ttsdhd
INNER JOIN ttsditbarang ON ttsdhd.SDNo = ttsditbarang.SDNo
WHERE ttsditbarang.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(SDTgl) BETWEEN TglAwalSaldoTransaksi AND TglAkhirSaldo
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttsditbarang.BrgKode,'NULL');
CREATE INDEX BrgKode ON tvsumbrg(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `SD`;
CREATE TEMPORARY TABLE IF NOT EXISTS `SD` AS
SELECT tdbarang.BrgKode, IFNULL(tvsumbrg.MySUM, 0) AS MySum FROM tdbarang
LEFT OUTER JOIN tvsumbrg
ON tdbarang.BrgKode = tvsumbrg.BrgKode
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus)
ORDER BY tdbarang.BrgGroup, tdbarang.BrgKode;
CREATE INDEX BrgKode ON SD(BrgKode);

DROP TEMPORARY TABLE IF EXISTS `tvsumbrg`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrg` AS
SELECT IFNULL(ttsiit.BrgKode,'NULL') AS BrgKode, SUM(ttsiit.SIQty) AS MySum FROM ttsihd
INNER JOIN ttsiit ON ttsihd.SINo = ttsiit.SINo
WHERE ttsiit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(SITgl) BETWEEN TglAwalSaldoTransaksi AND TglAkhirSaldo
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttsiit.BrgKode,'NULL');
CREATE INDEX BrgKode ON tvsumbrg(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `SI`;
CREATE TEMPORARY TABLE IF NOT EXISTS `SI` AS
SELECT tdbarang.BrgKode, IFNULL(tvsumbrg.MySUM, 0) AS MySum FROM tdbarang
LEFT OUTER JOIN tvsumbrg
ON tdbarang.BrgKode = tvsumbrg.BrgKode
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus)
ORDER BY tdbarang.BrgGroup, tdbarang.BrgKode;
CREATE INDEX BrgKode ON SI(BrgKode);

DROP TEMPORARY TABLE IF EXISTS `tvsumbrg`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrg` AS
SELECT IFNULL(ttPRit.BrgKode,'NULL') AS BrgKode, SUM(ttPRit.PRQty) AS MySum FROM ttPRhd
INNER JOIN ttPRit ON ttPRhd.PRNo = ttPRit.PRNo
WHERE ttPRit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(PRTgl) BETWEEN TglAwalSaldoTransaksi AND TglAkhirSaldo
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttPRit.BrgKode,'NULL');
CREATE INDEX BrgKode ON tvsumbrg(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `PR`;
CREATE TEMPORARY TABLE IF NOT EXISTS `PR` AS
SELECT tdbarang.BrgKode, IFNULL(tvsumbrg.MySUM, 0) AS MySum FROM tdbarang
LEFT OUTER JOIN tvsumbrg
ON tdbarang.BrgKode = tvsumbrg.BrgKode
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus)
ORDER BY tdbarang.BrgGroup, tdbarang.BrgKode;
CREATE INDEX BrgKode ON PR(BrgKode);

DROP TEMPORARY TABLE IF EXISTS `tvsumbrg`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrg` AS
SELECT IFNULL(ttPSit.BrgKode,'NULL') AS BrgKode, SUM(ttPSit.PSQty) AS MySum FROM ttPShd
INNER JOIN ttPSit ON ttPShd.PSNo = ttPSit.PSNo
WHERE ttPSit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(PSTgl) BETWEEN TglAwalSaldoTransaksi AND TglAkhirSaldo
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttPSit.BrgKode,'NULL');
CREATE INDEX BrgKode ON tvsumbrg(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `PS`;
CREATE TEMPORARY TABLE IF NOT EXISTS `PS` AS
SELECT tdbarang.BrgKode, IFNULL(tvsumbrg.MySUM, 0) AS MySum FROM tdbarang
LEFT OUTER JOIN tvsumbrg
ON tdbarang.BrgKode = tvsumbrg.BrgKode
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus)
ORDER BY tdbarang.BrgGroup, tdbarang.BrgKode;
CREATE INDEX BrgKode ON PS(BrgKode);

DROP TEMPORARY TABLE IF EXISTS `tvsumbrg`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrg` AS
SELECT IFNULL(ttPRit.BrgKode,'NULL') AS BrgKode, SUM(ttPRit.PRQty) AS MySum FROM ttPRhd
INNER JOIN ttPRit ON ttPRhd.PRNo = ttPRit.PRNo
WHERE ttPRit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(PRTgl) BETWEEN TglAwalSaldoTransaksi AND TglAkhirSaldo
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttPRit.BrgKode,'NULL');
CREATE INDEX BrgKode ON tvsumbrg(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `PR`;
CREATE TEMPORARY TABLE IF NOT EXISTS `PR` AS
SELECT tdbarang.BrgKode, IFNULL(tvsumbrg.MySUM, 0) AS MySum FROM tdbarang
LEFT OUTER JOIN tvsumbrg
ON tdbarang.BrgKode = tvsumbrg.BrgKode
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus)
ORDER BY tdbarang.BrgGroup, tdbarang.BrgKode;
CREATE INDEX BrgKode ON PR(BrgKode);

DROP TEMPORARY TABLE IF EXISTS `tvsumbrg`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrg` AS
SELECT IFNULL(ttSRit.BrgKode,'NULL') AS BrgKode, SUM(ttSRit.SRQty) AS MySum FROM ttSRhd
INNER JOIN ttSRit ON ttSRhd.SRNo = ttSRit.SRNo
WHERE ttSRit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(SRTgl) BETWEEN TglAwalSaldoTransaksi AND TglAkhirSaldo
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttSRit.BrgKode,'NULL');
CREATE INDEX BrgKode ON tvsumbrg(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `SR`;
CREATE TEMPORARY TABLE IF NOT EXISTS `SR` AS
SELECT tdbarang.BrgKode, IFNULL(tvsumbrg.MySUM, 0) AS MySum FROM tdbarang
LEFT OUTER JOIN tvsumbrg
ON tdbarang.BrgKode = tvsumbrg.BrgKode
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus)
ORDER BY tdbarang.BrgGroup, tdbarang.BrgKode;
CREATE INDEX BrgKode ON SR(BrgKode);

DROP TEMPORARY TABLE IF EXISTS `tvsumbrg`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrg` AS
SELECT IFNULL(ttTAit.BrgKode,'NULL') AS BrgKode, SUM(ttTAit.TAQty) AS MySum FROM ttTAhd
INNER JOIN ttTAit ON ttTAhd.TANo = ttTAit.TANo
WHERE ttTAit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(TATgl) BETWEEN TglAwalSaldoTransaksi AND TglAkhirSaldo
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttTAit.BrgKode,'NULL');
CREATE INDEX BrgKode ON tvsumbrg(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `TA`;
CREATE TEMPORARY TABLE IF NOT EXISTS `TA` AS
SELECT tdbarang.BrgKode, IFNULL(tvsumbrg.MySUM, 0) AS MySum FROM tdbarang
LEFT OUTER JOIN tvsumbrg
ON tdbarang.BrgKode = tvsumbrg.BrgKode
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus)
ORDER BY tdbarang.BrgGroup, tdbarang.BrgKode;
CREATE INDEX BrgKode ON TA(BrgKode);

DROP TEMPORARY TABLE IF EXISTS `tvsumbrg`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrg` AS
SELECT IFNULL(ttTIit.BrgKode,'NULL') AS BrgKode, SUM(ttTIit.TIQty) AS MySum FROM ttTIhd 
INNER JOIN ttTIit ON ttTIhd.TINo = ttTIit.TINo 
WHERE ttTIit.LokasiTujuan LIKE CONCAT('%',MyLokasi) AND DATE(TITgl) BETWEEN TglAwalSaldoTransaksi AND TglAkhirSaldo
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttTIit.BrgKode,'NULL');
CREATE INDEX BrgKode ON tvsumbrg(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `TIPlus`;
CREATE TEMPORARY TABLE IF NOT EXISTS `TIPlus` AS
SELECT tdbarang.BrgKode, IFNULL(tvsumbrg.MySUM, 0) AS MySum FROM tdbarang
LEFT OUTER JOIN tvsumbrg
ON tdbarang.BrgKode = tvsumbrg.BrgKode
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus)
ORDER BY tdbarang.BrgGroup, tdbarang.BrgKode;
CREATE INDEX BrgKode ON TIPlus(BrgKode);

DROP TEMPORARY TABLE IF EXISTS `tvsumbrg`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrg` AS
SELECT IFNULL(ttTIit.BrgKode,'NULL') AS BrgKode, SUM(ttTIit.TIQty) AS MySum FROM ttTIhd 
INNER JOIN ttTIit ON ttTIhd.TINo = ttTIit.TINo 
WHERE ttTIit.LokasiAsal LIKE CONCAT('%',MyLokasi) AND DATE(TITgl) BETWEEN TglAwalSaldoTransaksi AND TglAkhirSaldo
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttTIit.BrgKode,'NULL');
CREATE INDEX BrgKode ON tvsumbrg(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `TIMin`;
CREATE TEMPORARY TABLE IF NOT EXISTS `TIMin` AS
SELECT tdbarang.BrgKode, IFNULL(tvsumbrg.MySUM, 0) AS MySum FROM tdbarang
LEFT OUTER JOIN tvsumbrg
ON tdbarang.BrgKode = tvsumbrg.BrgKode
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus)
ORDER BY tdbarang.BrgGroup, tdbarang.BrgKode;
CREATE INDEX BrgKode ON TIMin(BrgKode);

UPDATE StockBarang INNER JOIN SAwal ON SAwal.BrgKode = StockBarang.BrgKode SET  SaldoAwal = MySum ;
UPDATE StockBarang INNER JOIN SD ON SD.BrgKode = StockBarang.BrgKode SET  SD = MySum ;
UPDATE StockBarang INNER JOIN SI ON SI.BrgKode = StockBarang.BrgKode SET  SI = MySum ;
UPDATE StockBarang INNER JOIN SR ON SR.BrgKode = StockBarang.BrgKode SET  SR = MySum ;
UPDATE StockBarang INNER JOIN PS ON PS.BrgKode = StockBarang.BrgKode SET  PS = MySum ;
UPDATE StockBarang INNER JOIN PR ON PR.BrgKode = StockBarang.BrgKode SET  PR = MySum ;
UPDATE StockBarang INNER JOIN TA ON TA.BrgKode = StockBarang.BrgKode SET  TA = MySum ;
UPDATE StockBarang INNER JOIN TIPlus ON TIPlus.BrgKode = StockBarang.BrgKode SET  TIPlus = MySum ;
UPDATE StockBarang INNER JOIN TIMin ON TIMin.BrgKode = StockBarang.BrgKode SET  TIMin = MySum ;
UPDATE StockBarang SET SaldoAwal = SaldoAwal + PS + SR + TIPlus + TA - SD - SI - PR - TIMin;

DROP TEMPORARY TABLE IF EXISTS `tvsumbrg`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrg` AS
SELECT IFNULL(ttsditbarang.BrgKode,'NULL') AS BrgKode, SUM(ttsditbarang.SDQty) AS MySum FROM ttsdhd
INNER JOIN ttsditbarang ON ttsdhd.SDNo = ttsditbarang.SDNo
WHERE ttsditbarang.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(SDTgl) BETWEEN TglAwal AND TglAkhir
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttsditbarang.BrgKode,'NULL');
CREATE INDEX BrgKode ON tvsumbrg(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `SD`;
CREATE TEMPORARY TABLE IF NOT EXISTS `SD` AS
SELECT tdbarang.BrgKode, IFNULL(tvsumbrg.MySUM, 0) AS MySum FROM tdbarang
LEFT OUTER JOIN tvsumbrg
ON tdbarang.BrgKode = tvsumbrg.BrgKode
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus)
ORDER BY tdbarang.BrgGroup, tdbarang.BrgKode;
CREATE INDEX BrgKode ON SD(BrgKode);

DROP TEMPORARY TABLE IF EXISTS `tvsumbrg`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrg` AS
SELECT IFNULL(ttsiit.BrgKode,'NULL') AS BrgKode, SUM(ttsiit.SIQty) AS MySum FROM ttsihd
INNER JOIN ttsiit ON ttsihd.SINo = ttsiit.SINo
WHERE ttsiit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(SITgl) BETWEEN TglAwal AND TglAkhir
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttsiit.BrgKode,'NULL');
CREATE INDEX BrgKode ON tvsumbrg(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `SI`;
CREATE TEMPORARY TABLE IF NOT EXISTS `SI` AS
SELECT tdbarang.BrgKode, IFNULL(tvsumbrg.MySUM, 0) AS MySum FROM tdbarang
LEFT OUTER JOIN tvsumbrg
ON tdbarang.BrgKode = tvsumbrg.BrgKode
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus)
ORDER BY tdbarang.BrgGroup, tdbarang.BrgKode;
CREATE INDEX BrgKode ON SI(BrgKode);

DROP TEMPORARY TABLE IF EXISTS `tvsumbrg`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrg` AS
SELECT IFNULL(ttPRit.BrgKode,'NULL') AS BrgKode, SUM(ttPRit.PRQty) AS MySum FROM ttPRhd
INNER JOIN ttPRit ON ttPRhd.PRNo = ttPRit.PRNo
WHERE ttPRit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(PRTgl) BETWEEN TglAwal AND TglAkhir
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttPRit.BrgKode,'NULL');
CREATE INDEX BrgKode ON tvsumbrg(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `PR`;
CREATE TEMPORARY TABLE IF NOT EXISTS `PR` AS
SELECT tdbarang.BrgKode, IFNULL(tvsumbrg.MySUM, 0) AS MySum FROM tdbarang
LEFT OUTER JOIN tvsumbrg
ON tdbarang.BrgKode = tvsumbrg.BrgKode
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus)
ORDER BY tdbarang.BrgGroup, tdbarang.BrgKode;
CREATE INDEX BrgKode ON PR(BrgKode);

DROP TEMPORARY TABLE IF EXISTS `tvsumbrg`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrg` AS
SELECT IFNULL(ttPSit.BrgKode,'NULL') AS BrgKode, SUM(ttPSit.PSQty) AS MySum FROM ttPShd
INNER JOIN ttPSit ON ttPShd.PSNo = ttPSit.PSNo
WHERE ttPSit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(PSTgl) BETWEEN TglAwal AND TglAkhir
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttPSit.BrgKode,'NULL');
CREATE INDEX BrgKode ON tvsumbrg(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `PS`;
CREATE TEMPORARY TABLE IF NOT EXISTS `PS` AS
SELECT tdbarang.BrgKode, IFNULL(tvsumbrg.MySUM, 0) AS MySum FROM tdbarang
LEFT OUTER JOIN tvsumbrg
ON tdbarang.BrgKode = tvsumbrg.BrgKode
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus)
ORDER BY tdbarang.BrgGroup, tdbarang.BrgKode;
CREATE INDEX BrgKode ON PS(BrgKode);

DROP TEMPORARY TABLE IF EXISTS `tvsumbrg`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrg` AS
SELECT IFNULL(ttPRit.BrgKode,'NULL') AS BrgKode, SUM(ttPRit.PRQty) AS MySum FROM ttPRhd
INNER JOIN ttPRit ON ttPRhd.PRNo = ttPRit.PRNo
WHERE ttPRit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(PRTgl) BETWEEN TglAwal AND TglAkhir
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttPRit.BrgKode,'NULL');
CREATE INDEX BrgKode ON tvsumbrg(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `PR`;
CREATE TEMPORARY TABLE IF NOT EXISTS `PR` AS
SELECT tdbarang.BrgKode, IFNULL(tvsumbrg.MySUM, 0) AS MySum FROM tdbarang
LEFT OUTER JOIN tvsumbrg
ON tdbarang.BrgKode = tvsumbrg.BrgKode
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus)
ORDER BY tdbarang.BrgGroup, tdbarang.BrgKode;
CREATE INDEX BrgKode ON PR(BrgKode);

DROP TEMPORARY TABLE IF EXISTS `tvsumbrg`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrg` AS
SELECT IFNULL(ttSRit.BrgKode,'NULL') AS BrgKode, SUM(ttSRit.SRQty) AS MySum FROM ttSRhd
INNER JOIN ttSRit ON ttSRhd.SRNo = ttSRit.SRNo
WHERE ttSRit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(SRTgl) BETWEEN TglAwal AND TglAkhir
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttSRit.BrgKode,'NULL');
CREATE INDEX BrgKode ON tvsumbrg(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `SR`;
CREATE TEMPORARY TABLE IF NOT EXISTS `SR` AS
SELECT tdbarang.BrgKode, IFNULL(tvsumbrg.MySUM, 0) AS MySum FROM tdbarang
LEFT OUTER JOIN tvsumbrg
ON tdbarang.BrgKode = tvsumbrg.BrgKode
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus)
ORDER BY tdbarang.BrgGroup, tdbarang.BrgKode;
CREATE INDEX BrgKode ON SR(BrgKode);

DROP TEMPORARY TABLE IF EXISTS `tvsumbrg`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrg` AS
SELECT IFNULL(ttTAit.BrgKode,'NULL') AS BrgKode, SUM(ttTAit.TAQty) AS MySum FROM ttTAhd
INNER JOIN ttTAit ON ttTAhd.TANo = ttTAit.TANo
WHERE ttTAit.LokasiKode LIKE CONCAT('%',MyLokasi) AND DATE(TATgl) BETWEEN TglAwal AND TglAkhir
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttTAit.BrgKode,'NULL');
CREATE INDEX BrgKode ON tvsumbrg(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `TA`;
CREATE TEMPORARY TABLE IF NOT EXISTS `TA` AS
SELECT tdbarang.BrgKode, IFNULL(tvsumbrg.MySUM, 0) AS MySum FROM tdbarang
LEFT OUTER JOIN tvsumbrg
ON tdbarang.BrgKode = tvsumbrg.BrgKode
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus)
ORDER BY tdbarang.BrgGroup, tdbarang.BrgKode;
CREATE INDEX BrgKode ON TA(BrgKode);

DROP TEMPORARY TABLE IF EXISTS `tvsumbrg`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrg` AS
SELECT IFNULL(ttTIit.BrgKode,'NULL') AS BrgKode, SUM(ttTIit.TIQty) AS MySum FROM ttTIhd
INNER JOIN ttTIit ON ttTIhd.TINo = ttTIit.TINo
WHERE ttTIit.LokasiTujuan LIKE CONCAT('%',MyLokasi) AND DATE(TITgl) BETWEEN TglAwal AND TglAkhir
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttTIit.BrgKode,'NULL');
CREATE INDEX BrgKode ON tvsumbrg(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `TIPlus`;
CREATE TEMPORARY TABLE IF NOT EXISTS `TIPlus` AS
SELECT tdbarang.BrgKode, IFNULL(tvsumbrg.MySUM, 0) AS MySum FROM tdbarang
LEFT OUTER JOIN tvsumbrg
ON tdbarang.BrgKode = tvsumbrg.BrgKode
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus)
ORDER BY tdbarang.BrgGroup, tdbarang.BrgKode;
CREATE INDEX BrgKode ON TIPlus(BrgKode);

DROP TEMPORARY TABLE IF EXISTS `tvsumbrg`;
CREATE TEMPORARY TABLE IF NOT EXISTS `tvsumbrg` AS
SELECT IFNULL(ttTIit.BrgKode,'NULL') AS BrgKode, SUM(ttTIit.TIQty) AS MySum FROM ttTIhd
INNER JOIN ttTIit ON ttTIhd.TINo = ttTIit.TINo
WHERE ttTIit.LokasiAsal LIKE CONCAT('%',MyLokasi) AND DATE(TITgl) BETWEEN TglAwal AND TglAkhir
AND BrgKode LIKE CONCAT('%',MyBrgKode)
GROUP BY IFNULL(ttTIit.BrgKode,'NULL');
CREATE INDEX BrgKode ON tvsumbrg(BrgKode);
DROP TEMPORARY TABLE IF EXISTS `TIMin`;
CREATE TEMPORARY TABLE IF NOT EXISTS `TIMin` AS
SELECT tdbarang.BrgKode, IFNULL(tvsumbrg.MySUM, 0) AS MySum FROM tdbarang
LEFT OUTER JOIN tvsumbrg
ON tdbarang.BrgKode = tvsumbrg.BrgKode
WHERE tdbarang.BrgKode LIKE CONCAT('%',MyBrgKode) AND tdbarang.BrgGroup LIKE CONCAT('%',MyBrgGroup) AND BrgStatus LIKE CONCAT('%',MyBrgStatus)
ORDER BY tdbarang.BrgGroup, tdbarang.BrgKode;
CREATE INDEX BrgKode ON TIMin(BrgKode);

UPDATE StockBarang SET PS =0, SR =0, TIPlus = 0, TA = 0, SD = 0, SI =0, PR = 0, TIMin = 0;
UPDATE StockBarang INNER JOIN SD ON SD.BrgKode = StockBarang.BrgKode SET  SD = MySum ;
UPDATE StockBarang INNER JOIN SI ON SI.BrgKode = StockBarang.BrgKode SET  SI = MySum ;
UPDATE StockBarang INNER JOIN SR ON SR.BrgKode = StockBarang.BrgKode SET  SR = MySum ;
UPDATE StockBarang INNER JOIN PS ON PS.BrgKode = StockBarang.BrgKode SET  PS = MySum ;
UPDATE StockBarang INNER JOIN PR ON PR.BrgKode = StockBarang.BrgKode SET  PR = MySum ;
UPDATE StockBarang INNER JOIN TA ON TA.BrgKode = StockBarang.BrgKode SET  TA = MySum ;
UPDATE StockBarang INNER JOIN TIPlus ON TIPlus.BrgKode = StockBarang.BrgKode SET  TIPlus = MySum ;
UPDATE StockBarang INNER JOIN TIMin ON TIMin.BrgKode = StockBarang.BrgKode SET  TIMin = MySum ;
UPDATE StockBarang SET SaldoAkhir = SaldoAwal + PS + SR + TIPlus + TA - SD - SI - PR - TIMin;

END$$
DELIMITER ;

