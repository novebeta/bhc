DELIMITER $$
DROP PROCEDURE IF EXISTS `rPembelianInvoice`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rPembelianInvoice`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,
IN xTanggal VARCHAR(100), IN xTCPS VARCHAR(100), IN xTCPI VARCHAR(100),IN xSupplier VARCHAR(100),IN xStatus VARCHAR(100),
IN xSort VARCHAR(150),IN xOrder VARCHAR(10))
BEGIN
/*
		CALL rPembelianInvoice('Header1','DATE(ttpihd.PITgl)','2020-01-01','2020-05-31','','','','','PITgl','ASC');
*/
	IF xTipe = "Header1" THEN
		SET @MyQuery = CONCAT("
      SELECT ttpihd.PINo, ttpihd.PITgl, ttpihd.SupKode, ttpihd.PINoRef, ttpihd.LokasiKode, ttpihd.PIKeterangan, ttpihd.PISubTotal, ttpihd.PIDiscFinal, ttpihd.PITotalPajak, ttpihd.PIBiayaKirim, ttpihd.PITotal, ttpihd.PITerm, ttpihd.PITglTempo, 
      ttpihd.UserID, tdsupplier.SupNama, ttpihd.NoGL, ttpihd.KodeTrans, ttpihd.PSNo, IFNULL(ttpshd.PSTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS PSTgl, ttpihd.PosKode, ttpihd.Cetak, ttpihd.PIJenis, ttpihd.PITerbayar
		FROM ttpihd INNER JOIN
		tdsupplier ON ttpihd.SupKode = tdsupplier.SupKode LEFT OUTER JOIN
      ttpshd ON ttpihd.PSNo = ttpshd.PSNo ");
      SET @MyQuery = CONCAT(@MyQuery,"WHERE ", xTanggal ," BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery," AND ttpshd.KodeTrans LIKE '%", xTCPS ,"' ");
      SET @MyQuery = CONCAT(@MyQuery," AND ttpihd.SupKode LIKE '%", xSupplier ,"' ");
      SET @MyQuery = CONCAT(@MyQuery," AND ttpihd.KodeTrans LIKE '%", xTCPI ,"' ");
      SET @MyQuery = CONCAT(@MyQuery, xStatus );
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
	END IF;
	IF xTipe = "Item1" OR xTipe = "Item2" THEN
		SET @MyQuery = CONCAT("
		SELECT  ttpiit.PINo,ttpihd.PITgl, ttpiit.PIAuto, ttpiit.BrgKode, ttpiit.PIQty, ttpiit.PIHrgBeli, ttpiit.PIHrgJual, ttpiit.PIDiscount, ttpiit.PIPajak, 
		tdbarang.BrgNama, tdbarang.BrgSatuan, ttpiit.PIDiscount / ttpiit.PIHrgJual * 100 AS Disc, 
      ttpiit.PIQty * ttpiit.PIHrgJual - ttpiit.PIDiscount AS Jumlah, ttpiit.LokasiKode, tdbarang.BrgGroup, ttpiit.PSNo, ttpsit.PSQty, ttpshd.PSTgl 
      FROM  ttpiit 
      INNER JOIN tdbarang ON ttpiit.BrgKode = tdbarang.BrgKode 
      INNER JOIN ttpihd ON ttpiit.PINo = ttpihd.PINo 
      LEFT OUTER JOIN ttpsit ON ttpiit.BrgKode = ttpsit.BrgKode AND ttpiit.PSNo = ttpsit.PSNo
		LEFT OUTER JOIN ttpshd ON ttpsit.PSNo = ttpshd.PSNo		
		WHERE ", xTanggal ," BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery," AND ttpshd.KodeTrans LIKE '%", xTCPS ,"' ");
      SET @MyQuery = CONCAT(@MyQuery," AND ttpihd.SupKode LIKE '%", xSupplier ,"' ");
      SET @MyQuery = CONCAT(@MyQuery," AND ttpihd.KodeTrans LIKE '%", xTCPI ,"' ");
      SET @MyQuery = CONCAT(@MyQuery, xStatus );
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
	END IF;
	IF xTipe = "Price Variance" THEN
		SET @MyQuery = CONCAT("
		SELECT ttpiit.PINo, BrgKode,ttpiit.PSNo,ttpihd.SupKode,ttpshd.KodeTrans, PIQty, 
      PIHrgBeli, PIHrgJual, (PIDiscount/PIQty) AS PIDiscount, (PIHrgJual - PIDiscount/PIQty) AS HrgInv, 
      (PIHrgJual -PIHrgBeli - PIDiscount/PIQty) AS PV, (PIQty * (PIHrgJual -PIHrgBeli)) - PIDiscount AS PVTotal
      FROM ttpiit
      INNER JOIN ttpihd ON ttpihd.PiNo = ttpiit.PiNo
      INNER JOIN ttpshd ON ttpshd.PSNo = ttpiit.PSNo
		WHERE piQty > 0 AND DATE(ttpihd.PITgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");	
      SET @MyQuery = CONCAT(@MyQuery," AND ttpshd.KodeTrans LIKE '%", xTCPS ,"' ");
      SET @MyQuery = CONCAT(@MyQuery," AND ttpihd.KodeTrans LIKE '%", xTCPI ,"' ");
		SET @MyQuery = CONCAT(@MyQuery," UNION
		SELECT ttucit.UCNo, BrgKode, '--' AS PSNo,'--' AS SupKode, KodeTrans, UCQty, 
      UCHrgBeliLama, UCHrgBeliBaru, 0 AS PIDiscount, UCHrgBeliBaru AS HrgInv, 
      (UCHrgBeliLama - UCHrgBeliBaru) AS PV, (UCQty* (UCHrgBeliLama - UCHrgBeliBaru))  AS PVTotal
      FROM ttucit
      INNER JOIN ttuchd ON ttuchd.UCNo = ttucit.UCNo
		WHERE DATE(ttuchd.UCTgl)  BETWEEN '",xTgl1,"' AND '",xTgl2,"'");	
		SET @MyQuery = CONCAT(@MyQuery," UNION
		SELECT ttgeneralledgerhd.NoGL,'-' AS BrgKode, '--' AS PSNo,'--' AS SupKode, KodeTrans, 0 AS UCQty,
      SUM(DebetGL) AS HargaStandar, SUM(KreditGL) AS HargaInvoice, 0 AS PIDiscount, SUM(KreditGL)AS HrgInv,
      SUM(DebetGL)-SUM(KreditGL)  AS PV, SUM(DebetGL)-SUM(KreditGL) AS PVTotal
      FROM ttgeneralledgerhd 
      INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL
      WHERE NoAccount = '11200000' AND KodeTrans = 'GL'
      AND ttgeneralledgerhd.TglGL BETWEEN '",xTgl1,"' AND '",xTgl2,"'");	
	END IF;
   IF xTipe = "PV Detail" THEN
		SET @MyQuery = CONCAT("
			SELECT * FROM (
         SELECT ttgeneralledgerhd.TglGL, ttpiit.PINo, ttpiit.BrgKode, ttpiit.PIQty, ttpiit.PIHrgBeli AS HargaStandar, ttpiit.PIHrgJual AS HargaInvoice,
         SUM(PIQty* (PIHrgJual-PIHrgBeli)-PIDiscount) AS PriceVarianJumlah, ttpiit.PIDiscount / PIQty AS PIDiscount, ttpiit.PSNo, ttpiit.LokasiKode,
         AVG(DebetGL) AS Debet,AVG(KreditGL) AS Kredit
         FROM ttpiit
         INNER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttpiit.PINo
         INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL
         INNER JOIN ttpihd ON ttpihd.PINo = ttpiit.PINo
         WHERE NoAccount = '11200000' 
         AND ttgeneralledgerhd.TglGL BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
         AND ttpihd.KodeTrans LIKE '%", xTCPI ,"'
         GROUP BY ttpiit.PINo, ttpiit.BrgKode
         UNION
         SELECT ttgeneralledgerhd.TglGL, ttUCit.UCNo, ttUCit.BrgKode, ttUCit.UCQty, ttUCit.UCHrgBeliLama AS HargaStandar, ttUCit.UCHrgBeliBaru AS HargaInvoice,
         SUM(UCQty* (UCHrgBeliLama-UCHrgBeliBaru)) AS PriceVarianJumlah, 0 AS PIDiscount, '--' AS PSNo, '--' AS LokasiKode,
         AVG(DebetGL) AS Debet, AVG(KreditGL) AS Kredit
         FROM ttUCit
         INNER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttUCit.UCNo
         INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL
         WHERE NoAccount = '11200000' 
         AND ttgeneralledgerhd.TglGL BETWEEN '",xTgl1,"' AND '",xTgl2,"'  
         GROUP BY ttUCit.UCNo, ttUCit.BrgKode
         UNION
         SELECT ttgeneralledgerhd.TglGL, ttgeneralledgerhd.NoGL, '--' AS BrgKode, 0 AS UCQty,  SUM(DebetGL) AS HargaStandar, SUM(KreditGL) AS HargaInvoice,
         SUM(DebetGL)-SUM(KreditGL)  AS PriceVarianJumlah, 0 AS PIDiscount, '--' AS PSNo, '--' AS LokasiKode,
         SUM(DebetGL) AS Debet, SUM(KreditGL) AS Kredit
         FROM ttgeneralledgerhd 
         INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL
         WHERE NoAccount = '11200000' AND KodeTrans = 'GL'
         AND ttgeneralledgerhd.TglGL BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
         GROUP BY ttgeneralledgerhd.NoGL
         ) Gabungan ");	
	END IF;
   
   IF xTipe = "PV Rekap" THEN
		SET @MyQuery = CONCAT("
			SELECT * FROM (
         SELECT ttpihd.PSNo, IFNULL(ttpshd.PSTgl,STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS PSTgl, PSNoRef, ttpihd.PINo, ttpihd.PITgl,PINoRef,
         SUM(DebetGL) AS Debet,SUM(KreditGL) AS Kredit,ttpihd.SupKode
         FROM ttpihd
         LEFT OUTER JOIN ttpshd ON ttpshd.PSNo = ttpihd.PSNo
         INNER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttpihd.PINo
         INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL
         WHERE NoAccount = '11200000' 
         AND ttgeneralledgerhd.TglGL BETWEEN '",xTgl1,"' AND '",xTgl2,"'   
         AND ttpihd.KodeTrans LIKE '%", xTCPI ,"'
         GROUP BY ttpihd.PINo
         UNION
         SELECT ttUChd.UCNo AS PSNo, ttUChd.UCTgl AS PSTgl, '--' AS PSNoRef, 
         ttUChd.UCNo, ttUChd.UCTgl, '--' AS PINoRef, SUM(DebetGL) AS Debet, SUM(KreditGL) AS Kredit, '--' AS SupKode
         FROM ttUChd
         INNER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttUChd.UCNo
         INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL
         WHERE NoAccount = '11200000' 
         AND ttgeneralledgerhd.TglGL BETWEEN '",xTgl1,"' AND '",xTgl2,"'   
         GROUP BY ttUChd.UCNo
         UNION 
         SELECT ttgeneralledgerhd.NoGL AS PSNo, ttgeneralledgerhd.TglGL AS PSTgl, '--' AS PSNoRef, 
         ttgeneralledgerhd.NoGL, ttgeneralledgerhd.TglGL, '--' AS PINoRef, SUM(DebetGL) AS Debet, SUM(KreditGL) AS Kredit, '--' AS SupKode
         FROM ttgeneralledgerhd 
         INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL
         WHERE NoAccount = '11200000' AND KodeTrans = 'GL'
         AND ttgeneralledgerhd.TglGL BETWEEN '",xTgl1,"' AND '",xTgl2,"'   
         GROUP BY ttgeneralledgerhd.NoGL
         ) Gabungan ");	
	END IF;
   
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$
DELIMITER ;

