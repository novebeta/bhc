DELIMITER $$
DROP PROCEDURE IF EXISTS `rKlaimKPB`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rKlaimKPB`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,
IN xSort VARCHAR(150),IN xOrder VARCHAR(10))
BEGIN
/*	CALL rKlaimKPB('Header1','2020-01-01','2020-05-31','CKTgl','ASC'); */
	IF xTipe = "Header1" THEN
		SET @MyQuery = CONCAT("
		SELECT CKNo,CKTgl,CKTotalPart,CKTotalJasa,CKMemo,NoGL,Cetak,UserID,CKTotalPart + CKTotalJasa AS CKTotal, CKNoKlaim
      FROM ttckhd                       
      WHERE DATE(ttckhd.CKTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
	END IF;
	IF xTipe = "Item1" THEN
		SET @MyQuery = CONCAT("
		SELECT ttckit.CKNo, ttckit.SDNo, ttckit.CKPart, ttckit.CKJasa, ttsdhd.SDTgl, ttsdhd.MotorNoPolisi, 
         ttckit.CKPart + ttckit.CKJasa AS CKTotal, tdcustomer.CusNama, tdcustomer.MotorType, tdcustomer.MotorNama, ttsdhd.SVNo, ttckhd.CKTgl
         FROM ttckit 
         INNER JOIN ttsdhd ON ttckit.SDNo = ttsdhd.SDNo 
         INNER JOIN tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
         INNER JOIN ttckhd ON ttckit.CKNo = ttckhd.CKNo
      WHERE DATE(ttckhd.CKTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
	END IF;
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$
DELIMITER ;

