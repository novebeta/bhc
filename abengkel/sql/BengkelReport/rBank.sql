DELIMITER $$

DROP PROCEDURE IF EXISTS `rBank`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rBank`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,
IN xTC VARCHAR(100),IN xKas VARCHAR(100), xJenis VARCHAR(100), 
IN xSort VARCHAR(150),IN xOrder VARCHAR(10))
BEGIN
/*
		CALL rBank('Bank Masuk','2020-01-01','2020-05-31','','','','BMTgl','ASC');
*/
	DECLARE Tanggal DATE;
	DECLARE MyDateAwalSaldo DATE;
	DECLARE TanggalSaldo DATE;
	DECLARE TanggalAwal DATE;
	DECLARE TanggalAkhir DATE;
	DECLARE SaldoAwal DOUBLE;
	DECLARE SaldoAkhir DOUBLE;
	DECLARE BankKeluar DOUBLE;
	DECLARE BankMasuk DOUBLE;
	DECLARE MySaldo DOUBLE;
	DECLARE done INT DEFAULT 0;
	DECLARE MyBankNo VARCHAR(100);
	DECLARE MyBankDebet DOUBLE;
	DECLARE MyBankKredit DOUBLE;
	DECLARE cur1 CURSOR FOR SELECT BankNo, BankDebet, BankKredit FROM vtbanksaldo;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
	

	IF xTipe = "Bank Masuk" THEN
		SET @MyQuery = CONCAT("
		SELECT BMNo, BMTgl, BMMemo, BMNominal, BMJenis, BankKode, MotorNoPolisi, BMPerson, BMCekNo, BMCekTempo, BMAC, UserID, KodeTrans FROM ttbmhd
      WHERE DATE(BMTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery," AND BMJenis LIKE '%" , xJenis , "' ");
      SET @MyQuery = CONCAT(@MyQuery," AND BankKode LIKE '%" , xKas , "' ");
      SET @MyQuery = CONCAT(@MyQuery," AND KodeTrans LIKE '%" , xTC , "' ");     
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
	END IF;
	IF xTipe = "Bank Keluar" THEN
		SET @MyQuery = CONCAT("
		SELECT BKNo, BKTgl, BKMemo, BKNominal, BKJenis, BankKode, SupKode, BKPerson, BKCekNo, BKCekTempo, BKAC, UserID, KodeTrans FROM ttbkhd 
      WHERE DATE(BKTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery," AND BKJenis LIKE '%" , xJenis , "' ");
      SET @MyQuery = CONCAT(@MyQuery," AND BankKode LIKE '%" , xKas , "' ");
      SET @MyQuery = CONCAT(@MyQuery," AND KodeTrans LIKE '%" , xTC , "' ");     
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
	END IF;
	
	IF xTipe = "Bank Saldo" THEN
		SET Tanggal = DATE_ADD(xTgl1, INTERVAL -1 DAY);
		SELECT IFNULL(MAX(SaldoBankTgl), STR_TO_DATE('01/01/1900', '%m/%d/%Y')) INTO MyDateAwalSaldo FROM tsaldobank WHERE SaldoBankTgl <= Tanggal AND BankKode LIKE CONCAT('%',xKas) ;
		SET TanggalSaldo = MyDateAwalSaldo;
		SET TanggalAwal = DATE_ADD(TanggalSaldo, INTERVAL 1 DAY);
		SET TanggalAkhir = Tanggal;
		SELECT IFNULL(SUM(SaldoBankSaldo),0) INTO SaldoAwal FROM tsaldobank WHERE  DATE(SaldoBankTgl) = TanggalSaldo AND BankKode LIKE CONCAT('%',xKas);
		SELECT IFNULL(SUM(BKNominal),0) INTO BankKeluar FROM ttbkhd WHERE  DATE(BKTgl) BETWEEN TanggalAwal AND TanggalAkhir AND BankKode LIKE CONCAT('%',xKas);
		SELECT IFNULL(SUM(BMNominal),0) INTO BankMasuk FROM ttbmhd WHERE  DATE(BMTgl) BETWEEN TanggalAwal AND TanggalAkhir AND BankKode LIKE CONCAT('%',xKas);
		SET SaldoAkhir = SaldoAwal + BankMasuk - BankKeluar;
		SET MySaldo = SaldoAkhir;
		DROP TEMPORARY TABLE IF EXISTS `vtbanksaldo`;
		CREATE TEMPORARY TABLE IF NOT EXISTS `vtbanksaldo` AS
		SELECT '--' AS BankNo, Tanggal AS BankTgl, 'Saldo Awal' AS BankMemo,0 AS BankDebet,0 AS BankKredit,
		 '' AS BankJenis,'System' AS BankCekNo, Tanggal AS BankTempo, '--' AS BankPerson, '--' AS BankAC, 
		 'Foen' AS UserID, xKas AS BankKode,'' AS KodePerson, 'Saldo Awal' AS KodeTrans, SaldoAkhir AS BankSaldo, '--' AS NoGL
		UNION
		SELECT BankNo, BankTgl, BankMemo, BankDebet, BankKredit, 
		BankJenis, BankCekNo, BankCekTempo, BankPerson, BankAC, 
		UserID, BankKode, KodePerson, KodeTrans, BankSaldo, NoGL  FROM vtbanksaldo
		WHERE vtbanksaldo.BankKode LIKE  CONCAT('%',xKas) AND DATE(vtbanksaldo.BankTgl) BETWEEN xTgl1 AND xTgl2
		ORDER BY BankTgl, BankNo;
		CREATE INDEX BankNo ON vtbanksaldo(BankNo);
		/*
		OPEN cur1;
		REPEAT FETCH cur1 INTO MyKasNo, MyKasDebet, MyKasKredit;
			IF ! done THEN
				SET MySaldo = MySaldo + MyKasDebet - MyKasKredit;
				UPDATE vtkassaldo SET KasSaldo = MySaldo WHERE KasNo = MyKasNo ;	
			END IF;
		UNTIL done END REPEAT;
		CLOSE cur1;
		*/		
		SET @MyQuery = CONCAT("SELECT * FROM vtbanksaldo;");
	END IF;
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$

DELIMITER ;

