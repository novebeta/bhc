DELIMITER $$
DROP PROCEDURE IF EXISTS `rServisInvoice`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rServisInvoice`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,IN xTC VARCHAR(30),IN xPOS VARCHAR(30),IN xStatus VARCHAR(250),IN xASS VARCHAR(250),IN xSort VARCHAR(20),IN xOrder VARCHAR(10))
BEGIN
/*
	CALL rServisInvoice('Header1','2020-01-01','2020-01-31','','','','','SDTgl','ASC');
*/
	SET xTC = CONCAT("%",xTC);
	SET xPOS = CONCAT("%",xPOS);
	SET xASS = CONCAT("%",xASS);
	SET xStatus = xStatus;

	UPDATE ttsdhd SET PKBNo = REPLACE(PKBNo,'--', '') WHERE PKBNo <> '--' AND DATE(ttsdhd.SVTgl) BETWEEN xTgl1 AND xTgl2;
	
	IF xTipe = "Header1" THEN
		SET @MyQuery = CONCAT("
		SELECT ttsdhd.SDNo, ttsdhd.SDTgl, ttsdhd.SDJamMasuk, ttsdhd.SDNoUrut, ttsdhd.SDStatus, ttsdhd.SDKmSekarang, ttsdhd.SDKmBerikut, ttsdhd.SDKeluhan, ttsdhd.MotorNoPolisi, ttsdhd.KarKode, ttsdhd.LokasiKode, 
		ttsdhd.SDJamDikerjakan, ttsdhd.SDJamSelesai, ttsdhd.SDTotalWaktu, ttsdhd.SDTotalJasa, ttsdhd.SDTotalQty, ttsdhd.SDTotalPart, ttsdhd.SDTotalGratis, ttsdhd.SDTotalNonGratis, ttsdhd.SDDiscFinal, ttsdhd.SDTotalPajak, 
      ttsdhd.SDTotalBiaya, ttsdhd.SVNo, ttsdhd.SVTgl, ttsdhd.UserID, IFNULL(tdkaryawan.KarNama, '--') AS KarNama, IFNULL(tdcustomer.CusNama, '--') AS CusNama, IFNULL(tdcustomer.MotorType, '--') AS MotorType, 
      IFNULL(tdcustomer.MotorWarna, '--') AS MotorWarna, IFNULL(tdmotortype.MotorNama, '--') AS MotorNama, fWaktu(ttsdhd.SDTotalWaktu) AS SDWaktu, tdcustomer.CusTelepon, tdcustomer.MotorNoMesin, 
      tdcustomer.MotorNoRangka, ttsdhd.SDPembawaMotor, ttsdhd.SDHubunganPembawa, ttsdhd.SDAlasanServis, ttsdhd.SDKasKeluar, ttsdhd.NoGLSD, ttsdhd.NoGLSV, ttsdhd.KodeTrans, ttsdhd.PKBNo, ttsdhd.PosKode, 
      ttsdhd.Cetak, ttsdhd.PrgNama, ttsdhd.SDKeterangan, ttsdhd.SDUangMuka, ttsdhd.SDJenis, ttsdhd.SDTOP, ttsdhd.SDDurasi, ttsdhd.SENo, ttsdhd.ASS, ttsdhd.SDTerbayar, ttsdhd.KlaimKPB, ttsdhd.KlaimC2
		FROM ttsdhd LEFT OUTER JOIN
      tdkaryawan ON ttsdhd.KarKode = tdkaryawan.KarKode LEFT OUTER JOIN
      tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi LEFT OUTER JOIN
      tdmotortype ON tdcustomer.MotorType = tdmotortype.MotorType
      WHERE DATE(ttsdhd.SVTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'",
      " AND KodeTrans LIKE '",xTC,"' ",
      " AND ASS LIKE  '",xASS,"' ", 
      " AND ttsdhd.PosKode LIKE '",xPOS,"' ", xStatus,
      "ORDER BY ",xSort," ",xOrder);
	END IF;
	IF xTipe = "Item1" THEN
		SET @MyQuery = CONCAT("
		SELECT ttsdhd.SVNo, ttsdhd.SVTgl, tdcustomer.MotorNoPolisi,tdcustomer.CusNama, ttsditjasa.SDNo, ttsditjasa.SDAuto, ttsditjasa.JasaKode AS BrgKode, ttsditjasa.SDWaktuKerja AS SDQty, ttsditjasa.SDWaktuSatuan AS BrgSatuan, ttsditjasa.SDHrgJual, 0 AS SDHargaBeli, ttsditjasa.SDJualDisc, 
      tdjasa.JasaNama AS BrgNama, ttsditjasa.SDJualDisc / ttsditjasa.SDHrgJual * 100 AS Disc, ttsditjasa.SDHrgJual AS Jumlah, 'Jasa' AS Jenis, ttsdhd.SDTgl, ttsdhd.KodeTrans, ttsdhd.ASS, ttsdhd.SDStatus, 
      tdjasa.JasaGroup AS BrgGroup, ttsdhd.SDDiscFinal, ttsdhd.KarKode, ttsditjasa.SDHrgJual - ttsditjasa.SDJualDisc AS Laba
		FROM ttsditjasa INNER JOIN
      tdjasa ON ttsditjasa.JasaKode = tdjasa.JasaKode INNER JOIN
      ttsdhd ON ttsditjasa.SDNo = ttsdhd.SDNo
      INNER JOIN tdcustomer ON tdcustomer.MotorNoPolisi = ttsdhd.MotorNoPolisi
      WHERE DATE(ttsdhd.SVTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'",
      " AND KodeTrans LIKE '",xTC,"' ",
      " AND ASS LIKE  '",xASS,"' ", 
      " AND ttsdhd.PosKode LIKE '",xPOS,"' ", xStatus,
		"UNION
		SELECT ttsdhd_1.SVNo, ttsdhd_1.SVTgl, tdcustomer.MotorNoPolisi,tdcustomer.CusNama, ttsditbarang.SDNo, ttsditbarang.SDAuto, ttsditbarang.BrgKode, ttsditbarang.SDQty, tdbarang.BrgSatuan, ttsditbarang.SDHrgJual, 
		(ttsditbarang.SDQty * ttsditbarang.SDHrgBeli) AS SDHrgBeli, ttsditbarang.SDJualDisc, tdbarang.BrgNama, 
      ttsditbarang.SDJualDisc / ttsditbarang.SDHrgJual * 100 AS Disc, ttsditbarang.SDQty * ttsditbarang.SDHrgJual AS Jumlah, 'Barang' AS Jenis, ttsdhd_1.SDTgl, ttsdhd_1.KodeTrans, ttsdhd_1.ASS, 
      ttsdhd_1.SDStatus, tdbarang.BrgGroup, ttsdhd_1.SDDiscFinal, ttsdhd_1.KarKode, (ttsditbarang.SDQty * (ttsditbarang.SDHrgJual-ttsditbarang.SDHrgBeli)) - ttsditbarang.SDJualDisc AS Laba
		FROM ttsditbarang INNER JOIN
      tdbarang ON ttsditbarang.BrgKode = tdbarang.BrgKode INNER JOIN
      ttsdhd ttsdhd_1 ON ttsditbarang.SDNo = ttsdhd_1.SDNo
      INNER JOIN tdcustomer ON tdcustomer.MotorNoPolisi = ttsdhd_1.MotorNoPolisi
      WHERE DATE(ttsdhd_1.SVTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'",
      " AND ttsdhd_1.KodeTrans LIKE '",xTC,"' ",
      " AND ttsdhd_1.ASS LIKE  '",xASS,"' ", 
      " AND ttsdhd_1.PosKode LIKE '",xPOS,"' ", xStatus);
	END IF;
	IF xTipe = "Item2" THEN
		SET @MyQuery = CONCAT("
		SELECT ttsdhd.SVNo, ttsdhd.SVTgl, tdcustomer.MotorNoPolisi,tdcustomer.CusNama, ttsditjasa.SDNo, ttsditjasa.SDAuto, ttsditjasa.JasaKode AS BrgKode, ttsditjasa.SDWaktuKerja AS SDQty, ttsditjasa.SDWaktuSatuan AS BrgSatuan, ttsditjasa.SDHrgJual, 0 AS SDHargaBeli, ttsditjasa.SDJualDisc, 
      tdjasa.JasaNama AS BrgNama, ttsditjasa.SDJualDisc / ttsditjasa.SDHrgJual * 100 AS Disc, ttsditjasa.SDHrgJual - ttsditjasa.SDJualDisc AS Jumlah, 'Jasa' AS Jenis, ttsdhd.SDTgl, ttsdhd.KodeTrans, ttsdhd.ASS, ttsdhd.SDStatus, 
      tdjasa.JasaGroup AS BrgGroup, ttsdhd.SDDiscFinal, ttsdhd.KarKode, ttsditjasa.SDHrgJual - ttsditjasa.SDJualDisc AS Laba
		FROM ttsditjasa INNER JOIN
      tdjasa ON ttsditjasa.JasaKode = tdjasa.JasaKode INNER JOIN
      ttsdhd ON ttsditjasa.SDNo = ttsdhd.SDNo
      INNER JOIN tdcustomer ON tdcustomer.MotorNoPolisi = ttsdhd.MotorNoPolisi
      WHERE DATE(ttsdhd.SVTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'",
      " AND KodeTrans LIKE '",xTC,"' ",
      " AND ASS LIKE  '",xASS,"' ", 
      " AND ttsdhd.PosKode LIKE '",xPOS,"' ", xStatus,
		"UNION
		SELECT ttsdhd_1.SVNo, ttsdhd_1.SVTgl, tdcustomer.MotorNoPolisi,tdcustomer.CusNama, ttsditbarang.SDNo, ttsditbarang.SDAuto, ttsditbarang.BrgKode, ttsditbarang.SDQty, tdbarang.BrgSatuan, ttsditbarang.SDHrgJual, ttsditbarang.SDHrgBeli, ttsditbarang.SDJualDisc, tdbarang.BrgNama, 
      ttsditbarang.SDJualDisc / ttsditbarang.SDHrgJual * 100 AS Disc, ttsditbarang.SDQty * ttsditbarang.SDHrgJual - ttsditbarang.SDJualDisc AS Jumlah, 'Barang' AS Jenis, ttsdhd_1.SDTgl, ttsdhd_1.KodeTrans, ttsdhd_1.ASS, 
      ttsdhd_1.SDStatus, tdbarang.BrgGroup, ttsdhd_1.SDDiscFinal, ttsdhd_1.KarKode, (ttsditbarang.SDQty * (ttsditbarang.SDHrgJual-ttsditbarang.SDHrgBeli)) - ttsditbarang.SDJualDisc AS Laba
		FROM ttsditbarang INNER JOIN
      tdbarang ON ttsditbarang.BrgKode = tdbarang.BrgKode INNER JOIN
      ttsdhd ttsdhd_1 ON ttsditbarang.SDNo = ttsdhd_1.SDNo
      INNER JOIN tdcustomer ON tdcustomer.MotorNoPolisi = ttsdhd_1.MotorNoPolisi
      WHERE DATE(ttsdhd_1.SVTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'",
      " AND ttsdhd_1.KodeTrans LIKE '",xTC,"' ",
      " AND ttsdhd_1.ASS LIKE  '",xASS,"' ", 
      " AND ttsdhd_1.PosKode LIKE '",xPOS,"' ", xStatus);	END IF;
	IF xTipe = "Item3" THEN
		SET @MyQuery = CONCAT("
		SELECT ttsdhd.SVNo, ttsdhd.SVTgl, tdcustomer.MotorNoPolisi,tdcustomer.CusNama, ttsditjasa.SDNo, ttsditjasa.SDAuto, ttsditjasa.JasaKode AS BrgKode, ttsditjasa.SDWaktuKerja AS SDQty, ttsditjasa.SDWaktuSatuan AS BrgSatuan, ttsditjasa.SDHrgJual, 0 AS SDHargaBeli, ttsditjasa.SDJualDisc, 
      tdjasa.JasaNama AS BrgNama, ttsditjasa.SDJualDisc / ttsditjasa.SDHrgJual * 100 AS Disc, ttsditjasa.SDHrgJual - ttsditjasa.SDJualDisc AS Jumlah, 'Jasa' AS Jenis, ttsdhd.SDTgl, ttsdhd.KodeTrans, ttsdhd.ASS, ttsdhd.SDStatus, 
      tdjasa.JasaGroup AS BrgGroup, ttsdhd.SDDiscFinal, ttsdhd.KarKode, ttsditjasa.SDHrgJual - ttsditjasa.SDJualDisc AS Laba
		FROM ttsditjasa INNER JOIN
      tdjasa ON ttsditjasa.JasaKode = tdjasa.JasaKode INNER JOIN
      ttsdhd ON ttsditjasa.SDNo = ttsdhd.SDNo
      INNER JOIN tdcustomer ON tdcustomer.MotorNoPolisi = ttsdhd.MotorNoPolisi
      WHERE DATE(ttsdhd.SVTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'",
      " AND KodeTrans LIKE '",xTC,"' ",
      " AND ASS LIKE  '",xASS,"' ", 
      " AND ttsdhd.PosKode LIKE '",xPOS,"' ", xStatus,
		"UNION
		SELECT ttsdhd_1.SVNo, ttsdhd_1.SVTgl, tdcustomer.MotorNoPolisi,tdcustomer.CusNama, ttsditbarang.SDNo, ttsditbarang.SDAuto, ttsditbarang.BrgKode, ttsditbarang.SDQty, tdbarang.BrgSatuan, ttsditbarang.SDHrgJual, ttsditbarang.SDHrgBeli, ttsditbarang.SDJualDisc, tdbarang.BrgNama, 
      ttsditbarang.SDJualDisc / ttsditbarang.SDHrgJual * 100 AS Disc, ttsditbarang.SDQty * ttsditbarang.SDHrgJual - ttsditbarang.SDJualDisc AS Jumlah, 'Barang' AS Jenis, ttsdhd_1.SDTgl, ttsdhd_1.KodeTrans, ttsdhd_1.ASS, 
      ttsdhd_1.SDStatus, tdbarang.BrgGroup, ttsdhd_1.SDDiscFinal, ttsdhd_1.KarKode, (ttsditbarang.SDQty * (ttsditbarang.SDHrgJual-ttsditbarang.SDHrgBeli)) - ttsditbarang.SDJualDisc AS Laba
		FROM ttsditbarang INNER JOIN
      tdbarang ON ttsditbarang.BrgKode = tdbarang.BrgKode INNER JOIN
      ttsdhd ttsdhd_1 ON ttsditbarang.SDNo = ttsdhd_1.SDNo
      INNER JOIN tdcustomer ON tdcustomer.MotorNoPolisi = ttsdhd_1.MotorNoPolisi
      WHERE DATE(ttsdhd_1.SVTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'",
      " AND ttsdhd_1.KodeTrans LIKE '",xTC,"' ",
      " AND ttsdhd_1.ASS LIKE  '",xASS,"' ", 
      " AND ttsdhd_1.PosKode LIKE '",xPOS,"' ", xStatus);   
   END IF;
   PREPARE STMT FROM @MyQuery;
   EXECUTE Stmt;
END$$
DELIMITER ;

