DELIMITER $$
DROP PROCEDURE IF EXISTS `rPenjualanRetur`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rPenjualanRetur`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,
IN xStatus VARCHAR(100),
IN xSort VARCHAR(150),IN xOrder VARCHAR(10))
BEGIN
/*
		CALL rPenjualanRetur('Header1','2020-01-01','2020-05-31','','SRTgl','ASC');
*/
	IF xTipe = "Header1" THEN
		SET @MyQuery = CONCAT("
		SELECT ttsrhd.SRNo, ttsrhd.SRTgl, ttsrhd.MotorNoPolisi, ttsrhd.LokasiKode, ttsrhd.KarKode, ttsrhd.SRKeterangan, ttsrhd.SRSubTotal, ttsrhd.SRDiscFinal, ttsrhd.SRTotalPajak, ttsrhd.SRBiayaKirim, ttsrhd.SRTotal, ttsrhd.UserID, 
      tdkaryawan.KarNama, tdmotortype.MotorNama, tdcustomer.MotorWarna, tdcustomer.CusNama, ttsrhd.NoGL, ttsrhd.KodeTrans, ttsrhd.SINo, ttsrhd.PosKode, ttsrhd.Cetak, ttsrhd.SRTerbayar
      FROM ttsrhd INNER JOIN
      tdkaryawan ON ttsrhd.KarKode = tdkaryawan.KarKode LEFT OUTER JOIN
      tdcustomer ON ttsrhd.MotorNoPolisi = tdcustomer.MotorNoPolisi LEFT OUTER JOIN
      tdmotortype ON tdcustomer.MotorType = tdmotortype.MotorType
      WHERE DATE(ttsrhd.SRTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery,xStatus);
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
	END IF;
	IF xTipe = "Item1" THEN
		SET @MyQuery = CONCAT("
		SELECT ttsrit.SRNo, ttsrit.SRAuto, ttsrit.BrgKode, ttsrit.SRQty, ttsrit.SRHrgBeli, ttsrit.SRHrgJual, ttsrit.SRDiscount, ttsrit.SRPajak, tdbarang.BrgNama, tdbarang.BrgSatuan, ttsrit.SRDiscount / (ttsrit.SRHrgBeli * ttsrit.SRQty) * 100 AS Disc, 
      ttsrit.SRQty * ttsrit.SRHrgBeli - ttsrit.SRDiscount AS Jumlah, ttsrit.LokasiKode, tdbarang.BrgGroup, ttsrhd.SRTgl
		FROM ttsrit INNER JOIN
      tdbarang ON ttsrit.BrgKode = tdbarang.BrgKode INNER JOIN
      ttsrhd ON ttsrit.SRNo = ttsrhd.SRNo
      WHERE DATE(ttsrhd.SRTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery,xStatus);    
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY ttsrit.SRNo, ttsrit.SRAuto,  " ,xSort," ",xOrder );    
	END IF;
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$
DELIMITER ;

