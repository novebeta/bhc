DELIMITER $$
DROP PROCEDURE IF EXISTS `rPickingList`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rPickingList`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,
IN xTC VARCHAR(10),IN xASS VARCHAR(10),IN xStatus VARCHAR(10),IN xCetak VARCHAR(10),
IN xSort VARCHAR(150),IN xOrder VARCHAR(10))
BEGIN
/*
	CALL rPickingList('Header1','2020-01-01','2020-05-31','','','','','ttsdhd.SDTgl','ASC');
*/
	IF xTipe = "Header1" THEN
		SET @MyQuery = CONCAT("
      SELECT ttsdhd.SDNo, ttsdhd.SDTgl, ttsditbarang.SDPickingNo, ttsditbarang.SDPickingDate, ttsditbarang.Cetak, 
      SUM(ttsditbarang.SDQty) AS SDQty, SUM(ttsditbarang.SDHrgBeli) AS SDHrgBeli, SUM(ttsditbarang.SDHrgJual) AS SDHrgJual, SUM(ttsditbarang.SDJualDisc) AS SDJualDisc, ttsdhd.KodeTrans, ttsdhd.ASS, ttsdhd.KarKode
      FROM  ttsditbarang INNER JOIN ttsdhd ON ttsdhd.SDNo = ttsditbarang.SDNo
      WHERE DATE(ttsditbarang.SDPickingDate) BETWEEN '",xTgl1,"' AND '",xTgl2,"'",
      " AND KodeTrans LIKE '%" , xTC , "%' AND ASS LIKE '%" , xASS , "' AND SDStatus LIKE '%" , xStatus , "' ",
      " AND ttsditbarang.Cetak LIKE '%" , xCetak , "' ",
      " GROUP BY SDPickingNo");

		SET @MyQuery = CONCAT(@MyQuery," UNION
      SELECT ttsihd.SINo AS SDNo, ttsihd.SITgl AS SDTgl, ttsiit.SIPickingNo AS SDPickingNo, ttsiit.SIPickingDate AS SDPickingDate, ttsiit.Cetak AS Cetak, 
      SUM(ttsiit.SIQty) AS SDQty, SUM(ttsiit.SIHrgBeli) AS SDHrgBeli, SUM(ttsiit.SIHrgJual) AS SDHrgJual, SUM(ttsiit.SIDiscount) AS SDJualDisc, ttsihd.KodeTrans, '--' AS ASS, ttsihd.KarKode
      FROM  ttsiit INNER JOIN ttsihd ON ttsihd.SINo = ttsiit.SINo
      WHERE DATE(ttsiit.SIPickingDate) BETWEEN '",xTgl1,"' AND '",xTgl2,"'",
      " AND KodeTrans LIKE '%" , xTC , "%'",
      " AND ttsiit.Cetak  LIKE '%" , xCetak , "'",
      " GROUP BY SIPickingNo");
  
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
	END IF;
	IF xTipe = "Item1" THEN
		SET @MyQuery = CONCAT("
		SELECT ttsdhd.SDNo, ttsditbarang.SDPickingNo, ttsditbarang.SDPickingDate, ttsditbarang.BrgKode, tdbarang.BrgNama, 
		ttsditbarang.SDQty, ttsditbarang.SDHrgBeli, ttsditbarang.LokasiKode, ttsditbarang.Cetak, tdbarang.BrgSatuan, 	
		tdbarang.BrgGroup, ttsdhd.KodeTrans
		FROM  ttsditbarang 
      INNER JOIN ttsdhd ON ttsdhd.SDNo = ttsditbarang.SDNo 
      INNER JOIN tdbarang ON ttsditbarang.BrgKode = tdbarang.BrgKode
      WHERE DATE(ttsditbarang.SDPickingDate) BETWEEN '",xTgl1,"' AND '",xTgl2,"'"
		" AND KodeTrans LIKE '%" , xTC , "%' AND ASS LIKE '%" , xASS , "' AND SDStatus LIKE '%" , xStatus , "' "
		" AND ttsditbarang.Cetak LIKE '%" , xCetak , "' "
      );
      SET @MyQuery = CONCAT(@MyQuery," UNION
      SELECT ttsihd.siNo, ttsiit.siPickingNo, ttsiit.siPickingDate, ttsiit.BrgKode, tdbarang.BrgNama, 
      ttsiit.siQty, ttsiit.siHrgBeli, ttsiit.LokasiKode, ttsiit.Cetak, tdbarang.BrgSatuan, 
      tdbarang.BrgGroup, ttsihd.KodeTrans
      FROM  ttsiit 
      INNER JOIN ttsihd ON ttsihd.siNo = ttsiit.siNo 
      INNER JOIN tdbarang ON ttsiit.BrgKode = tdbarang.BrgKode
      WHERE DATE(ttsiit.SIPickingDate) BETWEEN '",xTgl1,"' AND '",xTgl2,"'",
      " AND KodeTrans LIKE '%" , xTC , "%'",
      " AND ttsiit.Cetak  LIKE '%" , xCetak , "'");
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );    
	END IF;
	
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$
DELIMITER ;

