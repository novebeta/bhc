DELIMITER $$
DROP PROCEDURE IF EXISTS `rTransfer`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rTransfer`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,
IN xTC VARCHAR(100), IN xSupplier VARCHAR(100),
IN xSort VARCHAR(150),IN xOrder VARCHAR(10))
BEGIN
/*
		CALL rTransfer('Header1','2020-01-01','2020-05-31','','','TITgl','ASC');
*/
	IF xTipe = "Header1" THEN
		SET @MyQuery = CONCAT("
		SELECT tttihd.Cetak, tttihd.KodeTrans, tttihd.LokasiAsal, tttihd.LokasiTujuan, tttihd.NoGL, tttihd.PSNo, 
      tttihd.PosKode, tttihd.TIKeterangan, tttihd.TINo, tttihd.TITgl, tttihd.TITotal, tttihd.UserID, ttpshd.SupKode
      FROM tttihd LEFT OUTER JOIN ttpshd ON tttihd.PSNo = ttpshd.PSNo
      WHERE DATE(tttihd.TITgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery," AND tttihd.KodeTrans LIKE '%" , xTC , "' ");
      SET @MyQuery = CONCAT(@MyQuery," AND ttpshd.SupKode LIKE '%" , xSupplier , "' ");
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
	END IF;
	IF xTipe = "Item1" THEN
		SET @MyQuery = CONCAT("
		SELECT tttiit.TINo, tttiit.TIAuto, tttiit.BrgKode, tttiit.TIQty, tttiit.TIHrgBeli, tdbarang.BrgNama, tdbarang.BrgSatuan, tttiit.TIQty * tttiit.TIHrgBeli AS Jumlah, tttiit.LokasiAsal, tttiit.LokasiTujuan, tdbarang.BrgRak1, tdbarang.BrgGroup, tttihd.TITgl 
      FROM  tttiit 
      INNER JOIN tdbarang ON tttiit.BrgKode = tdbarang.BrgKode 
      INNER JOIN tttihd ON tttiit.TINo = tttihd.TINo 
      LEFT OUTER JOIN ttpshd ON ttpshd.PSNo = tttihd.PSNo 
      WHERE DATE(tttihd.TITgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery," AND tttihd.KodeTrans LIKE '%" , xTC , "' ");
      SET @MyQuery = CONCAT(@MyQuery," AND ttpshd.SupKode LIKE '%" , xSupplier , "' ");
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY tttiit.TINo, tttiit.TIAuto, " ,xSort," ",xOrder );
	END IF;
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$
DELIMITER ;

