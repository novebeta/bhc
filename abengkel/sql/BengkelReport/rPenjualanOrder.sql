DELIMITER $$
DROP PROCEDURE IF EXISTS `rPenjualanOrder`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rPenjualanOrder`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,
IN xStatus VARCHAR(100),
IN xSort VARCHAR(150),IN xOrder VARCHAR(10))
BEGIN
/*
		CALL rPenjualanOrder('Header1','2020-01-01','2020-05-31','','SOTgl','ASC');
*/
	IF xTipe = "Header1" THEN
		SET @MyQuery = CONCAT("
		SELECT ttsohd.SONo, ttsohd.SOTgl, ttsohd.SINo, ttsohd.MotorNoPolisi, ttsohd.LokasiKode, ttsohd.KarKode, ttsohd.SOKeterangan, ttsohd.SOSubTotal, ttsohd.SODiscFinal, ttsohd.SOTotalPajak, ttsohd.SOBiayaKirim, ttsohd.SOTotal, 
      ttsohd.UserID, ttsohd.KodeTrans, ttsohd.PosKode, ttsohd.Cetak, tdkaryawan.KarNama, tdcustomer.CusNama, tdcustomer.MotorWarna, tdcustomer.MotorNama, ttsohd.PONo, ttsohd.SOTerbayar
		FROM ttsohd INNER JOIN
		tdkaryawan ON ttsohd.KarKode = tdkaryawan.KarKode LEFT OUTER JOIN
      tdcustomer ON ttsohd.MotorNoPolisi = tdcustomer.MotorNoPolisi
      WHERE DATE(ttsohd.SOTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery,xStatus);
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
	END IF;
	IF xTipe = "Item1" THEN
		SET @MyQuery = CONCAT("
		SELECT ttsoit.SONo, ttsoit.SOAuto, ttsoit.BrgKode, ttsoit.SOQty, ttsoit.SOHrgBeli, ttsoit.SOHrgJual, ttsoit.SODiscount, ttsoit.SOPajak, tdbarang.BrgNama, tdbarang.BrgSatuan, 
		ttsoit.SODiscount / (ttsoit.SOHrgBeli * ttsoit.SOQty) * 100 AS Disc, ttsoit.SOQty * ttsoit.SOHrgBeli - ttsoit.SODiscount AS Jumlah, tdbarang.BrgGroup, ttsoit.SINo, ttsoit.PONo, ttsohd.SOTgl
		FROM ttsoit INNER JOIN
      tdbarang ON ttsoit.BrgKode = tdbarang.BrgKode INNER JOIN
      ttsohd ON ttsoit.SONo = ttsohd.SONo
      WHERE DATE(ttsohd.SOTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery,xStatus);
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY ttsoit.SONo, ttsoit.SOAuto, " ,xSort," ",xOrder );    
	END IF;
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$
DELIMITER ;

