DELIMITER $$
DROP PROCEDURE IF EXISTS `rKas`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rKas`(IN xTipe VARCHAR(20),IN TglAwal DATE,IN TglAkhir DATE,
IN xTC VARCHAR(100),IN xKas VARCHAR(100), xJenis VARCHAR(100), 
IN xSort VARCHAR(150),IN xOrder VARCHAR(10))
BEGIN
/*
		CALL rKas('Header1','2020-01-01','2020-05-31','','','','KMTgl','ASC');
*/
	DECLARE Tanggal DATE;
	DECLARE MyDateAwalSaldo DATE;
	DECLARE TanggalSaldo DATE;
	DECLARE TanggalAwal DATE;
	DECLARE TanggalAkhir DATE;
	DECLARE SaldoAwal DOUBLE;
	DECLARE SaldoAkhir DOUBLE;
	DECLARE KasKeluar DOUBLE;
	DECLARE KasMasuk DOUBLE;
	DECLARE MySaldo DOUBLE;
	DECLARE done INT DEFAULT 0;
	DECLARE MyKasNo VARCHAR(100);
	DECLARE MyKasDebet DOUBLE;
	DECLARE MyKasKredit DOUBLE;
	DECLARE cur1 CURSOR FOR SELECT KasNo, KasDebet, KasKredit FROM vtkassaldo;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

	IF xTipe = "Kas Masuk" THEN
		SET @MyQuery = CONCAT("
		SELECT KMNo, KMTgl, KMJenis, KasKode, MotorNoPolisi, KMPerson, KMMemo, KMBayarTunai, KMNominal, UserID, KodeTrans FROM ttkmhd
      WHERE DATE(KMTgl) BETWEEN '",TglAwal,"' AND '",TglAkhir,"'");
      SET @MyQuery = CONCAT(@MyQuery," AND KMJenis LIKE '%" , xJenis , "' ");
      SET @MyQuery = CONCAT(@MyQuery," AND KasKode LIKE '%" , xKas , "' ");
      SET @MyQuery = CONCAT(@MyQuery," AND KodeTrans LIKE '%" , xTC , "' ");     
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
	END IF;
	IF xTipe = "Kas Keluar" THEN
		SET @MyQuery = CONCAT("
		SELECT KKNo, KKTgl, KKJenis, KasKode, SupKode, KKPerson, KKMemo, KKNominal, UserID, KodeTrans FROM ttkkhd 
      WHERE DATE(KKTgl) BETWEEN '",TglAwal,"' AND '",TglAkhir,"'");
      SET @MyQuery = CONCAT(@MyQuery," AND KKJenis LIKE '%" , xJenis , "' ");
      SET @MyQuery = CONCAT(@MyQuery," AND KasKode LIKE '%" , xKas , "' ");
      SET @MyQuery = CONCAT(@MyQuery," AND KodeTrans LIKE '%" , xTC , "' ");     
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
	END IF;

	IF xTipe = "Kas Saldo" THEN
		SET Tanggal = DATE_ADD(TglAwal, INTERVAL -1 DAY);
		SELECT IFNULL(MAX(SaldoKasTgl), STR_TO_DATE('01/01/1900', '%m/%d/%Y')) INTO MyDateAwalSaldo FROM tsaldokas WHERE SaldoKasTgl <= Tanggal AND KasKode LIKE CONCAT('%',xKas) ;
		SET TanggalSaldo = MyDateAwalSaldo;
		SET TanggalAwal = DATE_ADD(TanggalSaldo, INTERVAL 1 DAY);
		SET TanggalAkhir = Tanggal;
		SELECT IFNULL(SUM(SaldoKasSaldo),0) INTO SaldoAwal FROM tsaldokas WHERE  DATE(SaldoKasTgl) = TanggalSaldo AND KasKode LIKE CONCAT('%',xKas);
		SELECT IFNULL(SUM(KKNominal),0) INTO KasKeluar FROM ttkkhd WHERE  DATE(KKTgl) BETWEEN TanggalAwal AND TanggalAkhir AND KasKode LIKE CONCAT('%',xKas);
		SELECT IFNULL(SUM(KMNominal),0) INTO KasMasuk FROM ttkmhd WHERE  DATE(KMTgl) BETWEEN TanggalAwal AND TanggalAkhir AND KasKode LIKE CONCAT('%',xKas);
		SET SaldoAkhir = SaldoAwal + KasMasuk - KasKeluar;

		SET MySaldo = SaldoAkhir;
		DROP TEMPORARY TABLE IF EXISTS `vtkassaldo`;
		CREATE TEMPORARY TABLE IF NOT EXISTS `vtkassaldo` AS
		SELECT '--' AS KasNo, Tanggal AS KasTgl, 'Saldo Awal' AS KasJenis, xKas AS KasKode,'System' AS KodePerson, 'Saldo Awal' AS KasMemo,0 AS KasDebet,0 AS KasKredit, 'System' AS KasPerson, 'Foen' AS UserID, 'Saldo Awal' AS KodeTrans, SaldoAkhir AS KasSaldo, '--' AS NoGL
		UNION
		SELECT KasNo, KasTgl, KasJenis, KasKode, KodePerson, KasMemo, KasDebet, KasKredit, KasPerson, UserID, KodeTrans, KasSaldo, NoGL  FROM vtkassaldo
		WHERE vtkassaldo.KasKode LIKE  CONCAT('%',xKas) AND DATE(vtkassaldo.KasTgl) BETWEEN TglAwal AND TglAkhir
		ORDER BY KasTgl, KasNo;
		CREATE INDEX KasNo ON vtkassaldo(KasNo);
/*
		OPEN cur1;
		REPEAT FETCH cur1 INTO MyKasNo, MyKasDebet, MyKasKredit;
			IF ! done THEN
				SET MySaldo = MySaldo + MyKasDebet - MyKasKredit;
				UPDATE vtkassaldo SET KasSaldo = MySaldo WHERE KasNo = MyKasNo ;	
			END IF;
		UNTIL done END REPEAT;
		CLOSE cur1;
*/
		SET @MyQuery = CONCAT("SELECT * FROM vtkassaldo;");
	END IF;
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$
DELIMITER ;

