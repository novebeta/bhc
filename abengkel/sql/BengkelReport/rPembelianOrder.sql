DELIMITER $$
DROP PROCEDURE IF EXISTS `rPembelianOrder`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rPembelianOrder`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,
IN xStatus VARCHAR(100),
IN xSort VARCHAR(150),IN xOrder VARCHAR(10))
BEGIN
/*
		CALL rPembelianOrder('Header1','2020-01-01','2020-05-31',' AND ttpohd.PSNo = \'--\' ','POTgl','ASC');
*/
	IF xTipe = "Header1" THEN
		SET @MyQuery = CONCAT("
      SELECT ttpohd.PONo, ttpohd.POTgl, ttpohd.PSNo, ttpohd.SONo, ttpohd.SupKode, ttpohd.LokasiKode, ttpohd.POKeterangan, ttpohd.POSubTotal, ttpohd.PODiscFinal, ttpohd.POTotalPajak, ttpohd.POBiayaKirim, ttpohd.POTotal, 
		ttpohd.UserID, ttpohd.KodeTrans, ttpohd.PosKode, ttpohd.Cetak, tdsupplier.SupNama, IFNULL(ttpshd.PSTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS PSTgl, IFNULL(ttsohd.SOTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SOTgl
		FROM ttpohd INNER JOIN
		tdsupplier ON ttpohd.SupKode = tdsupplier.SupKode LEFT OUTER JOIN
      ttpshd ON ttpohd.PSNo = ttpshd.PSNo LEFT OUTER JOIN
      ttsohd ON ttpohd.SONo = ttsohd.SONo
      WHERE DATE(ttpohd.POTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'",
		xStatus );
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
	END IF;
	IF xTipe = "Item1" OR xTipe = "Item2" THEN
		SET @MyQuery = CONCAT("
		SELECT  ttpoit.PONo, ttpoit.POAuto, ttpoit.BrgKode, ttpoit.POQty, ttpoit.POHrgBeli, ttpoit.POHrgJual, 
		ttpoit.PODiscount, ttpoit.POPajak, tdbarang.BrgNama, tdbarang.BrgSatuan, 0 AS Disc, 
		ttpoit.POQty * ttpoit.POHrgJual - ttpoit.PODiscount AS Jumlah, tdbarang.BrgGroup, ttpoit.PSNo,PSQtyS, ttpoit.SONo, ttpohd.POTgl, ttpshd.PSTgl
		FROM ttpoit 
		INNER JOIN tdbarang ON ttpoit.BrgKode = tdbarang.BrgKode
		INNER JOIN ttpohd ON ttpoit.PONo = ttpohd.PONo
		LEFT OUTER JOIN ttpshd ON ttpoit.PSNo = ttpshd.PSNo
      WHERE DATE(ttpohd.POTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'", 
      xStatus );
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY ttpoit.PONo, ttpoit.POAuto, " ,xSort," ",xOrder );    
	END IF;
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$
DELIMITER ;

