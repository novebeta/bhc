DELIMITER $$
DROP PROCEDURE IF EXISTS `rGLNeracaPercobaan`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rGLNeracaPercobaan`(IN xTipe VARCHAR(20),IN TglAwal DATE,IN TglAkhir DATE,
IN xPerkiraan1 VARCHAR(10),IN xRingkas DECIMAL(1), 
OUT xOpeningBalance DECIMAL(18,2), OUT xDebet DECIMAL(18,2), OUT xKredit DECIMAL(18,2), OUT xSaldo DECIMAL(18,2) )
BEGIN
/* CALL rglNeracaPercobaan('Data Perkiraan','2020-01-01','2020-05-31','%',0,@OpeningBalance,@Debet,@Kredit,@Saldo); */
	UPDATE traccount SET LevelAccount = fCOALevel(NoAccount);
	CALL rglNeraca(TglAwal, TglAkhir, TglAkhir);
	SELECT OpeningBalance, Debet , Kredit, Saldo INTO xOpeningBalance, xDebet, xKredit, xSaldo FROM traccountsaldo WHERE NoAccount = '00000000';
	IF xTipe = "Data Perkiraan" THEN
		SET @MyQuery = CONCAT("SELECT '' AS NoAccount,  CONCAT(REPEAT(SPACE(19),LevelAccount),NoAccount,SPACE(3),NamaAccount) AS NamaAccount, 
		OpeningBalance, JenisAccount, StatusAccount, NoParent, Debet, Kredit, TglSaldo, Saldo, LevelAccount FROM traccountsaldo
		WHERE NoAccount <> '00000000';");
 	END IF;
	
	IF xTipe = "Neraca Percobaan 1" THEN
		IF xRingkas = 1 THEN
			SET @MyQuery = CONCAT("SELECT '' AS NoAccount,  CONCAT(REPEAT(SPACE(19),LevelAccount),NoAccount,SPACE(3),NamaAccount) AS NamaAccount,  
			OpeningBalance, JenisAccount, StatusAccount, NoParent, Debet, Kredit, TglSaldo, Saldo, LevelAccount FROM traccountsaldo
			WHERE NoAccount LIKE '%", xPerkiraan1 ,"' 
			AND NoAccount <> '00000000'			
			AND (OpeningBalance <> 0 OR Debet <> 0 OR Kredit <> 0 OR Saldo <> 0);");	
		ELSE
			SET @MyQuery = CONCAT("SELECT '' AS NoAccount,  CONCAT(REPEAT(SPACE(19),LevelAccount),NoAccount,SPACE(3),NamaAccount) AS NamaAccount,  
			OpeningBalance, JenisAccount, StatusAccount, NoParent, Debet, Kredit, TglSaldo, Saldo, LevelAccount FROM traccountsaldo
			WHERE NoAccount LIKE '%", xPerkiraan1 ,"'
			AND NoAccount <> '00000000';");
		END IF;
	END IF;
	IF xTipe = "Neraca Percobaan 2" THEN
		IF xRingkas = 1 THEN
			SET @MyQuery = CONCAT("SELECT '' AS NoAccount,  CONCAT(REPEAT(SPACE(19),LevelAccount),NoAccount,SPACE(3),NamaAccount) AS NamaAccount,  
			OpeningBalance, JenisAccount, StatusAccount, NoParent, Debet, Kredit, TglSaldo, Saldo, LevelAccount FROM traccountsaldo
			WHERE NoAccount LIKE '%", xPerkiraan1 ,"' 
			AND NoAccount <> '00000000'		
			AND OpeningBalance <> 0 AND Debet <> 0 AND Kredit <> 0 AND Saldo <> 0;");	
		ELSE
			SET @MyQuery = CONCAT("SELECT '' AS NoAccount,  CONCAT(REPEAT(SPACE(19),LevelAccount),NoAccount,SPACE(3),NamaAccount) AS NamaAccount,  
			OpeningBalance, JenisAccount, StatusAccount, NoParent, Debet, Kredit, TglSaldo, Saldo, LevelAccount FROM traccountsaldo
			WHERE NoAccount LIKE '%", xPerkiraan1 ,"'
			AND NoAccount <> '00000000'		
			;");
		END IF;
	END IF;
	IF xTipe = "Neraca Saldo Detail" THEN
		SET @MyQuery = CONCAT("SELECT  IF(LEFT(NoAccount,1)='1','Aktiva','Pasiva') as MyNoAccount, '' AS NoAccount,  CONCAT(REPEAT(SPACE(19),LevelAccount),NoAccount,SPACE(3),NamaAccount) AS NamaAccount,  
			OpeningBalance, JenisAccount, StatusAccount, NoParent, Debet, Kredit, TglSaldo, 
			IF((JenisAccount = 'Header'),0,Saldo) AS Saldo, LevelAccount FROM traccountsaldo
			WHERE (NoAccount LIKE '1%' OR NoAccount LIKE '2%')
			AND NoAccount <> '00000000'		
			AND Saldo <> 0;");	
	END IF;
	
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$
DELIMITER ;

