DELIMITER $$
DROP PROCEDURE IF EXISTS `rMekanikPivot`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rMekanikPivot`(IN xTipe VARCHAR(50),IN xTgl1 DATE,IN xTgl2 DATE,
IN xJenis VARCHAR(100),IN xUnitEntry VARCHAR(1))
BEGIN
/* CALL rMekanikPivot('Mekanik Type Motor','2020-01-01','2020-05-31','',''); */
	IF xTipe = "Mekanik Group Jasa" THEN
		SET @MyQuery = CONCAT("
		SELECT ttsdhd.KarKode, tdkaryawan.KarNama, JasaGroup, JasaNama, COUNT(ttsdhd.SDNo) AS Jumlah, SUM(SDHrgJual-SDJualDisc) AS JumlahNominal
         FROM ttsdhd
         INNER JOIN ttsditjasa ON ttsdhd.SDNo = ttsditjasa.SDNo
         INNER JOIN tdjasa ON ttsditjasa.JasaKode = tdjasa.JasaKode
         INNER JOIN tdkaryawan ON tdkaryawan.KarKode = ttsdhd.KarKode
      WHERE DATE(ttsdhd.SVTgl)  BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery," GROUP BY KarKode, ttsditjasa.JasaKode ;");
	END IF;
	   
	IF xTipe = "Mekanik Group Part" THEN
		SET @MyQuery = CONCAT("
		SELECT ttsdhd.KarKode, tdkaryawan.KarNama, BrgGroup,  SUM(SDQty) AS Jumlah, SUM(SDQty*SDHrgJual-ttsditbarang.SDJualDisc) AS JumlahNominal
		FROM ttsdhd
		INNER JOIN ttsditbarang ON ttsdhd.SDNo = ttsditbarang.SDNo
		INNER JOIN tdbarang ON ttsditbarang.BrgKode = tdbarang.BrgKode
		INNER JOIN tdkaryawan ON tdkaryawan.KarKode = ttsdhd.KarKode
      WHERE DATE(ttsdhd.SVTgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery," GROUP BY KarKode, tdbarang.BrgGroup ;");
	END IF;
    
	IF xTipe = "Sales Group Part" THEN      
		SET @MyQuery = CONCAT("
		SELECT ttsihd.KarKode, tdkaryawan.KarNama, BrgGroup,  SUM(SIQty) AS Jumlah, SUM(SIQty*SIHrgJual-ttsiit.SIDiscount) AS JumlahNominal
		FROM ttsihd
		INNER JOIN ttsiit ON ttsihd.SINo = ttsiit.SINo
		INNER JOIN tdbarang ON ttsiit.BrgKode = tdbarang.BrgKode
		INNER JOIN tdkaryawan ON tdkaryawan.KarKode = ttsihd.KarKode
      WHERE DATE(ttsihd.SITgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery," GROUP BY KarKode, tdbarang.BrgGroup ;");
    END IF;

	IF xTipe = "Mekanik Part" THEN          
		SET @MyQuery = CONCAT("
		SELECT ttsdhd.KarKode, tdkaryawan.KarNama, CONCAT_WS('\n',ttsditbarang.BrgKode, tdbarang.BrgNama) AS BrgKode,  SUM(SDQty) AS Jumlah, SUM(SDQty*SDHrgJual-ttsditbarang.SDJualDisc) AS JumlahNominal
		FROM ttsdhd
		INNER JOIN ttsditbarang ON ttsdhd.SDNo = ttsditbarang.SDNo
		INNER JOIN tdbarang ON ttsditbarang.BrgKode = tdbarang.BrgKode
		INNER JOIN tdkaryawan ON tdkaryawan.KarKode = ttsdhd.KarKode
      WHERE DATE(ttsdhd.SVTgl)  BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery," GROUP BY KarKode, tdbarang.BrgKode ;");
	END IF;

	IF xTipe = "Mekanik Jasa" THEN          
		SET @MyQuery = CONCAT("
		SELECT ttsdhd.KarKode, tdkaryawan.KarNama, JasaGroup, JasaNama, COUNT(ttsdhd.SDNo) AS Jumlah, SUM(SDHrgJual-SDJualDisc) AS JumlahNominal
         FROM ttsdhd
         INNER JOIN ttsditjasa ON ttsdhd.SDNo = ttsditjasa.SDNo
         INNER JOIN tdjasa ON ttsditjasa.JasaKode = tdjasa.JasaKode
         INNER JOIN tdkaryawan ON tdkaryawan.KarKode = ttsdhd.KarKode
      WHERE DATE(ttsdhd.SVTgl)  BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery," GROUP BY KarKode, ttsditjasa.JasaKode ;");
	END IF;

	IF xTipe = "Sales Part" THEN            
		SET @MyQuery = CONCAT("
		SELECT ttsihd.KarKode, tdkaryawan.KarNama, CONCAT_WS('\n',ttsiit.BrgKode, tdbarang.BrgNama) AS BrgKode,  SUM(SIQty) AS Jumlah, SUM(SIQty*SIHrgJual-ttsiit.SIDiscount) AS JumlahNominal
		FROM ttsihd
		INNER JOIN ttsiit ON ttsihd.SINo = ttsiit.SINo
		INNER JOIN tdbarang ON ttsiit.BrgKode = tdbarang.BrgKode
		INNER JOIN tdkaryawan ON tdkaryawan.KarKode = ttsihd.KarKode
      WHERE DATE(ttsihd.SITgl)  BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery," GROUP BY KarKode, tdbarang.BrgKode ;");
	END IF;

	IF xTipe = "Mekanik Type Motor" THEN  
		IF xUnitEntry = "0" THEN
			SET @MyQuery = CONCAT("
			SELECT  ttsdhd.KarKode, tdkaryawan.KarNama, MotorType, COUNT(SDNo) AS Jumlah
			FROM ttsdhd
			INNER JOIN tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi
			INNER JOIN tdkaryawan ON tdkaryawan.KarKode = ttsdhd.KarKode
			WHERE DATE(ttsdhd.SVTgl)  BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
			SET @MyQuery = CONCAT(@MyQuery," GROUP BY KarKode, MotorType ;");
		ELSE
			SET @MyQuery = CONCAT("
			SELECT KarKode, KarNama, MotorType, COUNT(SDNo) AS Jumlah FROM 
			(SELECT  ttsdhd.KarKode, tdkaryawan.KarNama, MotorType, SDNo
			FROM ttsdhd
			INNER JOIN tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi
			INNER JOIN tdkaryawan ON tdkaryawan.KarKode = ttsdhd.KarKode
			WHERE DATE(ttsdhd.SVTgl)  BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
			SET @MyQuery = CONCAT(@MyQuery," GROUP BY ttsdhd.MotorNoPolisi, DATE(ttsdhd.SDTgl)) A ");
			SET @MyQuery = CONCAT(@MyQuery," GROUP BY KarKode, MotorType ;");
		END IF;
	END IF;

	IF xTipe = "Mekanik Kategori Motor" THEN
		IF xUnitEntry = "0" THEN
			SET @MyQuery = CONCAT("
			SELECT  ttsdhd.KarKode, tdkaryawan.KarNama, MotorKategori, COUNT(SDNo) AS Jumlah
			FROM ttsdhd
			INNER JOIN tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi
			INNER JOIN tdkaryawan ON tdkaryawan.KarKode = ttsdhd.KarKode
			WHERE DATE(ttsdhd.SVTgl)  BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
			SET @MyQuery = CONCAT(@MyQuery," GROUP BY KarKode, MotorKategori ;");
		ELSE
			SET @MyQuery = CONCAT("
			SELECT KarKode, KarNama, MotorKategori, COUNT(SDNo) AS Jumlah FROM 
				(SELECT  ttsdhd.KarKode, tdkaryawan.KarNama, MotorKategori, SDNo
				FROM ttsdhd
				INNER JOIN tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi
				INNER JOIN tdkaryawan ON tdkaryawan.KarKode = ttsdhd.KarKode
			WHERE DATE(ttsdhd.SVTgl)  BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
			SET @MyQuery = CONCAT(@MyQuery," GROUP BY ttsdhd.MotorNoPolisi, DATE(ttsdhd.SDTgl)) A ");
			SET @MyQuery = CONCAT(@MyQuery," GROUP BY KarKode, MotorKategori ;");
		END IF;
	END IF;

	IF xTipe = "Jasa Type Motor" THEN       
		IF xUnitEntry = "0" THEN
			SET @MyQuery = CONCAT("
			SELECT MotorType, JasaGroup, COUNT(ttsdhd.SDNo) AS Jumlah, SUM(SDHrgJual-SDJualDisc) AS JumlahNominal
			FROM ttsdhd
			INNER JOIN ttsditjasa ON ttsdhd.SDNo = ttsditjasa.SDNo
			INNER JOIN tdjasa ON ttsditjasa.JasaKode = tdjasa.JasaKode
			INNER JOIN (SELECT MotorNoPolisi, MotorType, MotorKategori FROM tdcustomer GROUP BY MotorNoPolisi) tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi
			WHERE DATE(ttsdhd.SVTgl)  BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
			SET @MyQuery = CONCAT(@MyQuery," GROUP BY MotorType, JasaGroup ;");
		ELSE
			SET @MyQuery = CONCAT("
			SELECT MotorType, JasaGroup, COUNT(SDNo) AS Jumlah, SUM(SDHrgJual-SDJualDisc) AS JumlahNominal FROM
				(SELECT MotorType, JasaGroup, ttsdhd.SDNo, SDHrgJual,SDJualDisc
				FROM ttsdhd
				INNER JOIN ttsditjasa ON ttsdhd.SDNo = ttsditjasa.SDNo
				INNER JOIN tdjasa ON ttsditjasa.JasaKode = tdjasa.JasaKode
				INNER JOIN (SELECT MotorNoPolisi, MotorType, MotorKategori FROM tdcustomer GROUP BY MotorNoPolisi) tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi
				WHERE DATE(ttsdhd.SVTgl)  BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
			SET @MyQuery = CONCAT(@MyQuery," GROUP BY ttsdhd.MotorNoPolisi, DATE(ttsdhd.SDTgl)) A ");
			SET @MyQuery = CONCAT(@MyQuery," GROUP BY MotorType, JasaGroup ;");
		END IF;
	END IF;

	IF xTipe = "Jasa Kategori Motor" THEN   
		IF xUnitEntry = "0" THEN
			SET @MyQuery = CONCAT("
			SELECT MotorKategori, JasaGroup, COUNT(ttsdhd.SDNo) AS Jumlah, SUM(SDHrgJual-SDJualDisc) AS JumlahNominal
			FROM ttsdhd
			INNER JOIN ttsditjasa ON ttsdhd.SDNo = ttsditjasa.SDNo
			INNER JOIN tdjasa ON ttsditjasa.JasaKode = tdjasa.JasaKode
			INNER JOIN (SELECT MotorNoPolisi, MotorType, MotorKategori FROM tdcustomer GROUP BY MotorNoPolisi) tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi
			WHERE DATE(ttsdhd.SVTgl)  BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
			SET @MyQuery = CONCAT(@MyQuery," GROUP BY MotorKategori, JasaGroup ;");
		ELSE
			SET @MyQuery = CONCAT("
			SELECT MotorKategori, JasaGroup, COUNT(SDNo) AS Jumlah, SUM(SDHrgJual-SDJualDisc) AS JumlahNominal FROM
			(SELECT MotorKategori, JasaGroup, ttsdhd.SDNo, SDHrgJual,SDJualDisc
				FROM ttsdhd
				INNER JOIN ttsditjasa ON ttsdhd.SDNo = ttsditjasa.SDNo
				INNER JOIN tdjasa ON ttsditjasa.JasaKode = tdjasa.JasaKode
				INNER JOIN (SELECT MotorNoPolisi, MotorType, MotorKategori FROM tdcustomer GROUP BY MotorNoPolisi) tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi
				WHERE DATE(ttsdhd.SVTgl)  BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
			SET @MyQuery = CONCAT(@MyQuery," GROUP BY ttsdhd.MotorNoPolisi, DATE(ttsdhd.SDTgl)) A ");
			SET @MyQuery = CONCAT(@MyQuery," GROUP BY MotorKategori, JasaGroup ;");
		END IF;
	END IF;

	IF xTipe = "Mekanik Laba Harian" OR xTipe = "Mekanik Jasa Harian" OR xTipe = "Mekanik Part Harian" THEN 
		DROP TEMPORARY TABLE IF EXISTS RLHDetail;
		CREATE TEMPORARY TABLE IF NOT EXISTS RLHDetail AS
		SELECT SPACE(20) AS PosKode, SPACE(10) AS SVNo, CURDATE()  AS SVTgl, SPACE(20) AS PKBNo,
		SPACE(4) AS KodeTrans, SPACE(10) AS ASS, SPACE (12) AS MotorNoPolisi, SPACE(50) AS CusNama, SPACE(10) AS KarKode,
		000000000000000000.00 AS JasaASS,
		000000000000000000.00 AS JasaC2,
		000000000000000000.00 AS JasaReguler,
		000000000000000000.00 AS JasaTotal,
		000000000000000000.00 AS JasaDisc,
		000000000000000000.00 AS JasaBersih,
		000000000000000000.00 AS PartJual,
		000000000000000000.00 AS OliJual,
		000000000000000000.00 AS OliJualKPB, 
		000000000000000000.00 AS PartBeli,
		000000000000000000.00 AS OliBeli,
		000000000000000000.00 AS OliBeliKPB,
		000000000000000000.00 AS DiscPart,
		000000000000000000.00 AS PartLaba,
		000000000000000000.00 AS OliLaba,
		000000000000000000.00 AS PartOliLaba,
		000000000000000000.00 AS DiscFinal,
		000000000000000000.00 AS LabaTotal;
		CREATE INDEX SVNo ON RLHDetail(SVNo);
		DELETE FROM RLHDetail;
		IF xJenis = "%" OR  xJenis = "SV" THEN 
			INSERT INTO RLHDetail
			SELECT ttsdhd.PosKode, ttsdhd.SVNo,ttsdhd.SVTgl,PKBNo,KodeTrans, ASS,ttsdhd.MotorNoPolisi, tdcustomer.CusNama,ttsdhd.KarKode,
			000000000000000000.00 AS JasaASS,
			000000000000000000.00 AS JasaC2,
			000000000000000000.00 AS JasaReguler,
			000000000000000000.00 AS JasaTotal,
			000000000000000000.00 AS JasaDisc,
			000000000000000000.00 AS JasaBersih,
			SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) AS PartJual,
			SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) AS OliJual,
			SUM(IF(BrgGroup ='Oli' AND KodeTrans = 'TC14',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) AS OliJualKPB, 
			SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS PartBeli,
			SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS OliBeli,
			SUM(IF(BrgGroup ='Oli' AND KodeTrans = 'TC14',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS OliBeliKPB,
			SUM(IFNULL(ttsditbarang.SDJualDisc,000000000000000000.00)) AS DiscPart,
			SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00))- SUM(IF(BrgGroup <>'Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS PartLaba,
			SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00)) - SUM(IF(BrgGroup ='Oli',ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,000000000000000000.00)) AS OliLaba,
			SUM(IFNULL(ttsditbarang.SDQty*ttsditbarang.SDHrgJual,000000000000000000.00))- SUM(IFNULL(ttsditbarang.SDQty*ttsditbarang.SDHrgBeli,0)) - SUM(IFNULL(ttsditbarang.SDJualDisc,000000000000000000.00)) AS PartOliLaba,
			ttsdhd.SDDiscFinal AS DiscFinal,
			000000000000000000.00 AS LabaTotal
			FROM ttsdhd
			LEFT OUTER JOIN ttsditbarang ON ttsdhd.SDNo = ttsditbarang.SDNo
			LEFT OUTER JOIN tdbarang ON tdbarang.BrgKode = ttsditbarang.BrgKode
			LEFT OUTER JOIN tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi
			WHERE DATE(ttsdhd.SVTgl) BETWEEN xTgl1 AND xTgl2
			GROUP BY ttsdhd.SDNo ORDER BY SDTgl;

			DROP TEMPORARY TABLE IF EXISTS RLHDetailJasa;
			CREATE TEMPORARY TABLE IF NOT EXISTS RLHDetailJasa AS
			SELECT ttsdhd.PosKode, ttsdhd.SVNo,ttsdhd.SVTgl,PKBNo,KodeTrans,ASS,ttsdhd.MotorNoPolisi, '--' AS CusNama,ttsdhd.KarKode,
			SUM(IF(LEFT(JasaGroup,3)='ASS',ttsditjasa.SDHrgJual,000000000000000000.00)) AS JasaASS,
			SUM(IF(JasaGroup='CLAIMC2',ttsditjasa.SDHrgJual,000000000000000000.00)) AS JasaC2,
			SUM(IF(LEFT(JasaGroup,3)<>'ASS' AND JasaGroup<>'CLAIMC2',ttsditjasa.SDHrgJual,0)) AS JasaReguler,
			IFNULL(SUM(ttsditjasa.SDHrgJual),000000000000000000.00) AS JasaTotal,
			IFNULL(SUM(ttsditjasa.SDJualDisc),000000000000000000.00) AS JasaDisc,
			IFNULL(SUM(ttsditjasa.SDHrgJual),000000000000000000.00)-IFNULL(SUM(ttsditjasa.SDJualDisc),000000000000000000.00) AS JasaBersih
			FROM ttsdhd
			LEFT OUTER JOIN ttsditjasa ON ttsdhd.SDNo = ttsditjasa.SDNo
			LEFT OUTER JOIN tdjasa ON tdjasa.JasaKode = ttsditjasa.JasaKode
			WHERE DATE(ttsdhd.SVTgl) BETWEEN xTgl1 AND xTgl2
			GROUP BY ttsdhd.SDNo ORDER BY SDTgl;
			CREATE INDEX SVNo ON RLHDetailJasa(SVNo);

			UPDATE RLHDetail INNER JOIN RLHDetailJasa ON RLHDetail.SVNo = RLHDetailJasa.SVNo SET
			RLHDetail.JasaASS = RLHDetailJasa.JasaASS,
			RLHDetail.JasaC2 = RLHDetailJasa.JasaC2,
			RLHDetail.JasaReguler = RLHDetailJasa.JasaReguler,
			RLHDetail.JasaTotal = RLHDetailJasa.JasaTotal,
			RLHDetail.JasaDisc = RLHDetailJasa.JasaDisc,
			RLHDetail.JasaBersih = RLHDetailJasa.JasaBersih,
			RLHDetail.LabaTotal =  RLHDetail.PartOliLaba + RLHDetailJasa.JasaBersih - RLHDetail.DiscFinal;
		END IF;

		IF xJenis = "%" OR  xJenis = "SI" THEN 	
			INSERT INTO RLHDetail
			SELECT ttSIhd.PosKode, ttSIhd.SINo AS SVNo, ttSIhd.SITgl AS SVTgl, ttSIhd.SONo AS PKBNo, KodeTrans, SIJenis AS ASS, ttsihd.MotorNoPolisi, '--' AS CusNama,ttsihd.KarKode,
			000000000000000000.00 AS JasaASS,
			000000000000000000.00 AS JasaC2,
			000000000000000000.00 AS JasaReguler,
			000000000000000000.00 AS JasaTotal,
			000000000000000000.00 AS JasaDisc,
			000000000000000000.00 AS JasaBersih,
			SUM(IF(BrgGroup <>'Oli',ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00)) AS PartJual,
			SUM(IF(BrgGroup ='Oli',ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00)) AS OliJual,
			000000000000000000.00 AS OliJualKPB,
			SUM(IF(BrgGroup <>'Oli',ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) AS PartBeli,
			SUM(IF(BrgGroup ='Oli',ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) AS OliBeli,
			000000000000000000.00 AS OliBeliKPB,
			SUM(IFNULL(ttsiit.SIDiscount,000000000000000000.00)) AS DiscPart,
			SUM(IF(BrgGroup <>'Oli',ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00))- SUM(IF(BrgGroup <>'Oli',ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) AS PartLaba,
			SUM(IF(BrgGroup ='Oli',ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00)) - SUM(IF(BrgGroup ='Oli',ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) AS OliLaba,
			SUM(IFNULL(ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00))- SUM(IFNULL(ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) - SUM(IFNULL(ttsiit.SIDiscount,000000000000000000.00)) AS PartOliLaba,
			ttSIhd.SIDiscFinal AS DiscFinal,
			SUM(IFNULL(ttsiit.SIQty*ttsiit.SIHrgJual,000000000000000000.00))- SUM(IFNULL(ttsiit.SIQty*ttsiit.SIHrgBeli,000000000000000000.00)) - SUM(IFNULL(ttsiit.SIDiscount,000000000000000000.00))-ttSIhd.SIDiscFinal AS LabaTotal
			FROM ttSIhd
			LEFT OUTER JOIN ttsiit ON ttSIhd.SINo = ttsiit.SINo
			LEFT OUTER JOIN tdbarang ON tdbarang.BrgKode = ttsiit.BrgKode
			WHERE DATE(ttSIhd.SITgl) BETWEEN xTgl1 AND xTgl2
			GROUP BY ttSIhd.SINO ORDER BY SITgl;
		END IF;
		IF xJenis = "%" OR  xJenis = "SR" THEN 	
			INSERT INTO RLHDetail
			SELECT ttsrhd.PosKode, ttsrhd.SRNo AS SVNo, ttsrhd.srTgl AS SVTgl,'--' AS PKBNo, KodeTrans,'SR' AS ASS, ttsrhd.MotorNoPolisi, '--' AS CusNama,ttsrhd.KarKode,
			000000000000000000.00 AS JasaASS,
			000000000000000000.00 AS JasaC2,
			000000000000000000.00 AS JasaReguler,
			000000000000000000.00 AS JasaTotal,
			000000000000000000.00 AS JasaDisc,
			000000000000000000.00 AS JasaBersih,
			-SUM(IF(BrgGroup <>'Oli',ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00)) AS PartJual,
			-SUM(IF(BrgGroup ='Oli',ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00)) AS OliJual,
			000000000000000000.00 AS OliJualKPB,
			-SUM(IF(BrgGroup <>'Oli',ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) AS PartBeli,
			-SUM(IF(BrgGroup ='Oli',ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) AS OliBeli,
			000000000000000000.00 AS OliBeliKPB,
			-SUM(IFNULL(ttsrit.srDiscount,000000000000000000.00)) AS DiscPart,
			-SUM(IF(BrgGroup <>'Oli',ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00))- SUM(IF(BrgGroup <>'Oli',ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) AS PartLaba,
			-SUM(IF(BrgGroup ='Oli',ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00)) - SUM(IF(BrgGroup ='Oli',ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) AS OliLaba,
			-(SUM(IFNULL(ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00))- SUM(IFNULL(ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) - SUM(IFNULL(ttsrit.srDiscount,000000000000000000.00))) AS PartOliLaba,
			-ttsrhd.srDiscFinal AS DiscFinal,
			-(SUM(IFNULL(ttsrit.SRQty*ttsrit.SRHrgJual,000000000000000000.00))- SUM(IFNULL(ttsrit.SRQty*ttsrit.SRHrgBeli,000000000000000000.00)) - SUM(IFNULL(ttsrit.srDiscount,000000000000000000.00))-ttsrhd.srDiscFinal) AS LabaTotal
			FROM ttsrhd
			LEFT OUTER JOIN ttsrit ON ttsrhd.srNo = ttsrit.srNo
			LEFT OUTER JOIN tdbarang ON tdbarang.BrgKode = ttsrit.BrgKode
			WHERE DATE(ttSRhd.SRTgl) BETWEEN xTgl1 AND xTgl2
			GROUP BY ttsrhd.SRNO  ORDER BY SRTgl;
		END IF;
		SET @MyQuery = CONCAT("SELECT * FROM RLHDetail;");
	END IF;
	
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$
DELIMITER ;

