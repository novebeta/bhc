DELIMITER $$
DROP PROCEDURE IF EXISTS `rServisEstimasi`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rServisEstimasi`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,
IN xStatus VARCHAR(250), IN xSort VARCHAR(150),IN xOrder VARCHAR(10))
BEGIN
/*
	CALL rServisEstimasi('Header1','2020-01-01','2020-05-31','','SETgl','ASC');
*/

	IF xTipe = "Header1" THEN
		SET @MyQuery = CONCAT("
		SELECT ttsehd.SENo, ttsehd.SETgl, ttsehd.SDNo, ttsehd.MotorNoPolisi, ttsehd.KodeTrans, ttsehd.SETotalWaktu, ttsehd.SETotalJasa, ttsehd.SETotalPart, ttsehd.SETotalBiaya, ttsehd.Cetak, ttsehd.UserID, 
		IFNULL(tdcustomer.CusNama,'--') AS CusNama, IFNULL(tdcustomer.MotorType, '--') AS MotorType, IFNULL(tdcustomer.MotorWarna, '--') AS MotorWarna, IFNULL(tdmotortype.MotorNama, '--') AS MotorNama, fWaktu(ttsehd.SETotalWaktu) AS SDWaktu, 
		tdcustomer.CusTelepon, tdcustomer.MotorNoMesin, tdcustomer.MotorNoRangka, ttsehd.SEKeterangan, ttsehd.KarKode, tdkaryawan.KarNama, ttsehd.SETerbayar
		FROM ttsehd INNER JOIN
      tdkaryawan ON ttsehd.KarKode = tdkaryawan.KarKode LEFT OUTER JOIN
      tdcustomer ON ttsehd.MotorNoPolisi = tdcustomer.MotorNoPolisi LEFT OUTER JOIN
      tdmotortype ON tdcustomer.MotorType = tdmotortype.MotorType                       
      WHERE DATE(ttsehd.SETgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery,xStatus);
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY " ,xSort," ",xOrder );
	END IF;
	IF xTipe = "Item1" THEN
		SET @MyQuery = CONCAT("
		SELECT ttsehd.SENo, ttsehd.SETgl, ttseitjasa.SEAuto, ttseitjasa.JasaKode AS BrgKode, ttseitjasa.SEWaktuKerja AS SEQty, ttseitjasa.SEWaktuSatuan AS BrgSatuan, ttseitjasa.SEHrgJual, 
      ttseitjasa.SEJualDisc, tdjasa.JasaNama AS BrgNama, ttseitjasa.SEJualDisc / ttseitjasa.SEHrgJual * 100 AS Disc, ttseitjasa.SEHrgJual - ttseitjasa.SEJualDisc AS Jumlah, 'Jasa' AS Jenis,  
      ttsehd.KodeTrans, tdjasa.JasaGroup AS BrgGroup
		FROM ttseitjasa INNER JOIN
      tdjasa ON ttseitjasa.JasaKode = tdjasa.JasaKode INNER JOIN
      ttsehd ON ttseitjasa.SENo = ttsehd.SENo
      WHERE DATE(ttsehd.SETgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery,xStatus);
      SET @MyQuery = CONCAT(@MyQuery," UNION 
		SELECT ttSEhd_1.SENo, ttSEhd_1.SETgl,  ttseitbarang.SEAuto, ttseitbarang.BrgKode, ttseitbarang.SEQty, tdbarang.BrgSatuan, ttseitbarang.SEHrgJual, ttseitbarang.SEJualDisc, tdbarang.BrgNama, 
      ttseitbarang.SEJualDisc / ttseitbarang.SEHrgJual * 100 AS Disc, ttseitbarang.SEQty * ttseitbarang.SEHrgJual - ttseitbarang.SEJualDisc AS Jumlah, 'Barang' AS Jenis, ttSEhd_1.KodeTrans, 
      tdbarang.BrgGroup
		FROM ttseitbarang INNER JOIN
      tdbarang ON ttseitbarang.BrgKode = tdbarang.BrgKode INNER JOIN
      ttsehd ttSEhd_1 ON ttseitbarang.SENo = ttSEhd_1.SENo
      WHERE DATE(ttSEhd_1.SETgl) BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
      SET @MyQuery = CONCAT(@MyQuery,xStatus);
	END IF;
	
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$
DELIMITER ;

