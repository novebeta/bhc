DELIMITER $$
DROP PROCEDURE IF EXISTS `rStockKartu`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rStockKartu`(IN xTipe VARCHAR(50),IN xTgl1 DATETIME,IN xTgl2 DATETIME,
IN xLokasi VARCHAR(50), IN xBrgGroup VARCHAR(50), IN xBrgKode VARCHAR(50),IN xSort VARCHAR(50),IN xOrder VARCHAR(50))
BEGIN
/* 
Call rStockKartu('Kartu Stock','2020-04-01','2020-04-20','Gudang1','%','0001ZKFV860','Tanggal','ASC'); 
CALL rStockKartu('Pergerakan Stock 1','2020-04-01','2020-04-20','Gudang1','Oli','','Tanggal','ASC');
*/

   DECLARE myNomor VARCHAR(100);
   DECLARE myTanggal DATETIME;
   DECLARE myLokasi VARCHAR(20);
   DECLARE myBrgKode VARCHAR(20);
   DECLARE myKodeTrans VARCHAR(20);
   DECLARE myMasuk DOUBLE;
   DECLARE myKeluar DOUBLE;
   DECLARE myBrgNama VARCHAR(150);
   DECLARE myBrgRak1 VARCHAR(20);
   DECLARE myJenis VARCHAR(20);
   DECLARE myAwal DOUBLE;
   DECLARE mySaldo DOUBLE;
   DECLARE myBrgNamaKu VARCHAR(150);

	DECLARE HitungSaldo DOUBLE;
	DECLARE MyStockAwal DOUBLE;     

   DECLARE oBrgKode VARCHAR(20);
   DECLARE oLokasiKode VARCHAR(20);
   DECLARE oBrgBarCode VARCHAR(20);
   DECLARE oBrgNama VARCHAR(150);
   DECLARE oBrgGroup VARCHAR(20);
   DECLARE oBrgSatuan VARCHAR(20);
   DECLARE oBrgHrgBeli DOUBLE;
   DECLARE oBrgHrgJual DOUBLE;
   DECLARE oBrgRak1 VARCHAR(20);
   DECLARE oBrgMinStock SMALLINT;
   DECLARE oBrgMaxStock SMALLINT;
   DECLARE oBrgStatus VARCHAR(20);
   DECLARE oSD, oSI, oSR, oPS, oPR, oTIPlus, oTIMin, oTA, oSaldoAwal, oSaldoAkhir SMALLINT;
   DECLARE done0 INT DEFAULT 0;
	DECLARE cur0 CURSOR FOR SELECT BrgKode,LokasiKode, BrgBarCode, BrgNama, BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual,	BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus,SD, SI, SR, PS, PR, TIPlus, TIMin, TA, SaldoAwal, SaldoAkhir FROM StockBarangLokasi ORDER BY BrgKode ;
   DECLARE cur1 CURSOR FOR SELECT Nomor, Tanggal, LokasiKode, BrgKode, KodeTrans, Masuk, Keluar, BrgRak1, Jenis, Awal, Saldo, BrgNama FROM TKartu0 ORDER BY  BrgKode, Tanggal ;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done0 = 1;
   								
	IF xTipe = "Kartu Stock" THEN
		CALL rStockHitungLokasi(xBrgKode,'',xBrgGroup,xLokasi,ADDDATE(xTgl1, INTERVAL -1 DAY),ADDDATE(xTgl1, INTERVAL -1 DAY));

		DROP TEMPORARY TABLE IF EXISTS TKartu0;
  		CREATE TEMPORARY TABLE IF NOT EXISTS TKartu0 AS
      SELECT  '00000000000A' AS Nomor, ADDDATE(xTgl1, INTERVAL -1 DAY) AS Tanggal, LokasiKode AS LokasiKode,BrgKode AS BrgKode, '---------' AS KodeTrans,
      000000000 AS Masuk, 000000000 AS Keluar, BrgNama AS BrgNama, BrgRak1 AS BrgRak1, 'Awal                   ' AS Jenis, SaldoAkhir AS Awal, SaldoAkhir AS Saldo
      FROM StockBarang;
  		CREATE INDEX Tanggal ON TKartu0(Tanggal);
  		CREATE INDEX Nomor ON TKartu0(Nomor);

      INSERT INTO TKartu0
      SELECT ttsdhd.SDNo AS Nomor, ttsdhd.SDTgl AS Tanggal , ttsditbarang.LokasiKode, ttsditbarang.BrgKode, KodeTrans,
      0 AS Masuk, ttsditbarang.SDQty AS Keluar, BrgNama, BrgRak1, 'Penjualan Service' AS Jenis,0 AS Awal, 0 AS Saldo FROM ttsdhd
      INNER JOIN ttsditbarang ON ttsdhd.SDNo = ttsditbarang.SDNo
      JOIN tdbarang ON tdbarang.BrgKode = ttsditbarang.BrgKode
      WHERE (DATE(SDTgl) BETWEEN xTgl1 AND xTgl2) AND (ttsditbarang.BrgKode LIKE CONCAT("%",xBrgKode)) AND (ttsditbarang.LokasiKode LIKE CONCAT("%",xLokasi))
      UNION
      SELECT ttSIhd.SINo AS Nomor, ttSIhd.SITgl AS Tanggal, ttSIit.LokasiKode, ttSIit.BrgKode, KodeTrans,
      0 AS Masuk, ttSIit.SIQty AS Keluar, BrgNama, BrgRak1, 'Penjualan Item' AS Jenis,0 AS Awal, 0 AS Saldo  FROM ttSIhd
      INNER JOIN ttSIit ON ttSIhd.SINo = ttSIit.SINo
      INNER JOIN tdbarang ON tdbarang.BrgKode = ttSIit.BrgKode
      WHERE (DATE(SITgl) BETWEEN xTgl1 AND xTgl2) AND (ttSIit.BrgKode LIKE CONCAT("%",xBrgKode)) AND (ttSIit.LokasiKode LIKE CONCAT("%",xLokasi))
      UNION
      SELECT ttPRhd.PRNo AS Nomor, ttPRhd.PRTgl AS Tanggal, ttPRit.LokasiKode, ttPRit.BrgKode, KodeTrans,
      0 AS Masuk, ttPRit.PRQty AS Keluar, BrgNama, BrgRak1,'Retur Pembelian' AS Jenis,0 AS Awal, 0 AS Saldo  FROM ttPRhd
      INNER JOIN ttPRit ON ttPRhd.PRNo = ttPRit.PRNo
      INNER JOIN tdbarang ON tdbarang.BrgKode = ttPRit.BrgKode
      WHERE (DATE(PRTgl) BETWEEN xTgl1 AND xTgl2) AND (ttPRit.BrgKode LIKE CONCAT("%",xBrgKode)) AND (ttPRit.LokasiKode LIKE CONCAT("%",xLokasi))
      UNION
      SELECT ttPShd.PSNo AS Nomor, ttPShd.PSTgl AS Tanggal, ttPSit.LokasiKode, ttPSit.BrgKode, KodeTrans,
      ttPSit.PSQty AS Masuk, 0 AS Keluar, BrgNama, BrgRak1, 'Pembelian Item' AS Jenis, 0 AS Awal, 0 AS Saldo FROM ttPShd
      INNER JOIN ttPSit ON ttPShd.PSNo = ttPSit.PSNo
      INNER JOIN tdbarang ON tdbarang.BrgKode = ttPSit.BrgKode
      WHERE (DATE(PSTgl) BETWEEN xTgl1 AND xTgl2) AND (ttPSit.BrgKode LIKE CONCAT("%",xBrgKode)) AND (ttPSit.LokasiKode LIKE CONCAT("%",xLokasi))
      UNION
      SELECT ttSRhd.SRNo AS Nomor, ttSRhd.SRTgl AS Tanggal, ttSRit.LokasiKode, ttSRit.BrgKode, KodeTrans,
      ttSRit.SRQty AS Masuk, 0 AS Keluar, BrgNama, BrgRak1, 'Retur Penjualan' AS Jenis, 0 AS Awal, 0 AS Saldo FROM ttSRhd
      INNER JOIN ttSRit ON ttSRhd.SRNo = ttSRit.SRNo
      INNER JOIN tdbarang ON tdbarang.BrgKode = ttSRit.BrgKode
      WHERE (DATE(SRTgl) BETWEEN xTgl1 AND xTgl2) AND (ttSRit.BrgKode LIKE CONCAT("%",xBrgKode)) AND (ttSRit.LokasiKode LIKE CONCAT("%",xLokasi))
      UNION
      SELECT ttTIhd.TINo AS Nomor, ttTIhd.TITgl AS Tanggal, ttTIit.LokasiAsal AS LokasiKode, ttTIit.BrgKode, KodeTrans,
      0 AS Masuk, ttTIit.TIQty AS Keluar, BrgNama, BrgRak1, 'Transfer Keluar' AS Jenis, 0 AS Awal, 0 AS Saldo FROM ttTIhd
      INNER JOIN ttTIit ON ttTIhd.TINo = ttTIit.TINo
      INNER JOIN tdbarang ON tdbarang.BrgKode = ttTIit.BrgKode
      WHERE (DATE(TITgl) BETWEEN xTgl1 AND xTgl2) AND (ttTIit.BrgKode LIKE CONCAT("%",xBrgKode)) AND (ttTIit.LokasiAsal LIKE CONCAT("%",xLokasi))
      UNION
      SELECT ttTIhd.TINo AS Nomor, ttTIhd.TITgl AS Tanggal, ttTIit.LokasiTujuan AS LokasiKode, ttTIit.BrgKode, KodeTrans,
      ttTIit.TIQty AS Masuk, 0 AS Keluar, BrgNama, BrgRak1, 'Transfer Masuk' AS Jenis, 0 AS Awal, 0 AS Saldo FROM ttTIhd
      INNER JOIN ttTIit ON ttTIhd.TINo = ttTIit.TINo
      INNER JOIN tdbarang ON tdbarang.BrgKode = ttTIit.BrgKode
      WHERE (DATE(TITgl) BETWEEN xTgl1 AND xTgl2) AND (ttTIit.BrgKode LIKE CONCAT("%",xBrgKode)) AND (ttTIit.LokasiTujuan LIKE CONCAT("%",xLokasi))
      UNION
      SELECT ttTAhd.TANo AS Nomor, ttTAhd.TATgl AS Tanggal, ttTAit.LokasiKode, ttTAit.BrgKode, KodeTrans,
      ttTAit.TAQty AS Masuk, 0 AS Keluar, BrgNama, BrgRak1, 'Penyesuaian' AS Jenis, 0 AS Awal, 0 AS Saldo FROM ttTAhd
      INNER JOIN ttTAit ON ttTAhd.TANo = ttTAit.TANo
      INNER JOIN tdbarang ON tdbarang.BrgKode = ttTAit.BrgKode
      WHERE (DATE(TATgl) BETWEEN xTgl1 AND xTgl2) AND (ttTAit.BrgKode LIKE CONCAT("%",xBrgKode)) AND (ttTAit.LokasiKode LIKE CONCAT("%",xLokasi));

  		DROP TEMPORARY TABLE IF EXISTS TKartu;
  		CREATE TEMPORARY TABLE IF NOT EXISTS TKartu LIKE TKartu0;
     
      SELECT Saldo INTO HitungSaldo FROM TKartu0 LIMIT 1;
		SET MyStockAwal = HitungSaldo;
		OPEN cur1;
		REPEAT FETCH cur1 INTO myNomor, myTanggal, myLokasi, myBrgKode, myKodeTrans, myMasuk, myKeluar, myBrgRak1, myJenis, myAwal, mySaldo, MyBrgNama;
			IF ! done0 THEN
				SET myAwal = MyStockAwal;
				SET HitungSaldo = HitungSaldo + myMasuk - MyKeluar;
				SET mySaldo = HitungSaldo;			
				INSERT INTO TKartu (Nomor, Tanggal, LokasiKode, BrgKode, KodeTrans, Masuk, Keluar, BrgNama, BrgRak1, Jenis, Awal, Saldo) VALUES 
				(myNomor,myTanggal,myLokasi,myBrgKode,myKodeTrans,myMasuk,myKeluar,myBrgNama,myBrgRak1,myJenis,myAwal,mySaldo);
				SET MyStockAwal = HitungSaldo;
			END IF;
		UNTIL done0 END REPEAT;
		CLOSE cur1;

		SET @MyQuery = CONCAT("SELECT * FROM TKartu ORDER BY  ",xSort, " ", xOrder);
		/*SET @MyQuery = CONCAT("SELECT * FROM TKartu ORDER BY BrgKode, LokasiKode, Tanggal;");*/
	END IF;

	IF xTipe = "Pergerakan Stock 1" OR xTipe = "Pergerakan Stock 2" THEN
	
		DROP TEMPORARY TABLE IF EXISTS TKartu0;
		CREATE TEMPORARY TABLE IF NOT EXISTS TKartu0 AS
      SELECT  '00000000000A' AS Nomor, NOW() AS Tanggal, SPACE(20) AS LokasiKode, SPACE(20) AS BrgKode, SPACE(20) AS KodeTrans,
      000000000 AS Masuk, 000000000 AS Keluar, SPACE(150) AS BrgNama, SPACE(20) AS BrgRak1, 'Awal                   ' AS Jenis, 0000000000 AS Awal, 0000000000 AS Saldo;
      CREATE INDEX Tanggal ON TKartu0(Tanggal);
  		CREATE INDEX Nomor ON TKartu0(Nomor);
		DELETE FROM TKartu0;
		DROP TEMPORARY TABLE IF EXISTS TKartu;
		CREATE TEMPORARY TABLE IF NOT EXISTS TKartu LIKE TKartu0;	
	
		CALL rStockHitungLokasi(xBrgKode,'',xBrgGroup,xLokasi,ADDDATE(xTgl1, INTERVAL -1 DAY),ADDDATE(xTgl1, INTERVAL -1 DAY));
		DROP TEMPORARY TABLE IF EXISTS StockBarangLokasi;
		CREATE TEMPORARY TABLE IF NOT EXISTS StockBarangLokasi AS
		SELECT BrgKode,LokasiKode, BrgBarCode, BrgNama, BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual,
		BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus,SD, SI, SR, PS, PR, TIPlus, TIMin, TA, SaldoAwal, SaldoAkhir FROM StockBarang
		ORDER BY BrgKode ;
      CREATE INDEX BrgKode ON StockBarangLokasi(BrgKode);
  		CREATE INDEX LokasiKode ON StockBarangLokasi(LokasiKode);

		CALL rStockHitungLokasi(xBrgKode,'',xBrgGroup,xLokasi,xTgl1,xTgl2);
		DROP TEMPORARY TABLE IF EXISTS StockBarangDelete;
		CREATE TEMPORARY TABLE IF NOT EXISTS StockBarangDelete AS
		SELECT BrgKode,LokasiKode
		FROM StockBarang
		WHERE SD = 0 AND SI = 0 AND SR = 0 AND PS = 0 AND PR = 0 AND TA = 0 AND SaldoAwal = 0 AND SaldoAkhir = 0 AND TIMin = 0 AND TIPlus = 0
		ORDER BY BrgKode ;
      CREATE INDEX BrgKode ON StockBarangDelete(BrgKode);
  		CREATE INDEX LokasiKode ON StockBarangDelete(LokasiKode);
		
		DELETE StockBarangLokasi FROM StockBarangLokasi INNER JOIN StockBarangDelete ON StockBarangLokasi.BrgKode = StockBarangDelete.BrgKode AND StockBarangLokasi.LokasiKode = StockBarangDelete.LokasiKode;

		DROP TEMPORARY TABLE IF EXISTS KartuSemua;
		CREATE TEMPORARY TABLE IF NOT EXISTS KartuSemua AS
		SELECT  '00000000000A' AS Nomor, ADDDATE(xTgl1, INTERVAL -1 DAY) AS Tanggal, LokasiKode AS LokasiKode,BrgKode AS BrgKode, '---------' AS KodeTrans,
		000000000 AS Masuk, 000000000 AS Keluar, BrgNama AS BrgNama, BrgRak1 AS BrgRak1, 'Awal                   ' AS Jenis, SaldoAkhir AS Awal, SaldoAkhir AS Saldo
		FROM StockBarangLokasi 
		UNION
		SELECT ttsdhd.SDNo AS Nomor, ttsdhd.SDTgl AS Tanggal , ttsditbarang.LokasiKode, ttsditbarang.BrgKode, KodeTrans,
		0 AS Masuk, ttsditbarang.SDQty AS Keluar, BrgNama, BrgRak1, 'Penjualan Service' AS Jenis,0 AS Awal, 0 AS Saldo FROM ttsdhd
		INNER JOIN ttsditbarang ON ttsdhd.SDNo = ttsditbarang.SDNo
		JOIN tdbarang ON tdbarang.BrgKode = ttsditbarang.BrgKode
		WHERE (DATE(SDTgl) BETWEEN xTgl1 AND xTgl2) 
		UNION
		SELECT ttSIhd.SINo AS Nomor, ttSIhd.SITgl AS Tanggal, ttSIit.LokasiKode, ttSIit.BrgKode, KodeTrans,
		0 AS Masuk, ttSIit.SIQty AS Keluar, BrgNama, BrgRak1, 'Penjualan Item' AS Jenis,0 AS Awal, 0 AS Saldo  FROM ttSIhd
		INNER JOIN ttSIit ON ttSIhd.SINo = ttSIit.SINo
		INNER JOIN tdbarang ON tdbarang.BrgKode = ttSIit.BrgKode
		WHERE (DATE(SITgl) BETWEEN xTgl1 AND xTgl2) 
		UNION
		SELECT ttPRhd.PRNo AS Nomor, ttPRhd.PRTgl AS Tanggal, ttPRit.LokasiKode, ttPRit.BrgKode, KodeTrans,
		0 AS Masuk, ttPRit.PRQty AS Keluar, BrgNama, BrgRak1,'Retur Pembelian' AS Jenis,0 AS Awal, 0 AS Saldo  FROM ttPRhd
		INNER JOIN ttPRit ON ttPRhd.PRNo = ttPRit.PRNo
		INNER JOIN tdbarang ON tdbarang.BrgKode = ttPRit.BrgKode
		WHERE (DATE(PRTgl) BETWEEN xTgl1 AND xTgl2) 
		UNION
		SELECT ttPShd.PSNo AS Nomor, ttPShd.PSTgl AS Tanggal, ttPSit.LokasiKode, ttPSit.BrgKode, KodeTrans,
		ttPSit.PSQty AS Masuk, 0 AS Keluar, BrgNama, BrgRak1, 'Pembelian Item' AS Jenis, 0 AS Awal, 0 AS Saldo FROM ttPShd
		INNER JOIN ttPSit ON ttPShd.PSNo = ttPSit.PSNo
		INNER JOIN tdbarang ON tdbarang.BrgKode = ttPSit.BrgKode
		WHERE (DATE(PSTgl) BETWEEN xTgl1 AND xTgl2) 
		UNION
		SELECT ttSRhd.SRNo AS Nomor, ttSRhd.SRTgl AS Tanggal, ttSRit.LokasiKode, ttSRit.BrgKode, KodeTrans,
		ttSRit.SRQty AS Masuk, 0 AS Keluar, BrgNama, BrgRak1, 'Retur Penjualan' AS Jenis, 0 AS Awal, 0 AS Saldo FROM ttSRhd
		INNER JOIN ttSRit ON ttSRhd.SRNo = ttSRit.SRNo
		INNER JOIN tdbarang ON tdbarang.BrgKode = ttSRit.BrgKode
		WHERE (DATE(SRTgl) BETWEEN xTgl1 AND xTgl2) 
		UNION
		SELECT ttTIhd.TINo AS Nomor, ttTIhd.TITgl AS Tanggal, ttTIit.LokasiAsal AS LokasiKode, ttTIit.BrgKode, KodeTrans,
		0 AS Masuk, ttTIit.TIQty AS Keluar, BrgNama, BrgRak1, 'Transfer Keluar' AS Jenis, 0 AS Awal, 0 AS Saldo FROM ttTIhd
		INNER JOIN ttTIit ON ttTIhd.TINo = ttTIit.TINo
		INNER JOIN tdbarang ON tdbarang.BrgKode = ttTIit.BrgKode
		WHERE (DATE(TITgl) BETWEEN xTgl1 AND xTgl2) 
		UNION
		SELECT ttTIhd.TINo AS Nomor, ttTIhd.TITgl AS Tanggal, ttTIit.LokasiTujuan AS LokasiKode, ttTIit.BrgKode, KodeTrans,
		ttTIit.TIQty AS Masuk, 0 AS Keluar, BrgNama, BrgRak1, 'Transfer Masuk' AS Jenis, 0 AS Awal, 0 AS Saldo FROM ttTIhd
		INNER JOIN ttTIit ON ttTIhd.TINo = ttTIit.TINo
		INNER JOIN tdbarang ON tdbarang.BrgKode = ttTIit.BrgKode
		WHERE (DATE(TITgl) BETWEEN xTgl1 AND xTgl2) 
		UNION
		SELECT ttTAhd.TANo AS Nomor, ttTAhd.TATgl AS Tanggal, ttTAit.LokasiKode, ttTAit.BrgKode, KodeTrans,
		ttTAit.TAQty AS Masuk, 0 AS Keluar, BrgNama, BrgRak1, 'Penyesuaian' AS Jenis, 0 AS Awal, 0 AS Saldo FROM ttTAhd
		INNER JOIN ttTAit ON ttTAhd.TANo = ttTAit.TANo
		INNER JOIN tdbarang ON tdbarang.BrgKode = ttTAit.BrgKode
		WHERE (DATE(TATgl) BETWEEN xTgl1 AND xTgl2); 
      CREATE INDEX BrgKode ON KartuSemua(BrgKode);
  		CREATE INDEX LokasiKode ON KartuSemua(LokasiKode);
  		CREATE INDEX Tanggal ON KartuSemua(Tanggal);
   
		OPEN cur0;
		REPEAT FETCH cur0 INTO oBrgKode, oLokasiKode, oBrgBarCode, oBrgNama, oBrgGroup, oBrgSatuan, oBrgHrgBeli, oBrgHrgJual,
                                   oBrgRak1, oBrgMinStock, oBrgMaxStock, oBrgStatus, oSD, oSI, oSR, oPS, oPR, oTIPlus, oTIMin, oTA, oSaldoAwal, oSaldoAkhir;
			IF ! done0 THEN		
				INSERT INTO TKartu0	SELECT * FROM KartuSemua WHERE (BrgKode LIKE CONCAT("%",oBrgKode)) AND (LokasiKode LIKE CONCAT("%",oLokasiKode));
				
   			SET HitungSaldo = 0;
   			SET MyStockAwal = 0;
				OPEN cur1;
				block2: BEGIN
				DECLARE done1 INT DEFAULT 0;
   			DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = 1;
   			SELECT Saldo INTO HitungSaldo FROM TKartu0 LIMIT 1;
				SET MyStockAwal = HitungSaldo;
				REPEAT FETCH cur1 INTO myNomor, myTanggal, myLokasi, myBrgKode, myKodeTrans, myMasuk, myKeluar, myBrgRak1, myJenis, myAwal, mySaldo, MyBrgNama;
					IF ! done1 THEN
						SET myAwal = MyStockAwal;
						SET HitungSaldo = HitungSaldo + myMasuk - MyKeluar;
						SET mySaldo = HitungSaldo;
						INSERT INTO TKartu (Nomor, Tanggal, LokasiKode, BrgKode, KodeTrans, Masuk, Keluar, BrgNama, BrgRak1, Jenis, Awal, Saldo) VALUES 
							(myNomor,myTanggal,myLokasi,myBrgKode,myKodeTrans,myMasuk,myKeluar,myBrgNama,myBrgRak1,myJenis,myAwal,mySaldo);
						SET MyStockAwal = HitungSaldo;
					END IF;
				UNTIL done1 END REPEAT;
				END block2;
				CLOSE cur1;			
				DELETE FROM TKartu0;
			END IF;		
		UNTIL done0 END REPEAT;
		CLOSE cur0;
		SET @MyQuery = CONCAT("SELECT * FROM TKartu ORDER BY BrgKode, LokasiKode, Tanggal;");
	END IF;
	
	IF xTipe = "Pergerakan Stock TC" THEN
	
		DROP TEMPORARY TABLE IF EXISTS TKartu0;
		CREATE TEMPORARY TABLE IF NOT EXISTS TKartu0 AS
      SELECT  '00000000000A' AS Nomor, NOW() AS Tanggal, SPACE(20) AS LokasiKode, SPACE(20) AS BrgKode, SPACE(20) AS KodeTrans,
      000000000 AS Masuk, 000000000 AS Keluar, SPACE(150) AS BrgNama, SPACE(20) AS BrgRak1, 'Awal                   ' AS Jenis, 0000000000 AS Awal, 0000000000 AS Saldo;
      CREATE INDEX Tanggal ON TKartu0(Tanggal);
  		CREATE INDEX Nomor ON TKartu0(Nomor);
		DELETE FROM TKartu0;
		DROP TEMPORARY TABLE IF EXISTS TKartu;
		CREATE TEMPORARY TABLE IF NOT EXISTS TKartu LIKE TKartu0;	

		DROP TEMPORARY TABLE IF EXISTS PSTC;
		CREATE TEMPORARY TABLE IF NOT EXISTS PSTC AS
		SELECT BrgKode FROM ttpsit INNER JOIN ttpshd ON ttpshd.PSno = ttpsit.PSNo WHERE (KodeTrans = 'TC02' OR KodeTrans = 'TC05') AND (DATE(PSTgl) BETWEEN xTgl1 AND xTgl2) GROUP BY BrgKode;
      CREATE INDEX BrgKode ON PSTC(BrgKode);
		
		/*CALL rStockHitungLokasi(xBrgKode,'',xBrgGroup,xLokasi,xTgl1,xTgl2);*/
		
		CALL rStockHitungLokasi(xBrgKode,'',xBrgGroup,xLokasi,ADDDATE(xTgl1, INTERVAL -1 DAY),ADDDATE(xTgl1, INTERVAL -1 DAY));
		DROP TEMPORARY TABLE IF EXISTS StockBarangLokasi;
		CREATE TEMPORARY TABLE IF NOT EXISTS StockBarangLokasi AS
		SELECT StockBarang.BrgKode,LokasiKode, BrgBarCode, BrgNama, BrgGroup, BrgSatuan, BrgHrgBeli, BrgHrgJual,
		BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus,SD, SI, SR, PS, PR, TIPlus, TIMin, TA, SaldoAwal, SaldoAkhir FROM StockBarang
		INNER JOIN 
		PSTC
		ON PSTC.BrgKode = StockBarang.BrgKode
		ORDER BY BrgKode ;
      CREATE INDEX BrgKode ON StockBarangLokasi(BrgKode);
  		CREATE INDEX LokasiKode ON StockBarangLokasi(LokasiKode);
  		
  		

		OPEN cur0;
		REPEAT FETCH cur0 INTO oBrgKode, oLokasiKode, oBrgBarCode, oBrgNama, oBrgGroup, oBrgSatuan, oBrgHrgBeli, oBrgHrgJual,
                                   oBrgRak1, oBrgMinStock, oBrgMaxStock, oBrgStatus, oSD, oSI, oSR, oPS, oPR, oTIPlus, oTIMin, oTA, oSaldoAwal, oSaldoAkhir;
			IF ! done0 THEN

				INSERT INTO TKartu0
				SELECT  '00000000000A' AS Nomor, ADDDATE(xTgl1, INTERVAL -1 DAY) AS Tanggal, LokasiKode AS LokasiKode,BrgKode AS BrgKode, '---------' AS KodeTrans,
				000000000 AS Masuk, 000000000 AS Keluar, BrgNama AS BrgNama, BrgRak1 AS BrgRak1, 'Awal                   ' AS Jenis, SaldoAkhir AS Awal, SaldoAkhir AS Saldo
				FROM StockBarangLokasi WHERE BrgKode = oBrgKode AND LokasiKode = oLokasiKode;

				INSERT INTO TKartu0
				SELECT ttsdhd.SDNo AS Nomor, ttsdhd.SDTgl AS Tanggal , ttsditbarang.LokasiKode, ttsditbarang.BrgKode, KodeTrans,
				0 AS Masuk, ttsditbarang.SDQty AS Keluar, BrgNama, BrgRak1, 'Penjualan Service' AS Jenis,0 AS Awal, 0 AS Saldo FROM ttsdhd
				INNER JOIN ttsditbarang ON ttsdhd.SDNo = ttsditbarang.SDNo
				JOIN tdbarang ON tdbarang.BrgKode = ttsditbarang.BrgKode
				WHERE (DATE(SDTgl) BETWEEN xTgl1 AND xTgl2) AND (ttsditbarang.BrgKode LIKE CONCAT("%",oBrgKode)) AND (ttsditbarang.LokasiKode LIKE CONCAT("%",oLokasiKode))
				UNION
				SELECT ttSIhd.SINo AS Nomor, ttSIhd.SITgl AS Tanggal, ttSIit.LokasiKode, ttSIit.BrgKode, KodeTrans,
				0 AS Masuk, ttSIit.SIQty AS Keluar, BrgNama, BrgRak1, 'Penjualan Item' AS Jenis,0 AS Awal, 0 AS Saldo  FROM ttSIhd
				INNER JOIN ttSIit ON ttSIhd.SINo = ttSIit.SINo
				INNER JOIN tdbarang ON tdbarang.BrgKode = ttSIit.BrgKode
				WHERE (DATE(SITgl) BETWEEN xTgl1 AND xTgl2) AND (ttSIit.BrgKode LIKE CONCAT("%",oBrgKode)) AND (ttSIit.LokasiKode LIKE CONCAT("%",oLokasiKode))
				UNION
				SELECT ttPRhd.PRNo AS Nomor, ttPRhd.PRTgl AS Tanggal, ttPRit.LokasiKode, ttPRit.BrgKode, KodeTrans,
				0 AS Masuk, ttPRit.PRQty AS Keluar, BrgNama, BrgRak1,'Retur Pembelian' AS Jenis,0 AS Awal, 0 AS Saldo  FROM ttPRhd
				INNER JOIN ttPRit ON ttPRhd.PRNo = ttPRit.PRNo
				INNER JOIN tdbarang ON tdbarang.BrgKode = ttPRit.BrgKode
				WHERE (DATE(PRTgl) BETWEEN xTgl1 AND xTgl2) AND (ttPRit.BrgKode LIKE CONCAT("%",oBrgKode)) AND (ttPRit.LokasiKode LIKE CONCAT("%",oLokasiKode))
				UNION
				SELECT ttPShd.PSNo AS Nomor, ttPShd.PSTgl AS Tanggal, ttPSit.LokasiKode, ttPSit.BrgKode, KodeTrans,
				ttPSit.PSQty AS Masuk, 0 AS Keluar, BrgNama, BrgRak1, 'Pembelian Item' AS Jenis, 0 AS Awal, 0 AS Saldo FROM ttPShd
				INNER JOIN ttPSit ON ttPShd.PSNo = ttPSit.PSNo
				INNER JOIN tdbarang ON tdbarang.BrgKode = ttPSit.BrgKode
				WHERE (DATE(PSTgl) BETWEEN xTgl1 AND xTgl2) AND (ttPSit.BrgKode LIKE CONCAT("%",oBrgKode)) AND (ttPSit.LokasiKode LIKE CONCAT("%",oLokasiKode))
				UNION
				SELECT ttSRhd.SRNo AS Nomor, ttSRhd.SRTgl AS Tanggal, ttSRit.LokasiKode, ttSRit.BrgKode, KodeTrans,
				ttSRit.SRQty AS Masuk, 0 AS Keluar, BrgNama, BrgRak1, 'Retur Penjualan' AS Jenis, 0 AS Awal, 0 AS Saldo FROM ttSRhd
				INNER JOIN ttSRit ON ttSRhd.SRNo = ttSRit.SRNo
				INNER JOIN tdbarang ON tdbarang.BrgKode = ttSRit.BrgKode
				WHERE (DATE(SRTgl) BETWEEN xTgl1 AND xTgl2) AND (ttSRit.BrgKode LIKE CONCAT("%",oBrgKode)) AND (ttSRit.LokasiKode LIKE CONCAT("%",oLokasiKode))
				UNION
				SELECT ttTIhd.TINo AS Nomor, ttTIhd.TITgl AS Tanggal, ttTIit.LokasiAsal AS LokasiKode, ttTIit.BrgKode, KodeTrans,
				0 AS Masuk, ttTIit.TIQty AS Keluar, BrgNama, BrgRak1, 'Transfer Keluar' AS Jenis, 0 AS Awal, 0 AS Saldo FROM ttTIhd
				INNER JOIN ttTIit ON ttTIhd.TINo = ttTIit.TINo
				INNER JOIN tdbarang ON tdbarang.BrgKode = ttTIit.BrgKode
				WHERE (DATE(TITgl) BETWEEN xTgl1 AND xTgl2) AND (ttTIit.BrgKode LIKE CONCAT("%",oBrgKode)) AND (ttTIit.LokasiAsal LIKE CONCAT("%",oLokasiKode))
				UNION
				SELECT ttTIhd.TINo AS Nomor, ttTIhd.TITgl AS Tanggal, ttTIit.LokasiTujuan AS LokasiKode, ttTIit.BrgKode, KodeTrans,
				ttTIit.TIQty AS Masuk, 0 AS Keluar, BrgNama, BrgRak1, 'Transfer Masuk' AS Jenis, 0 AS Awal, 0 AS Saldo FROM ttTIhd
				INNER JOIN ttTIit ON ttTIhd.TINo = ttTIit.TINo
				INNER JOIN tdbarang ON tdbarang.BrgKode = ttTIit.BrgKode
				WHERE (DATE(TITgl) BETWEEN xTgl1 AND xTgl2) AND (ttTIit.BrgKode LIKE CONCAT("%",oBrgKode)) AND (ttTIit.LokasiTujuan LIKE CONCAT("%",oLokasiKode))
				UNION
				SELECT ttTAhd.TANo AS Nomor, ttTAhd.TATgl AS Tanggal, ttTAit.LokasiKode, ttTAit.BrgKode, KodeTrans,
				ttTAit.TAQty AS Masuk, 0 AS Keluar, BrgNama, BrgRak1, 'Penyesuaian' AS Jenis, 0 AS Awal, 0 AS Saldo FROM ttTAhd
				INNER JOIN ttTAit ON ttTAhd.TANo = ttTAit.TANo
				INNER JOIN tdbarang ON tdbarang.BrgKode = ttTAit.BrgKode
				WHERE (DATE(TATgl) BETWEEN xTgl1 AND xTgl2) AND (ttTAit.BrgKode LIKE CONCAT("%",oBrgKode)) AND (ttTAit.LokasiKode LIKE CONCAT("%",oLokasiKode));

   			SET HitungSaldo = 0;
   			SET MyStockAwal = 0;
				OPEN cur1;
				block2: BEGIN
				DECLARE done1 INT DEFAULT 0;
   			DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = 1;
   			SELECT Saldo INTO HitungSaldo FROM TKartu0 LIMIT 1;
				SET MyStockAwal = HitungSaldo;
				REPEAT FETCH cur1 INTO myNomor, myTanggal, myLokasi, myBrgKode, myKodeTrans, myMasuk, myKeluar, myBrgRak1, myJenis, myAwal, mySaldo, MyBrgNama;
					IF ! done1 THEN
						SET myAwal = MyStockAwal;
						SET HitungSaldo = HitungSaldo + myMasuk - MyKeluar;
						SET mySaldo = HitungSaldo;
						INSERT INTO TKartu (Nomor, Tanggal, LokasiKode, BrgKode, KodeTrans, Masuk, Keluar, BrgNama, BrgRak1, Jenis, Awal, Saldo) VALUES 
							(myNomor,myTanggal,myLokasi,myBrgKode,myKodeTrans,myMasuk,myKeluar,myBrgNama,myBrgRak1,myJenis,myAwal,mySaldo);
						SET MyStockAwal = HitungSaldo;
					END IF;
				UNTIL done1 END REPEAT;
				END block2;
				CLOSE cur1;			
				DELETE FROM TKartu0;
			END IF;		
		UNTIL done0 END REPEAT;
		CLOSE cur0;
		SET @MyQuery = CONCAT("SELECT * FROM TKartu ORDER BY BrgKode,LokasiKode, Tanggal;");
	END IF;


	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;  
END$$
DELIMITER ;

