DELIMITER $$

DROP PROCEDURE IF EXISTS `fccit`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fccit`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xCCNo VARCHAR(10),
IN xSDNo VARCHAR(10),
IN xCCPart DECIMAL(14, 2),
IN xCCJasa DECIMAL(14, 2),
IN xCCNoOLD VARCHAR(10),
IN xSDNoOLD VARCHAR(10)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;
 			
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xCCNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xCCNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert
			IF oStatus = 0 THEN		
				INSERT INTO ttccit (CCNo, SDNo, CCPart, CCJasa) VALUES (xCCNo, xSDNo, xCCPart, xCCJasa);

            #Hitung Total
            SELECT SUM(CCPart) INTO @CCPart FROM ttccit WHERE CCNo = xCCNo; 
            IF  @CCPart IS NOT NULL THEN UPDATE ttcchd set CCTotalPart = @CCPart WHERE CCNo = xCCNo; END IF;
            SELECT SUM(CCJasa) INTO @CCJasa FROM ttccit WHERE CCNo = xCCNo; 
            IF  @CCJasa IS NOT NULL THEN UPDATE ttcchd set CCTotalJasa = @CCJasa WHERE CCNo = xCCNo; END IF;

				SET oKeterangan = CONCAT('Berhasil menambahkan data ', xCCNo,' - ', xSDNo, ' - ', FORMAT(xCCPart,2), ' - ', FORMAT(xCCJasa,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xCCNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xCCNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Update
			IF oStatus = 0 THEN
				UPDATE ttccit	SET CCNo = xCCNo, SDNo = xSDNo, CCPart = xCCPart, CCJasa = xCCJasa
				WHERE CCNoOLD = xCCNoOLD AND SDNoOLD = xSDNoOLD;
            
            #Hitung Total
            SELECT SUM(CCPart) INTO @CCPart FROM ttccit WHERE CCNo = xCCNo; 
            IF  @CCPart IS NOT NULL THEN UPDATE ttcchd set CCTotalPart = @CCPart WHERE CCNo = xCCNo; END IF;
            SELECT SUM(CCJasa) INTO @CCJasa FROM ttccit WHERE CCNo = xCCNo; 
            IF  @CCJasa IS NOT NULL THEN UPDATE ttcchd set CCTotalJasa = @CCJasa WHERE CCNo = xCCNo; END IF;

				SET oKeterangan = CONCAT("Berhasil mengubah data : ", xCCNo , ' - ' ,xSDNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;						
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xCCNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xCCNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Delete
			IF oStatus = 0 THEN
				DELETE FROM ttccit WHERE SDNo = xSDNo AND CCNo = xCCNo;

            #Hitung Total
            SELECT SUM(CCPart) INTO @CCPart FROM ttccit WHERE CCNo = xCCNo; 
            IF  @CCPart IS NOT NULL THEN UPDATE ttcchd set CCTotalPart = @CCPart WHERE CCNo = xCCNo; END IF;
            SELECT SUM(CCJasa) INTO @CCJasa FROM ttccit WHERE CCNo = xCCNo; 
            IF  @CCJasa IS NOT NULL THEN UPDATE ttcchd set CCTotalJasa = @CCJasa WHERE CCNo = xCCNo; END IF;

				SET oKeterangan = CONCAT("Berhasil menghapus data : ", xCCNo , ' - ' ,xSDNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;		
		END;
		WHEN "Cancel" THEN
		BEGIN
			SET oStatus = 0; SET oKeterangan = CONCAT("Batal menyimpan data : ", xSDNo );
		END;
	END CASE;
END$$

DELIMITER ;

