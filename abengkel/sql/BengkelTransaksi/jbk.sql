DELIMITER $$

DROP PROCEDURE IF EXISTS `jbk`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jbk`(IN xBKNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE oSumBKBayarIT DECIMAL(19,2) DEFAULT 0;
	DECLARE oSumBKBayarCOA DECIMAL(19,2) DEFAULT 0;
	DECLARE MyPesanKu VARCHAR(300) DEFAULT '';
	DECLARE MyPesankuItem VARCHAR(150) DEFAULT '';
	DECLARE oKategori VARCHAR(100) DEFAULT '';

	DECLARE MyAccountPOS  VARCHAR(10) DEFAULT '11010100';

	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xBKNo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;

	SELECT BKNo, BKTgl, BKJenis, KodeTrans, PosKode, SupKode, BankKode, BKCekTempo, BKAC, BKPerson, BKCekNo, BKNominal, BKMemo, NoGL, Cetak, UserID
	INTO @BKNo, @BKTgl, @BKJenis, @KodeTrans, @PosKode, @SupKode, @BankKode, @BKCekTempo, @BKAC, @BKPerson, @BKCekNo, @BKNominal, @BKMemo, @NoGL, @Cetak, @UserID
	FROM ttbkhd	WHERE BKNo = xBKNo;
	SELECT IFNULL(SUM(BKBayar),0) INTO oSumBKBayarIT FROM ttbkit WHERE BKNo = xBKNo;
	SELECT IFNULL(SUM(BKBayar),0) INTO oSumBKBayarCOA FROM ttbkitcoa WHERE BKNo = xBKNo;

   CASE @KodeTrans
		WHEN  'UM' THEN
		   SET oKategori = 'Umum';
		WHEN 'PL' THEN
		   SET oKategori = 'Pengerjaan Luar';
		WHEN 'KM' THEN
		   SET oKategori = 'BK Ke Kas';
		WHEN 'PI' THEN
		   SET oKategori = 'Purchase Invoice';
		WHEN 'SR' THEN
		   SET oKategori = 'Sales Retur';		
	END CASE;

	SET MyPesanKu = CONCAT("Post  BK - ",oKategori);
	IF @BKPerson <> '--' THEN SET MyPesanKu = CONCAT(MyPesanKu,' - ' , @BKPerson); END IF;
	IF @BKMemo <> "--" THEN SET MyPesanKu = CONCAT(MyPesanKu,' - ' , @BKMemo); END IF;
	SET MyPesanKu = TRIM(MyPesanKu);
	IF LENGTH(MyPesanKu) > 300 THEN SET MyPesanKu = SUBSTRING(MyPesanKu,0, 299); END IF;
	SET MyPesankuItem = MyPesanKu ;
	IF LENGTH(MyPesankuItem) > 150 THEN SET MyPesankuItem = SUBSTRING(MyPesankuItem,0, 149); END IF;

	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;

	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL; DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;DELETE FROM ttglhpit WHERE NoGL = xNoGL;
	DELETE FROM ttgeneralledgerhd WHERE GLLink = xBKNo;

	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, DATE(@BKTgl), 'BK', MyPesanKu, 0, 0, xBKNo, xUserID, '--', xGLValid);

	#Item
	SET @NoUrutku = 0;

  CASE @KodeTrans
		WHEN  'UM' THEN
			DROP TEMPORARY TABLE IF EXISTS ScriptTtglhpit; CREATE TEMPORARY TABLE IF NOT EXISTS ScriptTtglhpit AS
			SELECT NoGL, TglGL, GLLink, NoAccount, HPJenis, HPNilai FROM ttglhpit WHERE NoGL = xNoGL;

			INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@BKTgl), (@NoUrutku:=@NoUrutku + 1), NoAccount,  BKKeterangan, BKBayar, 0 FROM ttBKitcoa WHERE BKNo = xBKNo AND BKBayar >= 0;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@BKTgl), (@NoUrutku:=@NoUrutku + 1), NoAccount,  BKKeterangan, 0, BKBayar FROM ttBKitcoa WHERE BKNo = xBKNo AND BKBayar < 0;

			SELECT COUNT(NoGL) INTO @CountNoGL FROM ScriptTtglhpit;
			IF @CountNoGL > 0 THEN
				INSERT INTO ttglhpit SELECT * FROM ScriptTtglhpit;
				DELETE FROM ttglhpit WHERE NoAccount NOT IN (SELECT NoAccount FROM ttgeneralledgerit WHERE NoGL  = xNoGL) AND NoGL  = xNoGL;
				UPDATE ttgeneralledgerhd SET HPLink = 'Many'  WHERE NoGL = xNoGL;
			END IF;

			UPDATE ttBKhd SET BKNominal = oSumBKBayarCOA WHERE BKNo = xBKNo;
		WHEN 'PL' THEN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@BKTgl), (@NoUrutku:=@NoUrutku + 1), 50050000 , MyPesankuItem , @BKNominal, 0  ;
		WHEN 'KM' THEN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@BKTgl), (@NoUrutku:=@NoUrutku + 1), 11040000 , MyPesankuItem , @BKNominal, 0  ;
		WHEN 'H1' THEN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@BKTgl), (@NoUrutku:=@NoUrutku + 1), 11070101 , MyPesankuItem , @BKNominal, 0  ;
		WHEN 'SR' THEN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@BKTgl), (@NoUrutku:=@NoUrutku + 1), 24052100 , MyPesankuItem , @BKNominal, 0  ;
			#Link Piutang
			DELETE FROM ttglhpit WHERE NoGL = xNoGL;
			INSERT INTO ttglhpit SELECT xNoGL, DATE(@BKTgl), BKLink, '24052100','Hutang', BKBayar FROM ttBKit WHERE BKNo = xBKNo;
		WHEN 'PI' THEN
			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@BKTgl), (@NoUrutku:=@NoUrutku + 1),'24010103',CONCAT(MyPesankuItem,' - ',BKLink) , BKBayar, 0 FROM ttBKit
			INNER JOIN ttpshd ON ttpshd.PINo = ttBKit.BKlink
			WHERE BKNo = xBKNo AND ttpshd.KodeTrans = 'TC01';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@BKTgl), (@NoUrutku:=@NoUrutku + 1),'24010200',CONCAT(MyPesankuItem,' - ',BKLink) , BKBayar, 0 FROM ttBKit
			INNER JOIN ttpshd ON ttpshd.PINo = ttBKit.BKlink
			WHERE BKNo = xBKNo AND ttpshd.KodeTrans = 'TC02';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@BKTgl), (@NoUrutku:=@NoUrutku + 1),'24020102',CONCAT(MyPesankuItem,' - ',BKLink) , BKBayar, 0 FROM ttBKit
			INNER JOIN ttpshd ON ttpshd.PINo = ttBKit.BKlink
			WHERE BKNo = xBKNo AND ttpshd.KodeTrans = 'TC03';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@BKTgl), (@NoUrutku:=@NoUrutku + 1),'11130101',CONCAT(MyPesankuItem,' - ',BKLink) , BKBayar, 0 FROM ttBKit
			INNER JOIN ttpshd ON ttpshd.PINo = ttBKit.BKlink
			WHERE BKNo = xBKNo AND ttpshd.KodeTrans = 'TC06';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@BKTgl), (@NoUrutku:=@NoUrutku + 1),'24010103',CONCAT(MyPesankuItem,' - ',BKLink) , BKBayar, 0 FROM ttBKit
			INNER JOIN ttpshd ON ttpshd.PINo = ttBKit.BKlink
			WHERE BKNo = xBKNo AND ttpshd.KodeTrans = 'TC07';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@BKTgl), (@NoUrutku:=@NoUrutku + 1),'11130201',CONCAT(MyPesankuItem,' - ',BKLink) , BKBayar, 0 FROM ttBKit
			INNER JOIN ttpshd ON ttpshd.PINo = ttBKit.BKlink
			WHERE BKNo = xBKNo AND ttpshd.KodeTrans = 'TC08';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@BKTgl), (@NoUrutku:=@NoUrutku + 1),'24010103',CONCAT(MyPesankuItem,' - ',BKLink) , BKBayar, 0 FROM ttBKit
			INNER JOIN ttpshd ON ttpshd.PINo = ttBKit.BKlink
			WHERE BKNo = xBKNo AND ttpshd.KodeTrans = 'TC09';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@BKTgl), (@NoUrutku:=@NoUrutku + 1),'24010900',CONCAT(MyPesankuItem,' - ',BKLink) , BKBayar, 0 FROM ttBKit
			INNER JOIN ttpshd ON ttpshd.PINo = ttBKit.BKlink
			WHERE BKNo = xBKNo AND ttpshd.KodeTrans = 'TC10';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@BKTgl), (@NoUrutku:=@NoUrutku + 1),IFNULL(NoAccount,'24020701'),CONCAT(MyPesankuItem,' - ',BKLink), BKBayar, 0
			FROM ttBKit
			INNER JOIN ttpsit ON ttpsit.PINo = ttBKit.BKlink
			INNER JOIN ttpshd ON ttpshd.PSNo = ttpsit.PSNo
			INNER JOIN tddealer ON ttpshd.DealerKode = tddealer.DealerKode
			LEFT OUTER JOIN (SELECT NoAccount, NamaAccount FROM traccount WHERE JenisAccount ='Detail' AND NoParent = '24020700') traccount ON CONCAT('%',SUBSTRING_INDEX(TRIM(NamaAccount), ' ', -1)) = CONCAT('%',SUBSTRING_INDEX(TRIM(DealerNama), ' ', -1))
			WHERE BKNo = xBKNo AND KodeTrans = 'TC05';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@BKTgl), (@NoUrutku:=@NoUrutku + 1), IFNULL(NoAccount,'24020701'), CONCAT(MyPesankuItem,' - ',BKLink),BKBayar,0
			FROM ttBKit
			INNER JOIN ttpihd ON PINo = BKLink
			INNER JOIN ttpshd ON ttpihd.PSNo = ttpshd.PSNo
			INNER JOIN tddealer ON ttpshd.DealerKode = tddealer.DealerKode
			LEFT OUTER JOIN
			(SELECT NoAccount, NamaAccount FROM traccount WHERE JenisAccount ='Detail' AND NoParent = '24020700') traccount ON CONCAT('%',SUBSTRING_INDEX(TRIM(NamaAccount), ' ', -1)) = CONCAT('%',SUBSTRING_INDEX(TRIM(DealerNama), ' ', -1))
			WHERE BKNo = xBKNo AND ttpshd.KodeTrans = 'TC05';

			#Link Piutang
			DELETE FROM ttglhpit WHERE NoGL = xNoGL;
			INSERT INTO ttglhpit SELECT xNoGL, DATE(@BKTgl), BKlink ,'24010103','Hutang', BKBayar FROM ttBKit
			INNER JOIN ttpihd ON ttpihd.PINo = ttBKit.BKlink
			INNER JOIN ttpshd ON ttpihd.PSNo = ttpshd.PSNo
			WHERE BKNo = xBKNo AND ttpshd.KodeTrans = 'TC01';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@BKTgl), BKlink ,'24010200','Hutang', BKBayar FROM ttBKit
			INNER JOIN ttpihd ON ttpihd.PINo = ttBKit.BKlink
			INNER JOIN ttpshd ON ttpihd.PSNo = ttpshd.PSNo
			WHERE BKNo = xBKNo AND ttpshd.KodeTrans = 'TC02';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@BKTgl), BKlink ,'24020102','Hutang', BKBayar FROM ttBKit
			INNER JOIN ttpihd ON ttpihd.PINo = ttBKit.BKlink
			INNER JOIN ttpshd ON ttpihd.PSNo = ttpshd.PSNo
			WHERE BKNo = xBKNo AND ttpshd.KodeTrans = 'TC03';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@BKTgl), BKlink ,'11130101','Hutang', BKBayar FROM ttBKit
			INNER JOIN ttpihd ON ttpihd.PINo = ttBKit.BKlink
			INNER JOIN ttpshd ON ttpihd.PSNo = ttpshd.PSNo
			WHERE BKNo = xBKNo AND ttpshd.KodeTrans = 'TC06';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@BKTgl), BKlink ,'24010103','Hutang', BKBayar FROM ttBKit
			INNER JOIN ttpihd ON ttpihd.PINo = ttBKit.BKlink
			INNER JOIN ttpshd ON ttpihd.PSNo = ttpshd.PSNo
			WHERE BKNo = xBKNo AND ttpshd.KodeTrans = 'TC07';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@BKTgl), BKlink ,'11130201','Hutang', BKBayar FROM ttBKit
			INNER JOIN ttpihd ON ttpihd.PINo = ttBKit.BKlink
			INNER JOIN ttpshd ON ttpihd.PSNo = ttpshd.PSNo
			WHERE BKNo = xBKNo AND ttpshd.KodeTrans = 'TC08';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@BKTgl), BKlink ,'24010103','Hutang', BKBayar FROM ttBKit
			INNER JOIN ttpihd ON ttpihd.PINo = ttBKit.BKlink
			INNER JOIN ttpshd ON ttpihd.PSNo = ttpshd.PSNo
			WHERE BKNo = xBKNo AND ttpshd.KodeTrans = 'TC09';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@BKTgl), BKlink ,'24010900','Hutang', BKBayar FROM ttBKit
			INNER JOIN ttpihd ON ttpihd.PINo = ttBKit.BKlink
			INNER JOIN ttpshd ON ttpihd.PSNo = ttpshd.PSNo
			WHERE BKNo = xBKNo AND ttpshd.KodeTrans = 'TC10';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@BKTgl), BKlink ,IFNULL(NoAccount,'24020701'),'Hutang', BKBayar FROM ttBKit
			INNER JOIN ttpihd ON ttpihd.PINo = ttBKit.BKlink
			INNER JOIN ttpshd ON ttpihd.PSNo = ttpshd.PSNo
			INNER JOIN tddealer ON ttpshd.DealerKode = tddealer.DealerKode
			LEFT OUTER JOIN (SELECT NoAccount, NamaAccount FROM traccount WHERE JenisAccount ='Detail' AND NoParent = '24020700') traccount ON CONCAT('%',SUBSTRING_INDEX(TRIM(NamaAccount), ' ', -1)) = CONCAT('%',SUBSTRING_INDEX(TRIM(DealerNama), ' ', -1))
			WHERE BKNo = xBKNo AND ttpshd.KodeTrans = 'TC05';
	END CASE;
	INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@BKTgl), (@NoUrutku:=@NoUrutku + 1), @BankKode , MyPesankuItem , 0, @BKNominal ;

	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttBKhd SET NoGL = xNoGL  WHERE BKNo = xBKNo;

	#Memberi Nilai HPLink
	SET @CountGL = 0;
	SELECT MAX(GLLink), IFNULL(COUNT(NoGL),0) INTO @GLLink, @CountGL FROM ttglhpit WHERE NoGL = xNoGL GROUP BY NoGL;
	IF @CountGL = 0 THEN
		UPDATE ttgeneralledgerhd SET HPLink = '--' WHERE NoGL = xNoGL;
	ELSEIF @CountGL = 1 THEN
		UPDATE ttgeneralledgerhd SET HPLink = @GLLink WHERE NoGL = xNoGL;
	ELSE
		UPDATE ttgeneralledgerhd SET HPLink = 'Many' WHERE NoGL = xNoGL;
	END IF;
END$$

DELIMITER ;

