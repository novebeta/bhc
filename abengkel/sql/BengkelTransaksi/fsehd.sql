DELIMITER $$

DROP PROCEDURE IF EXISTS `fsehd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fsehd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xSENo VARCHAR(10),
IN xSENoBaru VARCHAR(10),
IN xSETgl DATETIME,
IN xSDNo VARCHAR(10),
IN xKodeTrans VARCHAR(4),
IN xMotorNoPolisi VARCHAR(12),
IN xKarKode VARCHAR(10),
IN xSETotalWaktu DECIMAL(14, 2),
IN xSETotalJasa DECIMAL(14, 2),
IN xSETotalPart DECIMAL(14, 2),
IN xSETotalBiaya DECIMAL(14, 2),
IN xSETerbayar DECIMAL(14, 2),
IN xSEKeterangan VARCHAR(150),
IN xCetak VARCHAR(5)
)
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
         DELETE FROM ttsihd WHERE (SINo LIKE '%=%') AND SITgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);	
         CALL cUPDATEtdbaranglokasi(); 		
         SET oStatus = 0; SET oKeterangan = CONCAT("Daftar Estimasi Servis [SE] ");
		END;
		WHEN "Load" THEN
		BEGIN
			 SET oStatus = 0; SET oKeterangan = CONCAT("Load data Estimasi Servis [SE] No: ", xSENo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SET @AutoNo = CONCAT('SE',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(SENo,5)+1),5,0)) INTO @AutoNo FROM ttsehd WHERE ((SENo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(SENo) = 10) ORDER BY SENo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('SE',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xSENo = @AutoNo;
			SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Estimasi Servis [SE] No: ",xSENo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @SENoOLD = xSENo;	
					
			IF LOCATE('=',xSENo) <> 0  THEN 
				SET @AutoNo = CONCAT('SE',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(SENo,5)+1),5,0)) INTO @AutoNo FROM ttsehd WHERE ((SENo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(SENo) = 10) ORDER BY SENo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('SE',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xSENoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xSENoBaru = xSENo;
			END IF;				  

			IF oStatus = 0 THEN
				UPDATE ttsehd
				SET SENo = xSENoBaru, SETgl = xSETgl, SDNo = xSDNo, KodeTrans = xKodeTrans, MotorNoPolisi = xMotorNoPolisi,
				KarKode = xKarKode, SETotalWaktu = xSETotalWaktu, SETotalJasa = xSETotalJasa, SETotalPart = xSETotalPart,
				SETotalBiaya = xSETotalBiaya, SETerbayar = xSETerbayar, SEKeterangan = xSEKeterangan, Cetak = xCetak, UserID = xUserID
				WHERE SENo = xSENo;
				SET xSENo = xSENoBaru;
				
				#AutoNumber
				UPDATE ttSEitbarang SET SEAuto = -(SEAuto) WHERE SENo = xSENo;
				blockIT: BEGIN
				DECLARE mySENo VARCHAR(10);
				DECLARE mySEAuto SMALLINT;
				DECLARE myBrgKode VARCHAR(20);
				DECLARE doneIT INT DEFAULT 0;  	
				DECLARE curIT CURSOR FOR SELECT SENo, SEAuto, BrgKode FROM ttSEitbarang WHERE SENo = xSENo ORDER BY ABS(SEAuto);	
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET doneIT = 1;
				SET @row_number = 0; 
				OPEN curIT;
				REPEAT FETCH curIT INTO mySENo,mySEAuto,myBrgKode ;
					IF NOT doneIT THEN				
						SET @row_number = @row_number + 1; 
						UPDATE ttSEitbarang SET SEAuto = @row_number
						WHERE SENo = mySENo AND SEAuto = mySEAuto AND BrgKode = myBrgKode;
					END IF;
				UNTIL doneIT END REPEAT;
				CLOSE curIT;
				END blockIT;

				#AutoNumber
				UPDATE ttSEitjasa SET SEAuto = -(SEAuto) WHERE SENo = xSENo;
				blockIT: BEGIN
				DECLARE mySENo VARCHAR(10);
				DECLARE mySEAuto SMALLINT;
				DECLARE myJasaKode VARCHAR(20);
				DECLARE doneIT INT DEFAULT 0;  	
				DECLARE curIT CURSOR FOR SELECT SENo, SEAuto, JasaKode FROM ttSEitjasa WHERE SENo = xSENo ORDER BY ABS(SEAuto);	
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET doneIT = 1;
				SET @row_number = 0; 
				OPEN curIT;
				REPEAT FETCH curIT INTO mySENo,mySEAuto,myJasaKode ;
					IF NOT doneIT THEN				
						SET @row_number = @row_number + 1; 
						UPDATE ttSEitjasa SET SEAuto = @row_number
						WHERE SENo = mySENo AND SEAuto = mySEAuto AND JasaKode = myJasaKode;
					END IF;
				UNTIL doneIT END REPEAT;
				CLOSE curIT;
				END blockIT;				

            #Hitung SubTotal & Total
            SELECT SUM((SEQty * SEHrgJual) - SEJualDisc) INTO @SETotalPart FROM ttseitbarang WHERE SENo = xSENo;
            SELECT SUM(SEHrgJual - SEJualDisc) INTO @SETotalJasa FROM ttseitjasa WHERE SENo = xSENo;
            IF @SETotalPart IS NULL THEN SET  @SETotalPart = 0; END IF;
            IF @SETotalJasa IS NULL THEN SET  @SETotalJasa = 0; END IF;
            UPDATE ttsehd SET SETotalJasa = @SETotalJasa, SETotalPart = @SETotalPart,
            SETotalBiaya = @SETotalJasa + @SETotalPart  WHERE SENo = xSENo;

				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Estimasi Servis [SE]','ttsehd', xSENo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Estimasi Servis [SE] No: ", xSENo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Estimasi Servis [SE]','ttsehd', xSENo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Estimasi Servis [SE] No: ", xSENo);
				END IF;
			ELSEIF oStatus = 1 THEN
   			IF LOCATE('=',xSENo) <> 0 THEN
   				DELETE FROM ttsehd WHERE SENo = xSENo;
   				SELECT IFNULL(SENo,'--') INTO xSENo FROM ttSEhd WHERE SENo NOT LIKE '%=%' AND LEFT(SENo,2) = 'SE' ORDER BY SETgl DESC LIMIT 1; 
   				IF xSENo IS NULL OR LOCATE('=',xSDNo) <> 0 THEN SET xSENo = '--'; END IF;
   			END IF;
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0; SET oKeterangan = ""; 
			IF oStatus = 0 THEN
				DELETE FROM ttsehd WHERE SENo = xSENo;
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Estimasi Servis [SE]  No: ",xSENo);
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Daftar Servis [SD]','ttsdhd', xSDNo); 			
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;								
		END;
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xSENo) <> 0 THEN
				DELETE FROM ttsehd WHERE SENo = xSENo;
				SET oKeterangan = CONCAT("Batal menyimpan data Estimasi Servis [SE]  No: ", xSENoBaru);
				SELECT IFNULL(SENo,'--') INTO xSENo FROM ttSEhd WHERE SENo NOT LIKE '%=%' AND LEFT(SENo,2) = 'SE' ORDER BY SETgl DESC LIMIT 1; 
				IF xSENo IS NULL OR LOCATE('=',xSDNo) <> 0 THEN SET xSENo = '--'; END IF;
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Estimasi Servis [SE]  No: ", xSENo);
			END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
		
		END;
	END CASE;
END$$

DELIMITER ;

