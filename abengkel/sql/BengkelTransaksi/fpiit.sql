DELIMITER $$

DROP PROCEDURE IF EXISTS `fpiit`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fpiit`(
    IN xAction VARCHAR(10),
    OUT oStatus INT,
    OUT oKeterangan TEXT,
    IN xUserID VARCHAR(15),
    IN xKodeAkses VARCHAR(15),
    IN xLokasiKode VARCHAR(15),
    IN xPINo VARCHAR(10),
    IN xPIAuto VARCHAR(6),
    IN xBrgKode VARCHAR(20),
    IN xPIQty DECIMAL(5, 0),
    IN xPIHrgBeli DECIMAL(14,2),
    IN xPIHrgJual DECIMAL(14,2),
    IN xPIDiscount DECIMAL(14,2),
    IN xPIPajak DECIMAL(2,0),
    IN xPSNo VARCHAR(10),
    IN xPINoOLD VARCHAR(10),
    IN xPIAutoOLD SMALLINT(6),
    IN xBrgKodeOLD VARCHAR(20)
    )
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xPINo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xPINo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Mengambil Harga Beli
			SELECT BrgHrgBeli INTO xPIHrgBeli FROM tdbarang WHERE BrgKode = xBrgKode;

			#Insert
			IF oStatus = 0 THEN
				SELECT IF(ISNULL((MAX(PIAuto)+1)),1,(MAX(PIAuto)+1)) INTO xPIAuto FROM ttpiit WHERE PINo = xPINo; #AutoN		
				INSERT INTO ttpiit
				(PINo, PIAuto, BrgKode, PIQty, PIHrgBeli, PIHrgJual, PIDiscount, PIPajak, PSNo, LokasiKode) VALUES
				(xPINo, xPIAuto, xBrgKode, xPIQty, xPIHrgBeli, xPIHrgJual, xPIDiscount, xPIPajak, xPSNo, xLokasiKode);

            #Hitung SubTotal & Total
            SELECT SUM((PIQty * PIHrgJual) - PIDiscount) INTO @SubTotal FROM ttPIit WHERE PINo = xPINo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttPIhd set PISubTotal = @SubTotal, PITotal = (@SubTotal - PIDiscFinal -  PITotalPajak - PIBiayaKirim) WHERE PINo = xPINo;
            END IF;

				SET oKeterangan = CONCAT('Berhasil menambahkan data Barang ', xBrgKode, ' = ' , FORMAT(xPIQty,0));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;			
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
		 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xPINo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xPINo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			 #Jika Beda Kode Barang, ambil Harga Beli
			 IF xBrgKode <> xBrgKodeOLD THEN
					SELECT BrgHrgBeli INTO xPIHrgBeli FROM tdbarang WHERE BrgKode = xBrgKode;
			 END IF;
			 
         IF oStatus = 0 THEN
            UPDATE ttpiit
            SET PINo = xPINo, PIAuto = xPIAuto, BrgKode = xBrgKode, PIQty = xPIQty, PIHrgBeli = xPIHrgBeli, PIHrgJual = xPIHrgJual, PIDiscount = xPIDiscount, PIPajak = xPIPajak, PSNo = xPSNo, LokasiKode = xLokasiKode
            WHERE PINo = xPINoOLD AND PIAuto = xPIAutoOLD AND BrgKode = xBrgKodeOLD;
         
            #Hitung SubTotal & Total
            SELECT SUM((PIQty * PIHrgJual) - PIDiscount) INTO @SubTotal FROM ttPIit WHERE PINo = xPINo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttPIhd set PISubTotal = @SubTotal, PITotal = (@SubTotal - PIDiscFinal -  PITotalPajak - PIBiayaKirim) WHERE PINo = xPINo;
            END IF;
         
            SET oKeterangan = CONCAT("Berhasil mengubah data Barang : ", xBrgKode, ' = ' , FORMAT(xPIQty,0));
         ELSE
            SET oKeterangan = oKeterangan;
         END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xPINo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xPINo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM ttpiit WHERE PIAuto = xPIAuto AND PINo = xPINo AND BrgKode = xBrgKode;

            #Hitung SubTotal & Total
            SELECT SUM((PIQty * PIHrgJual) - PIDiscount) INTO @SubTotal FROM ttPIit WHERE PINo = xPINo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttPIhd set PISubTotal = @SubTotal, PITotal = (@SubTotal - PIDiscFinal -  PITotalPajak - PIBiayaKirim) WHERE PINo = xPINo;
            END IF;

				SET oKeterangan = CONCAT("Berhasil menghapus data Barang : ", xBrgKode, ' = ' , FORMAT(xPIQty,0));
			ELSE
				SET oKeterangan = CONCAT("Gagal menghapus data Barang : ", xBrgKode, ' = ' , FORMAT(xPIQty,0));
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Batal menyimpan data Barang: ", xPIAuto );
		END;
	END CASE;
END$$

DELIMITER ;

