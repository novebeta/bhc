    DELIMITER $$

DROP PROCEDURE IF EXISTS `fsditjasa`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fsditjasa`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xSDNo VARCHAR(10),
IN xSDAuto SMALLINT(6),
IN xJasaKode VARCHAR(10),
IN xSDWaktuKerja DECIMAL(3, 0),
IN xSDWaktuSatuan VARCHAR(5),
IN xSDWaktuKerjaMenit DECIMAL(3, 0),
IN xSDHargaBeli DECIMAL(14, 2),
IN xSDHrgJual DECIMAL(14, 2),
IN xSDJualDisc DECIMAL(14, 2),
IN xSDPajak DECIMAL(2, 0),
IN xSDNoOLD VARCHAR(10),
IN xSDAutoOLD VARCHAR(10),
IN xJasaKodeOLD VARCHAR(10)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSDNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSDNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Mengambil Harga Beli
			SELECT JasaHrgBeli INTO xSDHargaBeli FROM tdjasa WHERE JasaKode = xJasaKode;
			
			#Insert
			IF oStatus = 0 THEN
				SELECT IF(ISNULL((MAX(SDAuto)+1)),1,(MAX(SDAuto)+1)) INTO xSDAuto FROM ttsditjasa WHERE SDNo = xSDNo; #AutoN												
				INSERT INTO ttsditjasa
				(SDNo, SDAuto, JasaKode, SDWaktuKerja, SDWaktuSatuan, SDWaktuKerjaMenit, SDHargaBeli, SDHrgJual, SDJualDisc, SDPajak) VALUES
				(xSDNo, xSDAuto, xJasaKode, xSDWaktuKerja, xSDWaktuSatuan, xSDWaktuKerjaMenit, xSDHargaBeli, xSDHrgJual, xSDJualDisc, xSDPajak);

            #Hitung SubTotal & Total
            SELECT SUM(SDQty) INTO @SDTotalQty FROM ttSDitbarang WHERE SDNo = xSDNo;
            SELECT SUM((SDQty * SDHrgJual) - SDJualDisc) INTO @SDTotalPart FROM ttSDitbarang WHERE SDNo = xSDNo;
            SELECT SUM(SDHrgJual - SDJualDisc) INTO @SDTotalJasa FROM ttSDitjasa WHERE SDNo = xSDNo;
            IF @SDTotalPart IS NULL THEN SET  @SDTotalPart = 0; END IF;
            IF @SDTotalJasa IS NULL THEN SET  @SDTotalJasa = 0; END IF;
            UPDATE ttSDhd SET
            SDTotalJasa = @SDTotalJasa,
            SDTotalPart = @SDTotalPart,
            SDTotalQty = @SDTotalQty,
            SDTotalNonGratis = @SDTotalJasa + @SDTotalPart - SDDiscFinal - SDUangMuka - SDTotalPajak,
            SDTotalBiaya = @SDTotalJasa + @SDTotalPart - SDDiscFinal - SDUangMuka - SDTotalPajak
            WHERE SDNo = xSDNo;


				SET oKeterangan = CONCAT('Berhasil menambahkan data Jasa ', xJasaKode, ' = ' , FORMAT(xSDHrgJual,0));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
			
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
		 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSDNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSDNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Jika Beda Kode Jasa, ambil Harga Beli
			IF xJasaKode <> xJasaKodeOLD THEN
				SELECT JasaHrgBeli INTO xSDHargaBeli FROM tdjasa WHERE JasaKode = xJasaKode;
			END IF;
			
			IF oStatus = 0 THEN
				UPDATE ttsditjasa
				SET SDNo = xSDNo, SDAuto = xSDAuto, JasaKode = xJasaKode, SDWaktuKerja = xSDWaktuKerja, SDWaktuSatuan = xSDWaktuSatuan, SDWaktuKerjaMenit = xSDWaktuKerjaMenit, 
				SDJualDisc = xSDJualDisc, SDPajak = xSDPajak, SDHrgJual = xSDHrgJual, SDHargaBeli = xSDHargaBeli
				WHERE SDNo = xSDNoOLD AND SDAuto = xSDAutoOLD AND JasaKode = xJasaKodeOLD;

            #Hitung SubTotal & Total
            SELECT SUM(SDQty) INTO @SDTotalQty FROM ttSDitbarang WHERE SDNo = xSDNo;
            SELECT SUM((SDQty * SDHrgJual) - SDJualDisc) INTO @SDTotalPart FROM ttSDitbarang WHERE SDNo = xSDNo;
            SELECT SUM(SDHrgJual - SDJualDisc) INTO @SDTotalJasa FROM ttSDitjasa WHERE SDNo = xSDNo;
            IF @SDTotalPart IS NULL THEN SET  @SDTotalPart = 0; END IF;
            IF @SDTotalJasa IS NULL THEN SET  @SDTotalJasa = 0; END IF;
            UPDATE ttSDhd SET
            SDTotalJasa = @SDTotalJasa,
            SDTotalPart = @SDTotalPart,
            SDTotalQty = @SDTotalQty,
            SDTotalNonGratis = @SDTotalJasa + @SDTotalPart - SDDiscFinal - SDUangMuka - SDTotalPajak,
            SDTotalBiaya = @SDTotalJasa + @SDTotalPart - SDDiscFinal - SDUangMuka - SDTotalPajak
            WHERE SDNo = xSDNo;


				SET oKeterangan = CONCAT("Berhasil mengubah data Jasa : ", xJasaKode, ' = ' , FORMAT(xSDHrgJual,0));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSDNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSDNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			IF oStatus = 0 THEN
				DELETE FROM ttsditjasa WHERE SDAuto = xSDAuto AND SDNo = xSDNo AND JasaKode = xJasaKode;

            #Hitung SubTotal & Total
            SELECT SUM(SDQty) INTO @SDTotalQty FROM ttSDitbarang WHERE SDNo = xSDNo;
            SELECT SUM((SDQty * SDHrgJual) - SDJualDisc) INTO @SDTotalPart FROM ttSDitbarang WHERE SDNo = xSDNo;
            SELECT SUM(SDHrgJual - SDJualDisc) INTO @SDTotalJasa FROM ttSDitjasa WHERE SDNo = xSDNo;
            IF @SDTotalPart IS NULL THEN SET  @SDTotalPart = 0; END IF;
            IF @SDTotalJasa IS NULL THEN SET  @SDTotalJasa = 0; END IF;
            UPDATE ttSDhd SET
            SDTotalJasa = @SDTotalJasa,
            SDTotalPart = @SDTotalPart,
            SDTotalQty = @SDTotalQty,
            SDTotalNonGratis = @SDTotalJasa + @SDTotalPart - SDDiscFinal - SDUangMuka - SDTotalPajak,
            SDTotalBiaya = @SDTotalJasa + @SDTotalPart - SDDiscFinal - SDUangMuka - SDTotalPajak
            WHERE SDNo = xSDNo;

				SET oKeterangan = CONCAT("Berhasil menghapus data Jasa : ", xJasaKode, ' = ' , FORMAT(xSDHrgJual,0));
			ELSE
				SET oKeterangan = 0;
			END IF;				
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0; SET oKeterangan = CONCAT("Batal menyimpan data jasa: ", xJasaKode, ' = ' , FORMAT(xSDHrgJual,0));
		END;
	END CASE;
END$$

DELIMITER ;

