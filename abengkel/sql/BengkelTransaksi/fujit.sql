DELIMITER $$

DROP PROCEDURE IF EXISTS `fujit`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fujit`(
    IN xAction VARCHAR(10),
    OUT oStatus INT,
    OUT oKeterangan TEXT,
    IN xUserID VARCHAR(15),
    IN xKodeAkses VARCHAR(15),
    IN xLokasiKode VARCHAR(15),
    IN xUJNo VARCHAR(10),
    IN xUJAuto VARCHAR(10),
    IN xJasaKode VARCHAR(20),
    IN xUJHrgJualLama DECIMAL(14, 2),
    IN xUJHrgJualBaru DECIMAL(14, 2),
    IN xUJHrgBeliLama DECIMAL(14, 2),
    IN xUJHrgBeliBaru DECIMAL(14, 2),
    IN xUJNoOLD VARCHAR(10),
    IN xUJAutoOLD VARCHAR(10),
    IN xJasaKodeOLD VARCHAR(20)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;

 			#Mengambil Harga Beli
			SELECT JasaHrgBeli, JasaHrgJual INTO xUJHrgBeliLama, xUJHrgJualLama FROM tdjasa WHERE JasaKode = xJasaKode;
			
			SELECT IF(ISNULL((MAX(UJAuto)+1)),1,(MAX(UJAuto)+1)) INTO xUJAuto FROM ttujit WHERE UJNo = xUJNo; #AutoN	
			INSERT INTO ttujit
			(UJNo, UJAuto, JasaKode, UJHrgJualLama, UJHrgJualBaru, UJHrgBeliLama, UJHrgBeliBaru) VALUES
			(xUJNo, xUJAuto, xJasaKode, xUJHrgJualLama, xUJHrgJualBaru, xUJHrgBeliLama, xUJHrgBeliBaru);
			 
			 SET oKeterangan = CONCAT('Berhasil menambahkan data Jasa ', xJasaKode);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			
			#Jika Beda Kode Barang, ambil Harga Beli
			IF xJasaKode <> xJasaKodeOLD THEN
				SELECT JasaHrgBeli, JasaHrgJual INTO xUJHrgBeliLama, xUJHrgJualLama FROM tdjasa WHERE JasaKode = xJasaKode;
			END IF;
			
			IF oStatus = 0 THEN
				UPDATE ttujit
				SET UJNo = xUJNo, UJAuto = xUJAuto, JasaKode = xJasaKode, UJHrgJualLama = xUJHrgJualLama, UJHrgJualBaru = xUJHrgJualBaru, UJHrgBeliLama = xUJHrgBeliLama, UJHrgBeliBaru = xUJHrgBeliBaru
				WHERE UJNo = xUJNoOLD AND UJAuto = xUJAutoOLD AND xJasaKode = xJasaKodeOLD;
				SET oKeterangan = CONCAT("Berhasil mengubah data Jasa : ", xJasaKode);
			ELSE
				SET oKeterangan = CONCAT("Gagal mengubah data Jasa : ", xJasaKode);
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			 SET oStatus = 0;
			 IF oStatus = 0 THEN
				  DELETE FROM ttujit WHERE UJAuto = xUJAuto AND UJNo = xUJNo AND JasaKode = xJasaKode;
				  SET oKeterangan = CONCAT("Berhasil menghapus data Jasa : ", xJasaKode);
			 ELSE
				  SET oKeterangan = CONCAT("Gagal menghapus data Jasa : ", xJasaKode);
			 END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Batal menyimpan data Jasa : ", xJasaKode );
		END;
	END CASE;
END$$

DELIMITER ;

