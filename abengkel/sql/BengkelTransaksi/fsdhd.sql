DELIMITER $$
DROP PROCEDURE IF EXISTS `fsdhd`$$
CREATE DEFINER=`root`@`%` PROCEDURE `fsdhd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xSDNo VARCHAR(10),
IN xSDNoBaru VARCHAR(10),
IN xSVNo VARCHAR(10),
IN xSENo VARCHAR(10),
IN xNoGLSD VARCHAR(10),
IN xNoGLSV VARCHAR(10),
IN xSDTgl DATETIME,
IN xSVTgl DATETIME,
IN xKodeTrans VARCHAR(4),
IN xASS VARCHAR(10),
IN xPosKode VARCHAR(20),
IN xKarKode VARCHAR(10),
IN xPrgNama VARCHAR(25),
IN xPKBNo VARCHAR(20),
IN xMotorNoPolisi VARCHAR(12),
IN xSDNoUrut VARCHAR(3),
IN xSDJenis VARCHAR(6),
IN xSDTOP DECIMAL(3, 2),
IN xSDDurasi DECIMAL(9, 2),
IN xSDJamMasuk DATETIME,
IN xSDJamSelesai DATETIME,
IN xSDJamDikerjakan DATETIME,
IN xSDTotalWaktu DECIMAL(3, 0),
IN xSDStatus VARCHAR(15),
IN xSDKmSekarang DECIMAL(6, 2),
IN xSDKmBerikut DECIMAL(6, 2),
IN xSDKeluhan VARCHAR(150),
IN xSDKeterangan VARCHAR(150),
IN xSDPembawaMotor VARCHAR(75),
IN xSDHubunganPembawa VARCHAR(25),
IN xSDAlasanServis VARCHAR(15),
IN xSDTotalJasa DECIMAL(14, 2),
IN xSDTotalQty DECIMAL(3, 2),
IN xSDTotalPart DECIMAL(14, 2),
IN xSDTotalGratis DECIMAL(14, 2),
IN xSDTotalNonGratis DECIMAL(14, 2),
IN xSDDiscFinal DECIMAL(14, 2),
IN xSDTotalPajak DECIMAL(14, 2),
IN xSDUangMuka DECIMAL(14, 2),
IN xSDTotalBiaya DECIMAL(14, 2),
IN xSDKasKeluar DECIMAL(14, 2),
IN xSDTerbayar DECIMAL(14, 2),
IN xKlaimKPB VARCHAR(5),
IN xKlaimC2 VARCHAR(5),
IN xCetak VARCHAR(5))

BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
         DELETE FROM ttsdhd WHERE (SDNo LIKE '%=%') AND SDTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);	
			#ReJurnal Transaksi Tidak Memiliki Jurnal
			cek01: BEGIN
   			DECLARE rSDNo VARCHAR(100) DEFAULT '';
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT SDNo FROM ttSDhd
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttSDhd.SDNo 
				WHERE ttgeneralledgerhd.NoGL IS NULL AND SDTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rSDNo;
					IF rSDNo IS NOT NULL AND rSDNo <> '' THEN
						CALL jSD(rSDNo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;	
         CALL cUPDATEtdbaranglokasi();          		
			SET oStatus = 0; SET oKeterangan = CONCAT("Daftar Servis [SD] ");
		END;
		WHEN "Load" THEN
		BEGIN
			SET oStatus = 0; SET oKeterangan = CONCAT("Load data Daftar Servis [SD] No: ", xSDNo," Status :",xSDStatus);
		END;
		WHEN "Insert" THEN
		BEGIN
			 SET @AutoNo = CONCAT('SD',RIGHT(YEAR(NOW()),2),'-');
			 SELECT CONCAT(@AutoNo,LPAD((RIGHT(SDNo,5)+1),5,0)) INTO @AutoNo FROM ttsdhd WHERE ((SDNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(SDNo) = 10) ORDER BY SDNo DESC LIMIT 1;
			 IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('SD',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			 SET xSDNo = @AutoNo;
			 SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Daftar Servis [SD] No: ",xSDNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @SDNoOLD = xSDNo;

            SET oKeterangan = xSDStatus;

            /*
            IF LOCATE('=',xSDNo) <> 0  THEN
                SET @AutoNo = CONCAT('SD',RIGHT(YEAR(NOW()),2),'-');
                SELECT CONCAT(@AutoNo,LPAD((RIGHT(SDNo,5)+1),5,0)) INTO @AutoNo FROM ttsdhd WHERE ((SDNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(SDNo) = 10) ORDER BY SDNo DESC LIMIT 1;
                IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('SD',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
                SET xSDNoBaru = @AutoNo;
            ELSE
                SET @AutoNo = 'Edit';
            END IF;

            if (xSDNoUrut='--') Then
                SELECT IF(ISNULL(MAX(SDNoUrut)) OR MAX(SDNoUrut)="--","001",RIGHT(MAX(SDNoUrut)+1001,3)) into @xSDNoUrut FROM ttsdhd WHERE DATE_FORMAT(SDTgl,"%Y-%m-%d") = date_format(xSDTgl,"%Y-%m-%d");
                Set xSDNoUrut = @xSDNoUrut;
            end if;

            #Cek Jurnal
            SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSDNo;
            IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
                SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSDNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
            END IF;


            IF oStatus = 0 THEN
                UPDATE ttsdhd
                SET SDNo = xSDNoBaru, SVNo = xSVNo, SENo = xSENo, NoGLSD = xNoGLSD, NoGLSV = xNoGLSV, SDTgl = xSDTgl, SVTgl = xSVTgl, KodeTrans = xKodeTrans,
                ASS = xASS, PosKode = xPosKode, KarKode = xKarKode, PrgNama = xPrgNama, PKBNo = xPKBNo, MotorNoPolisi = xMotorNoPolisi, LokasiKode = xLokasiKode,
                SDNoUrut = xSDNoUrut, SDJenis = xSDJenis, SDTOP = xSDTOP, SDDurasi = xSDDurasi, SDJamMasuk = xSDJamMasuk, SDJamSelesai = xSDJamSelesai, SDJamDikerjakan = xSDJamDikerjakan,
                SDTotalWaktu = xSDTotalWaktu, SDStatus = xSDStatus, SDKmSekarang = xSDKmSekarang, SDKmBerikut = xSDKmBerikut, SDKeluhan = xSDKeluhan, SDKeterangan = xSDKeterangan,
                SDPembawaMotor = xSDPembawaMotor, SDHubunganPembawa = xSDHubunganPembawa, SDAlasanServis = xSDAlasanServis, SDTotalJasa = xSDTotalJasa, SDTotalQty = xSDTotalQty,
                SDTotalPart = xSDTotalPart, SDTotalGratis = xSDTotalGratis, SDTotalNonGratis = xSDTotalNonGratis, SDDiscFinal = xSDDiscFinal, SDTotalPajak = xSDTotalPajak, SDUangMuka = xSDUangMuka,
                SDTotalBiaya = xSDTotalBiaya, SDKasKeluar = xSDKasKeluar, SDTerbayar = xSDTerbayar, KlaimKPB = xKlaimKPB, KlaimC2 = xKlaimC2, SDTOP = xSDTOP, Cetak = xCetak, UserID = xUserID
                WHERE SDNo = xSDNo;
                SET xSDNo = xSDNoBaru;

                #Auto Barang
                UPDATE ttsditbarang SET SDAuto = -(3*SDAuto) WHERE SDNo = xSDNo;
                SET @row_number = 0;
                DROP TEMPORARY TABLE IF EXISTS TNoUrut;
                CREATE TEMPORARY TABLE IF NOT EXISTS TNoUrut AS
                SELECT SDNo, SDAuto,(@row_number:=@row_number + 1) AS NoUrut , BrgKode FROM ttsditbarang WHERE SDNo = xSDNo ORDER BY SDAuto DESC;
                UPDATE ttsditbarang INNER JOIN TNoUrut
                ON  ttsditbarang.SDNo = TNoUrut.SDNo
                AND ttsditbarang.BrgKode = TNoUrut.BrgKode
                AND ttsditbarang.SDAuto = TNoUrut.SDAuto
                SET ttsditbarang.SDAuto = TNoUrut.NoUrut;
                DROP TEMPORARY TABLE IF EXISTS TNoUrut;

                #Auto Jasa
                UPDATE ttsditjasa SET SDAuto = -(3*SDAuto) WHERE SDNo = xSDNo;
                SET @row_number = 0;
                DROP TEMPORARY TABLE IF EXISTS TNoUrut;
                CREATE TEMPORARY TABLE IF NOT EXISTS TNoUrut AS
                SELECT SDNo, SDAuto,(@row_number:=@row_number + 1) AS NoUrut , JasaKode FROM ttsditjasa WHERE SDNo = xSDNo ORDER BY SDAuto DESC;
                UPDATE ttsditjasa INNER JOIN TNoUrut
                ON  ttsditjasa.SDNo = TNoUrut.SDNo
                AND ttsditjasa.JasaKode = TNoUrut.JasaKode
                AND ttsditjasa.SDAuto = TNoUrut.SDAuto
                SET ttsditjasa.SDAuto = TNoUrut.NoUrut;
                DROP TEMPORARY TABLE IF EXISTS TNoUrut;

            #Hitung SubTotal & Total
            SELECT SUM(SDQty) INTO @SDTotalQty FROM ttSDitbarang WHERE SDNo = xSDNo;
            SELECT SUM((SDQty * SDHrgJual) - SDJualDisc) INTO @SDTotalPart FROM ttSDitbarang WHERE SDNo = xSDNo;
            SELECT SUM(SDHrgJual - SDJualDisc) INTO @SDTotalJasa FROM ttSDitjasa WHERE SDNo = xSDNo;
            IF @SDTotalPart IS NULL THEN SET  @SDTotalPart = 0; END IF;
            IF @SDTotalJasa IS NULL THEN SET  @SDTotalJasa = 0; END IF;
            UPDATE ttSDhd SET
            SDTotalJasa = @SDTotalJasa,
            SDTotalPart = @SDTotalPart,
            SDTotalQty = @SDTotalQty,
            SDTotalNonGratis = @SDTotalJasa + @SDTotalPart - SDDiscFinal - SDUangMuka - SDTotalPajak,
            SDTotalBiaya = @SDTotalJasa + @SDTotalPart - SDDiscFinal - SDUangMuka - SDTotalPajak
            WHERE SDNo = xSDNo;

                #Jurnal
                DELETE FROM ttgeneralledgerhd WHERE GLLink = @SDNoOLD; #Hapus Jurnal No Transaksi Lama
                CALL jsd(xSDNo,xUserID);

                  IF @AutoNo = 'Edit' THEN
                    INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE
                    (NOW(), xUserID , 'Edit' , 'Servis Daftar [SD]','ttsdhd', xSDNo);
                    SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Daftar Servis [SD] No: ", xSDNo," Status :",xSDStatus);
                ELSE
                    INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE
                    (NOW(), xUserID , 'Tambah' , 'Servis Daftar [SD]','ttsdhd', xSDNo);
                    SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Daftar Servis [SD] No: ", xSDNo," Status :",xSDStatus);
                END IF;
            ELSEIF oStatus = 1 THEN
                SET oKeterangan = oKeterangan;
            END IF;
            */
		END;
		WHEN "Delete" THEN
		BEGIN	
			SET oKeterangan = "";
			SET oStatus = 0;

 			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSDNo;
			IF @GLValid = 'Sudah' THEN	
				SET oStatus = 1; SET oKeterangan = CONCAT("No SD:", xSDNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM ttsdhd WHERE SDNo = xSDNo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xSDNo;
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Daftar Servis [SD] No: ",xSDNo);
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Daftar Servis [SD]','ttsdhd', xSDNo); 			
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;		
		END;
		WHEN "Loop" THEN
		BEGIN
            UPDATE ttSEhd SET SDNo = xSDNo WHERE SENo = xSENo;
            UPDATE ttSDhd SET SENo = xSENo,SDUangMuka = xSDUangMuka,MotorNoPolisi = xMotorNoPolisi WHERE SDNo = xSDNo;
		END;		
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xSDNo) <> 0 THEN
				DELETE FROM ttsdhd WHERE SDNo = xSDNo;
				SET oKeterangan = CONCAT("Batal menyimpan data Daftar Servis [SD]  No: ", xSDNo);
				SELECT IFNULL(SDNo,'--') INTO xSDNo FROM ttSDhd WHERE SDNo NOT LIKE '%=%' AND LEFT(SDNo,2) = 'SD' ORDER BY SDTgl DESC LIMIT 1; 
				IF xSDNo IS NULL OR LOCATE('=',xSDNo) <> 0 THEN SET xSDNo = '--'; END IF;
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Daftar Servis [SD]  No: ", xSDNo);
			END IF;
		END;
		WHEN "Jurnal" THEN
		BEGIN
			 IF LOCATE('=',xSDNo) <> 0 THEN 
				  SET oStatus = 1;
				  SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			 ELSE
				  /*CALL jsd(xSDNo,xUserID);*/
				  SET oStatus = 0;
				  SET oKeterangan = CONCAT("Berhasil memproses Jurnal Daftar Servis [SD]  No: ", xSDNo);
			 END IF;
		END;

	END CASE;
END$$

DELIMITER ;

