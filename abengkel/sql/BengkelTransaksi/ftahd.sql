DELIMITER $$

DROP PROCEDURE IF EXISTS `ftahd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `ftahd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xTANo VARCHAR(10),
IN xTANoBaru VARCHAR(10),
IN xTATgl DATETIME,
IN xKodeTrans VARCHAR(4),
IN xPosKode VARCHAR(20),
IN xTATotal DECIMAL(14, 2),
IN xTAKeterangan VARCHAR(150),
IN xNoGL VARCHAR(10),
IN xCetak VARCHAR(5))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
         DELETE FROM tttahd WHERE (TANo LIKE '%=%') AND TATgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
			#ReJurnal Transaksi Tidak Memiliki Jurnal
			cek01: BEGIN
   			DECLARE rTANo VARCHAR(100) DEFAULT '';
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT TANo FROM ttTAhd
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttTAhd.TANo 
				WHERE ttgeneralledgerhd.NoGL IS NULL AND TATgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rTANo;
					IF rTANo IS NOT NULL AND rTANo <> '' THEN
						CALL jTA(rTANo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;			
         CALL cUPDATEtdbaranglokasi(); 		        
			SET oStatus = 0; SET oKeterangan = CONCAT("Daftar Penyesuaian [TA] ");
		END;
		WHEN "Load" THEN
		BEGIN
			 SET oStatus = 0; SET oKeterangan = CONCAT("Load data Penyesuaian [TA] No: ", xTANo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SET @AutoNo = CONCAT('TA',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(TANo,5)+1),5,0)) INTO @AutoNo FROM tttahd WHERE ((TANo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(TANo) = 10) ORDER BY TANo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('TA',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xTANo = @AutoNo;
			SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Penyesuaian [TA] No: ",xTANo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @TANoOLD = xTANo;	
		
			IF LOCATE('=',xTANo) THEN 
				SET @AutoNo = CONCAT('TA',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(TANo,5)+1),5,0)) INTO @AutoNo FROM tttahd WHERE ((TANo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(TANo) = 10) ORDER BY TANo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('TA',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xTANoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';				
				SET xTANoBaru = xTANo;
			END IF;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xTANo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xTANo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN 
				UPDATE tttahd
				SET TANo = xTANoBaru, TATgl = xTATgl, KodeTrans = xKodeTrans, PosKode = xPosKode, LokasiKode = xLokasiKode, TATotal = xTATotal, TAKeterangan = xTAKeterangan, NoGL = xNoGL, Cetak = xCetak, UserID = xUserID
				WHERE TANo = xTANo;
				SET xTANo = xTANoBaru;

				#AutoNumber
				UPDATE ttTAit SET TAAuto = -(3*TAAuto) WHERE TANo = xTANo;
				SET @row_number = 0;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;
				CREATE TEMPORARY TABLE IF NOT EXISTS TNoUrut AS
				SELECT TANo, TAAuto,(@row_number:=@row_number + 1) AS NoUrut , BrgKode FROM ttTAit
				WHERE TANo = xTANo ORDER BY TAAuto DESC;
				UPDATE ttTAit INNER JOIN TNoUrut
				ON  ttTAit.TANo = TNoUrut.TANo
				AND ttTAit.BrgKode = TNoUrut.BrgKode
				AND ttTAit.TAAuto = TNoUrut.TAAuto
				SET ttTAit.TAAuto = TNoUrut.NoUrut;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;

            #Hitung SubTotal & Total
            SELECT SUM((TAQty * TAHrgBeli) ) INTO @SubTotal FROM ttTAit WHERE TANo = xTANo;
            IF @Subtotal IS NOT NULL THEN
               UPDATE ttTAhd SET TATotal = @SubTotal WHERE TANo = xTANo;
            END IF;
							
				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @TANoOLD; #Hapus Jurnal No Transaksi Lama
				CALL jTA(xTANo,xUserID); 

  				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Penyesuaian [TA]','tttahd', xTANo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Penyesuaian [TA] No: ", xTANo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Penyesuaian [TA]','tttahd', xTANo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Penyesuaian [TA] No: ", xTANo);
				END IF;
			ELSEIF oStatus = 1 THEN
   			IF LOCATE('=',xTANo) <> 0 THEN
   				DELETE FROM tttahd WHERE TANo = xTANo;
   				SELECT IFNULL(TANo,'--') INTO xTANo FROM ttTAhd WHERE TANo NOT LIKE '%=%' AND LEFT(TANo,2) = 'TA' ORDER BY TATgl DESC LIMIT 1; 
   				IF xTANo IS NULL OR LOCATE('=',xTANo) <> 0 THEN SET xTANo = '--'; END IF;
   			END IF;
            SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;

 			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xTANo;
			IF @GLValid = 'Sudah' THEN	
				SET oStatus = 1; SET oKeterangan = CONCAT("No TA:", xTANo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM tttahd WHERE TANo = xTANo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xTANo;
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Penyesuaian [TA] No: ",xTANo);
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Penyesuaian [TA]','tttahd', xTANo); 			
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;		
		END;
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xTANo) <> 0 THEN
				DELETE FROM tttahd WHERE TANo = xTANo;
				SELECT IFNULL(TANo,'--') INTO xTANo FROM ttTAhd WHERE TANo NOT LIKE '%=%' AND LEFT(TANo,2) = 'TA' ORDER BY TATgl DESC LIMIT 1; 
				IF xTANo IS NULL OR LOCATE('=',xTANo) <> 0 THEN SET xTANo = '--'; END IF;
				SET oKeterangan = CONCAT("Batal menyimpan data Penyesuaian [TA] No: ", xTANoBaru);
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Penyesuaian [TA] No: ", xTANo);
			END IF;
		END;
		WHEN "Jurnal" THEN
		BEGIN
			 IF LOCATE('=',xTANo) <> 0 THEN /* Data Baru, belum tersimpan tidak boleh di jurnal */
				  SET oStatus = 1;
				  SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			 ELSE
				  CALL jta(xTANo,xUserID);
				  SET oStatus = 0;
				  SET oKeterangan = CONCAT("Berhasil memproses Jurnal Penyesuaian [TA] No: ", xTANo);
			 END IF;
		END;
	END CASE;
END$$

DELIMITER ;

