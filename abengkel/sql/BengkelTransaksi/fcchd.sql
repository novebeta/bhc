DELIMITER $$

DROP PROCEDURE IF EXISTS `fcchd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fcchd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xCCNo VARCHAR(10),
IN xCCNoBaru VARCHAR(10),
IN xCCTgl DATETIME,
IN xCCTotalPart DECIMAL(14, 2),
IN xCCTotalJasa DECIMAL(14, 2),
IN xCCNoKlaim VARCHAR(100),
IN xCCMemo VARCHAR(150),
IN xCetak VARCHAR(5),
IN xNoGL VARCHAR(10)
)
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			DELETE FROM ttcchd WHERE (CCNo LIKE '%=%') AND CCTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);	
         
			#ReJurnal Transaksi Tidak Memiliki Jurnal
			cek01: BEGIN
   			DECLARE rCCNo VARCHAR(100) DEFAULT '';
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT CCNo FROM ttCChd
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttCChd.CCNo 
				WHERE ttgeneralledgerhd.NoGL IS NULL AND CCTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rCCNo;
					IF rCCNo IS NOT NULL AND rCCNo <> '' THEN
						CALL jcc(rCCNo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;			
			SET oStatus = 0;SET oKeterangan = CONCAT("Daftar Klaim C2 [CC] ");
		END;
		WHEN "Load" THEN
		BEGIN
			SET oStatus = 0; SET oKeterangan = CONCAT("Load data Klaim C2 [CC] No: ", xCCNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SET @AutoNo = CONCAT('CC',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(CCNo,5)+1),5,0)) INTO @AutoNo FROM ttcchd WHERE ((CCNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(CCNo) = 10) ORDER BY CCNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('CC',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xCCNo = @AutoNo;
			SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Klaim C2 [CC] No: ",xCCNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @CCNoOLD = xCCNo;	
			
			IF LOCATE('=',xCCNo) <> 0 THEN
				SET @AutoNo = CONCAT('CC',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(CCNo,5)+1),5,0)) INTO @AutoNo FROM ttcchd WHERE ((CCNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(CCNo) = 10) ORDER BY CCNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('CC',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xCCNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xCCNoBaru = xCCNo;
			END IF;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xCCNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xCCNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			# Update 
			IF oStatus = 0 THEN 
				UPDATE ttcchd
				SET CCNo = xCCNoBaru, CCTgl = xCCTgl, CCTotalPart = xCCTotalPart, CCTotalJasa = xCCTotalJasa, CCNoKlaim = xCCNoKlaim, CCMemo = xCCMemo, NoGL = xNoGL, Cetak = xCetak, UserID = xUserID
				WHERE CCNo = xCCNo;
				SET xCCNo = xCCNoBaru;
				
				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @CCNoOLD; #Hapus Jurnal No Transaksi Lama
				CALL jcc(xCCNo,xUserID); #Posting Jurnal
				
				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Klaim C2','ttcchd', xCCNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Klaim C2 [CC] No: ", xCCNo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Klaim C2','ttcchd', xCCNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Klaim C2 [CC] No: ", xCCNo);
				END IF;
			ELSEIF oStatus = 1 THEN
            IF LOCATE('=',xCCNo) <> 0 THEN
               DELETE FROM ttcchd WHERE CCNo = xCCNo;
               SELECT IFNULL(CCNo,'--') INTO xCCNo FROM ttCChd WHERE CCNo NOT LIKE '%=%' AND LEFT(CCNo,2) = 'CC' ORDER BY CCTgl DESC LIMIT 1; 
               IF xCCNo IS NULL OR LOCATE('=',xCCNo) <> 0 THEN SET xCCNo = '--'; END IF;
            END IF;
				SET oKeterangan = oKeterangan ;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;
			
			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xCCNo;
			IF @GLValid = 'Sudah' THEN	
				SET oStatus = 1; SET oKeterangan = CONCAT("No CC:", xCCNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM ttcchd WHERE CCNo = xCCNo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xCCNo;
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Klaim C2 [CC] No: ",xCCNo);
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Klaim C2','ttcchd', xCCNo); 			
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;		
		END;
		WHEN "Cancel" THEN
		BEGIN
			 IF LOCATE('=',xCCNo) <> 0 THEN
				DELETE FROM ttcchd WHERE CCNo = xCCNo;
				SET oKeterangan = CONCAT("Batal menyimpan data Klaim C2 [CC] No: ", xCCNoBaru);
				SELECT IFNULL(CCNo,'--') INTO xCCNo FROM ttCChd WHERE CCNo NOT LIKE '%=%' AND LEFT(CCNo,2) = 'CC' ORDER BY CCTgl DESC LIMIT 1; 
				IF xCCNo IS NULL OR LOCATE('=',xCCNo) <> 0 THEN SET xCCNo = '--'; END IF;
			 ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Klaim C2 [CC] No: ", xCCNo);
			 END IF;
		END;
		WHEN "Jurnal" THEN
		BEGIN
			 IF LOCATE('=',xCCNo) <> 0 THEN 
				  SET oStatus = 1; SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			 ELSE
				  CALL jcc(xCCNo,xUserID);
				  SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil memproses Jurnal Klaim C2 [CC] No: ", xCCNo);
			 END IF;
		END;
	END CASE;
END$$

DELIMITER ;

