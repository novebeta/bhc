DELIMITER $$

DROP PROCEDURE IF EXISTS `fgeneralledgerit`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fgeneralledgerit`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xNoGL VARCHAR(10),
IN xTglGL DATE,
IN xGLAutoN SMALLINT(5),
IN xNoAccount VARCHAR(10),
IN xKeteranganGL VARCHAR(150),
IN xDebetGL DECIMAL(14,2),
IN xKreditGL DECIMAL(14,2),
IN xNoGLOLD VARCHAR(10),
IN xTglGLOLD DATE,
IN xGLAutoNOLD SMALLINT(5),
IN xNoAccountOLD VARCHAR(10))
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;
			
			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE NoGL = xNoGL;
			IF @GLValid = 'Sudah' THEN	
			  SET oStatus = 1; SET oKeterangan = CONCAT("No Jurnal : ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN	
				SELECT (MAX(GLAutoN)+1) INTO xGLAutoN FROM ttgeneralledgerit WHERE NoGL = xNoGL; #AutoN
				INSERT INTO ttgeneralledgerit
				(`NoGL`, `TglGL`, `GLAutoN`, `NoAccount`, `KeteranganGL`, `DebetGL`, `KreditGL`) VALUES
				(xNoGL, xTglGL, xGLAutoN, xNoAccount, xKeteranganGL, xDebetGL, xKreditGL);
				SET oKeterangan = CONCAT('Berhasil menambahkan data Perkiraan ', xNoGL,' ', xGLAutoN);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Update" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;

			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE NoGL = xNoGL;
			IF @GLValid = 'Sudah' THEN	
				SET oStatus = 1; SET oKeterangan = CONCAT("No Jurnal : ",@NoGL," sudah tervalidasi");
			END IF;	

			 IF oStatus = 0 THEN
				UPDATE ttgeneralledgerit
				SET NoGL = xNoGL, TglGL = xTglGL, GLAutoN = xGLAutoN ,NoAccount = xNoAccount, KeteranganGL = xKeteranganGL,
				DebetGL = xDebetGL, KreditGL = xKreditGL
				WHERE NoGL = xNoGLOLD AND GLAutoN = xGLAutoNOLD AND NoAccount = xNoAccountOLD;
				SET oKeterangan = CONCAT("Berhasil mengubah data Perkiraan : ", xGLAutoN , ' - ' ,xNoGL);
			 ELSE
				SET oKeterangan = oKeterangan;
			 END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;
			
			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE NoGL = xNoGL;
			IF @GLValid = 'Sudah' THEN	
			  SET oStatus = 1; SET oKeterangan = CONCAT("No Jurnal : ",@NoGL," sudah tervalidasi");
			END IF;		
		
			IF oStatus = 0 THEN
				DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL AND GLAutoN = xGLAutoN AND NoAccount = xNoAccount;
				DELETE FROM ttglhpit WHERE NoGL = xNoGL AND NoAccount = xNoAccount;			
				SET oKeterangan = CONCAT("Berhasil menghapus data Perkiraan : ", xGLAutoN , ' - ' ,xNoGL);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Batal menyimpan data Perkiraan: ", xNoGL );
		END;
	END CASE;
END$$

DELIMITER ;

