DELIMITER $$

DROP PROCEDURE IF EXISTS `jcc`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jcc`(IN xCCNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE MyPesanKu VARCHAR(300) DEFAULT '';
	DECLARE MyPesankuItem VARCHAR(150) DEFAULT '';

	DECLARE oCCTotalPart, oCCTotalJasa DOUBLE DEFAULT 0;
	SET @NoUrutku = 0;

	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xCCNo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;

	SELECT CCNo, CCTgl, CCTotalPart, CCTotalJasa, CCNoKlaim, CCMemo, NoGL, Cetak, UserID
	INTO @CCNo, @CCTgl, oCCTotalPart, oCCTotalJasa, @CCNoKlaim, @CCMemo, @NoGL, @Cetak, @UserID
	FROM ttcchd	WHERE CCNo = xCCNo;

	SET MyPesanKu = CONCAT('Post Klaim C2 No : ' , @CCNo , ' - ' , @CCNoKlaim, ' - ', @CCMemo );
	SET MyPesanKu = TRIM(MyPesanKu);  IF LENGTH(MyPesanKu) > 300 THEN SET MyPesanKu = SUBSTRING(MyPesanKu,0, 299); END IF;
	IF LENGTH(MyPesanku) > 150 THEN SET MyPesankuItem = SUBSTRING(MyPesanku,0, 149); ELSE SET MyPesankuItem = MyPesanku; END IF;

	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;

	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL; DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;DELETE FROM ttglhpit WHERE NoGL = xNoGL;
	DELETE FROM ttgeneralledgerhd WHERE GLLink = xCCNo;

	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, DATE(@CCTgl), 'CC', MyPesanKu, 0, 0, xCCNo, xUserID, '--', xGLValid);

	#Item
	IF oCCTotalPart > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@CCTgl), (@NoUrutku:=@NoUrutku + 1),'11130201',MyPesankuItem, oCCTotalPart ,0;
	END IF;
	IF oCCTotalJasa > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@CCTgl), (@NoUrutku:=@NoUrutku + 1),'11130202',MyPesankuItem, oCCTotalJasa ,0;
	END IF;
	IF oCCTotalPart > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@CCTgl), (@NoUrutku:=@NoUrutku + 1),'11060301',MyPesankuItem, 0, oCCTotalPart;
	END IF;
	IF oCCTotalJasa > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@CCTgl), (@NoUrutku:=@NoUrutku + 1),'11060302',MyPesankuItem, 0, oCCTotalJasa;
	END IF;

	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttCChd SET NoGL = xNoGL  WHERE CCNo = xCCNo;

	#Link Piutang
	DELETE FROM ttglhpit WHERE NoGL = xNoGL;

	INSERT INTO ttglhpit
	SELECT xNoGL, DATE(@CCTgl), REPLACE(SDNo,'SD','SV') AS SVNo, '11060301', 'Piutang', CCJasa  FROM ttCCit WHERE CCNO = xCCNo AND CCJasa > 0
	UNION
	SELECT xNoGL, DATE(@CCTgl), REPLACE(SDNo,'SD','SV') AS SVNo, '11060302', 'Piutang', CCPart  FROM ttCCit WHERE CCNO = xCCNo AND CCPart > 0
	ORDER BY SVNo;

	#Memberi Nilai HPLink
	SET @CountGL = 0;
	SELECT MAX(GLLink), IFNULL(COUNT(NoGL),0) INTO @GLLink, @CountGL FROM ttglhpit WHERE NoGL = xNoGL GROUP BY NoGL;
	IF @CountGL = 0 THEN
		UPDATE ttgeneralledgerhd SET HPLink = '--' WHERE NoGL = xNoGL;
	ELSEIF @CountGL = 1 THEN
		UPDATE ttgeneralledgerhd SET HPLink = @GLLink WHERE NoGL = xNoGL;
	ELSE
		UPDATE ttgeneralledgerhd SET HPLink = 'Many' WHERE NoGL = xNoGL;
	END IF;
END$$

DELIMITER ;

