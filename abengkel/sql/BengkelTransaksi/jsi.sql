DELIMITER $$

DROP PROCEDURE IF EXISTS `jsi`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jsi`(IN xSINo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
   DECLARE MyPesanKu VARCHAR(300) DEFAULT '';
   DECLARE MyPesankuItem VARCHAR(150) DEFAULT '';

	DECLARE oPotonganHGP DOUBLE DEFAULT 0;
	DECLARE oPotonganOLI DOUBLE DEFAULT 0;
	DECLARE oNilaiHGP  DOUBLE DEFAULT 0;
	DECLARE oNilaiOLI  DOUBLE DEFAULT 0;
	DECLARE oHPPHGP  DOUBLE DEFAULT 0;
	DECLARE oHPPOLI  DOUBLE DEFAULT 0;

	DECLARE oSITotal DOUBLE DEFAULT 0;
	DECLARE oSIDiscFinal DOUBLE DEFAULT 0;
	DECLARE oSIUangMuka DOUBLE DEFAULT 0;

	SET  @NoUrutku = 0;
	
	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xSINo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;
	
	SELECT SINo, SITgl, SONo, SDNo, SIJenis, PosKode, KodeTrans, KarKode, PrgNama, MotorNoPolisi, LokasiKode, DealerKode, SISubTotal, SIDiscFinal, SITotalPajak, SIBiayaKirim, SIUangMuka, SITotal, SITOP, SITerbayar, SIKeterangan, NoGL, Cetak, UserID
	INTO  @SINo,@SITgl,@SONo,@SDNo,@SIJenis,@PosKode,@KodeTrans,@KarKode,@PrgNama,@MotorNoPolisi,@LokasiKode,@DealerKode,@SISubTotal,oSIDiscFinal,@SITotalPajak,@SIBiayaKirim,oSIUangMuka,oSITotal,@SITOP,@SITerbayar,@SIKeterangan,@NoGL,@Cetak,@UserID
	FROM ttSIhd	WHERE SINo = xSINo;

   SET MyPesanKu = CONCAT("Post Invoice Penjualan : " , @SINo , " - " , @KodeTrans , " - " , @DealerKode);
   SET MyPesanKu = TRIM(MyPesanKu);  IF LENGTH(MyPesanKu) > 300 THEN SET MyPesanKu = SUBSTRING(MyPesanKu,0, 299); END IF;
   IF LENGTH(MyPesanku) > 150 THEN SET MyPesankuItem = SUBSTRING(MyPesanku,0, 149); ELSE SET MyPesankuItem = MyPesanku; END IF;

	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;

/*
  SELECT  ttsiit.SINo, ttsiit.SIAuto, ttsiit.BrgKode, ttsiit.SIQty, ttsiit.SIHrgBeli, ttsiit.SIHrgJual, ttsiit.SIDiscount, ttsiit.SIPajak, tdbarang.BrgNama, tdbarang.BrgSatuan,
  IFNULL(ttsiit.SIDiscount / (ttsiit.SIHrgJual * ttsiit.SIQty) * 100, 0) AS Disc,
  ttsiit.SIQty * ttsiit.SIHrgJual - ttsiit.SIDiscount AS Jumlah,
  ttsiit.LokasiKode, ttsiit.SIPickingNo, ttsiit.SIPickingDate, tdbarang.BrgGroup, ttsiit.SONo, ttsiit.Cetak, tdbarang.BrgRak1
  FROM  ttsiit INNER JOIN tdbarang ON ttsiit.BrgKode = tdbarang.BrgKode
  WHERE (ttsiit.SINo = xSINo)
  ORDER BY ttsiit.SINo, ttsiit.SIAuto
*/

	SELECT IFNULL(SUM(SIDiscount),0) INTO oPotonganHGP FROM ttsiit
	INNER JOIN tdbarang ON ttsiit.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) <> 'OLI' AND ttsiit.SINo = xSINo ;
	SELECT IFNULL(SUM(SIDiscount),0) INTO oPotonganOLI FROM ttsiit
	INNER JOIN tdbarang ON ttsiit.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) = 'OLI' AND ttsiit.SINo = xSINo ;
	SELECT IFNULL(SUM(ttsiit.SIQty * ttsiit.SIHrgJual - ttsiit.SIDiscount),0) INTO oNilaiHGP FROM ttsiit
	INNER JOIN tdbarang ON ttsiit.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) <> 'OLI' AND ttsiit.SINo = xSINo ;
	SELECT IFNULL(SUM(ttsiit.SIQty * ttsiit.SIHrgJual - ttsiit.SIDiscount),0) INTO oNilaiOLI FROM ttsiit
	INNER JOIN tdbarang ON ttsiit.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) = 'OLI' AND ttsiit.SINo = xSINo ;
	SELECT IFNULL(SUM(ttsiit.SIQty * ttsiit.SIHrgBeli),0) INTO oHPPHGP FROM ttsiit
	INNER JOIN tdbarang ON ttsiit.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) <> 'OLI' AND ttsiit.SINo = xSINo ;
	SELECT IFNULL(SUM(ttsiit.SIQty * ttsiit.SIHrgBeli),0) INTO oHPPOLI FROM ttsiit
	INNER JOIN tdbarang ON ttsiit.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) = 'OLI' AND ttsiit.SINo = xSINo ;

	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL; DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;DELETE FROM ttglhpit WHERE NoGL = xNoGL;
	DELETE FROM ttgeneralledgerhd WHERE GLLink = xSINo;

	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, DATE(@SITgl), 'SI', MyPesanKu, 0, 0, xSINo, xUserID, '--', xGLValid);

	#Item
	IF oSITotal > 0 THEN
		CASE @KodeTrans
			WHEN "TC12" THEN INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SITgl), (@NoUrutku:=@NoUrutku + 1),'11060100',MyPesankuItem, oSITotal,0;
			WHEN "TC15" THEN INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SITgl), (@NoUrutku:=@NoUrutku + 1),'60270200',MyPesankuItem, HPPHGP + HPPOLI,0;
			WHEN "TC17" THEN
				SELECT DealerNama INTO @DealerNama FROM tddealer WHERE DealerKode = @DealerKode;
				SELECT NoAccount INTO @PiutAfiliasi FROM traccount
				WHERE NamaAccount LIKE CONCAT('%',SUBSTRING_INDEX(TRIM(@DealerNama), ' ', -1)) AND JenisAccount ='Detail' AND NoParent = '11070700';
				IF @PiutAfiliasi IS NULL THEN SET @PiutAfiliasi = '11070701'; END IF;
				INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SITgl), (@NoUrutku:=@NoUrutku + 1),@PiutAfiliasi,MyPesankuItem, oSITotal,0;
			WHEN "TC18" THEN INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SITgl), (@NoUrutku:=@NoUrutku + 1),'24020102',MyPesankuItem, HPPHGP + HPPOLI,0;
			WHEN "TC20" THEN INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SITgl), (@NoUrutku:=@NoUrutku + 1),'11060401',MyPesankuItem, oSITotal,0;
			WHEN "TC21" THEN INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SITgl), (@NoUrutku:=@NoUrutku + 1),'11060402',MyPesankuItem, oSITotal,0;
			WHEN "TC22" THEN INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SITgl), (@NoUrutku:=@NoUrutku + 1),'11060500',MyPesankuItem, oSITotal,0;
			WHEN "TC99" THEN INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SITgl), (@NoUrutku:=@NoUrutku + 1),'60999000',MyPesankuItem, HPPHGP + HPPOLI,0;
		END CASE;
	END IF;

	IF oSIUangMuka > 0 THEN
		IF @SONo = '--' OR @SONo = '' OR @SONo IS NULL THEN
			INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SITgl),(@NoUrutku:=@NoUrutku + 1),'24052000',MyPesankuItem, oSIUangMuka,0;
		ELSE
			INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SITgl),(@NoUrutku:=@NoUrutku + 1),'24052200',MyPesankuItem, oSIUangMuka,0;
		END IF;
	END IF;
	IF oPotonganHGP > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SITgl),(@NoUrutku:=@NoUrutku + 1),'30090102',MyPesankuItem, oPotonganHGP,0;
	END IF;
	IF oPotonganOLI > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SITgl),(@NoUrutku:=@NoUrutku + 1),'30090202',MyPesankuItem, oPotonganOLI,0;
	END IF;

	IF (oNilaiHGP + oPotonganHGP) > 0 THEN
		IF @KodeTrans <> 'TC15' AND @KodeTrans <> 'TC99' AND @KodeTrans <> 'TC18' THEN
			INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SITgl),(@NoUrutku:=@NoUrutku + 1),'30050200',MyPesankuItem, 0,oNilaiHGP + oPotonganHGP;
		END IF;
	END IF;
	IF (oNilaiOLI + oPotonganOLI) > 0 THEN
		IF @KodeTrans <> 'TC15' AND @KodeTrans <> 'TC99' AND @KodeTrans <> 'TC18' THEN
			INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SITgl),(@NoUrutku:=@NoUrutku + 1),'30060200',MyPesankuItem, 0,oNilaiOLI + oPotonganOLI;
		END IF;
	END IF;

	IF oHPPHGP > 0 THEN
		IF @KodeTrans <> "TC15" AND @KodeTrans <> "TC99" AND @KodeTrans <> "TC18" THEN
			INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SITgl),(@NoUrutku:=@NoUrutku + 1),'50010200',MyPesankuItem, oHPPHGP,0;
		END IF;
	END IF;
	IF oHPPOLI > 0 THEN
		IF @KodeTrans <> "TC15" AND @KodeTrans <> "TC99" AND @KodeTrans <> "TC18" THEN
			INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SITgl),(@NoUrutku:=@NoUrutku + 1),'50020200',MyPesankuItem, oHPPOLI,0;
		END IF;
	END IF;
	IF oHPPHGP > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SITgl),(@NoUrutku:=@NoUrutku + 1),'11150602',MyPesankuItem, 0, oHPPHGP;
	END IF;
	IF oHPPOLI > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SITgl),(@NoUrutku:=@NoUrutku + 1),'11150602',MyPesankuItem, 0, oHPPOLI;
	END IF;

	IF oSIDiscFinal > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SITgl),(@NoUrutku:=@NoUrutku + 1),'30090400',MyPesankuItem, oSIDiscFinal,0;
	END IF;

	#Menghitung Total Debet & Kredit
	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	#Update NoGL link di table transaksi
	UPDATE ttSIhd SET NoGL = xNoGL  WHERE SINo = xSINo;

	#Link Piutang Titipan Konsumen
	DELETE FROM ttglhpit WHERE NoGL = xNoGL;
	DROP TEMPORARY TABLE IF EXISTS tvSO; CREATE TEMPORARY TABLE tvSO AS
	SELECT SONo FROM ttsiit WHERE SINO = xSINo GROUP BY SONo;
	INSERT INTO ttglhpit SELECT xNoGL, DATE(@SITgl), KMNo, '24052200', 'Hutang', KMBayar FROM ttkmit WHERE KMLink IN (SELECT SONo FROM tvSO);
	DROP TEMPORARY TABLE IF EXISTS tvSO;
  
	#Memberi Nilai HPLink
	SET @CountGL = 0;
	SELECT MAX(GLLink), IFNULL(COUNT(NoGL),0) INTO @GLLink, @CountGL FROM ttglhpit WHERE NoGL = xNoGL GROUP BY NoGL;
	IF @CountGL = 0 THEN
		UPDATE ttgeneralledgerhd SET HPLink = '--' WHERE NoGL = xNoGL;
	ELSEIF @CountGL = 1 THEN
		UPDATE ttgeneralledgerhd SET HPLink = @GLLink WHERE NoGL = xNoGL;
	ELSE
		UPDATE ttgeneralledgerhd SET HPLink = 'Many' WHERE NoGL = xNoGL;
	END IF;
END$$

DELIMITER ;

