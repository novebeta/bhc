DELIMITER $$

DROP PROCEDURE IF EXISTS `fpshd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fpshd`(
    IN xAction VARCHAR(10),
    OUT oStatus INT,
    OUT oKeterangan TEXT,
    IN xUserID VARCHAR(15),
    IN xKodeAkses VARCHAR(15),
    IN xLokasiKode VARCHAR(15),
    INOUT xPSNo VARCHAR(10),
    IN xPSNoBaru VARCHAR(10),
    IN xPSTgl DATETIME,
    IN xPINo VARCHAR(10),
    IN xPONo VARCHAR(10),
    IN xTINo VARCHAR(10),
    IN xKodeTrans VARCHAR(4),
    IN xPosKode VARCHAR(20),
    IN xSupKode VARCHAR(15),
    IN xDealerKode VARCHAR(15),
    IN xPSNoRef VARCHAR(10),
    IN xPSSubTotal DECIMAL(14, 2),
    IN xPSDiscFinal DECIMAL(14, 2),
    IN xPSTotalPajak DECIMAL(14, 2),
    IN xPSBiayaKirim DECIMAL(14, 2),
    IN xPSTotal DECIMAL(14, 2),
    IN xPSTerbayar DECIMAL(14, 2),
    IN xPSKeterangan VARCHAR(150),
    IN xCetak VARCHAR(10),
    IN xNoGL VARCHAR(10))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			DELETE FROM ttpshd WHERE (PSNo LIKE '%=%') AND PSTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);	
   		#ReJurnal Transaksi Tidak Memiliki Jurnal
			cek01: BEGIN
   			DECLARE rPSNo VARCHAR(100) DEFAULT '';
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT PSNo FROM ttPShd
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttPShd.PSNo 
				WHERE ttgeneralledgerhd.NoGL IS NULL AND PSTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rPSNo;
					IF rPSNo IS NOT NULL AND rPSNo <> '' THEN
						CALL jPS(rPSNo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;			
         CALL cUPDATEtdbaranglokasi(); 
			SET oStatus = 0; SET oKeterangan = CONCAT("Daftar Penerimaan Barang [PS] ");
		END;
		WHEN "Load" THEN
		BEGIN
			 SET oStatus = 0; SET oKeterangan = CONCAT("Load data Penerimaan Barang [PS] No: ", xPSNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			 SET @AutoNo = CONCAT('PS',RIGHT(YEAR(NOW()),2),'-');
			 SELECT CONCAT(@AutoNo,LPAD((RIGHT(PSNo,5)+1),5,0)) INTO @AutoNo FROM ttpshd WHERE ((PSNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(PSNo) = 10) ORDER BY PSNo DESC LIMIT 1;
			 IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('PS',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			 SET xPSNo = @AutoNo;
			 SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Penerimaan Barang [PS] No: ",xPSNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @PSNoOLD = xPSNo;	
					
			IF LOCATE('=',xPSNo) <> 0  THEN 
				SET @AutoNo = CONCAT('PS',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(PSNo,5)+1),5,0)) INTO @AutoNo FROM ttpshd WHERE ((PSNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(PSNo) = 10) ORDER BY PSNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('PS',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xPSNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xPSNoBaru = xPSNo;
			END IF;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xPSNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xPSNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
					
			IF oStatus = 0 THEN 
				UPDATE ttpshd
				SET PSNo = xPSNoBaru, PSTgl = xPSTgl, LokasiKode = xLokasiKode, PINo = xPINo, PONo = xPONo, TINo = xTINo, KodeTrans = xKodeTrans, PosKode = xPosKode, SupKode = xSupKode, DealerKode = xDealerKode, PSNoRef = xPSNoRef, PSSubTotal = xPSSubTotal, PSDiscFinal = xPSDiscFinal, PSTotalPajak = xPSTotalPajak, PSBiayaKirim = xPSBiayaKirim, PSTotal = xPSTotal, PSTerbayar = xPSTerbayar, PSKeterangan = xPSKeterangan, NoGL = xNoGL, Cetak = xCetak, UserID = xUserID
				WHERE PSNo = xPSNo;
				SET xPSNo = xPSNoBaru;
				
				#AutoNumber
				UPDATE ttPSit SET PSAuto = -(3*PSAuto) WHERE PSNo = xPSNo;
				SET @row_number = 0;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;
				CREATE TEMPORARY TABLE IF NOT EXISTS TNoUrut AS
				SELECT PSNo, PSAuto,(@row_number:=@row_number + 1) AS NoUrut , BrgKode FROM ttPSit
				WHERE PSNo = xPSNo ORDER BY PSAuto DESC;
				UPDATE ttPSit INNER JOIN TNoUrut
				ON  ttPSit.PSNo = TNoUrut.PSNo
				AND ttPSit.BrgKode = TNoUrut.BrgKode
				AND ttPSit.PSAuto = TNoUrut.PSAuto
				SET ttPSit.PSAuto = TNoUrut.NoUrut;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;				

            #Hitung SubTotal , Total
            SELECT SUM((PSQty * PSHrgJual) - PSDiscount) INTO @SubTotal FROM ttpsit WHERE PSNo = xPSNo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttpshd set PSSubTotal = @SubTotal, PSTotal = (@SubTotal - PSDiscFinal -  PSTotalPajak - PSBiayaKirim) WHERE PSNo = xPSNo;
            END IF;
				
				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @PSNoOLD; #Hapus Jurnal No Transaksi Lama
				CALL jps(xPSNo,xUserID); 

  				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Penerimaan Barang [PS]','ttpshd', xPRNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Penerimaan Barang [PS] No: ", xPINo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Penerimaan Barang [PS]','ttpshd', xPINo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Penerimaan Barang [PS] No: ", xPINo);
				END IF;
			ELSEIF oStatus = 1 THEN
            IF LOCATE('=',xPSNo) <> 0 THEN
            	DELETE FROM ttpshd WHERE PSNo = xPSNo;
            	SELECT IFNULL(PSNo,'--') INTO xPSNo FROM ttPShd WHERE PSNo NOT LIKE '%=%' AND LEFT(PSNo,2) = 'PS' ORDER BY PSTgl DESC LIMIT 1; 
            	IF xPSNo IS NULL OR LOCATE('=',xPSNo) <> 0 THEN SET xPSNo = '--'; END IF;
            END IF;
            SET oKeterangan = oKeterangan;
			END IF;		
		END;
		WHEN "Delete" THEN
		BEGIN	
			SET oKeterangan = "";
			SET oStatus = 0;

			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xPSNo;
			IF @GLValid = 'Sudah' THEN	
				SET oStatus = 1; SET oKeterangan = CONCAT("No PS:", xPSNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			IF oStatus = 0 THEN
				DELETE FROM ttpshd WHERE PSNo = xPSNo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xPSNo;
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Penerimaan Barang [PS] No: ",xPSNo);
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Penerimaan Barang [PS]','ttpshd', xPRNo); 			
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;	
		END;
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xPSNo) <> 0 THEN
				DELETE FROM ttpshd WHERE PSNo = xPSNo;
				SET oKeterangan = CONCAT("Batal menyimpan data Penerimaan Barang [PS] No: ", xPSNoBaru);
				SELECT IFNULL(PSNo,'--') INTO xPSNo FROM ttPShd WHERE PSNo NOT LIKE '%=%' AND LEFT(PSNo,2) = 'PS' ORDER BY PSTgl DESC LIMIT 1; 
				IF xPSNo IS NULL OR LOCATE('=',xPSNo) <> 0 THEN SET xPSNo = '--'; END IF;
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Penerimaan Barang [PS] No: ", xPSNo);
			END IF;
		END;
		WHEN "Jurnal" THEN
		BEGIN
			 IF LOCATE('=',xPSNo) <> 0 THEN 
				  SET oStatus = 1;
				  SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			 ELSE
				  CALL jps(xPSNo,xUserID);
				  SET oStatus = 0;
				  SET oKeterangan = CONCAT("Berhasil memproses Jurnal Penerimaan Barang [PS] No: ", xPSNo);
			 END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
            if xCetak = "TidakAda" Then
                UPDATE ttpohd SET PSNo = xPSNo WHERE PONo = xPONo;
                Update ttpshd set PONo = xPONo where PSNo = xPSNo;

                SET oStatus = 0;
                set oKeterangan = "";
            end if;
            if xCetak = "AdaNo" Then
                UPDATE ttpohd SET PSNo = '--' WHERE PONo IN (SELECT  PONo FROM ttPSit WHERE PSNo = xPSNo GROUP BY PONO);
                DELETE FROM ttPSit WHERE PSNo = xPSNo;

                UPDATE ttpohd SET PSNo = xPSNo WHERE PONo = xPONo;
                Update ttpshd set PONo = xPONo where PSNo = xPSNo;

                SET oStatus = 0;
                set oKeterangan = "";
            end if;
            If xCetak = "AdaYes" Then
                SELECT  IFNULL(LokasiKode,'--') into @A FROM ttpsit WHERE PSNo = xPSNo AND LokasiKode = xLokasiKode;
                if isnull(@A) Then
                    set oStatus = 1;
                    set oKeterangan = Concat("Kode Lokasi dari item sebelum ini tidak sama dengan Lokasi sekarang ",xLokasiKode);
                else
                    set oStatus = 0;
                    set oKeterangan = "";
                    UPDATE ttpohd SET PSNo = xPSNo WHERE PONo = xPONo;
                    Update ttpshd set PONo = xPONo where PSNo = xPSNo;
                end if;
            end if;
		END;
	END CASE;
END$$

DELIMITER ;


