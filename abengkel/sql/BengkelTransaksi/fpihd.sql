DELIMITER $$

DROP PROCEDURE IF EXISTS `fpihd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fpihd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xPINo VARCHAR(10),
IN xPINoBaru VARCHAR(10),
IN xPITgl DATETIME,
IN xPIJenis VARCHAR(6),
IN xPSNo VARCHAR(10),
IN xKodeTrans VARCHAR(4),
IN xPosKode VARCHAR(20),
IN xSupKode VARCHAR(15),
IN xPINoRef VARCHAR(35),
IN xPITerm DECIMAL(3, 0),
IN xPITglTempo DATE,
IN xPISubTotal DECIMAL(14, 2),
IN xPIDiscFinal DECIMAL(14, 2),
IN xPITotalPajak DECIMAL(14, 2),
IN xPIBiayaKirim DECIMAL(14, 2),
IN xPITotal DECIMAL(14, 2),
IN xPITerbayar DECIMAL(14, 2),
IN xPIKeterangan VARCHAR(150),
IN xNoGL VARCHAR(10),
IN xCetak VARCHAR(5))

BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
         DELETE FROM ttpihd WHERE (PINo LIKE '%=%') AND PITgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);	

			#ReJurnal Transaksi Tidak Memiliki Jurnal
			cek01: BEGIN
   			DECLARE rPINo VARCHAR(100) DEFAULT '';
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT PINo FROM ttPIhd
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttPIhd.PINo 
				WHERE ttgeneralledgerhd.NoGL IS NULL AND PITgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rPINo;
					IF rPINo IS NOT NULL AND rPINo <> '' THEN
						CALL jPI(rPINo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;	
         CALL cUPDATEtdbaranglokasi(); 	
			SET oStatus = 0; SET oKeterangan = CONCAT("Daftar Invoice Pembelian [PI] ");
		END;
		WHEN "Load" THEN
		BEGIN
			 SET oStatus = 0; SET oKeterangan = CONCAT("Load data Invoice Pembelian [PI] No: ", xPINo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SET @AutoNo = CONCAT('PI',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(PINo,5)+1),5,0)) INTO @AutoNo FROM ttpihd WHERE ((PINo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(PINo) = 10) ORDER BY PINo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('PI',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xPINo = @AutoNo;
			SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Invoice Pembelian [PI] No: ",xPINo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @PINoOLD = xPINo;	

			#AutoNumber
			IF LOCATE('=',xPINo) <> 0 THEN
				SET @AutoNo = CONCAT('PI',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(PINo,5)+1),5,0)) INTO @AutoNo FROM ttpihd WHERE ((PINo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(PINo) = 10) ORDER BY PINo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('PI',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xPINoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xPINoBaru = xPINo;
			END IF;
						
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xPINo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xPINo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN 
				UPDATE ttpihd
				SET PINo = xPINoBaru, PITgl = xPITgl, LokasiKode = xLokasiKode, PIJenis = xPIJenis, PSNo = xPSNo, KodeTrans = xKodeTrans, PosKode = xPosKode, SupKode = xSupKode, PINoRef = xPINoRef, PITerm = xPITerm, PITglTempo = xPITglTempo, PISubTotal = xPISubTotal, PIDiscFinal = xPIDiscFinal, PITotalPajak = xPITotalPajak, PIBiayaKirim = xPIBiayaKirim, PITotal = xPITotal, PITerbayar = xPITerbayar, PIKeterangan = xPIKeterangan, NoGL = xNoGL, Cetak = xCetak, UserID = xUserID
				WHERE PINo = xPINo;
				SET xPINo = xPINoBaru;

				#AutoNumber
				UPDATE ttpiit SET PIAuto = -(3*PIAuto) WHERE PINo = xPINo;
				SET @row_number = 0;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;
				CREATE TEMPORARY TABLE IF NOT EXISTS TNoUrut AS
				SELECT PINo, PIAuto,(@row_number:=@row_number + 1) AS NoUrut , BrgKode FROM ttpiit
				WHERE PINo = xPINo ORDER BY PIAuto DESC;
				UPDATE ttpiit INNER JOIN TNoUrut
				ON  ttpiit.PINo = TNoUrut.PINo
				AND ttpiit.BrgKode = TNoUrut.BrgKode
				AND ttpiit.PIAuto = TNoUrut.PIAuto
				SET ttpiit.PIAuto = TNoUrut.NoUrut;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;		
            
            #Hitung SubTotal & Total
            SELECT SUM((PIQty * PIHrgJual) - PIDiscount) INTO @SubTotal FROM ttPIit WHERE PINo = xPINo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttPIhd set PISubTotal = @SubTotal, PITotal = (@SubTotal - PIDiscFinal -  PITotalPajak - PIBiayaKirim) WHERE PINo = xPINo;
            END IF;
					
				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @PINoOLD; #Hapus Jurnal No Transaksi Lama
				CALL jpi(xPINo,xUserID); 
			  
  				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Invoice Pembelian [PI]','ttpihd', xPINo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Invoice Pembelian [PI] No: ", xPINo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Invoice Pembelian [PI]','ttpihd', xPINo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Invoice Pembelian [PI] No: ", xPINo);
				END IF;
				
			ELSEIF oStatus = 1 THEN
            IF LOCATE('=',xPINo) <> 0 THEN
            	DELETE FROM ttpihd WHERE PINo = xPINo;
            	SELECT IFNULL(PINo,'--') INTO xPINo FROM ttPIhd WHERE PINo NOT LIKE '%=%' AND LEFT(PINo,2) = 'PI' ORDER BY PITgl DESC LIMIT 1; 
            	IF xPINo IS NULL OR LOCATE('=',xPINo) <> 0 THEN SET xPINo = '--'; END IF;
            END IF;
			  SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;

 			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xPINo;
			IF @GLValid = 'Sudah' THEN	
				SET oStatus = 1; SET oKeterangan = CONCAT("No PI:", xPINo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM ttpihd WHERE PINo = xPINo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xPINo;
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Invoice Pembelian [PI] No: ",xPINo);
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Invoice Pembelian [PI]','ttpihd', xPINo); 			
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;		
		END;
		WHEN "Loop" THEN
		BEGIN
            SELECT PSNo into @A FROM ttpshd WHERE PINo = xPINo AND PSNo <> xPSNo;
            if isnull(@A) Then
                UPDATE ttpshd SET PINo = '--' WHERE PSNo IN (SELECT  PSNo FROM ttpiit WHERE PINo = xPINo GROUP BY PSNO);
                DELETE FROM ttpiit WHERE PINo = xPINo;
            end if;
            UPDATE ttpshd SET PINo = xPINo WHERE PSNo = xPSNo;
            update ttpihd set PSNo = xPSNo where PINo = xPINo;
		END;
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xPINo) <> 0 THEN
				DELETE FROM ttpihd WHERE PINo = xPINo;
				SET oKeterangan = CONCAT("Batal menyimpan data Invoice Pembelian [PI] No: ", xPINoBaru);
				SELECT IFNULL(PINo,'--') INTO xPINo FROM ttPIhd WHERE PINo NOT LIKE '%=%' AND LEFT(PINo,2) = 'PI' ORDER BY PITgl DESC LIMIT 1; 
				IF xPINo IS NULL OR LOCATE('=',xPINo) <> 0 THEN SET xPINo = '--'; END IF;
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Invoice Pembelian [PI] No: ", xPINo);
			END IF;
		END;
		WHEN "Jurnal" THEN
		BEGIN
			 IF LOCATE('=',xPINo) <> 0 THEN 
				  SET oStatus = 1; SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			 ELSE
				  CALL jpi(xPINo,xUserID);
				  SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil memproses Jurnal Invoice Pembelian [PI] No: ", xPINo);
			 END IF;
		END;
	END CASE;
END$$

DELIMITER ;

