DELIMITER $$

DROP PROCEDURE IF EXISTS `fsoit`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fsoit`(
IN xAction VARCHAR(150),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xSONo VARCHAR(10),
IN xSOAuto SMALLINT(6),
IN xBrgKode VARCHAR(20),
IN xSOQty DECIMAL(5,0),
IN xSOHrgBeli DECIMAL(14,2),
IN xSOHrgJual DECIMAL(14,2),
IN xSODiscount DECIMAL(14,2),
IN xSOPajak DECIMAL(2,0),
IN xSINo VARCHAR(10),
IN xPONo VARCHAR(10),
IN xSIQtyS DECIMAL(5,0),
IN xPOQtyS DECIMAL(5,0),
IN xSONoOLD VARCHAR(10),
IN xSOAutoOLD SMALLINT(6),
IN xBrgKodeOLD VARCHAR(20)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;

			#Mengambil Harga Beli
			SELECT BrgHrgBeli INTO xSOHrgBeli FROM tdbarang WHERE BrgKode = xBrgKode;

			SELECT if(isnull(MAX(SOAuto)+1),1,MAX(SOAuto)+1) INTO xSOAuto FROM ttsoit WHERE SONo = xSONo; #AutoN
			INSERT INTO ttsoit
			(SONo, SOAuto, BrgKode, SOQty, SOHrgBeli, SOHrgJual, SODiscount, SOPajak, SINo, PONo, SIQtyS, POQtyS) VALUES
			(xSONo, xSOAuto, xBrgKode, xSOQty, xSOHrgBeli, xSOHrgJual, xSODiscount, xSOPajak, xSINo, xPONo, xSIQtyS, xPOQtyS);

         #Hitung SubTotal & Total
         SELECT SUM((SOQty * SOHrgJual) - SODiscount) INTO @SubTotal FROM ttSOit WHERE SONo = xSONo;
         IF @Subtotal IS NOT NULL THEN 
            UPDATE ttSOhd SET SOSubTotal = @SubTotal, SOTotal = (@SubTotal - SODiscFinal -  SOTotalPajak - SOBiayaKirim) WHERE SONo = xSONo;
         END IF;

			SET oKeterangan = CONCAT('Berhasil menambahkan data Barang ', xBrgKode, ' = ' , FORMAT(xSOQty,0));
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;

			#Jika Beda Kode Barang, ambil Harga Beli
			IF xBrgKode <> xBrgKodeOLD THEN
				SELECT BrgHrgBeli INTO xSOHrgBeli FROM tdbarang WHERE BrgKode = xBrgKode;
			END IF;

			IF oStatus = 0 THEN
				UPDATE ttsoit
				SET SONo = xSONo, SOAuto = xSOAuto, BrgKode = xBrgKode, SOQty = xSOQty, SOHrgBeli = xSOHrgBeli, SOHrgJual = xSOHrgJual, SODiscount = xSODiscount, SOPajak = xSOPajak, SINo = xSINo, PONo = xPONo, SIQtyS = xSIQtyS, POQtyS = xPOQtyS
				WHERE SONo = xSONoOLD AND SOAuto = xSOAutoOLD AND xBrgKode = xBrgKodeOLD;

            #Hitung SubTotal & Total
            SELECT SUM((SOQty * SOHrgJual) - SODiscount) INTO @SubTotal FROM ttSOit WHERE SONo = xSONo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttSOhd SET SOSubTotal = @SubTotal, SOTotal = (@SubTotal - SODiscFinal -  SOTotalPajak - SOBiayaKirim) WHERE SONo = xSONo;
            END IF;

				SET oKeterangan = CONCAT("Berhasil mengubah data Barang : ",  xBrgKode, ' = ' , FORMAT(xSOQty,0));
			ELSE
				  SET oKeterangan = CONCAT("Gagal mengubah data Barang : ", xBrgKode, ' = ' , FORMAT(xSOQty,0));
			END IF;			
		END;
		WHEN "Delete" THEN
		BEGIN
         SET oStatus = 0;
         IF oStatus = 0 THEN
            DELETE FROM ttsoit WHERE SOAuto = xSOAuto AND SONo = xSONo  AND BrgKode = xBrgKode;
         
            #Hitung SubTotal & Total
            SELECT SUM((SOQty * SOHrgJual) - SODiscount) INTO @SubTotal FROM ttSOit WHERE SONo = xSONo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttSOhd SET SOSubTotal = @SubTotal, SOTotal = (@SubTotal - SODiscFinal -  SOTotalPajak - SOBiayaKirim) WHERE SONo = xSONo;
            END IF;
         
            SET oKeterangan = CONCAT("Berhasil menghapus data Barang : ",  xBrgKode, ' = ' , FORMAT(xSOQty,0));
         ELSE
            SET oKeterangan = CONCAT("Gagal menghapus data Barang : ",  xBrgKode, ' = ' , FORMAT(xSOQty,0));
         END IF;
		END;
	END CASE;
END$$

DELIMITER ;

