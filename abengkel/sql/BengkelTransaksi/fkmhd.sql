DELIMITER $$

DROP PROCEDURE IF EXISTS `fkmhd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fkmhd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xKMNo VARCHAR(12),
IN xKMNoBaru VARCHAR(12),
IN xKMTgl DATETIME,
IN xKMJenis VARCHAR(20),
IN xKodeTrans VARCHAR(4),
IN xPosKode VARCHAR(20),
IN xMotorNoPolisi VARCHAR(12),
IN xKasKode VARCHAR(25),
IN xKMPerson VARCHAR(50),
IN xKMBayarTunai DECIMAL(14, 2),
IN xKMNominal DECIMAL(14, 2),
IN xKMMemo VARCHAR(150),
IN xNoGL VARCHAR(10),
IN xCetak VARCHAR(5)
)
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			DELETE FROM ttKMhd WHERE (KodeTrans ='' OR KodeTrans ='--' OR KMNo LIKE '%=%') 
			AND KMTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
			#ReJurnal Transaksi Tidak Memiliki Jurnal
			cek01: BEGIN
				DECLARE rKMNo VARCHAR(100) DEFAULT '';
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT KMNo FROM ttKMhd
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttKMhd.KMNo 
				WHERE ttgeneralledgerhd.NoGL IS NULL AND KMNo NOT LIKE '%=%' AND KMTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rKMNo;
					IF rKMNo IS NOT NULL AND rKMNo <> '' THEN
						CALL jKM(rKMNo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;		

			SET oStatus = 0; SET oKeterangan = CONCAT("Daftar Kas Masuk ");
		END;
		WHEN "Load" THEN
		BEGIN
			 SET oStatus = 0; SET oKeterangan = CONCAT("Load data Kas Masuk No: ", xKMNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SELECT PosNomor INTO @PosNomor FROM tdpos WHERE PosKode = xPosKode;
			SET @AutoNo = CONCAT('KM',@PosNomor, RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(KMNo,5)+1),5,0)) INTO @AutoNo FROM ttkmhd WHERE ((KMNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(KMNo) = 12) ORDER BY KMNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 12 THEN SET @AutoNo = CONCAT('KM',@PosNomor,RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xKMNo = @AutoNo;
			SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Kas Masuk No: ",xKMNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @KMNoOLD = xKMNo;		
		
			IF LOCATE('=',xKMNo) <> 0  THEN 
				SELECT PosNomor INTO @PosNomor FROM tdpos WHERE PosKode = xPosKode;
				SET @AutoNo = CONCAT('KM',@PosNomor, RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(KMNo,5)+1),5,0)) INTO @AutoNo FROM ttkmhd WHERE ((KMNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(KMNo) = 12) ORDER BY KMNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 12 THEN SET @AutoNo = CONCAT('KM',@PosNomor,RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xKMNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xKMNoBaru = xKMNo;			
			END IF;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN 
				UPDATE ttkmhd
				SET KMNo = xKMNoBaru, KMTgl = xKMTgl, KMJenis = xKMJenis, KodeTrans = xKodeTrans, PosKode = xPosKode, MotorNoPolisi = xMotorNoPolisi, 
				KasKode = xKasKode, KMPerson = xKMPerson, KMBayarTunai = xKMBayarTunai, KMNominal = xKMNominal, KMMemo = xKMMemo, NoGL = xNoGL, Cetak = xCetak, UserID = xUserID
				WHERE KMNo = xKMNo;
				SET xKMNo = xKMNoBaru;

				#KMAutoN
				IF xKodeTrans = 'UM' THEN
					UPDATE ttKMitcoa SET KMAutoN = -(3*KMAutoN) WHERE KMNo = xKMNo;
					SET @row_number = 0;
					DROP TEMPORARY TABLE IF EXISTS TNoUrut;
					CREATE TEMPORARY TABLE IF NOT EXISTS TNoUrut AS
					SELECT KMNo, KMAutoN,(@row_number:=@row_number + 1) AS NoUrut , NoAccount FROM ttKMitcoa
					WHERE KMNo = xKMNo ORDER BY KMAutoN DESC;
					UPDATE ttKMitcoa INNER JOIN TNoUrut
					ON  ttKMitcoa.KMNo = TNoUrut.KMNo
					AND ttKMitcoa.NoAccount = TNoUrut.NoAccount
					AND ttKMitcoa.KMAutoN = TNoUrut.KMAutoN
					SET ttKMitcoa.KMAutoN = TNoUrut.NoUrut;
					DROP TEMPORARY TABLE IF EXISTS TNoUrut;		
				END IF;

				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @KMNoOLD; #Hapus Jurnal No Transaksi Lama
				CALL jKM(xKMNo,xUserID); #Posting Jurnal
			
				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Kas Masuk','ttkmhd', xKMNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Kas Masuk No: ", xKMNo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Kas Masuk','ttkmhd', xKMNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Kas Masuk No: ", xKMNo);
				END IF;	

			ELSEIF oStatus = 1 THEN
				IF LOCATE('=',xKMNo) <> 0 THEN
					DELETE FROM ttkmhd WHERE KMNo = xKMNo;
					SELECT IFNULL(PosNomor,'01') INTO @PosNomor FROM tdpos WHERE PosKode = xLokasiKode; IF @PosNomor IS NULL THEN SET @PosNomor = '01'; END IF;
					SELECT IFNULL(KMNo,'--') INTO xKMNo FROM ttKMhd WHERE KMNo NOT LIKE '%=%' AND LEFT(KMNo,4) = CONCAT('KM',@PosNomor) AND KodeTrans = xKodeTrans ORDER BY KMTgl DESC LIMIT 1; 
					IF xKMNo IS NULL OR LOCATE('=',xKMNo) <> 0 THEN SET xKMNo = '--'; END IF;
				END IF;			
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;

			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKMNo;
			IF @GLValid = 'Sudah' THEN	
				SET oStatus = 1; SET oKeterangan = CONCAT("No KM:", xKMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM ttkmhd WHERE KMNo = xKMNo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xKMNo;
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Kas Masuk No: ",xKMNo);
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Kas Masuk','ttkmhd', xKMNo); 			
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xKMNo) <> 0 THEN
				DELETE FROM ttkmhd WHERE KMNo = xKMNo;
				SET oKeterangan = CONCAT("Batal menyimpan data Kas Masuk No: ", xKMNoBaru);
				SELECT IFNULL(PosNomor,'01') INTO @PosNomor FROM tdpos WHERE PosKode = xLokasiKode; IF @PosNomor IS NULL THEN SET @PosNomor = '01'; END IF;
				SELECT IFNULL(KMNo,'--') INTO xKMNo FROM ttKMhd WHERE KMNo NOT LIKE '%=%' AND LEFT(KMNo,4) = CONCAT('KM',@PosNomor) AND KodeTrans = xKodeTrans ORDER BY KMTgl DESC LIMIT 1; 
				IF xKMNo IS NULL OR LOCATE('=',xKMNo) <> 0 THEN SET xKMNo = '--'; END IF;
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Kas Masuk No: ", xKMNo);
			END IF;
		END;
		WHEN "Jurnal" THEN
		BEGIN
			IF LOCATE('=',xKMNo) <> 0 THEN 
				SET oStatus = 1; SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			ELSE
				CALL jkm(xKMNo,xUserID);
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil memproses Jurnal Kas Masuk No: ", xKMNo);
			END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
		END;
		WHEN "LoopAfter" THEN
		BEGIN
			SELECT SUM(KMBayar) INTO @sumKMBayar FROM ttKMit WHERE KMNo = xKMNo;
			IF @sumKMBayar IS NOT NULL THEN
				UPDATE ttKMhd SET KMNominal = @sumKMBayar WHERE KMNo = xKMNo AND KMNominal <= @sumKMBayar;
			END IF;		
		END;		
	END CASE;
END$$

DELIMITER ;

