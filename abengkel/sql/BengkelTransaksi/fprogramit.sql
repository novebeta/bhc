DELIMITER $$

DROP PROCEDURE IF EXISTS `fprogramit`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fprogramit`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xPrgNama VARCHAR(25),
IN xPrgAuto SMALLINT(6),
IN xBrgKode VARCHAR(20),
IN xPrgHrgJual DECIMAL(14, 2),
IN xDisc DECIMAL(5, 0),
IN xPrgDiscount DECIMAL(14, 2),
IN xxPrgNamaOLD VARCHAR(10),
IN xPrgAutoOLD SMALLINT(6),
IN xBrgKodeOLD VARCHAR(20)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;
			SELECT (MAX(PrgAuto)+1) INTO xPrgAuto FROM ttpoit WHERE PrgNama = xPrgNama; #AutoN					 
			INSERT INTO tdprogramit
			(PrgNama, PrgAuto, BrgKode, PrgHrgJual, PrgDiscount) VALUES
			(xPrgNama, xPrgAuto, xBrgKode, xPrgHrgJual, xPrgDiscount);
			SET oKeterangan = CONCAT('Berhasil menambahkan data Barang ', xBrgKode);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			IF oStatus = 0 THEN
				UPDATE tdprogramit
				SET PrgNama = xPrgNama, PrgAuto = xPrgAuto, BrgKode = xBrgKode, PrgHrgJual = xPrgHrgJual, PrgDiscount = xPrgDiscount
				WHERE PrgNama = xPrgNamaOLD AND PrgAuto = xPrgAutoOLD AND BrgKode = xBrgKodeOLD;
				SET oKeterangan = CONCAT("Berhasil mengubah data Barang : ", xBrgKode );
			ELSE
				SET oKeterangan = CONCAT("Gagal mengubah data Barang : ", xBrgKode );
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			IF oStatus = 0 THEN
				DELETE FROM tdprogramit WHERE PrgNama = xPrgNama AND PrgAuto = xPrgAuto AND BrgKode = xBrgKode;
				SET oKeterangan = CONCAT("Berhasil menghapus data Barang : ", xBrgKode );
			ELSE
				SET oKeterangan = CONCAT("Gagal menghapus data Barang : ", xBrgKode );
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			SET oStatus = 0;SET oKeterangan = CONCAT("Batal menyimpan data Barang: ", xPrgAuto );
		END;
	END CASE;
END$$

DELIMITER ;

