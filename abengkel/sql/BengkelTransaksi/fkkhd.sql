DELIMITER $$

DROP PROCEDURE IF EXISTS `fkkhd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fkkhd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xKKNo VARCHAR(12),
IN xKKNoBaru VARCHAR(12),
IN xKKTgl DATETIME,
IN xKKJenis VARCHAR(20),
IN xKodeTrans VARCHAR(4),
IN xPosKode VARCHAR(20),
IN xSupKode VARCHAR(15),
IN xKasKode VARCHAR(25),
IN xKKPerson VARCHAR(50),
IN xKKNominal DECIMAL(14, 2),
IN xKKMemo VARCHAR(150),
IN xNoGL VARCHAR(10),
IN xCetak VARCHAR(5))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			DELETE FROM ttkkhd WHERE (KKNo LIKE '%=%') AND KKTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);	

			#ReJurnal Transaksi Tidak Memiliki Jurnal
			cek01: BEGIN
				DECLARE rKKNo VARCHAR(100) DEFAULT '';
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT KKNo FROM ttKKhd
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttKKhd.KKNo 
				WHERE ttgeneralledgerhd.NoGL IS NULL AND KKTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rKKNo;
					IF rKKNo IS NOT NULL AND rKKNo <> '' THEN
						CALL jKK(rKKNo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;
			SET oStatus = 0;SET oKeterangan = CONCAT("Daftar Kas Keluar ");
		END;
		WHEN "Load" THEN
		BEGIN
			 SET oStatus = 0;SET oKeterangan = CONCAT("Load data Kas Keluar No: ", xKKNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SELECT PosNomor INTO @PosNomor FROM tdpos WHERE PosKode = xPosKode;
			SET @AutoNo = CONCAT('KK',@PosNomor, RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(KKNo,5)+1),5,0)) INTO @AutoNo FROM ttKKhd WHERE ((KKNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(KKNo) = 12) ORDER BY KKNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 12 THEN SET @AutoNo = CONCAT('KK',@PosNomor,RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xKKNo = @AutoNo;
			SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Kas Keluar No: ",xKKNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @KKNoOLD = xKKNo;	
			
			IF LOCATE('=',xKKNo) <> 0 THEN
				SELECT PosNomor INTO @PosNomor FROM tdpos WHERE PosKode = xPosKode;
				SET @AutoNo = CONCAT('KK',@PosNomor, RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(KKNo,5)+1),5,0)) INTO @AutoNo FROM ttKKhd WHERE ((KKNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(KKNo) = 12) ORDER BY KKNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 12 THEN SET @AutoNo = CONCAT('KK',@PosNomor,RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xKKNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xKKNoBaru = xKKNo;
			END IF;
					
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			# Update 
			IF oStatus = 0 THEN 			
				UPDATE ttkkhd
				SET KKNo = xKKNoBaru, KKTgl = xKKTgl, KKJenis = xKKJenis, KodeTrans = xKodeTrans, PosKode = xPosKode, SupKode = xSupKode, Kaskode = xKasKode, 
				KKPerson = xKKPerson, KKNominal = xKKNominal, KKMemo = xKKMemo, NoGL = xNoGL, Cetak = xCetak, UserID = xUserID
				WHERE KKNo = xKKNo;
				SET xKKNo = xKKNoBaru;
				
				#KKAutoN
				IF xKodeTrans = 'UM' THEN
					UPDATE ttKKitcoa SET KKAutoN = -(3*KKAutoN) WHERE KKNo = xKKNo;
					SET @row_number = 0;
					DROP TEMPORARY TABLE IF EXISTS TNoUrut;
					CREATE TEMPORARY TABLE IF NOT EXISTS TNoUrut AS
					SELECT KKNo, KKAutoN,(@row_number:=@row_number + 1) AS NoUrut , NoAccount FROM ttKKitcoa
					WHERE KKNo = xKKNo ORDER BY KKAutoN DESC;
					UPDATE ttKKitcoa INNER JOIN TNoUrut
					ON  ttKKitcoa.KKNo = TNoUrut.KKNo
					AND ttKKitcoa.NoAccount = TNoUrut.NoAccount
					AND ttKKitcoa.KKAutoN = TNoUrut.KKAutoN
					SET ttKKitcoa.KKAutoN = TNoUrut.NoUrut;
					DROP TEMPORARY TABLE IF EXISTS TNoUrut;	
				END IF;
				
				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @KKNoOLD; #Hapus Jurnal No Transaksi Lama
				CALL jkk(xKKNo,xUserID); #Posting Jurnal
				
				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Kas Keluar','ttkkhd', xKKNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Kas Keluar No: ", xKKNo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Kas Keluar','ttkkhd', xKKNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Kas Keluar No: ", xKKNo);
				END IF;		
			ELSEIF oStatus = 1 THEN
				IF LOCATE('=',xKKNo) <> 0 THEN
					DELETE FROM ttkkhd WHERE KKNo = xKKNo;
					SELECT IFNULL(PosNomor,'01') INTO @PosNomor FROM tdpos WHERE PosKode = xLokasiKode; IF @PosNomor IS NULL THEN SET @PosNomor = '01'; END IF;
					SELECT IFNULL(KKNo,'--') INTO xKKNo FROM ttKKhd WHERE KKNo NOT LIKE '%=%' AND LEFT(KKNo,4) = CONCAT('KK',@PosNomor) AND KodeTrans = xKodeTrans ORDER BY KKTgl DESC LIMIT 1; 
					IF xKKNo IS NULL OR LOCATE('=',xKKNo) <> 0 THEN SET xKKNo = '--'; END IF;
				END IF;			
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;
		
			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKKNo;
			IF @GLValid = 'Sudah' THEN	
				SET oStatus = 1; SET oKeterangan = CONCAT("No KK:", xKKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM ttkkhd WHERE KKNo = xKKNo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xKKNo;
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Kas Keluar No: ",xKKNo);
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Kas Keluar','ttkkhd', xKKNo); 			
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xKKNo) <> 0 THEN
				DELETE FROM ttkkhd WHERE KKNo = xKKNo;
				SET oKeterangan = CONCAT("Batal menyimpan data Kas Keluar No: ", xKKNoBaru);
				SELECT IFNULL(PosNomor,'01') INTO @PosNomor FROM tdpos WHERE PosKode = xLokasiKode; IF @PosNomor IS NULL THEN SET @PosNomor = '01'; END IF;
				SELECT IFNULL(KKNo,'--') INTO xKKNo FROM ttKKhd WHERE KKNo NOT LIKE '%=%' AND LEFT(KKNo,4) = CONCAT('KK',@PosNomor) AND KodeTrans = xKodeTrans ORDER BY KKTgl DESC LIMIT 1; 
				IF xKKNo IS NULL OR LOCATE('=',xKKNo) <> 0 THEN SET xKKNo = '--'; END IF;
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Kas Keluar No: ", xKKNo);
			END IF;
		END;
		WHEN "Jurnal" THEN
		BEGIN
			 IF LOCATE('=',xKKNo) <> 0 THEN 
				  SET oStatus = 1;
				  SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			 ELSE
				  CALL jkk(xKKNo,xUserID);
				  SET oStatus = 0;
				  SET oKeterangan = CONCAT("Berhasil memproses Jurnal Kas Keluar No: ", xKKNo);
			 END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
		END;
		WHEN "LoopAfter" THEN
		BEGIN
			SELECT SUM(KKBayar) INTO @sumKKBayar FROM ttKKit WHERE KKNo = xKKNo;
			IF @sumKKBayar IS NOT NULL THEN
				UPDATE ttKKhd SET KKNominal = @sumKKBayar WHERE KKNo = xKKNo AND KKNominal <= @sumKKBayar;
			END IF;		
		END;
	END CASE;
END$$

DELIMITER ;

