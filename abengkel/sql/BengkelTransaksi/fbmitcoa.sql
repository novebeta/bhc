DELIMITER $$

DROP PROCEDURE IF EXISTS `fbmitcoa`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fbmitcoa`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xBMNo VARCHAR(12),
IN xBMAutoN SMALLINT(5),
IN xNoAccount VARCHAR(10),
IN xBMKeterangan VARCHAR(150),
IN xBMBayar DECIMAL(14, 2),
IN xBMNoOLD VARCHAR(12),
IN xBMAutoNOLD SMALLINT(5),
IN xBMAccountOLD VARCHAR(10)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert
			IF oStatus = 0 THEN				
				SELECT IFNULL((MAX(BMAutoN)+1),0) INTO xBMAutoN FROM ttBMitcoa WHERE BMNo = xBMNo; #AutoN
				INSERT INTO ttBMitcoa (BMNo, BMAutoN, NoAccount, BMBayar, BMKeterangan) VALUES (xBMNo, xBMAutoN, xNoAccount, xBMBayar, xBMKeterangan);

            #Hitung Total
            SELECT SUM(BMBayar) INTO @BMBayar FROM ttbmitcoa WHERE BMNo = xBMNo; 
            IF @BMBayar IS NOT NULL THEN UPDATE ttbmhd set BMNominal = @BMBayar WHERE BMNo = xBMNo; END IF;

				SET oKeterangan = CONCAT('Berhasil menambahkan data Bank Masuk ', xBMNo,' - ', xNoAccount, ' - ', FORMAT(xBMBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				UPDATE ttBMitcoa SET BMNo = xBMNo, BMAutoN = xBMAutoN, NoAccount = xNoAccount, BMBayar = xBMBayar, BMKeterangan = xBMKeterangan
				WHERE BMNo = xBMNoOLD AND BMAutoN = xBMAutoNOLD AND NoAccount = xNoAccountOLD;

            #Hitung Total
            SELECT SUM(BMBayar) INTO @BMBayar FROM ttbmitcoa WHERE BMNo = xBMNo; 
            IF @BMBayar IS NOT NULL THEN UPDATE ttbmhd set BMNominal = @BMBayar WHERE BMNo = xBMNo; END IF;

				SET oKeterangan = CONCAT("Berhasil mengubah data Bank Masuk: ", xBMNo , ' - ' , xNoAccount);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Delete
			IF oStatus = 0 THEN
				DELETE FROM ttBMitcoa WHERE BMAutoN = xBMAutoN AND NoAccount = xNoAccount AND BMNo = xBMNo;
            
            #Hitung Total
            SELECT SUM(BMBayar) INTO @BMBayar FROM ttbmitcoa WHERE BMNo = xBMNo; 
            IF @BMBayar IS NOT NULL THEN UPDATE ttbmhd set BMNominal = @BMBayar WHERE BMNo = xBMNo; END IF;

				SET oKeterangan = CONCAT("Berhasil menghapus data Bank Masuk : ", xBMNo , ' - ' ,xNoAccount);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Batal menyimpan data Bank Masuk: ", xNoAccount );
		END;
	END CASE;
END$$

DELIMITER ;

