DELIMITER $$

DROP PROCEDURE IF EXISTS `jta`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jta`(IN xTANo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE MyPesanKu VARCHAR(300) DEFAULT '';
	DECLARE MyPesankuItem VARCHAR(150) DEFAULT '';

	DECLARE oNilaiHPPHGP, oNilaiHPPOLI DOUBLE DEFAULT 0;

	SET @NoUrutku = 0;

	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xTANo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;
	
	SELECT TANo, TATgl, KodeTrans, PosKode, LokasiKode, TATotal, TAKeterangan, NoGL, Cetak, UserID
	INTO  @TANo,@TATgl,@KodeTrans,@PosKode,@LokasiKode,@TATotal,@TAKeterangan,@NoGL,@Cetak,@UserID
	FROM tttahd	WHERE TANo = xTANo;

   SET MyPesanKu = CONCAT("Post Penyesuaian Stock : " , @TANo , " - " , @KodeTrans );
   SET MyPesanKu = TRIM(MyPesanKu);  IF LENGTH(MyPesanKu) > 300 THEN SET MyPesanKu = SUBSTRING(MyPesanKu,0, 299); END IF;
   IF LENGTH(MyPesanku) > 150 THEN SET MyPesankuItem = SUBSTRING(MyPesanku,0, 149); ELSE SET MyPesankuItem = MyPesanku; END IF;
               
	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;

	SELECT IFNULL(SUM(ttTAit.TAQty * ttTAit.TAHrgBeli),0) INTO oNilaiHPPHGP FROM ttTAit
	INNER JOIN tdbarang ON ttTAit.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) <> 'OLI' AND ttTAit.TANo = xTANo ;

	SELECT IFNULL(SUM(ttTAit.TAQty * ttTAit.TAHrgBeli),0) INTO oNilaiHPPOLI FROM ttTAit
	INNER JOIN tdbarang ON ttTAit.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) = 'OLI' AND ttTAit.TANo = xTANo ;

	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL; DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;DELETE FROM ttglhpit WHERE NoGL = xNoGL;
	DELETE FROM ttgeneralledgerhd WHERE GLLink = xTANo;

	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, DATE(@TATgl), 'TA', MyPesanKu, 0, 0, xTANo, xUserID, '--', xGLValid);

	#Item
	IF (oNilaiHPPHGP) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@TATgl), (@NoUrutku:=@NoUrutku + 1),'11150602',MyPesankuItem, ABS(oNilaiHPPHGP),0;
	END IF;
	IF (oNilaiHPPOLI) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@TATgl), (@NoUrutku:=@NoUrutku + 1),'11150702',MyPesankuItem, ABS(oNilaiHPPOLI),0;
	END IF;
	IF oNilaiHPPHGP < 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@TATgl), (@NoUrutku:=@NoUrutku + 1),'11159991',MyPesankuItem, ABS(oNilaiHPPHGP),0;
	END IF;
	IF (oNilaiHPPOLI) < 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@TATgl), (@NoUrutku:=@NoUrutku + 1),'11159992',MyPesankuItem, ABS(oNilaiHPPOLI),0;
	END IF;

	IF (oNilaiHPPHGP) < 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@TATgl), (@NoUrutku:=@NoUrutku + 1),'11150602',MyPesankuItem, 0,ABS(oNilaiHPPHGP);
	END IF;
	IF (oNilaiHPPOLI) < 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@TATgl), (@NoUrutku:=@NoUrutku + 1),'11150702',MyPesankuItem, 0,ABS(oNilaiHPPOLI);
	END IF;
	IF (oNilaiHPPHGP) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@TATgl), (@NoUrutku:=@NoUrutku + 1),'11159991',MyPesankuItem, 0,ABS(oNilaiHPPHGP);
	END IF;
	IF (oNilaiHPPOLI) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@TATgl), (@NoUrutku:=@NoUrutku + 1),'11159992',MyPesankuItem, 0,ABS(oNilaiHPPOLI);
	END IF;

	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttTAhd SET NoGL = xNoGL  WHERE TANo = xTANo;
END$$

DELIMITER ;

