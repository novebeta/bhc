DELIMITER $$

DROP PROCEDURE IF EXISTS `fkmit`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fkmit`(
    IN xAction VARCHAR(10),
    OUT oStatus INT,
    OUT oKeterangan TEXT,
    IN xUserID VARCHAR(15),
    IN xKodeAkses VARCHAR(15),
    IN xLokasiKode VARCHAR(15),
    IN xKMNo VARCHAR(12),
    IN xKMLink VARCHAR(18),
    IN xKMBayar DECIMAL(14, 2),
    IN xKMNoOLD VARCHAR(12),
    IN xKMLinkOLD VARCHAR(18)
    )
BEGIN
	 CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert
			IF oStatus = 0 THEN		
				INSERT INTO ttkmit (KMNo, KMLink, KMBayar) VALUES (xKMNo, xKMLink, xKMBayar);

            SELECT SUM(KMBayar) INTO @KMBayar FROM ttkmit WHERE KMNo = xKMNo; 
            IF @KMBayar IS NOT NULL THEN UPDATE ttkmhd set KMNominal = @KMBayar WHERE KMNo = xKMNo; END IF;

				SET oKeterangan = CONCAT('Berhasil menambahkan data Kas Masuk ', xKMNo,' - ', xKMLink, ' - ', FORMAT(xKMBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Update
			IF oStatus = 0 THEN
            UPDATE ttKMit SET KMNo = xKMNo, KMLink = xKMLink, KMBayar = xKMBayar WHERE KMNo = xKMNoOLD AND KMLink = xKMLinkOLD;

            SELECT SUM(KMBayar) INTO @KMBayar FROM ttkmit WHERE KMNo = xKMNo; 
            IF @KMBayar IS NOT NULL THEN UPDATE ttkmhd set KMNominal = @KMBayar WHERE KMNo = xKMNo; END IF;

				SET oKeterangan = CONCAT("Berhasil mengubah data Kas Masuk : ", xKMNo , ' - ' ,xKMLink);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Delete
			IF oStatus = 0 THEN
				DELETE FROM ttKMit WHERE KMLink = xKMLink AND KMNo = xKMNo;

            SELECT SUM(KMBayar) INTO @KMBayar FROM ttkmit WHERE KMNo = xKMNo; 
            IF @KMBayar IS NOT NULL THEN UPDATE ttkmhd set KMNominal = @KMBayar WHERE KMNo = xKMNo; END IF;

				SET oKeterangan = CONCAT("Berhasil menghapus data Kas Masuk : ", xKMNo , ' - ' ,xKMLink);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Batal menyimpan data kas masuk: ", xKMLink );
		END;
	END CASE;
END$$

DELIMITER ;

