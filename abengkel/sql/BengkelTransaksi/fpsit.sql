DELIMITER $$

DROP PROCEDURE IF EXISTS `fpsit`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fpsit`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xPSNo VARCHAR(10),
IN xPSAuto SMALLINT(6),
IN xBrgKode VARCHAR(20),
IN xPSQty DECIMAL(5, 0),
IN xPSHrgBeli DECIMAL(14,2),
IN xPSHrgJual DECIMAL(14,2),
IN xPSDiscount DECIMAL(14,2),
IN xPSPajak DECIMAL(2,0),
IN xPONo VARCHAR(10),
IN xPINo VARCHAR(10),
IN xPIQtyS DECIMAL(5,0),
IN xPSNoOLD VARCHAR(10),
IN xPSAutoOLD SMALLINT(6),
IN xBrgKodeOLD VARCHAR(20)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xPSNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xPSNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Mengambil Harga Beli
			SELECT BrgHrgBeli INTO xPSHrgBeli FROM tdbarang WHERE BrgKode = xBrgKode;

			#Insert
			IF oStatus = 0 THEN
				SELECT IF(ISNULL((MAX(PSAuto)+1)),1,(MAX(PSAuto)+1)) INTO xPSAuto FROM ttpsit WHERE PSNo = xPSNo; #AutoN			
				INSERT INTO ttpsit
				(PSNo, PSAuto, BrgKode, PSQty, PSHrgBeli, PSHrgJual, PSDiscount, PSPajak, PONo, PINo, LokasiKode ,PIQtyS) VALUES
				(xPSNo, xPSAuto, xBrgKode, xPSQty, xPSHrgBeli, xPSHrgJual, xPSDiscount, xPSPajak, xPONo, xPINo, xLokasiKode, xPIQtyS);

            #Hitung SubTotal , Total
            SELECT SUM((PSQty * PSHrgJual) - PSDiscount) INTO @SubTotal FROM ttpsit WHERE PSNo = xPSNo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttpshd set PSSubTotal = @SubTotal, PSTotal = (@SubTotal - PSDiscFinal -  PSTotalPajak - PSBiayaKirim) WHERE PSNo = xPSNo;
            END IF;

				SET oKeterangan = CONCAT('Berhasil menambahkan data Barang ', xBrgKode, ' = ' , FORMAT(xPSQty,0));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;	
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
		 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xPSNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xPSNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Jika Beda Kode Barang, ambil Harga Beli
			IF xBrgKode <> xBrgKodeOLD THEN
				SELECT BrgHrgBeli INTO xPSHrgBeli FROM tdbarang WHERE BrgKode = xBrgKode;
			END IF;
			
			IF oStatus = 0 THEN
				UPDATE ttpsit
				SET PSNo = xPSNo, PSAuto = xPSAuto, BrgKode = xBrgKode, PSQty = xPSQty, PSHrgBeli = xPSHrgBeli, PSHrgJual = xPSHrgJual, PSDiscount = xPSDiscount, PSPajak = xPSPajak, PONo = xPONo, PINo = xPINo, LokasiKode = xLokasiKode, PIQtyS = xPIQtyS
				WHERE PSNo = xPSNoOLD AND PSAuto = xPSAutoOLD AND BrgKode = xBrgKodeOLD;

            #Hitung SubTotal , Total
            SELECT SUM((PSQty * PSHrgJual) - PSDiscount) INTO @SubTotal FROM ttpsit WHERE PSNo = xPSNo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttpshd set PSSubTotal = @SubTotal, PSTotal = (@SubTotal - PSDiscFinal -  PSTotalPajak - PSBiayaKirim) WHERE PSNo = xPSNo;
            END IF;

				SET oKeterangan = CONCAT("Berhasil mengubah data Barang : ", xBrgKode, ' = ' , FORMAT(xPSQty,0));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN	
			SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xPSNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xPSNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			IF oStatus = 0 THEN
				DELETE FROM ttpsit WHERE PSAuto = xPSAuto AND PSNo = xPSNo AND BrgKode = xBrgKode;
            
            #Hitung SubTotal , Total
            SELECT SUM((PSQty * PSHrgJual) - PSDiscount) INTO @SubTotal FROM ttpsit WHERE PSNo = xPSNo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttpshd set PSSubTotal = @SubTotal, PSTotal = (@SubTotal - PSDiscFinal -  PSTotalPajak - PSBiayaKirim) WHERE PSNo = xPSNo;
            END IF;

				SET oKeterangan = CONCAT("Berhasil menghapus data Barang : ", xBrgKode, ' = ' , FORMAT(xPSQty,0));
			ELSE
				SET oKeterangan = CONCAT("Gagal menghapus data Barang : ", xBrgKode, ' = ' , FORMAT(xPSQty,0));
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Batal menyimpan data Barang: ", xPSAuto );
		END;
	END CASE;
END$$

DELIMITER ;

