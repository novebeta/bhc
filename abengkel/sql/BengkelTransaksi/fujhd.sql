DELIMITER $$

DROP PROCEDURE IF EXISTS `fujhd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fujhd`(
    IN xAction VARCHAR(10),
    OUT oStatus INT,
    OUT oKeterangan TEXT,
    IN xUserID VARCHAR(15),
    IN xKodeAkses VARCHAR(15),
    IN xLokasiKode VARCHAR(15),
    INOUT xUJNo VARCHAR(10),
    IN xUJNoBaru VARCHAR(10),
    IN xUJTgl DATETIME,
    IN xUJKeterangan VARCHAR(150),
    IN xKodeTrans VARCHAR(4),
    IN xPosKode VARCHAR(20),
    IN xNoGL VARCHAR(10),
    IN xCetak VARCHAR(5))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
         DELETE FROM ttujhd WHERE (UJNo LIKE '%=%') AND UJTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);	
         
			SET oStatus = 0; SET oKeterangan = CONCAT("Daftar Update Harga Jasa ");
		END;
		WHEN "Load" THEN
		BEGIN
			SET oStatus = 0; SET oKeterangan = CONCAT("Load data Update Harga Jasa No: ", xUJNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SET @AutoNo = CONCAT('UJ',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(UJNo,5)+1),5,0)) INTO @AutoNo FROM ttujhd WHERE ((UJNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(UJNo) = 10) ORDER BY UJNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('UJ',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xUJNo = @AutoNo;
			SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Update Harga Jasa No: ",xUJNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @UJNoOLD = xUJNo;	
			IF xPosKode = '--' OR xPosKode="" OR ISNULL(xPosKode) THEN
			   SET xPosKode = xLokasiKode;
			END IF;
					
			IF LOCATE('=',xUJNo) <> 0  AND LENGTH(xUJNoBaru) = 10 THEN /* Generate new auto number */
				SET @AutoNo = CONCAT('UJ',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(UJNo,5)+1),5,0)) INTO @AutoNo FROM ttujhd WHERE ((UJNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(UJNo) = 10) ORDER BY UJNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('UJ',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xUJNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';				
				SET xUJNoBaru = xUJNo;
			END IF;

			IF oStatus = 0 THEN 
				UPDATE ttujhd
				SET UJNo = xUJNoBaru, UJTgl = xUJTgl,UJKeterangan = xUJKeterangan, KodeTrans = xKodeTrans, PosKode = xPosKode, Cetak = xCetak, UserID = xUserID
				WHERE UJNo = xUJNo;
				SET xUJNo = xUJNoBaru;

				#AutoNumber
				UPDATE ttUJit SET UJAuto = -(3*UJAuto) WHERE UJNo = xUJNo;
				SET @row_number = 0;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;
				CREATE TEMPORARY TABLE IF NOT EXISTS TNoUrut AS
				SELECT UJNo, UJAuto,(@row_number:=@row_number + 1) AS NoUrut , JasaKode FROM ttUJit
				WHERE UJNo = xUJNo ORDER BY UJAuto DESC;
				UPDATE ttUJit INNER JOIN TNoUrut
				ON  ttUJit.UJNo = TNoUrut.UJNo
				AND ttUJit.JasaKode = TNoUrut.JasaKode
				AND ttUJit.UJAuto = TNoUrut.UJAuto
				SET ttUJit.UJAuto = TNoUrut.NoUrut;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;				
				
				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Update Harga Jasa','ttujhd', xUJNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data Update Harga Jasa No: ", xUJNo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Update Harga Jasa','ttujhd', xUJNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Update Harga Jasa No: ", xUJNo);
				END IF;				  
			ELSEIF oStatus = 1 THEN
            IF LOCATE('=',xUJNo) <> 0 THEN
               DELETE FROM ttujhd WHERE UJNo = xUJNo;
               SELECT IFNULL(UJNo,'--') INTO xUJNo FROM ttujhd WHERE UJNo NOT LIKE '%=%' AND LEFT(UJNo,2) = 'UJ' ORDER BY UJTgl DESC LIMIT 1; 
               IF xUJNo IS NULL OR LOCATE('=',xUJNo) <> 0 THEN SET xUJNo = '--'; END IF;
            END IF;
            SET oKeterangan = CONCAT("Gagal, silahkan melengkapi data Update Harga Jasa No: ", xUJNo);
			END IF;	
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;				
			SET oKeterangan = "";
		
			IF oStatus = 0 THEN
				DELETE FROM ttujhd WHERE UJNo = xUJNo;
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Update Harga Jasa No: ",xUJNo);
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Update Harga Jasa [UJ]','ttujhd', xUJNo); 			
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;				
		END;
		WHEN "Cancel" THEN
		BEGIN
			 IF LOCATE('=',xUJNo) <> 0 THEN
				  DELETE FROM ttujhd WHERE UJNo = xUJNo;
				  SELECT IFNULL(UJNo,'--') INTO xUJNo FROM ttujhd WHERE UJNo NOT LIKE '%=%' AND LEFT(UJNo,2) = 'UJ' ORDER BY UJTgl DESC LIMIT 1; 
				  IF xUJNo IS NULL OR LOCATE('=',xUJNo) <> 0 THEN SET xUJNo = '--'; END IF;
				  SET oKeterangan = CONCAT("Batal menyimpan data Update Harga Jasa No: ", xUJNoBaru);
			 ELSE
				  SET oKeterangan = CONCAT("Batal menyimpan data Update Harga Jasa No: ", xUJNo);
			 END IF;
		END;
	END CASE;
END$$

DELIMITER ;

