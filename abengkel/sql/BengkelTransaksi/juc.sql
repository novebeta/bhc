DELIMITER $$

DROP PROCEDURE IF EXISTS `juc`$$

CREATE DEFINER=`root`@`%` PROCEDURE `juc`(IN xUCNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE MyPesanKu VARCHAR(300) DEFAULT '';
	DECLARE MyPesankuItem VARCHAR(150) DEFAULT '';

	DECLARE oNilaiHGP, oNilaiOLI, oNilaiSelisih  DOUBLE DEFAULT 0;

	SET  @NoUrutku = 0;

	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xUCNo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;
	
	SELECT UCNo, UCTgl, KodeTrans, PosKode, UCHrgLama, UCHrgBaru, UCSelisih, UCKeterangan, NoGL, Cetak, UserID
	INTO  @UCNo,@UCTgl,@KodeTrans,@PosKode,@UCHrgLama,@UCHrgBaru,@UCSelisih,@UCKeterangan,@NoGL,@Cetak,@UserID
	FROM ttUChd	WHERE UCNo = xUCNo;

   SET MyPesanKu = CONCAT("Post Update Standar Cost : " , @UCNo , " - " , @KodeTrans );
   SET MyPesanKu = TRIM(MyPesanKu);  IF LENGTH(MyPesanKu) > 300 THEN SET MyPesanKu = SUBSTRING(MyPesanKu,0, 299); END IF;
   IF LENGTH(MyPesanku) > 150 THEN SET MyPesankuItem = SUBSTRING(MyPesanku,0, 149); ELSE SET MyPesankuItem = MyPesanku; END IF;
               
	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;


#SELECT ttucit.UCNo, ttucit.UCAuto, ttucit.BrgKode, ttucit.UCQty, ttucit.UCHrgBeliLama, ttucit.UCHrgBeliBaru, ttucit.UCHrgJualLama, ttucit.UCHrgJualBaru, tdbarang.BrgNama, tdbarang.BrgSatuan,tdbarang.BrgGroup,
#ttucit.UCQty * ttucit.UCHrgBeliLama AS JumHargaBeliLama, ttucit.UCQty * ttucit.UCHrgBeliBaru AS JumHargaBeliBaru, ttucit.UCQty * ttucit.UCHrgBeliBaru - ttucit.UCQty * ttucit.UCHrgBeliLama AS SelisihHargaBeli,
#ttucit.UCQty * ttucit.UCHrgJualLama AS JumHargaJualLama, ttucit.UCQty * ttucit.UCHrgJualBaru AS JumHargaJualBaru, ttucit.UCQty * ttucit.UCHrgJualBaru - ttucit.UCQty * ttucit.UCHrgJualLama AS SelisihHargaJual
#FROM ttucit INNER JOIN tdbarang ON ttucit.BrgKode = tdbarang.BrgKode WHERE (ttucit.UCNo = :UCNo)

	SELECT IFNULL(SUM(ttucit.UCQty * ttucit.UCHrgBeliBaru - ttucit.UCQty * ttucit.UCHrgBeliLama),0) INTO oNilaiHGP FROM ttUCit
	INNER JOIN tdbarang ON ttUCit.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) <> 'OLI' AND ttUCit.UCNo = xUCNo ;
	SELECT IFNULL(SUM(ttucit.UCQty * ttucit.UCHrgBeliBaru - ttucit.UCQty * ttucit.UCHrgBeliLama),0) INTO oNilaiOLI FROM ttUCit
	INNER JOIN tdbarang ON ttUCit.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) = 'OLI' AND ttUCit.UCNo = xUCNo ;
	SELECT IFNULL(SUM(ttucit.UCQty * ttucit.UCHrgBeliBaru - ttucit.UCQty * ttucit.UCHrgBeliLama),0) INTO oNilaiHGP FROM ttUCit
	INNER JOIN tdbarang ON ttUCit.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) <> 'OLI' AND ttUCit.UCNo = xUCNo ;
	SELECT IFNULL(SUM(ttucit.UCQty * ttucit.UCHrgBeliBaru - ttucit.UCQty * ttucit.UCHrgBeliLama),0) INTO oNilaiSelisih FROM ttUCit
	INNER JOIN tdbarang ON ttUCit.BrgKode = tdbarang.BrgKode
	WHERE ttUCit.UCNo = xUCNo ;

	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL; DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;DELETE FROM ttglhpit WHERE NoGL = xNoGL;
   DELETE FROM ttgeneralledgerhd WHERE GLLink = xUCNo;

	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, DATE(@UCTgl), 'UC', MyPesanKu, 0, 0, xUCNo, xUserID, '--', xGLValid);

	#Item
	IF (oNilaiHGP) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@UCTgl), (@NoUrutku:=@NoUrutku + 1),'11150602',MyPesankuItem, ABS(oNilaiHGP),0;
	ELSEIF (oNilaiHGP) < 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@UCTgl), (@NoUrutku:=@NoUrutku + 1),'11150602',MyPesankuItem,0, ABS(oNilaiHGP);
	END IF;
	IF (oNilaiOLI) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@UCTgl), (@NoUrutku:=@NoUrutku + 1),'11150702',MyPesankuItem, ABS(oNilaiOLI),0;
	ELSEIF (oNilaiOLI) < 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@UCTgl), (@NoUrutku:=@NoUrutku + 1),'11150702',MyPesankuItem,0, ABS(oNilaiOLI);
	END IF;

	IF (oNilaiSelisih) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@UCTgl), (@NoUrutku:=@NoUrutku + 1),'11200000',MyPesankuItem,0, ABS(oNilaiSelisih);
	ELSEIF (oNilaiSelisih) < 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@UCTgl), (@NoUrutku:=@NoUrutku + 1),'11200000',MyPesankuItem, ABS(oNilaiSelisih),0;
	END IF;

	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttUChd SET NoGL = xNoGL  WHERE UCNo = xUCNo;

END$$

DELIMITER ;

