DELIMITER $$

DROP PROCEDURE IF EXISTS `jkk`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jkk`(IN xKKNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE oSumKKBayarIT DECIMAL(19,2) DEFAULT 0;
	DECLARE oSumKKBayarCOA DECIMAL(19,2) DEFAULT 0;
	DECLARE MyPesanKu VARCHAR(300) DEFAULT '';
	DECLARE MyPesankuItem VARCHAR(150) DEFAULT '';
	DECLARE oKategori VARCHAR(100) DEFAULT '';

	DECLARE MyAccountPOS  VARCHAR(10) DEFAULT '11010100';

	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xKKNo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;

	SELECT KKNo, KKTgl, KKJenis, KodeTrans, PosKode, SupKode, KasKode, KKPerson, KKNominal, KKMemo, NoGL, Cetak, UserID
	INTO @KKNo, @KKTgl, @KKJenis, @KodeTrans, @PosKode, @SupKode, @KasKode, @KKPerson, @KKNominal, @KKMemo, @NoGL, @Cetak, @UserID
	FROM ttKKhd	WHERE KKNo = xKKNo;
	SELECT IFNULL(SUM(KKBayar),0) INTO oSumKKBayarIT FROM ttKKit WHERE KKNo = xKKNo;
	SELECT IFNULL(SUM(KKBayar),0) INTO oSumKKBayarCOA FROM ttKKitcoa WHERE KKNo = xKKNo;

  CASE @KodeTrans
		WHEN  'UM' THEN
		   SET oKategori = 'Umum';
		WHEN 'PL' THEN
		   SET oKategori = 'Pengerjaan Luar';
		WHEN 'BM' THEN
		   SET oKategori = 'KK Ke Bank';
		WHEN  'H1' THEN
		   SET oKategori = 'KK Ke H1';
		WHEN 'PR' THEN
		   SET oKategori = 'Purchase Retur';
		WHEN 'PI' THEN
		   SET oKategori = 'Purchase Invoice';
		WHEN 'SR' THEN
		   SET oKategori = 'Sales Retur';
	END CASE;

	SET MyPesanKu = CONCAT("Post  KK - ",oKategori);
	IF @KKPerson <> '--' THEN SET MyPesanKu = CONCAT(MyPesanKu,' - ' , @KKPerson); END IF;
	IF @KKMemo <> "--" THEN SET MyPesanKu = CONCAT(MyPesanKu,' - ' , @KKMemo); END IF;
	SET MyPesanKu = TRIM(MyPesanKu);
	IF LENGTH(MyPesanKu) > 300 THEN SET MyPesanKu = SUBSTRING(MyPesanKu,0, 299); END IF;
	SET MyPesankuItem = MyPesanKu ;
	IF LENGTH(MyPesankuItem) > 150 THEN SET MyPesankuItem = SUBSTRING(MyPesankuItem,0, 149); END IF;

	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;

	SELECT IFNULL(tdpos.NoAccount,'11010100') INTO MyAccountPOS FROM tdpos INNER JOIN traccount ON traccount.NoAccount = tdpos.NoAccount WHERE PosKode = @PosKode;
	IF MyAccountPOS = '' OR (LENGTH(TRIM(MyAccountPOS)) < 10) THEN SET MyAccountPOS = '11010100'; END IF;

	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL; DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;DELETE FROM ttglhpit WHERE NoGL = xNoGL;
	DELETE FROM ttgeneralledgerhd WHERE GLLink = xKKNo;

	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, DATE(@KKTgl), 'KK', MyPesanKu, 0, 0, xKKNo, xUserID, '--', xGLValid);

	#Item
	SET @NoUrutku = 0;

	CASE @KodeTrans
		WHEN  'UM' THEN
			DROP TEMPORARY TABLE IF EXISTS ScriptTtglhpit; CREATE TEMPORARY TABLE IF NOT EXISTS ScriptTtglhpit AS
			SELECT NoGL, TglGL, GLLink, NoAccount, HPJenis, HPNilai FROM ttglhpit WHERE NoGL = xNoGL;

			INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@KKTgl), (@NoUrutku:=@NoUrutku + 1), NoAccount,  KKKeterangan, KKBayar, 0 FROM ttKKitcoa WHERE KKNo = xKKNo AND KKBayar >= 0;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@KKTgl), (@NoUrutku:=@NoUrutku + 1), NoAccount,  KKKeterangan, 0, KKBayar FROM ttKKitcoa WHERE KKNo = xKKNo AND KKBayar < 0;

			SELECT COUNT(NoGL) INTO @CountNoGL FROM ScriptTtglhpit;
			IF @CountNoGL > 0 THEN
				INSERT INTO ttglhpit SELECT * FROM ScriptTtglhpit;
				DELETE FROM ttglhpit WHERE NoAccount NOT IN (SELECT NoAccount FROM ttgeneralledgerit WHERE NoGL  = xNoGL) AND NoGL  = xNoGL;
				UPDATE ttgeneralledgerhd SET HPLink = 'Many'  WHERE NoGL = xNoGL;
			END IF;

			UPDATE ttKKhd SET KKNominal = oSumKKBayarCOA WHERE KKNo = xKKNo;
		WHEN 'PL' THEN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@KKTgl), (@NoUrutku:=@NoUrutku + 1), 50050000 , MyPesankuItem , @KKNominal, 0  ;
		WHEN 'BM' THEN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@KKTgl), (@NoUrutku:=@NoUrutku + 1), 11040000 , MyPesankuItem , @KKNominal, 0  ;
		WHEN 'H1' THEN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@KKTgl), (@NoUrutku:=@NoUrutku + 1), 11070101 , MyPesankuItem , @KKNominal, 0  ;
		WHEN 'SR' THEN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@KKTgl), (@NoUrutku:=@NoUrutku + 1), 24052100 , MyPesankuItem , @KKNominal, 0  ;
			#Link Piutang
			DELETE FROM ttglhpit WHERE NoGL = xNoGL;
			INSERT INTO ttglhpit SELECT xNoGL, DATE(@KKTgl), KKLink, '24052100','Hutang', KKBayar FROM ttkkit WHERE KKNo = xKKNo;
		WHEN 'PI' THEN
			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@KKTgl), (@NoUrutku:=@NoUrutku + 1),'24010103',MyPesankuItem , KKBayar, 0 FROM ttkkit
			INNER JOIN ttpshd ON ttpshd.PINo = ttkkit.KKlink
			WHERE KKNo = xKKNo AND ttpshd.KodeTrans = 'TC01';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@KKTgl), (@NoUrutku:=@NoUrutku + 1),'24010200',MyPesankuItem , KKBayar, 0 FROM ttkkit
			INNER JOIN ttpshd ON ttpshd.PINo = ttkkit.KKlink
			WHERE KKNo = xKKNo AND ttpshd.KodeTrans = 'TC02';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@KKTgl), (@NoUrutku:=@NoUrutku + 1),'24020102',MyPesankuItem , KKBayar, 0 FROM ttkkit
			INNER JOIN ttpshd ON ttpshd.PINo = ttkkit.KKlink
			WHERE KKNo = xKKNo AND ttpshd.KodeTrans = 'TC03';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@KKTgl), (@NoUrutku:=@NoUrutku + 1),'11130101',MyPesankuItem , KKBayar, 0 FROM ttkkit
			INNER JOIN ttpshd ON ttpshd.PINo = ttkkit.KKlink
			WHERE KKNo = xKKNo AND ttpshd.KodeTrans = 'TC06';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@KKTgl), (@NoUrutku:=@NoUrutku + 1),'24010103',MyPesankuItem , KKBayar, 0 FROM ttkkit
			INNER JOIN ttpshd ON ttpshd.PINo = ttkkit.KKlink
			WHERE KKNo = xKKNo AND ttpshd.KodeTrans = 'TC07';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@KKTgl), (@NoUrutku:=@NoUrutku + 1),'11130201',MyPesankuItem , KKBayar, 0 FROM ttkkit
			INNER JOIN ttpshd ON ttpshd.PINo = ttkkit.KKlink
			WHERE KKNo = xKKNo AND ttpshd.KodeTrans = 'TC08';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@KKTgl), (@NoUrutku:=@NoUrutku + 1),'24010103',MyPesankuItem , KKBayar, 0 FROM ttkkit
			INNER JOIN ttpshd ON ttpshd.PINo = ttkkit.KKlink
			WHERE KKNo = xKKNo AND ttpshd.KodeTrans = 'TC09';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@KKTgl), (@NoUrutku:=@NoUrutku + 1),'24010900',MyPesankuItem , KKBayar, 0 FROM ttkkit
			INNER JOIN ttpshd ON ttpshd.PINo = ttkkit.KKlink
			WHERE KKNo = xKKNo AND ttpshd.KodeTrans = 'TC10';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@KKTgl), (@NoUrutku:=@NoUrutku + 1),IFNULL(NoAccount,'24020701'),MyPesankuItem, KKBayar, 0
			FROM ttKKit
			INNER JOIN ttpsit ON ttpsit.PINo = ttKKit.KKlink
			INNER JOIN ttpshd ON ttpshd.PSNo = ttpsit.PSNo
			INNER JOIN tddealer ON ttpshd.DealerKode = tddealer.DealerKode
			LEFT OUTER JOIN (SELECT NoAccount, NamaAccount FROM traccount WHERE JenisAccount ='Detail' AND NoParent = '24020700') traccount ON CONCAT('%',SUBSTRING_INDEX(TRIM(NamaAccount), ' ', -1)) = CONCAT('%',SUBSTRING_INDEX(TRIM(DealerNama), ' ', -1))
			WHERE KKNo = xKKNo AND KodeTrans = 'TC05';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@KKTgl), (@NoUrutku:=@NoUrutku + 1), IFNULL(NoAccount,'24020701'), MyPesankuItem,KKBayar,0
			FROM ttkkit
			INNER JOIN ttpihd ON PINo = KKLink
			INNER JOIN ttpshd ON ttpihd.PSNo = ttpshd.PSNo
			INNER JOIN tddealer ON ttpshd.DealerKode = tddealer.DealerKode
			LEFT OUTER JOIN
			(SELECT NoAccount, NamaAccount FROM traccount WHERE JenisAccount ='Detail' AND NoParent = '24020700') traccount ON CONCAT('%',SUBSTRING_INDEX(TRIM(NamaAccount), ' ', -1)) = CONCAT('%',SUBSTRING_INDEX(TRIM(DealerNama), ' ', -1))
			WHERE KKNo = xKKNo AND ttpshd.KodeTrans = 'TC05';

			#Link Piutang
			DELETE FROM ttglhpit WHERE NoGL = xNoGL;
			INSERT INTO ttglhpit SELECT xNoGL, DATE(@KKTgl), KKlink ,'24010103','Hutang', KKBayar FROM ttkkit
			INNER JOIN ttpihd ON ttpihd.PINo = ttkkit.KKlink
			INNER JOIN ttpshd ON ttpihd.PSNo = ttpshd.PSNo
			WHERE KKNo = xKKNo AND ttpshd.KodeTrans = 'TC01';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@KKTgl), KKlink ,'24010200','Hutang', KKBayar FROM ttkkit
			INNER JOIN ttpihd ON ttpihd.PINo = ttkkit.KKlink
			INNER JOIN ttpshd ON ttpihd.PSNo = ttpshd.PSNo
			WHERE KKNo = xKKNo AND ttpshd.KodeTrans = 'TC02';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@KKTgl), KKlink ,'24020102','Hutang', KKBayar FROM ttkkit
			INNER JOIN ttpihd ON ttpihd.PINo = ttkkit.KKlink
			INNER JOIN ttpshd ON ttpihd.PSNo = ttpshd.PSNo
			WHERE KKNo = xKKNo AND ttpshd.KodeTrans = 'TC03';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@KKTgl), KKlink ,'11130101','Hutang', KKBayar FROM ttkkit
			INNER JOIN ttpihd ON ttpihd.PINo = ttkkit.KKlink
			INNER JOIN ttpshd ON ttpihd.PSNo = ttpshd.PSNo
			WHERE KKNo = xKKNo AND ttpshd.KodeTrans = 'TC06';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@KKTgl), KKlink ,'24010103','Hutang', KKBayar FROM ttkkit
			INNER JOIN ttpihd ON ttpihd.PINo = ttkkit.KKlink
			INNER JOIN ttpshd ON ttpihd.PSNo = ttpshd.PSNo
			WHERE KKNo = xKKNo AND ttpshd.KodeTrans = 'TC07';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@KKTgl), KKlink ,'11130201','Hutang', KKBayar FROM ttkkit
			INNER JOIN ttpihd ON ttpihd.PINo = ttkkit.KKlink
			INNER JOIN ttpshd ON ttpihd.PSNo = ttpshd.PSNo
			WHERE KKNo = xKKNo AND ttpshd.KodeTrans = 'TC08';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@KKTgl), KKlink ,'24010103','Hutang', KKBayar FROM ttkkit
			INNER JOIN ttpihd ON ttpihd.PINo = ttkkit.KKlink
			INNER JOIN ttpshd ON ttpihd.PSNo = ttpshd.PSNo
			WHERE KKNo = xKKNo AND ttpshd.KodeTrans = 'TC09';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@KKTgl), KKlink ,'24010900','Hutang', KKBayar FROM ttkkit
			INNER JOIN ttpihd ON ttpihd.PINo = ttkkit.KKlink
			INNER JOIN ttpshd ON ttpihd.PSNo = ttpshd.PSNo
			WHERE KKNo = xKKNo AND ttpshd.KodeTrans = 'TC10';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@KKTgl), KKlink ,IFNULL(NoAccount,'24020701'),'Hutang', KKBayar FROM ttkkit
			INNER JOIN ttpihd ON ttpihd.PINo = ttkkit.KKlink
			INNER JOIN ttpshd ON ttpihd.PSNo = ttpshd.PSNo
			INNER JOIN tddealer ON ttpshd.DealerKode = tddealer.DealerKode
			LEFT OUTER JOIN (SELECT NoAccount, NamaAccount FROM traccount WHERE JenisAccount ='Detail' AND NoParent = '24020700') traccount ON CONCAT('%',SUBSTRING_INDEX(TRIM(NamaAccount), ' ', -1)) = CONCAT('%',SUBSTRING_INDEX(TRIM(DealerNama), ' ', -1))
			WHERE KKNo = xKKNo AND ttpshd.KodeTrans = 'TC05';
	END CASE;
	INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@KKTgl), (@NoUrutku:=@NoUrutku + 1), @KasKode , MyPesankuItem , 0, @KKNominal ;

	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttKKhd SET NoGL = xNoGL  WHERE KKNo = xKKNo;

	#Memberi Nilai HPLink
	SET @CountGL = 0;
	SELECT MAX(GLLink), IFNULL(COUNT(NoGL),0) INTO @GLLink, @CountGL FROM ttglhpit WHERE NoGL = xNoGL GROUP BY NoGL;
	IF @CountGL = 0 THEN
		UPDATE ttgeneralledgerhd SET HPLink = '--' WHERE NoGL = xNoGL;
	ELSEIF @CountGL = 1 THEN
		UPDATE ttgeneralledgerhd SET HPLink = @GLLink WHERE NoGL = xNoGL;
	ELSE
		UPDATE ttgeneralledgerhd SET HPLink = 'Many' WHERE NoGL = xNoGL;
	END IF;
	
END$$

DELIMITER ;

