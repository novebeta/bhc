DELIMITER $$

DROP PROCEDURE IF EXISTS `fuchd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fuchd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xUCNo VARCHAR(10),
IN xUCNoBaru VARCHAR(10),
IN xUCTgl DATETIME,
IN xKodeTrans VARCHAR(4),
IN xPosKode VARCHAR(20),
IN xUCHrgLama DECIMAL(14, 2),
IN xUCHrgBaru DECIMAL(14, 2),
IN xUCSelisih DECIMAL(14, 2),
IN xUCKeterangan VARCHAR(150),
IN xNoGL VARCHAR(10),
IN xCetak VARCHAR(5))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
         DELETE FROM ttuchd WHERE (UCNo LIKE '%=%') AND UCTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);	

			#ReJurnal Transaksi Tidak Memiliki Jurnal
			cek01: BEGIN
   			DECLARE rUCNo VARCHAR(100) DEFAULT '';
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT UCNo FROM ttUChd
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttUChd.UCNo 
				WHERE ttgeneralledgerhd.NoGL IS NULL AND UCTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rUCNo;
					IF rUCNo IS NOT NULL AND rUCNo <> '' THEN
						CALL jUC(rUCNo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;			

         CALL cUPDATEtdbaranglokasi(); 		

			SET oStatus = 0; SET oKeterangan = CONCAT("Daftar Update Standart Cost [UC] ");
		END;
		WHEN "Load" THEN
		BEGIN
			 SET oStatus = 0; SET oKeterangan = CONCAT("Load data Update Standart Cost [UC] No: ", xUCNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			 SET @AutoNo = CONCAT('UC',RIGHT(YEAR(NOW()),2),'-');
			 SELECT CONCAT(@AutoNo,LPAD((RIGHT(UCNo,5)+1),5,0)) INTO @AutoNo FROM ttuchd WHERE ((UCNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(UCNo) = 10) ORDER BY UCNo DESC LIMIT 1;
			 IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('UC',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			 SET xUCNo = @AutoNo;
			 SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Update Standart Cost [UC] No: ",xUCNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @UCNoOLD = xUCNo;	
			IF xPosKode = '--' OR xPosKode="" OR ISNULL(xPosKode) THEN
			   SET xPosKode = xLokasiKode;
			END IF;

			#AutoNumber
			IF LOCATE('=',xUCNo) <> 0  THEN 
				SET @AutoNo = CONCAT('UC',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(UCNo,5)+1),5,0)) INTO @AutoNo FROM ttuchd WHERE ((UCNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(UCNo) = 10) ORDER BY UCNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('UC',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xUCNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';		
				SET xUCNoBaru = xUCNo;
			END IF;

			#AutoNumber		
			UPDATE ttUCit SET UCAuto = -(3*UCAuto) WHERE UCNo = xUCNo;
			SET @row_number = 0;
			DROP TEMPORARY TABLE IF EXISTS TNoUrut;
			CREATE TEMPORARY TABLE IF NOT EXISTS TNoUrut AS
			SELECT UCNo, UCAuto,(@row_number:=@row_number + 1) AS NoUrut , BrgKode FROM ttUCit
			WHERE UCNo = xUCNo ORDER BY UCAuto DESC;
			UPDATE ttUCit INNER JOIN TNoUrut
			ON  ttUCit.UCNo = TNoUrut.UCNo
			AND ttUCit.BrgKode = TNoUrut.BrgKode
			AND ttUCit.UCAuto = TNoUrut.UCAuto
			SET ttUCit.UCAuto = TNoUrut.NoUrut;
			DROP TEMPORARY TABLE IF EXISTS TNoUrut;
		
         #Hitung SubTotal & Total
         SELECT SUM(UCQty * UCHrgBeliLama), SUM(UCQty * UCHrgBeliBaru) INTO @UCHrgLama, @UCHrgBaru FROM ttucit WHERE UCNo = xUCNo;
         IF @UCHrgLama IS NULL THEN SET  @UCHrgLama = 0; END IF;
         IF @UCHrgBaru IS NULL THEN SET  @UCHrgBaru = 0; END IF;
         UPDATE ttuchd SET
         UCHrgLama = @UCHrgLama,
         UCHrgBaru = @UCHrgBaru,
         UCSelisih = ABS(@UCHrgBaru-@UCHrgLama)
         WHERE UCNo = xUCNo;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xUCNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xUCNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			IF oStatus = 0 THEN 
				UPDATE ttuchd
				SET UCNo = xUCNoBaru, UCTgl = xUCTgl, KodeTrans = xKodeTrans, PosKode = xPosKode, UCHrgLama = xUCHrgLama, UCHrgBaru = xUCHrgBaru, UCSelisih = xUCSelisih, UCKeterangan = xUCKeterangan, Cetak = xCetak, UserID = xUserID
				WHERE UCNo = xUCNo;
				SET xUCNo = xUCNoBaru;

				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @UCNoOLD; #Hapus Jurnal No Transaksi Lama
				CALL juc(xUCNo,xUserID); 
			  
  				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Update Standart Cost [UC]','ttuchd', xUCNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Update Standart Cost [UC] No: ", xUCNo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Update Standart Cost [UC]','ttuchd', xUCNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Update Standart Cost [UC] No: ", xUCNo);
				END IF;			
			ELSEIF oStatus = 1 THEN
            IF LOCATE('=',xUCNo) <> 0 THEN
               DELETE FROM ttuchd WHERE UCNo = xUCNo;
               SELECT IFNULL(UCNo,'--') INTO xUCNo FROM ttUChd WHERE UCNo NOT LIKE '%=%' AND LEFT(UCNo,2) = 'UC' ORDER BY UCTgl DESC LIMIT 1; 
               IF xUCNo IS NULL OR LOCATE('=',xUCNo) <> 0 THEN SET xUCNo = '--'; END IF;
            END IF;
            SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;

 			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xUCNo;
			IF @GLValid = 'Sudah' THEN	
				SET oStatus = 1; SET oKeterangan = CONCAT("No UC:", xUCNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM ttuchd WHERE UCNo = xUCNo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xUCNo;
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Update Standart Cost [UC] No: ",xUCNo);
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Update Standar Cost [UC]','ttuchd', xUCNo); 			
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;	
		END;
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xUCNo) <> 0 THEN
				DELETE FROM ttuchd WHERE UCNo = xUCNo;
				SELECT IFNULL(UCNo,'--') INTO xUCNo FROM ttUChd WHERE UCNo NOT LIKE '%=%' AND LEFT(UCNo,2) = 'UC' ORDER BY UCTgl DESC LIMIT 1; 
				IF xUCNo IS NULL OR LOCATE('=',xUCNo) <> 0 THEN SET xUCNo = '--'; END IF;
				SET oKeterangan = CONCAT("Batal menyimpan data Update Standart Cost [UC] No: ", xUCNoBaru);
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Update Standart Cost [UC] No: ", xUCNo);
			END IF;
		END;
		WHEN "Jurnal" THEN
		BEGIN
			 IF LOCATE('=',xUCNo) <> 0 THEN /* Data Baru, belum tersimpan tidak boleh di jurnal */
				  SET oStatus = 1;
				  SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			 ELSE
				  CALL juc(xUCNo,xUserID);
				  SET oStatus = 0;
				  SET oKeterangan = CONCAT("Berhasil memproses Jurnal Update Standart Cost [UC] No: ", xUCNo);
			 END IF;
		END;
	END CASE;
END$$

DELIMITER ;

