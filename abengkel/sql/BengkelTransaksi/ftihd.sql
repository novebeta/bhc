DELIMITER $$

DROP PROCEDURE IF EXISTS `ftihd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `ftihd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xTINo VARCHAR(10),
IN xTINoBaru VARCHAR(10),
IN xTITgl DATETIME,
IN xKodeTrans VARCHAR(4),
IN xPosKode VARCHAR(20),
IN xLokasiAsal VARCHAR(15),
IN xLokasiTujuan VARCHAR(15),
IN xPSNo VARCHAR(10),
IN xTITotal DECIMAL(14, 2),
IN xTIKeterangan VARCHAR(150),
IN xCetak VARCHAR(5),
IN xNoGL VARCHAR(10))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
         DELETE FROM tttihd WHERE (TINo LIKE '%=%') AND TITgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
         CALL cUPDATEtdbaranglokasi(); 		        
         SET oStatus = 0; SET oKeterangan = CONCAT("Daftar Transfer [TI] ");
		END;
		WHEN "Load" THEN
		BEGIN
			 SET oStatus = 0; SET oKeterangan = CONCAT("Load data Transfer [TI] No: ", xTINo);
		END;
		WHEN "Insert" THEN
		BEGIN
			 SET @AutoNo = CONCAT('TI',RIGHT(YEAR(NOW()),2),'-');
			 SELECT CONCAT(@AutoNo,LPAD((RIGHT(TINo,5)+1),5,0)) INTO @AutoNo FROM tttihd WHERE ((TINo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(TINo) = 10) ORDER BY TINo DESC LIMIT 1;
			 IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('TI',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			 SET xTINo = @AutoNo;
			 SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Transfer [TI] No: ",xTINo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @TINoOLD = xTINo;	
					
			IF LOCATE('=',xTINo) <> 0  THEN /* Generate new auto number */
				SET @AutoNo = CONCAT('TI',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(TINo,5)+1),5,0)) INTO @AutoNo FROM tttihd WHERE ((TINo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(TINo) = 10) ORDER BY TINo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('TI',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xTINoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';				
				SET xTINoBaru = xTINo;
			END IF;

			IF oStatus = 0 THEN 
				UPDATE tttihd
				SET TINo = xTINoBaru, TITgl = xTITgl, KodeTrans = xKodeTrans, PosKode = xPosKode, LokasiTujuan = xLokasiTujuan, LokasiAsal = xLokasiAsal, PSNo = xPSNo, TITotal = xTITotal, TIKeterangan = xTIKeterangan, NoGL = xNoGL, Cetak = xCetak, UserID = xUserID
				WHERE TINo = xTINo;
				SET xTINo = xTINoBaru;

				#AutoNumber
				UPDATE ttTIit SET TIAuto = -(3*TIAuto) WHERE TINo = xTINo;
				SET @row_number = 0;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;
				CREATE TEMPORARY TABLE IF NOT EXISTS TNoUrut AS
				SELECT TINo, TIAuto,(@row_number:=@row_number + 1) AS NoUrut , BrgKode FROM ttTIit
				WHERE TINo = xTINo ORDER BY TIAuto DESC;
				UPDATE ttTIit INNER JOIN TNoUrut
				ON  ttTIit.TINo = TNoUrut.TINo
				AND ttTIit.BrgKode = TNoUrut.BrgKode
				AND ttTIit.TIAuto = TNoUrut.TIAuto
				SET ttTIit.TIAuto = TNoUrut.NoUrut;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;

            #Hitung SubTotal & Total
            SELECT SUM((TIQty * TIHrgBeli) ) INTO @SubTotal FROM ttTIit WHERE TINo = xTINo;
            IF @Subtotal IS NOT NULL THEN
               UPDATE ttTIhd SET TITotal = @SubTotal WHERE TINo = xTINo;
            END IF;
            							
				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Transer Item [TI]','tttihd', xTINo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Transfer [TI] No: ", xTINo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Transfer Item [TI]','tttihd', xTINo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Transfer [TI] No: ", xTINo);
				END IF;				  
			ELSEIF oStatus = 1 THEN
     			IF LOCATE('=',xTINo) <> 0 THEN
   				DELETE FROM tttihd WHERE TINo = xTINo;
   				SELECT IFNULL(TINo,'--') INTO xTINo FROM ttTIhd WHERE TINo NOT LIKE '%=%' AND LEFT(TINo,2) = 'TI' ORDER BY TITgl DESC LIMIT 1; 
   				IF xTINo IS NULL OR  LOCATE('=',xTINo) <> 0 THEN SET xTINo = '--'; END IF;
   			END IF;
            SET oKeterangan = CONCAT("Gagal, silahkan melengkapi data Transfer [TI] No: ", xTINo);
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;			
			IF oStatus = 0 THEN
				DELETE FROM tttihd WHERE TINo = xTINo;
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Transfer [TI] No: ",xTINo);
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Transfer Item [TI]','tttihd', xTINo); 			
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;				
		END;
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xTINo) <> 0 THEN
				DELETE FROM tttihd WHERE TINo = xTINo;
				SELECT IFNULL(TINo,'--') INTO xTINo FROM ttTIhd WHERE TINo NOT LIKE '%=%' AND LEFT(TINo,2) = 'TI' ORDER BY TITgl DESC LIMIT 1; 
				IF xTINo IS NULL OR  LOCATE('=',xTINo) <> 0 THEN SET xTINo = '--'; END IF;
				SET oKeterangan = CONCAT("Batal menyimpan data Transfer [TI] No: ", xTINoBaru);
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Transfer [TI] No: ", xTINo);
			END IF;
		END;
        WHEN "Loop" THEN
            BEGIN
                UPDATE ttpshd SET TINo = xTINo WHERE PSNo = xPSNo;
                UPDATE tttihd set PSNo = xPSNo where TINo = xTINo;
            END;
	END CASE;
END$$

DELIMITER ;

