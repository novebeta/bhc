DELIMITER $$

DROP PROCEDURE IF EXISTS `fuhit`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fuhit`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xUHNo VARCHAR(10),
IN xUHAuto VARCHAR(10),
IN xBrgKode VARCHAR(20),
IN xUHHrgJualLama DECIMAL(14, 2),
IN xUHHrgJualBaru DECIMAL(14, 2),
IN xUHNoOLD VARCHAR(10),
IN xUHAutoOLD VARCHAR(10),
IN xBrgKodeOLD VARCHAR(20)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;

			#Mengambil Harga Beli
			SELECT BrgHrgJual INTO xUHHrgJualLama FROM tdbarang WHERE BrgKode = xBrgKode;
			
			SELECT IF(ISNULL((MAX(UHAuto)+1)),1,(MAX(UHAuto)+1)) INTO xUHAuto FROM ttuhit WHERE UHNo = xUHNo; #AutoN		
			INSERT INTO ttuhit
			(UHNo, UHAuto, BrgKode, UHHrgJualLama, UHHrgJualBaru) VALUES
			(xUHNo, xUHAuto, xBrgKode, xUHHrgJualLama, xUHHrgJualBaru);
			
			SET oKeterangan = CONCAT('Berhasil menambahkan data Barang ', xBrgKode);
		END;
		WHEN "Update" THEN
		BEGIN	
			SET oStatus = 0;

			#Jika Beda Kode Barang, ambil Harga Beli
			IF xBrgKode <> xBrgKodeOLD THEN
				SELECT BrgHrgJual INTO xUHHrgJualLama FROM tdbarang WHERE BrgKode = xBrgKode;
			END IF;
					
			IF oStatus = 0 THEN
				UPDATE ttuhit
				SET UHNo = xUHNo, UHAuto = xUHAuto, BrgKode = xBrgKode, UHHrgJualLama = xUHHrgJualLama, UHHrgJualBaru = xUHHrgJualBaru
				WHERE UHNo = xUHNoOLD AND UHAuto = xUHAutoOLD AND xBrgKode = xBrgKodeOLD;
				SET oKeterangan = CONCAT("Berhasil mengubah data Barang : ", xBrgKode);
			ELSE
				SET oKeterangan = CONCAT("Gagal mengubah data Barang : ", xBrgKode);
			END IF;
				
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			IF oStatus = 0 THEN
				DELETE FROM ttuhit WHERE UHAuto = xUHAuto AND UHNo = xUHNo AND BrgKode = xBrgKode;
				SET oKeterangan = CONCAT("Berhasil menghapus data Barang : ", xBrgKode );
			ELSE
				SET oKeterangan = CONCAT("Gagal menghapus data Barang : ", xBrgKode );
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			SET oStatus = 0; SET oKeterangan = CONCAT("Batal menyimpan data Barang: ", xBrgKode );
		END;
	END CASE;
END$$

DELIMITER ;

