DELIMITER $$

DROP PROCEDURE IF EXISTS `fseitbarang`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fseitbarang`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xSENo VARCHAR(10),
IN xSEAuto SMALLINT(6),
IN xBrgKode VARCHAR(20),
IN xSEQty DECIMAL(5, 0),
IN xSEHrgBeli DECIMAL(14, 2),
IN xSEHrgJual DECIMAL(14, 2),
IN xSEJualDisc DECIMAL(14, 2),
IN xSENoOLD VARCHAR(10),
IN xSEAutoOLD SMALLINT(6),
IN xBrgKodeOLD VARCHAR(20)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;
			
			#Mengambil Harga Beli
			SELECT BrgHrgBeli INTO xSEHrgBeli FROM tdbarang WHERE BrgKode = xBrgKode;

			#Insert
			IF oStatus = 0 THEN
				SELECT IF(ISNULL((MAX(SEAuto)+1)),1,(MAX(SEAuto)+1)) INTO xSEAuto FROM ttseitbarang WHERE SENo = xSENo; #AutoN												
				INSERT INTO ttseitbarang
				(SENo, SEAuto, BrgKode, SEQty, SEHrgBeli, SEHrgJual, SEJualDisc, LokasiKode) VALUES
				(xSENo, xSEAuto, xBrgKode, xSEQty, xSEHrgBeli, xSEHrgJual, xSEJualDisc, xLokasiKode);

            #Hitung SubTotal & Total
            SELECT SUM((SEQty * SEHrgJual) - SEJualDisc) INTO @SETotalPart FROM ttseitbarang WHERE SENo = xSENo;
            SELECT SUM(SEHrgJual - SEJualDisc) INTO @SETotalJasa FROM ttseitjasa WHERE SENo = xSENo;
            IF @SETotalPart IS NULL THEN SET  @SETotalPart = 0; END IF;
            IF @SETotalJasa IS NULL THEN SET  @SETotalJasa = 0; END IF;
            UPDATE ttsehd SET SETotalJasa = @SETotalJasa, SETotalPart = @SETotalPart,
            SETotalBiaya = @SETotalJasa + @SETotalPart  WHERE SENo = xSENo;


				SET oKeterangan = CONCAT('Berhasil menambahkan data Barang ', xBrgKode, ' = ' , FORMAT(xSEQty,0));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
		 
			#Jika Beda Kode Barang, ambil Harga Beli
			IF xBrgKode <> xBrgKodeOLD THEN
				SELECT BrgHrgBeli INTO xSEHrgBeli FROM tdbarang WHERE BrgKode = xBrgKode;
			END IF;
			
			IF oStatus = 0 THEN
				UPDATE ttseitbarang
				SET SENo = xSENo, SEAuto = xSEAuto, BrgKode = xBrgKode, SEQty = xSEQty, SEHrgBeli = xSEHrgBeli, SEHrgJual = xSEHrgJual, SEJualDisc = xSEJualDisc, LokasiKode = xLokasiKode
				WHERE SENo = xSENoOLD AND SEAuto = xSEAutoOLD AND BrgKode = xBrgKodeOLD;

            #Hitung SubTotal & Total
            SELECT SUM((SEQty * SEHrgJual) - SEJualDisc) INTO @SETotalPart FROM ttseitbarang WHERE SENo = xSENo;
            SELECT SUM(SEHrgJual - SEJualDisc) INTO @SETotalJasa FROM ttseitjasa WHERE SENo = xSENo;
            IF @SETotalPart IS NULL THEN SET  @SETotalPart = 0; END IF;
            IF @SETotalJasa IS NULL THEN SET  @SETotalJasa = 0; END IF;
            UPDATE ttsehd SET SETotalJasa = @SETotalJasa, SETotalPart = @SETotalPart,
            SETotalBiaya = @SETotalJasa + @SETotalPart  WHERE SENo = xSENo;

				SET oKeterangan = CONCAT("Berhasil mengubah data Barang : ",  xBrgKode, ' = ' , FORMAT(xSEQty,0));
			ELSE
				SET oKeterangan = CONCAT("Gagal mengubah data Barang : ",  xBrgKode, ' = ' , FORMAT(xSEQty,0));
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;

 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSENo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSENo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			IF oStatus = 0 THEN
				DELETE FROM ttseitbarang WHERE SEAuto = xSEAuto AND SENo = xSENo AND BrgKode = xBrgKode;

            #Hitung SubTotal & Total
            SELECT SUM((SEQty * SEHrgJual) - SEJualDisc) INTO @SETotalPart FROM ttseitbarang WHERE SENo = xSENo;
            SELECT SUM(SEHrgJual - SEJualDisc) INTO @SETotalJasa FROM ttseitjasa WHERE SENo = xSENo;
            IF @SETotalPart IS NULL THEN SET  @SETotalPart = 0; END IF;
            IF @SETotalJasa IS NULL THEN SET  @SETotalJasa = 0; END IF;
            UPDATE ttsehd SET SETotalJasa = @SETotalJasa, SETotalPart = @SETotalPart,
            SETotalBiaya = @SETotalJasa + @SETotalPart  WHERE SENo = xSENo;


				SET oKeterangan = CONCAT("Berhasil menghapus data Barang : ",  xBrgKode, ' = ' , FORMAT(xSEQty,0));
			ELSE
				SET oKeterangan = CONCAT("Gagal menghapus data Barang : ", xBrgKode, ' = ' , FORMAT(xSEQty,0));
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Batal menyimpan data Barang: ", xSEAuto );
		END;
	END CASE;
END$$

DELIMITER ;

