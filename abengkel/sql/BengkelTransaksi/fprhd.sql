DELIMITER $$

DROP PROCEDURE IF EXISTS `fprhd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fprhd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xPRNo VARCHAR(10),
IN xPRNoBaru VARCHAR(10),
IN xPRTgl DATETIME,
IN xPINo VARCHAR(10),
IN xKodeTrans VARCHAR(4),
IN xPosKode VARCHAR(20),
IN xSupKode VARCHAR(15),
IN xPRSubTotal DECIMAL(14, 2),
IN xPRDiscFinal DECIMAL(14, 2),
IN xPRTotalPajak DECIMAL(14, 2),
IN xPRBiayaKirim DECIMAL(14, 2),
IN xPRTotal DECIMAL(14, 2),
IN xPRTerbayar DECIMAL(14, 2),
IN xPRKeterangan VARCHAR(150),
IN xCetak VARCHAR(5),
IN xNoGL VARCHAR(10)
)
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			DELETE FROM ttprhd WHERE (PRNo LIKE '%=%') AND PRTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);	

			#ReJurnal Transaksi Tidak Memiliki Jurnal
			cek01: BEGIN
   			DECLARE rPRNo VARCHAR(100) DEFAULT '';
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT PRNo FROM ttPRhd
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttPRhd.PRNo 
				WHERE ttgeneralledgerhd.NoGL IS NULL AND PRTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rPRNo;
					IF rPRNo IS NOT NULL AND rPRNo <> '' THEN
						CALL jPR(rPRNo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;		
         CALL cUPDATEtdbaranglokasi(); 
			SET oStatus = 0; SET oKeterangan = CONCAT("Daftar Retur Pembelian [PR] ");
		END;
		WHEN "Load" THEN
		BEGIN
			 SET oStatus = 0; SET oKeterangan = CONCAT("Load data Retur Pembelian [PR] No: ", xPRNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			 SET @AutoNo = CONCAT('PR',RIGHT(YEAR(NOW()),2),'-');
			 SELECT CONCAT(@AutoNo,LPAD((RIGHT(PRNo,5)+1),5,0)) INTO @AutoNo FROM ttprhd WHERE ((PRNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(PRNo) = 10) ORDER BY PRNo DESC LIMIT 1;
			 IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('PR',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			 SET xPRNo = @AutoNo;
			 SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Retur Pembelian [PR] No: ",xPRNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @PRNoOLD = xPRNo;	
			
			IF LOCATE('=',xPRNo) <> 0  THEN 
				SET @AutoNo = CONCAT('PR',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(PRNo,5)+1),5,0)) INTO @AutoNo FROM ttprhd WHERE ((PRNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(PRNo) = 10) ORDER BY PRNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('PR',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xPRNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xPRNoBaru = xPRNo;
			END IF;
		
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xPRNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xPRNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN 
				UPDATE ttprhd
				SET PRNo = xPRNoBaru, PRTgl = xPRTgl, LokasiKode = xLokasiKode, PINo = xPINo, KodeTrans = xKodeTrans, PosKode = xPosKode, SupKode = xSupKode, PRSubTotal = xPRSubTotal, PRDiscFinal = xPRDiscFinal, PRTotalPajak = xPRTotalPajak, PRBiayaKirim = xPRBiayaKirim, PRTotal = xPRTotal, PRTerbayar = xPRTerbayar, PRKeterangan = xPRKeterangan, NoGL = xNoGL, Cetak = xCetak, UserID = xUserID
				WHERE PRNo = xPRNo;
				SET xPRNo = xPRNoBaru;

				#AutoNumber
				UPDATE ttPRit SET PRAuto = -(3*PRAuto) WHERE PRNo = xPRNo;
				SET @row_number = 0;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;
				CREATE TEMPORARY TABLE IF NOT EXISTS TNoUrut AS
				SELECT PRNo, PRAuto,(@row_number:=@row_number + 1) AS NoUrut , BrgKode FROM ttPRit
				WHERE PRNo = xPRNo ORDER BY PRAuto DESC;
				UPDATE ttPRit INNER JOIN TNoUrut
				ON  ttPRit.PRNo = TNoUrut.PRNo
				AND ttPRit.BrgKode = TNoUrut.BrgKode
				AND ttPRit.PRAuto = TNoUrut.PRAuto
				SET ttPRit.PRAuto = TNoUrut.NoUrut;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;
				
            #Hitung SubTotal , Total
            SELECT SUM((PRQty * PRHrgJual) - PRDiscount) INTO @SubTotal FROM ttPRit WHERE PRNo = xPRNo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttPRhd set PRSubTotal = @SubTotal, PRTotal = (@SubTotal - PRDiscFinal -  PRTotalPajak - PRBiayaKirim) WHERE PRNo = xPRNo;
            END IF;

				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @PRNoOLD; #Hapus Jurnal No Transaksi Lama
				CALL jpr(xPRNo,xUserID); 

  				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Retur Pembelian [PR]','ttprhd', xPRNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Retur Pembelian [PR] No: ", xPRNo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Retur Pembelian [PR]','ttprhd', xPRNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Retur Pembelian [PR] No: ", xPRNo);
				END IF;
			ELSEIF oStatus = 1 THEN
   			IF LOCATE('=',xPRNo) <> 0 THEN
   				DELETE FROM ttprhd WHERE PRNo = xPRNo;
   				SELECT IFNULL(PRNo,'--') INTO xPRNo FROM ttPRhd WHERE PRNo NOT LIKE '%=%' AND LEFT(PRNo,2) = 'PR' ORDER BY PRTgl DESC LIMIT 1; 
   				IF xPRNo IS NULL OR LOCATE('=',xPRNo) <> 0 THEN SET xPRNo = '--'; END IF;		  
   			END IF;
            SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;

 			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xPRNo;
			IF @GLValid = 'Sudah' THEN	
				SET oStatus = 1; SET oKeterangan = CONCAT("No PR:", xPRNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM ttprhd WHERE PRNo = xPRNo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xPRNo;
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Retur Pembelian [PR] No: ",xPRNo);
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Retur Pembelian [PR]','ttprhd', xPRNo); 			
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xPRNo) <> 0 THEN
				DELETE FROM ttprhd WHERE PRNo = xPRNo;
				SET oKeterangan = CONCAT("Batal menyimpan data Retur Pembelian [PR] No: ", xPRNoBaru);
				SELECT IFNULL(PRNo,'--') INTO xPRNo FROM ttPRhd WHERE PRNo NOT LIKE '%=%' AND LEFT(PRNo,2) = 'PR' ORDER BY PRTgl DESC LIMIT 1; 
				IF xPRNo IS NULL OR LOCATE('=',xPRNo) <> 0 THEN SET xPRNo = '--'; END IF;		  
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Retur Pembelian [PR] No: ", xPRNo);
			END IF;
		END;
		WHEN "Jurnal" THEN
		BEGIN
			 IF LOCATE('=',xPRNo) <> 0 THEN 
				  SET oStatus = 1;
				  SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			 ELSE
				  CALL jpr(xPRNo,xUserID);
				  SET oStatus = 0;
				  SET oKeterangan = CONCAT("Berhasil memproses Jurnal Retur Pembelian [PR] No: ", xPRNo);
			 END IF;
		END;
	END CASE;
END$$

DELIMITER ;

