DELIMITER $$

DROP PROCEDURE IF EXISTS `fbkit`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fbkit`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xBKNo VARCHAR(12),
IN xBKLink VARCHAR(18),
IN xBKBayar DECIMAL(14, 2),
IN xBKNoOLD VARCHAR(12),
IN xBKLinkOLD VARCHAR(18)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN	
			SET oStatus = 0;
 			
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert
			IF oStatus = 0 THEN		
				INSERT INTO ttbkit (BKNo, BKLink, BKBayar) VALUES (xBKNo, xBKLink, xBKBayar);
            
            #Hitung
            SELECT SUM(BKBayar) INTO @BKBayar FROM ttbkit WHERE BKNo = xBKNo; 
            IF @BKBayar IS NOT NULL THEN UPDATE ttbkhd set BKNominal = @BKBayar WHERE BKNo = xBKNo; END IF;

				SET oKeterangan = CONCAT('Berhasil menambahkan data Bank Keluar ', xBKNo,' - ', xBKNo, ' - ', FORMAT(xBKBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Update
			IF oStatus = 0 THEN
				UPDATE ttbkit SET BKNo = xBKNo, BKLink = xBKLink, BKBayar = xBKBayar	WHERE BKNo = xBKNoOLD AND BKLink = xBKLinkOLD; 
           
           #Hitung
            SELECT SUM(BKBayar) INTO @BKBayar FROM ttbkit WHERE BKNo = xBKNo; 
            IF @BKBayar IS NOT NULL THEN UPDATE ttbkhd set BKNominal = @BKBayar WHERE BKNo = xBKNo; END IF;

				SET oKeterangan = CONCAT("Berhasil mengubah data Bank Keluar : ", xBKNo , ' - ' ,xBKLink);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Delete
			IF oStatus = 0 THEN
				DELETE FROM ttbkit WHERE BKLink = xBKLink AND BKNo = xBKNo;
            
            #Hitung
            SELECT SUM(BKBayar) INTO @BKBayar FROM ttbkit WHERE BKNo = xBKNo; 
            IF @BKBayar IS NOT NULL THEN UPDATE ttbkhd set BKNominal = @BKBayar WHERE BKNo = xBKNo; END IF;

				SET oKeterangan = CONCAT("Berhasil menghapus data bank keluar : ", xBKNo , ' - ' ,xBKLink);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Batal menyimpan data bank keluar: ", xBKLink );
		END;
	END CASE;
END$$

DELIMITER ;

