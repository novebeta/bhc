DELIMITER $$

DROP PROCEDURE IF EXISTS `fsohd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fsohd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xSONo VARCHAR(10),
IN xSONoBaru VARCHAR(10),
IN xSOTgl DATETIME,
IN xSINo VARCHAR(10),
IN xPONo VARCHAR(10),
IN xKodeTrans VARCHAR(4),
IN xPosKode VARCHAR(20),
IN xKarKode VARCHAR(10),
IN xMotorNoPolisi VARCHAR(12),
IN xSOSubTotal DECIMAL(14, 2),
IN xSODiscFinal DECIMAL(14, 2),
IN xSOTotalPajak DECIMAL(14, 2),
IN xSOBiayaKirim DECIMAL(14, 2),
IN xSOTotal DECIMAL(14, 2),
IN xSOTerbayar DECIMAL(14, 2),
IN xSOKeterangan VARCHAR(150),
IN xCetak VARCHAR(5))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
         DELETE FROM ttsohd WHERE (SONo LIKE '%=%') AND SOTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);	
         CALL cUPDATEtdbaranglokasi(); 		
			SET oStatus = 0; SET oKeterangan = CONCAT("Daftar Order Penjualan [SO] ");
		END;
		WHEN "Load" THEN
		BEGIN
			SET oStatus = 0;SET oKeterangan = CONCAT("Load data Order Penjualan [SO] No: ", xSONo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SET @AutoNo = CONCAT('SO',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(SONo,5)+1),5,0)) INTO @AutoNo FROM ttsohd WHERE ((SONo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(SONo) = 10) ORDER BY SONo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('SO',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xSONo = @AutoNo;
			SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Order Penjualan [SO] No: ",xSONo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @SONoOLD = xSONo;	        

			IF LOCATE('=',xSONo) <> 0  THEN 
				SET @AutoNo = CONCAT('SO',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(SONo,5)+1),5,0)) INTO @AutoNo FROM ttsohd WHERE ((SONo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(SONo) = 10) ORDER BY SONo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('SO',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xSONoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';				
				SET xSONoBaru = xSONo;
			END IF;
			
			IF oStatus = 0 THEN 
				UPDATE ttsohd
				SET SONo = xSONoBaru, SOTgl = xSOTgl,LokasiKode = xLokasiKode, SINo = xSINo, PONo = xPONo, KodeTrans = xKodeTrans, 
				PosKode = xPosKode, KarKode = xKarKode, MotorNoPolisi = xMotorNoPolisi, SOSubTotal = xSOSubTotal, SODiscFinal = xSODiscFinal, 
				SOTotalPajak = xSOTotalPajak, SOBiayaKirim = xSOBiayaKirim, SOTotal = xSOTotal, SOTerbayar = xSOTerbayar, SOKeterangan = xSOKeterangan, Cetak = xCetak, UserID = xUserID
				WHERE SONo = xSONo;
				SET xSONo = xSONoBaru;
				
				#AutoNumber
				UPDATE ttSOit SET SOAuto = -(3*SOAuto) WHERE SONo = xSONo;
				SET @row_number = 0;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;
				CREATE TEMPORARY TABLE IF NOT EXISTS TNoUrut AS
				SELECT SONo, SOAuto,(@row_number:=@row_number + 1) AS NoUrut , BrgKode FROM ttSOit
				WHERE SONo = xSONo ORDER BY SOAuto DESC;
				UPDATE ttSOit INNER JOIN TNoUrut
				ON  ttSOit.SONo = TNoUrut.SONo
				AND ttSOit.BrgKode = TNoUrut.BrgKode
				AND ttSOit.SOAuto = TNoUrut.SOAuto
				SET ttSOit.SOAuto = TNoUrut.NoUrut;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;
				
            #Hitung SubTotal & Total
            SELECT SUM((SOQty * SOHrgJual) - SODiscount) INTO @SubTotal FROM ttSOit WHERE SONo = xSONo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttSOhd SET SOSubTotal = @SubTotal, SOTotal = (@SubTotal - SODiscFinal -  SOTotalPajak - SOBiayaKirim) WHERE SONo = xSONo;
            END IF;

				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Order Penjualan [SO]','ttsohd', xSONo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Order Penjualan [SO] No: ", xSONo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Order Penjualan [SO]','ttsohd', xSONo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Order Penjualan [SO] No: ", xSONo);
				END IF;
			ELSEIF oStatus = 1 THEN
   			IF LOCATE('=',xSONo) <> 0 THEN
   				DELETE FROM ttsohd WHERE SONo = xSONo;
   				SELECT IFNULL(SONo,'--') INTO xSONo FROM ttSOhd WHERE SONo NOT LIKE '%=%' AND LEFT(SONo,2) = 'SO' ORDER BY SOTgl DESC LIMIT 1; 
   				IF xSONo IS NULL OR LOCATE('=',xSONo) <> 0 THEN SET xSONo = '--'; END IF;
   			END IF;
				SET oKeterangan = CONCAT("Gagal, silahkan melengkapi data Order Penjualan [SO] No: ", xSONo);
			END IF;		
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;			
			IF oStatus = 0 THEN
				DELETE FROM ttsohd WHERE SONo = xSONo;
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Order Penjualan [SO] No: ",xSONo);
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Order Penjualan [SO]','ttsohd', xSONo); 			
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;	
		END;
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xSONo) <> 0 THEN
				DELETE FROM ttsohd WHERE SONo = xSONo;
				SET oKeterangan = CONCAT("Batal menyimpan data Order Penjualan [SO] No: ", xSONoBaru);
				SELECT IFNULL(SONo,'--') INTO xSONo FROM ttSOhd WHERE SONo NOT LIKE '%=%' AND LEFT(SONo,2) = 'SO' ORDER BY SOTgl DESC LIMIT 1; 
				IF xSONo IS NULL OR LOCATE('=',xSONo) <> 0 THEN SET xSONo = '--'; END IF;
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Order Penjualan [SO] No: ", xSONo);
			END IF;
		END;
	END CASE;
END$$

DELIMITER ;

