DELIMITER $$

DROP PROCEDURE IF EXISTS `fprogramhd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fprogramhd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xPrgNama VARCHAR(10),
IN xPrgNamaBaru VARCHAR(10),
IN xPrgTglMulai DATE,
IN xPrgTglAkhir DATE,
IN xPrgMemo VARCHAR(300))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			 SET oStatus = 0; SET oKeterangan = CONCAT("Daftar Program ");
		END;
		WHEN "Load" THEN
		BEGIN
			 SET oStatus = 0; SET oKeterangan = CONCAT("Load data Program No: ", xPrgNama);
		END;
		WHEN "Insert" THEN
		BEGIN
			 SET @AutoNo = CONCAT('PD',RIGHT(YEAR(NOW()),2),'-');
			 SELECT CONCAT(@AutoNo,LPAD((RIGHT(PrgNama,5)+1),5,0)) INTO @AutoNo FROM tdprogramhd WHERE ((PrgNama LIKE CONCAT(@AutoNo,'%')) AND LENGTH(PrgNama) = 10) ORDER BY PrgNama DESC LIMIT 1;
			 IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('PD',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			 SET xPrgNama = @AutoNo;
			 SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data Program No: ",xPrgNama);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @PrgNamaOLD = xPrgNama;	
					
			IF LOCATE('=',xPrgNama) <> 0  THEN 
				SET @AutoNo = CONCAT('PD',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(PrgNama,5)+1),5,0)) INTO @AutoNo FROM tdprogramhd WHERE ((PrgNama LIKE CONCAT(@AutoNo,'%')) AND LENGTH(PrgNama) = 10) ORDER BY PrgNama DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('PD',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xPrgNamaBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xPrgNamaBaru = xPrgNama;
			END IF;
			
			IF oStatus = 0 THEN
				UPDATE tdprogramhd
				SET PrgNama = xPrgNamaBaru, PrgTglMulai = xPrgTglMulai, PrgTglAkhir = xPrgTglAkhir, PrgMemo = xPrgMemo
				WHERE PrgNama = xPrgNama;
				SET xPrgNama = xPrgNamaBaru;

				#AutoNumber
				UPDATE tdprogramit SET PrgAuto = -(3*PrgAuto) WHERE PrgNama = xPrgNama;
				SET @row_number = 0;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;
				CREATE TEMPORARY TABLE IF NOT EXISTS TNoUrut AS
				SELECT PrgNama, PrgAuto,(@row_number:=@row_number + 1) AS NoUrut , BrgKode FROM tdprogramit
				WHERE PrgNama = xPrgNama ORDER BY PrgAuto DESC;
				UPDATE tdprogramit INNER JOIN TNoUrut
				ON  tdprogramit.PrgNama = TNoUrut.PrgNama
				AND tdprogramit.BrgKode = TNoUrut.BrgKode
				AND tdprogramit.PrgAuto = TNoUrut.PrgAuto
				SET tdprogramit.PrgAuto = TNoUrut.NoUrut;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;				
				
  				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Program Diskon','tdprogramhd', xPrgNama); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Program ", xPrgNama);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Program Diskon','tdprogramhd', xPrgNama);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Program ", xPrgNama);
				END IF;			
			ELSEIF oStatus = 1 THEN
				SET oKeterangan = CONCAT("Gagal, silahkan melengkapi data Program No: ", xPrgNama);
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;		
			IF oStatus = 0 THEN		
				DELETE FROM tdprogramhd WHERE PrgNama = xPrgNama;
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Program No: ",xPrgNama);
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Program Diskon','tdprogramhd', xPrgNama); 			
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xPrgNama) <> 0 THEN
				DELETE FROM tdprogramhd WHERE PrgNama = xPrgNama;
				SET oKeterangan = CONCAT("Batal menyimpan data Program No: ", xPrgNamaBaru);
				SET xPrgNama = '--';
				SELECT IFNULL(PrgNama,'--') INTO xPrgNama FROM tdprogramhd WHERE PrgNama NOT LIKE '%=%'  ORDER BY PrgTglMulai DESC LIMIT 1; 
				IF xPrgNama IS NULL THEN SET xPrgNama = '--'; END IF;			
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Program No: ", xPrgNama);
			END IF;
		END;
	END CASE;
END$$

DELIMITER ;

