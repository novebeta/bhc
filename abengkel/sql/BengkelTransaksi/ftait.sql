DELIMITER $$

DROP PROCEDURE IF EXISTS `ftait`$$

CREATE DEFINER=`root`@`%` PROCEDURE `ftait`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xTANo VARCHAR(10),
IN xTAAuto SMALLINT(6),
IN xBrgKode VARCHAR(20),
IN xTAQty DECIMAL(5, 0),
IN xTAHrgBeli DECIMAL(14, 2),
IN xTANoOLD VARCHAR(10),
IN xTAAutoOLD SMALLINT(6),
IN xBrgKodeOLD VARCHAR(20)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			 SET oStatus = 0;		 

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xTANo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xTANo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;


			#Insert
			IF oStatus = 0 THEN	
				SELECT (MAX(TAAuto)+1) INTO xTAAuto FROM tttait WHERE TANo = xTANo; #AutoN			
				INSERT INTO tttait
				(TANo, TAAuto, BrgKode, TAQty, TAHrgBeli, LokasiKode) VALUES
				(xTANo, xTAAuto, xBrgKode, xTAQty, xTAHrgBeli, xLokasiKode);

            #Hitung SubTotal & Total
            SELECT SUM((TAQty * TAHrgBeli) ) INTO @SubTotal FROM ttTAit WHERE TANo = xTANo;
            IF @Subtotal IS NOT NULL THEN
               UPDATE ttTAhd SET TATotal = @SubTotal WHERE TANo = xTANo;
            END IF;

				SET oKeterangan = CONCAT('Berhasil menambahkan data Barang ', xBrgKode, ' = ' , FORMAT(xTAQty,0));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
					 
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
		 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xTANo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xTANo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Jika Beda Kode Barang, ambil Harga Beli
			IF xBrgKode <> xBrgKodeOLD AND xTAHrgBeli <= 0 THEN
				SELECT BrgHrgBeli INTO xTAHrgBeli FROM tdbarang WHERE BrgKode = xBrgKode;
			END IF;
			 
         IF oStatus = 0 THEN
            UPDATE tttait
            SET TANo = xTANo, TAAuto = xTAAuto, BrgKode = xBrgKode, TAQty = xTAQty, TAHrgBeli = xTAHrgBeli, LokasiKode = xLokasiKode
            WHERE TANo = xTANoOLD AND TAAuto = xTAAutoOLD AND BrgKode = xBrgKodeOLD;
            
            #Hitung SubTotal & Total
            SELECT SUM((TAQty * TAHrgBeli) ) INTO @SubTotal FROM ttTAit WHERE TANo = xTANo;
            IF @Subtotal IS NOT NULL THEN
               UPDATE ttTAhd SET TATotal = @SubTotal WHERE TANo = xTANo;
            END IF;           
            
            SET oKeterangan = CONCAT("Berhasil mengubah data Barang : ", xBrgKode, ' = ' , FORMAT(xTAQty,0));
         ELSE
            SET oKeterangan = oKeterangan;
         END IF;			 		
		END;
		WHEN "Delete" THEN
		BEGIN
         /* Program untuk cek apakah boleh di hapus */
         SET oStatus = 0;
         IF oStatus = 0 THEN
            DELETE FROM tttait WHERE TAAuto = xTAAuto AND TANo = xTANo;

            #Hitung SubTotal & Total
            SELECT SUM((TAQty * TAHrgBeli) ) INTO @SubTotal FROM ttTAit WHERE TANo = xTANo;
            IF @Subtotal IS NOT NULL THEN
               UPDATE ttTAhd SET TATotal = @SubTotal WHERE TANo = xTANo;
            END IF;

            SET oKeterangan = CONCAT("Berhasil menghapus data Barang : ", xTANo , ' - ' ,xTAAuto);
         ELSE
            SET oKeterangan = CONCAT("Gagal menghapus data Barang : ", xTANo , ' - ' ,xTAAuto);
         END IF;
		END;
	END CASE;
END$$

DELIMITER ;

