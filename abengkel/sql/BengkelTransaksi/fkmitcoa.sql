DELIMITER $$

DROP PROCEDURE IF EXISTS `fkmitcoa`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fkmitcoa`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xKMNo VARCHAR(12),
IN xKMAutoN SMALLINT(5),
IN xNoAccount VARCHAR(10),
IN xKMKeterangan VARCHAR(150),
IN xKMBayar DECIMAL(14, 2),
IN xKMNoOLD VARCHAR(12),
IN xKMAutoNOLD SMALLINT(5),
IN xNoAccountOLD VARCHAR(10)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Insert
			IF oStatus = 0 THEN				
				SELECT (MAX(KMAutoN)+1) INTO xKMAutoN FROM ttKMitcoa WHERE KMNo = xKMNo; #AutoN
				INSERT INTO ttKMitcoa (KMNo, KMAutoN, NoAccount, KMBayar, KMKeterangan) VALUES (xKMNo, xKMAutoN, xNoAccount, xKMBayar, xKMKeterangan);

            SELECT SUM(KMBayar) INTO @KMBayar FROM ttkmitcoa WHERE KMNo = xKMNo; 
            IF @KMBayar IS NOT NULL THEN UPDATE ttkmhd set KMNominal = @KMBayar WHERE KMNo = xKMNo; END IF;

				SET oKeterangan = CONCAT('Berhasil menambahkan data Kas Masuk ', xKMNo,' - ', xNoAccount, ' - ', FORMAT(xKMBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
			  UPDATE ttKMitcoa
			  SET KMNo = xKMNo, KMAutoN = xKMAutoN, NoAccount = xNoAccount, KMBayar = xKMBayar, KMKeterangan = xKMKeterangan
			  WHERE KMNo = xKMNoOLD AND KMAutoN = xKMAutoNOLD AND NoAccount = xNoAccountOLD;

            SELECT SUM(KMBayar) INTO @KMBayar FROM ttkmitcoa WHERE KMNo = xKMNo; 
            IF @KMBayar IS NOT NULL THEN UPDATE ttkmhd set KMNominal = @KMBayar WHERE KMNo = xKMNo; END IF;

			  SET oKeterangan = CONCAT("Berhasil mengubah data Kas Masuk : ", xKMNo , ' - ', xNoAccount, ' - ', FORMAT(xKMBayar,2));
			ELSE
			  SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Delete
			IF oStatus = 0 THEN
				DELETE FROM ttKMitcoa WHERE KMAutoN = xKMAutoN AND NoAccount = xNoAccount AND KMNo = xKMNo;

            SELECT SUM(KMBayar) INTO @KMBayar FROM ttkmitcoa WHERE KMNo = xKMNo; 
            IF @KMBayar IS NOT NULL THEN UPDATE ttkmhd set KMNominal = @KMBayar WHERE KMNo = xKMNo; END IF;

				SET oKeterangan = CONCAT("Berhasil menghapus data Kas Masuk : ", xKMNo , ' - ' ,xNoAccount,' - ', FORMAT(xKMBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;		
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Batal menyimpan data kas Masuk: ", xKMAutoN );
		END;
	END CASE;
END$$

DELIMITER ;

