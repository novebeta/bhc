DELIMITER $$

DROP PROCEDURE IF EXISTS `ftiit`$$

CREATE DEFINER=`root`@`%` PROCEDURE `ftiit`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xTINo VARCHAR(10),
IN xTIAuto VARCHAR(10),
IN xBrgKode VARCHAR(20),
IN xTIQty DECIMAL(5, 0),
IN xTIHrgBeli DECIMAL(14, 2),
IN xLokasiAsal VARCHAR(15),
IN xLokasiTujuan VARCHAR(15),
IN xTINoOLD VARCHAR(10),
IN xTIAutoOLD VARCHAR(10),
IN xBrgKodeOLD VARCHAR(20)
)
BEGIN
    CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;
			
			#Mengambil Harga Beli
			SELECT BrgHrgBeli INTO xTIHrgBeli FROM tdbarang WHERE BrgKode = xBrgKode;
			
			SELECT IF(ISNULL((MAX(TIAuto)+1)),1,(MAX(TIAuto)+1)) INTO xTIAuto FROM tttiit WHERE TINo = xTINo; #AutoN
			INSERT INTO tttiit
			(TINo, TIAuto, BrgKode, TIQty, TIHrgBeli, LokasiAsal, LokasiTujuan) VALUES
			(xTINo, xTIAuto, xBrgKode, xTIQty, xTIHrgBeli, xLokasiAsal, xLokasiTujuan);

         #Hitung SubTotal & Total
         SELECT SUM((TIQty * TIHrgBeli) ) INTO @SubTotal FROM ttTIit WHERE TINo = xTINo;
         IF @Subtotal IS NOT NULL THEN
            UPDATE ttTIhd SET TITotal = @SubTotal WHERE TINo = xTINo;
         END IF;
			
			SET oKeterangan = CONCAT('Berhasil menambahkan data Barang ', xBrgKode, ' = ' , FORMAT(xTIQty,0));
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;

			#Jika Beda Kode Barang, ambil Harga Beli
			IF xBrgKode <> xBrgKodeOLD THEN
				SELECT BrgHrgBeli INTO xTIHrgBeli FROM tdbarang WHERE BrgKode = xBrgKode;
			END IF;
					
			IF oStatus = 0 THEN
				UPDATE tttiit
				SET TINo = xTINo, TIAuto = xTIAuto, BrgKode = xBrgKode, TIQty = xTIQty, TIHrgBeli = xTIHrgBeli, LokasiAsal = xLokasiAsal, LokasiTujuan = xLokasiTujuan
				WHERE TINo = xTINoOLD AND TIAuto = xTIAutoOLD AND xBrgKode = xBrgKodeOLD;

            #Hitung SubTotal & Total
            SELECT SUM((TIQty * TIHrgBeli) ) INTO @SubTotal FROM ttTIit WHERE TINo = xTINo;
            IF @Subtotal IS NOT NULL THEN
               UPDATE ttTIhd SET TITotal = @SubTotal WHERE TINo = xTINo;
            END IF;

				SET oKeterangan = CONCAT("Berhasil mengubah data Barang : ",  xBrgKode, ' = ' , FORMAT(xTIQty,0));
			ELSE
				SET oKeterangan = CONCAT("Gagal mengubah data Barang : ", xTINo , ' - ' ,xTIAuto);
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			IF oStatus = 0 THEN
				DELETE FROM tttiit WHERE TIAuto = xTIAuto AND TINo = xTINo  AND BrgKode = xBrgKode;

            #Hitung SubTotal & Total
            SELECT SUM((TIQty * TIHrgBeli) ) INTO @SubTotal FROM ttTIit WHERE TINo = xTINo;
            IF @Subtotal IS NOT NULL THEN
               UPDATE ttTIhd SET TITotal = @SubTotal WHERE TINo = xTINo;
            END IF;

				SET oKeterangan = CONCAT("Berhasil menghapus data Barang : ",  xBrgKode, ' = ' , FORMAT(xTIQty,0));
			ELSE
				SET oKeterangan = CONCAT("Gagal menghapus data Barang : ",  xBrgKode, ' = ' , FORMAT(xTIQty,0));
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Batal menyimpan data Barang: ", xTIAuto );
		END;
	END CASE;
END$$

DELIMITER ;

