DELIMITER $$

DROP PROCEDURE IF EXISTS `fsiit`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fsiit`(
IN xAction VARCHAR(150),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xSINo VARCHAR(10),
IN xSIAuto SMALLINT(6),
IN xBrgKode VARCHAR(20),
IN xSIQty DECIMAL(5,0),
IN xSIHrgBeli DECIMAL(14,2),
IN xSIHrgJual DECIMAL(14,2),
IN xSIDiscount DECIMAL(14,2),
IN xSIPajak DECIMAL(2,0),
IN xSONo VARCHAR(10),
IN xSIPickingNo VARCHAR(10),
IN xSIPickingDate DATETIME,
IN xCetak VARCHAR(5),
IN xSINoOLD VARCHAR(10),
IN xSIAutoOLD SMALLINT(6),
IN xBrgKodeOLD VARCHAR(20)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSINo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSINo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Mengambil Harga Beli
			SELECT BrgHrgBeli INTO xSIHrgBeli FROM tdbarang WHERE BrgKode = xBrgKode;

			#Insert
			IF oStatus = 0 THEN		
				SELECT IF(ISNULL((MAX(SIAuto)+1)),1,(MAX(SIAuto)+1)) INTO xSIAuto FROM ttsiit WHERE SINo = xSINo; #AutoN			
				INSERT INTO ttsiit
				(SINo, SIAuto, BrgKode, SIQty, SIHrgBeli, SIHrgJual, SIDiscount, SIPajak, SONo, LokasiKode, SIPickingNo, SIPickingDate, Cetak) VALUES
				(xSINo, xSIAuto, xBrgKode, xSIQty, xSIHrgBeli, xSIHrgJual, xSIDiscount, xSIPajak, xSONo, xLokasiKode, xSIPickingNo, xSIPickingDate, xCetak);

            #Hitung SubTotal & Total
            SELECT SUM((SIQty * SIHrgJual) - SIDiscount) INTO @SubTotal FROM ttSIit WHERE SINo = xSINo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttSIhd SET SISubTotal = @SubTotal, SITotal = (@SubTotal - SIDiscFinal -  SITotalPajak - SIBiayaKirim  - SIUangMuka) WHERE SINo = xSINo;
            END IF;

				SET oKeterangan = CONCAT('Berhasil menambahkan data Barang ', xBrgKode, ' = ' , FORMAT(xSIQty,0));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;			
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
		 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSINo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSINo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			 #Jika Beda Kode Barang, ambil Harga Beli
			 IF xBrgKode <> xBrgKodeOLD THEN
					SELECT BrgHrgBeli INTO xSIHrgBeli FROM tdbarang WHERE BrgKode = xBrgKode;
			 END IF;

         IF oStatus = 0 THEN
            UPDATE ttsiit
            SET SINo = xSINo, SIAuto = xSIAuto, BrgKode = xBrgKode, SIQty = xSIQty, SIHrgBeli = xSIHrgBeli, SIHrgJual = xSIHrgJual, SIDiscount = xSIDiscount, SIPajak = xSIPajak, SONo = xSONo, LokasiKode = xLokasiKode, SIPickingNo = xSIPickingNo, SIPickingDate = xSIPickingDate, Cetak = xCetak
            WHERE SINo = xSINoOLD AND SIAuto = xSIAutoOLD AND BrgKode = xBrgKodeOLD;

            #Hitung SubTotal & Total
            SELECT SUM((SIQty * SIHrgJual) - SIDiscount) INTO @SubTotal FROM ttSIit WHERE SINo = xSINo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttSIhd SET SISubTotal = @SubTotal, SITotal = (@SubTotal - SIDiscFinal -  SITotalPajak - SIBiayaKirim  - SIUangMuka) WHERE SINo = xSINo;
            END IF;

            SET oKeterangan = CONCAT("Berhasil mengubah data Barang : ", xBrgKode, ' = ' , FORMAT(xSIQty,0));
         ELSE
            SET oKeterangan = oKeterangan;
         END IF;		 			
		END;
		WHEN "Delete" THEN
		BEGIN

			SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSINo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSINo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM ttsiit WHERE SIAuto = xSIAuto AND SINo = xSINo AND BrgKode = xBrgKode;

            #Hitung SubTotal & Total
            SELECT SUM((SIQty * SIHrgJual) - SIDiscount) INTO @SubTotal FROM ttSIit WHERE SINo = xSINo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttSIhd SET SISubTotal = @SubTotal, SITotal = (@SubTotal - SIDiscFinal -  SITotalPajak - SIBiayaKirim  - SIUangMuka) WHERE SINo = xSINo;
            END IF;

				SET oKeterangan = CONCAT("Berhasil menghapus data Barang : ", xBrgKode, ' = ' , FORMAT(xSIQty,0));
			ELSE
				SET oKeterangan = CONCAT("Gagal menghapus data Barang : ", xBrgKode, ' = ' , FORMAT(xSIQty,0));
			END IF;				
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Batal menyimpan data Barang: ", xBrgKode );
		END;
	END CASE;
END$$

DELIMITER ;

