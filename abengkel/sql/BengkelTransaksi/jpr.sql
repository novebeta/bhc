DELIMITER $$

DROP PROCEDURE IF EXISTS `jpr`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jpr`(IN xPRNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
   DECLARE MyPesanKu VARCHAR(300) DEFAULT '';
   DECLARE MyPesankuItem VARCHAR(150) DEFAULT '';

	DECLARE oNilaiHPPHGP DOUBLE DEFAULT 0;
	DECLARE oNilaiHPPOLI DOUBLE DEFAULT 0;
	DECLARE oPotonganHGP DOUBLE DEFAULT 0;
	DECLARE oPotonganOLI DOUBLE DEFAULT 0;
	DECLARE oPriceVariance DOUBLE DEFAULT 0;
	DECLARE oPRTotal DOUBLE DEFAULT 0;

	SET @NoUrutku = 0;

	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xPRNo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;
	
	SELECT PRNo, PRTgl, PINo, KodeTrans, PosKode, SupKode, LokasiKode, PRSubTotal, PRDiscFinal, PRTotalPajak, PRBiayaKirim, PRTotal, PRTerbayar, PRKeterangan, NoGL, Cetak, UserID
	INTO  @PRNo,@PRTgl,@PINo,@KodeTrans,@PosKode,@SupKode,@LokasiKode,@PRSubTotal,@PRDiscFinal,@PRTotalPajak,@PRBiayaKirim,oPRTotal,@PRTerbayar,@PRKeterangan,@NoGL,@Cetak,@UserID
	FROM ttPRhd	WHERE PRNo = xPRNo;

   SET MyPesanKu = CONCAT("Post Retur Pembelian : " , @PRNo , " - " , @KodeTrans , " - ", @SupKode);
   SET MyPesanKu = TRIM(MyPesanKu);  IF LENGTH(MyPesanKu) > 300 THEN SET MyPesanKu = SUBSTRING(MyPesanKu,0, 299); END IF;
   IF LENGTH(MyPesanku) > 150 THEN SET MyPesankuItem = SUBSTRING(MyPesanku,0, 149); ELSE SET MyPesankuItem = MyPesanku; END IF;
               
	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;

	SELECT IFNULL(SUM(ttprit.PRHrgBeli * ttprit.PRQty),0) INTO oNilaiHPPHGP FROM ttprit
	INNER JOIN tdbarang ON ttprit.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) <> 'OLI' AND ttprit.PRNo = xPRNo ;

	SELECT IFNULL(SUM(ttprit.PRHrgBeli * ttprit.PRQty),0) INTO oNilaiHPPOLI FROM ttprit
	INNER JOIN tdbarang ON ttprit.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) = 'OLI' AND ttprit.PRNo = xPRNo ;

	SELECT IFNULL(SUM(PRDiscount),0) INTO oPotonganHGP FROM ttprit
	INNER JOIN tdbarang ON ttprit.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) <> 'OLI' AND ttprit.PRNo = xPRNo ;

	SELECT IFNULL(SUM(PRDiscount),0) INTO oPotonganOLI FROM ttprit
	INNER JOIN tdbarang ON ttprit.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) = 'OLI' AND ttprit.PRNo = xPRNo ;
	SET oPriceVariance  = oPRTotal - (oNilaiHPPHGP + oNilaiHPPOLI);

	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL; DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;DELETE FROM ttglhpit WHERE NoGL = xNoGL;
	DELETE FROM ttgeneralledgerhd WHERE GLLink = xPRNo;

	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, DATE(@PRTgl), 'PR', MyPesanKu, 0, 0, xPRNo, xUserID, '--', xGLValid);

	#Item
	IF oPRTotal > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PRTgl), (@NoUrutku:=@NoUrutku + 1),'11159910',MyPesankuItem, oPRTotal,0;
	END IF;
	IF (oNilaiHPPHGP) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PRTgl), (@NoUrutku:=@NoUrutku + 1),'11150602',MyPesankuItem, 0,oNilaiHPPHGP;
	END IF;
	IF (oNilaiHPPOLI) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PRTgl), (@NoUrutku:=@NoUrutku + 1),'11150702',MyPesankuItem, 0,oNilaiHPPOLI;
	END IF;
	IF oPriceVariance > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PRTgl), (@NoUrutku:=@NoUrutku + 1),'11200000',MyPesankuItem, 0,ABS(oPriceVariance);
	ELSEIF oPriceVariance < 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PRTgl), (@NoUrutku:=@NoUrutku + 1),'11200000',MyPesankuItem, ABS(oPriceVariance),0;
	END IF;

	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttPRhd SET NoGL = xNoGL  WHERE PRNo = xPRNo;

END$$

DELIMITER ;

