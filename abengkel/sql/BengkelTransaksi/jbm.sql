DELIMITER $$

DROP PROCEDURE IF EXISTS `jbm`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jbm`(IN xBMNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
  DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE oSumBMBayar DECIMAL(19,2) DEFAULT 0;
	DECLARE oSumBMBayarIT DECIMAL(19,2) DEFAULT 0;
	DECLARE oSumBMBayarCOA DECIMAL(19,2) DEFAULT 0;
	DECLARE oKategori VARCHAR(100) DEFAULT '';
	DECLARE MyPesanKu VARCHAR(100) DEFAULT '';
	DECLARE MyPesankuItem VARCHAR(100) DEFAULT '';
	
	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xBMNo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;

	SELECT BMNo, BMTgl, BMJenis, KodeTrans, PosKode, MotorNoPolisi, BankKode, BMPerson, BMCekNo, BMCekTempo, BMAC, BMNominal, BMMemo, NoGL, Cetak, UserID
	INTO @BMNo, @BMTgl, @BMJenis, @KodeTrans, @PosKode, @MotorNoPolisi, @BankKode, @BMPerson, @BMCekNo, @BMCekTempo, @BMAC, @BMNominal, @BMMemo, @NoGL, @Cetak, @UserID
	FROM ttbmhd	WHERE BMNo = xBMNo;
	SELECT IFNULL(SUM(BMBayar),0) INTO oSumBMBayarIT FROM ttbmit WHERE BMNo = xBMNo;
	SELECT IFNULL(SUM(BMBayar),0) INTO oSumBMBayarCOA FROM ttbmitcoa WHERE BMNo = xBMNo;
	SET oSumBMBayar = oSumBMBayarIT + oSumBMBayarCOA;

	CASE @KodeTrans
		WHEN 'UM' THEN
			SET oKategori = 'Umum';
		WHEN 'KK' THEN
			SET oKategori = 'BM Dari Kas';
		WHEN 'SE' THEN
			SET oKategori = 'Service Estimation';
		WHEN 'SO' THEN
			SET oKategori = 'Sales Order';
		WHEN 'SV' THEN
			SET oKategori = 'Service Invoice';
		WHEN 'PR' THEN
			SET oKategori = 'Purchase Retur';
		WHEN 'H1' THEN
			SET oKategori = 'BM Dari H1';
		WHEN 'CK' THEN
			SET oKategori = 'Klaim KPB';
		WHEN 'CC' THEN
			SET oKategori = 'Klaim C2';
		WHEN 'SI' THEN
			SET oKategori = 'Sales Invoice';
    WHEN 'BT' THEN
			SET oKategori = 'Bank Transfer';
	END CASE;

	SET MyPesanKu = CONCAT("Post BM - ",oKategori);
	IF @BMPerson <> '--' THEN SET MyPesanKu = CONCAT(MyPesanKu,' - ' , @BMPerson); END IF;
	IF @BMMemo <> "--" THEN SET MyPesanKu = CONCAT(MyPesanKu,' - ' , @BMMemo); END IF;
	SET MyPesanKu = TRIM(MyPesanKu);
	IF LENGTH(MyPesanKu) > 300 THEN SET MyPesanKu = SUBSTRING(MyPesanKu,0, 299); END IF;
	SET MyPesankuItem = MyPesanKu ;
	IF LENGTH(MyPesankuItem) > 150 THEN SET MyPesankuItem = SUBSTRING(MyPesankuItem,0, 149); END IF;

	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;

	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL; DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;DELETE FROM ttglhpit WHERE NoGL = xNoGL;
   DELETE FROM ttgeneralledgerhd WHERE GLLink = xBMNo;

	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, DATE(@BMTgl), 'BM', CONCAT('Post BM - ',oKategori), @BMNominal, @BMNominal, xBMNo, xUserID, '--', xGLValid);

	#Item
	SET @NoUrutku = 0;
	INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@BMTgl), (@NoUrutku:=@NoUrutku + 1), @BankKode , MyPesankuItem , @BMNominal, 0 ;
	CASE @KodeTrans
		WHEN 'UM' THEN
			DROP TEMPORARY TABLE IF EXISTS ScriptTtglhpit; CREATE TEMPORARY TABLE IF NOT EXISTS ScriptTtglhpit AS
			SELECT NoGL, TglGL, GLLink, NoAccount, HPJenis, HPNilai FROM ttglhpit WHERE NoGL = xNoGL;

			INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@BMTgl), (@NoUrutku:=@NoUrutku + 1), NoAccount,  BMKeterangan, 0, BMBayar FROM ttBMitcoa WHERE BMNo = xBMNo AND BMBayar >= 0;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@BMTgl), (@NoUrutku:=@NoUrutku + 1), NoAccount,  BMKeterangan, BMBayar, 0 FROM ttBMitcoa WHERE BMNo = xBMNo AND BMBayar < 0;

			SELECT COUNT(NoGL) INTO @CountNoGL FROM ScriptTtglhpit;
			IF @CountNoGL > 0 THEN
				INSERT INTO ttglhpit SELECT * FROM ScriptTtglhpit;
				DELETE FROM ttglhpit WHERE NoAccount NOT IN (SELECT NoAccount FROM ttgeneralledgerit WHERE NoGL  = xNoGL) AND NoGL  = xNoGL;
				UPDATE ttgeneralledgerhd SET HPLink = 'Many'  WHERE NoGL = xNoGL;
			END IF;

			UPDATE ttBMhd SET BMNominal = oSumBMBayarCOA WHERE BMNo = xBMNo;
    WHEN 'KK' THEN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@BMTgl), (@NoUrutku:=@NoUrutku + 1), '11040000',  MyPesankuItem, 0, @BMNominal ;
		WHEN 'SE' THEN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@BMTgl), (@NoUrutku:=@NoUrutku + 1), '24052000',  MyPesankuItem, 0, @BMNominal ;
		WHEN 'SO' THEN
			INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@BMTgl), (@NoUrutku:=@NoUrutku + 1), '24052200', MyPesankuItem , 0, @BMNominal;
		WHEN 'SV' THEN
			INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@BMTgl), (@NoUrutku:=@NoUrutku + 1), '11060100', MyPesankuItem , 0, @BMNominal;
			#Link Piutang
			DELETE FROM ttglhpit WHERE NoGL = xNoGL;
			INSERT INTO ttglhpit SELECT xNoGL, DATE(@BMTgl), BMLink, '11060100','Piutang', KMBayar FROM ttbmit WHERE BMNo = xBMNo;
    WHEN 'SI' THEN
			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@BMTgl), (@NoUrutku:=@NoUrutku + 1),'11060100',MyPesankuItem , 0, BMBayar FROM ttBMit
			INNER JOIN ttsihd ON ttsihd.SINo = ttBMit.BMlink WHERE BMNo = xBMNo AND KodeTrans = 'TC12';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@BMTgl), (@NoUrutku:=@NoUrutku + 1),IFNULL(NoAccount,'11070710'),MyPesankuItem, 0, BMBayar FROM ttBMit
			INNER JOIN ttsihd ON ttsihd.SINo = ttBMit.BMlink
			INNER JOIN  tddealer ON ttsihd.DealerKode = tddealer.DealerKode
			LEFT OUTER JOIN (SELECT NoAccount, NamaAccount FROM traccount WHERE JenisAccount ='Detail' AND NoParent = '11070700') traccount ON CONCAT('%',SUBSTRING_INDEX(TRIM(NamaAccount), ' ', -1)) = CONCAT('%',SUBSTRING_INDEX(TRIM(DealerNama), ' ', -1))
			WHERE BMNo = xBMNo AND KodeTrans = 'TC17';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@BMTgl), (@NoUrutku:=@NoUrutku + 1),'11060401',MyPesankuItem , 0, BMBayar FROM ttBMit
			INNER JOIN ttsihd ON ttsihd.SINo = ttBMit.BMlink WHERE BMNo = xBMNo AND KodeTrans = 'TC20';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@BMTgl), (@NoUrutku:=@NoUrutku + 1),'11060500',MyPesankuItem , 0, BMBayar FROM ttBMit
			INNER JOIN ttsihd ON ttsihd.SINo = ttBMit.BMlink WHERE BMNo = xBMNo AND KodeTrans = 'TC22';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@BMTgl), (@NoUrutku:=@NoUrutku + 1),'11060402',MyPesankuItem , 0, BMBayar FROM ttBMit
			INNER JOIN ttsihd ON ttsihd.SINo = ttBMit.BMlink WHERE BMNo = xBMNo AND KodeTrans = 'TC21';

			#Link Piutang
			DELETE FROM ttglhpit WHERE NoGL = xNoGL;
			INSERT INTO ttglhpit SELECT xNoGL, DATE(@BMTgl), BMlink ,'11060100','Piutang', BMBayar FROM ttBMit
			INNER JOIN ttsihd ON ttsihd.SINo = ttBMit.BMlink WHERE BMNo = xBMNo AND KodeTrans = 'TC12';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@BMTgl), BMlink ,IFNULL(NoAccount,'11070710'),'Piutang', BMBayar FROM ttBMit
			INNER JOIN ttsihd ON ttsihd.SINo = ttBMit.BMlink
			INNER JOIN  tddealer ON ttsihd.DealerKode = tddealer.DealerKode
			LEFT OUTER JOIN (SELECT NoAccount, NamaAccount FROM traccount WHERE JenisAccount ='Detail' AND NoParent = '11070700') traccount ON CONCAT('%',SUBSTRING_INDEX(TRIM(NamaAccount), ' ', -1)) = CONCAT('%',SUBSTRING_INDEX(TRIM(DealerNama), ' ', -1))
			WHERE BMNo = xBMNo AND KodeTrans = 'TC17';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@BMTgl), BMlink ,'11060401','Piutang', BMBayar FROM ttBMit
			INNER JOIN ttsihd ON ttsihd.SINo = ttBMit.BMlink WHERE BMNo = xBMNo AND KodeTrans = 'TC20';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@BMTgl), BMlink ,'11060500','Piutang', BMBayar FROM ttBMit
			INNER JOIN ttsihd ON ttsihd.SINo = ttBMit.BMlink WHERE BMNo = xBMNo AND KodeTrans = 'TC22';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@BMTgl), BMlink ,'11060402','Piutang', BMBayar FROM ttBMit
			INNER JOIN ttsihd ON ttsihd.SINo = ttBMit.BMlink WHERE BMNo = xBMNo AND KodeTrans = 'TC21';
		WHEN 'PR' THEN
      INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@BMTgl), (@NoUrutku:=@NoUrutku + 1), '11159910', MyPesankuItem , 0, @BMNominal;
		WHEN 'CK' THEN
      INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@BMTgl), (@NoUrutku:=@NoUrutku + 1), '24052131', MyPesankuItem , 0, @BMNominal;
      #Link Piutang
			#DELETE FROM ttglhpit WHERE NoGL = xNoGL;
			#INSERT INTO ttglhpit SELECT xNoGL, DATE(@BMTgl), BMlink ,'11130101','Piutang', BMBayar FROM ttBMit WHERE BMNo = xBMNo;
		WHEN 'CC' THEN
      INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@BMTgl), (@NoUrutku:=@NoUrutku + 1), '24052132', MyPesankuItem , 0, @BMNominal;
      #Link Piutang
			#DELETE FROM ttglhpit WHERE NoGL = xNoGL;
			#INSERT INTO ttglhpit SELECT xNoGL, DATE(@BMTgl), BMlink ,'11130201','Piutang', BMBayar FROM ttBMit WHERE BMNo = xBMNo;
		WHEN 'H1' THEN
      INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@BMTgl), (@NoUrutku:=@NoUrutku + 1), '11070101', MyPesankuItem , 0, @BMNominal;

      #Link Piutang
      DELETE FROM ttglhpit WHERE NoGL = xNoGL;
      SELECT ttkkhd.KKNo INTO @KKH1 FROM ttkkhd INNER JOIN ttkkit ON ttkkhd.kkno = ttkkit.KKNo WHERE KKLink = @MotorNoPolisi;
      INSERT INTO ttglhpit SELECT xNoGL, DATE(@BMTgl), @KKH1 ,'11070101','Piutang', BMBayar FROM ttBMit WHERE BMNo = xBMNo;
  END CASE;

	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttbmhd SET NoGL = xNoGL  WHERE BMNo = xBMNo;

	#Memberi Nilai HPLink
	SET @CountGL = 0;
	SELECT MAX(GLLink), IFNULL(COUNT(NoGL),0) INTO @GLLink, @CountGL FROM ttglhpit WHERE NoGL = xNoGL GROUP BY NoGL;
	IF @CountGL = 0 THEN
		UPDATE ttgeneralledgerhd SET HPLink = '--' WHERE NoGL = xNoGL;
	ELSEIF @CountGL = 1 THEN
		UPDATE ttgeneralledgerhd SET HPLink = @GLLink WHERE NoGL = xNoGL;
	ELSE
		UPDATE ttgeneralledgerhd SET HPLink = 'Many' WHERE NoGL = xNoGL;
	END IF;

END$$

DELIMITER ;

