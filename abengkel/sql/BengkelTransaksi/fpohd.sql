DELIMITER $$

DROP PROCEDURE IF EXISTS `fpohd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fpohd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xPONo VARCHAR(10),
IN xPONoBaru VARCHAR(10),
IN xPOTgl DATETIME,
IN xSupKode VARCHAR(15),
IN xSONo VARCHAR(10),
IN xPSNo VARCHAR(10),
IN xKodeTrans VARCHAR(4),
IN xPOKeterangan VARCHAR(150),
IN xCetak VARCHAR(100),
IN xPOSubTotal DECIMAL(14, 2),
IN xPODiscFinal DECIMAL(14, 2),
IN xPOTotal DECIMAL(14, 2))

BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
         DELETE FROM ttpohd WHERE (PONo LIKE '%=%') AND POTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
         CALL cUPDATEtdbaranglokasi(); 
         SET oStatus = 0; SET oKeterangan = CONCAT("Daftar Order Pembelian [PO] ");
		END;
		WHEN "Load" THEN
		BEGIN
			 SET oStatus = 0; SET oKeterangan = CONCAT("Load data Order Pembelian [PO] No: ", xPONo);
		END;
		WHEN "Insert" THEN
		BEGIN
			 SET @AutoNo = CONCAT('PO',RIGHT(YEAR(NOW()),2),'-');
			 SELECT CONCAT(@AutoNo,LPAD((RIGHT(PONo,5)+1),5,0)) INTO @AutoNo FROM ttpohd WHERE ((PONo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(PONo) = 10) ORDER BY PONo DESC LIMIT 1;
			 IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('PO',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			 SET xPONo = @AutoNo;
			 SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Order Pembelian [PO] No: ",xPONo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @PONoOLD = xPONo;	
			
			IF LOCATE('=',xPONo) <> 0  THEN 
				SET @AutoNo = CONCAT('PO',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(PONo,5)+1),5,0)) INTO @AutoNo FROM ttpohd WHERE ((PONo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(PONo) = 10) ORDER BY PONo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('PO',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xPONoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';				
				SET xPONoBaru = xPONo;
			END IF;
		
			IF oStatus = 0 THEN 
				UPDATE ttpohd
				SET PONo = xPONoBaru, POTgl = xPOTgl, LokasiKode = xLokasiKode, PSNo = xPSNo, SONo = xSONo, KodeTrans = xKodeTrans, 
				PosKode = '--', SupKode = xSupKode, POSubTotal = xPOSubTotal, PODiscFinal = xPODiscFinal, POTotalPajak = 0, POBiayaKirim = 0, POTotal = xPOTotal, 
				POKeterangan = xPOKeterangan, Cetak = xCetak, UserID = xUserID
				WHERE PONo = xPONo;
				SET xPONo = xPONoBaru;

				#AutoNumber
				UPDATE ttPOit SET POAuto = -(3*POAuto) WHERE PONo = xPONo;
				SET @row_number = 0;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;
				CREATE TEMPORARY TABLE IF NOT EXISTS TNoUrut AS
				SELECT PONo, POAuto,(@row_number:=@row_number + 1) AS NoUrut , BrgKode FROM ttPOit
				WHERE PONo = xPONo ORDER BY POAuto DESC;
				UPDATE ttPOit INNER JOIN TNoUrut
				ON  ttPOit.PONo = TNoUrut.PONo
				AND ttPOit.BrgKode = TNoUrut.BrgKode
				AND ttPOit.POAuto = TNoUrut.POAuto
				SET ttPOit.POAuto = TNoUrut.NoUrut;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;

            #Hitung SubTotal & Total
            SELECT SUM((POQty * POHrgJual) - PODiscount) INTO @SubTotal FROM ttPOit WHERE PONo = xPONo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttPOhd set POSubTotal = @SubTotal, POTotal = (@SubTotal - PODiscFinal -  POTotalPajak - POBiayaKirim) WHERE PONo = xPONo;
            END IF;
								
  				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Order Pembelian [PO]','ttpohd', xPONo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Order Pembelian [PO] No: ", xPONo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Order Pembelian [PO]','ttpohd', xPONo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Order Pembelian [PO] No: ", xPONo);
				END IF;
			ELSEIF oStatus = 1 THEN
            IF LOCATE('=',xPONo) <> 0 THEN
               DELETE FROM ttpohd WHERE PONo = xPONo;
               SELECT IFNULL(PONo,'--') INTO xPONo FROM ttPOhd WHERE PONo NOT LIKE '%=%' AND LEFT(PONo,2) = 'PO' ORDER BY POTgl DESC LIMIT 1; 
               IF xPONo IS NULL OR LOCATE('=',xPONo) <> 0 THEN SET xPONo = '--'; END IF;
            END IF;
				SET oKeterangan = CONCAT("Gagal, silahkan melengkapi data Order Pembelian [PO] No: ", xPONo);
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;			
			IF oStatus = 0 THEN
				DELETE FROM ttpohd WHERE PONo = xPONo;
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Order Pembelian [PO] No: ",xPONo);
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Order Pembelian [PO]','ttpohd', xPONo); 			
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;	
		END;
		WHEN "Loop" THEN
		BEGIN
            SELECT SONo into @A FROM ttsohd WHERE PONo = xPONo AND SONo <> xSONo;
            if isnull(@A) Then
                UPDATE ttsohd SET PONo = '--' WHERE SONo IN (SELECT  SONo FROM ttpoit WHERE PONo = xPONo GROUP BY SONO);
                DELETE FROM ttpoit WHERE PONo = xPONo;
            end if;
            UPDATE ttsohd SET PONo = xPONo WHERE SONo = xSONo;
            update ttpohd set SONo = xSONo where PONo = xPONo;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 IF LOCATE('=',xPONo) <> 0 THEN
				DELETE FROM ttpohd WHERE PONo = xPONo;
				SET oKeterangan = CONCAT("Batal menyimpan data Order Pembelian [PO] No: ", xPONoBaru);
				SELECT IFNULL(PONo,'--') INTO xPONo FROM ttPOhd WHERE PONo NOT LIKE '%=%' AND LEFT(PONo,2) = 'PO' ORDER BY POTgl DESC LIMIT 1; 
				IF xPONo IS NULL OR LOCATE('=',xPONo) <> 0 THEN SET xPONo = '--'; END IF;
			 ELSE
				  SET oKeterangan = CONCAT("Batal menyimpan data Order Pembelian [PO] No: ", xPONo);
			 END IF;
		END;
		WHEN "Print" THEN
		BEGIN
		  IF xCetak = "1" THEN
			  SELECT ttpohd.*,tdsupplier.`SupNama` FROM ttpohd,tdsupplier WHERE ttpohd.`SupKode`=tdsupplier.`SupKode` AND ttpohd.`PONo` = xPONoBaru;
		  END IF;
		  IF xCetak = "2" THEN
			  SELECT ttpoit.`PONo`,ttpoit.`POAuto`,ttpoit.`BrgKode`,ttpoit.`POQty`,ttpoit.`POHrgBeli`,ttpoit.`POHrgJual`,ttpoit.`PODiscount`,ttpoit.`POPajak`,
			  tdbarang.`BrgNama`,tdbarang.`BrgSatuan`,ttpoit.`PODiscount` AS Disc,ttpoit.`PODiscount`,tdbarang.`BrgSatuan`,(ttpoit.`POQty`*ttpoit.`POHrgBeli`) - ttpoit.`PODiscount` AS Jumlah 
			  FROM ttpoit,tdbarang WHERE ttpoit.`BrgKode`=tdbarang.`BrgKode` AND ttpoit.`PONo`=xPONoBaru;
		  END IF;
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Cetak data Order Pembelian [PO] No: ", xPONo);
		END;
	END CASE;
END$$

DELIMITER ;

