DELIMITER $$

DROP PROCEDURE IF EXISTS `fucit`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fucit`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xUCNo VARCHAR(10),
IN xUCAuto VARCHAR(10),
IN xBrgKode VARCHAR(20),
IN xUCQty DECIMAL(5, 0),
IN xUCHrgBeliLama DECIMAL(14, 2),
IN xUCHrgBeliBaru DECIMAL(14, 2),
IN xUCHrgJualLama DECIMAL(14, 2),
IN xUCHrgJualBaru DECIMAL(14, 2),
IN xUCNoOLD VARCHAR(10),
IN xUCAutoOLD VARCHAR(10),
IN xBrgKodeOLD VARCHAR(20)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xUCNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xUCNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Mengambil Harga Beli
			SELECT BrgHrgBeli, BrgHrgJual INTO xUCHrgBeliLama, xUCHrgJualLama FROM tdbarang WHERE BrgKode = xBrgKode;
			
         #Mengambil UCQty dari stock saat itu
         SELECT SUM(SaldoQty) INTO @SaldoQty FROM tdbaranglokasi WHERE BrgKode = xBrgKode;
         IF @SaldoQty IS NULL THEN SET  @SaldoQty = 0; END IF;

			#Insert
			IF oStatus = 0 THEN
				SELECT IF(ISNULL((MAX(UCAuto)+1)),1,(MAX(UCAuto)+1)) INTO xUCAuto FROM ttUCit WHERE UCNo = xUCNo; #AutoN		
				INSERT INTO ttucit
				(UCNo, UCAuto, BrgKode, UCQty, UCHrgBeliLama, UCHrgBeliBaru, UCHrgJualLama,UCHrgJualBaru) VALUES
				(xUCNo, xUCAuto, xBrgKode, @SaldoQty, xUCHrgBeliLama, xUCHrgBeliBaru, xUCHrgJualLama, xUCHrgJualBaru);

            #Hitung SubTotal & Total
            SELECT SUM(UCQty * UCHrgBeliLama), SUM(UCQty * UCHrgBeliBaru) INTO @UCHrgLama, @UCHrgBaru FROM ttucit WHERE UCNo = xUCNo;
            IF @UCHrgLama IS NULL THEN SET  @UCHrgLama = 0; END IF;
            IF @UCHrgBaru IS NULL THEN SET  @UCHrgBaru = 0; END IF;
            UPDATE ttuchd SET
            UCHrgLama = @UCHrgLama,
            UCHrgBaru = @UCHrgBaru,
            UCSelisih = ABS(@UCHrgBaru-@UCHrgLama)
            WHERE UCNo = xUCNo;

				SET oKeterangan = CONCAT('Berhasil menambahkan data Barang ', xBrgKode, ' = ' , FORMAT(xUCQty,0));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
			
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
		 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xUCNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xUCNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Jika Beda Kode Barang, ambil Harga Beli
			IF xBrgKode <> xBrgKodeOLD THEN
				SELECT BrgHrgBeli, BrgHrgJual INTO xUCHrgBeliLama, xUCHrgJualLama FROM tdbarang WHERE BrgKode = xBrgKode;
			END IF;

			 
			 IF oStatus = 0 THEN
				  UPDATE ttucit
				  SET UCNo = xUCNo, UCAuto = xUCAuto, BrgKode = xBrgKode, UCQty = xUCQty, UCHrgBeliLama = xUCHrgBeliLama,UCHrgBeliBaru = xUCHrgBeliBaru, UCHrgJualLama = xUCHrgJualLama, UCHrgJualBaru = xUCHrgJualBaru
				  WHERE UCNo = xUCNoOLD AND UCAuto = xUCAutoOLD AND BrgKode = xBrgKodeOLD;

               #Hitung SubTotal & Total
               SELECT SUM(UCQty * UCHrgBeliLama), SUM(UCQty * UCHrgBeliBaru) INTO @UCHrgLama, @UCHrgBaru FROM ttucit WHERE UCNo = xUCNo;
               IF @UCHrgLama IS NULL THEN SET  @UCHrgLama = 0; END IF;
               IF @UCHrgBaru IS NULL THEN SET  @UCHrgBaru = 0; END IF;
               UPDATE ttuchd SET
               UCHrgLama = @UCHrgLama,
               UCHrgBaru = @UCHrgBaru,
               UCSelisih = ABS(@UCHrgBaru-@UCHrgLama)
               WHERE UCNo = xUCNo;

				  SET oKeterangan = CONCAT("Berhasil mengubah data Barang : ", xBrgKode, ' = ' , FORMAT(xUCQty,0));
			 ELSE
				  SET oKeterangan = oKeterangan;
			 END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xUCNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xUCNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM ttucit WHERE UCAuto = xUCAuto AND UCNo = xUCNo AND BrgKode = xBrgKode;

            #Hitung SubTotal & Total
            SELECT SUM(UCQty * UCHrgBeliLama), SUM(UCQty * UCHrgBeliBaru) INTO @UCHrgLama, @UCHrgBaru FROM ttucit WHERE UCNo = xUCNo;
            IF @UCHrgLama IS NULL THEN SET  @UCHrgLama = 0; END IF;
            IF @UCHrgBaru IS NULL THEN SET  @UCHrgBaru = 0; END IF;
            UPDATE ttuchd SET
            UCHrgLama = @UCHrgLama,
            UCHrgBaru = @UCHrgBaru,
            UCSelisih = ABS(@UCHrgBaru-@UCHrgLama)
            WHERE UCNo = xUCNo;

				SET oKeterangan = CONCAT("Berhasil menghapus data Barang : ", xBrgKode, ' = ' , FORMAT(xUCQty,0));
			ELSE
				SET oKeterangan = CONCAT("Gagal menghapus data Barang : ", xBrgKode, ' = ' , FORMAT(xUCQty,0));
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0; SET oKeterangan = CONCAT("Batal menyimpan data Barang: ", xUCAuto );
		END;
	END CASE;
END$$

DELIMITER ;

