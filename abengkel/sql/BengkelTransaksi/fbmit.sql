DELIMITER $$
DROP PROCEDURE IF EXISTS `fbmit`$$
CREATE DEFINER=`root`@`%` PROCEDURE `fbmit`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xBMNo VARCHAR(12),
IN xBMLink VARCHAR(18),
IN xBMBayar DECIMAL(14, 2),
IN xBMNoOLD VARCHAR(12),
IN xBMLinkOLD VARCHAR(18)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert
			IF oStatus = 0 THEN		
				INSERT INTO ttbmit (BMNo, BMLink, BMBayar) VALUES (xBMNo, xBMLink, xBMBayar);

            #Hitung Total
            SELECT SUM(BMBayar) INTO @BMBayar FROM ttbmit WHERE BMNo = xBMNo; 
            IF @BMBayar IS NOT NULL THEN UPDATE ttbmhd set BMNominal = @BMBayar WHERE BMNo = xBMNo; END IF;

				SET oKeterangan = CONCAT('Berhasil menambahkan data Bank Masuk ', xBMNo,' - ', xBMLink, ' - ', FORMAT(xBMBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Update" THEN
		BEGIN	
			SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Update
			IF oStatus = 0 THEN
				UPDATE ttbmit	SET BMNo = xBMNo, BMLink = xBMLink, BMBayar = xBMBayar WHERE BMNo = xBMNoOLD AND BMLink = xBMLinkOLD;

            #Hitung Total
            SELECT SUM(BMBayar) INTO @BMBayar FROM ttbmit WHERE BMNo = xBMNo; 
            IF @BMBayar IS NOT NULL THEN UPDATE ttbmhd set BMNominal = @BMBayar WHERE BMNo = xBMNo; END IF;

				SET oKeterangan = CONCAT("Berhasil mengubah data Bank Masuk : ", xBMNo , ' - ' ,xBMLink);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;		
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Delete
			IF oStatus = 0 THEN
				DELETE FROM ttbmit WHERE BMLink = xBMLink AND BMNo = xBMNo;
            
            #Hitung Total
            SELECT SUM(BMBayar) INTO @BMBayar FROM ttbmit WHERE BMNo = xBMNo; 
            IF @BMBayar IS NOT NULL THEN UPDATE ttbmhd set BMNominal = @BMBayar WHERE BMNo = xBMNo; END IF;

				SET oKeterangan = CONCAT("Berhasil menghapus data bank keluar : ", xBMNo , ' - ' ,xBMLink);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;		
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Batal menyimpan data Bank Masuk: ", xBMNo , ' - ' ,xBMLink );
		END;
	END CASE;
END$$

DELIMITER ;

