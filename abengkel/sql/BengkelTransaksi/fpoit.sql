DELIMITER $$

DROP PROCEDURE IF EXISTS `fpoit`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fpoit`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xPONo VARCHAR(10),
IN xPOAuto SMALLINT(6),
IN xBrgKode VARCHAR(20),
IN xPOQty DECIMAL(5,0),
IN xPOHrgBeli DECIMAL(14,2),
IN xPOHrgJual DECIMAL(14,2),
IN xPODiscount DECIMAL(14,2),
IN xPOPajak DECIMAL(2,0),
IN xSONo VARCHAR(10),
IN xPSNo VARCHAR(10),
IN xPSQtyS DECIMAL(5,0),
IN xPONoOLD VARCHAR(10),
IN xPOAutoOLD SMALLINT(6),
IN xBrgKodeOLD VARCHAR(20)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
         SET oStatus = 0;
         
         #Mengambil Harga Beli
         SELECT BrgHrgBeli INTO xPOHrgBeli FROM tdbarang WHERE BrgKode = xBrgKode;
         
         SELECT IF(ISNULL((MAX(POAuto)+1)),1,(MAX(POAuto)+1)) INTO xPOAuto FROM ttpoit WHERE PONo = xPONo; #AutoN					 
         INSERT INTO ttpoit
         (PONo, POAuto, BrgKode, POQty, POHrgBeli, POHrgJual, PODiscount, POPajak, SONo, PSNo, PSQtyS) VALUES
         (xPONo, xPOAuto, xBrgKode, xPOQty, xPOHrgBeli, xPOHrgJual, xPODiscount, xPOPajak, xSONo, xPSNo, xPSQtyS);

         SELECT SUM((POQty * POHrgJual) - PODiscount) INTO @SubTotal FROM ttPOit WHERE PONo = xPONo;
         IF @Subtotal IS NOT NULL THEN 
            UPDATE ttPOhd set POSubTotal = @SubTotal, POTotal = (@SubTotal - PODiscFinal -  POTotalPajak - POBiayaKirim) WHERE PONo = xPONo;
         END IF;

			SET oKeterangan = CONCAT('Berhasil menambahkan data Barang ', xBrgKode, ' = ' , FORMAT(xPOQty,0));
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;

			#Jika Beda Kode Barang, ambil Harga Beli
			IF xBrgKode <> xBrgKodeOLD THEN
				SELECT BrgHrgBeli INTO xPOHrgBeli FROM tdbarang WHERE BrgKode = xBrgKode;
			END IF;
		 
			IF oStatus = 0 THEN
				UPDATE ttpoit
				SET PONo = xPONo, POAuto = xPOAuto, BrgKode = xBrgKode, POQty = xPOQty, POHrgBeli = xPOHrgBeli, POHrgJual = xPOHrgJual, PODiscount = xPODiscount, 
				POPajak = xPOPajak, SONo = xSONo, PSNo = xPSNo, PSQtyS = xPSQtyS
				WHERE PONo = xPONoOLD AND POAuto = xPOAutoOLD AND xBrgKode = xBrgKodeOLD;

            SELECT SUM((POQty * POHrgJual) - PODiscount) INTO @SubTotal FROM ttPOit WHERE PONo = xPONo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttPOhd set POSubTotal = @SubTotal, POTotal = (@SubTotal - PODiscFinal -  POTotalPajak - POBiayaKirim) WHERE PONo = xPONo;
            END IF;

				SET oKeterangan = CONCAT("Berhasil mengubah data Barang : ",  xBrgKode, ' = ' , FORMAT(xPOQty,0));
			ELSE
				SET oKeterangan = CONCAT("Gagal mengubah data Barang : ", xBrgKode, ' = ' , FORMAT(xPOQty,0));
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
         SET oStatus = 0;
         IF oStatus = 0 THEN
            DELETE FROM ttpoit WHERE POAuto = xPOAuto AND PONo = xPONo AND BrgKode = xBrgKode;

            SELECT SUM((POQty * POHrgJual) - PODiscount) INTO @SubTotal FROM ttPOit WHERE PONo = xPONo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttPOhd set POSubTotal = @SubTotal, POTotal = (@SubTotal - PODiscFinal -  POTotalPajak - POBiayaKirim) WHERE PONo = xPONo;
            END IF;

            SET oKeterangan = CONCAT("Berhasil menghapus data Barang : ", xBrgKode, ' = ' , FORMAT(xPOQty,0));
         ELSE
            SET oKeterangan = CONCAT("Gagal menghapus data Barang : ", xBrgKode, ' = ' , FORMAT(xPOQty,0));
         END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Batal menyimpan data Barang: ", xPOAuto );
		END;
	END CASE;
END$$

DELIMITER ;

