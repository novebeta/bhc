DELIMITER $$

DROP PROCEDURE IF EXISTS `fuhhd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fuhhd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xUHNo VARCHAR(10),
IN xUHNoBaru VARCHAR(10),
IN xUHTgl DATETIME,
IN xUHKeterangan VARCHAR(150),
IN xKodeTrans VARCHAR(4),
IN xPosKode VARCHAR(20),
IN xNoGL VARCHAR(10),
IN xCetak VARCHAR(5))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
         DELETE FROM ttuhhd WHERE (UHNo LIKE '%=%') AND UHTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);	

			SET oStatus = 0; SET oKeterangan = CONCAT("Daftar Update Harga Jual [HET] ");
		END;
		WHEN "Load" THEN
		BEGIN
			SET oStatus = 0; SET oKeterangan = CONCAT("Load data Update Harga Jual [HET] No: ", xUHNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SET @AutoNo = CONCAT('UH',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(UHNo,5)+1),5,0)) INTO @AutoNo FROM ttuhhd WHERE ((UHNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(UHNo) = 10) ORDER BY UHNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('UH',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xUHNo = @AutoNo;
			SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Update Harga Jual [HET] No: ",xUHNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @UHNoOLD = xUHNo;	
			IF xPosKode = '--' OR xPosKode="" OR ISNULL(xPosKode) THEN
			   SET xPosKode = xLokasiKode;
			END IF;

			IF LOCATE('=',xUHNo) <> 0  AND LENGTH(xUHNoBaru) = 10 THEN /* Generate new auto number */
				SET @AutoNo = CONCAT('UH',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(UHNo,5)+1),5,0)) INTO @AutoNo FROM ttuhhd WHERE ((UHNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(UHNo) = 10) ORDER BY UHNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('UH',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xUHNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';	
				SET xUHNoBaru = xUHNo;			
			END IF;

			IF oStatus = 0 THEN 
				UPDATE ttuhhd
				SET UHNo = xUHNoBaru, UHTgl = xUHTgl,UHKeterangan = xUHKeterangan, KodeTrans = xKodeTrans, PosKode = xPosKode, Cetak = xCetak, UserID = xUserID
				WHERE UHNo = xUHNo;
				SET xUHNo = xUHNoBaru;

				#AutoNumber
				UPDATE ttUHit SET UHAuto = -(3*UHAuto) WHERE UHNo = xUHNo;
				SET @row_number = 0;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;
				CREATE TEMPORARY TABLE IF NOT EXISTS TNoUrut AS
				SELECT UHNo, UHAuto,(@row_number:=@row_number + 1) AS NoUrut , BrgKode FROM ttUHit
				WHERE UHNo = xUHNo ORDER BY UHAuto DESC;
				UPDATE ttUHit INNER JOIN TNoUrut
				ON  ttUHit.UHNo = TNoUrut.UHNo
				AND ttUHit.BrgKode = TNoUrut.BrgKode
				AND ttUHit.UHAuto = TNoUrut.UHAuto
				SET ttUHit.UHAuto = TNoUrut.NoUrut;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;		
				
				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Update Harga Jual [HET]','ttuhhd', xUHNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Update Harga Jual [HET] No: ", xUHNo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Update Harga Jual [HET]','ttuhhd', xUHNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Update Harga Jual [HET] No: ", xUHNo);
				END IF;				  
			ELSEIF oStatus = 1 THEN
				  SET oKeterangan = CONCAT("Gagal, silahkan melengkapi data Update Harga Jual [HET] No: ", xUHNo);
			END IF;					 
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0; SET oKeterangan = "";
			IF oStatus = 0 THEN
				  DELETE FROM ttuhhd WHERE UHNo = xUHNo;
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Update Harga Jual [HET] No: ",xUHNo);
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Update Harga Jual [HET]','ttuhhd', xUHNo); 			
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xUHNo) <> 0 THEN
				DELETE FROM ttuhhd WHERE UHNo = xUHNo;
				SELECT IFNULL(UHNo,'--') INTO xUHNo FROM ttUHhd WHERE UHNo NOT LIKE '%=%' AND LEFT(UHNo,2) = 'UH' ORDER BY UHTgl DESC LIMIT 1; 
				IF xUHNo IS NULL OR LOCATE('=',xUHNo) <> 0 THEN SET xUHNo = '--'; END IF;
				SET oKeterangan = CONCAT("Batal menyimpan data Update Harga Jual [HET] No: ", xUHNoBaru);
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Update Harga Jual [HET] No: ", xUHNo);
			END IF;
		END;
	END CASE;
END$$

DELIMITER ;

