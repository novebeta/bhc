DELIMITER $$

DROP PROCEDURE IF EXISTS `jpi`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jpi`(IN xPINo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE MyPesanKu VARCHAR(300) DEFAULT '';
	DECLARE MyPesankuItem VARCHAR(150) DEFAULT '';

	DECLARE oNilaiHPPHGPStandardCostPS DOUBLE DEFAULT 0;
	DECLARE oNilaiHPPOLIStandardCostPS DOUBLE DEFAULT 0;
	DECLARE oNilaiHPPHGPRealCostPS DOUBLE DEFAULT 0;
	DECLARE oNilaiHPPOLIRealCostPS DOUBLE DEFAULT 0;
	DECLARE oPotonganHGP DOUBLE DEFAULT 0;
	DECLARE oPotonganOLI DOUBLE DEFAULT 0;
	DECLARE oPriceVariance DOUBLE DEFAULT 0;
	DECLARE oNilaiSelisihStandardCostHGP DOUBLE DEFAULT 0;
	DECLARE oNilaiSelisihStandardCostOLI DOUBLE DEFAULT 0;
	DECLARE oNilaiSelisihRealCostHGP DOUBLE DEFAULT 0;
	DECLARE oNilaiSelisihRealCostOLI DOUBLE DEFAULT 0;
	DECLARE MyTC VARCHAR(5) DEFAULT 'TC01';
	DECLARE MyDealerNama VARCHAR(100) DEFAULT '--';
	DECLARE oPITotal DOUBLE DEFAULT 0;
	SET @NoUrutku = 0;

	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xPINo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;

	SELECT PINo, PITgl, PIJenis, PSNo, KodeTrans, PosKode, SupKode, LokasiKode, PINoRef, PITerm, PITglTempo, PISubTotal, PIDiscFinal, PITotalPajak, PIBiayaKirim, PITotal, PITerbayar, PIKeterangan, NoGL, Cetak, UserID
	INTO 	@PINo,@PITgl,@PIJenis,@PSNo,@KodeTrans,@PosKode,@SupKode,@LokasiKode,@PINoRef,@PITerm,@PITglTempo,@PISubTotal,@PIDiscFinal,@PITotalPajak,@PIBiayaKirim,oPITotal,@PITerbayar,@PIKeterangan,@NoGL,@Cetak,@UserID
	FROM ttPIhd	WHERE PINo = xPINo;

	SET MyPesanKu = CONCAT("Post Invoice Pembelian : " , @PINo , " - " , @KodeTrans , " - NoRef : " , @PINoRef , " - " , @SupKode);
	SET MyPesanKu = TRIM(MyPesanKu);  IF LENGTH(MyPesanKu) > 300 THEN SET MyPesanKu = SUBSTRING(MyPesanKu,0, 299); END IF;
	IF LENGTH(MyPesanku) > 150 THEN SET MyPesankuItem = SUBSTRING(MyPesanku,0, 149); ELSE SET MyPesankuItem = MyPesanku; END IF;

	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;

	SELECT IFNULL(SUM(IFNULL(ttpsit.PSQty,0) * ttpiit.PIHrgBeli),0) INTO oNilaiHPPHGPStandardCostPS FROM ttpiit
	INNER JOIN tdbarang ON ttpiit.BrgKode = tdbarang.BrgKode
	LEFT OUTER JOIN ttpsit ON ttpiit.BrgKode = ttpsit.BrgKode AND ttpiit.PSNo = ttpsit.PSNo
	WHERE UPPER(BrgGroup) <> 'OLI' AND ttpiit.PINo = xPINo ;

	SELECT IFNULL(SUM(IFNULL(ttpsit.PSQty,0) * ttpiit.PIHrgBeli),0) INTO oNilaiHPPOLIStandardCostPS FROM ttpiit
	INNER JOIN tdbarang ON ttpiit.BrgKode = tdbarang.BrgKode
	LEFT OUTER JOIN ttpsit ON ttpiit.BrgKode = ttpsit.BrgKode AND ttpiit.PSNo = ttpsit.PSNo
	WHERE UPPER(BrgGroup) = 'OLI' AND ttpiit.PINo = xPINo ;

	SELECT IFNULL(SUM(IFNULL(ttpsit.PSQty,0) * ttpiit.PIHrgJual),0) INTO oNilaiHPPHGPRealCostPS FROM ttpiit
	INNER JOIN tdbarang ON ttpiit.BrgKode = tdbarang.BrgKode
	LEFT OUTER JOIN ttpsit ON ttpiit.BrgKode = ttpsit.BrgKode AND ttpiit.PSNo = ttpsit.PSNo
	WHERE UPPER(BrgGroup) <> 'OLI' AND ttpiit.PINo = xPINo ;

	SELECT IFNULL(SUM(IFNULL(ttpsit.PSQty,0) * ttpiit.PIHrgJual),0) INTO oNilaiHPPOLIRealCostPS FROM ttpiit
	INNER JOIN tdbarang ON ttpiit.BrgKode = tdbarang.BrgKode
	LEFT OUTER JOIN ttpsit ON ttpiit.BrgKode = ttpsit.BrgKode AND ttpiit.PSNo = ttpsit.PSNo
	WHERE UPPER(BrgGroup) = 'OLI' AND ttpiit.PINo = xPINo ;

	SELECT IFNULL(SUM(IFNULL(PIDiscount,0)),0) INTO oPotonganHGP FROM ttpiit
	INNER JOIN tdbarang ON ttpiit.BrgKode = tdbarang.BrgKode
	LEFT OUTER JOIN ttpsit ON ttpiit.BrgKode = ttpsit.BrgKode AND ttpiit.PSNo = ttpsit.PSNo
	WHERE UPPER(BrgGroup) <> 'OLI' AND ttpiit.PINo = xPINo ;

	SELECT IFNULL(SUM(IFNULL(PIDiscount,0)),0) INTO oPotonganOLI FROM ttpiit
	INNER JOIN tdbarang ON ttpiit.BrgKode = tdbarang.BrgKode
	LEFT OUTER JOIN ttpsit ON ttpiit.BrgKode = ttpsit.BrgKode AND ttpiit.PSNo = ttpsit.PSNo
	WHERE UPPER(BrgGroup) = 'OLI' AND ttpiit.PINo = xPINo ;

	SET oPriceVariance  = (oNilaiHPPHGPRealCostPS + oNilaiHPPOLIRealCostPS - oPotonganHGP - oPotonganOLI) - (oNilaiHPPHGPStandardCostPS + oNilaiHPPOLIStandardCostPS);

	SELECT IFNULL(SUM(PIHrgBeli * (PIQty-PSQty)),0) INTO oNilaiSelisihStandardCostHGP FROM ttpiit
	INNER JOIN tdbarang ON ttpiit.BrgKode = tdbarang.BrgKode
	LEFT OUTER JOIN ttpsit ON ttpiit.BrgKode = ttpsit.BrgKode AND ttpiit.PSNo = ttpsit.PSNo
	WHERE UPPER(BrgGroup) <> 'OLI' AND ttpiit.PINo = xPINo ;

	SELECT IFNULL(SUM(PIHrgBeli * (PIQty-PSQty)),0) INTO oNilaiSelisihStandardCostOLI FROM ttpiit
	INNER JOIN tdbarang ON ttpiit.BrgKode = tdbarang.BrgKode
	LEFT OUTER JOIN ttpsit ON ttpiit.BrgKode = ttpsit.BrgKode AND ttpiit.PSNo = ttpsit.PSNo
	WHERE UPPER(BrgGroup) = 'OLI' AND ttpiit.PINo = xPINo ;

	SELECT IFNULL(SUM(PIHrgJual * (PIQty-PSQty)),0) INTO oNilaiSelisihRealCostHGP FROM ttpiit
	INNER JOIN tdbarang ON ttpiit.BrgKode = tdbarang.BrgKode
	LEFT OUTER JOIN ttpsit ON ttpiit.BrgKode = ttpsit.BrgKode AND ttpiit.PSNo = ttpsit.PSNo
	WHERE UPPER(BrgGroup) <> 'OLI' AND ttpiit.PINo = xPINo ;

	SELECT IFNULL(SUM(PIHrgJual * (PIQty-PSQty)),0) INTO oNilaiSelisihRealCostOLI FROM ttpiit
	INNER JOIN tdbarang ON ttpiit.BrgKode = tdbarang.BrgKode
	LEFT OUTER JOIN ttpsit ON ttpiit.BrgKode = ttpsit.BrgKode AND ttpiit.PSNo = ttpsit.PSNo
	WHERE UPPER(BrgGroup) = 'OLI' AND ttpiit.PINo = xPINo ;

	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL; DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;DELETE FROM ttglhpit WHERE NoGL = xNoGL;
	DELETE FROM ttgeneralledgerhd WHERE GLLink = xPINo;

	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, DATE(@PITgl), 'PI', MyPesanKu, 0, 0, xPINo, xUserID, '--', xGLValid);

	#Item
	IF (oNilaiHPPHGPRealCostPS) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PITgl), (@NoUrutku:=@NoUrutku + 1),'11150801',MyPesankuItem, oNilaiHPPHGPStandardCostPS,0;
	END IF;
	IF (oNilaiHPPOLIRealCostPS) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PITgl), (@NoUrutku:=@NoUrutku + 1),'11150802',MyPesankuItem, NilaiHPPOLIStandardCostPS,0;
	END IF;

	IF oPriceVariance > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PITgl), (@NoUrutku:=@NoUrutku + 1),'11200000',MyPesankuItem, ABS(oPriceVariance),0;
	ELSEIF oPriceVariance < 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PITgl), (@NoUrutku:=@NoUrutku + 1),'11200000',MyPesankuItem, 0, ABS(oPriceVariance);
	END IF;

	IF (oNilaiSelisihRealCostHGP + oNilaiSelisihRealCostOLI) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PITgl), (@NoUrutku:=@NoUrutku + 1),'11130500',MyPesankuItem, ABS((oNilaiSelisihRealCostHGP + oNilaiSelisihRealCostOLI)),0;
	ELSEIF (oNilaiSelisihRealCostHGP + oNilaiSelisihRealCostOLI) < 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PITgl), (@NoUrutku:=@NoUrutku + 1),'24052134',MyPesankuItem, 0, ABS((oNilaiSelisihRealCostHGP + oNilaiSelisihRealCostOLI));
	END IF;

	SELECT IFNULL(ttpshd.KodeTrans,'--'), IFNULL(DealerNama,'--') INTO MyTC, MyDealerNama
	FROM ttpiit
	INNER JOIN ttpshd ON ttpiit.PSNo = ttpshd.PSNo
	LEFT OUTER JOIN tddealer ON tddealer.DealerKode = ttpshd.DealerKode
	WHERE ttpiit.PINo = xPINo  GROUP BY KodeTrans;

	IF oPITotal > 0 THEN
		CASE MyTC
			WHEN "TC01" THEN INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PITgl), (@NoUrutku:=@NoUrutku + 1),'24010103',MyPesankuItem, 0,oPITotal;
			WHEN "TC02" THEN INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PITgl), (@NoUrutku:=@NoUrutku + 1),'24010200',MyPesankuItem, 0,oPITotal;
			WHEN "TC03" THEN INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PITgl), (@NoUrutku:=@NoUrutku + 1),'24020102',MyPesankuItem, 0,oPITotal;
			WHEN "TC05" THEN
				SELECT NoAccount INTO @HutangAfiliasi FROM traccount
				WHERE NamaAccount LIKE CONCAT('%',SUBSTRING_INDEX(TRIM(MyDealerNama), ' ', -1)) AND JenisAccount ='Detail' AND NoParent = '24020700';
				IF @HutangAfiliasi IS NULL THEN SET @HutangAfiliasi = '24020701'; END IF;
				INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PITgl), (@NoUrutku:=@NoUrutku + 1),@HutangAfiliasi,MyPesankuItem, 0,oPITotal;
			WHEN "TC06" THEN INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PITgl), (@NoUrutku:=@NoUrutku + 1),'24052131',MyPesankuItem, 0,oPITotal;
			WHEN "TC07" THEN INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PITgl), (@NoUrutku:=@NoUrutku + 1),'24052132',MyPesankuItem, 0,oPITotal;
			WHEN "TC08" THEN INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PITgl), (@NoUrutku:=@NoUrutku + 1),'24052132',MyPesankuItem, 0,oPITotal;
			WHEN "TC09" THEN INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PITgl), (@NoUrutku:=@NoUrutku + 1),'24010103',MyPesankuItem, 0,oPITotal;
			WHEN "TC10" THEN INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PITgl), (@NoUrutku:=@NoUrutku + 1),'24010900',MyPesankuItem, 0,oPITotal;
			WHEN "TC41" THEN INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PITgl), (@NoUrutku:=@NoUrutku + 1),'26010000',MyPesankuItem, 0,oPITotal;
		END CASE;
	END IF;

	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttPIhd SET NoGL = xNoGL  WHERE PINo = xPINo;
END$$

DELIMITER ;

