DELIMITER $$

DROP PROCEDURE IF EXISTS `fbkhd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fbkhd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xBKNo VARCHAR(12),
IN xBKNoBaru VARCHAR(12),
IN xBKTgl DATETIME,
IN xBKJenis VARCHAR(20),
IN xKodeTrans VARCHAR(4),
IN xPosKode VARCHAR(20),
IN xSupKode VARCHAR(15),
IN xBankKode VARCHAR(25),
IN xBKCekTempo DATE,
IN xBKAC VARCHAR(25),
IN xBKPerson VARCHAR(50),
IN xBKCekNo VARCHAR(25),
IN xBKNominal DECIMAL(14, 2),
IN xBKMemo VARCHAR(150),
IN xNoGL VARCHAR(10),
IN xCetak VARCHAR(5))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			DELETE FROM ttbkhd WHERE (BKNo LIKE '%=%') AND BKTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);	

			#ReJurnal Transaksi Tidak Memiliki Jurnal
			cek01: BEGIN
				DECLARE rBKNo VARCHAR(100) DEFAULT '';
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT BKNo FROM ttBKhd
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttbkhd.BKNo 
				WHERE ttgeneralledgerhd.NoGL IS NULL AND BKNo NOT LIKE '%=%' AND BKTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rBKNo;
					IF rBKNo IS NOT NULL AND rBKNo <> '' THEN
						CALL jBK(rBKNo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;
			SET oStatus = 0; SET oKeterangan = CONCAT("Daftar Bank Keluar");
		END;
		WHEN "Load" THEN
		BEGIN
			 SET oStatus = 0; SET oKeterangan = CONCAT("Load data Bank Keluar No: ", xBKNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SET @AutoNo = CONCAT('BK','01',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(BKNo,5)+1),5,0)) INTO @AutoNo FROM ttbkhd WHERE ((BKNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(BKNo) = 12) ORDER BY BKNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 12 THEN SET @AutoNo = CONCAT('BK','01',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xBKNo = @AutoNo;		
			SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru bank keluar No: ",xBKNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @BKNoOLD = xBKNo;	
			
			IF LOCATE('=',xBKNo) <> 0 THEN
				SET @AutoNo = CONCAT('BK',RIGHT(xBankKode,2),RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(BKNo,5)+1),5,0)) INTO @AutoNo FROM ttbkhd WHERE ((BKNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(BKNo) = 12) ORDER BY BKNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 12 THEN SET @AutoNo = CONCAT('BK',RIGHT(xBankKode,2),RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xBKNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xBKNoBaru = xBKNo;
			END IF;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			# Update 
			IF oStatus = 0 THEN 			
				UPDATE ttbkhd
				SET BKNo = xBKNoBaru, BKTgl = xBKTgl, BKJenis = xBKJenis, KodeTrans = xKodeTrans, PosKode = xPosKode, SupKode = xSupKode, BankKode = xBankKode, 
				BKCekTempo = xBKCekTempo, BKAC = xBKAC, BKPerson = xBKPerson, BKCekNo = xBKCekNo, BKNominal = xBKNominal, BKMemo = xBKMemo, NoGL = xNoGL, Cetak = xCetak, UserID = xUserID
				WHERE BKNo = xBKNo;
				SET xBKNo = xBKNoBaru;
				
				#BKAutoN
				IF xKodeTrans = 'UM' THEN
					UPDATE ttbkitcoa SET BKAutoN = -(3*BKAutoN) WHERE BKNo = xBKNo;
					SET @row_number = 0;
					DROP TEMPORARY TABLE IF EXISTS TNoUrut;
					CREATE TEMPORARY TABLE IF NOT EXISTS TNoUrut AS
					SELECT BKNo, BKAutoN,(@row_number:=@row_number + 1) AS NoUrut , NoAccount FROM ttbkitcoa
					WHERE BKNo = xBKNo ORDER BY BKAutoN DESC;
					UPDATE ttbkitcoa INNER JOIN TNoUrut
					ON  ttbkitcoa.BKNo = TNoUrut.BKNo
					AND ttbkitcoa.NoAccount = TNoUrut.NoAccount
					AND ttbkitcoa.BKAutoN = TNoUrut.BKAutoN
					SET ttbkitcoa.BKAutoN = TNoUrut.NoUrut;
					DROP TEMPORARY TABLE IF EXISTS TNoUrut;	
				END IF;
				
				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @BKNoOLD; #Hapus Jurnal No Transaksi Lama
				CALL jbk(xBKNo,xUserID); #Posting Jurnal
				
				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Bank Keluar','ttbkhd', xBKNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Bank Keluar No: ", xBKNo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Bank Keluar','ttbkhd', xBKNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Bank Keluar No: ", xBKNo);
				END IF;
			ELSEIF oStatus = 1 THEN
				IF LOCATE('=',xBKNo) <> 0 THEN
					DELETE FROM ttbkhd WHERE BKNo = xBKNo;
					SET oKeterangan = CONCAT("Batal menyimpan data Bank Keluar No: ", xBKNoBaru);
					SELECT IFNULL(BKNo,'--') INTO xBKNo FROM ttBKhd WHERE BKNo NOT LIKE '%=%' AND LEFT(BKNo,2) = 'BK' AND KodeTrans = xKodeTrans ORDER BY BKTgl DESC LIMIT 1; 
					IF xBKNo IS NULL OR LOCATE('=',xBKNo) <> 0 THEN SET xBKNo = '--'; END IF;
				END IF;			 			
				SET oKeterangan = oKeterangan;
			END IF;			
		END;		
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;
			
			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBKNo;
			IF @GLValid = 'Sudah' THEN	
				SET oStatus = 1; SET oKeterangan = CONCAT("No BK:", xBKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM ttbkhd WHERE BKNo = xBKNo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xBKNo;
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Bank Keluar','ttbkhd', xBKNo); 			
				SET oKeterangan = CONCAT("Berhasil menghapus data Bank Keluar No: ",xBKNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 IF LOCATE('=',xBKNo) <> 0 THEN
				DELETE FROM ttbkhd WHERE BKNo = xBKNo;
				SET oKeterangan = CONCAT("Batal menyimpan data Bank Keluar No: ", xBKNoBaru);
				SELECT IFNULL(BKNo,'--') INTO xBKNo FROM ttBKhd WHERE BKNo NOT LIKE '%=%' AND LEFT(BKNo,2) = 'BK' AND KodeTrans = xKodeTrans ORDER BY BKTgl DESC LIMIT 1; 
				IF xBKNo IS NULL OR LOCATE('=',xBKNo) <> 0 THEN SET xBKNo = '--'; END IF;
			 ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Bank Keluar No: ", xBKNo);
			 END IF;
		END;
		WHEN "Jurnal" THEN
		BEGIN
			 IF LOCATE('=',xBKNo) <> 0 THEN /* Data Baru, belum tersimpan tidak boleh di jurnal */
				  SET oStatus = 1;
				  SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			 ELSE
				  CALL jbk(xBKNo,xUserID);
				  SET oStatus = 0;
				  SET oKeterangan = CONCAT("Berhasil memproses Jurnal Bank Keluar No: ", xBKNo);
			 END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
		END;
		WHEN "LoopAfter" THEN
		BEGIN
			SELECT SUM(BMBayar) INTO @sumBMBayar FROM ttbmit WHERE BMNo = xBMNo;
			IF @sumBMBayar IS NOT NULL THEN
				UPDATE ttbmhd SET BMNominal = @sumBMBayar WHERE BMNo = xBMNo AND BMNominal <= @sumBMBayar;
			END IF;
		END;
	END CASE;
END$$

DELIMITER ;

