DELIMITER $$

DROP PROCEDURE IF EXISTS `fsihd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fsihd`(IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xJenis VARCHAR(10),
INOUT xSINo VARCHAR(10),
IN xSINoBaru VARCHAR(10),
IN xSITgl DATETIME,
IN xSONo VARCHAR(10),
IN xSDNo VARCHAR(25),
IN xSIJenis VARCHAR(6),
IN xPosKode VARCHAR(20),
IN xKodeTrans VARCHAR(4),
IN xKarKode VARCHAR(10),
IN xPrgNama VARCHAR(25),
IN xMotorNoPolisi VARCHAR(12),
IN xDealerKode VARCHAR(10),
IN xSISubTotal DECIMAL(14, 2),
IN xSIDiscFinal DECIMAL(14, 2),
IN xSITotalPajak DECIMAL(14, 2),
IN xSIBiayaKirim DECIMAL(14, 2),
IN xSIUangMuka DECIMAL(14, 2),
IN xSITotal DECIMAL(14, 2),
IN xSITOP DECIMAL(3, 2),
IN xSITerbayar DECIMAL(14, 2),
IN xSIKeterangan VARCHAR(150),
IN xNoGL VARCHAR(10),
IN xCetak VARCHAR(5))

BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
         DELETE FROM ttsihd WHERE (SINo LIKE '%=%') AND SITgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);	

			#ReJurnal Transaksi Tidak Memiliki Jurnal
			cek01: BEGIN
   			DECLARE rSINo VARCHAR(100) DEFAULT '';
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT SINo FROM ttSIhd
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttSIhd.SINo 
				WHERE ttgeneralledgerhd.NoGL IS NULL AND SITgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rSINo;
					IF rSINo IS NOT NULL AND rSINo <> '' THEN
						CALL jSI(rSINo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;			

         CALL cUPDATEtdbaranglokasi(); 		
				
			SET oStatus = 0;SET oKeterangan = CONCAT("Daftar Invoice Penjualan [SI] ");
		END;
		WHEN "Load" THEN
		BEGIN
			SET oStatus = 0; SET oKeterangan = CONCAT("Load data Invoice Penjualan [SI] No: ", xSINo);
		END;
		WHEN "Insert" THEN
		BEGIN
			 SET @AutoNo = CONCAT('SI',RIGHT(YEAR(NOW()),2),'-');
			 SELECT CONCAT(@AutoNo,LPAD((RIGHT(SINo,5)+1),5,0)) INTO @AutoNo FROM ttsihd WHERE ((SINo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(SINo) = 10) ORDER BY SINo DESC LIMIT 1;
			 IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('SI',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			 SET xSINo = @AutoNo;
			 SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Invoice Penjualan [SI] No: ",xSINo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @SINoOLD = xSINo;
					
			IF LOCATE('=',xSINo) <> 0 THEN 
				SET @AutoNo = CONCAT('SI',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(SINo,5)+1),5,0)) INTO @AutoNo FROM ttsihd WHERE ((SINo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(SINo) = 10) ORDER BY SINo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('SI',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xSINoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xSINoBaru = xSINo;
			END IF;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSINo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSINo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			IF oStatus = 0 THEN 
				UPDATE ttsihd
				SET SINo = xSINoBaru, SITgl = xSITgl,LokasiKode = xLokasiKode, SONo = xSONo, SDNo = xSDNo, SIJenis = xSIJenis, PosKode = xPosKode, KodeTrans = xKodeTrans, KarKode = xKarKode, PrgNama = xPrgNama, MotorNoPolisi = xMotorNoPolisi, DealerKode = xDealerKode, SISubTotal = xSISubTotal, SIDiscFinal = xSIDiscFinal, SITotalPajak = xSITotalPajak, SIBiayaKirim = xSIBiayaKirim, SIUangMuka = xSIUangMuka, SITotal = xSITotal, SITOP = xSITOP, SITerbayar = xSITerbayar, SIKeterangan = xSIKeterangan, NoGL = xNoGL, Cetak = xCetak, UserID = xUserID
				WHERE SINo = xSINo;
				SET xSINo = xSINoBaru;

				#AutoNumber
				UPDATE ttSIit SET SIAuto = -(3*SIAuto) WHERE SINo = xSINo;
				SET @row_number = 0;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;
				CREATE TEMPORARY TABLE IF NOT EXISTS TNoUrut AS
				SELECT SINo, SIAuto,(@row_number:=@row_number + 1) AS NoUrut , BrgKode FROM ttSIit
				WHERE SINo = xSINo ORDER BY SIAuto DESC;
				UPDATE ttSIit INNER JOIN TNoUrut
				ON  ttSIit.SINo = TNoUrut.SINo
				AND ttSIit.BrgKode = TNoUrut.BrgKode
				AND ttSIit.SIAuto = TNoUrut.SIAuto
				SET ttSIit.SIAuto = TNoUrut.NoUrut;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;
				
            #Hitung SubTotal & Total
            SELECT SUM((SIQty * SIHrgJual) - SIDiscount) INTO @SubTotal FROM ttSIit WHERE SINo = xSINo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttSIhd SET SISubTotal = @SubTotal, SITotal = (@SubTotal - SIDiscFinal -  SITotalPajak - SIBiayaKirim  - SIUangMuka) WHERE SINo = xSINo;
            END IF;

				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @SINoOLD; #Hapus Jurnal No Transaksi Lama
				CALL jsi(xSINo,xUserID); 
			  
  				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Invoice Penjualan [SI]','ttsihd', xSINo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Invoice Penjualan [SI] No: ", xSINo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Invoice Penjualan [SI]','ttsihd', xSINo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Invoice Penjualan [SI] No: ", xSINo);
				END IF;			
			ELSEIF oStatus = 1 THEN
   			IF LOCATE('=',xSINo) <> 0 THEN
   				DELETE FROM ttsihd WHERE SINo = xSINo;
   				SELECT IFNULL(SINo,'--') INTO xSINo FROM ttSIhd WHERE SINo NOT LIKE '%=%' AND LEFT(SINo,2) = 'SI' ORDER BY SITgl DESC LIMIT 1; 
   				IF xSINo IS NULL OR LOCATE('=',xSINo) <> 0 THEN SET xSINo = '--'; END IF;
   			END IF;
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;

 			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSINo;
			IF @GLValid = 'Sudah' THEN	
				SET oStatus = 1; SET oKeterangan = CONCAT("No SI:", xSINo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM ttSIhd WHERE SINo = xSINo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xSINo;
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Invoice Penjualan [SI] No: ",xSINo);
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Invoice Penjualan [SI]','ttsihd', xSINo); 			
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;	
		END;
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xSINo) <> 0 THEN
				DELETE FROM ttsihd WHERE SINo = xSINo;
				SET oKeterangan = CONCAT("Batal menyimpan data Invoice Penjualan [SI] No: ", xSINoBaru);
				SELECT IFNULL(SINo,'--') INTO xSINo FROM ttSIhd WHERE SINo NOT LIKE '%=%' AND LEFT(SINo,2) = 'SI' ORDER BY SITgl DESC LIMIT 1; 
				IF xSINo IS NULL OR LOCATE('=',xSINo) <> 0 THEN SET xSINo = '--'; END IF;
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Invoice Penjualan [SI] No: ", xSINo);
			END IF;
		END;
		WHEN "Jurnal" THEN
		BEGIN
			 IF LOCATE('=',xSINo) <> 0 THEN 
				SET oStatus = 1; SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			 ELSE
				  CALL jsi(xSINo,xUserID);
				  SET oStatus = 0;
				  SET oKeterangan = CONCAT("Berhasil memproses Jurnal Invoice Penjualan [SI] No: ", xSINo);
			 END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
		    set oKeterangan = xCetak;
		    If xCetak = "SO" Then
                SELECT SONo into @A FROM ttsohd WHERE SINo = xSINo AND SONo <> xSONo;
                if isnull(@A) Then
                    UPDATE ttsohd SET SINo = '--' WHERE SONo IN (SELECT  SONo FROM ttsiit WHERE SINo = xSINo GROUP BY SONO);
                    DELETE FROM ttsiit WHERE SINo = xSINo;
                end if;
                UPDATE ttsohd SET SINo = xSINo WHERE SONo = xSONo;
                UPDATE ttsihd set SONo = xSONo WHERE SINo = xSINo;
            End if;
		    If xCetak = "SD" Then
                UPDATE ttsihd set SDNo = xSDNo WHERE SINo = xSINo;
            end if;
		END;		
	END CASE;
END$$

DELIMITER ;

