DELIMITER $$

DROP PROCEDURE IF EXISTS `fprit`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fprit`(
    IN xAction VARCHAR(150),
    OUT oStatus INT,
    OUT oKeterangan TEXT,
    IN xUserID VARCHAR(15),
    IN xKodeAkses VARCHAR(15),
    IN xLokasiKode VARCHAR(15),
    IN xPRNo VARCHAR(10),
    IN xPRAuto SMALLINT(6),
    IN xBrgKode VARCHAR(20),
    IN xPRQty DECIMAL(5,0),
    IN xPRHrgBeli DECIMAL(14,2),
    IN xPRHrgJual DECIMAL(14,2),
    IN xPRDiscount DECIMAL(14,2),
    IN xPRPajak DECIMAL(2,0),
    IN xPRNoOLD VARCHAR(10),
    IN xPRAutoOLD SMALLINT(6),
    IN xBrgKodeOLD VARCHAR(20)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xPRNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xPRNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Mengambil Harga Beli
			SELECT BrgHrgBeli INTO xPRHrgBeli FROM tdbarang WHERE BrgKode = xBrgKode;

			#Insert
			IF oStatus = 0 THEN		
				SELECT IF(ISNULL((MAX(PRAuto)+1)),1,(MAX(PRAuto)+1)) INTO xPRAuto FROM ttprit WHERE PRNo = xPRNo; #AutoN					 
				INSERT INTO ttprit
				(PRNo, PRAuto, BrgKode, PRQty, PRHrgBeli, PRHrgJual, PRDiscount, PRPajak, LokasiKode) VALUES
				(xPRNo, xPRAuto, xBrgKode, xPRQty, xPRHrgBeli, xPRHrgJual, xPRDiscount, xPRPajak, xLokasiKode);

            #Hitung SubTotal , Total
            SELECT SUM((PRQty * PRHrgJual) - PRDiscount) INTO @SubTotal FROM ttPRit WHERE PRNo = xPRNo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttPRhd set PRSubTotal = @SubTotal, PRTotal = (@SubTotal - PRDiscFinal -  PRTotalPajak - PRBiayaKirim) WHERE PRNo = xPRNo;
            END IF;

				SET oKeterangan = CONCAT('Berhasil menambahkan data Barang ', xBrgKode, ' = ' , FORMAT(xPRQty,0));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;	
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
		 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xPRNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xPRNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Jika Beda Kode Barang, ambil Harga Beli
			IF xBrgKode <> xBrgKodeOLD THEN
				SELECT BrgHrgBeli INTO xPRHrgBeli FROM tdbarang WHERE BrgKode = xBrgKode;
			END IF;
			 
			IF oStatus = 0 THEN
				UPDATE ttprit
				SET PRNo = xPRNo, PRAuto = xPRAuto, BrgKode = xBrgKode, PRQty = xPRQty, PRHrgBeli = xPRHrgBeli, PRHrgJual = xPRHrgJual, PRDiscount = xPRDiscount, PRPajak = xPRPajak, LokasiKode = xLokasiKode
				WHERE PRNo = xPRNoOLD AND PRAuto = xPRAutoOLD AND BrgKode = xBrgKodeOLD;

            #Hitung SubTotal , Total
            SELECT SUM((PRQty * PRHrgJual) - PRDiscount) INTO @SubTotal FROM ttPRit WHERE PRNo = xPRNo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttPRhd set PRSubTotal = @SubTotal, PRTotal = (@SubTotal - PRDiscFinal -  PRTotalPajak - PRBiayaKirim) WHERE PRNo = xPRNo;
            END IF;

				SET oKeterangan = CONCAT("Berhasil mengubah data Barang : ", xBrgKode, ' = ' , FORMAT(xPRQty,0));
			 ELSE
				  SET oKeterangan = oKeterangan;
			 END IF;		 
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xPRNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xPRNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM ttprit WHERE PRAuto = xPRAuto AND PRNo = xPRNo AND BrgKode = xBrgKode;

            #Hitung SubTotal , Total
            SELECT SUM((PRQty * PRHrgJual) - PRDiscount) INTO @SubTotal FROM ttPRit WHERE PRNo = xPRNo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttPRhd set PRSubTotal = @SubTotal, PRTotal = (@SubTotal - PRDiscFinal -  PRTotalPajak - PRBiayaKirim) WHERE PRNo = xPRNo;
            END IF;

				SET oKeterangan = CONCAT("Berhasil menghapus data Barang : ", xBrgKode, ' = ' , FORMAT(xPRQty,0));
			ELSE
				SET oKeterangan = CONCAT("Gagal menghapus data Barang : ", xBrgKode, ' = ' , FORMAT(xPRQty,0));
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Batal menyimpan data Barang: ", xPRAuto );
		END;
	END CASE;
END$$

DELIMITER ;

