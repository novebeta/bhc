DELIMITER $$

DROP PROCEDURE IF EXISTS `fkkit`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fkkit`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xKKNo VARCHAR(12),
IN xKKLink VARCHAR(18),
IN xKKBayar DECIMAL(14, 2),
IN xKKNoOLD VARCHAR(12),
IN xKKLinkOLD VARCHAR(18)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert
			IF oStatus = 0 THEN		
				INSERT INTO ttkkit (KKNo, KKLink, KKBayar) VALUES (xKKNo, xKKLink, xKKBayar);

            SELECT SUM(KKBayar) INTO @KKBayar FROM ttkkit WHERE KKNo = xKKNo; 
            IF @KKBayar IS NOT NULL THEN UPDATE ttkkhd set KKNominal = @KKBayar WHERE KKNo = xKKNo; END IF;

				SET oKeterangan = CONCAT('Berhasil menambahkan data Kas Keluar ', xKKNo,' - ', xKKLink, ' - ', FORMAT(xKKBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Update
			IF oStatus = 0 THEN
            UPDATE ttkkit SET KKNo = xKKNo, KKLink = xKKLink, KKBayar = xKKBayar WHERE KKNo = xKKNoOLD AND KKLink = xKKLinkOLD;
				
            SELECT SUM(KKBayar) INTO @KKBayar FROM ttkkit WHERE KKNo = xKKNo; 
            IF @KKBayar IS NOT NULL THEN UPDATE ttkkhd set KKNominal = @KKBayar WHERE KKNo = xKKNo; END IF;

            SET oKeterangan = CONCAT("Berhasil mengubah data Kas Keluar : ", xKKNo , ' - ' ,xKKLink);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN		
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Delete
			IF oStatus = 0 THEN
				DELETE FROM ttkkit WHERE KKLink = xKKLink AND KKNo = xKKNo;

            SELECT SUM(KKBayar) INTO @KKBayar FROM ttkkit WHERE KKNo = xKKNo; 
            IF @KKBayar IS NOT NULL THEN UPDATE ttkkhd set KKNominal = @KKBayar WHERE KKNo = xKKNo; END IF;

				SET oKeterangan = CONCAT("Berhasil menghapus data Kas Keluar : ", xKKNo , ' - ' ,xKKLink);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Batal menyimpan data Kas Keluar: ", xKKLink );
		END;
	END CASE;
END$$

DELIMITER ;

