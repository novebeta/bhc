DELIMITER $$

DROP PROCEDURE IF EXISTS `jsv`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jsv`(IN xSVNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
  DECLARE MyPesanKu VARCHAR(300) DEFAULT '';
  DECLARE MyPesankuItem VARCHAR(150) DEFAULT '';

  DECLARE oNilaiHGP, oNilaiOLI DOUBLE DEFAULT 0;
  DECLARE oPotonganHGP, oPotonganOLI DOUBLE DEFAULT 0;

  DECLARE oNilaiJasa, oPotonganJasa, oNilaiHPPHGP,oNilaiHPPOLI,oSDDiscFinal  DOUBLE DEFAULT 0;

  SET @NoUrutku = 0;

	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xSVNo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;

	SELECT SDNo, SVNo, SENo, NoGLSD, NoGLSV, SDTgl, SVTgl, KodeTrans, ASS, PosKode, KarKode, PrgNama, PKBNo, MotorNoPolisi, LokasiKode, SDNoUrut, SDJenis, SDTOP, SDDurasi, SDJamMasuk, SDJamSelesai, SDJamDikerjakan, SDTotalWaktu, SDStatus, SDKmSekarang, SDKmBerikut, SDKeluhan, SDKeterangan, SDPembawaMotor, SDHubunganPembawa, SDAlasanServis, SDTotalJasa, SDTotalQty, SDTotalPart, SDTotalGratis, SDTotalNonGratis, SDDiscFinal, SDTotalPajak, SDUangMuka, SDTotalBiaya, SDKasKeluar, SDTerbayar, KlaimKPB, KlaimC2, Cetak, UserID
	INTO  @SDNo,@SVNo,@SENo,@NoGLSD,@NoGLSV,@SDTgl,@SVTgl,@KodeTrans,@ASS,@PosKode,@KarKode,@PrgNama,@PKBNo,@MotorNoPolisi,@LokasiKode,@SDNoUrut,@SDJenis,@SDTOP,@SDDurasi,@SDJamMasuk,@SDJamSelesai,@SDJamDikerjakan,@SDTotalWaktu,@SDStatus,@SDKmSekarang,@SDKmBerikut,@SDKeluhan,@SDKeterangan,@SDPembawaMotor,@SDHubunganPembawa,@SDAlasanServis,@SDTotalJasa,@SDTotalQty,@SDTotalPart,@SDTotalGratis,@SDTotalNonGratis,oSDDiscFinal,@SDTotalPajak,@SDUangMuka,@SDTotalBiaya,@SDKasKeluar,@SDTerbayar,@KlaimKPB,@KlaimC2,@Cetak,@UserID
	FROM ttSDhd	WHERE SVNo = xSVNo;

  SELECT CusNama INTO @CusNama FROM tdcustomer WHERE MotorNoPolisi = @MotorNoPolisi;
  IF @CusNama IS NULL THEN SET @CusNama = ''; END IF;

   SET MyPesanKu = CONCAT("Post Service Daftar: " , @SVNo , " - " , @KodeTrans , " - ", @ASS, ' - ',@MotorNoPolisi , ' - ',@CusNama );
   SET MyPesanKu = TRIM(MyPesanKu);  IF LENGTH(MyPesanKu) > 300 THEN SET MyPesanKu = SUBSTRING(MyPesanKu,0, 299); END IF;
   IF LENGTH(MyPesanku) > 150 THEN SET MyPesankuItem = SUBSTRING(MyPesanku,0, 149); ELSE SET MyPesankuItem = MyPesanku; END IF;

	IF xNoGL = '--' OR xNoGL <> @NoGLSV THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGLSV;
	END IF;

	SELECT IFNULL(SUM(ttsditbarang.SDHrgJual * ttsditbarang.SDQty),0) INTO oNilaiHGP FROM ttsditbarang
	INNER JOIN tdbarang ON ttsditbarang.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) <> 'OLI' AND ttsditbarang.SDNo = @SDNo ;

	SELECT IFNULL(SUM(ttsditbarang.SDHrgJual * ttsditbarang.SDQty),0) INTO oNilaiOLI FROM ttsditbarang
	INNER JOIN tdbarang ON ttsditbarang.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) = 'OLI' AND ttsditbarang.SDNo = @SDNo ;

	SELECT IFNULL(SUM(SDJualDisc),0) INTO oPotonganHGP FROM ttsditbarang
	INNER JOIN tdbarang ON ttsditbarang.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) <> 'OLI' AND ttsditbarang.SDNo = @SDNo ;

	SELECT IFNULL(SUM(SDJualDisc),0) INTO oPotonganOLI FROM ttsditbarang
	INNER JOIN tdbarang ON ttsditbarang.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) = 'OLI' AND ttsditbarang.SDNo = @SDNo ;

	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL; DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;DELETE FROM ttglhpit WHERE NoGL = xNoGL;
  DELETE FROM ttgeneralledgerhd WHERE GLLink = @SDNo;

	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, DATE(@SVTgl), 'SV', MyPesanKu, 0, 0, xSVNo, xUserID, '--', xGLValid);

	#Item
	SELECT IFNULL(SUM(SDQty * SDHrgJual - SDJualDisc),0) INTO @SDTotalPart FROM ttsditbarang WHERE SDNo = @SDNo;
	SELECT IFNULL(SUM(SDHrgJual - SDJualDisc),0) INTO @SDTotalJasa FROM ttsditjasa WHERE SDNo = @SDNo;

  IF @KodeTrans = 'TC14' THEN
    IF @SDTotalPart > 0 THEN
      INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SVTgl), (@NoUrutku:=@NoUrutku + 1),'11060201',MyPesankuItem, IFNULL(@SDTotalPart,0) ,0;
    END IF;
    IF @SDTotalJasa > 0 THEN
      INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SVTgl), (@NoUrutku:=@NoUrutku + 1),'11060202',MyPesankuItem, IFNULL(@SDTotalJasa,0) ,0;
    END IF;
  ELSEIF @KodeTrans = 'TC13' THEN
    IF @SDTotalPart > 0 THEN
      INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SVTgl), (@NoUrutku:=@NoUrutku + 1),'11060301',MyPesankuItem, IFNULL(@SDTotalPart,0) ,0;
    END IF;
    IF @SDTotalJasa > 0 THEN
      INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SVTgl), (@NoUrutku:=@NoUrutku + 1),'11060302',MyPesankuItem, IFNULL(@SDTotalJasa,0) ,0;
    END IF;
  ELSE
    IF @SDTotalBiaya > 0 THEN
      INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SVTgl), (@NoUrutku:=@NoUrutku + 1),'11060100',MyPesankuItem, IFNULL(@SDTotalBiaya,0) ,0;
    END IF;
    IF @SDUangMuka > 0 THEN
      INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SVTgl), (@NoUrutku:=@NoUrutku + 1),'24052000',MyPesankuItem, IFNULL(@SDUangMuka,0) ,0;
    END IF;
  END IF;

	IF (oNilaiHGP) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SVTgl), (@NoUrutku:=@NoUrutku + 1),'30050100',MyPesankuItem,0, oNilaiHGP ;
	END IF;
	IF (oNilaiOLI) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SVTgl), (@NoUrutku:=@NoUrutku + 1),'30060100',MyPesankuItem,0, oNilaiOLI;
	END IF;

  IF (oPotonganHGP) > 0 THEN
    INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SVTgl), (@NoUrutku:=@NoUrutku + 1),'30090101',MyPesankuItem,oPotonganHGP, 0 ;
  END IF;
  IF (oPotonganOLI) > 0 THEN
    INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SVTgl), (@NoUrutku:=@NoUrutku + 1),'30090201',MyPesankuItem,oPotonganOLI, 0 ;
  END IF;

  SELECT IFNULL(SUM(SDHrgJual),0) INTO oNilaiJasa FROM ttsditjasa WHERE SDNo = @SDNo;
  IF (oNilaiJasa) > 0 THEN
    IF @KodeTrans = 'TC14' THEN
      INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SVTgl), (@NoUrutku:=@NoUrutku + 1),'40050000',MyPesankuItem, 0, oNilaiJasa  ;
    ELSEIF @KodeTrans = 'TC13' THEN
      INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SVTgl), (@NoUrutku:=@NoUrutku + 1),'40060000',MyPesankuItem, 0, oNilaiJasa  ;
    ELSE
      INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SVTgl), (@NoUrutku:=@NoUrutku + 1),'30070000',MyPesankuItem, 0, oNilaiJasa  ;
    END IF;
  END IF;

  SELECT IFNULL(SUM(SDJualDisc),0) INTO oPotonganJasa FROM ttsditjasa WHERE SDNo = @SDNo;
  IF (oPotonganJasa) > 0 THEN
      INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SVTgl), (@NoUrutku:=@NoUrutku + 1),'30090300',MyPesankuItem, PotonganJasa, 0  ;
  END IF;

  SELECT IFNULL(SUM(SDJualDisc),0) INTO oPotonganJasa FROM ttsditjasa WHERE SDNo = @SDNo;

	SELECT IFNULL(SUM(ttsditbarang.SDHrgBeli * ttsditbarang.SDQty),0) INTO oNilaiHPPHGP
  FROM ttsditbarang
	INNER JOIN tdbarang ON ttsditbarang.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) <> 'OLI' AND ttsditbarang.SDNo = @SDNo ;

	SELECT IFNULL(SUM(ttsditbarang.SDHrgBeli * ttsditbarang.SDQty),0) INTO oNilaiHPPOLI
  FROM ttsditbarang
	INNER JOIN tdbarang ON ttsditbarang.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) = 'OLI' AND ttsditbarang.SDNo = @SDNo ;

  IF (oNilaiHPPHGP) > 0 THEN
      INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SVTgl), (@NoUrutku:=@NoUrutku + 1),'50010100',MyPesankuItem, oNilaiHPPHGP, 0  ;
  END IF;
  IF (oNilaiHPPOLI) > 0 THEN
      INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SVTgl), (@NoUrutku:=@NoUrutku + 1),'50020100',MyPesankuItem, oNilaiHPPOLI, 0  ;
  END IF;
  IF (oNilaiHPPHGP) > 0 THEN
      INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SVTgl), (@NoUrutku:=@NoUrutku + 1),'11150601',MyPesankuItem, 0, oNilaiHPPHGP  ;
  END IF;
  IF (oNilaiHPPOLI) > 0 THEN
      INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SVTgl), (@NoUrutku:=@NoUrutku + 1),'11150701',MyPesankuItem, 0, oNilaiHPPOLI  ;
  END IF;
  IF oSDDiscFinal > 0 THEN
      INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SVTgl), (@NoUrutku:=@NoUrutku + 1),'30090400',MyPesankuItem, oSDDiscFinal, 0;
  END IF;

	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttSDhd SET NoGLSV = xNoGL  WHERE SVNo = xSVNo;
END$$

DELIMITER ;

