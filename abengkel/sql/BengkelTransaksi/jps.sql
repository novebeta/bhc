DELIMITER $$

DROP PROCEDURE IF EXISTS `jps`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jps`(IN xPSNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE MyPesanKu VARCHAR(300) DEFAULT '';
	DECLARE MyPesankuItem VARCHAR(150) DEFAULT '';
	DECLARE oNilaiHGP DOUBLE DEFAULT 0;
	DECLARE oNilaiOLI DOUBLE DEFAULT 0;

	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xPSNo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;

	SELECT PSNo, PSTgl, PINo, PONo, TINo, KodeTrans, PosKode, SupKode, DealerKode, LokasiKode, PSNoRef, PSSubTotal, PSDiscFinal, PSTotalPajak, PSBiayaKirim, PSTotal, PSTerbayar, PSKeterangan, NoGL, Cetak, UserID
	INTO  @PSNo,@PSTgl,@PINo,@PONo,@TINo,@KodeTrans,@PosKode,@SupKode,@DealerKode,@LokasiKode,@PSNoRef,@PSSubTotal,@PSDiscFinal,@PSTotalPajak,@PSBiayaKirim,@PSTotal,@PSTerbayar,@PSKeterangan,@NoGL,@Cetak,@UserID
	FROM ttPShd	WHERE PSNo = xPSNo;

	SET MyPesanKu = CONCAT("Post Penerimaan Barang : " , @PSNo , " - " , @KodeTrans , " - ", @SupKode);
	SET MyPesanKu = TRIM(MyPesanKu);  IF LENGTH(MyPesanKu) > 300 THEN SET MyPesanKu = SUBSTRING(MyPesanKu,0, 299); END IF;
	IF LENGTH(MyPesanku) > 150 THEN SET MyPesankuItem = SUBSTRING(MyPesanku,0, 149); ELSE SET MyPesankuItem = MyPesanku; END IF;

	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;

	SELECT IFNULL((ttpsit.PSQty * ttpsit.PSHrgJual - ttpsit.PSDiscount),0) AS Jumlah INTO oNilaiHGP FROM ttpsit
	INNER JOIN tdbarang ON tdbarang.BrgKode = ttpsit.BrgKode
	WHERE UPPER(BrgGroup) <> 'OLI' AND PSNo = xPSNo ;

	SELECT IFNULL((ttpsit.PSQty * ttpsit.PSHrgJual - ttpsit.PSDiscount),0) AS Jumlah INTO oNilaiOLI FROM ttpsit
	INNER JOIN tdbarang ON tdbarang.BrgKode = ttpsit.BrgKode
	WHERE UPPER(BrgGroup) = 'OLI' AND PSNo = xPSNo ;

	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL; DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;DELETE FROM ttglhpit WHERE NoGL = xNoGL;
	DELETE FROM ttgeneralledgerhd WHERE GLLink = xPSNo;

	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, DATE(@PSTgl), 'PS', MyPesanKu, 0, 0, xPSNo, xUserID, '--', xGLValid);

	#Item
	SET @NoUrutku = 0;
	
	#11130201 Piutang MD Claim C2 Part/Olie
	IF (oNilaiHGP) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PSTgl), (@NoUrutku:=@NoUrutku + 1),'11150602',MyPesankuItem,oNilaiHGP, 0;
	END IF;
	IF (oNilaiOLI) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PSTgl), (@NoUrutku:=@NoUrutku + 1),'11150702',MyPesankuItem,NilaiOLI, 0;
	END IF;

	IF @KodeTrans <> 'TC09' THEN
		IF (oNilaiHGP) > 0 THEN
			INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PSTgl), (@NoUrutku:=@NoUrutku + 1),'11150801',MyPesankuItem, 0,oNilaiHGP;
		END IF;
		IF (oNilaiOLI) > 0 THEN
			INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PSTgl), (@NoUrutku:=@NoUrutku + 1),'11150802',MyPesankuItem, 0,oNilaiOLI;
		END IF;
	ELSE
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@PSTgl), (@NoUrutku:=@NoUrutku + 1),'11130201',MyPesankuItem, 0,oNilaiOLI + oNilaiHGP;
	END IF;

	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttPShd SET NoGL = xNoGL  WHERE PSNo = xPSNo;
END$$

DELIMITER ;

