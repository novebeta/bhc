DELIMITER $$

DROP PROCEDURE IF EXISTS `fsrhd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fsrhd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xSRNo VARCHAR(10),
IN xSRNoBaru VARCHAR(10),
IN xSRTgl DATETIME,
IN xSINo VARCHAR(10),
IN xKodeTrans VARCHAR(4),
IN xSRSubTotal DECIMAL(14, 2),
IN xPosKode VARCHAR(20),
IN xKarKode VARCHAR(10),
IN xMotorNoPolisi VARCHAR(12),
IN xSRDiscFinal DECIMAL(14, 2),
IN xSRTotalPajak DECIMAL(14, 2),
IN xSRBiayaKirim DECIMAL(14, 2),
IN xSRTotal DECIMAL(14, 2),
IN xSRTerbayar DECIMAL(14, 2),
IN xSRKeterangan VARCHAR(150),
IN xNoGL VARCHAR(10),
IN xCetak VARCHAR(5))

BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
         DELETE FROM ttsrhd WHERE (SRNo LIKE '%=%') AND SRTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);	
			#ReJurnal Transaksi Tidak Memiliki Jurnal
			cek01: BEGIN
   			DECLARE rSRNo VARCHAR(100) DEFAULT '';
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT SRNo FROM ttSRhd
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttSRhd.SRNo 
				WHERE ttgeneralledgerhd.NoGL IS NULL AND SRTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rSRNo;
					IF rSRNo IS NOT NULL AND rSRNo <> '' THEN
						CALL jSR(rSRNo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;	
         CALL cUPDATEtdbaranglokasi(); 		
			SET oStatus = 0;SET oKeterangan = CONCAT("Daftar Retur Penjualan [SR] ");
		END;
		WHEN "Load" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Load data Retur Penjualan [SR] No: ", xSRNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SET @AutoNo = CONCAT('SR',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(SRNo,5)+1),5,0)) INTO @AutoNo FROM ttsrhd WHERE ((SRNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(SRNo) = 10) ORDER BY SRNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('SR',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xSRNo = @AutoNo;
			SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Retur Penjualan [SR] No: ",xSRNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @SRNoOLD = xSRNo;	
			
			IF LOCATE('=',xSRNo) <> 0  THEN 
				SET @AutoNo = CONCAT('SR',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(SRNo,5)+1),5,0)) INTO @AutoNo FROM ttSRhd WHERE ((SRNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(SRNo) = 10) ORDER BY SRNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('SR',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xSRNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xSRNoBaru = xSRNo;
			END IF;
		
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSRNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSRNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN 
				UPDATE ttsrhd
				SET SRNo = xSRNoBaru, SRTgl = xSRTgl, SINo = xSINo, KodeTrans = xKodeTrans, LokasiKode = xLokasiKode, SRSubTotal = xSRSubTotal, PosKode = xPosKode, KarKode = xKarKode, MotorNoPolisi = xMotorNoPolisi, 
				SRDiscFinal = xSRDiscFinal, SRTotalPajak = xSRTotalPajak, SRBiayaKirim = xSRBiayaKirim, SRTotal = xSRTotal, SRTerbayar = xSRTerbayar, SRKeterangan = xSRKeterangan, NoGL = xNoGL, Cetak = xCetak, UserID = xUserID
				WHERE SRNo = xSRNo;
				SET xSRNo = xSRNoBaru;

				#AutoNumber
				UPDATE ttSRit SET SRAuto = -(3*SRAuto) WHERE SRNo = xSRNo;
				SET @row_number = 0;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;
				CREATE TEMPORARY TABLE IF NOT EXISTS TNoUrut AS
				SELECT SRNo, SRAuto,(@row_number:=@row_number + 1) AS NoUrut , BrgKode FROM ttSRit
				WHERE SRNo = xSRNo ORDER BY SRAuto DESC;
				UPDATE ttSRit INNER JOIN TNoUrut
				ON  ttSRit.SRNo = TNoUrut.SRNo
				AND ttSRit.BrgKode = TNoUrut.BrgKode
				AND ttSRit.SRAuto = TNoUrut.SRAuto
				SET ttSRit.SRAuto = TNoUrut.NoUrut;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;

            #Hitung SubTotal & Total
            SELECT SUM((SRQty * SRHrgJual) - SRDiscount) INTO @SubTotal FROM ttsrit WHERE SRNo = xSRNo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttsrhd set SRSubTotal = @SubTotal, SRTotal = (@SubTotal - SRDiscFinal -  SRTotalPajak - SRBiayaKirim) WHERE SRNo = xSRNo;
            END IF;
															
				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @SRNoOLD; #Hapus Jurnal No Transaksi Lama
				CALL jSR(xSRNo,xUserID); 

  				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Retur Penjualan [SR]','ttsrhd', xSRNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Retur Pembelian [SR] No: ", xSRNo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Retur Penjualan [SR]','ttsrhd', xSRNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Retur Pembelian [SR] No: ", xSRNo);
				END IF;
			ELSEIF oStatus = 1 THEN
   			IF LOCATE('=',xSRNo) <> 0 THEN
   				DELETE FROM ttsrhd WHERE SRNo = xSRNo;
   				SELECT IFNULL(SRNo,'--') INTO xSRNo FROM ttSRhd WHERE SRNo NOT LIKE '%=%' AND LEFT(SRNo,2) = 'SR' ORDER BY SRTgl DESC LIMIT 1; 
   				IF xSRNo IS NULL OR LOCATE('=',xSRNo) <> 0 THEN SET xSRNo = '--'; END IF;
   			END IF;
            SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;

 			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSRNo;
			IF @GLValid = 'Sudah' THEN	
				SET oStatus = 1; SET oKeterangan = CONCAT("No SR:", xSRNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM ttSRhd WHERE SRNo = xSRNo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xSRNo;
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Retur Penjualan [SR] No: ",xSRNo);
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Retur Penjualan [SR]','ttsrhd', xSRNo); 			
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xSRNo) <> 0 THEN
				DELETE FROM ttsrhd WHERE SRNo = xSRNo;
				SELECT IFNULL(SRNo,'--') INTO xSRNo FROM ttSRhd WHERE SRNo NOT LIKE '%=%' AND LEFT(SRNo,2) = 'SR' ORDER BY SRTgl DESC LIMIT 1; 
				IF xSRNo IS NULL OR LOCATE('=',xSRNo) <> 0 THEN SET xSRNo = '--'; END IF;
				SET oKeterangan = CONCAT("Batal menyimpan data Retur Penjualan [SR] No: ", xSRNoBaru);
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Retur Penjualan [SR] No: ", xSRNo);
			END IF;
		END;
		WHEN "Jurnal" THEN
		BEGIN
			IF LOCATE('=',xSRNo) <> 0 THEN /* Data Baru, belum tersimpan tidak boleh di jurnal */
				SET oStatus = 1;
				SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			ELSE
				CALL jsr(xSRNo,xUserID);
				SET oStatus = 0;
				SET oKeterangan = CONCAT("Berhasil memproses Jurnal Retur Penjualan [SR] No: ", xSRNo);
			END IF;
		END;
		WHEN "Loop" THEN
	    BEGIN
            update ttsrhd set SINO=xSINo where SRNo = xSRNo;
      END;
	END CASE;
END$$

DELIMITER ;

