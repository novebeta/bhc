DELIMITER $$

DROP PROCEDURE IF EXISTS `jck`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jck`(IN xCKNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE MyPesanKu VARCHAR(300) DEFAULT '';
	DECLARE MyPesankuItem VARCHAR(150) DEFAULT '';

	DECLARE oCKTotalPart, oCKTotalJasa DOUBLE DEFAULT 0;
	SET @NoUrutku = 0;

	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xCKNo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;

	SELECT CKNo, CKTgl, CKTotalPart, CKTotalJasa, CKNoKlaim, CKMemo, NoGL, Cetak, UserID
	INTO @CKNo, @CKTgl, oCKTotalPart, oCKTotalJasa, @CKNoKlaim, @CKMemo, @NoGL, @Cetak, @UserID
	FROM ttCKhd	WHERE CKNo = xCKNo;

	SET MyPesanKu = CONCAT('Post Klaim KPB No : ' , @CKNo , ' - ' , @CKNoKlaim, ' - ', @CKMemo );
	SET MyPesanKu = TRIM(MyPesanKu);  IF LENGTH(MyPesanKu) > 300 THEN SET MyPesanKu = SUBSTRING(MyPesanKu,0, 299); END IF;
	IF LENGTH(MyPesanku) > 150 THEN SET MyPesankuItem = SUBSTRING(MyPesanku,0, 149); ELSE SET MyPesankuItem = MyPesanku; END IF;

	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;

	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL; DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;DELETE FROM ttglhpit WHERE NoGL = xNoGL;
	DELETE FROM ttgeneralledgerhd WHERE GLLink = xCKNo;

	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, DATE(@CKTgl), 'CK', MyPesanKu, 0, 0, xCKNo, xUserID, '--', xGLValid);

	#Item
	IF oCKTotalPart > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@CKTgl), (@NoUrutku:=@NoUrutku + 1),'11130101',MyPesankuItem, oCKTotalPart ,0;
	END IF;
	IF oCKTotalJasa > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@CKTgl), (@NoUrutku:=@NoUrutku + 1),'11130102',MyPesankuItem, oCKTotalJasa ,0;
	END IF;
	IF oCKTotalPart > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@CKTgl), (@NoUrutku:=@NoUrutku + 1),'11060201',MyPesankuItem, 0, oCKTotalPart;
	END IF;
	IF oCKTotalJasa > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@CKTgl), (@NoUrutku:=@NoUrutku + 1),'11060202',MyPesankuItem, 0, oCKTotalJasa;
	END IF;

	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttCKhd SET NoGL = xNoGL  WHERE CKNo = xCKNo;

	#Link Piutang
	DELETE FROM ttglhpit WHERE NoGL = xNoGL;

	INSERT INTO ttglhpit
	SELECT xNoGL, DATE(@CKTgl), REPLACE(SDNo,'SD','SV') AS SVNo, '11060202', 'Piutang', CKJasa  FROM ttckit WHERE CKNO = xCKNo AND CKJasa > 0
	UNION
	SELECT xNoGL, DATE(@CKTgl), REPLACE(SDNo,'SD','SV') AS SVNo, '11060201', 'Piutang', CKPart  FROM ttckit WHERE CKNO = xCKNo AND CKPart > 0
	ORDER BY SVNo;

	#Memberi Nilai HPLink
	SET @CountGL = 0;
	SELECT MAX(GLLink), IFNULL(COUNT(NoGL),0) INTO @GLLink, @CountGL FROM ttglhpit WHERE NoGL = xNoGL GROUP BY NoGL;
	IF @CountGL = 0 THEN
		UPDATE ttgeneralledgerhd SET HPLink = '--' WHERE NoGL = xNoGL;
	ELSEIF @CountGL = 1 THEN
		UPDATE ttgeneralledgerhd SET HPLink = @GLLink WHERE NoGL = xNoGL;
	ELSE
		UPDATE ttgeneralledgerhd SET HPLink = 'Many' WHERE NoGL = xNoGL;
	END IF;
END$$

DELIMITER ;

