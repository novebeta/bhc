DELIMITER $$

DROP PROCEDURE IF EXISTS `fsrit`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fsrit`(
IN xAction VARCHAR(150),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xSRNo VARCHAR(10),
IN xSRAuto SMALLINT(6),
IN xBrgKode VARCHAR(20),
IN xSRQty DECIMAL(5,0),
IN xSRHrgBeli DECIMAL(14,2),
IN xSRHrgJual DECIMAL(14,2),
IN xSRDiscount DECIMAL(14,2),
IN xSRPajak DECIMAL(2,0),
IN xSRNoOLD VARCHAR(10),
IN xSRAutoOLD SMALLINT(6),
IN xBrgKodeOLD VARCHAR(20)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSRNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSRNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Mengambil Harga Beli
			SELECT BrgHrgBeli INTO xSRHrgBeli FROM tdbarang WHERE BrgKode = xBrgKode;

			#Insert
			IF oStatus = 0 THEN	
				SELECT IF(ISNULL((MAX(SRAuto)+1)),1,(MAX(SRAuto)+1)) INTO xSRAuto FROM ttsrit WHERE SRNo = xSRNo; #AutoN			
				INSERT INTO ttsrit
				(SRNo, SRAuto, BrgKode, SRQty, SRHrgBeli, SRHrgJual, SRDiscount, SRPajak, LokasiKode) VALUES
				(xSRNo, xSRAuto, xBrgKode, xSRQty, xSRHrgBeli, xSRHrgJual, xSRDiscount, xSRPajak, xLokasiKode);

            #Hitung SubTotal & Total
            SELECT SUM((SRQty * SRHrgJual) - SRDiscount) INTO @SubTotal FROM ttsrit WHERE SRNo = xSRNo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttsrhd set SRSubTotal = @SubTotal, SRTotal = (@SubTotal - SRDiscFinal -  SRTotalPajak - SRBiayaKirim) WHERE SRNo = xSRNo;
            END IF;

				SET oKeterangan = CONCAT('Berhasil menambahkan data Barang ', xBrgKode, ' = ' , FORMAT(xSRQty,0));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;	
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
		 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSRNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSRNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Jika Beda Kode Barang, ambil Harga Beli
			IF xBrgKode <> xBrgKodeOLD THEN
				SELECT BrgHrgBeli INTO xSRHrgBeli FROM tdbarang WHERE BrgKode = xBrgKode;
			END IF;
			 
         IF oStatus = 0 THEN
            UPDATE ttsrit
            SET SRNo = xSRNo, SRAuto = xSRAuto, BrgKode = xBrgKode, SRQty = xSRQty, SRHrgBeli = xSRHrgBeli, SRHrgJual = xSRHrgJual, SRDiscount = xSRDiscount, SRPajak = xSRPajak, LokasiKode = xLokasiKode
            WHERE SRNo = xSRNoOLD AND SRAuto = xSRAutoOLD AND BrgKode = xBrgKodeOLD;
         
            #Hitung SubTotal & Total
            SELECT SUM((SRQty * SRHrgJual) - SRDiscount) INTO @SubTotal FROM ttsrit WHERE SRNo = xSRNo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttsrhd set SRSubTotal = @SubTotal, SRTotal = (@SubTotal - SRDiscFinal -  SRTotalPajak - SRBiayaKirim) WHERE SRNo = xSRNo;
            END IF;
         
            SET oKeterangan = CONCAT("Berhasil mengubah data Barang : ", xBrgKode, ' = ' , FORMAT(xSRQty,0));
         ELSE
            SET oKeterangan = oKeterangan;
         END IF;				 		
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSRNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSRNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM ttsrit WHERE SRAuto = xSRAuto AND SRNo = xSRNo AND BrgKode = xBrgKode;

            #Hitung SubTotal & Total
            SELECT SUM((SRQty * SRHrgJual) - SRDiscount) INTO @SubTotal FROM ttsrit WHERE SRNo = xSRNo;
            IF @Subtotal IS NOT NULL THEN 
               UPDATE ttsrhd set SRSubTotal = @SubTotal, SRTotal = (@SubTotal - SRDiscFinal -  SRTotalPajak - SRBiayaKirim) WHERE SRNo = xSRNo;
            END IF;

				SET oKeterangan = CONCAT("Berhasil menghapus data Barang : ", xBrgKode, ' = ' , FORMAT(xSRQty,0));
			ELSE
				SET oKeterangan = CONCAT("Gagal menghapus data Barang : ", xBrgKode, ' = ' , FORMAT(xSRQty,0));
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Batal menyimpan data Barang: ", xSRAuto );
		END;
	END CASE;
END$$

DELIMITER ;

