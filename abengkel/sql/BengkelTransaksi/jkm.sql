DELIMITER $$

DROP PROCEDURE IF EXISTS `jkm`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jkm`(IN xKMNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE oSumKMBayarIT DOUBLE DEFAULT 0;
	DECLARE oSumKMBayarCOA DOUBLE DEFAULT 0;
	DECLARE MyPesanKu VARCHAR(300) DEFAULT '';
	DECLARE MyPesankuItem VARCHAR(150) DEFAULT '';
	DECLARE oKategori VARCHAR(100) DEFAULT '';

	DECLARE MyAccountPOS  VARCHAR(10) DEFAULT '11010100';
	DECLARE oNoAccount VARCHAR(10) DEFAULT '';
	DECLARE oNoAccountTitipan VARCHAR(10) DEFAULT '';
	SET @NoUrutku = 0;

	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xKMNo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;

	SELECT KMNo, KMTgl, KMJenis, KodeTrans, PosKode, MotorNoPolisi, KasKode, KMPerson, KMBayarTunai, KMNominal, KMMemo, NoGL, Cetak, UserID
	INTO @KMNo, @KMTgl, @KMJenis, @KodeTrans, @PosKode, @MotorNoPolisi, @KasKode, @KMPerson, @KMBayarTunai, @KMNominal, @KMMemo, @NoGL, @Cetak, @UserID
	FROM ttKMhd	WHERE KMNo = xKMNo;
	SELECT IFNULL(SUM(KMBayar),0) INTO oSumKMBayarIT FROM ttKMit WHERE KMNo = xKMNo;
	SELECT IFNULL(SUM(KMBayar),0) INTO oSumKMBayarCOA FROM ttKMitcoa WHERE KMNo = xKMNo;

	CASE @KodeTrans
		WHEN 'UM' THEN
			SET oKategori = 'Umum';
		WHEN 'BK' THEN
			SET oKategori = 'KM Dari Bank';
		WHEN 'SE' THEN
			SET oKategori = 'Service Estimation';
		WHEN 'SO' THEN
			SET oKategori = 'Sales Order';
		WHEN 'SV' THEN
			SET oKategori = 'Service Invoice';
		WHEN 'SI' THEN
			SET oKategori = 'Sales Invoice';
		WHEN 'PR' THEN
			SET oKategori = 'Purchase Retur';
		WHEN 'KT' THEN
			SET oKategori = 'Kas Transfer';
	END CASE;

	SET MyPesanKu = CONCAT("Post  KM - ",oKategori);
	IF @KMPerson <> '--' THEN SET MyPesanKu = CONCAT(MyPesanKu,' - ' , @KMPerson); END IF;
	IF @KMMemo <> "--" THEN SET MyPesanKu = CONCAT(MyPesanKu,' - ' , @KMMemo); END IF;
	SET MyPesanKu = TRIM(MyPesanKu);
	IF LENGTH(MyPesanKu) > 300 THEN SET MyPesanKu = SUBSTRING(MyPesanKu,0, 299); END IF;
	SET MyPesankuItem = MyPesanKu ;
	IF LENGTH(MyPesankuItem) > 150 THEN SET MyPesankuItem = SUBSTRING(MyPesankuItem,0, 149); END IF;

	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;

  SELECT IFNULL(tdpos.NoAccount,'11010100') INTO MyAccountPOS FROM tdpos INNER JOIN traccount ON traccount.NoAccount = tdpos.NoAccount WHERE PosKode = @PosKode;
	IF MyAccountPOS = '' OR (LENGTH(TRIM(MyAccountPOS)) < 10) THEN SET MyAccountPOS = '11010100'; END IF;

	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL; DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;DELETE FROM ttglhpit WHERE NoGL = xNoGL;
	DELETE FROM ttgeneralledgerhd WHERE GLLink = xKMNo;
			
	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, DATE(@KMTgl), 'KM', MyPesanKu, 0, 0, xKMNo, xUserID, '--', xGLValid);

	#Item
	SET @NoUrutku = 0;
	INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@KMTgl), (@NoUrutku:=@NoUrutku + 1), MyAccountPOS , MyPesankuItem , @KMNominal, 0 ;
	CASE @KodeTrans
		WHEN 'UM' THEN
			DROP TEMPORARY TABLE IF EXISTS ScriptTtglhpit; CREATE TEMPORARY TABLE IF NOT EXISTS ScriptTtglhpit AS
			SELECT NoGL, TglGL, GLLink, NoAccount, HPJenis, HPNilai FROM ttglhpit WHERE NoGL = xNoGL;

			INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@KMTgl), (@NoUrutku:=@NoUrutku + 1), NoAccount,  KMKeterangan, 0, KMBayar FROM ttKMitcoa WHERE KMNo = xKMNo AND KMBayar >= 0;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@KMTgl), (@NoUrutku:=@NoUrutku + 1), NoAccount,  KMKeterangan, KMBayar, 0 FROM ttKMitcoa WHERE KMNo = xKMNo AND KMBayar < 0;

			SELECT COUNT(NoGL) INTO @CountNoGL FROM ScriptTtglhpit;
			IF @CountNoGL > 0 THEN
				INSERT INTO ttglhpit SELECT * FROM ScriptTtglhpit;
				DELETE FROM ttglhpit WHERE NoAccount NOT IN (SELECT NoAccount FROM ttgeneralledgerit WHERE NoGL  = xNoGL) AND NoGL  = xNoGL;
				UPDATE ttgeneralledgerhd SET HPLink = 'Many'  WHERE NoGL = xNoGL;
			END IF;

			UPDATE ttKMhd SET KMNominal = oSumKMBayarCOA WHERE KMNo = xKMNo;
		WHEN 'BK' THEN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@KMTgl), (@NoUrutku:=@NoUrutku + 1), '11040000',  MyPesankuItem, 0, @KMNominal ;
		WHEN 'SE' THEN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, DATE(@KMTgl), (@NoUrutku:=@NoUrutku + 1), '24052000',  MyPesankuItem, 0, @KMNominal ;
		WHEN 'SO' THEN
			INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@KMTgl), (@NoUrutku:=@NoUrutku + 1), '24052200', MyPesankuItem , 0, @KMNominal;
		WHEN 'SV' THEN
			INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@KMTgl), (@NoUrutku:=@NoUrutku + 1), '11060100', MyPesankuItem , 0, @KMNominal;
			#Link Piutang
			DELETE FROM ttglhpit WHERE NoGL = xNoGL;
			INSERT INTO ttglhpit SELECT xNoGL, DATE(@KMTgl), KMLink, '11060100','Piutang', KMBayar FROM ttkmit WHERE KMNo = xKMNo;
		WHEN 'SI' THEN
			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@KMTgl), (@NoUrutku:=@NoUrutku + 1),'11060100',MyPesankuItem , 0, KMBayar FROM ttkmit
			INNER JOIN ttsihd ON ttsihd.SINo = ttkmit.KMlink WHERE KMNo = xKMNo AND KodeTrans = 'TC12';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@KMTgl), (@NoUrutku:=@NoUrutku + 1),IFNULL(NoAccount,'11070710'),MyPesankuItem, 0, KMBayar FROM ttkmit
			INNER JOIN ttsihd ON ttsihd.SINo = ttkmit.KMlink
			INNER JOIN  tddealer ON ttsihd.DealerKode = tddealer.DealerKode
			LEFT OUTER JOIN (SELECT NoAccount, NamaAccount FROM traccount WHERE JenisAccount ='Detail' AND NoParent = '11070700') traccount ON CONCAT('%',SUBSTRING_INDEX(TRIM(NamaAccount), ' ', -1)) = CONCAT('%',SUBSTRING_INDEX(TRIM(DealerNama), ' ', -1))
			WHERE KMNo = xKMNo AND KodeTrans = 'TC17';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@KMTgl), (@NoUrutku:=@NoUrutku + 1),'11060401',MyPesankuItem , 0, KMBayar FROM ttkmit
			INNER JOIN ttsihd ON ttsihd.SINo = ttkmit.KMlink WHERE KMNo = xKMNo AND KodeTrans = 'TC20';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@KMTgl), (@NoUrutku:=@NoUrutku + 1),'11060500',MyPesankuItem , 0, KMBayar FROM ttkmit
			INNER JOIN ttsihd ON ttsihd.SINo = ttkmit.KMlink WHERE KMNo = xKMNo AND KodeTrans = 'TC22';

			INSERT INTO ttgeneralledgerit
			SELECT xNoGL, DATE(@KMTgl), (@NoUrutku:=@NoUrutku + 1),'11060402',MyPesankuItem , 0, KMBayar FROM ttkmit
			INNER JOIN ttsihd ON ttsihd.SINo = ttkmit.KMlink WHERE KMNo = xKMNo AND KodeTrans = 'TC21';

			#Link Piutang
			DELETE FROM ttglhpit WHERE NoGL = xNoGL;
			INSERT INTO ttglhpit SELECT xNoGL, DATE(@KMTgl), KMlink ,'11060100','Piutang', KMBayar FROM ttkmit
			INNER JOIN ttsihd ON ttsihd.SINo = ttkmit.KMlink WHERE KMNo = xKMNo AND KodeTrans = 'TC12';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@KMTgl), KMlink ,IFNULL(NoAccount,'11070710'),'Piutang', KMBayar FROM ttkmit
			INNER JOIN ttsihd ON ttsihd.SINo = ttkmit.KMlink
			INNER JOIN  tddealer ON ttsihd.DealerKode = tddealer.DealerKode
			LEFT OUTER JOIN (SELECT NoAccount, NamaAccount FROM traccount WHERE JenisAccount ='Detail' AND NoParent = '11070700') traccount ON CONCAT('%',SUBSTRING_INDEX(TRIM(NamaAccount), ' ', -1)) = CONCAT('%',SUBSTRING_INDEX(TRIM(DealerNama), ' ', -1))
			WHERE KMNo = xKMNo AND KodeTrans = 'TC17';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@KMTgl), KMlink ,'11060401','Piutang', KMBayar FROM ttkmit
			INNER JOIN ttsihd ON ttsihd.SINo = ttkmit.KMlink WHERE KMNo = xKMNo AND KodeTrans = 'TC20';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@KMTgl), KMlink ,'11060500','Piutang', KMBayar FROM ttkmit
			INNER JOIN ttsihd ON ttsihd.SINo = ttkmit.KMlink WHERE KMNo = xKMNo AND KodeTrans = 'TC22';

			INSERT INTO ttglhpit SELECT xNoGL, DATE(@KMTgl), KMlink ,'11060402','Piutang', KMBayar FROM ttkmit
			INNER JOIN ttsihd ON ttsihd.SINo = ttkmit.KMlink WHERE KMNo = xKMNo AND KodeTrans = 'TC21';
		WHEN 'PR' THEN
			INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@KMTgl), (@NoUrutku:=@NoUrutku + 1), '11159910', MyPesankuItem , 0, @KMNominal;
		WHEN 'KT' THEN
			SET oKategori = 'Kas Transfer';
	END CASE;

	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttKMhd SET NoGL = xNoGL  WHERE KMNo = xKMNo;

	#Memberi Nilai HPLink
	SET @CountGL = 0;
	SELECT MAX(GLLink), IFNULL(COUNT(NoGL),0) INTO @GLLink, @CountGL FROM ttglhpit WHERE NoGL = xNoGL GROUP BY NoGL;
	IF @CountGL = 0 THEN
		UPDATE ttgeneralledgerhd SET HPLink = '--' WHERE NoGL = xNoGL;
	ELSEIF @CountGL = 1 THEN
		UPDATE ttgeneralledgerhd SET HPLink = @GLLink WHERE NoGL = xNoGL;
	ELSE
		UPDATE ttgeneralledgerhd SET HPLink = 'Many' WHERE NoGL = xNoGL;
	END IF;
END$$

DELIMITER ;

