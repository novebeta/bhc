DELIMITER $$

DROP PROCEDURE IF EXISTS `fsditbarang`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fsditbarang`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xSDNo VARCHAR(10),
IN xSDAuto SMALLINT(6),
IN xBrgKode VARCHAR(20),
IN xSDQty DECIMAL(5, 0),
IN xSDHrgBeli DECIMAL(14, 2),
IN xSDHrgJual DECIMAL(14, 2),
IN xSDJualDisc DECIMAL(14, 2),
IN xSDPajak DECIMAL(2, 2),
IN xSDPickingNo VARCHAR(10),
IN xSDPickingDate DATETIME,
IN xCetak VARCHAR(5),
IN xSDNoOLD VARCHAR(10),
IN xSDAutoOLD VARCHAR(10),
IN xBrgKodeOLD VARCHAR(20)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSDNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSDNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Mengambil Harga Beli
			SELECT BrgHrgBeli INTO xSDHrgBeli FROM tdbarang WHERE BrgKode = xBrgKode;

			#Insert
			IF oStatus = 0 THEN
				SELECT IF(ISNULL((MAX(SDAuto)+1)),1,(MAX(SDAuto)+1)) INTO xSDAuto FROM ttsditbarang WHERE SDNo = xSDNo; #AutoN							
				INSERT INTO ttsditbarang
				(SDNo, SDAuto, BrgKode, SDQty, SDHrgBeli, SDHrgJual, SDJualDisc, SDPajak, LokasiKode, SDPickingNo, SDPickingDate, Cetak) VALUES
				(xSDNo, xSDAuto, xBrgKode, xSDQty, xSDHrgBeli, xSDHrgJual, xSDJualDisc, xSDPajak, xLokasiKode, xSDPickingNo, xSDPickingDate, xCetak);

            #Hitung SubTotal & Total
            SELECT SUM(SDQty) INTO @SDTotalQty FROM ttSDitbarang WHERE SDNo = xSDNo;
            SELECT SUM((SDQty * SDHrgJual) - SDJualDisc) INTO @SDTotalPart FROM ttSDitbarang WHERE SDNo = xSDNo;
            SELECT SUM(SDHrgJual - SDJualDisc) INTO @SDTotalJasa FROM ttSDitjasa WHERE SDNo = xSDNo;
            IF @SDTotalPart IS NULL THEN SET  @SDTotalPart = 0; END IF;
            IF @SDTotalJasa IS NULL THEN SET  @SDTotalJasa = 0; END IF;
            UPDATE ttSDhd SET
            SDTotalJasa = @SDTotalJasa,
            SDTotalPart = @SDTotalPart,
            SDTotalQty = @SDTotalQty,
            SDTotalNonGratis = @SDTotalJasa + @SDTotalPart - SDDiscFinal - SDUangMuka - SDTotalPajak,
            SDTotalBiaya = @SDTotalJasa + @SDTotalPart - SDDiscFinal - SDUangMuka - SDTotalPajak
            WHERE SDNo = xSDNo;

				SET oKeterangan = CONCAT('Berhasil menambahkan data Barang ', xBrgKode, ' = ' , FORMAT(xSDQty,0));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;	
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
		 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSDNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSDNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Jika Beda Kode Barang, ambil Harga Beli
			IF xBrgKode <> xBrgKodeOLD THEN
				SELECT BrgHrgBeli INTO xSDHrgBeli FROM tdbarang WHERE BrgKode = xBrgKode;
			END IF;
			 
			IF oStatus = 0 THEN
				UPDATE ttsditbarang
				SET SDNo = xSDNo, SDAuto = xSDAuto, BrgKode = xBrgKode, SDQty = xSDQty, SDHrgBeli = xSDHrgBeli, SDHrgJual = xSDHrgJual, SDJualDisc = xSDJualDisc, SDPajak = xSDPajak, 
				LokasiKode = xLokasiKode, SDPickingNo = xSDPickingNo, SDPickingDate = xSDPickingDate, Cetak = xCetak
				WHERE SDNo = xSDNoOLD AND SDAuto = xSDAutoOLD AND BrgKode = xBrgKodeOLD;

            #Hitung SubTotal & Total
            SELECT SUM(SDQty) INTO @SDTotalQty FROM ttSDitbarang WHERE SDNo = xSDNo;
            SELECT SUM((SDQty * SDHrgJual) - SDJualDisc) INTO @SDTotalPart FROM ttSDitbarang WHERE SDNo = xSDNo;
            SELECT SUM(SDHrgJual - SDJualDisc) INTO @SDTotalJasa FROM ttSDitjasa WHERE SDNo = xSDNo;
            IF @SDTotalPart IS NULL THEN SET  @SDTotalPart = 0; END IF;
            IF @SDTotalJasa IS NULL THEN SET  @SDTotalJasa = 0; END IF;
            UPDATE ttSDhd SET
            SDTotalJasa = @SDTotalJasa,
            SDTotalPart = @SDTotalPart,
            SDTotalQty = @SDTotalQty,
            SDTotalNonGratis = @SDTotalJasa + @SDTotalPart - SDDiscFinal - SDUangMuka - SDTotalPajak,
            SDTotalBiaya = @SDTotalJasa + @SDTotalPart - SDDiscFinal - SDUangMuka - SDTotalPajak
            WHERE SDNo = xSDNo;

				SET oKeterangan = CONCAT("Berhasil mengubah data Barang : ", xBrgKode, ' = ' , FORMAT(xSDQty,0));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSDNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSDNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			IF oStatus = 0 THEN
				DELETE FROM ttsditbarang WHERE SDAuto = xSDAuto AND SDNo = xSDNo AND BrgKode = xBrgKode;

            #Hitung SubTotal & Total
            SELECT SUM(SDQty) INTO @SDTotalQty FROM ttSDitbarang WHERE SDNo = xSDNo;
            SELECT SUM((SDQty * SDHrgJual) - SDJualDisc) INTO @SDTotalPart FROM ttSDitbarang WHERE SDNo = xSDNo;
            SELECT SUM(SDHrgJual - SDJualDisc) INTO @SDTotalJasa FROM ttSDitjasa WHERE SDNo = xSDNo;
            IF @SDTotalPart IS NULL THEN SET  @SDTotalPart = 0; END IF;
            IF @SDTotalJasa IS NULL THEN SET  @SDTotalJasa = 0; END IF;
            UPDATE ttSDhd SET
            SDTotalJasa = @SDTotalJasa,
            SDTotalPart = @SDTotalPart,
            SDTotalQty = @SDTotalQty,
            SDTotalNonGratis = @SDTotalJasa + @SDTotalPart - SDDiscFinal - SDUangMuka - SDTotalPajak,
            SDTotalBiaya = @SDTotalJasa + @SDTotalPart - SDDiscFinal - SDUangMuka - SDTotalPajak
            WHERE SDNo = xSDNo;

				SET oKeterangan = CONCAT("Berhasil menghapus data Barang : ", xBrgKode, ' = ' , FORMAT(xSDQty,0));
			ELSE
				SET oKeterangan = CONCAT("Gagal menghapus data Barang : ", xBrgKode, ' = ' , FORMAT(xSDQty,0));
			END IF;
		END;
	END CASE;
END$$

DELIMITER ;

