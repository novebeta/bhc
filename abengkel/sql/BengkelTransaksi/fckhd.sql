DELIMITER $$

DROP PROCEDURE IF EXISTS `fckhd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fckhd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xCKNo VARCHAR(10),
IN xCKNoBaru VARCHAR(10),
IN xCKTgl DATETIME,
IN xCKTotalPart DECIMAL(14, 2),
IN xCKTotalJasa DECIMAL(14, 2),
IN xCKNoKlaim VARCHAR(35),
IN xCKMemo VARCHAR(150),
IN xCetak VARCHAR(5),
IN xNoGL VARCHAR(10))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			DELETE FROM ttckhd WHERE (CKNo LIKE '%=%') AND CKTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);	
			#ReJurnal Transaksi Tidak Memiliki Jurnal
			cek01: BEGIN
   			DECLARE rCKNo VARCHAR(100) DEFAULT '';
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT CKNo FROM ttCKhd
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttCKhd.CKNo 
				WHERE ttgeneralledgerhd.NoGL IS NULL AND CKTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rCKNo;
					IF rCKNo IS NOT NULL AND rCKNo <> '' THEN
						CALL jCK(rCKNo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;						
			SET oStatus = 0; SET oKeterangan = CONCAT("Daftar Klaim KPB [CK] ");
		END;
		WHEN "Load" THEN
		BEGIN
			SET oStatus = 0; SET oKeterangan = CONCAT("Load data Klaim KPB [CK] No: ", xCKNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			 SET @AutoNo = CONCAT('CK',RIGHT(YEAR(NOW()),2),'-');
			 SELECT CONCAT(@AutoNo,LPAD((RIGHT(CKNo,5)+1),5,0)) INTO @AutoNo FROM ttckhd WHERE ((CKNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(CKNo) = 10) ORDER BY CKNo DESC LIMIT 1;
			 IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('CK',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			 SET xCKNo = @AutoNo;
			 SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Klaim KPB [CK] No: ",xCKNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @CKNoOLD = xCKNo;	
			
			IF LOCATE('=',xCKNo) <> 0  THEN 
				SET @AutoNo = CONCAT('CK',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(CKNo,5)+1),5,0)) INTO @AutoNo FROM ttckhd WHERE ((CKNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(CKNo) = 10) ORDER BY CKNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('CK',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xCKNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xCKNoBaru = xCKNo;
			END IF;
			 
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xCKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xCKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			# Update 
			IF oStatus = 0 THEN 
				UPDATE ttckhd
				SET CKNo = xCKNoBaru, CKTgl = xCKTgl, CKNoKlaim = xCKNoKlaim, CKTotalJasa = xCKTotalJasa, CKTotalPart = xCKTotalPart, CKMemo = xCKMemo, NoGL = xNoGL, Cetak = xCetak, UserID = xUserID
				WHERE CKNo = xCKNo;
				SET xCKNo = xCKNoBaru;
			
				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @CKNoOLD; #Hapus Jurnal No Transaksi Lama
				CALL jck(xCKNo,xUserID); #Posting Jurnal
				
				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Klaim KPB','ttckhd', xCKNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Klaim KPB [CK] No: ", xCKNo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Klaim KPB','ttckhd', xCKNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Klaim KPB [CK] No: ", xCKNo);
				END IF;
			ELSEIF oStatus = 1 THEN
   			IF LOCATE('=',xCKNo) <> 0 THEN
   				DELETE FROM ttckhd WHERE CKNo = xCKNo;
   				SELECT IFNULL(CKNo,'--') INTO xCKNo FROM ttCKhd WHERE CKNo NOT LIKE '%=%' AND LEFT(CKNo,2) = 'CK' ORDER BY CKTgl DESC LIMIT 1; 
   				IF xCKNo IS NULL OR LOCATE('=',xCKNo) <> 0 THEN SET xCKNo = '--'; END IF;
   			END IF;
				SET oKeterangan = oKeterangan ;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;
			
			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xCKNo;
			IF @GLValid = 'Sudah' THEN	
				SET oStatus = 1; SET oKeterangan = CONCAT("No CK:", xCKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM ttckhd WHERE CKNo = xCKNo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xCKNo;
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Klaim KPB [CK]  No: ",xCKNo);
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Klaim KPB','ttckhd', xCKNo); 			
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xCKNo) <> 0 THEN
				DELETE FROM ttckhd WHERE CKNo = xCKNo;
				SET oKeterangan = CONCAT("Batal menyimpan data Klaim KPB [CK]  No: ", xCKNoBaru);
				SELECT IFNULL(CKNo,'--') INTO xCKNo FROM ttCKhd WHERE CKNo NOT LIKE '%=%' AND LEFT(CKNo,2) = 'CK' ORDER BY CKTgl DESC LIMIT 1; 
				IF xCKNo IS NULL OR LOCATE('=',xCKNo) <> 0 THEN SET xCKNo = '--'; END IF;
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Klaim KPB [CK]  No: ", xCKNo);
			END IF;
		END;
		WHEN "Jurnal" THEN
		BEGIN
			 IF LOCATE('=',xCKNo) <> 0 THEN 
				  SET oStatus = 1; SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			 ELSE
				  CALL jck(xCKNo,xUserID);
				  SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil memproses Jurnal Klaim KPB [CK]  No: ", xCKNo);
			 END IF;
		END;
	END CASE;
END$$

DELIMITER ;

