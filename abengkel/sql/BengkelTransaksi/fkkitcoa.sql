DELIMITER $$

DROP PROCEDURE IF EXISTS `fkkitcoa`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fkkitcoa`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xKKNo VARCHAR(12),
IN xKKAutoN SMALLINT(5),
IN xNoAccount VARCHAR(10),
IN xKKKeterangan VARCHAR(150),
IN xKKBayar DECIMAL(14, 2),
IN xKKNoOLD VARCHAR(12),
IN xKKAutoNOLD SMALLINT(5),
IN xNoAccountOLD VARCHAR(10)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Insert
			IF oStatus = 0 THEN				
				SELECT IFNULL((MAX(KKAutoN)+1),0) INTO xKKAutoN FROM ttkkitcoa WHERE KKNo = xKKNo; #AutoN
				INSERT INTO ttkkitcoa (KKNo, KKAutoN, NoAccount, KKBayar, KKKeterangan) VALUES (xKKNo, xKKAutoN, xNoAccount, xKKBayar, xKKKeterangan);

            SELECT SUM(KKBayar) INTO @KKBayar FROM ttkkitcoa WHERE KKNo = xKKNo; 
            IF @KKBayar IS NOT NULL THEN UPDATE ttkkhd set KKNominal = @KKBayar WHERE KKNo = xKKNo; END IF;

				SET oKeterangan = CONCAT('Berhasil menambahkan data Kas Keluar ', xKKNo,' - ', xNoAccount, ' - ', FORMAT(xKKBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
			  UPDATE ttkkitcoa
			  SET KKNo = xKKNo, KKAutoN = xKKAutoN, NoAccount = xNoAccount, KKBayar = xKKBayar, KKKeterangan = xKKKeterangan
			  WHERE KKNo = xKKNoOLD AND KKAutoN = xKKAutoNOLD AND NoAccount = xNoAccountOLD;

            SELECT SUM(KKBayar) INTO @KKBayar FROM ttkkitcoa WHERE KKNo = xKKNo; 
            IF @KKBayar IS NOT NULL THEN UPDATE ttkkhd set KKNominal = @KKBayar WHERE KKNo = xKKNo; END IF;

			  SET oKeterangan = CONCAT("Berhasil mengubah data Kas Keluar : ", xKKNo , ' - ', xNoAccount, ' - ', FORMAT(xKKBayar,2));
			ELSE
			  SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Delete
			IF oStatus = 0 THEN
				DELETE FROM ttKKitcoa WHERE KKAutoN = xKKAutoN AND NoAccount = xNoAccount AND KKNo = xKKNo;

            SELECT SUM(KKBayar) INTO @KKBayar FROM ttkkitcoa WHERE KKNo = xKKNo; 
            IF @KKBayar IS NOT NULL THEN UPDATE ttkkhd set KKNominal = @KKBayar WHERE KKNo = xKKNo; END IF;

				SET oKeterangan = CONCAT("Berhasil menghapus data Kas Keluar : ", xKKNo , ' - ' ,xNoAccount,' - ', FORMAT(xKKBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;		
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Batal menyimpan data kas keluar: ", xKKAutoN );
		END;
	END CASE;
END$$

DELIMITER ;

