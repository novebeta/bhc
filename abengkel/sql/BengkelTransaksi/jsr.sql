DELIMITER $$

DROP PROCEDURE IF EXISTS `jsr`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jsr`(IN xSRNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE MyPesanKu VARCHAR(300) DEFAULT '';
	DECLARE MyPesankuItem VARCHAR(150) DEFAULT '';

	DECLARE oPotonganHGP, oPotonganOLI  DOUBLE DEFAULT 0;
	DECLARE oNilaiHGP, oNilaiOLI DOUBLE DEFAULT 0;
	DECLARE oHPPHGP, oHPPOLI DOUBLE DEFAULT 0;
	DECLARE oSRTotal,oSRDiscFinal DOUBLE DEFAULT 0;

	SET @NoUrutku = 0;

	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xSRNo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;
	
	SELECT SRNo, SRTgl, SINo, KodeTrans, LokasiKode, SRSubTotal, PosKode, KarKode, MotorNoPolisi, SRDiscFinal, SRTotalPajak, SRBiayaKirim, SRTotal, SRTerbayar, SRKeterangan, NoGL, Cetak, UserID
	INTO  @SRNo,@SRTgl,@SINo,@KodeTrans,@LokasiKode,@SRSubTotal,@PosKode,@KarKode,@MotorNoPolisi,oSRDiscFinal,@SRTotalPajak,@SRBiayaKirim,oSRTotal,@SRTerbayar,@SRKeterangan,@NoGL,@Cetak,@UserID
	FROM ttsrhd	WHERE SRNo = xSRNo;

   SET MyPesanKu = CONCAT("Post Retur Penjualan : " , @SRNo , " - " , @KodeTrans , " - ",@MotorNoPolisi );
   SET MyPesanKu = TRIM(MyPesanKu);  IF LENGTH(MyPesanKu) > 300 THEN SET MyPesanKu = SUBSTRING(MyPesanKu,0, 299); END IF;
   IF LENGTH(MyPesanku) > 150 THEN SET MyPesankuItem = SUBSTRING(MyPesanku,0, 149); ELSE SET MyPesankuItem = MyPesanku; END IF;
               
	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;

	SELECT IFNULL(SUM(IFNULL(SRDiscount,0)),0) INTO oPotonganHGP FROM ttSRit
	INNER JOIN tdbarang ON ttSRit.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) <> 'OLI' AND ttSRit.SRNo = xSRNo ;

	SELECT IFNULL(SUM(IFNULL(SRDiscount,0)),0) INTO oPotonganOLI FROM ttSRit
	INNER JOIN tdbarang ON ttSRit.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) = 'OLI' AND ttSRit.SRNo = xSRNo ;

	SELECT IFNULL(SUM(ttsrit.SRQty * ttsrit.SRHrgJual - ttsrit.SRDiscount),0) INTO oNilaiHGP FROM ttSRit
	INNER JOIN tdbarang ON ttSRit.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) <> 'OLI' AND ttSRit.SRNo = xSRNo ;

	SELECT IFNULL(SUM(ttsrit.SRQty * ttsrit.SRHrgJual - ttsrit.SRDiscount),0) INTO oNilaiOLI FROM ttSRit
	INNER JOIN tdbarang ON ttSRit.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) = 'OLI' AND ttSRit.SRNo = xSRNo ;


	SELECT IFNULL(SUM(ttsrit.SRQty * ttsrit.SRHrgBeli),0) INTO oHPPHGP FROM ttSRit
	INNER JOIN tdbarang ON ttSRit.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) <> 'OLI' AND ttSRit.SRNo = xSRNo ;

	SELECT IFNULL(SUM(ttsrit.SRQty * ttsrit.SRHrgBeli),0) INTO oHPPOLI FROM ttSRit
	INNER JOIN tdbarang ON ttSRit.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) = 'OLI' AND ttSRit.SRNo = xSRNo ;


	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL; DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;DELETE FROM ttglhpit WHERE NoGL = xNoGL;
	DELETE FROM ttgeneralledgerhd WHERE GLLink = xSRNo;

	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, DATE(@SRTgl), 'SR', MyPesanKu, 0, 0, xSRNo, xUserID, '--', xGLValid);

	#Item
	IF (oNilaiHGP + oPotonganHGP) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SRTgl), (@NoUrutku:=@NoUrutku + 1),'30100100',MyPesankuItem, oNilaiHGP + oPotonganHGP,0;
	END IF;
	IF (oNilaiOLI + oPotonganOLI) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SRTgl), (@NoUrutku:=@NoUrutku + 1),'30100200',MyPesankuItem, oNilaiOLI + oPotonganOLI,0;
	END IF;

	IF oPotonganHGP > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SRTgl), (@NoUrutku:=@NoUrutku + 1),'30090102',MyPesankuItem, 0,oPotonganHGP;
	END IF;
	IF oPotonganOLI > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SRTgl), (@NoUrutku:=@NoUrutku + 1),'30090202',MyPesankuItem, 0,oPotonganOLI;
	END IF;

	IF oSRTotal > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SRTgl), (@NoUrutku:=@NoUrutku + 1),'24052100',MyPesankuItem, 0,oSRTotal;
	END IF;

	IF oHPPHGP > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SRTgl), (@NoUrutku:=@NoUrutku + 1),'50030200',MyPesankuItem, 0,oHPPHGP;
	END IF;
	IF oHPPOLI > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SRTgl), (@NoUrutku:=@NoUrutku + 1),'50040200',MyPesankuItem, 0,oHPPOLI;
	END IF;
	IF oHPPHGP > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SRTgl), (@NoUrutku:=@NoUrutku + 1),'11150602',MyPesankuItem, oHPPHGP,0;
	END IF;
	IF oHPPOLI > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SRTgl), (@NoUrutku:=@NoUrutku + 1),'11150702',MyPesankuItem, oHPPOLI,0;
	END IF;

	IF oSRDiscFinal > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SRTgl), (@NoUrutku:=@NoUrutku + 1),'30090400',MyPesankuItem, 0,oSRDiscFinal;
	END IF;

	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttSRhd SET NoGL = xNoGL  WHERE SRNo = xSRNo;END$$
DELIMITER ;

