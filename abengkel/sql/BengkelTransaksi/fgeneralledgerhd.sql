DELIMITER $$

DROP PROCEDURE IF EXISTS `fgeneralledgerhd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fgeneralledgerhd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan VARCHAR(150),
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xNoGL VARCHAR(10),
IN xNoGLBaru VARCHAR(18),
IN xTglGL DATE,
IN xKodeTrans VARCHAR(2),
IN xMemoGL VARCHAR(300),
IN xTotalDebetGL DECIMAL(18,2),
IN xTotalKreditGL DECIMAL(18,2),
IN xGLLink VARCHAR(18),
IN xHPLink VARCHAR(18),
IN xGLValid VARCHAR(5))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			SET oStatus = 0; SET oKeterangan = CONCAT("Daftar Jurnal");
		END;
		WHEN "Load" THEN
		BEGIN
			SET oStatus = 0; SET oKeterangan = CONCAT("Load data Jurnal No: ",xNoGL);
		END;
		WHEN "Insert" THEN
		BEGIN
			IF	xKodeTrans = 'JA' THEN
				SET @AutoNo = CONCAT('JA',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNo FROM ttgeneralledgerhd WHERE ((NoGL LIKE CONCAT(@AutoNo,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('JA',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xNoGL = @AutoNo;		
			ELSEIF xKodeTrans = 'JP' THEN
				SET @AutoNo = CONCAT('JP',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNo FROM ttgeneralledgerhd WHERE ((NoGL LIKE CONCAT(@AutoNo,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('JP',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xNoGL = @AutoNo;					
			ELSEIF xKodeTrans = 'GL' THEN
				SET @AutoNo = CONCAT('MM',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNo FROM ttgeneralledgerhd WHERE ((NoGL LIKE CONCAT(@AutoNo,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('MM',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xNoGL = @AutoNo;							
			ELSE
				SET @AutoNo = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNo FROM ttgeneralledgerhd WHERE ((NoGL LIKE CONCAT(@AutoNo,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xNoGL = @AutoNo;							
			END IF;
				 
			SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Jurnal No: ",xNoGL);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @NoGLOLD = xNoGL;
					
			IF LOCATE('=',xNoGL) <> 0  THEN
				IF	xKodeTrans = 'JA' THEN
					SET @AutoNo = CONCAT('JA',RIGHT(YEAR(NOW()),2),'-');
					SELECT CONCAT(@AutoNo,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNo FROM ttgeneralledgerhd WHERE ((NoGL LIKE CONCAT(@AutoNo,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
					IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('JA',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				ELSEIF xKodeTrans = 'JP' THEN
					SET @AutoNo = CONCAT('JP',RIGHT(YEAR(NOW()),2),'-');
					SELECT CONCAT(@AutoNo,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNo FROM ttgeneralledgerhd WHERE ((NoGL LIKE CONCAT(@AutoNo,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
					IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('JP',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				ELSEIF xKodeTrans = 'GL' THEN
					SET @AutoNo = CONCAT('MM',RIGHT(YEAR(NOW()),2),'-');
					SELECT CONCAT(@AutoNo,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNo FROM ttgeneralledgerhd WHERE ((NoGL LIKE CONCAT(@AutoNo,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
					IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('MM',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				ELSE
					SET @AutoNo = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
					SELECT CONCAT(@AutoNo,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNo FROM ttgeneralledgerhd WHERE ((NoGL LIKE CONCAT(@AutoNo,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
					IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				END IF;
				SET xNoGLBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xNoGLBaru = xNoGL;
			END IF;

			IF oStatus = 0 THEN 
				IF xKodeTrans = 'GL' THEN
					SET xGLLink = xNoGLBaru;
				END IF;
			
				UPDATE ttgeneralledgerhd
				SET NoGL = xNoGLBaru, `KodeTrans` = xKodeTrans, `MemoGL` = xMemoGL, `TotalDebetGL` = xTotalDebetGL, `TotalKreditGL` = xTotalKreditGL,
				`GLLink` = xGLLink, `UserID` = xUserID, `HPLink` = xHPLink, `GLValid` = xGLValid
				WHERE `NoGL` = xNoGL;
				SET xNoGL = xNoGLBaru;

				#AutoNumber
				UPDATE ttgeneralledgerit SET GLAutoN = -(3*GLAutoN) WHERE NoGL = xNoGL;
				SET @row_number = 0;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;
				CREATE TEMPORARY TABLE IF NOT EXISTS TNoUrut AS
				SELECT NoGL, GLAutoN,(@row_number:=@row_number + 1) AS NoUrut , NoAccount FROM ttgeneralledgerit
				WHERE NoGL = xNoGL ORDER BY GLAutoN DESC;
				UPDATE ttgeneralledgerit INNER JOIN TNoUrut
				ON  ttgeneralledgerit.NoGL = TNoUrut.NoGL
				AND ttgeneralledgerit.NoAccount = TNoUrut.NoAccount
				AND ttgeneralledgerit.GLAutoN = TNoUrut.GLAutoN
				SET ttgeneralledgerit.GLAutoN = TNoUrut.NoUrut;
				DROP TEMPORARY TABLE IF EXISTS TNoUrut;

				IF @AutoNo = 'Edit' THEN #Edit
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , CONCAT('Jurnal ',xKodeTrans,' - ',xGLLink),'ttgeneralledgerhd', xNoGL); 
					SET oKeterangan = CONCAT("Berhasil menyimpan data Jurnal No: ", xNoGL);
				ELSE #Add
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , CONCAT('Jurnal ',xKodeTrans,' - ',xGLLink),'ttgeneralledgerhd', xNoGL); 
					SET oKeterangan = CONCAT("Berhasil menyimpan data baru Jurnal No: ", xNoGL);
				END IF;
			ELSEIF oStatus = 1 THEN
			  SET oKeterangan = CONCAT("Gagal, silahkan melengkapi data Jurnal No: ", xNoGL);
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;
			
			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE NoGL = xNoGL;
			IF @GLValid = 'Sudah' THEN	
			  SET oStatus = 1; SET oKeterangan = CONCAT("No Jurnal : ",@NoGL," sudah tervalidasi");
			END IF;
					
			IF oStatus = 0 THEN
				DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL;			
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , CONCAT('Jurnal ',xKodeTrans,' - ',xGLLink),'ttgeneralledgerhd', xNoGL); 
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Jurnal No: ",xNoGL);
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xNoGL) <> 0 THEN
				DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL;
				SET oKeterangan = CONCAT("Batal menyimpan data Jurnal No: ", xNoGLBaru);
				IF	xKodeTrans = 'JA' THEN
					SELECT IFNULL(NoGL,'--') INTO xNoGL FROM ttgeneralledgerhd WHERE NoGL NOT LIKE '%=%' AND LEFT(NoGL,2) = 'JA' ORDER BY TglGL DESC LIMIT 1; 
					IF xNoGL IS NULL OR LOCATE('=',xNoGL) <> 0 THEN SET xNoGL = '--'; END IF;
				ELSEIF xKodeTrans = 'JP' THEN
					SELECT IFNULL(NoGL,'--') INTO xNoGL FROM ttgeneralledgerhd WHERE NoGL NOT LIKE '%=%' AND LEFT(NoGL,2) = 'JP' ORDER BY TglGL DESC LIMIT 1;
					IF xNoGL IS NULL OR LOCATE('=',xNoGL) <> 0 THEN SET xNoGL = '--'; END IF;
				ELSEIF xKodeTrans = 'GL' THEN
					SELECT IFNULL(NoGL,'--') INTO xNoGL FROM ttgeneralledgerhd WHERE NoGL NOT LIKE '%=%' AND LEFT(NoGL,2) = 'MM' ORDER BY TglGL DESC LIMIT 1;
					IF xNoGL IS NULL OR LOCATE('=',xNoGL) <> 0 THEN SET xNoGL = '--'; END IF;
				ELSE
					SELECT IFNULL(NoGL,'--') INTO xNoGL FROM ttgeneralledgerhd WHERE NoGL NOT LIKE '%=%' AND LEFT(NoGL,2) = 'JT' ORDER BY TglGL DESC LIMIT 1;
					IF xNoGL IS NULL OR LOCATE('=',xNoGL) <> 0 THEN SET xNoGL = '--'; END IF;
				END IF;				
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Jurnal No: ", xNoGL);
			END IF;
		END;
	END CASE;
END$$

DELIMITER ;

