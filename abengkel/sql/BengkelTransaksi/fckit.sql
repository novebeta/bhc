DELIMITER $$

DROP PROCEDURE IF EXISTS `fckit`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fckit`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xCKNo VARCHAR(10),
IN xSDNo VARCHAR(10),
IN xCKPart DECIMAL(14, 2),
IN xCKJasa DECIMAL(14, 2),
IN xCKNoOLD VARCHAR(10),
IN xSDNoOLD VARCHAR(10)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;
 			
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xCKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xCKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert
			IF oStatus = 0 THEN		
				INSERT INTO ttckit (CKNo, SDNo, CKPart, CKJasa) VALUES (xCKNo, xSDNo, xCKPart, xCKJasa);

            #Hitung Total
            SELECT SUM(CKPart) INTO @CKPart FROM ttCKit WHERE CKNo = xCKNo; 
            IF  @CKPart IS NOT NULL THEN UPDATE ttCKhd set CKTotalPart = @CKPart WHERE CKNo = xCKNo; END IF;
            SELECT SUM(CKJasa) INTO @CKJasa FROM ttCKit WHERE CKNo = xCKNo; 
            IF  @CKJasa IS NOT NULL THEN UPDATE ttCKhd set CKTotalJasa = @CKJasa WHERE CKNo = xCKNo; END IF;

				SET oKeterangan = CONCAT('Berhasil menambahkan data ', xCKNo,' - ', xSDNo, ' - ', FORMAT(xCKPart,2), ' - ', FORMAT(xCKJasa,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xCKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xCKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;		

			#Update
			IF oStatus = 0 THEN
				UPDATE ttckit
				SET CKNo = xCKNo, SDNo = xSDNo, CKPart = xCKPart, CKJasa = xCKJasa WHERE CKNoOLD = xCKNoOLD AND SDNoOLD = xSDNoOLD;

            #Hitung Total
            SELECT SUM(CKPart) INTO @CKPart FROM ttCKit WHERE CKNo = xCKNo; 
            IF  @CKPart IS NOT NULL THEN UPDATE ttCKhd set CKTotalPart = @CKPart WHERE CKNo = xCKNo; END IF;
            SELECT SUM(CKJasa) INTO @CKJasa FROM ttCKit WHERE CKNo = xCKNo; 
            IF  @CKJasa IS NOT NULL THEN UPDATE ttCKhd set CKTotalJasa = @CKJasa WHERE CKNo = xCKNo; END IF;

				SET oKeterangan = CONCAT("Berhasil mengubah data : ", xCKNo , ' - ' ,xSDNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;	
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xCKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xCKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Delete
			IF oStatus = 0 THEN
				DELETE FROM ttCKit WHERE SDNo = xSDNo AND CKNo = xCKNo;
            
            #Hitung Total
            SELECT SUM(CKPart) INTO @CKPart FROM ttCKit WHERE CKNo = xCKNo; 
            IF  @CKPart IS NOT NULL THEN UPDATE ttCKhd set CKTotalPart = @CKPart WHERE CKNo = xCKNo; END IF;
            SELECT SUM(CKJasa) INTO @CKJasa FROM ttCKit WHERE CKNo = xCKNo; 
            IF  @CKJasa IS NOT NULL THEN UPDATE ttCKhd set CKTotalJasa = @CKJasa WHERE CKNo = xCKNo; END IF;

				SET oKeterangan = CONCAT("Berhasil menghapus data : ", xCKNo , ' - ' ,xSDNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;	
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Batal menyimpan data : ", xSDNo );
		END;
	END CASE;
END$$

DELIMITER ;

