DELIMITER $$

DROP PROCEDURE IF EXISTS `jsd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jsd`(IN xSDNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE MyPesanKu VARCHAR(300) DEFAULT '';
	DECLARE MyPesankuItem VARCHAR(150) DEFAULT '';

	DECLARE oNilaiHGP DOUBLE DEFAULT 0;
	DECLARE oNilaiOLI DOUBLE DEFAULT 0;

	SET @NoUrutku = 0;

	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xSDNo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;

	SELECT SDNo, SVNo, SENo, NoGLSD, NoGLSV, SDTgl, SVTgl, KodeTrans, ASS, PosKode, KarKode, PrgNama, PKBNo, MotorNoPolisi, LokasiKode, SDNoUrut, SDJenis, SDTOP, SDDurasi, SDJamMasuk, SDJamSelesai, SDJamDikerjakan, SDTotalWaktu, SDStatus, SDKmSekarang, SDKmBerikut, SDKeluhan, SDKeterangan, SDPembawaMotor, SDHubunganPembawa, SDAlasanServis, SDTotalJasa, SDTotalQty, SDTotalPart, SDTotalGratis, SDTotalNonGratis, SDDiscFinal, SDTotalPajak, SDUangMuka, SDTotalBiaya, SDKasKeluar, SDTerbayar, KlaimKPB, KlaimC2, Cetak, UserID
	INTO  @SDNo,@SVNo,@SENo,@NoGLSD,@NoGLSV,@SDTgl,@SVTgl,@KodeTrans,@ASS,@PosKode,@KarKode,@PrgNama,@PKBNo,@MotorNoPolisi,@LokasiKode,@SDNoUrut,@SDJenis,@SDTOP,@SDDurasi,@SDJamMasuk,@SDJamSelesai,@SDJamDikerjakan,@SDTotalWaktu,@SDStatus,@SDKmSekarang,@SDKmBerikut,@SDKeluhan,@SDKeterangan,@SDPembawaMotor,@SDHubunganPembawa,@SDAlasanServis,@SDTotalJasa,@SDTotalQty,@SDTotalPart,@SDTotalGratis,@SDTotalNonGratis,@SDDiscFinal,@SDTotalPajak,@SDUangMuka,@SDTotalBiaya,@SDKasKeluar,@SDTerbayar,@KlaimKPB,@KlaimC2,@Cetak,@UserID
	FROM ttSDhd	WHERE SDNo = xSDNo;

	SELECT CusNama INTO @CusNama FROM tdcustomer WHERE MotorNoPolisi = @MotorNoPolisi; IF @CusNama IS NULL THEN SET @CusNama = ''; END IF;
   SET MyPesanKu = CONCAT("Post Service Daftar: " , @SDNo , " - " , @KodeTrans , " - ", @ASS, ' - ',@MotorNoPolisi , ' - ',@CusNama );
   SET MyPesanKu = TRIM(MyPesanKu);  IF LENGTH(MyPesanKu) > 300 THEN SET MyPesanKu = SUBSTRING(MyPesanKu,0, 299); END IF;
   IF LENGTH(MyPesanku) > 150 THEN SET MyPesankuItem = SUBSTRING(MyPesanku,0, 149); ELSE SET MyPesankuItem = MyPesanku; END IF;

	IF xNoGL = '--' OR xNoGL <> @NoGLSD THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGLSD;
	END IF;

	SELECT IFNULL(SUM(ttsditbarang.SDHrgBeli * ttsditbarang.SDQty),0) INTO oNilaiHGP FROM ttsditbarang
	INNER JOIN tdbarang ON ttsditbarang.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) <> 'OLI' AND ttsditbarang.SDNo = xSDNo ;

	SELECT IFNULL(SUM(ttsditbarang.SDHrgBeli * ttsditbarang.SDQty),0) INTO oNilaiOLI FROM ttsditbarang
	INNER JOIN tdbarang ON ttsditbarang.BrgKode = tdbarang.BrgKode
	WHERE UPPER(BrgGroup) = 'OLI' AND ttsditbarang.SDNo = xSDNo ;

	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL; DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;DELETE FROM ttglhpit WHERE NoGL = xNoGL;
   DELETE FROM ttgeneralledgerhd WHERE GLLink = xSDNo;

	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, DATE(@SDTgl), 'SD', MyPesanKu, 0, 0, xSDNo, xUserID, '--', xGLValid);

	#Item
	IF (oNilaiHGP) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SDTgl), (@NoUrutku:=@NoUrutku + 1),'11150601',MyPesankuItem, oNilaiHGP,0;
	END IF;
	IF (oNilaiOLI) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SDTgl), (@NoUrutku:=@NoUrutku + 1),'11150701',MyPesankuItem, oNilaiOLI,0;
	END IF;
	IF (oNilaiHGP) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SDTgl), (@NoUrutku:=@NoUrutku + 1),'11150602',MyPesankuItem, 0,oNilaiHGP;
	END IF;
	IF (oNilaiOLI) > 0 THEN
		INSERT INTO ttgeneralledgerit SELECT xNoGL, DATE(@SDTgl), (@NoUrutku:=@NoUrutku + 1),'11150702',MyPesankuItem, 0,oNilaiOLI;
	END IF;

	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttSDhd SET NoGLSD = xNoGL  WHERE SDNo = xSDNo;END$$

DELIMITER ;

