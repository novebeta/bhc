DELIMITER $$

DROP PROCEDURE IF EXISTS `fbmhd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fbmhd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xBMNo VARCHAR(12),
IN xBMNoBaru VARCHAR(12),
IN xBMTgl DATETIME,
IN xBMJenis VARCHAR(20),
IN xKodeTrans VARCHAR(4),
IN xPosKode VARCHAR(20),
IN xMotorNoPolisi VARCHAR(12),
IN xBankKode VARCHAR(25),
IN xBMPerson VARCHAR(50),
IN xBMCekNo VARCHAR(25),
IN xBMCekTempo DATE,
IN xBMAC VARCHAR(25),
IN xBMNominal DECIMAL(14, 2),
IN xBMMemo VARCHAR(150),
IN xNoGL VARCHAR(10),
IN xCetak VARCHAR(5)
)
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			DELETE FROM ttbmhd WHERE (BMNo LIKE '%=%') AND BMTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);			
			#ReJurnal Transaksi Tidak Memiliki Jurnal		
			cek01: BEGIN
				DECLARE rBMNo VARCHAR(100) DEFAULT '';
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT BMNo FROM ttbmhd
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttbmhd.BMNo 
				WHERE ttgeneralledgerhd.NoGL IS NULL AND BMNo NOT LIKE '%=%' AND BMTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rBMNo;
					IF rBMNo IS NOT NULL AND rBMNo <> '' THEN
						CALL jbm(rBMNo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;		
			SET oStatus = 0;SET oKeterangan = CONCAT("Daftar Bank Masuk ");
		END;
		WHEN "Load" THEN
		BEGIN
			 SET oStatus = 0; SET oKeterangan = CONCAT("Load data Bank Masuk No: ", xBMNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SET @AutoNo = CONCAT('BM','01',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(BMNo,5)+1),5,0)) INTO @AutoNo FROM ttbmhd WHERE ((BMNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(BMNo) = 12) ORDER BY BMNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 12 THEN SET @AutoNo = CONCAT('BM','01',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xBMNo = @AutoNo;
			SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Bank Masuk No: ",xBMNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @BMNoOLD = xBMNo;
			
			IF LOCATE('=',xBMNo) <> 0 THEN
				SET @AutoNo = CONCAT('BM',RIGHT(xBankKode,2),RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(BMNo,5)+1),5,0)) INTO @AutoNo FROM ttBMhd WHERE ((BMNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(BMNo) = 12) ORDER BY BMNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 12 THEN SET @AutoNo = CONCAT('BM',RIGHT(xBankKode,2),RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xBMNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xBMNoBaru = xBMNo;
			END IF;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Update
			IF oStatus = 0 THEN 
				UPDATE ttbmhd
				SET BMNo = xBMNoBaru, BMTgl = xBMTgl, BMJenis = xBMJenis, KodeTrans = xKodeTrans, PosKode = xPosKode, MotorNoPolisi = xMotorNoPolisi, 
				BankKode = xBankKode, BMPerson = xBMPerson, BMCekNo = xBMCekNo, BMCekTempo = xBMCekTempo, BMAC = xBMAC, BMNominal = xBMNominal, 
				BMMemo = xBMMemo, NoGL = xNoGL, Cetak = xCetak, UserID = xUserID
				WHERE BMNo = xBMNo;
				SET xBMNo = xBMNoBaru;
			
				#BMAutoN
				IF xKodeTrans = 'UM' THEN
					UPDATE ttbmitcoa SET BMAutoN = -(3*BMAutoN) WHERE BMNo = xBMNo;
					SET @row_number = 0;
					DROP TEMPORARY TABLE IF EXISTS TNoUrut;
					CREATE TEMPORARY TABLE IF NOT EXISTS TNoUrut AS
					SELECT BMNo, BMAutoN,(@row_number:=@row_number + 1) AS NoUrut , NoAccount FROM ttbmitcoa
					WHERE BMNo = xBMNo ORDER BY BMAutoN DESC;
					UPDATE ttbmitcoa INNER JOIN TNoUrut
					ON  ttbmitcoa.BMNo = TNoUrut.BMNo
					AND ttbmitcoa.NoAccount = TNoUrut.NoAccount
					AND ttbmitcoa.BMAutoN = TNoUrut.BMAutoN
					SET ttbmitcoa.BMAutoN = TNoUrut.NoUrut;
					DROP TEMPORARY TABLE IF EXISTS TNoUrut;
				END IF;
				
				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @BMNoOLD; #Hapus Jurnal No Transaksi Lama
				CALL jbm(xBMNo,xUserID); #Posting Jurnal
				
				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Bank Masuk','ttbmhd', xBMNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Bank Masuk No: ", xBMNo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Bank Masuk','ttbmhd', xBMNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Bank Masuk No: ", xBMNo);
				END IF;
			ELSEIF oStatus = 1 THEN
				IF LOCATE('=',xBMNo) <> 0 THEN
					DELETE FROM ttbmhd WHERE BMNo = xBMNo;
					SELECT IFNULL(BMNo,'--') INTO xBMNo FROM ttBMhd WHERE BMNo NOT LIKE '%=%' AND LEFT(BMNo,2) = 'BM' AND KodeTrans = xKodeTrans ORDER BY BMTgl DESC LIMIT 1; 
					IF xBMNo IS NULL OR LOCATE('=',xBMNo) <> 0 THEN SET xBMNo = '--'; END IF;
				END IF;			
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;
			
			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBMNo;
			IF @GLValid = 'Sudah' THEN	
				SET oStatus = 1; SET oKeterangan = CONCAT("No BM:", xBMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM ttbmhd WHERE BMNo = xBMNo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xBMNo;
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Bank Masuk','ttbmhd', xBMNo); 
				SET oKeterangan = CONCAT("Berhasil menghapus data Bank Masuk No: ",xBMNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xBMNo) <> 0 THEN
				DELETE FROM ttbmhd WHERE BMNo = xBMNo;
				SET oKeterangan = CONCAT("Batal menyimpan data Bank Masuk No: ", xBMNoBaru);
				SELECT IFNULL(BMNo,'--') INTO xBMNo FROM ttBMhd WHERE BMNo NOT LIKE '%=%' AND LEFT(BMNo,2) = 'BM' AND KodeTrans = xKodeTrans ORDER BY BMTgl DESC LIMIT 1; 
				IF xBMNo IS NULL OR LOCATE('=',xBMNo) <> 0 THEN SET xBMNo = '--'; END IF;
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Bank Masuk No: ", xBMNo);
			END IF;
		END;
		WHEN "Jurnal" THEN
		BEGIN
			 IF LOCATE('=',xBMNo) <> 0 THEN 
				  SET oStatus = 1;
				  SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			 ELSE
				  CALL jbm(xBMNo,xUserID);
				  SET oStatus = 0;
				  SET oKeterangan = CONCAT("Berhasil memproses Jurnal Bank Masuk No: ", xBMNo);
			 END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
		END;
		WHEN "LoopAfter" THEN
		BEGIN
			SELECT SUM(BKBayar) INTO @sumBKBayar FROM ttBKit WHERE BKNo = xBKNo;
			IF @sumBKBayar IS NOT NULL THEN
				UPDATE ttBKhd SET BKNominal = @sumBKBayar WHERE BKNo = xBKNo AND BKNominal <= @sumBKBayar;
			END IF;
		END;
	END CASE;
END$$

DELIMITER ;

