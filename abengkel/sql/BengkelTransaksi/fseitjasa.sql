DELIMITER $$

DROP PROCEDURE IF EXISTS `fseitjasa`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fseitjasa`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xSENo VARCHAR(10),
IN xSEAuto SMALLINT(6),
IN xJasaKode VARCHAR(10),
IN xSEWaktuKerja DECIMAL(3, 0),
IN xSEWaktuSatuan VARCHAR(5),
IN xSEWaktuKerjaMenit DECIMAL(3, 0),
IN xSEHargaBeli DECIMAL(14, 2),
IN xSEHrgJual DECIMAL(14, 2),
IN xSEJualDisc DECIMAL(14, 2),
IN xSENoOLD VARCHAR(10),
IN xSEAutoOLD VARCHAR(10),
IN xJasaKodeOLD VARCHAR(10)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			 SET oStatus = 0;
			 
 			#Mengambil Harga Beli
			SELECT JasaHrgBeli INTO xSEHargaBeli FROM tdjasa WHERE JasaKode = xJasaKode;
			
			#Insert
			IF oStatus = 0 THEN
				SELECT IF(ISNULL((MAX(SEAuto)+1)),1,(MAX(SEAuto)+1)) INTO xSEAuto FROM ttseitjasa WHERE SENo = xSENo; #AutoN													
				INSERT INTO ttseitjasa
				(SENo, SEAuto, JasaKode, SEWaktuKerja, SEWaktuSatuan, SEWaktuKerjaMenit, SEHargaBeli, SEHrgJual, SEJualDisc) VALUES
				(xSENo, xSEAuto, xJasaKode, xSEWaktuKerja, xSEWaktuSatuan, xSEWaktuKerjaMenit, xSEHargaBeli, xSEHrgJual, xSEJualDisc);

            #Hitung SubTotal & Total
            SELECT SUM((SEQty * SEHrgJual) - SEJualDisc) INTO @SETotalPart FROM ttseitbarang WHERE SENo = xSENo;
            SELECT SUM(SEHrgJual - SEJualDisc) INTO @SETotalJasa FROM ttseitjasa WHERE SENo = xSENo;
            IF @SETotalPart IS NULL THEN SET  @SETotalPart = 0; END IF;
            IF @SETotalJasa IS NULL THEN SET  @SETotalJasa = 0; END IF;
            UPDATE ttsehd SET SETotalJasa = @SETotalJasa, SETotalPart = @SETotalPart,
            SETotalBiaya = @SETotalJasa + @SETotalPart  WHERE SENo = xSENo;

				SET oKeterangan = CONCAT('Berhasil menambahkan data Jasa ', xJasaKode, ' = ' , FORMAT(xSEHrgJual,0));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
		 
			#Jika Beda Kode Jasa, ambil Harga Beli
			IF xJasaKode <> xJasaKodeOLD THEN
				SELECT JasaHrgBeli INTO xSEHargaBeli FROM tdjasa WHERE JasaKode = xJasaKode;
			END IF;
					
			IF oStatus = 0 THEN
				UPDATE ttseitjasa
				SET SENo = xSENo, SEAuto = xSEAuto, JasaKode = xJasaKode, SEWaktuKerja = xSEWaktuKerja, SEWaktuSatuan = xSEWaktuSatuan, 
				SEWaktuKerjaMenit = xSEWaktuKerjaMenit, SEHargaBeli = xSEHargaBeli, SEHrgJual = xSEHrgJual, SEJualDisc = xSEJualDisc
				WHERE SENo = xSENoOLD AND SEAuto = xSEAutoOLD AND JasaKode = xJasaKodeOLD;

            #Hitung SubTotal & Total
            SELECT SUM((SEQty * SEHrgJual) - SEJualDisc) INTO @SETotalPart FROM ttseitbarang WHERE SENo = xSENo;
            SELECT SUM(SEHrgJual - SEJualDisc) INTO @SETotalJasa FROM ttseitjasa WHERE SENo = xSENo;
            IF @SETotalPart IS NULL THEN SET  @SETotalPart = 0; END IF;
            IF @SETotalJasa IS NULL THEN SET  @SETotalJasa = 0; END IF;
            UPDATE ttsehd SET SETotalJasa = @SETotalJasa, SETotalPart = @SETotalPart,
            SETotalBiaya = @SETotalJasa + @SETotalPart  WHERE SENo = xSENo;

				SET oKeterangan = CONCAT("Berhasil mengubah data Jasa : ", xJasaKode, ' = ' , FORMAT(xSEHrgJual,0));
			ELSE
				SET oKeterangan = CONCAT("Gagal mengubah data jasa : ", xJasaKode, ' = ' , FORMAT(xSEHrgJual,0));
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSENo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSENo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			IF oStatus = 0 THEN
				DELETE FROM ttseitjasa WHERE SEAuto = xSEAuto AND SENo = xSENo AND JasaKode = xJasaKode;

            #Hitung SubTotal & Total
            SELECT SUM((SEQty * SEHrgJual) - SEJualDisc) INTO @SETotalPart FROM ttseitbarang WHERE SENo = xSENo;
            SELECT SUM(SEHrgJual - SEJualDisc) INTO @SETotalJasa FROM ttseitjasa WHERE SENo = xSENo;
            IF @SETotalPart IS NULL THEN SET  @SETotalPart = 0; END IF;
            IF @SETotalJasa IS NULL THEN SET  @SETotalJasa = 0; END IF;
            UPDATE ttsehd SET SETotalJasa = @SETotalJasa, SETotalPart = @SETotalPart,
            SETotalBiaya = @SETotalJasa + @SETotalPart  WHERE SENo = xSENo;

				SET oKeterangan = CONCAT("Berhasil menghapus data Jasa : ", xJasaKode, ' = ' , FORMAT(xSEHrgJual,0));
			ELSE
				SET oKeterangan = CONCAT("Gagal menghapus data Jasa : ", xJasaKode,  ' = ' , FORMAT(xSEHrgJual,0));
			END IF;	
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Batal menyimpan data jasa: ", xSEAuto );
		END;
	END CASE;
END$$

DELIMITER ;

