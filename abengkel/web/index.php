<?php
$server_name = $_SERVER[ 'SERVER_NAME' ];
if ( ! in_array( $_SERVER[ 'SERVER_PORT' ], [ 80, 443 ] ) ) {
	$port = ":$_SERVER[SERVER_PORT]";
} else {
	$port = '';
}
if ( ! empty( $_SERVER[ 'HTTPS' ] ) && ( strtolower( $_SERVER[ 'HTTPS' ] ) == 'on' || $_SERVER[ 'HTTPS' ] == '1' ) ) {
	$scheme = 'https';
} else {
	$scheme = 'http';
}
if ( isset( $_COOKIE[ '_outlet' ] ) && isset( $_COOKIE[ 'PHPSESSID' ] ) ) {
//	if ( (strlen( base64_decode($_COOKIE['_outlet']) ) == 3) || (strtolower(base64_decode( $_COOKIE['_outlet'] )) == 'hfinance') ) {
//		header( "Location: " . $scheme . '://' . $server_name . $port . '/aunit/web/index.php?r=site/index' );
//		die();
//	}
	switch ( strlen( base64_decode( $_COOKIE[ '_outlet' ] ) ) ) {
		case 3 :
			header( "Location: " . $scheme . '://' . $server_name . $port . '/aunit/web/index.php?r=site/index' );
			die();
			break;
		case 4 :
			break;
		default :
			header( "Location: " . $scheme . '://' . $server_name . $port . '/afinance/web/index.php?r=site/index' );
			die();
			break;
	}
} else {
	session_start();
	session_destroy();
	setcookie( 'PHPSESSID', '', time() - 1000, '/' );
	header( "Location: " . $scheme . '://' . $server_name . $port . '/login/web/index.php?r=site/index' );
	die();
}
$GLOBALS[ 'timezone' ] = "Asia/Jakarta";
if ( in_array( strtolower( base64_decode( $_COOKIE[ '_outlet' ] ) ), [ 'baug', 'bamt', 'bap1', 'bap4', 'bap5', 'bap2', 'bap7', 'bmup', 'bapm', 'bap3', 'bcml', 'bap8','bapo' ] ) ) {
	$GLOBALS[ 'timezone' ] = "Asia/Makassar";
}
date_default_timezone_set( $GLOBALS[ 'timezone' ] );
defined( 'YII_DEBUG' ) or define( 'YII_DEBUG', false );
defined( 'YII_ENV' ) or define( 'YII_ENV', 'prod' );
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/../../common/config/bootstrap.php';
require __DIR__ . '/../config/bootstrap.php';
$config = yii\helpers\ArrayHelper::merge(
	require __DIR__ . '/../../common/config/main.php',
	require __DIR__ . '/../../common/config/main-local.php',
	require __DIR__ . '/../config/main.php',
	require __DIR__ . '/../config/main-local.php'
);
( new yii\web\Application( $config ) )->run();
