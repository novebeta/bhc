$.var = {};
$.fn.utilForm = function () {
    var form = this;
    return {
        addHidden(array=[]){
            var input = {};
            if (Array.isArray(array)) {
                var keys = _.keys(array);
                for (var i = 0; i < keys.length; i++) {
                    input = $("<input>").attr("type", "hidden")
                        .attr("name", keys[i])
                        .val(array[keys[i]]);
                    form.append($(input));
                }
            }
            return form;
        },
        getData(serialize = false){
            var data = (serialize ? '' : []);
            form.find('[name]').each(function(){
                let nama = $(this).prop('name');
                if (nama !== undefined){
                    if (serialize){
                        data += '&' + encodeURIComponent(nama) + '=' + encodeURIComponent($(this).val());
                    }else {
                        data[nama] = $(this).val();
                    }
                }
            });
            return data;
        },
        submit(action, method, target){
            let arr = form.utilForm().getData();
            var formNew = $(document.createElement('form'));
            if(action === undefined){
                action = form.attr('action');
            }
            if(method === undefined){
                method = form.attr('method');
            }
            if(target === undefined){
                target = form.attr('target');
            }
            $(formNew).attr("action", action);
            $(formNew).attr("method", method);
            $(formNew).attr("target", target);
            $(formNew).utilForm().addHidden(arr);
            $(formNew).appendTo( $('body') );
            $(formNew).submit();
        }
    };
};
$.fn.utilSelect2 = function () {
    var select2 = this;

    function _addItem(data) {
        var key1 = Object.keys(data)[0],
            value1 = data[key1],
            id = data['id'] !== undefined ? data['id'] : (data['value'] !== undefined ? data['value'] : value1),
            text = data['text'] !== undefined ? data['text'] : (data['label'] !== undefined ? data['label'] : value1);
        select2.append(new Option(text, id, false, false)); //.trigger('change');
    }

    function _addItems(data, replaceAll) {
        if (replaceAll) _clearItems();
        if (Array.isArray(data)) for (i = 0; i < data.length; i++) _addItem(data[i]);
    }

    function _ajax(url, params, replaceAll, callback) {
        var data = params || {};
        $.ajax({
            type: 'POST',
            url: url,
            data: data
        }).then(function (data) { // console.log(data);
            if (data.success) {
                _addItems(data.rows, replaceAll);
                _trigger(data.rows);
            } else {
                console.log(data.message);
            }
            if (typeof callback === "function")
                callback(data);
        });
    }

    function _trigger(data) {
        select2.trigger({
            type: 'select2:select',
            params: {
                data: data
            }
        });
    }

    function _clearItems() {
        select2.empty();
    }

    return {
        setSelection: function (value) {
            select2.val(value);
            select2.trigger('change');
            return this;
        },
        clearSelection: function () {
            select2.val(null).trigger('change');
            return this;
        },
        getSelectedData: function () {
            return select2.select2('data');
        },
        clearAllItems: function () {
            _clearItems();
            return this;
        },
        loadData: function (data, replaceAll) {
            if (Array.isArray(data) && data.length > 0 && data[0].constructor === ({}).constructor) {
                _addItems(data, replaceAll);
                _trigger(data);
            } else if (data.constructor === ({}).constructor) {
                if (replaceAll === true) _clearItems();
                _addItem(data);
                _trigger([data]);
            }
            return this;
        },
        loadRemote: function (url, params, replaceAll, callback) {
            _ajax(url, params, replaceAll, callback);
        }
    };
};
$.fn.utilNumberControl = function () {
    var numberControl = this,
        numberControlDisp = $('[id="' + numberControl.attr('id') + '-disp"]');

    function _ajax(url, params, callback) {
        var data = params || {};
        $.ajax({
            type: 'POST',
            url: url,
            data: data
        }).then(function (data) { // console.log(data);
            if (data.success) {
                let value = parseFloat(data.rows[params.field]);
                if (numberControlDisp.length > 0){
                    numberControl.val(value);
                    numberControlDisp.inputmask('setvalue',value);
                } else{
                    numberControl.inputmask('setvalue',value);
                }
            } else {
                console.log(data.message);
            }
            if (typeof callback === "function")
                callback(data);
        });
    }

    return {
        cmp: function (){
            return numberControlDisp;
        },
        val: function (v) {
            if (v === undefined)
                return parseFloat(numberControl.val());
            else {
                let value = window.util.number.getValueOfEuropeanNumber(v);
                if (numberControlDisp.length > 0){
                    numberControl.val(value);
                    numberControlDisp.inputmask('setvalue',value);
                } else{
                    numberControl.inputmask('setvalue',value);
                }
                return this;
            }
        },
        loadRemote: function (url, params, callback) {
            _ajax(url, params, callback);
        }
    }
};
$.fn.utilDateControl = function () {
    var dateControl = this,
        dateControlDisp = $('[id="' + dateControl.attr('id') + '-disp"]');
    return {
        cmp: function (){
            return dateControlDisp;
        },
        getDate: function (){
            return dateControlDisp.kvDatepicker('getDate');
        },
        val: function (v) {
            /* input: yyyy-mm-dd, saved format dd/mm/yyyy */
            var arrDate = v.split("-");
            dateControlDisp.val(arrDate[2] + '/' + arrDate[1] + '/' + arrDate[0]).trigger('change');
            return this;
        },
        setDate(dt){
            dateControlDisp.kvDatepicker("update", dt).trigger('change');
        }
    }
};
$.var.utilJqGrid = {};
$.fn.utilJqGrid = function (configs={}) {
    var grid = this,
        gridId = grid.attr('id');
    defaultPagerId = null,
        pager = null,
        defaultConfigs = null,
        ajaxRowOpt = null,
        /* custom config */
        gridVariable = $.var.utilJqGrid[gridId] === undefined ? {} : $.var.utilJqGrid[gridId];
    readOnly = false,
        navButtonTambah = true;
    let searchParams = new URLSearchParams(window.location.search);
    if (searchParams.get('action') === 'view' || searchParams.get('action') === 'display'){
        configs.readOnly = true;
    }
    var colFunctions = {
        actionsFormatter: function (cellVal, options, rowObject) {

            var btnEdit = '<div title="Edit data terpilih" style="float: left; cursor: pointer;" class="ui-pg-div ui-inline-edit" id="jEditButton_' + gridId + '_' + options.rowId + '" onclick="$(\'#' + gridId + '\').utilJqGrid().editRow(\'' + options.rowId + '\');" onmouseover="$(this).addClass(\'active\');" onmouseout="$(this).removeClass(\'active\');">' +
                '<span class="glyphicon glyphicon-edit"></span>' +
                '</div>';
            if (configs.hasOwnProperty('navButtonEdit') && configs.navButtonEdit === false){
                btnEdit = '';
            }
            var btnHapus = '<div title="Hapus baris terpilih" style="float: left; cursor: pointer;" class="ui-pg-div ui-inline-del" id="jDeleteButton_' + gridId + '_' + options.rowId + '" onclick="$(\'#' + gridId + '\').utilJqGrid().deleteRow(\'' + options.rowId + '\');" onmouseover="$(this).addClass(\'active\');" onmouseout="$(this).removeClass(\'active\');">' +
                '<span class="glyphicon glyphicon-trash"></span>' +
                '</div>';
            if (configs.hasOwnProperty('navButtonHapus') && configs.navButtonHapus === false){
                btnHapus = '';
            }
            return '' +
                '<div style="margin-left:8px;">' + btnEdit + btnHapus +
                '<div title="Simpan" style="float: left; display: none; cursor: pointer;" class="ui-pg-div ui-inline-save" id="jSaveButton_' + gridId + '_' + options.rowId + '" onclick="$(\'#' + gridId + '\').utilJqGrid().saveRow(this,\'' + options.rowId + '\');" onmouseover="$(this).addClass(\'active\');" onmouseout="$(this).removeClass(\'active\');">' +
                '<span class="glyphicon glyphicon-floppy-disk"></span>' +
                '</div>' +
                '<div title="Batal" style="float: left; display: none; cursor: pointer;" class="ui-pg-div ui-inline-cancel" id="jCancelButton_' + gridId + '_' + options.rowId + '" onclick="$.fn.fmatter.rowactions.call(this,\'cancel\');" onmouseover="$(this).addClass(\'active\');" onmouseout="$(this).removeClass(\'active\');">' +
                '<span class="fa fa-ban"></span>' +
                '</div></div>';
        },
        mergeConfig: function (target, source, exclude) {
            exclude = Array.isArray(exclude) ? exclude : (typeof exclude === 'string' ? exclude.split(',') : []);
            for (var property in source) {
                if (source.hasOwnProperty(property) && !exclude.includes(property))
                    target[property] = source[property];
            }
        },
        isEditable: function (colModel) {
            return colModel.hasOwnProperty('editable') && colModel.editable;
        },
        getName: function (colMOdel) {
            return colMOdel.hasOwnProperty('name') ? colMOdel.name : (colMOdel.hasOwnProperty('index') ? colMOdel.index : null);
        },
        addOnEditAction: function (func) {
            if(gridVariable['onEditActions'] === undefined) gridVariable['onEditActions'] = [];
            if(typeof func === "function") gridVariable.onEditActions.push(func);
        }
    };

    function mergeConfig() {
        exclude = ['readOnly', 'extraParams', 'listeners'];
        for (var property in defaultConfigs) {
            if (defaultConfigs.hasOwnProperty(property))
                if (!configs.hasOwnProperty(property) && !exclude.includes(property))
                    configs[property] = defaultConfigs[property];
                else if (property === 'pager') {
                    $(configs[property]).remove();
                    configs[property] = defaultConfigs[property];
                }
        }
        pager.insertAfter(grid);
    }

    function applyCustomConfig() {
        gridVariable['listeners'] = {};
        let gridEvent = ['loadComplete', 'onSelectRow', 'ondblClickRow'];
        for (var property in configs) {
            if (configs.hasOwnProperty(property))
                switch (property) {
                    case 'readOnly':
                        readOnly = configs.readOnly;
                        gridVariable[property] = configs[property];
                        break;
                    case 'navButtonTambah':
                        navButtonTambah = configs.navButtonTambah;
                        break;
                    case 'listeners':
                        gridVariable[property] = configs[property];
                        for(var event in configs[property]) {
                            if(configs[property].hasOwnProperty(event) && gridEvent.includes(event)){
                                configs[event] = configs[property][event];
                            }
                        }
                        break;
                    case 'multiselect':
                    case 'url':
                        gridVariable[property] = configs[property];
                        break;
                    case 'extraParams':
                        var extraParams = '';
                        for (var key in configs.extraParams)
                            if (configs.extraParams.hasOwnProperty(key))
                                extraParams += "&" + key + "=" + encodeURIComponent(configs.extraParams[key]);//encodeURIComponent to handle symbol + in url (KK0124+00281)
                        gridVariable[property] = extraParams;
                        break;
                    default :
                        gridVariable[property] = configs[property];
                        break;
                }
        }
        gridVariable.newRowCounter = 0;
        $.var.utilJqGrid[gridId] = gridVariable;
    }

    function parseColModel(colModels) {
        var cols = [];
        gridVariable.colName = [];
        gridVariable.colEditor = [];

        if(!window.hasOwnProperty('cmp')) window.cmp = {};

        for (var i = 0; i < colModels.length; i++) {
            var colModel = colModels[i],
                useTemplate = colModel.hasOwnProperty('template'),
                isEditable = colFunctions.isEditable(colModel) && !readOnly,
                hasEditorConfig = colModel.hasOwnProperty('editor'),

                colName = colModel.hasOwnProperty('name') ? colModel.name : '',
                colName_initScript = colName ? 'window.cmp[options.id] = $(element); $.var.utilJqGrid[\''+gridId+'\'][\'colEditor\'][options.id] = $(element);' : '';

            if(colName)
                gridVariable.colName.push(colName);

            if (!useTemplate && !hasEditorConfig) {

                if(isEditable && colName) {
                    if(colModel['editoptions'] === undefined) colModel['editoptions'] = {};
                    colModel['editoptions']['dataInit'] = new Function('element,options', colName_initScript);
                }
                cols.push(colModel);
                continue;
            }

            var col = {},target = {},
                exclude = [];
            if (colModel.hasOwnProperty('editoptions')){
                col['editoptions'] = colModel.editoptions;
            }
            if (useTemplate) {
                /* parse template */
                var defaultEditor;
                switch (colModel.template) {
                    case 'actions':
                        if (!readOnly) {
                            target = {
                                label: "Actions",
                                name: "actions",
                                width: 65,
                                formatter: colFunctions.actionsFormatter,
                                formatoptions: {
                                    editbutton: false,
                                    isDisplayButtons: function (opts, rwd, act) {
                                        return {
                                            save: { display: true },
                                            cancel: { display: true }
                                        };
                                    }
                                }};

                        }else{
                            target = {
                                label: "Actions",
                                name: "actions",
                                width: 65,
                            };
                        }
                        col = Object.assign(target, col);
                        exclude.concat(['name', 'formatter']);
                        break;
                    case 'tahun':
                        target = {
                            align: "right",
                        };
                        col = Object.assign(target, col);
                        exclude.concat([ 'align', 'formatter', 'formatoptions' ]);

                        if(isEditable)
                            if(!hasEditorConfig) {
                                hasEditorConfig = true;
                                colModel.editor = { type: 'tahun' };
                            } else if(!colModel.editor.hasOwnProperty('type'))
                                colModel.editor['type'] = 'tahun';

                        break;
                    case 'integer':
                        target = {
                            align: "right",
                            formatter: "number",
                            formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0}
                        };
                        col = Object.assign(target, col);
                        exclude.concat([ 'align', 'formatter', 'formatoptions' ]);

                        if(isEditable)
                            if(!hasEditorConfig) {
                                hasEditorConfig = true;
                                colModel.editor = { type: 'integer' };
                            } else if(!colModel.editor.hasOwnProperty('type'))
                                colModel.editor['type'] = 'integer';
                            break;
                    case 'number':
                        target = {
                            align: "right",
                            formatter: "number",
                            formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0}
                        };
                        col = Object.assign(target, col);
                        exclude.concat([ 'align', 'formatter', 'formatoptions' ]);

                        if(isEditable)
                            if(!hasEditorConfig) {
                                hasEditorConfig = true;
                                colModel.editor = { type: 'number' };
                            } else if(!colModel.editor.hasOwnProperty('type'))
                                colModel.editor['type'] = 'number';

                        break;
                    case 'money':
                        target = {
                            align: "right",
                            formatter: "number",
                            formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 2}
                        };

                        col = Object.assign(target, col);
                        exclude.concat([ 'align', 'formatter', 'formatoptions' ]);

                        if(isEditable)
                            if(!hasEditorConfig) {
                                hasEditorConfig = true;
                                colModel.editor = { type: 'money' };
                            } else if(!colModel.editor.hasOwnProperty('type'))
                                colModel.editor['type'] = 'money';

                        break;
                }

                if(!hasEditorConfig && isEditable && defaultEditor) {
                    hasEditorConfig = true;
                    colModel.editor = defaultEditor;
                }
            }

            if (hasEditorConfig && isEditable){
                /* parse editor */
                gridVariable['editorConfig'] = {};
                if(gridVariable['europeanNumberFields'] === undefined) gridVariable['europeanNumberFields'] = [];
                if(gridVariable['editorConfigs'] === undefined) gridVariable['editorConfigs'] = { select2: {}, jqxComboBox: {} };

                var cmpId = colModel.editor.hasOwnProperty('cmpId') ? colModel.editor.cmpId : '',
                    cmpId_initScript = (cmpId ? 'window.cmp[options.rowId+\'_'+cmpId+'\'] = $(element);' : '') + colName_initScript;

                switch (colModel.editor.type) {
                    case 'text':
                        col['editoptions'] = {};

                        if(colModel.editor.hasOwnProperty('readOnly') && (colModel.editor.readOnly == true))
                            col['editoptions']['readonly'] = 'readonly';

                        col['edittype'] = "text";
                        exclude.concat([ 'edittype', 'editoptions' ]);
                        break;

                    case 'money':
                        if(col['editoptions'] === undefined) col['editoptions'] = {};
                        col['editoptions']['dataInit'] = new Function('element,options','$(element).attr(\'style\', \'text-align: right;\').inputmask({alias:"numeric",digits:2,digitsOptional:false,groupSeparator:".",radixPoint:",",autoGroup:true,autoUnmask:false}); '+cmpId_initScript);

                        if(colModel.editor.hasOwnProperty('readOnly')&& (colModel.editor.readOnly == true))
                            col['editoptions']['readonly'] = 'readonly';

                        colFunctions.addOnEditAction(new Function('grid, rowId','let data = grid.utilJqGrid().getData(rowId); $(\'[id="\'+rowId+\'_\'+\''+colModel.name+'\'+\'"]\').inputmask("setvalue",parseFloat(data.data.'+colModel.name+'));'));
                        col['edittype'] = "text";
                        exclude.concat([ 'edittype', 'editoptions' ]);
                        gridVariable['europeanNumberFields'].push(colFunctions.getName(colModel));
                        break;

                    case 'number':
                        if(col['editoptions'] === undefined) col['editoptions'] = {};
                        let prop = '{alias:"numeric",digits:0,groupSeparator:".",radixPoint:",",autoGroup:true,autoUnmask:false}';
                        if(colModel.editor.hasOwnProperty('prop'))
                            prop = JSON.stringify(colModel.editor.prop);
                        col['editoptions']['dataInit'] = new Function('element,options','$(element).attr(\'style\', \'text-align: right;\').inputmask('+prop+'); '+cmpId_initScript);

                        if(colModel.editor.hasOwnProperty('readOnly')&& (colModel.editor.readOnly == true))
                            col['editoptions']['readonly'] = 'readonly';

                        colFunctions.addOnEditAction(new Function('grid, rowId','let data = grid.utilJqGrid().getData(rowId); $(\'[id="\'+rowId+\'_\'+\''+colModel.name+'\'+\'"]\').inputmask("setvalue",parseFloat(data.data.'+colModel.name+'));'));
                        col['edittype'] = "text";
                        exclude.concat([ 'edittype', 'editoptions' ]);
                        gridVariable['europeanNumberFields'].push(colFunctions.getName(colModel));
                        break;

                    case 'integer':
                        if(col['editoptions'] === undefined) col['editoptions'] = {};
                        col['editoptions']['dataInit'] = new Function('element,options','$(element).attr(\'style\', \'text-align: right;\').inputmask({alias:"numeric",integerDigits:5,digits:0,groupSeparator:".",radixPoint:",",autoGroup:true,autoUnmask:false}); '+cmpId_initScript);

                        if(colModel.editor.hasOwnProperty('readOnly')&& (colModel.editor.readOnly == true))
                            col['editoptions']['readonly'] = 'readonly';

                        col['edittype'] = "text";
                        exclude.concat([ 'edittype', 'editoptions' ]);
                        gridVariable['europeanNumberFields'].push(colFunctions.getName(colModel));
                        break;

                    case 'tahun':
                        if(col['editoptions'] === undefined) col['editoptions'] = {};
                        col['editoptions']['dataInit'] = new Function('element,options','$(element).attr(\'style\', \'text-align: right;\').inputmask({alias:"numeric",integerDigits:4,digits:0,groupSeparator:"",radixPoint:"",autoGroup:false,autoUnmask:false}); '+cmpId_initScript);

                        if(colModel.editor.hasOwnProperty('readOnly')&& (colModel.editor.readOnly == true))
                            col['editoptions']['readonly'] = 'readonly';

                        col['edittype'] = "text";
                        exclude.concat([ 'edittype', 'editoptions' ]);
                        gridVariable['europeanNumberFields'].push(colFunctions.getName(colModel));
                        break;

                    case 'select2':
                        let configIndex = Object.keys(gridVariable['editorConfigs']['select2']).length;

                        var editorConfig = {
                            dropdownAutoWidth: true,
                            closeOnSelect: true,
                        };

                        if(colModel.editor.hasOwnProperty('editorConfig'))
                            editorConfig = Object.assign(editorConfig, colModel.editor.editorConfig);

                        if(colModel.editor.hasOwnProperty('readOnly')&& (colModel.editor.readOnly == true))
                            editorConfig['disabled'] = colModel.editor.readOnly;

                        if(colModel.editor.hasOwnProperty('dropdownAutoWidth'))
                            editorConfig['dropdownAutoWidth'] = colModel.editor.dropdownAutoWidth;

                        if(colModel.editor.hasOwnProperty('data')) {
                            let data = colModel.editor.data;

                            if(colModel.editor.hasOwnProperty('valueField')) {
                                $.map(data, function (row) {
                                    row.id = row[colModel.editor.valueField];
                                    return row;
                                });
                            } else if(data.length && !data[0].hasOwnProperty('id')) {
                                if(data[0].hasOwnProperty('value'))
                                    $.map(data, function (row) { row.id = row.value; return row; });
                                else
                                    $.map(data, function (row) { row.id = row[Object.keys(data[0])[0]]; return row; });
                            }

                            if(colModel.editor.hasOwnProperty('displayField')) {
                                $.map(data, function (row) {
                                    row.text = row[colModel.editor.displayField];
                                    return row;
                                });
                            } else if(data.length && !data[0].hasOwnProperty('text')) {
                                if(data[0].hasOwnProperty('label'))
                                    $.map(data, function (row) { row.text = row.label; return row; });
                                else
                                    $.map(data, function (row) { row.text = row[Object.keys(data[0])[0]]; return row; });
                            }

                            editorConfig['data'] = data;
                        }

                        if(colModel.editor.hasOwnProperty('templateResult') && typeof colModel.editor.templateResult === "function") {
                            /* example: function(data) { return  $('<span>'+data.id+'</span>'); } */
                            editorConfig['templateResult'] = colModel.editor.templateResult;
                        }

                        if(colModel.editor.hasOwnProperty('searchFields') && Array.isArray(colModel.editor.searchFields)) {
                            editorConfig['searchFields'] = colModel.editor.searchFields;
                            editorConfig['matcher'] = function matchCustom(params, data) {
                                if ($.trim(params.term) === '') return data;
                                if (typeof data.text === 'undefined') return null;
                                let searchFields = $.var.utilJqGrid[gridId]['editorConfigs']['select2'][configIndex]['searchFields'];

                                for (var i = 0; i < searchFields.length; i++)
                                    if (data[searchFields[i]].toLowerCase().indexOf(params.term) > -1) return data;

                                return null;
                            };
                        }

                        gridVariable['editorConfigs']['select2'][configIndex] = editorConfig;
                        if(col['editoptions'] === undefined) col['editoptions'] = {};
                        col['editoptions']['dataInit'] = new Function('element,options','$(element).select2($.var.utilJqGrid[\''+gridId+'\'][\'editorConfigs\'][\'select2\']['+configIndex+']); '+cmpId_initScript);
                        col['edittype'] = "select";

                        colFunctions.addOnEditAction(new Function('grid, rowId','let data = grid.utilJqGrid().getData(rowId); $(\'[id="\'+rowId+\'_\'+\''+colModel.name+'\'+\'"]\').utilSelect2().setSelection(data.data.'+colModel.name+')'));

                        exclude.concat([ 'edittype', 'editoptions' ]);
                        break;

                    case 'jqxComboBox':
                    case 'jqcombobox':
                        var jqxComboBoxConfigIndex = Object.keys(gridVariable['editorConfigs']['jqxComboBox']).length;

                        var jqxComboBoxSettings = {
                            height: '30px',
                            theme: 'bootstrap'
                        };

                        if(colModel.hasOwnProperty('width'))
                            jqxComboBoxSettings.width = colModel.width-5;

                        if(colModel.editor.hasOwnProperty('data')) {
                            let data = colModel.editor.data;

                            if(colModel.editor.hasOwnProperty('valueField')) {
                                $.map(data, function (row) {
                                    row.value = row[colModel.editor.valueField];
                                    return row;
                                });
                            } else if(data.length && !data[0].hasOwnProperty('value')) {
                                if(data[0].hasOwnProperty('id'))
                                    $.map(data, function (row) { row.value = row.id; return row; });
                                else
                                    $.map(data, function (row) { row.value = row[Object.keys(data[0])[0]]; return row; });
                            }

                            if(colModel.editor.hasOwnProperty('displayField')) {
                                $.map(data, function (row) {
                                    row.label = row[colModel.editor.displayField];
                                    return row;
                                });
                            } else if(data.length && !data[0].hasOwnProperty('label')) {
                                if(data[0].hasOwnProperty('text'))
                                    $.map(data, function (row) { row.label = row.text; return row; });
                                else
                                    $.map(data, function (row) { row.label = row[Object.keys(data[0])[0]]; return row; });
                            }

                            colModel.editor.data = data;
                        }

                        if(colModel.editor.hasOwnProperty('settings'))
                            for (var setting in colModel.editor.settings)
                                if (colModel.editor.settings.hasOwnProperty(setting))
                                    jqxComboBoxSettings[setting]= colModel.editor.settings[setting];

                        jqxComboBoxSettings['source'] = new $.jqx.dataAdapter({
                            localdata: colModel.editor.hasOwnProperty('data') ? colModel.editor.data : [],
                            datatype: "array"
                        });

                        //.jqxComboBox({ selectedIndex: 0, source: dataAdapter, displayMember: "ContactName", valueMember: "CompanyName", width: 200, height: 30,});
                        if(colModel.editor.hasOwnProperty('readOnly')&& (colModel.editor.readOnly == true))
                            jqxComboBoxSettings['disabled'] = colModel.editor.readOnly;

                        gridVariable['editorConfigs']['jqxComboBox'][jqxComboBoxConfigIndex] = jqxComboBoxSettings;

                        col['editoptions'] = {
                            custom_element: function(value, options) {
                                return $("<div></div>");
                            },
                            custom_value: function (elem, operation, value) {
                                if(operation === 'get') {
                                    return $(elem).val();
                                } else if(operation === 'set') {
                                    $(elem).val(value);
                                }
                            },
                            dataInit: new Function('element,options','$(element).jqxComboBox($.var.utilJqGrid[\''+gridId+'\'][\'editorConfigs\'][\'jqxComboBox\']['+jqxComboBoxConfigIndex+']); '+cmpId_initScript)
                        };
                        col['edittype'] = "custom";

                        exclude.concat([ 'edittype', 'editoptions' ]);
                        break;
                }

                if(colName) {
                    if(!col.hasOwnProperty('editoptions')) col['editoptions'] = {};
                    if(!col.editoptions.hasOwnProperty('dataInit'))
                        col['editoptions']['dataInit'] = new Function('element,options', colName_initScript);
                }

                if(colModel.editor.hasOwnProperty('onchange') && typeof colModel.editor.onchange === "function")
                    col['editoptions']['dataEvents'] = [{"type":"change","fn": colModel.editor.onchange}];

                if(colModel.editor.hasOwnProperty('events')){
                    let events = colModel.editor.events;
                    Object.keys(events).forEach(function(key,index) {
                        if(typeof Object.values(events)[index] === "function") {
                            if (col['editoptions']['dataEvents'] !== undefined) {
                                col['editoptions']['dataEvents'].push({"type": key, "fn": Object.values(events)[index]});
                            } else {
                                col['editoptions']['dataEvents'] = [{"type": key, "fn": Object.values(events)[index]}];
                            }
                        }
                    });
                }
            }

            if(col === {}) continue;

            exclude.concat(['editor', 'type']);
            colFunctions.mergeConfig(col, colModel, exclude);
            cols.push(col);
        }
        return cols;
    }

    function loadGrid(url, params) {
        if(url === undefined) url = $.var.utilJqGrid[gridId].url;
        if(params === undefined) params = $.var.utilJqGrid[gridId].extraParams;

        if($.var.utilJqGrid[gridId].ajaxSpinner)
            ajaxStart();
        window.rowId = -1;
        $.ajax({
            url: url,
            method:"POST",
            data: params,
            success: function(response, textStatus, jqXHR) {
                grid.jqGrid('clearGridData')
                    .jqGrid('setGridParam', { datatype: 'local', data: response.rows })
                    .trigger('reloadGrid')
                    .jqGrid('setGridParam', { datatype: 'json' });

                var gridVar = $.var.utilJqGrid[gridId];

                if(gridVar.listeners.afterLoad !== undefined && typeof gridVar.listeners.afterLoad === "function")
                    gridVar.listeners.afterLoad(grid, response);

                gridVar['url'] = url;
                gridVar['rows'] = response.rows;
                $('#norecs').remove();
                if($.var.utilJqGrid[gridId].ajaxSpinner)
                    ajaxStop();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if($.var.utilJqGrid[gridId].ajaxSpinner)
                    ajaxStop();
                // console.log(jqXHR);
                // console.log(textStatus);
                // console.log(errorThrown);
                if($.var.utilJqGrid[gridId].showNotification)
                    bootbox.confirm({
                        title: textStatus,
                        message: "Proses muat ulang data tidak berhasil.<br/>Coba muat ulang data lagi?",
                        buttons: {
                            cancel: {
                                label: 'Tidak',
                                className: 'btn-default'
                            },
                            confirm: {
                                label: 'Coba lagi',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if(result) loadGrid();
                        }
                    });
            }
        });
    }

    var rowAction = {
        toggleActionButton: function(rowId, editMode) {
            let id = gridId + '_' + rowId;
            let btnEdit = $('[id="jEditButton_' + id + '"]');
            let btnDel = $('[id="jDeleteButton_' + id + '"]');
            let btnSave = $('[id="jSaveButton_' + id + '"]');
            let btnCancel = $('[id="jCancelButton_' + id + '"]');

            if(editMode === undefined)
                editMode = btnEdit.css('display') === 'none';

            btnEdit.css('display', editMode ? 'none' : '');
            btnDel.css('display', editMode ? 'none' : '');
            btnSave.css('display', editMode ? '' : 'none');
            btnCancel.css('display', editMode ? '' : 'none');
        },
        modal: {
            deleteRow: {
                confirm: function (rowId) {
                    bootbox.confirm({
                        title: 'Konfirmasi',
                        message: "Anda yakin ingin menghapus record ini ?",
                        size: "small",
                        buttons: {
                            confirm: {
                                label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                                className: 'btn btn-warning'
                            },
                            cancel: {
                                label: '<span class="fa fa-ban"></span> Cancel',
                                className: 'btn btn-default'
                            }
                        },
                        callback: function (result) {
                            if(result) rowAction.deleteRow(rowId);
                        }
                    });
                },
                failed: function (rowId,message) {
                    $.notify({message: message},{type: 'warning'});
                    // bootbox.confirm({
                    //     message: "Proses hapus data terpilih tidak berhasil.",
                    //     buttons: {
                    //         confirm: {
                    //             label: 'Coba lagi',
                    //             className: 'btn-danger'
                    //         },
                    //         cancel: {
                    //             label: 'Batal',
                    //             className: 'btn-default'
                    //         }
                    //     },
                    //     callback: function (result) {
                    //         if(result) rowAction.deleteRow(rowId);
                    //     }
                    // });
                }
            }
        },
        addRow: function(initdata) {
            // console.log(initdata);
            if (window.rowId !== -1) {
                bootbox.alert("Selesaikan baris dalam mode edit terlebih dahulu!");
                return false;
            }
            initdata = initdata || {};
            let rowId = 'new_row'+(++gridVariable.newRowCounter);
            window.rowId = rowId;
            grid.jqGrid('addRow', {
                rowID : rowId,
                initdata : initdata,
                position :"first",
                useDefValues : false,
                useFormatter : false,
                addRowParams : {extraparam:{}}
            });

            if(!gridVariable.hasOwnProperty('newRowCounter')) gridVariable.newRowCounter = 0;
            rowAction.editRow(rowId, { oneditfunc: null });
            // rowAction.toggleActionButton(rowId, true);
            // $('[id="jEditButton_'+gridId+'_'+rowId+'"]').click();
        },
        editRow: function(rowId, params) {
            if (window.rowId !== -1 && !window.rowId.includes("new_row")) {
                bootbox.alert("Selesaikan baris dalam mode edit terlebih dahulu!");
                return false;
            }
            var defaultParams = {
                keys : false,
                oneditfunc: function(rowId) { // console.log('oneditfunc');
                    if(gridVariable.hasOwnProperty('onEditActions') && Array.isArray(gridVariable.onEditActions))
                        for(var i=0; i < gridVariable.onEditActions.length; i++)
                            gridVariable.onEditActions[i](grid, rowId);
                },
                successfunc : null,
                url : null,
                extraparam : {},
                aftersavefunc : null,
                errorfunc: null,
                afterrestorefunc : null,
                restoreAfterError : true,
                mtype : "POST"
            };

            if(params)
                for(param in params)
                    if(params.hasOwnProperty(param))
                        defaultParams[param] = params.param;

            grid.jqGrid('editRow', rowId, defaultParams);

            rowAction.toggleActionButton(rowId, true);
        },
        saveRow: function(rowId, params) {
            var extraParams = {};
            if (gridVariable.hasOwnProperty('onSaveExtraParams')){
                if (gridVariable.onSaveExtraParams === "function" ){
                    extraParams = gridVariable.onSaveExtraParams();
                }else{
                    extraParams = gridVariable.onSaveExtraParams;
                }
            }
            var defaultParams = {
                successfunc : null,
                url : null,
                extraparam : extraParams,
                aftersavefunc: function (rowid, response, options) {
                    // console.log(response);
                    $.notify({message: response.responseJSON.message},{type: response.responseJSON.success ? 'success' : 'warning'});
                },
                errorfunc: null,
                afterrestorefunc : null,
                restoreAfterError : true,
                mtype : "POST"
            };

            if (gridVariable.hasOwnProperty('saveRowParams')){
                defaultParams = {
                    ...defaultParams,
                    ...gridVariable.saveRowParams
                }
            }

            if(params)
                for(param in params)
                    if(params.hasOwnProperty(param))
                        defaultParams[param] = params.param;

            grid.jqGrid('saveRow', rowId, defaultParams);

            // rowAction.toggleActionButton(rowId, false);
        },
        deleteRow: function(rowId) {
            if (grid.getGridParam('datatype') === 'local') {
                grid.delRowData(rowId);
            } else {
                /* delete row request */
                $.ajax({
                    url: gridVariable.url,
                    method: "POST",
                    data: {oper: 'del', id: rowId},
                    success: function (response) {
                        // console.log(response);
                        if (!response.success) {
                            rowAction.modal.deleteRow.failed(rowId,response.message);
                        }else{

                            $.notify({message: response.message},{type: 'success'});
                            /* remove row from grid */
                            loadGrid();
                        }
                    },
                    error: function (response) {
                        // console.log(response);
                        $.notify({message: response.message},{type: 'warning'});
                        // bootbox.alert({
                        //     message: response.responseJSON.message,
                        //     size: 'small'
                        // });
                    }
                });
            }
            if(gridVariable.listeners.afterDeleteRow !== undefined && typeof gridVariable.listeners.afterDeleteRow === "function")
                gridVariable.listeners.afterDeleteRow(grid, rowId);
        }
    };

    function resizeGrid() {
        grid.setGridWidth($.var.utilJqGrid[gridId].fitToElement.width());
    }

    var gridAction = {
        getSelectedRowId: function () {
            return grid.getGridParam('selrow');
        },
        getSelectedArrayRowId: function () {
            return grid.getGridParam("selarrrow");
        },
        formatData: function (rowId, data) {
            return { id: rowId, data: data };
        },
        getRowData: function(rowId) {
            for (var i = 0; i < gridVariable.rows.length; i++) {
                if (gridVariable.rows[i].hasOwnProperty('id') && gridVariable.rows[i].id === rowId)
                    return gridVariable.rows[i];
            }
            return false;
        },
        getData: function (rowId) {
            var data;
            if(Array.isArray(rowId)) {
                var result = [];
                for (var i = 0; i < rowId.length; i++) {
                    data = gridAction.getRowData(rowId[i]);
                    result.push(gridAction.formatData(rowId, data? data :grid.getRowData(rowId[i])));
                }
                return result;
            } else {
                data = gridAction.getRowData(rowId);
                return gridAction.formatData(rowId, data? data :grid.getRowData(rowId));
            }
        },
        customNavButtonAction: function(customAction) {
            customAction(grid);
        },
        addNavButton: function(buttonConfig) {
            var defaultConfig = {
                caption: 'new Button',
                position:'last'
            };
            for(var config in buttonConfig )
                if(buttonConfig.hasOwnProperty(config))
                    switch (config) {
                        case 'onClickButton':
                            if(typeof buttonConfig[config] === "function") {
                                if(!gridVariable.navBtnlisteners)
                                    gridVariable['navBtnlisteners'] = {};

                                var index = Object.keys(gridVariable.navBtnlisteners).length;
                                gridVariable.navBtnlisteners[index] = buttonConfig[config];

                                defaultConfig[config] = new Function("$('#"+gridId+"').utilJqGrid().callCustomNavButtonAction("+index+");");
                            }
                            break;
                        case 'caption':
                        default:
                            defaultConfig[config] = buttonConfig[config];
                    }

            grid.navButtonAdd('#'+defaultPagerId, defaultConfig);
        }
    };

    return {
        callCustomNavButtonAction: function(index) { gridVariable.navBtnlisteners[index](grid, rowAction); },
        init: function () {
            window.rowId = -1;
            defaultPagerId = gridId +'-pager';
            pager = $('<div id="'+defaultPagerId+'"></div>');
            defaultConfigs = {
                /* UI */
                styleUI : 'Bootstrap',
                responsive: false,
                width: 780,
                height: 250,
                /* data */
                datatype: "local",
                mtype: "POST",
                jsonReader: {
                    root: 'rows',
                    records: 'rowsCount',
                    id: 'rowId',
                    repeatitems: false,
                    page:  function(obj) { return 1; },
                    total: function(obj) { return 1; }
                },
                rowNum: 100,
                /* columns */
                //colModel: [],
                shrinkToFit : false,
                autowidth: true,
                /* pager */
                pager: "#"+defaultPagerId,
                pgbuttons: false,
                pgtext: null,
                viewrecords: false,
                navButtonTambah: true,
                /* others */
                ajaxSpinner: true,
                showNotification: true
            };
            ajaxRowOpt = {
                beforeSend : function(jqXHR, ajaxOptions) {
                    var data = ajaxOptions.data.split('&').map(function (v) { return window.util.url.decode(v); });
                    // console.log(data);
                    var dataId = /id=([^&]+)/.exec(ajaxOptions.data),
                        rowId = dataId ? dataId[1] : '';

                    /* cek kelengkapan data yang disubmit */
                    loop1:
                        for(var iii=0; iii<gridVariable.colName.length; iii++) {
                            var colName = gridVariable.colName[iii];

                            for(var ii=0; ii<data.length; ii++) {
                                let itemArr = /([^=]+)=(.*)/.exec(data[ii]);
                                if(itemArr && itemArr[1] !== undefined && itemArr[1] === colName)
                                    continue loop1;
                            }
                            if(gridVariable.colEditor.hasOwnProperty(rowId+'_'+colName))
                                data.push(colName+'='+window.util.url.encode(gridVariable.colEditor[rowId+'_'+colName].val()));
                        }

                    //console.log(data);

                    if(gridVariable.europeanNumberFields !== undefined && gridVariable.europeanNumberFields.length > 0) { // console.log(ajaxOptions.data);
                        // let arrData = ajaxOptions.data.split('&');
                        for(i=0; i<gridVariable.europeanNumberFields.length; i++) {
                            for(ii=0; ii<data.length; ii++) {
                                let itemArr = /([^=]+)=(.*)/.exec(data[ii]); // console.log(itemArr);
                                if(itemArr[1] === gridVariable.europeanNumberFields[i]) { // console.log(itemArr[1]);
                                    data[ii] = itemArr[1]+'='+window.util.url.encode(window.util.number.getValueOfEuropeanNumber(window.util.url.decode(itemArr[2])));
                                }
                            }
                        }
                        // console.log(data);
                        ajaxOptions.data = data.join('&');
                    }

                    if(gridVariable.listeners.rowAjaxBeforeSend !== undefined && typeof gridVariable.listeners.rowAjaxBeforeSend === "function") {
                        // var data = window.util.url.decode(ajaxOptions.data);
                        // data = data.split('&');
                        dataJson = {};
                        for (var i = 0; i < data.length; i++) {
                            let matches = /([^=]+)=(.+)/.exec(data[i]);
                            if(matches) dataJson[matches[1]] = matches[2];
                        }

                        if(gridVariable.listeners.rowAjaxBeforeSend(dataJson) === false) {
                            $('#load_'+gridId).hide();
                            return false;
                        }
                    }

                    ajaxOptions.data += gridVariable.extraParams;
                },
                success : function(response, status, error) { // debugger;

                    if(!response.success)
                        $.notify({message: response.message},{type: 'warning'});

                    loadGrid();
                },
                error : function(response, status, error) { // debugger;
                    if(response.hasOwnProperty('responseJSON') && response.responseJSON.hasOwnProperty('message'))
                        $.notify({message: response.responseJSON.message},{type: 'warning'});

                }
            };
            gridVariable = $.var.utilJqGrid[gridId] === undefined ? {} : $.var.utilJqGrid[gridId];
            readOnly = false;

            mergeConfig();
            applyCustomConfig();

            if (configs.hasOwnProperty('colModel'))
                configs['colModel'] = parseColModel(configs['colModel']);

            if (!configs.hasOwnProperty('url') && configs.hasOwnProperty('editurl'))
                configs['url'] = configs.editurl;

            //configs['datatype'] = /* configs.hasOwnProperty('url') && */ configs.hasOwnProperty('editurl') ? 'json' : 'local';

            if (!readOnly && !configs.hasOwnProperty('ajaxRowOptions'))
                configs['ajaxRowOptions'] = ajaxRowOpt;

            grid.jqGrid(configs)
                .jqGrid("setLabel", "rn", "No", {'text-align':'center'});
            if(!readOnly) {
                grid.navGrid('#' + defaultPagerId, {edit: false, add: false, del: false, search: false, refresh: false});

                if(navButtonTambah)
                    grid.navButtonAdd('#' + defaultPagerId, {
                        caption: ' Tambah',
                        buttonicon: 'glyphicon glyphicon-plus',
                        onClickButton: rowAction.addRow,
                        position:'last'
                    });

            }

            $('#norecs').remove();
            grid.jqGrid('setGridParam', { loadComplete: function(data){ window.rowId = -1;} } );
            grid.bind("jqGridInlineAfterRestoreRow", function (a,b) {window.rowId = -1;});
            grid.bind("jqGridInlineEditRow", function (e, rowId, orgClickEvent) {window.rowId = rowId;});
            return this;
        },
        addNavButton: function(buttonConfig) {
            if(Array.isArray(buttonConfig))
                for(var i = 0; i < buttonConfig.length; i++)
                    gridAction.addNavButton(buttonConfig[i]);
            else
                gridAction.addNavButton(buttonConfig);

            return this;
        },
        fit: function(element) {
            $.var.utilJqGrid[gridId].fitToElement = element;
            $.var.utilJqGrid[gridId].fitToElement.resize(resizeGrid);
            resizeGrid();
            return this;
        },
        load: function (config) {
            var url = config === undefined || config.url === undefined ? $.var.utilJqGrid[gridId].url : config.url,
                param = config === undefined || config.param === undefined ? $.var.utilJqGrid[gridId].extraParams : config.param;
            loadGrid(url, param);
            return this;
        },
        loadData: function(json, append) {
            if(append === false)
                grid.jqGrid('clearGridData');

            grid.jqGrid('setGridParam', { datatype: 'local', data: response.rows }).trigger('reloadGrid');
        },
        setHeight: function (height) {
            grid.setGridHeight(height);
            return this;
        },
        editRow: function(rowid) {
            rowAction.editRow(rowid);
        },
        saveRow: function (t,rowid) {
            if (grid.getGridParam('datatype') === 'local') {
                grid.saveRow(rowid, false, 'clientArray');
                $.fn.fmatter.rowactions.call(t,'cancel');

                if(gridVariable.europeanNumberFields.length) {
                    var arrayData = grid.getGridParam ('data');

                    for (var a=0; a < arrayData.length; a++) {
                        var data = arrayData[a];
                        if(data.hasOwnProperty('id') && data.id === rowid) {
                            for(var i=0; i<gridVariable.europeanNumberFields.length; i++) {
                                for(var field in data) {
                                    if(data.hasOwnProperty(field) && gridVariable.europeanNumberFields[i] === field) {
                                        data[field] = window.util.number.getValueOfEuropeanNumber(data[field]);
                                    }
                                }

                                grid.jqGrid('setRowData', rowid, data);
                            }
                        }
                    }
                }

            }else{
                // $.fn.fmatter.rowactions.call(t,'save');
                rowAction.saveRow(rowid);
            }
            window.rowId = -1;
        },
        deleteRow: function (rowId, silent) {
            if(silent === true)
                rowAction.deleteRow(rowId);
            else
                rowAction.modal.deleteRow.confirm(rowId);
        },
        selected: {
            getId: function () {
                return gridAction.getSelectedRowId();
            },
            getArrayId: function () {
                return gridAction.getSelectedArrayRowId();
            },
            getData: function () {
                let rowId = gridVariable.multiselect ? gridAction.getSelectedArrayRowId() : gridAction.getSelectedRowId();
                return gridAction.getData(rowId);
            }
        },
        getData: function (rowId) {
            return gridAction.getData(rowId);
        }
    };
};


$.fn.utilFormRemoteValidation = function (param) {
    var loadingBox = null;
    function showLoadingBox(message) {
        loadingBox = bootbox.dialog({
            message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> '+message+'</div>',
            closeButton: false,
            size: 'sm'
        });
    }
    function hideLoadingBox() { loadingBox.modal('hide'); }

    if (param.hasOwnProperty('url') && param.url)
        this.attr('action', param.url);

    if (param.hasOwnProperty('validationUrl') && param.validationUrl)
        this.attr('validationUrl', param.validationUrl);

    if (param.hasOwnProperty('submitButton') && param.submitButton) {
        var formId = this.attr('id');

        if(!formId) {
            formId = Math.random().toString(36).replace('0.','');
            this.attr('id', formId)
        }
        param.submitButton.attr('form-id', formId);

        param.submitButton.click(function () {
            let formId = $(this).attr('form-id'),
                form = $('#'+formId),
                validationUrl = form.attr('validationUrl');

            if(validationUrl) {
                showLoadingBox('Proses validasi ...');
                $.ajax({
                    url: validationUrl,
                    type: "POST",
                    data: form.serialize(),
                    success: function (data) {
                        hideLoadingBox();
                        if (data.success) {
                            showLoadingBox('Menyimpan ...');
                            form.submit();
                        } else
                            bootbox.alert({
                                message: data.message,
                                size: 'small'
                            });
                    },
                    error: function (jXHR, textStatus, errorThrown) {
                        hideLoadingBox();
                        // console.log(errorThrown);
                        bootbox.alert({
                            message: '<b>Proses validasi gagal.</b><br/>'+errorThrown,
                            size: 'small'
                        });
                    },
                    async: false
                });
            } else {
                showLoadingBox('Menyimpan ...');
                form.submit();
            }
        });
    }
};