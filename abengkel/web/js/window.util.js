window.util = {
    copyToClp: function (txt) {
        txt = document.createTextNode(txt);
        var m = document;
        var w = window;
        var b = m.body;
        b.appendChild(txt);
        if (b.createTextRange) {
            var d = b.createTextRange();
            d.moveToElementText(txt);
            d.select();
            m.execCommand('copy');
        } else {
            var d = m.createRange();
            var g = w.getSelection;
            d.selectNodeContents(txt);
            g().removeAllRanges();
            g().addRange(d);
            m.execCommand('copy');
            g().removeAllRanges();
        }
        txt.remove();
    },
    date: {
        dateTimeParser: /(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2}):(\d{2})/,
        dateParser: /(\d{2})\/(\d{2})\/(\d{4})/,
        fromStringTime: function (stringTime) {
            var match = stringTime.match(this.dateTimeParser);
            return new Date(
                match[3],  // year
                match[2] - 1,  // monthIndex
                match[1],  // day
                match[4],  // hours
                match[5],  // minutes
                match[6]  //seconds
            );
        },
        fromStringDate: function (stringDate) {
            var match = stringDate.match(this.dateParser);
            return new Date(
                match[3],  // year
                match[2] - 1,  // monthIndex
                match[1],  // day
            );
        },
        sDate2sql: function (stringDate) {
            var match = stringDate.match(this.dateParser);
            let d = new Date(
                match[3],  // year
                match[2] - 1,  // monthIndex
                match[1],  // day
            );
            return d.toISOString().slice(0, 10);
        },
        sql2sDate: function (sqlDate) {
            let d = new Date(sqlDate);
            let options = {year: 'numeric', month: '2-digit', day: '2-digit'};
            return d.toLocaleDateString('id-ID', options);
        },
        sql2sTime: function (sqlDate) {
            return sqlDate.slice(-8);
        },
        sql2sDatetime: function (sqlDate) {
            let d = new Date(sqlDate);
            let options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute: '2-digit', second: '2-digit'};
            return d.toLocaleTimeString('id-ID', options);
        },
    },
    number: {
        getValueOfEuropeanNumber: function (number) {
            if (typeof number === "number") return number;
            if (typeof number !== "string") return number;
            return parseFloat(window.util.number.parseFloatFromString(number, true));
        },
        parseFloatFromString: function (value, coerce) {
            value = String(value).trim();
            if ('' === value) {
                return value;
            }
            // check if the string can be converted to float as-is
            // var parsed = parseFloat(value);
            // if (String(parsed) === value) {
            //     return window.util.number.fixDecimals(parsed, 2);
            // }
            // replace arabic numbers by latin
            value = value
                // arabic
                .replace(/[\u0660-\u0669]/g, function (d) {
                    return d.charCodeAt(0) - 1632;
                })

                // persian
                .replace(/[\u06F0-\u06F9]/g, function (d) {
                    return d.charCodeAt(0) - 1776;
                });
            // remove all non-digit characters
            var split = value.split(/[^\dE-]+/);
            if (1 === split.length) {
                // there's no decimal part
                return window.util.number.fixDecimals(parseFloat(value), 2);
            }
            for (var i = 0; i < split.length; i++) {
                if ('' === split[i]) {
                    return coerce ? window.util.number.fixDecimals(parseFloat(0), 2) : NaN;
                }
            }
            // use the last part as decimal
            var decimal = '00';
            if (split[split.length - 1].length < 3) {
                decimal = split.pop();
            }
            // reconstruct the number using dot as decimal separator
            return window.util.number.fixDecimals(parseFloat(split.join('') + '.' + decimal), 2);
        },
        fixDecimals: function (num, precision) {
            return (Math.floor(num * 1000) / 1000).toFixed(precision);
        },
        toFixed: function (number, decimalPlaces) {
            return parseFloat(parseFloat(number).toFixed(decimalPlaces ? decimalPlaces : 2));
        },
        format: function (number) {
            return Intl.NumberFormat('id-ID').format(number);
        }
    },
    url: {
        replaceQueryParam: function (param, newval, search) {
            var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
            var query = search.replace(regex, "$1").replace(/&$/, '');
            return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
        },
        encode: function (url) {
            return encodeURIComponent(url).replace(/'/g, "%27").replace(/"/g, "%22");
        },
        decode: function (url) {
            return decodeURIComponent(url.replace(/\+/g, " "));
        },
        getParams: function (url) {
            var params = {};
            var parser = document.createElement('a');
            parser.href = url;
            var query = parser.search.substring(1);
            var vars = query.split('&');
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split('=');
                params[pair[0]] = decodeURIComponent(pair[1]);
            }
            return params;
        },
        addParam: function (url, key, value) {
            key = encodeURI(key);
            value = encodeURI(value);
            var match = /^(.+)\?(.*)$/.exec(url), params = [];
            if (match && match[2] !== undefined) {
                var oldParams = match[2].split('&');
                var m = 0, p;
                for (var i = 0; i < oldParams.length; i++) {
                    if (!oldParams[i]) continue;
                    p = oldParams[i].split('=');
                    if (p[0] === key) {
                        m = 1;
                        p[1] = value
                    }
                    ;
                    params.push(p.join('='));
                }
                if (!m) params.push([key, value].join('='));
                url = match[1];
            } else {
                params.push([key, value].join('='));
            }
            return url + '?' + params.join('&');
        }
    },
    app: {
        dialogListData: {
            iframe: function () {
                return document.getElementById('iframe-dialogListData');
            },
            dialog: null,
            callback: null,
            show: function (options, callback) {
                if (typeof callback === 'function') {
                    this.callback = callback;
                } else {
                    return true;
                }
                var button = {
                    ok: {
                        label: '<i class="glyphicon glyphicon-ok"></i>&nbsp&nbsp Simpan &nbsp&nbsp',
                        className: 'btn-success',
                        callback: this.closeTriggerFromIframe
                    },
                    cancel: {
                        label: '<i class="fa fa-ban"></i>&nbsp&nbsp Keluar &nbsp',
                        className: 'btn-danger',
                        callback: this.callback
                    }
                };
                if (options.button !== undefined) {
                    button = options.button;
                }
                this.dialog = bootbox.dialog({
                    title: options.title,
                    message: '<iframe id="iframe-dialogListData" src="' + options.url + '" style="width: 100%; min-height: 500px; border: none; margin: 0; padding: 0; display: block;"></iframe>',
                    size: 'large',
                    onEscape: true,
                    backdrop: true,
                    buttons: button
                });
                this.dialog.find('.modal-dialog').width(1000);
            },
            closeTriggerFromIframe: function () {
                let iframe = document.getElementById('iframe-dialogListData');
                iframe.contentWindow.util.app.dialogListData.closeTriggerFromIframe();
                return true;
            },
            close: function (data) {
                this.dialog.modal('hide');
                window.util.app.dialogListData.callback(data);
            },
            gridFn: {
                var: {
                    mode: null,
                    multiSelect: 0,
                    showDetail: 0
                },
                mainGrid: null,
                selectedItem: {},
                selectedHeaderRowId: null,
                selectedHeaderData: null,
                getIdAndDataByRowId: function (rowId, grid) {
                    var gridFn = window.util.app.dialogListData.gridFn;
                    grid = grid || gridFn.mainGrid;
                    var data = {id: rowId, data: grid.getRowData(rowId)};
                    if (gridFn.var.mode === 'select-detail' && grid === gridFn.detail.element){
                        if(gridFn.selectedHeaderData == null){
                            rowIdHeader = gridFn.header.element.getGridParam("selrow");
                            gridFn.selectedHeaderData = gridFn.getIdAndDataByRowId(rowIdHeader, gridFn.header.element);
                        }
                        data['header'] = gridFn.selectedHeaderData;
                    }
                    return data;
                },
                getSelectedRow: function (clear= false) {
                    var gridFn = window.util.app.dialogListData.gridFn;
                    // console.log(gridFn.mainGrid.getGridParam().id);
                    if (gridFn.var.multiSelect) {
                        var result = [];
                        if (window.multiSelectID !== undefined && window.multiSelectID[gridFn.mainGrid.getGridParam().id] !== undefined) {
                            result = window.multiSelectID[gridFn.mainGrid.getGridParam().id].slice();
                            if (clear) window.multiSelectID[gridFn.mainGrid.getGridParam().id] = [];
                        } else {
                            var rowIds = gridFn.mainGrid.getGridParam("selarrrow");
                            for (var i = 0; i < rowIds.length; i++)
                                result.push(gridFn.getIdAndDataByRowId(rowIds[i]));
                        }
                        // console.log(rowIdsSell);
                        // console.log(result);
                        resultData = result.length > 0 ? result : false;
                    } else {
                        var rowId = gridFn.mainGrid.jqGrid('getGridParam', 'selrow');
                        resultData = rowId !== null ? gridFn.getIdAndDataByRowId(rowId) : false;
                    }
                    return resultData;
                },
                getArraySelectedItem: function () {
                    var result = [],
                        gridFn = window.util.app.dialogListData.gridFn;
                    for (headerId in gridFn.selectedItem) {
                        if (gridFn.selectedItem.hasOwnProperty(headerId))
                            result = result.concat(gridFn.selectedItem[headerId]);
                    }
                    return result;
                },
                header: {
                    element: null,
                    multiSelect: function () {
                        var gridFn = window.util.app.dialogListData.gridFn;
                        return gridFn.var.mode === 'select' && gridFn.var.multiSelect ? 1 : 0;
                    },
                    onSelectRow: function (rowId, status) {
                        var gridFn = window.util.app.dialogListData.gridFn;
                        if (gridFn.var.showDetail && gridFn.selectedHeaderRowId !== rowId) {
                            gridFn.selectedHeaderRowId = rowId;
                            gridFn.selectedHeaderData = gridFn.getIdAndDataByRowId(rowId, gridFn.header.element);
                            gridFn.detail.element.utilJqGrid().load({param: gridFn.selectedHeaderData.data});
                        }
                        if (gridFn.var.multiSelect) {
                            if (window.multiSelectID === undefined || gridFn.header.multiSelect() == 0) {
                                window.multiSelectID = [];
                                window.multiSelectID[gridFn.header.element.getGridParam().id] = [];
                            }
                            window.multiSelectID[gridFn.header.element.getGridParam().id] = _.remove(window.multiSelectID[gridFn.header.element.getGridParam().id], function (n) {
                                return n.id != rowId;
                            });
                            if (status) {
                                window.multiSelectID[gridFn.header.element.getGridParam().id].push(gridFn.getIdAndDataByRowId(rowId));
                            }
                        }
                        // console.log(window.multiSelectID[gridFn.header.element.getGridParam().id]);
                    },
                    onSelectAll: function (rowId, status) {
                        var gridFn = window.util.app.dialogListData.gridFn;
                        if (gridFn.var.showDetail && gridFn.selectedHeaderRowId !== rowId) {
                            gridFn.selectedHeaderRowId = rowId;
                            gridFn.selectedHeaderData = gridFn.getIdAndDataByRowId(rowId, gridFn.header.element);
                            gridFn.detail.element.utilJqGrid().load({param: gridFn.selectedHeaderData.data});
                        }
                        if (gridFn.var.multiSelect) {
                            if (window.multiSelectID === undefined) {
                                window.multiSelectID = [];
                                window.multiSelectID[gridFn.header.element.getGridParam().id] = [];
                            }
                            window.multiSelectID[gridFn.header.element.getGridParam().id] = _.remove(window.multiSelectID[gridFn.header.element.getGridParam().id], function (n) {
                                return n.id != rowId;
                            });
                            if (status) {
                                rowId.forEach(function (item, index) {
                                    window.multiSelectID[gridFn.header.element.getGridParam().id].push(gridFn.getIdAndDataByRowId(item));
                                });
                            }else{
                                window.util.app.dialogListData.gridFn.selectedHeaderData = null;
                                window.util.app.dialogListData.gridFn.selectedHeaderRowId = null;
                                window.util.app.dialogListData.gridFn.selectedItem = {};
                                window.multiSelectID = [];
                            }
                        }
                        console.log(window.multiSelectID[gridFn.header.element.getGridParam().id]);
                    },
                    ondblClickRow: function (rowId) {
                        var gridFn = window.util.app.dialogListData.gridFn;
                        if (gridFn.var.mode === 'select' && !gridFn.var.multiSelect) {
                            parent.window.util.app.dialogListData.close(gridFn.getIdAndDataByRowId(rowId));
                        }
                    }
                },
                detail: {
                    height: 190,
                    element: null,
                    multiSelectID: [],
                    multiSelect: function () {
                        var gridFn = window.util.app.dialogListData.gridFn;
                        return gridFn.var.mode === 'select-detail' && gridFn.var.multiSelect ? 1 : 0;
                    },
                    onSelectRow: function (rowId, status) {
                        var gridFn = window.util.app.dialogListData.gridFn;
                        gridFn.selectedItem = [];
                        if (gridFn.var.showDetail && gridFn.var.mode === 'select-detail' && gridFn.var.multiSelect) {
                            if (window.multiSelectID === undefined) {
                                window.multiSelectID = [];
                                window.multiSelectID[gridFn.detail.element.getGridParam().id] = [];
                            }
                            window.multiSelectID[gridFn.detail.element.getGridParam().id] = _.remove(window.multiSelectID[gridFn.detail.element.getGridParam().id], function (n) {
                                return n.id != rowId;
                            });
                            if (status) {
                                window.multiSelectID[gridFn.detail.element.getGridParam().id].push(gridFn.getIdAndDataByRowId(rowId));
                            }else{
                                window.util.app.dialogListData.gridFn.selectedHeaderData = null;
                                window.util.app.dialogListData.gridFn.selectedHeaderRowId = null;
                                window.util.app.dialogListData.gridFn.selectedItem = {};
                                window.multiSelectID = [];
                            }
                            // var rowIdsSell = gridFn.mainGrid.getGridParam("selarrrow");
                            // window.multiSelectID[gridFn.detail.element.getGridParam().id] = gridFn.getIdAndDataByRowId(rowId);
                            
                            gridFn.selectedItem[gridFn.selectedHeaderRowId] = gridFn.getSelectedRow();
                            // console.log(window.multiSelectID[gridFn.detail.element.getGridParam().id]);
                        }
                    },
                    onSelectAll: function (rowIds, status) {
                        var gridFn = window.util.app.dialogListData.gridFn;
                        gridFn.selectedItem = [];
                        if (gridFn.var.showDetail && gridFn.var.mode === 'select-detail' && gridFn.var.multiSelect) {
                            if (window.multiSelectID === undefined) {
                                window.multiSelectID = [];
                                window.multiSelectID[gridFn.detail.element.getGridParam().id] = [];
                            }
                            window.multiSelectID[gridFn.detail.element.getGridParam().id] = _.remove(window.multiSelectID[gridFn.detail.element.getGridParam().id], function (n) {
                                return n.id != rowId;
                            });
                            if (status) {
                                rowIds.forEach(function (item, index) {
                                    window.multiSelectID[gridFn.detail.element.getGridParam().id].push(gridFn.getIdAndDataByRowId(item));
                                });
                            }else{
                                window.util.app.dialogListData.gridFn.selectedItem = {};
                                window.multiSelectID = [];
                            }
                            gridFn.selectedItem[gridFn.selectedHeaderRowId] = gridFn.getSelectedRow();
                            console.log(window.multiSelectID[gridFn.detail.element.getGridParam().id]);
                        }
                    },
                    ondblClickRow: function (rowId) {
                        var gridFn = window.util.app.dialogListData.gridFn;
                        if (gridFn.var.mode === 'select-detail' && !gridFn.var.multiSelect) {
                            var data = gridFn.getIdAndDataByRowId(rowId);
                            parent.window.util.app.dialogListData.close(data);
                        }
                    },
                    loadComplete: function (data) {
                        var gridFn = window.util.app.dialogListData.gridFn;
                        if (gridFn.var.mode === 'select-detail' && gridFn.var.multiSelect && gridFn.selectedItem.hasOwnProperty(gridFn.selectedHeaderRowId)) {
                            var rows = gridFn.selectedItem[gridFn.selectedHeaderRowId];
                            for (var i = 0; i < rows.length; i++) {
                                //$('#jqg_jqGridDetail_'+rows[i].id).prop('checked', true);
                                gridFn.detail.element.setSelection(rows[i].id);
                            }
                        }
                    }
                },
                set: function (grid) {
                    var gridFn = window.util.app.dialogListData.gridFn;
                    if (gridFn.var.showDetail)
                        $('.panel-body').append($('<table id="jqGridDetail"></table>'));
                        $('.panel-body').append($('<table id="scnjqGridDetail"></table>'));
                    gridFn.header.element = grid;
                    gridFn.detail.element = $('#jqGridDetail');
                    gridFn.mainGrid = gridFn.var.mode === 'select' ? grid :
                        (gridFn.var.mode === 'select-detail' ? gridFn.detail.element : null);
                    if (gridFn.var.mode) {
                        window.util.app.dialogListData.closeTriggerFromIframe = function () {
                            var gridFn = window.util.app.dialogListData.gridFn;
                            var data = (gridFn.var.mode === 'select-detail' && !gridFn.var.multiSelect) ?
                                gridFn.getArraySelectedItem() : gridFn.getSelectedRow();
                            // gridFn.getSelectedRow() : gridFn.getSelectedRow();
                            if (data !== false)
                                parent.window.util.app.dialogListData.close(data);
                        };
                    }
                }
            }
        },
        dialogPrint: {
            iframe: function () {
                return document.getElementById('iframe-dialogPrint');
            },
            dialog: null,
            callback: null,
            selectedItem: null,
            show: function (options, callback) {
                this.dialog = bootbox.dialog({
                    title: options.title,
                    message: '<iframe id="iframe-dialogPrint" src="' + options.url + '" style="width: 100%; min-height: 400px; border: none; margin: 0; padding: 0; display: block;"></iframe>',
                    size: 'large',
                    onEscape: true,
                    backdrop: true,
                    buttons: {
                        ok: {
                            label: '<i class="glyphicon glyphicon-print"></i>&nbsp&nbspPrint&nbsp&nbsp',
                            className: 'btn-warning',
                            callback: window.util.app.dialogPrint.print
                        },
                        cancel: {
                            label: '<i class="fa fa-ban"></i>&nbsp&nbsp Batal &nbsp',
                            className: 'btn-danger',
                            callback: function () {
                                return true;
                            }
                        }
                    }
                });
                this.dialog.find('.modal-dialog').width(1000);
                if (typeof callback === 'function') this.callback = callback;
            },
            print: function () {
                if (window.util.app.dialogPrint.selectedItem) {
                    window.util.app.dialogPrint.callback(window.util.app.dialogPrint.selectedItem);
                    window.util.app.dialogPrint.close();
                }
            },
            close: function () {
                this.dialog.modal('hide');
                this.selectedItem = null;
            }
        },
        popUpForm: {
            form: null,
            callback: null,
            param: {
                key: 'mode',
                value: 'popup-form'
            },
            show: function (options, callback) {
                var height = 0,
                    heightStyle = 'min-height: 430px;';
                if (options.height) {
                    height = options.height;
                    heightStyle = 'height: ' + height + 'px;';
                }
                var button = {
                    cancel: {
                        label: '<i class="fa fa-ban"></i>&nbsp&nbsp Keluar &nbsp',
                        className: 'btn-danger',
                        callback: this.callback
                    }
                };
                if (options.button !== undefined) {
                    button = options.button;
                }
                this.form = bootbox.dialog({
                    title: options.title,
                    message: '<iframe id="iframe-popUpForm" src="' + window.util.url.addParam(options.url, this.param.key, this.param.value) + '" style="width: 100%; ' + heightStyle + ' border: none; margin: 0; padding: 0; display: block;"></iframe>',
                    size: options.size ? options.size : 'large',
                    onEscape: true,
                    backdrop: true,
                    buttons: button
                });
                var style = "";
                if (options.width)
                    style += 'width: ' + options.width + 'px !important;';
                // this.form.find("div.modal-dialog").attr('style', 'width: '+options.width+'px !important;');
                if (height)
                    style += 'height: ' + height + 'px !important;';
                // this.form.find('div.modal-content').attr('style', 'height: '+height+'px !important;');
                if (style)
                    this.form.find('div.modal-dialog').attr('style', style);
                if (typeof callback === 'function') this.callback = callback;
                $('iframe#iframe-popUpForm').on('load', function () {
                    var iframe = $('iframe#iframe-popUpForm'),
                        form = iframe.contents().find('form');
                    var actionUrl = form.attr('action');
                    form.attr('action', window.util.url.addParam(actionUrl, window.util.app.popUpForm.param.key, window.util.app.popUpForm.param.value));
                });
            },
            close: function (data) {
                //this.form.modal('hide');
                this.form.find("button.bootbox-close-button.close").click();
                this.callback(data);
            }
        },
        submit(action, target, data = []) {
            var formNew = $(document.createElement('form'));
            $(formNew).attr("action", action);
            $(formNew).attr("method", "POST");
            $(formNew).attr("target", target);
            $(formNew).utilForm().addHidden(data);
            $(formNew).appendTo(document.body);
            $(formNew).submit();
            $(formNew).remove();
        },
    },
    calculatePrice: function (data) {
        if (typeof data == "undefined")
            return {qty: 0, price: 0, bruto: 0, disc: 0, discRp: 0, total: 0};
        var result = {
            qty: parseFloat(data.qty),
            price: parseFloat(data.price),
            disc: 0,
            discRp: 0
        };
        result.bruto = result.qty * result.price;
        if (data.hasOwnProperty('disc'))
            result.disc = parseFloat(data.disc);
            result.discRp = result.bruto * result.disc / 100;
        if (data.hasOwnProperty('discRp'))
            result.discRp = parseFloat(data.discRp);
        if (result.disc == 0 && result.discRp != 0) {
            result.disc = Math.round(result.discRp * 100 / result.bruto);
        }
        result.total = result.bruto - result.discRp;
        return result;
    },
    minuteToString: function (v) {
        v = parseInt(v);
        if (v == 0) return '0 MENIT';
        var jam = parseInt(v / 60);
        var menit = v % 60;
        return (jam > 0 ? jam + ' JAM ' : '') + (menit > 0 ? menit + ' MENIT ' : '');
    },
    sleep: function (milliseconds) {
        const date = Date.now();
        let currentDate = null;
        do {
            currentDate = Date.now();
        } while (currentDate - date < milliseconds);
    },
    randomInteger: function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },
    spellIndonesianNumber: function (number) {
        if (isNaN(number)) return '';
        var dasar = ['', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan'],
            angka = [1000000, 1000, 100, 10, 1],
            satuan = ['Juta', 'Ribu', 'Ratus', 'Puluh', ''],
            str = '',
            i = 0;
        while (number != 0) {
            var count = parseInt(number / angka[i]);
            if (count >= 10) str += window.util.spellIndonesianNumber(count) + " " + satuan[i] + " ";
            else if (count > 0 && count < 10)
                str += dasar[count] + " " + satuan[i] + " ";
            number -= angka[i] * count;
            i++;
        }
        str = str.trim().replace("  ", " ");
        dasar.forEach(function (v, i) {
            if (i) str = str.replace('Satu Puluh ' + v, v + ' Belas');
        });
        ['Ribu', 'Ratus', 'Puluh', 'Belas'].forEach(function (v, i) {
            if (i) str = str.replace('Satu ' + v, 'Se' + v.toLowerCase());
        });
        return str;
    },
    isNullOrWhitespace: function (input) {
        if (input == null) return true;
        return input.replace(/\s/gi, '').length < 1;
    }
};
document.addEventListener('keydown', function (event) {
    if (event.ctrlKey && event.key === 'x') {
        if (document.activeElement !== undefined) {
            switch (document.activeElement.nodeName) {
                case 'SELECT' :
                    window.util.copyToClp(document.activeElement.value);
                    break;
                case 'SPAN' :
                    if (document.activeElement.parentNode !== undefined && document.activeElement.parentNode.className === 'selection') {
                        var select = document.activeElement.parentNode.parentNode.parentNode.getElementsByTagName('select');
                        if (select.length > 0) {
                            let name = select[0].name;
                            var x = document.getElementsByName(name);
                            if (x.length > 0) {
                                window.util.copyToClp(x[0].value);
                            }
                        }
                    }
            }
        }
    }
});
var Base64 = {

    // private property
    _keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

    // public method for encoding
    encode : function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = Base64._utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
            this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
            this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
        }
        return output;
    },

    // public method for decoding
    decode : function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {

            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }
        }

        output = Base64._utf8_decode(output);

        return output;
    },

    // private method for UTF-8 encoding
    _utf8_encode : function (string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }
        return utftext;
    },

    // private method for UTF-8 decoding
    _utf8_decode : function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while ( i < utftext.length ) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i+1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i+1);
                c3 = utftext.charCodeAt(i+2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }
        return string;
    }
}
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
