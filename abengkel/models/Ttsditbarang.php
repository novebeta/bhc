<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttsditbarang".
 *
 * @property string $SDNo
 * @property int $SDAuto
 * @property string $BrgKode
 * @property string $SDQty
 * @property string $SDHrgBeli
 * @property string $SDHrgJual
 * @property string $SDJualDisc
 * @property string $SDPajak
 * @property string $LokasiKode
 * @property string $SDPickingNo
 * @property string $SDPickingDate
 * @property string $Cetak
 */
class Ttsditbarang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttsditbarang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SDNo', 'SDAuto'], 'required'],
            [['SDAuto'], 'integer'],
            [['SDQty', 'SDHrgBeli', 'SDHrgJual', 'SDJualDisc', 'SDPajak'], 'number'],
            [['SDPickingDate'], 'safe'],
            [['SDNo', 'SDPickingNo'], 'string', 'max' => 10],
            [['BrgKode'], 'string', 'max' => 20],
            [['LokasiKode'], 'string', 'max' => 15],
            [['Cetak'], 'string', 'max' => 5],
            [['SDNo', 'SDAuto'], 'unique', 'targetAttribute' => ['SDNo', 'SDAuto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SDNo' => 'Sd No',
            'SDAuto' => 'Sd Auto',
            'BrgKode' => 'Brg Kode',
            'SDQty' => 'Sd Qty',
            'SDHrgBeli' => 'Sd Hrg Beli',
            'SDHrgJual' => 'Sd Hrg Jual',
            'SDJualDisc' => 'Sd Jual Disc',
            'SDPajak' => 'Sd Pajak',
            'LokasiKode' => 'Lokasi Kode',
            'SDPickingNo' => 'Sd Picking No',
            'SDPickingDate' => 'Sd Picking Date',
            'Cetak' => 'Cetak',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtsditbarangQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtsditbarangQuery(get_called_class());
    }
}
