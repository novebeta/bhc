<?php

namespace abengkel\models;

/**
 * This is the model class for table "tddealer".
 *
 * @property string $DealerKode
 * @property string $DealerNama
 * @property string $DealerContact
 * @property string $DealerAlamat
 * @property string $DealerKota
 * @property string $DealerProvinsi
 * @property string $DealerTelepon
 * @property string $DealerFax
 * @property string $DealerEmail
 * @property string $DealerKeterangan
 * @property string $DealerStatus
 * @property string $DealerKodeDAstra
 * @property string $DealerKodeDMain
 */
class Tddealer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tddealer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DealerKode'], 'required'],
            [['DealerKode'], 'string', 'max' => 10],
            [['DealerNama', 'DealerContact', 'DealerKota', 'DealerProvinsi', 'DealerFax'], 'string', 'max' => 50],
            [['DealerAlamat', 'DealerTelepon'], 'string', 'max' => 75],
            [['DealerEmail'], 'string', 'max' => 25],
            [['DealerKeterangan'], 'string', 'max' => 100],
            [['DealerStatus'], 'string', 'max' => 1],
            [['DealerKodeDAstra'], 'string', 'max' => 5],
            [['DealerKodeDMain'], 'string', 'max' => 3],
            [['DealerKode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'DealerKode'       => 'Dealer Kode',
            'DealerNama'       => 'Dealer Nama',
            'DealerContact'    => 'Dealer Contact',
            'DealerAlamat'     => 'Dealer Alamat',
            'DealerKota'       => 'Dealer Kota',
            'DealerProvinsi'   => 'Dealer Provinsi',
            'DealerTelepon'    => 'Dealer Telepon',
            'DealerFax'        => 'Dealer Fax',
            'DealerEmail'      => 'Dealer Email',
            'DealerKeterangan' => 'Dealer Keterangan',
            'DealerStatus'     => 'Dealer Status',
            'DealerKodeDAstra' => 'Dealer Kode D Astra',
            'DealerKodeDMain'  => 'Dealer Kode D Main',
        ];
    }

	public static function colGrid()
	{
		return [
			'DealerKode'        => ['width' => 60,'label' => 'Kode ','name'  => 'DealerKode'],
			'DealerNama'        => ['width' => 225,'label' => 'Nama Dealer','name'  => 'DealerNama'],
			'DealerAlamat'      => ['width' => 175,'label' => 'Alamat','name'  => 'DealerAlamat'],
			'DealerProvinsi'    => ['width' => 100,'label' => 'Provinsi','name'  => 'DealerProvinsi'],
			'DealerKota'        => ['width' => 90,'label' => 'Kota','name'  => 'DealerKota'],
			'DealerContact'     => ['width' => 100,'label' => 'Kontak','name'  => 'DealerContact'],
			'DealerTelepon'     => ['width' => 125,'label' => 'Telepon','name'  => 'DealerTelepon'],
			'DealerStatus'      => ['width' => 55,'label' => 'Status','name'  => 'DealerStatus'],
		];
	}

	    /**
     * {@inheritdoc}
     * @return TddealerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TddealerQuery(get_called_class());
    }
}
