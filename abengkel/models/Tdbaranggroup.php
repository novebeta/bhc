<?php

namespace abengkel\models;

/**
 * This is the model class for table "tdbaranggroup".
 *
 * @property string $BrgGroup
 * @property string $BrgGroupNama
 */
class Tdbaranggroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdbaranggroup';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BrgGroup'], 'required'],
            [['BrgGroup'], 'string', 'max' => 15],
            [['BrgGroupNama'], 'string', 'max' => 75],
            [['BrgGroup'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'BrgGroup' => 'Brg Group',
            'BrgGroupNama' => 'Brg Group Nama',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TdbaranggroupQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdbaranggroupQuery(get_called_class());
    }
}
