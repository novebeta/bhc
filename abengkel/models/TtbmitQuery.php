<?php
namespace abengkel\models;
use common\components\General;
use abengkel\components\Menu;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttbmit]].
 *
 * @see Ttbmit
 */
class TtbmitQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttbmit[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttbmit|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]     = Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'PosKode' ]    = Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'BMNo' ]       = $post[ 'BMNo' ] ?? '--';
		$post[ 'BMLink' ]     = $post[ 'BMLink' ] ?? '--';
        $post[ 'BMBayar' ]    = floatval( $post[ 'BMBayar' ] ?? 0 );
        $post[ 'BMNoOLD' ]    = $post[ 'BMNoOLD' ] ?? '--';
        $post[ 'BMLinkOLD' ]  = $post[ 'BMLinkOLD' ] ?? '--';
        try {
		General::cCmd( "CALL fbmit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?);" )
		       ->bindParam( 1, $post[ 'Action' ] )
		       ->bindParam( 2, $post[ 'UserID' ] )
		       ->bindParam( 3, $post[ 'KodeAkses' ] )
		       ->bindParam( 4, $post[ 'PosKode' ] )
		       ->bindParam( 5, $post[ 'BMNo' ] )
		       ->bindParam( 6, $post[ 'BMLink' ] )
		       ->bindParam( 7, $post[ 'BMBayar' ] )
		       ->bindParam( 8, $post[ 'BMNoOLD' ] )
		       ->bindParam( 9, $post[ 'BMLinkOLD' ] )
		       ->execute();
		$exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
	}
}
