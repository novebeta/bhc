<?php

namespace abengkel\models;

/**
 * This is the model class for table "vglall".
 *
 * @property string $NoGL
 * @property string $TglGL
 * @property string $KodeTrans SS,FB,DK,SK,SM,PB,SD,PD,KK,KM,BK,BM
 * @property string $MemoGL
 * @property string $GLLink
 * @property string $HPLink
 * @property string $UserID
 * @property string $TotalDebetGL
 * @property string $TotalKreditGL
 * @property int $GLAutoN
 * @property string $NoAccount
 * @property string $NamaAccount
 * @property string $KeteranganGL
 * @property string $DebetGL
 * @property string $KreditGL
 */
class Vglall extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vglall';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['TglGL'], 'safe'],
            [['TotalDebetGL', 'TotalKreditGL', 'DebetGL', 'KreditGL'], 'number'],
            [['GLAutoN'], 'required'],
            [['GLAutoN'], 'integer'],
            [['NoGL', 'NoAccount'], 'string', 'max' => 10],
            [['KodeTrans'], 'string', 'max' => 2],
            [['MemoGL'], 'string', 'max' => 300],
            [['GLLink', 'HPLink'], 'string', 'max' => 18],
            [['UserID'], 'string', 'max' => 15],
            [['NamaAccount', 'KeteranganGL'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'NoGL' => 'No Gl',
            'TglGL' => 'Tgl Gl',
            'KodeTrans' => 'Kode Trans',
            'MemoGL' => 'Memo Gl',
            'GLLink' => 'Gl Link',
            'HPLink' => 'Hp Link',
            'UserID' => 'User ID',
            'TotalDebetGL' => 'Total Debet Gl',
            'TotalKreditGL' => 'Total Kredit Gl',
            'GLAutoN' => 'Gl Auto N',
            'NoAccount' => 'No Account',
            'NamaAccount' => 'Nama Account',
            'KeteranganGL' => 'Keterangan Gl',
            'DebetGL' => 'Debet Gl',
            'KreditGL' => 'Kredit Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VglallQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VglallQuery(get_called_class());
    }
}
