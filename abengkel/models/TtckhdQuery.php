<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttckhd]].
 *
 * @see Ttckhd
 */
class TtckhdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttckhd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttckhd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function ttckhdFillByNo() {
        return $this->select( new Expression( "CKMemo, CKNo, CKNoKlaim, CKTgl, CKTotalPart + CKTotalJasa AS CKTotal, CKTotalJasa, CKTotalPart, Cetak, NoGL, UserID"));
    }

	public function callSP( $post ) {
		$post[ 'UserID' ]     = Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'PosKode' ] = Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'CKNo' ]       = $post[ 'CKNo' ] ?? '--';
		$post[ 'CKNoBaru' ]   = $post[ 'CKNoView' ] ?? '--';
		$post[ 'CKTgl' ]      = date('Y-m-d H:i:s', strtotime($post[ 'CKTgl' ] ?? date( 'Y-m-d H:i:s' )));
		$post[ 'CKTotalPart'] = floatval( $post[ 'CKTotalPart' ] ?? 0 );
		$post[ 'CKTotalJasa'] = floatval( $post[ 'CKTotalJasa' ] ?? 0 );
		$post[ 'CKNoKlaim' ]  = $post[ 'CKNoKlaim' ] ?? '--';
		$post[ 'CKMemo' ]     = $post[ 'CKMemo' ] ?? '--';
        $post[ 'Cetak' ]      = $post[ 'Cetak' ] ?? 'Belum';
		$post[ 'NoGL' ]       = $post[ 'NoGL' ] ?? '--';
        try {
            General::cCmd( "SET @CKNo = ?;" )->bindParam( 1, $post[ 'CKNo' ] )->execute();
            General::cCmd( "CALL fckhd(?,@Status,@Keterangan,?,?,?,@CKNo,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'CKNoBaru' ] )
                ->bindParam( 6, $post[ 'CKTgl' ] )
                ->bindParam( 7, $post[ 'CKTotalPart' ] )
                ->bindParam( 8, $post[ 'CKTotalJasa' ] )
                ->bindParam( 9, $post[ 'CKNoKlaim' ] )
                ->bindParam( 10, $post[ 'CKMemo' ] )
                ->bindParam( 11, $post[ 'Cetak' ] )
                ->bindParam( 12, $post[ 'NoGL' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @CKNo,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'CKNo'       => $post[ 'CKNo' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $CKNo       = $exe[ '@CKNo' ] ?? $post[ 'CKNo' ];
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'CKNo'       => $CKNo,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
	}
}
