<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttpshd]].
 *
 * @see Ttpshd
 */
class TtpshdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttpshd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttpshd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function ttpshdFillByNo() {
        return $this->select( new Expression( "ttpshd.PSNo,
                                ttpshd.PSTgl,
                                ttpshd.SupKode,
                                ttpshd.LokasiKode,
                                ttpshd.PSKeterangan,
                                ttpshd.PSSubTotal,
                                ttpshd.PSDiscFinal,
                                ttpshd.PSTotalPajak,
                                ttpshd.PSBiayaKirim,
                                ttpshd.PSTotal,
                                ttpshd.UserID,
                                ttpshd.NoGL,
                                ttpshd.KodeTrans,
                                tdsupplier.SupNama,
                                ttpshd.PINo,
                                IFNULL(
                                    ttpihd.PITgl,
                                    STR_TO_DATE('01/01/1900', '%m/%d/%Y')
                                ) AS PITgl,
                                ttpshd.PONo,
                                IFNULL(
                                    ttpohd.POTgl,
                                    STR_TO_DATE('01/01/1900', '%m/%d/%Y')
                                ) AS POTgl,
                                ttpshd.PosKode,
                                ttpshd.Cetak,
                                ttpshd.PSNoRef,
                                ttpshd.DealerKode,
                                ttpshd.PSTerbayar,
                                IFNULL(tddealer.DealerNama, '--') AS DealerNama,
                                ttpshd.TINo, ttpshd.ZnoPenerimaan, ttpshd.ZnoShippingList" ) )
            ->join( "LEFT OUTER JOIN", "tdsupplier", "ttpshd.SupKode = tdsupplier.SupKode" )
            ->join( "LEFT OUTER JOIN", "tddealer", "ttpshd.DealerKode = tddealer.DealerKode" )
            ->join( "LEFT OUTER JOIN", "ttpihd", "ttpihd.PSNo = ttpshd.PSNo OR ttpihd.PINo = ttpshd.PINo" )
            ->join( "LEFT OUTER JOIN", "ttpohd", "ttpohd.PONo = ttpohd.PONo" )
            ->groupBy("ttpshd.PSNo");
    }

    public function callSP( $post ) {
        if(!empty($post['KodeTrans']) && $post['KodeTrans'] == 'TC05'){
            $post[ 'SupKode' ] = $post[ 'DealerKode' ];
        }
        $post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'PSNo' ]          = $post[ 'PSNo' ] ?? '--';
        $post[ 'PSNoBaru' ]      = $post[ 'PSNoView' ] ?? '--';
        $post[ 'PSTgl' ]         = date('Y-m-d H:i:s', strtotime($post[ 'PSTgl' ] ?? date( 'Y-m-d H:i:s' )));
        $post[ 'PINo' ]          = $post[ 'PINo' ] ?? '--';
        $post[ 'PONo' ]          = $post[ 'PONo' ] ?? '--';
        $post[ 'TINo' ]          = $post[ 'TINo' ] ?? '--';
        $post[ 'KodeTrans' ]     = $post[ 'KodeTrans' ] ?? '--';
        $post[ 'LokasiKode' ]    = $post[ 'LokasiKode' ] ?? '--';
        $post[ 'SupKode' ]       = $post[ 'SupKode' ] ?? '--';
        $post[ 'DealerKode' ]    = $post[ 'DealerKode' ] ?? '--';
        $post[ 'PSNoRef' ]       = $post[ 'PSNoRef' ] ?? '--';
        $post[ 'PSSubTotal' ]    = floatval( $post[ 'PSSubTotal' ] ?? 0 );
        $post[ 'PSDiscFinal' ]   = floatval( $post[ 'PSDiscFinal' ] ?? 0 );
        $post[ 'PSTotalPajak' ]  = floatval( $post[ 'PSTotalPajak' ] ?? 0 );
        $post[ 'PSBiayaKirim' ]  = floatval( $post[ 'PSBiayaKirim' ] ?? 0 );
        $post[ 'PSTotal' ]       = floatval( $post[ 'PSTotal' ] ?? 0 );
        $post[ 'PSTerbayar' ]    = floatval( $post[ 'PSTerbayar' ] ?? 0 );
        $post[ 'PSKeterangan' ]  = $post[ 'PSKeterangan' ] ?? '--';
        $post[ 'Cetak' ]         = $post[ 'Cetak' ] ?? 'Belum';
        $post[ 'NoGL' ]          = $post[ 'NoGL' ] ?? '--';
        $post[ 'ZnoPenerimaan' ]          = $post[ 'ZnoPenerimaan' ] ?? '--';
        $post[ 'ZnoShippingList' ]          = $post[ 'ZnoShippingList' ] ?? '--';
        try {
            General::cCmd( "SET @PSNo = ?;" )->bindParam( 1, $post[ 'PSNo' ] )->execute();
            General::cCmd( "CALL fpshd(?,@Status,@Keterangan,?,?,?,@PSNo,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'PSNoBaru' ] )
                ->bindParam( 6, $post[ 'PSTgl' ] )
                ->bindParam( 7, $post[ 'PINo' ] )
                ->bindParam( 8, $post[ 'PONo' ] )
                ->bindParam( 9, $post[ 'TINo' ] )
                ->bindParam( 10, $post[ 'KodeTrans' ] )
                ->bindParam( 11, $post[ 'LokasiKode' ] )
                ->bindParam( 12, $post[ 'SupKode' ] )
                ->bindParam( 13, $post[ 'DealerKode' ] )
                ->bindParam( 14, $post[ 'PSNoRef' ] )
                ->bindParam( 15, $post[ 'PSSubTotal' ] )
                ->bindParam( 16, $post[ 'PSDiscFinal' ] )
                ->bindParam( 17, $post[ 'PSTotalPajak' ] )
                ->bindParam( 18, $post[ 'PSBiayaKirim' ] )
                ->bindParam( 19, $post[ 'PSTotal' ] )
                ->bindParam( 20, $post[ 'PSTerbayar' ] )
                ->bindParam( 21, $post[ 'PSKeterangan' ] )
                ->bindParam( 22, $post[ 'Cetak' ] )
                ->bindParam( 23, $post[ 'NoGL' ] )
                ->bindParam( 24, $post[ 'ZnoPenerimaan' ] )
                ->bindParam( 25, $post[ 'ZnoShippingList' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @PSNo,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'PSNo'       => $post[ 'PSNo' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $PSNo       = $exe[ '@PSNo' ] ?? $post[ 'PSNo' ];
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'PSNo'       => $PSNo,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
