<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttsohd".
 *
 * @property string $SONo
 * @property string $SOTgl
 * @property string $SINo
 * @property string $PONo
 * @property string $KodeTrans
 * @property string $PosKode
 * @property string $KarKode
 * @property string $MotorNoPolisi
 * @property string $LokasiKode
 * @property string $SOSubTotal
 * @property string $SODiscFinal
 * @property string $SOTotalPajak
 * @property string $SOBiayaKirim
 * @property string $SOTotal
 * @property string $SOTerbayar
 * @property string $SOKeterangan
 * @property string $Cetak
 * @property string $UserID
 */
class Ttsohd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttsohd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SONo'], 'required'],
            [['SOTgl'], 'safe'],
            [['SOSubTotal', 'SODiscFinal', 'SOTotalPajak', 'SOBiayaKirim', 'SOTotal', 'SOTerbayar'], 'number'],
            [['SONo', 'SINo', 'PONo', 'KarKode'], 'string', 'max' => 10],
            [['KodeTrans'], 'string', 'max' => 4],
            [['PosKode'], 'string', 'max' => 20],
            [['MotorNoPolisi'], 'string', 'max' => 12],
            [['LokasiKode', 'UserID'], 'string', 'max' => 15],
            [['SOKeterangan'], 'string', 'max' => 150],
            [['Cetak'], 'string', 'max' => 5],
            [['SONo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SONo'          => 'So No',
            'SOTgl'         => 'So Tgl',
            'SINo'          => 'Si No',
            'PONo'          => 'Po No',
            'KodeTrans'     => 'Kode Trans',
            'PosKode'       => 'Pos Kode',
            'KarKode'       => 'Kar Kode',
            'MotorNoPolisi' => 'Motor No Polisi',
            'LokasiKode'    => 'Lokasi Kode',
            'SOSubTotal'    => 'So Sub Total',
            'SODiscFinal'   => 'So Disc Final',
            'SOTotalPajak'  => 'So Total Pajak',
            'SOBiayaKirim'  => 'So Biaya Kirim',
            'SOTotal'       => 'So Total',
            'SOTerbayar'    => 'So Terbayar',
            'SOKeterangan'  => 'So Keterangan',
            'Cetak'         => 'Cetak',
            'UserID'        => 'User ID',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
        return [
            'SONo'                      => ['width' => 90,'label' => 'No SO','name'  => 'SONo'],
            'SOTgl'                     => ['width' => 115,'label' => 'Tgl SO','name'  => 'SOTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y H:i:s']],
			'PONo'                      => ['width' => 90,'label' => 'No PO','name'  => 'PONo'],
			'tdcustomer.MotorNoPolisi'             => ['width' => 85,'label' => 'No Polisi','name'  => 'MotorNoPolisi'],
			'tdcustomer.CusNama'                   => ['width' => 150,'label' => 'Konsumen','name'  => 'CusNama'],
			'tdcustomer.MotorNama'                 => ['width' => 205,'label' => 'Nama Motor','name'  => 'MotorNama'],
			'KarKode'                   => ['width' => 90,'label' => 'Karyawan','name'  => 'KarKode'],
            'SOTotal'                   => ['width' => 110,'label' => 'Total','name'  => 'SOTotal', 'formatter' => 'number', 'align' => 'right'],
            'SOTerbayar'                => ['width' => 110,'label' => 'Terbayar','name'  => 'SOTerbayar', 'formatter' => 'number', 'align' => 'right'],
			'Cetak'                     => ['width' => 50,'label' => 'Cetak','name'  => 'Cetak'],
			'UserID'                    => ['width' => 80,'label' => 'UserID','name'  => 'UserID'],
			'SINo'                      => ['width' => 90,'label' => 'No SI','name'  => 'SINo'],
			'PosKode'					=> ['width' => 90,'label' => 'Pos','name'  => 'PosKode'],
			'SOKeterangan'              => ['width' => 300,'label' => 'Keterangan','name'  => 'SOKeterangan'],
        ];
    }

    public static function colGridPick()
    {
        return [
            'SONo'                          => ['width' => 100,'label' => 'No SO','name'  => 'SONo'],
            'SOTgl'                         => ['width' => 120,'label' => 'Tgl SO','name'  => 'SOTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y']],
            'MotorNoPolisi'                 => ['width' => 120,'label' => 'No Polisi','name'  => 'MotorNoPolisi'],
            'CusNama'                       => ['width' => 120,'label' => 'Nama Konsumen','name'  => 'CusNama'],
            'LokasiKode'                    => ['width' => 100,'label' => 'Lokasi','name'  => 'LokasiKode'],
            'KodeTrans'                     => ['width' => 80,'label' => 'Kode','name'  => 'KodeTrans'],
            'SOKeterangan'                  => ['width' => 200,'label' => 'Keterangan','name'  => 'SOKeterangan'],
            'KarKode'                       => ['width' => 200,'label' => 'Karyawan','name'  => 'KarKode'],
        ];
    }


	public function getCustomer() {
		return $this->hasOne( Tdcustomer::className(), [ 'MotorNoPolisi' => 'MotorNoPolisi' ] );
	}

    /**
     * {@inheritdoc}
     * @return TtsohdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtsohdQuery(get_called_class());
    }
}
