<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttpsit".
 *
 * @property string $PSNo
 * @property int $PSAuto
 * @property string $BrgKode
 * @property string $PSQty
 * @property string $PSHrgBeli
 * @property string $PSHrgJual
 * @property string $PSDiscount
 * @property string $PSPajak
 * @property string $PONo
 * @property string $PINo
 * @property string $LokasiKode
 * @property string $PIQtyS
 */
class Ttpsit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttpsit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PSNo', 'PSAuto'], 'required'],
            [['PSAuto'], 'integer'],
            [['PSQty', 'PSHrgBeli', 'PSHrgJual', 'PSDiscount', 'PSPajak', 'PIQtyS'], 'number'],
            [['PSNo', 'PONo', 'PINo'], 'string', 'max' => 10],
            [['BrgKode'], 'string', 'max' => 20],
            [['LokasiKode'], 'string', 'max' => 15],
            [['PSNo', 'PSAuto'], 'unique', 'targetAttribute' => ['PSNo', 'PSAuto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PSNo' => 'Ps No',
            'PSAuto' => 'Ps Auto',
            'BrgKode' => 'Brg Kode',
            'PSQty' => 'Ps Qty',
            'PSHrgBeli' => 'Ps Hrg Beli',
            'PSHrgJual' => 'Ps Hrg Jual',
            'PSDiscount' => 'Ps Discount',
            'PSPajak' => 'Ps Pajak',
            'PONo' => 'Po No',
            'PINo' => 'Pi No',
            'LokasiKode' => 'Lokasi Kode',
            'PIQtyS' => 'Pi Qty S',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtpsitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtpsitQuery(get_called_class());
    }
}
