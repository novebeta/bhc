<?php
namespace abengkel\models;
/**
 * This is the ActiveQuery class for [[Tdcustomer]].
 *
 * @see Tdcustomer
 */
class TdcustomerQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Tdcustomer|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function combo( $where = null ) {
		$q = $this->select( [ "CONCAT(MotorNoPolisi,' - ',CusNama) as label", "MotorNoPolisi as value" ] )
		          ->orderBy( 'CusStatus, MotorNoPolisi' );
		if ( $where != null ) {
			$q->where( 'TglAkhirServis BETWEEN :from AND :to', [
				':from' => $where[ 'from' ],
				':to'   => $where[ 'to' ],
			] );
		}
		return $q->asArray()
		         ->all();
	}
	/**
	 * {@inheritdoc}
	 * @return Tdcustomer[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
}
