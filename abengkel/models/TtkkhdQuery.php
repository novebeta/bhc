<?php
namespace abengkel\models;
use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttkkhd]].
 *
 * @see Ttkkhd
 */
class TtkkhdQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttkkhd[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttkkhd|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function ttkkhdFillGetData() {
		return $this->select( new Expression( "ttkkhd.KKNo, ttkkhd.KKTgl, ttkkhd.KKJenis, ttkkhd.KasKode, ttkkhd.SupKode, ttkkhd.KKPerson, ttkkhd.KKMemo, ttkkhd.KKNominal, ttkkhd.UserID, 
            ttkkhd.KodeTrans, ttkkhd.NoGL, traccount.NamaAccount, ttkkhd.PosKode, ttkkhd.Cetak" ) )
		            ->join( "LEFT JOIN", "traccount", "ttkkhd.KasKode = traccount.NoAccount" );
	}
	public function transferKasGetData() {
		return $this->select( new Expression( "ttkkhd.KKNo, ttkkhd.KKTgl, ttkkhd.KKJenis, ttkkhd.KodeTrans, ttkkhd.PosKode, ttkkhd.SupKode, ttkkhd.KasKode AS KasKodeKK,
					ttkkhd.KasKode, ttkkhd.KKPerson, ttkkhd.KKNominal, ttkkhd.KKMemo, ttkkhd.NoGL,ttkkhd.NoGL AS NoGLKK, ttkkhd.Cetak, ttkkhd.UserID, ttkkit.KKLink, 
					ttkkit.KKBayar, ttkmit.KMLink, ttkmit.KMBayar, ttkmhd.KMNo, ttkmhd.KMTgl, ttkmhd.KMJenis,ttkmhd.NoGL AS NoGLKM, ttkmhd.KasKode AS KasKodeKM,
					ttkmhd.MotorNoPolisi, ttkmhd.KMPerson, ttkmhd.KMBayarTunai, ttkmhd.KMNominal, ttkmhd.KMMemo" ) )
		            ->join( "INNER JOIN", "ttkkit", "ttkkhd.KKNo = ttkkit.KKNo" )
		            ->join( "INNER JOIN", "ttkmit", "ttkkit.KKLInk = ttkmit.KMLink" )
		            ->join( "INNER JOIN", "ttkmhd", "ttkmhd.KMNo = ttkmit.KMNo" )
		            ->andWhere( "(ttkkhd.KKJenis = 'Kas Transfer' AND ttkmhd.KMJenis = 'Kas Transfer')" );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'PosKode' ]    = Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'KKNo' ]       = $post[ 'KKNo' ] ?? '--';
		$post[ 'KKNoBaru' ]   = $post[ 'KKNoView' ] ?? '--';
		$post[ 'KKTgl' ]      = date('Y-m-d H:i:s', strtotime($post[ 'KKTgl' ] ?? date( 'Y-m-d H:i:s' )));
		$post[ 'KKJenis' ]    = $post[ 'KKJenis' ] ?? '--';
		$post[ 'KodeTrans' ]  = $post[ 'KodeTrans' ] ?? '--';
		$post[ 'LokasiKode' ] = $post[ 'LokasiKode' ] ?? '--';
		$post[ 'SupKode' ]    = $post[ 'SupKode' ] ?? '--';
		$post[ 'KasKode' ]    = $post[ 'KasKode' ] ?? '--';
		$post[ 'KKPerson' ]   = $post[ 'KKPerson' ] ?? '--';
		$post[ 'KKNominal' ]  = floatval( $post[ 'KKNominal' ] ?? 0 );
		$post[ 'KKMemo' ]     = $post[ 'KKMemo' ] ?? '--';
		$post[ 'NoGL' ]       = $post[ 'NoGL' ] ?? '--';
		$post[ 'Cetak' ]      = $post[ 'Cetak' ] ?? 'Belum';
		try {
			General::cCmd( "SET @KKNo = ?;" )->bindParam( 1, $post[ 'KKNo' ] )->execute();
			General::cCmd( "CALL fkkhd(?,@Status,@Keterangan,?,?,?,@KKNo,?,?,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'PosKode' ] )
			       ->bindParam( 5, $post[ 'KKNoBaru' ] )
			       ->bindParam( 6, $post[ 'KKTgl' ] )
			       ->bindParam( 7, $post[ 'KKJenis' ] )
			       ->bindParam( 8, $post[ 'KodeTrans' ] )
			       ->bindParam( 9, $post[ 'LokasiKode' ] )
			       ->bindParam( 10, $post[ 'SupKode' ] )
			       ->bindParam( 11, $post[ 'KasKode' ] )
			       ->bindParam( 12, $post[ 'KKPerson' ] )
			       ->bindParam( 13, $post[ 'KKNominal' ] )
			       ->bindParam( 14, $post[ 'KKMemo' ] )
			       ->bindParam( 15, $post[ 'NoGL' ] )
			       ->bindParam( 16, $post[ 'Cetak' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @KKNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'KKNo'       => $post[ 'KKNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$KKNo       = $exe[ '@KKNo' ] ?? $post[ 'KKNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'KKNo'       => $KKNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
	public function callSPTransfer( $post ) {
		$post[ 'UserID' ]    = $post[ 'UserID' ] ?? \abengkel\components\Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ] = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'PosKode' ]   = Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'KTNo' ]      = $post[ 'KTNo' ] ?? '--';
		$post[ 'KTNoBaru' ]  = $post[ 'KTNoBaru' ] ?? '--';
		$post[ 'KTTgl' ]     = $post[ 'KKTgl' ] ?? date( 'Y-m-d' );
		$post[ 'KTMemo' ]         = $post[ 'KKMemo' ] ?? '--';
		$post[ 'KTNominal' ]      = $post[ 'KKNominal' ] ?? 0;
		$post[ 'KTJenis' ]        = $post[ 'KKJenis' ] ?? '--';
		$post[ 'KKPerson' ]       = $post[ 'KKPerson' ] ?? '--';
		$post[ 'KTPersonAsal' ]   = $post[ 'KKPerson' ] ?? '--';
		$post[ 'KTPersonTujuan' ] = $post[ 'KMPerson' ] ?? '--';
		$post[ 'KKNo' ]           = $post[ 'KKNo' ] ?? '--';
		$post[ 'KMNo' ]           = $post[ 'KMNo' ] ?? '--';
		$post[ 'NoGLKK' ]         = $post[ 'NoGLKK' ] ?? '--';
		$post[ 'NoGLKM' ]         = $post[ 'NoGLKM' ] ?? '--';
		$post[ 'KasKodeKK' ]      = $post[ 'KasKodeKK' ] ?? '--';
		$post[ 'KasKodeKM' ]      = $post[ 'KasKodeKM' ] ?? '--';
		try {
			General::cCmd( "SET @KTNo = ?;" )->bindParam( 1, $post[ 'KTNo' ] )->execute();
			General::cCmd( "CALL fkt(?,@Status,@Keterangan,?,?,?,@KTNo,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'PosKode' ] )
			       ->bindParam( 5, $post[ 'KTNoBaru' ] )
			       ->bindParam( 6, $post[ 'KTTgl' ] )
			       ->bindParam( 7, $post[ 'KTMemo' ] )
			       ->bindParam( 8, $post[ 'KTNominal' ] )
			       ->bindParam( 9, $post[ 'KTJenis' ] )
			       ->bindParam( 10, $post[ 'KTPersonAsal' ] )
			       ->bindParam( 11, $post[ 'KTPersonTujuan' ] )
			       ->bindParam( 12, $post[ 'KKNo' ] )
			       ->bindParam( 13, $post[ 'KMNo' ] )
			       ->bindParam( 14, $post[ 'NoGLKK' ] )
			       ->bindParam( 15, $post[ 'NoGLKM' ] )
			       ->bindParam( 16, $post[ 'KasKodeKK' ] )
			       ->bindParam( 17, $post[ 'KasKodeKM' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @KTNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'KTNo'       => $post[ 'KTNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$KTNo       = $exe[ '@KTNo' ] ?? $post[ 'KTNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'KTNo'       => $KTNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}


	public function callSPJKK( $post ) {
		$post[ 'KKNo' ]   = $post[ 'KKNo' ] ?? '--';
		$post[ 'UserID' ] = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		try {
			General::cCmd( "CALL jkk(?,?);" )
			       ->bindParam( 1, $post[ 'KKNo' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->execute();
		} catch ( Exception $e ) {
			return [
				'KKNo'       => $post[ 'KKNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$KKNo       = $post[ 'KKNo' ];
		$keterangan = 'Jurnal ' . $post[ 'KKNo' ] . ' berhasil.';
		return [
			'KKNo'       => $KKNo,
			'status'     => 0,
			'keterangan' => $keterangan
		];
	}
}
