<?php

namespace abengkel\models;

use common\components\General;
use yii\base\Exception;

/**
 * This is the ActiveQuery class for [[Tdbaranglokasi]].
 *
 * @see Tdbaranglokasi
 */
class TdbaranglokasiQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdbaranglokasi[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdbaranglokasi|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function callSP() {
		$status = 0;
        try {
		General::cCmd( "CALL cUPDATEtdbaranglokasi();" )
		       ->execute();
        } catch ( Exception $e ) {
            $status = 1;
//            $keterangan = $e->getMessage();
        }
        return [
            'status'     => $status,
//            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
	}
}
