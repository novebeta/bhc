<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttqshd".
 *
 * @property string $QSPanggil
 * @property string $QSTujuan
 * @property string $QSKasir
 * @property string $QSMekanik
 * @property string $QSPanggilSudah
 * @property string $QSPanggilApa
 * @property string $QsPanggilNama
 */
class Ttqshd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttqshd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['QSPanggil', 'QSTujuan', 'QSKasir', 'QSMekanik'], 'required'],
            [['QSPanggil', 'QSKasir', 'QSMekanik'], 'string', 'max' => 10],
            [['QSTujuan'], 'string', 'max' => 15],
            [['QSPanggilSudah'], 'string', 'max' => 5],
            [['QSPanggilApa'], 'string', 'max' => 20],
            [['QsPanggilNama'], 'string', 'max' => 2000],
            [['QSPanggil', 'QSTujuan', 'QSKasir', 'QSMekanik'], 'unique', 'targetAttribute' => ['QSPanggil', 'QSTujuan', 'QSKasir', 'QSMekanik']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'QSPanggil' => 'Qs Panggil',
            'QSTujuan' => 'Qs Tujuan',
            'QSKasir' => 'Qs Kasir',
            'QSMekanik' => 'Qs Mekanik',
            'QSPanggilSudah' => 'Qs Panggil Sudah',
            'QSPanggilApa' => 'Qs Panggil Apa',
            'QsPanggilNama' => 'Qs Panggil Nama',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtqshdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtqshdQuery(get_called_class());
    }
}
