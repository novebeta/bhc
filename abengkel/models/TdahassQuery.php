<?php

namespace abengkel\models;

use yii\helpers\ArrayHelper;

/**
 * This is the ActiveQuery class for [[Tdahass]].
 *
 * @see Tdahass
 */
class TdahassQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdahass[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdahass|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	public function combo() {
		return $this->select( [ "CONCAT(AHASSNomor,' - ',AHASSNama) as label", "AHASSNomor as value" ] )
		            ->orderBy( 'AHASSStatus, AHASSNomor' )
		            ->asArray()
		            ->all();
	}

    public function ahass()
    {
        return ArrayHelper::map($this->select(["CONCAT(AHASSNama) as label", "AHASSNomor as value"])
            ->asArray()
            ->all(), 'value', 'label');
    }

    public function select2( $value, $label, $orderBy, $where = [ 'condition' => null, 'params' => [] ] , $allOption = false) {
        $option =
            $this->select( [ "CONCAT(" . implode( ",'||',", $label ) . ") as label", "$value as value" ] )
                ->orderBy( $orderBy )
                ->groupBy( $value )
                ->where( $where[ 'condition' ], $where[ 'params' ] )
                ->asArray()
                ->all();
        if ( $allOption ) {
            return array_merge( [ [ 'value' => '%', 'label' => 'Semua' ] ], $option );
        } else {
            return $option;
        }
    }
}
