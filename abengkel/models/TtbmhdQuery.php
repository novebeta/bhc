<?php
namespace abengkel\models;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
use abengkel\components\Menu;
/**
 * This is the ActiveQuery class for [[Ttbmhd]].
 *
 * @see Ttbmhd
 */
class TtbmhdQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttbmhd[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttbmhd|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function ttbmhdFillGetData() {
		return $this->select( new Expression( "ttbmhd.BMNo, ttbmhd.BMTgl, ttbmhd.BMJenis, ttbmhd.KodeTrans, ttbmhd.PosKode, ttbmhd.MotorNoPolisi, ttbmhd.NoGL, ttbmhd.BankKode, ttbmhd.BMPerson,
            ttbmhd.BMCekNo, ttbmhd.BMCekTempo, ttbmhd.BMAC, ttbmhd.BMNominal, ttbmhd.BMMemo, ttbmhd.Cetak, ttbmhd.UserID, traccount.NamaAccount" ) )
		            ->join( "LEFT JOIN", "traccount", "ttbmhd.BankKode = traccount.NoAccount" );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'BMNo' ]          = $post[ 'BMNo' ] ?? '--';
		$post[ 'BMNoBaru' ]      = $post[ 'BMNoView' ] ?? '--';
		$post[ 'BMTgl' ]         = date('Y-m-d H:i:s', strtotime($post[ 'BMTgl' ] ?? date( 'Y-m-d H:i:s' )));
		$post[ 'BMJenis' ]       = $post[ 'BMJenis' ] ?? '--';
		$post[ 'KodeTrans' ]     = $post[ 'KodeTrans' ] ?? '--';
		$post[ 'LokasiKode' ]    = $post[ 'LokasiKode' ] ?? '--';
		$post[ 'MotorNoPolisi' ] = $post[ 'MotorNoPolisi' ] ?? '--';
		$post[ 'BankKode' ]      = $post[ 'BankKode' ] ?? '--';
		$post[ 'BMPerson' ]      = $post[ 'BMPerson' ] ?? '--';
		$post[ 'BMCekNo' ]       = $post[ 'BMCekNo' ] ?? '--';
        $post[ 'BMCekTempo' ]    = date('Y-m-d', strtotime($post[ 'BMCekTempo' ] ?? date( 'Y-m-d' )));
        $post[ 'BMAC' ]          = $post[ 'BMAC' ] ?? '--';
        $post[ 'BMNominal' ]     = floatval( $post[ 'BMNominal' ] ?? 0 );
        $post[ 'BMMemo' ]        = $post[ 'BMMemo' ] ?? '--';
        $post[ 'NoGL' ]          = $post[ 'NoGL' ] ?? '--';
        $post[ 'Cetak' ]         = $post[ 'Cetak' ] ?? 'Belum';
        try {
		General::cCmd( "SET @BMNo = ?;" )->bindParam( 1, $post[ 'BMNo' ] )->execute();
		General::cCmd( "CALL fbmhd(?,@Status,@Keterangan,?,?,?,@BMNo,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
		       ->bindParam( 1, $post[ 'Action' ] )
		       ->bindParam( 2, $post[ 'UserID' ] )
		       ->bindParam( 3, $post[ 'KodeAkses' ] )
		       ->bindParam( 4, $post[ 'PosKode' ] )
		       ->bindParam( 5, $post[ 'BMNoBaru' ] )
		       ->bindParam( 6, $post[ 'BMTgl' ] )
		       ->bindParam( 7, $post[ 'BMJenis' ] )
		       ->bindParam( 8, $post[ 'KodeTrans' ] )
		       ->bindParam( 9, $post[ 'LokasiKode' ] )
		       ->bindParam( 10, $post[ 'MotorNoPolisi' ] )
		       ->bindParam( 11, $post[ 'BankKode' ] )
		       ->bindParam( 12, $post[ 'BMPerson' ] )
		       ->bindParam( 13, $post[ 'BMCekNo' ] )
		       ->bindParam( 14, $post[ 'BMCekTempo' ] )
		       ->bindParam( 15, $post[ 'BMAC' ] )
		       ->bindParam( 16, $post[ 'BMNominal' ] )
		       ->bindParam( 17, $post[ 'BMMemo' ] )
		       ->bindParam( 18, $post[ 'NoGL' ] )
		       ->bindParam( 19, $post[ 'Cetak' ] )
		       ->execute();
		$exe = General::cCmd( "SELECT @BMNo,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'BMNo'       => $post[ 'BMNo' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $BMNo       = $exe[ '@BMNo' ] ?? $post[ 'BMNo' ];
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'BMNo'       => $BMNo,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
	}

	public function callSPJBM( $post ) {
		$post[ 'BMNo' ]   = $post[ 'BMNo' ] ?? '--';
		$post[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		try {
			General::cCmd( "CALL jbm(?,?);" )
			       ->bindParam( 1, $post[ 'BMNo' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->execute();
		} catch ( Exception $e ) {
			return [
				'BMNo'       => $post[ 'BMNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$BMNo       = $post[ 'BMNo' ];
		$keterangan = 'Jurnal ' . $post[ 'BMNo' ] . ' berhasil.';
		return [
			'BMNo'       => $BMNo,
			'status'     => 0,
			'keterangan' => $keterangan
		];
	}
}
