<?php

namespace abengkel\models;

/**
 * This is the model class for table "tvplatno".
 *
 * @property string $MotorNoMesin
 * @property string $PlatNo
 */
class Tvplatno extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tvplatno';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['MotorNoMesin', 'PlatNo'], 'required'],
            [['MotorNoMesin'], 'string', 'max' => 25],
            [['PlatNo'], 'string', 'max' => 12],
            [['MotorNoMesin', 'PlatNo'], 'unique', 'targetAttribute' => ['MotorNoMesin', 'PlatNo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'MotorNoMesin' => 'Motor No Mesin',
            'PlatNo' => 'Plat No',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TvplatnoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TvplatnoQuery(get_called_class());
    }
}
