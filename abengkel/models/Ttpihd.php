<?php

namespace abengkel\models;
/**
 * This is the model class for table "ttpihd".
 *
 * @property string $PINo
 * @property string $PITgl
 * @property string $PIJenis
 * @property string $PSNo
 * @property string $KodeTrans
 * @property string $PosKode
 * @property string $SupKode
 * @property string $LokasiKode
 * @property string $PINoRef
 * @property string $PITerm
 * @property string $PITglTempo
 * @property string $PISubTotal
 * @property string $PIDiscFinal
 * @property string $PITotalPajak
 * @property string $PIBiayaKirim
 * @property string $PITotal
 * @property string $PITerbayar
 * @property string $PIKeterangan
 * @property string $NoGL
 * @property string $Cetak
 * @property string $UserID
 */
class Ttpihd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttpihd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PINo'], 'required'],
            [['PITgl', 'PITglTempo'], 'safe'],
            [['PITerm', 'PISubTotal', 'PIDiscFinal', 'PITotalPajak', 'PIBiayaKirim', 'PITotal', 'PITerbayar'], 'number'],
            [['PINo', 'PSNo', 'NoGL'], 'string', 'max' => 10],
            [['PIJenis'], 'string', 'max' => 6],
            [['KodeTrans'], 'string', 'max' => 4],
            [['PosKode'], 'string', 'max' => 20],
            [['SupKode', 'LokasiKode', 'UserID'], 'string', 'max' => 15],
            [['PINoRef'], 'string', 'max' => 35],
            [['PIKeterangan'], 'string', 'max' => 150],
            [['Cetak'], 'string', 'max' => 5],
            [['PINo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PINo'         => 'Pi No',
            'PITgl'        => 'Pi Tgl',
            'PIJenis'      => 'Pi Jenis',
            'PSNo'         => 'Ps No',
            'KodeTrans'    => 'Kode Trans',
            'PosKode'      => 'Pos Kode',
            'SupKode'      => 'Sup Kode',
            'LokasiKode'   => 'Lokasi Kode',
            'PINoRef'      => 'Pi No Ref',
            'PITerm'       => 'Pi Term',
            'PITglTempo'   => 'Pi Tgl Tempo',
            'PISubTotal'   => 'Pi Sub Total',
            'PIDiscFinal'  => 'Pi Disc Final',
            'PITotalPajak' => 'Pi Total Pajak',
            'PIBiayaKirim' => 'Pi Biaya Kirim',
            'PITotal'      => 'Pi Total',
            'PITerbayar'   => 'Pi Terbayar',
            'PIKeterangan' => 'Pi Keterangan',
            'NoGL'         => 'No Gl',
            'Cetak'        => 'Cetak',
            'UserID'       => 'User ID',
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
		$col  = [
		    'PINo'         => ['width' => 85,'label' => 'No PI','name'  => 'PINo'],
            'PITgl'        => ['width' => 115,'label' => 'Tgl PI','name'  => 'PITgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y H:i:s']],
            'PIJenis'      => ['width' => 50,'label' => 'Jenis','name'  => 'PIJenis'],
            'PSNo'         => ['width' => 85,'label' => 'No PS','name'  => 'PSNo'],
            'KodeTrans'    => ['width' => 60,'label' => 'TC','name'  => 'KodeTrans'],
            'SupKode'      => ['width' => 70,'label' => 'Supplier','name'  => 'SupKode'],
            'LokasiKode'   => ['width' => 85,'label' => 'Lokasi','name'  => 'LokasiKode'],
            'PINoRef'      => ['width' => 105,'label' => 'No REf','name'  => 'PINoRef'],
            'PITotal'      => ['width' => 100,'label' => 'Total','name'  => 'PITotal','formatter' => 'number', 'align' => 'right'],
            'PITerbayar'   => ['width' => 100,'label' => 'Terbayar','name'  => 'PITerbayar','formatter' => 'number', 'align' => 'right'],
		];
		$col['(PITotal-PITerbayar) AS PIStatus'] = ['label' => 'Sisa','width' => 80,'name'  => 'PIStatus','formatter' => 'number', 'align' => 'right'];
		$col['NoGL'] = ['label' => 'NoGL','width' => 70,'name'  => 'NoGL'];
		$col['Cetak'] = ['label' => 'Cetak','width' => 50,'name'  => 'Cetak'];
		$col['UserID'] = ['label' => 'UserID','width' => 80,'name'  => 'UserID'];
		$col['PIKeterangan'] = ['label' => 'Keterangan','width' => 300,'name'  => 'PIKeterangan'];
		$col['PosKode'] = ['label' => 'Pos','width' => 90,'name'  => 'PosKode'];
		return $col;
    }


    public static function colGridImportItems()
    {
        return [
            'noPenerimaan' => ['label' => 'No Penerimaan','width' => 100, 'name' => 'noPenerimaan'],
            'tglPenerimaan' => ['label' => 'Tgl Penerimaan','width' => 90, 'name' => 'tglPenerimaan'],
            'noShippingList' => ['label' => 'No Shipping List','width' => 100, 'name' => 'noShippingList'],
        ];
    }

    // public static function colGridImportItems()
    // {
    //     return [
    //         'noWorkOrder' => ['width' => 75, 'name' => 'noWorkOrder'],
    //         'noPolisi' => ['width' => 75, 'name' => 'noPolisi'],
    //         'namaPembawa' => ['width' => 75, 'name' => 'namaPembawa'],
    //         'namaPemilik' => ['width' => 75, 'name' => 'namaPemilik'],
    //         'noBukuClaimC2' => ['width' => 75, 'name' => 'noBukuClaimC2'],
    //         'noMesin' => ['width' => 75, 'name' => 'noMesin'],
    //         'noRangka' => ['width' => 75, 'name' => 'noRangka'],
    //         'noSAForm' => ['width' => 75, 'name' => 'noSAForm'],
    //         'noTelpPembawa' => ['width' => 75, 'name' => 'noTelpPembawa'],
    //         'noWorkOrderJobReturn' => ['width' => 75, 'name' => 'noWorkOrderJobReturn'],
    //         'alamatPembawa' => ['width' => 75, 'name' => 'alamatPembawa'],
    //         'alamatPemilik' => ['width' => 75, 'name' => 'alamatPemilik'],
    //         'asalUnitEntry' => ['width' => 75, 'name' => 'asalUnitEntry'],
    //         'catatanTambahan' => ['width' => 75, 'name' => 'catatanTambahan'],
    //         'createdTime' => ['width' => 75, 'name' => 'createdTime'],
    //         'hondaIdMekanik' => ['width' => 75, 'name' => 'hondaIdMekanik'],
    //         'hondaIdSA' => ['width' => 75, 'name' => 'hondaIdSA'],
    //         'hubunganDenganPemilik' => ['width' => 75, 'name' => 'hubunganDenganPemilik'],
    //         'idPIT' => ['width' => 75, 'name' => 'idPIT'],
    //         'informasiBensin' => ['width' => 75, 'name' => 'informasiBensin'],
    //         'jenisPIT' => ['width' => 75, 'name' => 'jenisPIT'],
    //         'keluhanKonsumen' => ['width' => 75, 'name' => 'keluhanKonsumen'],
    //         'kmTerakhir' => ['width' => 75, 'name' => 'kmTerakhir'],
    //         'kmTerakhir' => ['width' => 75, 'name' => 'kmTerakhir'],
    //         'kodeKecamatanPembawa' => ['width' => 75, 'name' => 'kodeKecamatanPembawa'],
    //         'kodeKecamatanPemilik' => ['width' => 75, 'name' => 'kodeKecamatanPemilik'],
    //         'kodeKelurahanPembawa' => ['width' => 75, 'name' => 'kodeKelurahanPembawa'],
    //         'kodeKelurahanPemilik' => ['width' => 75, 'name' => 'kodeKelurahanPemilik'],
    //         'kodeKotaPembawa' => ['width' => 75, 'name' => 'kodeKotaPembawa'],
    //         'kodeKotaPemilik' => ['width' => 75, 'name' => 'kodeKotaPemilik'],
    //         'kodePosPembawa' => ['width' => 75, 'name' => 'kodePosPembawa'],
    //         'kodePosPemilik' => ['width' => 75, 'name' => 'kodePosPemilik'],
    //         'kodePropinsiPembawa' => ['width' => 75, 'name' => 'kodePropinsiPembawa'],
    //         'kodePropinsiPemilik' => ['width' => 75, 'name' => 'kodePropinsiPemilik'],
    //         'kodeTipeUnit' => ['width' => 75, 'name' => 'kodeTipeUnit'],
    //         'konfirmasiPekerjaanTambahan' => ['width' => 75, 'name' => 'konfirmasiPekerjaanTambahan'],
    //         'rekomendasiSA' => ['width' => 75, 'name' => 'rekomendasiSA'],
    //         'saranMekanik' => ['width' => 75, 'name' => 'saranMekanik'],
    //         'setUpPembayaran' => ['width' => 75, 'name' => 'setUpPembayaran'],
    //         'statusWorkOrder' => ['width' => 75, 'name' => 'statusWorkOrder'],
    //         'tahunMotor' => ['width' => 75, 'name' => 'tahunMotor'],
    //         'tanggalServis' => ['width' => 75, 'name' => 'tanggalServis'],
    //         'tipeComingCustomer' => ['width' => 75, 'name' => 'tipeComingCustomer'],
    //         'totalBiayaService' => ['width' => 75, 'name' => 'totalBiayaService'],
    //         'totalFRT' => ['width' => 75, 'name' => 'totalFRT'],
    //         'waktuPKB' => ['width' => 75, 'name' => 'waktuPKB'],
    //         'waktuPekerjaan' => ['width' => 75, 'name' => 'waktuPekerjaan'],
    //         'waktuPendaftaran' => ['width' => 75, 'name' => 'waktuPendaftaran'],
    //         'waktuSelesai' => ['width' => 75, 'name' => 'waktuSelesai'],
    //     ];
    // }


    public static function colGridPick()
    {
        return [
            'PINo'                          => ['width' => 100,'label' => 'No PI','name'  => 'PINo'],
            'PITgl'                         => ['width' => 120,'label' => 'Tgl PI','name'  => 'PITgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y']],
            'KodeTrans'                     => ['width' => 80,'label' => 'TC','name'  => 'KodeTrans'],
            'SupKode'                       => ['width' => 70,'label' => 'Supplier','name'  => 'SupKode'],
            'PISubTotal'                       => ['width' => 70,'label' => 'Sub Total','name'  => 'PISubTotal'],
            'PIDiscFinal'                       => ['width' => 70,'label' => 'Discount','name'  => 'PIDiscFinal'],
            'PITotal'                       => ['width' => 70,'label' => 'Total','name'  => 'PITotal'],

        ];
    }
    /**
     * {@inheritdoc}
     * @return TtpihdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtpihdQuery(get_called_class());
    }
}
