<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttpoit".
 *
 * @property string $PONo
 * @property int $POAuto
 * @property string $BrgKode
 * @property string $POQty
 * @property string $POHrgBeli Standard Cost
 * @property string $POHrgJual HargaOrderReal
 * @property string $PODiscount
 * @property string $POPajak
 * @property string $SONo
 * @property string $PSNo
 * @property string $PSQtyS
 */
class Ttpoit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttpoit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PONo', 'POAuto'], 'required'],
            [['POAuto'], 'integer'],
            [['POQty', 'POHrgBeli', 'POHrgJual', 'PODiscount', 'POPajak', 'PSQtyS'], 'number'],
            [['PONo', 'SONo', 'PSNo'], 'string', 'max' => 10],
            [['BrgKode'], 'string', 'max' => 20],
            [['PONo', 'POAuto'], 'unique', 'targetAttribute' => ['PONo', 'POAuto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PONo' => 'Po No',
            'POAuto' => 'Po Auto',
            'BrgKode' => 'Brg Kode',
            'POQty' => 'Po Qty',
            'POHrgBeli' => 'Po Hrg Beli',
            'POHrgJual' => 'Po Hrg Jual',
            'PODiscount' => 'Po Discount',
            'POPajak' => 'Po Pajak',
            'SONo' => 'So No',
            'PSNo' => 'Ps No',
            'PSQtyS' => 'Ps Qty S',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtpoitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtpoitQuery(get_called_class());
    }
}
