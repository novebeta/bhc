<?php

namespace abengkel\models;

/**
 * This is the ActiveQuery class for [[Tdlokasi]].
 *
 * @see Tdlokasi
 */
class TdlokasiQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdlokasi[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdlokasi|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
    public function LokasiInduk() {
        return $this->select('LokasiInduk as label,LokasiInduk as value' )
            ->groupBy( 'LokasiInduk' )
            ->orderBy( 'LokasiInduk' )
            ->asArray()
            ->all();
    }

    public function select2($value, $label = [ 'LokasiKode' ], $orderBy = [], $where = [ 'condition' => null, 'params' => [] ] , $allOption = false) {
        $option =
            $this->select( [ "CONCAT(" . implode( ",'||',", $label ) . ") as label", "$value as value" ] )
                ->orderBy( $orderBy )
                ->where( $where[ 'condition' ], $where[ 'params' ] )
                ->asArray()
                ->all();
        if ( $allOption ) {
            return array_merge( [ [ 'value' => '%', 'label' => 'Semua' ] ], $option );
        } else {
            return $option;
        }
    }

	public function combo() {
		return $this->select( [ "CONCAT(LokasiKode,' - ',LokasiNama) as label", "LokasiKode as value" ] )
		            ->orderBy( 'LokasiStatus, LokasiKode' )
		            ->asArray()
		            ->all();
	}
}
