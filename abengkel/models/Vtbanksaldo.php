<?php

namespace abengkel\models;

/**
 * This is the model class for table "vtbanksaldo".
 *
 * @property string $BankNo
 * @property string $BankTgl
 * @property string $BankMemo
 * @property string $BankDebet
 * @property string $BankKredit
 * @property string $BankJenis
 * @property string $BankCekNo
 * @property string $BankCekTempo
 * @property string $BankPerson
 * @property string $BankAC
 * @property string $UserID
 * @property string $BankKode
 * @property string $KodePerson
 * @property string $KodeTrans
 * @property int $BankSaldo
 * @property string $NoGL
 */
class Vtbanksaldo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vtbanksaldo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BankTgl', 'BankCekTempo'], 'safe'],
            [['BankDebet', 'BankKredit'], 'number'],
            [['BankSaldo'], 'integer'],
            [['BankNo'], 'string', 'max' => 12],
            [['BankMemo'], 'string', 'max' => 150],
            [['BankJenis'], 'string', 'max' => 20],
            [['BankCekNo', 'BankAC', 'BankKode'], 'string', 'max' => 25],
            [['BankPerson'], 'string', 'max' => 50],
            [['UserID', 'KodePerson'], 'string', 'max' => 15],
            [['KodeTrans'], 'string', 'max' => 4],
            [['NoGL'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'BankNo' => 'Bank No',
            'BankTgl' => 'Bank Tgl',
            'BankMemo' => 'Bank Memo',
            'BankDebet' => 'Bank Debet',
            'BankKredit' => 'Bank Kredit',
            'BankJenis' => 'Bank Jenis',
            'BankCekNo' => 'Bank Cek No',
            'BankCekTempo' => 'Bank Cek Tempo',
            'BankPerson' => 'Bank Person',
            'BankAC' => 'Bank Ac',
            'UserID' => 'User ID',
            'BankKode' => 'Bank Kode',
            'KodePerson' => 'Kode Person',
            'KodeTrans' => 'Kode Trans',
            'BankSaldo' => 'Bank Saldo',
            'NoGL' => 'No Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VtbanksaldoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VtbanksaldoQuery(get_called_class());
    }
}
