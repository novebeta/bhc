<?php

namespace abengkel\models;

/**
 * This is the model class for table "tdcustomer".
 *
 * @property string $MotorNoPolisi
 * @property string $MotorNoMesin
 * @property string $MotorNoRangka
 * @property string $CusKTP
 * @property string $CusNama
 * @property string $CusAlamat
 * @property string $CusRT
 * @property string $CusRW
 * @property string $CusProvinsi
 * @property string $CusKabupaten
 * @property string $CusKecamatan
 * @property string $CusKelurahan
 * @property string $CusKodePos
 * @property string $CusTelepon
 * @property string $CusSex
 * @property string $CusTempatLhr
 * @property string $CusTglLhr
 * @property string $CusAgama
 * @property string $CusPekerjaan
 * @property string $CusEmail
 * @property string $CusKeterangan
 * @property string $CusKK
 * @property string $CusStatus
 * @property string $MotorType
 * @property string $MotorWarna
 * @property string $MotorTahun
 * @property string $MotorNama
 * @property string $MotorKategori
 * @property string $MotorCC
 * @property string $TglAkhirServis
 * @property string $AHASSNomor
 * @property string $TglBeli
 * @property string $NoBukuServis
 * @property string $CusJenis
 */
class Tdcustomer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdcustomer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['MotorNoPolisi'], 'required'],
            [['CusTglLhr', 'TglAkhirServis', 'TglBeli'], 'safe'],
            [['MotorTahun', 'MotorCC'], 'number'],
            [['MotorNoPolisi', 'CusSex'], 'string', 'max' => 12],
            [['MotorNoMesin', 'MotorNoRangka'], 'string', 'max' => 25],
            [['CusKTP'], 'string', 'max' => 18],
            [['CusNama', 'CusKabupaten', 'CusKecamatan', 'CusKelurahan', 'CusEmail', 'MotorType'], 'string', 'max' => 50],
            [['CusAlamat'], 'string', 'max' => 75],
            [['CusRT', 'CusRW'], 'string', 'max' => 3],
            [['CusProvinsi', 'CusTelepon', 'CusTempatLhr'], 'string', 'max' => 30],
            [['CusKodePos'], 'string', 'max' => 7],
            [['CusAgama', 'MotorKategori'], 'string', 'max' => 15],
            [['CusPekerjaan', 'MotorWarna'], 'string', 'max' => 35],
            [['CusKeterangan','CusJenis', 'MotorNama'], 'string', 'max' => 100],
            [['CusKK'], 'string', 'max' => 16],
            [['CusStatus'], 'string', 'max' => 1],
            [['AHASSNomor'], 'string', 'max' => 10],
            [['NoBukuServis'], 'string', 'max' => 20],
            [['MotorNoPolisi'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'MotorNoPolisi' => 'Motor No Polisi',
            'MotorNoMesin' => 'Motor No Mesin',
            'MotorNoRangka' => 'Motor No Rangka',
            'CusKTP' => 'Cus Ktp',
            'CusNama' => 'Cus Nama',
            'CusAlamat' => 'Cus Alamat',
            'CusRT' => 'Cus Rt',
            'CusRW' => 'Cus Rw',
            'CusProvinsi' => 'Cus Provinsi',
            'CusKabupaten' => 'Cus Kabupaten',
            'CusKecamatan' => 'Cus Kecamatan',
            'CusKelurahan' => 'Cus Kelurahan',
            'CusKodePos' => 'Cus Kode Pos',
            'CusTelepon' => 'Cus Telepon',
            'CusSex' => 'Cus Sex',
            'CusTempatLhr' => 'Cus Tempat Lhr',
            'CusTglLhr' => 'Cus Tgl Lhr',
            'CusAgama' => 'Cus Agama',
            'CusPekerjaan' => 'Cus Pekerjaan',
            'CusEmail' => 'Cus Email',
            'CusKeterangan' => 'Cus Keterangan',
            'CusKK' => 'Cus Kk',
            'CusStatus' => 'Cus Status',
            'MotorType' => 'Motor Type',
            'MotorWarna' => 'Motor Warna',
            'MotorTahun' => 'Motor Tahun',
            'MotorNama' => 'Motor Nama',
            'MotorKategori' => 'Motor Kategori',
            'MotorCC' => 'Motor Cc',
            'TglAkhirServis' => 'Tgl Akhir Servis',
            'AHASSNomor' => 'Ahass Nomor',
            'TglBeli' => 'Tgl Beli',
            'NoBukuServis' => 'No Buku Servis',
            'CusJenis' => 'Jenis',
        ];
    }


	public static function colGrid()
	{
		return [
			'MotorNoPolisi'     => ['width' => 80,'label' => 'No Polisi ','name'  => 'MotorNoPolisi'],
			'MotorNoMesin'      => ['width' => 90,'label' => 'No Mesin','name'  => 'MotorNoMesin'],
			'MotorNama'			=> ['width' => 200,'label' => 'Motor','name'  => 'MotorNama'],
			'MotorWarna'			=> ['width' => 200,'label' => 'Motor Warna','name'  => 'MotorWarna'],
			'CusNama'           => ['width' => 150,'label' => 'Nama','name'  => 'CusNama'],
			'CusKabupaten'      => ['width' => 100,'label' => 'Kabupaten','name'  => 'CusKabupaten'],
			'CusKecamatan'      => ['width' => 100,'label' => 'Kecamatan','name'  => 'CusKecamatan'],
			'CusTelepon'         => ['width' => 100,'label' => 'Telepon','name'  => 'CusTelepon'],
			'MotorNoRangka'         => ['width' => 100,'label' => 'No Rangka','name'  => 'MotorNoRangka'],
			'MotorType'         => ['width' => 100,'label' => 'Motor Type','name'  => 'MotorType'],
			'MotorKategori'         => ['width' => 100,'label' => 'Motor Kategory','name'  => 'MotorKategori'],
    /*
			'AHASSNomor'         => ['width' => 100,'label' => 'AHASSNomor','name'  => 'AHASSNomor'],
    */
		];
	}

	public function getData($param = null){
		return $this->toArray();
	}

	public function getAhass() {
		return $this->hasOne( Tdahass::className(), [ 'AHASSNomor' => 'AHASSNomor' ] );
	}
    /**
     * {@inheritdoc}
     * @return TdcustomerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdcustomerQuery(get_called_class());
    }
}
