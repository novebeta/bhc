<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttcchd]].
 *
 * @see Ttcchd
 */
class TtcchdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttcchd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttcchd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
    public function ttcchdFillByNo() {
        return $this->select( new Expression( "CCMemo, CCNo, CCNoKlaim, CCTgl, CCTotalPart + CCTotalJasa AS CCTotal, CCTotalJasa, CCTotalPart, Cetak, NoGL, UserID "));
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]     = Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]     = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'CCNo' ]       = $post[ 'CCNo' ] ?? '--';
        $post[ 'CCNoBaru' ]   = $post[ 'CCNoView' ] ?? '--';
        $post[ 'CCTgl' ]      = date('Y-m-d H:i:s', strtotime($post[ 'CCTgl' ] ?? date( 'Y-m-d H:i:s' )));
        $post[ 'CCTotalPart'] = floatval( $post[ 'CCTotalPart' ] ?? 0 );
        $post[ 'CCTotalJasa'] = floatval( $post[ 'CCTotalJasa' ] ?? 0 );
        $post[ 'CCNoKlaim' ]  = $post[ 'CCNoKlaim' ] ?? '--';
        $post[ 'CCMemo' ]     = $post[ 'CCMemo' ] ?? '--';
        $post[ 'Cetak' ]      = $post[ 'Cetak' ] ?? 'Belum';
        $post[ 'NoGL' ]       = $post[ 'NoGL' ] ?? '--';
        try {
            General::cCmd( "SET @CCNo = ?;" )->bindParam( 1, $post[ 'CCNo' ] )->execute();
            General::cCmd( "CALL fcchd(?,@Status,@Keterangan,?,?,?,@CCNo,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'CCNoBaru' ] )
                ->bindParam( 6, $post[ 'CCTgl' ] )
                ->bindParam( 7, $post[ 'CCTotalPart' ] )
                ->bindParam( 8, $post[ 'CCTotalJasa' ] )
                ->bindParam( 9, $post[ 'CCNoKlaim' ] )
                ->bindParam( 10, $post[ 'CCMemo' ] )
                ->bindParam( 11, $post[ 'Cetak' ] )
                ->bindParam( 12, $post[ 'NoGL' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @CCNo,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'CCNo'       => $post[ 'CCNo' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $CCNo       = $exe[ '@CCNo' ] ?? $post[ 'CCNo' ];
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'CCNo'       => $CCNo,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
