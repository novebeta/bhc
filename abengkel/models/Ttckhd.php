<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttckhd".
 *
 * @property string $CKNo
 * @property string $CKTgl
 * @property string $CKTotalPart
 * @property string $CKTotalJasa
 * @property string $CKNoKlaim
 * @property string $CKMemo
 * @property string $NoGL
 * @property string $Cetak
 * @property string $UserID
 */
class Ttckhd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttckhd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CKNo'], 'required'],
            [['CKTgl'], 'safe'],
            [['CKTotalPart', 'CKTotalJasa'], 'number'],
            [['CKNo', 'NoGL'], 'string', 'max' => 10],
            [['CKNoKlaim'], 'string', 'max' => 35],
            [['CKMemo'], 'string', 'max' => 150],
            [['Cetak'], 'string', 'max' => 5],
            [['UserID'], 'string', 'max' => 15],
            [['CKNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CKNo'        => 'Ck No',
            'CKTgl'       => 'Ck Tgl',
            'CKTotalPart' => 'Ck Total Part',
            'CKTotalJasa' => 'Ck Total Jasa',
            'CKNoKlaim'   => 'Ck No Klaim',
            'CKMemo'      => 'Ck Memo',
            'NoGL'        => 'No Gl',
            'Cetak'       => 'Cetak',
            'UserID'      => 'User ID',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
        return [
			'CKNo'                   => ['width' => 80,'label' => 'No CK','name'  => 'CKNo'],
			'CKTgl'                  => ['width' => 110,'label' => 'Tgl CK','name'  => 'CKTgl','formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y H:i:s']],
			'CKTotalPart'            => ['width' => 105,'label' => 'Total Part','name'  => 'CKTotalPart', 'formatter' => 'number', 'align' => 'right'],
			'CKTotalJasa'            => ['width' => 105,'label' => 'Total Jasa','name'  => 'CKTotalJasa', 'formatter' => 'number', 'align' => 'right'],
            'CKNoKlaim'              => ['width' => 100,'label' => 'No Klaim','name'  => 'CKNoKlaim'],
            'CKMemo'                 => ['width' => 215,'label' => 'Keterangan','name'  => 'CKMemo'],
            'NoGL'                   => ['width' => 75,'label' => 'No GL','name'  => 'NoGL'],
            'Cetak'                  => ['width' => 60,'label' => 'No Cetak','name'  => 'Cetak'],
            'UserID'                 => ['width' => 80,'label' => 'User ID','name'  => 'UserID'],
        ];
    }

    public static function colGridPick()
    {
        return [
            'NO'                         => [ 'width' => 70, 'label' => 'No Servis', 'name' => 'NO' ],
            'SDTgl'                      => [ 'width' => 115, 'label' => 'Tgl Servis', 'name' => 'Tgl' , 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y H:i:s']],
            'MotorNama'                  => [ 'width' => 200, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
            'MotorNoPolisi'              => [ 'width' => 90, 'label' => 'No Polisi', 'name' => 'Kode' ],
            'CusNama'                    => [ 'width' => 150, 'label' => 'Nama Konsumen', 'name' => 'Nama' ],
            'SDTotalJasa'                => [ 'width' => 80, 'label' => 'Total Jasa', 'name' => 'SDTotalJasa' ],
            'SDTotalPart'                => [ 'width' => 80, 'label' => 'Total Part', 'name' => 'SDTotalPart' ],
            'SDTotal'                    => [ 'width' => 80, 'label' => 'Total', 'name' => 'SDTotal' ],
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtckhdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtckhdQuery(get_called_class());
    }
}
