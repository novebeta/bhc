<?php

namespace abengkel\models;

/**
 * This is the model class for table "vtkbk".
 *
 * @property string $KBKNo
 * @property string $KBKTgl
 * @property string $KBKLink
 * @property string $KBKBayar
 * @property string $Kode
 * @property string $Person
 * @property string $Memo
 * @property string $NoGL
 */
class Vtkbk extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vtkbk';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KBKTgl'], 'safe'],
            [['KBKBayar'], 'number'],
            [['KBKNo'], 'string', 'max' => 12],
            [['KBKLink'], 'string', 'max' => 18],
            [['Kode'], 'string', 'max' => 25],
            [['Person'], 'string', 'max' => 50],
            [['Memo'], 'string', 'max' => 150],
            [['NoGL'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'KBKNo' => 'Kbk No',
            'KBKTgl' => 'Kbk Tgl',
            'KBKLink' => 'Kbk Link',
            'KBKBayar' => 'Kbk Bayar',
            'Kode' => 'Kode',
            'Person' => 'Person',
            'Memo' => 'Memo',
            'NoGL' => 'No Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VtkbkQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VtkbkQuery(get_called_class());
    }
}
