<?php

namespace abengkel\models;

/**
 * This is the model class for table "tdareaahass".
 *
 * @property double $KODE_PROVINSI
 * @property string $PROVINSI
 * @property string $KODE_KARESIDENAN
 * @property string $KARESIDENAN
 * @property double $KODE_KABUPATEN
 * @property string $KABUPATEN
 * @property double $KODE_KECAMATAN
 * @property string $KECAMATAN
 * @property double $KODE_KELURAHAN
 * @property string $KELURAHAN
 */
class Tdareaahass extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdareaahass';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KODE_PROVINSI', 'KODE_KABUPATEN', 'KODE_KECAMATAN', 'KODE_KELURAHAN'], 'number'],
            [['PROVINSI', 'KODE_KARESIDENAN', 'KARESIDENAN', 'KABUPATEN', 'KECAMATAN', 'KELURAHAN'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'KODE_PROVINSI' => 'Kode Provinsi',
            'PROVINSI' => 'Provinsi',
            'KODE_KARESIDENAN' => 'Kode Karesidenan',
            'KARESIDENAN' => 'Karesidenan',
            'KODE_KABUPATEN' => 'Kode Kabupaten',
            'KABUPATEN' => 'Kabupaten',
            'KODE_KECAMATAN' => 'Kode Kecamatan',
            'KECAMATAN' => 'Kecamatan',
            'KODE_KELURAHAN' => 'Kode Kelurahan',
            'KELURAHAN' => 'Kelurahan',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TdareaahassQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdareaahassQuery(get_called_class());
    }
}
