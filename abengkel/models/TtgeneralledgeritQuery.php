<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttgeneralledgerit]].
 *
 * @see Ttgeneralledgerit
 */
class TtgeneralledgeritQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttgeneralledgerit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttgeneralledgerit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
    public function callSP( $post ) {
        $post[ 'UserID' ]       = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]    = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]      = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'NoGL' ]         = $post[ 'NoGL' ] ?? '--';
        $post[ 'TglGL' ]        = $post[ 'TglGL' ] ?? '1900-01-01';
        $post[ 'GLAutoN' ]      = floatval( $post[ 'GLAutoN' ] ?? 0 );
        $post[ 'NoAccount' ]    = $post[ 'NoAccount' ] ?? '--';
        $post[ 'KeteranganGL' ] = $post[ 'KeteranganGL' ] ?? '--';
        $post[ 'DebetGL' ]      = floatval( $post[ 'DebetGL' ] ?? 0 );
        $post[ 'KreditGL' ]     = floatval( $post[ 'KreditGL' ] ?? 0 );
        $post[ 'NoGLOLD' ]      = $post[ 'NoGLOLD' ] ?? '--';
        $post[ 'TglGLOLD' ]     = $post[ 'TglGLOLD' ] ?? '1900-01-01';
        $post[ 'GLAutoNOLD' ]   = floatval( $post[ 'GLAutoNOLD' ] ?? 0 );
        $post[ 'NoAccountOLD' ] = $post[ 'NoAccountOLD' ] ?? '--';
        try {
            General::cCmd( "CALL fgeneralledgerit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'NoGL' ] )
                ->bindParam( 6, $post[ 'TglGL' ] )
                ->bindParam( 7, $post[ 'GLAutoN' ] )
                ->bindParam( 8, $post[ 'NoAccount' ] )
                ->bindParam( 9, $post[ 'KeteranganGL' ] )
                ->bindParam( 10, $post[ 'DebetGL' ] )
                ->bindParam( 11, $post[ 'KreditGL' ] )
                ->bindParam( 12, $post[ 'NoGLOLD' ] )
                ->bindParam( 13, $post[ 'TglGLOLD' ] )
                ->bindParam( 14, $post[ 'GLAutoNOLD' ] )
                ->bindParam( 15, $post[ 'NoAccountOLD' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
        ];
    }
}
