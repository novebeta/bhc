<?php

namespace abengkel\models;

/**
 * This is the model class for table "tvcustomer".
 *
 * @property string $MotorNoPolisi
 * @property string $MotorNoMesin
 * @property string $MotorNoRangka
 * @property string $CusKTP
 * @property string $CusNama
 * @property string $CusAlamat
 * @property string $CusRT
 * @property string $CusRW
 * @property string $CusProvinsi
 * @property string $CusKabupaten
 * @property string $CusKecamatan
 * @property string $CusKelurahan
 * @property string $CusKodePos
 * @property string $CusTelepon
 * @property string $CusSex
 * @property string $CusTempatLhr
 * @property string $CusTglLhr
 * @property string $CusAgama
 * @property string $CusPekerjaan
 * @property string $CusEmail
 * @property string $CusKeterangan
 * @property string $CusStatus
 * @property string $CusKK
 * @property string $MotorType
 * @property string $MotorWarna
 * @property string $MotorTahun
 * @property string $MotorNama
 * @property string $MotorKategori
 * @property string $MotorCC
 * @property string $TglAkhirServis
 * @property string $InsStatus
 * @property string $TglBeli
 * @property string $NoBukuServis
 */
class Tvcustomer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tvcustomer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['MotorNoPolisi', 'MotorNoMesin', 'MotorNoRangka', 'CusKTP'], 'required'],
            [['CusTglLhr', 'TglAkhirServis', 'TglBeli'], 'safe'],
            [['MotorTahun', 'MotorCC'], 'number'],
            [['MotorNoPolisi', 'MotorNoMesin', 'CusSex'], 'string', 'max' => 12],
            [['MotorNoRangka', 'MotorType'], 'string', 'max' => 25],
            [['CusKTP'], 'string', 'max' => 18],
            [['CusNama', 'CusKabupaten', 'CusKecamatan', 'CusKelurahan', 'CusEmail'], 'string', 'max' => 50],
            [['CusAlamat'], 'string', 'max' => 75],
            [['CusRT', 'CusRW'], 'string', 'max' => 3],
            [['CusProvinsi', 'CusTelepon', 'CusTempatLhr'], 'string', 'max' => 30],
            [['CusKodePos'], 'string', 'max' => 7],
            [['CusAgama', 'MotorKategori'], 'string', 'max' => 15],
            [['CusPekerjaan', 'MotorWarna'], 'string', 'max' => 35],
            [['CusKeterangan', 'MotorNama'], 'string', 'max' => 100],
            [['CusStatus'], 'string', 'max' => 1],
            [['CusKK'], 'string', 'max' => 16],
            [['InsStatus'], 'string', 'max' => 5],
            [['NoBukuServis'], 'string', 'max' => 20],
            [['MotorNoPolisi', 'MotorNoMesin', 'MotorNoRangka', 'CusKTP'], 'unique', 'targetAttribute' => ['MotorNoPolisi', 'MotorNoMesin', 'MotorNoRangka', 'CusKTP']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'MotorNoPolisi' => 'Motor No Polisi',
            'MotorNoMesin' => 'Motor No Mesin',
            'MotorNoRangka' => 'Motor No Rangka',
            'CusKTP' => 'Cus Ktp',
            'CusNama' => 'Cus Nama',
            'CusAlamat' => 'Cus Alamat',
            'CusRT' => 'Cus Rt',
            'CusRW' => 'Cus Rw',
            'CusProvinsi' => 'Cus Provinsi',
            'CusKabupaten' => 'Cus Kabupaten',
            'CusKecamatan' => 'Cus Kecamatan',
            'CusKelurahan' => 'Cus Kelurahan',
            'CusKodePos' => 'Cus Kode Pos',
            'CusTelepon' => 'Cus Telepon',
            'CusSex' => 'Cus Sex',
            'CusTempatLhr' => 'Cus Tempat Lhr',
            'CusTglLhr' => 'Cus Tgl Lhr',
            'CusAgama' => 'Cus Agama',
            'CusPekerjaan' => 'Cus Pekerjaan',
            'CusEmail' => 'Cus Email',
            'CusKeterangan' => 'Cus Keterangan',
            'CusStatus' => 'Cus Status',
            'CusKK' => 'Cus Kk',
            'MotorType' => 'Motor Type',
            'MotorWarna' => 'Motor Warna',
            'MotorTahun' => 'Motor Tahun',
            'MotorNama' => 'Motor Nama',
            'MotorKategori' => 'Motor Kategori',
            'MotorCC' => 'Motor Cc',
            'TglAkhirServis' => 'Tgl Akhir Servis',
            'InsStatus' => 'Ins Status',
            'TglBeli' => 'Tgl Beli',
            'NoBukuServis' => 'No Buku Servis',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TvcustomerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TvcustomerQuery(get_called_class());
    }
}
