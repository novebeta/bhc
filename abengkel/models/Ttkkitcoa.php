<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttkkitcoa".
 *
 * @property string $KKNo
 * @property int $KKAutoN
 * @property string $NoAccount
 * @property string $KKBayar
 * @property string $KKKeterangan
 */
class Ttkkitcoa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttkkitcoa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KKNo', 'KKAutoN', 'NoAccount'], 'required'],
            [['KKAutoN'], 'integer'],
            [['KKBayar'], 'number'],
            [['KKNo'], 'string', 'max' => 12],
            [['NoAccount'], 'string', 'max' => 10],
            [['KKKeterangan'], 'string', 'max' => 150],
            [['KKNo', 'KKAutoN', 'NoAccount'], 'unique', 'targetAttribute' => ['KKNo', 'KKAutoN', 'NoAccount']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'KKNo' => 'Kk No',
            'KKAutoN' => 'Kk Auto N',
            'NoAccount' => 'No Account',
            'KKBayar' => 'Kk Bayar',
            'KKKeterangan' => 'Kk Keterangan',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtkkitcoaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtkkitcoaQuery(get_called_class());
    }
}
