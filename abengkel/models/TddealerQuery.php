<?php

namespace abengkel\models;

/**
 * This is the ActiveQuery class for [[Tddealer]].
 *
 * @see Tddealer
 */
class TddealerQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tddealer[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tddealer|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
    
	public function combo() {
        return $this->select( [ "CONCAT(DealerKode,' - ',DealerNama) as label", "DealerKode as value" ] )
            ->orderBy( 'DealerStatus, DealerKode' )
            ->asArray()
            ->all();
    }

    public function select2($value, $label = [ 'DealerNama' ] , $orderBy = ['DealerNama'], $where = [ 'condition' => null, 'params' => [] ], $allOption = false ) {
        $option =
            $this->select( [ "CONCAT(" . implode( ",'||',", $label ) . ") as label", "$value as value" ] )
                ->orderBy( $orderBy )
                ->where( $where[ 'condition' ], $where[ 'params' ] )
                ->asArray()
                ->all();
        if ( $allOption ) {
            return array_merge( [ [ 'value' => '%', 'label' => 'Semua' ] ], $option );
        } else {
            return $option;
        }
    }
}
