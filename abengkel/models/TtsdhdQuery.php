<?php

namespace abengkel\models;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
use abengkel\components\Menu;
/**
 * This is the ActiveQuery class for [[Ttsdhd]].
 *
 * @see Ttsdhd
 */
class TtsdhdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttsdhd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttsdhd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function select2( $value, $label, $orderBy, $where = [ 'condition' => null, 'params' => [] ] , $allOption = false) {
        $option =
            $this->select( [ "CONCAT(" . implode( ",'||',", $label ) . ") as label", "$value as value" ] )
                ->orderBy( $orderBy )
                ->groupBy( $value )
                ->where( $where[ 'condition' ], $where[ 'params' ] )
                ->asArray()
                ->all();
        if ( $allOption ) {
            return array_merge( [ [ 'value' => '%', 'label' => 'Semua' ] ], $option );
        } else {
            return $option;
        }
    }

    public function ttsdhdFillByNo() {
        return $this->select( new Expression( "ttsdhd.SDNo, ttsdhd.SDTgl, ttsdhd.SDJamMasuk, ttsdhd.SDNoUrut, ttsdhd.SDStatus, ttsdhd.SDKmSekarang, ttsdhd.SDKmBerikut, ttsdhd.SDKeluhan, ttsdhd.MotorNoPolisi, ttsdhd.KarKode, ttsdhd.LokasiKode, 
        ttsdhd.SDJamDikerjakan, ttsdhd.SDJamSelesai, ttsdhd.SDTotalWaktu, ttsdhd.SDTotalJasa, ttsdhd.SDTotalQty, ttsdhd.SDTotalPart, ttsdhd.SDTotalGratis, ttsdhd.SDTotalNonGratis, ttsdhd.SDDiscFinal, ttsdhd.SDTotalPajak, 
        ttsdhd.SDTotalBiaya, ttsdhd.SVNo, ttsdhd.SVTgl, ttsdhd.UserID, IFNULL(tdkaryawan.KarNama, '--') AS KarNama, IFNULL(tdcustomer.CusNama, '--') AS CusNama, IFNULL(tdcustomer.MotorType, '--') AS MotorType, tdcustomer.CusJenis,
        IFNULL(tdcustomer.MotorWarna, '--') AS MotorWarna, IFNULL(tdmotortype.MotorNama, '--') AS MotorNama, fWaktu(ttsdhd.SDTotalWaktu) AS SDWaktu, tdcustomer.CusTelepon, tdcustomer.MotorNoMesin, 
        tdcustomer.MotorNoRangka, ttsdhd.SDPembawaMotor, ttsdhd.SDHubunganPembawa, ttsdhd.SDAlasanServis, ttsdhd.SDKasKeluar, ttsdhd.NoGLSD, ttsdhd.NoGLSV, ttsdhd.KodeTrans, ttsdhd.PKBNo, ttsdhd.PosKode, 
        ttsdhd.Cetak, ttsdhd.PrgNama, ttsdhd.SDKeterangan, ttsdhd.SDUangMuka, ttsdhd.SDJenis, ttsdhd.SDTOP, ttsdhd.SDDurasi, ttsdhd.SENo, ttsdhd.ASS, ttsdhd.SDTerbayar, ttsdhd.KlaimKPB, ttsdhd.KlaimC2, ttsdhd.ZnoWorkOrder, 
        ttsdhd.KarKodeSA, ttsdhd.NPWP, tdcustomer.CusPeringkat"))
                         ->join( "LEFT OUTER JOIN", "tdkaryawan", "ttsdhd.KarKode = tdkaryawan.KarKode ")
                         ->join( "LEFT OUTER JOIN", "tdcustomer", "ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi ")
                         ->join( "LEFT OUTER JOIN", "tdmotortype", "tdcustomer.MotorType = tdmotortype.MotorType" )
                         ->groupBy("ttsdhd.SDNo, ttsdhd.ZnoWorkOrder, ttsdhd.KarKodeSA, ttsdhd.NPWP, tdcustomer.CusPeringkat");
    }

    public function getListAntrian($MyPosKodeKu) {
        return \Yii::$app->db->createCommand("
        SELECT a.MotorNoPolisi, A.SDNo, A.SDNoUrut, CONCAT_WS('', '/',CONVERT(A.SDTotalWaktu , CHAR(10))) AS SDTotalWaktu,
           IFNULL(WAKTUTUNGGU, 0) AS WaktuTunggu, IFNULL(KarNama,'--') AS KarNama, SDPembawaMotor, ''  AS ProgressBar,
           IFNULL(TIMESTAMPDIFF(MINUTE, A.SDJamMasuk, NOW()),0) AS WTunggu,
            CASE
              WHEN SDStatus = 'Menunggu'  THEN 4
              WHEN SDStatus = 'Selesai'   THEN 1
              WHEN SDStatus = 'Diproses'  THEN 2
              WHEN SDStatus = 'Istirahat' THEN 3
              ELSE 10
            END AS Urutan,
            CASE
              WHEN STATUS IS NULL THEN 'Menunggu'
              WHEN STATUS = ''    THEN 'Diproses'
              ELSE STATUS
            END AS StatusAntrian,
            CASE
              WHEN STATUS = 'Selesai'  THEN 300
              WHEN STATUS <> 'Selesai' AND WAKTUTUNGGU >= SDTOTALWAKTU THEN 300
              WHEN STATUS <> 'Selesai' AND WAKTUTUNGGU IS NULL THEN 0
              ELSE ROUND(IFNULL(IFNULL(WAKTUTUNGGU, 0) / SDTOTALWAKTU * 300, 0))
            END AS Durasi,
            CASE
              WHEN STATUS = 'Selesai'  THEN 100
              WHEN STATUS <> 'Selesai' AND WAKTUTUNGGU >= SDTOTALWAKTU THEN 100
              WHEN STATUS <> 'Selesai' AND WAKTUTUNGGU IS NULL THEN 0
              ELSE ROUND(IFNULL(IFNULL(WAKTUTUNGGU, 0) / SDTOTALWAKTU * 100, 0))
            END AS Persen ,
            CASE
              WHEN STATUS = 'Selesai'  THEN 100
              WHEN STATUS <> 'Selesai' AND WAKTUTUNGGU >= SDTOTALWAKTU THEN 100
              WHEN STATUS <> 'Selesai' AND WAKTUTUNGGU IS NULL THEN 0
              ELSE ROUND(IFNULL(IFNULL(WAKTUTUNGGU, 0) / SDTOTALWAKTU * 100, 0))
            END AS PersenProgres
          FROM ttsdhd a
            LEFT OUTER JOIN
                (SELECT  A.SDNO, WAKTUTUNGGU, STATUS, C.KARNAMA FROM
                (SELECT SDNO, MAX(NOMOR) AS NOMOR, SUM(WAKTUTUNGGU) AS WAKTUTUNGGU FROM
                (SELECT b.sdtotalwaktu, a.sdno, nomor,
                    CASE
                      WHEN TIMESTAMPDIFF(DAY, FINISH, '2100-12-31') = 0
                      THEN TIMESTAMPDIFF(MINUTE, START, NOW())
                      ELSE TIMESTAMPDIFF(MINUTE, START, FINISH)
                    END AS WAKTUTUNGGU
                   FROM ttsdittime A INNER JOIN TTSDHD B ON A.SDNO = B.SDNO) A
                GROUP BY SDNo) A
                INNER JOIN TTSDITTIME B ON A.SDNO = B.SDNO AND A.NOMOR = B.Nomor
                LEFT OUTER JOIN TDKARYAWAN C ON B.KARKODE = C.KARKODE) b ON a.sdno = b.sdno
          WHERE (SVNo = '--') AND DATE(SDTgl) = CURRENT_DATE AND PosKode = :PosKode
        ORDER BY urutan, sdnourut, SDNO LIMIT 0, 15
        ",[':PosKode' => $MyPosKodeKu])->queryAll();

    }

    public function callSP( $post ) {
        $post[ 'UserID' ]             = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]          = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]            = $post[ 'PosKode' ] ?? Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'SDNo' ]               = $post[ 'SDNo' ] ?? '--';
        $post[ 'SDNoBaru' ]           = $post[ 'SDNoView' ] ?? '--';
        $post[ 'SVNo' ]               = $post[ 'SVNo' ] ?? '--';
        $post[ 'SENo' ]               = $post[ 'SENo' ] ?? '--';
        $post[ 'NoGLSD' ]             = $post[ 'NoGLSD' ] ?? '--';
        $post[ 'NoGLSV' ]             = $post[ 'NoGLSV' ] ?? '--';
        $post[ 'SDTgl' ]              = date('Y-m-d H:i:s', strtotime($post[ 'SDTgl' ] ?? date( 'Y-m-d H:i:s' )));
        $post[ 'SVTgl' ]              = date('Y-m-d H:i:s', strtotime($post[ 'SVTgl' ] ?? date( 'Y-m-d H:i:s' )));
        $post[ 'KodeTrans' ]          = $post[ 'KodeTrans' ] ?? '--';
        $post[ 'ASS' ]                = $post[ 'ASS' ] ?? null;
        $post[ 'LokasiKode' ]         = $post[ 'LokasiKode' ] ?? '--';
        $post[ 'KarKode' ]            = $post[ 'KarKode' ] ?? '--';
        $post[ 'PrgNama' ]            = $post[ 'PrgNama' ] ?? '--';
        $post[ 'PKBNo' ]              = $post[ 'PKBNo' ] ?? '--';
        $post[ 'MotorNoPolisi' ]      = $post[ 'MotorNoPolisi' ] ?? '--';
        $post[ 'SDNoUrut' ]           = $post[ 'SDNoUrut' ] ?? '--';
        $post[ 'SDJenis' ]            = $post[ 'SDJenis' ] ?? 'Tunai';
        $post[ 'SDTOP' ]              = floatval( $post[ 'SDTOP' ] ?? 0 );
        $post[ 'SDDurasi' ]           = floatval( $post[ 'SDDurasi' ] ?? 0 );
        $post[ 'SDJamMasuk' ]         = $post[ 'SDJamMasuk' ] ?? null;
        $post[ 'SDJamSelesai' ]       = $post[ 'SDJamSelesai' ] ?? null;
        $post[ 'SDJamDikerjakan' ]    = $post[ 'SDJamDikerjakan' ] ?? null;
        $post[ 'SDTotalWaktu' ]       = floatval( $post[ 'SDTotalWaktu' ] ?? 0 );
        $post[ 'SDStatus' ]           = $post[ 'SDStatus' ] ?? null;
        $post[ 'SDKmSekarang' ]       = floatval( $post[ 'SDKmSekarang' ] ?? 0 );
        $post[ 'SDKmBerikut' ]        = floatval( $post[ 'SDKmBerikut' ] ?? 0 );
        $post[ 'SDKeluhan' ]          = $post[ 'SDKeluhan' ] ?? '--';
        $post[ 'SDKeterangan' ]       = $post[ 'SDKeterangan' ] ?? '--';
        $post[ 'SDPembawaMotor' ]     = $post[ 'SDPembawaMotor' ] ?? '--';
        $post[ 'SDHubunganPembawa' ]  = $post[ 'SDHubunganPembawa' ] ?? 'Teman';
        $post[ 'SDAlasanServis' ]     = $post[ 'SDAlasanServis' ] ?? 'Ingat Sendiri';
        $post[ 'SDTotalJasa' ]        = floatval( $post[ 'SDTotalJasa' ] ?? 0 );
        $post[ 'SDTotalQty' ]         = floatval( $post[ 'SDTotalQty' ] ?? 0 );
        $post[ 'SDTotalPart' ]        = floatval( $post[ 'SDTotalPart' ] ?? 0 );
        $post[ 'SDTotalGratis' ]      = floatval( $post[ 'SDTotalGratis' ] ?? 0 );
        $post[ 'SDTotalNonGratis' ]   = floatval( $post[ 'SDTotalNonGratis' ] ?? 0 );
        $post[ 'SDDiscFinal' ]        = floatval( $post[ 'SDDiscFinal' ] ?? 0 );
        $post[ 'SDTotalPajak' ]       = floatval( $post[ 'SDTotalPajak' ] ?? 0 );
        $post[ 'SDUangMuka' ]         = floatval( $post[ 'SDUangMuka' ] ?? 0 );
        $post[ 'SDTotalBiaya' ]       = floatval( $post[ 'SDTotalBiaya' ] ?? 0 );
        $post[ 'SDKasKeluar' ]        = floatval( $post[ 'SDKasKeluar' ] ?? 0 );
        $post[ 'SDTerbayar' ]         = floatval( $post[ 'SDTerbayar' ] ?? 0 );
        $post[ 'KlaimKPB' ]           = $post[ 'KlaimKPB' ] ?? 'Belum';
        $post[ 'KlaimC2' ]            = $post[ 'KlaimC2' ] ?? 'Belum';
        $post[ 'Cetak' ]              = $post[ 'Cetak' ] ?? 'Belum';
        $post[ 'ZnoWorkOrder' ]              = $post[ 'ZnoWorkOrder' ] ?? '--';
        $post[ 'KarKodeSA' ]              = $post[ 'KarKodeSA' ] ?? '--';
        $post[ 'NPWP' ]              = $post[ 'NPWP' ] ?? '--';
        try {
            General::cCmd( "SET @SDNo = ?;" )->bindParam( 1, $post[ 'SDNo' ] )->execute();
            General::cCmd( "CALL fsdhd(?,@Status,@Keterangan,?,?,?,@SDNo,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'SDNoBaru' ] )
                ->bindParam( 6, $post[ 'SVNo' ] )
                ->bindParam( 7, $post[ 'SENo' ] )
                ->bindParam( 8, $post[ 'NoGLSD' ] )
                ->bindParam( 9, $post[ 'NoGLSV' ] )
                ->bindParam( 10, $post[ 'SDTgl' ] )
                ->bindParam( 11, $post[ 'SVTgl' ] )
                ->bindParam( 12, $post[ 'KodeTrans' ] )
                ->bindParam( 13, $post[ 'ASS' ] )
                ->bindParam( 14, $post[ 'LokasiKode' ] )
                ->bindParam( 15, $post[ 'KarKode' ] )
                ->bindParam( 16, $post[ 'PrgNama' ] )
                ->bindParam( 17, $post[ 'PKBNo' ] )
                ->bindParam( 18, $post[ 'MotorNoPolisi' ] )
                ->bindParam( 19, $post[ 'SDNoUrut' ] )
                ->bindParam( 20, $post[ 'SDJenis' ] )
                ->bindParam( 21, $post[ 'SDTOP' ] )
                ->bindParam( 22, $post[ 'SDDurasi' ] )
                ->bindParam( 23, $post[ 'SDJamMasuk' ] )
                ->bindParam( 24, $post[ 'SDJamSelesai' ] )
                ->bindParam( 25, $post[ 'SDJamDikerjakan' ] )
                ->bindParam( 26, $post[ 'SDTotalWaktu' ] )
                ->bindParam( 27, $post[ 'SDStatus' ] )
                ->bindParam( 28, $post[ 'SDKmSekarang' ] )
                ->bindParam( 29, $post[ 'SDKmBerikut' ] )
                ->bindParam( 30, $post[ 'SDKeluhan' ] )
                ->bindParam( 31, $post[ 'SDKeterangan' ] )
                ->bindParam( 32, $post[ 'SDPembawaMotor' ] )
                ->bindParam( 33, $post[ 'SDHubunganPembawa' ] )
                ->bindParam( 34, $post[ 'SDAlasanServis' ] )
                ->bindParam( 35, $post[ 'SDTotalJasa' ] )
                ->bindParam( 36, $post[ 'SDTotalQty' ] )
                ->bindParam( 37, $post[ 'SDTotalPart' ] )
                ->bindParam( 38, $post[ 'SDTotalGratis' ] )
                ->bindParam( 39, $post[ 'SDTotalNonGratis' ] )
                ->bindParam( 40, $post[ 'SDDiscFinal' ] )
                ->bindParam( 41, $post[ 'SDTotalPajak' ] )
                ->bindParam( 42, $post[ 'SDUangMuka' ] )
                ->bindParam( 43, $post[ 'SDTotalBiaya' ] )
                ->bindParam( 44, $post[ 'SDKasKeluar' ] )
                ->bindParam( 45, $post[ 'SDTerbayar' ] )
                ->bindParam( 46, $post[ 'KlaimKPB' ] )
                ->bindParam( 47, $post[ 'KlaimC2' ] )
                ->bindParam( 48, $post[ 'Cetak' ] )
                ->bindParam( 49, $post[ 'ZnoWorkOrder' ] )
                ->bindParam( 50, $post[ 'KarKodeSA' ] )
                ->bindParam( 51, $post[ 'NPWP' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @SDNo,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'SDNo'       => $post[ 'SDNo' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $SDNo       = $exe[ '@SDNo' ] ?? $post[ 'SDNo' ];
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'SDNo'       => $SDNo,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }

}
