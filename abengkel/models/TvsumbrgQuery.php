<?php

namespace abengkel\models;

/**
 * This is the ActiveQuery class for [[Tvsumbrg]].
 *
 * @see Tvsumbrg
 */
class TvsumbrgQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tvsumbrg[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tvsumbrg|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
