<?php

namespace abengkel\models;

/**
 * This is the model class for table "vhutangbayar".
 *
 * @property string $HPLink
 * @property string $NoAccount
 * @property string $SumDebetGL
 */
class Vhutangbayar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vhutangbayar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['HPLink', 'NoAccount'], 'required'],
            [['SumDebetGL'], 'number'],
            [['HPLink'], 'string', 'max' => 18],
            [['NoAccount'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'HPLink' => 'Hp Link',
            'NoAccount' => 'No Account',
            'SumDebetGL' => 'Sum Debet Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VhutangbayarQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VhutangbayarQuery(get_called_class());
    }
}
