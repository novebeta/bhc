<?php
namespace abengkel\models;
/**
 * This is the model class for table "tdkaryawan".
 *
 * @property string $KarKode
 * @property string $KarNama
 * @property string $KarAlamat
 * @property string $KarKTP
 * @property string $KarRT
 * @property string $KarRW
 * @property string $KarProvinsi
 * @property string $KarKabupaten
 * @property string $KarKecamatan
 * @property string $KarKelurahan
 * @property string $KarKodePos
 * @property string $KarTelepon
 * @property string $KarEmail
 * @property string $KarSex
 * @property string $KarTempatLhr
 * @property string $KarTglLhr
 * @property string $KarTglMasuk
 * @property string $KarTglKeluar
 * @property string $KarAgama
 * @property string $KarJabatan
 * @property string $KarStatus
 * @property string $KarPassword
 */
class Tdkaryawan extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'tdkaryawan';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'KarKode' ], 'required' ],
			[ [ 'KarTglLhr', 'KarTglMasuk', 'KarTglKeluar' ], 'safe' ],
			[ [ 'KarKode' ], 'string', 'max' => 10 ],
			[ [ 'KarNama', 'KarPassword' ], 'string', 'max' => 35 ],
			[ [ 'KarAlamat' ], 'string', 'max' => 75 ],
			[ [ 'KarKTP', 'KarJabatan' ], 'string', 'max' => 25 ],
			[ [ 'KarRT', 'KarRW' ], 'string', 'max' => 3 ],
			[ [ 'KarProvinsi', 'KarTelepon', 'KarTempatLhr' ], 'string', 'max' => 30 ],
			[ [ 'KarKabupaten', 'KarKecamatan', 'KarKelurahan', 'KarEmail' ], 'string', 'max' => 50 ],
			[ [ 'KarKodePos' ], 'string', 'max' => 7 ],
			[ [ 'KarSex' ], 'string', 'max' => 12 ],
			[ [ 'KarAgama' ], 'string', 'max' => 15 ],
			[ [ 'KarStatus' ], 'string', 'max' => 1 ],
			[ [ 'KarKode' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'KarKode'      => 'Kar Kode',
			'KarNama'      => 'Kar Nama',
			'KarAlamat'    => 'Kar Alamat',
			'KarKTP'       => 'Kar Ktp',
			'KarRT'        => 'Kar Rt',
			'KarRW'        => 'Kar Rw',
			'KarProvinsi'  => 'Kar Provinsi',
			'KarKabupaten' => 'Kar Kabupaten',
			'KarKecamatan' => 'Kar Kecamatan',
			'KarKelurahan' => 'Kar Kelurahan',
			'KarKodePos'   => 'Kar Kode Pos',
			'KarTelepon'   => 'Kar Telepon',
			'KarEmail'     => 'Kar Email',
			'KarSex'       => 'Kar Sex',
			'KarTempatLhr' => 'Kar Tempat Lhr',
			'KarTglLhr'    => 'Kar Tgl Lhr',
			'KarTglMasuk'  => 'Kar Tgl Masuk',
			'KarTglKeluar' => 'Kar Tgl Keluar',
			'KarAgama'     => 'Kar Agama',
			'KarJabatan'   => 'Kar Jabatan',
			'KarStatus'    => 'Kar Status',
			'KarPassword'  => 'Kar Password',
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public static function colGrid() {
		return [
			'KarKode'      => ['width' => 100,'label' => 'Kode','name'  => 'KarKode'],
			'KarNama'      => ['width' => 225,'label' => 'Nama','name'  => 'KarNama'],
			'KarSex'       => ['width' => 80,'label' => 'Sex','name'  => 'KarSex'],
			'KarJabatan'   =>  ['width' => 120,'label' => 'Jabatan','name'  => 'KarJabatan'],
			'KarAlamat'    => ['width' => 230,'label' => 'Alamat','name'  => 'KarAlamat'],
			'KarTelepon'   =>  ['width' => 120,'label' => 'Telepon','name'  => 'KarTelepon'],
			'KarStatus'    => ['width' => 55,'label' => 'Status','name'  => 'KarStatus'],
		];
	}
	/**
	 * {@inheritdoc}
	 * @return TdkaryawanQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TdkaryawanQuery( get_called_class() );
	}
}
