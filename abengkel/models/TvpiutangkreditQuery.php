<?php

namespace abengkel\models;

/**
 * This is the ActiveQuery class for [[Tvpiutangkredit]].
 *
 * @see Tvpiutangkredit
 */
class TvpiutangkreditQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tvpiutangkredit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tvpiutangkredit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
