<?php

namespace abengkel\models;

/**
 * This is the model class for table "tdlokasi".
 *
 * @property string $LokasiKode
 * @property string $LokasiNama
 * @property string $KarKode
 * @property string $LokasiAlamat
 * @property string $LokasiStatus
 * @property string $LokasiTelepon
 * @property string $LokasiNomor
 * @property string $LokasiJenis
 * @property string $LokasiInduk
 */
class Tdlokasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdlokasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['LokasiKode'], 'required'],
            [['LokasiKode', 'LokasiInduk'], 'string', 'max' => 15],
            [['LokasiNama'], 'string', 'max' => 35],
            [['KarKode', 'LokasiJenis'], 'string', 'max' => 10],
            [['LokasiAlamat'], 'string', 'max' => 75],
            [['LokasiStatus'], 'string', 'max' => 1],
            [['LokasiTelepon'], 'string', 'max' => 30],
            [['LokasiNomor'], 'string', 'max' => 3],
            [['LokasiKode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'LokasiKode'    => 'Lokasi Kode',
            'LokasiNama'    => 'Lokasi Nama',
            'KarKode'       => 'Kar Kode',
            'LokasiAlamat'  => 'Lokasi Alamat',
            'LokasiStatus'  => 'Lokasi Status',
            'LokasiTelepon' => 'Lokasi Telepon',
            'LokasiNomor'   => 'Lokasi Nomor',
            'LokasiJenis'   => 'Lokasi Jenis',
            'LokasiInduk'   => 'Lokasi Induk',
        ];
    }

	/**
	 * {@inheritdoc}
	 */
	public static function colGrid()
	{
		return [
			'LokasiKode'         => ['width' => 120,'label' => 'Kode Lokasi','name'  => 'LokasiKode'],
			'LokasiNama'         => ['width' => 170,'label' => 'Nama Lokasi','name'  => 'LokasiNama'],
			'LokasiNomor'        => ['width' => 60,'label' => 'Nomor','name'  => 'LokasiNomor'],
			'Tdkaryawan.KarKode'            => ['width' => 90,'label' => 'Karyawan','name'  => 'KarKode'],
			'LokasiJenis'        => ['width' => 90,'label' => 'Jenis','name'  => 'LokasiJenis'],
			'LokasiInduk'        => ['width' => 100,'label' => 'Induk','name'  => 'LokasiInduk'],
			'LokasiAlamat'       => ['width' => 150,'label' => 'Alamat','name'  => 'LokasiAlamat'],
			'LokasiTelepon'      => ['width' => 90,'label' => 'Telepon','name'  => 'LokasiTelepon'],
			'LokasiStatus'       => ['width' => 60,'label' => 'Status','name'  => 'LokasiStatus'],
		];
	}

	public function getKaryawan() {
		return $this->hasOne( Tdkaryawan::className(), [ 'KarKode' => 'KarKode' ] );
	}

    /**
     * {@inheritdoc}
     * @return TdlokasiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdlokasiQuery(get_called_class());
    }
}
