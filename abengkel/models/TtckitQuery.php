<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttckit]].
 *
 * @see Ttckit
 */
class TtckitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttckit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttckit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'CKNo' ]          = $post[ 'CKNo' ] ?? '--';
        $post[ 'SDNo' ]          = $post[ 'SDNo' ] ?? '--';
        $post[ 'CKPart' ]        = floatval( $post[ 'CKPart' ] ?? 0 );
        $post[ 'CKJasa' ]        = floatval( $post[ 'CKJasa' ] ?? 0 );
        $post[ 'CKNoOLD' ]       = $post[ 'CKNoOLD' ] ?? '--';
        $post[ 'SDNoOLD' ]       = $post[ 'SDNoOLD' ] ?? '--';
        try {
            General::cCmd( "CALL fckit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'CKNo' ] )
                ->bindParam( 6, $post[ 'SDNo' ] )
                ->bindParam( 7, $post[ 'CKPart' ] )
                ->bindParam( 8, $post[ 'CKJasa' ] )
                ->bindParam( 9, $post[ 'CKNoOLD' ] )
                ->bindParam( 10, $post[ 'SDNoOLD' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
