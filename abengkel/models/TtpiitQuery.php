<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttpiit]].
 *
 * @see Ttpiit
 */
class TtpiitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttpiit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttpiit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	public function callSP( $post ) {
		$post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'PosKode' ]    = Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'PINo' ]       = $post[ 'PINo' ] ?? '--';
		$post[ 'PIAuto' ]     = floatval( $post[ 'PIAuto' ] ?? 0 );
		$post[ 'BrgKode' ]    = $post[ 'BrgKode' ] ?? '--';
		$post[ 'PIQty' ]      = floatval( $post[ 'PIQty' ] ?? 0 );
		$post[ 'PIHrgBeli' ]  = floatval( $post[ 'PIHrgBeli' ] ?? 0 );
		$post[ 'PIHrgJual' ]  = floatval( $post[ 'PIHrgJual' ] ?? 0 );
		$post[ 'PIDiscount' ] = floatval( $post[ 'PIDiscount' ] ?? 0 );
		$post[ 'PIPajak' ]    = floatval( $post[ 'PIPajak' ] ?? 0 );
		$post[ 'PSNo' ]       = $post[ 'PSNo' ] ?? '--';
        $post[ 'PINoOLD' ]    = $post[ 'PINoOLD' ] ?? '--';
        $post[ 'PIAutoOLD' ]  = floatval( $post[ 'PIAutoOLD' ] ?? 0 );
        $post[ 'BrgKodeOLD' ] = $post[ 'BrgKodeOLD' ] ?? '--';
        try {
            General::cCmd( "CALL fpiit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'PINo' ] )
                ->bindParam( 6, $post[ 'PIAuto' ] )
                ->bindParam( 7, $post[ 'BrgKode' ] )
                ->bindParam( 8, $post[ 'PIQty' ] )
                ->bindParam( 9, $post[ 'PIHrgBeli' ] )
                ->bindParam( 10, $post[ 'PIHrgJual' ] )
                ->bindParam( 11, $post[ 'PIDiscount' ] )
                ->bindParam( 12, $post[ 'PIPajak' ] )
                ->bindParam( 13, $post[ 'PSNo' ] )
                ->bindParam( 14, $post[ 'PINoOLD' ] )
                ->bindParam( 15, $post[ 'PIAutoOLD' ] )
                ->bindParam( 16, $post[ 'BrgKodeOLD' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
	}
}
