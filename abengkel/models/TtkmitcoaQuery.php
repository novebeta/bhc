<?php
namespace abengkel\models;
use common\components\General;
use abengkel\components\Menu;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttkmitcoa]].
 *
 * @see Ttkmitcoa
 */
class TtkmitcoaQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttkmitcoa[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttkmitcoa|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]       = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]    = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'PosKode' ]      = Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'KMNo' ]         = $post[ 'KMNo' ] ?? '--';
		$post[ 'KMAutoN' ]      = floatval( $post[ 'KMAutoN' ] ?? 0 );
		$post[ 'NoAccount' ]    = $post[ 'NoAccount' ] ?? '--';
		$post[ 'KMKeterangan' ] = $post[ 'KMKeterangan' ] ?? '--';
		$post[ 'KMBayar' ]      = floatval( $post[ 'KMBayar' ] ?? 0 );
        $post[ 'KMNoOLD' ]      = $post[ 'KMNoOLD' ] ?? '--';
        $post[ 'KMAutoNOLD' ]   = floatval( $post[ 'KMAutoNOLD' ] ?? 0 );
        $post[ 'NoAccountOLD' ] = $post[ 'NoAccountOLD' ] ?? '--';
        try {
		General::cCmd( "CALL fkmitcoa(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?);" )
		       ->bindParam( 1, $post[ 'Action' ] )
		       ->bindParam( 2, $post[ 'UserID' ] )
		       ->bindParam( 3, $post[ 'KodeAkses' ] )
		       ->bindParam( 4, $post[ 'PosKode' ] )
		       ->bindParam( 5, $post[ 'KMNo' ] )
		       ->bindParam( 6, $post[ 'KMAutoN' ] )
		       ->bindParam( 7, $post[ 'NoAccount' ] )
		       ->bindParam( 8, $post[ 'KMKeterangan' ] )
		       ->bindParam( 9, $post[ 'KMBayar' ] )
                ->bindParam( 10, $post[ 'KMNoOLD' ] )
                ->bindParam( 11, $post[ 'KMAutoNOLD' ] )
                ->bindParam( 12, $post[ 'NoAccountOLD' ] )
		       ->execute();
		$exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
	}
}
