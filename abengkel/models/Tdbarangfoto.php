<?php

namespace abengkel\models;

/**
 * This is the model class for table "tdbarangfoto".
 *
 * @property string $BrgKode
 * @property resource $BrgFoto
 */
class Tdbarangfoto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdbarangfoto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BrgKode'], 'required'],
            [['BrgFoto'], 'string'],
            [['BrgKode'], 'string', 'max' => 20],
            [['BrgKode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'BrgKode' => 'Brg Kode',
            'BrgFoto' => 'Brg Foto',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TdbarangfotoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdbarangfotoQuery(get_called_class());
    }
}
