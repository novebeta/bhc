<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttseitbarang".
 *
 * @property string $SENo
 * @property int $SEAuto
 * @property string $BrgKode
 * @property string $SEQty
 * @property string $SEHrgBeli
 * @property string $SEHrgJual
 * @property string $SEJualDisc
 * @property string $LokasiKode
 */
class Ttseitbarang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttseitbarang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SENo', 'SEAuto'], 'required'],
            [['SEAuto'], 'integer'],
            [['SEQty', 'SEHrgBeli', 'SEHrgJual', 'SEJualDisc'], 'number'],
            [['SENo'], 'string', 'max' => 10],
            [['BrgKode'], 'string', 'max' => 20],
            [['LokasiKode'], 'string', 'max' => 15],
            [['SENo', 'SEAuto'], 'unique', 'targetAttribute' => ['SENo', 'SEAuto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SENo' => 'Se No',
            'SEAuto' => 'Se Auto',
            'BrgKode' => 'Brg Kode',
            'SEQty' => 'Se Qty',
            'SEHrgBeli' => 'Se Hrg Beli',
            'SEHrgJual' => 'Se Hrg Jual',
            'SEJualDisc' => 'Se Jual Disc',
            'LokasiKode' => 'Lokasi Kode',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtseitbarangQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtseitbarangQuery(get_called_class());
    }
}
