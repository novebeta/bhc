<?php

namespace abengkel\models;

/**
 * This is the model class for table "tdjasagroup".
 *
 * @property string $JasaGroup
 * @property string $JasaGroupNama
 */
class Tdjasagroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdjasagroup';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['JasaGroup'], 'required'],
            [['JasaGroup'], 'string', 'max' => 10],
            [['JasaGroupNama'], 'string', 'max' => 50],
            [['JasaGroup'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'JasaGroup' => 'Jasa Group',
            'JasaGroupNama' => 'Jasa Group Nama',
        ];
    }
	public static  function colGrid() {
		return [
			'JasaGroup'        => ['width' => 80,'label' => 'Group Jasa','name'  => 'JasaGroup'],
			'JasaGroupNama'        => ['width' => 150,'label' => 'Group Nama','name'  => 'JasaGroupNama'],
		];
	}
    /**
     * {@inheritdoc}
     * @return TdjasagroupQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdjasagroupQuery(get_called_class());
    }
}
