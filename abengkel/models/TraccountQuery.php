<?php
namespace abengkel\models;
use abengkel\components\Menu;
use common\components\General;
use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the ActiveQuery class for [[Traccount]].
 *
 * @see Traccount
 */
class TraccountQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Traccount[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Traccount|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function combo() {
		return $this->select( [ "CONCAT(NoAccount,' - ',NamaAccount) as label", "NoAccount as value" ] )
		            ->orderBy( 'NoAccount' )
		            ->asArray()
		            ->all();
	}
	public function header() {
		return ArrayHelper::map( $this->select( [
			"CONCAT(NoAccount,' - ',NamaAccount) as label",
			"NoAccount as value"
		] )
		                              ->where( [ 'StatusAccount' => 'A', 'JenisAccount' => 'Header' ] )
		                              ->orderBy( 'NoAccount' )
		                              ->asArray()
		                              ->all(), 'value', 'label' );
	}
	public function comboSelect2( $namaAccount = false ) {
		return $this->select( [ 'CONCAT(NoAccount, ":", ' . ( $namaAccount ? 'REPLACE(NamaAccount, ":","")' : 'NoAccount' ) . ')' ] )
		            ->where( 'JenisAccount = "Detail"' )
		            ->orderBy( 'NoAccount' )
		            ->asArray()
		            ->column();
	}
	public function getChild( $NoParent ) {
		$hasil  = Traccount::find()
		                   ->select(
			                   [
				                   '*',
				                   'NoAccount AS id',
				                   'IF(JenisAccount = "Header", "img/folder.png", "img/card.png") as icon',
				                   'CONCAT(NoAccount," ",NamaAccount) AS label'
			                   ] )
		                   ->where( [ 'NoParent' => $NoParent ] )
		                   ->asArray()
		                   ->all();
		$hasil1 = [];
		foreach ( $hasil as $row ) {
			$row[ 'value' ] = json_encode( $row );
			$hasil1[]       = $row;
			$hasil1         = array_merge( $hasil1, self::getChild( $row[ 'NoAccount' ] ) );
		}
		return $hasil1;
	}
	public function getAccount( $NoAccount ) {
		$aktiva            = Traccount::find()
		                              ->select(
			                              [
				                              '*',
				                              'NoAccount AS id',
				                              'IF(JenisAccount = "Header", "img/folder.png", "img/card.png") as icon',
				                              'CONCAT(NoAccount," ",NamaAccount) AS label'
			                              ] )
		                              ->where( [ 'NoAccount' => $NoAccount ] )->asArray()->one();
		$aktiva[ 'value' ] = json_encode( $aktiva );
		$child_aktiva      = self::getChild( $NoAccount );
		$child_aktiva[]    = $aktiva;
		return $child_aktiva;
	}
	public function select2() {
		return ArrayHelper::map( $this->select( [
			"CONCAT(NamaAccount) as label",
			"NoAccount as value"
		] )
		                              ->where( [ 'StatusAccount' => 'A', 'JenisAccount' => 'Header' ] )
		                              ->orderBy( 'NoAccount' )
		                              ->asArray()
		                              ->all(), 'value', 'label' );
	}
	public function NoAccount( $value, $label = [ 'NamaAccount' ], $orderBy = [ 'NamaAccount' ], $where = [ 'condition' => null, 'params' => [] ], $allOption = false ) {
		$option =
			$this->select( [ "CONCAT(" . implode( ",'||',", $label ) . ") as label", "$value as value" ] )
			     ->orderBy( $orderBy )
			     ->where( $where[ 'condition' ], $where[ 'params' ] )
			     ->asArray()
			     ->all();
		if ( $allOption ) {
			return array_merge( [ [ 'value' => '%', 'label' => 'Semua' ] ], $option );
		} else {
			return $option;
		}
	}
	public function Account() {
		return ArrayHelper::map( $this->select( [ "CONCAT(NamaAccount) as label", "NoAccount as value" ] )
		                              ->asArray()
		                              ->all(), 'value', 'label' );
	}
	public function bank( $convert = true ) {
		$arr = General::cCmd( "SELECT NoAccount as id, NamaAccount as text FROM traccount 
		WHERE (NoParent = '11020100' OR NoParent = '11020200' OR  NoParent = '11030000' )  
		  AND StatusAccount = 'A' 
		UNION SELECT NoAccount as id, NamaAccount  as text FROM traccount 
		WHERE (NoParent = '11020300')  AND StatusAccount = 'A'" )->queryAll();
		return $convert ? ArrayHelper::map( $arr, 'id', 'text' ) : $arr;
	}
	public function kas( $convert = true ) {
		$LokasiKode = Menu::getUserLokasi()[ 'PosKode' ];
		$arr        = [];
		if ( strpos( strtolower( $LokasiKode ), 'pos' ) !== false ) {
			$arr = General::cCmd( "SELECT  tdpos.NoAccount as id,NamaAccount as text FROM tdpos 
			INNER JOIN tuser ON tuser.LokasiKode = tdpos.PosKode 
			INNER JOIN traccount ON traccount.NoAccount = tdpos.NoAccount 
			WHERE UserID = :UserID AND LokasiKode LIKE 'Pos%';",[':UserID' => Yii::$app->user->id] )->queryAll();
		} else {
			$arr = General::cCmd( "SELECT NoAccount as id, NamaAccount as text FROM traccount 
			WHERE LEFT(NoAccount,5) = '11010'  AND JenisAccount = 'Detail' ORDER BY NoAccount" )->queryAll();
		}
		return $convert ? ArrayHelper::map( $arr, 'id', 'text' ) : $arr;
	}
    public function kastujuan( $convert = true ) {
        $LokasiKode = Menu::getUserLokasi()[ 'PosKode' ];
        $arr        = [];
           $arr = General::cCmd( "SELECT NoAccount as id, NamaAccount as text FROM traccount 
			WHERE LEFT(NoAccount,5) = '11010'  AND JenisAccount = 'Detail' ORDER BY NoAccount" )->queryAll();
        return $convert ? ArrayHelper::map( $arr, 'id', 'text' ) : $arr;
    }
}
