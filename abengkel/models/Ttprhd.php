<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttprhd".
 *
 * @property string $PRNo
 * @property string $PRTgl
 * @property string $PINo
 * @property string $KodeTrans
 * @property string $PosKode
 * @property string $SupKode
 * @property string $LokasiKode
 * @property string $PRSubTotal
 * @property string $PRDiscFinal
 * @property string $PRTotalPajak
 * @property string $PRBiayaKirim
 * @property string $PRTotal
 * @property string $PRTerbayar
 * @property string $PRKeterangan
 * @property string $NoGL
 * @property string $Cetak
 * @property string $UserID
 */
class Ttprhd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttprhd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PRNo'], 'required'],
            [['PRTgl'], 'safe'],
            [['PRSubTotal', 'PRDiscFinal', 'PRTotalPajak', 'PRBiayaKirim', 'PRTotal', 'PRTerbayar'], 'number'],
            [['PRNo', 'PINo', 'NoGL'], 'string', 'max' => 10],
            [['KodeTrans'], 'string', 'max' => 4],
            [['PosKode'], 'string', 'max' => 20],
            [['SupKode', 'LokasiKode', 'UserID'], 'string', 'max' => 15],
            [['PRKeterangan'], 'string', 'max' => 150],
            [['Cetak'], 'string', 'max' => 5],
            [['PRNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PRNo'         => 'Pr No',
            'PRTgl'        => 'Pr Tgl',
            'PINo'         => 'Pi No',
            'KodeTrans'    => 'Kode Trans',
            'PosKode'      => 'Pos Kode',
            'SupKode'      => 'Sup Kode',
            'LokasiKode'   => 'Lokasi Kode',
            'PRSubTotal'   => 'Pr Sub Total',
            'PRDiscFinal'  => 'Pr Disc Final',
            'PRTotalPajak' => 'Pr Total Pajak',
            'PRBiayaKirim' => 'Pr Biaya Kirim',
            'PRTotal'      => 'Pr Total',
            'PRTerbayar'   => 'Pr Terbayar',
            'PRKeterangan' => 'Pr Keterangan',
            'NoGL'         => 'No Gl',
            'Cetak'        => 'Cetak',
            'UserID'       => 'User ID',
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
		$col  = [
		    'PRNo'         => ['width' => 85,'label' => 'No PR','name'  => 'PRNo'],
            'PRTgl'        => ['width' => 115,'label' => 'Tgl PR','name'  => 'PRTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y H:i:s']],
            'PINo'         => ['width' => 85,'label' => 'No PI','name'  => 'PINo'],
            'KodeTrans'    => ['width' => 60,'label' => 'TC','name'  => 'KodeTrans'],
            'SupKode'      => ['width' => 70,'label' => 'Supplier','name'  => 'SupKode'],
            'LokasiKode'   => ['width' => 80,'label' => 'Lokasi','name'  => 'LokasiKode'],
            'PRTotal'      => ['width' => 90,'label' => 'Total','name'  => 'PRTotal','formatter' => 'number', 'align' => 'right'],
            'PRTerbayar'   => ['width' => 90,'label' => 'Terbayar','name'  => 'PRTerbayar','formatter' => 'number', 'align' => 'right'],
		];
		$col['IF(PRTotal-PRTerbayar = 0,"Lunas","Belum") AS PRStatus'] = ['label' => 'Status','width' => 60,'name'  => 'PRStatus'];
		$col['NoGL'] = ['label' => 'NoGL','width' => 70,'name'  => 'NoGL'];
		$col['Cetak'] = ['label' => 'Cetak','width' => 50,'name'  => 'Cetak'];
		$col['UserID'] = ['label' => 'UserID','width' => 85,'name'  => 'UserID'];
		$col['PRKeterangan'] = ['label' => 'Keterangan','width' => 300,'name'  => 'PRKeterangan'];
		$col['PosKode'] = ['label' => 'Pos','width' => 90,'name'  => 'PosKode'];
		return $col;

    }

    /**
     * {@inheritdoc}
     * @return TtprhdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtprhdQuery(get_called_class());
    }
}
