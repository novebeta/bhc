<?php
namespace abengkel\models;
use common\components\General;
use yii\db\Exception;
/**
 * This is the ActiveQuery class for [[Vtkbm]].
 *
 * @see Vtkbm
 */
class VtkbmQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Vtkbm[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Vtkbm|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function getTotalBayar( $KBMLink ) {
		$Terbayar = 0;
		try {
			$Terbayar = General::cCmd( "SELECT IFNULL(SUM(KBMBayar),0) FROM vtkbm WHERE KBMLink = :KBMLink GROUP BY KBMLink ", [ ':KBMLink' => $KBMLink ] )->queryScalar();
			if ( $Terbayar === false ) {
				$Terbayar = 0;
			}
		} catch ( Exception $e ) {
			$Terbayar = 0;
		}
		return $Terbayar;
	}
}
