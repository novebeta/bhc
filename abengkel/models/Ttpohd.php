<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttpohd".
 *
 * @property string $PONo
 * @property string $POTgl
 * @property string $PSNo
 * @property string $SONo
 * @property string $KodeTrans
 * @property string $PosKode
 * @property string $SupKode
 * @property string $LokasiKode
 * @property string $POSubTotal
 * @property string $PODiscFinal
 * @property string $POTotalPajak
 * @property string $POBiayaKirim
 * @property string $POTotal
 * @property string $POKeterangan
 * @property string $Cetak
 * @property string $UserID
 */
class Ttpohd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttpohd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PONo'], 'required'],
            [['POTgl'], 'safe'],
            [['POSubTotal', 'PODiscFinal', 'POTotalPajak', 'POBiayaKirim', 'POTotal'], 'number'],
            [['PONo', 'PSNo', 'SONo'], 'string', 'max' => 10],
            [['KodeTrans'], 'string', 'max' => 4],
            [['PosKode'], 'string', 'max' => 20],
            [['SupKode', 'LokasiKode', 'UserID'], 'string', 'max' => 15],
            [['POKeterangan'], 'string', 'max' => 150],
            [['Cetak'], 'string', 'max' => 5],
            [['PONo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PONo'         => 'Po No',
            'POTgl'        => 'Po Tgl',
            'PSNo'         => 'Ps No',
            'SONo'         => 'So No',
            'KodeTrans'    => 'Kode Trans',
            'PosKode'      => 'Pos Kode',
            'SupKode'      => 'Sup Kode',
            'LokasiKode'   => 'Lokasi Kode',
            'POSubTotal'   => 'Po Sub Total',
            'PODiscFinal'  => 'Po Disc Final',
            'POTotalPajak' => 'Po Total Pajak',
            'POBiayaKirim' => 'Po Biaya Kirim',
            'POTotal'      => 'Po Total',
            'POKeterangan' => 'Po Keterangan',
            'Cetak'        => 'Cetak',
            'UserID'       => 'User ID',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
        return [
            'PONo'         => ['width' => 90,'label' => 'No PO','name'  => 'PONo'],
            'POTgl'        => ['width' => 115,'label' => 'Tgl PO','name'  => 'POTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y H:i:s']],
            'PSNo'         => ['width' => 90,'label' => 'No PS','name'  => 'PSNo'],
            'SONo'         => ['width' => 90,'label' => 'No SO','name'  => 'SONo'],
            'KodeTrans'    => ['width' => 50,'label' => 'TC','name'  => 'KodeTrans'],
            'SupKode'      => ['width' => 100,'label' => 'Supplier','name'  => 'SupKode'],
            'POTotal'      => ['width' => 110,'label' => 'Total','name'  => 'POTotal', 'formatter' => 'number', 'align' => 'right'],
            'POAcc'        => ['width' => 90,'label' => 'Acc','name'  => 'POAcc'],
            'POStatus'     => ['width' => 90,'label' => 'Status','name'  => 'POStatus'],
            'POKeterangan' => ['width' => 205,'label' => 'Keterangan','name'  => 'POKeterangan'],
            'UserID'       => ['width' => 90,'label' => 'UserID','name'  => 'UserID'],
			'Cetak'		   => ['width' => 50,'label' => 'Cetak','name'  => 'Cetak'],
			'PosKode'	=> ['width' => 90,'label' => 'Pos','name'  => 'PosKode'],
        ];
    }

    public static function colGridPick()
    {
        return [
            'PONo'         => ['width' => 100,'label' => 'No PO','name'  => 'PONo'],
            'POTgl'        => ['width' => 120,'label' => 'Tgl PO','name'  => 'POTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y']],
            'SupKode'      => ['width' => 120,'label' => 'Kode Supplier','name' => 'SupKode'],
            'LokasiKode'   => ['width' => 100,'label' => 'Lokasi','name'  => 'LokasiKode'],
            'KodeTrans'    => ['width' => 80,'label' => 'Kode','name'  => 'KodeTrans'],
            'SONo'         => ['width' => 100,'label' => 'No SO','name'  => 'SONo'],
            'POKeterangan' => ['width' => 200,'label' => 'Keterangan','name'  => 'POKeterangan'],
        ];
    }

    public static function colGridImportItems()
    {
        return [
            'noSO' => ['label' => 'No SO','width' => 100, 'name' => 'noSO'],
            'tglSO' => ['label' => 'Tgl SO','width' => 85, 'name' => 'tglSO'],
            'idCustomer' => ['label' => 'Id Customer','width' => 90, 'name' => 'idCustomer'],
            'namaCustomer' => ['label' => 'Nama Customer','width' => 100, 'name' => 'namaCustomer'],
            'discSO' => ['label' => 'Disc SO','width' => 80, 'name' => 'discSO', 'formatter' => 'number',],
            'totalHargaSO' => ['label' => 'Total Harga SO','width' => 100, 'name' => 'totalHargaSO', 'formatter' => 'number',],
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtpohdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtpohdQuery(get_called_class());
    }
}
