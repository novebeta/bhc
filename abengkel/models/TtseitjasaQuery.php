<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttseitjasa]].
 *
 * @see Ttseitjasa
 */
class TtseitjasaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttseitjasa[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttseitjasa|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]            = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]         = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]           = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'SENo' ]              = $post[ 'SENo' ] ?? '--';
        $post[ 'SEAuto' ]            = floatval( $post[ 'SEAuto' ] ?? 0 );
        $post[ 'JasaKode' ]          = $post[ 'JasaKode' ] ?? '--';
        $post[ 'SEWaktuKerja' ]      = floatval( $post[ 'SEWaktuKerja' ] ?? 0 );
        $post[ 'SEWaktuSatuan' ]     = floatval( $post[ 'SEWaktuSatuan' ] ?? 0 );
        $post[ 'SEWaktuKerjaMenit' ] = floatval( $post[ 'SEWaktuKerjaMenit' ] ?? 0 );
        $post[ 'SEHargaBeli' ]       = floatval( $post[ 'SEHargaBeli' ] ?? 0 );
        $post[ 'SEHrgJual' ]         = floatval( $post[ 'SEHrgJual' ] ?? 0 );
        $post[ 'SEJualDisc' ]        = floatval( $post[ 'SEJualDisc' ] ?? 0 );
        $post[ 'SENoOLD' ]           = $post[ 'SENoOLD' ] ?? '--';
        $post[ 'SEAutoOLD' ]         = floatval( $post[ 'SEAutoOLD' ] ?? 0 );
        $post[ 'JasaKodeOLD' ]       = $post[ 'JasaKodeOLD' ] ?? '--';
        try {
            General::cCmd( "CALL fseitjasa(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'SENo' ] )
                ->bindParam( 6, $post[ 'SEAuto' ] )
                ->bindParam( 7, $post[ 'JasaKode' ] )
                ->bindParam( 8, $post[ 'SEWaktuKerja' ] )
                ->bindParam( 9, $post[ 'SEWaktuSatuan' ] )
                ->bindParam( 10, $post[ 'SEWaktuKerjaMenit' ] )
                ->bindParam( 11, $post[ 'SEHargaBeli' ] )
                ->bindParam( 12, $post[ 'SEHrgJual' ] )
                ->bindParam( 13, $post[ 'SEJualDisc' ] )
                ->bindParam( 14, $post[ 'SENoOLD' ] )
                ->bindParam( 15, $post[ 'SEAutoOLD' ] )
                ->bindParam( 16, $post[ 'JasaKodeOLD' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
