<?php
namespace abengkel\models;
use abengkel\components\Menu;
/**
 * This is the model class for table "ttkmhd".
 *
 * @property string $KMNo
 * @property string $KMTgl
 * @property string $KMJenis
 * @property string $KodeTrans
 * @property string $PosKode
 * @property string $MotorNoPolisi
 * @property string $KasKode
 * @property string $KMPerson
 * @property string $KMBayarTunai
 * @property string $KMNominal
 * @property string $KMMemo
 * @property string $NoGL
 * @property string $Cetak
 * @property mixed $account
 * @property string $UserID
 */
class Ttkmhd extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttkmhd';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'KMNo' ], 'required' ],
			[ [ 'KMTgl' ], 'safe' ],
			[ [ 'KMBayarTunai', 'KMNominal' ], 'number' ],
			[ [ 'KMNo', 'MotorNoPolisi' ], 'string', 'max' => 12 ],
			[ [ 'KMJenis', 'PosKode' ], 'string', 'max' => 20 ],
			[ [ 'KodeTrans' ], 'string', 'max' => 4 ],
			[ [ 'KasKode' ], 'string', 'max' => 25 ],
			[ [ 'KMPerson' ], 'string', 'max' => 50 ],
			[ [ 'KMMemo' ], 'string', 'max' => 150 ],
			[ [ 'NoGL' ], 'string', 'max' => 10 ],
			[ [ 'Cetak' ], 'string', 'max' => 5 ],
			[ [ 'UserID' ], 'string', 'max' => 15 ],
			[ [ 'KMNo' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'KMNo'          => 'Km No',
			'KMTgl'         => 'Km Tgl',
			'KMJenis'       => 'Km Jenis',
			'KodeTrans'     => 'Kode Trans',
			'PosKode'       => 'Pos Kode',
			'MotorNoPolisi' => 'Motor No Polisi',
			'KasKode'       => 'Kas Kode',
			'KMPerson'      => 'Km Person',
			'KMBayarTunai'  => 'Km Bayar Tunai',
			'KMNominal'     => 'Km Nominal',
			'KMMemo'        => 'Km Memo',
			'NoGL'          => 'No Gl',
			'Cetak'         => 'Cetak',
			'UserID'        => 'User ID',
		];
	}
	public static function colGrid() {
        return [
            'KMNo'           => ['width' => 95,'label' => 'No KM','name'  => 'KMNo'],
            'KMTgl'          => ['width' => 120,'label' => 'Tanggal KM','name'  => 'KMTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y  H:i:s'] ],
            'KodeTrans'      => ['width' => 45,'label' => 'TC','name'  => 'KodeTrans'],      
            'KasKode'        => ['width' => 70,'label' => 'Kode Kas','name'  => 'KasKode'],      
            'NamaAccount'    => ['width' => 140,'label' => 'Nama Kas','name'  => 'NamaAccount'],      
            'MotorNoPolisi'  => ['width' => 80,'label' => 'NoPolisi','name'  => 'MotorNoPolisi'],      
            'KMPerson'       => ['width' => 190,'label' => 'Terima Dari','name'  => 'KMPerson'],      
            'KMNominal'      => ['width' => 100,'label' => 'Nominal','name'  => 'KMNominal','formatter' => 'number', 'align' => 'right'],      
            'PosKode'        => ['width' => 100,'label' => 'Pos','name'  => 'PosKode'],      
            'KMJenis'        => ['width' => 60,'label' => 'Jenis','name'  => 'KMJenis'],      
            'NoGL'           => ['width' => 80,'label' => 'No JT','name'  => 'NoGL'],      
            'Cetak'          => ['width' => 50,'label' => 'Cetak','name'  => 'Cetak'],
			'UserID'         => ['width' => 105,'label' => 'UserID','name'  => 'UserID'],
            'KMMemo'         => ['width' => 500,'label' => 'Keterangan','name'  => 'KMMemo'],      
            'KMBayarTunai'   => ['width' => 100,'label' => 'BayarTunai','name'  => 'KMBayarTunai','formatter' => 'number', 'align' => 'right'],      
        ];
    }
    public function getAccount() {
        return $this->hasOne( Traccount::className(), [ 'NoAccount' => 'KasKode' ] );
    }
    /**
	 * {@inheritdoc}
	 * @return TtkmhdQuery the active query used by this AR class.
	 */
	public static function generateKMNo( $temporary = false ) {
		$sign = $temporary ? '=': '-';
		$kode    = 'KM';
		$posNo = Menu::getUserLokasi()['PosNomor'];
		$yNow    = date( "y" );
		$maxNoGl = ( new \yii\db\Query() )
			->from( self::tableName() )
			->where( "KMNo LIKE '$kode$posNo$yNow$sign%'" )
			->max( 'KMNo' );
		if ( $maxNoGl && preg_match( "/$kode\d{4}$sign(\d+)/", $maxNoGl, $result ) ) {
			[ $all, $counter ] = $result;
			$digitCount  = strlen( $counter );
			$val         = (int) $counter;
			$nextCounter = sprintf( '%0' . $digitCount . 'd', ++ $val );
		} else {
			$nextCounter = "00001";
		}
		return "$kode$posNo$yNow$sign$nextCounter";
	}

	/**
	 * {@inheritdoc}
	 */
	public static function colGridPick() {
		return [
			'No'        => [ 'width' => 95, 'label' => 'No', 'name' => 'NO' ],
			'Tgl'       => [ 'width' => 120, 'label' => 'Tgl', 'name' => 'Tgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y  H:i:s' ] ],
			'Kode'      => [ 'width' => 70, 'label' => 'Kode', 'name' => 'Kode' ],
			'Nama'      => [ 'width' => 70, 'label' => 'Nama', 'name' => 'Nama' ],
			'MotorNama' => [ 'width' => 140, 'label' => 'MotorNama', 'name' => 'MotorNama' ],
			'Total'     => [ 'width' => 100, 'label' => 'Total', 'name' => 'Total', 'formatter' => 'number', 'align' => 'right' ],
			'Terbayar'  => [ 'width' => 100, 'label' => 'Terbayar', 'name' => 'Terbayar', 'formatter' => 'number', 'align' => 'right' ],
			'Sisa'      => [ 'width' => 100, 'label' => 'Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
			'Bayar'     => [ 'width' => 100, 'label' => 'Bayar', 'name' => 'Bayar', 'formatter' => 'number', 'align' => 'right' ],
		];
	}

    public static function colGridPickSV() {
        return [
            'No'        => [ 'width' => 95, 'label' => 'No Servis', 'name' => 'NO' ],
            'Tgl'       => [ 'width' => 120, 'label' => 'Tgl Servis', 'name' => 'Tgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y  H:i:s' ] ],
            'Kode'      => [ 'width' => 70, 'label' => 'No Polisi', 'name' => 'Kode' ],
            'Nama'      => [ 'width' => 130, 'label' => 'Nama Konsumen', 'name' => 'Nama' ],
            'MotorNama' => [ 'width' => 160, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
            'Total'     => [ 'width' => 80, 'label' => 'Total', 'name' => 'Total', 'formatter' => 'number', 'align' => 'right' ],
            'Terbayar'  => [ 'width' => 80, 'label' => 'Terbayar', 'name' => 'Terbayar', 'formatter' => 'number', 'align' => 'right' ],
            'Sisa'      => [ 'width' => 80, 'label' => 'Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
            'Bayar'     => [ 'width' => 80, 'label' => 'Bayar', 'name' => 'Bayar', 'formatter' => 'number', 'align' => 'right' ],
        ];
    }

    public static function colGridPickSI() {
        return [
            'No'        => [ 'width' => 95, 'label' => 'No Jual', 'name' => 'NO' ],
            'Tgl'       => [ 'width' => 120, 'label' => 'Tgl Jual', 'name' => 'Tgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y  H:i:s' ] ],
            'Kode'      => [ 'width' => 70, 'label' => 'No Polisi', 'name' => 'Kode' ],
            'Nama'      => [ 'width' => 130, 'label' => 'Nama Konsumen', 'name' => 'Nama' ],
            'MotorNama' => [ 'width' => 160, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
            'Total'     => [ 'width' => 80, 'label' => 'Total', 'name' => 'Total', 'formatter' => 'number', 'align' => 'right' ],
            'Terbayar'  => [ 'width' => 80, 'label' => 'Terbayar', 'name' => 'Terbayar', 'formatter' => 'number', 'align' => 'right' ],
            'Sisa'      => [ 'width' => 80, 'label' => 'Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
            'Bayar'     => [ 'width' => 80, 'label' => 'Bayar', 'name' => 'Bayar', 'formatter' => 'number', 'align' => 'right' ],
        ];
    }

    public static function colGridPickSO() {
        return [
            'No'        => [ 'width' => 95, 'label' => 'No Order', 'name' => 'NO' ],
            'Tgl'       => [ 'width' => 120, 'label' => 'Tgl Order', 'name' => 'Tgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y  H:i:s' ] ],
            'Kode'      => [ 'width' => 70, 'label' => 'No Polisi', 'name' => 'Kode' ],
            'Nama'      => [ 'width' => 130, 'label' => 'Nama Konsumen', 'name' => 'Nama' ],
            'MotorNama' => [ 'width' => 160, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
            'Total'     => [ 'width' => 80, 'label' => 'Total', 'name' => 'Total', 'formatter' => 'number', 'align' => 'right' ],
            'Terbayar'  => [ 'width' => 80, 'label' => 'Terbayar', 'name' => 'Terbayar', 'formatter' => 'number', 'align' => 'right' ],
            'Sisa'      => [ 'width' => 80, 'label' => 'Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
            'Bayar'     => [ 'width' => 80, 'label' => 'Bayar', 'name' => 'Bayar', 'formatter' => 'number', 'align' => 'right' ],
        ];
    }

    public static function colGridPickPR() {
        return [
            'No'        => [ 'width' => 95, 'label' => 'No Retur Beli', 'name' => 'NO' ],
            'Tgl'       => [ 'width' => 120, 'label' => 'Tgl Servis Retur Beli', 'name' => 'Tgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y  H:i:s' ] ],
            'Kode'      => [ 'width' => 70, 'label' => 'Supplier', 'name' => 'Kode' ],
            'Nama'      => [ 'width' => 130, 'label' => 'Nama Supplier', 'name' => 'Nama' ],
            'MotorNama' => [ 'width' => 160, 'label' => 'Lokasi', 'name' => 'MotorNama' ],
            'Total'     => [ 'width' => 80, 'label' => 'Total', 'name' => 'Total', 'formatter' => 'number', 'align' => 'right' ],
            'Terbayar'  => [ 'width' => 80, 'label' => 'Terbayar', 'name' => 'Terbayar', 'formatter' => 'number', 'align' => 'right' ],
            'Sisa'      => [ 'width' => 80, 'label' => 'Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
            'Bayar'     => [ 'width' => 80, 'label' => 'Bayar', 'name' => 'Bayar', 'formatter' => 'number', 'align' => 'right' ],
        ];
    }

    public static function colGridPickSE() {
        return [
            'No'        => [ 'width' => 95, 'label' => 'No Estimasi', 'name' => 'NO' ],
            'Tgl'       => [ 'width' => 120, 'label' => 'Tgl Estimasi', 'name' => 'Tgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y  H:i:s' ] ],
            'Kode'      => [ 'width' => 70, 'label' => 'No Polisi', 'name' => 'Kode' ],
            'Nama'      => [ 'width' => 130, 'label' => 'Nama Konsumen', 'name' => 'Nama' ],
            'MotorNama' => [ 'width' => 160, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
            'Total'     => [ 'width' => 80, 'label' => 'Total', 'name' => 'Total', 'formatter' => 'number', 'align' => 'right' ],
            'Terbayar'  => [ 'width' => 80, 'label' => 'Terbayar', 'name' => 'Terbayar', 'formatter' => 'number', 'align' => 'right' ],
            'Sisa'      => [ 'width' => 80, 'label' => 'Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
            'Bayar'     => [ 'width' => 80, 'label' => 'Bayar', 'name' => 'Bayar', 'formatter' => 'number', 'align' => 'right' ],
        ];
    }

	public static function getRoute( $KodeTrans, $param = []) {
		$index  = [];
		$update = [];
		$create = [];
		switch ( $KodeTrans ) {
			case 'UM' :
				$index = [ 'ttkmhd/kas-masuk-umum' ];
				$update = [ 'ttkmhd/kas-masuk-umum-update' ];
				$create = [ 'ttkmhd/kas-masuk-umum-create' ];
				break;
			case 'BK' :
				$index = [ 'ttkmhd/kas-masuk-dari-bank' ];
				$update = [ 'ttkmhd/kas-masuk-dari-bank-update' ];
				$create = [ 'ttkmhd/kas-masuk-dari-bank-create' ];
				break;
			case 'SV' :
				$index = [ 'ttkmhd/kas-masuk-service-invoice' ];
				$update = [ 'ttkmhd/kas-masuk-service-invoice-update' ];
				$create = [ 'ttkmhd/kas-masuk-service-invoice-create' ];
				break;
			case 'SI' :
				$index = [ 'ttkmhd/kas-masuk-sales-invoice' ];
				$update = [ 'ttkmhd/kas-masuk-sales-invoice-update' ];
				$create = [ 'ttkmhd/kas-masuk-sales-invoice-create' ];
				break;
			case 'SE' :
				$index = [ 'ttkmhd/kas-masuk-estimasi-servis' ];
				$update = [ 'ttkmhd/kas-masuk-estimasi-servis-update' ];
				$create = [ 'ttkmhd/kas-masuk-estimasi-servis-create' ];
				break;
			case 'SO' :
				$index = [ 'ttkmhd/kas-masuk-order-penjualan' ];
				$update = [ 'ttkmhd/kas-masuk-order-penjualan-update' ];
				$create = [ 'ttkmhd/kas-masuk-order-penjualan-create' ];
				break;
			case 'PR' :
				$index = [ 'ttkmhd/kas-masuk-retur-pembelian' ];
				$update = [ 'ttkmhd/kas-masuk-retur-pembelian-update' ];
				$create = [ 'ttkmhd/kas-masuk-retur-pembelian-create' ];
				break;
		}
		return [
			'index'  => \yii\helpers\Url::toRoute( array_merge( $index, $param ) ),
			'update' => \yii\helpers\Url::toRoute( array_merge( $update, $param ) ),
			'create' => \yii\helpers\Url::toRoute( array_merge( $create, $param ) ),
		];
	}

	public static function find() {
		return new TtkmhdQuery( get_called_class() );
	}
}
