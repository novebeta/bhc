<?php

namespace abengkel\models;

/**
 * This is the model class for table "tvsumbrglokasi".
 *
 * @property string $BrgKode
 * @property string $LokasiKode
 * @property string $MySum
 */
class Tvsumbrglokasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tvsumbrglokasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BrgKode', 'LokasiKode'], 'required'],
            [['MySum'], 'number'],
            [['BrgKode'], 'string', 'max' => 20],
            [['LokasiKode'], 'string', 'max' => 15],
            [['BrgKode', 'LokasiKode'], 'unique', 'targetAttribute' => ['BrgKode', 'LokasiKode']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'BrgKode' => 'Brg Kode',
            'LokasiKode' => 'Lokasi Kode',
            'MySum' => 'My Sum',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TvsumbrglokasiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TvsumbrglokasiQuery(get_called_class());
    }
}
