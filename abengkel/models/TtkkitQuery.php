<?php
namespace abengkel\models;
use common\components\General;
use abengkel\components\Menu;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttkkit]].
 *
 * @see Ttkkit
 */
class TtkkitQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttkkit[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttkkit|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'PosKode' ]    = Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'KKNo' ]       = $post[ 'KKNo' ] ?? '--';
		$post[ 'KKLink' ]     = $post[ 'KKLink' ] ?? '--';
		$post[ 'KKBayar' ]    = floatval( $post[ 'KKBayar' ] ?? 0 );
        $post[ 'KKNoOLD' ]    = $post[ 'KKNoOLD' ] ?? '--';
        $post[ 'KKLinkOLD' ]  = $post[ 'KKLinkOLD' ] ?? '--';
        try {
		General::cCmd( "CALL fkkit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?);" )
		       ->bindParam( 1, $post[ 'Action' ] )
		       ->bindParam( 2, $post[ 'UserID' ] )
		       ->bindParam( 3, $post[ 'KodeAkses' ] )
		       ->bindParam( 4, $post[ 'PosKode' ] )
		       ->bindParam( 5, $post[ 'KKNo' ] )
		       ->bindParam( 6, $post[ 'KKLink' ] )
		       ->bindParam( 7, $post[ 'KKBayar' ] )
		       ->bindParam( 8, $post[ 'KKNoOLD' ] )
		       ->bindParam( 9, $post[ 'KKLinkOLD' ] )
		       ->execute();
		$exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
	}
}
