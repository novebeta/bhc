<?php
namespace abengkel\models;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
use abengkel\components\Menu;
/**
 * This is the ActiveQuery class for [[Ttsihd]].
 *
 * @see Ttsihd
 */
class TtsihdQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttsihd[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttsihd|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function ttsihdFillByNo() {
		return $this->select( new Expression( "ttsihd.SINo, ttsihd.SITgl, ttsihd.SONo, ttsihd.SIJenis, ttsihd.PosKode, ttsihd.KodeTrans, ttsihd.KarKode, ttsihd.PrgNama, ttsihd.MotorNoPolisi, ttsihd.LokasiKode, ttsihd.SISubTotal, ttsihd.SIDiscFinal, ttsihd.SITotalPajak, 
                ttsihd.SIBiayaKirim, ttsihd.SIUangMuka, ttsihd.SITotal, ttsihd.SITOP, ttsihd.SITerbayar, ttsihd.SIKeterangan, ttsihd.NoGL, ttsihd.Cetak, ttsihd.UserID, tdkaryawan.KarNama, tdcustomer.CusNama, tdcustomer.MotorWarna, 
                tdmotortype.MotorNama, IFNULL(ttsohd.SOTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SOTgl, ttsihd.DealerKode, IFNULL(tddealer.DealerNama, '--') AS DealerNama, ttsihd.SDNo, ttsihd.ZnoSO, ttsihd.NPWP" ) )
		            ->join( "LEFT JOIN", "tdkaryawan", "ttsihd.KarKode = tdkaryawan.KarKode" )
		            ->join( "LEFT OUTER JOIN", "tddealer", "ttsihd.DealerKode = tddealer.DealerKode" )
		            ->join( "LEFT OUTER JOIN", "ttsohd", "ttsihd.SONo = ttsohd.SONo" )
		            ->join( "LEFT OUTER JOIN", "tdcustomer", "ttsihd.MotorNoPolisi = tdcustomer.MotorNoPolisi " )
		            ->join( "LEFT OUTER JOIN", "tdmotortype", "tdcustomer.MotorType = tdmotortype.MotorType" );
	}
	public function CekStatusBayar( $MyNumber, $SOTotal ) {
		$Terbayar    = \Yii::$app->db->createCommand( "SELECT IFNULL(SUM(KBMBayar),0) FROM vtkbm WHERE KBMLink = :KBMLink GROUP BY KBMLink", [
			':KBMLink' => $MyNumber,
		] )->queryScalar();
		$StatusBayar = 'BELUM';
		if ( ( $SOTotal - $Terbayar ) > 0 || $SOTotal == 0 ) {
			$StatusBayar = 'BELUM';
		} else if ( ( $SOTotal - $Terbayar ) <= 0 ) {
			$StatusBayar = 'LUNAS';
		}
		return [
			'TotalBayar'  => $Terbayar,
			'StatusBayar' => $StatusBayar
		];
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'PosKode' ]       = $post[ 'PosKode' ] ?? Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'SINo' ]          = $post[ 'SINo' ] ?? '--';
		$post[ 'SINoBaru' ]      = $post[ 'SINoView' ] ?? '--';
		$post[ 'SITgl' ]         = date('Y-m-d H:i:s', strtotime($post[ 'SITgl' ] ?? date( 'Y-m-d H:i:s' )));
		$post[ 'SONo' ]          = $post[ 'SONo' ] ?? '--';
		$post[ 'SDNo' ]          = $post[ 'SDNo' ] ?? '--';
		$post[ 'SIJenis' ]       = $post[ 'SIJenis' ] ?? '--';
		$post[ 'LokasiKode' ]    = $post[ 'LokasiKode' ] ?? '--';
		$post[ 'KodeTrans' ]     = $post[ 'KodeTrans' ] ?? '--';
		$post[ 'KarKode' ]       = $post[ 'KarKode' ] ?? '--';
		$post[ 'PrgNama' ]       = $post[ 'PrgNama' ] ?? '--';
		$post[ 'MotorNoPolisi' ] = $post[ 'MotorNoPolisi' ] ?? '--';
		$post[ 'DealerKode' ]    = $post[ 'DealerKode' ] ?? '--';
		$post[ 'SISubTotal' ]    = floatval( $post[ 'SISubTotal' ] ?? 0 );
		$post[ 'SIDiscFinal' ]   = floatval( $post[ 'SIDiscFinal' ] ?? 0 );
		$post[ 'SITotalPajak' ]  = floatval( $post[ 'SITotalPajak' ] ?? 0 );
		$post[ 'SIBiayaKirim' ]  = floatval( $post[ 'SIBiayaKirim' ] ?? 0 );
		$post[ 'SIUangMuka' ]    = floatval( $post[ 'SIUangMuka' ] ?? 0 );
		$post[ 'SITotal' ]       = floatval( $post[ 'SITotal' ] ?? 0 );
		$post[ 'SITOP' ]         = floatval( $post[ 'SITOP' ] ?? 0 );
		$post[ 'SITerbayar' ]    = floatval( $post[ 'SITerbayar' ] ?? 0 );
		$post[ 'SIKeterangan' ]  = $post[ 'SIKeterangan' ] ?? '--';
		$post[ 'NoGL' ]          = $post[ 'NoGL' ] ?? '--';
		$post[ 'Cetak' ]         = $post[ 'Cetak' ] ?? 'Belum';
		$post[ 'ZnoSO' ]         = $post[ 'ZnoSO' ] ?? '--';
        $post[ 'NPWP' ]              = $post[ 'NPWP' ] ?? '--';
        try {
		General::cCmd( "SET @SINo = ?;" )->bindParam( 1, $post[ 'SINo' ] )->execute();
		General::cCmd( "CALL fsihd(?,@Status,@Keterangan,?,?,?,?,@SINo,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
		       ->bindParam( 1, $post[ 'Action' ] )
		       ->bindParam( 2, $post[ 'UserID' ] )
		       ->bindParam( 3, $post[ 'KodeAkses' ] )
		       ->bindParam( 4, $post[ 'PosKode' ] )
		       ->bindParam( 5, $post[ 'Jenis' ] )
		       ->bindParam( 6, $post[ 'SINoBaru' ] )
		       ->bindParam( 7, $post[ 'SITgl' ] )
		       ->bindParam( 8, $post[ 'SONo' ] )
		       ->bindParam( 9, $post[ 'SDNo' ] )
		       ->bindParam( 10, $post[ 'SIJenis' ] )
		       ->bindParam( 11, $post[ 'LokasiKode' ] )
		       ->bindParam( 12, $post[ 'KodeTrans' ] )
		       ->bindParam( 13, $post[ 'KarKode' ] )
		       ->bindParam( 14, $post[ 'PrgNama' ] )
		       ->bindParam( 15, $post[ 'MotorNoPolisi' ] )
		       ->bindParam( 16, $post[ 'DealerKode' ] )
		       ->bindParam( 17, $post[ 'SISubTotal' ] )
		       ->bindParam( 18, $post[ 'SIDiscFinal' ] )
		       ->bindParam( 19, $post[ 'SITotalPajak' ] )
		       ->bindParam( 20, $post[ 'SIBiayaKirim' ] )
		       ->bindParam( 21, $post[ 'SIUangMuka' ] )
		       ->bindParam( 22, $post[ 'SITotal' ] )
		       ->bindParam( 23, $post[ 'SITOP' ] )
		       ->bindParam( 24, $post[ 'SITerbayar' ] )
		       ->bindParam( 25, $post[ 'SIKeterangan' ] )
		       ->bindParam( 26, $post[ 'NoGL' ] )
		       ->bindParam( 27, $post[ 'Cetak' ] )
		       ->bindParam( 28, $post[ 'ZnoSO' ] )
               ->bindParam( 29, $post[ 'NPWP' ] )
		       ->execute();
		$exe = General::cCmd( "SELECT @SINo,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'SINo'       => $post[ 'SINo' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $SINo = $exe[ '@SINo' ] ?? $post['SINo'];
        $status = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : ['.$post['Action'].'] Keterangan NULL';
        return [
            'SINo'       => $SINo,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
	}
}
