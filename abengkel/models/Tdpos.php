<?php

namespace abengkel\models;

/**
 * This is the model class for table "tdpos".
 *
 * @property string $PosKode
 * @property string $PosNama
 * @property string $PosAlamat
 * @property string $PosTelepon
 * @property string $KarKode
 * @property string $PosNomor
 * @property string $PosStatus
 * @property string $PosJenis Mandiri, Non Mandiri
 * @property mixed $account
 * @property mixed $karyawan
 * @property string $NoAccount
 */
class Tdpos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdpos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PosKode'], 'required'],
            [['PosKode'], 'string', 'max' => 20],
            [['PosNama'], 'string', 'max' => 75],
            [['PosAlamat'], 'string', 'max' => 150],
            [['PosTelepon'], 'string', 'max' => 30],
            [['KarKode', 'NoAccount'], 'string', 'max' => 10],
            [['PosNomor'], 'string', 'max' => 2],
            [['PosStatus'], 'string', 'max' => 1],
            [['PosJenis'], 'string', 'max' => 15],
            [['PosKode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PosKode' => 'Pos Kode',
            'PosNama' => 'Pos Nama',
            'PosAlamat' => 'Pos Alamat',
            'PosTelepon' => 'Pos Telepon',
            'KarKode' => 'Kar Kode',
            'PosNomor' => 'Pos Nomor',
            'PosStatus' => 'Pos Status',
            'PosJenis' => 'Pos Jenis',
            'NoAccount' => 'No Account',
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
        return [
            'PosKode'          => ['width' => 120,'label' => 'Kode POS','name'  => 'PosKode'],
            'PosNama'          => ['width' => 220,'label' => 'Nama POS','name'  => 'PosNama'],
            'PosNomor'         => ['width' => 30,'label' => 'No','name'  => 'PosNomor'],
            'PosJenis'         => ['width' => 70,'label' => 'Jenis','name'  => 'PosJenis'],
            'Tdkaryawan.KarKode'          => ['width' => 100,'label' => 'Karyawan','name'  => 'KarKode'],
            'Traccount.NoAccount'        => ['width' => 70,'label' => 'NoAccount','name'  => 'NoAccount'],
            'PosAlamat'        => ['width' => 140,'label' => 'Alamat','name'  => 'PosAlamat'],
            'PosTelepon'       => ['width' => 100,'label' => 'Telepon','name'  => 'PosTelepon'],
			'PosStatus'        => ['width' => 60,'label' => 'Status','name'  => 'PosStatus'],
        ];
    }

	public function getKaryawan() {
		return $this->hasOne( Tdkaryawan::className(), [ 'KarKode' => 'KarKode' ] );
	}

	public function getAccount() {
		return $this->hasOne( Traccount::className(), [ 'NoAccount' => 'NoAccount' ] );
	}
    /**
     * {@inheritdoc}
     * @return TdposQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdposQuery(get_called_class());
    }
}
