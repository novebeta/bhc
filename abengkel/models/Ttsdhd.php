<?php
namespace abengkel\models;
use common\components\General;
/**
 * This is the model class for table "ttsdhd".
 *
 * @property string $SDNo
 * @property string $SVNo
 * @property string $SENo
 * @property string $NoGLSD
 * @property string $NoGLSV
 * @property string $SDTgl
 * @property string $SVTgl
 * @property string $KodeTrans
 * @property string $ASS
 * @property string $PosKode
 * @property string $KarKode
 * @property string $PrgNama
 * @property string $PKBNo
 * @property string $MotorNoPolisi
 * @property string $LokasiKode
 * @property string $SDNoUrut
 * @property string $SDJenis
 * @property string $SDTOP
 * @property string $SDDurasi
 * @property string $SDJamMasuk
 * @property string $SDJamSelesai
 * @property string $SDJamDikerjakan
 * @property string $SDTotalWaktu
 * @property string $SDStatus Menunggu, Diproses, Selesai, Lunas
 * @property string $SDKmSekarang
 * @property string $SDKmBerikut
 * @property string $SDKeluhan
 * @property string $SDKeterangan
 * @property string $SDPembawaMotor
 * @property string $SDHubunganPembawa
 * @property string $SDAlasanServis
 * @property string $SDTotalJasa
 * @property string $SDTotalQty
 * @property string $SDTotalPart
 * @property string $SDTotalGratis
 * @property string $SDTotalNonGratis
 * @property string $SDDiscFinal
 * @property string $SDTotalPajak
 * @property string $SDUangMuka
 * @property string $SDTotalBiaya
 * @property string $SDKasKeluar
 * @property string $SDTerbayar
 * @property string $KlaimKPB
 * @property string $KlaimC2
 * @property string $Cetak
 * @property string $UserID
 */
class Ttsdhd extends \yii\db\ActiveRecord {
	public static function tableName() {
		return 'ttsdhd';
	}
	public function rules() {
		return [
			[ [ 'SDNo', 'SVNo' ], 'required' ],
			[ [ 'SDTgl', 'SVTgl', 'SDJamMasuk', 'SDJamSelesai', 'SDJamDikerjakan' ], 'safe' ],
			[
				[
					'SDTOP',
					'SDDurasi',
					'SDTotalWaktu',
					'SDKmSekarang',
					'SDKmBerikut',
					'SDTotalJasa',
					'SDTotalQty',
					'SDTotalPart',
					'SDTotalGratis',
					'SDTotalNonGratis',
					'SDDiscFinal',
					'SDTotalPajak',
					'SDUangMuka',
					'SDTotalBiaya',
					'SDKasKeluar',
					'SDTerbayar'
				],
				'number'
			],
			[ [ 'SDNo', 'SVNo', 'SENo', 'NoGLSD', 'NoGLSV', 'ASS', 'KarKode' ], 'string', 'max' => 10 ],
			[ [ 'KodeTrans' ], 'string', 'max' => 4 ],
			[ [ 'PosKode', 'PKBNo' ], 'string', 'max' => 20 ],
			[ [ 'PrgNama', 'SDHubunganPembawa' ], 'string', 'max' => 25 ],
			[ [ 'MotorNoPolisi' ], 'string', 'max' => 12 ],
			[ [ 'LokasiKode', 'SDStatus', 'SDAlasanServis', 'UserID' ], 'string', 'max' => 15 ],
			[ [ 'SDNoUrut' ], 'string', 'max' => 3 ],
			[ [ 'SDJenis' ], 'string', 'max' => 6 ],
			[ [ 'SDKeluhan', 'SDKeterangan' ], 'string', 'max' => 150 ],
			[ [ 'SDPembawaMotor' ], 'string', 'max' => 75 ],
			[ [ 'KlaimKPB', 'KlaimC2', 'Cetak' ], 'string', 'max' => 5 ],
			[ [ 'SDNo', 'SVNo' ], 'unique', 'targetAttribute' => [ 'SDNo', 'SVNo' ] ],
		];
	}
	public function attributeLabels() {
		return [
			'SDNo'              => 'Sd No',
			'SVNo'              => 'Sv No',
			'SENo'              => 'Se No',
			'NoGLSD'            => 'No Glsd',
			'NoGLSV'            => 'No Glsv',
			'SDTgl'             => 'Sd Tgl',
			'SVTgl'             => 'Sv Tgl',
			'KodeTrans'         => 'Kode Trans',
			'ASS'               => 'Ass',
			'PosKode'           => 'Pos Kode',
			'KarKode'           => 'Kar Kode',
			'PrgNama'           => 'Prg Nama',
			'PKBNo'             => 'Pkb No',
			'MotorNoPolisi'     => 'Motor No Polisi',
			'LokasiKode'        => 'Lokasi Kode',
			'SDNoUrut'          => 'Sd No Urut',
			'SDJenis'           => 'Sd Jenis',
			'SDTOP'             => 'Sdtop',
			'SDDurasi'          => 'Sd Durasi',
			'SDJamMasuk'        => 'Sd Jam Masuk',
			'SDJamSelesai'      => 'Sd Jam Selesai',
			'SDJamDikerjakan'   => 'Sd Jam Dikerjakan',
			'SDTotalWaktu'      => 'Sd Total Waktu',
			'SDStatus'          => 'Sd Status',
			'SDKmSekarang'      => 'Sd Km Sekarang',
			'SDKmBerikut'       => 'Sd Km Berikut',
			'SDKeluhan'         => 'Sd Keluhan',
			'SDKeterangan'      => 'Sd Keterangan',
			'SDPembawaMotor'    => 'Sd Pembawa Motor',
			'SDHubunganPembawa' => 'Sd Hubungan Pembawa',
			'SDAlasanServis'    => 'Sd Alasan Servis',
			'SDTotalJasa'       => 'Sd Total Jasa',
			'SDTotalQty'        => 'Sd Total Qty',
			'SDTotalPart'       => 'Sd Total Part',
			'SDTotalGratis'     => 'Sd Total Gratis',
			'SDTotalNonGratis'  => 'Sd Total Non Gratis',
			'SDDiscFinal'       => 'Sd Disc Final',
			'SDTotalPajak'      => 'Sd Total Pajak',
			'SDUangMuka'        => 'Sd Uang Muka',
			'SDTotalBiaya'      => 'Sd Total Biaya',
			'SDKasKeluar'       => 'Sd Kas Keluar',
			'SDTerbayar'        => 'Sd Terbayar',
			'KlaimKPB'          => 'Klaim Kpb',
			'KlaimC2'           => 'Klaim C2',
			'Cetak'             => 'Cetak',
			'UserID'            => 'User ID',
		];
	}
	public static function colGrid() {
		return [
			'SDNo'                     => [ 'width' => 70, 'label' => 'No SD', 'name' => 'SDNo' ],
			'SDTgl'                    => [ 'width' => 115, 'label' => 'Tgl SD', 'name' => 'SDTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y H:i:s' ] ],
			'MotorNoPolisi'            => [ 'width' => 90, 'label' => 'No Polisi', 'name' => 'MotorNoPolisi' ],
			'SVNo'                     => [ 'width' => 70, 'label' => 'No SV', 'name' => 'SVNo' ],
			'CusNama'                  => [ 'width' => 200, 'label' => 'Nama Konsumen', 'name' => 'CusNama' ],
			'MotorNama'                => [ 'width' => 150, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
			'PKBNo'                    => [ 'width' => 60, 'label' => 'No PKB', 'name' => 'PKBNo' ],
			'SDStatus'                 => [ 'width' => 60, 'label' => 'Status', 'name' => 'SDStatus' ],
			'KodeTrans'                => [ 'width' => 50, 'label' => 'TC', 'name' => 'KodeTrans' ],
			'KarKode'                  => [ 'width' => 71, 'label' => 'Mekanik', 'name' => 'KarKode' ],
			'NoGLSD'                   => [ 'width' => 70, 'label' => 'No GL SD', 'name' => 'NoGLSD' ],
			'NoGLSV'                   => [ 'width' => 70, 'label' => 'No GL SV', 'name' => 'NoGLSV' ],
			'Cetak'                    => [ 'width' => 50, 'label' => 'Cetak', 'name' => 'Cetak' ],
			'UserID'                   => [ 'width' => 70, 'label' => 'UserID', 'name' => 'UserID' ],
		];
	}
	public static function colGridDaftarServis() {
		$arr = [
			'SDNo'                     => [ 'width' => 70, 'label' => 'No SD', 'name' => 'SDNo' ],
			'SDTgl'                    => [ 'width' => 115, 'label' => 'Tgl SD', 'name' => 'SDTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y H:i:s' ] ],
			'tdcustomer.MotorNoPolisi'            => [ 'width' => 90, 'label' => 'No Polisi', 'name' => 'MotorNoPolisi' ],
			'SVNo'                     => [ 'width' => 70, 'label' => 'No SV', 'name' => 'SVNo' ],
			'tdcustomer.CusNama'                  => [ 'width' => 185, 'label' => 'Nama Konsumen', 'name' => 'CusNama' ],
			'tdcustomer.MotorNama'                => [ 'width' => 160, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
			'PKBNo'                    => [ 'width' => 60, 'label' => 'No PKB', 'name' => 'PKBNo' ],
			'SDStatus'                 => [ 'width' => 60, 'label' => 'Status', 'name' => 'SDStatus' ],
			'KodeTrans'                => [ 'width' => 50, 'label' => 'TC', 'name' => 'KodeTrans' ],
			'KarKode'                  => [ 'width' => 80, 'label' => 'Mekanik', 'name' => 'KarKode' ],
			'NoGLSD'                   => [ 'width' => 70, 'label' => 'No GL SD', 'name' => 'NoGLSD' ],
			'NoGLSV'                   => [ 'width' => 70, 'label' => 'No GL SV', 'name' => 'NoGLSV' ],
			'SDTotalBiaya'             => [ 'width' => 100, 'label' => 'Total', 'name' => 'SDTotalBiaya', 'formatter' => 'number', 'align' => 'right' ],
			'SDTerbayar'               => [ 'width' => 100, 'label' => 'Terbayar', 'name' => 'SDTerbayar', 'formatter' => 'number', 'align' => 'right' ],
			'ASS'                      => [ 'width' => 70, 'label' => 'ASS', 'name' => 'ASS' ],
			'KlaimKPB'                 => [ 'width' => 50, 'label' => 'KlaimKPB', 'name' => 'KlaimKPB' ],
			'KlaimC2'                  => [ 'width' => 50, 'label' => 'KlaimC2', 'name' => 'KlaimC2' ],
			'SDNoUrut'                 => [ 'width' => 50, 'label' => 'NoUrut', 'name' => 'SDNoUrut' ],
			'PosKode'                  => [ 'width' => 90, 'label' => 'Pos', 'name' => 'PosKode' ],
			'Cetak'                    => [ 'width' => 50, 'label' => 'Cetak', 'name' => 'Cetak' ],
			'UserID'                   => [ 'width' => 80, 'label' => 'UserID', 'name' => 'UserID' ],
			'SDKeterangan'             => [ 'width' => 300, 'label' => 'Keterangan', 'name' => 'SDKeterangan' ],
		];
		if ( \abengkel\components\Menu::showOnlyHakAkses( [ 'warehouse', 'gudang' ] ) ) {
			$arr[ 'PLCetak' ] = [ 'width' => 50, 'label' => 'PLCetak', 'name' => 'PLCetak' ];
		}
		return $arr;
	}
	public static function colGridInvoiceServis() {
		$arr = [
			'SDNo'                     => [ 'width' => 70, 'label' => 'No SD', 'name' => 'SDNo' ],
			'SDTgl'                    => [ 'width' => 115, 'label' => 'Tgl SD', 'name' => 'SDTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y H:i:s' ] ],
			'tdcustomer.MotorNoPolisi'            => [ 'width' => 90, 'label' => 'No Polisi', 'name' => 'MotorNoPolisi' ],
			'SVNo'                     => [ 'width' => 70, 'label' => 'No Inv', 'name' => 'SVNo' ],
			'tdcustomer.CusNama'                  => [ 'width' => 185, 'label' => 'Nama Konsumen', 'name' => 'CusNama' ],
			'tdcustomer.MotorNama'                => [ 'width' => 160, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
			'PKBNo'                    => [ 'width' => 60, 'label' => 'No PKB', 'name' => 'PKBNo' ],
			'SDStatus'                 => [ 'width' => 60, 'label' => 'Status', 'name' => 'SDStatus' ],
			'KodeTrans'                => [ 'width' => 50, 'label' => 'TC', 'name' => 'KodeTrans' ],
			'KarKode'                  => [ 'width' => 80, 'label' => 'Mekanik', 'name' => 'KarKode' ],
			'NoGLSD'                   => [ 'width' => 70, 'label' => 'No GL SD', 'name' => 'NoGLSD' ],
			'NoGLSV'                   => [ 'width' => 70, 'label' => 'No GL SV', 'name' => 'NoGLSV' ],
			'SDTotalBiaya'             => [ 'width' => 100, 'label' => 'Total', 'name' => 'SDTotalBiaya', 'formatter' => 'number', 'align' => 'right' ],
			'SDTerbayar'               => [ 'width' => 100, 'label' => 'Terbayar', 'name' => 'SDTerbayar', 'formatter' => 'number', 'align' => 'right' ],
			'ASS'                      => [ 'width' => 70, 'label' => 'ASS', 'name' => 'ASS' ],
			'KlaimKPB'                 => [ 'width' => 50, 'label' => 'KlaimKPB', 'name' => 'KlaimKPB' ],
			'KlaimC2'                  => [ 'width' => 50, 'label' => 'KlaimC2', 'name' => 'KlaimC2' ],
			'SDNoUrut'                 => [ 'width' => 50, 'label' => 'NoUrut', 'name' => 'SDNoUrut' ],
			'PosKode'                  => [ 'width' => 90, 'label' => 'Pos', 'name' => 'PosKode' ],
			'Cetak'                    => [ 'width' => 50, 'label' => 'Cetak', 'name' => 'Cetak' ],
			'UserID'                   => [ 'width' => 80, 'label' => 'UserID', 'name' => 'UserID' ],
			'SDKeterangan'             => [ 'width' => 300, 'label' => 'Keterangan', 'name' => 'SDKeterangan' ],
		];
		if ( \abengkel\components\Menu::showOnlyHakAkses( [ 'warehouse', 'gudang' ] ) ) {
			$arr[ 'PLCetak' ] = [ 'width' => 50, 'label' => 'PLCetak', 'name' => 'PLCetak' ];
		}
		return $arr;
	}
	public static function colGridInvoiceServisSelect() {
		return [
			'SVNo'                     => [ 'width' => 70, 'label' => 'No Servis', 'name' => 'SVNo' ],
			'SVTgl'                    => [ 'width' => 115, 'label' => 'Tgl Servis', 'name' => 'SVTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y H:i:s' ] ],
			'tdcustomer.MotorNoPolisi' => [ 'width' => 90, 'label' => 'No Polisi', 'name' => 'MotorNoPolisi' ],
			'tdcustomer.CusNama'       => [ 'width' => 185, 'label' => 'Nama Konsumen', 'name' => 'CusNama' ],
			'tdcustomer.MotorNama'     => [ 'width' => 160, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
			'SDTotalBiaya'             => [ 'width' => 100, 'label' => 'Total', 'name' => 'SDTotalBiaya', 'formatter' => 'number', 'align' => 'right' ],
			'SDTerbayar'               => [ 'width' => 100, 'label' => 'Terbayar', 'name' => 'SDTerbayar', 'formatter' => 'number', 'align' => 'right' ],
		];
	}
	public static function colGridSD() {
		return [
			'SDNo'                     => [ 'width' => 70, 'label' => 'No SD', 'name' => 'SDNo' ],
			'SDTgl'                    => [ 'width' => 115, 'label' => 'Tgl SD', 'name' => 'SDTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y H:i:s' ] ],
			'MotorNoPolisi'            => [ 'width' => 90, 'label' => 'No Polisi', 'name' => 'MotorNoPolisi' ],
			'CusNama'                  => [ 'width' => 200, 'label' => 'Nama Konsumen', 'name' => 'CusNama' ],
			'LokasiKode'               => [ 'width' => 200, 'label' => 'Lokasi', 'name' => 'LokasiKode' ],
			'KodeTrans'                => [ 'width' => 50, 'label' => 'SD', 'name' => 'KodeTrans' ],
			'KarKode'                  => [ 'width' => 50, 'label' => 'Karyawan', 'name' => 'KarKode' ],
			'SDTerbayar'               => [ 'width' => 50, 'label' => 'Uang Muka', 'name' => 'SDTerbayar' ],
			'SDKeterangan'             => [ 'width' => 50, 'label' => 'Keterangan', 'name' => 'SDKeterangan' ],
		];
	}
	public static function colGridKlaim() {
		return [
			'NO'                       => [ 'width' => 70, 'label' => 'No Servis', 'name' => 'SDNo' ],
			'SDTgl'                    => [ 'width' => 115, 'label' => 'Tgl Servis', 'name' => 'SDTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y H:i:s' ] ],
			'MotorNama'                => [ 'width' => 90, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
			'MotorNoPolisi'            => [ 'width' => 90, 'label' => 'No Polisi', 'name' => 'MotorNoPolisi' ],
			'CusNama'                  => [ 'width' => 200, 'label' => 'Nama Konsumen', 'name' => 'CusNama' ],
			'SDTotalJasa'              => [ 'width' => 50, 'label' => 'Total Jasa', 'name' => 'SDTotalJasa' ],
			'SDTotalPart'              => [ 'width' => 50, 'label' => 'Total Part', 'name' => 'SDTotalPart' ],
			'SDTotal'                  => [ 'width' => 50, 'label' => 'Total', 'name' => 'SDTotalBiaya' ],
		];
	}

	public static function colGridImportItems()
    {
        return [
            'noPolisi' => ['label' => 'No Polisi','width' => 80, 'name' => 'noPolisi'],
            'namaPemilik' => ['label' => 'Nama Pemilik','width' => 170, 'name' => 'namaPemilik'],
            'noMesin' => ['label' => 'No Mesin','width' => 100, 'name' => 'noMesin'],
            'noRangka' => ['label' => 'No Rangka','width' => 130, 'name' => 'noRangka'],
            'noSAForm' => ['label' => 'No SA Form','width' => 120, 'name' => 'noSAForm'],
            'noTelpPembawa' => ['label' => 'No Telp Pembawa','width' => 110, 'name' => 'noTelpPembawa'],
            'noWorkOrderJobReturn' => ['label' => 'No Work Order Job Return','width' => 150, 'name' => 'noWorkOrderJobReturn'],
            'alamatPembawa' => ['label' => 'Alamat Pembawa','width' => 120, 'name' => 'alamatPembawa'],
            'alamatPemilik' => ['label' => 'Alamat Pemilik','width' => 120, 'name' => 'alamatPemilik'],
            'asalUnitEntry' => ['label' => 'Asal Unit Entry','width' => 100, 'name' => 'asalUnitEntry'],
            'catatanTambahan' => ['label' => 'Catatan Tambahan','width' => 120, 'name' => 'catatanTambahan'],
            'createdTime' => ['label' => 'Created Time','width' => 130, 'name' => 'createdTime'],
            'hondaIdMekanik' => ['label' => 'Honda Id Mekanik','width' => 110, 'name' => 'hondaIdMekanik'],
            'hondaIdSA' => ['label' => 'Honda Id SA','width' => 80, 'name' => 'hondaIdSA'],
            'hubunganDenganPemilik' => ['label' => 'Hubungan Dengan Pemilik','width' => 150, 'name' => 'hubunganDenganPemilik'],
            'idPIT' => ['label' => 'Id PIT','width' => 60, 'name' => 'idPIT'],
            'informasiBensin' => ['label' => 'Informasi Bensin','width' => 100, 'name' => 'informasiBensin'],
            'jenisPIT' => ['label' => 'Jenis PIT','width' => 80, 'name' => 'jenisPIT'],
            'keluhanKonsumen' => ['label' => 'Keluhan Konsumen','width' => 100, 'name' => 'keluhanKonsumen'],
            'kmTerakhir' => ['label' => 'Km Terakhir','width' => 90, 'name' => 'kmTerakhir'],
            'kodeKecamatanPembawa' => ['label' => 'Kode Kecamatan Pembawa','width' => 150, 'name' => 'kodeKecamatanPembawa'],
            'kodeKecamatanPemilik' => ['label' => 'Kode Kecamatan Pemilik','width' => 150, 'name' => 'kodeKecamatanPemilik'],
            'kodeKelurahanPembawa' => ['label' => 'Kode Kelurahan Pembawa','width' => 150, 'name' => 'kodeKelurahanPembawa'],
            'kodeKelurahanPemilik' => ['label' => 'Kode Kelurahan Pemilik','width' => 150, 'name' => 'kodeKelurahanPemilik'],
            'kodeKotaPembawa' => ['label' => 'Kode Kota Pembawa','width' => 130, 'name' => 'kodeKotaPembawa'],
            'kodeKotaPemilik' => ['label' => 'Kode Kota Pemilik','width' => 130, 'name' => 'kodeKotaPemilik'],
            'namaPembawa' => ['label' => 'Nama Pembawa','width' => 150, 'name' => 'namaPembawa'],
            'kodePosPembawa' => ['label' => 'Kode Pos Pembawa','width' => 130, 'name' => 'kodePosPembawa'],
            'kodePosPemilik' => ['label' => 'Kode Pos Pemilik','width' => 130, 'name' => 'kodePosPemilik'],
            'kodePropinsiPembawa' => ['label' => 'Kode Propinsi Pembawa','width' => 130, 'name' => 'kodePropinsiPembawa'],
            'kodePropinsiPemilik' => ['label' => 'Kode Propinsi Pemilik','width' => 130, 'name' => 'kodePropinsiPemilik'],
            'kodeTipeUnit' => ['label' => 'Kode Tipe Unit','width' => 90, 'name' => 'kodeTipeUnit'],
            'konfirmasiPekerjaanTambahan' => ['label' => 'Konfirmasi Pekerjaan Tambahan','width' => 190, 'name' => 'konfirmasiPekerjaanTambahan'],
            'rekomendasiSA' => ['label' => 'Rekomendasi SA','width' => 130, 'name' => 'rekomendasiSA'],
            'saranMekanik' => ['label' => 'Saran Mekanik','width' => 100, 'name' => 'saranMekanik'],
            'setUpPembayaran' => ['label' => 'Set Up Pembayaran','width' => 120, 'name' => 'setUpPembayaran'],
            'statusWorkOrder' => ['label' => 'Status Work Order','width' => 120, 'name' => 'statusWorkOrder'],
            'tahunMotor' => ['label' => 'Tahun Motor','width' => 90, 'name' => 'tahunMotor'],
            'tanggalServis' => ['label' => 'Tgl Servis','width' => 100, 'name' => 'tanggalServis'],
            'tipeComingCustomer' => ['label' => 'Tipe Coming Customer','width' => 130, 'name' => 'tipeComingCustomer'],
            'totalBiayaService' => ['label' => 'Total Biaya Service','width' => 120, 'name' => 'totalBiayaService', 'formatter' => 'number'],
            'totalFRT' => ['label' => 'Total FRT','width' => 80, 'name' => 'totalFRT'],
            'waktuPKB' => ['label' => 'Waktu PKB','width' => 120, 'name' => 'waktuPKB'],
            'waktuPekerjaan' => ['label' => 'Waktu Pekerjaan','width' => 100, 'name' => 'waktuPekerjaan'],
            'waktuPendaftaran' => ['label' => 'Waktu Pendaftaran','width' => 120, 'name' => 'waktuPendaftaran'],
            'waktuSelesai' => ['label' => 'Waktu Selesai','width' => 120, 'name' => 'waktuSelesai'],
            'noWorkOrder' => [ 'label' => 'No Work Order','width' => 120, 'name' => 'noWorkOrder'],
            'noBukuClaimC2' => ['label' => 'No Buku Claim C2','width' => 110, 'name' => 'noBukuClaimC2'],
        ];
    }

	public function getCustomer() {
		return $this->hasOne( Tdcustomer::className(), [ 'MotorNoPolisi' => 'MotorNoPolisi' ] );
	}
	public function getDetilBarang() {
		return $this->hasOne( Ttsditbarang::className(), [ 'SDNo' => 'SDNo' ] );
	}
	public static function find() {
		return new TtsdhdQuery( get_called_class() );
	}
	public static function getRoute( $SDJenis, $param = [] ) {
		$index  = [];
		$update = [];
		$create = [];
		switch ( $SDJenis ) {
			case 'SD' :
				$index  = [ 'ttsdhd/daftar-servis' ];
				$update = [ 'ttsdhd/daftar-servis-update' ];
				$create = [ 'ttsdhd/daftar-servis-create' ];
				break;
			case 'SV' :
				$index  = [ 'ttsdhd/invoice-servis' ];
				$update = [ 'ttsdhd/invoice-servis-update' ];
				$create = [ 'ttsdhd/invoice-servis-create' ];
				break;
		}
		return [
			'index'  => \yii\helpers\Url::toRoute( array_merge( $index, $param ) ),
			'update' => \yii\helpers\Url::toRoute( array_merge( $update, $param ) ),
			'create' => \yii\helpers\Url::toRoute( array_merge( $create, $param ) ),
		];
	}

    public static function colGridPushInv2()
    {
        return [
            'ZnoWorkOrder' => ['width' => 130, 'name' => 'ZnoWorkOrder', 'label' => 'Z No Work Order'],
            'noNJB' => ['width' => 75, 'name' => 'noNJB', 'label' => 'No NJB'],
            'tanggalNJB' => ['width' => 130, 'name' => 'tanggalNJB', 'label' => 'Tgl NJB'],
            'totalHargaNJB' => ['width' => 100, 'name' => 'totalHargaNJB', 'label' => 'Total Harga NJB', 'formatter' => 'number',],
            'noNSC' => ['width' => 75, 'name' => 'noNSC', 'label' => 'No NSC'],
            'tanggalNSC' => ['width' => 130, 'name' => 'tanggalNSC', 'label' => 'Tgl NCS'],
            'totalHargaNSC' => ['width' => 120, 'name' => 'totalHargaNSC', 'label' => 'Total Harga NSC', 'formatter' => 'number',],
            'HondaidSA' => ['width' => 95, 'name' => 'HondaidSA', 'label' => 'Honda Id SA'],
            'HondaidMekanik' => ['width' => 120, 'name' => 'HondaidMekanik', 'label' => 'Honda Id Mekanik'],
        ];
    }

    public static function updateMekanik($SDNo){
    	return General::cCmd("UPDATE ttsdhd
		LEFT OUTER JOIN
		(SELECT  ttsdittime.SDNo AS SDNo, IFNULL(ttsdittime.KarKode,'--') AS KarKode, STATUS AS MyStatus FROM  ttsdhd
		LEFT OUTER JOIN ttsdittime ON ttsdittime.SDNo = ttsdhd.SDNO
		WHERE (STATUS = 'Selesai' OR STATUS = 'Istirahat') AND ttsdhd.SDNo = :SDNo) Mekanik
		ON Mekanik.SDNo = ttsdhd.SDNo
		SET ttsdhd.Karkode = IFNULL(Mekanik.KarKode,'--'), ttsdhd.SDStatus = IFNULL(Mekanik.MyStatus,'Menunggu')
		WHERE ttsdhd.SDNo = :SDNo",[':SDNo'=>$SDNo])->execute();
    }
}
