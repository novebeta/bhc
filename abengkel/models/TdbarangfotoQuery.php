<?php

namespace abengkel\models;

/**
 * This is the ActiveQuery class for [[Tdbarangfoto]].
 *
 * @see Tdbarangfoto
 */
class TdbarangfotoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdbarangfoto[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdbarangfoto|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
