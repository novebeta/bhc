<?php

namespace abengkel\models;

/**
 * This is the model class for table "vtkassaldo".
 *
 * @property string $KasNo
 * @property string $KasTgl
 * @property string $KasJenis
 * @property string $KasKode
 * @property string $KodePerson
 * @property string $KasMemo
 * @property string $KasDebet
 * @property string $KasKredit
 * @property string $KasPerson
 * @property string $UserID
 * @property string $KodeTrans
 * @property int $KasSaldo
 * @property string $NoGL
 */
class Vtkassaldo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vtkassaldo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KasTgl'], 'safe'],
            [['KasDebet', 'KasKredit'], 'number'],
            [['KasSaldo'], 'integer'],
            [['KasNo'], 'string', 'max' => 12],
            [['KasJenis'], 'string', 'max' => 20],
            [['KasKode'], 'string', 'max' => 25],
            [['KodePerson', 'UserID'], 'string', 'max' => 15],
            [['KasMemo'], 'string', 'max' => 150],
            [['KasPerson'], 'string', 'max' => 50],
            [['KodeTrans'], 'string', 'max' => 4],
            [['NoGL'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'KasNo' => 'Kas No',
            'KasTgl' => 'Kas Tgl',
            'KasJenis' => 'Kas Jenis',
            'KasKode' => 'Kas Kode',
            'KodePerson' => 'Kode Person',
            'KasMemo' => 'Kas Memo',
            'KasDebet' => 'Kas Debet',
            'KasKredit' => 'Kas Kredit',
            'KasPerson' => 'Kas Person',
            'UserID' => 'User ID',
            'KodeTrans' => 'Kode Trans',
            'KasSaldo' => 'Kas Saldo',
            'NoGL' => 'No Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VtkassaldoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VtkassaldoQuery(get_called_class());
    }
}
