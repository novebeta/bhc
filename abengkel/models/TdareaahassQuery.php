<?php

namespace abengkel\models;

/**
 * This is the ActiveQuery class for [[Tdareaahass]].
 *
 * @see Tdareaahass
 */
class TdareaahassQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdareaahass[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdareaahass|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
