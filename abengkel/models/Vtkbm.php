<?php

namespace abengkel\models;

/**
 * This is the model class for table "vtkbm".
 *
 * @property string $KBMNo
 * @property string $KBMTgl
 * @property string $KBMLink
 * @property string $KBMBayar
 * @property string $Kode
 * @property string $Person
 * @property string $Memo
 * @property string $NoGL
 */
class Vtkbm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vtkbm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KBMTgl'], 'safe'],
            [['KBMBayar'], 'number'],
            [['KBMNo'], 'string', 'max' => 12],
            [['KBMLink'], 'string', 'max' => 18],
            [['Kode'], 'string', 'max' => 25],
            [['Person'], 'string', 'max' => 50],
            [['Memo'], 'string', 'max' => 150],
            [['NoGL'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'KBMNo' => 'Kbm No',
            'KBMTgl' => 'Kbm Tgl',
            'KBMLink' => 'Kbm Link',
            'KBMBayar' => 'Kbm Bayar',
            'Kode' => 'Kode',
            'Person' => 'Person',
            'Memo' => 'Memo',
            'NoGL' => 'No Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VtkbmQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VtkbmQuery(get_called_class());
    }
}
