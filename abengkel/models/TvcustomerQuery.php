<?php

namespace abengkel\models;

/**
 * This is the ActiveQuery class for [[Tvcustomer]].
 *
 * @see Tvcustomer
 */
class TvcustomerQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tvcustomer[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tvcustomer|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
