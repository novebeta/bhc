<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttujit]].
 *
 * @see Ttujit
 */
class TtujitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttujit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttujit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'UJNo' ]          = $post[ 'UJNo' ] ?? '--';
        $post[ 'UJAuto' ]        = floatval( $post[ 'UJAuto' ] ?? 0 );
        $post[ 'JasaKode' ]      = $post[ 'JasaKode' ] ?? '--';
        $post[ 'UJHrgJualLama' ] = floatval( $post[ 'UJHrgJualLama' ] ?? 0 );
        $post[ 'UJHrgJualBaru' ] = floatval( $post[ 'UJHrgJualBaru' ] ?? 0 );
        $post[ 'UJHrgBeliLama' ] = floatval( $post[ 'UJHrgBeliLama' ] ?? 0 );
        $post[ 'UJHrgBeliBaru' ] = floatval( $post[ 'UJHrgBeliBaru' ] ?? 0 );
        $post[ 'UJNoOLD' ]       = $post[ 'UJNoOLD' ] ?? '--';
        $post[ 'UJAutoOLD' ]     = floatval( $post[ 'UJAutoOLD' ] ?? 0 );
        $post[ 'JasaKodeOLD' ]   = $post[ 'JasaKodeOLD' ] ?? '--';
        try {
            General::cCmd( "CALL fujit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'UJNo' ] )
                ->bindParam( 6, $post[ 'UJAuto' ] )
                ->bindParam( 7, $post[ 'JasaKode' ] )
                ->bindParam( 8, $post[ 'UJHrgJualLama' ] )
                ->bindParam( 9, $post[ 'UJHrgJualBaru' ] )
                ->bindParam( 10, $post[ 'UJHrgBeliLama' ] )
                ->bindParam( 11, $post[ 'UJHrgBeliBaru' ] )
                ->bindParam( 12, $post[ 'UJNoOLD' ] )
                ->bindParam( 13, $post[ 'UJAutoOLD' ] )
                ->bindParam( 14, $post[ 'JasaKodeOLD' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
