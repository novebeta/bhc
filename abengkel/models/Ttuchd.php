<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttuchd".
 *
 * @property string $UCNo
 * @property string $UCTgl
 * @property string $KodeTrans
 * @property string $PosKode
 * @property string $UCHrgLama
 * @property string $UCHrgBaru
 * @property string $UCSelisih
 * @property string $UCKeterangan
 * @property string $NoGL
 * @property string $Cetak
 * @property string $UserID
 */
class Ttuchd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttuchd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['UCNo'], 'required'],
            [['UCTgl'], 'safe'],
            [['UCHrgLama', 'UCHrgBaru', 'UCSelisih'], 'number'],
            [['UCNo', 'NoGL'], 'string', 'max' => 10],
            [['KodeTrans'], 'string', 'max' => 4],
            [['PosKode'], 'string', 'max' => 20],
            [['UCKeterangan'], 'string', 'max' => 150],
            [['Cetak'], 'string', 'max' => 5],
            [['UserID'], 'string', 'max' => 15],
            [['UCNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'UCNo'         => 'Uc No',
            'UCTgl'        => 'Uc Tgl',
            'KodeTrans'    => 'Kode Trans',
            'PosKode'      => 'Pos Kode',
            'UCHrgLama'    => 'Uc Hrg Lama',
            'UCHrgBaru'    => 'Uc Hrg Baru',
            'UCSelisih'    => 'Uc Selisih',
            'UCKeterangan' => 'Uc Keterangan',
            'NoGL'         => 'No Gl',
            'Cetak'        => 'Cetak',
            'UserID'       => 'User ID',
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
        return [
            'UCNo'          => ['width' => 90,'label' => 'No UC','name'  => 'UCNo'],
            'UCTgl'         => ['width' => 120,'label' => 'Tanggal UC','name'  => 'UCTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y H:i:s']],
            'KodeTrans'     => ['width' => 60,'label' => 'TC','name'  => 'KodeTrans'],
            'PosKode'       => ['width' => 100,'label' => 'Pos Kode','name'  => 'PosKode'],
            'UCHrgLama'     => ['width' => 105,'label' => 'Hrg Lama','name'  => 'UCHrgLama', 'formatter' => 'number', 'align' => 'right'],
            'UCHrgBaru'     => ['width' => 105,'label' => 'Hrg Baru','name'  => 'UCHrgBaru', 'formatter' => 'number', 'align' => 'right'],
            'UCSelisih'     => ['width' => 105,'label' => 'Selisih','name'  => 'UCSelisih', 'formatter' => 'number', 'align' => 'right'],
            'NoGL'          => ['width' => 80,'label' => 'NoGL','name'  => 'NoGL'],
            'Cetak'         => ['width' => 70,'label' => 'Cetak','name'  => 'Cetak'],
            'UserID'        => ['width' => 105,'label' => 'UserID','name'  => 'UserID'],
			'UCKeterangan'  => ['width' => 300, 'label' => 'Keterangan', 'name' => 'UCKeterangan' ],		
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtuchdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtuchdQuery(get_called_class());
    }
}
