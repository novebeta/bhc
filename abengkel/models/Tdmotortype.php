<?php

namespace abengkel\models;

/**
 * This is the model class for table "tdmotortype".
 *
 * @property string $MotorType
 * @property string $MotorNama
 * @property string $MotorKategori
 * @property string $MotorCC
 * @property string $MotorStatus
 * @property string $MotorNoMesin
 * @property string $MotorNoRangka
 */
class Tdmotortype extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdmotortype';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['MotorType'], 'required'],
            [['MotorType'], 'string', 'max' => 50],
            [['MotorNama'], 'string', 'max' => 100],
            [['MotorKategori'], 'string', 'max' => 15],
            [['MotorCC'], 'string', 'max' => 3],
            [['MotorStatus'], 'string', 'max' => 1],
            [['MotorNoMesin', 'MotorNoRangka'], 'string', 'max' => 10],
            [['MotorType'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'MotorType'     => 'Motor Type',
            'MotorNama'     => 'Motor Nama',
            'MotorKategori' => 'Motor Kategori',
            'MotorCC'       => 'Motor Cc',
            'MotorStatus'   => 'Motor Status',
            'MotorNoMesin'  => 'Motor No Mesin',
            'MotorNoRangka' => 'Motor No Rangka',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
        return [
            'MotorType'     => ['width' => 200,'label' => 'Type Motor','name'  => 'MotorType'],
            'MotorNama'     => ['width' => 300,'label' => 'Nama Motor','name'  => 'MotorNama'],
            'MotorKategori' => ['width' => 90,'label' => 'Kategori','name'  => 'MotorKategori'],
            'MotorCC'       => ['width' => 50,'label' => 'CC','name'  => 'MotorCC'],
            'MotorNoMesin'  => ['width' => 80,'label' => 'No Mesin','name'  => 'MotorNoMesin'],
            'MotorNoRangka' => ['width' => 80,'label' => 'No Rangka','name'  => 'MotorNoRangka'],
            'MotorStatus'   => ['width' => 60,'label' => 'Status','name'  => 'MotorStatus'],
        ];
    }

    /**
     * {@inheritdoc}
     * @return TdmotortypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdmotortypeQuery(get_called_class());
    }
}
