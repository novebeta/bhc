<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttccit".
 *
 * @property string $CCNo
 * @property string $SDNo
 * @property string $CCPart
 * @property string $CCJasa
 */
class Ttccit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttccit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CCNo', 'SDNo'], 'required'],
            [['CCPart', 'CCJasa'], 'number'],
            [['CCNo', 'SDNo'], 'string', 'max' => 10],
            [['CCNo', 'SDNo'], 'unique', 'targetAttribute' => ['CCNo', 'SDNo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CCNo' => 'Cc No',
            'SDNo' => 'Sd No',
            'CCPart' => 'Cc Part',
            'CCJasa' => 'Cc Jasa',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtccitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtccitQuery(get_called_class());
    }
}
