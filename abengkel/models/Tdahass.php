<?php

namespace abengkel\models;

/**
 * This is the model class for table "tdahass".
 *
 * @property string $AHASSNomor
 * @property string $AHASSNama
 * @property string $AHASSAlamat
 * @property string $AHASSTelepon
 * @property string $AHASSKontak
 * @property string $AHASSJenisLayanan
 * @property string $AHASSJenisDealer
 * @property string $AHASSProvinsi
 * @property string $AHASSKecamatan
 * @property string $AHASSKabupaten
 * @property string $AHASSMainDealer
 * @property string $AHASSStatus
 */
class Tdahass extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdahass';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AHASSNomor'], 'required'],
            [['AHASSNomor'], 'string', 'max' => 10],
            [['AHASSNama', 'AHASSKontak', 'AHASSMainDealer'], 'string', 'max' => 75],
            [['AHASSAlamat'], 'string', 'max' => 150],
            [['AHASSTelepon', 'AHASSKabupaten'], 'string', 'max' => 50],
            [['AHASSJenisLayanan'], 'string', 'max' => 20],
            [['AHASSJenisDealer'], 'string', 'max' => 15],
            [['AHASSProvinsi', 'AHASSKecamatan'], 'string', 'max' => 30],
            [['AHASSStatus'], 'string', 'max' => 1],
            [['AHASSNomor'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AHASSNomor' => 'Ahass Nomor',
            'AHASSNama' => 'Ahass Nama',
            'AHASSAlamat' => 'Ahass Alamat',
            'AHASSTelepon' => 'Ahass Telepon',
            'AHASSKontak' => 'Ahass Kontak',
            'AHASSJenisLayanan' => 'Ahass Jenis Layanan',
            'AHASSJenisDealer' => 'Ahass Jenis Dealer',
            'AHASSProvinsi' => 'Ahass Provinsi',
            'AHASSKecamatan' => 'Ahass Kecamatan',
            'AHASSKabupaten' => 'Ahass Kabupaten',
            'AHASSMainDealer' => 'Ahass Main Dealer',
            'AHASSStatus' => 'Ahass Status',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
        return [
            'AHASSNomor'        => ['width' => 100,'label' => 'Nomor AHASS','name'  => 'AHASSNomor'],
            'AHASSNama'         => ['width' => 175,'label' => 'Nama AHASS','name'  => 'AHASSNama'],
            'AHASSJenisLayanan' => ['width' => 90,'label' => 'Jenis Layanan','name'  => 'AHASSJenisLayanan'],
            'AHASSJenisDealer'  => ['width' => 90,'label' => 'Jenis Dealer','name'  => 'AHASSJenisDealer'],
            'AHASSProvinsi'     => ['width' => 100,'label' => 'Provinsi','name'  => 'AHASSProvinsi'],
            'AHASSKabupaten'    => ['width' => 125,'label' => 'Kabupaten','name'  => 'AHASSKabupaten'],
            'AHASSKecamatan'    => ['width' => 125,'label' => 'Kecamatan','name'  => 'AHASSKecamatan'],
            'AHASSMainDealer'   => ['width' => 125,'label' => 'Main Dealer','name'  => 'AHASSMainDealer'],
            'AHASSAlamat'       => ['width' => 275,'label' => 'Alamat','name'  => 'AHASSAlamat'],
            'AHASSTelepon'      => ['width' => 175,'label' => 'Telepon','name'  => 'AHASSTelepon'],
            'AHASSStatus'       => ['width' => 55,'label' => 'Status','name'  => 'AHASSStatus'],
        ];
    }

    /**
     * {@inheritdoc}
     * @return TdahassQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdahassQuery(get_called_class());
    }
}
