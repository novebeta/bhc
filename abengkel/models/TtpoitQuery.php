<?php

namespace abengkel\models;
use common\components\General;
use abengkel\components\Menu;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttpoit]].
 *
 * @see Ttpoit
 */
class TtpoitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttpoit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttpoit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]    = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'PONo' ]       = $post[ 'PONo' ] ?? '--';
        $post[ 'POAuto' ]     = floatval( $post[ 'POAuto' ] ?? 0 );
        $post[ 'BrgKode' ]    = $post[ 'BrgKode' ] ?? '--';
        $post[ 'POQty' ]      = floatval( $post[ 'POQty' ] ?? 0 );
        $post[ 'POHrgBeli' ]  = floatval( $post[ 'POHrgBeli' ] ?? 0 );
        $post[ 'POHrgJual' ]  = floatval( $post[ 'POHrgJual' ] ?? 0 );
        $post[ 'PODiscount' ] = floatval( $post[ 'PODiscount' ] ?? 0 );
        $post[ 'POPajak' ]    = floatval( $post[ 'POPajak' ] ?? 0 );
        $post[ 'SONo' ]       = $post[ 'SONo' ] ?? '--';
        $post[ 'PSNo' ]       = $post[ 'PSNo' ] ?? '--';
        $post[ 'PSQtyS' ]     = floatval( $post[ 'PSQtyS' ] ?? 0 );
        $post[ 'POSaldo' ]     = floatval( $post[ 'POSaldo' ] ?? 0 );
        $post[ 'POQtyAju' ]     = floatval( $post[ 'POQtyAju' ] ?? 0 );
        $post[ 'POSLAwal' ]     = floatval( $post[ 'POSLAwal' ] ?? 0 );
        $post[ 'POSLNow' ]     = floatval( $post[ 'POSLNow' ] ?? 0 );
        $post[ 'PONoOLD' ]    = $post[ 'PONoOLD' ] ?? '--';
        $post[ 'POAutoOLD' ]  = floatval( $post[ 'POAutoOLD' ] ?? 0 );
        $post[ 'BrgKodeOLD' ] = $post[ 'BrgKodeOLD' ] ?? '--';
        try {
            General::cCmd( "CALL fpoit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'PONo' ] )
                ->bindParam( 6, $post[ 'POAuto' ] )
                ->bindParam( 7, $post[ 'BrgKode' ] )
                ->bindParam( 8, $post[ 'POQty' ] )
                ->bindParam( 9, $post[ 'POHrgBeli' ] )
                ->bindParam( 10, $post[ 'POHrgJual' ] )
                ->bindParam( 11, $post[ 'PODiscount' ] )
                ->bindParam( 12, $post[ 'POPajak' ] )
                ->bindParam( 13, $post[ 'SONo' ] )
                ->bindParam( 14, $post[ 'PSNo' ] )
                ->bindParam( 15, $post[ 'PSQtyS' ] )
                ->bindParam( 16, $post[ 'POSaldo' ] )
                ->bindParam( 17, $post[ 'POQtyAju' ] )
                ->bindParam( 18, $post[ 'POSLAwal' ] )
                ->bindParam( 19, $post[ 'POSLNow' ] )
                ->bindParam( 20, $post[ 'PONoOLD' ] )
                ->bindParam( 21, $post[ 'POAutoOLD' ] )
                ->bindParam( 22, $post[ 'BrgKodeOLD' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
