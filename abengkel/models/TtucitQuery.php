<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttucit]].
 *
 * @see Ttucit
 */
class TtucitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttucit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttucit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'UCNo' ]          = $post[ 'UCNo' ] ?? '--';
        $post[ 'UCAuto' ]        = floatval( $post[ 'UCAuto' ] ?? 0 );
        $post[ 'BrgKode' ]       = $post[ 'BrgKode' ] ?? '--';
        $post[ 'UCQty' ]         = floatval( $post[ 'UCQty' ] ?? 0 );
        $post[ 'UCHrgBeliLama' ] = floatval( $post[ 'UCHrgBeliLama' ] ?? 0 );
        $post[ 'UCHrgBeliBaru' ] = floatval( $post[ 'UCHrgBeliBaru' ] ?? 0 );
        $post[ 'UCHrgJualLama' ] = floatval( $post[ 'UCHrgJualLama' ] ?? 0 );
        $post[ 'UCHrgJualBaru' ] = floatval( $post[ 'UCHrgJualBaru' ] ?? 0 );
        $post[ 'UCNoOLD' ]       = $post[ 'UCNoOLD' ] ?? '--';
        $post[ 'UCAutoOLD' ]     = floatval( $post[ 'UCAutoOLD' ] ?? 0 );
        $post[ 'BrgKodeOLD' ]    = $post[ 'BrgKodeOLD' ] ?? '--';
        try {
            General::cCmd( "CALL fucit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'UCNo' ] )
                ->bindParam( 6, $post[ 'UCAuto' ] )
                ->bindParam( 7, $post[ 'BrgKode' ] )
                ->bindParam( 8, $post[ 'UCQty' ] )
                ->bindParam( 9, $post[ 'UCHrgBeliLama' ] )
                ->bindParam( 10, $post[ 'UCHrgBeliBaru' ] )
                ->bindParam( 11, $post[ 'UCHrgJualLama' ] )
                ->bindParam( 12, $post[ 'UCHrgJualBaru' ] )
                ->bindParam( 13, $post[ 'UCNoOLD' ] )
                ->bindParam( 14, $post[ 'UCAutoOLD' ] )
                ->bindParam( 15, $post[ 'BrgKodeOLD' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
