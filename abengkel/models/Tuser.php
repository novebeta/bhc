<?php

namespace abengkel\models;

/**
 * This is the model class for table "tuser".
 *
 * @property string $UserID
 * @property string $UserPass
 * @property string $KodeAkses
 * @property string $UserNama
 * @property string $UserAlamat
 * @property string $UserTelepon
 * @property string $UserKeterangan
 * @property string $UserStatus
 * @property string $LokasiKode
 */
class Tuser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tuser';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['UserID'], 'required'],
            [['UserID', 'KodeAkses', 'LokasiKode'], 'string', 'max' => 15],
            [['UserPass'], 'string', 'max' => 10],
            [['UserNama'], 'string', 'max' => 75],
            [['UserAlamat'], 'string', 'max' => 100],
            [['UserTelepon'], 'string', 'max' => 38],
            [['UserKeterangan'], 'string', 'max' => 150],
            [['UserStatus'], 'string', 'max' => 1],
            [['UserID'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'UserID'         => 'User ID',
            'UserPass'       => 'User Pass',
            'KodeAkses'      => 'Kode Akses',
            'UserNama'       => 'User Nama',
            'UserAlamat'     => 'User Alamat',
            'UserTelepon'    => 'User Telepon',
            'UserKeterangan' => 'User Keterangan',
            'UserStatus'     => 'User Status',
            'LokasiKode'     => 'Lokasi Kode',
        ];
    }

	//UserID, UserPass, KodeAkses, UserNama, UserAlamat, UserTelepon, UserKeterangan, UserStatus, LokasiKode
	public static function colGrid()
	{
		return [
			'UserID'	     => ['width' => 90,'label' => 'User ID','name'  => 'UserID'],
			'UserNama'       => ['width' => 150,'label' => 'User Nama','name'  => 'UserNama'],
			'KodeAkses'      => ['width' => 90,'label' => 'KodeAkses','name'  => 'KodeAkses'],
			'UserStatus'     => ['width' => 90,'label' => 'Status','name'  => 'UserStatus'],
            'LokasiKode'     => ['width' => 90,'label' => 'Lokasi','name'  => 'LokasiKode'],
		];
	}
    /**
     * {@inheritdoc}
     * @return TuserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TuserQuery(get_called_class());
    }
}
