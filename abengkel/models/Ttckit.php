<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttckit".
 *
 * @property string $CKNo
 * @property string $SDNo
 * @property string $CKPart
 * @property string $CKJasa
 */
class Ttckit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttckit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CKNo', 'SDNo'], 'required'],
            [['CKPart', 'CKJasa'], 'number'],
            [['CKNo', 'SDNo'], 'string', 'max' => 10],
            [['CKNo', 'SDNo'], 'unique', 'targetAttribute' => ['CKNo', 'SDNo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CKNo' => 'Ck No',
            'SDNo' => 'Sd No',
            'CKPart' => 'Ck Part',
            'CKJasa' => 'Ck Jasa',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtckitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtckitQuery(get_called_class());
    }
}
