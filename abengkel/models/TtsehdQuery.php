<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttsehd]].
 *
 * @see Ttsehd
 */
class TtsehdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttsehd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttsehd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function ttsehdFillByNo() {
        return $this->select( new Expression( "
                ttsehd.SENo,
                ttsehd.SETgl,
                ttsehd.SDNo,
                ttsehd.MotorNoPolisi,
                ttsehd.KodeTrans,
                ttsehd.SETotalWaktu,
                ttsehd.SETerbayar,
                ttsehd.SETotalJasa,
                ttsehd.SETotalPart,
                ttsehd.SETotalBiaya,
                ttsehd.Cetak,
                ttsehd.UserID,
                IFNULL(tdcustomer.CusNama,'--') AS CusNama,
                IFNULL(tdcustomer.MotorType, '--') AS MotorType,
                IFNULL(tdcustomer.MotorWarna, '--') AS MotorWarna,
                IFNULL(tdmotortype.MotorNama, '--') AS MotorNama,
                fWaktu(ttsehd.SETotalWaktu) AS SDWaktu,
                tdcustomer.CusTelepon,
                tdcustomer.MotorNoMesin,
                tdcustomer.MotorNoRangka,
                ttsehd.SEKeterangan,
                ttsehd.KarKode,
                tdkaryawan.KarNama"))
            ->join( "INNER JOIN", "tdkaryawan", "ttsehd.KarKode = tdkaryawan.KarKode ")
            ->join( "LEFT OUTER JOIN", "tdcustomer", "ttsehd.MotorNoPolisi = tdcustomer.MotorNoPolisi ")
            ->join( "LEFT OUTER JOIN", "tdmotortype", "tdcustomer.MotorType = tdmotortype.MotorType" );
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]             = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]          = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]            = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'SENo' ]               = $post[ 'SENo' ] ?? '--';
        $post[ 'SENoBaru' ]           = $post[ 'SENoView' ] ?? '--';
        $post[ 'SETgl' ]              = date('Y-m-d H:i:s', strtotime($post[ 'SETgl' ] ?? date( 'Y-m-d H:i:s' )));
        $post[ 'SDNo' ]               = $post[ 'SDNo' ] ?? '--';
        $post[ 'KodeTrans' ]          = $post[ 'KodeTrans' ] ?? '--';
        $post[ 'MotorNoPolisi' ]      = $post[ 'MotorNoPolisi' ] ?? '--';
        $post[ 'KarKode' ]            = $post[ 'KarKode' ] ?? '--';
        $post[ 'SETotalWaktu' ]       = floatval( $post[ 'SETotalWaktu' ] ?? 0 );
        $post[ 'SETotalJasa' ]        = floatval( $post[ 'SETotalJasa' ] ?? 0 );
        $post[ 'SETotalPart' ]        = floatval( $post[ 'SETotalPart' ] ?? 0 );
        $post[ 'SETotalBiaya' ]       = floatval( $post[ 'SETotalBiaya' ] ?? 0 );
        $post[ 'SETerbayar' ]         = floatval( $post[ 'SETerbayar' ] ?? 0 );
        $post[ 'SEKeterangan' ]       = $post[ 'SEKeterangan' ] ?? '--';
        $post[ 'Cetak' ]              = $post[ 'Cetak' ] ?? 'Belum';
        try {
            General::cCmd( "SET @SENo = ?;" )->bindParam( 1, $post[ 'SENo' ] )->execute();
//            General::cCmd( "CALL fsehd('Update',@Status,@Keterangan,'Foen','Admin','Kedungwuni',@SENo,'SE20-00001','2020-07-09 17:45:15','--','--','UMUM','--','0','0.00','0.00','0.00',0,'--','Belum');" )->execute();
            General::cCmd( "CALL fsehd(?,@Status,@Keterangan,?,?,?,@SENo,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'SENoBaru' ] )
                ->bindParam( 6, $post[ 'SETgl' ] )
                ->bindParam( 7, $post[ 'SDNo' ] )
                ->bindParam( 8, $post[ 'KodeTrans' ] )
                ->bindParam( 9, $post[ 'MotorNoPolisi' ] )
                ->bindParam( 10, $post[ 'KarKode' ] )
                ->bindParam( 11, $post[ 'SETotalWaktu' ] )
                ->bindParam( 12, $post[ 'SETotalJasa' ] )
                ->bindParam( 13, $post[ 'SETotalPart' ] )
                ->bindParam( 14, $post[ 'SETotalBiaya' ] )
                ->bindParam( 15, $post[ 'SETerbayar' ] )
                ->bindParam( 16, $post[ 'SEKeterangan' ] )
                ->bindParam( 17, $post[ 'Cetak' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @SENo,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'SENo'       => $post[ 'SENo' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $SENo       = $exe[ '@SENo' ] ?? $post[ 'SENo' ];
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'SENo'       => $SENo,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
