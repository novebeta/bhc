<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttuhit".
 *
 * @property string $UHNo
 * @property int $UHAuto
 * @property string $BrgKode
 * @property string $UHHrgJualLama
 * @property string $UHHrgJualBaru
 */
class Ttuhit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttuhit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['UHNo', 'UHAuto'], 'required'],
            [['UHAuto'], 'integer'],
            [['UHHrgJualLama', 'UHHrgJualBaru'], 'number'],
            [['UHNo'], 'string', 'max' => 10],
            [['BrgKode'], 'string', 'max' => 20],
            [['UHNo', 'UHAuto'], 'unique', 'targetAttribute' => ['UHNo', 'UHAuto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'UHNo' => 'Uh No',
            'UHAuto' => 'Uh Auto',
            'BrgKode' => 'Brg Kode',
            'UHHrgJualLama' => 'Uh Hrg Jual Lama',
            'UHHrgJualBaru' => 'Uh Hrg Jual Baru',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtuhitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtuhitQuery(get_called_class());
    }
}
