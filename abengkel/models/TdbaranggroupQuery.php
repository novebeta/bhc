<?php

namespace abengkel\models;

/**
 * This is the ActiveQuery class for [[Tdbaranggroup]].
 *
 * @see Tdbaranggroup
 */
class TdbaranggroupQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdbaranggroup[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdbaranggroup|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
