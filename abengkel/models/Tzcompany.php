<?php

namespace abengkel\models;

/**
 * This is the model class for table "tzcompany".
 *
 * @property string $PerusahaanNama
 * @property string $PerusahaanSlogan
 * @property string $PerusahaanAlamat
 * @property string $PerusahaanTelepon
 * @property string $PrinterDotMatrix
 * @property string $PrinterInkJet
 * @property string $PrinterView
 * @property string $AHASS
 * @property string $MainDealer
 * @property string $ReleaseDate
 */
class Tzcompany extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tzcompany';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ReleaseDate'], 'safe'],
            [['PerusahaanNama', 'PerusahaanSlogan', 'PerusahaanTelepon', 'PrinterDotMatrix', 'PrinterInkJet', 'PrinterView'], 'string', 'max' => 50],
            [['PerusahaanAlamat'], 'string', 'max' => 75],
            [['AHASS'], 'string', 'max' => 10],
            [['MainDealer'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PerusahaanNama' => 'Perusahaan Nama',
            'PerusahaanSlogan' => 'Perusahaan Slogan',
            'PerusahaanAlamat' => 'Perusahaan Alamat',
            'PerusahaanTelepon' => 'Perusahaan Telepon',
            'PrinterDotMatrix' => 'Printer Dot Matrix',
            'PrinterInkJet' => 'Printer Ink Jet',
            'PrinterView' => 'Printer View',
            'AHASS' => 'Ahass',
            'MainDealer' => 'Main Dealer',
            'ReleaseDate' => 'Release Date',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TzcompanyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TzcompanyQuery(get_called_class());
    }
}
