<?php

namespace abengkel\models;

use yii\helpers\ArrayHelper;

/**
 * This is the ActiveQuery class for [[Tdjasagroup]].
 *
 * @see Tdjasagroup
 */
class TdjasagroupQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdjasagroup[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdjasagroup|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function JasaGroup() {
        return $this->select('JasaGroupNama as label,JasaGroup as value' )
            ->groupBy( 'JasaGroup' )
            ->orderBy( 'JasaGroup' )
            ->asArray()
            ->all();
    }
    public function Group()
    {
        return ArrayHelper::map($this->select(["CONCAT(JasaGroupNama) as label", "JasaGroup as value"])
            ->asArray()
            ->all(), 'value', 'label');
    }

	public function combo() {
		return $this->select( [ "CONCAT(JasaGroup,' - ',JasaGroupNama) as label", "JasaGroup as value" ] )
		            ->orderBy( 'JasaGroup' )
		            ->asArray()
		            ->all();
	}
}
