<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttucit".
 *
 * @property string $UCNo
 * @property int $UCAuto
 * @property string $BrgKode
 * @property string $UCQty
 * @property string $UCHrgBeliLama
 * @property string $UCHrgBeliBaru
 * @property string $UCHrgJualLama
 * @property string $UCHrgJualBaru
 */
class Ttucit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttucit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['UCNo', 'UCAuto'], 'required'],
            [['UCAuto'], 'integer'],
            [['UCQty', 'UCHrgBeliLama', 'UCHrgBeliBaru', 'UCHrgJualLama', 'UCHrgJualBaru'], 'number'],
            [['UCNo'], 'string', 'max' => 10],
            [['BrgKode'], 'string', 'max' => 20],
            [['UCNo', 'UCAuto'], 'unique', 'targetAttribute' => ['UCNo', 'UCAuto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'UCNo' => 'Uc No',
            'UCAuto' => 'Uc Auto',
            'BrgKode' => 'Brg Kode',
            'UCQty' => 'Uc Qty',
            'UCHrgBeliLama' => 'Uc Hrg Beli Lama',
            'UCHrgBeliBaru' => 'Uc Hrg Beli Baru',
            'UCHrgJualLama' => 'Uc Hrg Jual Lama',
            'UCHrgJualBaru' => 'Uc Hrg Jual Baru',
        ];
    }

	    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
        return [
            'BrgKode'                => ['width' => 125,'label' => 'Kode Barang','name'  => 'Brg Kode'],
            'BrgNama'                => ['width' => 275,'label' => 'Nama Barang','name'  => 'Brg Nama'],
            'tdbaranggroup.BrgGroup' => ['width' => 90,'label' => 'Group','name'  => 'Brg Group'],
            'BrgSatuan'              => ['width' => 50,'label' => 'Status','name'  => 'Brg Satuan'],
            'BrgHrgBeli'             => ['width' => 90,'label' => 'Hrg Beli','name'  => 'Brg HrgBeli', 'formatter' => 'number', 'align' => 'right'],
            'BrgHrgJual'             => ['width' => 90,'label' => 'Hrg Jual','name'  => 'Brg HrgJual', 'formatter' => 'number', 'align' => 'right'],
            'BrgRak1'                => ['width' => 50,'label' => 'Rak 1','name'  => 'Brg Rak1'],
            'BrgMinStock'            => ['width' => 45,'label' => 'Min','name'  => 'Brg Min Stock', 'formatter' => 'integer', 'align' => 'right'],
            'BrgMaxStock'            => ['width' => 45,'label' => 'Max','name'  => 'Brg Max Stock', 'formatter' => 'integer', 'align' => 'right'],
            'BrgStatus'              => ['width' => 60,'label' => 'Status','name'  => 'Brg Status'],
        ];
    }
	
    /**
     * {@inheritdoc}
     * @return TtucitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtucitQuery(get_called_class());
    }
}
