<?php

namespace abengkel\models;

/**
 * This is the ActiveQuery class for [[Tvhutangdebet]].
 *
 * @see Tvhutangdebet
 */
class TvhutangdebetQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tvhutangdebet[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tvhutangdebet|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
