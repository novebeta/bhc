<?php

namespace abengkel\models;

use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
use abengkel\components\Menu;
/**
 * This is the ActiveQuery class for [[Tttahd]].
 *
 * @see Tttahd
 */
class TttahdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tttahd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tttahd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function tttahdFillByNo() {
        return $this->select( new Expression( "TANo, TATgl, LokasiKode, TAKeterangan, TATotal, UserID, NoGL, KodeTrans, PosKode, Cetak"));
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'TANo' ]          = $post[ 'TANo' ] ?? '--';
        $post[ 'TANoBaru' ]      = $post[ 'TANoView' ] ?? '--';
        $post[ 'TATgl' ]         = date('Y-m-d H:i:s', strtotime($post[ 'TATgl' ] ?? date( 'Y-m-d H:i:s' )));
        $post[ 'KodeTrans' ]     = $post[ 'KodeTrans' ] ?? '--';
        $post[ 'LokasiKode' ]    = $post[ 'LokasiKode' ] ?? '--';
        $post[ 'TATotal' ]       = floatval( $post[ 'TATotal' ] ?? 0 );
        $post[ 'TAKeterangan' ]  = $post[ 'TAKeterangan' ] ?? '--';
        $post[ 'NoGL' ]          = $post[ 'NoGL' ] ?? '--';
        $post[ 'Cetak' ]         = $post[ 'Cetak' ] ?? 'Belum';
        try {
            General::cCmd( "SET @TANo = ?;" )->bindParam( 1, $post[ 'TANo' ] )->execute();
            General::cCmd( "CALL ftahd(?,@Status,@Keterangan,?,?,?,@TANo,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'TANoBaru' ] )
                ->bindParam( 6, $post[ 'TATgl' ] )
                ->bindParam( 7, $post[ 'KodeTrans' ] )
                ->bindParam( 8, $post[ 'LokasiKode' ] )
                ->bindParam( 9, $post[ 'TATotal' ] )
                ->bindParam( 10, $post[ 'TAKeterangan' ] )
                ->bindParam( 11, $post[ 'NoGL' ] )
                ->bindParam( 12, $post[ 'Cetak' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @TANo,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'TANo'       => $post[ 'TANo' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $TANo       = $exe[ '@TANo' ] ?? $post[ 'TANo' ];
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'TANo'       => $TANo,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
