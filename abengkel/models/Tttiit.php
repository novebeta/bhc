<?php

namespace abengkel\models;

/**
 * This is the model class for table "tttiit".
 *
 * @property string $TINo
 * @property int $TIAuto
 * @property string $BrgKode
 * @property string $TIQty
 * @property string $TIHrgBeli
 * @property string $LokasiAsal
 * @property string $LokasiTujuan
 */
class Tttiit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tttiit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['TINo', 'TIAuto'], 'required'],
            [['TIAuto'], 'integer'],
            [['TIQty', 'TIHrgBeli'], 'number'],
            [['TINo'], 'string', 'max' => 10],
            [['BrgKode'], 'string', 'max' => 20],
            [['LokasiAsal', 'LokasiTujuan'], 'string', 'max' => 15],
            [['TINo', 'TIAuto'], 'unique', 'targetAttribute' => ['TINo', 'TIAuto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'TINo' => 'Ti No',
            'TIAuto' => 'Ti Auto',
            'BrgKode' => 'Brg Kode',
            'TIQty' => 'Ti Qty',
            'TIHrgBeli' => 'Ti Hrg Beli',
            'LokasiAsal' => 'Lokasi Asal',
            'LokasiTujuan' => 'Lokasi Tujuan',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TttiitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TttiitQuery(get_called_class());
    }
}
