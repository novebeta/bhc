<?php
namespace abengkel\models;
use common\components\General;
use abengkel\components\Menu;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttkmit]].
 *
 * @see Ttkmit
 */
class TtkmitQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttkmit[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttkmit|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'PosKode' ]    = Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'KMNo' ]       = $post[ 'KMNo' ] ?? '--';
		$post[ 'KMLink' ]     = $post[ 'KMLink' ] ?? '--';
		$post[ 'KMBayar' ]    = floatval( $post[ 'KMBayar' ] ?? 0 );
        $post[ 'KMNoOLD' ]    = $post[ 'KMNoOLD' ] ?? '--';
        $post[ 'KMLinkOLD' ]  = $post[ 'KMLinkOLD' ] ?? '--';
        try {
		General::cCmd( "CALL fkmit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?);" )
		       ->bindParam( 1, $post[ 'Action' ] )
		       ->bindParam( 2, $post[ 'UserID' ] )
		       ->bindParam( 3, $post[ 'KodeAkses' ] )
		       ->bindParam( 4, $post[ 'PosKode' ] )
		       ->bindParam( 5, $post[ 'KMNo' ] )
		       ->bindParam( 6, $post[ 'KMLink' ] )
		       ->bindParam( 7, $post[ 'KMBayar' ] )
		       ->bindParam( 8, $post[ 'KMNoOLD' ] )
		       ->bindParam( 9, $post[ 'KMLinkOLD' ] )
		       ->execute();
		$exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
	}
}
