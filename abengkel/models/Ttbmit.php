<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttbmit".
 *
 * @property string $BMNo
 * @property string $BMLink
 * @property string $BMBayar
 */
class Ttbmit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttbmit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BMNo', 'BMLink'], 'required'],
            [['BMBayar'], 'number'],
            [['BMNo'], 'string', 'max' => 12],
            [['BMLink'], 'string', 'max' => 18],
            [['BMNo', 'BMLink'], 'unique', 'targetAttribute' => ['BMNo', 'BMLink']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'BMNo' => 'Bm No',
            'BMLink' => 'Bm Link',
            'BMBayar' => 'Bm Bayar',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtbmitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtbmitQuery(get_called_class());
    }
}
