<?php

namespace abengkel\models;
use common\components\General;
use abengkel\components\Menu;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttpohd]].
 *
 * @see Ttpohd
 */
class TtpohdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttpohd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttpohd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function ttpohdFillByNo() {
        return $this->select( new Expression( "ttpohd.PONo, ttpohd.POTgl, ttpohd.PSNo, ttpohd.SONo, ttpohd.SupKode, ttpohd.LokasiKode,
         ttpohd.POKeterangan, ttpohd.POSubTotal, ttpohd.PODiscFinal, ttpohd.POTotalPajak, ttpohd.POBiayaKirim, ttpohd.POTotal, 
         ttpohd.UserID, ttpohd.KodeTrans, ttpohd.PosKode, ttpohd.Cetak, tdsupplier.SupNama, IFNULL(ttpshd.PSTgl, 
         STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS PSTgl, IFNULL(ttsohd.SOTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SOTgl, 
         ttpohd.POAcc, ttpohd.TglAcc, ttpohd.POStatus, IF(ttpohd.POStatus = 'Acc', 1, 0) AS Acc, 
         IF(ttpohd.POStatus = 'Ditolak', 1, 0) AS Tolak, TglRelease") )
            ->join( "LEFT OUTER JOIN", "tdsupplier", "ttpohd.SupKode = tdsupplier.SupKode" )
            ->join( "LEFT OUTER JOIN", "ttpshd", "ttpohd.PSNo = ttpshd.PSNo" )
            ->join( "LEFT OUTER JOIN", "ttsohd", "ttpohd.SONo = ttsohd.SONo" )
            ->groupBy("ttpohd.PONo");
    }

	public function callSP( $post ) {
		$post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'PONo' ]          = $post[ 'PONo' ] ?? '--';
		$post[ 'PONoBaru' ]      = $post[ 'PONoView' ] ?? '--';
		$post[ 'POTgl' ]         = date('Y-m-d H:i:s', strtotime($post[ 'POTgl' ] ?? date( 'Y-m-d H:i:s' )));
		$post[ 'SupKode' ]       = $post[ 'SupKode' ] ?? '--';
		$post[ 'SONo' ]          = $post[ 'SONo' ] ?? '--';
        $post[ 'PSNo' ]          = $post[ 'PSNo' ] ?? '--';
        $post[ 'KodeTrans' ]     = $post[ 'KodeTrans' ] ?? '--';
		$post[ 'POKeterangan' ]  = $post[ 'POKeterangan' ] ?? '--';
		$post[ 'Cetak' ]         = $post[ 'Cetak' ] ?? '--';
		$post[ 'POSubTotal' ]    = floatval( $post[ 'POSubTotal' ] ?? 0 );
		$post[ 'PODiscFinal' ]   = floatval( $post[ 'PODiscFinal' ] ?? 0 );
		$post[ 'POTotal' ]       = floatval( $post[ 'POTotal' ] ?? 0 );
		$post[ 'LokasiKode' ]       = $post[ 'LokasiKode' ] ?? '--';

		$post[ 'POAcc' ]        = $post[ 'POAcc' ] ?? '--';
		$post[ 'TglAcc' ]       = date('Y-m-d H:i:s', strtotime($post[ 'TglAcc' ] ?? date( 'Y-m-d H:i:s' )));
		$post[ 'TglRelease' ]       = date('Y-m-d H:i:s', strtotime($post[ 'TglRelease' ] ?? date( 'Y-m-d H:i:s' )));
		$post[ 'POStatus' ]       = $post[ 'POStatus' ] ?? '--';
        try {
		General::cCmd( "SET @PONo = ?;" )->bindParam( 1, $post[ 'PONo' ] )->execute();
		General::cCmd( "CALL fpohd(?,@Status,@Keterangan,?,?,?,@PONo,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
		       ->bindParam( 1, $post[ 'Action' ] )
		       ->bindParam( 2, $post[ 'UserID' ] )
		       ->bindParam( 3, $post[ 'KodeAkses' ] )
		       ->bindParam( 4, $post[ 'PosKode' ] )
		       ->bindParam( 5, $post[ 'PONoBaru' ] )
		       ->bindParam( 6, $post[ 'POTgl' ] )
		       ->bindParam( 7, $post[ 'SupKode' ] )
		       ->bindParam( 8, $post[ 'SONo' ] )
		       ->bindParam( 9, $post[ 'PSNo' ] )
		       ->bindParam( 10, $post[ 'KodeTrans' ] )
		       ->bindParam( 11, $post[ 'POKeterangan' ] )
		       ->bindParam( 12, $post[ 'Cetak' ] )
		       ->bindParam( 13, $post[ 'POSubTotal' ] )
		       ->bindParam( 14, $post[ 'PODiscFinal' ] )
		       ->bindParam( 15, $post[ 'POTotal' ] )
		       ->bindParam( 16, $post[ 'LokasiKode' ] )
		       ->bindParam( 17, $post[ 'POAcc' ] )
		       ->bindParam( 18, $post[ 'TglAcc' ] )
		       ->bindParam( 19, $post[ 'TglRelease' ] )
		       ->bindParam( 20, $post[ 'POStatus' ] )
		       ->execute();
            $exe = General::cCmd( "SELECT @PONo,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'PONo'       => $post[ 'PONo' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $PONo       = $exe[ '@PONo' ] ?? $post[ 'PONo' ];
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'PONo'       => $PONo,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
	}
}
