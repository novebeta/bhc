<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttsehd".
 *
 * @property string $SENo
 * @property string $SETgl
 * @property string $SDNo
 * @property string $KodeTrans
 * @property string $MotorNoPolisi
 * @property string $KarKode
 * @property string $SETotalWaktu
 * @property string $SETotalJasa
 * @property string $SETotalPart
 * @property string $SETotalBiaya
 * @property string $SETerbayar
 * @property string $SEKeterangan
 * @property string $Cetak
 * @property string $UserID
 */
class Ttsehd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttsehd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SENo'], 'required'],
            [['SETgl'], 'safe'],
            [['SETotalWaktu', 'SETotalJasa', 'SETotalPart', 'SETotalBiaya', 'SETerbayar'], 'number'],
            [['SENo', 'SDNo', 'KarKode'], 'string', 'max' => 10],
            [['KodeTrans'], 'string', 'max' => 4],
            [['MotorNoPolisi'], 'string', 'max' => 12],
            [['SEKeterangan'], 'string', 'max' => 150],
            [['Cetak'], 'string', 'max' => 5],
            [['UserID'], 'string', 'max' => 15],
            [['SENo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SENo'          => 'Se No',
            'SETgl'         => 'Se Tgl',
            'SDNo'          => 'Sd No',
            'KodeTrans'     => 'Kode Trans',
            'MotorNoPolisi' => 'Motor No Polisi',
            'KarKode'       => 'Kar Kode',
            'SETotalWaktu'  => 'Se Total Waktu',
            'SETotalJasa'   => 'Se Total Jasa',
            'SETotalPart'   => 'Se Total Part',
            'SETotalBiaya'  => 'Se Total Biaya',
            'SETerbayar'    => 'Se Terbayar',
            'SEKeterangan'  => 'Se Keterangan',
            'Cetak'         => 'Cetak',
            'UserID'        => 'User ID',
        ];
    }


	/**
	 * {@inheritdoc}
	 */
	public static function colGrid()
	{
		return [
			'SENo'                       => ['width' => 80,'label' => 'No SE','name'  => 'SENo'],
			'SETgl'                      => ['width' => 110,'label' => 'Tgl SE','name'  => 'SETgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y H:i:s']],
			'tdcustomer.MotorNoPolisi'              => ['width' => 80,'label' => 'No Polisi','name'  => 'MotorNoPolisi'],
			'SDNo'                       => ['width' => 80,'label' => 'No SD','name'  => 'SDNo'],
			'tdcustomer.CusNama'                    => ['width' => 100,'label' => 'Konsumen','name'  => 'CusNama'],
			'tdcustomer.MotorNama'                  => ['width' => 205,'label' => 'Nama Motor','name'  => 'MotorNama'],
			'KarKode'                    => ['width' => 90,'label' => 'Karyawan','name'  => 'KarKode'],
			'SETotalBiaya'               => ['width' => 95,'label' => 'Total','name'  => 'SETotalBiaya', 'formatter' => 'number', 'align' => 'right'],
			'SETerbayar'                 => ['width' => 95,'label' => 'Terbayar','name'  => 'SETerbayar', 'formatter' => 'number', 'align' => 'right'],
			'Cetak'                      => ['width' => 60,'label' => 'Cetak','name'  => 'Cetak'],
			'UserID'                     => ['width' => 60,'label' => 'UserID','name'  => 'UserID'],
			'SEKeterangan'               => ['width' => 160,'label' => 'Keterangan','name'  => 'SEKeterangan'],

		];
	}


	public function getCustomer() {
		return $this->hasOne( Tdcustomer::className(), [ 'MotorNoPolisi' => 'MotorNoPolisi' ] );
	}

    /**
     * {@inheritdoc}
     * @return TtsehdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtsehdQuery(get_called_class());
    }
}
