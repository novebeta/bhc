<?php

namespace abengkel\models;

/**
 * This is the model class for table "tdprogramhd".
 *
 * @property string $PrgNama
 * @property string $PrgTglMulai
 * @property string $PrgTglAkhir
 * @property string $PrgMemo
 */
class Tdprogramhd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdprogramhd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PrgNama'], 'required'],
            [['PrgTglMulai', 'PrgTglAkhir'], 'safe'],
            [['PrgNama'], 'string', 'max' => 25],
            [['PrgMemo'], 'string', 'max' => 300],
            [['PrgNama'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PrgNama'     => 'Prg Nama',
            'PrgTglMulai' => 'Prg Tgl Mulai',
            'PrgTglAkhir' => 'Prg Tgl Akhir',
            'PrgMemo'     => 'Prg Memo',
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
        return [
            'PrgNama'     => ['width' => 250,'label' => 'Nama','name'  => 'PrgNama'],
            'PrgTglMulai' => ['width' => 100,'label' => 'Tgl Mulai','name'  => 'PrgTglMulai'],
            'PrgTglAkhir' => ['width' => 100,'label' => 'Tgl Akhir','name'  => 'PrgTglAkhir'],
            'PrgMemo'     => ['width' => 450,'label' => 'Keterangan','name'  => 'PrgMemo'],
        ];
    }

    /**
     * {@inheritdoc}
     * @return TdprogramhdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdprogramhdQuery(get_called_class());
    }
}
