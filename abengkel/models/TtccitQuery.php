<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttccit]].
 *
 * @see Ttccit
 */
class TtccitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttccit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttccit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]        = Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'CCNo' ]          = $post[ 'CCNo' ] ?? '--';
        $post[ 'SDNo' ]          = $post[ 'SDNo' ] ?? '--';
        $post[ 'CCPart' ]        = floatval( $post[ 'CCPart' ] ?? 0 );
        $post[ 'CCJasa' ]        = floatval( $post[ 'CCJasa' ] ?? 0 );
        $post[ 'CCNoOLD' ]       = $post[ 'CCNoOLD' ] ?? '--';
        $post[ 'SDNoOLD' ]       = $post[ 'SDNoOLD' ] ?? '--';
        try {
            General::cCmd( "CALL fccit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'CCNo' ] )
                ->bindParam( 6, $post[ 'SDNo' ] )
                ->bindParam( 7, $post[ 'CCPart' ] )
                ->bindParam( 8, $post[ 'CCJasa' ] )
                ->bindParam( 9, $post[ 'CCNoOLD' ] )
                ->bindParam( 10, $post[ 'SDNoOLD' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
