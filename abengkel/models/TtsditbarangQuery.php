<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;

/**
 * This is the ActiveQuery class for [[Ttsditbarang]].
 *
 * @see Ttsditbarang
 */
class TtsditbarangQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttsditbarang[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttsditbarang|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'SDNo' ]          = $post[ 'SDNo' ] ?? '--';
        $post[ 'SDAuto' ]        = floatval( $post[ 'SDAuto' ] ?? 0 );
        $post[ 'BrgKode' ]       = $post[ 'BrgKode' ] ?? '--';
        $post[ 'SDQty' ]         = floatval( $post[ 'SDQty' ] ?? 0 );
        $post[ 'SDHrgBeli' ]     = floatval( $post[ 'SDHrgBeli' ] ?? 0 );
        $post[ 'SDHrgJual' ]     = floatval( $post[ 'SDHrgJual' ] ?? 0 );
        $post[ 'SDJualDisc' ]    = floatval( $post[ 'SDJualDisc' ] ?? 0 );
        $post[ 'SDPajak' ]       = floatval( $post[ 'SDPajak' ] ?? 0 );
        $post[ 'SDPickingNo' ]   = $post[ 'SDPickingNo' ] ?? '--';
        $post[ 'SDPickingDate' ] = date('Y-m-d H:i:s', strtotime($post[ 'SDPickingDate' ] ?? date( 'Y-m-d H:i:s' )));
        $post[ 'Cetak' ]         = $post[ 'Cetak' ] ?? '--';
        $post[ 'SDNoOLD' ]       = $post[ 'SDNoOLD' ] ?? '--';
        $post[ 'SDAutoOLD' ]     = floatval( $post[ 'SDAutoOLD' ] ?? 0 );
        $post[ 'BrgKodeOLD' ]    = $post[ 'BrgKodeOLD' ] ?? '--';
        $post[ 'KodeTrans' ]    = $post[ 'KodeTrans' ] ?? '--';
        $post[ 'PrgNama' ]    = $post[ 'PrgNama' ] ?? '--';
        try {
            General::cCmd( "CALL fsditbarang(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'SDNo' ] )
                ->bindParam( 6, $post[ 'SDAuto' ] )
                ->bindParam( 7, $post[ 'BrgKode' ] )
                ->bindParam( 8, $post[ 'SDQty' ] )
                ->bindParam( 9, $post[ 'SDHrgBeli' ] )
                ->bindParam( 10, $post[ 'SDHrgJual' ] )
                ->bindParam( 11, $post[ 'SDJualDisc' ] )
                ->bindParam( 12, $post[ 'SDPajak' ] )
                ->bindParam( 13, $post[ 'SDPickingNo' ] )
                ->bindParam( 14, $post[ 'SDPickingDate' ] )
                ->bindParam( 15, $post[ 'Cetak' ] )
                ->bindParam( 16, $post[ 'SDNoOLD' ] )
                ->bindParam( 17, $post[ 'SDAutoOLD' ] )
                ->bindParam( 18, $post[ 'BrgKodeOLD' ] )
                ->bindParam( 19, $post[ 'KodeTrans' ] )
                ->bindParam( 20, $post[ 'PrgNama' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }

    public function fillHeaderPL($SDNo){
        return $this->select( new Expression( "ttSDhd.SDNo, DATE_FORMAT(SDTgl, '%d/%m/%Y') SDTgl, SDPickingNo, DATE_FORMAT(SDPickingDate, '%d/%m/%Y') SDPickingDate, ttSDitbarang.Cetak "))
            ->join( "INNER JOIN", "ttsdhd", "ttSDhd.SDNo = ttSDitbarang.SDNo")
            ->where( "ttsdhd.SDNo = :SDNo", [ ":SDNo" => $SDNo ])
            ->groupBy( "SDPickingNo, SDPickingDate" );
    }

    public function fillItemPL($SDPickingNo){
        return $this->select( new Expression( "
                ttsditbarang.SDNo,
                ttsditbarang.SDAuto,
                ttsditbarang.BrgKode,
                ttsditbarang.SDQty,
                ttsditbarang.SDHrgBeli,
                ttsditbarang.SDHrgJual,
                ttsditbarang.SDJualDisc,
                ttsditbarang.SDPajak,
                tdbarang.BrgNama,
                tdbarang.BrgSatuan,
                IF((ttsditbarang.SDHrgJual * ttsditbarang.SDQty) > 0, ttsditbarang.SDJualDisc / (ttsditbarang.SDHrgJual * ttsditbarang.SDQty) * 100, 0) AS Disc,
                ttsditbarang.SDQty * ttsditbarang.SDHrgJual - ttsditbarang.SDJualDisc AS Jumlah,
                ttsditbarang.LokasiKode,
                ttsditbarang.SDPickingNo,
                ttsditbarang.SDPickingDate,
                tdbarang.BrgGroup,
                tdbarang.BrgRak1,
                SaldoQty "))
            ->join( "INNER JOIN", "tdbarang", "ttsditbarang.BrgKode = tdbarang.BrgKode")
            ->join( "INNER JOIN", "tdbaranglokasi", "tdbaranglokasi.BrgKode = ttsditbarang.BrgKode AND tdbaranglokasi.lokasiKode = ttsditbarang.lokasiKode")
            ->where("ttsditbarang.SDPickingNo = :SDPickingNo", [ ":SDPickingNo" => $SDPickingNo ])
            ->orderBy("ttsditbarang.SDNo, ttsditbarang.SDAuto");
    }
}
