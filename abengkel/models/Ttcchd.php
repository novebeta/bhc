<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttcchd".
 *
 * @property string $CCNo
 * @property string $CCTgl
 * @property string $CCTotalPart
 * @property string $CCTotalJasa
 * @property string $CCNoKlaim
 * @property string $CCMemo
 * @property string $NoGL
 * @property string $Cetak
 * @property string $UserID
 */
class Ttcchd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttcchd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CCNo'], 'required'],
            [['CCTgl'], 'safe'],
            [['CCTotalPart', 'CCTotalJasa'], 'number'],
            [['CCNo', 'NoGL'], 'string', 'max' => 10],
            [['CCNoKlaim'], 'string', 'max' => 35],
            [['CCMemo'], 'string', 'max' => 150],
            [['Cetak'], 'string', 'max' => 5],
            [['UserID'], 'string', 'max' => 15],
            [['CCNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CCNo'        => 'Cc No',
            'CCTgl'       => 'Cc Tgl',
            'CCTotalPart' => 'Cc Total Part',
            'CCTotalJasa' => 'Cc Total Jasa',
            'CCNoKlaim'   => 'Cc No Klaim',
            'CCMemo'      => 'Cc Memo',
            'NoGL'        => 'No Gl',
            'Cetak'       => 'Cetak',
            'UserID'      => 'User ID',
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
        return [
			'CCNo'                   => ['width' => 80,'label' => 'No CC','name'  => 'CCNo'],
			'CCTgl'                  => ['width' => 110,'label' => 'Tgl CC','name'  => 'CCTgl','formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y H:i:s']],
			'CCTotalPart'            => ['width' => 105,'label' => 'Total Part','name'  => 'CCTotalPart',  'formatter' => 'number', 'align' => 'right'],
			'CCTotalJasa'            => ['width' => 105,'label' => 'Total Jasa','name'  => 'CCTotalJasa',  'formatter' => 'number', 'align' => 'right'],
            'CCNoKlaim'              => ['width' => 100,'label' => 'No Klaim','name'  => 'CCNoKlaim'],
            'CCMemo'                 => ['width' => 215,'label' => 'Keterangan','name'  => 'CCMemo'],
            'NoGL'                   => ['width' => 75,'label' => 'No GL','name'  => 'NoGL'],
            'Cetak'                  => ['width' => 60,'label' => 'No Cetak','name'  => 'Cetak'],
            'UserID'                 => ['width' => 80,'label' => 'User ID','name'  => 'UserID'],
        ];
    }

    public static function colGridPick()
    {
        return [
            'NO'                         => [ 'width' => 70, 'label' => 'No Servis', 'name' => 'NO' ],
            'SDTgl'                      => [ 'width' => 115, 'label' => 'Tgl Servis', 'name' => 'Tgl' , 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y H:i:s']],
            'MotorNama'                  => [ 'width' => 200, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
            'MotorNoPolisi'              => [ 'width' => 90, 'label' => 'No Polisi', 'name' => 'Kode' ],
            'CusNama'                    => [ 'width' => 150, 'label' => 'Nama Konsumen', 'name' => 'Nama' ],
            'SDTotalJasa'                => [ 'width' => 80, 'label' => 'Total Jasa', 'name' => 'SDTotalJasa' ],
            'SDTotalPart'                => [ 'width' => 80, 'label' => 'Total Part', 'name' => 'SDTotalPart' ],
            'SDTotal'                    => [ 'width' => 80, 'label' => 'Total', 'name' => 'SDTotal' ],
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtcchdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtcchdQuery(get_called_class());
    }
}
