<?php
namespace abengkel\models;
use yii\helpers\ArrayHelper;
/**
 * This is the ActiveQuery class for [[Tdkaryawan]].
 *
 * @see Tdkaryawan
 */
class TdkaryawanQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Tdkaryawan[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Tdkaryawan|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function select2( $value, $label = [ 'KarNama' ], $orderBy = [ 'KarNama' ], $where = [ 'condition' => null, 'params' => [] ], $allOption = false ) {
		$option =
			$this->select( [ "CONCAT(" . implode( ",'||',", $label ) . ") as label", "$value as value" ] )
			     ->orderBy( $orderBy )
			     ->where( $where[ 'condition' ], $where[ 'params' ] )
			     ->asArray()
			     ->all();
		if ( $allOption ) {
			return array_merge( [ [ 'value' => '%', 'label' => 'Semua' ] ], $option );
		} else {
			return $option;
		}
	}
	public function combo() {
		return $this->select( [ "CONCAT(KarKode,' - ',KarNama) as label", "KarKode as value" ] )
		            ->orderBy( 'KarStatus, KarKode' )
		            ->asArray()
		            ->all();
	}
	public function KarKode() {
		return ArrayHelper::map( $this->select( [ "CONCAT(KarNama) as label", "KarKode as value" ] )
		                              ->asArray()
		                              ->all(), 'value', 'label' );
	}
	public function KaryawanStartStop() {
		return ArrayHelper::map(
			$this->select( [ "KarNama as label", "KarKode as value" ] )
			     ->where( "KarKode <> '--' AND (KarJabatan LIKE '%Mekanik' OR KarJabatan = 'Servis Advisor') AND KarStatus = 'A'" )
			     ->asArray()
			     ->all(), 'value', 'label' );
	}

    public function daftarkarkode() {
        return $this->select('KarKode as label, KarKode as value' )
            ->where( "KarKode <> '--' AND KarStatus = 'A' AND (KarJabatan = 'Servis Advisor' OR KarJabatan = 'Kepala Bengkel')" )
            ->orderBy( 'KarStatus' )
            ->asArray()
            ->all();
    }

    public function daftar( $value, $label = [ 'KarKode' ], $orderBy = [], $where = [ 'condition' => null, 'params' => [] ] , $allOption = false ) {
        $option =
            $this->select( [ "CONCAT(" . implode( ",'||',", $label ) . ") as label", "$value as value" ] )
                ->orderBy( $orderBy )
                ->where( $where[ 'condition' ], $where[ 'params' ] )
                ->asArray()
                ->all();
        if ( $allOption ) {
            return array_merge( [ [ 'value' => '%', 'label' => 'Semua' ] ], $option );
        } else {
            return $option;
        }
    }

}
