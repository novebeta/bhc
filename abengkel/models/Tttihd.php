<?php

namespace abengkel\models;

/**
 * This is the model class for table "tttihd".
 *
 * @property string $TINo
 * @property string $TITgl
 * @property string $KodeTrans
 * @property string $PosKode
 * @property string $LokasiTujuan
 * @property string $LokasiAsal
 * @property string $PSNo
 * @property string $TITotal
 * @property string $TIKeterangan
 * @property string $NoGL
 * @property string $Cetak
 * @property string $UserID
 */
class Tttihd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tttihd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['TINo'], 'required'],
            [['TITgl'], 'safe'],
            [['TITotal'], 'number'],
            [['TINo', 'PSNo', 'NoGL'], 'string', 'max' => 10],
            [['KodeTrans'], 'string', 'max' => 4],
            [['PosKode'], 'string', 'max' => 20],
            [['LokasiTujuan', 'LokasiAsal', 'UserID'], 'string', 'max' => 15],
            [['TIKeterangan'], 'string', 'max' => 150],
            [['Cetak'], 'string', 'max' => 5],
            [['TINo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'TINo' => 'Ti No',
            'TITgl' => 'Ti Tgl',
            'KodeTrans' => 'Kode Trans',
            'PosKode' => 'Pos Kode',
            'LokasiTujuan' => 'Lokasi Tujuan',
            'LokasiAsal' => 'Lokasi Asal',
            'PSNo' => 'Ps No',
            'TITotal' => 'Ti Total',
            'TIKeterangan' => 'Ti Keterangan',
            'NoGL' => 'No Gl',
            'Cetak' => 'Cetak',
            'UserID' => 'User ID',
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
        return [
            'TINo'         => ['width' => 85,'label' => 'No TI','name'  => 'TINo'],
            'TITgl'        => ['width' => 115,'label' => 'Tgl TI','name'  => 'TITgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y H:i:s']],
            'KodeTrans'    => ['width' => 60,'label' => 'TC','name'  => 'KodeTrans'],
            'PosKode'      => ['width' => 100,'label' => 'POS','name'  => 'PosKode'],
            'PSNo'         => ['width' => 75,'label' => 'No PS','name'  => 'PSNo'],
            'TITotal'      => ['width' => 100,'label' => 'Total','name'  => 'TITotal','formatter' => 'number', 'align' => 'right'],
            'TIKeterangan' => ['width' => 260,'label' => 'Keterangan','name'  => 'TIKeterangan'],
            'Cetak'        => ['width' => 55,'label' => 'Cetak','name'  => 'Cetak'],
            'UserID'       => ['width' => 80,'label' => 'UserID','name'  => 'UserID'],
        ];
    }

    /**
     * {@inheritdoc}
     * @return TttihdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TttihdQuery(get_called_class());
    }
}
