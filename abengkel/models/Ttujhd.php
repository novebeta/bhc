<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttujhd".
 *
 * @property string $UJNo
 * @property string $UJTgl
 * @property string $UJKeterangan
 * @property string $KodeTrans
 * @property string $PosKode
 * @property string $Cetak
 * @property string $UserID
 */
class Ttujhd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttujhd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['UJNo'], 'required'],
            [['UJTgl'], 'safe'],
            [['UJNo'], 'string', 'max' => 10],
            [['UJKeterangan'], 'string', 'max' => 150],
            [['KodeTrans'], 'string', 'max' => 4],
            [['PosKode'], 'string', 'max' => 20],
            [['Cetak', 'UserID'], 'string', 'max' => 15],
            [['UJNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'UJNo' => 'Uj No',
            'UJTgl' => 'Uj Tgl',
            'UJKeterangan' => 'Uj Keterangan',
            'KodeTrans' => 'Kode Trans',
            'PosKode' => 'Pos Kode',
            'Cetak' => 'Cetak',
            'UserID' => 'User ID',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
        return [
            'UJNo'         => ['width' => 90,'label' => 'No UJ','name'  => 'UJNo'],
            'UJTgl'        => ['width' => 120,'label' => 'Tgl UJ','name'  => 'UJTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y H:i:s']],
            'KodeTrans'    => ['width' => 50,'label' => 'TC','name'  => 'KodeTrans'],
            'PosKode'      => ['width' => 90,'label' => 'PosKode','name'  => 'PosKode'],
            'Cetak'        => ['width' => 60,'label' => 'Cetak','name'  => 'Cetak'],
            'UJKeterangan' => ['width' => 240,'label' => 'Keterangan','name'  => 'UJKeterangan'],
            'UserID'       => ['width' => 90,'label' => 'UserID','name'  => 'UserID'],
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtujhdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtujhdQuery(get_called_class());
    }
}
