<?php

namespace abengkel\models;

/**
 * This is the model class for table "tvpiutangbayar".
 *
 * @property string $HPLink
 * @property string $NoAccount
 * @property string $SumKreditGL
 */
class Tvpiutangbayar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tvpiutangbayar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['HPLink', 'NoAccount'], 'required'],
            [['SumKreditGL'], 'number'],
            [['HPLink'], 'string', 'max' => 18],
            [['NoAccount'], 'string', 'max' => 10],
            [['HPLink', 'NoAccount'], 'unique', 'targetAttribute' => ['HPLink', 'NoAccount']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'HPLink' => 'Hp Link',
            'NoAccount' => 'No Account',
            'SumKreditGL' => 'Sum Kredit Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TvpiutangbayarQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TvpiutangbayarQuery(get_called_class());
    }
}
