<?php

namespace abengkel\models;

/**
 * This is the model class for table "tttait".
 *
 * @property string $TANo
 * @property int $TAAuto
 * @property string $BrgKode
 * @property string $TAQty
 * @property string $TAHrgBeli
 * @property string $LokasiKode
 */
class Tttait extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tttait';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['TANo', 'TAAuto'], 'required'],
            [['TAAuto'], 'integer'],
            [['TAQty', 'TAHrgBeli'], 'number'],
            [['TANo'], 'string', 'max' => 10],
            [['BrgKode'], 'string', 'max' => 20],
            [['LokasiKode'], 'string', 'max' => 15],
            [['TANo', 'TAAuto'], 'unique', 'targetAttribute' => ['TANo', 'TAAuto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'TANo' => 'Ta No',
            'TAAuto' => 'Ta Auto',
            'BrgKode' => 'Brg Kode',
            'TAQty' => 'Ta Qty',
            'TAHrgBeli' => 'Ta Hrg Beli',
            'LokasiKode' => 'Lokasi Kode',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TttaitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TttaitQuery(get_called_class());
    }
}
