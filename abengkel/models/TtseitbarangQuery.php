<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttseitbarang]].
 *
 * @see Ttseitbarang
 */
class TtseitbarangQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttseitbarang[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttseitbarang|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'SENo' ]          = $post[ 'SENo' ] ?? '--';
        $post[ 'SEAuto' ]        = floatval( $post[ 'SEAuto' ] ?? 0 );
        $post[ 'BrgKode' ]       = $post[ 'BrgKode' ] ?? '--';
        $post[ 'SEQty' ]         = floatval( $post[ 'SEQty' ] ?? 0 );
        $post[ 'SEHrgBeli' ]     = floatval( $post[ 'SEHrgBeli' ] ?? 0 );
        $post[ 'SEHrgJual' ]     = floatval( $post[ 'SEHrgJual' ] ?? 0 );
        $post[ 'SEJualDisc' ]    = floatval( $post[ 'SEAutoOLD' ] ?? 0 );
        $post[ 'SENoOLD' ]       = $post[ 'SENoOLD' ] ?? '--';
        $post[ 'SEAutoOLD' ]     = floatval( $post[ 'SDJualDisc' ] ?? 0 );
        $post[ 'BrgKodeOLD' ]    = $post[ 'BrgKodeOLD' ] ?? '--';
        try {
            General::cCmd( "CALL fseitbarang(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'SENo' ] )
                ->bindParam( 6, $post[ 'SEAuto' ] )
                ->bindParam( 7, $post[ 'BrgKode' ] )
                ->bindParam( 8, $post[ 'SEQty' ] )
                ->bindParam( 9, $post[ 'SEHrgBeli' ] )
                ->bindParam( 10, $post[ 'SEHrgJual' ] )
                ->bindParam( 11, $post[ 'SEJualDisc' ] )
                ->bindParam( 12, $post[ 'SENoOLD' ] )
                ->bindParam( 13, $post[ 'SEAutoOLD' ] )
                ->bindParam( 14, $post[ 'BrgKodeOLD' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
