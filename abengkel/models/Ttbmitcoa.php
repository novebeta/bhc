<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttbmitcoa".
 *
 * @property string $BMNo
 * @property int $BMAutoN
 * @property string $NoAccount
 * @property string $BMBayar
 * @property string $BMKeterangan
 */
class Ttbmitcoa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttbmitcoa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BMNo', 'BMAutoN', 'NoAccount'], 'required'],
            [['BMAutoN'], 'integer'],
            [['BMBayar'], 'number'],
            [['BMNo'], 'string', 'max' => 12],
            [['NoAccount'], 'string', 'max' => 10],
            [['BMKeterangan'], 'string', 'max' => 150],
            [['BMNo', 'BMAutoN', 'NoAccount'], 'unique', 'targetAttribute' => ['BMNo', 'BMAutoN', 'NoAccount']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'BMNo' => 'Bm No',
            'BMAutoN' => 'Bm Auto N',
            'NoAccount' => 'No Account',
            'BMBayar' => 'Bm Bayar',
            'BMKeterangan' => 'Bm Keterangan',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtbmitcoaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtbmitcoaQuery(get_called_class());
    }
}
