<?php

namespace abengkel\models;

/**
 * This is the ActiveQuery class for [[Tzcompany]].
 *
 * @see Tzcompany
 */
class TzcompanyQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tzcompany[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tzcompany|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
