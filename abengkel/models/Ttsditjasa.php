<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttsditjasa".
 *
 * @property string $SDNo
 * @property int $SDAuto
 * @property string $JasaKode
 * @property string $SDWaktuKerja
 * @property string $SDWaktuSatuan
 * @property string $SDWaktuKerjaMenit
 * @property string $SDHargaBeli
 * @property string $SDHrgJual
 * @property string $SDJualDisc
 * @property string $SDPajak
 */
class Ttsditjasa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttsditjasa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SDNo', 'SDAuto'], 'required'],
            [['SDAuto'], 'integer'],
            [['SDWaktuKerja', 'SDWaktuKerjaMenit', 'SDHargaBeli', 'SDHrgJual', 'SDJualDisc', 'SDPajak'], 'number'],
            [['SDNo', 'JasaKode'], 'string', 'max' => 10],
            [['SDWaktuSatuan'], 'string', 'max' => 5],
            [['SDNo', 'SDAuto'], 'unique', 'targetAttribute' => ['SDNo', 'SDAuto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SDNo' => 'Sd No',
            'SDAuto' => 'Sd Auto',
            'JasaKode' => 'Jasa Kode',
            'SDWaktuKerja' => 'Sd Waktu Kerja',
            'SDWaktuSatuan' => 'Sd Waktu Satuan',
            'SDWaktuKerjaMenit' => 'Sd Waktu Kerja Menit',
            'SDHargaBeli' => 'Sd Harga Beli',
            'SDHrgJual' => 'Sd Hrg Jual',
            'SDJualDisc' => 'Sd Jual Disc',
            'SDPajak' => 'Sd Pajak',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtsditjasaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtsditjasaQuery(get_called_class());
    }
}
