<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Tdprogramit]].
 *
 * @see Tdprogramit
 */
class TdprogramitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdprogramit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdprogramit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]       = Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]    = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]      = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'PrgNama' ]      = $post[ 'PrgNama' ] ?? '--';
        $post[ 'PrgAuto' ]      = floatval( $post[ 'PrgAuto' ] ?? 0 );
        $post[ 'BrgKode' ]      = $post[ 'BrgKode' ] ?? '--';
        $post[ 'PrgHrgJual' ]   = floatval( $post[ 'PrgHrgJual' ] ?? 0 );
        $post[ 'PrgDiscount' ]  = floatval( $post[ 'PrgDiscount' ] ?? 0 );
        $post[ 'Disc' ]         = floatval( $post[ 'Disc' ] ?? 0 );
        $post[ 'PrgNamaOLD' ]   = $post[ 'PrgNamaOLD' ] ?? '--';
        $post[ 'PrgAutoOLD' ]   = floatval( $post[ 'PrgAutoOLD' ] ?? 0 );
        $post[ 'BrgKodeOLD' ]   = $post[ 'BrgKodeOLD' ] ?? '--';
        try {
            General::cCmd( "CALL fprogramit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'PrgNama' ] )
                ->bindParam( 6, $post[ 'PrgAuto' ] )
                ->bindParam( 7, $post[ 'BrgKode' ] )
                ->bindParam( 8, $post[ 'PrgHrgJual' ] )
                ->bindParam( 9, $post[ 'Disc' ] )
                ->bindParam( 10, $post[ 'PrgDiscount' ] )
                ->bindParam( 11, $post[ 'PrgNamaOLD' ] )
                ->bindParam( 12, $post[ 'PrgAutoOLD' ] )
                ->bindParam( 13, $post[ 'BrgKodeOLD' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
