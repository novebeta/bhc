<?php
namespace abengkel\models;
use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
class TtgeneralledgerhdQuery extends \yii\db\ActiveQuery {

    public function all( $db = null ) {
        return parent::all( $db );
    }

    public function one( $db = null ) {
        return parent::one( $db );
    }

    public function GetLastValidasi() {
        return $this->select( new Expression( "IFNULL(TglGL, '1900-01-01') AS last" ) )
            ->where( "GLValid = 'Sudah'" )
            ->orderBy( "TGLGL DESC" )
            ->limit( 1 )
            ->scalar();
    }

    public function TglSaldoAwal() {
        $no = $nilai = $this->select( [ 'TglGL' ] )
            ->where( [ 'NoGL' => '00NA00000' ] )
            ->scalar();
        if($no === null || $no === false){
            $no = '1900-01-01';
        }
        return $no;
    }

    public function dsJurnalFill() {
        return $this->select("ttgeneralledgerhd.NoGL, ttgeneralledgerhd.TglGL, ttgeneralledgerhd.KodeTrans, ttgeneralledgerhd.MemoGL, ttgeneralledgerhd.TotalDebetGL, ttgeneralledgerhd.TotalKreditGL, ttgeneralledgerhd.GLLink, ttgeneralledgerhd.UserID, ttgeneralledgerhd.HPLink, ttgeneralledgerhd.GLValid");
    }

    public function dsJurnalFillByNo($NoGL, $TglGL) {
        return $this->select("ttgeneralledgerhd.NoGL, ttgeneralledgerhd.TglGL, ttgeneralledgerhd.KodeTrans, ttgeneralledgerhd.MemoGL, ttgeneralledgerhd.TotalDebetGL, ttgeneralledgerhd.TotalKreditGL, ttgeneralledgerhd.GLLink, ttgeneralledgerhd.UserID, ttgeneralledgerhd.HPLink, ttgeneralledgerhd.GLValid")
            ->where("(NoGL = :NoGL) AND (TglGL = :TglGL)", [ ':NoGL' => $NoGL, ':TglGL' => $TglGL ]);
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        if($post[ 'UserID' ] == '--'){
            $post[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
        }
        $post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'NoGL' ]          = $post[ 'NoGL' ] ?? '--';
        $post[ 'NoGLBaru' ]      = $post[ 'NoGLView' ] ?? '--';
        $post[ 'TglGL' ]         = date('Y-m-d H:i:s', strtotime($post[ 'TglGL' ] ?? date( 'Y-m-d H:i:s' )));
        $post[ 'KodeTrans' ]     = $post[ 'KodeTrans' ] ?? '--';
        $post[ 'MemoGL' ]        = $post[ 'MemoGL' ] ?? '--';
        $post[ 'TotalDebetGL' ]  = floatval( $post[ 'TotalDebetGL' ] ?? 0 );
        $post[ 'TotalKreditGL' ] = floatval( $post[ 'TotalKreditGL' ] ?? 0 );
        $post[ 'GLLink' ]        = $post[ 'GLLink' ] ?? '--';
        $post[ 'HPLink' ]        = $post[ 'HPLink' ] ?? '--';
        $post[ 'GLValid' ]       = $post[ 'GLValid' ] ?? '--';
        try {
            General::cCmd( "SET @NoGL = ?;" )->bindParam( 1, $post[ 'NoGL' ] )->execute();
            General::cCmd( "CALL fgeneralledgerhd(?,@Status,@Keterangan,?,?,?,@NoGL,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'NoGLBaru' ] )
                ->bindParam( 6, $post[ 'TglGL' ] )
                ->bindParam( 7, $post[ 'KodeTrans' ] )
                ->bindParam( 8, $post[ 'MemoGL' ] )
                ->bindParam( 9, $post[ 'TotalDebetGL' ] )
                ->bindParam( 10, $post[ 'TotalKreditGL' ] )
                ->bindParam( 11, $post[ 'GLLink' ] )
                ->bindParam( 12, $post[ 'HPLink' ] )
                ->bindParam( 13, $post[ 'GLValid' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @NoGL,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'NoGL'       => $post[ 'NoGL' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $NoGL       = $exe[ '@NoGL' ] ?? $post[ 'NoGL' ];
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'NoGL'       => $NoGL,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }

    public function FillByNo() {
        return $this->select( new Expression( "NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid" ) );
    }

}
