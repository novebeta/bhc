<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttuhit]].
 *
 * @see Ttuhit
 */
class TtuhitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttuhit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttuhit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'UHNo' ]          = $post[ 'UHNo' ] ?? '--';
        $post[ 'UHAuto' ]        = floatval( $post[ 'UHAuto' ] ?? 0 );
        $post[ 'BrgKode' ]       = $post[ 'BrgKode' ] ?? '--';
        $post[ 'UHHrgJualLama' ] = floatval( $post[ 'UHHrgJualLama' ] ?? 0 );
        $post[ 'UHHrgJualBaru' ] = floatval( $post[ 'UHHrgJualBaru' ] ?? 0 );
        $post[ 'UHNoOLD' ]       = $post[ 'UHNoOLD' ] ?? '--';
        $post[ 'UHAutoOLD' ]     = floatval( $post[ 'UHAutoOLD' ] ?? 0 );
        $post[ 'BrgKodeOLD' ]    = $post[ 'BrgKodeOLD' ] ?? '--';
        try {
            General::cCmd( "CALL fuhit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'UHNo' ] )
                ->bindParam( 6, $post[ 'UHAuto' ] )
                ->bindParam( 7, $post[ 'BrgKode' ] )
                ->bindParam( 8, $post[ 'UHHrgJualLama' ] )
                ->bindParam( 9, $post[ 'UHHrgJualBaru' ] )
                ->bindParam( 10, $post[ 'UHNoOLD' ] )
                ->bindParam( 11, $post[ 'UHAutoOLD' ] )
                ->bindParam( 12, $post[ 'BrgKodeOLD' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
