<?php
namespace abengkel\models;
use common\components\General;
use abengkel\components\Menu;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttsoit]].
 *
 * @see Ttsoit
 */
class TtsoitQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttsoit[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttsoit|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'PosKode' ]    = Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'SONo' ]       = $post[ 'SONo' ] ?? '--';
		$post[ 'SOAuto' ]     = floatval( $post[ 'SOAuto' ] ?? 0 );
		$post[ 'BrgKode' ]    = $post[ 'BrgKode' ] ?? '--';
		$post[ 'SOQty' ]      = floatval( $post[ 'SOQty' ] ?? 0 );
		$post[ 'SOHrgBeli' ]  = floatval( $post[ 'SOHrgBeli' ] ?? 0 );
		$post[ 'SOHrgJual' ]  = floatval( $post[ 'SOHrgJual' ] ?? 0 );
		$post[ 'SODiscount' ] = floatval( $post[ 'SODiscount' ] ?? 0 );
		$post[ 'SOPajak' ]    = floatval( $post[ 'SOPajak' ] ?? 0 );
		$post[ 'SINo' ]       = $post[ 'SINo' ] ?? '--';
		$post[ 'PONo' ]       = $post[ 'PONo' ] ?? '--';
		$post[ 'SIQtyS' ]     = floatval( $post[ 'SIQtyS' ] ?? 0 );
		$post[ 'POQtyS' ]     = floatval( $post[ 'POQtyS' ] ?? 0 );
		$post[ 'SONoOLD' ]    = $post[ 'SONoOLD' ] ?? '--';
		$post[ 'SOAutoOLD' ]  = floatval( $post[ 'SOAutoOLD' ] ?? 0 );
		$post[ 'BrgKodeOLD' ] = $post[ 'BrgKodeOLD' ] ?? '--';
        try {
            General::cCmd( "CALL fsoit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                   ->bindParam( 1, $post[ 'Action' ] )
                   ->bindParam( 2, $post[ 'UserID' ] )
                   ->bindParam( 3, $post[ 'KodeAkses' ] )
                   ->bindParam( 4, $post[ 'PosKode' ] )
                   ->bindParam( 5, $post[ 'SONo' ] )
                   ->bindParam( 6, $post[ 'SOAuto' ] )
                   ->bindParam( 7, $post[ 'BrgKode' ] )
                   ->bindParam( 8, $post[ 'SOQty' ] )
                   ->bindParam( 9, $post[ 'SOHrgBeli' ] )
                   ->bindParam( 10, $post[ 'SOHrgJual' ] )
                   ->bindParam( 11, $post[ 'SODiscount' ] )
                   ->bindParam( 12, $post[ 'SOPajak' ] )
                   ->bindParam( 13, $post[ 'SINo' ] )
                   ->bindParam( 14, $post[ 'PONo' ] )
                   ->bindParam( 15, $post[ 'SIQtyS' ] )
                   ->bindParam( 16, $post[ 'POQtyS' ] )
                   ->bindParam( 17, $post[ 'SONoOLD' ] )
                   ->bindParam( 18, $post[ 'SOAutoOLD' ] )
                   ->bindParam( 19, $post[ 'BrgKodeOLD' ] )
                   ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
	}
}
