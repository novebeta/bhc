<?php

namespace abengkel\models;

/**
 * This is the model class for table "tsaldokas".
 *
 * @property string $SaldoKasTgl
 * @property string $KasKode
 * @property string $SaldoKasAwal
 * @property string $SaldoKasMasuk
 * @property string $SaldoKasKeluar
 * @property string $SaldoKasSaldo
 */
class Tsaldoka extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tsaldokas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SaldoKasTgl', 'KasKode'], 'required'],
            [['SaldoKasTgl'], 'safe'],
            [['SaldoKasAwal', 'SaldoKasMasuk', 'SaldoKasKeluar', 'SaldoKasSaldo'], 'number'],
            [['KasKode'], 'string', 'max' => 25],
            [['SaldoKasTgl', 'KasKode'], 'unique', 'targetAttribute' => ['SaldoKasTgl', 'KasKode']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SaldoKasTgl' => 'Saldo Kas Tgl',
            'KasKode' => 'Kas Kode',
            'SaldoKasAwal' => 'Saldo Kas Awal',
            'SaldoKasMasuk' => 'Saldo Kas Masuk',
            'SaldoKasKeluar' => 'Saldo Kas Keluar',
            'SaldoKasSaldo' => 'Saldo Kas Saldo',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TsaldokaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TsaldokaQuery(get_called_class());
    }
}
