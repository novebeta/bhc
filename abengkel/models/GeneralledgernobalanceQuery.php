<?php

namespace abengkel\models;

/**
 * This is the ActiveQuery class for [[Generalledgernobalance]].
 *
 * @see Generalledgernobalance
 */
class GeneralledgernobalanceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Generalledgernobalance[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Generalledgernobalance|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
