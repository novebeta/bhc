<?php

namespace abengkel\models;

/**
 * This is the ActiveQuery class for [[Tvhutangbayar]].
 *
 * @see Tvhutangbayar
 */
class TvhutangbayarQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tvhutangbayar[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tvhutangbayar|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
