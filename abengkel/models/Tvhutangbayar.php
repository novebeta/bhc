<?php

namespace abengkel\models;

/**
 * This is the model class for table "tvhutangbayar".
 *
 * @property string $HPLink
 * @property string $NoAccount
 * @property string $SumDebetGL
 */
class Tvhutangbayar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tvhutangbayar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['HPLink', 'NoAccount'], 'required'],
            [['SumDebetGL'], 'number'],
            [['HPLink'], 'string', 'max' => 18],
            [['NoAccount'], 'string', 'max' => 10],
            [['HPLink', 'NoAccount'], 'unique', 'targetAttribute' => ['HPLink', 'NoAccount']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'HPLink' => 'Hp Link',
            'NoAccount' => 'No Account',
            'SumDebetGL' => 'Sum Debet Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TvhutangbayarQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TvhutangbayarQuery(get_called_class());
    }
}
