<?php

namespace abengkel\models;

/**
 * This is the model class for table "tdarea".
 *
 * @property string $Provinsi
 * @property string $Kabupaten
 * @property string $Kecamatan
 * @property string $Kelurahan
 * @property string $KodePos
 * @property string $AreaStatus
 * @property string $ProvinsiAstra
 * @property string $KabupatenAstra
 */
class Tdarea extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdarea';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Provinsi', 'Kabupaten', 'Kecamatan', 'Kelurahan', 'KodePos', 'ProvinsiAstra', 'KabupatenAstra'], 'required'],
            [['Provinsi'], 'string', 'max' => 30],
            [['Kabupaten', 'Kecamatan', 'Kelurahan'], 'string', 'max' => 50],
            [['KodePos'], 'string', 'max' => 7],
            [['AreaStatus'], 'string', 'max' => 1],
            [['ProvinsiAstra'], 'string', 'max' => 38],
            [['KabupatenAstra'], 'string', 'max' => 65],
            [['Provinsi', 'Kabupaten', 'Kecamatan', 'Kelurahan', 'KodePos'], 'unique', 'targetAttribute' => ['Provinsi', 'Kabupaten', 'Kecamatan', 'Kelurahan', 'KodePos']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Provinsi' => 'Provinsi',
            'Kabupaten' => 'Kabupaten',
            'Kecamatan' => 'Kecamatan',
            'Kelurahan' => 'Kelurahan',
            'KodePos' => 'Kode Pos',
            'AreaStatus' => 'Area Status',
            'ProvinsiAstra' => 'Provinsi Astra',
            'KabupatenAstra' => 'Kabupaten Astra',
        ];
    }

	public static function colGrid()
	{
		return [
            'Provinsi'       => ['width' => 90,'label' => 'Provinsi','name'  => 'Provinsi'],
            'Kabupaten'      => ['width' => 105,'label' => 'Kabupaten','name'  => 'Kabupaten'],
            'Kecamatan'      => ['width' => 105,'label' => 'Kecamatan','name'  => 'Kecamatan'],
            'Kelurahan'      => ['width' => 105,'label' => 'Kelurahan','name'  => 'Kelurahan'],
            'KodePos'        => ['width' => 57,'label' => 'Kode Pos','name'  => 'KodePos'],
            'AreaStatus'     => ['width' => 55,'label' => 'Status','name'  => 'AreaStatus'],
            'ProvinsiAstra'  => ['width' => 90,'label' => 'Provinsi Astra','name'  => 'ProvinsiAstra'],
            'KabupatenAstra' => ['width' => 100,'label' => 'Kabupaten Astra','name'  => 'KabupatenAstra'],
            'KecamatanAstra' => ['width' => 100,'label' => 'Kecamatan Astra','name'  => 'KecamatanAstra'],
            'KelurahanAstra' => ['width' => 100,'label' => 'Kelurahan Astra','name'  => 'KelurahanAstra'],
		];
	}
    /**
     * {@inheritdoc}
     * @return TdareaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdareaQuery(get_called_class());
    }
}
