<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttbmhd".
 *
 * @property string $BMNo
 * @property string $BMTgl
 * @property string $BMJenis
 * @property string $KodeTrans
 * @property string $PosKode
 * @property string $MotorNoPolisi
 * @property string $BankKode
 * @property string $BMPerson
 * @property string $BMCekNo
 * @property string $BMCekTempo
 * @property string $BMAC
 * @property string $BMNominal
 * @property string $BMMemo
 * @property string $NoGL
 * @property string $Cetak
 * @property string $UserID
 */
class Ttbmhd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttbmhd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BMNo'], 'required'],
            [['BMTgl', 'BMCekTempo'], 'safe'],
            [['BMNominal'], 'number'],
            [['BMNo', 'MotorNoPolisi'], 'string', 'max' => 12],
            [['BMJenis', 'PosKode'], 'string', 'max' => 20],
            [['KodeTrans'], 'string', 'max' => 4],
            [['BankKode', 'BMCekNo', 'BMAC'], 'string', 'max' => 25],
            [['BMPerson'], 'string', 'max' => 50],
            [['BMMemo'], 'string', 'max' => 150],
            [['NoGL'], 'string', 'max' => 10],
            [['Cetak'], 'string', 'max' => 5],
            [['UserID'], 'string', 'max' => 15],
            [['BMNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'BMNo'          => 'Bm No',
            'BMTgl'         => 'Bm Tgl',
            'BMJenis'       => 'Bm Jenis',
            'KodeTrans'     => 'Kode Trans',
            'PosKode'       => 'Pos Kode',
            'MotorNoPolisi' => 'Motor No Polisi',
            'BankKode'      => 'Bank Kode',
            'BMPerson'      => 'Bm Person',
            'BMCekNo'       => 'Bm Cek No',
            'BMCekTempo'    => 'Bm Cek Tempo',
            'BMAC'          => 'Bmac',
            'BMNominal'     => 'Bm Nominal',
            'BMMemo'        => 'Bm Memo',
            'NoGL'          => 'No Gl',
            'Cetak'         => 'Cetak',
            'UserID'        => 'User ID',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function colGrid() {
        return [
            'BMNo'           => ['width' => 95,'label' => 'No BM','name'  => 'BMNo'],
            'BMTgl'          => ['width' => 120,'label' => 'Tanggal BM','name'  => 'BMTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y  H:i:s'] ],
            'KodeTrans'      => ['width' => 40,'label' => 'TC','name'  => 'KodeTrans'],
            'BankKode'       => ['width' => 70,'label' => 'Kode Bank','name'  => 'BankKode'],
            'NamaAccount'    => ['width' => 205,'label' => 'Nama Bank','name'  => 'NamaAccount'],
            'MotorNoPolisi'  => ['width' => 90,'label' => 'NoPolisi','name'  => 'MotorNoPolisi'],
            'BMPerson'       => ['width' => 215,'label' => 'Terima Dari','name'  => 'BMPerson'],
            'BMNominal'      => ['width' => 100,'label' => 'Nominal','name'  => 'BMNominal','formatter' => 'number', 'align' => 'right'],
            'BMJenis'        => ['width' => 50,'label' => 'Jenis','name'  => 'BMJenis'],
            'BMCekNo'        => ['width' => 70,'label' => 'No Cek','name'  => 'BMCekNo'],
            'BMAC'           => ['width' => 70,'label' => 'AC','name'  => 'BMAC'],
            'BMCekTempo'     => ['width' => 70,'label' => 'AC Tempo','name'  => 'BMCekTempo', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y'] ],
            'NoGL'			 => ['width' => 70,'label' => 'No GL','name'  => 'NoGL'],
            'UserID'         => ['width' => 80,'label' => 'User','name'  => 'UserID'],
            'Cetak'          => ['width' => 50,'label' => 'Cetak','name'  => 'Cetak'],      
            'BMMemo'         => ['width' => 500,'label' => 'Keterangan','name'  => 'BMMemo'],      
            'PosKode'		 => ['width' => 100,'label' => 'PosKode','name'  => 'PosKode'],		
        ];
    }
	public static function colGridPickServisInvoice() {
		return [
			'No'        => [ 'width' => 95, 'label' => 'No Servis', 'name' => 'NO' ],
			'Tgl'       => [ 'width' => 120, 'label' => 'Tgl Servis', 'name' => 'Tgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y  H:i:s' ] ],
			'Kode'      => [ 'width' => 70, 'label' => 'No Polisi', 'name' => 'Kode' ],
			'Nama'      => [ 'width' => 130, 'label' => 'Nama Konsumen', 'name' => 'Nama' ],
			'MotorNama' => [ 'width' => 160, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
			'Total'     => [ 'width' => 80, 'label' => 'Total', 'name' => 'Total', 'formatter' => 'number', 'align' => 'right' ],
			'Terbayar'  => [ 'width' => 80, 'label' => 'Terbayar', 'name' => 'Terbayar', 'formatter' => 'number', 'align' => 'right' ],
			'Sisa'      => [ 'width' => 80, 'label' => 'Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
		];
	}
    public static function colGridPickSalesInvoice() {
        return [
            'No'        => [ 'width' => 95, 'label' => 'No Jual', 'name' => 'NO' ],
            'Tgl'       => [ 'width' => 120, 'label' => 'Tgl Jual', 'name' => 'Tgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y  H:i:s' ] ],
            'Kode'      => [ 'width' => 70, 'label' => 'No Polisi', 'name' => 'Kode' ],
            'Nama'      => [ 'width' => 130, 'label' => 'Nama Konsumen', 'name' => 'Nama' ],
            'MotorNama' => [ 'width' => 160, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
            'Total'     => [ 'width' => 80, 'label' => 'Total', 'name' => 'Total', 'formatter' => 'number', 'align' => 'right' ],
            'Terbayar'  => [ 'width' => 80, 'label' => 'Terbayar', 'name' => 'Terbayar', 'formatter' => 'number', 'align' => 'right' ],
            'Sisa'      => [ 'width' => 80, 'label' => 'Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
        ];
    }

    public static function colGridPickEstimasiServis() {
        return [
            'No'        => [ 'width' => 95, 'label' => 'No Estimasi', 'name' => 'NO' ],
            'Tgl'       => [ 'width' => 120, 'label' => 'Tgl Estimasi', 'name' => 'Tgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y  H:i:s' ] ],
            'Kode'      => [ 'width' => 70, 'label' => 'No Polisi', 'name' => 'Kode' ],
            'Nama'      => [ 'width' => 130, 'label' => 'Nama Konsumen', 'name' => 'Nama' ],
            'MotorNama' => [ 'width' => 160, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
            'Total'     => [ 'width' => 80, 'label' => 'Total', 'name' => 'Total', 'formatter' => 'number', 'align' => 'right' ],
            'Terbayar'  => [ 'width' => 80, 'label' => 'Terbayar', 'name' => 'Terbayar', 'formatter' => 'number', 'align' => 'right' ],
            'Sisa'      => [ 'width' => 80, 'label' => 'Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
        ];
    }

    public static function colGridPickSalesOrder() {
        return [
            'No'        => [ 'width' => 95, 'label' => 'No Order', 'name' => 'NO' ],
            'Tgl'       => [ 'width' => 120, 'label' => 'Tgl Order', 'name' => 'Tgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y  H:i:s' ] ],
            'Kode'      => [ 'width' => 70, 'label' => 'No Polisi', 'name' => 'Kode' ],
            'Nama'      => [ 'width' => 130, 'label' => 'Nama Konsumen', 'name' => 'Nama' ],
            'MotorNama' => [ 'width' => 160, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
            'Total'     => [ 'width' => 80, 'label' => 'Total', 'name' => 'Total', 'formatter' => 'number', 'align' => 'right' ],
            'Terbayar'  => [ 'width' => 80, 'label' => 'Terbayar', 'name' => 'Terbayar', 'formatter' => 'number', 'align' => 'right' ],
            'Sisa'      => [ 'width' => 80, 'label' => 'Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
        ];
    }

    public static function colGridPickSalesRetur() {
        return [
            'No'        => [ 'width' => 95, 'label' => 'No Retur Beli', 'name' => 'NO' ],
            'Tgl'       => [ 'width' => 120, 'label' => 'Tgl Retur', 'name' => 'Tgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y  H:i:s' ] ],
            'Kode'      => [ 'width' => 70, 'label' => 'Supplier', 'name' => 'Kode' ],
            'Nama'      => [ 'width' => 70, 'label' => 'Nama Supplier', 'name' => 'Nama' ],
            'MotorNama' => [ 'width' => 140, 'label' => 'Lokasi', 'name' => 'MotorNama' ],
            'Total'     => [ 'width' => 100, 'label' => 'Total', 'name' => 'Total', 'formatter' => 'number', 'align' => 'right' ],
            'Terbayar'  => [ 'width' => 100, 'label' => 'Terbayar', 'name' => 'Terbayar', 'formatter' => 'number', 'align' => 'right' ],
            'Sisa'      => [ 'width' => 100, 'label' => 'Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
        ];
    }



    public function getAccount() {
        return $this->hasOne( Traccount::className(), [ 'NoAccount' => 'BankKode' ] );
    }
    public static function generateBMNo($BankKode, $temporary = false) {
	    $sign = $temporary ? '=': '-';
        $kode = 'BM';
        $kBank = substr($BankKode, -2);
        $yNow = date("y");
        $maxNoGl = (new \yii\db\Query())
            ->from(self::tableName())
            ->where("BMNo LIKE '$kode$kBank$yNow$sign%'")
            ->max('BMNo');

        if($maxNoGl && preg_match("/$kode\d{4}$sign(\d+)/", $maxNoGl, $result)) {
            list($all, $counter) = $result;
            $digitCount = strlen($counter);
            $val = (int)$counter;
            $nextCounter = sprintf('%0'.$digitCount.'d', ++$val);
        } else {
            $nextCounter = "00001";
        }

        return "$kode$kBank$yNow$sign$nextCounter";

    }

    /**
     * {@inheritdoc}
     * @return TtbmhdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtbmhdQuery(get_called_class());
    }

    public static function getRoute( $BMJenis, $param = []) {
        $index  = [];
        $update = [];
        $create = [];
        switch ( $BMJenis ) {
            case 'H1' :
                $index = [ 'ttbmhd/bank-masuk-dari-h1' ];
                $update = [ 'ttbmhd/bank-masuk-dari-h1-update' ];
                $create = [ 'ttbmhd/bank-masuk-dari-h1-create' ];
                break;
            case 'UM' :
                $index = [ 'ttbmhd/bank-masuk-umum' ];
                $update = [ 'ttbmhd/bank-masuk-umum-update' ];
                $create = [ 'ttbmhd/bank-masuk-umum-create' ];
                break;
            case 'KK' :
                $index = [ 'ttbmhd/bank-masuk-dari-kas' ];
                $update = [ 'ttbmhd/bank-masuk-dari-kas-update' ];
                $create = [ 'ttbmhd/bank-masuk-dari-kas-create' ];
                break;
            case 'SV' :
                $index = [ 'ttbmhd/bank-masuk-service-invoice' ];
                $update = [ 'ttbmhd/bank-masuk-service-invoice-update' ];
                $create = [ 'ttbmhd/bank-masuk-service-invoice-create' ];
                break;
            case 'SI' :
                $index = [ 'ttbmhd/bank-masuk-penjualan' ];
                $update = [ 'ttbmhd/bank-masuk-penjualan-update' ];
                $create = [ 'ttbmhd/bank-masuk-penjualan-create' ];
                break;
            case 'SE' :
                $index = [ 'ttbmhd/bank-masuk-estimasi' ];
                $update = [ 'ttbmhd/bank-masuk-estimasi-update' ];
                $create = [ 'ttbmhd/bank-masuk-estimasi-create' ];
                break;
            case 'SO' :
                $index = [ 'ttbmhd/bank-masuk-order-penjualan' ];
                $update = [ 'ttbmhd/bank-masuk-order-penjualan-update' ];
                $create = [ 'ttbmhd/bank-masuk-order-penjualan-create' ];
                break;
            case 'PR' :
                $index = [ 'ttbmhd/bank-masuk-retur-pembelian' ];
                $update = [ 'ttbmhd/bank-masuk-retur-pembelian-update' ];
                $create = [ 'ttbmhd/bank-masuk-retur-pembelian-create' ];
                break;
            case 'CK' :
                $index = [ 'ttbmhd/bank-masuk-klaim-kpb' ];
                $update = [ 'ttbmhd/bank-masuk-klaim-kpb-update' ];
                $create = [ 'ttbmhd/bank-masuk-klaim-kpb-create' ];
                break;
            case 'CC' :
                $index = [ 'ttbmhd/bank-masuk-klaim-c2' ];
                $update = [ 'ttbmhd/bank-masuk-klaim-c2-update' ];
                $create = [ 'ttbmhd/bank-masuk-klaim-c2-create' ];
                break;
        }
        return [
            'index'  => \yii\helpers\Url::toRoute( array_merge( $index, $param ) ),
            'update' => \yii\helpers\Url::toRoute( array_merge( $update, $param ) ),
            'create' => \yii\helpers\Url::toRoute( array_merge( $create, $param ) ),
        ];
    }
}
