<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttbkitcoa]].
 *
 * @see Ttbkitcoa
 */
class TtbkitcoaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttbkitcoa[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttbkitcoa|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	public function callSP( $post ) {
		$post[ 'UserID' ]        = Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'BKNo' ]          = $post[ 'BKNo' ] ?? '--';
		$post[ 'BKAutoN' ]       = floatval( $post[ 'BKAutoN' ] ?? 0 );
		$post[ 'NoAccount' ]     = $post[ 'NoAccount' ] ?? '--';
		$post[ 'BKKeterangan' ]  = $post[ 'BKKeterangan' ] ?? '--';
		$post[ 'BKBayar' ]       = floatval( $post[ 'BKBayar' ] ?? 0 );
        $post[ 'BKNoOLD' ]       = $post[ 'BKNoOLD' ] ?? '--';
        $post[ 'BKAutoNOLD' ]    = floatval( $post[ 'BKAutoNOLD' ] ?? 0 );
        $post[ 'NoAccountOLD' ]  = $post[ 'NoAccountOLD' ] ?? '--';
        try {
		General::cCmd( "CALL fbkitcoa(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?);" )
		       ->bindParam( 1, $post[ 'Action' ] )
		       ->bindParam( 2, $post[ 'UserID' ] )
		       ->bindParam( 3, $post[ 'KodeAkses' ] )
		       ->bindParam( 4, $post[ 'PosKode' ] )
		       ->bindParam( 5, $post[ 'BKNo' ] )
		       ->bindParam( 6, $post[ 'BKAutoN' ] )
		       ->bindParam( 7, $post[ 'NoAccount' ] )
		       ->bindParam( 8, $post[ 'BKKeterangan' ] )
		       ->bindParam( 9, $post[ 'BKBayar' ] )
		       ->bindParam( 10, $post[ 'BKNoOLD' ] )
		       ->bindParam( 11, $post[ 'BKAutoNOLD' ] )
		       ->bindParam( 12, $post[ 'NoAccountOLD' ] )
		       ->execute();
		$exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
	}
}
