<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;

/**
 * This is the ActiveQuery class for [[Ttuhhd]].
 *
 * @see Ttuhhd
 */
class TtuhhdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttuhhd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttuhhd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function ttuhhdFillByNo() {
        return $this->select( new Expression( "UHNo, UHTgl, UHKeterangan, KodeTrans, PosKode, Cetak, UserID"));
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'UHNo' ]          = $post[ 'UHNo' ] ?? '--';
        $post[ 'UHNoBaru' ]      = $post[ 'UHNoView' ] ?? '--';
        $post[ 'UHTgl' ]         = date('Y-m-d H:i:s', strtotime($post[ 'UHTgl' ] ?? date( 'Y-m-d H:i:s' )));
        $post[ 'UHKeterangan' ]  = $post[ 'UHKeterangan' ] ?? '--';
        $post[ 'KodeTrans' ]     = $post[ 'KodeTrans' ] ?? 'UH';
        $post[ 'LokasiKode' ]    = $post[ 'LokasiKode' ] ?? '--';
        $post[ 'NoGL' ]          = $post[ 'NoGL' ] ?? '--';
        $post[ 'Cetak' ]         = $post[ 'Cetak' ] ?? 'Belum';
        try {
            General::cCmd( "SET @UHNo = ?;" )->bindParam( 1, $post[ 'UHNo' ] )->execute();
            General::cCmd( "CALL fuhhd(?,@Status,@Keterangan,?,?,?,@UHNo,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'UHNoBaru' ] )
                ->bindParam( 6, $post[ 'UHTgl' ] )
                ->bindParam( 7, $post[ 'UHKeterangan' ] )
                ->bindParam( 8, $post[ 'KodeTrans' ] )
                ->bindParam( 9, $post[ 'LokasiKode' ] )
                ->bindParam( 10, $post[ 'NoGL' ] )
                ->bindParam( 11, $post[ 'Cetak' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @UHNo,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'UHNo'       => $post[ 'UHNo' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $UHNo       = $exe[ '@UHNo' ] ?? $post[ 'UHNo' ];
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'UHNo'       => $UHNo,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
