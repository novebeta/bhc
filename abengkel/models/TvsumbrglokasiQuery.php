<?php

namespace abengkel\models;

/**
 * This is the ActiveQuery class for [[Tvsumbrglokasi]].
 *
 * @see Tvsumbrglokasi
 */
class TvsumbrglokasiQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tvsumbrglokasi[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tvsumbrglokasi|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
