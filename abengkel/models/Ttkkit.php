<?php
namespace abengkel\models;
use abengkel\components\Menu;
/**
 * This is the model class for table "ttkkit".
 *
 * @property string $KKNo
 * @property string $KKLink
 * @property string $KKBayar
 */
class Ttkkit extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttkkit';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'KKNo', 'KKLink' ], 'required' ],
			[ [ 'KKBayar' ], 'number' ],
			[ [ 'KKNo' ], 'string', 'max' => 12 ],
			[ [ 'KKLink' ], 'string', 'max' => 18 ],
			[ [ 'KKNo', 'KKLink' ], 'unique', 'targetAttribute' => [ 'KKNo', 'KKLink' ] ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'KKNo'    => 'Kk No',
			'KKLink'  => 'Kk Link',
			'KKBayar' => 'Kk Bayar',
		];
	}
	/**
	 * {@inheritdoc}
	 * @return TtkkitQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TtkkitQuery( get_called_class() );
	}
	public static function generateKTNo( $temporary = false ) {
		$sign    = $temporary ? '=' : '-';
		$kode    = 'KT';
		$posNo   = Menu::getUserLokasi()[ 'PosNomor' ];
		$yNow    = date( "y" );
		$maxNoGl = ( new \yii\db\Query() )
			->from( self::tableName() )
			->where( "KKLink LIKE '$kode$posNo$yNow$sign%'" )
			->max( 'KKLink' );
		if ( $maxNoGl && preg_match( "/$kode\d{4}$sign(\d+)/", $maxNoGl, $result ) ) {
			[ $all, $counter ] = $result;
			$digitCount  = strlen( $counter );
			$val         = (int) $counter;
			$nextCounter = sprintf( '%0' . $digitCount . 'd', ++ $val );
		} else {
			$nextCounter = "00001";
		}
		return "$kode$posNo$yNow$sign$nextCounter";
	}
}
