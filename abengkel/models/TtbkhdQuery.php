<?php
namespace abengkel\models;
use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttbkhd]].
 *
 * @see Ttbkhd
 */
class TtbkhdQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttbkhd[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttbkhd|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function ttbkhdFillGetData() {
		return $this->select( new Expression( "ttbkhd.BKNo, ttbkhd.BKTgl, ttbkhd.BKJenis, ttbkhd.BankKode, ttbkhd.SupKode, ttbkhd.KodeTrans, ttbkhd.PosKode, ttbkhd.NoGL, ttbkhd.BKCekTempo, 
            ttbkhd.BKAC, ttbkhd.BKPerson, ttbkhd.BKCekNo, ttbkhd.BKNominal, ttbkhd.BKMemo, ttbkhd.Cetak, ttbkhd.UserID, traccount.NamaAccount" ) )
		            ->join( "LEFT JOIN", "traccount", "ttbkhd.BankKode = traccount.NoAccount" );
	}
	public function transferBankGetData( $link ) {
		$BK = General::cCmd( "SELECT  ttbkhd.BKNo,  ttbkhd.BKTgl,  ttbkhd.BKJenis,  ttbkhd.KodeTrans,  ttbkhd.PosKode,
  ttbkhd.SupKode,  ttbkhd.BankKode AS NoAccountBK,  ttbkhd.BKCekTempo,  ttbkhd.BKAC,  ttbkhd.BKPerson,  ttbkhd.BKCekNo,
  ttbkhd.BKNominal,  ttbkhd.BKMemo,  ttbkhd.NoGL AS NoGLBK,  ttbkhd.Cetak,  ttbkhd.UserID, ttbkit.BKLink, traccount.NamaAccount FROM ttbkhd
			LEFT JOIN ttbkit ON ttbkhd.bkNo = ttbkit.bkNo
			LEFT JOIN traccount ON ttbkhd.BankKode = traccount.NoAccount
			WHERE  (BKLink = :BKLink)", [ ':BKLink' => $link ] )->queryOne();
		$BM = General::cCmd( "SELECT ttbmhd.BMNo,  ttbmhd.BMTgl,  ttbmhd.BMJenis,  ttbmhd.KodeTrans,  ttbmhd.PosKode,
  ttbmhd.MotorNoPolisi,  ttbmhd.BankKode AS NoAccountBM,  ttbmhd.BMPerson,  ttbmhd.BMCekNo,  ttbmhd.BMCekTempo,  ttbmhd.BMAC,  ttbmhd.BMNominal,
  ttbmhd.BMMemo,  ttbmhd.NoGL AS NoGLBM,  ttbmhd.Cetak,  ttbmhd.UserID, ttbmit.BMLink, traccount.NamaAccount FROM ttbmhd
			LEFT JOIN ttbmit ON ttbmhd.BMNo = ttbmit.BMNo
			LEFT JOIN traccount ON ttbmhd.BankKode = traccount.NoAccount
			WHERE  (BMLink = :BMLink)", [ ':BMLink' => $link ] )->queryOne();
		if ( $BK === false ) {
			$BK = [];
		}
		if ( $BM === false ) {
			$BM = [];
		}
		return array_merge( $BK, $BM );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'PosKode' ]    = Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'BKNo' ]       = $post[ 'BKNo' ] ?? '--';
		$post[ 'BKNoBaru' ]   = $post[ 'BKNoView' ] ?? '--';
		$post[ 'BKTgl' ]      = date( 'Y-m-d H:i:s', strtotime( $post[ 'BKTgl' ] ?? date( 'Y-m-d H:i:s' ) ) );
		$post[ 'BKJenis' ]    = $post[ 'BKJenis' ] ?? '--';
		$post[ 'KodeTrans' ]  = $post[ 'KodeTrans' ] ?? '--';
		$post[ 'LokasiKode' ] = $post[ 'LokasiKode' ] ?? '--';
		$post[ 'SupKode' ]    = $post[ 'SupKode' ] ?? '--';
		$post[ 'BankKode' ]   = $post[ 'BankKode' ] ?? '--';
		$post[ 'BKCekTempo' ] = date( 'Y-m-d', strtotime( $post[ 'BKCekTempo' ] ?? date( 'Y-m-d' ) ) );
		$post[ 'BKAC' ]       = $post[ 'BKAC' ] ?? '--';
		$post[ 'BKPerson' ]   = $post[ 'BKPerson' ] ?? '--';
		$post[ 'BKCekNo' ]    = $post[ 'BKCekNo' ] ?? '--';
		$post[ 'BKNominal' ]  = floatval( $post[ 'BKNominal' ] ?? 0 );
		$post[ 'BKMemo' ]     = $post[ 'BKMemo' ] ?? '--';
		$post[ 'NoGL' ]       = $post[ 'NoGL' ] ?? '--';
		$post[ 'Cetak' ]      = $post[ 'Cetak' ] ?? 'Belum';
		try {
			General::cCmd( "SET @BKNo = ?;" )->bindParam( 1, $post[ 'BKNo' ] )->execute();
			General::cCmd( "CALL fbkhd(?,@Status,@Keterangan,?,?,?,@BKNo,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'BKNoBaru' ] )
			       ->bindParam( 6, $post[ 'BKTgl' ] )
			       ->bindParam( 7, $post[ 'BKJenis' ] )
			       ->bindParam( 8, $post[ 'KodeTrans' ] )
			       ->bindParam( 9, $post[ 'PosKode' ] )
			       ->bindParam( 10, $post[ 'SupKode' ] )
			       ->bindParam( 11, $post[ 'BankKode' ] )
			       ->bindParam( 12, $post[ 'BKCekTempo' ] )
			       ->bindParam( 13, $post[ 'BKAC' ] )
			       ->bindParam( 14, $post[ 'BKPerson' ] )
			       ->bindParam( 15, $post[ 'BKCekNo' ] )
			       ->bindParam( 16, $post[ 'BKNominal' ] )
			       ->bindParam( 17, $post[ 'BKMemo' ] )
			       ->bindParam( 18, $post[ 'NoGL' ] )
			       ->bindParam( 19, $post[ 'Cetak' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @BKNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'BKNo'       => $post[ 'BKNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$BKNo       = $exe[ '@BKNo' ] ?? $post[ 'BKNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'BKNo'       => $BKNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
	public function callSPTransfer( $post ) {
		$post[ 'UserID' ]      = Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]   = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'PosKode' ]     = Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'BTNo' ]        = $post[ 'BTNo' ] ?? '--';
		$post[ 'BTNoBaru' ]    = $post[ 'BKNoBaru' ] ?? '--';
		$post[ 'BTTgl' ]       = $post[ 'BTTgl' ] ?? date( 'Y-m-d' );
		$post[ 'BTMemo' ]      = $post[ 'BKMemo' ] ?? '--';
		$post[ 'BTNominal' ]   = $post[ 'BKNominal' ] ?? 0;
		$post[ 'BTJenis' ]     = $post[ 'BKJenis' ] ?? '--';
		$post[ 'BKCekNo' ]     = $post[ 'BKCekNo' ] ?? '--';
		$post[ 'BKCekTempo' ]  = $post[ 'BKCekTempo' ] ?? date( 'Y-m-d' );
		$post[ 'BKPerson' ]    = $post[ 'BKPerson' ] ?? '--';
		$post[ 'BKAC' ]        = $post[ 'BKAC' ] ?? '--';
		$post[ 'NoAccountBK' ] = $post[ 'NoAccountBK' ] ?? '--';
		$post[ 'BKNoGL' ]      = $post[ 'BKNoGL' ] ?? '--';
		$post[ 'BMCekNo' ]     = $post[ 'BMCekNo' ] ?? '--';
		$post[ 'BMCekTempo' ]  = $post[ 'BMCekTempo' ] ?? date( 'Y-m-d' );
		$post[ 'BMPerson' ]    = $post[ 'BMPerson' ] ?? '--';
		$post[ 'BMAC' ]        = $post[ 'BMAC' ] ?? '--';
		$post[ 'NoAccountBM' ] = $post[ 'NoAccountBM' ] ?? '--';
		$post[ 'BMNoGL' ]      = $post[ 'BMNoGL' ] ?? '--';
		$post[ 'BKNo' ]        = $post[ 'BKNo' ] ?? '--';
		$post[ 'BMNo' ]        = $post[ 'BMNo' ] ?? '--';
		try {
			General::cCmd( "SET @BTNo = ?;" )->bindParam( 1, $post[ 'BTNo' ] )->execute();
			General::cCmd( "CALL fbt(?,@Status,@Keterangan,?,?,?,@BTNo,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'PosKode' ] )
			       ->bindParam( 5, $post[ 'BTNoBaru' ] )
			       ->bindParam( 6, $post[ 'BTTgl' ] )
			       ->bindParam( 7, $post[ 'BTMemo' ] )
			       ->bindParam( 8, $post[ 'BTNominal' ] )
			       ->bindParam( 9, $post[ 'BTJenis' ] )
			       ->bindParam( 10, $post[ 'BKCekNo' ] )
			       ->bindParam( 11, $post[ 'BKCekTempo' ] )
			       ->bindParam( 12, $post[ 'BKPerson' ] )
			       ->bindParam( 13, $post[ 'BKAC' ] )
			       ->bindParam( 14, $post[ 'NoAccountBK' ] )
			       ->bindParam( 15, $post[ 'BKNoGL' ] )
			       ->bindParam( 16, $post[ 'BMCekNo' ] )
			       ->bindParam( 17, $post[ 'BMCekTempo' ] )
			       ->bindParam( 18, $post[ 'BMPerson' ] )
			       ->bindParam( 19, $post[ 'BMAC' ] )
			       ->bindParam( 20, $post[ 'NoAccountBM' ] )
			       ->bindParam( 21, $post[ 'BMNoGL' ] )
			       ->bindParam( 22, $post[ 'BKNo' ] )
			       ->bindParam( 23, $post[ 'BMNo' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @BTNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'BTNo'       => $post[ 'BTNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$BTNo       = $exe[ '@BTNo' ] ?? $post[ 'BTNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'BTNo'       => $BTNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
	public function callSPJBK( $post ) {
		$post[ 'BKNo' ]   = $post[ 'BKNo' ] ?? '--';
		$post[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		try {
			General::cCmd( "CALL jbk(?,?);" )
			       ->bindParam( 1, $post[ 'BKNo' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->execute();
		} catch ( Exception $e ) {
			return [
				'BKNo'       => $post[ 'BKNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$BKNo       = $post[ 'BKNo' ];
		$keterangan = 'Jurnal ' . $post[ 'BKNo' ] . ' berhasil.';
		return [
			'BKNo'       => $BKNo,
			'status'     => 0,
			'keterangan' => $keterangan
		];
	}
}
