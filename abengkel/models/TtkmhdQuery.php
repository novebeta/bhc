<?php
namespace abengkel\models;
use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttkmhd]].
 *
 * @see Ttkmhd
 */
class TtkmhdQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttkmhd[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttkmhd|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function ttkmhdFillGetData() {
		return $this->select( new Expression( "ttkmhd.KMNo, ttkmhd.KMTgl, ttkmhd.KMJenis, ttkmhd.KasKode, ttkmhd.MotorNoPolisi, ttkmhd.KMPerson, ttkmhd.KMMemo, ttkmhd.KMBayarTunai, 
            ttkmhd.KMNominal, ttkmhd.UserID, ttkmhd.KodeTrans, traccount.NamaAccount, ttkmhd.NoGL, ttkmhd.PosKode, ttkmhd.Cetak" ) )
		            ->join( "LEFT JOIN", "traccount", "ttkmhd.KasKode = traccount.NoAccount" );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'KMNo' ]          = $post[ 'KMNo' ] ?? '--';
		$post[ 'KMNoBaru' ]      = $post[ 'KMNoView' ] ?? '--';
		$post[ 'KMTgl' ]         = date( 'Y-m-d H:i:s', strtotime( $post[ 'KMTgl' ] ?? date( 'Y-m-d H:i:s' ) ) );
		$post[ 'KMJenis' ]       = $post[ 'KMJenis' ] ?? '--';
		$post[ 'KodeTrans' ]     = $post[ 'KodeTrans' ] ?? '--';
		$post[ 'PosKode' ]       = $post[ 'PosKode' ] ?? '--';
		$post[ 'MotorNoPolisi' ] = $post[ 'MotorNoPolisi' ] ?? '--';
		$post[ 'KasKode' ]       = $post[ 'KasKode' ] ?? '--';
		$post[ 'KMPerson' ]      = $post[ 'KMPerson' ] ?? '--';
		$post[ 'KMBayarTunai' ]  = floatval( $post[ 'KMBayarTunai' ] ?? 0 );
		$post[ 'KMNominal' ]     = floatval( $post[ 'KMNominal' ] ?? 0 );
		$post[ 'KMMemo' ]        = $post[ 'KMMemo' ] ?? '--';
		$post[ 'NoGL' ]          = $post[ 'NoGL' ] ?? '--';
		$post[ 'Cetak' ]         = $post[ 'Cetak' ] ?? 'Belum';
		try {
			General::cCmd( "SET @KMNo = ?;" )->bindParam( 1, $post[ 'KMNo' ] )->execute();
			General::cCmd( "CALL fkmhd(?,@Status,@Keterangan,?,?,?,@KMNo,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'PosKode' ] )
			       ->bindParam( 5, $post[ 'KMNoBaru' ] )
			       ->bindParam( 6, $post[ 'KMTgl' ] )
			       ->bindParam( 7, $post[ 'KMJenis' ] )
			       ->bindParam( 8, $post[ 'KodeTrans' ] )
			       ->bindParam( 9, $post[ 'PosKode' ] )
			       ->bindParam( 10, $post[ 'MotorNoPolisi' ] )
			       ->bindParam( 11, $post[ 'KasKode' ] )
			       ->bindParam( 12, $post[ 'KMPerson' ] )
			       ->bindParam( 13, $post[ 'KMBayarTunai' ] )
			       ->bindParam( 14, $post[ 'KMNominal' ] )
			       ->bindParam( 15, $post[ 'KMMemo' ] )
			       ->bindParam( 16, $post[ 'NoGL' ] )
			       ->bindParam( 17, $post[ 'Cetak' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @KMNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'KMNo'       => $post[ 'KMNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$KMNo       = $exe[ '@KMNo' ] ?? $post[ 'KMNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'KMNo'       => $KMNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
	public function callSPJKM( $post ) {
		$post[ 'KMNo' ]   = $post[ 'KMNo' ] ?? '--';
		$post[ 'UserID' ] = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		try {
			General::cCmd( "CALL jkm(?,?);" )
			       ->bindParam( 1, $post[ 'KMNo' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->execute();
		} catch ( Exception $e ) {
			return [
				'KMNo'       => $post[ 'KMNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$KMNo       = $post[ 'KMNo' ];
		$keterangan = 'Jurnal ' . $post[ 'KMNo' ] . ' berhasil.';
		return [
			'KMNo'       => $KMNo,
			'status'     => 0,
			'keterangan' => $keterangan
		];
	}
}
