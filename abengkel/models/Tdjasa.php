<?php

namespace abengkel\models;

/**
 * This is the model class for table "tdjasa".
 *
 * @property string $JasaKode
 * @property string $JasaNama
 * @property string $JasaGroup
 * @property string $JasaHrgBeli
 * @property string $JasaHrgJual
 * @property string $JasaWaktuKerja
 * @property string $JasaWaktuSatuan
 * @property string $JasaWaktuKerjaMenit
 * @property string $JasaStatus
 * @property string $JasaKeterangan
 */
class Tdjasa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdjasa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['JasaKode', 'JasaNama'], 'required'],
            [['JasaHrgBeli', 'JasaHrgJual', 'JasaWaktuKerja', 'JasaWaktuKerjaMenit'], 'number'],
            [['JasaKode', 'JasaGroup'], 'string', 'max' => 10],
            [['JasaNama'], 'string', 'max' => 50],
            [['JasaWaktuSatuan'], 'string', 'max' => 5],
            [['JasaStatus'], 'string', 'max' => 1],
            [['JasaKeterangan'], 'string', 'max' => 255],
            [['JasaKode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'JasaKode' => 'Jasa Kode',
            'JasaNama' => 'Jasa Nama',
            'JasaGroup' => 'Jasa Group',
            'JasaHrgBeli' => 'Jasa Hrg Beli',
            'JasaHrgJual' => 'Jasa Hrg Jual',
            'JasaWaktuKerja' => 'Jasa Waktu Kerja',
            'JasaWaktuSatuan' => 'Jasa Waktu Satuan',
            'JasaWaktuKerjaMenit' => 'Jasa Waktu Kerja Menit',
            'JasaStatus' => 'Jasa Status',
            'JasaKeterangan' => 'Jasa Keterangan',
        ];
    }

	public static  function colGrid() {
		return [
			'JasaKode'         => ['width' => 80,'label' => 'Kode Jasa','name'  => 'JasaKode'],
			'JasaNama'         => ['width' => 400,'label' => 'Nama Jasa','name'  => 'JasaNama'],
			'JasaGroup'        => ['width' => 80,'label' => 'Group Jasa','name'  => 'JasaGroup'],
			'JasaHrgBeli'      => ['width' => 90,'label' => 'Hrg Jasa','name'  => 'JasaHrgBeli', 'formatter' => 'number', 'align' => 'right'],
			'JasaWaktuKerja'   => ['width' => 60,'label' => 'Waktu','name'  => 'JasaWaktuKerja', 'formatter' => 'integer', 'align' => 'right'],
			'JasaWaktuSatuan'  => ['width' => 60,'label' => 'Satuan','name'  => 'JasaWaktuSatuan'],
			'JasaStatus'       => ['width' => 60,'label' => 'Status','name'  => 'JasaStatus'],
		];
	}

    /**
     * {@inheritdoc}
     * @return TdjasaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdjasaQuery(get_called_class());
    }
}
