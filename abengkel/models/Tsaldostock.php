<?php

namespace abengkel\models;

/**
 * This is the model class for table "tsaldostock".
 *
 * @property string $BrgKode
 * @property string $LokasiKode
 * @property string $TglSaldo
 * @property string $Saldo
 */
class Tsaldostock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tsaldostock';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BrgKode', 'LokasiKode', 'TglSaldo'], 'required'],
            [['TglSaldo'], 'safe'],
            [['Saldo'], 'number'],
            [['BrgKode'], 'string', 'max' => 20],
            [['LokasiKode'], 'string', 'max' => 15],
            [['BrgKode', 'LokasiKode', 'TglSaldo'], 'unique', 'targetAttribute' => ['BrgKode', 'LokasiKode', 'TglSaldo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'BrgKode' => 'Brg Kode',
            'LokasiKode' => 'Lokasi Kode',
            'TglSaldo' => 'Tgl Saldo',
            'Saldo' => 'Saldo',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TsaldostockQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TsaldostockQuery(get_called_class());
    }
}
