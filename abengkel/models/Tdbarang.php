<?php

namespace abengkel\models;

/**
 * This is the model class for table "tdbarang".
 *
 * @property string $BrgKode
 * @property string $BrgBarCode
 * @property string $BrgNama
 * @property string $BrgNamaPasar1
 * @property string $BrgNamaPasar2
 * @property string $BrgGroup
 * @property string $BrgSatuan
 * @property string $BrgHrgBeli HPP
 * @property string $BrgHrgJual HET
 * @property string $BrgRak1
 * @property string $BrgRak2
 * @property string $BrgRak3
 * @property string $BrgMinStock
 * @property string $BrgMaxStock
 * @property string $BrgStatus
 * @property string $BrgMemo
 * @property string $TglUpdate
 */
class Tdbarang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdbarang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BrgKode'], 'required'],
            [['BrgHrgBeli', 'BrgHrgJual', 'BrgMinStock', 'BrgMaxStock'], 'number'],
            [['TglUpdate'], 'safe'],
            [['BrgKode'], 'string', 'max' => 20],
            [['BrgBarCode'], 'string', 'max' => 50],
            [['BrgNama'], 'string', 'max' => 75],
            [['BrgNamaPasar1', 'BrgNamaPasar2'], 'string', 'max' => 100],
            [['BrgGroup', 'BrgSatuan', 'BrgRak1', 'BrgRak2', 'BrgRak3'], 'string', 'max' => 15],
            [['BrgStatus'], 'string', 'max' => 1],
            [['BrgMemo'], 'string', 'max' => 150],
            [['BrgKode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'BrgKode'       => 'Brg Kode',
            'BrgBarCode'    => 'Brg Bar Code',
            'BrgNama'       => 'Brg Nama',
            'BrgNamaPasar1' => 'Brg Nama Pasar1',
            'BrgNamaPasar2' => 'Brg Nama Pasar2',
            'BrgGroup'      => 'Brg Group',
            'BrgSatuan'     => 'Brg Satuan',
            'BrgHrgBeli'    => 'Brg Hrg Beli',
            'BrgHrgJual'    => 'Brg Hrg Jual',
            'BrgRak1'       => 'Brg Rak1',
            'BrgRak2'       => 'Brg Rak2',
            'BrgRak3'       => 'Brg Rak3',
            'BrgMinStock'   => 'Brg Min Stock',
            'BrgMaxStock'   => 'Brg Max Stock',
            'BrgStatus'     => 'Brg Status',
            'BrgMemo'       => 'Brg Memo',
            'TglUpdate'     => 'Tgl Update',
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
        return [
            'BrgKode'                => ['width' => 125,'label' => 'Kode Barang','name'  => 'BrgKode'],
            'BrgNama'                => ['width' => 275,'label' => 'Nama Barang','name'  => 'BrgNama'],
            'tdbaranggroup.BrgGroup'               => ['width' => 90,'label' => 'Group','name'  => 'BrgGroup'],
            'BrgSatuan'              => ['width' => 50,'label' => 'Status','name'  => 'BrgSatuan'],
            'BrgHrgBeli'             => ['width' => 90,'label' => 'Hrg Beli','name'  => 'BrgHrgBeli', 'formatter' => 'number', 'align' => 'right'],
            'BrgHrgJual'             => ['width' => 90,'label' => 'Hrg Jual','name'  => 'BrgHrgJual', 'formatter' => 'number', 'align' => 'right'],
            'BrgRak1'                => ['width' => 75,'label' => 'Rak 1','name'  => 'BrgRak1'],
            'BrgMinStock'            => ['width' => 30,'label' => 'Min','name'  => 'BrgMinStock', 'formatter' => 'integer', 'align' => 'right'],
            'BrgMaxStock'            => ['width' => 45,'label' => 'Max','name'  => 'BrgMaxStock', 'formatter' => 'integer', 'align' => 'right'],
            'BrgStatus'              => ['width' => 60,'label' => 'Status','name'  => 'BrgStatus'],
        ];
    }


	public function getBarangGroup() {
		return $this->hasOne( Tdbaranggroup::className(), [ 'BrgGroup' => 'BrgGroup' ] );
	}

    /**
     * {@inheritdoc}
     * @return TdbarangQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdbarangQuery(get_called_class());
    }
}
