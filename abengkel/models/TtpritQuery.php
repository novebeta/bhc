<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttprit]].
 *
 * @see Ttprit
 */
class TtpritQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttprit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttprit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]    = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'PRNo' ]       = $post[ 'PRNo' ] ?? '--';
        $post[ 'PRAuto' ]     = floatval( $post[ 'PRAuto' ] ?? 0 );
        $post[ 'BrgKode' ]    = $post[ 'BrgKode' ] ?? '--';
        $post[ 'PRQty' ]      = floatval( $post[ 'PRQty' ] ?? 0 );
        $post[ 'PRHrgBeli' ]  = floatval( $post[ 'PRHrgBeli' ] ?? 0 );
        $post[ 'PRHrgJual' ]  = floatval( $post[ 'PRHrgJual' ] ?? 0 );
        $post[ 'PRDiscount' ] = floatval( $post[ 'PRDiscount' ] ?? 0 );
        $post[ 'PRPajak' ]    = floatval( $post[ 'PRPajak' ] ?? 0 );
        $post[ 'PRNoOLD' ]    = $post[ 'PRNoOLD' ] ?? '--';
        $post[ 'PRAutoOLD' ]  = floatval( $post[ 'PRAutoOLD' ] ?? 0 );
        $post[ 'BrgKodeOLD' ] = $post[ 'BrgKodeOLD' ] ?? '--';
        try {
            General::cCmd( "CALL fprit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'PRNo' ] )
                ->bindParam( 6, $post[ 'PRAuto' ] )
                ->bindParam( 7, $post[ 'BrgKode' ] )
                ->bindParam( 8, $post[ 'PRQty' ] )
                ->bindParam( 9, $post[ 'PRHrgBeli' ] )
                ->bindParam( 10, $post[ 'PRHrgJual' ] )
                ->bindParam( 11, $post[ 'PRDiscount' ] )
                ->bindParam( 12, $post[ 'PRPajak' ] )
                ->bindParam( 13, $post[ 'PRNoOLD' ] )
                ->bindParam( 14, $post[ 'PRAutoOLD' ] )
                ->bindParam( 15, $post[ 'BrgKodeOLD' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }

}
