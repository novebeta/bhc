<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;

/**
 * This is the ActiveQuery class for [[Ttuchd]].
 *
 * @see Ttuchd
 */
class TtuchdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttuchd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttuchd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function ttuchdFillByNo() {
        return $this->select( new Expression( "UCNo, UCTgl, KodeTrans, PosKode, UCHrgLama, UCHrgBaru, UCSelisih, UCKeterangan, NoGL, Cetak, UserID"));
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'UCNo' ]          = $post[ 'UCNo' ] ?? '--';
        $post[ 'UCNoBaru' ]      = $post[ 'UCNoView' ] ?? '--';
        $post[ 'UCTgl' ]         = date('Y-m-d H:i:s', strtotime($post[ 'UCTgl' ] ?? date( 'Y-m-d H:i:s' )));
        $post[ 'KodeTrans' ]     = $post[ 'KodeTrans' ] ?? 'UC';
        $post[ 'LokasiKode' ]    = $post[ 'LokasiKode' ] ?? '--';
        $post[ 'UCHrgLama' ]     = floatval( $post[ 'UCHrgLama' ] ?? 0 );
        $post[ 'UCHrgBaru' ]     = floatval( $post[ 'UCHrgBaru' ] ?? 0 );
        $post[ 'UCSelisih' ]     = floatval( $post[ 'UCSelisih' ] ?? 0 );
        $post[ 'UCKeterangan' ]  = $post[ 'UCKeterangan' ] ?? '--';
        $post[ 'NoGL' ]          = $post[ 'NoGL' ] ?? '--';
        $post[ 'Cetak' ]         = $post[ 'Cetak' ] ?? 'Belum';
        try {
            General::cCmd( "SET @UCNo = ?;" )->bindParam( 1, $post[ 'UCNo' ] )->execute();
            General::cCmd( "CALL fuchd(?,@Status,@Keterangan,?,?,?,@UCNo,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'UCNoBaru' ] )
                ->bindParam( 6, $post[ 'UCTgl' ] )
                ->bindParam( 7, $post[ 'KodeTrans' ] )
                ->bindParam( 8, $post[ 'LokasiKode' ] )
                ->bindParam( 9, $post[ 'UCHrgLama' ] )
                ->bindParam( 10, $post[ 'UCHrgBaru' ] )
                ->bindParam( 11, $post[ 'UCSelisih' ] )
                ->bindParam( 12, $post[ 'UCKeterangan' ] )
                ->bindParam( 13, $post[ 'NoGL' ] )
                ->bindParam( 14, $post[ 'Cetak' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @UCNo,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'UCNo'       => $post[ 'UCNo' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $UCNo       = $exe[ '@UCNo' ] ?? $post[ 'UCNo' ];
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'UCNo'       => $UCNo,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
