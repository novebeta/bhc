<?php

namespace abengkel\models;

/**
 * This is the model class for table "tvsumbrg".
 *
 * @property string $BrgKode
 * @property string $MySum
 */
class Tvsumbrg extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tvsumbrg';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BrgKode'], 'required'],
            [['MySum'], 'number'],
            [['BrgKode'], 'string', 'max' => 20],
            [['BrgKode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'BrgKode' => 'Brg Kode',
            'MySum' => 'My Sum',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TvsumbrgQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TvsumbrgQuery(get_called_class());
    }
}
