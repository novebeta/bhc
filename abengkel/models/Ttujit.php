<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttujit".
 *
 * @property string $UJNo
 * @property int $UJAuto
 * @property string $JasaKode
 * @property string $UJHrgJualLama
 * @property string $UJHrgJualBaru
 * @property string $UJHrgBeliLama
 * @property string $UJHrgBeliBaru
 */
class Ttujit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttujit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['UJNo', 'UJAuto'], 'required'],
            [['UJAuto'], 'integer'],
            [['UJHrgJualLama', 'UJHrgJualBaru', 'UJHrgBeliLama', 'UJHrgBeliBaru'], 'number'],
            [['UJNo', 'JasaKode'], 'string', 'max' => 10],
            [['UJNo', 'UJAuto'], 'unique', 'targetAttribute' => ['UJNo', 'UJAuto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'UJNo' => 'Uj No',
            'UJAuto' => 'Uj Auto',
            'JasaKode' => 'Jasa Kode',
            'UJHrgJualLama' => 'Uj Hrg Jual Lama',
            'UJHrgJualBaru' => 'Uj Hrg Jual Baru',
            'UJHrgBeliLama' => 'Uj Hrg Beli Lama',
            'UJHrgBeliBaru' => 'Uj Hrg Beli Baru',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtujitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtujitQuery(get_called_class());
    }
}
