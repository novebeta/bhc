<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttsrit]].
 *
 * @see Ttsrit
 */
class TtsritQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttsrit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttsrit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }


	public function callSP( $post ) {
		$post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'SRNo' ]          = $post[ 'SRNo' ] ?? '--';
		$post[ 'SRAuto' ]        = floatval( $post[ 'SRAuto' ] ?? 0 );
		$post[ 'BrgKode' ]       = $post[ 'BrgKode' ] ?? '--';
		$post[ 'SRQty' ]         = floatval( $post[ 'SRQty' ] ?? 0 );
		$post[ 'SRHrgBeli' ]     = floatval( $post[ 'SRHrgBeli' ] ?? 0 );
		$post[ 'SRHrgJual' ]     = floatval( $post[ 'SRHrgJual' ] ?? 0 );
		$post[ 'SRDiscount' ]    = floatval( $post[ 'SRDiscount' ] ?? 0 );
		$post[ 'SRPajak' ]       = floatval( $post[ 'SRPajak' ] ?? 0 );
		$post[ 'SRNoOLD' ]       = $post[ 'SRNoOLD' ] ?? '--';
		$post[ 'SRAutoOLD' ]     = floatval( $post[ 'SRAutoOLD' ] ?? 0 );
		$post[ 'BrgKodeOLD' ]    = $post[ 'BrgKodeOLD' ] ?? '--';
        try {
		General::cCmd( "CALL fsrit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
		       ->bindParam( 1, $post[ 'Action' ] )
		       ->bindParam( 2, $post[ 'UserID' ] )
		       ->bindParam( 3, $post[ 'KodeAkses' ] )
		       ->bindParam( 4, $post[ 'PosKode' ] )
		       ->bindParam( 5, $post[ 'SRNo' ] )
		       ->bindParam( 6, $post[ 'SRAuto' ] )
		       ->bindParam( 7, $post[ 'BrgKode' ] )
		       ->bindParam( 8, $post[ 'SRQty' ] )
		       ->bindParam( 9, $post[ 'SRHrgBeli' ] )
		       ->bindParam( 10, $post[ 'SRHrgJual' ] )
		       ->bindParam( 11, $post[ 'SRDiscount' ] )
		       ->bindParam( 12, $post[ 'SRPajak' ] )
		       ->bindParam( 13, $post[ 'SRNoOLD' ] )
		       ->bindParam( 14, $post[ 'SRAutoOLD' ] )
		       ->bindParam( 15, $post[ 'BrgKodeOLD' ] )
		       ->execute();
		$exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
	}
}
