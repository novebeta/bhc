<?php

namespace abengkel\models;

/**
 * This is the ActiveQuery class for [[Tdjasa]].
 *
 * @see Tdjasa
 */
class TdjasaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdjasa[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdjasa|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	public function combo() {
		return $this->select( [ "CONCAT(JasaKode,' - ',JasaNama) as label", "JasaKode as value" ] )
		            ->orderBy( 'JasaStatus, JasaKode' )
		            ->asArray()
		            ->all();
	}

    public function comboSelect2( $namaAccount = false ) {
        return $this->select( [ 'JasaKode AS id', 'JasaNama AS text', 'tdjasa.*' ] )
            ->orderBy( 'JasaKode' )
            ->asArray()
            ->all();
    }
}
