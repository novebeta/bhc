<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttsoit".
 *
 * @property string $SONo
 * @property int $SOAuto
 * @property string $BrgKode
 * @property string $SOQty
 * @property string $SOHrgBeli
 * @property string $SOHrgJual
 * @property string $SODiscount
 * @property string $SOPajak
 * @property string $SINo
 * @property string $PONo
 * @property string $SIQtyS
 * @property string $POQtyS
 */
class Ttsoit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttsoit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SONo', 'SOAuto'], 'required'],
            [['SOAuto'], 'integer'],
            [['SOQty', 'SOHrgBeli', 'SOHrgJual', 'SODiscount', 'SOPajak', 'SIQtyS', 'POQtyS'], 'number'],
            [['SONo', 'SINo', 'PONo'], 'string', 'max' => 10],
            [['BrgKode'], 'string', 'max' => 20],
            [['SONo', 'SOAuto'], 'unique', 'targetAttribute' => ['SONo', 'SOAuto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SONo' => 'So No',
            'SOAuto' => 'So Auto',
            'BrgKode' => 'Brg Kode',
            'SOQty' => 'So Qty',
            'SOHrgBeli' => 'So Hrg Beli',
            'SOHrgJual' => 'So Hrg Jual',
            'SODiscount' => 'So Discount',
            'SOPajak' => 'So Pajak',
            'SINo' => 'Si No',
            'PONo' => 'Po No',
            'SIQtyS' => 'Si Qty S',
            'POQtyS' => 'Po Qty S',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtsoitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtsoitQuery(get_called_class());
    }
}
