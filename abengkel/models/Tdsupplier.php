<?php

namespace abengkel\models;

/**
 * This is the model class for table "tdsupplier".
 *
 * @property string $SupKode
 * @property string $SupNama
 * @property string $SupContact
 * @property string $SupAlamat
 * @property string $SupKota
 * @property string $SupTelepon
 * @property string $SupFax
 * @property string $SupEmail
 * @property string $SupKeterangan
 * @property string $SupStatus
 */
class Tdsupplier extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdsupplier';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SupKode'], 'required'],
            [['SupKode'], 'string', 'max' => 15],
            [['SupNama', 'SupContact', 'SupFax'], 'string', 'max' => 50],
            [['SupAlamat', 'SupTelepon'], 'string', 'max' => 75],
            [['SupKota', 'SupEmail'], 'string', 'max' => 25],
            [['SupKeterangan'], 'string', 'max' => 100],
            [['SupStatus'], 'string', 'max' => 1],
            [['SupKode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SupKode'       => 'Sup Kode',
            'SupNama'       => 'Sup Nama',
            'SupContact'    => 'Sup Contact',
            'SupAlamat'     => 'Sup Alamat',
            'SupKota'       => 'Sup Kota',
            'SupTelepon'    => 'Sup Telepon',
            'SupFax'        => 'Sup Fax',
            'SupEmail'      => 'Sup Email',
            'SupKeterangan' => 'Sup Keterangan',
            'SupStatus'     => 'Sup Status',
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
        return [
            'SupKode'       => ['width' => 90,'label' => 'Kode','name'  => 'SupKode'],
            'SupNama'       => ['width' => 230,'label' => 'Nama','name'  => 'SupNama'],
            'SupContact'    => ['width' => 150,'label' => 'Contact','name'  => 'SupContact'],
            'SupKota'       => ['width' => 110,'label' => 'Kota','name'  => 'SupKota'],
            'SupAlamat'     => ['width' => 200,'label' => 'Alamat','name'  => 'SupAlamat'],
            'SupTelepon'    => ['width' => 95,'label' => 'Telepon','name'  => 'SupTelepon'],
            'SupStatus'     => ['width' => 55,'label' => 'Status','name'  => 'SupStatus'],
        ];
    }

    /**
     * {@inheritdoc}
     * @return TdsupplierQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdsupplierQuery(get_called_class());
    }
}
