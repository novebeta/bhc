<?php
namespace abengkel\models;
use abengkel\components\Menu;
/**
 * This is the model class for table "ttkkhd".
 *
 * @property string $KKNo
 * @property string $KKTgl
 * @property string $KKJenis
 * @property string $KodeTrans
 * @property string $PosKode
 * @property string $SupKode No KM H1
 * @property string $KasKode
 * @property string $KKPerson
 * @property string $KKNominal
 * @property string $KKMemo
 * @property string $NoGL
 * @property string $Cetak
 * @property string $UserID
 */
class Ttkkhd extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttkkhd';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'KKNo' ], 'required' ],
			[ [ 'KKTgl' ], 'safe' ],
			[ [ 'KKNominal' ], 'number' ],
			[ [ 'KKNo' ], 'string', 'max' => 12 ],
			[ [ 'KKJenis', 'PosKode' ], 'string', 'max' => 20 ],
			[ [ 'KodeTrans' ], 'string', 'max' => 4 ],
			[ [ 'SupKode', 'UserID' ], 'string', 'max' => 15 ],
			[ [ 'KasKode' ], 'string', 'max' => 25 ],
			[ [ 'KKPerson' ], 'string', 'max' => 50 ],
			[ [ 'KKMemo' ], 'string', 'max' => 150 ],
			[ [ 'NoGL' ], 'string', 'max' => 10 ],
			[ [ 'Cetak' ], 'string', 'max' => 5 ],
			[ [ 'KKNo' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'KKNo'      => 'Kk No',
			'KKTgl'     => 'Kk Tgl',
			'KKJenis'   => 'Kk Jenis',
			'KodeTrans' => 'Kode Trans',
			'PosKode'   => 'Pos Kode',
			'SupKode'   => 'Sup Kode',
			'KasKode'   => 'Kas Kode',
			'KKPerson'  => 'Kk Person',
			'KKNominal' => 'Kk Nominal',
			'KKMemo'    => 'Kk Memo',
			'NoGL'      => 'No Gl',
			'Cetak'     => 'Cetak',
			'UserID'    => 'User ID',
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public static function colGrid() {
		return [
			'ttkkhd.KKNo'      => [ 'width' => 95, 'label' => 'No KK', 'name' => 'KKNo' ],
			'KKTgl'            => [ 'width' => 120, 'label' => 'Tgl KK', 'name' => 'KKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y  H:i:s' ] ],
			'KodeTrans'        => [ 'width' => 70, 'label' => 'Kode Trans', 'name' => 'KodeTrans' ],
			'KasKode'          => [ 'width' => 70, 'label' => 'Kas', 'name' => 'KasKode' ],
			'NamaAccount'      => [ 'width' => 120, 'label' => 'Nama Kas', 'name' => 'NamaAccount' ],
			'SupKode'          => [ 'width' => 70, 'label' => 'Supplier', 'name' => 'SupKode' ],
			'KKPerson'         => [ 'width' => 125, 'label' => 'Terima Dari', 'name' => 'KKPerson' ],
			'KKNominal'        => [ 'width' => 100, 'label' => 'Nominal', 'name' => 'KKNominal', 'formatter' => 'number', 'align' => 'right' ],
			'KKJenis'          => [ 'width' => 60, 'label' => 'Jenis', 'name' => 'KKJenis' ],
			'PosKode'          => [ 'width' => 105, 'label' => 'Pos', 'name' => 'PosKode' ],
			'NoGL'             => [ 'width' => 80, 'label' => 'No JT', 'name' => 'NoGL' ],
			'Cetak'            => [ 'width' => 50, 'label' => 'Cetak', 'name' => 'Cetak' ],
			'UserID'           => [ 'width' => 105, 'label' => 'UserID', 'name' => 'UserID' ],
			'KKMemo'           => [ 'width' => 500, 'label' => 'Keterangan', 'name' => 'KKMemo' ],
		];
	}
	public static function colGridTransferKas() {
		return [
			'KKLink'        => [ 'width' => 70, 'label' => 'No KT', 'name' => 'KKLink' ],
			'KKTgl'         => [ 'width' => 115, 'label' => 'Tgl KK', 'name' => 'KKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y H:i:s' ] ],
			'NoAccountKK'   => [ 'label' => 'COA KK', 'width' => 60, 'name' => 'NoAccountKK' ],
			'NamaAccountKK' => [ 'label' => 'Kas Keluar', 'width' => 120, 'name' => 'NamaAccountKK' ],
			'NoAccountKM'   => [ 'label' => 'COA KM', 'width' => 60, 'name' => 'NoAccountKM' ],
			'NamaAccountKM' => [ 'label' => 'Kas Masuk', 'width' => 120, 'name' => 'NamaAccountKM' ],
			'KKNominal'     => [ 'width' => 100, 'label' => 'Nominal', 'name' => 'KKNominal', 'formatter' => 'number', 'align' => 'right' ],
			'KKMemo'        => [ 'width' => 300, 'label' => 'Keterangan', 'name' => 'KKMemo' ],
			'KKPerson'      => [ 'width' => 140, 'label' => 'Pengirim', 'name' => 'KKPerson' ],
			'KMPerson'      => [ 'width' => 140, 'label' => 'Penerima', 'name' => 'KMPerson' ],
			'KKNo'          => [ 'width' => 80, 'label' => 'No KK', 'name' => 'KKNo' ],
			'KMNo'          => [ 'width' => 80, 'label' => 'No KM', 'name' => 'KMNo' ],
			'NoGLKK'        => [ 'width' => 70, 'label' => 'No JT KK', 'name' => 'NoGLKK' ],
			'NoGLKM'        => [ 'width' => 70, 'label' => 'No JT KM', 'name' => 'NoGLKM' ],
			'UserID'        => [ 'width' => 80, 'label' => 'UserID', 'name' => 'UserID' ],
		];
	}
	public static function colGridPick() {
		return [
			'No'        => [ 'width' => 95, 'label' => 'No', 'name' => 'NO' ],
			'Tgl'       => [ 'width' => 120, 'label' => 'Tgl', 'name' => 'Tgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y  H:i:s' ] ],
			'Kode'      => [ 'width' => 70, 'label' => 'Kode', 'name' => 'Kode' ],
			'Nama'      => [ 'width' => 70, 'label' => 'Nama', 'name' => 'Nama' ],
			'MotorNama' => [ 'width' => 140, 'label' => 'MotorNama', 'name' => 'MotorNama' ],
			'Total'     => [ 'width' => 100, 'label' => 'Total', 'name' => 'Total', 'formatter' => 'number', 'align' => 'right' ],
			'Terbayar'  => [ 'width' => 100, 'label' => 'Terbayar', 'name' => 'Terbayar', 'formatter' => 'number', 'align' => 'right' ],
			'Sisa'      => [ 'width' => 100, 'label' => 'Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
		];
	}
	public static function colGridPickPI() {
		return [
			'No'        => [ 'width' => 95, 'label' => 'No Beli', 'name' => 'NO' ],
			'Tgl'       => [ 'width' => 120, 'label' => 'Tgl Beli', 'name' => 'Tgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y  H:i:s' ] ],
			'Kode'      => [ 'width' => 70, 'label' => 'Supplier', 'name' => 'Kode' ],
			'Nama'      => [ 'width' => 130, 'label' => 'Nama Supplier', 'name' => 'Nama' ],
			'MotorNama' => [ 'width' => 160, 'label' => 'Lokasi', 'name' => 'MotorNama' ],
			'Total'     => [ 'width' => 80, 'label' => 'Total', 'name' => 'Total', 'formatter' => 'number', 'align' => 'right' ],
			'Terbayar'  => [ 'width' => 80, 'label' => 'Terbayar', 'name' => 'Terbayar', 'formatter' => 'number', 'align' => 'right' ],
			'Sisa'      => [ 'width' => 80, 'label' => 'Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
		];
	}
	public static function colGridPickSR() {
		return [
			'No'        => [ 'width' => 95, 'label' => 'No Retur Beli', 'name' => 'NO' ],
			'Tgl'       => [ 'width' => 120, 'label' => 'Tgl Retur Beli', 'name' => 'Tgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y  H:i:s' ] ],
			'Kode'      => [ 'width' => 70, 'label' => 'No Polisi', 'name' => 'Kode' ],
			'Nama'      => [ 'width' => 130, 'label' => 'Nama Konsumen', 'name' => 'Nama' ],
			'MotorNama' => [ 'width' => 160, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
			'Total'     => [ 'width' => 80, 'label' => 'Total', 'name' => 'Total', 'formatter' => 'number', 'align' => 'right' ],
			'Terbayar'  => [ 'width' => 80, 'label' => 'Terbayar', 'name' => 'Terbayar', 'formatter' => 'number', 'align' => 'right' ],
			'Sisa'      => [ 'width' => 80, 'label' => 'Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
		];
	}
	public static function colGridSelectKkKas() {
		return [
			'No'          => [ 'width' => 95, 'label' => 'No KK', 'name' => 'No' ],
			'Tgl'         => [ 'width' => 120, 'label' => 'Tgl KK', 'name' => 'Tgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y  H:i:s' ] ],
			'KasKode'     => [ 'width' => 70, 'label' => 'Bank', 'name' => 'KasKode' ],
			'NamaAccount' => [ 'width' => 130, 'label' => 'Nama Bank', 'name' => 'NamaAccount' ],
			'KKNominal'   => [ 'width' => 80, 'label' => 'Nominal', 'name' => 'KKNominal', 'formatter' => 'number', 'align' => 'right' ],
			'KKPerson'    => [ 'width' => 160, 'label' => 'Terima Dari', 'name' => 'KKPerson' ],
			'KKMemo'      => [ 'width' => 80, 'label' => 'Keterangan', 'name' => 'KKMemo' ],
			'KKJenis'     => [ 'width' => 80, 'label' => 'Jenis', 'name' => 'KKJenis' ],
			'UserID'      => [ 'width' => 80, 'label' => 'UserID', 'name' => 'UserID' ],
		];
	}
	public static function colGridSelectKkBmDariH1() {
		return [
			'KKNo'      => [ 'width' => 95, 'label' => 'No KK', 'name' => 'KKNo' ],
			'KKTgl'     => [ 'width' => 120, 'label' => 'Tgl KK', 'name' => 'KKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y H:i:s' ] ],
			'KKLink'    => [ 'width' => 70, 'label' => 'KKLink', 'name' => 'KKLink' ],
			'KKJenis'   => [ 'width' => 80, 'label' => 'Jenis', 'name' => 'KKJenis' ],
			'KKNominal' => [ 'width' => 80, 'label' => 'Nominal', 'name' => 'KKNominal', 'formatter' => 'number', 'align' => 'right' ],
			'KKPerson'  => [ 'width' => 160, 'label' => 'Terima Dari', 'name' => 'KKPerson' ],
			'KKMemo'    => [ 'width' => 80, 'label' => 'Keterangan', 'name' => 'KKMemo' ],
			'UserID'    => [ 'width' => 80, 'label' => 'UserID', 'name' => 'UserID' ],
		];
	}
	public function getAccount() {
		return $this->hasOne( Traccount::className(), [ 'NoAccount' => 'KasKode' ] );
	}
	public function getKasKeluarItem() {
		return $this->hasMany( Ttkkit::className(), [ 'KKNo' => 'KKNo' ] );
	}
	public function getKasMasukItem() {
		return $this->hasMany( Ttkmit::className(), [ 'KMLink' => 'KKLInk' ] )
		            ->via( 'kasKeluarItem' );
	}
	public function getKasMasukHeader() {
		return $this->hasOne( Ttkmhd::className(), [ 'KMNo' => 'KMNo' ] )
		            ->via( 'kasMasukItem' );
	}
	public static function generateKKNo( $temporary = false ) {
		$sign    = $temporary ? '=' : '-';
		$kode    = 'KK';
		$posNo   = Menu::getUserLokasi()[ 'PosNomor' ];
		$yNow    = date( "y" );
		$maxNoGl = ( new \yii\db\Query() )
			->from( self::tableName() )
			->where( "KKNo LIKE '$kode$posNo$yNow$sign%'" )
			->max( 'KKNo' );
		if ( $maxNoGl && preg_match( "/$kode\d{4}$sign(\d+)/", $maxNoGl, $result ) ) {
			[ $all, $counter ] = $result;
			$digitCount  = strlen( $counter );
			$val         = (int) $counter;
			$nextCounter = sprintf( '%0' . $digitCount . 'd', ++ $val );
		} else {
			$nextCounter = "00001";
		}
		//        return $temporary ? str_replace('-', '=', $nextRef) : $nextRef;
		return "$kode$posNo$yNow$sign$nextCounter";
	}
	public static function getRoute( $KodeTrans, $param = [] ) {
		$index  = [];
		$update = [];
		$create = [];
		switch ( $KodeTrans ) {
			case 'UM' :
				$index  = [ 'ttkkhd/kas-keluar-umum' ];
				$update = [ 'ttkkhd/kas-keluar-umum-update' ];
				$create = [ 'ttkkhd/kas-keluar-umum-create' ];
				break;
			case 'BM' :
				$index  = [ 'ttkkhd/kas-keluar-ke-bank' ];
				$update = [ 'ttkkhd/kas-keluar-ke-bank-update' ];
				$create = [ 'ttkkhd/kas-keluar-ke-bank-create' ];
				break;
			case 'H1' :
				$index  = [ 'ttkkhd/kas-keluar-ke-h1' ];
				$update = [ 'ttkkhd/kas-keluar-ke-h1-update' ];
				$create = [ 'ttkkhd/kas-keluar-ke-h1-create' ];
				break;
			case 'PI' :
				$index  = [ 'ttkkhd/kas-keluar-pembelian' ];
				$update = [ 'ttkkhd/kas-keluar-pembelian-update' ];
				$create = [ 'ttkkhd/kas-keluar-pembelian-create' ];
				break;
			case 'PL' :
				$index  = [ 'ttkkhd/kas-keluar-order-pengerjaan-luar' ];
				$update = [ 'ttkkhd/kas-keluar-order-pengerjaan-luar-update' ];
				$create = [ 'ttkkhd/kas-keluar-order-pengerjaan-luar-create' ];
				break;
			case 'SR' :
				$index  = [ 'ttkkhd/kas-keluar-retur-penjualan' ];
				$update = [ 'ttkkhd/kas-keluar-retur-penjualan-update' ];
				$create = [ 'ttkkhd/kas-keluar-retur-penjualan-create' ];
				break;
			case 'Kas Transfer' :
				$index  = [ 'ttkkhd/transfer-antar-kas' ];
				$update = [ 'ttkkhd/transfer-antar-kas-update' ];
				$create = [ 'ttkkhd/transfer-antar-kas-create' ];
				break;
		}
		return [
			'index'  => \yii\helpers\Url::toRoute( array_merge( $index, $param ) ),
			'update' => \yii\helpers\Url::toRoute( array_merge( $update, $param ) ),
			'create' => \yii\helpers\Url::toRoute( array_merge( $create, $param ) ),
		];
	}
	/**
	 * {@inheritdoc}
	 * @return TtkkhdQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TtkkhdQuery( get_called_class() );
	}

    public static function colGridKasKeluarUmum() {
        return [
            'KKNo'       => [ 'width' => 95, 'label' => 'No KK', 'name' => 'KKNo' ],
            'KKTgl'      => [ 'width' => 80, 'label' => 'Tgl KK', 'name' => 'KKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
            'KKNominal'  => [ 'width' => 90, 'label' => 'Nominal', 'name' => 'KKNominal', 'formatter' => 'number', 'align' => 'right' ],
            'KKMemo'     => [ 'width' => 280, 'label' => 'Keterangan', 'name' => 'KKMemo' ],
            'KKPerson'   => [ 'width' => 175, 'label' => 'Penerima', 'name' => 'KKPerson' ],
            'KKJam'      => [ 'width' => 50, 'label' => 'Jam', 'name' => 'KKJam', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'H:i:s' ] ],
            'KKJenis'    => [ 'width' => 80, 'label' => 'Jenis', 'name' => 'KKJenis' ],
            'KKLink'     => [ 'width' => 90, 'label' => 'No Transaksi', 'name' => 'KKLink' ],
            'NoGL'       => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
            'UserID'     => [ 'width' => 70, 'label' => 'User ID', 'name' => 'UserID' ],
            'LokasiKode' => [ 'width' => 100, 'label' => 'LokasiKode', 'name' => 'LokasiKode' ],
        ];
    }
}
