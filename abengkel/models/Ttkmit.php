<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttkmit".
 *
 * @property string $KMNo
 * @property string $KMLink
 * @property string $KMBayar
 */
class Ttkmit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttkmit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KMNo', 'KMLink'], 'required'],
            [['KMBayar'], 'number'],
            [['KMNo'], 'string', 'max' => 12],
            [['KMLink'], 'string', 'max' => 18],
            [['KMNo', 'KMLink'], 'unique', 'targetAttribute' => ['KMNo', 'KMLink']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'KMNo' => 'Km No',
            'KMLink' => 'Km Link',
            'KMBayar' => 'Km Bayar',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtkmitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtkmitQuery(get_called_class());
    }
}
