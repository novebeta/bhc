<?php

namespace abengkel\models;

/**
 * This is the model class for table "tsaldobank".
 *
 * @property string $SaldoBankTgl
 * @property string $BankKode
 * @property string $SaldoBankAwal
 * @property string $SaldoBankMasuk
 * @property string $SaldoBankKeluar
 * @property string $SaldoBankSaldo
 */
class Tsaldobank extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tsaldobank';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SaldoBankTgl', 'BankKode'], 'required'],
            [['SaldoBankTgl'], 'safe'],
            [['SaldoBankAwal', 'SaldoBankMasuk', 'SaldoBankKeluar', 'SaldoBankSaldo'], 'number'],
            [['BankKode'], 'string', 'max' => 10],
            [['SaldoBankTgl', 'BankKode'], 'unique', 'targetAttribute' => ['SaldoBankTgl', 'BankKode']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SaldoBankTgl' => 'Saldo Bank Tgl',
            'BankKode' => 'Bank Kode',
            'SaldoBankAwal' => 'Saldo Bank Awal',
            'SaldoBankMasuk' => 'Saldo Bank Masuk',
            'SaldoBankKeluar' => 'Saldo Bank Keluar',
            'SaldoBankSaldo' => 'Saldo Bank Saldo',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TsaldobankQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TsaldobankQuery(get_called_class());
    }
}
