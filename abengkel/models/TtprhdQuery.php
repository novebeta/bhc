<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttprhd]].
 *
 * @see Ttprhd
 */
class TtprhdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttprhd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttprhd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function ttprhdFillByNo() {
        return $this->select( new Expression( "
                                ttprhd.PRNo,
                                ttprhd.PRTgl,
                                ttprhd.SupKode,
                                ttprhd.LokasiKode,
                                ttprhd.PRKeterangan,
                                ttprhd.PRSubTotal,
                                ttprhd.PRDiscFinal,
                                ttprhd.PRTotalPajak,
                                ttprhd.PRBiayaKirim,
                                ttprhd.PRTotal,
                                ttprhd.PRTerbayar,
                                ttprhd.UserID,
                                tdsupplier.SupNama,
                                ttprhd.NoGL,
                                ttprhd.KodeTrans,
                                ttprhd.PINo,
                                ttprhd.PosKode,
                                ttprhd.Cetak" ) )
            ->join( "LEFT OUTER JOIN", "tdsupplier", "ttprhd.SupKode = tdsupplier.SupKode" )
            ->groupBy("ttprhd.PRNo");
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'PRNo' ]          = $post[ 'PRNo' ] ?? '--';
        $post[ 'PRNoBaru' ]      = $post[ 'PRNoView' ] ?? '--';
        $post[ 'PRTgl' ]         = date('Y-m-d H:i:s', strtotime($post[ 'PRTgl' ] ?? date( 'Y-m-d H:i:s' )));
        $post[ 'PINo' ]          = $post[ 'PINo' ] ?? '--';
        $post[ 'KodeTrans' ]     = $post[ 'KodeTrans' ] ?? '--';
        $post[ 'LokasiKode' ]    = $post[ 'LokasiKode' ] ?? '--';
        $post[ 'SupKode' ]       = $post[ 'SupKode' ] ?? '--';
        $post[ 'PRSubTotal' ]    = floatval( $post[ 'PRSubTotal' ] ?? 0 );
        $post[ 'PRDiscFinal' ]   = floatval( $post[ 'PRDiscFinal' ] ?? 0 );
        $post[ 'PRTotalPajak' ]  = floatval( $post[ 'PRTotalPajak' ] ?? 0 );
        $post[ 'PRBiayaKirim' ]  = floatval( $post[ 'PRBiayaKirim' ] ?? 0 );
        $post[ 'PRTotal' ]       = floatval( $post[ 'PRTotal' ] ?? 0 );
        $post[ 'PRTerbayar' ]    = floatval( $post[ 'PRTerbayar' ] ?? 0 );
        $post[ 'PRKeterangan' ]  = $post[ 'PRKeterangan' ] ?? '--';
        $post[ 'NoGL' ]          = $post[ 'NoGL' ] ?? '--';
        $post[ 'Cetak' ]         = $post[ 'Cetak' ] ?? 'Belum';
        try {
            General::cCmd( "SET @PRNo = ?;" )->bindParam( 1, $post[ 'PRNo' ] )->execute();
            General::cCmd( "CALL fprhd(?,@Status,@Keterangan,?,?,?,@PRNo,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'PRNoBaru' ] )
                ->bindParam( 6, $post[ 'PRTgl' ] )
                ->bindParam( 7, $post[ 'PINo' ] )
                ->bindParam( 8, $post[ 'KodeTrans' ] )
                ->bindParam( 9, $post[ 'LokasiKode' ] )
                ->bindParam( 10, $post[ 'SupKode' ] )
                ->bindParam( 11, $post[ 'PRSubTotal' ] )
                ->bindParam( 12, $post[ 'PRDiscFinal' ] )
                ->bindParam( 13, $post[ 'PRTotalPajak' ] )
                ->bindParam( 14, $post[ 'PRBiayaKirim' ] )
                ->bindParam( 15, $post[ 'PRTotal' ] )
                ->bindParam( 16, $post[ 'PRTerbayar' ] )
                ->bindParam( 17, $post[ 'PRKeterangan' ] )
                ->bindParam( 18, $post[ 'Cetak' ] )
                ->bindParam( 19, $post[ 'NoGL' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @PRNo,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'PRNo'       => $post[ 'PRNo' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $PRNo       = $exe[ '@PRNo' ] ?? $post[ 'PRNo' ];
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'PRNo'       => $PRNo,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
