<?php

namespace abengkel\models;

/**
 * This is the model class for table "tttahd".
 *
 * @property string $TANo
 * @property string $TATgl
 * @property string $KodeTrans
 * @property string $PosKode
 * @property string $LokasiKode
 * @property string $TATotal
 * @property string $TAKeterangan
 * @property string $NoGL
 * @property string $Cetak
 * @property string $UserID
 */
class Tttahd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tttahd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['TANo'], 'required'],
            [['TATgl'], 'safe'],
            [['TATotal'], 'number'],
            [['TANo', 'PosKode', 'NoGL'], 'string', 'max' => 20],
            [['KodeTrans'], 'string', 'max' => 4],
            [['LokasiKode', 'UserID'], 'string', 'max' => 15],
            [['TAKeterangan'], 'string', 'max' => 150],
            [['Cetak'], 'string', 'max' => 5],
            [['TANo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'TANo'         => 'Ta No',
            'TATgl'        => 'Ta Tgl',
            'KodeTrans'    => 'Kode Trans',
            'PosKode'      => 'Pos Kode',
            'LokasiKode'   => 'Lokasi Kode',
            'TATotal'      => 'Ta Total',
            'TAKeterangan' => 'Ta Keterangan',
            'NoGL'         => 'No Gl',
            'Cetak'        => 'Cetak',
            'UserID'       => 'User ID',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
        return [
            'TANo'         =>  ['width' => 85,'label' => 'No TA','name'  => 'TANo'],
            'TATgl'        =>  ['width' => 115,'label' => 'Tgl TA','name'  => 'TATgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y H:i:s']],
            'KodeTrans'    => ['width' => 50,'label' => 'TC','name'  => 'KodeTrans'],
            'TATotal'      =>  ['width' => 100,'label' => 'Total','name'  => 'TATotal','formatter' => 'number', 'align' => 'right'],
            'TAKeterangan' => ['width' => 380,'label' => 'Keterangan','name'  => 'TAKeterangan'],
            'NoGL'         => ['width' => 70,'label' => 'No JT','name'  => 'NoGL'],
            'Cetak'        => ['width' => 50,'label' => 'Cetak','name'  => 'Cetak'],
            'UserID'       => ['width' => 80,'label' => 'UserID','name'  => 'UserID'],
        ];
    }

    /**
     * {@inheritdoc}
     * @return TttahdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TttahdQuery(get_called_class());
    }
}
