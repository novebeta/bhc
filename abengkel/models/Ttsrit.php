<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttsrit".
 *
 * @property string $SRNo
 * @property int $SRAuto
 * @property string $BrgKode
 * @property string $SRQty
 * @property string $SRHrgBeli
 * @property string $SRHrgJual
 * @property string $SRDiscount
 * @property string $SRPajak
 * @property string $LokasiKode
 */
class Ttsrit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttsrit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SRNo', 'SRAuto'], 'required'],
            [['SRAuto'], 'integer'],
            [['SRQty', 'SRHrgBeli', 'SRHrgJual', 'SRDiscount', 'SRPajak'], 'number'],
            [['SRNo'], 'string', 'max' => 10],
            [['BrgKode'], 'string', 'max' => 20],
            [['LokasiKode'], 'string', 'max' => 15],
            [['SRNo', 'SRAuto'], 'unique', 'targetAttribute' => ['SRNo', 'SRAuto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SRNo' => 'Sr No',
            'SRAuto' => 'Sr Auto',
            'BrgKode' => 'Brg Kode',
            'SRQty' => 'Sr Qty',
            'SRHrgBeli' => 'Sr Hrg Beli',
            'SRHrgJual' => 'Sr Hrg Jual',
            'SRDiscount' => 'Sr Discount',
            'SRPajak' => 'Sr Pajak',
            'LokasiKode' => 'Lokasi Kode',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtsritQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtsritQuery(get_called_class());
    }
}
