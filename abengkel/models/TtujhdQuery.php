<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;

/**
 * This is the ActiveQuery class for [[Ttujhd]].
 *
 * @see Ttujhd
 */
class TtujhdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttujhd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttujhd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function ttujhdFillByNo() {
        return $this->select( new Expression( "UJNo, UJTgl, UJKeterangan, KodeTrans, PosKode, Cetak, UserID"));
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'UJNo' ]          = $post[ 'UJNo' ] ?? '--';
        $post[ 'UJNoBaru' ]      = $post[ 'UJNoView' ] ?? '--';
        $post[ 'UJTgl' ]         = date('Y-m-d H:i:s', strtotime($post[ 'UJTgl' ] ?? date( 'Y-m-d H:i:s' )));
        $post[ 'UJKeterangan' ]  = $post[ 'UJKeterangan' ] ?? '--';
        $post[ 'KodeTrans' ]     = $post[ 'KodeTrans' ] ?? 'UJ';
        $post[ 'LokasiKode' ]    = $post[ 'LokasiKode' ] ?? '--';
        $post[ 'NoGL' ]          = $post[ 'NoGL' ] ?? '--';
        $post[ 'Cetak' ]         = $post[ 'Cetak' ] ?? 'Belum';
        try {
            General::cCmd( "SET @UJNo = ?;" )->bindParam( 1, $post[ 'UJNo' ] )->execute();
            General::cCmd( "CALL fujhd(?,@Status,@Keterangan,?,?,?,@UJNo,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'UJNoBaru' ] )
                ->bindParam( 6, $post[ 'UJTgl' ] )
                ->bindParam( 7, $post[ 'UJKeterangan' ] )
                ->bindParam( 8, $post[ 'KodeTrans' ] )
                ->bindParam( 9, $post[ 'LokasiKode' ] )
                ->bindParam( 10, $post[ 'NoGL' ] )
                ->bindParam( 11, $post[ 'Cetak' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @UJNo,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'UJNo'       => $post[ 'UJNo' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $UJNo       = $exe[ '@UJNo' ] ?? $post[ 'UJNo' ];
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'UJNo'       => $UJNo,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
