<?php

namespace abengkel\models;

/**
 * This is the ActiveQuery class for [[Tdbarang]].
 *
 * @see Tdbarang
 */
class TdbarangQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdbarang[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdbarang|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	public function select2( $value, $label = [ 'BrgNama' ], $orderBy = [], $where = [ 'condition' => null, 'params' => [] ], $allOption = false ) {
    	$option =
			$this->select( [ "CONCAT(" . implode( ",'||',", $label ) . ") as label", "$value as value" ] )
			     ->orderBy( $orderBy )
			     ->where( $where[ 'condition' ], $where[ 'params' ] )
			     ->asArray()
			     ->all();
		if ( $allOption ) {
			return array_merge( [ [ 'value' => '%', 'label' => 'Semua' ] ], $option );
		} else {
			return $option;
		}
	}
}
