<?php

namespace abengkel\models;

/**
 * This is the ActiveQuery class for [[Ttglhpit]].
 *
 * @see Ttglhpit
 */
class TtglhpitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttglhpit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttglhpit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
