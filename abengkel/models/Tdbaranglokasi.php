<?php

namespace abengkel\models;

/**
 * This is the model class for table "tdbaranglokasi".
 *
 * @property string $BrgKode
 * @property string $LokasiKode
 * @property string $SDQty
 * @property string $SIQty
 * @property string $SRQty
 * @property string $PSQty
 * @property string $PRQty
 * @property string $TAQty
 * @property string $TIAddQty
 * @property string $TIMinQty
 * @property string $SaldoQty
 * @property string $PLSDQty
 * @property string $PLSIQty
 * @property string $SaldoQtyReal
 */
class Tdbaranglokasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdbaranglokasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BrgKode', 'LokasiKode'], 'required'],
            [['SDQty', 'SIQty', 'SRQty', 'PSQty', 'PRQty', 'TAQty', 'TIAddQty', 'TIMinQty', 'SaldoQty', 'PLSDQty', 'PLSIQty', 'SaldoQtyReal'], 'number'],
            [['BrgKode'], 'string', 'max' => 20],
            [['LokasiKode'], 'string', 'max' => 15],
            [['BrgKode', 'LokasiKode'], 'unique', 'targetAttribute' => ['BrgKode', 'LokasiKode']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'BrgKode' => 'Brg Kode',
            'LokasiKode' => 'Lokasi Kode',
            'SDQty' => 'Sd Qty',
            'SIQty' => 'Si Qty',
            'SRQty' => 'Sr Qty',
            'PSQty' => 'Ps Qty',
            'PRQty' => 'Pr Qty',
            'TAQty' => 'Ta Qty',
            'TIAddQty' => 'Ti Add Qty',
            'TIMinQty' => 'Ti Min Qty',
            'SaldoQty' => 'Saldo Qty',
            'PLSDQty' => 'Plsd Qty',
            'PLSIQty' => 'Plsi Qty',
            'SaldoQtyReal' => 'Saldo Qty Real',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TdbaranglokasiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdbaranglokasiQuery(get_called_class());
    }
}
