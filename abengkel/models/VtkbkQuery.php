<?php

namespace abengkel\models;

use common\components\General;
use yii\db\Exception;
/**
 * This is the ActiveQuery class for [[Vtkbk]].
 *
 * @see Vtkbk
 */
class VtkbkQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Vtkbk[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Vtkbk|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }


	public function getTotalBayar( $KBKLink ) {
		$Terbayar = 0;
		try {
			$Terbayar = General::cCmd( "SELECT IFNULL(SUM(KBKBayar),0) FROM vtkbk WHERE KBKLink = :KBKLink GROUP BY KBKLink ", [ ':KBKLink' => $KBKLink ] )->queryScalar();
			if ( $Terbayar === false ) {
				$Terbayar = 0;
			}
		} catch ( Exception $e ) {
			$Terbayar = 0;
		}
		return $Terbayar;
	}
}
