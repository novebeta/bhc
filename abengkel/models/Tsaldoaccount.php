<?php

namespace abengkel\models;

/**
 * This is the model class for table "tsaldoaccount".
 *
 * @property string $NoAccount
 * @property string $TglSaldo
 * @property string $Debet
 * @property string $Kredit
 * @property string $SaldoAccount
 * @property string $TglOpen
 * @property string $MyOpening
 */
class Tsaldoaccount extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tsaldoaccount';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NoAccount', 'TglSaldo'], 'required'],
            [['TglSaldo', 'TglOpen'], 'safe'],
            [['Debet', 'Kredit', 'SaldoAccount', 'MyOpening'], 'number'],
            [['NoAccount'], 'string', 'max' => 10],
            [['NoAccount', 'TglSaldo'], 'unique', 'targetAttribute' => ['NoAccount', 'TglSaldo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'NoAccount' => 'No Account',
            'TglSaldo' => 'Tgl Saldo',
            'Debet' => 'Debet',
            'Kredit' => 'Kredit',
            'SaldoAccount' => 'Saldo Account',
            'TglOpen' => 'Tgl Open',
            'MyOpening' => 'My Opening',
        ];
    }


	/**
	 * {@inheritdoc}
	 */
	public static function colGrid()
	{
		return [
			'NoAccount' => 'No Account',
			'TglSaldo' => 'Tgl Saldo',
			'Debet' => 'Debet',
			'Kredit' => 'Kredit',
			'SaldoAccount' => 'Saldo Account',
			'TglOpen' => 'Tgl Open',
			'MyOpening' => 'My Opening',
		];
	}
    /**
     * {@inheritdoc}
     * @return TsaldoaccountQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TsaldoaccountQuery(get_called_class());
    }
}
