<?php

namespace abengkel\models;

/**
 * This is the ActiveQuery class for [[Tvpiutangbayar]].
 *
 * @see Tvpiutangbayar
 */
class TvpiutangbayarQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tvpiutangbayar[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tvpiutangbayar|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
