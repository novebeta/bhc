<?php

namespace abengkel\models;

/**
 * This is the model class for table "tdprogramit".
 *
 * @property string $PrgNama
 * @property int $PrgAuto
 * @property string $BrgKode
 * @property string $PrgHrgJual
 * @property string $PrgDiscount
 */
class Tdprogramit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdprogramit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PrgNama', 'PrgAuto'], 'required'],
            [['PrgAuto'], 'integer'],
            [['PrgHrgJual', 'PrgDiscount'], 'number'],
            [['PrgNama'], 'string', 'max' => 25],
            [['BrgKode'], 'string', 'max' => 20],
            [['PrgNama', 'PrgAuto'], 'unique', 'targetAttribute' => ['PrgNama', 'PrgAuto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PrgNama' => 'Prg Nama',
            'PrgAuto' => 'Prg Auto',
            'BrgKode' => 'Brg Kode',
            'PrgHrgJual' => 'Prg Hrg Jual',
            'PrgDiscount' => 'Prg Discount',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TdprogramitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdprogramitQuery(get_called_class());
    }
}
