<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttseitjasa".
 *
 * @property string $SENo
 * @property int $SEAuto
 * @property string $JasaKode
 * @property string $SEWaktuKerja
 * @property string $SEWaktuSatuan
 * @property string $SEWaktuKerjaMenit
 * @property string $SEHargaBeli
 * @property string $SEHrgJual
 * @property string $SEJualDisc
 */
class Ttseitjasa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttseitjasa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SENo', 'SEAuto'], 'required'],
            [['SEAuto'], 'integer'],
            [['SEWaktuKerja', 'SEWaktuKerjaMenit', 'SEHargaBeli', 'SEHrgJual', 'SEJualDisc'], 'number'],
            [['SENo', 'JasaKode'], 'string', 'max' => 10],
            [['SEWaktuSatuan'], 'string', 'max' => 5],
            [['SENo', 'SEAuto'], 'unique', 'targetAttribute' => ['SENo', 'SEAuto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SENo' => 'Se No',
            'SEAuto' => 'Se Auto',
            'JasaKode' => 'Jasa Kode',
            'SEWaktuKerja' => 'Se Waktu Kerja',
            'SEWaktuSatuan' => 'Se Waktu Satuan',
            'SEWaktuKerjaMenit' => 'Se Waktu Kerja Menit',
            'SEHargaBeli' => 'Se Harga Beli',
            'SEHrgJual' => 'Se Hrg Jual',
            'SEJualDisc' => 'Se Jual Disc',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtseitjasaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtseitjasaQuery(get_called_class());
    }
}
