<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttuhhd".
 *
 * @property string $UHNo
 * @property string $UHTgl
 * @property string $UHKeterangan
 * @property string $KodeTrans
 * @property string $PosKode
 * @property string $Cetak
 * @property string $UserID
 */
class Ttuhhd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttuhhd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['UHNo'], 'required'],
            [['UHTgl'], 'safe'],
            [['UHNo'], 'string', 'max' => 10],
            [['UHKeterangan'], 'string', 'max' => 150],
            [['KodeTrans'], 'string', 'max' => 4],
            [['PosKode'], 'string', 'max' => 20],
            [['Cetak', 'UserID'], 'string', 'max' => 15],
            [['UHNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'UHNo'         => 'Uh No',
            'UHTgl'        => 'Uh Tgl',
            'UHKeterangan' => 'Uh Keterangan',
            'KodeTrans'    => 'Kode Trans',
            'PosKode'      => 'Pos Kode',
            'Cetak'        => 'Cetak',
            'UserID'       => 'User ID',
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
        return [
            'UHNo'         => ['width' => 90,'label' => 'No UH','name'  => 'UHNo'],
            'UHTgl'        => ['width' => 120,'label' => 'Tgl UH','name'  => 'UHTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y H:i:s']],
            'KodeTrans'    => ['width' => 50,'label' => 'TC','name'  => 'KodeTrans'],
            'PosKode'      => ['width' => 90,'label' => 'PosKode','name'  => 'PosKode'],
            'Cetak'        => ['width' => 60,'label' => 'Cetak','name'  => 'Cetak'],
            'UHKeterangan' => ['width' => 240,'label' => 'Keterangan','name'  => 'UHKeterangan'],
            'UserID'       => ['width' => 90,'label' => 'UserID','name'  => 'UserID'],
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtuhhdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtuhhdQuery(get_called_class());
    }
}
