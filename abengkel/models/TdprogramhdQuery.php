<?php

namespace abengkel\models;

use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;

/**
 * This is the ActiveQuery class for [[Tdprogramhd]].
 *
 * @see Tdprogramhd
 */
class TdprogramhdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdprogramhd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdprogramhd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function combo() {
        return $this->select( [ "PrgNama as label", "PrgNama as value" ] )
            ->orderBy( 'PrgNama' )
            ->asArray()
            ->all();
    }

    public function select2($value, $label = [ 'PrgNama' ] , $orderBy = ['PrgTglAkhir' => SORT_DESC], $where = [ 'condition' => null, 'params' => [] ], $allOption = false ) {
        $option =
            $this->select( [ "CONCAT(" . implode( ",'||',", $label ) . ") as label", "$value as value" ] )
                ->orderBy( $orderBy )
                ->where( $where[ 'condition' ], $where[ 'params' ] )
                ->asArray()
                ->all();
        if ( $allOption ) {
            return array_merge( [ [ 'value' => '%', 'label' => 'Semua' ] ], $option );
        } else {
            return $option;
        }
    }

    public function tdprogramhdFillByNo() {
        return $this->select( new Expression( "PrgNama, PrgTglMulai, PrgTglAkhir, PrgMemo"));
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]        = Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'PrgNama' ]       = $post[ 'PrgNama' ] ?? '--';
        $post[ 'PrgNamaBaru' ]   = $post[ 'PrgNamaBaru' ] ?? '--';
        $post[ 'PrgTglMulai' ]   = date('Y-m-d H:i:s', strtotime($post[ 'PrgTglMulai' ] ?? date( 'Y-m-d H:i:s' )));
        $post[ 'PrgTglAkhir' ]   = date('Y-m-d H:i:s', strtotime($post[ 'PrgTglAkhir' ] ?? date( 'Y-m-d H:i:s' )));
        $post[ 'PrgMemo' ]       = $post[ 'PrgMemo' ] ?? '--';
        try {
            General::cCmd( "SET @PrgNama = ?;" )->bindParam( 1, $post[ 'PrgNama' ] )->execute();
            General::cCmd( "CALL fprogramhd(?,@Status,@Keterangan,?,?,?,@PrgNama,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'PrgNamaBaru' ] )
                ->bindParam( 6, $post[ 'PrgTglMulai' ] )
                ->bindParam( 7, $post[ 'PrgTglAkhir' ] )
                ->bindParam( 8, $post[ 'PrgMemo' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @PrgNama,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'PrgNama'    => $post[ 'PrgNama' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $PrgNama    = $exe[ '@PrgNama' ] ?? $post[ 'PrgNama' ];
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'PrgNama'    => $PrgNama,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
