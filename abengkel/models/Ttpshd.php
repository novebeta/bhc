<?php

namespace abengkel\models;

/**
 * This is the model class for table "ttpshd".
 *
 * @property string $PSNo
 * @property string $PSTgl
 * @property string $PINo
 * @property string $PONo
 * @property string $TINo
 * @property string $KodeTrans
 * @property string $PosKode
 * @property string $SupKode
 * @property string $DealerKode
 * @property string $LokasiKode
 * @property string $PSNoRef
 * @property string $PSSubTotal
 * @property string $PSDiscFinal
 * @property string $PSTotalPajak
 * @property string $PSBiayaKirim
 * @property string $PSTotal
 * @property string $PSTerbayar
 * @property string $PSKeterangan
 * @property string $NoGL
 * @property string $Cetak
 * @property string $UserID
 */
class Ttpshd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttpshd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PSNo'], 'required'],
            [['PSTgl'], 'safe'],
            [['PSSubTotal', 'PSDiscFinal', 'PSTotalPajak', 'PSBiayaKirim', 'PSTotal', 'PSTerbayar'], 'number'],
            [['PSNo', 'PINo', 'PONo', 'TINo', 'DealerKode', 'NoGL'], 'string', 'max' => 10],
            [['KodeTrans'], 'string', 'max' => 4],
            [['PosKode'], 'string', 'max' => 20],
            [['SupKode', 'LokasiKode', 'UserID'], 'string', 'max' => 15],
            [['PSNoRef'], 'string', 'max' => 35],
            [['PSKeterangan'], 'string', 'max' => 150],
            [['Cetak'], 'string', 'max' => 5],
            [['PSNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PSNo'         => 'Ps No',
            'PSTgl'        => 'Ps Tgl',
            'PINo'         => 'Pi No',
            'PONo'         => 'Po No',
            'TINo'         => 'Ti No',
            'KodeTrans'    => 'Kode Trans',
            'PosKode'      => 'Pos Kode',
            'SupKode'      => 'Sup Kode',
            'DealerKode'   => 'Dealer Kode',
            'LokasiKode'   => 'Lokasi Kode',
            'PSNoRef'      => 'Ps No Ref',
            'PSSubTotal'   => 'Ps Sub Total',
            'PSDiscFinal'  => 'Ps Disc Final',
            'PSTotalPajak' => 'Ps Total Pajak',
            'PSBiayaKirim' => 'Ps Biaya Kirim',
            'PSTotal'      => 'Ps Total',
            'PSTerbayar'   => 'Ps Terbayar',
            'PSKeterangan' => 'Ps Keterangan',
            'NoGL'         => 'No Gl',
            'Cetak'        => 'Cetak',
            'UserID'       => 'User ID',
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
        return [
            'PSNo'         => ['width' => 80,'label' => 'No PS','name'  => 'PSNo'],
            'PSTgl'        => ['width' => 115,'label' => 'Tgl PS','name'  => 'PSTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y H:i:s']],
            'PINo'         => ['width' => 80,'label' => 'No PI','name'  => 'PINo'],
            'PONo'         => ['width' => 80,'label' => 'No PO','name'  => 'PONo'],
            'TINo'         => ['width' => 80,'label' => 'No TI','name'  => 'TINo'],
            'KodeTrans'    => ['width' => 47,'label' => 'TC','name'  => 'KodeTrans'],
            'SupKode'      => ['width' => 90,'label' => 'Supplier','name'  => 'SupKode'],
            'LokasiKode'   => ['width' => 100,'label' => 'Lokasi','name'  => 'LokasiKode'],
            'PSNoRef'      => ['width' => 150,'label' => 'No Ref','name'  => 'PSNoRef'],
            'PSSubTotal'      => ['width' => 115,'label' => 'Total','name'  => 'PSSubTotal', 'formatter' => 'number', 'align' => 'right'],
			'NoGL'         => ['width' => 70,'label' => 'No JT','name'  => 'NoGL'],
			'Cetak'        => ['width' => 50,'label' => 'Cetak','name'  => 'Cetak'],
			'UserID'       => ['width' => 70,'label' => 'UserID','name'  => 'UserID'],
            'PSKeterangan' => ['width' => 300,'label' => 'Keterangan','name'  => 'PSKeterangan'],
			'PosKode'	=> ['width' => 90,'label' => 'Pos','name'  => 'PosKode'],
        ];
    }

    public static function colGridPick()
    {
        return [
            'PSNo'         => ['width' => 100,'label' => 'No PS','name'  => 'PSNo'],
            'PSTgl'        => ['width' => 120,'label' => 'Tgl PS','name'  => 'PSTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y']],
            'SupKode'      => ['width' => 120,'label' => 'Kode Supplier','name'  => 'SupKode'],
            'PSNoRef'      => ['width' => 100,'label' => 'No Ref','name'  => 'PSNoRef'],
            'LokasiKode'   => ['width' => 100,'label' => 'Lokasi','name'  => 'LokasiKode'],
            'KodeTrans'    => ['width' => 80,'label' => 'Kode','name'  => 'KodeTrans'],
            'PONo'         => ['width' => 100,'label' => 'No PO','name'  => 'PONo'],
            'PSKeterangan' => ['width' => 200,'label' => 'Keterangan','name'  => 'PSKeterangan'],
        ];
    }

    public static function colGridImportItems()
    {
        return [
            'noPenerimaan' => ['label' => 'No Penerimaan','width' => 170, 'name' => 'noPenerimaan'],
            'tglPenerimaan' => ['label' => 'Tgl Penerimaan','width' => 90, 'name' => 'tglPenerimaan'],
            'noShippingList' => ['label' => 'No Shipping List','width' => 170, 'name' => 'noShippingList'],
        ];
    }
    /**
     * {@inheritdoc}
     * @return TtpshdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtpshdQuery(get_called_class());
    }
}
