<?php
namespace abengkel\models;
use abengkel\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttsohd]].
 *
 * @see Ttsohd
 */
class TtsohdQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttsohd[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttsohd|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function GetDataByNo() {
		return $this->select( new Expression( "ttsohd.SONo, ttsohd.SOTgl, ttsohd.SINo, ttsohd.MotorNoPolisi, ttsohd.LokasiKode, ttsohd.KarKode,
		                 ttsohd.SOKeterangan, ttsohd.SOSubTotal, ttsohd.SODiscFinal, ttsohd.SOTotalPajak, ttsohd.SOBiayaKirim, ttsohd.SOTotal, 
                         ttsohd.UserID, ttsohd.KodeTrans, ttsohd.PosKode, ttsohd.Cetak, tdkaryawan.KarNama, tdcustomer.CusNama, tdcustomer.MotorWarna, 
                         tdcustomer.MotorNama, ttsohd.PONo, ttsohd.SOTerbayar" ) )
		            ->join( "LEFT JOIN", "tdkaryawan", "ttsohd.KarKode = tdkaryawan.KarKode" )
		            ->join( "LEFT OUTER JOIN", "tdcustomer", "ttsohd.MotorNoPolisi = tdcustomer.MotorNoPolisi" );
	}
	public function CekStatusBayar( $MyNumber, $SOTotal ) {
		$Terbayar    = \Yii::$app->db->createCommand( "SELECT IFNULL(SUM(KBMBayar),0) FROM vtkbm WHERE KBMLink = :KBMLink GROUP BY KBMLink", [
			':KBMLink' => $MyNumber,
		] )->queryScalar();
		$StatusBayar = 'BELUM';
		if ( ( $SOTotal - $Terbayar ) > 0 || $SOTotal == 0 ) {
			$StatusBayar = 'BELUM';
		} else if ( ( $SOTotal - $Terbayar ) <= 0 ) {
			$StatusBayar = 'LUNAS';
		}
		return [
			'TotalBayar'  => $Terbayar,
			'StatusBayar' => $StatusBayar
		];
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'SONo' ]          = $post[ 'SONo' ] ?? '--';
		$post[ 'SONoBaru' ]      = $post[ 'SONoView' ] ?? '--';
		$post[ 'SOTgl' ]         = date('Y-m-d H:i:s', strtotime($post[ 'SOTgl' ] ?? date( 'Y-m-d H:i:s' )));
		$post[ 'SINo' ]          = $post[ 'SINo' ] ?? '--';
		$post[ 'PONo' ]          = $post[ 'PONo' ] ?? '--';
		$post[ 'KodeTrans' ]     = $post[ 'KodeTrans' ] ?? '--';
		$post[ 'LokasiKode' ]    = $post[ 'LokasiKode' ] ?? '--';
		$post[ 'KarKode' ]       = $post[ 'KarKode' ] ?? '--';
		$post[ 'MotorNoPolisi' ] = $post[ 'MotorNoPolisi' ] ?? '--';
		$post[ 'SOSubTotal' ]    = floatval( $post[ 'SOSubTotal' ] ?? 0 );
		$post[ 'SODiscFinal' ]   = floatval( $post[ 'SODiscFinal' ] ?? 0 );
		$post[ 'SOTotalPajak' ]  = floatval( $post[ 'SOTotalPajak' ] ?? 0 );
		$post[ 'SOBiayaKirim' ]  = floatval( $post[ 'SOBiayaKirim' ] ?? 0 );
		$post[ 'SOTotal' ]       = floatval( $post[ 'SOTotal' ] ?? 0 );
		$post[ 'SOTerbayar' ]    = floatval( $post[ 'SOTerbayar' ] ?? 0 );
		$post[ 'SOKeterangan' ]  = $post[ 'SOKeterangan' ] ?? '--';
		$post[ 'Cetak' ]         = $post[ 'Cetak' ] ?? 'Belum';
		try {
			General::cCmd( "SET @SONo = ?;" )->bindParam( 1, $post[ 'SONo' ] )->execute();
			General::cCmd( "CALL fsohd(?,@Status,@Keterangan,?,?,?,@SONo,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'PosKode' ] )
			       ->bindParam( 5, $post[ 'SONoBaru' ] )
			       ->bindParam( 6, $post[ 'SOTgl' ] )
			       ->bindParam( 7, $post[ 'SINo' ] )
			       ->bindParam( 8, $post[ 'PONo' ] )
			       ->bindParam( 9, $post[ 'KodeTrans' ] )
			       ->bindParam( 10, $post[ 'LokasiKode' ] )
			       ->bindParam( 11, $post[ 'KarKode' ] )
			       ->bindParam( 12, $post[ 'MotorNoPolisi' ] )
			       ->bindParam( 13, $post[ 'SOSubTotal' ] )
			       ->bindParam( 14, $post[ 'SODiscFinal' ] )
			       ->bindParam( 15, $post[ 'SOTotalPajak' ] )
			       ->bindParam( 16, $post[ 'SOBiayaKirim' ] )
			       ->bindParam( 17, $post[ 'SOTotal' ] )
			       ->bindParam( 18, $post[ 'SOTerbayar' ] )
			       ->bindParam( 19, $post[ 'SOKeterangan' ] )
			       ->bindParam( 20, $post[ 'Cetak' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @SONo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'SONo'       => $post[ 'SONo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$SONo       = $exe[ '@SONo' ] ?? $post[ 'SONo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'SONo'       => $SONo,
			'status'     => $status,
			'keterangan' => str_replace(" ","&nbsp;",$keterangan)
		];
	}
}
