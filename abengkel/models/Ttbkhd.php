<?php
namespace abengkel\models;
/**
 * This is the model class for table "ttbkhd".
 *
 * @property string $BKNo
 * @property string $BKTgl
 * @property string $BKJenis
 * @property string $KodeTrans
 * @property string $PosKode
 * @property string $SupKode
 * @property string $BankKode
 * @property string $BKCekTempo
 * @property string $BKAC
 * @property string $BKPerson
 * @property string $BKCekNo
 * @property string $BKNominal
 * @property string $BKMemo
 * @property string $NoGL
 * @property string $Cetak
 * @property string $UserID
 */
class Ttbkhd extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttbkhd';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'BKNo' ], 'required' ],
			[ [ 'BKTgl', 'BKCekTempo' ], 'safe' ],
			[ [ 'BKNominal' ], 'number' ],
			[ [ 'BKNo' ], 'string', 'max' => 12 ],
			[ [ 'BKJenis', 'PosKode' ], 'string', 'max' => 20 ],
			[ [ 'KodeTrans' ], 'string', 'max' => 4 ],
			[ [ 'SupKode', 'UserID' ], 'string', 'max' => 15 ],
			[ [ 'BankKode', 'BKAC', 'BKCekNo' ], 'string', 'max' => 25 ],
			[ [ 'BKPerson' ], 'string', 'max' => 50 ],
			[ [ 'BKMemo' ], 'string', 'max' => 150 ],
			[ [ 'NoGL' ], 'string', 'max' => 10 ],
			[ [ 'Cetak' ], 'string', 'max' => 5 ],
			[ [ 'BKNo' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'BKNo'       => 'Bk No',
			'BKTgl'      => 'Bk Tgl',
			'BKJenis'    => 'Bk Jenis',
			'KodeTrans'  => 'Kode Trans',
			'PosKode'    => 'Pos Kode',
			'SupKode'    => 'Sup Kode',
			'BankKode'   => 'Bank Kode',
			'BKCekTempo' => 'Bk Cek Tempo',
			'BKAC'       => 'Bkac',
			'BKPerson'   => 'Bk Person',
			'BKCekNo'    => 'Bk Cek No',
			'BKNominal'  => 'Bk Nominal',
			'BKMemo'     => 'Bk Memo',
			'NoGL'       => 'No Gl',
			'Cetak'      => 'Cetak',
			'UserID'     => 'User ID',
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public static function colGrid() {
		return [
			'BKNo'        => [ 'width' => 95, 'label' => 'No BK', 'name' => 'BKNo' ],
			'BKTgl'       => [ 'width' => 120, 'label' => 'Tanggal BK', 'name' => 'BKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y H:i:s' ] ],
			'KodeTrans'   => [ 'width' => 40, 'label' => 'TC', 'name' => 'KodeTrans' ],
			'BankKode'    => [ 'width' => 70, 'label' => 'Kode Bank', 'name' => 'BankKode' ],
			'NamaAccount' => [ 'width' => 200, 'label' => 'Nama Bank', 'name' => 'NamaAccount' ],
			'SupKode'     => [ 'width' => 90, 'label' => 'Supplier', 'name' => 'SupKode' ],
			'BKPerson'    => [ 'width' => 220, 'label' => 'Terima Dari', 'name' => 'BKPerson' ],
			'BKNominal'   => [ 'width' => 100, 'label' => 'Nominal', 'name' => 'BKNominal', 'formatter' => 'number', 'align' => 'right' ],
			'BKJenis'     => [ 'width' => 50, 'label' => 'Jenis', 'name' => 'BKJenis' ],
			'NoGL'        => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
			'BKCekNo'     => [ 'width' => 70, 'label' => 'No Cek', 'name' => 'BKCekNo' ],
			'BKAC'        => [ 'width' => 70, 'label' => 'AC', 'name' => 'BKAC' ],
			'BKCekTempo'  => [ 'width' => 70, 'label' => 'AC Tempo', 'name' => 'BKCekTempo', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'UserID'      => [ 'width' => 80, 'label' => 'User', 'name' => 'UserID' ],
			'Cetak'       => [ 'width' => 50, 'label' => 'Cetak', 'name' => 'Cetak' ],
			'BKMemo'      => [ 'width' => 500, 'label' => 'Keterangan', 'name' => 'BKMemo' ],
			'PosKode'     => [ 'width' => 100, 'label' => 'PosKode', 'name' => 'PosKode' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public static function colGridPick() {
		return [
			'No'        => [ 'width' => 95, 'label' => 'No', 'name' => 'NO' ],
			'Tgl'       => [ 'width' => 120, 'label' => 'Tgl', 'name' => 'Tgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y  H:i:s' ] ],
			'Kode'      => [ 'width' => 70, 'label' => 'Kode', 'name' => 'Kode' ],
			'Nama'      => [ 'width' => 70, 'label' => 'Nama', 'name' => 'Nama' ],
			'MotorNama' => [ 'width' => 140, 'label' => 'MotorNama', 'name' => 'MotorNama' ],
			'Total'     => [ 'width' => 100, 'label' => 'Total', 'name' => 'Total', 'formatter' => 'number', 'align' => 'right' ],
			'Terbayar'  => [ 'width' => 100, 'label' => 'Terbayar', 'name' => 'Terbayar', 'formatter' => 'number', 'align' => 'right' ],
			'Sisa'      => [ 'width' => 100, 'label' => 'Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
			'Bayar'     => [ 'width' => 100, 'label' => 'Bayar', 'name' => 'Bayar', 'formatter' => 'number', 'align' => 'right' ],
		];
	}
	public static function colGridTransferBank() {
		return [
			'BKLink'                => [ 'label' => 'No BT', 'width' => 75, 'name' => 'BKLink' ],
			'BKTgl'                 => [ 'label' => 'Tgl BT', 'width' => 115, 'name' => 'BKTgl' ],
			'NoAccountBK'           => [ 'label' => 'COA BK', 'width' => 60, 'name' => 'NoAccountBK' ],
			'NamaAccountBK'         => [ 'label' => 'Bank Keluar', 'width' => 175, 'name' => 'NamaAccountBK' ],
			'NoAccountBM'           => [ 'label' => 'COA BM', 'width' => 60, 'name' => 'NoAccountBM' ],
			'NamaAccountBM'         => [ 'label' => 'Bank Masuk', 'width' => 175, 'name' => 'NamaAccountBM' ],
			'BKNominal'             => [ 'label' => 'Nominal', 'width' => 105, 'name' => 'BKNominal', 'formatter' => 'number', 'align' => 'right' ],
			'ttbkit.BKNo'           => [ 'label' => 'No BK', 'width' => 85, 'name' => 'BKNo' ],
			'ttbmit.BMNo'           => [ 'label' => 'No BM', 'width' => 85, 'name' => 'BMNo' ],
			'BKMemo'                => [ 'width' => 300, 'label' => 'Keterangan', 'name' => 'BKMemo' ],
			'BKPerson'              => [ 'label' => 'Pengirim', 'width' => 150, 'name' => 'BKPerson' ],
			'BMPerson'              => [ 'label' => 'Penerima', 'width' => 150, 'name' => 'BMPerson' ],
			'ttbkhd.NoGL AS NoGLBK' => [ 'label' => 'No JT BK', 'width' => 70, 'name' => 'NoGLBK' ],
			'ttbmhd.NoGL AS NoGLBM' => [ 'label' => 'No JT BM', 'width' => 70, 'name' => 'NoGLBM' ],
			'ttbkhd.UserID'         => [ 'label' => 'UserID', 'width' => 80, 'name' => 'UserID' ]
		];
	}
	public static function colGridPickPI() {
		return [
			'No'        => [ 'width' => 95, 'label' => 'No Beli', 'name' => 'NO' ],
			'Tgl'       => [ 'width' => 120, 'label' => 'Tgl Beli', 'name' => 'Tgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y  H:i:s' ] ],
			'Kode'      => [ 'width' => 70, 'label' => 'Suplier', 'name' => 'Kode' ],
			'Nama'      => [ 'width' => 130, 'label' => 'Nama Supplier', 'name' => 'Nama' ],
			'MotorNama' => [ 'width' => 160, 'label' => 'Lokasi', 'name' => 'MotorNama' ],
			'Total'     => [ 'width' => 80, 'label' => 'Total', 'name' => 'Total', 'formatter' => 'number', 'align' => 'right' ],
			'Terbayar'  => [ 'width' => 80, 'label' => 'Terbayar', 'name' => 'Terbayar', 'formatter' => 'number', 'align' => 'right' ],
			'Sisa'      => [ 'width' => 80, 'label' => 'Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
			'Bayar'     => [ 'width' => 80, 'label' => 'Bayar', 'name' => 'Bayar', 'formatter' => 'number', 'align' => 'right' ],
		];
	}
	public static function colGridPickSR() {
		return [
			'No'        => [ 'width' => 95, 'label' => 'No Retur Jual', 'name' => 'NO' ],
			'Tgl'       => [ 'width' => 120, 'label' => 'Tgl Retur Jual', 'name' => 'Tgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y  H:i:s' ] ],
			'Kode'      => [ 'width' => 70, 'label' => 'No Polisi', 'name' => 'Kode' ],
			'Nama'      => [ 'width' => 130, 'label' => 'Nama Konsumen', 'name' => 'Nama' ],
			'MotorNama' => [ 'width' => 160, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
			'Total'     => [ 'width' => 80, 'label' => 'Total', 'name' => 'Total', 'formatter' => 'number', 'align' => 'right' ],
			'Terbayar'  => [ 'width' => 80, 'label' => 'Terbayar', 'name' => 'Terbayar', 'formatter' => 'number', 'align' => 'right' ],
			'Sisa'      => [ 'width' => 80, 'label' => 'Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
			'Bayar'     => [ 'width' => 80, 'label' => 'Bayar', 'name' => 'Bayar', 'formatter' => 'number', 'align' => 'right' ],
		];
	}
	public static function colGridSelectBk() {
		return [
			'No'          => [ 'width' => 95, 'label' => 'No BK', 'name' => 'No' ],
			'BKTgl'         => [ 'width' => 120, 'label' => 'Tgl BK', 'name' => 'BKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y  H:i:s' ] ],
			'BankKode'    => [ 'width' => 70, 'label' => 'Nama Bank', 'name' => 'BankKode' ],
			'NamaAccount' => [ 'width' => 160, 'label' => 'Nama Bank', 'name' => 'NamaAccount' ],
			'BKNominal'   => [ 'width' => 100, 'label' => 'Nominal', 'name' => 'BKNominal', 'formatter' => 'number', 'align' => 'right' ],
			'BKPerson'    => [ 'width' => 100, 'label' => 'Terima Dari', 'name' => 'BKPerson' ],
			'BKMemo'      => [ 'width' => 160, 'label' => 'Keterangan', 'name' => 'BKMemo' ],
			'BKJenis'     => [ 'width' => 80, 'label' => 'Jenis', 'name' => 'BKJenis' ],
			'BKCekNo'     => [ 'width' => 80, 'label' => 'No Cek', 'name' => 'BKCekNo' ],
			'BKAC'        => [ 'width' => 80, 'label' => 'No A/C', 'name' => 'BKAC' ],
			'UserID'      => [ 'width' => 80, 'label' => 'UserID', 'name' => 'UserID' ],
		];
	}
	public function getAccount() {
		return $this->hasOne( Traccount::className(), [ 'NoAccount' => 'BankKode' ] );
	}
	public function getBankKeluarItem() {
		return $this->hasMany( Ttbkit::className(), [ 'BKNo' => 'BKNo' ] );
	}
	public function getBankMasukItem() {
		return $this->hasMany( Ttbmit::className(), [ 'BMLink' => 'BKLink' ] )
		            ->via( 'bankKeluarItem' );
	}
	public function getBankMasukHeader() {
		return $this->hasOne( Ttbmhd::className(), [ 'BMNo' => 'BMNo' ] )
		            ->via( 'bankMasukItem' );
	}
	public function getAccountBankMasuk() {
		return $this->hasOne( Traccount::className(), [ 'NoAccount' => 'BankKode' ] )
		            ->via( 'bankMasukHeader' )
		            ->from( Traccount::tableName() . ' traccountbm' );
	}
	public function getAccountBankKeluar() {
		return $this->hasOne( Traccount::className(), [ 'NoAccount' => 'BankKode' ] )
		            ->from( Traccount::tableName() . ' traccountbk' );
	}
	public static function generateBKNo( $BankKode, $temporary = false ) {
		$sign    = $temporary ? '=' : '-';
		$kode    = 'BK';
		$kBank   = substr( $BankKode, - 2 );
		$yNow    = date( "y" );
		$maxNoGl = ( new \yii\db\Query() )
			->from( self::tableName() )
			->where( "BKNo LIKE '$kode$kBank$yNow$sign%'" )
			->max( 'BKNo' );
		if ( $maxNoGl && preg_match( "/$kode\d{4}$sign(\d+)/", $maxNoGl, $result ) ) {
			[ $all, $counter ] = $result;
			$digitCount  = strlen( $counter );
			$val         = (int) $counter;
			$nextCounter = sprintf( '%0' . $digitCount . 'd', ++ $val );
		} else {
			$nextCounter = "00001";
		}
		return "$kode$kBank$yNow$sign$nextCounter";
	}
	/**
	 * {@inheritdoc}
	 * @return TtbkhdQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TtbkhdQuery( get_called_class() );
	}
	public static function getRoute( $BKJenis, $param = [] ) {
		$index  = [];
		$update = [];
		$create = [];
		switch ( $BKJenis ) {
			case 'UM' :
				$index  = [ 'ttbkhd/bank-keluar-umum' ];
				$update = [ 'ttbkhd/bank-keluar-umum-update' ];
				$create = [ 'ttbkhd/bank-keluar-umum-create' ];
				break;
			case 'KM' :
				$index  = [ 'ttbkhd/bank-keluar-ke-kas' ];
				$update = [ 'ttbkhd/bank-keluar-ke-kas-update' ];
				$create = [ 'ttbkhd/bank-keluar-ke-kas-create' ];
				break;
			case 'PI' :
				$index  = [ 'ttbkhd/bank-keluar-pembelian' ];
				$update = [ 'ttbkhd/bank-keluar-pembelian-update' ];
				$create = [ 'ttbkhd/bank-keluar-pembelian-create' ];
				break;
			case 'PL' :
				$index  = [ 'ttbkhd/bank-keluar-order-pengerjaan-luar' ];
				$update = [ 'ttbkhd/bank-keluar-order-pengerjaan-luar-update' ];
				$create = [ 'ttbkhd/bank-keluar-order-pengerjaan-luar-create' ];
				break;
			case 'SR' :
				$index  = [ 'ttbkhd/bank-keluar-retur-penjualan' ];
				$update = [ 'ttbkhd/bank-keluar-retur-penjualan-update' ];
				$create = [ 'ttbkhd/bank-keluar-retur-penjualan-create' ];
				break;
			case 'BT' :
				$index  = [ 'ttbkhd/transfer-antar-bank' ];
				$update = [ 'ttbkhd/transfer-antar-bank-update' ];
				$create = [ 'ttbkhd/transfer-antar-bank-create' ];
				break;
		}
		return [
			'index'  => \yii\helpers\Url::toRoute( array_merge( $index, $param ) ),
			'update' => \yii\helpers\Url::toRoute( array_merge( $update, $param ) ),
			'create' => \yii\helpers\Url::toRoute( array_merge( $create, $param ) ),
		];
	}
}
