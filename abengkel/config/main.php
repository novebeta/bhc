<?php
$params = array_merge(
	require __DIR__ . '/../../common/config/params.php',
	require __DIR__ . '/../../common/config/params-local.php',
	require __DIR__ . '/params.php',
	require __DIR__ . '/params-local.php'
);
return [
	'id'                  => 'app-backend',
	'name'                => 'HVYS – Bengkel',
	'basePath'            => dirname( __DIR__ ),
	'controllerNamespace' => 'abengkel\controllers',
	'bootstrap'           => [ 'log' ],
	'modules'             => [],
	'components'          => [
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		/*
		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'rules' => [
			],
		],
		*/
	],
	'params'              => $params,
];
