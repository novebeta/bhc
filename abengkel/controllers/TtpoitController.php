<?php

namespace abengkel\controllers;

use abengkel\components\AbengkelController;
use abengkel\models\Ttpohd;
use abengkel\models\Ttpoit;
use abengkel\models\Ttsohd;
use Yii;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtpoitController implements the CRUD actions for Ttpoit model.
 */
class TtpoitController extends AbengkelController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ttpoit models.
     * @return mixed
     */
    public function actionIndex()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $requestData                 = \Yii::$app->request->post();
        $primaryKeys                 = Ttpoit::getTableSchema()->primaryKey;
        if ( isset( $requestData[ 'oper' ] ) ) {
            /* operation : create, edit or delete */
            $oper = $requestData[ 'oper' ];
            /** @var Ttpoit $model */
            $model = null;
            if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
                $model = $this->findModelBase64( 'ttpoit', $requestData[ 'id' ] );
            }
            switch ( $oper ) {
                case 'add'  :
                case 'edit' :
                if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
                        $requestData[ 'Action' ] = 'Insert';
                        $requestData[ 'POAuto' ] = 0;
                    } else {
                        $requestData[ 'Action' ] = 'Update';
                        if ( $model != null ) {
                            $requestData[ 'PONoOLD' ]   = $model->PONo;
                            $requestData[ 'POAutoOLD' ] = $model->POAuto;
                            $requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
                        }
                    }
                    $result = Ttpoit::find()->callSP( $requestData );
                        if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                            $response = $this->responseSuccess( $result[ 'keterangan' ] );
                        } else {
                            $response = $this->responseFailed( $result[ 'keterangan' ] );
                        }
                    break;
                case 'batch' :
	                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	                $respon                      = [];
	                $base64                      = urldecode( base64_decode( $requestData[ 'header' ] ) );
	                parse_str( $base64, $header );
	                /** @var Ttsohd $SO */
	                $SO = Ttsohd::findOne($header[ 'PSNo' ]);
	                if($SO != null){
		                $header[ 'SOTgl' ] = $SO->SOTgl;
	                }
	                $header[ 'Action' ] = 'Loop';
	                $result             = Ttpohd::find()->callSP( $header );
	                Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
	                foreach ( $requestData[ 'data' ] as $item ) {
		                $item[ 'Action' ]     = 'Insert';
		                $result                    = Ttpoit::find()->callSP( $item );
		                Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
	                }
	                $header[ 'Action' ] = 'LoopAfter';
	                $result             = Ttpohd::find()->callSP( $header );
	                Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
	                return $respon;
                    break;
                case 'del'  :
                    $requestData             = array_merge( $requestData, $model->attributes );
                    $requestData[ 'Action' ] = 'Delete';
                    if ( $model != null ) {
                        $requestData[ 'PONoLD' ]   = $model->PONo;
                        $requestData[ 'POAutoOLD' ] = $model->POAuto;
                        $requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
                    }
                    $result = Ttpoit::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
                    break;
            }
        } else {
            for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
                $primaryKeys[ $i ] = 'Ttpoit.' . $primaryKeys[ $i ];
            }

            $jenis = $_GET['jenis'] ?? ' ';

            switch ( $jenis ) {
                case 'FillItemPOPS':
                    $query = (new \yii\db\Query())
                        ->select(new Expression(
                            'CONCAT(' . implode(',"||",', $primaryKeys) . ') AS id,
							    	ttpoit.PONo, ttpoit.POAuto, ttpoit.BrgKode, (ttpoit.POQty - ttpoit.PSQtyS) As POQty, ttpoit.POHrgBeli, ttpoit.POHrgJual, ttpoit.PODiscount, ttpoit.POPajak, tdbarang.BrgNama, tdbarang.BrgSatuan, 0 AS Disc, 
                                    ttpoit.POQty * ttpoit.POHrgJual - ttpoit.PODiscount AS Jumlah,  tdbarang.BrgGroup, ttpohd.LokasiKode'))
                        ->from('ttpoit')
                        ->join('INNER JOIN', 'tdbarang', 'ttpoit.BrgKode = tdbarang.BrgKode')
                        ->join('INNER JOIN', 'ttpohd', 'ttpohd.PONo = ttpoit.PONo')
                        ->where('ttpoit.PSQtyS = 0 AND ttpoit.PONo = :PONo',[':PONo' => $requestData['PONo']])
                        ->orderBy('	ttpoit.PONo, ttpoit.POAuto');
                    break;
                default:
                    $query = (new \yii\db\Query())
                        ->select(new Expression(
                            'CONCAT(' . implode(',"||",', $primaryKeys) . ') AS id,
							    	ttpoit.PONo,ttpoit.POAuto,ttpoit.BrgKode,ttpoit.POQty,ttpoit.POSaldo,ttpoit.POQtyAju,ttpoit.POSLAwal,ttpoit.POHrgBeli,ttpoit.POHrgJual,
                                    ttpoit.PODiscount,ttpoit.POPajak,tdbarang.BrgNama,tdbarang.BrgSatuan,
                                    IFNULL(ttpoit.PODiscount / ( ttpoit.POHrgJual * ttpoit.POQty) * 100, 0 ) AS Disc,
                                    ttpoit.POQty * ttpoit.POHrgJual - ttpoit.PODiscount AS Jumlah,tdbarang.BrgGroup,ttpoit.PSNo,ttpoit.SONo'
                        ))
                        ->from('ttpoit')
                        ->join('INNER JOIN', 'tdbarang', 'ttpoit.BrgKode = tdbarang.BrgKode')
                        ->where(['ttpoit.PONo' => $requestData['PONo']])
                        ->orderBy('	ttpoit.PONo, ttpoit.POAuto');
            }
            $rowsCount = $query->count();
            $rows      = $query->all();
            /* encode row id */
            for ( $i = 0; $i < count( $rows ); $i ++ ) {
                $rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
            }
            $response = [
                'rows'      => $rows,
                'rowsCount' => $rowsCount
            ];
        }
        return $response;
    }

    /**
     * Creates a new Ttpoit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ttpoit();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'PONo' => $model->PONo, 'POAuto' => $model->POAuto]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ttpoit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $PONo
     * @param integer $POAuto
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($PONo, $POAuto)
    {
        $model = $this->findModel($PONo, $POAuto);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'PONo' => $model->PONo, 'POAuto' => $model->POAuto]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ttpoit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $PONo
     * @param integer $POAuto
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($PONo, $POAuto)
    {
        $this->findModel($PONo, $POAuto)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ttpoit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $PONo
     * @param integer $POAuto
     * @return Ttpoit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($PONo, $POAuto)
    {
        if (($model = Ttpoit::findOne(['PONo' => $PONo, 'POAuto' => $POAuto])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
