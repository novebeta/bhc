<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\models\Ttkmhd;
use abengkel\models\Ttkmit;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtkmitController implements the CRUD actions for Ttkmit model.
 */
class TtkmitController extends AbengkelController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttkmit models.
	 * @return mixed
	 */
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttkmit::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
			/** @var Ttkmit $model */
			$model = null;
			if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
				$model = $this->findModelBase64( 'ttkmit', $requestData[ 'id' ] );
			}
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'Action' ] = 'Insert';
						$requestData[ 'BMLink' ] = '--';
					} else {
						$requestData[ 'Action' ] = 'Update';
						if ( $model != null ) {
							$requestData[ 'KMNoOLD' ]   = $model->KMNo;
							$requestData[ 'KMLinkOLD' ] = $model->KMLink;
						}
					}
					$result = Ttkmit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'batch' :
					\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
					$respon                      = $this->responseFailed( 'Add Item' );
					$header                      = $requestData[ 'header' ];
					$header[ 'Action' ]          = 'Loop';
					$result                      = Ttkmhd::find()->callSP( $header );
					$KMNominal                   = 0;
					foreach ( $requestData[ 'data' ] as $item ) {
						$items[ 'Action' ]  = 'Insert';
						$items[ 'KMNo' ]    = $header[ 'KMNo' ];
						$items[ 'KMLink' ]  = $item[ 'data' ][ 'NO' ];
						$items[ 'KMBayar' ] = $item[ 'data' ][ 'Sisa' ];
						$KMNominal          += floatval( $item[ 'data' ][ 'Sisa' ] );
						$result             = Ttkmit::find()->callSP( $items );
					}
					$header[ 'Action' ]    = 'LoopAfter';
					$header[ 'KMNominal' ] = $KMNominal;
					$result                = Ttkmhd::find()->callSP( $header );
					return $this->responseSuccess( $requestData );
					break;
				case 'del'  :
					$requestData             = array_merge( $requestData, $model->attributes );
					$requestData[ 'Action' ] = 'Delete';
					if ( $model != null ) {
						$requestData[ 'KMNoOLD' ]   = $model->KMNo;
						$requestData[ 'KMLinkOLD' ] = $model->KMLink;
					}
					$result = Ttkmit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
			}
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'Ttkmit.' . $primaryKeys[ $i ];
			}
			/** @var Ttkmhd $model */
			$model = Ttkmhd::find()->where( [ 'KMNo' => $requestData[ 'KMNo' ] ] )->one();
			$query = new Query();
			switch ( $model->KodeTrans ) {
				case 'SO' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,KMNo, KMLink, KMBayar,SOTgl AS TglLink, MotorNoPolisi AS BiayaNama,
					CusNama,SOTotal AS Total, KMPaid.Paid - KMBayar AS Terbayar,  SOTotal - KMPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(SELECT ttKMit.*, ttSOhd.*, tdcustomer.CusNama FROM ttKMit 
                     INNER JOIN ttSOhd ON KMLink = SONo INNER JOIN tdcustomer ON ttSOhd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                     WHERE ttKMit.KMNo = :KMNo) ttKMit" ) )
					      ->addParams( [ ':KMNo' => $model->KMNo ] )
					      ->innerJoin( "(SELECT KBMLINK, IFNULL(SUM(KBMBayar), 0) AS Paid FROM vtkbm  GROUP BY KBMLINK) KMPaid",
						      "ttKMit.KMLink = KMPaid.KBMLINK" );
					break;
				case 'SV' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,KMNo, KMLink, KMBayar,SDTgl AS TglLink, MotorNoPolisi AS BiayaNama,
					CusNama,SDTotalBiaya AS Total, KMPaid.Paid - KMBayar AS Terbayar,  SDTotalBiaya - KMPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(SELECT ttKMit.*, ttSDhd.*, tdcustomer.CusNama FROM ttKMit 
                     INNER JOIN ttSDhd ON KMLink = SVNo INNER JOIN tdcustomer ON ttSDhd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                     WHERE ttKMit.KMNo = :KMNo) ttKMit" ) )
					      ->addParams( [ ':KMNo' => $model->KMNo ] )
					      ->innerJoin( "(SELECT KBMLINK, IFNULL(SUM(KBMBayar), 0) AS Paid FROM vtkbm  GROUP BY KBMLINK) KMPaid",
						      "ttKMit.KMLink = KMPaid.KBMLINK" );
					break;
				case 'SI' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,KMNo, KMLink, KMBayar,SITgl AS TglLink, MotorNoPolisi AS BiayaNama,
					CusNama,SITotal AS Total, KMPaid.Paid - KMBayar AS Terbayar,  SITotal - KMPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(SELECT ttKMit.*, ttsihd.*,tdcustomer.CusNama FROM ttKMit 
                     INNER JOIN ttsihd ON KMLink = SINo 
                     INNER JOIN tdcustomer ON ttsihd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                     WHERE ttKMit.KMNo = :KMNo) ttKMit" ) )
					      ->addParams( [ ':KMNo' => $model->KMNo ] )
					      ->innerJoin( "(SELECT KBMLINK, IFNULL(SUM(KBMBayar), 0) AS Paid FROM vtkBM  GROUP BY KBMLINK) KMPaid",
						      "ttKMit.KMLink = KMPaid.KBMLINK" );
					break;
				case 'PR' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,KMNo, KMLink, KMBayar,PRTgl AS TglLink, SupKode AS BiayaNama,
					CusNama,PRTotal AS Total, KMPaid.Paid - KMBayar AS Terbayar,  PRTotal - KMPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(SELECT ttKMit.*, ttprhd.*, tdsupplier.SupNama AS CusNama FROM ttKMit 
                     INNER JOIN ttprhd ON KMLink = PRNo 
                     INNER JOIN tdsupplier ON ttprhd.SupKode = tdsupplier.SupKode  
                     WHERE ttKMit.KMNo = :KMNo) ttKMit" ) )
					      ->addParams( [ ':KMNo' => $model->KMNo ] )
					      ->innerJoin( "(SELECT KBMLINK, IFNULL(SUM(KBMBayar), 0) AS Paid FROM vtKBM  GROUP BY KBMLINK) KMPaid",
						      "ttKMit.KMLink = KMPaid.KBMLINK" );
					break;
				case 'SE' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,KMNo, KMLink, KMBayar,SETgl AS TglLink, MotorNoPolisi AS BiayaNama,
					CusNama,SETotalBiaya AS Total, KMPaid.Paid - KMBayar AS Terbayar,  SETotalBiaya - KMPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(SELECT ttKMit.*, ttSEhd.*, tdcustomer.CusNama FROM ttKMit 
                     INNER JOIN ttSEhd ON KMLink = SENo INNER JOIN tdcustomer ON ttSEhd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                     WHERE ttKMit.KMNo = :KMNo) ttKMit" ) )
					      ->addParams( [ ':KMNo' => $model->KMNo ] )
					      ->innerJoin( "(SELECT KBMLINK, IFNULL(SUM(KBMBayar), 0) AS Paid FROM vtkbm  GROUP BY KBMLINK) KMPaid",
						      "ttKMit.KMLink = KMPaid.KBMLINK" );
					break;
				default;
			}
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	/**
	 * Displays a single Ttkmit model.
	 *
	 * @param string $KMNo
	 * @param string $KMLink
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $KMNo, $KMLink ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $KMNo, $KMLink ),
		] );
	}
	/**
	 * Finds the Ttkmit model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $KMNo
	 * @param string $KMLink
	 *
	 * @return Ttkmit the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $KMNo, $KMLink ) {
		if ( ( $model = Ttkmit::findOne( [ 'KMNo' => $KMNo, 'KMLink' => $KMLink ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttkmit model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Ttkmit();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'KMNo' => $model->KMNo, 'KMLink' => $model->KMLink ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Ttkmit model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $KMNo
	 * @param string $KMLink
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $KMNo, $KMLink ) {
		$model = $this->findModel( $KMNo, $KMLink );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'KMNo' => $model->KMNo, 'KMLink' => $model->KMLink ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Ttkmit model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $KMNo
	 * @param string $KMLink
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $KMNo, $KMLink ) {
		$this->findModel( $KMNo, $KMLink )->delete();
		return $this->redirect( [ 'index' ] );
	}
}
