<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\components\TUi;
use abengkel\models\Tdarea;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
class TdareaController extends AbengkelController {

	public function beforeAction( $action ) {
		$GLOBALS[ 'MenuText' ] = 'Area';
		switch ( $action->id ) {
			case 'create':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::ADD );
				break;
			case 'update':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::UPDATE );
				break;
			case 'delete':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::DELETE );
				break;
			case 'index':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::DISPLAY );
				break;
			case 'td':
			case 'find':
			case 'cancel':
				$this->isAllow = 1;
				break;
		}
		if($this->isAllow == 0) throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
		return true;
	}

	public function actions() {
		return [
			'td' => [
				'class' => TdAction::class,
				'model' => Tdarea::class,
			],
		];
	}
	public function actionIndex() {
		return $this->render( 'index' );
	}
	/**
	 * Creates a new Tdarea model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		TUi::$actionMode = Menu::ADD;
		$model           = new Tdarea();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'tdarea/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
		}
		return $this->render( 'create', [
			'model' => $model,
			'id'    => 'new'
		] );
	}
	/**
	 * Updates an existing Tdarea model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		$index = \yii\helpers\Url::toRoute( [ 'tdarea/index' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$model = $this->findModelBase64( 'Tdarea', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [
				'tdarea/update',
				'Provinsi'  => $model->Provinsi,
				'Kabupaten' => $model->Kabupaten,
				'Kecamatan' => $model->Kecamatan,
				'Kelurahan' => $model->Kelurahan,
				'KodePos'   => $model->KodePos,
				'id'        => $this->generateIdBase64FromModel( $model ),
				'action'    => 'display'
			] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	protected function findModel( $Provinsi, $Kabupaten, $Kecamatan, $Kelurahan, $KodePos ) {
		if ( ( $model = Tdarea::findOne( [
				'Provinsi'  => $Provinsi,
				'Kabupaten' => $Kabupaten,
				'Kecamatan' => $Kecamatan,
				'Kelurahan' => $Kelurahan,
				'KodePos'   => $KodePos
			] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	public function actionFind() {
		$params = \Yii::$app->request->post();
		$rows   = [];
		if ( $params[ 'mode' ] === 'combo' ) {
			$rows = Tdarea::find()->combo( $params );
		}
		return $this->responseDataJson( $rows );
	}
	public function actionCancel( $id, $action ) {
		$model = Tdarea::findOne( $id );
		if ( $model == null ) {
			$model = Tdarea::find()->orderBy( [ 'AreaStatus' => SORT_ASC ] )->one();
			if ( $model == null ) {
				return $this->redirect( [ 'tdarea/index' ] );
			}
		}
		return $this->redirect( [ 'tdarea/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
	}
}
