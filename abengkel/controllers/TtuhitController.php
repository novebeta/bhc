<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\models\Ttuhit;
use common\components\Custom;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtuhitController implements the CRUD actions for Ttuhit model.
 */
class TtuhitController extends AbengkelController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttuhit::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
            /** @var Ttuhit $model */
            $model = null;
            if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
                $model = $this->findModelBase64( 'ttuhit', $requestData[ 'id' ] );
            }
            switch ( $oper ) {
                case 'add'  :
                case 'edit' :
                    if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
                        $requestData[ 'Action' ] = 'Insert';
                        $requestData[ 'UHAuto' ] = 0;
                    } else {
                        $requestData[ 'Action' ] = 'Update';
                        if ( $model != null ) {
                            $requestData[ 'UHNoOLD' ]   = $model->UHNo;
                            $requestData[ 'UHAutoOLD' ] = $model->UHAuto;
                            $requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
                        }
                    }
                    $result = Ttuhit::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
                    break;
                case 'batch' :
                    break;
                case 'del'  :
                    $requestData             = array_merge( $requestData, $model->attributes );
                    $requestData[ 'Action' ] = 'Delete';
                    if ( $model != null ) {
                        $requestData[ 'UHNoLD' ]   = $model->UHNo;
                        $requestData[ 'UHAutoOLD' ] = $model->UHAuto;
                        $requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
                    }
                    $result = Ttuhit::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
                    break;
            }
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'ttuhit.' . $primaryKeys[ $i ];
			}
			$query     = ( new \yii\db\Query() )
				->select( new Expression(
					'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
								ttuhit.UHNo, ttuhit.UHAuto, ttuhit.BrgKode, ttuhit.UHHrgJualLama, ttuhit.UHHrgJualBaru, tdbarang.BrgNama, tdbarang.BrgGroup, tdbarang.BrgSatuan'
				) )
				->from( 'ttuhit' )
				->join( 'INNER JOIN', 'tdbarang', 'ttuhit.BrgKode = tdbarang.BrgKode' )
				->where( "(ttuhit.UHNo = :UHNo)", [ ':UHNo' => $requestData[ 'UHNo' ] ] );
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	protected function findModel( $UHNo, $UHAuto ) {
		if ( ( $model = Ttuhit::findOne( [ 'UHNo' => $UHNo, 'UHAuto' => $UHAuto ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
