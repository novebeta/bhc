<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\models\Traccount;
use abengkel\models\Ttbmhd;
use abengkel\models\Ttbmit;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtbmhdController implements the CRUD actions for Ttbmhd model.
 */
class TtbmhdController extends AbengkelController {
	public function actions() {
		$colGrid               = null;
		$joinWith              = [];
		$join                  = [];
		$where                 = [];
		$joinWith[ 'account' ] = [
			'type' => 'INNER JOIN'
		];
		$sqlraw                = null;
		$queryRawPK            = null;
		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			switch ( $r ) {
				case 'ttbmhd/bank-masuk-dari-h1':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.KodeTrans = 'H1' AND ttbmhd.BMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-umum':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.KodeTrans = 'UM' AND ttbmhd.BMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-dari-kas':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.KodeTrans = 'KK' AND ttbmhd.BMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-service-invoice':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.KodeTrans = 'SV' AND ttbmhd.BMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-penjualan':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.KodeTrans = 'SI' AND ttbmhd.BMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-estimasi':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.KodeTrans = 'SE' AND ttbmhd.BMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-order-penjualan':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.KodeTrans = 'SO' AND ttbmhd.BMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-retur-pembelian':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.KodeTrans = 'PR' AND ttbmhd.BMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-klaim-kpb':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.KodeTrans = 'CK' AND ttbmhd.BMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-klaim-c2':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.KodeTrans = 'CC' AND ttbmhd.BMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/order':
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$KodeTrans = $urlParams[ 'KodeTrans' ];
					switch ( $KodeTrans ) {
						case 'SE':
							$sqlraw  = "SELECT ttsehd.seNo AS NO, ttsehd.seTgl AS Tgl, tdcustomer.MotorNoPolisi AS Kode, tdcustomer.CusNama AS Nama,
       						tdcustomer.MotorNama, SETotalBiaya AS Total, IFNULL(KBPaid.Paid, 0) AS Terbayar, SETotalBiaya - IFNULL(KBPaid.Paid, 0) AS Sisa, 0.00 AS Bayar 
                            FROM ttsehd 
                            INNER JOIN tdcustomer ON ttsehd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                            LEFT OUTER JOIN 
                            (SELECT KBMLINK, IFNULL(SUM(KBMBayar), 0) AS Paid FROM vtkbm WHERE LEFT(KBMLink, 2) = 'SE' GROUP BY KBMLINK) KBPaid 
                            ON seNo = KBPaid.KBMLINK 
                            WHERE (SETotalBiaya - IFNULL(KBPaid.Paid, 0) > 0)";
							$colGrid = Ttbmhd::colGridPickEstimasiServis();
							break;
						case 'SV':
							$sqlraw  = "SELECT ttSDhd.SVNo AS NO, ttSDhd.SVTgl AS Tgl, tdcustomer.MotorNoPolisi AS Kode, tdcustomer.CusNama AS Nama, tdcustomer.MotorNama, 
                            SDTotalBiaya AS Total, IFNULL(KBPaid.Paid, 0) AS Terbayar, SDTotalBiaya - IFNULL(KBPaid.Paid, 0) AS Sisa, 0.00 AS Bayar 
                            FROM ttSDhd 
                            INNER JOIN tdcustomer  ON ttSDhd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                            LEFT OUTER JOIN 
                            (SELECT KBMLINK, IFNULL(SUM(KBMBayar), 0) AS Paid FROM vtkbm WHERE LEFT(KBMLink, 2) = 'SV' GROUP BY KBMLINK) KBPaid 
                            ON SVNo = KBPaid.KBMLINK 
                            WHERE SVNo <> '--' AND (SDTotalBiaya - IFNULL(KBPaid.Paid, 0) > 0)";
							$colGrid = Ttbmhd::colGridPickServisInvoice();
							break;
						case 'SI':
							$sqlraw  = "SELECT ttSIhd.SINo AS NO, ttSIhd.SITgl AS Tgl, tdcustomer.MotorNoPolisi AS Kode, tdcustomer.CusNama AS Nama, tdcustomer.MotorNama, 
       						SITotal AS Total, IFNULL(KBPaid.Paid, 0) AS Terbayar, SITotal - IFNULL(KBPaid.Paid, 0) AS Sisa, 0.00 AS Bayar 
                            FROM ttSIhd 
                            INNER JOIN tdcustomer ON ttSIhd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                            LEFT OUTER JOIN 
                            (SELECT KBMLINK, IFNULL(SUM(KBMBayar), 0) AS Paid FROM vtkbm WHERE LEFT(KBMLink, 2) = 'SI' GROUP BY KBMLINK) KBPaid 
                            ON SINo = KBPaid.KBMLINK 
                            WHERE (SITotal - IFNULL(KBPaid.Paid, 0) > 0) ";
							$colGrid = Ttbmhd::colGridPickSalesInvoice();
							break;
						case 'SO':
							$sqlraw  = "SELECT ttsohd.SONo AS NO, ttsohd.SOTgl AS Tgl, tdcustomer.MotorNoPolisi AS Kode, tdcustomer.CusNama AS Nama, tdcustomer.MotorNama, 
       						SOTotal AS Total, IFNULL(KBPaid.Paid, 0) AS Terbayar, SOTotal - IFNULL(KBPaid.Paid, 0) AS Sisa, 0.00 AS Bayar 
                            FROM ttsohd 
                            INNER JOIN tdcustomer ON ttsohd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                            LEFT OUTER JOIN 
                            (SELECT KBMLINK, IFNULL(SUM(KBMBayar), 0) AS Paid FROM vtkbm WHERE LEFT(KBMLink, 2) = 'SO' GROUP BY KBMLINK) KBPaid 
                            ON SONo = KBPaid.KBMLINK 
                            WHERE (SOTotal - IFNULL(KBPaid.Paid, 0) > 0)";
							$colGrid = Ttbmhd::colGridPickSalesOrder();
							break;
						case 'PR':
							$sqlraw  = "SELECT ttPRhd.PRNo AS NO, ttPRhd.PRTgl AS Tgl, tdsupplier.SupKode AS Kode, tdsupplier.SupNama AS Nama, LokasiKode AS MotorNama, 
       						PRTotal AS Total, IFNULL(KBPaid.Paid, 0) AS Terbayar, PRTotal - IFNULL(KBPaid.Paid, 0) AS Sisa, 0.00 AS Bayar 
                            FROM ttPRhd 
                            INNER JOIN tdsupplier ON ttPRhd.SupKode = tdsupplier.SupKode 
                            LEFT OUTER JOIN 
                            (SELECT KBMLINK, IFNULL(SUM(KBMBayar), 0) AS Paid FROM vtkbm WHERE LEFT(KBMLink, 2) = 'PR' GROUP BY KBMLINK) KBPaid 
                            ON PRNo = KBPaid.KBMLINK 
                            WHERE (PRTotal - IFNULL(KBPaid.Paid, 0) > 0)";
							$colGrid = Ttbmhd::colGridPickSalesRetur();
							break;
					}
					$queryRawPK = [ 'No' ];
					break;
			}
		}
		return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Ttbmhd::class,
				'queryRaw'   => $sqlraw,
				'queryRawPK' => $queryRawPK,
				'colGrid'    => $colGrid,
				'joinWith'   => $joinWith,
				'join'       => $join,
				'where'      => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionBayarOtomatis() {
		$Nominal          = $_GET[ 'NominalBayar' ];
		$this->layout     = 'popup-form';
		$BankKode         = Traccount::find()->where( "LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'" )
		                             ->orderBy( 'NoAccount' )->one();
		$model            = new Ttbmhd();
		$model->BMNo      = General::createTemporaryNo( 'BM', 'Ttbmhd', 'BMNo' );
		$model->BMTgl     = date( 'Y-m-d H:i:s' );
		$model->BMNominal = $Nominal;
		$model->BankKode  = ( $BankKode != null ) ? $BankKode->NoAccount : '';
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttbmhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang                = Ttbmhd::find()->ttbmhdFillGetData()->where( [ 'ttbmhd.BMNo' => $model->BMNo ] )->asArray( true )->one();
		$dsTUang[ 'CaraBayar' ] = '';
		$dsTUang[ 'NoKartu' ]   = '';
		$id                     = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'BMNoView' ]  = $result[ 'BMNo' ];
		return $this->render( '_formBayar', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionLoopBankMasuk() {
		$_POST[ 'Action' ]     = 'Loop';
		$result                = Ttbmhd::find()->callSP( $_POST );
		if($result[ 'status' ] == 1){
			Yii::$app->session->addFlash( 'warning', $result[ 'keterangan' ] );
		}
        $postItem[ 'Action' ]  = 'Insert';
        $postItem[ 'BMNo' ]    = $result[ 'BMNo' ];
        $postItem[ 'BMLink' ]  = $_POST[ 'BMLink' ];
        $postItem[ 'BMBayar' ] = $_POST[ 'BMNominal' ];
        $resultItem            = Ttbmit::find()->callSP( $postItem );
		if($resultItem[ 'status' ] == 1){
			Yii::$app->session->addFlash( 'warning', $resultItem[ 'keterangan' ] );
		}
		$_POST[ 'Action' ]    = 'LoopAfter';
        $resultAfter                = Ttbmhd::find()->callSP( $_POST );
        if($resultAfter[ 'status' ] == 1){
            Yii::$app->session->addFlash( 'warning', $resultAfter[ 'keterangan' ] );
        }
	}
	public function actionOrder( $KodeTrans ) {
		$this->layout = 'select-mode';
		switch ( $KodeTrans ) {
			case 'SV':
				return $this->render( 'order',
					[
						'arr'     => [
							'cmbTxt'    => [
								'CusNama'       => 'Nama Konsumen',
								'NO'            => 'No Servis',
//								'SVNo'          => 'No Servis',
								'MotorNoPolisi' => 'No Polisi',
								'MotorNama'     => 'Nama Motor',
							],
							'cmbTgl'    => [
								'Tgl' => 'Tgl Servis',
							],
							'cmbNum'    => [],
							'sortname'  => "Tgl",
							'sortorder' => 'desc'
						],
						'options' => [ 'colGrid' => Ttbmhd::colGridPickServisInvoice(), 'mode' => self::MODE_SELECT_MULTISELECT ]
					] );
				break;
			case 'SI':
				return $this->render( 'order',
					[
						'arr'     => [
							'cmbTxt'    => [
								'CusNama'       => 'Nama Konsumen',
								'SINo'          => 'No Jual',
								'MotorNoPolisi' => 'No Polisi',
								'MotorNama'     => 'Nama Motor',
							],
							'cmbTgl'    => [
								'Tgl' => 'Tgl Jual',
							],
							'cmbNum'    => [
								'SITotal' => 'Total',
							],
							'sortname'  => "Tgl",
							'sortorder' => 'desc'
						],
						'options' => [ 'colGrid' => Ttbmhd::colGridPickSalesInvoice(), 'mode' => self::MODE_SELECT_MULTISELECT ]
					] );
				break;
			case 'SE':
				return $this->render( 'order',
					[
						'arr'     => [
							'cmbTxt'    => [
								'CusNama'       => 'Nama Konsumen',
								'SONo'          => 'No Jual',
								'MotorNoPolisi' => 'No Polisi',
								'MotorNama'     => 'Nama Motor',
							],
							'cmbTgl'    => [
								'Tgl' => 'Tgl Estimatis',
							],
							'cmbNum'    => [
								'SETotalBiaya' => 'Total',
							],
							'sortname'  => "Tgl",
							'sortorder' => 'desc'
						],
						'options' => [ 'colGrid' => Ttbmhd::colGridPickEstimasiServis(), 'mode' => self::MODE_SELECT_MULTISELECT ]
					] );
				break;
			case 'SO':
				return $this->render( 'order',
					[
						'arr'     => [
							'cmbTxt'    => [
								'CusNama'       => 'Nama Konsumen',
								'SONo'          => 'No Jual',
								'MotorNoPolisi' => 'No Polisi',
								'MotorNama'     => 'Nama Motor',
							],
							'cmbTgl'    => [
								'Tgl' => 'Tgl Jual',
							],
							'cmbNum'    => [
								'SOTotal' => 'Total',
							],
							'sortname'  => "Tgl",
							'sortorder' => 'desc'
						],
						'options' => [ 'colGrid' => Ttbmhd::colGridPickSalesOrder(), 'mode' => self::MODE_SELECT_MULTISELECT ]
					] );
				break;
			case 'PR':
				return $this->render( 'order',
					[
						'arr'     => [
							'cmbTxt'    => [
								'SupKode'    => 'Kode Supplier',
								'PRNo'       => 'No Retur Beli',
								'SupNama'    => 'Nama Supplier',
								'LokasiKode' => 'Lokasi',
							],
							'cmbTgl'    => [
								'Tgl' => 'Tgl Retur Beli',
							],
							'cmbNum'    => [
								'PRTotal' => 'Total',
							],
							'sortname'  => "Tgl",
							'sortorder' => 'desc'
						],
						'options' => [ 'colGrid' => Ttbmhd::colGridPickSalesRetur(), 'mode' => self::MODE_SELECT_MULTISELECT ]
					] );
				break;
		}
	}
	public function actionBankMasukDariH1() {
		$post[ 'Action' ] = 'Display';
		$result           = Ttbmhd::find()->callSP( $post );
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankMasukDariH1', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbmhd/td' ] )
		] );
	}
	public function actionBankMasukDariH1Create() {
		$BankKode             = Traccount::find()->where( "LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'" )
		                                 ->orderBy( 'NoAccount' )->one();
		$model                = new Ttbmhd();
		$model->BMNo          = General::createTemporaryNo( 'BM', 'Ttbmhd', 'BMNo' );
		$model->BMTgl         = date( 'Y-m-d H:i:s' );
		$model->BMCekTempo    = date( 'Y-m-d H:i:s' );
		$model->BMPerson      = "--";
		$model->BMMemo        = "--";
		$model->BMNominal     = 0;
		$model->BMJenis       = "Tunai";
		$model->PosKode       = $this->LokasiKode();
		$model->UserID        = $this->UserID();
		$model->KodeTrans     = "H1";
		$model->BankKode      = ( $BankKode != null ) ? $BankKode->NoAccount : '';
		$model->BMCekNo       = "--";
		$model->BMAC          = "--";
		$model->MotorNoPolisi = "--";
		$model->NoGL          = "--";
		$model->Cetak         = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttbmhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttbmhd::find()->ttbmhdFillGetData()->where( [ 'ttbmhd.BMNo' => $model->BMNo ] )->asArray( true )->one();
		$dsTUang[ 'BMLink' ]   = '';
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'BMNoView' ] = $result[ 'BMNo' ];
		return $this->render( 'bank-masuk-dari-h1-create', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionBankMasukDariH1Update( $id ) {
		$index = Ttbmhd::getRoute( 'H1', [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		/** @var Ttbmhd $model */
		$BankKode = Traccount::find()->where( "LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'" )
		                     ->orderBy( 'NoAccount' )->one();
		$model    = $this->findModelBase64( 'Ttbmhd', $id );
		$dsTUang  = Ttbmhd::find()->ttbmhdFillGetData()->where( [ 'ttbmhd.BMNo' => $model->BMNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post             = \Yii::$app->request->post();
            $post[ 'BMTgl' ]   = $post[ 'BMTgl' ] . ' ' . $post[ 'BMJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttbmhd::findOne( $result[ 'BMNo' ] );
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'BMNoView' ] = $result[ 'BMNo' ];
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				return $this->redirect( Ttbmhd::getRoute( 'H1', [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'bank-masuk-dari-h1-update', [
					'model'   => $model,
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttbmhd::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'BMNo' ] = $dsTUang[ 'BMNo' ];
		}
		$ttkkit                = Ttbmit::find()->where( [ 'BMNo' => $model->BMNo ] )->one();
		$dsTUang[ 'BMLink' ]   = ( $ttkkit == null ) ? '' : $ttkkit->BMLink;
		$dsTUang[ 'BMNoView' ] = $_GET[ 'BMNoView' ] ?? $result[ 'BMNo' ];
//        $dsTUang[ 'BMTgl' ]   = date( 'Y-m-d H:i:s' );
		return $this->render( 'bank-masuk-dari-h1-update', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionBankMasukUmum() {
		$post[ 'Action' ] = 'Display';
		$result           = Ttbmhd::find()->callSP( $post );
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankMasukUmum', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbmhd/td' ] )
		] );
	}
	public function actionBankMasukUmumCreate() {
		$BankKode          = Traccount::find()->where( "LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'" )
		                              ->orderBy( 'NoAccount' )->one();
		$model             = new Ttbmhd();
		$model->BMNo       = General::createTemporaryNo( 'BM', 'Ttbmhd', 'BMNo' );
		$model->BMTgl      = date( 'Y-m-d H:i:s' );
		$model->BMCekTempo = date( 'Y-m-d H:i:s' );
		$model->BMPerson   = "--";
		$model->BMMemo     = "--";
		$model->BMNominal  = 0;
		$model->BMJenis    = "Tunai";
		$model->PosKode    = $this->LokasiKode();
		$model->UserID     = $this->UserID();
		$model->KodeTrans  = "UM";
		$model->BankKode   = ( $BankKode != null ) ? $BankKode->NoAccount : '';;
		$model->BMCekNo       = "--";
		$model->BMAC          = "--";
		$model->MotorNoPolisi = "--";
		$model->NoGL          = "--";
		$model->Cetak         = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttbmhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttbmhd::find()->ttbmhdFillGetData()->where( [ 'ttbmhd.BMNo' => $model->BMNo ] )->asArray( true )->one();
		$dsTUang[ 'BMLink' ]   = '';
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'BMNoView' ] = $result[ 'BMNo' ];
		return $this->render( 'bank-masuk-umum-create', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionBankMasukUmumUpdate( $id ) {
		$index = Ttbmhd::getRoute( 'UM', [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}

		/** @var Ttbmhd $model */
		$BankKode = Traccount::find()->where( "LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'" )
		                     ->orderBy( 'NoAccount' )->one();
		$model    = $this->findModelBase64( 'Ttbmhd', $id );
		$dsTUang  = Ttbmhd::find()->ttbmhdFillGetData()->where( [ 'ttbmhd.BMNo' => $model->BMNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post             = \Yii::$app->request->post();
			$post[ 'Action' ] = 'Update';
			$result           = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model = Ttbmhd::findOne( $result[ 'BMNo' ] );
			$id    = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				$post[ 'BMNoView' ] = $result[ 'BMNo' ];
				return $this->redirect( Ttbmhd::getRoute( 'UM', [ 'id' => $id, 'action' => 'display' ] ) [ 'update' ] );
			} else {
				return $this->render( 'bank-masuk-umum-update', [
					'model'   => $model,
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttbmhd::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'BMNo' ] = $dsTUang[ 'BMNo' ];
		}
		$ttkkit                = Ttbmit::find()->where( [ 'BMNo' => $model->BMNo ] )->one();
		$dsTUang[ 'BMLink' ]   = ( $ttkkit == null ) ? '' : $ttkkit->BMLink;
		$dsTUang[ 'BMNoView' ] = $_GET[ 'BMNoView' ] ?? $result[ 'BMNo' ];
		return $this->render( 'bank-masuk-umum-update', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionBankMasukDariKas() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankMasukDariKas', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbmhd/td' ] )
		] );
	}
	public function actionBankMasukDariKasCreate() {
		$BankKode          = Traccount::find()->where( "LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'" )
		                              ->orderBy( 'NoAccount' )->one();
		$model             = new Ttbmhd();
		$model->BMNo       = General::createTemporaryNo( 'BM', 'Ttbmhd', 'BMNo' );
		$model->BMTgl      = date( 'Y-m-d H:i:s' );
		$model->BMCekTempo = date( 'Y-m-d H:i:s' );
		$model->BMPerson   = "--";
		$model->BMMemo     = "--";
		$model->BMNominal  = 0;
		$model->BMJenis    = "Tunai";
		$model->PosKode    = $this->LokasiKode();
		$model->UserID     = $this->UserID();
		$model->KodeTrans  = "KK";
		$model->BankKode   = ( $BankKode != null ) ? $BankKode->NoAccount : '';;
		$model->BMCekNo       = "--";
		$model->BMAC          = "--";
		$model->MotorNoPolisi = "--";
		$model->NoGL          = "--";
		$model->Cetak         = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttbmhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttbmhd::find()->ttbmhdFillGetData()->where( [ 'ttbmhd.BMNo' => $model->BMNo ] )->asArray( true )->one();
		$dsTUang[ 'BMLink' ]   = '';
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'BMNoView' ] = $result[ 'BMNo' ];
		return $this->render( 'bank-masuk-dari-kas-create', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionBankMasukDariKasUpdate( $id ) {
		$index = Ttbmhd::getRoute( 'KK', [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		/** @var Ttbmhd $model */
		$BankKode = Traccount::find()->where( "LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'" )
		                     ->orderBy( 'NoAccount' )->one();
		$model    = $this->findModelBase64( 'Ttbmhd', $id );
		$dsTUang  = Ttbmhd::find()->ttbmhdFillGetData()->where( [ 'ttbmhd.BMNo' => $model->BMNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post             = \Yii::$app->request->post();
			$isNew = ( strpos( $post['BMNo'], '=' ) !== false);
			$post[ 'BMTgl' ]  = $post[ 'BMTgl' ] . ' ' . $post[ 'BMJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			if ( $result[ 'status' ] == 0 ) {
				$postit[ 'Action' ]  = $isNew ? 'Insert' : 'Update';
				$postit[ 'BMNo' ]    = $post[ 'BMNo' ];
				$postit[ 'BMBayar' ] = $post[ 'BMNominal' ];
				$postit[ 'BMLink' ]  = $post[ 'BMLink' ];
				$resultit            = Ttbmit::find()->callSP( $postit );
				if ( $resultit[ 'status' ] == 1 ) {
					Yii::$app->session->addFlash( 'warning', $resultit[ 'keterangan' ] );
				}
			}
			$model              = Ttbmhd::findOne( $result[ 'BMNo' ] );
			$id = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				$post[ 'BMNoView' ] = $result[ 'BMNo' ];
				return $this->redirect( Ttbmhd::getRoute( 'KK', [ 'id' => $id, 'action' => 'display' ] ) [ 'update' ] );
			} else {
				return $this->render( 'bank-masuk-dari-kas-update', [
					'model'   => $model,
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttbmhd::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'BMNo' ] = $dsTUang[ 'BMNo' ];
		}
		$ttkkit                = Ttbmit::find()->where( [ 'BMNo' => $model->BMNo ] )->one();
		$dsTUang[ 'BMLink' ]   = ( $ttkkit == null ) ? '' : $ttkkit->BMLink;
		$dsTUang[ 'BMNoView' ] = $_GET[ 'BMNoView' ] ?? $result[ 'BMNo' ];
        $dsTUang[ 'BMTgl' ]    = date( 'Y-m-d H:i:s' );
		return $this->render( 'bank-masuk-dari-kas-update', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionBankMasukServiceInvoice() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankMasukServiceInvoice', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbmhd/td' ] )
		] );
	}
	public function actionBankMasukServiceInvoiceCreate() {
		return $this->create( 'SV', 'bank-masuk-service-invoice', 'BANK MASUK - Servis' );
	}
	protected function create( $BMJenis, $view, $title ) {
		$BankKode             = Traccount::find()->where( "LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'" )
		                                 ->orderBy( 'NoAccount' )->one();
		$model                = new Ttbmhd();
		$model->BMNo          = General::createTemporaryNo( 'BM', 'Ttbmhd', 'BMNo' );
		$model->BMTgl         = date( 'Y-m-d H:i:s' );
		$model->BMCekTempo    = date( 'Y-m-d H:i:s' );
		$model->BMPerson      = "--";
		$model->BMMemo        = "--";
		$model->BMNominal     = 0;
		$model->BMJenis       = "Tunai";
		$model->PosKode       = $this->LokasiKode();
		$model->UserID        = $this->UserID();
		$model->KodeTrans     = $BMJenis;
		$model->BankKode      = ( $BankKode != null ) ? $BankKode->NoAccount : '';
		$model->BMCekNo       = "--";
		$model->BMAC          = "--";
		$model->MotorNoPolisi = "--";
		$model->NoGL          = "--";
		$model->Cetak         = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttbmhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttbmhd::find()->ttbmhdFillGetData()->where( [ 'ttbmhd.BMNo' => $model->BMNo ] )->asArray( true )->one();
		$dsTUang[ 'BMLink' ]   = '';
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'BMNoView' ] = $result[ 'BMNo' ];
		return $this->render( 'create', [
			'model'   => $model,
			'view'    => $view,
			'title'   => $title,
			'dsTUang' => $dsTUang,
			'id'      => $id,
		] );
	}
	public function actionBankMasukServiceInvoiceUpdate( $id ) {
		return $this->update( 'SV', 'bank-masuk-service-invoice', 'BANK MASUK - Servis', $id );
	}
	protected function update( $BMJenis, $view, $title, $id ) {
		$index = Ttbmhd::getRoute( $BMJenis, [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		/** @var Ttbmhd $model */
		$BankKode = Traccount::find()->where( "LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'" )
		                     ->orderBy( 'NoAccount' )->one();
		$model    = $this->findModelBase64( 'Ttbmhd', $id );
		$dsTUang  = Ttbmhd::find()->ttbmhdFillGetData()->where( [ 'ttbmhd.BMNo' => $model->BMNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post                = \Yii::$app->request->post();
			$post[ 'BMTgl' ]     = $post[ 'BMTgl' ] . ' ' . $post[ 'BMJam' ];
			$post[ 'KodeTrans' ] = $BMJenis;
			$post[ 'Action' ]    = 'Update';
			$result              = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttbmhd::findOne( $result[ 'BMNo' ] );
			$id = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 ) {
				$post[ 'BMNoView' ] = $result[ 'BMNo' ];
				return $this->redirect( Ttbmhd::getRoute( $BMJenis, [ 'id' => $id, 'action' => 'display' ] ) [ 'update' ] );
			} else {
				return $this->render( 'bank-masuk-dari-h1-update', [
					'model'   => $model,
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttbmhd::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'BMNo' ] = $dsTUang[ 'BMNo' ];
		}
		$ttkkit                = Ttbmit::find()->where( [ 'BMNo' => $model->BMNo ] )->one();
		$dsTUang[ 'BMLink' ]   = ( $ttkkit == null ) ? '' : $ttkkit->BMLink;
		$dsTUang[ 'BMNoView' ] = $_GET[ 'BMNoView' ] ?? $result[ 'BMNo' ];
		return $this->render( 'update', [
			'model'   => $model,
			'view'    => $view,
			'title'   => $title,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionBankMasukPenjualan() {
		$post[ 'Action' ] = 'Display';
		$result           = Ttbmhd::find()->callSP( $post );
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankMasukPenjualan', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbmhd/td' ] )
		] );
	}
	public function actionBankMasukPenjualanCreate() {
		return $this->create( 'SI', 'bank-masuk-penjualan', 'BANK MASUK - Penjualan' );
	}
	public function actionBankMasukPenjualanUpdate( $id ) {
		return $this->update( 'SI', 'bank-masuk-penjualan', 'BANK MASUK - Penjualan', $id );
	}
	public function actionBankMasukEstimasi() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankMasukEstimasi', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbmhd/td' ] )
		] );
	}
	public function actionBankMasukEstimasiCreate() {
		return $this->create( 'SE', 'bank-masuk-estimasi', 'BANK MASUK - Estimasi Servis' );
	}
	public function actionBankMasukEstimasiUpdate( $id ) {
		return $this->update( 'SE', 'bank-masuk-estimasi', 'BANK MASUK - Estimasi Servis', $id );
	}
	public function actionBankMasukOrderPenjualan() {
		$post[ 'Action' ] = 'Display';
		$result           = Ttbmhd::find()->callSP( $post );
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankMasukOrderPenjualan', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbmhd/td' ] )
		] );
	}
	public function actionBankMasukOrderPenjualanCreate() {
		return $this->create( 'SO', 'bank-masuk-order-penjualan', 'BANK MASUK - Order Penjualan' );
	}
	public function actionBankMasukOrderPenjualanUpdate( $id ) {
		return $this->update( 'SO', 'bank-masuk-order-penjualan', 'BANK MASUK - Order Penjualan', $id );
	}
	public function actionBankMasukReturPembelian() {
		$post[ 'Action' ] = 'Display';
		$result           = Ttbmhd::find()->callSP( $post );
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankMasukReturPembelian', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbmhd/td' ] )
		] );
	}
	public function actionBankMasukReturPembelianCreate() {
		return $this->create( 'PR', 'bank-masuk-retur-pembelian', 'BANK MASUK - Retur Pembelian' );
	}
	public function actionBankMasukReturPembelianUpdate( $id ) {
		return $this->update( 'PR', 'bank-masuk-retur-pembelian', 'BANK MASUK - Retur Pembelian', $id );
	}
	public function actionBankMasukKlaimKpb() {
		$post[ 'Action' ] = 'Display';
		$result           = Ttbmhd::find()->callSP( $post );
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankMasukKlaimKpb', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbmhd/td' ] )
		] );
	}
	public function actionBankMasukKlaimKpbCreate() {
		return $this->BmKlaimCreate( 'CK', 'bank-masuk-klaim-kpb', 'BANK MASUK - Klaim KPB' );
	}
	protected function bmKlaimCreate( $BMJenis, $view, $title ) {
		$BankKode             = Traccount::find()->where( "LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'" )
		                                 ->orderBy( 'NoAccount' )->one();
		$model                = new Ttbmhd();
		$model->BMNo          = General::createTemporaryNo( 'BM', 'Ttbmhd', 'BMNo' );
		$model->BMTgl         = date( 'Y-m-d H:i:s' );
		$model->BMCekTempo    = date( 'Y-m-d H:i:s' );
		$model->BMPerson      = "--";
		$model->BMMemo        = "--";
		$model->BMNominal     = 0;
		$model->BMJenis       = "Tunai";
		$model->PosKode       = $this->LokasiKode();
		$model->UserID        = $this->UserID();
		$model->KodeTrans     = $BMJenis;
		$model->BankKode      = ( $BankKode != null ) ? $BankKode->NoAccount : '';
		$model->BMCekNo       = "--";
		$model->BMAC          = "--";
		$model->MotorNoPolisi = "--";
		$model->NoGL          = "--";
		$model->Cetak         = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttbmhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttbmhd::find()->ttbmhdFillGetData()->where( [ 'ttbmhd.BMNo' => $model->BMNo ] )->asArray( true )->one();
		$ttkkit                = Ttbmit::find()->where( [ 'BMNo' => $model->BMNo ] )->one();
		$dsTUang[ 'BMLink' ]   = ( $ttkkit == null ) ? '' : $ttkkit->BMLink;
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'BMNoView' ] = $result[ 'BMNo' ];
		return $this->render( 'bm-klaim-create', [
			'model'   => $model,
			'view'    => $view,
			'title'   => $title,
			'dsTUang' => $dsTUang,
			'id'      => $id,
		] );
	}
	public function actionBankMasukKlaimKpbUpdate( $id ) {
		return $this->bmKlaimUpdate( 'CK', 'bank-masuk-klaim-kpb', 'BANK MASUK - Klaim KPB', $id );
	}
	protected function bmKlaimUpdate( $BMJenis, $view, $title, $id ) {
		$index = Ttbmhd::getRoute( $BMJenis, [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$BankKode            = Traccount::find()->where( "LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'" )
		                                ->orderBy( 'NoAccount' )->one();
		$model               = $this->findModelBase64( 'Ttbmhd', $id );
		$dsTUang             = Ttbmhd::find()->ttbmhdFillGetData()->where( [ 'ttbmhd.BMNo' => $model->BMNo ] )->asArray()->one();
		$dsTUang             = Ttbmhd::find()->ttbmhdFillGetData()->where( [ 'ttbmhd.BMNo' => $model->BMNo ] )->asArray( true )->one();
		$ttkkit              = Ttbmit::find()->where( [ 'BMNo' => $model->BMNo ] )->one();
		$dsTUang[ 'BMLink' ] = ( $ttkkit == null ) ? '' : $ttkkit->BMLink;
		if ( Yii::$app->request->isPost ) {
			$post = \Yii::$app->request->post();
			$post[ 'BMTgl' ] = $post[ 'BMTgl' ] . ' ' . $post[ 'BMJam' ];
			$post[ 'KodeTrans' ] = $BMJenis;
			$post[ 'Action' ]    = 'Update';
			$result              = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttbmhd::findOne( $result[ 'BMNo' ] );
			$id                 = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 ) {
				$post[ 'BMNoView' ] = $result[ 'BMNo' ];
				return $this->redirect( Ttbmhd::getRoute( $BMJenis, [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'bm-klaim-update', [
					'model'   => $model,
					'view'    => $view,
					'title'   => $title,
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttbmhd::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'BMNo' ] = $dsTUang[ 'BMNo' ];
		}
		$ttbmit                = Ttbmit::find()->where( [ 'BMNo' => $model->BMNo ] )->one();
		$dsTUang[ 'BMLink' ]   = ( $ttbmit == null ) ? '' : $ttkkit->BMLink;
		$dsTUang[ 'Total' ]    = 0;
		$dsTUang[ 'BMNoView' ] = $_GET[ 'BMNoView' ] ?? $result[ 'BMNo' ];
		return $this->render( 'bm-klaim-update', [
			'model'   => $model,
			'view'    => $view,
			'title'   => $title,
			'dsTUang' => $dsTUang,
			'id'      => $id,
		] );
	}
	public function actionBankMasukKlaimC2() {
		$post[ 'Action' ] = 'Display';
		$result           = Ttbmhd::find()->callSP( $post );
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankMasukKlaimC2', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbmhd/td' ] )
		] );
	}
	public function actionBankMasukKlaimC2Create() {
		return $this->BmKlaimCreate( 'CC', 'bank-masuk-klaim-c2', 'BANK MASUK - Klaim CC' );
	}
	public function actionBankMasukKlaimC2Update( $id ) {
		return $this->bmKlaimUpdate( 'CC', 'bank-masuk-klaim-c2', 'BANK MASUK - Klaim CC', $id );
	}
    public function actionDelete() {
        /** @var Ttbmhd $model */
        $request = Yii::$app->request;
        $PK         = base64_decode( $_REQUEST[ 'id' ] );
        $model      = null;
        $model             = Ttbmhd::findOne( $PK );
        $_POST[ 'BMNo' ]   = $model->BMNo;
        $_POST[ 'Action' ] = 'Delete';
        $BMJenis = $model->KodeTrans;
        $result = Ttbmhd::find()->callSP( $_POST );
        if ($request->isAjax){
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['BMNo'] == '--') {
                return $this->redirect( [ 'ttbmhd/pengajuan-bayar-bbn-bank' ] );
            } else {
                $model = Ttbmhd::findOne($result['BMNo']);
                return $this->redirect( Ttbmhd::getRoute( $BMJenis, [ 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] )[ 'update' ] );
            }
        }
    }

	public function actionCancel( $id, $action ) {
		/** @var Ttbmhd $model */
		$model             = $this->findModelBase64( 'Ttbmhd', $id );
		$KodeTrans         = $model->KodeTrans;
		$_POST[ 'BMNo' ]   = $model->BMNo;
		$_POST[ 'Action' ] = 'Cancel';
		$post              = \Yii::$app->request->post();
		$post[ 'BMTgl' ]   = $post[ 'BMTgl' ] . ' ' . $post[ 'BMJam' ];
		$result            = Ttbmhd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'BMNo' ] == '--' ) {
			return $this->redirect( Ttbmhd::getRoute( $KodeTrans, [ 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] )[ 'index' ] );
		} else {
			$model = Ttbmhd::findOne( $result[ 'BMNo' ] );
			return $this->redirect( Ttbmhd::getRoute( $KodeTrans, [ 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] )[ 'update' ] );
		}
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST         = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client        = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response      = $client->post( Yii::$app->params['H2'][$_POST['db']]['host_report'] . '/abengkel/fbmhd', [
			'body' => Json::encode( $_POST )
		] );
        $html                 = $response->getBody();
        return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	protected function findModel( $id ) {
		if ( ( $model = Ttbmhd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}

    public function actionJurnal() {
        $post             = \Yii::$app->request->post();
        $post[ 'Action' ] = 'Jurnal';
        $result           = Ttbmhd::find()->callSP( $post );
        Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        return $this->redirect( Ttbmhd::getRoute( $post[ 'KodeTrans' ], [ 'action' => 'display', 'id' => base64_encode( $result[ 'BMNo' ] ) ] )[ 'update' ] );
    }
}
