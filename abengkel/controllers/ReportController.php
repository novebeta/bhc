<?php
namespace abengkel\controllers;
use abengkel\components\Menu;
use abengkel\components\AbengkelController;
use abengkel\models\Tdbarang;
use abengkel\models\Tdbaranglokasi;
use abengkel\models\Ttbkhd;
use abengkel\models\Ttbmhd;
use abengkel\models\Ttcchd;
use abengkel\models\Ttckhd;
use abengkel\models\Ttgeneralledgerhd;
use abengkel\models\Ttkkhd;
use abengkel\models\Ttkmhd;
use abengkel\models\Ttpihd;
use abengkel\models\Ttpohd;
use abengkel\models\Ttprhd;
use abengkel\models\Ttpshd;
use abengkel\models\Ttsdhd;
use abengkel\models\Ttsehd;
use abengkel\models\Ttsihd;
use abengkel\models\Ttsohd;
use abengkel\models\Ttsrhd;
use abengkel\models\Tttahd;
use abengkel\models\Tttihd;
use GuzzleHttp\Client;
use Yii;
use yii\helpers\Json;
use yii\web\RangeNotSatisfiableHttpException;
class ReportController extends AbengkelController {
	const MIME = [
		'pdf' => 'application/pdf',
		'xls' => 'application/vnd.ms-excel',
		'doc' => 'application/msword',
		'rtf' => 'application/rtf',
	];
	protected function prosesLaporan( $target, $view ) {
		if ( isset( $_POST['tipe'] ) ) {
			unset( $_POST['_csrf-app'] );
			$_POST       = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
			$_POST['db'] = base64_decode( $_COOKIE['_outlet'] );
            $_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
			$client      = new Client( [
				'headers' => [ 'Content-Type' => 'application/octet-stream' ]
			] );
			$response    = $client->post(Yii::$app->params['H2'][$_POST['db']]['host_report'] . '/' . $target, [
				'body' => Json::encode( $_POST )
			] );
			try {
				if ( $_POST[ 'filetype' ] == 'xlsr' ) {
					$_POST[ 'filetype' ] = 'xls';
				}
				return Yii::$app->response->sendContentAsFile(
					$response->getBody(),
					$_POST[ 'tipe' ] . '.' . $_POST[ 'filetype' ],
					[
						'inline'   => true,
						'mimeType' => $this::MIME[ $_POST[ 'filetype' ] ]
					]
				);
			} catch ( RangeNotSatisfiableHttpException $e ) {
			}
		}
		unset( $_POST );
		return $this->render( $view );
	}

	public function actionDaftarServisDraft() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttsdhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rServisDraft', '@abengkel/views/report/Servis/DaftarServisDraft' );
	}
	public function actionDaftarInvoiceServis() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttsdhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rServisInvoice', '@abengkel/views/report/Servis/DaftarInvoiceServis' );
	}
	public function actionDaftarEstimasiServis() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttsehd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rServisEstimasi', '@abengkel/views/report/Servis/DaftarEstimasiServis' );
	}
	public function actionDaftarKlaimKpb() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttckhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rKlaimKPB', '@abengkel/views/report/Servis/DaftarKlaimKpb' );
	}
	public function actionDaftarKlaimC2() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttcchd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rKlaimC2', '@abengkel/views/report/Servis/DaftarKlaimC2' );
	}
	public function actionDaftarPickingList() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttgeneralledgerhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rPickingList', '@abengkel/views/report/Servis/DaftarPickingList' );
	}

	public function actionDaftarOrderPenjualan() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttsohd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rPenjualanOrder', '@abengkel/views/report/Penjualan/DaftarOrderPenjualan' );
	}
	public function actionDaftarInvoicePenjualan() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttsihd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rPenjualanInvoice', '@abengkel/views/report/Penjualan/DaftarInvoicePenjualan' );
	}
	public function actionDaftarReturPenjualan() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttsrhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rPenjualanRetur', '@abengkel/views/report/Penjualan/DaftarReturPenjualan' );
	}

	public function actionDaftarOrderPembelian() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttpohd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rPembelianOrder', '@abengkel/views/report/Pembelian/DaftarOrderPembelian' );
	}
	public function actionDaftarInvoicePembelian() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttpihd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rPembelianInvoice', '@abengkel/views/report/Pembelian/DaftarInvoicePembelian' );
	}
	public function actionDaftarReturPembelian() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttprhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rPembelianRetur', '@abengkel/views/report/Pembelian/DaftarReturPembelian' );
	}
	public function actionDaftarPenerimaanBarang() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttpshd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rPenerimaanBarang', '@abengkel/views/report/Pembelian/DaftarPenerimaanBarang' );
	}

	public function actionDaftarTransfer() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Tttihd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rTransfer', '@abengkel/views/report/Mutasi/DaftarTransfer' );
	}
	public function actionDaftarPenyesuaian() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Tttahd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rPenyesuaian', '@abengkel/views/report/Mutasi/DaftarPenyesuaian' );
	}

	public function actionDaftarKas() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttkkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$result           = Ttkmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rKas', '@abengkel/views/report/Keuangan/DaftarKas' );
	}
	public function actionDaftarBank() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$result           = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rBank', '@abengkel/views/report/Keuangan/DaftarBank' );
	}

	public function actionJurnalUmum() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttgeneralledgerhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rGLJurnalUmum', '@abengkel/views/report/Akuntansi/JurnalUmum' );
	}
	public function actionNeracaPercobaan() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttgeneralledgerhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rGLNeracaPercobaan', '@abengkel/views/report/Akuntansi/NeracaPercobaan' );
	}
	public function actionNeracaRugiLaba() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttgeneralledgerhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rGLNeracaRugiLaba', '@abengkel/views/report/Akuntansi/NeracaRugiLaba' );
	}
	public function actionLaporanPiutang() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttgeneralledgerhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rGLPiutang', '@abengkel/views/report/Auditor/LaporanPiutang' );
	}
	public function actionLaporanHutang() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttgeneralledgerhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rGLHutang', '@abengkel/views/report/Auditor/LaporanHutang' );
	}

	public function actionLaporanHarian() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttgeneralledgerhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rGLHarian', '@abengkel/views/report/Auditor/LaporanHarian' );
	}

	public function actionLaporanBulanan() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttgeneralledgerhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rGLBulanan', '@abengkel/views/report/Auditor/LaporanBulanan' );
	}

	public function actionItemStock() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$result           = Tdbaranglokasi::find()->callSP();
//			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rStockItem', '@abengkel/views/report/Persediaan/ItemStock' );
	}
	public function actionKartuStock() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$result           = Tdbaranglokasi::find()->callSP();
//			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rStockKartu', '@abengkel/views/report/Persediaan/KartuStock' );
	}

	public function actionDaftarPivotTabelMekanik() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttsdhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rMekanikPivot', '@abengkel/views/report/Mekanik/DaftarPivotTabelMekanik' );
	}
	public function actionDaftarRlhMekanik() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttsihd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rMekanikRLH', '@abengkel/views/report/Mekanik/DaftarRlhMekanik' );
	}
	public function actionDaftarStartStopMekanik() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'UserID' ] = 'System';
			$result           = Ttgeneralledgerhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->prosesLaporan( 'abengkel/rMekanikStartStop', '@abengkel/views/report/Mekanik/DaftarStartStopMekanik' );
	}

	public function actionLaporanData() {
		return $this->prosesLaporan( 'abengkel/rBData', '@abengkel/views/report/Data/LaporanData' );
	}
	public function actionLaporanUserLog() {
		return $this->prosesLaporan( 'abengkel/rBUseLog', '@abengkel/views/report/Data/LaporanUserLog' );
	}
	public function actionLaporanQuery() {
		return $this->prosesLaporan( 'abengkel/rBQuery', '@abengkel/views/report/Data/LaporanQuery' );
	}
	public function actionQuery() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$post                        = Yii::$app->request->post();
		$query                       = \Yii::$app->db->createCommand(base64_decode($post['q']))->queryAll();
		$columns                     = [];
		if ( sizeof( $query ) > 0 ) {
			$columns_arr = array_keys( $query[ 0 ] );
			foreach ($columns_arr as $item){
				$columns[] = ['text'=> $item,'datafield'=>$item];
			}
		}
		return [
			'columns' => $columns,
			'rows'    => $query
		];
	}

    public function actionDashboardMarketing() {
        return $this->prosesLaporan( '', '@abengkel/views/report/Auditor/DashboardMarketing' );
    }

    public function actionDashboardAkunting() {
        return $this->prosesLaporan( '', '@abengkel/views/report/Auditor/DashboardAkunting' );
    }

    public function actionDashboardCdb()
    {
        $query = "SELECT  Korwil FROM hconsole.tddealer WHERE Korwil <> 'Foen' AND MD <> 'Anper'  GROUP BY Korwil;";
        $row = Yii::$app->db->createCommand($query)->queryAll();

        $querymd = "SELECT  MD FROM hconsole.tddealer WHERE Korwil <> 'Foen' AND Korwil <> 'Sisca'  GROUP BY MD;";
        $rowmd = Yii::$app->db->createCommand($querymd)->queryAll();

        $querydealer = "SELECT  Dealer FROM hconsole.tddealer WHERE Korwil <> 'Foen' AND MD <> 'Anper'  ;";
        $rowdealer = Yii::$app->db->createCommand($querydealer)->queryAll();
        return $this->render('@abengkel/views/report/Auditor/DashboardCdb', ['data' => $row, 'datamd' => $rowmd, 'datadealer' => $rowdealer]);
    }

    public function actionLapdashboard()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $count = $requestData;
            if ($requestData == []) {
                $tgl1 = date('Y-m-01');
                $tgl2 = date('Y-m-d');
                $eom = json_encode($requestData['eom']);
                $level = json_encode($requestData['level']);
                $korwil = json_encode($requestData['korwil']);
                $md = json_encode($requestData['md']);
                $dealer = json_encode($requestData['dealer']);
                $user = $_SESSION['_userProfile']['UserID'];
//                $user = 'Lukman';
            }
            else {
                {
                    $gettgl1 = json_encode($requestData['tgl1']);
                    $tgl1 = str_replace('"', '', $gettgl1);
                    $gettgl2 = json_encode($requestData['tgl2']);
                    $tgl2 = str_replace('"', '', $gettgl2);
                    $eom = json_encode($requestData['eom']);
                    $level = json_encode($requestData['level']);
                    $kwl = str_replace("[","",json_encode($requestData['korwil']));
                    $kor = str_replace('"',"'",$kwl);
                    $korwil = str_replace("]","",$kor);
                    $getmd = str_replace("[","",json_encode($requestData['md']));
                    $gmd = str_replace('"',"'",$getmd);
                    $md = str_replace("]","",$gmd);

                    $dlr = str_replace("[","",json_encode($requestData['dealer']));
                    $gdealer = str_replace('"',"'",$dlr);
                    $dealer = str_replace("]","",$gdealer);
                    $user = $_SESSION['_userProfile']['UserID'];
//                    $user = 'Lukman';
                }
            }

            //JASA OLI
            $queryjasaoli = "CALL rdMarketingConsol('JPOPerUnit','" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user . "'," . $level . ",'','','');";
            if($korwil === "''"){
                $queryjasaoli = "CALL rdMarketingConsol('JPOPerUnit','".$tgl1."','".$tgl2."',".$eom.",'".$user."',".$level.",'','','');";
            }
            $jasaoli = Yii::$app->db->createCommand($queryjasaoli)->queryAll();

            //PRODUK
            $queryproduk = "CALL rdMarketingConsol('Produktivitas','" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user . "'," . $level . ",'','','');";
            if($korwil === "''"){
                $queryproduk = "CALL rdMarketingConsol('Produktivitas','" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user . "'," . $level . ",'','','');";
            }
            $produk = Yii::$app->db->createCommand($queryproduk)->queryAll();

            //JASA GROUP
            $queryjasagroup = "CALL rdMarketingConsol('JasaGroup','" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user . "'," . $level . ",'','','');";
            if($korwil === "''"){
                $queryjasagroup = "CALL rdMarketingConsol('JasaGroup','" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user . "'," . $level . ",'','','');";
            }
            $jasagroup = Yii::$app->db->createCommand($queryjasagroup)->queryAll();

            //DATA BIAYA
            $querydtbiaya = "CALL rdMarketingConsol('BiayaTabel','" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user . "'," . $level . ",'','','');";
            if($korwil === "''"){
                $querydtbiaya = "CALL rdMarketingConsol('BiayaTabel','" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user . "'," . $level . ",'','','');";
            }
            $dtbiaya = Yii::$app->db->createCommand($querydtbiaya)->queryAll();

            //SALES
            $querysales = "CALL rdMarketingConsol('TargetUE','" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user . "'," . $level . ",'','','');";
            if($korwil === "''"){
                $querysales = "CALL rdMarketingConsol('TargetUE','" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user . "'," . $level . ",'','','');";
            }
            $sales = Yii::$app->db->createCommand($querysales)->queryAll();

            //PERFORMANCE
            $queryperformance = "CALL rdMarketingConsol('Performance','" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user . "'," . $level . ",'','','');";
            if($korwil === "''"){
                $queryperformance = "CALL rdMarketingConsol('Performance','" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user . "'," . $level . ",'','','');";
            }
            $performance = Yii::$app->db->createCommand($queryperformance)->queryAll();

            //DAILY TARGET
            $querydaily = "CALL rdMarketingConsol('DailyUE','" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user . "'," . $level . ",'','','');";
            if($korwil === "''"){
                $querydaily = "CALL rdMarketingConsol('DailyUE','" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user . "'," . $level . ",'','','');";
            }
            $daily = Yii::$app->db->createCommand($querydaily)->queryAll();

            //BIAYA OPERASIONAL
            $querybiaya = "CALL rdMarketingConsol('BiayaGrafik','" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'Pierre','Dealer','','','');";
            if($korwil === "''"){
                $querybiaya = "CALL rdMarketingConsol('BiayaGrafik','" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'Pierre','Dealer','','','');";
            }
            $biaya = Yii::$app->db->createCommand($querybiaya)->queryAll();

            //DAILY TARGET
            $querydaily = "CALL rdMarketingConsol('DailyUE','" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'Lukman','Dealer','','','');";
            if($korwil === "''"){
                $querydaily = "CALL rdMarketingConsol('DailyUE','" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'Lukman','Dealer','','','');";
            }

            $rowdaily = Yii::$app->db->createCommand($querydaily)->queryAll();
            foreach ($rowdaily as $key => $val) {
                $LabelDaily[] = $val['Tanggal'];
            }
            if (!isset($LabelDaily)) {
                $labeldaily = [];
            } else {
                $labeldaily = $LabelDaily;
            }
            foreach ($rowdaily as $key => $val) {
                $actualDaily[] = $val['UnitEntry'];
            }
            if (!isset($actualDaily)) {
                $actualdaily = [];
            } else {
                $actualdaily = $actualDaily;
            }
            foreach ($rowdaily as $key => $val) {
                $targetDaily[] = strstr($val['TargetEntry'], '.', true);
            }
            if (!isset($targetDaily)) {
                $targetdaily = [];
            } else {
                $targetdaily = $targetDaily;
            }

            //BIAYA OPERASIONAL
            $querybiaya = "CALL rdMarketingConsol('BiayaGrafik','" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'Pierre','Dealer','','','');";
            if($korwil === "''"){
                $querybiaya = "CALL rdMarketingConsol('BiayaGrafik','" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'Pierre','Dealer','','','');";
            }
            $rowbiaya = Yii::$app->db->createCommand($querybiaya)->queryAll();


            foreach ($rowbiaya as $key => $val) {
                $LabelBiaya[] = $val['Bulan'];
            }
            if (!isset($LabelBiaya)) {
                $labelbiaya = [];
            } else {
                $labelbiaya = $LabelBiaya;
            }

            foreach ($rowbiaya as $key => $val) {
//                $nilaiBiaya[] = number_format( $val['Nilai'] , 0 , '.' , '.' );
                $nilaiBiaya[] = $val['Nilai']   ;
            }
            if (!isset($nilaiBiaya)) {
                $nilaibiaya = [];
            } else {
                $nilaibiaya = $nilaiBiaya;
            }
            $ago = $labelbiaya[0];
            $now = $labelbiaya[2];
            $before = $labelbiaya[1];


            return [
                'jasaoli' => $jasaoli,
                'produk' => $produk,
                'jasagroup' => $jasagroup,
                'dtbiaya' => $dtbiaya,
                'peformance' => $performance,
                'sales' => $sales,
                'daily' => $daily,
                'biaya' => $biaya,
                'labeldaily' => $labeldaily,
                'actualdaily' => $actualdaily,
                'targetdaily' => $targetdaily,
                'labelbiaya' => $labelbiaya,
                'nilaibiaya' => $nilaibiaya,
                'ago' => $ago,
                'now' => $now,
                'before' => $before,
            ];

        }
    }

    public function actionLapdashboardJasaoli()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];

            //JASA OLI
            $queryjasaoli = "CALL rdMarketingConsol('JPOPerUnit', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";
            $jasaoli = Yii::$app->db->createCommand($queryjasaoli)->queryAll();

            //BULAN
            $querybulan = "CALL rdMarketingConsol('BiayaGrafik', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";
            $bulan = Yii::$app->db->createCommand($querybulan)->queryAll();

            foreach ($bulan as $key => $val) {
                $LabelBulan[] = $val['Bulan'];
            }
            if (!isset($LabelBulan)) {
                $labelbulan = [];
            } else {
                $labelbulan = $LabelBulan;
            }

            $ago = $labelbulan[0];
            $now = $labelbulan[2];
            $before = $labelbulan[1];

            return [
                'jasaoli' => $jasaoli,
                'ago' => $ago,
                'now' => $now,
                'before' => $before,
            ];

        }
    }

    public function actionLapdashboardProduktifitas()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];

            //PRODUK
            $queryproduk = "CALL rdMarketingConsol('Produktivitas', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";
            $produk = Yii::$app->db->createCommand($queryproduk)->queryAll();

            //BULAN
            $querybulan = "CALL rdMarketingConsol('BiayaGrafik', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";
            $bulan = Yii::$app->db->createCommand($querybulan)->queryAll();

            foreach ($bulan as $key => $val) {
                $LabelBulan[] = $val['Bulan'];
            }
            if (!isset($LabelBulan)) {
                $labelbulan = [];
            } else {
                $labelbulan = $LabelBulan;
            }

            $ago = $labelbulan[0];
            $now = $labelbulan[2];
            $before = $labelbulan[1];

            return [
                'produk' => $produk,
                'ago' => $ago,
                'now' => $now,
                'before' => $before,
            ];

        }
    }

    public function actionLapdashboardJasagroup()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];

            $queryjasagroup = "CALL rdMarketingConsol('JasaGroup', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";
            $jasagroup = Yii::$app->db->createCommand($queryjasagroup)->queryAll();

            //BULAN
            $querybulan = "CALL rdMarketingConsol('BiayaGrafik', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";
            $bulan = Yii::$app->db->createCommand($querybulan)->queryAll();

            foreach ($bulan as $key => $val) {
                $LabelBulan[] = $val['Bulan'];
            }
            if (!isset($LabelBulan)) {
                $labelbulan = [];
            } else {
                $labelbulan = $LabelBulan;
            }

            $ago = $labelbulan[0];
            $now = $labelbulan[2];
            $before = $labelbulan[1];

            return [
                'jasagroup' => $jasagroup,
                'ago' => $ago,
                'now' => $now,
                'before' => $before,
            ];

        }
    }

    public function actionLapdashboardSalestarget()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];

            $querysales = "CALL rdMarketingConsol('TargetUE', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";
            $sales = Yii::$app->db->createCommand($querysales)->queryAll();

            return [
                'sales' => $sales
            ];

        }
    }

    public function actionLapdashboardDailytarget()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];

            $querydaily = "CALL rdMarketingConsol('DailyUE', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";

            $rowdaily = Yii::$app->db->createCommand($querydaily)->queryAll();
            foreach ($rowdaily as $key => $val) {
                $LabelDaily[] = $val['Tanggal'];
            }
            if (!isset($LabelDaily)) {
                $labeldaily = [];
            } else {
                $labeldaily = $LabelDaily;
            }
            foreach ($rowdaily as $key => $val) {
                $actualDaily[] = $val['UnitEntry'];
            }
            if (!isset($actualDaily)) {
                $actualdaily = [];
            } else {
                $actualdaily = $actualDaily;
            }
            foreach ($rowdaily as $key => $val) {
                $targetDaily[] = strstr($val['TargetEntry'], '.', true);
            }
            if (!isset($targetDaily)) {
                $targetdaily = [];
            } else {
                $targetdaily = $targetDaily;
            }

            return [
                'labeldaily' => $labeldaily,
                'actualdaily' => $actualdaily,
                'targetdaily' => $targetdaily,
            ];

        }
    }

    public function actionLapdashboardBiayaoperasional()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];

            $querybiaya = "CALL rdMarketingConsol('BiayaGrafik', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";
            $rowbiaya = Yii::$app->db->createCommand($querybiaya)->queryAll();
            foreach ($rowbiaya as $key => $val) {
                $LabelBiaya[] = $val['Bulan'];
            }
            if (!isset($LabelBiaya)) {
                $labelbiaya = [];
            } else {
                $labelbiaya = $LabelBiaya;
            }

            foreach ($rowbiaya as $key => $val) {
                $nilaiBiaya[] = $val['Nilai']   ;
            }
            if (!isset($nilaiBiaya)) {
                $nilaibiaya = [];
            } else {
                $nilaibiaya = $nilaiBiaya;
            }
            $ago = $labelbiaya[0];
            $now = $labelbiaya[2];
            $before = $labelbiaya[1];
            return [
                'labelbiaya' => $labelbiaya,
                'nilaibiaya' => $nilaibiaya,
                'ago' => $ago,
                'now' => $now,
                'before' => $before,
            ];

        }
    }

    public function actionLapdashboardDetailbiaya()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];

            $querydetail = "CALL rdMarketingConsol('BiayaTabel', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";
            $dtbiaya = Yii::$app->db->createCommand($querydetail)->queryAll();

            $querybiaya = "CALL rdMarketingConsol('BiayaGrafik', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";
            $rowbiaya = Yii::$app->db->createCommand($querybiaya)->queryAll();
            foreach ($rowbiaya as $key => $val) {
                $LabelBiaya[] = $val['Bulan'];
            }
            if (!isset($LabelBiaya)) {
                $labelbiaya = [];
            } else {
                $labelbiaya = $LabelBiaya;
            }


            $ago = $labelbiaya[0];
            $now = $labelbiaya[2];
            $before = $labelbiaya[1];
            return [
                'labelbiaya' => $labelbiaya,
                'dtbiaya' => $dtbiaya,
                'ago' => $ago,
                'now' => $now,
                'before' => $before,
            ];

        }
    }

    public function actionLapdashboardPeformance()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];

            //PERFORMANCE
            $queryperformance = "CALL rdMarketingConsol('Performance', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";
            $performance = Yii::$app->db->createCommand($queryperformance)->queryAll();
            return [
                'peformance' => $performance,
            ];

        }
    }

    public function actionShowDetail()
    {
        $param = $_GET['param'];

        //title
        switch (true) {
            case str_contains($param, 'JPOPerUnit'):
                $title = 'Jasa Oli Part per Unit';
                $no = '1';
                $param2 = str_replace('JPOPerUnit', 'BiayaGrafik', $param);
                $query = "CALL rdMarketingConsol($param2);";
                $bulan = Yii::$app->db->createCommand($query)->queryAll();
                break;
            case str_contains($param, 'Produktivitas'):
                $title = 'Produktivitas';
                $no = '2';
                $param2 = str_replace('Produktivitas', 'BiayaGrafik', $param);
                $query = "CALL rdMarketingConsol($param2);";
                $bulan = Yii::$app->db->createCommand($query)->queryAll();
                break;
            case str_contains($param, 'JasaGroup'):
                $title = 'Jasa Group';
                $no = '3';
                $param2 = str_replace('JasaGroup', 'BiayaGrafik', $param);
                $query = "CALL rdMarketingConsol($param2);";
                $bulan = Yii::$app->db->createCommand($query)->queryAll();
                break;
            case str_contains($param, 'Level12'):
                $title = 'Sales VS Target';
                $no = '4';
                $param2 = str_replace('Level12', 'BiayaGrafik', $param);
                $query = "CALL rdMarketingConsol($param2);";
                $bulan = Yii::$app->db->createCommand($query)->queryAll();
                break;
            case str_contains($param, 'Grade12'):
                $title = 'Detail Biaya Operasional';
                $no = '5';
                $param2 = str_replace('Grade12', 'BiayaGrafik', $param);
                $query = "CALL rdMarketingConsol($param2);";
                $bulan = Yii::$app->db->createCommand($query)->queryAll();
                break;
            case str_contains($param, 'Performance'):
                $title = 'Performance';
                $no = '6';
                $param2 = str_replace('Performance', 'BiayaGrafik', $param);
                $query = "CALL rdMarketingConsol($param2);";
                $bulan = Yii::$app->db->createCommand($query)->queryAll();
                break;
            case str_contains($param, 'Grade06'):
                $title = 'Biaya Operasional';
                $no = '7';
                $param2 = str_replace('Grade06', 'BiayaGrafik', $param);
                $query = "CALL rdMarketingConsol($param2);";
                $bulan = Yii::$app->db->createCommand($query)->queryAll();
                break;
            case str_contains($param, 'Level06'):
                $title = 'Daily Target';
                $no = '8';
                $param2 = str_replace('Level06', 'BiayaGrafik', $param);
                $query = "CALL rdMarketingConsol($param2);";
                $bulan = Yii::$app->db->createCommand($query)->queryAll();
                break;
        }

        if($no === '6'){
            $date1 = substr($param, 15, 10);
            $date2 = substr($param, 28, 10);
            $before1 = date('Y-m-d', strtotime('-6 months' , strtotime($date1)));
            $before2 = date('Y-m-d', strtotime('-6 months' , strtotime($date2)));
            $ago1 = date('Y-m-d', strtotime('-12 months' , strtotime($date1)));
            $ago2 = date('Y-m-d', strtotime('-12 months' , strtotime($date2)));

            $srtparam = substr($param, 0, 38);
            $newparam = str_replace($srtparam, '', $param);

            // tabel 1
            $query1 = "CALL rdMarketingConsol($param);";
            $data1 = Yii::$app->db->createCommand($query1)->queryAll();

            // tabel 2
            $param2 = '"Peformance","'.$before1.'","'.$before2.''.$newparam;
            $query2 = "CALL rdMarketingConsol($param2);";
            $data2 = Yii::$app->db->createCommand($query2)->queryAll();

            // tabel 3
            $param3 = '"Peformance","'.$ago1.'","'.$ago2.''.$newparam;
            $query3 = "CALL rdMarketingConsol($param3);";
            $data3 = Yii::$app->db->createCommand($query3)->queryAll();

            $data = [
                'data1' => $data1,
                'data2' => $data2,
                'data3' => $data3
            ];
        } else {
            $query = "CALL rdMarketingConsol($param);";
            $data = Yii::$app->db->createCommand($query)->queryAll();
        }

        //get bulan
        foreach ($bulan as $key => $val) {
            $LabelBiaya[] = $val['Bulan'];
        }
        if (!isset($LabelBiaya)) {
            $labelbiaya = [];
        } else {
            $labelbiaya = $LabelBiaya;
        }

        $ago = $labelbiaya[0];
        $now = $labelbiaya[2];
        $before = $labelbiaya[1];

        return $this->render('@abengkel/views/report/Auditor/ShowDetail', [
            'ago' => $ago,
            'now' => $now,
            'before' => $before,
            'title' => $title,
            'no' => $no,
            'data' => $data,
            ]);

    }

    public function actionDashboardAkukonsol()
    {
        return $this->prosesLaporan('', '@abengkel/views/report/Auditor/DashboardAkukonsol');

    }

    public function actionAkukonsolprofit()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $bulan = str_replace('"', '',date('"F Y"', strtotime($tgl1)));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];
            $callquery = "CALL rdAkuntansiConsol('DashboardConsol','" . $tgl1 . "','" . $tgl2 . "','".$eom."','".$user."',".$level.",'','','');";
            $get_query = Yii::$app->db->createCommand($callquery)->queryAll();
            foreach ($get_query as $query) {
                $kota[] = $query['Kota'];
                $profit[] = $query['Profit'];
                $profitperunit[] = $query['ProfitPerUnit'];
                $penjualanpart[] = $query['PenjualanPart'];
                $penjualanoli[] = $query['PenjualanOli'];
                $penjualanjasa[] = $query['PenjualanJasa'];

            }

            return [
                'kota' => $kota,
                'profit' => $profit,
                'profitperunit' => $profitperunit,
                'penjualanpart' => $penjualanpart,
                'penjualanoli' => $penjualanoli,
                'penjualanjasa' => $penjualanjasa,

            ];
        }
    }

    public function actionAkudashacct()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];
            $callquery = "CALL rdAkuntansiConsol('DashAcct','" . $tgl1 . "','" . $tgl2 . "','".$eom."','".$user."',".$level.",'','','');";
            $get_query = Yii::$app->db->createCommand($callquery)->queryAll();
            $Opex[] = 0;
            foreach ($get_query as $query) {
                if( $query['Keterangan'] == 'Current Asset'){
                    $CurrentAsset[] = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Current Liabilities'){
                    $CurrentLiabilities[] = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Working Capital'){
                    $WorkingCapital[] = $query['NilaiJuta'];
                }else if($query['Judul'] == 'Laba Rugi'){
                    $LabaRugi[] = $query['NilaiJuta'];
                    $LabaRugiLabel[] = $query['Keterangan'];
                }else if($query['Keterangan'] == 'Aktiva'){
                    $TotalAssets = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Cash & Bank'){
                    $CashBank = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Piutang'){
                    $Piutang= $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Persediaan'){
                    $Persediaan = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Depositor, Adv & Prepay'){
                    $Depositor = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Aktiva Tetap & Lain2'){
                    $AktivaTetap = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Pasiva'){
                    $TotalLiabilities = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Hutang'){
                    $Hutang = $query['NilaiJuta'];
                } else if($query['Keterangan'] == 'Titipan'){
                    $Titipan = $query['NilaiJuta'];
                } else if($query['Keterangan'] == 'Modal'){
                    $Modal = $query['NilaiJuta'];
                } else if($query['Keterangan'] == 'Total Rugi Laba'){
                    $TotalRugiLaba = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Total Asset'){
                    $TotalAsset[] = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Total Liabilites'){
                    $TotalLiabilites[] = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Total Equity'){
                    $TotalEquity[] = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Gaji & Tunj'){
                    $Gaji[] = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Insentif'){
                    $Insentif[] = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'By Pemasaran'){
                    $Pemasaran[] = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'By Penyusutan Asuransi & Sewa'){
                    $Asuransi[] = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'By Penyusutan Asset'){
                    $Asset[] = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Opex lainnya'){
                    $Opex[] = $query['NilaiJuta'];
                }
            }

            return [
                'TotalLiabilities' => str_replace(',', '.', number_format($TotalLiabilities)),
                'Hutang' => str_replace(',', '.', number_format($Hutang)),
                'Hutang1' => $Hutang,
                'Titipan' => str_replace(',', '.', number_format($Titipan)),
                'Titipan1' => $Titipan,
                'Modal' => str_replace(',', '.', number_format($Modal)),
                'Modal1' => $Modal,
                'TotalRugiLaba' => str_replace(',', '.', number_format($TotalRugiLaba)),
                'TotalRugiLaba1' => $TotalRugiLaba,
                'CurrentAsset' => $CurrentAsset,
                'CurrentLiabilities' => $CurrentLiabilities,
                'WorkingCapital' => $WorkingCapital,
                'LabaRugiLabel' => $LabaRugiLabel,
                'LabaRugi' => $LabaRugi,
                'TotalAssets' => str_replace(',', '.', number_format($TotalAssets)),
                'CashBank1' => $CashBank,
                'CashBank' => str_replace(',', '.', number_format($CashBank)),
                'Piutang' => str_replace(',', '.', number_format($Piutang)),
                'Piutang1' => $Piutang,
                'Persediaan' => str_replace(',', '.', number_format($Persediaan)),
                'Persediaan1' => $Persediaan,
                'Depositor' =>str_replace(',', '.', number_format( $Depositor)),
                'Depositor1' =>$Depositor,
                'AktivaTetap' => str_replace(',', '.', number_format($AktivaTetap)),
                'AktivaTetap1' => $AktivaTetap,
                'TotalAsset' => $TotalAsset,
                'TotalLiabilites' => $TotalLiabilites,
                'TotalEquity' => $TotalEquity,
                'Insentif' => $Insentif,
                'Gaji' => $Gaji,
                'Pemasaran' => $Pemasaran,
                'Asuransi' => $Asuransi,
                'Asset' => $Asset,
                'Opex' => $Opex,
            ];
        }
    }

    public function actionItemGrade() {
        return $this->prosesLaporan( 'abengkel/rdMarketing', '@abengkel/views/report/Persediaan/ItemGrade' );
    }

}
