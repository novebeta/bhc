<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\models\Ttckit;
use abengkel\models\Ttckhd;
use common\components\Custom;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtckitController implements the CRUD actions for Ttckit model.
 */
class TtckitController extends AbengkelController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttckit::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
            /** @var Ttckit $model */
            $model = null;
            if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
                $model = $this->findModelBase64( 'ttckit', $requestData[ 'id' ] );
            }
			switch ( $oper ) {
				case 'add'  :
                case 'edit' :
                    if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
                        $requestData[ 'Action' ] = 'Insert';
                        $requestData[ 'CKNo' ] = 0;
                    } else {
                        $requestData[ 'Action' ] = 'Update';
                        if ( $model != null ) {
                            $requestData[ 'CKNoOLD' ]   = $model->CKNo;
                            $requestData[ 'SDNoOLD' ] = $model->SDNo;
                        }
                    }
                    $result = Ttckit::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
                    break;
                case 'batch' :
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	                $header                      = $requestData[ 'header' ];
	                $header[ 'Action' ]          = 'Loop';
	                $result                      = Ttckhd::find()->callSP( $header );
                    foreach ( $requestData[ 'data' ] as $item ) {
	                    $items[ 'Action' ]  = 'Insert';
	                    $items[ 'CKNo' ]    = $header[ 'CKNo' ];
	                    $items[ 'SDNo' ]    = $item[ 'data' ][ 'NO' ];
	                    $items[ 'CKPart' ]    = $item[ 'data' ][ 'SDTotalPart' ];
	                    $items[ 'CKJasa' ]    = $item[ 'data' ][ 'SDTotalJasa' ];
                        $result             = Ttckit::find()->callSP( $items );
                    }
                    return $this->responseSuccess( $requestData );
                    break;
                case 'del'  :
                    $requestData             = array_merge( $requestData, $model->attributes );
                    $requestData[ 'Action' ] = 'Delete';
                    if ( $model != null ) {
                        $requestData[ 'CKNoOLD' ]   = $model->CKNo;
                        $requestData[ 'SDNoOLD' ] = $model->SDNo;
                    }
                    $result = Ttckit::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
                    break;
			}
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'Ttckit.' . $primaryKeys[ $i ];
			}
			$query = new Query();
			$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,ttckit.CKNo, ttckit.SDNo, ttckit.CKPart, ttckit.CKJasa, ttsdhd.SDTgl, ttsdhd.MotorNoPolisi, 
				ttckit.CKPart + ttckit.CKJasa AS CKTotal, tdcustomer.CusNama, tdcustomer.MotorType, tdcustomer.MotorNama, ttsdhd.SVNo' ) )
			      ->from( "ttckit" )
			      ->innerJoin( "ttsdhd", "ttckit.SDNo = ttsdhd.SDNo" )
			      ->innerJoin( "tdcustomer", "ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi" )
			      ->groupBy( "ttckit.SDNo, tdcustomer.MotorNama, ttsdhd.SVNo" )
			      ->where( "(ttckit.CKNo = :CKNo)", [ ':CKNo' => $requestData[ 'CKNo' ] ] );
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	protected function findModel( $CKNo, $SDNo ) {
		if ( ( $model = Ttckit::findOne( [ 'CKNo' => $CKNo, 'SDNo' => $SDNo ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
