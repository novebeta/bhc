<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\models\Ttseitjasa;
use Yii;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtseitjasaController implements the CRUD actions for Ttseitjasa model.
 */
class TtseitjasaController extends AbengkelController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttseitjasa models.
	 * @return mixed
	 */
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttseitjasa::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
			/** @var Ttseitjasa $model */
			$model = null;
			if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
				$model = $this->findModelBase64( 'ttseitjasa', $requestData[ 'id' ] );
			}
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'Action' ] = 'Insert';
						$requestData[ 'SEAuto' ] = 0;
					} else {
						$requestData[ 'Action' ] = 'Update';
						if ( $model != null ) {
							$requestData[ 'SENoOLD' ]     = $model->SENo;
							$requestData[ 'SEAutoOLD' ]   = $model->SEAuto;
							$requestData[ 'JasaKodeOLD' ] = $model->JasaKode;
						}
					}
					$result = Ttseitjasa::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'batch' :
					break;
				case 'del'  :
					$requestData             = array_merge( $requestData, $model->attributes );
					$requestData[ 'Action' ] = 'Delete';
					if ( $model != null ) {
						$requestData[ 'SENoOLD' ]     = $model->SENo;
						$requestData[ 'SEAutoOLD' ]   = $model->SEAuto;
						$requestData[ 'JasaKodeOLD' ] = $model->JasaKode;
					}
					$result = Ttseitjasa::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
			}
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'Ttseitjasa.' . $primaryKeys[ $i ];
			}
			$query     = ( new \yii\db\Query() )
				->select( new Expression(
					'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
                            ttseitjasa.SENo,
                            ttseitjasa.SEAuto,
                            ttseitjasa.JasaKode,
                            ttseitjasa.SEWaktuKerja,
                            ttseitjasa.SEWaktuSatuan,
                            ttseitjasa.SEWaktuKerjaMenit,
                            ttseitjasa.SEHrgJual,
                            ttseitjasa.SEJualDisc,
                            tdjasa.JasaNama,
                            IFNULL(ttseitjasa.SEJualDisc / ttseitjasa.SEHrgJual * 100, 0) AS Disc,
                            ttseitjasa.SEHrgJual - ttseitjasa.SEJualDisc AS Jumlah,
                            ttseitjasa.SEHargaBeli,
                            tdjasa.JasaGroup'
				) )
				->from( 'ttseitjasa' )
				->join( 'INNER JOIN', 'tdjasa', 'ttseitjasa.JasaKode = tdjasa.JasaKode' )
				->where( [ 'ttseitjasa.SENo' => $requestData[ 'SENo' ] ] )
				->orderBy( '	ttseitjasa.SENo,ttseitjasa.SEAuto' );
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	/**
	 * Displays a single Ttseitjasa model.
	 *
	 * @param string $SENo
	 * @param integer $SEAuto
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $SENo, $SEAuto ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $SENo, $SEAuto ),
		] );
	}
	/**
	 * Creates a new Ttseitjasa model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Ttseitjasa();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'SENo' => $model->SENo, 'SEAuto' => $model->SEAuto ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Ttseitjasa model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $SENo
	 * @param integer $SEAuto
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $SENo, $SEAuto ) {
		$model = $this->findModel( $SENo, $SEAuto );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'SENo' => $model->SENo, 'SEAuto' => $model->SEAuto ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Ttseitjasa model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $SENo
	 * @param integer $SEAuto
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $SENo, $SEAuto ) {
		$this->findModel( $SENo, $SEAuto )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Ttseitjasa model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $SENo
	 * @param integer $SEAuto
	 *
	 * @return Ttseitjasa the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $SENo, $SEAuto ) {
		if ( ( $model = Ttseitjasa::findOne( [ 'SENo' => $SENo, 'SEAuto' => $SEAuto ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
