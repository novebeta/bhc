<?php

namespace abengkel\controllers;

use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\components\TUi;
use abengkel\models\Tdlokasi;
use abengkel\models\Ttckhd;
use abengkel\models\Tttahd;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\RangeNotSatisfiableHttpException;

/**
 * TtckhdController implements the CRUD actions for Ttckhd model.
 */
class TtckhdController extends AbengkelController
{
    public function actions()
    {
        $colGrid               = null;
        $joinWith              = [];
        $join                  = [];
        $where                  = [
            [
                'op'        => 'AND',
                'condition' => "(CKNo NOT LIKE '%=%')",
                'params'    => []
            ]
        ];
        $queryRaw              = null;
        $queryRawPK            = null;

        if ( isset( $_POST[ 'query' ] ) ) {
            $json      = Json::decode( $_POST[ 'query' ], true );
            $r         = $json[ 'r' ];
            $urlParams = $json[ 'urlParams' ];
            switch ( $r ) {
                case 'ttckhd/order':
                    $where                  = [
                        [
                            'op'        => 'AND',
                            'condition' => "(NO NOT LIKE '%=%')",
                            'params'    => []
                        ]
                    ];
                            $queryRaw = "SELECT ttSDhd.SDNo AS NO, ttSDhd.SDTgl AS Tgl, tdcustomer.MotorNoPolisi AS Kode, tdcustomer.CusNama AS Nama, tdcustomer.MotorNama , 
                                    SDTotalJasa, SDTotalPart, SDTotalJasa + SDTotalPart AS SDTotal
                                    FROM ttSDhd 
                                    INNER JOIN tdcustomer  ON ttSDhd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                                    WHERE KlaimKPB = 'Belum' AND ttSDhd.KodeTrans = 'TC14' AND ttSDhd.SVNo <> '--'";
                    $queryRawPK = [ 'NO' ];
                    $colGrid    = Ttckhd::colGridPick();
            }
        }
        return [
            'td' => [
                'class'      => TdAction::class,
                'model'      => Ttckhd::class,
                'queryRaw'   => $queryRaw,
                'queryRawPK' => $queryRawPK,
                'colGrid'    => $colGrid,
                'joinWith'   => $joinWith,
                'join'       => $join,
                'where'      => $where
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionOrder() {
        $this->layout = 'select-mode';
        return $this->render( 'order',
            [
                'arr'     => [
                    'cmbTxt'    => [
                        'CusNama'       => 'Nama Konsumen',
                        'SVNo'          => 'No Servis',
                        'MotorNoPolisi' => 'No Polisi',
                        'MotorNama'     => 'Nama Motor',
                        'MotorNoMesin'     => 'Nomor Mesin',
                    ],
                    'cmbTgl'    => [
                        'Tgl' => 'Tgl Servis',
                    ],
                    'cmbNum'    => [
                        'SDTotal' => 'Total',
                    ],
                    'sortname'  => "Tgl",
                    'sortorder' => 'desc'
                ],
                'options' => [ 'colGrid' => Ttckhd::colGridPick(), 'mode' => self::MODE_SELECT_MULTISELECT ]
            ] );
    }

    /**
     * Lists all Ttckhd models.
     * @return mixed
     */
    public function actionIndex()
    {
	    if (isset($_GET['action']) && $_GET['action'] == 'display') {
	        $post['Action'] = 'Display';
	        $result = Ttckhd::find()->callSP($post);
	        Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->render('index', [
            'mode' => '',
            'url_add' => Url::toRoute(['ttckhd/td'])
        ]);
    }

    /**
     * Displays a single Ttckhd model.
     *
     * @param string $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    */
    /**
     * Finds the Ttckhd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param string $id
     *
     * @return Ttckhd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ttckhd::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new Ttckhd model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $LokasiKodeCmb       = Tdlokasi::find()->where( [ 'LokasiStatus' => 'A', 'LEFT(LokasiNomor,1)' => '7' ] )->orderBy( 'LokasiStatus, LokasiKode' )->one();
        $model              = new Ttckhd();
        $model->CKNo        = General::createTemporaryNo('CK', 'Ttckhd', 'CKNo');
        $model->CKTgl       = date( 'Y-m-d H:i:s' );
        $model->CKTotalPart = 0;
        $model->CKTotalJasa = 0;
        $model->CKNoKlaim   = '--';
        $model->CKMemo      = '--';
        $model->NoGL        = '--';
        $model->Cetak       = 'Belum';
        $model->UserID      = $this->UserID();
        $model->save();
        $post             = $model->attributes;
        $post['Action'] = 'Insert';
        $result = Ttckhd::find()->callSP($post);
        Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        $dsTServis = Ttckhd::find()->ttckhdFillByNo()->where(['ttckhd.CKNo' => $model->CKNo])->asArray(true)->one();
        $id = $this->generateIdBase64FromModel($model);
        $dsTServis[ 'CKNoView' ] = $result[ 'CKNo' ];
        return $this->render('create', [
            'model' => $model,
            'dsTServis' => $dsTServis,
            'id' => $id
        ]);
    }

    /**
     * Updates an existing Ttckhd model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param string $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $index = \yii\helpers\Url::toRoute( [ 'ttckhd/index' ] );
        if ( ! isset( $_GET[ 'action' ] ) ) {
            return $this->redirect( $index );
        }
        /** @var Ttckhd $model */
        $model = $this->findModelBase64('Ttckhd', $id);
        $dsTServis = Ttckhd::find()->ttckhdFillByNo()->where(['ttckhd.CKNo' => $model->CKNo])->asArray(true)->one();

        if (Yii::$app->request->isPost) {
	        $post               = array_merge($dsTServis, \Yii::$app->request->post());
	        $post[ 'CKTgl' ] = $post[ 'CKTgl' ].' '.$post[ 'CKJam' ];
            $post['Action'] = 'Update';
            $result = Ttckhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
            $model     = Ttckhd::findOne( $result[ 'CKNo' ] );
//            $model     = null;
            if ( $result[ 'CKNo' ] == '--' || $model == null) {
                if ( $model === null ) {
                    Yii::$app->session->addFlash( 'warning', 'CKNo : ' . $result[ 'CKNo' ]. ' tidak ditemukan.' );
                }
                return $this->redirect( $index);
            }
            $id        = $this->generateIdBase64FromModel( $model );
            $post[ 'CKNoView' ] = $result[ 'CKNo' ];
            if ($result['status'] == 0 && $result['status'] !== null ) {
                return $this->redirect(['ttckhd/update','action' => 'display', 'id' => $id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'dsTServis' => $post,
                    'id' => $id,
                ]);
            }
        }
        $dsTServis['Action'] = 'Load';
        $result = Ttckhd::find()->callSP($dsTServis);
        Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        $dsTServis[ 'CKNoView' ] = $_GET[ 'CKNoView' ] ?? $result[ 'CKNo' ];
        return $this->render('update', [
            'model' => $model,
            'dsTServis' => $dsTServis,
            'id' => $id
        ]);
    }

    /**
     * Deletes an existing Ttckhd model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param string $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
//    public function actionDelete()
//    {
//        /** @var Ttckhd $model */
//        $model = $model = $this->findModelBase64('Ttckhd', $_POST['id']);
//        $_POST['CKNo'] = $model->CKNo;
//        $_POST['Action'] = 'Delete';
//        $result = Ttckhd::find()->callSP($_POST);
//        if ($result['status'] == 0) {
//            return $this->responseSuccess($result['keterangan']);
//        } else {
//            return $this->responseFailed($result['keterangan']);
//        }
//    }

    public function actionDelete()
    {
        /** @var Ttckhd $model */
        $request = Yii::$app->request;
        if ($request->isAjax){
            $model = $this->findModelBase64('Ttckhd', $_POST['id']);
            $_POST['CKNo'] = $model->CKNo;
            $_POST['Action'] = 'Delete';
            $result = Ttckhd::find()->callSP($_POST);
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            $id = $request->get('id');
            $model = $this->findModelBase64('Ttckhd', $id);
            $_POST['CKNo'] = $model->CKNo;
            $_POST['Action'] = 'Delete';
            $result = Ttckhd::find()->callSP($_POST);
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['CKNo'] == '--') {
                return $this->redirect(['index']);
            } else {
                $model = Ttckhd::findOne($result['CKNo']);
                return $this->redirect(['ttckhd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
            }
        }
    }

    public function actionCancel($id, $action)
    {
//        /** @var Ttckhd $model */
//        $_POST[ 'CKNo' ]   = $model->CKNo;
        $_POST[ 'Action' ] = 'Cancel';
        $_POST[ 'CKTgl' ] = $_POST[ 'CKTgl' ].' '.$_POST[ 'CKJam' ];
        $result            = Ttckhd::find()->callSP( $_POST );
        Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        $model = Ttckhd::findOne($result[ 'CKNo' ]);
//        $model = null;
        if ( $result[ 'CKNo' ] == '--' || $model == null) {
            if ( $model === null ) {
                Yii::$app->session->addFlash( 'warning', 'CKNo : ' . $result[ 'CKNo' ]. ' tidak ditemukan.' );
            }
            return $this->redirect(['index']);
        }else{
            return $this->redirect( [ 'ttckhd/update',  'action' => 'display','id' => $this->generateIdBase64FromModel( $model )] );
        }
    }

    public function actionPrint($id)
    {
        unset($_POST['_csrf-app']);
        $_POST = array_merge($_POST, Yii::$app->session->get('_company'));
        $_POST['db'] = base64_decode($_COOKIE['_outlet']);
	    $_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
        $client = new Client([
            'headers' => ['Content-Type' => 'application/octet-stream']
        ]);
        $response = $client->post(Yii::$app->params['H2'][$_POST['db']]['host_report'] . '/abengkel/fckhd', [
            'body' => Json::encode($_POST)
        ]);
        $html                 = $response->getBody();
        return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
    }

    public function actionJurnal() {
        $post             = \Yii::$app->request->post();
        $post[ 'Action' ] = 'Jurnal';
        $result           = Ttckhd::find()->callSP( $post );
        Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        return $this->redirect( [ 'ttckhd/update',  'action' => 'display','id' =>  base64_encode($result['CKNo'])] );
    }
}
