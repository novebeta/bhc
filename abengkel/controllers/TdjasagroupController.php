<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\components\TUi;
use abengkel\models\Tdjasagroup;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
/**
 * TdjasagroupController implements the CRUD actions for Tdjasagroup model.
 */
class TdjasagroupController extends AbengkelController {

	public function actions() {
		return [
			'td' => [
				'class' => TdAction::class,
				'model' => Tdjasagroup::class,
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Tdjasagroup models.
	 * @return mixed
	 */
	public function actionIndex() {
		$dataProvider = new ActiveDataProvider( [
			'query' => Tdjasagroup::find(),
		] );
		return $this->render( 'index', [
			'dataProvider' => $dataProvider,
		] );
	}
	/**
	 * Finds the Tdjasagroup model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Tdjasagroup the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Tdjasagroup::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Tdjasagroup model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		TUi::$actionMode   = Menu::ADD;
		$model = new Tdjasagroup();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'tdjasa/update', 'id' => $model->JasaGroup, 'action' => 'display' ] );
		}
		return $this->render( 'create', [
			'model' => $model,
            'id'    => 'new'
		] );
	}
	/**
	 * Updates an existing Tdjasagroup model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		$model = $this->findModelBase64('Tdjasagroup',$id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect( [ 'tdjasa/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
		}

		return $this->render('update', [
			'model' => $model,
			'id'    => $this->generateIdBase64FromModel($model),
		]);
	}
	/**
	 * Deletes an existing Tdjasagroup model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}

    public function actionCancel( $id, $action ) {
        $model = Tdjasagroup::findOne( $id );
        if ( $model == null ) {
            $model = Tdjasagroup::find()->one();
            if ( $model == null ) {
                return $this->redirect( [ 'tdjasagroup/index' ] );
            }
        }
        return $this->redirect( [ 'tdjasagroup/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
    }
}
