<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\models\Traccount;
use abengkel\models\Ttbkhd;
use abengkel\models\Ttbkit;
use abengkel\models\Ttbkitcoa;
use abengkel\models\Ttbmhd;
use abengkel\models\Ttbmit;
use abengkel\models\Ttkkhd;
use abengkel\models\Tttahd;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use abengkel\components\TUi;
/**
 * TtbkhdController implements the CRUD actions for Ttbkhd model.
 */
class TtbkhdController extends AbengkelController {
	public function actions() {
		$colGrid               = null;
		$joinWith              = [];
		$join                  = [];
		$where                 = [];
		$joinWith[ 'account' ] = [
			'type' => 'INNER JOIN'
		];
		$sqlraw                = null;
		$queryRawPK            = null;
		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			switch ( $r ) {
				case 'ttbkhd/bank-keluar-umum':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.KodeTrans = 'UM' AND ttbkhd.BKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-keluar-ke-kas':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.KodeTrans = 'KM' AND ttbkhd.BKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-keluar-pembelian':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.KodeTrans = 'PI' AND ttbkhd.BKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-keluar-order-pengerjaan-luar':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.KodeTrans = 'PL' AND ttbkhd.BKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-keluar-retur-penjualan':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.KodeTrans = 'SR' AND ttbkhd.BKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/transfer-antar-bank':
					$colGrid    = Ttbkhd::colGridTransferBank();
                    $where = [
                        [
                            'op'        => 'AND',
                            'condition' => "(BKNo NOT LIKE '%=%')",
                            'params'    => []
                        ]
                    ];
					$sqlraw     = "SELECT ttbkit.BKLink, ttbkhd.BKTgl, ttbkit.BKNo, ttbmit.BMNo, ttbmhd.BMTgl, ttbkhd.BKNominal, ttbkhd.BKPerson, 
					ttbkhd.BankKode AS NoAccountBK, ttbmhd.BankKode AS NoAccountBM, ttbmhd.BMPerson,ttbkhd.UserID, ttbkhd.NoGL AS NoGLBK, 
       				ttbmhd.NoGL  AS NoGLBM, traccountbk.NamaAccount AS NamaAccountBM, traccountbm.NamaAccount AS NamaAccountBK
					 FROM ttbkhd 
			         INNER JOIN ttbkit ON ttbkhd.BKNo = ttbkit.BKNo
			         INNER JOIN ttbmit ON ttbkit.BKLink = ttbmit.BMLink
			         INNER JOIN ttbmhd ON ttbmhd.BMNo = ttbmit.BMNo
					INNER JOIN traccount traccountbm ON ttbmhd.BankKode = traccountbm.NoAccount
					INNER JOIN traccount traccountbk ON ttbkhd.BankKode = traccountbk.NoAccount
			         WHERE (ttbkhd.BKJenis = 'Bank Transfer' AND ttbmhd.BMJenis = 'Bank Transfer')";
					$queryRawPK = [ 'BKLink' ];
					break;
				case 'ttbkhd/order':
					$KodeTrans = $urlParams[ 'KodeTrans' ];
					switch ( $KodeTrans ) {
						case 'PI':
							$sqlraw = "SELECT ttPIhd.PINo AS NO,ttPIhd.PITgl AS Tgl, tdsupplier.SupKode AS Kode, tdsupplier.SupNama AS Nama, LokasiKode AS MotorNama, 
                                       PITotal AS Total, IFNULL(KBPaid.Paid,0) AS Terbayar,  PITotal - IFNULL(KBPaid.Paid,0) AS Sisa , 0.00 AS Bayar  FROM ttPIhd 
                                       INNER JOIN tdsupplier ON ttPIhd.SupKode = tdsupplier.SupKode 
                                       LEFT OUTER JOIN
                                          (SELECT KBKLINK, IFNULL(SUM(KBKBayar), 0) AS Paid FROM vtKBK WHERE LEFT(KBKLink,2) = 'PI' GROUP BY KBKLINK) KBPaid ON PINo = KBPaid.KBKLINK 
                                       WHERE (PITotal- IFNULL(KBPaid.Paid, 0) > 0)";
							break;
						case 'SR':
							$sqlraw = "SELECT ttSRhd.SRNo AS NO,ttSRhd.SRTgl AS Tgl, tdcustomer.MotorNoPolisi AS Kode, tdcustomer.CusNama AS Nama,tdcustomer.MotorNama, 
                                       SRTotal AS Total, IFNULL(KBPaid.Paid,0) AS Terbayar,  SRTotal - IFNULL(KBPaid.Paid,0) AS Sisa , 0.00 AS Bayar  FROM ttSRhd 
                                       INNER JOIN tdcustomer ON ttSRhd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                                       LEFT OUTER JOIN
                                          (SELECT KBKLINK, IFNULL(SUM(KBKBayar), 0) AS Paid FROM vtKBK WHERE LEFT(KBKLink,2) = 'SR' GROUP BY KBKLINK) KBPaid ON SRNo = KBPaid.KBKLINK 
                                       WHERE (SRTotal- IFNULL(KBPaid.Paid, 0) > 0)";
							break;
					}
					$queryRawPK = [ 'No' ];
					$colGrid    = Ttbkhd::colGridPick();
					break;
                case 'ttbkhd/select-bk':
                    $colGrid    = Ttbkhd::colGridSelectBk();
	                $whereRaw = '';
	                if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
		                $whereRaw = "AND DATE({$json['cmbTgl1']}) >= DATE('{$json['tgl1']}') AND DATE({$json['cmbTgl1']}) <= DATE('{$json['tgl2']}') ";
	                }
	                $KMNo   = $urlParams[ 'KMNo' ];
                    $sqlraw     = "SELECT BKNo AS No, BKTgl, BKMemo, BKNominal, BKJenis, BankKode, SupKode, BKPerson, BKCekNo, 
       				BKCekTempo, BKAC, ttBKhd.UserID, ttBKhd.KodeTrans, NamaAccount 
                    FROM ttBKhd
                    INNER JOIN traccount ON BankKode = NoAccount
                    LEFT OUTER JOIN ttkmit ON ttbkhd.BKNo = ttkmit.KMLink
                    WHERE (ttBKhd.KodeTrans = 'KM') AND  (ttkmit.KMNo is NULL OR ttkmit.KMNo = '{$KMNo}') {$whereRaw}";
                    $queryRawPK = [ 'No' ];
                    break;
			}
		}
		return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Ttbkhd::class,
				'queryRaw'   => $sqlraw,
				'queryRawPK' => $queryRawPK,
				'colGrid'    => $colGrid,
				'joinWith'   => $joinWith,
				'join'       => $join,
				'where'      => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}

    public function actionLoopBankKeluar() {
        $_POST[ 'Action' ] = 'Loop';
        $result            = Ttbkhd::find()->callSP( $_POST );
        if($result[ 'status' ] == 1){
            Yii::$app->session->addFlash( 'warning', $result[ 'keterangan' ] );
        }
	    $postItem[ 'Action' ]  = 'Insert';
	    $postItem[ 'BKNo' ]    = $result[ 'BKNo' ];
	    $postItem[ 'BKLink' ]  = $_POST[ 'BKLink' ];
	    $postItem[ 'BKBayar' ] = $_POST[ 'BKNominal' ];
	    $resultItem            = Ttbkit::find()->callSP( $postItem );
	    if($resultItem[ 'status' ] == 1){
		    Yii::$app->session->addFlash( 'warning', $resultItem[ 'keterangan' ] );
	    }
	    $_POST[ 'Action' ]    = 'LoopAfter';
	    $resultAfter                = Ttbkhd::find()->callSP( $_POST );
	    if($resultAfter[ 'status' ] == 1){
		    Yii::$app->session->addFlash( 'warning', $resultAfter[ 'keterangan' ] );
	    }
    }

    public function actionSelectBk() {
        $this->layout = 'select-mode';
        return $this->render( 'order',
            [
                'arr'     => [
                    'cmbTxt'    => [
                        'No'            => 'No Bank Keluar',
                    ],
                    'cmbTgl'    => [
                        'BKTgl' => 'Tgl Bank Keluar',
                    ],
                    'cmbNum'    => [],
                    'sortname'  => "BKTgl",
                    'sortorder' => 'desc'
                ],
                'options' => ['colGrid'=> Ttbkhd::colGridSelectBk(), 'mode' => self::MODE_SELECT,]
        ] );
    }


	public function actionOrder( $KodeTrans ) {
        $this->layout = 'select-mode';
        switch ( $KodeTrans ) {
            case 'PI':
                return $this->render('order',
                    [
                        'arr'     => [
                            'cmbTxt'    => [
                                'SupKode'       => 'Kode Supplier',
                                'PINo'          => 'No Beli',
                                'SupNama'       => 'Nama Supplier',
                                'LokasiKode'    => 'Lokasi',
                            ],
                            'cmbTgl'    => [
                                'Tgl' => 'Tgl Beli',
                            ],
                            'cmbNum'    => [],
                            'sortname'  => "Tgl",
                            'sortorder' => 'desc'
                        ],
                        'options' => ['colGrid' => Ttbkhd::colGridPickPI(), 'mode' => self::MODE_SELECT_MULTISELECT]
                    ]);
                break;
            case 'SR':
                return $this->render('order',
                    [
                        'arr'     => [
                            'cmbTxt'    => [
                                'CusNama'       => 'Nama Konsumen',
                                'SRNo'          => 'No Retur Jual',
                                'MotorNoPolisi' => 'No Polisi',
                                'MotorNama'    => 'Nama Motor',
                            ],
                            'cmbTgl'    => [
                                'Tgl' => 'Tgl Retur Jual',
                            ],
                            'cmbNum'    => [
                                'SRTotal' => 'Total',
                            ],
                            'sortname'  => "Tgl",
                            'sortorder' => 'desc'
                        ],
                        'options' => ['colGrid' => Ttbkhd::colGridPickSR(), 'mode' => self::MODE_SELECT_MULTISELECT]
                    ]);
                break;
        }
	}
	public function actionBankKeluarUmum() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankKeluarUmum', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbkhd/td' ] )
		] );
	}
	public function actionBankKeluarUmumCreate() {
		$BankKode          = Traccount::find()->where( "LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'" )
		                              ->orderBy( 'NoAccount' )->one();
		$model             = new Ttbkhd();
        $model->BKNo       = General::createTemporaryNo( 'BK', 'Ttbkhd', 'BKNo' );
		$model->BKTgl      = date( 'Y-m-d H:i:s' );
		$model->BKCekTempo = date( 'Y-m-d H:i:s' );
		$model->BKPerson   = "--";
		$model->BKMemo     = "--";
		$model->BKNominal  = 0;
		$model->BKJenis    = "Tunai";
		$model->PosKode    = $this->LokasiKode();
		$model->UserID     = $this->UserID();
		$model->KodeTrans  = "UM";
		$model->BankKode   = ( $BankKode != null ) ? $BankKode->NoAccount : '';
		$model->BKCekNo    = "--";
		$model->BKAC       = "--";
		$model->NoGL       = "--";
		$model->Cetak      = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttbkhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang             = Ttbkhd::find()->ttbkhdFillGetData()->where( [ 'ttbkhd.BKNo' => $model->BKNo ] )->asArray( true )->one();
		$dsTUang[ 'BKLink' ] = '';
		$id                  = $this->generateIdBase64FromModel( $model );
        $dsTUang[ 'BKNoView' ] = $result[ 'BKNo' ];
		return $this->render( 'bank-keluar-umum-create', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionBankKeluarUmumUpdate( $id ) {
        $index = Ttbkhd::getRoute( 'UM', [ 'id' => $id ] )[ 'index' ];
        if ( ! isset( $_GET[ 'action' ] ) ) {
            return $this->redirect( $index );
        }
		/** @var Ttbkhd $model */
        $BankKode  = Traccount::find()->where( "LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'" )
                    ->orderBy( 'NoAccount' )->one();
		$model   = $this->findModelBase64( 'Ttbkhd', $id );
		$dsTUang = Ttbkhd::find()->ttbkhdFillGetData()->where( [ 'ttbkhd.BKNo' => $model->BKNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post               = \Yii::$app->request->post();
            $post[ 'BKTgl' ]    = $post[ 'BKTgl' ] . ' ' . $post[ 'BKJam' ];
			$post[ 'Action' ]   = 'Update';
			$result             = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model = Ttbkhd::findOne($result['BKNo']);
			$id    = $this->generateIdBase64FromModel( $model );
            $post[ 'BKNoView' ] = $result[ 'BKNo' ];
            if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                $post[ 'BKNoView' ] = $result[ 'BKNo' ];
                return $this->redirect( Ttbkhd::getRoute( 'UM', [ 'id' => $id, 'action' => 'display' ] )['update']);
			} else {
				return $this->render( 'bank-keluar-umum-update', [
					'model'   => $model,
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
        if ( ! isset( $_GET[ 'oper' ] ) ) {
            $dsTUang['Action'] = 'Load';
            $result = Ttbkhd::find()->callSP($dsTUang);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        } else {
            $result['BKNo'] = $dsTUang['BKNo'];
        }
		$ttkkit              = Ttbkit::find()->where( [ 'BKNo' => $model->BKNo ] )->one();
		$dsTUang[ 'BKLink' ] = ( $ttkkit == null ) ? '' : $ttkkit->BKLink;
        $dsTUang[ 'BKNoView' ] = $_GET[ 'BKNoView' ] ?? $result[ 'BKNo' ];
		return $this->render( 'bank-keluar-umum-update', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}

	public function actionBankKeluarKeKas() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankKeluarKeKas', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbkhd/td' ] )
		] );
	}
	public function actionBankKeluarKeKasCreate() {
		$BankKode          = Traccount::find()->where( "LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'" )
		                              ->orderBy( 'NoAccount' )->one();
		$model             = new Ttbkhd();
        $model->BKNo       = General::createTemporaryNo( 'BK', 'Ttbkhd', 'BKNo' );
		$model->BKTgl      = date( 'Y-m-d H:i:s' );
		$model->BKCekTempo = date( 'Y-m-d H:i:s' );
		$model->BKPerson   = "--";
		$model->BKMemo     = "--";
		$model->BKNominal  = 0;
		$model->BKJenis    = "Tunai";
		$model->PosKode    = $this->LokasiKode();
		$model->UserID     = $this->UserID();
		$model->KodeTrans  = "KM";
		$model->BankKode   = ( $BankKode != null ) ? $BankKode->NoAccount : '';
		$model->BKCekNo    = "--";
		$model->BKAC       = "--";
		$model->NoGL       = "--";
		$model->Cetak      = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttbkhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang             = Ttbkhd::find()->ttbkhdFillGetData()->where( [ 'ttbkhd.BKNo' => $model->BKNo ] )->asArray( true )->one();
		$ttkkit              = Ttbkit::find()->where( [ 'BKNo' => $model->BKNo ] )->one();
		$dsTUang[ 'BKLink' ] = ( $ttkkit == null ) ? '' : $ttkkit->BKLink;
		$id                  = $this->generateIdBase64FromModel( $model );
        $dsTUang[ 'BKNoView' ] = $result[ 'BKNo' ];
		return $this->render( 'bank-keluar-ke-kas-create', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionBankKeluarKeKasUpdate( $id ) {
        $index = Ttbkhd::getRoute( 'KM', [ 'id' => $id ] )[ 'index' ];
        if ( ! isset( $_GET[ 'action' ] ) ) {
            return $this->redirect( $index );
        }
		/** @var Ttbkhd $model */
        $BankKode  = Traccount::find()->where( "LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'" )
            ->orderBy( 'NoAccount' )->one();
		$model   = $this->findModelBase64( 'Ttbkhd', $id );
		$dsTUang = Ttbkhd::find()->ttbkhdFillGetData()->where( [ 'ttbkhd.BKNo' => $model->BKNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post               = \Yii::$app->request->post();
			$isNew = ( strpos( $post['BKNo'], '=' ) !== false);
			$post[ 'BKTgl' ]    = $post[ 'BKTgl' ] . ' ' . $post[ 'BKJam' ];
			$post[ 'Action' ]   = 'Update';
			$result             = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			if ( $result[ 'status' ] == 0 ) {
				$postit[ 'Action' ]  = $isNew ? 'Insert' : 'Update';
				$postit[ 'BKNo' ]    = $post[ 'BKNo' ];
				$postit[ 'BKBayar' ] = $post[ 'BKNominal' ];
				$postit[ 'BKLink' ]  = $post[ 'BKLink' ];
				$resultit            = Ttbkit::find()->callSP( $postit );
				if ( $resultit[ 'status' ] == 1 ) {
					Yii::$app->session->addFlash( 'warning', $resultit[ 'keterangan' ] );
				}
			}
			$model = Ttbkhd::findOne($result['BKNo']);
			$id    = $this->generateIdBase64FromModel( $model );
            if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                $post[ 'BKNoView' ] = $result[ 'BKNo' ];
                return $this->redirect( Ttbkhd::getRoute( 'KM', [ 'id' => $id, 'action' => 'display' ] ) ['update']);
			} else {
				return $this->render( 'bank-keluar-ke-kas-update', [
					'model'   => $model,
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
        if ( ! isset( $_GET[ 'oper' ] ) ) {
            $dsTUang['Action'] = 'Load';
            $result = Ttbkhd::find()->callSP($dsTUang);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        } else {
            $result['BKNo'] = $dsTUang['BKNo'];
        }
		$ttkkit              = Ttbkit::find()->where( [ 'BKNo' => $model->BKNo ] )->one();
		$dsTUang[ 'BKLink' ] = ( $ttkkit == null ) ? '' : $ttkkit->BKLink;
		$id                  = $this->generateIdBase64FromModel( $model );
        $dsTUang[ 'BKNoView' ] = $_GET[ 'BKNoView' ] ?? $result[ 'BKNo' ];
		return $this->render( 'bank-keluar-ke-kas-update', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionBankKeluarPembelian() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankKeluarPembelian', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbkhd/td' ] )
		] );
	}
	public function actionBankKeluarPembelianCreate() {
		return $this->create( 'PI', 'bank-keluar-pembelian', 'BANK KELUAR - Pembelian' );
	}
	private function create( $BKJenis, $view, $title ) {
		$BankKode          = Traccount::find()->where( "LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'" )
		                              ->orderBy( 'NoAccount' )->one();
		$model             = new Ttbkhd();
        $model->BKNo       = General::createTemporaryNo( 'BK', 'Ttbkhd', 'BKNo' );
		$model->BKTgl      = date( 'Y-m-d H:i:s' );
		$model->BKCekTempo = date( 'Y-m-d H:i:s' );
		$model->BKPerson   = "--";
		$model->BKMemo     = "--";
		$model->BKNominal  = 0;
		$model->BKJenis    = "Tunai";
		$model->PosKode    = $this->LokasiKode();
		$model->UserID     = $this->UserID();
		$model->KodeTrans  = $BKJenis;
		$model->BankKode   = ( $BankKode != null ) ? $BankKode->NoAccount : '';
		$model->BKCekNo    = "--";
		$model->BKAC       = "--";
		$model->NoGL       = "--";
		$model->Cetak      = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttbkhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang             = Ttbkhd::find()->ttbkhdFillGetData()->where( [ 'ttbkhd.BKNo' => $model->BKNo ] )->asArray( true )->one();
		$dsTUang[ 'BKLink' ] = '';
		$id                  = $this->generateIdBase64FromModel( $model );
        $dsTUang[ 'BKNoView' ] = $result[ 'BKNo' ];
		return $this->render( 'create', [
			'model'   => $model,
			'view'    => $view,
			'title'   => $title,
			'dsTUang' => $dsTUang,
			'id'      => $id,
		] );
	}
	public function actionBankKeluarPembelianUpdate( $id ) {
		return $this->update( 'PI', 'bank-keluar-pembelian', 'BANK KELUAR - Pembelian', $id );
	}
	public function update( $BKJenis, $view, $title, $id ) {
        $index = Ttbkhd::getRoute( $BKJenis, [ 'id' => $id ] )[ 'index' ];
        if ( ! isset( $_GET[ 'action' ] ) ) {
            return $this->redirect( $index );
        }
		/** @var Ttbkhd $model */
        $BankKode  = Traccount::find()->where( "LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'" )
            ->orderBy( 'NoAccount' )->one();
		$model   = $this->findModelBase64( 'Ttbkhd', $id );
		$dsTUang = Ttbkhd::find()->ttbkhdFillGetData()->where( [ 'ttbkhd.BKNo' => $model->BKNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post               = \Yii::$app->request->post();
            $post[ 'BKTgl' ]    = $post[ 'BKTgl' ] . ' ' . $post[ 'BKJam' ];
            $post[ 'KodeTrans'] = $BKJenis;
			$post[ 'Action' ]   = 'Update';
			$result             = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model = Ttbkhd::findOne($result['BKNo']);
			$id    = $this->generateIdBase64FromModel( $model );
            $post[ 'BKNoView' ] = $result[ 'BKNo' ];
			if ( $result[ 'status' ] == 0 ) {
                $post[ 'BKNoView' ] = $result[ 'BKNo' ];
                return $this->redirect( Ttbkhd::getRoute( $BKJenis, [ 'id' => $id, 'action' => 'display' ] )['update']);
			} else {
				return $this->render( 'update', [
					'model'   => $model,
					'view'    => $view,
					'title'   => $title,
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
        if ( ! isset( $_GET[ 'oper' ] ) ) {
            $dsTUang['Action'] = 'Load';
            $result = Ttbkhd::find()->callSP($dsTUang);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        } else {
            $result['BKNo'] = $dsTUang['BKNo'];
        }
		$ttkkit              = Ttbkit::find()->where( [ 'BKNo' => $model->BKNo ] )->one();
		$dsTUang[ 'BKLink' ] = ( $ttkkit == null ) ? '' : $ttkkit->BKLink;
		$id                  = $this->generateIdBase64FromModel( $model );
        $dsTUang[ 'BKNoView' ] = $_GET[ 'BKNoView' ] ?? $result[ 'BKNo' ];
		return $this->render( 'update', [
			'model'   => $model,
			'view'    => $view,
			'title'   => $title,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}

	public function actionBankKeluarOrderPengerjaanLuar() {
		$post[ 'Action' ] = 'Display';
		$result           = Ttbkhd::find()->callSP( $post );
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankKeluarOrderPengerjaanLuar', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbkhd/td' ] )
		] );
	}

    public function actionBankKeluarOrderPengerjaanLuarCreate() {
        $BankKode          = Traccount::find()->where( "LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'" )
                                ->orderBy( 'NoAccount' )->one();
        $model             = new Ttbkhd();
        $model->BKNo       = General::createTemporaryNo( 'BK', 'Ttbkhd', 'BKNo' );
        $model->BKTgl      = date( 'Y-m-d H:i:s' );
        $model->BKCekTempo = date( 'Y-m-d H:i:s' );
        $model->BKPerson   = "--";
        $model->BKMemo     = "--";
        $model->BKNominal  = 0;
        $model->BKJenis    = "Tunai";
        $model->PosKode    = $this->LokasiKode();
        $model->UserID     = $this->UserID();
        $model->KodeTrans  = "PL";
        $model->BankKode   = ( $BankKode != null ) ? $BankKode->NoAccount : '';
        $model->BKCekNo    = "--";
        $model->BKAC       = "--";
        $model->NoGL       = "--";
        $model->Cetak      = "Belum";
        $model->save();
        $post             = $model->attributes;
        $post[ 'Action' ] = 'Insert';
        $result           = Ttbkhd::find()->callSP( $post );
        Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        $dsTUang             = Ttbkhd::find()->ttbkhdFillGetData()->where( [ 'ttbkhd.BKNo' => $model->BKNo ] )->asArray( true )->one();
        $ttkkit              = Ttbkit::find()->where( [ 'BKNo' => $model->BKNo ] )->one();
        $dsTUang[ 'BKLink' ] = ( $ttkkit == null ) ? '' : $ttkkit->BKLink;
        $id                  = $this->generateIdBase64FromModel( $model );
        $dsTUang[ 'BKNoView' ] = $result[ 'BKNo' ];
        return $this->render( 'bank-keluar-order-pengerjaan-luar-create', [
            'model'   => $model,
            'dsTUang' => $dsTUang,
            'id'      => $id
        ] );
    }

    public function actionBankKeluarOrderPengerjaanLuarUpdate( $id ) {
        $index = Ttbkhd::getRoute( 'PL', [ 'id' => $id ] )[ 'index' ];
        if ( ! isset( $_GET[ 'action' ] ) ) {
            return $this->redirect( $index );
        }
        /** @var Ttbkhd $model */
        $BankKode  = Traccount::find()->where( "LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'" )
            ->orderBy( 'NoAccount' )->one();
        $model   = $this->findModelBase64( 'Ttbkhd', $id );
        $dsTUang = Ttbkhd::find()->ttbkhdFillGetData()->where( [ 'ttbkhd.BKNo' => $model->BKNo ] )->asArray( true )->one();
        if ( Yii::$app->request->isPost ) {
            $post               = \Yii::$app->request->post();
            $post[ 'Action' ]   = 'Update';
	        $post['BKTgl']      = $post[ 'BKTgl' ].' '.$post[ 'BKJam' ];
            $result             = Ttbkhd::find()->callSP( $post );
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
	        $model = Ttbkhd::findOne($result['BKNo']);
            $id    = $this->generateIdBase64FromModel( $model );
            if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                $post[ 'BKNoView' ] = $result[ 'BKNo' ];
                return $this->redirect( Ttbkhd::getRoute( 'PL', [ 'id' => $id, 'action' => 'display' ] ) ['update'] );
            } else {
                return $this->render( 'bank-keluar-order-pengerjaan-luar-update', [
                    'model'   => $model,
                    'dsTUang' => $post,
                    'id'      => $id,
                ] );
            }
        }
        if ( ! isset( $_GET[ 'oper' ] ) ) {
            $dsTUang['Action'] = 'Load';
            $result = Ttbkhd::find()->callSP($dsTUang);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        } else {
            $result['BKNo'] = $dsTUang['BKNo'];
        }
        $ttkkit              = Ttbkit::find()->where( [ 'BKNo' => $model->BKNo ] )->one();
        $dsTUang[ 'BKLink' ] = ( $ttkkit == null ) ? '' : $ttkkit->BKLink;
        $id                  = $this->generateIdBase64FromModel( $model );
        $dsTUang[ 'BKNoView' ] = $_GET[ 'BKNoView' ] ?? $result[ 'BKNo' ];
        return $this->render( 'bank-keluar-order-pengerjaan-luar-update', [
            'model'   => $model,
            'dsTUang' => $dsTUang,
            'id'      => $id
        ] );
    }

 	public function actionBankKeluarReturPenjualan() {
	    if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankKeluarReturPenjualan', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbkhd/td' ] )
		] );
	}
	public function actionBankKeluarReturPenjualanCreate() {
		return $this->create( 'SR', 'bank-keluar-retur-penjualan', 'BANK KELUAR - Retur Jual' );
	}
	public function actionBankKeluarReturPenjualanUpdate( $id ) {
		return $this->update( 'SR', 'bank-keluar-retur-penjualan', 'BANK KELUAR - Retur Jual', $id );
	}
	public function actionTransferAntarBank() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'TransferAntarBank', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbkhd/td' ] )
		] );
	}
	public function actionTransferAntarBankCreate() {
		TUi::$actionMode = Menu::ADD;
		$model                    = new Ttbkhd();
		$BankKode                 = Traccount::find()->where( "LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'" )
		                                     ->orderBy( 'NoAccount' )->one();
		$dsTUang[ 'BTTgl' ]       = date( 'Y-m-d H:i:s' );
		$dsTUang[ 'BKTgl' ]       = $dsTUang[ 'BTTgl' ];
		$dsTUang[ 'BMTgl' ]       = $dsTUang[ 'BTTgl' ];
		$dsTUang[ 'BKCekTempo' ]  = date( 'Y-m-d H:i:s' );
		$dsTUang[ 'BMCekTempo' ]  = date( 'Y-m-d H:i:s' );
		$dsTUang[ 'BKJenis' ]     = 'Bank Transfer';
		$dsTUang[ 'BMJenis' ]     = 'Bank Transfer';
		$dsTUang[ 'KodeTrans' ]   = 'BT';
		$dsTUang[ 'PosKode' ]     = Menu::getUserLokasi()[ 'PosKode' ];
		$dsTUang[ 'NoGLBK' ]      = '--';
		$dsTUang[ 'NoGLBM' ]      = '--';
		$dsTUang[ 'BKPerson' ]    = '--';
		$dsTUang[ 'BMPerson' ]    = '--';
		$dsTUang[ 'BKMemo' ]      = '--';
		$dsTUang[ 'BKAC' ]        = '--';
		$dsTUang[ 'BMAC' ]        = '--';
		$dsTUang[ 'BKCekNo' ]     = '--';
		$dsTUang[ 'BMCekNo' ]     = '--';
        $dsTUang[ 'BKNo' ]        = '--';
        $dsTUang[ 'BMNo' ]        = '--';
		$dsTUang[ 'BKNominal' ]   = 0;
		$dsTUang[ 'NoAccountBK' ] = ( $BankKode != null ) ? $BankKode->NoAccount : '';
		$dsTUang[ 'NoAccountBM' ] = ( $BankKode != null ) ? $BankKode->NoAccount : '';
        $dsTUang[ 'Action' ]      = 'Insert';
        $result                   = Ttbkhd::find()->callSPTransfer( $dsTUang );
        Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        $dsTUang[ 'BTNo' ]         = $result[ 'BTNo' ];
		return $this->render( 'bank-keluar-transfer-create', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
            'id'      => base64_encode( $dsTUang[ 'BTNo' ] ),
		] );
	}
	public function actionTransferAntarBankUpdate( $id ) {
        $index = Ttbkhd::getRoute( 'BT', [ 'id' => $id ] )[ 'index' ];
        if ( ! isset( $_GET[ 'action' ] ) ) {
            return $this->redirect( $index );
        }
        $action = $_GET[ 'action' ];
        switch ( $action ) {
            case 'UpdateAdd':
                TUi::$actionMode = Menu::ADD;
                break;
            case 'UpdateEdit':
                TUi::$actionMode = Menu::UPDATE;
                break;
            case 'view':
                TUi::$actionMode = Menu::VIEW;
                break;
            case 'display':
                TUi::$actionMode = Menu::DISPLAY;
                break;
        }
		$dsTUang           = Ttbkhd::find()->transferBankGetData( base64_decode( $id ) );
		$dsTUang[ 'BTNo' ] = base64_decode( $id );
		if ( Yii::$app->request->isPost ) {
            $post             = array_merge( $dsTUang ?? [], \Yii::$app->request->post() );
            $post[ 'BTTgl' ]  = $post[ 'BTTgl' ] . ' ' . $post[ 'BTJam' ];
            $post[ 'Action' ] = $action;
            $result           = Ttbkhd::find()->callSPTransfer( $post );
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            $id             = base64_encode( $result[ 'BTNo' ] );
			if ( $result[ 'status' ] == 0 ) {
				return $this->redirect( [ 'ttbkhd/transfer-antar-bank-update','action'=>'display', 'id' => $id ] );
			} else {
				return $this->render( 'bank-keluar-transfer-update', [
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
		$dsTUang           = Ttbkhd::find()->transferBankGetData( base64_decode( $id ) );
        if($dsTUang == null || $dsTUang == []){
            $dsTUang[ 'BTNo' ] = base64_decode( $id );
            $details = Ttbkit::findOne(['BKNo' => $dsTUang[ 'BTNo' ]]);
            $dsTUang           = Ttbkhd::find()->transferBankGetData( $details->BKLink );
            $dsTUang[ 'BTNo' ] = $details->BKLink;
        }else{
            $dsTUang[ 'BTNo' ] = base64_decode( $id );
        }
        if ( ! isset( $_GET[ 'oper' ] ) ) {
            $dsTUang['Action'] = 'Load';
            $result              = Ttbkhd::find()->callSPTransfer( $dsTUang );
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        } else {
            $result['BKNo'] = $dsTUang['BKNo'];
        }
		return $this->render( 'bank-keluar-transfer-update', [
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}

    public function actionBankTransferCancel( $id ) {
        $post             = \Yii::$app->request->post();
        $post[ 'Action' ] = 'Cancel';
        $result           = Ttbkhd::find()->callSPTransfer( $post );
        Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        if ( $result[ 'BTNo' ] == '--' ) {
            return $this->redirect( [ 'ttbkhd/transfer-antar-bank' ] );
        } else {
            return $this->redirect( Ttbkhd::getRoute( 'BT', [ 'action' => 'display', 'id' => base64_encode( $result[ 'BTNo' ] ) ] )[ 'update' ] );
        }
    }

	protected function findModel( $id ) {
		if ( ( $model = Ttbkhd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}

    public function actionDelete()
    {
        /** @var Ttbkhd $model */
        $request = Yii::$app->request;
        $PK = base64_decode($_REQUEST['id']);
        $model = null;
        // $isTransfer = false;
        // if (strpos($PK, 'BT') !== false) {
        //     $details = Ttbkit::findOne(['BKLink' => $PK]);
        //     $PK = $details->BKNo;
        // }
        // $model = Ttbkhd::findOne($PK);
        // $_POST['BKNo'] = $model->BKNo;
        $_POST['Action'] = 'Delete';
        $BKJenis = 'BT';
        if (strpos($PK, 'BT') !== false) {
			$_POST['BTNo'] = $PK;
            $result = Ttbkhd::find()->callSPTransfer($_POST);
        }
        else {
			$model = Ttbkhd::findOne($PK);
        	$BKJenis = $model->KodeTrans;
			$_POST['BKNo'] = $model->BKNo;
            $result = Ttbkhd::find()->callSP($_POST);
        }

        if ($request->isAjax) {
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        } else {
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
            if ($result['BKNo'] == '--') {
                return $this->redirect(Ttbkhd::getRoute($BKJenis)['index']);
            } else {
                $model = Ttbkhd::findOne($result['BKNo']);
                if (empty($model)) {
                    $model = Ttbkhd::find()->where(['BKJenis' => $BKJenis])->andWhere(['not like', 'BKNo', '=',])->one();
                }
                if (empty($model)) {
                    return $this->redirect(Ttbkhd::getRoute($BKJenis)['index']);
                }
                return $this->redirect(Ttbkhd::getRoute($BKJenis, ['action' => 'display', 'id' => $this->generateIdBase64FromModel($model)])['update']);
            }
        }
    }

	public function actionCancel( $id, $action ) {
        /** @var Ttbkhd $model */
        $model = $this->findModelBase64( 'Ttbkhd', $id );
        $KodeTrans          = $model->KodeTrans;
        $_POST[ 'BKNo' ]    = $model->BKNo;
        $_POST[ 'Action' ]  = 'Cancel';
        $post               = \Yii::$app->request->post();
        $post['BKTgl']      = $post[ 'BKTgl' ].' '.$post[ 'BKJam' ];
        $result             = Ttbkhd::find()->callSP( $_POST );
        Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        if($result[ 'BKNo' ] == '--'){
            return $this->redirect( Ttbkhd::getRoute( $KodeTrans, [ 'id' => $this->generateIdBase64FromModel( $model ), 'action'=>'display' ] )[ 'index' ]);
        }else{
            $model = Ttbkhd::findOne($result[ 'BKNo' ]);
            return $this->redirect( Ttbkhd::getRoute( $KodeTrans, [ 'id' => $this->generateIdBase64FromModel( $model ), 'action'=>'display' ] )[ 'update' ]);
        }
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST         = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client        = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response      = $client->post( Yii::$app->params['H2'][$_POST['db']]['host_report'] . '/abengkel/fbkhd', [
			'body' => Json::encode( $_POST )
		] );
        $html                 = $response->getBody();
        return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}

    public function actionJurnal() {
        $post             = \Yii::$app->request->post();
        $post[ 'Action' ] = 'Jurnal';
        $result           = Ttbkhd::find()->callSP( $post );
        Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        return $this->redirect( Ttbkhd::getRoute( $post[ 'KodeTrans' ], [ 'action' => 'display', 'id' => base64_encode( $result[ 'BKNo' ] ) ] )[ 'update' ] );
    }
}
