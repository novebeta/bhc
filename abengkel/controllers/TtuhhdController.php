<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\models\Tdlokasi;
use abengkel\models\Tdsupplier;
use abengkel\models\Ttuhhd;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtuhhdController implements the CRUD actions for Ttuhhd model.
 */
class TtuhhdController extends AbengkelController {
	public function actions() {
		return [
			'td' => [
				'class' => TdAction::class,
				'model' => Ttuhhd::class,
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttuhhd models.
	 * @return mixed
	 */
	public function actionIndex() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttuhhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'index', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttuhhd/td' ] )
		] );
	}
	/**
	 * Displays a single Ttuhhd model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'index', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Finds the Ttuhhd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttuhhd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttuhhd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttuhhd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model               = new Ttuhhd();
		$model->UHNo         = General::createTemporaryNo( 'UH', 'Ttuhhd', 'UHNo' );
		$model->UHTgl        = date( 'Y-m-d H:i:s' );
		$model->UHKeterangan = "--";
		$model->KodeTrans    = "UH";
		$model->PosKode      = $this->LokasiKode();
		$model->Cetak        = "Belum";
		$model->UserID       = $this->UserID();
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttuhhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsSetup               = Ttuhhd::find()->ttuhhdFillByNo()
		                               ->where( [ 'ttuhhd.UHNo' => $model->UHNo ] )->asArray( true )->one();
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsSetup[ 'UHNoView' ] = $result[ 'UHNo' ];
		return $this->render( 'create', [
			'model'   => $model,
			'dsSetup' => $dsSetup,
			'id'      => $id
		] );
	}
	/**
	 * Updates an existing Ttuhhd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$index = \yii\helpers\Url::toRoute( [ 'ttuhhd/index' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$model   = $this->findModelBase64( 'Ttuhhd', $id );
		$dsSetup = Ttuhhd::find()->ttuhhdFillByNo()->where( [ 'ttuhhd.UHNo' => $model->UHNo ] )
		                 ->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post             = array_merge( $dsSetup, \Yii::$app->request->post() );
			$_POST[ 'UHTgl' ] = $_POST[ 'UHTgl' ] . ' ' . $_POST[ 'UHJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttuhhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttuhhd::findOne( $result[ 'UHNo' ] );
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'UHNoView' ] = $result[ 'UHNo' ];
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				return $this->redirect( [ 'ttuhhd/update', 'action' => 'display', 'id' => $id ] );
			} else {
				return $this->render( 'update', [
					'model'   => $model,
					'dsSetup' => $post,
					'id'      => $id,
				] );
			}
		}
		$dsSetup[ 'UHJam' ]  = $dsSetup[ 'UHTgl' ];
		$dsSetup[ 'NoGL' ]   = '--';
		$dsSetup[ 'POTgl' ]  = General::asDate( $dsSetup[ 'UHTgl' ] );
		$dsSetup[ 'Action' ] = 'Load';
		$result              = Ttuhhd::find()->callSP( $dsSetup );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsSetup[ 'UHNoView' ] = $_GET[ 'UHNoView' ] ?? $result[ 'UHNo' ];
		return $this->render( 'update', [
			'model'   => $model,
			'dsSetup' => $dsSetup,
			'id'      => $id
		] );
	}
	/**
	 * Deletes an existing Ttuhhd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
		/** @var Ttuhhd $model */
		$model             = $this->findModelBase64( 'Ttuhhd', $_POST[ 'id' ] );
		$_POST[ 'UHNo' ]   = $model->UHNo;
		$_POST[ 'Action' ] = 'Delete';
		$result            = Ttuhhd::find()->callSP( $_POST );
		if ( $result[ 'status' ] == 0 ) {
			return $this->responseSuccess( $result[ 'keterangan' ] );
		} else {
			return $this->responseFailed( $result[ 'keterangan' ] );
		}
		/*
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
		*/
	}
	public function actionCancel( $id ) {
		/** @var Ttuhhd $model */
		$model             = $this->findModelBase64( 'Ttuhhd', $id );
		$_POST[ 'UHNo' ]   = $model->UHNo;
		$_POST[ 'UHTgl' ]  = $_POST[ 'UHTgl' ] . ' ' . $_POST[ 'UHJam' ];
		$_POST[ 'Action' ] = 'Cancel';
		$result            = Ttuhhd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'UHNo' ] == '--' ) {
			return $this->redirect( [ 'index' ] );
		} else {
			$model = Ttuhhd::findOne( $result[ 'UHNo' ] );
			return $this->redirect( [ 'ttuhhd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client            = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response          = $client->post( Yii::$app->params['H2'][$_POST['db']]['host_report'] . '/abengkel/fuhhd', [
			'body' => Json::encode( $_POST )
		] );
//		return $response->getBody();
        $html                 = $response->getBody();
        return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
}
