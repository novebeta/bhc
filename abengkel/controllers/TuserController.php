<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\TdAction;
use abengkel\models\Tuser;
use abengkel\models\TuserSearch;
use common\components\Custom;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
/**
 * TuserController implements the CRUD actions for Tuser model.
 */
class TuserController extends AbengkelController {
	public function actions() {
		return [
			'td' => [
				'class'    => TdAction::class,
				'model'    => Tuser::class,
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Tuser models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index' );
	}
	/**
	 * Displays a single Tuser model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'index', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Creates a new Tuser model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Tuser();
		if ( $model->load( Yii::$app->request->post() ) ) {
			if ( $_POST[ 'new_pass' ] != '' && $_POST[ 'confirm_pass' ] != '' ) {
				if ( $_POST[ 'new_pass' ] != $_POST[ 'confirm_pass' ] ) {
					$model->addError( 'UserPass', 'Password baru dan konfirmasi tidak sesuai.' );
				} else {
					$model->UserPass = Custom::Encript( $_POST[ 'new_pass' ] );
				}
			}
			if ( $model->hasErrors() ) {
				Yii::$app->getSession()->setFlash( 'error', Html::errorSummary( $model ) );
			} else {
				if ( ! $model->save() ) {
					Yii::$app->getSession()->setFlash( 'error', Html::errorSummary( $model ) );
				}else{
					return $this->redirect(['tuser/index','id'=>$this->generateIdBase64FromModel( $model )]);
				}
			}
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Tuser model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate() {
		/** @var Tuser $model */
		$model = $this->findModel( Yii::$app->user->id );
		if ( $model->load( Yii::$app->request->post() ) ) {
			if ( $_POST['old_pass'] != '' || $_POST['new_pass'] != '' || $_POST['confirm_pass'] != '' ) {
				$old = Custom::Encript( $_POST['old_pass'] );
				if ( $model->UserPass != $old ) {
					$model->addError( 'UserPass', 'Password lama tidak sesuai.' );
				} else {
					if ( $_POST['new_pass'] != $_POST['confirm_pass'] ) {
						$model->addError( 'UserPass', 'Password baru dan konfirmasi tidak sesuai.' );
					} else {
						$model->UserPass = Custom::Encript( $_POST['new_pass'] );
					}
				}
			}
			if ( $model->hasErrors() ) {
				Yii::$app->getSession()->setFlash( 'error', Html::errorSummary( $model ) );
			} else {
				if ( ! $model->save() ) {
					Yii::$app->getSession()->setFlash( 'error', Html::errorSummary( $model ) );
				}
			}
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Tuser model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$model                       = $model = $this->findModelBase64( 'Tuser', $id );
		if ( $model != null ) {
			$result[ 'status' ] = $model->delete();
		}
		if ( $result[ 'status' ] !== false ) {
			return $this->responseSuccess( 'User berhasil dihapus' );
		} else {
			return $this->responseFailed( 'User gagal dihapus' );
		}
	}
	/**
	 * Finds the Tuser model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Tuser the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Tuser::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
