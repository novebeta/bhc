<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\models\Ttsehd;
use abengkel\models\Ttsohd;
use abengkel\models\Vtkbm;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtsehdController implements the CRUD actions for Ttsehd model.
 */
class TtsehdController extends AbengkelController {
	public function actions() {
		$colGrid    = null;
		$joinWith   = [ 'customer' => [
			'type' => 'LEFT JOIN' ] ];
		$join       = [];
        $where      = [
//            [
//                'op'        => 'AND',
//                'condition' => "(SENo NOT LIKE '%=%')",
//                'params'    => []
//            ]
        ];
		$sqlraw     = null;
		$queryRawPK = null;
		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			switch ( $r ) {
				case 'ttsehd/select-sd':
					$colGrid  = Ttsehd::colGrid();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$sqlraw     = "SELECT ttSEhd.*, tdcustomer.CusNama,
					      IFNULL(ttSDhd.SDTgl, DATE('1900-01-01')) AS SDTgl 
					      FROM ttSEhd 
					      INNER JOIN  tdcustomer ON tdcustomer.MotorNoPolisi = ttSEhd.MotorNoPolisi
					      LEFT OUTER JOIN ttSDhd ON ttSEhd.SDNo = ttSDhd.SDNo 
					      WHERE ttSEhd.SENo NOT LIKE '%=%'                              
						  ORDER BY ttsehd.SENo";
					$queryRawPK = [ 'SENo' ];
					break;
			}
		}
		return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Ttsehd::class,
				'queryRaw'   => $sqlraw,
				'queryRawPK' => $queryRawPK,
				'colGrid'    => $colGrid,
				'joinWith'   => $joinWith,
				'join'       => $join,
				'where'      => $where,
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionSelectSd() {
		$urlDetail    = Url::toRoute( [ 'ttseitbarang/index', 'jenis' => 'FillItemSESD' ] );
		$this->layout = 'select-mode';
		return $this->render( 'select', [
			'arr'       => [
				'cmbTxt'    => [
					'SENo'          => 'No SE',
					'SDNo'          => 'No SD',
					'KodeTrans'     => 'Kode Trans',
					'MotorNoPolisi' => 'No Polisi',
					'KarKode'       => 'Karyawan',
					'SEKeterangan'  => 'Keterangan',
					'Cetak'         => 'Cetak',
					'UserID'        => 'UserID',
				],
				'cmbTgl'    => [
					'SETgl' => 'Tanggal SE',
				],
				'cmbNum'    => [
					'SETotalBiaya' => 'Total Biaya',
					'SETotalJasa'  => 'Total Jasa',
					'SETotalPart'  => 'Total Part',
					'SETerbayar'   => 'Terbayar',
					'SETotalWaktu' => 'Total Waktu',
				],
				'sortname'  => "SETgl",
				'sortorder' => 'desc'
			],
			'options'   => [
				'colGrid' => Ttsehd::colGrid(),
				'mode'    => self::MODE_SELECT_DETAIL_MULTISELECT
			],
			'title'     => 'Daftar Service Estimation/ Estimasi Biaya Servis',
			'urlDetail' => $urlDetail
		] );
	}
	public function actionIndex() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttsehd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'index' );
	}
	public function actionCreate() {
		$model                = new Ttsehd();
		$model->SENo          = General::createTemporaryNo( 'SE', 'ttsehd', 'SENo' );
		$model->SETgl         = date( 'Y-m-d H:i:s' );
		$model->SDNo          = "--";
		$model->KodeTrans     = "";
		$model->MotorNoPolisi = "UMUM";
		$model->KarKode       = "--";
		$model->SETotalWaktu  = 0;
		$model->SETotalJasa   = 0;
		$model->SETotalPart   = 0;
		$model->SETotalBiaya  = 0;
		$model->SEKeterangan  = "--";
		$model->UserID        = $this->UserID();
		$model->Cetak         = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttsehd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$id                      = $this->generateIdBase64FromModel( $model );
		$dsTServis               = Ttsehd::find()->ttsehdFillByNo()
		                                 ->where( [ 'ttsehd.SENo' => $model->SENo ] )
		                                 ->asArray( true )->one();
		$dsTServis[ 'SENoView' ] = $result[ 'SENo' ];
		return $this->render( 'create', [
			'model'     => $model,
			'dsTServis' => $dsTServis,
			'id'        => $id
		] );
	}
	public function actionUpdate( $id ) {
		$index = \yii\helpers\Url::toRoute( [ 'ttsehd/index' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		/** @var Ttsehd $model */
		/** @var Ttsehd $dsTServis */
		$model    = $this->findModelBase64( 'Ttsehd', $id );
        if ( $model === null ) {
            Yii::$app->session->addFlash( 'warning', 'SENo : ' . base64_decode( $id ) . ' tidak ditemukan.' );
            return $this->redirect( $index );
        }
		$dsTServis = Ttsehd::find()->ttsehdFillByNo()->where( [ 'ttsehd.SENo' => $model->SENo ] )->asArray( true )->one();
		$Terbayar = floatval($dsTServis['SETerbayar']);
		if ( Yii::$app->request->isPost ) {
			$post             = array_merge( $dsTServis, \Yii::$app->request->post() );
            $post[ 'SETgl' ] = $_POST[ 'SETgl' ] . ' ' . $post[ 'SEJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttsehd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttsehd::findOne( $result[ 'SENo' ] );
            if ( $result[ 'SENo' ] == '--' || $model == null) {
                if ( $model === null ) {
                    Yii::$app->session->addFlash( 'warning', 'SENo : ' . $result[ 'SENo' ] . ' tidak ditemukan.' );
                }
                $index = \yii\helpers\Url::toRoute( [ 'ttsehd/index' ] );
                return $this->redirect( $index );
            }
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'SENoView' ] = $result[ 'SENo' ];
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				return $this->redirect( [ 'ttsehd/update', 'id' => $id, 'action' => 'display', ] );
			} else {
				return $this->render( 'update', [
					'model'     => $model,
					'dsTServis' => $post,
					'id'        => $id,
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTServis[ 'Action' ] = 'Load';
			$result                = Ttsehd::find()->callSP( $dsTServis );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'SENo' ] = $dsTServis[ 'SENo' ];
		}
		$dsJual[ 'SisaBayar' ] = floatval( $dsTServis[ 'SETotalBiaya' ] ) - $Terbayar;
		if ( $dsJual[ 'SisaBayar' ] > 0 || floatval( $dsTServis[ 'SETotalBiaya' ] ) == 0 ) {
			$dsTServis[ 'StatusBayar' ] = 'BELUM';
		} else if ( $dsJual[ 'SisaBayar' ] <= 0 ) {
			$dsTServis[ 'StatusBayar' ] = 'LUNAS';
		}
		$dsTServis[ 'SENoView' ] = $_GET[ 'SENoView' ] ?? $result[ 'SENo' ];
		return $this->render( 'update', [
			'model'     => $model,
			'dsTServis' => $dsTServis,
			'id'        => $id
		] );
	}
	/**
	 * Deletes an existing Ttsehd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
//	public function actionDelete() {
////		/** @var Ttsehd $model */
//		$model             = $model = $this->findModelBase64( 'Ttsehd', $_POST[ 'id' ] );
//		$_POST[ 'SENo' ]   = $model->SENo;
//		$_POST[ 'Action' ] = 'Delete';
//		$result            = Ttsehd::find()->callSP( $_POST );
//		if ( $result[ 'status' ] == 0 ) {
//			return $this->responseSuccess( $result[ 'keterangan' ] );
//		} else {
//			return $this->responseFailed( $result[ 'keterangan' ] );
//		}
//	}

    public function actionDelete()
    {
        /** @var Ttsehd $model */
        $request = Yii::$app->request;
        if ($request->isAjax){
            $model = $this->findModelBase64('Ttsehd', $_POST['id']);
            $_POST['SENo'] = $model->SENo;
            $_POST['Action'] = 'Delete';
            $result = Ttsehd::find()->callSP($_POST);
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            $id = $request->get('id');
            $model = $this->findModelBase64('Ttsehd', $id);
            $_POST['SENo'] = $model->SENo;
            $_POST['Action'] = 'Delete';
            $result = Ttsehd::find()->callSP($_POST);
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['SENo'] == '--') {
                return $this->redirect(['index']);
            } else {
                $model = Ttsehd::findOne($result['SENo']);
                return $this->redirect(['ttsehd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
            }
        }
    }

	public function actionCancel( $id ) {
		/** @var Ttsehd $model */
		$_POST[ 'Action' ] = 'Cancel';
		$_POST[ 'SETgl' ]  = $_POST[ 'SETgl' ] . ' ' . $_POST[ 'SEJam' ];
		$result            = Ttsehd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        $model = Ttsehd::findOne( $result[ 'SENo' ] );
		if ( $result[ 'SENo' ] == '--' || $model == null) {
            if ( $model === null ) {
                Yii::$app->session->addFlash( 'warning', 'SENo : ' . $result[ 'SENo' ] . ' tidak ditemukan.' );
            }
            $index = \yii\helpers\Url::toRoute( [ 'ttsehd/index' ] );
            return $this->redirect( $index );
		} else {
			return $this->redirect( [ 'ttsehd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client            = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response          = $client->post( Yii::$app->params['H2'][$_POST['db']]['host_report'] . '/abengkel/fsehd', [
			'body' => Json::encode( $_POST )
		] );
		$html              = $response->getBody();
		return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	/**
	 * Finds the Ttsehd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttsehd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttsehd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
