<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\components\TUi;
use abengkel\models\Tdjasa;
use Yii;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
/**
 * TdjasaController implements the CRUD actions for Tdjasa model.
 */
class TdjasaController extends AbengkelController {

	public function beforeAction( $action ) {
		$GLOBALS[ 'MenuText' ] = 'Jasa';
		switch ( $action->id ) {
			case 'create':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::ADD );
				break;
			case 'update':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::UPDATE );
				break;
			case 'delete':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::DELETE );
				break;
			case 'index':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::DISPLAY );
				break;
			case 'td':
			case 'cancel':
				$this->isAllow = 1;
				break;
		}
		if($this->isAllow == 0) throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
		return true;
	}

	public function actions() {
		return [
			'td' => [
				'class' => TdAction::class,
				'model' => Tdjasa::class,
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Tdjasa models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index' );
	}
	/**
	 * Displays a single Tdjasa model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'index', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Creates a new Tdjasa model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		TUi::$actionMode   = Menu::ADD;
		$model = new Tdjasa();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [
				'tdjasa/update',
				'id'     => base64_encode($model->JasaKode),
				'action' => 'display'
			] );
		}
		$model->JasaKode            = '--';
		$model->JasaNama            = '--';
		$model->JasaHrgBeli         = 0.00;
		$model->JasaHrgJual         = 0.00;
		$model->JasaWaktuKerja      = 0;
		$model->JasaWaktuSatuan     = "MENIT";
		$model->JasaWaktuKerjaMenit = 0;
		return $this->render( 'create', [
			'model' => $model,
			'id'    => 'new'
		] );
	}
	/**
	 * Updates an existing Tdjasa model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		$model = $this->findModelBase64( 'Tdjasa', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'tdjasa/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
		}
		return $this->render( 'update', [
			'model' => $model,
			'id'    => $this->generateIdBase64FromModel( $model ),
		] );
	}
	/**
	 * Deletes an existing Tdjasa model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Tdjasa model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Tdjasa the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Tdjasa::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	public function actionCancel( $id, $action ) {
		$model = Tdjasa::findOne( $id );
		if ( $model == null ) {
			$model = Tdjasa::find()->one();
			if ( $model == null ) {
				return $this->redirect( [ 'tdjasa/index' ] );
			}
		}
		return $this->redirect( [ 'tdjasa/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
	}
}
