<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\models\Ttpshd;
use abengkel\models\Tttihd;
use abengkel\models\Tttiit;
use Yii;
use yii\db\Expression;
use yii\filters\VerbFilter;
/**
 * TttiitController implements the CRUD actions for Tttiit model.
 */
class TttiitController extends AbengkelController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Tttiit models.
	 * @return mixed
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\web\HttpException
	 */
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Tttiit::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
			/** @var Tttiit $model */
			$model = null;
			if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
				$model = $this->findModelBase64( 'tttiit', $requestData[ 'id' ] );
			}
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'Action' ] = 'Insert';
						$requestData[ 'TIAuto' ] = 0;
					} else {
						$requestData[ 'Action' ] = 'Update';
						if ( $model != null ) {
							$requestData[ 'TINoOLD' ]    = $model->TINo;
							$requestData[ 'TIAutoOLD' ]  = $model->TIAuto;
							$requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
						}
					}
					$result = Tttiit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'batch' :
					\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
					$respon                      = [];
					$base64                      = urldecode( base64_decode( $requestData[ 'header' ] ) );
					parse_str( $base64, $header );
					/** @var Ttpshd $PS */
					$PS = Ttpshd::findOne( $header[ 'PSNo' ] );
					if ( $PS != null ) {
						$header[ 'PSTgl' ] = $PS->PSTgl;
					}
					$header[ 'Action' ] = 'Loop';
					$result             = Tttihd::find()->callSP( $header );
					if($result['status'] == 1) {
						Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
					}
					foreach ( $requestData[ 'data' ] as $item ) {
						$item[ 'Action' ] = 'Insert';
						$result           = Tttiit::find()->callSP( $item );
						if($result['status'] == 1){
							Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
						}
					}
					$header[ 'Action' ] = 'LoopAfter';
					$result             = Tttihd::find()->callSP( $header );
					if($result['status'] == 1) {
						Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
					}
					return $respon;
					break;
				case 'del'  :
					$requestData             = array_merge( $requestData, $model->attributes );
					$requestData[ 'Action' ] = 'Delete';
					if ( $model != null ) {
						$requestData[ 'TINoLD' ]     = $model->TINo;
						$requestData[ 'TIAutoOLD' ]  = $model->TIAuto;
						$requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
					}
					$result = Tttiit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
			}
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'Tttiit.' . $primaryKeys[ $i ];
			}
			$query     = ( new \yii\db\Query() )
				->select( new Expression(
					'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
							    tttiit.TINo,
                                tttiit.TIAuto,
                                tttiit.BrgKode,
                                tttiit.TIQty,
                                tttiit.TIHrgBeli,
                                tdbarang.BrgNama,
                                tdbarang.BrgSatuan,
                                tttiit.TIQty * tttiit.TIHrgBeli AS Jumlah,
                                tttiit.LokasiAsal,
                                tttiit.LokasiTujuan,
                                tdbarang.BrgRak1,
                                tdbarang.BrgGroup'
				) )
				->from( 'tttiit' )
				->join( 'INNER JOIN', 'tdbarang', 'tttiit.BrgKode = tdbarang.BrgKode' )
				->where( [ 'tttiit.TINo' => $requestData[ 'TINo' ] ] );
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
}
