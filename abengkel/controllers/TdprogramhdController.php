<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\components\TUi;
use abengkel\models\Tdprogramhd;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
/**
 * TdprogramhdController implements the CRUD actions for Tdprogramhd model.
 */
class TdprogramhdController extends AbengkelController {


	public function beforeAction( $action ) {
		$GLOBALS[ 'MenuText' ] = 'Program';
		switch ( $action->id ) {
			case 'create':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::ADD );
				break;
			case 'update':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::UPDATE );
				break;
			case 'delete':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::DELETE );
				break;
			case 'index':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::DISPLAY );
				break;
			case 'td':
			case 'cancel':
				$this->isAllow = 1;
				break;
		}
		if($this->isAllow == 0) throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
		return true;
	}

	public function actions() {
		return [
			'td' => [
				'class' => TdAction::class,
				'model' => Tdprogramhd::class
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Tdprogramhd models.
	 * @return mixed
	 */
	public function actionIndex() {
        $post[ 'Action' ] = 'Display';
        $result           = Tdprogramhd::find()->callSP( $post );
        if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        }
        return $this->render( 'index', [
            'mode'    => '',
            'url_add' => Url::toRoute( [ 'tdprogramhd/td' ] )
        ] );
	}
	/**
	 * Displays a single Tdprogramhd model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Creates a new Tdprogramhd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		TUi::$actionMode   = Menu::ADD;
        $model                = new Tdprogramhd();
        $model->PrgNama       = General::createTemporaryNo( 'PD', 'tdprogramhd', 'PrgNama' );
        $model->PrgTglMulai   = date( 'Y-m-d H:i:s' );
        $model->PrgTglAkhir   = date( 'Y-m-d H:i:s' );
        $model->PrgMemo       = '--';
        $model->save();
        $post                 = $model->attributes;
        $post[ 'Action' ]     = 'Insert';
        $result               = Tdprogramhd::find()->callSP( $post );
        Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        $dsSetup              = Tdprogramhd::find()->tdprogramhdFillByNo()->where( [ 'tdprogramhd.PrgNama' => $model->PrgNama ] )->asArray( true )->one();
        $id                   = $this->generateIdBase64FromModel( $model );
        $dsSetup[ 'PrgNamaView'] = $result[ 'PrgNama' ];
        return $this->render( 'create', [
            'model'   => $model,
            'dsSetup' => $dsSetup,
            'id'      => $id,
        ] );
	}
	/**
	 * Updates an existing Tdprogramhd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
        /** @var Tdprogramhd $model */
        $model      = $this->findModelBase64( 'Tdprogramhd', $id );
        $dsSetup              = Tdprogramhd::find()->tdprogramhdFillByNo()->where( [ 'tdprogramhd.PrgNama' => $model->PrgNama ] )->asArray( true )->one();
        if ( Yii::$app->request->isPost ) {
            $post                   = \Yii::$app->request->post();
            $post[ 'PrgNamaBaru' ]  = $post[ 'PrgNama' ];
            $post[ 'Action' ]       = 'Update';
            $result                 = Tdprogramhd::find()->callSP( $post );
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            $id                     = $this->generateIdBase64FromModel( $model );
            $post[ 'PrgNamaView' ]     = $result[ 'PrgNama' ];
            if ( $result[ 'status' ] == 0 ) {
                return $this->redirect( [ 'tdprogramhd/index', 'id' => $id ] );
            } else {
                return $this->render( 'update', [
                    'model'     => $model,
                    'dsSetup'   => $post,
                    'id'        => $id,
                ] );
            }
        }
        $dsSetup[ 'Action' ] = 'Load';
        $result              = Tdprogramhd::find()->callSP( $dsSetup );
        Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        $dsSetup[ 'PrgNamaView' ]     = $result[ 'PrgNama' ];
        return $this->render( 'update', [
            'model'     => $model,
            'dsSetup'   => $dsSetup,
            'id'        => $id
        ] );
	}
	/**
	 * Deletes an existing Tdprogramhd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
        /** @var Tdprogramhd $model */
        $model              = $this->findModelBase64( 'Tdprogramhd', $_POST[ 'id' ] );
        $_POST[ 'PrgNama' ] = $model->PrgNama;
        $_POST[ 'Action' ]  = 'Delete';
        $result             = Tdprogramhd::find()->callSP( $_POST );
        if ( $result[ 'status' ] == 0 ) {
            return $this->responseSuccess( $result[ 'keterangan' ] );
        } else {
            return $this->responseFailed( $result[ 'keterangan' ] );
        }
	}

    public function actionCancel( $id, $action ) {
        /** @var  $model Tdprogramhd */
        $model            = $this->findModelBase64( 'Tdprogramhd', $id );
        $post[ 'PrgNama' ]   = $model->PrgNama;
        $post[ 'Action' ] = 'Cancel';
        $result = Tdprogramhd::find()->callSP( $post );
        Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        if ( Yii::$app->request->isAjax ) {
            return '';
        }
        return $this->redirect( [ 'index' ] );
    }

    public function actionPrint( $id ) {
        unset( $_POST[ '_csrf-app' ] );
        $_POST         = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
        $_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
        $client        = new Client( [
            'headers' => [ 'Content-Type' => 'application/octet-stream' ]
        ] );
        $response      = $client->post( Yii::$app->params['H2'][$_POST['db']]['host_report'] . '/abengkel/fprogramhd', [
            'body' => Json::encode( $_POST )
        ] );
        $html                 = $response->getBody();
        return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
    }
	/**
	 * Finds the Tdprogramhd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Tdprogramhd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Tdprogramhd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
