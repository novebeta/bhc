<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\models\Ttcchd;
use abengkel\models\Ttccit;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtccitController implements the CRUD actions for Ttccit model.
 */
class TtccitController extends AbengkelController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttccit::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
			/** @var Ttccit $model */
			$model = null;
			if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
				$model = $this->findModelBase64( 'ttccit', $requestData[ 'id' ] );
			}
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'Action' ] = 'Insert';
						$requestData[ 'CCNo' ]   = 0;
					} else {
						$requestData[ 'Action' ] = 'Update';
						if ( $model != null ) {
							$requestData[ 'CCNoOLD' ] = $model->CCNo;
							$requestData[ 'SDNoOLD' ] = $model->SDNo;
						}
					}
					$result = Ttccit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'batch' :
					\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
					$respon                      = $this->responseFailed( 'Add Item' );
					$header                      = $requestData[ 'header' ];
					$header[ 'Action' ]          = 'Loop';
					$result                      = Ttcchd::find()->callSP( $header );
					foreach ( $requestData[ 'data' ] as $item ) {
						$items[ 'Action' ] = 'Insert';
						$items[ 'CCNo' ]   = $header[ 'CCNo' ];
						$items[ 'SDNo' ]   = $item[ 'data' ][ 'NO' ];
						$items[ 'CCPart' ] = $item[ 'data' ][ 'SDTotalPart' ];
						$items[ 'CCJasa' ] = $item[ 'data' ][ 'SDTotalJasa' ];
						$result            = Ttccit::find()->callSP( $items );
					}
					return $this->responseSuccess( $requestData );
					break;
				case 'del'  :
					$requestData             = array_merge( $requestData, $model->attributes );
					$requestData[ 'Action' ] = 'Delete';
					if ( $model != null ) {
						$requestData[ 'CCNoOLD' ] = $model->CCNo;
						$requestData[ 'SDNoOLD' ] = $model->SDNo;
					}
					$result = Ttccit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
			}
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'Ttccit.' . $primaryKeys[ $i ];
			}
			$query = new Query();
			$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,ttccit.CCNo, ttccit.SDNo, ttccit.CCPart, ttccit.CCJasa, ttsdhd.SDTgl, ttsdhd.MotorNoPolisi, 
					ttccit.CCPart + ttccit.CCJasa AS CCTotal, tdcustomer.MotorType, tdcustomer.MotorNama, ttsdhd.SVNo, tdcustomer.CusNama' ) )
			      ->from( "ttccit" )
			      ->innerJoin( "ttsdhd", "ttccit.SDNo = ttsdhd.SDNo" )
			      ->innerJoin( "tdcustomer", "ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi" )
			      ->groupBy( "ttccit.SDNo, ttsdhd.SVNo, tdcustomer.CusNama" )
			      ->where( "(ttccit.CCNo = :CCNo)", [ ':CCNo' => $requestData[ 'CCNo' ] ] );
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	protected function findModel( $CCNo, $SDNo ) {
		if ( ( $model = Ttccit::findOne( [ 'CCNo' => $CCNo, 'SDNo' => $SDNo ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
