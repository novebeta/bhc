<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\models\Tdbaranglokasi;
use abengkel\models\Tdlokasi;
use abengkel\models\Tdsupplier;
use abengkel\models\Ttpihd;
use abengkel\models\Vtkbk;
use common\components\Api;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtpihdController implements the CRUD actions for Ttpihd model.
 */
class TtpihdController extends AbengkelController {

    public function beforeAction( $action ) {
        if ( ! parent::beforeAction( $action ) ) {
            return false;
        }
        $GLOBALS[ 'MenuText' ] = '[PI] Invoice Pembelian';
        switch ( $action->id ) {
            case 'create':
                $this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::ADD );
                break;
            case 'update':
                $get = Yii::$app->request->get('action','view');
                $this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], ($get === 'view') ? Menu::VIEW : Menu::UPDATE );
                break;
            case 'delete':
                $this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::DELETE );
                break;
            case 'index':
                $this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::DISPLAY );
                break;
            case 'print':
                $this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::print );
                break;
            case 'td':
            case 'cancel':
                $this->isAllow = 1;
                break;
        }
        if ( $this->isAllow == 0 ) {
            //throw new ForbiddenHttpException( Yii::t( 'yii', 'You are not allowed to perform this action.' ) );
        }
        return true;
    }

    public function actions() {
        $colGrid    = null;
        $joinWith   = [];
        $join       = [];
        $where      = [
            [
                'op'        => 'AND',
                'condition' => "(PINo NOT LIKE '%=%')",
                'params'    => []
            ]
        ];
        $sqlraw     = null;
        $queryRawPK = null;
        if ( isset( $_POST[ 'query' ] ) ) {
            $json      = Json::decode( $_POST[ 'query' ], true );
            $r         = $json[ 'r' ];
            $urlParams = $json[ 'urlParams' ];
            switch ( $r ) {
                case 'ttpihd/select':
                    $colGrid    = Ttpihd::colGridPick();
                    $whereRaw = '';
                    if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
                        $whereRaw = " DATE(ttpihd.PITgl) BETWEEN '{$json[ 'tgl1' ]}' AND '{$json['tgl2']}'";
                    }
                    $sqlraw = "SELECT ttpihd.* FROM (SELECT ttpihd.PINo AS rowid, ttpihd.*
                                FROM ttpihd
                                INNER JOIN ttpiit ON ttpiit.PINo = ttpihd.PINo
                                WHERE PIQty > PRQtyS
                                GROUP BY ttpihd.PINo) ttpihd 
                                WHERE {$whereRaw}
                                ORDER BY ttpihd.PINo";
                    $queryRawPK = [ 'PINo' ];
                    break;
            }
        }
		return [
			'td' => [
				'class' => \abengkel\components\TdAction::class,
				'model' => Ttpihd::class,
                'queryRaw'   => $sqlraw,
                'queryRawPK' => $queryRawPK,
                'joinWith'   => $joinWith,
                'where' => $where,
                'colGrid'    => $colGrid,
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttpihd models.
	 * @return mixed
	 */
	public function actionIndex() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttpihd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'index', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttpihd/td' ] )
		] );
	}
	/**
	 * Creates a new Ttpihd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$LokasiKodeCmb       = Tdlokasi::find()->where( [ 'LokasiStatus' => 'A', 'LEFT(LokasiNomor,1)' => '7' ] )->orderBy( 'LokasiStatus, LokasiKode' )->one();
		$SupplierKodeCmb     = Tdsupplier::find()->where( [ 'SupStatus' => 'A', 'LEFT(SupStatus,1)' => 'D' ] )->orderBy( 'SupStatus, SupKode' )->one();
		$model               = new Ttpihd();
		$model->PINo         = General::createTemporaryNo( 'PI', 'Ttpihd', 'PINo' );
		$model->PITgl        = date( 'Y-m-d H:i:s' );
		$model->PITglTempo   = date( 'Y-m-d H:i:s' );
		$model->PIKeterangan = "--";
		$model->LokasiKode   = ( $LokasiKodeCmb != null ) ? $LokasiKodeCmb->LokasiKode : '';
		$model->SupKode      = ( $SupplierKodeCmb != null ) ? $SupplierKodeCmb->SupKode : '';
		$model->PITerm       = 10;
		$model->PISubTotal   = 0;
		$model->PIDiscFinal  = 0;
		$model->PITotalPajak = 0;
		$model->PIBiayaKirim = 0;
		$model->PITotal      = 0;
		$model->PITerbayar   = 0;
		$model->UserID       = $this->UserID();
		$model->KodeTrans    = "PI";
		$model->PSNo         = "--";
		$model->NoGL         = "--";
		$model->PINoRef      = "--";
		$model->PIJenis      = 'Tunai';
		$model->PosKode      = $this->LokasiKode();
		$model->Cetak        = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttpihd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTBeli                   = Ttpihd::find()->ttpihdFillByNo()->where( [ 'ttpihd.PINo' => $model->PINo ] )
		                                   ->asArray( true )->one();
		$dsTBeli[ 'StatusBayar' ]  = 'Belum';
		$dsTBeli[ 'PIDiscPersen' ] = 0;
		$dsTBeli[ 'PSTgl' ]        = date( 'Y-m-d H:i:s' );
		$id                        = $this->generateIdBase64FromModel( $model );
		$dsTBeli[ 'PINoView' ]     = $result[ 'PINo' ];
        $dsTBeli[ 'PPNPI']         = 0;
		return $this->render( 'create', [
			'model'   => $model,
			'dsTBeli' => $dsTBeli,
			'id'      => $id
		] );
	}
	/**
	 * Updates an existing Ttpihd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$index = \yii\helpers\Url::toRoute( [ 'ttpihd/index' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		/** @var Ttpihd $model */
		$model    = $this->findModelBase64( 'Ttpihd', $id );
		$dsTBeli  = Ttpihd::find()->ttpihdFillByNo()->where( [ 'ttpihd.PINo' => $model->PINo ] )
		                  ->asArray( true )->one();
		$Terbayar = floatval($dsTBeli['PITerbayar']); //Vtkbk::find()->getTotalBayar( $model->PINo );
//		if ( $model->PITerbayar != $Terbayar ) {
//			$model->PITerbayar = $Terbayar;
//			$model->save();
//		}
		if ( Yii::$app->request->isPost ) {
			$post             = array_merge( $dsTBeli, \Yii::$app->request->post() );
			$post[ 'PITgl' ]  = $post[ 'PITgl' ] . ' ' . $post[ 'PIJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttpihd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttpihd::findOne( $result[ 'PINo' ] );
            if ( $result[ 'PINo' ] == '--' || $model == null) {
                if ( $model === null ) {
                    Yii::$app->session->addFlash( 'warning', 'PINo : ' . $result[ 'PINo' ]. ' tidak ditemukan.' );
                }
                return $this->redirect( $index);
            }
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'PINoView' ] = $result[ 'PINo' ];
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				return $this->redirect( [ 'ttpihd/update', 'id' => $id, 'action' => 'display', ] );
			} else {
				return $this->render( 'update', [
					'model'   => $model,
					'dsTBeli' => $post,
					'id'      => $id,
				] );
			}
		}
		$dsTBeli[ 'PIDiscPersen' ] = 0;
		$dsTBeli[ 'StatusBayar' ] = 'Belum';
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTBeli[ 'Action' ] = 'Load';
			$result              = Ttpihd::find()->callSP( $dsTBeli );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'PINo' ] = $dsTBeli[ 'PINo' ];
		}
		$dsTBeli[ 'SisaBayar' ] = floatval( $dsTBeli[ 'PITotal' ] ) - $Terbayar;
		if ( $dsTBeli[ 'SisaBayar' ] > 0 || floatval( $dsTBeli[ 'PITotal' ] ) == 0 ) {
			$dsTBeli[ 'StatusBayar' ] = 'BELUM';
		} else if ( $dsTBeli[ 'SisaBayar' ] <= 0 ) {
			$dsTBeli[ 'StatusBayar' ] = 'LUNAS';
		}
		$dsTBeli[ 'PINoView' ] = $_GET[ 'PINoView' ] ?? $result[ 'PINo' ];
        $dsTBeli['PPNPI'] = $dsTBeli['fPPNPI(ttpihd.PINo)'];
		return $this->render( 'update', [
			'model'   => $model,
			'dsTBeli' => $dsTBeli,
			'id'      => $id
		] );
	}
	/**
	 * Deletes an existing Ttpihd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
//	public function actionDelete() {
//		/** @var Ttpihd $model */
//		$model             = $model = $this->findModelBase64( 'Ttpihd', $_POST[ 'id' ] );
//		$_POST[ 'PINo' ]   = $model->PINo;
//		$_POST[ 'Action' ] = 'Delete';
//		$result            = Ttpihd::find()->callSP( $_POST );
//		if ( $result[ 'status' ] == 0 ) {
//			return $this->responseSuccess( $result[ 'keterangan' ] );
//		} else {
//			return $this->responseFailed( $result[ 'keterangan' ] );
//		}
//	}

    public function actionDelete()
    {
        /** @var Ttpihd $model */
        $request = Yii::$app->request;
        if ($request->isAjax){
            $model = $this->findModelBase64('Ttpihd', $_POST['id']);
            $_POST['PINo'] = $model->PINo;
            $_POST['Action'] = 'Delete';
            $result = Ttpihd::find()->callSP($_POST);
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            $id = $request->get('id');
            $model = $this->findModelBase64('Ttpihd', $id);
            $_POST['PINo'] = $model->PINo;
            $_POST['Action'] = 'Delete';
            $result = Ttpihd::find()->callSP($_POST);
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['PINo'] == '--') {
                return $this->redirect(['index']);
            } else {
                $model = Ttpihd::findOne($result['PINo']);
                return $this->redirect(['ttpihd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
            }
        }
    }

	public function actionCancel( $id, $action ) {
		/** @var Ttpihd $model */
		$model             = $this->findModelBase64( 'Ttpihd', $id );
		$_POST[ 'PINo' ]   = $model->PINo;
		$_POST[ 'PIJam' ]  = $_POST[ 'PITgl' ] . ' ' . $_POST[ 'PIJam' ];
		$_POST[ 'Action' ] = 'Cancel';
		$result            = Ttpihd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'PINo' ] == '--' ) {
			return $this->redirect( [ 'index' ] );
		} else {
			$model = Ttpihd::findOne( $result[ 'PINo' ] );
			return $this->redirect( [ 'ttpihd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client            = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response          = $client->post( Yii::$app->params['H2'][$_POST['db']]['host_report'] . '/abengkel/fpihd', [
			'body' => Json::encode( $_POST )
		] );
        $html                 = $response->getBody();
        return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	/**
	 * Finds the Ttpihd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttpihd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttpihd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	public function actionCheckStock( $BrgKode, $LokasiKode ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return Tdbaranglokasi::find()
		                     ->where( 'BrgKode = :BrgKode AND LokasiKode = :LokasiKode', [ 'BrgKode' => $BrgKode, 'LokasiKode' => $LokasiKode ] )
		                     ->asArray()->one();
	}
	public function actionJurnal() {
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Jurnal';
		$result           = Ttpihd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( [ 'ttpihd/update', 'action' => 'display', 'id' => base64_encode( $result[ 'PINo' ] ) ] );
	}

    public function actionStatusOtomatis() {
        $this->layout   = 'popup-form';
        return $this->render( '_formStatus', [ 'data' => $_GET ] );
    }

    public function actionStatusOtomatisItem( $PINo ) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $dt = General::cCmd( "SELECT KBKNo, KbKTgl, KBKLink, KBKBayar, Kode, Person, Memo, NoGL FROM vtkbk WHERE KBKLink = :PINo ORDER BY KbKTgl;", [':PINo' => $PINo ] )->queryAll();
        return [
            'rows' => $dt
        ];

    }

    public function actionImport()
    {
        $this->layout = 'select-mode';
        return $this->render('import', [
            'colGrid' => Ttpihd::colGridImportItems(),
            'mode' => self::MODE_SELECT_DETAIL_MULTISELECT,
        ]);
    }

    public function actionImportItems()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $fromTime = Yii::$app->formatter->asDate('-90 day', 'yyyy-MM-dd');
        $toTime = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
		$addParam = [];
        if (isset($_POST['query'])) {
            $query = Json::decode($_POST['query'], true);
            $fromTime = $query['tgl1'] ?? $fromTime;
            $toTime = $query['tgl2'] ?? $toTime;
			if($query['txt1'] !== ''){
				$addParam = array_merge_recursive($addParam,[$query['cmbTxt1']=> $query['txt1']]);
			}
        }
        $api = new Api();
        $mainDealer = $api->getMainDealer();
        $arrHasil = [];
        switch ($mainDealer) {
            case API::MD_HSO:
                $arrParams = [
                    'fromTime' => $fromTime . ' 00:00:00',
                    'toTime' => $toTime . ' 23:59:59',
                    'dealerId' => $api->getDealerId(),
                ];

                break;
            case API::MD_ANPER:
                $arrParams = [
                    'fromTime' => $fromTime . ' 00:00:00',
                    'toTime' => $toTime . ' 23:59:59',
                    'dealerId' => $api->getDealerId(),
                    'requestServerTime' => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                    'requestServertimezone' => date('T'),
                    'requestServerTimestamp' => $api->getCurrUnixTime(),
                ];
                break;
			default:
				$arrParams = [
					'fromTime'               => $fromTime . ' 00:00:00',
					'toTime'                 => $toTime . ' 23:59:59',
					'dealerId'               => $api->getDealerId(),
					'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
					'requestServertimezone'  => date('T'),
					'requestServerTimestamp' => $api->getCurrUnixTime(),
				];
        }
		$arrParams = array_merge_recursive($arrParams,$addParam);
		$filterResult = [];
		if($query['txt2'] !== ''){
			$filterResult = [
				'key' => $query['cmbTxt2'],
				'value' => $query['txt2'],
			];
		}
        $hasil = $api->call(Api::TYPE_PINB, Json::encode($arrParams),$filterResult);
        $arrHasil = Json::decode($hasil);
        if (!isset($arrHasil['data'])) {
            return [
                'rows' => [],
                'rowsCount' => 0
            ];
        }
        $_SESSION['ttpihd']['import'] = $arrHasil['data'];
        return [
            'rows' => $arrHasil['data'],
            'rowsCount' => sizeof($arrHasil['data'])
        ];
    }

    public function actionImportDetails()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $key = array_search($_POST['noPenerimaan'], array_column($_SESSION['ttpihd']['import'], 'noPenerimaan'));
        return [
            'rows' => $_SESSION['ttpihd']['import'][$key]['po'],
            'rowsCount' => sizeof($_SESSION['ttpihd']['import'][$key]['po'])
        ];
    }


	public function actionImportLoop(){
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$respon = [];
		$base64 = urldecode(base64_decode($requestData['header']));
		parse_str($base64, $header);
		$requestData['data'][0]['header']['data']['Action'] = 'ImporBefore';
		$requestData['data'][0]['header']['data']['NoTrans'] = $header['PINo'];
		$result = Api::callPINB($requestData['data'][0]['header']['data']);
		Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		$model = Ttpihd::findOne($result['NoTrans']);
		$id = $this->generateIdBase64FromModel($model);
		$respon['id'] = $id;
		foreach ($requestData['data'] as $item) {
			$item['data']['Action'] = 'Impor';
			$item['data']['NoTrans'] = $model->PINo;
			$result = Api::callPINBit($item['data']);
			Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		}
		$requestData['data'][0]['header']['data']['Action'] = 'ImporAfter';
		$result = Api::callPINB($requestData['data'][0]['header']['data']);
		return $respon;
	}

    public function actionSelect() {
        $urlDetail    = Url::toRoute( [ 'ttpiit/index', 'jenis' => 'FillItemPIPR' ] );
        $this->layout = 'select-mode';
        return $this->render( 'select',
            [
                'arr'       => [
                    'cmbTxt'    => [
                        'PINo'         => 'No PI',
                    ],
                    'cmbTgl'    => [
                        'PITgl' => 'Tgl PI',
                    ],
                    'cmbNum'    => [
                        'PITotal' => 'Total',
                    ],
                    'sortname'  => "PITgl",
                    'sortorder' => 'desc'
                ],
                'title'     => 'Daftar Purchase Invoice / Invoice Pembelian',
                'options'   => [ 'colGrid' => Ttpihd::colGridPick(), 'mode' => self::MODE_SELECT_DETAIL_MULTISELECT ],
                'urlDetail' => $urlDetail
            ] );
    }
}
