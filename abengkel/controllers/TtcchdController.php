<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\components\TUi;
use abengkel\models\Tdlokasi;
use abengkel\models\Ttcchd;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtcchdController implements the CRUD actions for Ttcchd model.
 */
class TtcchdController extends AbengkelController {
	public function actions() {
		$colGrid    = null;
		$joinWith   = [];
		$join       = [];
        $where      = [
            [
                'op'        => 'AND',
                'condition' => "(CCNo NOT LIKE '%=%')",
                'params'    => []
            ]
        ];
		$queryRaw   = null;
		$queryRawPK = null;

		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			switch ( $r ) {
				case 'ttcchd/order':
                    $where                  = [
                        [
                            'op'        => 'AND',
                            'condition' => "(NO NOT LIKE '%=%')",
                            'params'    => []
                        ]
                    ];
					$queryRaw   = "SELECT ttSDhd.SDNo AS NO, ttSDhd.SDTgl AS Tgl, tdcustomer.MotorNoPolisi AS Kode, 
                                tdcustomer.CusNama AS Nama, tdcustomer.MotorNama, SDTotalJasa, SDTotalPart, SDTotalJasa + SDTotalPart AS SDTotal 
                                FROM ttSDhd 
                                INNER JOIN tdcustomer  ON ttSDhd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                                WHERE KlaimC2 = 'Belum' AND ttSDhd.KodeTrans = 'TC13' AND ttSDhd.SVNo <> '--'";
					$queryRawPK = [ 'NO' ];
					$colGrid    = Ttcchd::colGridPick();
			}
		}
		return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Ttcchd::class,
				'queryRaw'   => $queryRaw,
				'queryRawPK' => $queryRawPK,
				'colGrid'    => $colGrid,
				'joinWith'   => $joinWith,
				'join'       => $join,
				'where'      => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionOrder() {
		$this->layout = 'select-mode';
		return $this->render( 'order',
			[
				'arr'     => [
					'cmbTxt'    => [
						'CusNama'       => 'Nama Konsumen',
						'SVNo'          => 'No Servis',
						'MotorNoPolisi' => 'No Polisi',
						'MotorNama'     => 'Nama Motor',
					],
					'cmbTgl'    => [
						'Tgl' => 'Tgl Servis',
					],
					'cmbNum'    => [
						'SDTotal' => 'Total',
					],
					'sortname'  => "Tgl",
					'sortorder' => 'desc'
				],
				'options' => [ 'colGrid' => Ttcchd::colGridPick(), 'mode' => self::MODE_SELECT_MULTISELECT ]
			] );
	}
	/**
	 * Lists all Ttcchd models.
	 * @return mixed
	 */
	public function actionIndex() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttcchd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'index', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttcchd/td' ] )
		] );
	}
	/**
	 * Displays a single Ttcchd model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	/*
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	*/
	/**
	 * Finds the Ttcchd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttcchd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttcchd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttcchd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$LokasiKodeCmb      = Tdlokasi::find()->where( [ 'LokasiStatus' => 'A', 'LEFT(LokasiNomor,1)' => '7' ] )->orderBy( 'LokasiStatus, LokasiKode' )->one();
		$model              = new Ttcchd();
		$model->CCNo        = General::createTemporaryNo( 'CC', 'Ttcchd', 'CCNo' );
		$model->CCTgl       = Yii::$app->formatter->asDate( 'now', 'yyyy-MM-dd H:i:s' );
		$model->CCTotalPart = 0;
		$model->CCTotalJasa = 0;
		$model->CCNoKlaim   = "--";
		$model->CCMemo      = "--";
		$model->NoGL        = "--";
		$model->Cetak       = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttcchd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTServis               = Ttcchd::find()->ttcchdFillByNo()->where( [ 'ttcchd.CCNo' => $model->CCNo ] )->asArray( true )->one();
		$id                      = $this->generateIdBase64FromModel( $model );
		$dsTServis[ 'CCNoView' ] = $result[ 'CCNo' ];
		return $this->render( 'create', [
			'model'     => $model,
			'dsTServis' => $dsTServis,
			'id'        => $id
		] );
	}
	/**
	 * Updates an existing Ttcchd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$index = \yii\helpers\Url::toRoute( [ 'ttcchd/index' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		/** @var Ttcchd $model */
		$model     = $this->findModelBase64( 'Ttcchd', $id );
		$dsTServis = Ttcchd::find()->ttcchdFillByNo()->where( [ 'ttcchd.CCNo' => $model->CCNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post             = array_merge( $dsTServis, \Yii::$app->request->post() );
			$post[ 'CCTgl' ]  = $post[ 'CCTgl' ] . ' ' . $post[ 'CCJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttcchd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttcchd::findOne( $result[ 'CCNo' ] );
//            $model     = null;
            if ( $result[ 'CCNo' ] == '--' || $model == null) {
                if ( $model === null ) {
                    Yii::$app->session->addFlash( 'warning', 'CCNo : ' . $result[ 'CCNo' ]. ' tidak ditemukan.' );
                }
                return $this->redirect( $index);
            }
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'CCNoView' ] = $result[ 'CCNo' ];
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				return $this->redirect( [ 'ttcchd/update', 'action' => 'display', 'id' => $id ] );
			} else {
				return $this->render( 'update', [
					'model'     => $model,
					'dsTServis' => $post,
					'id'        => $id,
				] );
			}
		}
		$dsTServis[ 'Action' ] = 'Load';
		$result                = Ttcchd::find()->callSP( $dsTServis );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTServis[ 'CCNoView' ] = $_GET[ 'CCNoView' ] ?? $result[ 'CCNo' ];
		return $this->render( 'update', [
			'model'     => $model,
			'dsTServis' => $dsTServis,
			'id'        => $id
		] );
	}
	/**
	 * Deletes an existing Ttcchd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
//	public function actionDelete() {
//		/** @var Ttcchd $model */
//		$model             = $model = $this->findModelBase64( 'Ttcchd', $_POST[ 'id' ] );
//		$_POST[ 'CCNo' ]   = $model->CCNo;
//		$_POST[ 'Action' ] = 'Delete';
//		$result            = Ttcchd::find()->callSP( $_POST );
//		if ( $result[ 'status' ] == 0 ) {
//			return $this->responseSuccess( $result[ 'keterangan' ] );
//		} else {
//			return $this->responseFailed( $result[ 'keterangan' ] );
//		}
//	}
	public function actionCancel( $id, $action ) {
//		/** @var Ttcchd $model */
//		$model             = $this->findModelBase64( 'Ttcchd', $id );
//		$_POST[ 'CCNo' ]   = $model->CCNo;
		$_POST[ 'Action' ] = 'Cancel';
		$_POST[ 'CCTgl' ]  = $_POST[ 'CCTgl' ] . ' ' . $_POST[ 'CCJam' ];
		$result            = Ttcchd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        $model             = Ttcchd::findOne( $result[ 'CCNo' ] );
        if ( $result[ 'CCNo' ] == '--' || $model == null) {
            if ( $model === null ) {
                Yii::$app->session->addFlash( 'warning', 'CCNo : ' . $result[ 'CCNo' ]. ' tidak ditemukan.' );
            }
            return $this->redirect(['index']);
        } else {
			return $this->redirect( [ 'ttcchd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client            = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response          = $client->post( Yii::$app->params['H2'][$_POST['db']]['host_report'] . '/abengkel/fcchd', [
			'body' => Json::encode( $_POST )
		] );
        $html                 = $response->getBody();
        return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	public function actionJurnal() {
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Jurnal';
		$result           = Ttcchd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( [ 'ttcchd/update', 'action' => 'display', 'id' => base64_encode( $result[ 'CCNo' ] ) ] );
	}

    public function actionDelete()
    {
        /** @var Ttcchd $model */
        $request = Yii::$app->request;
        if ($request->isAjax){
            $model = $this->findModelBase64('Ttcchd', $_POST['id']);
            $_POST['CCNo'] = $model->CCNo;
            $_POST['Action'] = 'Delete';
            $result = Ttcchd::find()->callSP($_POST);
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            $id = $request->get('id');
            $model = $this->findModelBase64('Ttcchd', $id);
            $_POST['CCNo'] = $model->CCNo;
            $_POST['Action'] = 'Delete';
            $result = Ttcchd::find()->callSP($_POST);
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['CCNo'] == '--') {
                return $this->redirect(['index']);
            } else {
                $model = Ttcchd::findOne($result['CCNo']);
                return $this->redirect(['ttcchd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
            }
        }
    }
}
