<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\models\Tdkaryawan;
use abengkel\models\Tdlokasi;
use abengkel\models\Ttuchd;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtuchdController implements the CRUD actions for Ttuchd model.
 */
class TtuchdController extends AbengkelController {
	public function actions() {
		return [
			'td' => [
				'class' => TdAction::class,
				'model' => Ttuchd::class,
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttuchd models.
	 * @return mixed
	 */
	public function actionIndex() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttuchd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'index', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttuchd/td' ] )
		] );
	}
	/**
	 * Finds the Ttuchd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttuchd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttuchd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttuchd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model               = new Ttuchd();
		$model->UCNo         = General::createTemporaryNo( 'UC', 'Ttuchd', 'UCNo' );
		$model->UCTgl        = date( 'Y-m-d H:i:s' );
		$model->KodeTrans    = 'UC';
		$model->PosKode      = $this->LokasiKode();
		$model->UCHrgLama    = 0;
		$model->UCHrgLama    = 0;
		$model->UCSelisih    = 0;
		$model->UCKeterangan = "--";
		$model->UserID       = $this->UserID();
		$model->NoGL         = "--";
		$model->Cetak        = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttuchd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsSetup               = Ttuchd::find()->ttuchdFillByNo()->where( [ 'ttuchd.UCNo' => $model->UCNo ] )
		                               ->asArray( true )->one();
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsSetup[ 'UCNoView' ] = $result[ 'UCNo' ];
		return $this->render( 'create', [
			'model'   => $model,
			'dsSetup' => $dsSetup,
			'id'      => $id,
		] );
	}
	/**
	 * Updates an existing Ttuchd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$index = \yii\helpers\Url::toRoute( [ 'ttuchd/index' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		/** @var Ttuchd $model */
		$model   = $this->findModelBase64( 'Ttuchd', $id );
		$dsSetup = Ttuchd::find()->ttuchdFillByNo()->where( [ 'ttuchd.UCNo' => $model->UCNo ] )
		                 ->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post             = array_merge( $dsSetup, \Yii::$app->request->post() );
			$_POST[ 'UCTgl' ] = $_POST[ 'UCTgl' ] . ' ' . $_POST[ 'UCJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttuchd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttuchd::findOne( $result[ 'UCNo' ] );
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'UCNoView' ] = $result[ 'UCNo' ];
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				return $this->redirect( [ 'ttuchd/update', 'action' => 'display', 'id' => $id ] );
			} else {
				return $this->render( 'update', [
					'model'   => $model,
					'dsSetup' => $post,
					'id'      => $id,
				] );
			}
		}
		$dsSetup[ 'Action' ] = 'Load';
		$result              = Ttuchd::find()->callSP( $dsSetup );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsSetup[ 'UCNoView' ] = $result[ 'UCNo' ];
		$dsSetup[ 'UCNoView' ] = $_GET[ 'UCNoView' ] ?? $result[ 'UCNo' ];
		return $this->render( 'update', [
			'model'   => $model,
			'dsSetup' => $dsSetup,
			'id'      => $id
		] );
	}
	/**
	 * Deletes an existing Ttuchd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
		/** @var Ttuchd $model */
		$model             = $this->findModelBase64( 'Ttuchd', $_POST[ 'id' ] );
		$_POST[ 'UCNo' ]   = $model->UCNo;
		$_POST[ 'Action' ] = 'Delete';
		$result            = Ttuchd::find()->callSP( $_POST );
		if ( $result[ 'status' ] == 0 ) {
			return $this->responseSuccess( $result[ 'keterangan' ] );
		} else {
			return $this->responseFailed( $result[ 'keterangan' ] );
		}
	}
	public function actionCancel( $id, $action ) {
		/** @var Ttuchd $model */
		$model             = $this->findModelBase64( 'Ttuchd', $id );
		$_POST[ 'UCNo' ]   = $model->UCNo;
		$_POST[ 'UCTgl' ]  = $_POST[ 'UCTgl' ] . ' ' . $_POST[ 'UCJam' ];
		$_POST[ 'Action' ] = 'Cancel';
		$result            = Ttuchd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'UCNo' ] == '--' ) {
			return $this->redirect( [ 'index' ] );
		} else {
			$model = Ttuchd::findOne( $result[ 'UCNo' ] );
			return $this->redirect( [ 'ttuchd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client            = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response          = $client->post( Yii::$app->params['H2'][$_POST['db']]['host_report'] . '/abengkel/fuchd', [
			'body' => Json::encode( $_POST )
		] );
//		return $response->getBody();
        $html                 = $response->getBody();
        return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
}
