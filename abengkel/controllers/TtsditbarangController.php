<?php

namespace abengkel\controllers;
use abengkel\models\Ttsditjasa;
use Yii;
use abengkel\components\AbengkelController;
use abengkel\models\Ttsditbarang;
use abengkel\models\Ttsdhd;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtsditbarangController implements the CRUD actions for Ttsditbarang model.
 */
class TtsditbarangController extends AbengkelController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ttsditbarang models.
     * @return mixed
     */
    public function actionIndex()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $requestData                 = \Yii::$app->request->post();
        $primaryKeys                 = Ttsditbarang::getTableSchema()->primaryKey;
        if ( isset( $requestData[ 'oper' ] ) ) {
            /* operation : create, edit or delete */
            $oper = $requestData[ 'oper' ];
            /** @var Ttsditbarang $model */
            $model = null;
            if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
                $model = $this->findModelBase64( 'ttsditbarang', $requestData[ 'id' ] );
            }
            // \common\components\Custom::genSP($requestData, 'fsditbarang');
            switch ( $oper ) {
                case 'add'  :
                case 'edit' :
                    if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
                        $requestData[ 'Action' ] = 'Insert';
                        $requestData[ 'SDAuto' ] = 1;
                    } else {
                        $requestData[ 'Action' ] = 'Update';
                        if ( $model != null ) {
                            $requestData[ 'SDNoOLD' ]   = $model->SDNo;
                            $requestData[ 'SDAutoOLD' ] = $model->SDAuto;
                            $requestData[ 'BrgKodeOLD' ] = $model->BrgKode;

                        }
                    }
                    $requestData[ 'KodeTrans' ] = $requestData[ 'KodeTransBarang' ];
                    $requestData[ 'PrgNama' ] = $requestData[ 'PrgNamaBarang' ];
                    $result = Ttsditbarang::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
                    break;
                case 'batch' :
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    $respon                      = [];
                    $base64                      = urldecode( base64_decode( $requestData[ 'header' ] ) );
                    parse_str( $base64, $header );
                    /** @var Ttsdhd $SD */
                    $SD = Ttsdhd::findOne($header[ 'SDNo' ]);
                    if($SD != null){
                        $header[ 'SDTgl' ] = $SD->SDTgl;
                    }

                    $header[ 'Action' ] = 'Loop';
                    $result             = Ttsdhd::find()->callSP( $header );
                    Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
                    foreach ( $requestData[ 'data' ] as $item ) {
                        if($item['JenisMat'] == 'Barang'){
                            $item[ 'Action' ]       = 'Insert';
                            $item[ 'KodeTrans' ]       = $header[ 'KodeTrans' ];
                            $result                 = Ttsditbarang::find()->callSP( $item );
                            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
                        }
                        else {
                            $item['JasaKode']       =  $item['BrgKode'];
                            $item[ 'Action' ]       = 'Insert';
                            $result                 = Ttsditjasa::find()->callSP( $item );
                            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
                        }
                    }
	                $header[ 'Action' ] = 'LoopAfter';
	                $result             = Ttsdhd::find()->callSP( $header );
	                Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
	                return $respon;
                    break;
                case 'del'  :
                    $requestData             = array_merge( $requestData, $model->attributes );
                    $requestData[ 'Action' ] = 'Delete';
                    if ( $model != null ) {
                        $requestData[ 'SDNoOLD' ]   = $model->SDNo;
                        $requestData[ 'SDAutoOLD' ] = $model->SDAuto;
                        $requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
                    }
                    $result = Ttsditbarang::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
                    break;
            }
        } else {
            for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
                $primaryKeys[ $i ] = 'Ttsditbarang.' . $primaryKeys[ $i ];
            }

            $jenis = $_GET['jenis'] ?? ' ';
            switch ( $jenis ) {
                case 'FillItemSDSI':
                    $query     = ( new \yii\db\Query() )
                        ->select( new Expression(
                            'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
                            	ttSDitBarang.SDNo, ttSDitBarang.SDAuto, ttSDitBarang.BrgKode, ttSDitBarang.SDQty , ttSDitBarang.SDHrgBeli, ttSDitBarang.SDHrgJual, ttSDitBarang.SDJualDisc, ttSDitBarang.SDPajak, tdbarang.BrgNama, tdbarang.BrgSatuan, 0 AS Disc, 
                                ttSDitBarang.SDQty * ttSDitBarang.SDHrgJual - ttSDitBarang.SDJualDisc AS Jumlah, tdbarang.BrgGroup, ttSDhd.LokasiKode'
                        ) )
                        ->from( 'ttSDitBarang' )
                        ->join( 'INNER JOIN', 'tdbarang', 'ttSDitBarang.BrgKode = tdbarang.BrgKode' )
                        ->join( 'INNER JOIN', 'ttSDhd', 'ttSDhd.SDNo = ttSDitBarang.SDNo' )
                        ->where( [ 'ttSDitBarang.SDNo' => $requestData[ 'SDNo' ] ] )
                        ->orderBy('SDNo');
                    break;
                default:
                    $query     = ( new \yii\db\Query() )
                        ->select( new Expression(
                            'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
                            	ttsditbarang.SDNo, ttsditbarang.SDAuto, ttsditbarang.BrgKode, ttsditbarang.SDQty,
                                ttsditbarang.SDHrgBeli, ttsditbarang.SDHrgJual, ttsditbarang.SDJualDisc,
                                ttsditbarang.SDPajak, tdbarang.BrgNama, tdbarang.BrgSatuan, tdbarang.BrgHrgJual,
                                IFNULL( ttsditbarang.SDJualDisc / ttsditbarang.SDHrgJual * 100, 0 ) AS Disc,
                                ttsditbarang.SDQty * ttsditbarang.SDHrgJual - ttsditbarang.SDJualDisc AS Jumlah,
                                tdbarang.BrgGroup, ttsditbarang.SDPickingNo, ttsditbarang.SDPickingDate, ttsditbarang.LokasiKode, ttsditbarang.Cetak'
                        ) )
                        ->from( 'ttsditbarang' )
                        ->join( 'INNER JOIN', 'tdbarang', 'ttsditbarang.BrgKode = tdbarang.BrgKode' )
                        ->where( [ 'ttsditbarang.SDNo' => $requestData[ 'SDNo' ] ] )
                        ->orderBy('	ttsditbarang.SDNo,ttsditbarang.SDAuto');
            }

            $rowsCount = $query->count();
            $rows      = $query->all();
            /* encode row id */
            for ( $i = 0; $i < count( $rows ); $i ++ ) {
                $rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
            }
            $response = [
                'rows'      => $rows,
                'rowsCount' => $rowsCount
            ];
        }
        return $response;
    }

    /**
     * Finds the Ttsditbarang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $SDNo
     * @param integer $SDAuto
     * @return Ttsditbarang the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($SDNo, $SDAuto)
    {
        if (($model = Ttsditbarang::findOne(['SDNo' => $SDNo, 'SDAuto' => $SDAuto])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
