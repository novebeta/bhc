<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\components\TUi;
use abengkel\models\Tdbarang;
use abengkel\models\TdbarangSearch;
use common\components\General;
use Yii;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
/**
 * TdbarangController implements the CRUD actions for Tdbarang model.
 */
class TdbarangController extends AbengkelController {

	public function beforeAction( $action ) {
		$GLOBALS[ 'MenuText' ] = 'Spare Part';
		switch ( $action->id ) {
			case 'create':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::ADD );
				break;
			case 'update':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::UPDATE );
				break;
			case 'delete':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::DELETE );
				break;
			case 'index':
			case 'get-saldo-qty':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::DISPLAY );
				break;
			case 'td':
			case 'cancel':
				$this->isAllow = 1;
				break;
		}
		if($this->isAllow == 0) throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
		return true;
	}
	public function actions() {
		return [
			'td' => [
				'class'    => TdAction::class,
				'model'    => Tdbarang::class,
				'joinWith' => [
					'barangGroup' => [
						'type' => 'INNER JOIN'
					]
				]
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Tdbarang models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index' );
	}

	public function actionAjaxCombo(){
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return General::cCmd( "SELECT BrgKode, BrgNama, BrgBarCode, BrgGroup, BrgSatuan, BrgHrgBeli, 
       BrgHrgJual, BrgRak1, BrgMinStock, BrgMaxStock, BrgStatus FROM tdbarang WHERE BrgStatus = 'A' Order By BrgStatus, BrgKode" )->queryAll();
	}

	public function actionGetSaldoQty($BrgKode){
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$qty = General::cCmd( "SELECT SUM(SaldoQty)  FROM tdbaranglokasi WHERE BrgKode = :BrgKode;",[':BrgKode' => $BrgKode] )->queryScalar();
		return $qty === null ? 0 : $qty;
	}

	/**
	 * Finds the Tdbarang model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Tdbarang the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Tdbarang::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 *
	 */
	public function actionFind() {
		$params = \Yii::$app->request->post();
		$rows   = [];
		if ( $params[ 'mode' ] === 'combo' ) {
			$rows = Tdbarang::find()->select2( $params[ 'value' ], $params[ 'label' ], [], [
				'condition'  => $params[ 'condition' ],
				'params' => $params[ 'params' ]
			], $params[ 'allOption' ] );
		}
		return $this->responseDataJson( $rows );
	}
	/**
	 * Creates a new Tdbarang model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		TUi::$actionMode   = Menu::ADD;
		$model = new Tdbarang();
		$model->BrgRak1 = '--';
		$model->BrgRak2 = '--';
		$model->BrgRak3 = '--';
		$model->BrgSatuan = 'PCS';
		$model->TglUpdate = date( 'Y-m-d' );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'tdbarang/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
		}
		return $this->render( 'create', [
			'model' => $model,
            'id'    => 'new'
		] );
	}
	/**
	 * Updates an existing Tdbarang model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		$model = $this->findModelBase64( 'Tdbarang', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'tdbarang/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
		}
		return $this->render( 'update', [
			'model' => $model,
			'id'    => $this->generateIdBase64FromModel( $model ),
		] );
	}
	/**
	 * Deletes an existing Tdbarang model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$model                       = $model = $this->findModelBase64( 'Tdbarang', $id );
		if ( $model != null ) {
			$result[ 'status' ] = $model->delete();
		}
		if ( $result[ 'status' ] !== false ) {
			return $this->responseSuccess( 'Spartpart berhasil dihapus' );
		} else {
			return $this->responseFailed( 'Spartpart gagal dihapus' );
		}
	}

    public function actionCancel( $id, $action ) {
        $model = Tdbarang::findOne( $id );
        if ( $model == null ) {
            $model = Tdbarang::find()->one();
            if ( $model == null ) {
                return $this->redirect( [ 'tdbarang/index' ] );
            }
        }
        return $this->redirect( [ 'tdbarang/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
    }
}
