<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\TdAction;
use abengkel\models\Traccount;
use abengkel\models\TraccountSearch;
use abengkel\models\Ttgeneralledgerhd;
use Yii;
use yii\bootstrap\Html;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TraccountController implements the CRUD actions for Traccount model.
 */
class TraccountController extends AbengkelController {
	public function actions() {
		return [
			'td' => [
				'class'    => TdAction::class,
				'model'    => Traccount::class,
				'joinWith' => [
					'parent' => [
						'type' => 'INNER JOIN'
					]
				]
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Traccount models.
	 * @return mixed
	 */
	public function actionIndex() {
		

		return $this->render( 'index' );
	}
	public function actionDataPerkiraan() {
		return $this->render( 'DataPerkiraan', [
			'aktiva'       => Json::encode( Traccount::find()->getAccount( '10000000' ) ),
			'pasiva'       => Json::encode( Traccount::find()->getAccount( '20000000' ) ),
			'sales'        => Json::encode( Traccount::find()->getAccount( '30000000' ) ),
			'pend_ope'     => Json::encode( Traccount::find()->getAccount( '40000000' ) ),
			'hpp'          => Json::encode( Traccount::find()->getAccount( '50000000' ) ),
			'biaya_ope'    => Json::encode( Traccount::find()->getAccount( '60000000' ) ),
			'biaya_non'    => Json::encode( Traccount::find()->getAccount( '80000000' ) ),
			'pend_non_ope' => Json::encode( Traccount::find()->getAccount( '90000000' ) ),
		] );
	}
	public function actionNeracaAwal() {
		$tglAwal = Ttgeneralledgerhd::find()->TglSaldoAwal();
		return $this->render( 'NeracaAwal', [
			'tglAwal'      => $tglAwal,
			'aktiva'       => Json::encode( Traccount::find()->getAccount( '10000000' ) ),
			'pasiva'       => Json::encode( Traccount::find()->getAccount( '20000000' ) ),
			'sales'        => Json::encode( Traccount::find()->getAccount( '30000000' ) ),
			'pend_ope'     => Json::encode( Traccount::find()->getAccount( '40000000' ) ),
			'hpp'          => Json::encode( Traccount::find()->getAccount( '50000000' ) ),
			'biaya_ope'    => Json::encode( Traccount::find()->getAccount( '60000000' ) ),
			'biaya_non'    => Json::encode( Traccount::find()->getAccount( '80000000' ) ),
			'pend_non_ope' => Json::encode( Traccount::find()->getAccount( '90000000' ) ),
		] );
	}
	/**
	 * Creates a new Traccount model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionDataPerkiraanCreate() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$model                       = new Traccount();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return [
				'status' => true,
				'msg'    => 'Data perkiraan berhasil disimpan.',
				'record' => Traccount::find()->getAccount( $_POST['RootID'] ),
			];
		}
		return [ 'status' => false, 'msg' => 'Data perkiraan gagal disimpan .' . Html::errorSummary( $model ) ];
	}
	/**
	 * Updates an existing Traccount model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDataPerkiraanUpdate() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$id                          = $_POST['NoAccountOld'];
		$model                       = $this->findModel( $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return [
				'status' => true,
				'msg'    => 'Data perkiraan berhasil disimpan.',
				'record' => Traccount::find()->getAccount( $_POST['RootID'] ),
			];
		}
		return [ 'status' => false, 'msg' => 'Data perkiraan gagal disimpan .' . Html::errorSummary( $model ) ];
	}
	/**
	 * Deletes an existing Traccount model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDataPerkiraanDelete() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//		$this->findModel( $_POST['NoAccount'] )->delete();
		if ( $this->findModel( $_POST['NoAccount'] )->delete() ) {
			return [
				'status' => true,
				'msg'    => 'Data perkiraan berhasil dihapus.',
				'record' => Traccount::find()->getAccount( $_POST['RootID'] )
			];
		}
		return [ 'status' => false, 'msg' => 'Data perkiraan gagal dihapus .' ];
	}
	public function actionGetAccount( $NoAccount ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//		return Traccount::find()
//		              ->with(['parent'])
//			->asArray()
//		              ->all();
//		return  ArrayHelper::map(Traccount::find()->all(), 'NoAccount', function ($model) {
//			return ArrayHelper::toArray($model->parent);
//		});
		$induk   = Traccount::find()
		                    ->select(
			                    [
				                    'NoAccount',
				                    'CONCAT(NoAccount," ",NamaAccount) AS label',
				                    'IF(JenisAccount ="Detail","true","false") AS isLeaf',
				                    new Expression( '"0" as level' ),
				                    new Expression( '"true" as loaded' ),
				                    new Expression( '"true" as expanded' )
			                    ] )
		                    ->where( [ 'NoAccount' => $NoAccount ] )->asArray()->one();
		$child   = Traccount::find()->getChild( $NoAccount );
		$child[] = $induk;
		return [
			'rows' => $child
		];
	}
	/**
	 * Creates a new Traccount model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Traccount();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'id' => $model->NoAccount ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Traccount model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$model = $this->findModelBase64( 'Traccount', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'traccount/index' ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Traccount model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete() {
		$this->findModel( $_POST['NoAccount'] )->delete();
		return $this->redirect( [ 'traccount/data-perkiraan' ] );
	}
	/**
	 * Finds the Traccount model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Traccount the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Traccount::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
