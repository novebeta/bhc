<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\models\Traccount;
use abengkel\models\Ttkmhd;
use abengkel\models\Ttkmit;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtkmhdController implements the CRUD actions for Ttkmhd model.
 */
class TtkmhdController extends AbengkelController {
	public function actions() {
		$colGrid               = null;
		$joinWith              = [];
		$join                  = [];
		$where                 = [];
		$queryRaw              = null;
		$queryRawPK            = null;
		$joinWith[ 'account' ] = [
			'type' => 'INNER JOIN'
		];
		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			switch ( $r ) {
				case 'ttkmhd/kas-masuk-umum':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttKMhd.KodeTrans = 'UM' AND ttKMhd.KMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkmhd/kas-masuk-dari-bank':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttKMhd.KodeTrans = 'BK' AND ttKMhd.KMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkmhd/kas-masuk-service-invoice':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttKMhd.KodeTrans = 'SV' AND ttKMhd.KMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkmhd/kas-masuk-sales-invoice':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttKMhd.KodeTrans = 'SI' AND ttKMhd.KMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkmhd/kas-masuk-estimasi-servis':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttKMhd.KodeTrans = 'SE' AND ttKMhd.KMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkmhd/kas-masuk-order-penjualan':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttKMhd.KodeTrans = 'SO' AND ttKMhd.KMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkmhd/kas-masuk-retur-pembelian':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttKMhd.KodeTrans = 'PR' AND ttKMhd.KMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkmhd/order':
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$KodeTrans = $urlParams[ 'KodeTrans' ];
					switch ( $KodeTrans ) {
						case 'SV':
							$queryRaw = "SELECT ttSDhd.SVNo AS NO, ttSDhd.SVTgl AS Tgl, tdcustomer.MotorNoPolisi AS Kode, tdcustomer.CusNama AS Nama, tdcustomer.MotorNama, 
                            SDTotalBiaya AS Total, IFNULL(KBPaid.Paid, 0) AS Terbayar, SDTotalBiaya - IFNULL(KBPaid.Paid, 0) AS Sisa, 0.00 AS Bayar 
                            FROM ttSDhd 
                            INNER JOIN tdcustomer  ON ttSDhd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                            LEFT OUTER JOIN 
                            (SELECT KBMLINK, IFNULL(SUM(KBMBayar), 0) AS Paid FROM vtkbm WHERE LEFT(KBMLink, 2) = 'SV' GROUP BY KBMLINK) KBPaid 
                            ON SVNo = KBPaid.KBMLINK 
                            WHERE SVNo <> '--' AND (SDTotalBiaya - IFNULL(KBPaid.Paid, 0) > 0)";
							break;
						case 'SI':
							$queryRaw = "SELECT ttSIhd.SINo AS NO, ttSIhd.SITgl AS Tgl, tdcustomer.MotorNoPolisi AS Kode, tdcustomer.CusNama AS Nama, tdcustomer.MotorNama, 
       						SITotal AS Total, IFNULL(KBPaid.Paid, 0) AS Terbayar, SITotal - IFNULL(KBPaid.Paid, 0) AS Sisa, 0.00 AS Bayar 
                            FROM ttSIhd 
                            INNER JOIN tdcustomer ON ttSIhd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                            LEFT OUTER JOIN 
                            (SELECT KBMLINK, IFNULL(SUM(KBMBayar), 0) AS Paid FROM vtkbm WHERE LEFT(KBMLink, 2) = 'SI' GROUP BY KBMLINK) KBPaid 
                            ON SINo = KBPaid.KBMLINK 
                            WHERE (SITotal - IFNULL(KBPaid.Paid, 0) > 0)";
							break;
						case 'SO':
							$queryRaw = "SELECT ttsohd.SONo AS NO, ttsohd.SOTgl AS Tgl, tdcustomer.MotorNoPolisi AS Kode, tdcustomer.CusNama AS Nama, tdcustomer.MotorNama, 
       						SOTotal AS Total, IFNULL(KBPaid.Paid, 0) AS Terbayar, SOTotal - IFNULL(KBPaid.Paid, 0) AS Sisa, 0.00 AS Bayar 
                            FROM ttsohd 
                            INNER JOIN tdcustomer ON ttsohd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                            LEFT OUTER JOIN 
                            (SELECT KBMLINK, IFNULL(SUM(KBMBayar), 0) AS Paid FROM vtkbm WHERE LEFT(KBMLink, 2) = 'SO' 
                            GROUP BY KBMLINK) KBPaid ON SONo = KBPaid.KBMLINK 
                            WHERE (SOTotal - IFNULL(KBPaid.Paid, 0) > 0)";
							break;
						case 'PR':
							$queryRaw = "SELECT ttPRhd.PRNo AS NO, ttPRhd.PRTgl AS Tgl, tdsupplier.SupKode AS Kode, tdsupplier.SupNama AS Nama, LokasiKode AS MotorNama, 
       						PRTotal AS Total, IFNULL(KBPaid.Paid, 0) AS Terbayar, PRTotal - IFNULL(KBPaid.Paid, 0) AS Sisa, 0.00 AS Bayar 
                            FROM ttPRhd 
                            INNER JOIN tdsupplier ON ttPRhd.SupKode = tdsupplier.SupKode 
                            LEFT OUTER JOIN 
                            (SELECT KBMLINK, IFNULL(SUM(KBMBayar), 0) AS Paid FROM vtkbm WHERE LEFT(KBMLink, 2) = 'PR' 
                            GROUP BY KBMLINK) KBPaid ON PRNo = KBPaid.KBMLINK 
                            WHERE (PRTotal - IFNULL(KBPaid.Paid, 0) > 0)";
							break;
						case 'SE':
							$queryRaw = "SELECT ttsehd.seNo AS NO, ttsehd.seTgl AS Tgl, tdcustomer.MotorNoPolisi AS Kode, tdcustomer.CusNama AS Nama, tdcustomer.MotorNama, 
       						SETotalBiaya AS Total, IFNULL(KBPaid.Paid, 0) AS Terbayar, SETotalBiaya - IFNULL(KBPaid.Paid, 0) AS Sisa, 0.00 AS Bayar 
                            FROM ttsehd 
                            INNER JOIN tdcustomer ON ttsehd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                            LEFT OUTER JOIN 
                            (SELECT KBMLINK, IFNULL(SUM(KBMBayar), 0) AS Paid FROM vtkbm WHERE LEFT(KBMLink, 2) = 'SE' 
                            GROUP BY KBMLINK) KBPaid ON seNo = KBPaid.KBMLINK 
                            WHERE (SETotalBiaya - IFNULL(KBPaid.Paid, 0) > 0)";
							break;
					}
					$queryRawPK = [ 'No' ];
					$colGrid    = Ttkmhd::colGridPick();
					break;
			}
		}
		return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Ttkmhd::class,
				'queryRaw'   => $queryRaw,
				'queryRawPK' => $queryRawPK,
				'colGrid'    => $colGrid,
				'joinWith'   => $joinWith,
				'join'       => $join,
				'where'      => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionLoopKasMasuk() {
//		$_POST[ 'Action' ] = 'Loop';
//		$result            = Ttkmhd::find()->callSP( $_POST );
//        if($result[ 'status' ] == 1){
//            Yii::$app->session->addFlash( 'warning', $result[ 'keterangan' ] );
//        }
        $_POST[ 'Action' ]     = 'Loop';
        $result                = Ttkmhd::find()->callSP( $_POST );
        if($result[ 'status' ] == 1){
            Yii::$app->session->addFlash( 'warning', $result[ 'keterangan' ] );
        }
        $postItem[ 'Action' ]  = 'Insert';
        $postItem[ 'KMNo' ]    = $result[ 'KMNo' ];
        $postItem[ 'KMLink' ]  = $_POST[ 'KMLink' ];
        $postItem[ 'KMBayar' ] = $_POST[ 'KMNominal' ];
        $resultItem            = Ttkmit::find()->callSP( $postItem );
        if($resultItem[ 'status' ] == 1){
            Yii::$app->session->addFlash( 'warning', $resultItem[ 'keterangan' ] );
        }
        $_POST[ 'Action' ]    = 'LoopAfter';
        $resultAfter                = Ttkmhd::find()->callSP( $_POST );
        if($resultAfter[ 'status' ] == 1){
            Yii::$app->session->addFlash( 'warning', $resultAfter[ 'keterangan' ] );
        }
	}
	public function actionOrder( $KodeTrans ) {
		$this->layout = 'select-mode';
		switch ( $KodeTrans ) {
			case 'SV':
				return $this->render( 'order',
					[
						'arr'     => [
							'cmbTxt'    => [
								'CusNama'       => 'Nama Konsumen',
								'No'          => 'No Servis',
								'Kode' => 'No Polisi',
								'MotorNama'     => 'Nama Motor',
							],
							'cmbTgl'    => [
								'Tgl' => 'Tgl Servis',
							],
							'cmbNum'    => [
								'Total' => 'Total',
							],
							'sortname'  => "Tgl",
							'sortorder' => 'desc'
						],
						'options' => [ 'colGrid' => Ttkmhd::colGridPickSV(), 'mode' => self::MODE_SELECT_MULTISELECT ]
					] );
				break;
			case 'SI':
				return $this->render( 'order',
					[
						'arr'     => [
							'cmbTxt'    => [
								'CusNama'       => 'Nama Konsumen',
								'SINo'          => 'No Jual',
								'MotorNoPolisi' => 'No Polisi',
								'MotorNama'     => 'Nama Motor',
							],
							'cmbTgl'    => [
								'Tgl' => 'Tgl Jual',
							],
							'cmbNum'    => [
								'Total' => 'Total',
							],
							'sortname'  => "Tgl",
							'sortorder' => 'desc'
						],
						'options' => [ 'colGrid' => Ttkmhd::colGridPickSI(), 'mode' => self::MODE_SELECT_MULTISELECT ]
					] );
				break;
			case 'SO':
				return $this->render( 'order',
					[
						'arr'     => [
							'cmbTxt'    => [
								'CusNama'       => 'Nama Konsumen',
								'SONo'          => 'No Jual',
								'MotorNoPolisi' => 'No Polisi',
								'MotorNama'     => 'Nama Motor',
							],
							'cmbTgl'    => [
								'Tgl' => 'Tgl Jual',
							],
							'cmbNum'    => [
								'Total' => 'Total',
							],
							'sortname'  => "Tgl",
							'sortorder' => 'desc'
						],
						'options' => [ 'colGrid' => Ttkmhd::colGridPickSO(), 'mode' => self::MODE_SELECT_MULTISELECT ]
					] );
				break;
			case 'SE':
				return $this->render( 'order',
					[
						'arr'     => [
							'cmbTxt'    => [
								'CusNama'       => 'Nama Konsumen',
								'SONo'          => 'No Jual',
								'MotorNoPolisi' => 'No Polisi',
								'MotorNama'     => 'Nama Motor',
							],
							'cmbTgl'    => [
								'Tgl' => 'Tgl Estimatis',
							],
							'cmbNum'    => [
								'Total' => 'Total',
							],
							'sortname'  => "Tgl",
							'sortorder' => 'desc'
						],
						'options' => [ 'colGrid' => Ttkmhd::colGridPickSE(), 'mode' => self::MODE_SELECT_MULTISELECT ]
					] );
				break;
			case 'PR':
				return $this->render( 'order',
					[
						'arr'     => [
							'cmbTxt'    => [
								'SupKode'    => 'Kode Supplier',
								'PRNo'       => 'No Retur Beli',
								'SupNama'    => 'Nama Supplier',
								'LokasiKode' => 'Lokasi',
							],
							'cmbTgl'    => [
								'Tgl' => 'Tgl Retur Beli',
							],
							'cmbNum'    => [
								'Total' => 'Total',
							],
							'sortname'  => "Tgl",
							'sortorder' => 'desc'
						],
						'options' => [ 'colGrid' => Ttkmhd::colGridPickPR(), 'mode' => self::MODE_SELECT_MULTISELECT ]
					] );
				break;
		}
	}
	public function actionKasMasukUmum() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasMasukUmum', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkmhd/td' ] )
		] );
	}
	public function actionKasMasukUmumCreate() {
		$KasKode = Traccount::find()->where( "LEFT(NoAccount,5) = '11010' AND JenisAccount = 'Detail'" )->orderBy( 'NoAccount' )->one();
		$model   = new Ttkmhd();
		$model->KMNo          = General::createTemporaryNo( 'KM', 'Ttkmhd', 'KMNo' );
		$model->KMTgl         = date( 'Y-m-d H:i:s' );
		$model->KMPerson      = "--";
		$model->KMMemo        = "--";
		$model->KMBayarTunai  = 0;
		$model->KMNominal     = 0;
		$model->MotorNoPolisi = "UMUM";
		$model->KMJenis       = "Tunai";
		$model->KasKode       = ( $KasKode != null ) ? $KasKode->NoAccount : '';
		$model->UserID        = $this->UserID();
		$model->KodeTrans     = "UM";
		$model->NoGL          = "--";
		$model->PosKode       = $this->LokasiKode();
		$model->Cetak         = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttkmhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttkmhd::find()->ttkmhdFillGetData()->where( [ 'ttkmhd.KMNo' => $model->KMNo ] )->asArray()->one();
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'KMLink' ]   = '';
		$dsTUang[ 'KMNoView' ] = $result[ 'KMNo' ];
		return $this->render( 'kas-masuk-umum-create', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionKasMasukUmumUpdate( $id ) {
		$index = Ttkmhd::getRoute( 'UM', [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		/** @var Ttkmhd $model */
		$model   = $this->findModelBase64( 'Ttkmhd', $id );
		$dsTUang = Ttkmhd::find()->ttkmhdFillGetData()->where( [ 'ttkmhd.KMNo' => $model->KMNo ] )
		                 ->asArray()->one();
		if ( Yii::$app->request->isPost ) {
			$post             = \Yii::$app->request->post();
            $post[ 'KMTgl' ]  = $post[ 'KMTgl' ] . ' ' . $post[ 'KMJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttkmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model = Ttkmhd::findOne( $result[ 'KMNo' ] );
			$id    = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				$post[ 'KMNoView' ] = $result[ 'KMNo' ];
				return $this->redirect( Ttkmhd::getRoute( 'UM', [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'kas-masuk-umum-update', [
					'model'   => $model,
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttkmhd::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		$ttkmit                = Ttkmit::find()->where( [ 'KMNo' => $model->KMNo ] )->one();
		$dsTUang[ 'KMLink' ]   = ( $ttkmit == null ) ? '' : $ttkmit->KMLink;
		$dsTUang[ 'KMNoView' ] = $_GET[ 'KMNoView' ] ?? $result[ 'KMNo' ];
		return $this->render( 'kas-masuk-umum-update', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionKasMasukDariBank() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasMasukDariBank', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkmhd/td' ] )
		] );
	}
	public function actionKasMasukDariBankCreate() {
		$KasKode = Traccount::find()->where( "LEFT(NoAccount,5) = '11010' AND JenisAccount = 'Detail'" )
		                    ->orderBy( 'NoAccount' )->one();
		$model   = new Ttkmhd();
//		$model->KMNo          = Ttkmhd::generateKMNo( true);
		$model->KMNo          = General::createTemporaryNo( 'KM', 'Ttkmhd', 'KMNo' );
		$model->KMTgl         = date( 'Y-m-d H:i:s' );
		$model->KMPerson      = "--";
		$model->KMMemo        = "--";
		$model->KMBayarTunai  = 10;
		$model->KMNominal     = 0;
		$model->MotorNoPolisi = "--";
		$model->KMJenis       = "Tunai";
		$model->KasKode       = ( $KasKode != null ) ? $KasKode->NoAccount : '';;
		$model->UserID    = $this->UserID();
		$model->KodeTrans = "BK";
		$model->NoGL      = "--";
		$model->PosKode   = $this->LokasiKode();
		$model->Cetak     = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttkmhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttkmhd::find()->ttkmhdFillGetData()->where( [ 'ttkmhd.KMNo' => $model->KMNo ] )->asArray( true )->one();
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'KMLink' ]   = '';
		$dsTUang[ 'KMNoView' ] = $result[ 'KMNo' ];
		return $this->render( 'kas-masuk-dari-bank-create', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionKasMasukDariBankUpdate( $id ) {
		/** @var Ttkmhd $model */
		$KasKode = Traccount::find()->where( "LEFT(NoAccount,5) = '11010' AND JenisAccount = 'Detail'" )->orderBy( 'NoAccount' )->one();
		$model   = $this->findModelBase64( 'Ttkmhd', $id );
		$dsTUang = Ttkmhd::find()->ttkmhdFillGetData()->where( [ 'ttkmhd.KMNo' => $model->KMNo ] )->asArray()->one();
		if ( Yii::$app->request->isPost ) {
			$post             = \Yii::$app->request->post();
            $post[ 'KMTgl' ]  = $post[ 'KMTgl' ] . ' ' . $post[ 'KMJam' ];
			$isNew = ( strpos( $post['KMNo'], '=' ) !== false);
			$post[ 'Action' ] = 'Update';
			$result           = Ttkmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			if ( $result[ 'status' ] == 0 ) {
				$postit[ 'Action' ]  = $isNew ? 'Insert' : 'Update';
				$postit[ 'KMNo' ]    = $post[ 'KMNo' ];
				$postit[ 'KMBayar' ] = $post[ 'KMNominal' ];
				$postit[ 'KMLink' ]  = $post[ 'KMLink' ];
				$resultit            = Ttkmit::find()->callSP( $postit );
				if ( $resultit[ 'status' ] == 1 ) {
					Yii::$app->session->addFlash( 'warning', $resultit[ 'keterangan' ] );
				}
			}
			$model              = Ttkmhd::findOne( $result[ 'KMNo' ] );
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'KMNoView' ] = $result[ 'KMNo' ];
			if ( $result[ 'status' ] == 0 ) {
				$post[ 'KMNoView' ] = $result[ 'KMNo' ];
				return $this->redirect( Ttkmhd::getRoute( 'BK', [ 'id' => $id, 'action' => 'display' ] ) [ 'update' ] );
			} else {
				return $this->render( 'kas-masuk-dari-bank-update', [
					'model'   => $model,
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttkmhd::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'KMNo' ] = $dsTUang[ 'KMNo' ];
		}
		$ttkmit                = Ttkmit::find()->where( [ 'KMNo' => $model->KMNo ] )->one();
		$dsTUang[ 'KMLink' ]   = ( $ttkmit == null ) ? '' : $ttkmit->KMLink;
		$dsTUang[ 'KMNoView' ] = $_GET[ 'KMNoView' ] ?? $result[ 'KMNo' ];
		return $this->render( 'kas-masuk-dari-bank-update', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionKasMasukServiceInvoice() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasMasukServiceInvoice', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkmhd/td' ] )
		] );
	}
	public function actionKasMasukServiceInvoiceCreate() {
		return $this->create( 'SV', 'kas-masuk-service-invoice', 'KAS MASUK - Servis' );
	}
	public function actionKasMasukServiceInvoiceUpdate( $id ) {
		return $this->update( 'SV', 'kas-masuk-service-invoice', 'KAS MASUK - Servis', $id );
	}
	public function actionKasMasukSalesInvoice() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasMasukSalesInvoice', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkmhd/td' ] )
		] );
	}
	public function actionKasMasukSalesInvoiceCreate() {
		return $this->create( 'SI', 'kas-masuk-sales-invoice', 'KAS MASUK - Penjualan' );
	}
	public function actionKasMasukSalesInvoiceUpdate( $id ) {
		return $this->update( 'SI', 'kas-masuk-sales-invoice', 'KAS MASUK - Penjualan', $id );
	}
	public function actionKasMasukEstimasiServis() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasMasukEstimasiServis', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkmhd/td' ] )
		] );
	}
	public function actionKasMasukEstimasiServisCreate() {
		return $this->create( 'SE', 'kas-masuk-estimasi-servis', 'KAS MASUK - Estimasi Servis' );
	}
	public function actionKasMasukEstimasiServisUpdate( $id ) {
		return $this->update( 'SE', 'kas-masuk-estimasi-servis', 'KAS MASUK - Estimasi Servis', $id );
	}
	public function actionKasMasukOrderPenjualan() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasMasukOrderPenjualan', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkmhd/td' ] )
		] );
	}
	public function actionKasMasukOrderPenjualanCreate() {
		return $this->create( 'SO', 'kas-masuk-order-penjualan', 'KAS MASUK - Order Penjualan' );
	}
	public function actionKasMasukOrderPenjualanUpdate( $id ) {
		return $this->update( 'SO', 'kas-masuk-order-penjualan', 'KAS MASUK - Order Penjualan', $id );
	}
	public function actionKasMasukReturPembelian() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasMasukReturPembelian', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkmhd/td' ] )
		] );
	}
	public function actionKasMasukReturPembelianCreate() {
		return $this->create( 'PR', 'kas-masuk-retur-pembelian', 'KAS MASUK - Retur Pembelian' );
	}
	public function actionKasMasukReturPembelianUpdate( $id ) {
		return $this->update( 'PR', 'kas-masuk-retur-pembelian', 'KAS MASUK - Retur Pembelian', $id );
	}
	protected function create( $KodeTrans, $view, $title ) {
		$KasKode = Traccount::find()->where( "LEFT(NoAccount,5) = '11010' AND JenisAccount = 'Detail'" )
		                    ->orderBy( 'NoAccount' )->one();
		$model   = new Ttkmhd();
//		$model->KMNo          = Ttkmhd::generateKMNo( true);
		$model->KMNo          = General::createTemporaryNo( 'KM', 'Ttkmhd', 'KMNo' );
		$model->KMTgl         = date( 'Y-m-d H:i:s' );
		$model->KMPerson      = "--";
		$model->KMMemo        = "--";
		$model->KMBayarTunai  = 10;
		$model->KMNominal     = 0;
		$model->MotorNoPolisi = "UMUM";
		$model->KMJenis       = 'Tunai';
		$model->KasKode       = ( $KasKode != null ) ? $KasKode->NoAccount : '';;
		$model->UserID    = $this->UserID();
		$model->KodeTrans = $KodeTrans;
		$model->NoGL      = "--";
		$model->PosKode   = $this->LokasiKode();
		$model->Cetak     = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttkmhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttkmhd::find()->ttkmhdFillGetData()->where( [ 'ttkmhd.KMNo' => $model->KMNo ] )->asArray( true )->one();
		$dsTUang[ 'KMLink' ]   = '';
		$dsTUang[ 'Total' ]    = 0;
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'KMNoView' ] = $result[ 'KMNo' ];
		return $this->render( 'create', [
			'model'     => $model,
			'view'      => $view,
			'title'     => $title,
			'dsTUang'   => $dsTUang,
			'id'        => $id,
			'KodeTrans' => $KodeTrans
		] );
	}
	protected function update( $KMJenis, $view, $title, $id ) {
		$index = Ttkmhd::getRoute( $KMJenis, [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		/** @var Ttkmhd $model */
		$KasKode = Traccount::find()->where( "LEFT(NoAccount,5) = '11010' AND JenisAccount = 'Detail'" )->orderBy( 'NoAccount' )->one();
		$model   = $this->findModelBase64( 'Ttkmhd', $id );
		$dsTUang = Ttkmhd::find()->ttkmhdFillGetData()->where( [ 'ttkmhd.KMNo' => $model->KMNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post             = \Yii::$app->request->post();
            $post[ 'KMTgl' ]  = $post[ 'KMTgl' ] . ' ' . $post[ 'KMJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttkmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model = Ttkmhd::findOne( $result[ 'KMNo' ] );
			$id    = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 ) {
				$post[ 'KMNoView' ] = $result[ 'KMNo' ];
				return $this->redirect( Ttkmhd::getRoute( $KMJenis, [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'update', [
					'model'     => $model,
					'view'      => $view,
					'title'     => $title,
					'dsTUang'   => $post,
					'id'        => $id,
					'KodeTrans' => $KMJenis
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttkmhd::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'KMNo' ] = $dsTUang[ 'KMNo' ];
		}
		$ttkmit                = Ttkmit::find()->where( [ 'KMNo' => $model->KMNo ] )->one();
		$dsTUang[ 'KMLink' ]   = ( $ttkmit == null ) ? '' : $ttkmit->KMLink;
		$dsTUang[ 'Total' ]    = 0;
		$dsTUang[ 'KMNoView' ] = $_GET[ 'KMNoView' ] ?? $result[ 'KMNo' ];
		return $this->render( 'update', [
			'model'     => $model,
			'view'      => $view,
			'title'     => $title,
			'dsTUang'   => $dsTUang,
			'id'        => $id,
			'KodeTrans' => $KMJenis
		] );
	}
    public function actionDelete() {
        /** @var Ttkmhd $model */
        $request = Yii::$app->request;
        $PK         = base64_decode( $_REQUEST[ 'id' ] );
        $model      = null;
        $model = Ttkmhd::findOne( $PK );
        $_POST[ 'KMNo' ]   = $model->KMNo;
        $_POST[ 'Action' ] = 'Delete';
        $KMJenis = $model->KodeTrans;
        $result = Ttkmhd::find()->callSP( $_POST );
        if ($request->isAjax){
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ( $result[ 'KMNo' ] == '--' ) {
                return $this->redirect( Ttkmhd::getRoute( $KMJenis )[ 'index' ] );
            } else {
                $model = Ttkmhd::findOne( $result[ 'KMNo' ] );
                return $this->redirect( Ttkmhd::getRoute( $KMJenis, [ 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] )[ 'update' ] );
            }
        }
    }
	public function actionCancel( $id, $action ) {
		/** @var Ttkmhd $model */
		$model            = $this->findModelBase64( 'Ttkmhd', $id );
		$KodeTrans        = $model->KodeTrans;
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Cancel';
		$post[ 'KMJam' ]  = $post[ 'KMTgl' ] . ' ' . $post[ 'KMJam' ];
		$result           = Ttkmhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'KMNo' ] == '--' ) {
			return $this->redirect( Ttkmhd::getRoute( $KodeTrans )[ 'index' ] );
		} else {
			$model = Ttkmhd::findOne( $result[ 'KMNo' ] );
			return $this->redirect( Ttkmhd::getRoute( $KodeTrans, [ 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] )[ 'update' ] );
		}
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST         = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client        = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response      = $client->post( Yii::$app->params['H2'][$_POST['db']]['host_report'] . '/abengkel/fkmhd', [
			'body' => Json::encode( $_POST )
		] );
        $html                 = $response->getBody();
        return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	protected function findModel( $id ) {
		if ( ( $model = Ttkmhd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}

    public function actionJurnal() {
        $post             = \Yii::$app->request->post();
        $post[ 'Action' ] = 'Jurnal';
        $result           = Ttkmhd::find()->callSP( $post );
        Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        return $this->redirect( Ttkmhd::getRoute( $post[ 'KodeTrans' ], [ 'action' => 'display', 'id' => base64_encode( $result[ 'KMNo' ] ) ] )[ 'update' ] );
    }
}
