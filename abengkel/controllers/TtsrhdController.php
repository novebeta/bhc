<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\models\Tdbaranglokasi;
use abengkel\models\Tdkaryawan;
use abengkel\models\Tdlokasi;
use abengkel\models\Ttsrhd;
use abengkel\models\Vtkbk;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtsrhdController implements the CRUD actions for Ttsrhd model.
 */
class TtsrhdController extends AbengkelController {
	public function actions() {
        $where      = [
            [
                'op'        => 'AND',
                'condition' => "(SRNo NOT LIKE '%=%')",
                'params'    => []
            ]
        ];
		return [
			'td' => [
				'class'    => TdAction::class,
				'model'    => Ttsrhd::class,
                'where'    => $where,
				'joinWith' => [
					'customer' => [
						'type' => 'INNER JOIN'
					]
				]
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttsrhd models.
	 * @return mixed
	 */
	public function actionIndex() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttsrhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'index', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttsrhd/td' ] )
		] );
	}
	/**
	 * Creates a new Ttsrhd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
//        TUi::$actionMode   = Menu::ADD;
		$LokasiKodeCmb        = Tdlokasi::find()->where( "LokasiStatus = 'A' or LokasiStatus = '1'" )->orderBy( 'LokasiStatus, LokasiKode' )->one();
		$KarKodeCmb           = Tdkaryawan::find()->where( [ 'KarStatus' => 'A' ] )->orderBy( 'KarStatus,KarKode' )->one();
		$model                = new Ttsrhd();
		$model->SRNo          = General::createTemporaryNo( 'SR', 'Ttsrhd', 'SRNo' );
		$model->SRTgl         = date( 'Y-m-d H:i:s' );
		$model->KarKode       = ( $KarKodeCmb != null ) ? $KarKodeCmb->KarKode : '--';
		$model->MotorNoPolisi = "UMUM";
		$model->SRKeterangan  = "--";
		$model->LokasiKode    = ( $LokasiKodeCmb != null ) ? $LokasiKodeCmb->LokasiKode : 'Display';
		$model->SRSubTotal    = 0;
		$model->SRDiscFinal   = 0;
		$model->SRTotalPajak  = 0;
		$model->SRBiayaKirim  = 0;
		$model->SRTotal       = 0;
		$model->UserID        = $this->UserID();
		$model->NoGL          = "--";
		$model->KodeTrans     = 'SR';
		$model->PosKode       = $this->LokasiKode();
		$model->Cetak         = "Belum";
		$model->SINo          = "--";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttsrhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTJual                  = Ttsrhd::find()->ttsrhdFillByNo()->where( [ 'ttsrhd.SRNo' => $model->SRNo ] )
		                                  ->asArray( true )->one();
		$dsTJual[ 'StatusBayar' ] = 'BELUM';
		$id                       = $this->generateIdBase64FromModel( $model );
		$dsTJual[ 'SRNoView' ]    = $result[ 'SRNo' ];
		return $this->render( 'create', [
			'model'   => $model,
			'dsTJual' => $dsTJual,
			'id'      => $id,
		] );
	}
	/**
	 * Updates an existing Ttsrhd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 */
	public function actionUpdate( $id ) {
		$index = \yii\helpers\Url::toRoute( [ 'ttsrhd/index' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		/** @var Ttsrhd $model */
		$model    = $this->findModelBase64( 'Ttsrhd', $id );
		$dsTJual  = Ttsrhd::find()->ttsrhdFillByNo()->where( [ 'ttsrhd.SRNo' => $model->SRNo ] )
		                  ->asArray( true )->one();
		$Terbayar = floatval($dsTJual['SRTerbayar']); //Vtkbk::find()->getTotalBayar( $model->SRNo );
//		if ( $model->SRTerbayar != $Terbayar ) {
//			$model->SRTerbayar = $Terbayar;
//			$model->save();
//		}
		if ( Yii::$app->request->isPost ) {
			$post               = array_merge($dsTJual, \Yii::$app->request->post());
            $post[ 'SRTgl' ]    = $post[ 'SRTgl' ] . ' ' . $post[ 'SRJam' ];
			$post[ 'Action' ]   = 'Update';
			$result             = Ttsrhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttsrhd::findOne( $result[ 'SRNo' ] );
            if ( $result[ 'SRNo' ] == '--' || $model == null) {
                if ( $model === null ) {
                    Yii::$app->session->addFlash( 'warning', 'SRNo : ' . $result[ 'SRNo' ]. ' tidak ditemukan.' );
                }
                return $this->redirect( $index);
            }
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'SRNoView' ] = $result[ 'SRNo' ];
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				return $this->redirect( [ 'ttsrhd/update', 'action' => 'display', 'id' => $id ] );
			} else {
				return $this->render( 'update', [
					'model'   => $model,
					'dsTJual' => $post,
					'id'      => $id,
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTJual[ 'Action' ] = 'Load';
			$result              = Ttsrhd::find()->callSP( $dsTJual );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'SRNo' ] = $dsTJual[ 'SRNo' ];
		}

		$dsTJual[ 'SisaBayar' ]    = floatval( $dsTJual[ 'SRTotal' ] ) - $Terbayar;
		if ( $dsTJual[ 'SisaBayar' ] > 0 || floatval( $dsTJual[ 'SRTotal' ] ) == 0 ) {
			$dsTJual[ 'StatusBayar' ] = 'BELUM';
		} else if ( $dsTJual[ 'SisaBayar' ] <= 0 ) {
			$dsTJual[ 'StatusBayar' ] = 'LUNAS';
		}

		$dsTJual[ 'SRNoView' ] = $_GET[ 'SRNoView' ] ?? $result[ 'SRNo' ];
		return $this->render( 'update', [
			'model'   => $model,
			'dsTJual' => $dsTJual,
			'id'      => $id,
		] );
	}
	/**
	 * Deletes an existing Ttsrhd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
//	public function actionDelete() {
//		/** @var Ttsrhd $model */
//		$model             = $model = $this->findModelBase64( 'Ttsrhd', $_POST[ 'id' ] );
//		$_POST[ 'SRNo' ]   = $model->SRNo;
//		$_POST[ 'Action' ] = 'Delete';
//		$result            = Ttsrhd::find()->callSP( $_POST );
//		if ( $result[ 'status' ] == 0 ) {
//			return $this->responseSuccess( $result[ 'keterangan' ] );
//		} else {
//			return $this->responseFailed( $result[ 'keterangan' ] );
//		}
//	}

    public function actionDelete()
    {
        /** @var Ttsrhd $model */
        $request = Yii::$app->request;
        if ($request->isAjax){
            $model = $this->findModelBase64('Ttsrhd', $_POST['id']);
            $_POST['SRNo'] = $model->SRNo;
            $_POST['Action'] = 'Delete';
            $result = Ttsrhd::find()->callSP($_POST);
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            $id = $request->get('id');
            $model = $this->findModelBase64('Ttsrhd', $id);
            $_POST['SRNo'] = $model->SRNo;
            $_POST['Action'] = 'Delete';
            $result = Ttsrhd::find()->callSP($_POST);
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['SRNo'] == '--') {
                return $this->redirect(['index']);
            } else {
                $model = Ttsrhd::findOne($result['SRNo']);
                return $this->redirect(['ttsrhd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
            }
        }
    }

	public function actionCancel( $id, $action ) {
		/** @var Ttsrhd $model */
		$model             = $this->findModelBase64( 'Ttsrhd', $id );
		$_POST[ 'SRNo' ]   = $model->SRNo;
		$_POST[ 'SRJam' ]  = $_POST[ 'SRTgl' ] . ' ' . $_POST[ 'SRJam' ];
		$_POST[ 'Action' ] = 'Cancel';
		$result            = Ttsrhd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'SRNo' ] == '--' ) {
			return $this->redirect( [ 'index' ] );
		} else {
			$model = Ttsrhd::findOne( $result[ 'SRNo' ] );
			return $this->redirect( [ 'ttsrhd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST         = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client        = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response      = $client->post( Yii::$app->params['H2'][$_POST['db']]['host_report'] . '/abengkel/fsrhd', [
			'body' => Json::encode( $_POST )
		] );
//		return $response->getBody();
        $html                 = $response->getBody();
        return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	/**
	 * Finds the Ttsrhd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttsrhd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttsrhd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}

    public function actionCheckStock( $BrgKode, $LokasiKode ) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return Tdbaranglokasi::find()
            ->where( 'BrgKode = :BrgKode AND LokasiKode = :LokasiKode', [ 'BrgKode' => $BrgKode , 'LokasiKode' => $LokasiKode] )
            ->asArray()->one();
    }

    public function actionStatusOtomatis() {
        $this->layout   = 'popup-form';
        return $this->render( '_formStatus', [ 'data' => $_GET ] );
    }

    public function actionStatusOtomatisItem( $SRNo ) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $dt = General::cCmd( "SELECT KBKNo, KbKTgl, KBKLink, KBKBayar, Kode, Person, Memo,NoGL FROM vtkbk WHERE KBKLink = :SRNo ORDER BY KbKTgl;", [':SRNo' => $SRNo ] )->queryAll();
        return [
            'rows' => $dt
        ];

    }
}
