<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\components\TUi;
use abengkel\models\Tddealer;
use abengkel\models\Tdlokasi;
use abengkel\models\Tdsupplier;
use abengkel\models\Traccount;
use abengkel\models\Ttkkhd;
use abengkel\models\Ttkkit;
use aunit\models\Tdsales;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtkkhdController implements the CRUD actions for Ttkkhd model.
 */
class TtkkhdController extends AbengkelController {
	public function actions() {
		$colGrid               = null;
		$joinWith              = [];
		$join                  = [];
		$where                 = [];
		$joinWith[ 'account' ] = [
			'type' => 'INNER JOIN'
		];
		$sqlraw                = null;
		$queryRawPK            = null;
		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			switch ( $r ) {
				case 'ttkkhd/kas-keluar-umum':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkkhd.KodeTrans = 'UM' AND ttkkhd.KKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkkhd/kas-keluar-ke-bank':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkkhd.KodeTrans = 'BM' AND ttkkhd.KKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkkhd/kas-keluar-ke-h1':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkkhd.KodeTrans = 'H1' AND ttkkhd.KKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkkhd/kas-keluar-pembelian':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkkhd.KodeTrans = 'PI' AND ttkkhd.KKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkkhd/kas-keluar-order-pengerjaan-luar':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkkhd.KodeTrans = 'PL' AND ttkkhd.KKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkkhd/kas-keluar-retur-penjualan':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkkhd.KodeTrans = 'SR' AND ttkkhd.KKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkkhd/transfer-antar-kas':
					$colGrid    = Ttkkhd::colGridTransferKas();
                    $where = [
                        [
                            'op'        => 'AND',
                            'condition' => "(KKNo NOT LIKE '%=%')",
                            'params'    => []
                        ]
                    ];
					$sqlraw     = "SELECT 
					ttkkit.KKLink, ttkmit.KMLink, 
					ttkkhd.KKTgl, ttkmhd.KMTgl, 
					ttkkit.KKBayar,  ttkmit.KMBayar, 
					ttkkhd.KKJenis, ttkmhd.KMJenis,
					ttkkhd.KKNo, ttkmhd.KMNo,
					traccountkk.NamaAccount AS NamaAccountKK, 
					traccountkm.NamaAccount AS NamaAccountKM, 	
					ttkkhd.KasKode AS NoAccountKK,ttkmhd.KasKode AS NoAccountKM,
					ttkkhd.NoGL AS NoGLKK, ttkmhd.NoGL AS NoGLKM, 
					ttkkhd.KodeTrans, ttkkhd.PosKode, ttkkhd.SupKode, 
					ttkkhd.KKPerson, ttkkhd.KKNominal, ttkkhd.KKMemo, ttkkhd.Cetak, ttkkhd.UserID, 				
					ttkmhd.MotorNoPolisi, ttkmhd.KMPerson, ttkmhd.KMBayarTunai, ttkmhd.KMNominal, ttkmhd.KMMemo
					 FROM ttkkhd 
			         INNER JOIN ttkkit ON ttkkhd.KKNo = ttkkit.KKNo
			         INNER JOIN ttkmit ON ttkkit.KKLInk = ttkmit.KMLink 
			         INNER JOIN ttkmhd ON ttkmhd.KMNo = ttkmit.KMNo
					 INNER JOIN traccount traccountkk ON ttkkhd.KasKode = traccountkk.NoAccount 
					 INNER JOIN traccount traccountkm ON ttkmhd.KasKode = traccountkm.NoAccount 
			         WHERE (ttkkhd.KKJenis = 'Kas Transfer' AND ttkmhd.KMJenis = 'Kas Transfer')";
					$queryRawPK = [ 'KMLink' ];
					break;
				case 'ttkkhd/order':
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$KodeTrans = $urlParams[ 'KodeTrans' ];
					switch ( $KodeTrans ) {
						case 'PI':
							$sqlraw = "SELECT ttPIhd.PINo AS NO, ttPIhd.PITgl AS Tgl, tdsupplier.SupKode AS Kode, tdsupplier.SupNama AS Nama, 
       								LokasiKode AS MotorNama, PITotal AS Total, IFNULL(KBPaid.Paid, 0) AS Terbayar, PITotal - IFNULL(KBPaid.Paid, 0) AS Sisa, 0.00 AS Bayar 
                                    FROM ttPIhd
                                    INNER JOIN tdsupplier ON ttPIhd.SupKode = tdsupplier.SupKode 
                                      LEFT OUTER JOIN 
                                        (SELECT KBKLINK, IFNULL(SUM(KBKBayar), 0) AS Paid FROM vtKBK WHERE LEFT(KBKLink, 2) = 'PI' GROUP BY KBKLINK) KBPaid 
                                        ON PINo = KBPaid.KBKLINK 
                                    WHERE (PITotal - IFNULL(KBPaid.Paid, 0) > 0)";
							break;
						case 'SR':
							$sqlraw = "SELECT ttSRhd.SRNo AS NO, ttSRhd.SRTgl AS Tgl, tdcustomer.MotorNoPolisi AS Kode, tdcustomer.CusNama AS Nama, 
       								tdcustomer.MotorNama, SRTotal AS Total, IFNULL(KBPaid.Paid, 0) AS Terbayar, SRTotal - IFNULL(KBPaid.Paid, 0) AS Sisa, 0.00 AS Bayar 
                                    FROM ttSRhd 
                                    INNER JOIN tdcustomer ON ttSRhd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                                      LEFT OUTER JOIN 
                                        (SELECT KBKLINK, IFNULL(SUM(KBKBayar), 0) AS Paid FROM vtKBK WHERE LEFT(KBKLink, 2) = 'SR' GROUP BY KBKLINK) KBPaid 
                                        ON SRNo = KBPaid.KBKLINK 
                                    WHERE (SRTotal - IFNULL(KBPaid.Paid, 0) > 0)";
							break;
					}
					$queryRawPK = [ 'No' ];
					$colGrid    = Ttkkhd::colGridPick();
					break;
				case 'ttkkhd/select-kk-kas':
					$BMNo           = $urlParams[ 'BMNo' ];
					$colGrid    = Ttkkhd::colGridSelectKkKas();
					$sqlraw     = "SELECT ttkkhd.KKNo as No, ttkkhd.KKTgl as Tgl, ttkkhd.KKMemo, ttkkhd.KKNominal, ttkkhd.KKJenis, ttkkhd.KasKode, 
                    ttkkhd.SupKode, ttkkhd.KKPerson, ttkkhd.UserID, ttkkhd.KodeTrans, traccount.NamaAccount 
                    FROM ttkkhd INNER JOIN traccount ON KasKode = NoAccount 
                    LEFT OUTER JOIN ttbmit ON ttkkhd.KKNo = ttbmit.BMLink  
					LEFT OUTER JOIN ttbmhd ON ttbmhd.BMNo = ttbmit.BMNo
                    WHERE (ttkkhd.KodeTrans = 'BM') AND (ttbmit.BMNo is NULL  OR ttbmit.BMNo = '{$BMNo}' OR ttbmhd.KodeTrans = 'H1')  ";
					$queryRawPK = [ 'No' ];
					break;
				case 'ttkkhd/select-bm-dari-h1':
					$BMNo           = $urlParams[ 'BMNo' ];
					$colGrid    = Ttkkhd::colGridSelectKkBmDariH1();
					$db         = substr(base64_decode( $_COOKIE[ '_outlet' ] ), 1). '.';
					$sql     = "SELECT KKNo, KKTgl, KKMemo, KKNominal, KKJenis, KKPerson, KKLink, UserID, LokasiKode, KKJam, NoGL 
					FROM HMDS.ttkk 
                    LEFT OUTER JOIN ttbmit ON ttkk.KKNo = ttbmit.BMLink  
                    WHERE (ttKK.KKJenis = 'KK Ke Bengkel') AND (ttbmit.BMNo is NULL OR ttbmit.BMNo = '{$BMNo}') ";
					$sqlraw     = str_replace( 'HMDS.', $db, $sql );
					$queryRawPK = [ 'KKNo' ];
					break;
			}
		}
		return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Ttkkhd::class,
				'queryRaw'   => $sqlraw,
				'queryRawPK' => $queryRawPK,
				'colGrid'    => $colGrid,
				'joinWith'   => $joinWith,
				'join'       => $join,
				'where'      => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionSelectKkKas() {
		$this->layout = 'select-mode';
		return $this->render( 'order',
			[
				'arr'     => [
					'cmbTxt'    => [
						'No' => 'No KK',
					],
					'cmbTgl'    => [
						'Tgl' => 'Tgl',
					],
					'cmbNum'    => [],
					'sortname'  => "Tgl",
					'sortorder' => 'desc'
				],
				'options' => [ 'colGrid' => Ttkkhd::colGridSelectKkKas(), 'mode' => self::MODE_SELECT, ]
			] );
	}
	public function actionSelectBmDariH1() {
		$this->layout = 'select-mode';
		return $this->render( 'order',
			[
				'arr'     => [
					'cmbTxt'    => [
						'KKNo' => 'No KK',
					],
					'cmbTgl'    => [
						'KKTgl' => 'KKTgl',
					],
					'cmbNum'    => [],
					'sortname'  => "KKTgl",
					'sortorder' => 'desc'
				],
				'options' => [ 'colGrid' => Ttkkhd::colGridSelectKkBmDariH1(), 'mode' => self::MODE_SELECT, ]
			] );
	}
	public function actionLoopKasMasuk() {
		$_POST[ 'Action' ] = 'Loop';
		$result            = Ttkkhd::find()->callSP( $_POST );
		if ( $result[ 'status' ] == 1 ) {
			Yii::$app->session->addFlash( 'warning', $result[ 'keterangan' ] );
		}
	}
	public function actionOrder( $KodeTrans ) {
		$this->layout = 'select-mode';
		switch ( $KodeTrans ) {
			case 'PI':
				return $this->render( 'order',
					[
						'arr'     => [
							'cmbTxt'    => [
								'SupKode'    => 'Kode Supplier',
								'PINo'       => 'No Beli',
								'SupNama'    => 'Nama Supplier',
								'LokasiKode' => 'Lokasi',
							],
							'cmbTgl'    => [
								'Tgl' => 'Tgl Beli',
							],
							'cmbNum'    => [
								'Total' => 'Total',
							],
							'sortname'  => "Tgl",
							'sortorder' => 'desc'
						],
						'options' => [ 'colGrid' => Ttkkhd::colGridPickPI(), 'mode' => self::MODE_SELECT_MULTISELECT ]
					] );
				break;
			case 'SR':
				return $this->render( 'order',
					[
						'arr'     => [
							'cmbTxt'    => [
								'CusNama'       => 'Nama Konsumen',
								'SRNo'          => 'No Retur Jual',
								'MotorNoPolisi' => 'No Polisi',
								'MotorNama'     => 'Nama Motor',
							],
							'cmbTgl'    => [
								'Tgl' => 'Tgl Retur Jual',
							],
							'cmbNum'    => [],
							'sortname'  => "Tgl",
							'sortorder' => 'desc'
						],
						'options' => [ 'colGrid' => Ttkkhd::colGridPickSR(), 'mode' => self::MODE_SELECT_MULTISELECT ]
					] );
				break;
		}
	}
	public function actionKasKeluarUmum() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasKeluarUmum', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkkhd/td' ] )
		] );
	}
	public function actionKasKeluarUmumCreate() {
		$KasKode = Traccount::find()->where( "LEFT(NoAccount,5) = '11010' AND JenisAccount = 'Detail'" )
		                    ->orderBy( 'NoAccount' )->one();
		$model   = new Ttkkhd();
//		$model->KKNo      = Ttkkhd::generateKKNo( true );
		$model->KKNo      = General::createTemporaryNo( 'KK', 'Ttkkhd', 'KKNo' );
		$model->KKTgl     = date( 'Y-m-d H:i:s' );
		$model->KKPerson  = "--";
		$model->KKMemo    = "--";
		$model->KKNominal = 0;
		$model->KKJenis   = "Tunai";
		$model->KasKode   = ( $KasKode != null ) ? $KasKode->NoAccount : '';
		$model->PosKode   = $this->LokasiKode();
//		$model->SupKode   = Tddealer::findOne(['DealerStatus'=>'1'])->DealerKode;
		$model->SupKode   = '--';
		$model->UserID    = $this->UserID();
		$model->KodeTrans = "UM";
		$model->NoGL      = "--";
		$model->Cetak     = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttkkhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttkkhd::find()->ttkkhdFillGetData()->where( [ 'ttkkhd.KKNo' => $model->KKNo ] )->asArray( true )->one();
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'KKLink' ]   = '';
		$dsTUang[ 'KKNoView' ] = $result[ 'KKNo' ];
		return $this->render( 'kas-keluar-umum-create', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionKasKeluarUmumUpdate( $id ) {
		$index = Ttkkhd::getRoute( "UM", [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		/** @var Ttkkhd $model */
		$KasKode = Traccount::find()->where( "LEFT(NoAccount,5) = '11010' AND JenisAccount = 'Detail'" )
		                    ->orderBy( 'NoAccount' )->one();
		$model   = $this->findModelBase64( 'Ttkkhd', $id );
		$dsTUang = Ttkkhd::find()->ttkkhdFillGetData()->where( [ 'ttkkhd.KKNo' => $model->KKNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post = \Yii::$app->request->post();
			$post[ 'Action' ] = 'Update';
			$post[ 'KKTgl' ]    = $post[ 'KKTgl' ] . ' ' . $post[ 'KKJam' ];
			$result           = Ttkkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model = Ttkkhd::findOne( $result[ 'KKNo' ] );
			$id    = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				$post[ 'KKNoView' ] = $result[ 'KKNo' ];
				return $this->redirect( Ttkkhd::getRoute( "UM", [ 'id' => $id, 'action' => 'display' ] ) [ 'update' ] );
			} else {
				return $this->render( 'kas-keluar-umum-update', [
					'model'   => $model,
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttkkhd::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'KKNo' ] = $dsTUang[ 'KKNo' ];
		}
		$ttkkit                = Ttkkit::find()->where( [ 'KKNo' => $model->KKNo ] )->one();
		$dsTUang[ 'KKLink' ]   = ( $ttkkit == null ) ? '' : $ttkkit->KKLink;
		$dsTUang[ 'KKNoView' ] = $_GET[ 'KKNoView' ] ?? $result[ 'KKNo' ];
		return $this->render( 'kas-keluar-umum-update', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionKasKeluarKeBank() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasKeluarKeBank', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkkhd/td' ] )
		] );
	}
	public function actionKasKeluarKeBankCreate() {
		$KasKode = Traccount::find()->where( "LEFT(NoAccount,5) = '11010' AND JenisAccount = 'Detail'" )
		                    ->orderBy( 'NoAccount' )->one();
		$model   = new Ttkkhd();
//		$model->KKNo      = Ttkkhd::generateKKNo( true );
		$model->KKNo      = General::createTemporaryNo( 'KK', 'Ttkkhd', 'KKNo' );
		$model->KKTgl     = date( 'Y-m-d H:i:s' );
		$model->KKPerson  = "--";
		$model->KKMemo    = "--";
		$model->KKNominal = 0;
		$model->KKJenis   = "Tunai";
		$model->KasKode   = ( $KasKode != null ) ? $KasKode->NoAccount : '';
		$model->PosKode   = $this->LokasiKode();
		$model->SupKode   = Tddealer::findOne(['DealerStatus'=>'1'])->DealerKode;
		$model->UserID    = $this->UserID();
		$model->KodeTrans = "BM";
		$model->NoGL      = "--";
		$model->Cetak     = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttkkhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttkkhd::find()->ttkkhdFillGetData()->where( [ 'ttkkhd.KKNo' => $model->KKNo ] )->asArray( true )->one();
		$dsTUang[ 'KKLink' ]   = '';
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'KKNoView' ] = $result[ 'KKNo' ];
		return $this->render( 'kas-keluar-ke-bank-create', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionKasKeluarKeBankUpdate( $id ) {
		$index = Ttkkhd::getRoute( "BM", [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		/** @var Ttkkhd $model */
		$KasKode = Traccount::find()->where( "LEFT(NoAccount,5) = '11010' AND JenisAccount = 'Detail'" )
		                    ->orderBy( 'NoAccount' )->one();
		$model   = $this->findModelBase64( 'Ttkkhd', $id );
		$dsTUang = Ttkkhd::find()->ttkkhdFillGetData()->where( [ 'ttkkhd.KKNo' => $model->KKNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post = \Yii::$app->request->post();
			$isNew = ( strpos( $post['KKNo'], '=' ) !== false);
			$post[ 'Action' ] = 'Update';
			$post[ 'KKTgl' ]    = $post[ 'KKTgl' ] . ' ' . $post[ 'KKJam' ];
			$result           = Ttkkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			if ( $result[ 'status' ] == 0 ) {
				$postit[ 'Action' ]  = $isNew ? 'Insert' : 'Update';
				$postit[ 'KKNo' ]    = $post[ 'KKNo' ];
				$postit[ 'KKBayar' ] = $post[ 'KKNominal' ];
				$postit[ 'KKLink' ]  = "KK Ke Bank";
				$resultit            = Ttkkit::find()->callSP( $postit );
				if ( $resultit[ 'status' ] == 1 ) {
					Yii::$app->session->addFlash( 'warning', $resultit[ 'keterangan' ] );
				}
			}
			$model = Ttkkhd::findOne( $result[ 'KKNo' ] );
			$id    = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 ) {
				$post[ 'KKNoView' ] = $result[ 'KKNo' ];
				return $this->redirect( Ttkkhd::getRoute( "BM", [ 'id' => $id, 'action' => 'display' ] ) [ 'update' ] );
			} else {
				return $this->render( 'kas-keluar-ke-bank-update', [
					'model'   => $model,
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
		$dsTUang[ 'KasKode' ] = $dsTUang[ 'KasKode' ] ?? '--';
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttkkhd::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'KKNo' ] = $dsTUang[ 'KKNo' ];
		}
		$ttkkit                = Ttkkit::find()->where( [ 'KKNo' => $model->KKNo ] )->one();
		$dsTUang[ 'KKLink' ]   = ( $ttkkit == null ) ? '' : $ttkkit->KKLink;
		$dsTUang[ 'KKNoView' ] = $_GET[ 'KKNoView' ] ?? $result[ 'KKNo' ];
		return $this->render( 'kas-keluar-ke-bank-update', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionKasKeluarKeH1() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasKeluarKeH1', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkkhd/td' ] )
		] );
	}
	public function actionKasKeluarKeH1Create() {
		$KasKode = Traccount::find()->where( "LEFT(NoAccount,5) = '11010' AND JenisAccount = 'Detail'" )
		                    ->orderBy( 'NoAccount' )->one();
		$model   = new Ttkkhd();
//		$model->KKNo      = Ttkkhd::generateKKNo( true );
		$model->KKNo      = General::createTemporaryNo( 'KK', 'Ttkkhd', 'KKNo' );
		$model->KKTgl     = date( 'Y-m-d H:i:s' );
		$model->KKPerson  = "--";
		$model->KKMemo    = "--";
		$model->KKNominal = 0;
		$model->KKJenis   = "Tunai";
		$model->KasKode   = ( $KasKode != null ) ? $KasKode->NoAccount : '';
		$model->PosKode   = $this->LokasiKode();
		$model->SupKode   = Tddealer::findOne(['DealerStatus'=>'1'])->DealerKode;
		$model->UserID    = $this->UserID();
		$model->KodeTrans = "H1";
		$model->NoGL      = "--";
		$model->Cetak     = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttkkhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttkkhd::find()->ttkkhdFillGetData()->where( [ 'ttkkhd.KKNo' => $model->KKNo ] )->asArray( true )->one();
		$dsTUang[ 'KKLink' ]   = '';
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'KKNoView' ] = $result[ 'KKNo' ];
		return $this->render( 'kas-keluar-ke-h1-create', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionKasKeluarKeH1Update( $id ) {
		$index = Ttkkhd::getRoute( "H1", [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		/** @var Ttkkhd $model */
		$KasKode = Traccount::find()->where( "LEFT(NoAccount,5) = '11010' AND JenisAccount = 'Detail'" )
		                    ->orderBy( 'NoAccount' )->one();
		$model   = $this->findModelBase64( 'Ttkkhd', $id );
		$dsTUang = Ttkkhd::find()->ttkkhdFillGetData()->where( [ 'ttkkhd.KKNo' => $model->KKNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post = \Yii::$app->request->post();
			$isNew = ( strpos( $post['KKNo'], '=' ) !== false);
			$post[ 'Action' ] = 'Update';
			$post[ 'KKTgl' ]    = $post[ 'KKTgl' ] . ' ' . $post[ 'KKJam' ];
			$result           = Ttkkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			if ( $result[ 'status' ] == 0 ) {
				$postit[ 'Action' ]  = $isNew ? 'Insert' : 'Update';
				$postit[ 'KKNo' ]    = $post[ 'KKNo' ];
				$postit[ 'KKBayar' ] = $post[ 'KKNominal' ];
				$postit[ 'KKLink' ]  = "KK Ke H1";
				$resultit            = Ttkkit::find()->callSP( $postit );
				if ( $resultit[ 'status' ] == 1 ) {
					Yii::$app->session->addFlash( 'warning', $resultit[ 'keterangan' ] );
				}
			}
			$model = Ttkkhd::findOne( $result[ 'KKNo' ] );
			$id    = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 ) {
				$post[ 'KKNoView' ] = $result[ 'KKNo' ];
				return $this->redirect( Ttkkhd::getRoute( "H1", [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'kas-keluar-ke-h1-update', [
					'model'   => $model,
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
		$dsTUang[ 'KasKode' ] = $dsTUang[ 'KasKode' ] ?? '--';
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttkkhd::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'KKNo' ] = $dsTUang[ 'KKNo' ];
		}
		$ttkkit                = Ttkkit::find()->where( [ 'KKNo' => $model->KKNo ] )->one();
		$dsTUang[ 'KKLink' ]   = ( $ttkkit == null ) ? '' : $ttkkit->KKLink;
		$dsTUang[ 'KKNoView' ] = $_GET[ 'KKNoView' ] ?? $result[ 'KKNo' ];
		return $this->render( 'kas-keluar-ke-h1-update', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionKasKeluarOrderPengerjaanLuar() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasKeluarOrderPengerjaanLuar', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkkhd/td' ] )
		] );
	}
	public function actionKasKeluarOrderPengerjaanLuarCreate() {
		$KasKode          = Traccount::find()->where( "LEFT(NoAccount,5) = '11010' AND JenisAccount = 'Detail'" )
		                             ->orderBy( 'NoAccount' )->one();
		$model            = new Ttkkhd();
		$model->KKNo      = General::createTemporaryNo( 'KK', 'Ttkkhd', 'KKNo' );
		$model->KKTgl     = date( 'Y-m-d H:i:s' );
		$model->KKPerson  = "--";
		$model->KKMemo    = "--";
		$model->KKNominal = 0;
		$model->KKJenis   = "Tunai";
		$model->KasKode   = ( $KasKode != null ) ? $KasKode->NoAccount : '';;
		$model->PosKode   = $this->LokasiKode();
		$model->SupKode   = Tddealer::findOne(['DealerStatus'=>'1'])->DealerKode;
		$model->UserID    = $this->UserID();
		$model->KodeTrans = "PL";
		$model->NoGL      = "--";
		$model->Cetak     = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttkkhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttkkhd::find()->ttkkhdFillGetData()->where( [ 'ttkkhd.KKNo' => $model->KKNo ] )
		                               ->asArray( true )->one();
		$dsTUang[ 'KKLink' ]   = '';
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'KKNoView' ] = $result[ 'KKNo' ];
		return $this->render( 'kas-keluar-order-pengerjaan-luar-create', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionKasKeluarOrderPengerjaanLuarUpdate( $id ) {
		$index = Ttkkhd::getRoute( "PL", [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$KasKode = Traccount::find()->where( "LEFT(NoAccount,5) = '11010' AND JenisAccount = 'Detail'" )
		                    ->orderBy( 'NoAccount' )->one();
		/** @var Ttkkhd $model */
		$model   = $this->findModelBase64( 'Ttkkhd', $id );
		$dsTUang = Ttkkhd::find()->ttkkhdFillGetData()->where( [ 'ttkkhd.KKNo' => $model->KKNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post = \Yii::$app->request->post();
			$post[ 'Action' ] = 'Update';
			$post[ 'KKTgl' ]    = $post[ 'KKTgl' ] . ' ' . $post[ 'KKJam' ];
			$result           = Ttkkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model = Ttkkhd::findOne( $result[ 'KKNo' ] );
			$id    = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 ) {
				$post[ 'KKNoView' ] = $result[ 'KKNo' ];
				return $this->redirect( Ttkkhd::getRoute( "PL", [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'kas-keluar-order-pengerjaan-luar-update', [
					'model'   => $model,
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttkkhd::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'KKNo' ] = $dsTUang[ 'KKNo' ];
		}
		$ttkkit                = Ttkkit::find()->where( [ 'KKNo' => $model->KKNo ] )->one();
		$dsTUang[ 'KKLink' ]   = ( $ttkkit == null ) ? '' : $ttkkit->KKLink;
		$dsTUang[ 'KKNoView' ] = $_GET[ 'KKNoView' ] ?? $result[ 'KKNo' ];
		return $this->render( 'kas-keluar-order-pengerjaan-luar-update', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionKasKeluarPembelian() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasKeluarPembelian', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkkhd/td' ] )
		] );
	}
	public function actionKasKeluarPembelianCreate() {
		return $this->create( 'PI', 'kas-keluar-pembelian', 'KAS KELUAR - Pembelian' );
	}
	protected function create( $KKJenis, $view, $title ) {
		$KasKode          = Traccount::find()->where( "LEFT(NoAccount,5) = '11010' AND JenisAccount = 'Detail'" )
		                             ->orderBy( 'NoAccount' )->one();
		$model            = new Ttkkhd();
		$model->KKNo      = General::createTemporaryNo( 'KK', 'Ttkkhd', 'KKNo' );
		$model->KKTgl     = date( 'Y-m-d H:i:s' );
		$model->KKPerson  = "--";
		$model->KKMemo    = "--";
		$model->KKNominal = 0;
		$model->KKJenis   = "Tunai";
		$model->KasKode   = ( $KasKode != null ) ? $KasKode->NoAccount : '';
		$model->PosKode   = $this->LokasiKode();
		$model->SupKode   = Tddealer::findOne(['DealerStatus'=>'1'])->DealerKode;

		$model->UserID    = $this->UserID();
		$model->KodeTrans = $KKJenis;
		$model->NoGL      = "--";
		$model->Cetak     = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttkkhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttkkhd::find()->ttkkhdFillGetData()->where( [ 'ttkkhd.KKNo' => $model->KKNo ] )->asArray( true )->one();
		$dsTUang[ 'KKLink' ]   = '';
		$dsTUang[ 'Total' ]    = 0;
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'KKNoView' ] = $result[ 'KKNo' ];
		return $this->render( 'create', [
			'model'   => $model,
			'view'    => $view,
			'title'   => $title,
			'dsTUang' => $dsTUang,
			'id'      => $id,
		] );
	}
	public function actionKasKeluarPembelianUpdate( $id ) {
		return $this->update( 'PI', 'kas-keluar-pembelian', 'KAS KELUAR - Pembelian', $id );
	}
	protected function update( $KKJenis, $view, $title, $id ) {
		$index = Ttkkhd::getRoute( $KKJenis, [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		/** @var Ttkkhd $model */
		$model   = $this->findModelBase64( 'Ttkkhd', $id );
		$dsTUang = Ttkkhd::find()->ttkkhdFillGetData()->where( [ 'ttkkhd.KKNo' => $model->KKNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post = \Yii::$app->request->post();
			$post[ 'KodeTrans' ] = $KKJenis;
			$post[ 'Action' ]    = 'Update';
			$post[ 'KKTgl' ]    = $post[ 'KKTgl' ] . ' ' . $post[ 'KKJam' ];
			$result              = Ttkkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model = Ttkkhd::findOne( $result[ 'KKNo' ] );
			$id    = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 ) {
				$post[ 'KKNoView' ] = $result[ 'KKNo' ];
				return $this->redirect( Ttkkhd::getRoute( $KKJenis, [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'update', [
					'model'   => $model,
					'view'    => $view,
					'title'   => $title,
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttkkhd::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'KKNo' ] = $dsTUang[ 'KKNo' ];
		}
		$ttkkit                = Ttkkit::find()->where( [ 'KKNo' => $model->KKNo ] )->one();
		$dsTUang[ 'KKLink' ]   = ( $ttkkit == null ) ? '' : $ttkkit->KKLink;
		$dsTUang[ 'KKNoView' ] = $_GET[ 'KKNoView' ] ?? $result[ 'KKNo' ];
		return $this->render( 'update', [
			'model'   => $model,
			'view'    => $view,
			'title'   => $title,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionKasKeluarReturPenjualan() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasKeluarReturPenjualan', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkkhd/td' ] )
		] );
	}
	public function actionKasKeluarReturPenjualanCreate() {
		return $this->create( 'SR', 'kas-keluar-retur-penjualan', 'KAS KELUAR - Retur Penjualan' );
	}
	public function actionKasKeluarReturPenjualanUpdate( $id ) {
		return $this->update( 'SR', 'kas-keluar-retur-penjualan', 'KAS KELUAR - Retur Penjualan', $id );
	}
	public function actionTransferAntarKas() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkkhd::find()->callSPTransfer( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'TransferAntarKas', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkkhd/td' ] )
		] );
	}
	public function actionTransferAntarKasCreate() {
		TUi::$actionMode           = Menu::ADD;
		$KasKode                   = Traccount::find()->where( "LEFT(NoAccount,5) = '11010' AND JenisAccount = 'Detail'" )->orderBy( 'NoAccount' )->one();
		$dsTUang[ 'CusNama' ]      = '--';
		$dsTUang[ 'KKNo' ]         = '--';
		$dsTUang[ 'KMNo' ]         = '--';
		$dsTUang[ 'KTNo' ]         = '--';
		$dsTUang[ 'KKTgl' ]        = date( 'Y-m-d H:i:s' );
		$dsTUang[ 'KKJam' ]        = date( 'Y-m-d H:i:s' );
		$dsTUang[ 'KTJam' ]        = date( 'Y-m-d H:i:s' );
		$dsTUang[ 'NoGLKK' ]       = '--';
		$dsTUang[ 'NoGLKM' ]       = '--';
		$dsTUang[ 'KKPerson' ]     = '--';
		$dsTUang[ 'KMPerson' ]     = '--';
		$dsTUang[ 'LokasiKodeKK' ] = ( Tdlokasi::find()->where( "LokasiStatus <> 'N'" )->orderBy( "LokasiStatus,  LokasiNomor" )->one() )->LokasiKode;
		$dsTUang[ 'LokasiKodeKM' ] = ( Tdlokasi::find()->where( "LokasiStatus <> 'N' AND LokasiKode <> :LokasiKode", [ ':LokasiKode' => $dsTUang[ 'LokasiKodeKK' ] ] )->orderBy( "LokasiStatus,  LokasiKode" )->one() )->LokasiKode;
		$dsTUang[ 'KKMemo' ]       = '--';
		$dsTUang[ 'KKJenis' ]      = 'Kas Transfer';
		$dsTUang[ 'KKNominal' ]    = 0;
		$dsTUang[ 'KasKodeKK' ]    = ( $KasKode != null ) ? $KasKode->NoAccount : '';
		$dsTUang[ 'KasKodeKM' ]    = ( $KasKode != null ) ? $KasKode->NoAccount : '';
		$dsTUang[ 'Action' ]       = 'Insert';
		$result                    = Ttkkhd::find()->callSPTransfer( $dsTUang );
		$dsTUang[ 'KTNo' ]         = $result[ 'KTNo' ];
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->render( 'kas-keluar-transfer-antar-kas-create', [
			'dsTUang' => $dsTUang,
			'id'      => base64_encode( $dsTUang[ 'KTNo' ] ),
		] );
	}
	public function actionTransferAntarKasUpdate( $id, $action ) {
		$index = Ttkkhd::getRoute( 'Kas Transfer', [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'UpdateAdd':
				TUi::$actionMode = Menu::ADD;
				break;
			case 'UpdateEdit':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		$action  = $_GET[ 'action' ];
		$dsTUang = Ttkkhd::find()->transferKasGetData()->andWhere( [ 'KKLink' => base64_decode( $id ) ] )->asArray( true )->one();
		$dsTUang = $dsTUang ?? [];
		if ( Yii::$app->request->isPost ) {
			$post             = array_merge( $dsTUang, \Yii::$app->request->post() );
			$post[ 'Action' ] = $action;
			$post[ 'KKTgl' ]  = $post[ 'KKTgl' ] . ' ' . $post[ 'KKJam' ];
			$result           = Ttkkhd::find()->callSPTransfer( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			if ( $result[ 'status' ] == 0 ) {
				return $this->redirect( Ttkkhd::getRoute( 'Kas Transfer', [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'kas-keluar-transfer-antar-kas-update', [
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
		$dsTUang[ 'KTNo' ]   = $dsTUang[ 'KKLink' ];
		$dsTUang[ 'Action' ] = 'Load';
		$result              = Ttkkhd::find()->callSPTransfer( $dsTUang );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->render( 'kas-keluar-transfer-antar-kas-update', [
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	protected function findModel( $id ) {
		if ( ( $model = Ttkkhd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	public function actionDelete() {
		/** @var Ttkkhd $model */
        $request = Yii::$app->request;
		$PK         = base64_decode( $_REQUEST[ 'id' ] );
		$model      = null;
		$isTransfer = false;
		if ( strpos( $PK, 'KT' ) !== false ) {
			$item = Ttkkit::findOne( [ 'KKLink' => $PK ] );
			$model = Ttkkhd::findOne( $item->KKNo );
			$isTransfer = true;
		} else {
			$model = Ttkkhd::findOne( $PK );
		}
		$_POST[ 'KKNo' ]   = $model->KKNo;
		$_POST[ 'Action' ] = 'Delete';
        $KKJenis = $model->KodeTrans;
		if ( $isTransfer ) {
			$_POST[ 'KTNo' ]   = $PK;
			$result = Ttkkhd::find()->callSPTransfer( $_POST );
		} else {
			$result = Ttkkhd::find()->callSP( $_POST );
		}
        if ($request->isAjax){
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ( $result[ 'KKNo' ] == '--' ) {
                return $this->redirect( Ttkkhd::getRoute( $KKJenis )[ 'index' ] );
            } else {
                $model = Ttkkhd::findOne( $result[ 'KKNo' ] );
                return $this->redirect( Ttkkhd::getRoute( $KKJenis, [ 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] )[ 'update' ] );
            }
        }
	}
	public function actionKasTransferCancel( $id ) {
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Cancel';
		$result           = Ttkkhd::find()->callSPTransfer( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'KTNo' ] == '--' ) {
			return $this->redirect( [ 'ttkkhd/transfer-antar-kas' ] );
		} else {
			return $this->redirect( [ 'ttkkhd/transfer-antar-kas-update', 'action' => 'display', 'id' => base64_encode( $result[ 'KTNo' ] ) ] );
		}
	}
	public function actionCancel( $id, $action ) {
		/** @var Ttkkhd $model */
		$model             = $this->findModelBase64( 'Ttkkhd', $id );
		$KodeTrans         = $model->KodeTrans;
		$_POST[ 'KKNo' ]   = $model->KKNo;
		$_POST[ 'Action' ] = 'Cancel';
		$post              = \Yii::$app->request->post();
		$post[ 'KKJam' ]   = $post[ 'KKTgl' ] . ' ' . $post[ 'KKJam' ];
		$result            = Ttkkhd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'KKNo' ] == '--' ) {
			return $this->redirect(Ttkkhd::getRoute( $KodeTrans )[ 'index' ]);
		} else {
			$model = Ttkkhd::findOne( $result[ 'KKNo' ] );
			return $this->redirect( Ttkkhd::getRoute( $KodeTrans, [ 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] )[ 'update' ] );
		}
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client            = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response          = $client->post( Yii::$app->params['H2'][$_POST['db']]['host_report'] . '/abengkel/fkkhd', [
			'body' => Json::encode( $_POST )
		] );
        $html                 = $response->getBody();
        return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	public function actionJurnal() {
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Jurnal';
		$result           = Ttkkhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        return $this->redirect( Ttkkhd::getRoute( $post[ 'KodeTrans' ], [ 'action' => 'display', 'id' => base64_encode( $result[ 'KKNo' ] ) ] )[ 'update' ] );
	}
}
