<?php

namespace abengkel\controllers;

use abengkel\components\AbengkelController;
use abengkel\models\Ttbkitcoa;
use Yii;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtbkitcoaController implements the CRUD actions for Ttbkitcoa model.
 */
class TtbkitcoaController extends AbengkelController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ttbkitcoa models.
     * @return mixed
     */
    public function actionIndex()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $requestData                 = \Yii::$app->request->post();
        $primaryKeys                 = Ttbkitcoa::getTableSchema()->primaryKey;
        if ( isset( $requestData[ 'oper' ] ) ) {
            /* operation : create, edit or delete */
            $oper = $requestData[ 'oper' ];
            /** @var Ttbkitcoa $model */
            $model = null;
            if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
                $model = $this->findModelBase64( 'ttbkitcoa', $requestData[ 'id' ] );
            }
            switch ( $oper ) {
                case 'add'  :
                case 'edit' :
                if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
                    $requestData[ 'Action' ]  = 'Insert';
                    $requestData[ 'BKAutoN' ] = 0;
                } else {
                    $requestData[ 'Action' ] = 'Update';
                    if ( $model != null ) {
                        $requestData[ 'BKNoOLD' ]   = $model->BKNoOLD;
                        $requestData[ 'BKAutoNOLD' ] = $model->BKAutoNOLD;
                        $requestData[ 'NoAccountOLD' ] = $model->NoAccountOLD;
                    }
                }
                $result = Ttbkitcoa::find()->callSP( $requestData );
                if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                    $response = $this->responseSuccess( $result[ 'keterangan' ] );
                } else {
                    $response = $this->responseFailed( $result[ 'keterangan' ] );
                }
                break;
                case 'batch' :
                    break;
                case 'del'  :
                    $requestData             = array_merge( $requestData, $model->attributes );
                    $requestData[ 'Action' ] = 'Delete';
                    if ( $model != null ) {
                    	$requestData[ 'BKNoOLD' ]   = $model->BKNo;
                        $requestData[ 'BKAutoNOLD' ] = $model->BKAutoN;
                        $requestData[ 'NoAccountOLD' ] = $model->NoAccount;
                    }
                    $result = Ttbkitcoa::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
                    break;
            }
        } else {
            for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
                $primaryKeys[ $i ] = 'Ttbkitcoa.' . $primaryKeys[ $i ];
            }
            $query     = ( new \yii\db\Query() )
                ->select( new Expression(
                    'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
							    ttbkitcoa.BKNo, ttbkitcoa.BKAutoN, ttbkitcoa.NoAccount, ttbkitcoa.BKBayar, 
							    ttbkitcoa.BKKeterangan, traccount.NamaAccount'
                ) )
                ->from( 'ttbkitcoa' )
                ->join( 'INNER JOIN', 'traccount', 'ttbkitcoa.NoAccount = traccount.NoAccount' )
                ->where( [ 'ttbkitcoa.BKNo' => $requestData[ 'BKNo' ] ] )
                ->orderBy( 'ttbkitcoa.BKAutoN' );
            $rowsCount = $query->count();
            $rows      = $query->all();
            /* encode row id */
            for ( $i = 0; $i < count( $rows ); $i ++ ) {
                $rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
            }
            $response = [
                'rows'      => $rows,
                'rowsCount' => $rowsCount
            ];
        }
        return $response;
    }

    /**
     * Displays a single Ttbkitcoa model.
     * @param string $BKNo
     * @param integer $BKAutoN
     * @param string $NoAccount
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($BKNo, $BKAutoN, $NoAccount)
    {
        return $this->render('view', [
            'model' => $this->findModel($BKNo, $BKAutoN, $NoAccount),
        ]);
    }

    /**
     * Creates a new Ttbkitcoa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ttbkitcoa();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'BKNo' => $model->BKNo, 'BKAutoN' => $model->BKAutoN, 'NoAccount' => $model->NoAccount]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ttbkitcoa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $BKNo
     * @param integer $BKAutoN
     * @param string $NoAccount
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($BKNo, $BKAutoN, $NoAccount)
    {
        $model = $this->findModel($BKNo, $BKAutoN, $NoAccount);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'BKNo' => $model->BKNo, 'BKAutoN' => $model->BKAutoN, 'NoAccount' => $model->NoAccount]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ttbkitcoa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $BKNo
     * @param integer $BKAutoN
     * @param string $NoAccount
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($BKNo, $BKAutoN, $NoAccount)
    {
        $this->findModel($BKNo, $BKAutoN, $NoAccount)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ttbkitcoa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $BKNo
     * @param integer $BKAutoN
     * @param string $NoAccount
     * @return Ttbkitcoa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($BKNo, $BKAutoN, $NoAccount)
    {
        if (($model = Ttbkitcoa::findOne(['BKNo' => $BKNo, 'BKAutoN' => $BKAutoN, 'NoAccount' => $NoAccount])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
