<?php

namespace abengkel\controllers;

use abengkel\components\AbengkelController;
use abengkel\models\Ttkkhd;
use abengkel\models\Ttkkit;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtkkitController implements the CRUD actions for Ttkkit model.
 */
class TtkkitController extends AbengkelController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ttkkit models.
     * @return mixed
     */
    public function actionIndex()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $requestData                 = \Yii::$app->request->post();
        $primaryKeys                 = Ttkkit::getTableSchema()->primaryKey;
        if ( isset( $requestData[ 'oper' ] ) ) {
            /* operation : create, edit or delete */
            $oper = $requestData[ 'oper' ];
            /** @var Ttkkit $model */
            $model = null;
            if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
                $model = $this->findModelBase64( 'ttkkit', $requestData[ 'id' ] );
            }
            switch ( $oper ) {
                case 'add'  :
                case 'edit' :
                if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
                    $requestData[ 'Action' ] = 'Insert';
                    $requestData[ 'KKLink' ] = '--';
                } else {
                    $requestData[ 'Action' ] = 'Update';
                    if ( $model != null ) {
                        $requestData[ 'KKNoOLD' ]   = $model->KKNo;
                        $requestData[ 'KKLinkOLD' ] = $model->KKLink;
                    }
                }
                $result = Ttkkit::find()->callSP( $requestData );
                if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                    $response = $this->responseSuccess( $result[ 'keterangan' ] );
                } else {
                    $response = $this->responseFailed( $result[ 'keterangan' ] );
                }
                break;
                case 'batch' :
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    $respon                      = $this->responseFailed( 'Add Item' );
                    $header                      = $requestData[ 'header' ];
                    $header[ 'Action' ]          = 'Loop';
                    $result                      = Ttkkhd::find()->callSP( $header );
                    foreach ( $requestData[ 'data' ] as $item ) {
                        $items[ 'Action' ]  = 'Insert';
                        $items[ 'KKNo' ]    = $header[ 'KKNo' ];
	                    $items[ 'KKLink' ]  = $item[ 'data' ][ 'NO' ];
                        $items[ 'KKBayar' ] = $item[ 'data' ][ 'Sisa' ];
                        $result             = Ttkkit::find()->callSP( $items );
                    }
                    $header[ 'Action' ]    = 'LoopAfter';
                    $result                = Ttkkhd::find()->callSP( $header );
                    return $this->responseSuccess( $requestData );
                    break;
                case 'del'  :
                    $requestData             = array_merge( $requestData, $model->attributes );
                    $requestData[ 'Action' ] = 'Delete';
                    if ( $model != null ) {
                        $requestData[ 'KKNoOLD' ]   = $model->KKNo;
                        $requestData[ 'KKLinkOLD' ] = $model->KKLink;
                    }
                    $result = Ttkkit::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
                    break;
            }
        } else {
            for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
                $primaryKeys[ $i ] = 'Ttkkit.' . $primaryKeys[ $i ];
            }
	        /** @var Ttkkhd $model */
	        $model = Ttkkhd::find()->where( [ 'KKNo' => $requestData[ 'KKNo' ] ] )->one();
	        $query = new Query();
	        switch ( $model->KodeTrans ) {
		        case 'PI' :
			        $query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,KKNo, KKLink, KKBayar,PITgl AS TglLink, SupKode AS BiayaNama,
			        NamaTerang, PITotal AS Total, KKPaid.Paid - KKBayar AS Terbayar,  PITotal - KKPaid.Paid AS Sisa' ) )
			              ->from( new Expression( "(SELECT ttkkit.*, ttpihd.*,tdsupplier.SupNama As NamaTerang FROM ttkkit 
                     INNER JOIN ttpihd ON KKLink = PINo 
                     INNER JOIN tdsupplier ON ttpihd.SupKode = tdsupplier.SupKode  
                     WHERE ttkkit.KKNo = :KKNo) ttkkit" ) )
			              ->addParams( [ ':KKNo' => $model->KKNo ] )
			              ->innerJoin( "(SELECT KBKLINK, IFNULL(SUM(KBKBayar), 0) AS Paid FROM vtkBK  GROUP BY KBKLINK) KKPaid",
				              "ttkkit.KKLink = KKPaid.KBKLINK" );
			        break;
		        case 'SR' :
			        $query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,KKNo, KKLink, KKBayar,SRTgl AS TglLink, MotorNoPolisi AS BiayaNama,NamaTerang,
               SRTotal AS Total, KKPaid.Paid - KKBayar AS Terbayar,  SRTotal - KKPaid.Paid AS Sisa' ) )
			              ->from( new Expression( "(SELECT ttkkit.*,ttsrhd.*, tdcustomer.CusNama As NamaTerang FROM ttkkit 
                     INNER JOIN ttsrhd ON KKLink = SRNo 
                     INNER JOIN tdcustomer ON ttsrhd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                     WHERE KKNo = :KKNo) ttkkit" ) )
			              ->addParams( [ ':KKNo' => $model->KKNo ] )
			              ->innerJoin( "(SELECT KBKLINK, IFNULL(SUM(KBKBayar), 0) AS Paid FROM vtKBK  GROUP BY KBKLINK) KKPaid",
				              "ttkkit.KKLink = KKPaid.KBKLINK" );
			        break;
	        }

            $rowsCount = $query->count();
            $rows      = $query->all();
            /* encode row id */
            for ( $i = 0; $i < count( $rows ); $i ++ ) {
                $rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
            }
            $response = [
                'rows'      => $rows,
                'rowsCount' => $rowsCount
            ];
        }
        return $response;
    }

    /**
     * Displays a single Ttkkit model.
     * @param string $KKNo
     * @param string $KKLink
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($KKNo, $KKLink)
    {
        return $this->render('view', [
            'model' => $this->findModel($KKNo, $KKLink),
        ]);
    }

    /**
     * Creates a new Ttkkit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ttkkit();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'KKNo' => $model->KKNo, 'KKLink' => $model->KKLink]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ttkkit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $KKNo
     * @param string $KKLink
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($KKNo, $KKLink)
    {
        $model = $this->findModel($KKNo, $KKLink);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'KKNo' => $model->KKNo, 'KKLink' => $model->KKLink]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ttkkit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $KKNo
     * @param string $KKLink
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($KKNo, $KKLink)
    {
        $this->findModel($KKNo, $KKLink)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ttkkit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $KKNo
     * @param string $KKLink
     * @return Ttkkit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($KKNo, $KKLink)
    {
        if (($model = Ttkkit::findOne(['KKNo' => $KKNo, 'KKLink' => $KKLink])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
