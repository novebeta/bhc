<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\models\Ttbkhd;
use abengkel\models\Ttbkit;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtbkitController implements the CRUD actions for Ttbkit model.
 */
class TtbkitController extends AbengkelController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttbkit models.
	 * @return mixed
	 */
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttbkit::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
			/** @var Ttbkit $model */
			$model = null;
			if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
				$model = $this->findModelBase64( 'ttbkit', $requestData[ 'id' ] );
			}
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'Action' ] = 'Insert';
						$requestData[ 'BKLink' ] = '--';
					} else {
						$requestData[ 'Action' ] = 'Update';
						if ( $model != null ) {
							$requestData[ 'BKNoOLD' ]   = $model->BKNo;
							$requestData[ 'BKLinkOLD' ] = $model->BKLink;
						}
					}
					$result = Ttbkit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'batch' :
					\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
					$respon                      = $this->responseFailed( 'Add Item' );
					$base64                      = urldecode( base64_decode( $requestData[ 'header' ] ) );
					parse_str( $base64, $header );
//					$header                      = $requestData[ 'header' ];
					$header[ 'Action' ]          = 'Loop';
					$result                      = Ttbkhd::find()->callSP( $header );
					foreach ( $requestData[ 'data' ] as $item ) {
						$items[ 'Action' ]  = 'Insert';
						$items[ 'BKNo' ]    = $header[ 'BKNo' ];
						$items[ 'BKLink' ]  = $item[ 'data' ][ 'NO' ];
						$items[ 'BKBayar' ] = $item[ 'data' ][ 'Sisa' ];
						$result             = Ttbkit::find()->callSP( $items );
					}
					$header[ 'Action' ] = 'LoopAfter';
					$result             = Ttbkhd::find()->callSP( $header );
					return $this->responseSuccess( $requestData );
					break;
				case 'del'  :
					$requestData             = array_merge( $requestData, $model->attributes );
					$requestData[ 'Action' ] = 'Delete';
					if ( $model != null ) {
						$requestData[ 'BKNoOLD' ]   = $model->BKNo;
						$requestData[ 'BKLinkOLD' ] = $model->BKLink;
					}
					$result = Ttbkit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
			}
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'Ttbkit.' . $primaryKeys[ $i ];
			}
			/** @var Ttbkhd $model */
			$model = Ttbkhd::find()->where( [ 'BKNo' => $requestData[ 'BKNo' ] ] )->one();
			$query = new Query();
			switch ( $model->KodeTrans ) {
				case 'PI' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,BKNo, BKLink, BKBayar,PITgl AS TglLink, SupKode AS BiayaNama,
			        NamaTerang,PITotal AS Total, BKPaid.Paid - BKBayar AS Terbayar,  PITotal - BKPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(SELECT ttBKit.*, ttpihd.*, tdsupplier.SupNama As NamaTerang FROM ttBKit 
                     INNER JOIN ttpihd ON BKLink = PINo 
                     INNER JOIN tdsupplier ON ttpihd.SupKode = tdsupplier.SupKode  
                     WHERE ttBKit.BKNo = :BKNo) ttBKit" ) )
					      ->addParams( [ ':BKNo' => $model->BKNo ] )
					      ->innerJoin( "(SELECT KBKLINK, IFNULL(SUM(KBKBayar), 0) AS Paid FROM vtkbk  GROUP BY KBKLINK) BKPaid",
						      "ttBKit.BKLink = BKPaid.KBKLINK" );
					break;
				case 'SR' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,BKNo, BKLink, BKBayar,SRTgl AS TglLink, MotorNoPolisi AS BiayaNama,
			        NamaTerang, SRTotal AS Total, BKPaid.Paid - BKBayar AS Terbayar,  SRTotal - BKPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(SELECT ttBKit.*, ttsrhd.*, tdcustomer.CusNama As NamaTerang FROM ttBKit 
                     INNER JOIN ttsrhd ON BKLink = SRNo
                     INNER JOIN tdcustomer ON ttsrhd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                     WHERE BKNo = :BKNo) ttBKit" ) )
					      ->addParams( [ ':BKNo' => $model->BKNo ] )
					      ->innerJoin( "(SELECT KBKLINK, IFNULL(SUM(KBKBayar), 0) AS Paid FROM vtkbk  GROUP BY KBKLINK) BKPaid",
						      "ttBKit.BKLink = BKPaid.KBKLINK" );
					break;
			}
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	/**
	 * Displays a single Ttbkit model.
	 *
	 * @param string $BKNo
	 * @param string $BKLink
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $BKNo, $BKLink ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $BKNo, $BKLink ),
		] );
	}
	/**
	 * Creates a new Ttbkit model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Ttbkit();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'BKNo' => $model->BKNo, 'BKLink' => $model->BKLink ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Ttbkit model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $BKNo
	 * @param string $BKLink
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $BKNo, $BKLink ) {
		$model = $this->findModel( $BKNo, $BKLink );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'BKNo' => $model->BKNo, 'BKLink' => $model->BKLink ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Ttbkit model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $BKNo
	 * @param string $BKLink
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $BKNo, $BKLink ) {
		$this->findModel( $BKNo, $BKLink )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Ttbkit model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $BKNo
	 * @param string $BKLink
	 *
	 * @return Ttbkit the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $BKNo, $BKLink ) {
		if ( ( $model = Ttbkit::findOne( [ 'BKNo' => $BKNo, 'BKLink' => $BKLink ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
