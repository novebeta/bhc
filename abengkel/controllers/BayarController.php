<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\models\Ttbkhd;
use abengkel\models\Ttbkit;
use abengkel\models\Ttbmhd;
use abengkel\models\Ttbmit;
use abengkel\models\Ttkkhd;
use abengkel\models\Ttkkit;
use abengkel\models\Ttkmhd;
use abengkel\models\Ttkmit;
use common\components\General;
use yii\filters\VerbFilter;
/**
 * Site controller
 */
class BayarController extends AbengkelController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'logout' => [ 'post' ],
				],
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}
	public function actionSave() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$isKas                       = ( $requestData[ 'CaraBayar' ] == 'Tunai' ) || ( $requestData[ 'CaraBayar' ] == 'Kas Kredit' );
		$realBayar                   = ( floatval( $requestData[ 'BMNominal' ] ) - floatval( $requestData[ 'BMBayar' ] ) < 0 ) ? floatval( $requestData[ 'BMNominal' ] ) : floatval( $requestData[ 'BMBayar' ] );
		switch ( $requestData[ 'MyKodeTrans' ] ) {
			case 'SV':
			case 'SI':
			case 'PR':
			case 'SO':
			case 'SE':
				if ( $isKas ) {
					\Yii::$app->db->transaction( function () use ( $requestData, $realBayar ) {
						$model                = new Ttkmhd();
						$model->KMNo          = General::createTemporaryNo( 'KM', 'Ttkmhd', 'KMNo' );
						$model->KMTgl         = $requestData[ 'BMTgl' ] . ' ' . $requestData[ 'BMJam' ];
						$model->KMJenis       = $requestData[ 'CaraBayar' ];
						$model->KasKode       = $requestData[ 'NoAccount' ];
						$model->MotorNoPolisi = $requestData[ 'MyKode' ];
						$model->KMPerson      = $requestData[ 'MyNama' ];
						$model->KMMemo        = $requestData[ 'Memo' ];
						$model->KMBayarTunai  = floatval( $requestData[ 'BMBayar' ] );
						$model->KMNominal     = $realBayar;
						$model->UserID        = $this->UserID();
						$model->KodeTrans     = $requestData[ 'MyKodeTrans' ];
						$model->PosKode       = $this->LokasiKode();
						$model->Cetak         = "Belum";
						$model->save();
						$post                  = $model->attributes;


						$postItem[ 'Action' ]  = 'Insert';
						$postItem[ 'KMNo' ]    = $post[ 'KMNo' ];
						$postItem[ 'KMLink' ]  = $requestData[ 'MyNoTransaksi' ];
						$postItem[ 'KMBayar' ] = $realBayar;
						$resultItem            = Ttkmit::find()->callSP( $postItem );

						$post[ 'Action' ]      = 'Update';
						$resultHeader          = Ttkmhd::find()->callSP( $post );

						$post[ 'Action' ]      = 'LoopAfter';
						$resultHeader          = Ttkmhd::find()->callSP( $post );

						$resultJurnal          = Ttkmhd::find()->callSPJKM( $resultHeader );
					},\yii\db\Transaction::SERIALIZABLE );
				} else {
					\Yii::$app->db->transaction( function () use ( $requestData, $realBayar ) {
						$model                = new Ttbmhd();
						$model->BMNo          = General::createTemporaryNo( 'BM', 'Ttbmhd', 'BMNo' );
						$model->BMTgl         = $requestData[ 'BMTgl' ] . ' ' . $requestData[ 'BMJam' ];
						$model->BMJenis       = $requestData[ 'CaraBayar' ];
						$model->BankKode      = $requestData[ 'NoAccount' ];
						$model->MotorNoPolisi = $requestData[ 'MyKode' ];
						$model->BMPerson      = $requestData[ 'MyNama' ];;
						$model->BMMemo     = $requestData[ 'Memo' ];
						$model->BMNominal  = $realBayar;
						$model->BMCekNo    = "--";
						$model->BMCekTempo = $requestData[ 'BMTgl' ] . ' ' . $requestData[ 'BMJam' ];
						$model->BMAC       = $requestData[ 'NoKartu' ];
						$model->UserID     = $this->UserID();
						$model->KodeTrans  = $requestData[ 'MyKodeTrans' ];
						$model->PosKode    = $this->LokasiKode();
						$model->Cetak      = "Belum";
						$model->save();
						$post                  = $model->attributes;

						$postItem[ 'Action' ]  = 'Insert';
						$postItem[ 'BMNo' ]    = $post[ 'BMNo' ];
						$postItem[ 'BMLink' ]  = $requestData[ 'MyNoTransaksi' ];
						$postItem[ 'BMBayar' ] = $realBayar;
						$resultItem            = Ttbmit::find()->callSP( $postItem );

						$post[ 'Action' ]      = 'Update';
						$resultHeader          = Ttbmhd::find()->callSP( $post );

						$post[ 'Action' ]      = 'LoopAfter';
						$resultHeader          = Ttbmhd::find()->callSP( $post );

						$resultJurnal          = Ttbmhd::find()->callSPJBM( $resultHeader );
					},\yii\db\Transaction::SERIALIZABLE  );
				}
				break;
			case 'SR':
			case 'PI':
				if ( $isKas ) {
					\Yii::$app->db->transaction( function () use ( $requestData, $realBayar ) {
						$model            = new Ttkkhd();
						$model->KKNo      = General::createTemporaryNo( 'KK', 'TtKKhd', 'KKNo' );
						$model->KKTgl     = $requestData[ 'BMTgl' ] . ' ' . $requestData[ 'BMJam' ];
						$model->KKJenis   = $requestData[ 'CaraBayar' ];
						$model->KasKode   = $requestData[ 'NoAccount' ];
						$model->SupKode   = $requestData[ 'MyKode' ];
						$model->KKPerson  = $requestData[ 'MyNama' ];
						$model->KKMemo    = $requestData[ 'Memo' ];
						$model->KKNominal = $realBayar;
						$model->UserID    = $this->UserID();
						$model->KodeTrans = $requestData[ 'MyKodeTrans' ];
						$model->PosKode   = $this->LokasiKode();
						$model->Cetak     = "Belum";
						$model->save();
						$post                  = $model->attributes;

						$postItem[ 'Action' ]  = 'Insert';
						$postItem[ 'KKNo' ]    = $post[ 'KKNo' ];
						$postItem[ 'KKLink' ]  = $requestData[ 'MyNoTransaksi' ];
						$postItem[ 'KKBayar' ] = $realBayar;
						$resultItem            = TtKKit::find()->callSP( $postItem );

						$post[ 'Action' ]      = 'Update';
						$resultHeader          = TtKKhd::find()->callSP( $post );

						$post[ 'Action' ]      = 'LoopAfter';
						$resultHeader          = TtKKhd::find()->callSP( $post );

						$resultJurnal          = TtKKhd::find()->callSPJKK( $resultHeader );
					} ,\yii\db\Transaction::SERIALIZABLE );
				} else {
					\Yii::$app->db->transaction( function () use ( $requestData, $realBayar ) {
						$model           = new Ttbkhd();
						$model->BKNo     = General::createTemporaryNo( 'BK', 'TtBKhd', 'BKNo' );
						$model->BKTgl    = $requestData[ 'BMTgl' ] . ' ' . $requestData[ 'BMJam' ];
						$model->BKJenis  = $requestData[ 'CaraBayar' ];
						$model->BankKode = $requestData[ 'NoAccount' ];
						$model->SupKode  = $requestData[ 'MyKode' ];
						$model->BKPerson = $requestData[ 'MyNama' ];;
						$model->BKMemo     = $requestData[ 'Memo' ];
						$model->BKNominal  = $realBayar;
						$model->BKCekNo    = "--";
						$model->BKCekTempo = $requestData[ 'BMTgl' ] . ' ' . $requestData[ 'BMJam' ];
						$model->BKAC       = $requestData[ 'NoKartu' ];
						$model->UserID     = $this->UserID();
						$model->KodeTrans  = $requestData[ 'MyKodeTrans' ];
						$model->PosKode    = $this->LokasiKode();
						$model->Cetak      = "Belum";
						$model->save();
						$post                  = $model->attributes;

						$postItem[ 'Action' ]  = 'Insert';
						$postItem[ 'BKNo' ]    = $post[ 'BKNo' ];
						$postItem[ 'BKLink' ]  = $requestData[ 'MyNoTransaksi' ];
						$postItem[ 'BKBayar' ] = $realBayar;
						$resultItem            = TtBKit::find()->callSP( $postItem );

						$post[ 'Action' ]      = 'Update';
						$resultHeader          = TtBKhd::find()->callSP( $post );

						$post[ 'Action' ]      = 'LoopAfter';
						$resultHeader          = TtBKhd::find()->callSP( $post );

						$resultJurnal          = TtBKhd::find()->callSPJBK( $resultHeader );
					} ,\yii\db\Transaction::SERIALIZABLE );
				}
				break;
			case 'PL':
				if ( $isKas ) {
					\Yii::$app->db->transaction( function () use ( $requestData, $realBayar ) {
						$model            = new Ttkkhd();
						$model->KKNo      = General::createTemporaryNo( 'KK', 'TtKKhd', 'KKNo' );
						$model->KKTgl     = $requestData[ 'BMTgl' ] . ' ' . $requestData[ 'BMJam' ];
						$model->KKJenis   = $requestData[ 'CaraBayar' ];
						$model->KasKode   = $requestData[ 'NoAccount' ];
						$model->SupKode   = $requestData[ 'MyKode' ];
						$model->KKPerson  = $requestData[ 'MyNama' ];
						$model->KKMemo    = $requestData[ 'Memo' ];
						$model->KKNominal = floatval( $requestData[ 'BMBayar' ] );
						$model->UserID    = $this->UserID();
						$model->KodeTrans = $requestData[ 'MyKodeTrans' ];
						$model->PosKode   = $this->LokasiKode();
						$model->Cetak     = "Belum";
						$model->save();
						$post                  = $model->attributes;

						$postItem[ 'Action' ]  = 'Insert';
						$postItem[ 'KKNo' ]    = $post[ 'KKNo' ];
						$postItem[ 'KKLink' ]  = $requestData[ 'MyNoTransaksi' ];
						$postItem[ 'KKBayar' ] = floatval( $requestData[ 'BMBayar' ] );
						$resultItem            = TtKKit::find()->callSP( $postItem );

						$post[ 'Action' ]      = 'Update';
						$resultHeader          = TtKKhd::find()->callSP( $post );

						$post[ 'Action' ]      = 'LoopAfter';
						$resultHeader          = TtKKhd::find()->callSP( $post );

						$resultJurnal          = TtKKhd::find()->callSPJKK( $resultHeader );
					} ,\yii\db\Transaction::SERIALIZABLE );
				} else {
					\Yii::$app->db->transaction( function () use ( $requestData, $realBayar ) {
						$model           = new Ttbkhd();
						$model->BKNo     = General::createTemporaryNo( 'BK', 'TtBKhd', 'BKNo' );
						$model->BKTgl    = $requestData[ 'BMTgl' ] . ' ' . $requestData[ 'BMJam' ];
						$model->BKJenis  = $requestData[ 'CaraBayar' ];
						$model->BankKode = $requestData[ 'NoAccount' ];
						$model->SupKode  = $requestData[ 'MyKode' ];
						$model->BKPerson = $requestData[ 'MyNama' ];;
						$model->BKMemo     = $requestData[ 'Memo' ];
						$model->BKNominal  = floatval( $requestData[ 'BMBayar' ] );
						$model->BKCekNo    = "--";
						$model->BKCekTempo = $requestData[ 'BMTgl' ] . ' ' . $requestData[ 'BMJam' ];
						$model->BKAC       = $requestData[ 'NoKartu' ];
						$model->UserID     = $this->UserID();
						$model->KodeTrans  = $requestData[ 'MyKodeTrans' ];
						$model->PosKode    = $this->LokasiKode();
						$model->Cetak      = "Belum";
						$model->save();
						$post                  = $model->attributes;

						$postItem[ 'Action' ]  = 'Insert';
						$postItem[ 'BKNo' ]    = $post[ 'BKNo' ];
						$postItem[ 'BKLink' ]  = $requestData[ 'MyNoTransaksi' ];
						$postItem[ 'BKBayar' ] = floatval( $requestData[ 'BMBayar' ] );
						$resultItem            = TtBKit::find()->callSP( $postItem );

						$post[ 'Action' ]      = 'Update';
						$resultHeader          = TtBKhd::find()->callSP( $post );

						$post[ 'Action' ]      = 'LoopAfter';
						$resultHeader          = TtBKhd::find()->callSP( $post );

						$resultJurnal          = TtBKhd::find()->callSPJBK( $resultHeader );
					} ,\yii\db\Transaction::SERIALIZABLE );
				}
				break;
		}
		return $requestData;
	}
}
