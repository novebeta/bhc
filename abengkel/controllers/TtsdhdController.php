<?php
namespace abengkel\controllers;
use Yii;
use yii\helpers\Url;
use yii\helpers\Json;
use GuzzleHttp\Client;
use yii\db\Expression;
use abengkel\models\Vtkbm;
use common\components\Api;
use abengkel\models\Ttpihd;
use abengkel\models\Ttsdhd;
use yii\filters\VerbFilter;
use abengkel\components\Menu;
use common\components\General;
use abengkel\models\Ttsdittime;
use abengkel\components\TdAction;
use abengkel\models\Ttsditbarang;
use yii\web\NotFoundHttpException;
use abengkel\models\Tdbaranglokasi;
use abengkel\components\AbengkelController;
use abengkel\models\Ttgeneralledgerhd;
use yii\log\Logger;

/**
 * TtsdhdController implements the CRUD actions for Ttsdhd model.
 */
class TtsdhdController extends AbengkelController {
	public function actions() {
		$sqlraw     = null;
		$queryRawPK = null;
		$colGrid    = null;
		$where      = [];
		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			switch ( $r ) {
				case 'ttsdhd/daftar-servis':
					$colGrid = Ttsdhd::colGridDaftarServis();
                    $where      = [
                        [
                            'op'        => 'AND',
                            'condition' => "(SDNo NOT LIKE '%=%')",
                            'params'    => []
                        ]
                    ];
                    if(\abengkel\components\Menu::showOnlyHakAkses(['warehouse','gudang'])){
						$whereRaw = '';
						if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
							$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
						}
						$sqlraw     = "SELECT ttsdhd.SDNo, SDTgl, SDJamMasuk, SDNoUrut, SDStatus, SDKmSekarang, SDKmBerikut, SDKeluhan, 
							KarKode, ttsdhd.LokasiKode, SDJamDikerjakan, SDJamSelesai, SDTotalWaktu, SDTotalJasa, SDTotalQty, SDTotalPart, 
							SDTotalGratis, SDTotalNonGratis, SDDiscFinal, SDTotalPajak, SDTotalBiaya, SVNo, SVTgl, UserID,KodeTrans,
							ttsdhd.MotorNoPolisi, tdcustomer.CusNama , tdcustomer.MotorNama , tdcustomer.MotorWarna, PKBNo, IFNULL(ttsditbarang.Cetak,'--') AS PLCetak, tdcustomer.MotorNoMesin
							FROM ttsdhd INNER JOIN tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi
							LEFT OUTER JOIN (SELECT SDNo, Cetak , SDPickingPrint FROM ttsditbarang  GROUP BY SDNo) ttsditbarang ON ttsdhd.SDNo = ttsditbarang.SDNo
							WHERE ttsdhd.SDNo NOT LIKE '%=%'";
						$queryRawPK = [ 'SDNo' ];
					}
					break;
				case 'ttsdhd/invoice-servis':
					$colGrid = Ttsdhd::colGridInvoiceServis();
                    $where      = [
                        [
                            'op'        => 'AND',
                            'condition' => "(SDNo NOT LIKE '%=%')",
                            'params'    => []
                        ]
                    ];
					if(\abengkel\components\Menu::showOnlyHakAkses(['warehouse','gudang'])){
						$whereRaw = '';
						if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
							$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
						}
						$sqlraw     = "SELECT ttsdhd.SDNo, SDTgl, SDJamMasuk, SDNoUrut, SDStatus, SDKmSekarang, SDKmBerikut, SDKeluhan, 
							KarKode, ttsdhd.LokasiKode, SDJamDikerjakan, SDJamSelesai, SDTotalWaktu, SDTotalJasa, SDTotalQty, SDTotalPart, 
							SDTotalGratis, SDTotalNonGratis, SDDiscFinal, SDTotalPajak, SDTotalBiaya, SVNo, SVTgl, UserID,KodeTrans,
							ttsdhd.MotorNoPolisi, tdcustomer.CusNama , tdcustomer.MotorNama , tdcustomer.MotorWarna, PKBNo, IFNULL(ttsditbarang.Cetak,'--') AS PLCetak
							FROM ttsdhd INNER JOIN tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi
							LEFT OUTER JOIN (SELECT SDNo, Cetak , SDPickingPrint FROM ttsditbarang  GROUP BY SDNo) ttsditbarang ON ttsdhd.SDNo = ttsditbarang.SDNo
							WHERE ttsdhd.SDNo NOT LIKE '%=%'";
						$queryRawPK = [ 'SDNo' ];
					}
					break;
				case 'ttsdhd/select':
					$colGrid    = Ttsdhd::colGridSD();
                    $whereRaw = '';
                    if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
                        $whereRaw = "AND DATE({$json['cmbTgl1']}) >= DATE('{$json['tgl1']}') AND DATE({$json['cmbTgl1']}) <= DATE('{$json['tgl2']}') ";
                    }
					$sqlraw     = "SELECT ttSDhd.*, tdcustomer.CusNama
                                  FROM ttSDhd 
                                  INNER JOIN  tdcustomer ON tdcustomer.MotorNoPolisi = ttSDhd.MotorNoPolisi
                                  LEFT OUTER JOIN ttsihd ON ttSDhd.SDNo = ttsihd.SDNo 
                                  WHERE ttSDhd.SDNo NOT LIKE '%=%' {$whereRaw}
                                  GROUP BY ttSDhd.SDNo ORDER BY ttSDhd.SDNo";
					$queryRawPK = [ 'SDNo' ];
					break;
			}
		}
		return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Ttsdhd::class,
				'colGrid'    => $colGrid,
				'queryRaw'   => $sqlraw,
				'queryRawPK' => $queryRawPK,
                'where'      => $where,
				'joinWith'   => [
					'customer' => [
						'type' => 'LEFT JOIN'
					],
				]
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionSelect() {
		$urlDetail    = Url::toRoute( [ 'ttsditbarang/index', 'jenis' => 'FillItemSDSI' ] );
		$this->layout = 'select-mode';
		return $this->render( 'select', [
			'arr'       => [
				'cmbTxt'    => [
					'SDNo'          => 'No SD',
					'MotorNoPolisi' => 'No Polisi',
					'KarKode'       => 'Karyawan',
					'Cetak'         => 'Cetak',
					'KodeTrans'     => 'Kode Trans',
					'SDKeterangan'  => 'Keterangan',
					'SINo'          => 'No SI',
					'PONo'          => 'No PO',
					'PosKode'       => 'Pos',
					'UserID'        => 'UserID',
				],
				'cmbNum'    => [
				],
				'cmbTgl'    => [
					'SDTgl' => 'Tanggal SD',
				],
				'sortname'  => "SDTgl",
				'sortorder' => 'desc'
			],
			'options'   => [
				'colGrid' => Ttsdhd::colGridSD(),
				'mode'    => self::MODE_SELECT_DETAIL_MULTISELECT
			],
			'title'     => 'Daftar Service',
			'urlDetail' => $urlDetail
		] );
	}

    public function actionImport()
    {
        $this->layout = 'select-mode';
        return $this->render('import', [
			'title'     => 'Daftar Service',
            'colGrid' => Ttsdhd::colGridImportItems(),
            'mode' => self::MODE_SELECT_SHOW_DETAIL,
        ]);
    }

    public function actionImportItems()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $fromTime = Yii::$app->formatter->asDate('-90 day', 'yyyy-MM-dd');
        $toTime = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
		$addParam = [];
        if (isset($_POST['query'])) {
            $query = Json::decode($_POST['query'], true);
            $fromTime = $query['tgl1'] ?? $fromTime;
            $toTime = $query['tgl2'] ?? $toTime;
			if($query['txt1'] !== ''){
				$addParam = array_merge_recursive($addParam,[$query['cmbTxt1']=> $query['txt1']]);
			}
        }
        $api = new Api();
        $mainDealer = $api->getMainDealer();
        $arrHasil = [];
        switch ($mainDealer) {
            case API::MD_HSO:
                $arrParams = [
                    'fromTime' => $fromTime . ' 00:00:00',
                    'toTime' => $toTime . ' 23:59:59',
                    'dealerId' => $api->getDealerId(),
                    'NoWorkOrder' => '',
                ];

                break;
            case API::MD_ANPER:
                $arrParams = [
                    'fromTime' => $fromTime . ' 00:00:00',
                    'toTime' => $toTime . ' 23:59:59',
                    'dealerId' => $api->getDealerId(),
                    'requestServerTime' => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                    'requestServertimezone' => date('T'),
                    'requestServerTimestamp' => $api->getCurrUnixTime(),
                ];
                break;
			default:
				$arrParams = [
					'fromTime'               => $fromTime . ' 00:00:00',
					'toTime'                 => $toTime . ' 23:59:59',
					'dealerId'               => $api->getDealerId(),
					'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
					'requestServertimezone'  => date('T'),
					'requestServerTimestamp' => $api->getCurrUnixTime(),
				];
        }
		$arrParams = array_merge_recursive($arrParams,$addParam);
		$filterResult = [];
		if($query['txt2'] !== ''){
			$filterResult = [
				'key' => $query['cmbTxt2'],
				'value' => $query['txt2'],
			];
		}
        $hasil = $api->call(Api::TYPE_PKB, Json::encode($arrParams),$filterResult);
        $arrHasil = Json::decode($hasil);
        if (!isset($arrHasil['data'])) {
            return [
                'rows' => [],
                'rowsCount' => 0
            ];
        }
        $_SESSION['ttsdhd']['import'] = $arrHasil['data'];
        return [
            'rows' => $arrHasil['data'],
            'rowsCount' => sizeof($arrHasil['data'])
        ];
    }

    public function actionImportJasa()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $key = array_search($_POST['noWorkOrder'], array_column($_SESSION['ttsdhd']['import'], 'noWorkOrder'));
        return [
            'rows' => $_SESSION['ttsdhd']['import'][$key]['services'],
            'rowsCount' => sizeof($_SESSION['ttsdhd']['import'][$key]['services'])
        ];
    }
    public function actionImportPart()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $key = array_search($_POST['noWorkOrder'], array_column($_SESSION['ttsdhd']['import'], 'noWorkOrder'));
        return [
            'rows' => $_SESSION['ttsdhd']['import'][$key]['parts'],
            'rowsCount' => sizeof($_SESSION['ttsdhd']['import'][$key]['parts'])
        ];
    }

	public function actionOrder() {
		$urlDetail    = Url::toRoute( [ 'ttsditbarang/index', 'jenis' => 'FillItemSDSI' ] );
		$this->layout = 'select-mode';
		return $this->render( 'select', [
			'arr'       => [
				'cmbTxt'    => [
					'SDNo'          => 'No SD',
					'MotorNoPolisi' => 'No Polisi',
					'KarKode'       => 'Karyawan',
					'Cetak'         => 'Cetak',
					'KodeTrans'     => 'Kode Trans',
					'SDKeterangan'  => 'Keterangan',
					'SINo'          => 'No SI',
					'PONo'          => 'No PO',
					'PosKode'       => 'Pos',
					'UserID'        => 'UserID',
				],
				'cmbNum'    => [
				],
				'cmbTgl'    => [
					'SDTgl' => 'Tanggal SD',
				],
				'sortname'  => "SDTgl",
				'sortorder' => 'desc'
			],
			'options'   => [
				'colGrid' => Ttsdhd::colGridSD(),
				'mode'    => self::MODE_SELECT_MULTISELECT
			],
			'title'     => 'Daftar Service',
			'urlDetail' => $urlDetail
		] );
	}
	public function actionLoop() {
		$_POST[ 'Action' ] = 'Loop';
		$result            = Ttsdhd::find()->callSP( $_POST );
		if ( $result[ 'status' ] == 1 ) {
			Yii::$app->session->addFlash( 'warning', $result[ 'keterangan' ] );
		}
	}
	public function actionDaftarServis() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttsdhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'DaftarServis', [ 'Jenis' => 'SD' ] );
	}
	public function actionDaftarServisCreate() {
		return $this->create( 'SD' );
	}
	public function actionDaftarServisUpdate( $id, $action ) {
		return $this->update( $id, $action, 'SD' );
	}
	public function actionDaftarServisDelete( $id ) {
		return $this->delete( $id, 'SD' );
	}
	public function actionDaftarServisCancel($id) {
		return $this->cancel( $id,'SD' );
	}
    public function actionDaftarServisJurnal() {
        return $this->jurnal( 'SD' );
    }
	public function actionDaftarServisPrint( $id ) {
		return $this->print( $id, 'SD' );
	}
	public function actionInvoiceServis() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttsdhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'InvoiceServis', [ 'Jenis' => 'SV' ] );
	}
	public function actionInvoiceServisCreate() {
		return $this->create( 'SV' );
	}
	public function actionInvoiceServisUpdate( $id, $action ) {
		return $this->update( $id, $action, 'SV' );
	}
	public function actionInvoiceServisDelete( $id ) {
		return $this->delete( $id, 'SV' );
	}
	public function actionInvoiceServisCancel($id) {
		return $this->cancel( $id,'SV' );
	}
	public function actionInvoiceServisPrint( $id ) {
		return $this->print( $id, 'SV' );
	}
    public function actionInvoiceServisJurnal() {
        return $this->jurnal( 'SV' );
    }
	private function create( $Jenis ) {
		$model                    = new Ttsdhd();
		$model->SDNo              = General::createTemporaryNo( 'SD', 'ttsdhd', 'SDNo' );
		$model->SDTgl             = date( 'Y-m-d H:i:s' );
		$model->SDJamMasuk        = date( 'Y-m-d H:i:s' );
		$model->SDJamDikerjakan   = date( 'Y-m-d H:i:s' );
		$model->SDJamSelesai      = date( 'Y-m-d H:i:s' );
		$model->SDStatus          = "Menunggu";
		$model->MotorNoPolisi     = "UMUM";
		$model->KarKode           = "";
		$model->LokasiKode        = "";
		$model->SDPembawaMotor    = "Umum";
		$model->SDTotalWaktu      = 0;
		$model->SDTotalJasa       = 0;
		$model->SDTotalQty        = 0;
		$model->SDTotalPart       = 0;
		$model->SDTotalGratis     = 0;
		$model->SDTotalNonGratis  = 0;
		$model->SDDiscFinal       = 0;
		$model->SDTotalPajak      = 0;
		$model->SDTotalBiaya      = 0;
		$model->SDKasKeluar       = 0;
		$model->SDUangMuka        = 0;
		$model->SDDurasi          = 0;
		$model->SVNo              = "--";
		$model->SENo              = "--";
		$model->SVTgl             = date( 'Y-m-d H:i:s' );
		$model->UserID            = $this->UserID();
		$model->SDPembawaMotor    = "--";
		$model->SDHubunganPembawa = "Pemilik";
		$model->NoGLSD            = "--";
		$model->NoGLSV            = "--";
		$model->PKBNo             = "--";
		$model->PosKode           = $this->LokasiKode();
		$model->Cetak             = "Belum";
		$model->SDTOP             = 0;
		$model->SDTerbayar        = 0;
		$model->SDKmSekarang      = 0;
		$model->SDKmBerikut       = 0;
		$model->KlaimKPB          = "Belum";
		$model->KlaimC2           = "Belum";
		$model->ASS               = "";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttsdhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTServis                  = Ttsdhd::find()->ttsdhdFillByNo()
		                                    ->where( [ 'ttsdhd.SDNo' => $model->SDNo, 'ttsdhd.SVNo' => $model->SVNo ] )
		                                    ->asArray( true )->one();
		$id                         = $this->generateIdBase64FromModel( $model );
		$dsTServis[ 'SDNoView' ]    = $result[ 'SDNo' ];
		$dsTServis[ 'StatusBayar' ] = 'BELUM';
        $dsTServis['CusPeringkat']  =  '--';
		return $this->render( 'create', [
			'model'     => $model,
			'dsTServis' => $dsTServis,
			'id'        => $id,
			'Jenis'     => $Jenis
		] );
	}
	private function update( $id, $action, $Jenis ) {
		$index = Ttsdhd::getRoute( $Jenis, [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		/** @var Ttsdhd $model */
		/** @var Ttsdhd $dsTServis */
		$model     = $this->findModelBase64( 'Ttsdhd', $id );
		$dsTServis = Ttsdhd::find()->ttsdhdFillByNo()
		                   ->where( [ 'ttsdhd.SDNo' => $model->SDNo, 'ttsdhd.SVNo' => $model->SVNo ] )
		                   ->asArray( true )->one();

		$Terbayar = floatval( $dsTServis[ 'SDTerbayar' ] );
		if ( Yii::$app->request->isPost ) {
			$post = array_merge( $dsTServis, \Yii::$app->request->post() );
			if ( $Jenis != 'SD' ) { /* SD */
				$post[ 'SVNoBaru' ] = str_replace( 'SD', 'SV', $post[ 'SDNo' ] );
			}
			$post[ 'SDTgl' ]      = $post[ 'SDTgl' ] . ' ' . $post[ 'SDJam' ];
			$post[ 'SVTgl' ]      = $post[ 'SVTgl' ] . ' ' . $post[ 'SVJam' ];
			$post[ 'LokasiKode' ] = 'PosKode';
			$post[ 'Action' ]     = 'Update';
			$result               = Ttsdhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model = Ttsdhd::findOne( $result[ 'SDNo' ] );
            if ( $result[ 'SDNo' ] == '--' || $model == null) {
                if ( $model === null ) {
                    Yii::$app->session->addFlash( 'warning', $Jenis.'No : ' . $result[ 'SDNo' ]. ' tidak ditemukan.' );
                }
                return $this->redirect( Ttsdhd::getRoute( $Jenis, [ 'id' => $id ] )[ 'index' ]);
            }
			$id    = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 ) {
				$post[ 'SDNoView' ] = $result[ 'SDNo' ];
				return $this->redirect( Ttsdhd::getRoute( $Jenis, [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'update', [
					'model'     => $model,
					'dsTServis' => $post,
					'id'        => $id,
					'Jenis'     => $Jenis
				] );
			}
		}
		if ( ( floatval( $dsTServis[ 'SDTotalBiaya' ] ) - $Terbayar ) > 0 ) {
			$dsTServis[ 'StatusBayar' ] = 'BELUM';
		} else if ( ( floatval( $dsTServis[ 'SDTotalBiaya' ] ) - $Terbayar ) <= 0 ) {
			$dsTServis[ 'StatusBayar' ] = 'LUNAS';
		}
		$dsTServis[ 'SisaBayar' ] = floatval( $dsTServis[ 'SDTotalBiaya' ] ) - $Terbayar;
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTServis[ 'Action' ] = 'Load';
			$result                = Ttsdhd::find()->callSP( $dsTServis );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'SDNo' ] = $dsTServis[ 'SDNo' ];
		}
		$dsTServis[ 'SDNoView' ] = $_GET[ 'SDNoView' ] ?? $result[ 'SDNo' ];
		$validasi = false;
		if(!empty($dsTServis[ 'NoGLSD' ]) && !empty($dsTServis[ 'NoGLSV' ]) && $dsTServis[ 'NoGLSD' ] != '--' && $dsTServis[ 'NoGLSV' ] != '--'){
			$modelGLSD = Ttgeneralledgerhd::find()->where(['NOGL' => $dsTServis[ 'NoGLSD' ]])->one();
			$modelGLSV = Ttgeneralledgerhd::find()->where(['NOGL' => $dsTServis[ 'NoGLSV' ]])->one();
			if(!empty($modelGLSD) && !empty($modelGLSV) && $modelGLSD->GLValid == 'Sudah' && $modelGLSV->GLValid == 'Sudah' ){
				$validasi = true;
			}
		}
		$dsTServis['GLValid'] = $validasi;
		$dsTServis['CusPeringkat'] = $dsTServis['CusPeringkat'] ?? '--';
		return $this->render( 'update', [
			'model'     => $model,
			'dsTServis' => $dsTServis,
			'id'        => $id,
			'Jenis'     => $Jenis
		] );
	}
//	private function delete( $id, $Jenis ) {
//		/** @var Ttsdhd $model */
//		$model             = $model = $this->findModelBase64( 'Ttsdhd', $_POST[ 'id' ] );
//		$_POST[ 'SDNo' ]   = $model->SDNo;
//		$_POST[ 'SVNo' ]   = $model->SVNo;
//		$_POST[ 'Action' ] = 'Delete';
//		$result            = Ttsdhd::find()->callSP( $_POST );
//		if ( $result[ 'status' ] == 0 ) {
//			return $this->responseSuccess( $result[ 'keterangan' ] );
//		} else {
//			return $this->responseFailed( $result[ 'keterangan' ] );
//		}
//	}

//    public function actionDelete()
    public function delete($id, $Jenis)
    {
        /** @var Ttsdhd $model */
        $request = Yii::$app->request;
        if ($request->isAjax){
            $model = $this->findModelBase64('Ttsdhd', $_POST['id']);
            $_POST[ 'SDNo' ]   = $model->SDNo;
		    $_POST[ 'SVNo' ]   = $model->SVNo;
            $_POST['Action'] = 'Delete';
            $result = Ttsdhd::find()->callSP($_POST);
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            $id = $request->get('id');
            $model = $this->findModelBase64('Ttsdhd', $id);
            $_POST[ 'SDNo' ]   = $model->SDNo;
		    $_POST[ 'SVNo' ]   = $model->SVNo;
            $_POST['Action'] = 'Delete';
            $result = Ttsdhd::find()->callSP($_POST);
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['SDNo'] == '--') {
                return $this->redirect(['index']);
            } else {
                $model = Ttsdhd::findOne($result['SDNo']);
//                return $this->redirect(['ttsdhd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
                return $this->redirect( Ttsdhd::getRoute( $Jenis, [ 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] ) );
            }
        }
    }

	private function cancel( $id, $Jenis ) {
		/** @var  $model Ttsdhd */
		$post                = \Yii::$app->request->post();
		$post[ 'KodeTrans' ] = $Jenis;
		$post[ 'Action' ]    = 'Cancel';
		$result              = Ttsdhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        $model = Ttsdhd::findOne( $result[ 'SDNo' ] );
        if ( $result[ 'SDNo' ] == '--' || $model == null) {
            if ( $model === null ) {
                Yii::$app->session->addFlash( 'warning', $Jenis.'No : ' . base64_decode( $id ) . ' tidak ditemukan.' );
            }
            return $this->redirect( Ttsdhd::getRoute( $Jenis, [ 'id' => $id ] )[ 'index' ]);
		} else {
			return $this->redirect( Ttsdhd::getRoute( $Jenis, [ 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] )[ 'update' ] );
		}
	}
	private function print( $id, $jenis ) {
		unset( $_POST[ '_csrf-app' ] );
		$filename = $_POST['SDNo'].'.pdf';
		if($jenis == 'SV'){
			$filename = $_POST['SVNo'].'.pdf';
		}
		$_POST[ 'KodeTrans' ] = $jenis;
		$_POST                = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]        = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ]    = Menu::getUserLokasi()[ 'UserID' ];
		$client               = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response             = $client->post( Yii::$app->params['H2'][$_POST['db']]['host_report'] . '/abengkel/fsdhd', [
			'body' => Json::encode( $_POST )
		] );
		return Yii::$app->response->sendContentAsFile( $response->getBody(), $filename, [
			'inline'   => true,
			'mimeType' => 'application/pdf'
		]);
		// $html                 = $response->getBody();
		// return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	/* Picking List */
	public function actionPickingList( $id ) {
		$this->layout = 'print';
		return $this->render( 'pickingList', [
			'id'            => $id,
			'urlHeaderGrid' => Url::toRoute( [ 'ttsdhd/fill-header-p-l' ] ),
			'urlDetailGrid' => Url::toRoute( [ 'ttsdhd/fill-item-p-l' ] )
		] );
	}
	public function actionFillHeaderPL() {
		$rows = General::cCmd( "SELECT  ttSDhd.SDNo, SDTgl, SDPickingNo, SDPickingDate, ttSDitbarang.Cetak 
		FROM ttSDitbarang INNER JOIN ttSDhd ON ttSDhd.SDNo = ttSDitbarang.SDNo 
		WHERE ttSDhd.SDNo = :SDNo GROUP BY SDPickingNo, SDPickingDate;", [
			':SDNo' => base64_decode( \Yii::$app->request->post( 'id' ) )
		] )->queryAll();
		for ( $i = 0; $i < count( $rows ); $i ++ ) {
			$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'SDPickingNo' ] );
		}
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return [
			'rows'      => $rows,
			'rowsCount' => count( $rows )
		];
	}
	public function actionFillItemPL() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $requestData                 = \Yii::$app->request->post();
        if(isset( $requestData[ 'oper' ] ) ){
			$dataLama = Ttsditbarang::findOne([
				'SDNo' => $_POST['SDNo'], 
				'SDAuto'=> $_POST['SDAuto']
			]);
			if(empty($dataLama)){
				return $this->responseFailed( 'Gagal mengubah Lokasi Kode karena data tidak ditemukan.');
			}
            $SaldoQty = General::cCmd( "SELECT IFNULL(SaldoQty,0) FROM tdbaranglokasi 
			WHERE BrgKode = :BrgKode AND LokasiKode  = :LokasiKode",
			[':BrgKode' => $_POST['BrgKode'], ':LokasiKode'=> $_POST['LokasiKode']] )->queryScalar();
			$SaldoQty = $SaldoQty ?? 0;
			$SaldoQty = floatval($SaldoQty);
			if($SaldoQty <= 0){
				return $this->responseFailed( 'Gagal mengubah Lokasi Kode karena stok tidak mencukupi.');
			}else{
				$dataLama->LokasiKode = $_POST['LokasiKode'];
				$dataLama->save();
			}
			return $this->responseSuccess( 'Berhasil mengubah Lokasi Kode');
        }
        else{
          $rows = General::cCmd( "SELECT ttsditbarang.SDNo, ttsditbarang.SDAuto, ttsditbarang.BrgKode, ttsditbarang.SDQty, ttsditbarang.SDHrgBeli, ttsditbarang.SDHrgJual, ttsditbarang.SDJualDisc, ttsditbarang.SDPajak, tdbarang.BrgNama, tdbarang.BrgSatuan, 
                  IF((ttsditbarang.SDHrgJual * ttsditbarang.SDQty) > 0, ttsditbarang.SDJualDisc / (ttsditbarang.SDHrgJual * ttsditbarang.SDQty) * 100, 0) AS Disc, ttsditbarang.SDQty * ttsditbarang.SDHrgJual - ttsditbarang.SDJualDisc AS Jumlah, 
                  ttsditbarang.LokasiKode, ttsditbarang.SDPickingNo, ttsditbarang.SDPickingDate, tdbarang.BrgGroup, tdbarang.BrgRak1, SaldoQty
                  FROM ttsditbarang 
                  INNER JOIN tdbarang ON ttsditbarang.BrgKode = tdbarang.BrgKode
                  INNER JOIN tdbaranglokasi ON tdbaranglokasi.BrgKode = ttsditbarang.BrgKode AND tdbaranglokasi.lokasiKode = ttsditbarang.lokasiKode
                  WHERE (ttsditbarang.SDPickingNo = :SDPickingNo)
                  ORDER BY ttsditbarang.SDNo, ttsditbarang.SDAuto", [':SDPickingNo' => base64_decode( \Yii::$app->request->post( 'id' ) )] )->queryAll();
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return [
			'rows'      => $rows,
			'rowsCount' => count( $rows )
		];
	}
	public function actionPrintPickingList() {
		unset( $_POST[ '_csrf-app' ] );
		$_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client            = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response          = $client->post( Yii::$app->params['H2'][$_POST['db']]['host_report'] . '/abengkel/plsd', [
			'body' => Json::encode( $_POST )
		] );
		$html              = $response->getBody();
		return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	/* End of Picking List */
	public function actionCheckStock( $BrgKode, $LokasiKode ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return Tdbaranglokasi::find()
		                     ->where( 'BrgKode = :BrgKode AND LokasiKode = :LokasiKode', [ 'BrgKode' => $BrgKode, 'LokasiKode' => $LokasiKode ] )
		                     ->asArray()->one();
	}
	/* Panggil Antrian */
	public function actionPanggilAntrian() {
		return $this->render( 'PanggilAntrian' );
	}
	public function actionPanggilAntrianLoadData() {
		$antrian = ( new \yii\db\Query() )
			->select( 'SDNo, SDNoUrut, ttsdhd.MotorNoPolisi, SDPembawaMotor, SDStatus, SDTgl, CusKelurahan, CusKecamatan, MotorNama' )
			->from( 'ttsdhd' )
			->innerJoin( 'tdcustomer', 'tdcustomer.MotorNoPolisi = ttsdhd.MotorNoPolisi' )
			->where( "SVNo ='--'" )
			->andWhere( "DATE(SDTgl) = CURRENT_DATE" )
			->andWhere( "PosKode = :MyPOSLokasiKode", [ ':MyPOSLokasiKode' => $this->LokasiKode() ] )
			->all();
		$result  = [
			'SDNo'           => [],
			'SDNoUrut'       => [],
			'MotorNoPolisi'  => [],
			'SDPembawaMotor' => [],
			'CusKelurahan'   => [],
			'CusKecamatan'   => [],
			'MotorNama'      => []
		];
		foreach ( $antrian as $a ) {
			$a[ 'text' ]                  = $a[ 'SDNo' ];
			$a[ 'id' ]                    = $a[ 'SDNo' ];
			$result[ 'SDNo' ][]           = $a;
			$result[ 'SDNoUrut' ][]       = [ 'id' => $a[ 'id' ], 'text' => $a[ 'SDNoUrut' ] ];
			$result[ 'MotorNoPolisi' ][]  = [ 'id' => $a[ 'id' ], 'text' => $a[ 'MotorNoPolisi' ] ];
			$result[ 'SDPembawaMotor' ][] = [ 'id' => $a[ 'id' ], 'text' => $a[ 'SDPembawaMotor' ] ];
			$result[ 'CusKelurahan' ][]   = [ 'id' => $a[ 'id' ], 'text' => $a[ 'CusKelurahan' ] ];
			$result[ 'CusKecamatan' ][]   = [ 'id' => $a[ 'id' ], 'text' => $a[ 'CusKecamatan' ] ];
			$result[ 'MotorNama' ][]      = [ 'id' => $a[ 'id' ], 'text' => $a[ 'MotorNama' ] ];
		}
		return $this->responseJson( true, '', $result );
	}
	public function actionPanggilAntrianMulaiPanggil() {
		$post = \Yii::$app->request->post();
		$urlName = urlencode($post['urlNama']);
		$urlKelurahan = urlencode($post['urlKelurahan']);
		$urlKecamatan = urlencode($post['urlKecamatan']);
		$urlTujuan = urlencode($post['urlTujuan']);
		$urlGoogle = "https://translate.googleapis.com/translate_tts?ie=UTF-8&q=Panggilan+atas+nama+$urlName+$urlKelurahan+$urlKecamatan+silahkan+menuju+ke+$urlTujuan&tl=id&client=gtx";
		Yii::$app->db->createCommand( "UPDATE ttqshd SET QSPanggil = '" . $post[ 'SDNo' ] . "', QSTujuan = '" . $post[ 'Tujuan' ] . "',  QSPanggilSudah = 'Belum', QSPanggilApa = '" . $post[ 'PanggilApa' ] . "', QsPanggilNama = '" . $urlGoogle . "'; " )->execute();
		return $this->responseSuccess( 'Mulai Panggil' );
	}
	public function actionPanggilAntrianPanggilSudah() {
		Yii::$app->db->createCommand( "UPDATE ttqshd SET QSPanggilSudah = 'Sudah';" )->execute();
		return $this->responseSuccess( 'Panggil Sudah' );
	}
	public function actionPanggilAntrianDatang() {
		$post = \Yii::$app->request->post();
		if ( $post[ 'Tujuan' ] == 'Kasir-Bengkel' ) {
			$field = 'QSKasir';
		} else if ( $post[ 'Tujuan' ] == 'Mekanik S A' ) {
			$field = 'QSMekanik';
		} else {
			$this->responseFailed( 'Pilih Tujuan.' );
		}
		Yii::$app->db->createCommand( "UPDATE ttqshd SET " . $field . " = '" . $post[ 'SDNo' ] . "';" )->execute();
		$kasir   = ( new \yii\db\Query() )
			->select( 'SDNo, SDNoUrut, MotorNoPolisi, SDPembawaMotor, SDStatus, SDTgl' )
			->from( 'ttsdhd' )
			->innerJoin( 'ttqshd', 'ttqshd.QSKasir = ttsdhd.SDNo' )
			->one();
		$mekanik = ( new \yii\db\Query() )
			->select( 'SDNo, SDNoUrut, MotorNoPolisi, SDPembawaMotor, SDStatus, SDTgl' )
			->from( 'ttsdhd' )
			->innerJoin( 'ttqshd', 'ttqshd.QSMekanik = ttsdhd.SDNo' )
			->one();
		return $this->responseSuccess( 'Datang', [
			'kasir'   => $kasir,
			'mekanik' => $mekanik
		] );
	}
	public function actionPanggilAntrianSelesaiKasir() {
		Yii::$app->db->createCommand( "UPDATE ttqshd SET QSKasir = '--';" )->execute();
		return $this->responseSuccess( 'Kasir Selesai' );
	}
	public function actionPanggilAntrianSelesaiMekanik() {
		Yii::$app->db->createCommand( "UPDATE ttqshd SET QSMekanik = '--'" )->execute();
		return $this->responseSuccess( 'Mekanik Selesai' );
	}
	/* End of Panggil Antrian */
	public function actionStartStopMekanik() {
		return $this->render( 'StartStopMekanik' );
	}
	public function actionStartStopMekanikList() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$data                        = [];
		if ( isset( $_POST[ 'SDNo' ] ) ) {
			$data = General::cCmd( "SELECT SDNo, ttsdittime.KarKode, KarNama, Nomor, 
		       DATE_FORMAT(START, '%d/%m/%y %H:%i:%s') AS START, 
		      IF(Finish='2100-12-31 23:59:59','--',DATE_FORMAT(Finish, '%d/%m/%y %H:%i:%s')) AS FINISH, 
		      IF(STATUS='','Diproses',STATUS) AS STATUS FROM ttsdittime
		      INNER JOIN tdkaryawan ON tdkaryawan.KarKode = ttsdittime.KarKode
		      WHERE SDNo LIKE :SDNo 
		      ORDER BY Nomor", [ ':SDNo' => $_POST[ 'SDNo' ] ] )->queryAll();
		}
		return [
			'rows'      => $data,
			'rowsCount' => sizeof( $data )
		];
	}
	public function actionCekKarKodeDiproses() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$data                        = '--';
		if ( isset( $_POST[ 'KarKode' ] ) ) {
			$data = General::cCmd( "SELECT IFNULL(ttsdittime.SDNo,'--')  FROM ttsdittime 
		         INNER JOIN ttsdhd ON ttsdhd.SDNo = ttsdittime.SDNo
		         WHERE ttsdittime.KarKode = :KarKode AND (STATUS = '' OR STATUS = 'Diproses') AND 
               		SVNo = '--' AND SDStatus <> 'Selesai'", [ ':KarKode' => $_POST[ 'KarKode' ] ] )->queryScalar();
			if ( $data === false ) {
				$data = '';
			}
		}
		return [
			'rows'      => $data,
			'rowsCount' => 1
		];
	}
	public function actionStartStopMekanikListUpdate() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$data                        = [];
		switch ( $_POST[ 'oper' ] ) {
			case 'start':
				$Nomor = General::cCmd( "SELECT IFNULL(MAX(NOMOR),0) AS NOMOR FROM ttsdittime  WHERE SDNo = :SDNo",
					[ ':SDNo' => $_POST[ 'SDNo' ] ] )->queryScalar();
				if ( $Nomor === false ) {
					$Nomor = 0;
				}
				$Nomor ++;
				$ttsdittime          = new Ttsdittime;
				$ttsdittime->SDNo    = $_POST[ 'SDNo' ];
				$ttsdittime->KarKode = $_POST[ 'KarKode' ];
				$ttsdittime->Nomor   = $Nomor;
				$ttsdittime->Start   = new Expression( 'NOW()' );
				$ttsdittime->Finish  = '2100-12-31 23:59:59';
				$ttsdittime->Status  = '';
				$ttsdittime->save();
				break;
			case 'stop':
				Ttsdittime::updateAll( [ 'Status' => 'Istirahat', 'Finish' => new Expression( 'NOW()' ) ], [ 'SDNo' => $_POST[ 'SDNo' ], 'Status' => '' ] );
				break;
			case 'finish':
				Ttsdittime::updateAll( [ 'Status' => 'Selesai', 'Finish' => new Expression( 'NOW()' ) ], [ 'SDNo' => $_POST[ 'SDNo' ], 'Status' => '' ] );
				break;
		}
		return [
			'rows'      => $data,
			'rowsCount' => sizeof( $data )
		];
	}
	public function actionStartStopMekanikDurasi() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$SDNo                        = $_POST[ 'SDNo' ];
		if ( $SDNo == '' || $_POST[ 'SDNo' ] == '--' ) {
			$data = 0;
		} else {
			$data = General::cCmd( "SELECT IFNULL(SUM(TIMESTAMPDIFF(MINUTE, START, Finish)),0) 
			FROM ttsdittime WHERE SDNo = :SDNo AND Status <> '' ", [ ':SDNo' => $_POST[ 'SDNo' ] ] )->queryScalar();
		}
		return [
			'rows' => $data,
		];
	}
	/* List Antrian */
	public function actionListAntrian() {
		$dataCompany = \abengkel\models\Tzcompany::find()->one();
		return $this->render( 'ListAntrian', [
			'dataCompany' => $dataCompany
		] );
	}
	public function actionListAntrianLoadHeader() {
		$empty   = [ "QSPanggil" => "--", "QSTujuan" => "--", "SDPembawaMotor" => "--", "MotorNoPolisi" => "--", "SDNoUrut" => "--" ];
		$Pgl     = ( new \yii\db\Query() )
			->select( 'QSPanggil, QSTujuan, SDPembawaMotor, MotorNoPolisi, SDNoUrut' )
			->from( 'ttqshd' )
			->innerJoin( 'ttsdhd', 'ttsdhd.SDNo = QSPanggil' )
			->one();
		$Kasir   = ( new \yii\db\Query() )
			->select( 'QSPanggil, QSTujuan, SDPembawaMotor, MotorNoPolisi, SDNoUrut' )
			->from( 'ttqshd' )
			->innerJoin( 'ttsdhd', 'ttsdhd.SDNo = QSKasir' )
			->one();
		$Mekanik = ( new \yii\db\Query() )
			->select( 'QSPanggil, QSTujuan, SDPembawaMotor, MotorNoPolisi, SDNoUrut' )
			->from( 'ttqshd' )
			->innerJoin( 'ttsdhd', 'ttsdhd.SDNo = QSMekanik' )
			->one();
		return $this->responseJson( true, '', [
			'Pgl'     => $Pgl ? $Pgl : $empty,
			'Kasir'   => $Kasir ? $Kasir : $empty,
			'Mekanik' => $Mekanik ? $Mekanik : $empty,
		] );
	}
	public function actionListAntrianLoadAntrian() {
		return $this->responseDataJson( Ttsdhd::find()->getListAntrian( $this->LokasiKode() ) );
	}
	/* End of List Antrian */
	/**
	 * Finds the Ttsdhd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $SDNo
	 * @param string $SVNo
	 *
	 * @return Ttsdhd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $SDNo, $SVNo ) {
		if ( ( $model = Ttsdhd::findOne( [ 'SDNo' => $SDNo, 'SVNo' => $SVNo ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	public function actionHistoryOtomatis() {
		$this->layout = 'popup-form';
		$dt = General::cCmd( "SELECT MotorNoPolisi, CusNama, CusAlamat, CusTelepon, MotorType, MotorNoRangka, MotorNoMesin, MotorNama,
		fCusPeringkat(MotorNoPolisi) AS StatusPeringkat, fCusJumServis(MotorNoPolisi) AS JumlahServis,
		fCusAvgJasa(MotorNoPolisi) AS RataRataJasa, fCusMaxJasa(MotorNoPolisi) AS MaxJasa,
		fCusSiklus(MotorNoPolisi) AS Siklus, fCusProgram(MotorNoPolisi) AS PrgNama
		FROM tdcustomer WHERE MotorNoPolisi = :MotorNoPolisi", [ ':MotorNoPolisi' => $_GET['MotorNoPolisi'] ] )->queryOne();
		return $this->render( '_formHistory', [ 'data' => $dt ] );
	}
	public function actionHistoryOtomatisItem( $MotorNoPolisi = null, $SDNoService = null, $SDNoPart = null) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		if(!empty($MotorNoPolisi)){
			$dt                          = General::cCmd( "SELECT SDTgl, SDTotalJasa, SDTotalPart, ASS, PrgNama, SDNo
			FROM ttsdhd
			Where MotorNoPolisi = :MotorNoPolisi
			ORDER BY SDNo DESC", [ ':MotorNoPolisi' => $MotorNoPolisi ] )->queryAll();
			return [
				'rows' => $dt
			];
		}elseif(!empty($SDNoService)){
			$dt                          = General::cCmd( "SELECT ttsditjasa.JasaKode, tdjasa.JasaNama,
			ttsditjasa.SDHrgJual
			FROM ttsditjasa
			INNER JOIN tdjasa ON ttsditjasa.JasaKode = tdjasa.JasaKode
			WHERE SDNo = :SDNo", [ ':SDNo' => $SDNoService ] )->queryAll();
			return [
				'rows' => $dt
			];
		}elseif(!empty($SDNoPart)){
			$dt                          = General::cCmd( "SELECT ttsditbarang.BrgKode,  tdbarang.BrgNama,
			ttsditbarang.SDHrgJual
			FROM ttsditbarang
			INNER JOIN tdbarang ON ttsditbarang.BrgKode = tdbarang.BrgKode 
			WHERE SDNo = :SDNo", [ ':SDNo' => $SDNoPart ] )->queryAll();
			return [
				'rows' => $dt
			];
		}
	}
	public function actionMekanikOtomatis( $SDNo ) {
		$this->layout = 'popup-form';
		$karKode                          = General::cCmd( "SELECT KarKode, KarJabatan FROM tdkaryawan WHERE (KarJabatan = 'Mekanik' OR KarJabatan = 'Servis Advisor') AND KarStatus <> 'N' ORDER BY KarKode")->queryAll();
		return $this->render( '_formMekanik', [ 'SDNo' => $SDNo,'KarKode'=>$karKode ] );
	}
	public function actionMekanikOtomatisItem( $SDNo ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$request = Yii::$app->request;
		if ($request->isPost) {
			$requestData                 = $request->post();
			if(!empty($requestData[ 'oper' ]) && $requestData[ 'oper' ] == 'edit') {
                if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
                    $model = new Ttsdittime();
                    $model->SDNo = $SDNo;
                    $model->KarKode = $_REQUEST['KarKode'];
                    $model->Nomor = $_REQUEST['Nomor'];
                    $model->Start = $_REQUEST['Tgl'];
                    $model->Finish = $_REQUEST['Tgl'];
                    $model->Status = $_REQUEST['Status'];
                    if ( $model->save() ) {
                        $response = $this->responseSuccess( 'Berhasil ditambah' );
                    } else {
                        $response = $this->responseFailed( 'Gagal ditambah' );
                    }
                    return $response;
                } else {
                    $jml = Ttsdittime::updateAll( [ 'Status' => $requestData[ 'Status' ], 'KarKode' => $requestData[ 'KarKode' ] ], [ 'SDNo' => $requestData[ 'SDNo' ], 'Nomor' => $requestData[ 'Nomor' ] ] );
                    if ( $jml > 0 ) {
                        Ttsdhd::updateMekanik($requestData[ 'SDNo' ]);
                        $response = $this->responseSuccess( 'Berhasil diupdate' );
                    } else {
                        $response = $this->responseFailed( 'Tidak ada data yang diupdate.' );
                    }
                    return $response;
                }
			}elseif(!empty($requestData[ 'oper' ]) && $requestData[ 'oper' ] == 'del') {
				$model = $this->findModelBase64( 'Ttsdittime', $requestData['id'] );
				$jml = $model->delete();
				if ( $jml > 0 ) {
					Ttsdhd::updateMekanik($requestData[ 'SDNo' ]?? $SDNo);
                    $response = $this->responseSuccess( 'Berhasil didelete' );
                } else {
                    $response = $this->responseFailed( 'Tidak ada data yang diupdate.' );
                }
                return $response;
			}else{
				$primaryKeys                 = Ttsdittime::getTableSchema()->primaryKey;
				for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
	                $primaryKeys[ $i ] = 'Ttsdittime.' . $primaryKeys[ $i ];
	            }
				$query = ( new \yii\db\Query() )
                        ->select( new Expression(
                            'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
					SDNo, KarKode, Nomor, DATE_FORMAT(`Start`,"%d/%m/%Y %T") AS `Start`,
					DATE_FORMAT(`Finish`,"%d/%m/%Y %T") AS  `Finish`, `Status`'))
					->from( 'Ttsdittime' )
					->where( [ 'Ttsdittime.SDNo' => $SDNo ] )
                    ->orderBy('	Ttsdittime.Nomor');
				$rowsCount = $query->count();
	            $rows      = $query->all();
	            /* encode row id */
	            for ( $i = 0; $i < count( $rows ); $i ++ ) {
	                $rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
	            }
	            $response = [
	                'rows'      => $rows,
	                'rowsCount' => $rowsCount
	            ];
				return $response;
			}
		}
	}
	public function actionStatusOtomatis() {
		$this->layout = 'popup-form';
		return $this->render( '_formStatus', [ 'data' => $_GET ] );
	}
	public function actionStatusOtomatisItem( $SDNo, $SVNo ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$dt                          = General::cCmd( "SELECT KBMNo, KBMTgl, KBMLink, KBMBayar, Kode, Person, Memo, NoGL FROM vtKBM WHERE (KBMLink = :SVNo ) 
		UNION 
		SELECT KBKNo AS KBMNo, KBKTgl AS KBMTgl, KBKLink AS KBMLink, KBKBayar AS KBMBayar, Kode, Person, Memo, NoGL FROM vtKBK WHERE KBKLink = :SDNo
		ORDER BY KBMTgl;", [ ':SVNo' => $SVNo, ':SDNo' => $SDNo ] )->queryAll();
		return [
			'rows' => $dt
		];
	}
	public function actionSelectInvoiceServis() {
		$this->layout = 'select-mode';
		return $this->render( 'InvoiceServisSelect', [
			'mode'    => self::MODE_SELECT_MULTISELECT,
			'colGrid' => Ttsdhd::colGridInvoiceServisSelect()
		] );
	}
	public function actionKonfirm() {
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Konfirm';
		$result           = Ttsdhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( [ 'ttsdhd/invoice-servis-update', 'action' => 'update', 'id' => base64_encode( $result[ 'SDNo' ] ) ] );
	}
    private function jurnal($Jenis) {
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Jurnal';
		$result = Ttsdhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( Ttsdhd::getRoute( $Jenis, [ 'id' => base64_encode( $result[ 'SDNo' ] ), 'action' => 'display' ] )[ 'update' ] );
	}
	public function actionGetJasaBarang() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$jasa                        = [];
		$barang                      = [];
		switch ( $requestData[ 'SDTC' ] ) {
			case 'TC14' :
				$jasa = General::cCmd(
					"SELECT JasaKode, JasaNama, JasaGroup, JasaHrgBeli, JasaHrgJual, JasaWaktuKerja, JasaWaktuSatuan, JasaWaktuKerjaMenit 
						FROM tdjasa where JasaStatus = 'A' AND LEFT(JasaGroup,3) = 'ASS' Order By JasaStatus, JasaKode" )->queryAll();
				$barang = General::cCmd(
					"SELECT tdbarang.BrgKode, tdbarang.BrgNama, tdbarang.BrgBarCode, tdbarang.BrgGroup, tdbarang.BrgSatuan, tdbarang.BrgHrgBeli, tdbarang.BrgHrgJual, tdbarang.BrgRak1, tdbarang.BrgMinStock, tdbarang.BrgMaxStock, tdbarang.BrgStatus, ttsditbarang.SDQty
						FROM tdbarang
						INNER JOIN tdbaranglokasi ON tdbaranglokasi.BrgKode = tdbarang.BrgKode
						LEFT JOIN ttsditbarang ON ttsditbarang.BrgKode = tdbarang.BrgKode AND ttsditbarang.SDNo = :SDNo
						WHERE tdbaranglokasi.LokasiKode NOT IN ('Retur', 'Klaim', 'Kosong', 'Retur', 'Robbing', 'Rusak', 'Penerimaan1', 'Penerimaan2')
						AND  tdbarang.BrgGroup = 'Oli'
						GROUP BY tdbarang.BrgKode
						HAVING (SUM(SaldoQty)+COALESCE (ttsditbarang.SDQty,0)) > 0
						ORDER BY BrgStatus, BrgKode",[':SDNo'=>$requestData['SDNo']] )->queryAll();
				break;
			case 'TC11' :
				$jasa   = General::cCmd(
					"SELECT JasaKode, JasaNama, JasaGroup, JasaHrgBeli, JasaHrgJual, JasaWaktuKerja, JasaWaktuSatuan, JasaWaktuKerjaMenit 
				FROM tdjasa where JasaStatus = 'A' AND LEFT(JasaGroup,3) <> 'ASS' AND JasaGroup <> 'CLAIMC2' 
				Order By JasaStatus, JasaKode" )->queryAll();
				$barang = General::cCmd(
					"SELECT tdbarang.BrgKode, tdbarang.BrgNama, tdbarang.BrgBarCode, tdbarang.BrgGroup, tdbarang.BrgSatuan, tdbarang.BrgHrgBeli, tdbarang.BrgHrgJual, tdbarang.BrgRak1, tdbarang.BrgMinStock, tdbarang.BrgMaxStock, tdbarang.BrgStatus, ttsditbarang.SDQty
						FROM tdbarang
						INNER JOIN tdbaranglokasi ON tdbaranglokasi.BrgKode = tdbarang.BrgKode
						LEFT JOIN ttsditbarang ON ttsditbarang.BrgKode = tdbarang.BrgKode AND ttsditbarang.SDNo = :SDNo
						WHERE tdbaranglokasi.LokasiKode NOT IN ('Retur', 'Klaim', 'Kosong', 'Retur', 'Robbing', 'Rusak', 'Penerimaan1', 'Penerimaan2')
						GROUP BY tdbarang.BrgKode
						HAVING (SUM(SaldoQty)+COALESCE (ttsditbarang.SDQty,0)) > 0
						ORDER BY BrgStatus, BrgKode",[':SDNo'=>$requestData['SDNo']] )->queryAll();
				break;
			case 'TC13' :
				$jasa   = General::cCmd(
					"SELECT JasaKode, JasaNama, JasaGroup, JasaHrgBeli, JasaHrgJual, JasaWaktuKerja, JasaWaktuSatuan, JasaWaktuKerjaMenit 
						FROM tdjasa where JasaStatus = 'A' AND JasaGroup = 'CLAIMC2' Order By JasaStatus, JasaKode" )->queryAll();
				$barang = General::cCmd(
					"SELECT tdbarang.BrgKode, tdbarang.BrgNama, tdbarang.BrgBarCode, tdbarang.BrgGroup, tdbarang.BrgSatuan, tdbarang.BrgHrgBeli, tdbarang.BrgHrgJual, tdbarang.BrgRak1, tdbarang.BrgMinStock, tdbarang.BrgMaxStock, tdbarang.BrgStatus, ttsditbarang.SDQty
						FROM tdbarang
						INNER JOIN tdbaranglokasi ON tdbaranglokasi.BrgKode = tdbarang.BrgKode
						LEFT JOIN ttsditbarang ON ttsditbarang.BrgKode = tdbarang.BrgKode AND ttsditbarang.SDNo = :SDNo
						WHERE tdbaranglokasi.LokasiKode NOT IN ('Retur', 'Klaim', 'Kosong', 'Retur', 'Robbing', 'Rusak', 'Penerimaan1', 'Penerimaan2')
						GROUP BY tdbarang.BrgKode
						HAVING (SUM(SaldoQty)+COALESCE (ttsditbarang.SDQty,0)) > 0
						ORDER BY BrgStatus, BrgKode",[':SDNo'=>$requestData['SDNo']] )->queryAll();
				break;
		}
		return [
			'jasa'   => $jasa,
			'barang' => $barang
		];
	}

    public function actionGrid() {
        $urlDetail    = '';
        $this->layout = 'select-mode';
        return $this->render('../_formGrid', [
            'arr'       => [
                'cmbTxt'    => [
                    'SDNo'          => 'No SD',
                ],
                'cmbNum'    => [
                ],
                'cmbTgl'    => [
                    'SDTgl' => 'Tanggal SD',
                ],
                'sortname'  => "SDTgl",
                'sortorder' => 'desc'
            ],
            'options'   => [
                'colGrid' => Ttsdhd::colGridSD(),
                'mode'    => self::MODE_SELECT_DETAIL_MULTISELECT
            ],
            'title'     => 'Daftar Service',
            'urlDetail' => $urlDetail
        ] );
    }

	public function actionImportLoop(){
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$respon = [];
		$base64 = urldecode(base64_decode($requestData['header']));
		parse_str($base64, $header);
		$header['Action'] = 'Update';
		$header['ZnoWorkOrder'] = $requestData['data']['data']['noWorkOrder'] ;
		$header['MotorNoPolisi'] = $requestData['data']['data']['noPolisi'] ;
		$result = Ttsdhd::find()->callSP( $header );
		$requestData['data']['data']['Action'] = 'ImporBefore';
		$requestData['data']['data']['NoTrans'] = $result['SDNo'];
		$result = Api::callPKB($requestData['data']['data']);
		Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		$model = Ttsdhd::findOne($result['NoTrans']);
		// Yii::error(print_r($model,true));
		$id = $this->generateIdBase64FromModel($model);
		$respon['id'] = $id;
		if($result['status'] == 1){
			return $respon;
		}
		foreach ($requestData['dtGrid1'] ?? [] as $item) {
			$item['Action'] = 'Impor';
			$item['NoTrans'] = $model->SDNo;
			$result = Api::callPKBitjasa($item);
			Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		}
		foreach ($requestData['dtGrid2'] ?? [] as $dt) {
			$dt['Action'] = 'Impor';
			$dt['NoTrans'] = $model->SDNo;
			$result = Api::callPKBitbarang($dt);
			Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		}
		$requestData['data']['data']['Action'] = 'ImporAfter';
		$result = Api::callPKB($requestData['data']['data']);
		Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		return $respon;
	}


}
