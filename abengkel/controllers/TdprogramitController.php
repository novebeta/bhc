<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\models\Tdprogramit;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TdprogramitController implements the CRUD actions for Tdprogramit model.
 */
class TdprogramitController extends AbengkelController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Tdprogramit::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
            /** @var Tdprogramit $model */
            $model = null;
            if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
                $model = $this->findModelBase64( 'tdprogramit', $requestData[ 'id' ] );
            }
            switch ( $oper ) {
                case 'add'  :
                case 'edit' :
                    if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
                        $requestData[ 'Action' ] = 'Insert';
                        $requestData[ 'PrgAuto' ] = 0;
                    } else {
                        $requestData[ 'Action' ] = 'Update';
                        if ( $model != null ) {
                            $requestData[ 'PrgNamaOLD' ] = $model->PrgNama;
                            $requestData[ 'PrgAutoOLD' ] = $model->PrgAuto;
                            $requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
                        }
                    }
                    $result = Tdprogramit::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
                    break;
                case 'batch' :
                    break;
                case 'del'  :
                    $requestData             = array_merge( $requestData, $model->attributes );
                    $requestData[ 'Action' ] = 'Delete';
                    if ( $model != null ) {
                        $requestData[ 'PrgNamaOLD' ] = $model->PrgNama;
                        $requestData[ 'PrgAutoOLD' ] = $model->PrgAuto;
                        $requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
                    }
                    $result = Tdprogramit::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
                    break;
            }

		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'tdprogramit.' . $primaryKeys[ $i ];
			}
			$query     = ( new \yii\db\Query() )
				->select( new Expression(
					'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
								tdprogramit.PrgNama, tdprogramit.PrgAuto, tdprogramit.BrgKode, tdprogramit.PrgDiscount, tdbarang.BrgNama, tdbarang.BrgSatuan, tdprogramit.PrgHrgJual, 
                         tdprogramit.PrgDiscount / tdprogramit.PrgHrgJual * 100 AS Disc'
				) )
				->from( 'tdprogramit' )
				->join( 'INNER JOIN', 'tdbarang', 'tdprogramit.BrgKode = tdbarang.BrgKode' )
				->where( "(tdprogramit.PrgNama = :PrgNama)",[':PrgNama' => $requestData['PrgNama']]);
			$query1     = ( new \yii\db\Query() )
				->select( new Expression(
					'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
								tdprogramit.PrgNama, tdprogramit.PrgAuto, tdprogramit.BrgKode, tdprogramit.PrgDiscount, tdjasa.JasaNama, tdjasa.JasaWaktuSatuan, 
                         tdprogramit.PrgHrgJual, tdprogramit.PrgDiscount / tdprogramit.PrgHrgJual * 100 AS Disc'
				) )
				->from( 'tdprogramit' )
				->join( 'INNER JOIN', 'tdjasa', 'tdprogramit.BrgKode = tdjasa.JasaKode' )
				->where( "(tdprogramit.PrgNama = :PrgNama)",[':PrgNama' => $requestData['PrgNama']]);
			$query->union($query1);
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	protected function findModel( $PrgNama, $PrgAuto ) {
		if ( ( $model = Tdprogramit::findOne( [ 'PrgNama' => $PrgNama, 'PrgAuto' => $PrgAuto ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
