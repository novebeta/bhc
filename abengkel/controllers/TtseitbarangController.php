<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\models\Ttseitbarang;
use abengkel\models\Ttsehd;
use common\components\General;
use Yii;
use yii\db\Exception;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtseitbarangController implements the CRUD actions for Ttseitbarang model.
 */
class TtseitbarangController extends AbengkelController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttseitbarang models.
	 * @return mixed
	 */
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttseitbarang::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
			/** @var Ttseitbarang $model */
			$model = null;
			if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
				$model = $this->findModelBase64( 'ttseitbarang', $requestData[ 'id' ] );
			}
			// Custom::genSP($requestData, 'fseitbarang');
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'Action' ] = 'Insert';
						$requestData[ 'SEAuto' ] = 0;
					} else {
						$requestData[ 'Action' ] = 'Update';
						if ( $model != null ) {
							$requestData[ 'SENoOLD' ]    = $model->SENo;
							$requestData[ 'SEAutoOLD' ]  = $model->SEAuto;
							$requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
						}
					}
					$result = Ttseitbarang::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'batch' :
				case 'del'  :
					$requestData             = array_merge( $requestData, $model->attributes );
					$requestData[ 'Action' ] = 'Delete';
					if ( $model != null ) {
						$requestData[ 'SENoOLD' ]    = $model->SENo;
						$requestData[ 'SEAutoOLD' ]  = $model->SEAuto;
						$requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
					}
					$result = Ttseitbarang::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
			}
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'Ttseitbarang.' . $primaryKeys[ $i ];
			}
			$jenis = $_GET[ 'jenis' ] ?? ' ';
			switch ( $jenis ) {
				case 'FillItemSESD':
					$comm      = General::cCmd( 'SELECT 
                            CONCAT(SENo,"||", SEAuto,"||", JenisMat) AS id,SENo, SEAuto, BrgKode, SEQty, SEWaktuKerjaMenit, BrgSatuan, SEHrgJual,SEHrgBeli, SEJualDisc, JasaNama, Disc, Jumlah, JenisMat 
                            FROM
                              (SELECT 
                                ttseitjasa.SENo, ttseitjasa.SEAuto, ttseitjasa.JasaKode AS BrgKode, ttseitjasa.SEWaktuKerja AS SEQty, JasaWaktuKerjaMenit AS SEWaktuKerjaMenit, ttseitjasa.SEWaktuSatuan AS BrgSatuan, ttseitjasa.SEHrgJual, ttseitjasa.SEHargaBeli AS SEHrgBeli, ttseitjasa.SEJualDisc, tdjasa.JasaNama, ttseitjasa.SEJualDisc / ttseitjasa.SEHrgJual * 100 AS Disc, ttseitjasa.SEHrgJual - ttseitjasa.SEJualDisc AS Jumlah, "Jasa" AS JenisMat 
                              FROM
                                ttseitjasa 
                                INNER JOIN tdjasa 
                                  ON ttseitjasa.JasaKode = tdjasa.JasaKode 
                              UNION
                              SELECT 
                                ttseitbarang.SENo, ttseitbarang.SEAuto, ttseitbarang.BrgKode, ttseitbarang.SEQty,0 AS SEWaktuKerjaMenit, tdbarang.BrgSatuan, ttseitbarang.SEHrgJual, ttseitbarang.SEHrgBeli, ttseitbarang.SEJualDisc, tdbarang.BrgNama AS JasaNama, ttseitbarang.SEJualDisc / ttseitbarang.SEHrgJual * 100 AS Disc, ttseitbarang.SEQty * ttseitbarang.SEHrgJual - ttseitbarang.SEJualDisc AS Jumlah, "Barang" AS JenisMat 
                              FROM
                                ttseitbarang 
                                INNER JOIN tdbarang 
                                  ON ttseitbarang.BrgKode = tdbarang.BrgKode) ttSEit WHERE ttSEit.SENo  = :SENo ', [
						':SENo' => $requestData[ 'SENo' ] ] );
					$rowsArray = $comm->queryAll();
					for ( $i = 0; $i < count( $rowsArray ); $i ++ ) {
						$rowsArray[ $i ][ 'id' ] = base64_encode( $rowsArray[ $i ][ 'id' ] );
					}
					$response = [
						'rows'      => $rowsArray,
						'rowsCount' => sizeof( $rowsArray )
					];
					return $response;
					break;
				default:
					$query     = ( new \yii\db\Query() )
						->select( new Expression(
							'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
                                            ttseitbarang.SENo,
                                            ttseitbarang.SEAuto,
                                            ttseitbarang.BrgKode,
                                            ttseitbarang.SEQty,
                                            ttseitbarang.SEHrgBeli,
                                            ttseitbarang.SEHrgJual,
                                            ttseitbarang.SEJualDisc,
                                            tdbarang.BrgNama,
                                            tdbarang.BrgSatuan,
                                            IFNULL(ttseitbarang.SEJualDisc / ttseitbarang.SEHrgJual * 100, 0) AS Disc,
                                            ttseitbarang.SEQty * ttseitbarang.SEHrgJual - ttseitbarang.SEJualDisc AS Jumlah,
                                            tdbarang.BrgGroup,
                                            ttseitbarang.LokasiKode'
						) )
						->from( 'ttseitbarang' )
						->join( 'INNER JOIN', 'tdbarang', 'ttseitbarang.BrgKode = tdbarang.BrgKode' )
						->where( [ 'ttseitbarang.SENo' => $requestData[ 'SENo' ] ] )
						->orderBy( '	ttseitbarang.SENo,ttseitbarang.SEAuto' );
					$rows      = $query->all();
					$rowsCount = $query->count();
			}
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	/**
	 * Displays a single Ttseitbarang model.
	 *
	 * @param string $SENo
	 * @param integer $SEAuto
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $SENo, $SEAuto ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $SENo, $SEAuto ),
		] );
	}
	/**
	 * Creates a new Ttseitbarang model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Ttseitbarang();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'SENo' => $model->SENo, 'SEAuto' => $model->SEAuto ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Ttseitbarang model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $SENo
	 * @param integer $SEAuto
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $SENo, $SEAuto ) {
		$model = $this->findModel( $SENo, $SEAuto );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'SENo' => $model->SENo, 'SEAuto' => $model->SEAuto ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Ttseitbarang model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $SENo
	 * @param integer $SEAuto
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $SENo, $SEAuto ) {
		$this->findModel( $SENo, $SEAuto )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Ttseitbarang model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $SENo
	 * @param integer $SEAuto
	 *
	 * @return Ttseitbarang the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $SENo, $SEAuto ) {
		if ( ( $model = Ttseitbarang::findOne( [ 'SENo' => $SENo, 'SEAuto' => $SEAuto ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
