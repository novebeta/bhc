<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\components\TUi;
use abengkel\models\Tdbaranglokasi;
use abengkel\models\Tddealer;
use abengkel\models\Tdlokasi;
use abengkel\models\Tdsupplier;
use abengkel\models\Ttpshd;
use abengkel\models\Ttpsit;
use common\components\Api;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtpshdController implements the CRUD actions for Ttpshd model.
 */
class TtpshdController extends AbengkelController {
	public function actions() {
		$colGrid    = null;
		$joinWith   = [];
		$join       = [];
        $where      = [
            [
                'op'        => 'AND',
                'condition' => "(PSNo NOT LIKE '%=%')",
                'params'    => []
            ]
        ];
		$sqlraw     = null;
		$queryRawPK = null;
		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			switch ( $r ) {
				case 'ttpshd/select':
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND DATE({$json['cmbTgl1']}) BETWEEN '{$json['tgl1']}' AND '{$json['tgl2']}' ";
					}
					$sqlraw     = "SELECT ttpshd.PSNo, ttpshd.PSTgl, ttpshd.SupKode, ttpshd.LokasiKode, ttpshd.PSKeterangan, ttpshd.PONo, 
       				ttpshd.PSSubTotal, ttpshd.PSDiscFinal, ttpshd.PSTotalPajak, ttpshd.PSBiayaKirim, ttpshd.PSTotal, ttpshd.UserID, ttpshd.NoGL, ttpshd.KodeTrans,        
       				IFNULL(tdsupplier.SupNama, '--') AS SupNama, IFNULL(ttpihd.piTgl, DATE('1900-01-01')) AS PITgl, ttpshd.PSNoRef       
					FROM ttpshd
					INNER JOIN ttpsit ON ttpsit.PSNo = ttpshd.PSNo
					LEFT OUTER JOIN ttpihd ON ttpshd.PINo = ttpihd.PINo        
					LEFT OUTER JOIN tdsupplier ON ttpshd.SupKode = tdsupplier.SupKode        
					WHERE PSQty > PIQtyS  AND ttpshd.PSNo NOT LIKE '%=%' {$whereRaw}
					GROUP BY ttpshd.PSNo  ORDER BY ttPShd.PSNo";
					$queryRawPK = [ 'PSNo' ];
					$colGrid    = Ttpshd::colGridPick();
					break;
				case 'ttpshd/ti-find-ps':
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND DATE({$json['cmbTgl1']}) BETWEEN '{$json['tgl1']}' AND '{$json['tgl2']}' ";
					}
					$sqlraw     = "SELECT ttpshd.PSNo, ttpshd.PSTgl, ttpshd.SupKode, ttpshd.LokasiKode, ttpshd.PSKeterangan, ttpshd.PONo, ttpshd.PSSubTotal, ttpshd.PSDiscFinal, ttpshd.PSTotalPajak, ttpshd.PSBiayaKirim, ttpshd.PSTotal, ttpshd.UserID, ttpshd.NoGL, ttpshd.KodeTrans, 
				    IFNULL(tdsupplier.SupNama, '--') AS SupNama, IFNULL(tttihd.tiTgl, DATE('1900-01-01')) AS TITgl, ttpshd.PSNoRef
				    FROM ttpshd 
					LEFT OUTER JOIN tttihd ON ttpshd.PSNo = tttihd.PSNo 
      				LEFT OUTER JOIN tdsupplier ON ttpshd.SupKode = tdsupplier.SupKode        
					WHERE tttihd.PSNo IS NULL  AND  ttpshd.LokasiKode <> 'Lokal' AND ttpshd.PSNo NOT LIKE '%=%' {$whereRaw}
					GROUP BY ttpshd.PSNo  ORDER BY ttPShd.PSNo";
					$queryRawPK = [ 'PSNo' ];
					$colGrid    = Ttpshd::colGridPick();
					break;
			}
		}
		return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Ttpshd::class,
				'colGrid'    => $colGrid,
				'queryRaw'   => $sqlraw,
				'queryRawPK' => $queryRawPK,
                'where'      => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionSelect() {
		$urlDetail    = Url::toRoute( [ 'ttpsit/index', 'jenis' => 'FillItemPSPI' ] );
		$this->layout = 'select-mode';
		return $this->render( 'select',
			[
				'arr'       => [
					'cmbTxt'    => [
						'PSNo'         => 'No PS',
						'SupKode'      => 'Kode Supplier',
						'LokasiKode'   => 'Lokasi',
						'PSKeterangan' => 'Keterangan',
						'KodeTrans'    => 'Kode Trans',
						'NoGL'         => 'No JT',
						'UserID'       => 'User ID',
					],
					'cmbTgl'    => [
						'PSTgl' => 'Tgl PS',
					],
					'cmbNum'    => [
						'PSTotal' => 'Total',
					],
					'sortname'  => "PSTgl",
					'sortorder' => 'desc'
				],
				'title'     => 'Daftar Purchase Sheet / Penerimaan Barang',
				'options'   => [ 'colGrid' => Ttpshd::colGridPick(), 'mode' => self::MODE_SELECT_DETAIL_MULTISELECT ],
				'urlDetail' => $urlDetail
			] );
	}
	public function actionTiFindPs() {
		$urlDetail    = Url::toRoute( [ 'ttpsit/index', 'jenis' => 'FillItemPSTI' ] );
		$this->layout = 'select-mode';
		return $this->render( 'select',
			[
				'arr'       => [
					'cmbTxt'    => [
						'PSNo'         => 'No PS',
						'SupKode'      => 'Kode Supplier',
						'LokasiKode'   => 'Lokasi',
						'PSKeterangan' => 'Keterangan',
						'KodeTrans'    => 'Kode Trans',
						'NoGL'         => 'No JT',
						'UserID'       => 'User ID',
					],
					'cmbTgl'    => [
						'PSTgl' => 'Tgl PS',
					],
					'cmbNum'    => [
						'PSTotal' => 'Total',
					],
					'sortname'  => "PSTgl",
					'sortorder' => 'desc'
				],
				'title'     => 'Daftar Purchase Sheet / Penerimaan Barang',
				'options'   => [ 'colGrid' => Ttpshd::colGridPick(), 'mode' => self::MODE_SELECT_DETAIL_MULTISELECT ],
				'urlDetail' => $urlDetail
			] );
	}
	/**
	 * Lists all Ttpshd models.
	 * @return mixed
	 */
	public function actionIndex() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttpshd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'index', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttpshd/td' ] )
		] );
	}
	/**
	 * Creates a new Ttpshd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$LokasiKodeCmb       = Tdlokasi::find()->where( [ 'LokasiStatus' => 'A', 'LEFT(LokasiNomor,1)' => '7' ] )->orderBy( 'LokasiStatus, LokasiKode' )->one();
		$SupplierKodeCmb     = Tdsupplier::find()->where( [ 'SupStatus' => 'A', 'LEFT(SupStatus,1)' => '1' ] )->orderBy( 'SupStatus, SupKode' )->one();
		$DealerKodeCmb       = Tddealer::find()->where( [ 'DealerStatus' => 'A', 'LEFT(DealerStatus,1)' => '1' ] )->orderBy( 'DealerStatus, DealerKode' )->one();
		$model               = new Ttpshd();
		$model->PSNo         = General::createTemporaryNo( 'PS', 'Ttpshd', 'PSNo' );
		$model->PSTgl        = date( 'Y-m-d H:i:s' );
		$model->PSKeterangan = "--";
		$model->LokasiKode   = 'Penerimaan1';
		$model->SupKode      = ( $SupplierKodeCmb != null ) ? $SupplierKodeCmb->SupKode : '';
		$model->DealerKode   = ( $DealerKodeCmb != null ) ? $DealerKodeCmb->DealerKode : '';
		$model->PSSubTotal   = 0;
		$model->PSDiscFinal  = 0;
		$model->PSTotalPajak = 0;
		$model->PSBiayaKirim = 0;
		$model->PSTotal      = 0;
		$model->PSTerbayar   = 0;
		$model->UserID       = $this->UserID();
		$model->KodeTrans    = "TC01";
		$model->PINo         = "--";
		$model->PONo         = "--";
		$model->TINo         = "--";
		$model->NoGL         = "--";
		$model->PSNoRef      = "--";
		$model->PosKode      = $this->LokasiKode();
		$model->Cetak        = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttpshd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTBeli               = Ttpshd::find()->ttpshdFillByNo()->where( [ 'ttpshd.PSNo' => $model->PSNo ] )
		                               ->asArray( true )->one();
		$dsTBeli[ 'POTgl' ]    = date( 'Y-m-d H:i:s' );
		$dsTBeli[ 'PITgl' ]    = date( 'Y-m-d H:i:s' );
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTBeli[ 'PSNoView' ] = $result[ 'PSNo' ];
		return $this->render( 'create', [
			'model'   => $model,
			'dsTBeli' => $dsTBeli,
			'id'      => $id
		] );
	}
	/**
	 * Updates an existing Ttpshd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$index = \yii\helpers\Url::toRoute( [ 'ttpshd/index' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		/** @var Ttpshd $model */
		$model   = $this->findModelBase64( 'Ttpshd', $id );
		$dsTBeli = Ttpshd::find()->ttpshdFillByNo()->where( [ 'ttpshd.PSNo' => $model->PSNo ] )
		                 ->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post             = array_merge( $dsTBeli, \Yii::$app->request->post() );
            $post[ 'PSTgl' ]  = $post[ 'PSTgl' ] . ' ' . $post[ 'PSJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttpshd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttpshd::findOne( $result[ 'PSNo' ] );
            if ( $result[ 'PSNo' ] == '--' || $model == null) {
                if ( $model === null ) {
                    Yii::$app->session->addFlash( 'warning', 'PSNo : ' . $result[ 'PSNo' ]. ' tidak ditemukan.' );
                }
                return $this->redirect( $index);
            }
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'PSNoView' ] = $result[ 'PSNo' ];
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				return $this->redirect( [ 'ttpshd/update', 'id' => $id, 'action' => 'display', ] );
			} else {
				return $this->render( 'update', [
					'model'   => $model,
					'dsTBeli' => $post,
					'id'      => $id,
				] );
			}
		}
		$dsTBeli[ 'Action' ] = 'Load';
		$result              = Ttpshd::find()->callSP( $dsTBeli );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTBeli[ 'PSNoView' ] = $_GET[ 'PSNoView' ] ?? $result[ 'PSNo' ];
		return $this->render( 'update', [
			'model'   => $model,
			'dsTBeli' => $dsTBeli,
			'id'      => $id
		] );
	}
	/**
	 * Deletes an existing Ttpshd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
//	public function actionDelete() {
//		/** @var Ttpshd $model */
//		$model             = $model = $this->findModelBase64( 'Ttpshd', $_POST[ 'id' ] );
//		$_POST[ 'PSNo' ]   = $model->PSNo;
//		$_POST[ 'Action' ] = 'Delete';
//		$result            = Ttpshd::find()->callSP( $_POST );
//		if ( $result[ 'status' ] == 0 ) {
//			return $this->responseSuccess( $result[ 'keterangan' ] );
//		} else {
//			return $this->responseFailed( $result[ 'keterangan' ] );
//		}
//	}

    public function actionDelete()
    {
        /** @var Ttpshd $model */
        $request = Yii::$app->request;
        if ($request->isAjax){
            $model = $this->findModelBase64('Ttpshd', $_POST['id']);
            $_POST['PSNo'] = $model->PSNo;
            $_POST['Action'] = 'Delete';
            $result = Ttpshd::find()->callSP($_POST);
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            $id = $request->get('id');
            $model = $this->findModelBase64('Ttpshd', $id);
            $_POST['PSNo'] = $model->PSNo;
            $_POST['Action'] = 'Delete';
            $result = Ttpshd::find()->callSP($_POST);
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['PSNo'] == '--') {
                return $this->redirect(['index']);
            } else {
                $model = Ttpshd::findOne($result['PSNo']);
                return $this->redirect(['ttpshd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
            }
        }
    }

	public function actionCancel( $id, $action ) {
		/** @var Ttpshd $model */
		$model             = $this->findModelBase64( 'Ttpshd', $id );
		$_POST[ 'PSNo' ]   = $model->PSNo;
		$_POST[ 'PSJam' ]  = $_POST[ 'PSTgl' ] . ' ' . $_POST[ 'PSJam' ];
		$_POST[ 'Action' ] = 'Cancel';
		$result            = Ttpshd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'PSNo' ] == '--' ) {
			return $this->redirect( [ 'index' ] );
		} else {
			$model = Ttpshd::findOne( $result[ 'PSNo' ] );
			return $this->redirect( [ 'ttpshd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client            = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response          = $client->post( Yii::$app->params['H2'][$_POST['db']]['host_report'] . '/abengkel/fpshd', [
			'body' => Json::encode( $_POST )
		] );
//		return $response->getBody();
        $html                 = $response->getBody();
        return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	/**
	 * Finds the Ttpshd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttpshd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttpshd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	public function actionCheckStock( $BrgKode, $LokasiKode ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return Tdbaranglokasi::find()
		                     ->where( 'BrgKode = :BrgKode AND LokasiKode = :LokasiKode', [ 'BrgKode' => $BrgKode, 'LokasiKode' => $LokasiKode ] )
		                     ->asArray()->one();
	}
	public function actionJurnal() {
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Jurnal';
		$result           = Ttpshd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( [ 'ttpshd/update', 'action' => 'display', 'id' => base64_encode( $result[ 'PSNo' ] ) ] );
	}


    public function actionImport()
    {
        $this->layout = 'select-mode';
        return $this->render('import', [
            'colGrid' => Ttpshd::colGridImportItems(),
            'mode' => self::MODE_SELECT_DETAIL_MULTISELECT,
        ]);
    }

    public function actionImportItems()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $fromTime = Yii::$app->formatter->asDate('-90 day', 'yyyy-MM-dd');
        $toTime = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
		$addParam = ['noPO'=>''];
        if (isset($_POST['query'])) {
            $query = Json::decode($_POST['query'], true);
            $fromTime = $query['tgl1'] ?? $fromTime;
            $toTime = $query['tgl2'] ?? $toTime;
			if($query['txt1'] !== ''){
				$addParam = array_merge($addParam,[$query['cmbTxt1']=>$query['txt1']]);
			}
        }
        $api = new Api();
        $mainDealer = $api->getMainDealer();
        $arrHasil = [];
        switch ($mainDealer) {
            case API::MD_HSO:
                $arrParams = [
                    'fromTime' => $fromTime . ' 00:00:00',
                    'toTime' => $toTime . ' 23:59:59',
                    'dealerId' => $api->getDealerId(),
                ];

                break;
            case API::MD_DAMH2:
            case API::MD_DAWH2:
            case API::MD_ANPER:
                $arrParams = [
                    'fromTime' => $fromTime . ' 00:00:00',
                    'toTime' => $toTime . ' 23:59:59',
                    'dealerId' => $api->getDealerId(),
                    'requestServerTime' => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                    'requestServertimezone' => date('T'),
                    'requestServerTimestamp' => $api->getCurrUnixTime(),
                ];
                break;
			default:
				$arrParams = [
					'fromTime'               => $fromTime . ' 00:00:00',
					'toTime'                 => $toTime . ' 23:59:59',
					'dealerId'               => $api->getDealerId(),
					'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
					'requestServertimezone'  => date('T'),
					'requestServerTimestamp' => $api->getCurrUnixTime(),
				];
        }
		$arrParams = array_merge_recursive($arrParams,$addParam);
		$filterResult = [];
		if($query['txt2'] !== ''){
			$filterResult = [
				'key' => $query['cmbTxt2'],
				'value' => $query['txt2'],
			];
		}
        $hasil = $api->call(Api::TYPE_PINB, Json::encode($arrParams),$filterResult);
        $arrHasil = Json::decode($hasil);
        if (!isset($arrHasil['data'])) {
            return [
                'rows' => [],
                'rowsCount' => 0
            ];
        }
        $_SESSION['ttpshd']['import'] = $arrHasil['data'];
        return [
            'rows' => $arrHasil['data'],
            'rowsCount' => sizeof($arrHasil['data'])
        ];
    }

    public function actionImportDetails()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $key = array_search($_POST['noPenerimaan'], array_column($_SESSION['ttpshd']['import'], 'noPenerimaan'));
        return [
            'rows' => $_SESSION['ttpshd']['import'][$key]['po'],
            'rowsCount' => sizeof($_SESSION['ttpshd']['import'][$key]['po'])
        ];
    }

	public function actionImportLoop(){
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$respon = [];
		$base64 = urldecode(base64_decode($requestData['header']));
		parse_str($base64, $header);
		$requestData['data'][0]['header']['data']['Action'] = 'ImporBefore';
		$requestData['data'][0]['header']['data']['NoTrans'] = $header['PSNo'];
		$result = Api::callPINB($requestData['data'][0]['header']['data']);
		Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		$model = Ttpshd::findOne($result['NoTrans']);
		$id = $this->generateIdBase64FromModel($model);
		$respon['id'] = $id;
		if($result['status'] == 1){
			return $respon;
		}
		foreach ($requestData['data'] as $item) {
			$item['data']['Action'] = 'Impor';
			$item['data']['NoTrans'] = $model->PSNo;
			$result = Api::callPINBit($item['data']);
			Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		}
		$requestData['data'][0]['header']['data']['Action'] = 'ImporAfter';
		$result = Api::callPINB($requestData['data'][0]['header']['data']);
		Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		return $respon;
	}


}
