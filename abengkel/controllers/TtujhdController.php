<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\models\Tdlokasi;
use abengkel\models\Tdsupplier;
use abengkel\models\Ttpohd;
use abengkel\models\Ttujhd;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtujhdController implements the CRUD actions for Ttujhd model.
 */
class TtujhdController extends AbengkelController {
	public function actions() {
		return [
			'td' => [
				'class' => TdAction::class,
				'model' => Ttujhd::class,
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttujhd models.
	 * @return mixed
	 */
	public function actionIndex() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttujhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'index', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttujhd/td' ] )
		] );
		/*
		return $this->render( 'index' );
		*/
	}
	/**
	 * Displays a single Ttujhd model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'index', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Finds the Ttujhd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttujhd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttujhd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttujhd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model               = new Ttujhd();
		$model->UJNo         = General::createTemporaryNo( 'UJ', 'Ttujhd', 'UJNo' );
		$model->UJTgl        = date( 'Y-m-d H:i:s' );
		$model->UJKeterangan = "--";
		$model->KodeTrans    = "UJ";
		$model->PosKode      = $this->LokasiKode();
		$model->Cetak        = "Belum";
		$model->UserID       = $this->UserID();
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttujhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsSetup               = Ttujhd::find()->ttujhdFillByNo()
		                               ->where( [ 'ttujhd.UJNo' => $model->UJNo ] )->asArray( true )->one();
		$dsSetup[ 'UJJam' ]    = $dsSetup[ 'UJTgl' ];
		$dsSetup[ 'NoGL' ]     = '--';
		$dsSetup[ 'UJTgl' ]    = date( 'Y-m-d H:i:s' );
		$dsSetup[ 'UJTgl' ]    = date( 'Y-m-d H:i:s' );
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsSetup[ 'UJNoView' ] = $result[ 'UJNo' ];
		return $this->render( 'create', [
			'model'   => $model,
			'dsSetup' => $dsSetup,
			'id'      => $id
		] );
	}
	/**
	 * Updates an existing Ttujhd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$index = \yii\helpers\Url::toRoute( [ 'ttujhd/index' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$model   = $this->findModelBase64( 'Ttujhd', $id );
		$dsSetup = Ttujhd::find()->ttujhdFillByNo()->where( [ 'ttujhd.UJNo' => $model->UJNo ] )
		                 ->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post             = array_merge( $dsSetup, \Yii::$app->request->post() );
			$_POST[ 'UJTgl' ] = $_POST[ 'UJTgl' ] . ' ' . $_POST[ 'UJJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttujhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttujhd::findOne( $result[ 'UJNo' ] );
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'UJNoView' ] = $result[ 'UJNo' ];
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				return $this->redirect( [ 'ttujhd/update', 'action' => 'display', 'id' => $id ] );
			} else {
				return $this->render( 'update', [
					'model'   => $model,
					'dsSetup' => $post,
					'id'      => $id,
				] );
			}
		}
		$dsSetup[ 'UJJam' ]  = $dsSetup[ 'UJTgl' ];
		$dsSetup[ 'NoGL' ]   = '--';
		$dsSetup[ 'UJTgl' ]  = General::asDate( $dsSetup[ 'UJTgl' ] );
		$dsSetup[ 'Action' ] = 'Load';
		$result              = Ttujhd::find()->callSP( $dsSetup );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsSetup[ 'UJNoView' ] = $_GET[ 'UJNoView' ] ?? $result[ 'UJNo' ];
		return $this->render( 'update', [
			'model'   => $model,
			'dsSetup' => $dsSetup,
			'id'      => $id
		] );
	}
	/**
	 * Deletes an existing Ttujhd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
		/** @var Ttujhd $model */
		$model             = $this->findModelBase64( 'Ttujhd', $_POST[ 'id' ] );
		$_POST[ 'UJNo' ]   = $model->UJNo;
		$_POST[ 'Action' ] = 'Delete';
		$result            = Ttujhd::find()->callSP( $_POST );
		if ( $result[ 'status' ] == 0 ) {
			return $this->responseSuccess( $result[ 'keterangan' ] );
		} else {
			return $this->responseFailed( $result[ 'keterangan' ] );
		}
	}
	public function actionCancel( $id ) {
		/** @var Ttujhd $model */
		$model             = $this->findModelBase64( 'Ttujhd', $id );
		$_POST[ 'UJNo' ]   = $model->UJNo;
		$_POST[ 'UJTgl' ]  = $_POST[ 'UJTgl' ] . ' ' . $_POST[ 'UJJam' ];
		$_POST[ 'Action' ] = 'Cancel';
		$result            = Ttujhd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'UJNo' ] == '--' ) {
			return $this->redirect( [ 'index' ] );
		} else {
			$model = Ttujhd::findOne( $result[ 'UJNo' ] );
			return $this->redirect( [ 'ttujhd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client            = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response          = $client->post( Yii::$app->params['H2'][$_POST['db']]['host_report'] . '/abengkel/fujhd', [
			'body' => Json::encode( $_POST )
		] );
//		return $response->getBody();
        $html                 = $response->getBody();
        return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
}
