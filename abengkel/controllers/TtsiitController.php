<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\models\Ttsihd;
use abengkel\models\Ttsdhd;
use abengkel\models\Ttsiit;
use abengkel\models\Ttsohd;
use Yii;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtsiitController implements the CRUD actions for Ttsiit model.
 */
class TtsiitController extends AbengkelController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttsiit models.
	 * @return mixed
	 */
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttsiit::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
			/** @var Ttsiit $model */
			$model = null;
			if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
				$model = $this->findModelBase64( 'ttsiit', $requestData[ 'id' ] );
			}
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
				Yii::info($requestData,'REQUEST');
					if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'Action' ] = 'Insert';
						$requestData[ 'SIAuto' ] = 0;
					} else {
						$requestData[ 'Action' ] = 'Update';
						if ( $model != null ) {
							$requestData[ 'SINoOLD' ]    = $model->SINo;
							$requestData[ 'SIAutoOLD' ]  = $model->SIAuto;
							$requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
						}
					}
					$result = Ttsiit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'batchSO' :
					\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
					$respon                      = [];
					$base64                      = urldecode( base64_decode( $requestData[ 'header' ] ) );
					parse_str( $base64, $header );
					/** @var Ttsohd $SO */
					$SO = Ttsohd::findOne( $header[ 'SONo' ] );
					if ( $SO != null ) {
						$header[ 'SOTgl' ] = $SO->SOTgl;
					}
					$header[ 'Cetak' ]  = 'SO';
					$header[ 'Action' ] = 'Loop';
					$result             = Ttsihd::find()->callSP( $header );
					Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
					foreach ( $requestData[ 'data' ] as $item ) {
						$item[ 'Action' ] = 'Insert';
						$item[ 'KodeTrans' ]       = $header[ 'KodeTrans' ];
						$result           = Ttsiit::find()->callSP( $item );
						Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
					}
					$header[ 'Action' ] = 'LoopAfter';
					$result             = Ttsihd::find()->callSP( $header );
					Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
					return $respon;
					break;
				case 'batchSD' :
					\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
					$respon                      = [];
					$base64                      = urldecode( base64_decode( $requestData[ 'header' ] ) );
					parse_str( $base64, $header );
					/** @var Ttsdhd $SD */
					$SD = Ttsdhd::findOne( $header[ 'SDNo' ] );
					if ( $SD != null ) {
						$header[ 'SDTgl' ] = $SD->SDTgl;
					}
					$header[ 'Cetak' ]  = 'SD';
					$header[ 'Action' ] = 'Loop';
					$result             = Ttsihd::find()->callSP( $header );
					Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
					foreach ( $requestData[ 'data' ] as $item ) {
						$item[ 'Action' ] = 'Insert';
						$item[ 'KodeTrans' ]       = $header[ 'KodeTrans' ];
						$result           = Ttsiit::find()->callSP( $item );
						Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
					}
					$header[ 'Action' ] = 'LoopAfter';
					$result             = Ttsihd::find()->callSP( $header );
					Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
					return $respon;
					break;
				case 'del'  :
					$requestData             = array_merge( $requestData, $model->attributes );
					$requestData[ 'Action' ] = 'Delete';
					if ( $model != null ) {
						$requestData[ 'SINoOLD' ]    = $model->SINo;
						$requestData[ 'SIAutoOLD' ]  = $model->SIAuto;
						$requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
					}
					$result = Ttsiit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
			}
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'ttsiit.' . $primaryKeys[ $i ];
			}
			$jenis = $_GET[ 'jenis' ] ?? ' ';
			switch ( $jenis ) {
				case 'FillItemSISR':
					$query = ( new \yii\db\Query() )
						->select( new Expression(
							'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
								ttSIit.SINo, ttSIit.SIAuto, ttSIit.BrgKode, ttSIit.SIQty , ttSIit.SIHrgBeli, ttSIit.SIHrgJual, ttSIit.SIDiscount, ttSIit.SIPajak, tdbarang.BrgNama, tdbarang.BrgSatuan, 0 AS Disc, 
                                ttSIit.SIQty * ttSIit.SIHrgJual - ttSIit.SIDiscount AS Jumlah, tdbarang.BrgGroup, ttSIhd.LokasiKode'
						) )
						->from( 'ttSIit' )
						->join( 'INNER JOIN', 'tdbarang', 'ttSIit.BrgKode = tdbarang.BrgKode' )
						->join( 'INNER JOIN', 'ttSIhd', 'ttSIhd.SINo = ttSIit.SINo' )
						->where( [ 'ttSIit.SINo' => $requestData[ 'SINo' ] ] )
						->orderBy( 'ttSIit.SINo, ttSIit.SIAuto' );
					break;
				default:
					$query = ( new \yii\db\Query() )
						->select( new Expression(
							'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
								ttsiit.SINo, ttsiit.SIAuto, ttsiit.BrgKode, ttsiit.SIQty, ttsiit.SIHrgBeli, ttsiit.SIHrgJual, ttsiit.SIDiscount, ttsiit.SIPajak, 
								tdbarang.BrgNama, tdbarang.BrgSatuan, IFNULL(ttsiit.SIDiscount / (ttsiit.SIHrgJual * ttsiit.SIQty) * 100, 0) AS Disc, 
                                ttsiit.SIQty * ttsiit.SIHrgJual - ttsiit.SIDiscount AS Jumlah, ttsiit.LokasiKode, ttsiit.SIPickingNo, ttsiit.SIPickingDate, tdbarang.BrgGroup, 
                                ttsiit.SONo, ttsiit.Cetak, tdbarang.BrgRak1'
						) )
						->from( 'ttsiit' )
						->join( 'INNER JOIN', 'tdbarang', 'ttsiit.BrgKode = tdbarang.BrgKode' )
						->where( [ 'ttsiit.SINo' => $requestData[ 'SINo' ] ] )
						->orderBy( 'ttsiit.SINo, ttsiit.SIAuto' );
			}
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	/**
	 * Finds the Ttsiit model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $SINo
	 * @param integer $SIAuto
	 *
	 * @return Ttsiit the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $SINo, $SIAuto ) {
		if ( ( $model = Ttsiit::findOne( [ 'SINo' => $SINo, 'SIAuto' => $SIAuto ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttsiit model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Ttsiit();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'SINo' => $model->SINo, 'SIAuto' => $model->SIAuto ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Ttsiit model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $SINo
	 * @param integer $SIAuto
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $SINo, $SIAuto ) {
		$model = $this->findModel( $SINo, $SIAuto );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'SINo' => $model->SINo, 'SIAuto' => $model->SIAuto ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Ttsiit model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $SINo
	 * @param integer $SIAuto
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $SINo, $SIAuto ) {
		$this->findModel( $SINo, $SIAuto )->delete();
		return $this->redirect( [ 'index' ] );
	}
}
