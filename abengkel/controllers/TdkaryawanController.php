<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\components\TUi;
use abengkel\models\Tdkaryawan;
use common\components\General;
use Yii;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
/**
 * TdkaryawanController implements the CRUD actions for Tdkaryawan model.
 */
class TdkaryawanController extends AbengkelController {

	public function beforeAction( $action ) {
		$GLOBALS[ 'MenuText' ] = 'Karyawan';
		switch ( $action->id ) {
			case 'create':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::ADD );
				break;
			case 'update':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::UPDATE );
				break;
			case 'delete':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::DELETE );
				break;
			case 'index':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::DISPLAY );
				break;
			case 'td':
			case 'cancel':
			case 'cek-password':
				$this->isAllow = 1;
				break;
		}
//		if($this->isAllow == 0) throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
		return true;
	}

	public function actions() {
        $where                  = [
            [
                'op'        => 'AND',
                'condition' => "(KarStatus = 'A')",
                'params'    => []
            ]
        ];
		return [
			'td' => [
				'class' => TdAction::class,
				'model' => Tdkaryawan::class,
                'where'      => $where

			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Tdkaryawan models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index' );
	}
	/**
	 * Displays a single Tdkaryawan model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'index', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Creates a new Tdkaryawan model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		TUi::$actionMode   = Menu::ADD;
		$model = new Tdkaryawan();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'tdkaryawan/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
		}
		return $this->render( 'create', [
			'model' => $model,
			'id'    => 'new'
		] );
	}
	/**
	 * Updates an existing Tdkaryawan model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		$model = $this->findModelBase64( 'Tdkaryawan', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'tdkaryawan/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
		}
		return $this->render( 'update', [
			'model' => $model,
			'id'    => $this->generateIdBase64FromModel( $model ),
		] );
	}
	/**
	 * Deletes an existing Tdkaryawan model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
		$model = $this->findModelBase64( 'Tdkaryawan', $id );
		$model->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Tdkaryawan model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Tdkaryawan the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Tdkaryawan::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	public function actionCancel( $id, $action ) {
		$model = Tdkaryawan::findOne( $id );
		if ( $model == null ) {
			$model = Tdkaryawan::find()->one();
			if ( $model == null ) {
				return $this->redirect( [ 'tdkaryawan/index' ] );
			}
		}
		return $this->redirect( [ 'tdkaryawan/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
	}
	public function actionCekPassword() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$KarKode                     = Tdkaryawan::find()
		                                         ->select( [ 'KarKode' ] )
		                                         ->where( [ 'KarKode' => $_POST[ 'KarKode' ], 'KarPassword' => $_POST[ 'KarPassword' ] ] )
		                                         ->scalar();
		$data                        = [];
		if ( $KarKode !== false ) {
			$data = General::cCmd( "SELECT * FROM ttsdhd LEFT OUTER JOIN tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
				WHERE SVNO = ('--') AND  PosKode = :PosKode ", [ ':PosKode' => Menu::getUserLokasi()[ 'PosKode' ] ] )->queryAll();
		}
		return [
			'success' => true,
			'msg'     => ( $KarKode !== false ) ? $KarKode : 'false',
			'data'    => $data
		];
	}
}
