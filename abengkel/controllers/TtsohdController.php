<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\models\Tdbaranglokasi;
use abengkel\models\Tdkaryawan;
use abengkel\models\Tdlokasi;
use abengkel\models\Ttsohd;
use abengkel\models\Vtkbm;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
/**
 * TtsohdController implements the CRUD actions for Ttsohd model.
 */
class TtsohdController extends AbengkelController {

	public function beforeAction( $action ) {
		if ( ! parent::beforeAction( $action ) ) {
			return false;
		}
		$GLOBALS[ 'MenuText' ] = '[SO] Order Penjualan';
		switch ( $action->id ) {
			case 'create':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::ADD );
				break;
			case 'update':
				$get = Yii::$app->request->get('action','view');
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], ($get === 'view') ? Menu::VIEW : Menu::UPDATE );
				break;
			case 'delete':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::DELETE );
				break;
			case 'index':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::DISPLAY );
				break;
			case 'print':
				$this->isAllow = Menu::akses( $GLOBALS[ 'MenuText' ], Menu::print );
				break;
			case 'td':
			case 'cancel':
				$this->isAllow = 1;
				break;
		}
		if ( $this->isAllow == 0 ) {
			//throw new ForbiddenHttpException( Yii::t( 'yii', 'You are not allowed to perform this action.' ) );
		}
		return true;
	}

	public function actions() {
		$colGrid    = null;
		$joinWith   = [
			'customer' => [
				'type' => 'INNER JOIN'
			]
		];
		$join       = [];
        $where      = [
                        [
                            'op'        => 'AND',
                            'condition' => "(SONo NOT LIKE '%=%')",
                            'params'    => []
                        ]
        ];
		$sqlraw     = null;
		$queryRawPK = null;
		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			switch ( $r ) {
				case 'ttsohd/select':
                    $colGrid    = Ttsohd::colGridPick();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						// $whereRaw = " (SELECT REPLACE('{$json['tgl1']}','-', '/')) AND (SELECT REPLACE('{$json['tgl2']}','-', '/'))";
						$whereRaw = " AND DATE(ttsohd.SOTgl) BETWEEN '{$json[ 'tgl1' ]}' AND '{$json['tgl2']}'";
					}
					if($urlParams['loop'] == 'SI'){
					//SI
						$sqlraw = "SELECT  ttsohd.*, tdcustomer.CusNama, IFNULL (ttsihd.siTgl, DATE ('1900-01-01')) AS SITgl
							FROM ttsohd
							INNER JOIN ttsoit ON ttsohd.SONo = ttsoit.SONo
							INNER JOIN tdcustomer ON tdcustomer.MotorNoPolisi = ttsohd.MotorNoPolisi
							LEFT OUTER JOIN ttsihd ON ttsohd.SINo = ttSIhd.SINo
							WHERE ttsoit.SOQty > ttsoit.SIQtyS
                                  {$whereRaw}
                                  AND ttsohd.SONo LIKE '%%'
                                GROUP BY ttSOhd.SONo
                                ORDER BY ttSOhd.SONo ";
					}elseif($urlParams['loop'] == 'PO'){
					//PO
						$sqlraw = "SELECT
								ttsohd.*, tdcustomer.CusNama, IFNULL (ttpohd.POTgl,DATE('1900-01-01')) AS POTgl
								FROM
								ttsohd
								INNER JOIN ttsoit
									ON ttsohd.SONo = ttsoit.SONo
								INNER JOIN tdcustomer
									ON tdcustomer.MotorNoPolisi = ttsohd.MotorNoPolisi
								LEFT OUTER JOIN ttpohd
									ON ttsohd.PONo = ttpohd.PONo
								WHERE ttsoit.SOQty > ttsoit.POQtyS
								{$whereRaw}  AND ttsohd.SONo LIKE '%%'
								GROUP BY ttSOhd.SONo
								ORDER BY ttSOhd.SONo";
					}
					$queryRawPK = [ 'SONo' ];
					break;
			}
		}
		return [
			'td' => [
				'class'      => \abengkel\components\TdAction::class,
				'model'      => Ttsohd::class,
				'queryRaw'   => $sqlraw,
				'queryRawPK' => $queryRawPK,
				'joinWith'   => $joinWith,
                'where'      => $where,
                'colGrid'    => $colGrid,
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionSelect() {
		$urlDetail    = Url::toRoute( [ 'ttsoit/index', 'jenis' => 'FillItemSOPO' ] );
		$this->layout = 'select-mode';
		return $this->render( 'select',
			[
				'arr'       => [
					'cmbTxt'    => [
						'SONo'         => 'No SO',
						'SupKode'      => 'Kode Supplier',
						'LokasiKode'   => 'Lokasi',
						'SOKeterangan' => 'Keterangan',
						'KodeTrans'    => 'Kode Trans',
						'NoGL'         => 'No JT',
						'UserID'       => 'User ID',
					],
					'cmbTgl'    => [
						'SOTgl' => 'Tgl SO',
					],
					'cmbNum'    => [
						'SOTotal' => 'Total',
					],
					'sortname'  => "SOTgl",
					'sortorder' => 'desc'
				],
				'title'     => 'Daftar Sales Order / Order Penjualan',
				'options'   => [ 'colGrid' => Ttsohd::colGridPick(), 'mode' => self::MODE_SELECT_DETAIL_MULTISELECT ],
				'urlDetail' => $urlDetail,
			] );
	}
	/**
	 * Lists all Ttsohd models.
	 * @return mixed
	 */
	public function actionIndex() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttsohd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'index', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttsohd/td' ] )
		] );
	}
	/**
	 * Creates a new Ttsohd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$LokasiKodeCmb        = Tdlokasi::find()->where( [ 'LokasiStatus' => 'A', 'LEFT(LokasiNomor,1)' => '7' ] )->orderBy( 'LokasiStatus, LokasiKode' )->one();
		$KarKodeCmb           = Tdkaryawan::find()->where( [ 'KarStatus' => 'A' ] )->orderBy( 'KarStatus,KarKode' )->one();
		$model                = new Ttsohd();
		$model->SONo          = General::createTemporaryNo( 'SO', 'Ttsohd', 'SONo' );
		$model->SOTgl         = date( 'Y-m-d H:i:s' );
		$model->MotorNoPolisi = "UMUM";
		$model->SOKeterangan  = "--";
		$model->LokasiKode    = ( $LokasiKodeCmb != null ) ? $LokasiKodeCmb->LokasiKode : '';
		$model->KarKode       = ( $KarKodeCmb != null ) ? $KarKodeCmb->KarKode : '';
		$model->SOSubTotal    = 0;
		$model->SODiscFinal   = 0;
		$model->SOTotalPajak  = 0;
		$model->SOBiayaKirim  = 0;
		$model->SOTotal       = 0;
		$model->UserID        = $this->UserID();
		$model->KodeTrans     = "SO";
		$model->SINo          = "--";
		$model->PONo          = "--";
		$model->PosKode       = $this->LokasiKode();
		$model->Cetak         = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttsohd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsJual               = Ttsohd::find()->GetDataByNo()->where( [ 'ttsohd.SONo' => $model->SONo ] )
		                              ->groupBy( [ 'ttsohd.SONo' ] )
		                              ->asArray()->one();
		$dsJual[ 'SONoView' ] = $result[ 'SONo' ];
		$dsJual[ 'SOJam' ]    = $dsJual[ 'SOTgl' ];
		$dsJual[ 'StatusBayar' ]  = 'BELUM';
		$dsJual[ 'TotalBayar' ]   = 0;
		$dsJual[ 'SisaBayar' ]    = 0;
		$dsJual[ 'UangMuka' ]     = 0;
		$dsJual[ 'SOTotalPajak' ] = 0;
		$dsJual[ 'SOBiayaKirim' ] = 0;
		$dsJual[ 'SODiscPersen' ] = 0;
		$id                       = $this->generateIdBase64FromModel( $model );
		return $this->render( 'create', [
			'model'  => $model,
			'dsJual' => $dsJual,
			'id'     => $id,
		] );
	}
	/**
	 * Updates an existing Ttsohd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 */
	public function actionUpdate( $id ) {
		$index = \yii\helpers\Url::toRoute( [ 'ttsohd/index' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$model    = $this->findModelBase64( 'Ttsohd', $id );
		$dsJual   = Ttsohd::find()->GetDataByNo()->where( [ 'ttsohd.SONo' => $model->SONo ] )
		                  ->groupBy( [ 'ttsohd.SONo' ] )
		                  ->asArray()->one();
		$Terbayar = floatval($dsJual['SOTerbayar']); //Vtkbm::find()->getTotalBayar( $model->SONo );
		if ( Yii::$app->request->isPost ) {
			$post               = array_merge($dsJual, \Yii::$app->request->post());
			$post[ 'SOTgl' ]    = $post[ 'SOTgl' ] . ' ' . $post[ 'SOJam' ];
			$post[ 'Action' ]   = 'Update';
			$result             = Ttsohd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttsohd::findOne( $result[ 'SONo' ] );
            //            $model     = null;
            if ( $result[ 'SONo' ] == '--' || $model == null) {
                if ( $model === null ) {
                    Yii::$app->session->addFlash( 'warning', 'SONo : ' . $result[ 'SONo' ]. ' tidak ditemukan.' );
                }
                return $this->redirect( $index);
            }
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'SONoView' ] = $result[ 'SONo' ];
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				return $this->redirect( [ 'ttsohd/update', 'action' => 'display', 'id' => $id ] );
			} else {
				return $this->render( 'update', [
					'model'  => $model,
					'dsJual' => $post,
					'id'     => $id,
				] );
			}
		}
		$dsJual[ 'SOJam' ]      = $dsJual[ 'SOTgl' ];
		$dsJual[ 'SOTgl' ]      = General::asDate( $dsJual[ 'SOTgl' ] );
		$dsJual[ 'TotalBayar' ] = $Terbayar;
		$dsJual[ 'UangMuka' ]   = 0;
		$dsJual[ 'SOTotal' ]    = floatval( $dsJual[ 'SOSubTotal' ] ) + floatval( $dsJual[ 'SOTotalPajak' ] ) + floatval( $dsJual[ 'SOBiayaKirim' ] ) -
		                          floatval( $dsJual[ 'SODiscFinal' ] ) - floatval( $dsJual[ 'UangMuka' ] );
		if ( ( floatval( $dsJual[ 'SOTotal' ] ) - $Terbayar ) > 0 || floatval( $dsJual[ 'SOTotal' ] ) == 0 ) {
			$dsJual[ 'StatusBayar' ] = 'BELUM';
		} else if ( ( floatval( $dsJual[ 'SOTotal' ] ) - $Terbayar ) <= 0 ) {
			$dsJual[ 'StatusBayar' ] = 'LUNAS';
		}
		$dsJual[ 'SisaBayar' ]    = floatval( $dsJual[ 'SOTotal' ] ) - $Terbayar;
		$dsJual[ 'SOTotalPajak' ] = 0;
		$dsJual[ 'SOBiayaKirim' ] = 0;
		$dsJual[ 'SODiscPersen' ] = 0;
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsJual[ 'Action' ] = 'Load';
			$result             = Ttsohd::find()->callSP( $dsJual );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'SONo' ] = $dsJual[ 'SONo' ];
		}
		$dsJual[ 'SONoView' ] = $_GET[ 'SONoView' ] ?? $result[ 'SONo' ];
		return $this->render( 'update', [
			'model'  => $model,
			'dsJual' => $dsJual,
			'id'     => $id,
		] );
	}
	/**
	 * Deletes an existing Ttsohd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
//	public function actionDelete() {
//		/** @var Ttsohd $model */
//		$model             = $model = $this->findModelBase64( 'Ttsohd', $_POST[ 'id' ] );
//		$_POST[ 'SONo' ]   = $model->SONo;
//		$_POST[ 'Action' ] = 'Delete';
//		$result            = Ttsohd::find()->callSP( $_POST );
//		if ( $result[ 'status' ] == 0 ) {
//			return $this->responseSuccess( $result[ 'keterangan' ] );
//		} else {
//			return $this->responseFailed( $result[ 'keterangan' ] );
//		}
//	}

    public function actionDelete()
    {
        /** @var Ttsohd $model */
        $request = Yii::$app->request;
        if ($request->isAjax){
            $model = $this->findModelBase64('Ttsohd', $_POST['id']);
            $_POST['SONo'] = $model->SONo;
            $_POST['Action'] = 'Delete';
            $result = Ttsohd::find()->callSP($_POST);
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            $id = $request->get('id');
            $model = $this->findModelBase64('Ttsohd', $id);
            $_POST['SONo'] = $model->SONo;
            $_POST['Action'] = 'Delete';
            $result = Ttsohd::find()->callSP($_POST);
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['SONo'] == '--') {
                return $this->redirect(['index']);
            } else {
                $model = Ttsohd::findOne($result['SONo']);
                return $this->redirect(['ttsohd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
            }
        }
    }

	public function actionCancel( $id ) {
		/** @var Ttsohd $model */
		$model             = $this->findModelBase64( 'Ttsohd', $id );
		$_POST[ 'SOJam' ]  = $_POST[ 'SOTgl' ] . ' ' . $_POST[ 'SOJam' ];
		$_POST[ 'Action' ] = 'Cancel';
		$result            = Ttsohd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'SONo' ] == '--' ) {
			return $this->redirect( [ 'index' ] );
		} else {
			$model = Ttsohd::findOne( $result[ 'SONo' ] );
			return $this->redirect( [ 'ttsohd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST         = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client        = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response      = $client->post( Yii::$app->params['H2'][$_POST['db']]['host_report'] . '/abengkel/fsohd', [
			'body' => Json::encode( $_POST )
		] );
		$html          = $response->getBody();
		return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	/**
	 * Finds the Ttsohd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttsohd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttsohd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}

    public function actionCheckStock( $BrgKode, $LokasiKode ) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return Tdbaranglokasi::find()
            ->where( 'BrgKode = :BrgKode AND LokasiKode = :LokasiKode', [ 'BrgKode' => $BrgKode , 'LokasiKode' => $LokasiKode] )
            ->asArray()->one();
    }

    public function actionStatusOtomatis() {
        $this->layout   = 'popup-form';
        return $this->render( '_formStatus', [ 'data' => $_GET ] );
    }

    public function actionStatusOtomatisItem( $SONo ) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $dt = General::cCmd( "SELECT KBMNo, KBMTgl, KBMLink, KBMBayar, Kode, Person, Memo, NoGL FROM vtKBM WHERE KBMLink = :SONo ORDER BY KBMTgl;", [':SONo' => $SONo ] )->queryAll();
        return [
            'rows' => $dt
        ];

    }
}
