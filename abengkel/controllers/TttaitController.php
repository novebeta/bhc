<?php

namespace abengkel\controllers;

use abengkel\components\AbengkelController;
use abengkel\models\Tttait;
use yii\db\Expression;
use yii\filters\VerbFilter;
/**
 * TttaitController implements the CRUD actions for Tttait model.
 */
class TttaitController extends AbengkelController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tttait models.
     * @return mixed
     */
    public function actionIndex()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $requestData                 = \Yii::$app->request->post();
        $primaryKeys                 = Tttait::getTableSchema()->primaryKey;
        if ( isset( $requestData[ 'oper' ] ) ) {
            /* operation : create, edit or delete */
            $oper = $requestData[ 'oper' ];
            /** @var Tttait $model */
            $model = null;
            if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
                $model = $this->findModelBase64( 'tttait', $requestData[ 'id' ] );
            }
            switch ( $oper ) {
                case 'add'  :
                case 'edit' :
                    if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
                        $requestData[ 'Action' ] = 'Insert';
                        $requestData[ 'TAAuto' ] = 0;
                    } else {
                        $requestData[ 'Action' ] = 'Update';
                        if ( $model != null ) {
                            $requestData[ 'TANoOLD' ]   = $model->TANo;
                            $requestData[ 'TAAutoOLD' ] = $model->TAAuto;
                            $requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
                        }
                    }
                    $result = Tttait::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
                    break;
                case 'batch' :
                    break;
                case 'del'  :
                    $requestData             = array_merge( $requestData, $model->attributes );
                    $requestData[ 'Action' ] = 'Delete';
                    if ( $model != null ) {
                        $requestData[ 'TANoLD' ]   = $model->TANo;
                        $requestData[ 'TAAutoOLD' ] = $model->TAAuto;
                        $requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
                    }
                    $result = Tttait::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
                    break;
            }
        } else {
            for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
                $primaryKeys[ $i ] = 'Tttait.' . $primaryKeys[ $i ];
            }
            $query     = ( new \yii\db\Query() )
                ->select( new Expression(
                    'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
							    tttait.TANo,
                                tttait.TAAuto,
                                tttait.BrgKode,
                                tttait.TAQty,
                                tttait.TAHrgBeli,
                                tdbarang.BrgNama,
                                tdbarang.BrgSatuan,
                                tttait.TAQty * tttait.TAHrgBeli AS Jumlah,
                                tttait.LokasiKode,
                                tdbarang.BrgGroup,
                                tdbarang.BrgRak1'
                ) )
                ->from( 'tttait' )
                ->join( 'INNER JOIN', 'tdbarang', 'tttait.BrgKode = tdbarang.BrgKode' )
                ->where( [ 'tttait.TANo' => $requestData[ 'TANo' ] ] );
            $rowsCount = $query->count();
            $rows      = $query->all();
            /* encode row id */
            for ( $i = 0; $i < count( $rows ); $i ++ ) {
                $rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
            }
            $response = [
                'rows'      => $rows,
                'rowsCount' => $rowsCount
            ];
        }
        return $response;
    }

}
