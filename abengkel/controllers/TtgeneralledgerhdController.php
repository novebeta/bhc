<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\TdAction;
use abengkel\components\Menu;
use abengkel\components\TUi;
use abengkel\models\Ttgeneralledgerhd;
use abengkel\models\Ttglhpit;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\db\Exception;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtgeneralledgerhdController implements the CRUD actions for Ttgeneralledgerhd model.
 */
class TtgeneralledgerhdController extends AbengkelController {
    public function actions() {
        $colGrid    = null;
        $where      = [];
        $sqlraw     = null;
        $queryRawPK = null;
        if ( isset( $_POST[ 'query' ] ) ) {
            $json      = Json::decode( $_POST[ 'query' ], true );
            $r         = $json[ 'r' ];
            $urlParams = $json[ 'urlParams' ];
            switch ( $r ) {
                case 'ttgeneralledgerhd/memorial':
                    $where = [
                        [
                            'op'        => 'AND',
                            'condition' => "KodeTrans = 'GL'",
                            'params'    => []
                        ]
                    ];
                    break;
                case 'ttgeneralledgerhd/transaksi':
                    $where = [
                        [
                            'op'        => 'AND',
                            'condition' => "KodeTrans <> 'GL' AND KodeTrans <> 'JA' AND KodeTrans <> 'JP'",
                            'params'    => []
                        ]
                    ];
                    break;
                case 'ttgeneralledgerhd/adjustment':
                    $where = [
                        [
                            'op'        => 'AND',
                            'condition' => "KodeTrans = 'JA'",
                            'params'    => []
                        ]
                    ];
                    break;
                case 'ttgeneralledgerhd/pindahbuku':
                    $where = [
                        [
                            'op'        => 'AND',
                            'condition' => "KodeTrans = 'JP'",
                            'params'    => []
                        ]
                    ];
                    break;
                case 'ttgeneralledgerhd/select-mm-hutang':
                    $colGrid    = Ttgeneralledgerhd::colGridSelectMMHutang();
                    $queryRawPK = [ 'GLLink' ];
                    $whereRaw   = '';
                    if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
                        $whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
                    }
                    $NoGL   = $urlParams[ 'NoGL' ];
                    $sqlraw = "SELECT 
						  ttglhpit.NoAccount, traccount.NamaAccount, ttglhpit.GLLink, ttglhpit.HPNilai, ttgeneralledgerhd.NoGL, 
       						ttgeneralledgerhd.TglGL, ttgeneralledgerhd.MemoGL 
						FROM
						  ttglhpit 
						  INNER JOIN traccount 
						    ON ttglhpit.NoAccount = traccount.NoAccount 
						  INNER JOIN ttgeneralledgerhd 
						    ON ttglhpit.GLLink = ttgeneralledgerhd.GLLink 
						WHERE ttglhpit.NoGL = '{$NoGL}' 
						  AND ttgeneralledgerhd.MemoGL NOT LIKE 'Terima Stok Intransit%' 
						  AND ttgeneralledgerhd.KodeTrans <> 'JA'
							{$whereRaw}
						UNION
						SELECT 
						  ttglhpit.NoAccount, traccount.NamaAccount, ttglhpit.GLLink, ttglhpit.HPNilai, ttgeneralledgerhd.NoGL, 
						       ttgeneralledgerhd.TglGL, ttgeneralledgerhd.MemoGL 
						FROM
						  ttglhpit 
						  INNER JOIN traccount 
						    ON ttglhpit.NoAccount = traccount.NoAccount 
						  INNER JOIN ttgeneralledgerhd 
						    ON ttglhpit.GLLink = ttgeneralledgerhd.GLLink 
						  INNER JOIN ttgeneralledgerit 
						    ON ttgeneralledgerhd.NoGL = ttgeneralledgerit.NoGL 
						    AND ttgeneralledgerit.NoAccount = ttglhpit.NoAccount 
						WHERE ttglhpit.NoGL = '{$NoGL}' 
						  AND ttgeneralledgerhd.MemoGL NOT LIKE 'Terima Stok Intransit%' 
						  AND ttgeneralledgerhd.KodeTrans = 'JA'
							{$whereRaw} 
						GROUP BY ttglhpit.GLLink ";
                    break;
                case 'ttgeneralledgerhd/select-piutang-hutang':
                    $JenisHP       = $urlParams[ 'JenisHP' ];
                    $MyNoAccountHP = $urlParams[ 'MyNoAccountHP' ];
                    $colGrid       = ( $JenisHP === 'Piutang' ) ? Ttgeneralledgerhd::colGridSelectPiutang() : Ttgeneralledgerhd::colGridSelectHutang();
                    $queryRawPK    = [ 'GLLink' ];
                    $whereRaw      = '';
                    if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
                        $whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
                    }
                    $sqlraw = '';
                    if ( $JenisHP === 'Piutang' ) {
                        General::cCmd("TRUNCATE tvpiutangbayar; INSERT INTO tvpiutangbayar SELECT * FROM vpiutangbayar ;")->execute();
                        $sqlraw = "SELECT tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount,
						tvpiutangdebet.NamaAccount, tvpiutangdebet.KeteranganGL AS MemoGL, (tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar.SumKreditGL,0)) As Bayar,
						tvpiutangdebet.DebetGL, IFNULL(tvpiutangbayar.SumKreditGL,0) AS PiutangLunas, 0 AS PiutangSisa
						FROM tvpiutangdebet
						LEFT OUTER JOIN tvpiutangbayar ON tvpiutangdebet.GLLink = tvpiutangbayar.HPLink AND tvpiutangdebet.NoAccount = tvpiutangbayar.NoAccount
						WHERE tvpiutangdebet.NoAccount = ('{$MyNoAccountHP}') AND (DebetGL - IFNULL(tvpiutangbayar.SumKreditGL,0)) > 0 {$whereRaw}";
                    } else {
                        General::cCmd("TRUNCATE tvhutangbayar ; INSERT INTO tvhutangbayar SELECT * FROM vhutangbayar ;")->execute();
                        $sqlraw = "SELECT tvhutangkredit.GLLink, tvhutangkredit.NoGL, tvhutangkredit.TglGL, tvhutangkredit.NoAccount,
						tvhutangkredit.NamaAccount, tvhutangkredit.KeteranganGL AS MemoGL, (tvhutangkredit.KreditGL - IFNULL(tvhutangbayar.SumDebetGL,0)) As Bayar, 
						tvhutangkredit.KreditGL, IFNULL(tvhutangbayar.SumDebetGL,0) As HutangLunas,  0 AS HutangSisa
						FROM tvhutangkredit
						LEFT OUTER JOIN tvhutangbayar ON tvhutangkredit.GLLink = tvhutangbayar.HPLink AND tvhutangkredit.NoAccount = tvhutangbayar.NoAccount
						WHERE tvhutangkredit.NoAccount = ('{$MyNoAccountHP}') AND (KreditGL - IFNULL(tvhutangbayar.SumDebetGL,0)) > 0 {$whereRaw}";
                    }
                    break;
                case 'ttgeneralledgerhd/show-hutang-piutang':
                    $colGrid    = Ttgeneralledgerhd::colGridShowHutangPiutang();
                    $queryRawPK = [ 'GLLink' ];
                    $NoGL       = $urlParams[ 'NoGL' ];
                    $sqlraw     = "SELECT 
					  ttglhpit.NoAccount, traccount.NamaAccount, ttglhpit.GLLink, ttglhpit.HPNilai, ttgeneralledgerhd.NoGL, ttgeneralledgerhd.TglGL, ttgeneralledgerhd.MemoGL 
					FROM
					  ttglhpit 
					  INNER JOIN traccount 
					    ON ttglhpit.NoAccount = traccount.NoAccount 
					  INNER JOIN ttgeneralledgerhd 
					    ON ttglhpit.GLLink = ttgeneralledgerhd.GLLink 
					WHERE ttglhpit.NoGL = '{$NoGL}'
					  AND ttgeneralledgerhd.MemoGL NOT LIKE 'Terima Stok Intransit%' 
					  AND ttgeneralledgerhd.KodeTrans <> 'JA' 
					UNION
					SELECT 
					  ttglhpit.NoAccount, traccount.NamaAccount, ttglhpit.GLLink, ttglhpit.HPNilai, ttgeneralledgerhd.NoGL, ttgeneralledgerhd.TglGL, ttgeneralledgerhd.MemoGL 
					FROM
					  ttglhpit 
					  INNER JOIN traccount 
					    ON ttglhpit.NoAccount = traccount.NoAccount 
					  INNER JOIN ttgeneralledgerhd 
					    ON ttglhpit.GLLink = ttgeneralledgerhd.GLLink 
					  INNER JOIN ttgeneralledgerit 
					    ON ttgeneralledgerhd.NoGL = ttgeneralledgerit.NoGL 
					    AND ttgeneralledgerit.NoAccount = ttglhpit.NoAccount 
					WHERE ttglhpit.NoGL = '{$NoGL}' 
					  AND ttgeneralledgerhd.MemoGL NOT LIKE 'Terima Stok Intransit%' 
					  AND ttgeneralledgerhd.KodeTrans = 'JA' 
					GROUP BY ttglhpit.GLLink";
                    break;
                default:
            }
        }
        return [
            'td' => [
                'class'      => TdAction::class,
                'model'      => Ttgeneralledgerhd::class,
                'where'      => $where,
                'queryRaw'   => $sqlraw,
                'queryRawPK' => $queryRawPK,
                'colGrid'    => $colGrid,
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => [ 'POST' ],
                ],
            ],
        ];
    }
    public function actionSearchTrans() {
        $this->layout = 'select-mode';
        return $this->render( 'search-trans', [
            'title'   => 'search-trans',
            'arr'     => [
                'cmbTxt'    => [
                    "DKNo"          => "No Data Konsumen",
                    "CusNama"       => "Nama Konsumen",
                    "MotorNoMesin"  => "No Mesin",
                    "MotorNoRangka" => "No Rangka",
                    "MotorType"     => "Type Motor",
                    "CusKode"       => "Kode Konsumen",
                    "SalesKode"     => "Kode Sales",
                    "LeaseKode"     => "Kode Lease",
                    "DKMemo"        => "Keterangan",
                    "DKJenis"       => "Jenis",
                    "DKLunas"       => "Lunas",
                    "INNo"          => "No Inden",
                    "NamaHadiah"    => "Nama Hadiah",
                    "ProgramNama"   => "Nama Program",
                    "UserID"        => "User",
                ],
                'cmbTxt2'   => [
                    "Bayar BBN"       => "Bayar BBN",
                    "Kredit"          => "Uang Muka Diterima Kredit",
                    "Piutang UM"      => "Uang Muka Piutang Konsumen",
                    "Retur Harga"     => "Retur Harga",
                    "SubsidiDealer2"  => "Subsidi Dealer 2",
                    "Insentif Sales"  => "Insentif Sales",
                    "Potongan Khusus" => "Potongan Khusus",
                    "Harga OTR"       => "Harga OTR",
                    "PrgSubsSupplier" => "Subsidi Supplier",
                    "PrgSubsDealer"   => "Net Subsidi Dealer 1",
                    "PrgSubsFincoy"   => "Subsidi Fincoy",
                ],
                'cmbTgl'    => [
                    "DKTgl" => "Tgl Data Kons"
                ],
                'cmbNum'    => [
                    "DKHarga"         => "Harga OTR",
                    "DKHPP"           => "HPP",
                    "DKDPTotal"       => "DP Total",
                    "DKDPLLeasing"    => "DP Leasing",
                    "DKDPTerima"      => "DP Terima",
                    "DKNetto"         => "Netto",
                    "ProgramSubsidi"  => "Subsidi Program",
                    "PrgSubsSupplier" => "Subsidi Supplier",
                    "PrgSubsDealer"   => "Subsidi Dealer",
                    "PrgSubsFincoy"   => "Subsidi Fincoy",
                    "PotonganHarga"   => "Potongan Harga",
                    "ReturHarga"      => "Retur Harga",
                    "PotonganKhusus"  => "Potongan Khusus",
                    "BBN"             => "BBN",
                    "Jaket"           => "Jaket",
                ],
                'sortname'  => "",
                'sortorder' => ''
            ],
            'options' => [
                'url'     => Url::toRoute( [ 'ttgeneralledgerhd/td' ] ),
                'colGrid' => Ttgeneralledgerhd::colGridSearchTrans(),
                'mode'    => self::MODE_SELECT,
            ],
        ] );
    }
    public function actionSearchTransAfter($NoGL,$TglGL) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $post[ 'NoGL' ]   = $NoGL;
        $post[ 'TglGL' ]   = $TglGL;
        $post[ 'GLLink' ]   = $_POST['data']['id'];
        $post[ 'MemoGL' ]   = $_POST['jenis'];
        $post[ 'Action' ] = 'LoopJAAfter';
        $result            = Ttgeneralledgerhd::find()->callSP( $post );
        if ( $result[ 'status' ] == 1 ) {
            Yii::$app->session->addFlash('warning', $result[ 'keterangan' ] );
        }
        return $this->responseSuccess( $result[ 'keterangan' ] );
    }
    public function actionSelectMmHutang() {
        $this->layout = 'select-mode';
        return $this->render( 'select', [
            'title'   => 'Hutang',
            'arr'     => [
                'cmbTxt'    => [
                ],
                'cmbTgl'    => [
                ],
                'cmbNum'    => [
                ],
                'sortname'  => "",
                'sortorder' => ''
            ],
            'options' => [
                'colGrid' => Ttgeneralledgerhd::colGridSelectMMHutang(),
                'mode'    => self::MODE_SELECT,
            ],
        ] );
    }
    public function actionShowHutangPiutang() {
        $this->layout = 'select-mode';
        return $this->render( 'select', [
            'title'   => 'Pesan',
            'arr'     => [
                'cmbTxt'     => [
                ],
                'cmbTgl'     => [
                ],
                'cmbNum'     => [
                ],
                'sortname'   => "",
                'sortorder'  => '',
                'hideFilter' => true
            ],
            'options' => [
                'colGrid' => Ttgeneralledgerhd::colGridShowHutangPiutang(),
                'mode'    => self::MODE_SELECT,
            ]
        ] );
    }
    public function actionLinkPiutangHutang() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $respon                      = [];
        switch ( $_POST[ 'oper' ] ) {
            case 'del':
                $msg = General::cCmd( "SELECT * FROM ttglhpit WHERE NoGL = :NoGL", [ ':NoGL' => $_POST[ 'NoGL' ] ] )->queryAll();
                Ttglhpit::deleteAll( [ 'NoGL' => $_POST[ 'NoGL' ] ] );
                $base64                      = urldecode( base64_decode( $_POST[ 'header' ] ) );
                parse_str( $base64, $header );
                $header[ 'Action' ]          = 'Loop';
                $result                      = Ttgeneralledgerhd::find()->callSP( $header );
                $respon = [
                    'success' => true,
                    'msg'     => $msg
                ];
                break;
            case 'add':
                try {
                    Yii::$app->db->transaction( function () {
                        foreach ( $_POST[ 'data' ] as $item ) {
                            $ttglit = new Ttglhpit();
                            $ttglit->load( $item, '' );
                            if ( ! $ttglit->save() ) {
                                throw new Exception( 'Can\'t be saved ttglhpit. Errors: ' . join( ', ', $ttglit->getFirstErrors() ) );
                            }
                        }
                    } );
                    $base64                      = urldecode( base64_decode( $_POST[ 'header' ] ) );
                    parse_str( $base64, $header );
                    $header[ 'Action' ]          = 'LoopAfter';
                    $result                      = Ttgeneralledgerhd::find()->callSP( $header );
                    $respon = [
                        'success' => true,
                        'msg'     => ' Berhasil;'
                    ];
                } catch ( \Throwable $e ) {
                    $respon = [
                        'success' => false,
                        'msg'     => $e->getMessage()
                    ];
                }
                break;
        }
        return $respon;
    }
    /**
     * Lists all Ttgeneralledgerhd models.
     * @return mixed
     */
    public function actionIndex() {
        if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
            $post[ 'Action' ] = 'Display';
            $result           = Ttgeneralledgerhd::find()->callSP( $post );
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        }
        return $this->render( 'index', [
            'mode'    => '',
            'url_add' => Url::toRoute( [ 'ttgeneralledgerhd/td' ] )
        ] );
    }
    /**
     * Lists all Ttgeneralledgerhd models.
     * @return mixed
     */
    public function actionMemorial() {
        if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
            $post[ 'Action' ] = 'Display';
            $result           = Ttgeneralledgerhd::find()->callSP( $post );
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        }
        return $this->render( 'memorial', [
            'mode'    => '',
            'url_add' => Url::toRoute( [ 'ttgeneralledgerhd/td' ] )
        ] );
    }
    /**
     * @return string
     */
    public function actionMemorialCreate() {
        return $this->create( 'GL', 'memorial' );
    }
    /**
     * @param $id
     * @param $action
     *
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionMemorialUpdate( $id, $action ) {
        return $this->update( $id, $action, 'memorial' );
    }
    /**
     * @param $id
     * @param $action
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionMemorialCancel( $id, $action ) {
        return $this->cancel( $id, $action, 'memorial' );
    }
    /**
     * Lists all Ttgeneralledgerhd models.
     * @return mixed
     */
    public function actionTransaksi() {
        if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
            $post[ 'Action' ] = 'Display';
            $result           = Ttgeneralledgerhd::find()->callSP( $post );
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        }
        return $this->render( 'transaksi', [
            'mode'    => '',
            'url_add' => Url::toRoute( [ 'ttgeneralledgerhd/td' ] )
        ] );
    }
    /**
     * @return string
     */
    public function actionTransaksiCreate() {
        return $this->create( 'JT', 'transaksi' );
    }
    /**
     * @return string
     */
    public function actionValidasiJurnal() {
        if ( isset( $_POST[ 'mode' ] ) ) {
            Ttgeneralledgerhd::updateAll( [
                'GLValid' => $_POST[ 'mode' ]
            ], "TglGL BETWEEN :dtpTgl1 AND :dtpTgl2", [
                ':dtpTgl1' => $_POST[ 'dtpTgl1' ],
                ':dtpTgl2' => $_POST[ 'dtpTgl2' ],
            ] );
        }
        $last = Ttgeneralledgerhd::find()->GetLastValidasi();
        return $this->render( 'validasi-jurnal', [
            'last' => $last
        ] );
    }
    /**
     * @param $id
     * @param $action
     *
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionTransaksiUpdate( $id, $action ) {
        return $this->update( $id, $action, 'transaksi' );
    }
    /**
     * @param $id
     * @param $action
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionTransaksiCancel( $id, $action ) {
        return $this->cancel( $id, $action, 'transaksi' );
    }
    /**
     * Lists all Ttgeneralledgerhd models.
     * @return mixed
     */
    public function actionAdjustment() {
        if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
            $post[ 'Action' ] = 'Display';
            $result           = Ttgeneralledgerhd::find()->callSP( $post );
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        }
        return $this->render( 'adjustment', [
            'mode'    => '',
            'url_add' => Url::toRoute( [ 'ttgeneralledgerhd/td' ] )
        ] );
    }
    /**
     * @return string
     */
    public function actionAdjustmentCreate() {
        return $this->create( 'JA', 'adjustment' );
    }
    /**
     * @param $id
     * @param $action
     *
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionAdjustmentUpdate( $id, $action ) {
        return $this->update( $id, $action, 'adjustment' );
    }
    /**
     * @param $id
     * @param $action
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionAdjustmentCancel( $id, $action ) {
        return $this->cancel( $id, $action, 'adjustment' );
    }
    /**
     * Lists all Ttgeneralledgerhd models.
     * @return mixed
     */
    public function actionPindahbuku() {
        if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
            $post[ 'Action' ] = 'Display';
            $result           = Ttgeneralledgerhd::find()->callSP( $post );
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        }
        return $this->render( 'pindahbuku', [
            'mode'    => '',
            'url_add' => Url::toRoute( [ 'ttgeneralledgerhd/td' ] )
        ] );
    }
    /**
     * @return string
     */
    public function actionPindahbukuCreate() {
        return $this->create( 'JP', 'pindahbuku' );
    }
    /**
     * @param $id
     * @param $action
     *
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionPindahbukuUpdate( $id, $action ) {
        return $this->update( $id, $action, 'pindahbuku' );
    }
    /**
     * @param $id
     * @param $action
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionPindahbukuCancel( $id, $action ) {
        return $this->cancel( $id, $action, 'pindahbuku' );
    }
    /**
     * Displays a single Ttgeneralledgerhd model.
     *
     * @param string $NoGL
     * @param string $TglGL
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView( $NoGL, $TglGL ) {
        return $this->render( 'view', [
            'model' => $this->findModel( $NoGL, $TglGL ),
        ] );
    }
    /**
     * Deletes an existing Ttgeneralledgerhd model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param string $NoGL
     * @param string $TglGL
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionMemorialDelete( $NoGL, $TglGL ) {
        $this->findModel( $NoGL, $TglGL )->delete();
        return $this->redirect( [ 'index' ] );
    }
    public function actionDelete() {
        /** @var Ttgeneralledgerhd $model */
        $request = Yii::$app->request;
        $view = $_GET['type'];
        if ($request->isAjax){
            $model = $this->findModelBase64('Ttgeneralledgerhd', $_POST['id']);
            $_POST['NoGL'] = $model->NoGL;
            $_POST['Action'] = 'Delete';
            $result = Ttgeneralledgerhd::find()->callSP($_POST);
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            $id = $request->get('id');
            $model = $this->findModelBase64('Ttgeneralledgerhd', $id);
            $_POST['NoGL'] = $model->NoGL;
            $_POST['Action'] = 'Delete';
            $result = Ttgeneralledgerhd::find()->callSP($_POST);
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['NoGL'] == '--') {
                return $this->redirect(['index/'.$view]);
            } else {
                $model = Ttgeneralledgerhd::findOne($result['NoGL']);
                return $this->redirect(['ttgeneralledgerhd/'.$view.'-update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
            }
        }
    }
    /**
     * @param $kodeTrans
     * @param $view
     *
     * @return string
     */
    private function create( $kodeTrans, $view ) {
        $model            = new Ttgeneralledgerhd();
        $model->NoGL      = Ttgeneralledgerhd::generateNoGl( $kodeTrans, true );
        $model->TglGL     = date( "Y-m-d" );
        $model->KodeTrans = $kodeTrans;
        $model->GLValid   = 'Belum';
        $model->MemoGL    = '--';
        $model->save();
        $dsJurnal             = Ttgeneralledgerhd::find()->FillByNo()->where( [ 'ttgeneralledgerhd.NoGL' => $model->NoGL ] )->asArray( true )->one();
        $dsJurnal[ 'Action' ] = 'Insert';
        $result               = Ttgeneralledgerhd::find()->callSP( $dsJurnal );
        Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        $id                     = $this->generateIdBase64FromModel( $model );
        $dsJurnal[ 'NoGLView' ] = $result[ 'NoGL' ];
        return $this->render( $view . '-create', [
            'model'    => $model,
            'dsJurnal' => $dsJurnal,
            'id'       => $id,
            'view'     => $view
        ] );
    }
    /**
     * @param $id
     * @param $action
     * @param $view
     *
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    private function update( $id, $action, $view ) {
        $index = \yii\helpers\Url::toRoute( [ 'ttgeneralledgerhd/' . $view ] );
        if ( ! isset( $_GET[ 'action' ] ) ) {
            return $this->redirect( $index );
        }
        $action = $_GET[ 'action' ];
        switch ( $action ) {
            case 'create':
            case 'update':
                TUi::$actionMode = Menu::UPDATE;
                break;
            case 'view':
                TUi::$actionMode = Menu::VIEW;
                break;
            case 'display':
                TUi::$actionMode = Menu::DISPLAY;
                break;
        }
        /** @var Ttgeneralledgerhd $model */
        $model    = $this->findModelBase64( 'Ttgeneralledgerhd', $id );
        $dsJurnal = Ttgeneralledgerhd::find()->FillByNo()->where( [ 'ttgeneralledgerhd.NoGL' => $model->NoGL ] )
            ->asArray( true )->one();
        if ( Yii::$app->request->isPost ) {
            $post             = array_merge( $dsJurnal, \Yii::$app->request->post() );
            $post[ 'Action' ] = 'Update';
            $result           = Ttgeneralledgerhd::find()->callSP( $post );
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            $model = Ttgeneralledgerhd::findOne( $result[ 'NoGL' ] );
            $id    = $this->generateIdBase64FromModel( $model );
            if ( $result[ 'status' ] == 0 ) {
                $post[ 'NoGLView' ] = $result[ 'NoGL' ];
                return $this->redirect( [ 'ttgeneralledgerhd/' . $view . '-update', 'id' => $id, 'action' => 'display' ] );
            } else {
                return $this->render( $view . '-update', [
                    'model'    => $model,
                    'dsJurnal' => $post,
                    'id'       => $id,
                    'view'     => $view
                ] );
            }
        }
        if ( ! isset( $_GET[ 'oper' ] ) ) {
            $dsJurnal[ 'Action' ] = 'Load';
            $result               = Ttgeneralledgerhd::find()->callSP( $dsJurnal );
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        } else {
            $result[ 'NoGL' ] = $dsJurnal[ 'NoGL' ];
        }
        $dsJurnal[ 'NoGLView' ] = $_GET[ 'NoGLView' ] ?? $result[ 'NoGL' ];
        return $this->render( $view . '-update', [
            'model'    => $model,
            'dsJurnal' => $dsJurnal,
            'id'       => $id,
            'view'     => $view
        ] );
//        $model                  = $this->findModelBase64( 'Ttgeneralledgerhd', $id );
//        $dsJurnal               = Ttgeneralledgerhd::find()->FillByNo()->where( [ 'ttgeneralledgerhd.NoGL' => $model->NoGL ] )
//            ->asArray( true )->one();
//        $dsJurnal[ 'Action' ]   = 'Load';
//        $result                 = Ttgeneralledgerhd::find()->callSP($dsJurnal);
//        Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
//        $post  = \Yii::$app->request->post();
//        if ( $action === 'create' ) {
//            $noGl = Ttgeneralledgerhd::generateNoGl( $model->KodeTrans );
//            /* data detail : update NoGL  */
//            $modelDetails = Ttgeneralledgerit::findAll( [
//                'NoGL'  => $model->NoGL,
//                'TglGL' => $model->TglGL
//            ] );
//            foreach ( $modelDetails as $modelDetail ) {
//                $modelDetail->NoGL = $noGl;
//                $modelDetail->save();
//            }
//            /* data header : update NoGl */
//            $post['Ttgeneralledgerhd']['NoGL'] = $noGl;
//            /* Type 'GL', then GLLink refers to it's self */
//            if ( $model->KodeTrans = 'GL' ) {
//                $post['Ttgeneralledgerhd']['GLLink'] = $noGl;
//            }
//        }
//        if ( array_key_exists( 'Ttgeneralledgerhd', $post ) ) {
//            $post['Ttgeneralledgerhd']['GLValid'] = (int) $post['Ttgeneralledgerhd']['GLValid'] === 0 ? 'Belum' : 'Sudah';
//        }
//        Yii::$app->request->setBodyParams( $post );
//        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
//            return $this->redirect( [ $view ] );
//        }
//        return $this->render( $view . '-update', [
//            'model' => $model,
//            'id'    => $id,
//            'dsJurnal' => $dsJurnal,
//        ] );
    }
    /**
     * @param $id
     * @param $action
     * @param $view
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    private function cancel( $id, $action, $view ) {
        /** @var Ttgeneralledgerhd $model */
        $model = $this->findModelBase64( 'Ttgeneralledgerhd', $id );
        $post               = \Yii::$app->request->post();
        $post[ 'Action' ] = 'Cancel';
        $result           = Ttgeneralledgerhd::find()->callSP( $post );
        Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        if($result[ 'NoGL' ] == '--'){
            return $this->redirect([ 'ttgeneralledgerhd/' . $view ]);
        }else{
            $model = Ttgeneralledgerhd::findOne($result[ 'NoGL' ]);
            return $this->redirect( [ 'ttgeneralledgerhd/' . $view . '-update',  'action' => 'display','id' => $this->generateIdBase64FromModel( $model ) ] );
        }
    }
    /**
     * Finds the Ttgeneralledgerhd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param string $NoGL
     * @param string $TglGL
     *
     * @return Ttgeneralledgerhd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel( $NoGL, $TglGL ) {
        if ( ( $model = Ttgeneralledgerhd::findOne( [ 'NoGL' => $NoGL, 'TglGL' => $TglGL ] ) ) !== null ) {
            return $model;
        }
        throw new NotFoundHttpException( 'The requested page does not exist.' );
    }

    public function actionPrint( $id ) {
        unset( $_POST[ '_csrf-app' ] );
        $_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
        $_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
        $_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
        $client            = new Client( [
            'headers' => [ 'Content-Type' => 'application/octet-stream' ]
        ] );
        $response      = $client->post( Yii::$app->params['H2'][$_POST['db']]['host_report'] . '/abengkel/fgeneralledgerhd', [
            'body' => Json::encode( $_POST )
        ] );
        return Yii::$app->response->sendContentAsFile( $response->getBody(), 'fgeneralledgerhd.pdf', [
            'inline'   => true,
            'mimeType' => 'application/pdf'
        ] );
    }
}
