<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\models\Tzcompany;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TzcompanyController implements the CRUD actions for Tzcompany model.
 */
class TzcompanyController extends AbengkelController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Tzcompany models.
	 * @return mixed
	 */
//	public function actionIndex() {
//		
//
//		return $this->render( 'index', [
//			'searchModel'  => $searchModel,
//			'dataProvider' => $dataProvider,
//		] );
//	}
	/**
	 * Displays a single Tzcompany model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
//	public function actionView( $id ) {
//		return $this->render( 'view', [
//			'model' => $this->findModel( $id ),
//		] );
//	}
	/**
	 * Creates a new Tzcompany model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
//	public function actionCreate() {
//		$model = new Tzcompany();
//		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
//			return $this->redirect( [ 'view', 'id' => $model->PerusahaanNama ] );
//		}
//		return $this->render( 'create', [
//			'model' => $model,
//		] );
//	}
	/**
	 * Updates an existing Tzcompany model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate() {
		$model = $this->findModel();
		if ( $model->load( Yii::$app->request->post() ) && $model->save( false ) ) {
			return $this->redirect( [ 'update' ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Tzcompany model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
//	public function actionDelete( $id ) {
//		$this->findModel( $id )->delete();
//		return $this->redirect( [ 'index' ] );
//	}
	/**
	 * Finds the Tzcompany model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Tzcompany the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel() {
		if ( ( $model = Tzcompany::find()->one() ) !== null ) {
			return $model;
		}
		return new Tzcompany;
	}
}
