<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\components\TUi;
use abengkel\models\Tdlokasi;
use abengkel\models\Tmotor;
use abengkel\models\Trmp;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TrmpController implements the CRUD actions for Trmp model.
 */
class TrmpController extends AbengkelController {

    public function actions() {
        return [
            'td' => [
                'class' => TdAction::class,
                'model'    => Trmp::class,
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


	/**
	 * Lists all Trmp models.
	 * @return mixed
	 */
	public function actionIndex() {
        if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
            $post[ 'Action' ] = 'Display';
            $result           = Trmp::find()->callSP( $post );
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        }
        return $this->render( 'index' );
	}
	/**
	 * Displays a single Trmp model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Finds the Trmp model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Trmp the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Trmp::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Trmp model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
        TUi::$actionMode   = Menu::ADD;
        $model              = new Trmp();
        $model->MPNo        = General::createTemporaryNo( "MP", 'Trmp', 'MPNo' );
        $model->MPTgl       = date( 'Y-m-d' );
        $model->MPMemo = '--';
        $model->MPSaldoAwal = 0;
        $model->MPUmur = 0;
        $model->MPAwal = 0;
        $model->MPNext = 0;
        $model->MPCOADebet = '0';
        $model->MPCOAKredit = '0';
        $model->MPTglMulai = date( 'Y-m-d' );
        $model->MPSaldoNow = 0;
        $model->MPStatus = '--';
        $model->UserID      = $this->UserID();
        $model->save();
        $post               = $model->attributes;
        $post[ 'Action' ]   = 'Insert';
        $result             = Trmp::find()->callSP( $post );
        $postload               = $result;
        $postload[ 'Action' ]   = 'Load';
        $load             = Trmp::find()->callSP( $postload);
        Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        $dsMp = Trmp::find()->rmFillByNo()->where( [ 'trmp.MPNo' => $model->MPNo ] )->asArray( true )->one();
        $dsMp[ 'MPNoView' ] = $load[ 'MPNo' ];
        $id                  = $this->generateIdBase64FromModel( $model );
        return $this->render( 'create', [
            'model'  => $model,
            'dsMp' => $dsMp,
            'id'     => $id
        ] );

	}
	/**
	 * Updates an existing Trmp model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
        $index = \yii\helpers\Url::toRoute( [ 'trmp/index' ] );
        if ( ! isset( $_GET[ 'action' ] ) ) {
            return $this->redirect( $index );
        }
        /** @var Trmp $model */
        $model    = $this->findModelBase64( 'Trmp', $id );
		$dsMp = Trmp::find()->rmFillByNo()->where( [ 'trmp.MPNo' => $model->MPNo ] )
		                   ->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post             = array_merge( $dsMp, \Yii::$app->request->post() );
			$post[ 'MPTgl' ]  = date( "Y-m-d" );
			$post[ 'UserID' ] = $this->UserID();
			$post[ 'Action' ] = 'Update';
			$result           = Trmp::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model = Trmp::findOne( $result[ 'MPNo' ] );
			$id    = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 ) {
				$post[ 'MPNoView' ] = $result[ 'MPNo' ];
				return $this->redirect( [ 'trmp/update', 'action' => 'display', 'id' => $id ] );
			} else {
				return $this->render( 'update', [
					'model'     => $model,
					'dsMp' => $post,
					'id'        => $id,
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
            $dsMp[ 'Action' ] = 'Load';
			$result                = Trmp::find()->callSP( $dsMp );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
        $dsMp[ 'MPNoView' ] = $_GET[ 'MPNoView' ] ?? $result[ 'MPNo' ];
		return $this->render( 'update', [
			'model'     => $model,
			'dsMp' => $dsMp,
			'id'        => $id
		] );
	}
	/**
	 * Deletes an existing Trmp model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */




	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST         = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client        = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response      = $client->post( Yii::$app->params['H2'][$_POST['db']]['host_report'] . '/abengkel/frmp', [
			'body' => Json::encode( $_POST )
		] );
		return $response->getBody();
	}

	public function actionCancel( $id ) {
		/** @var Trmp $model */
		$_POST[ 'Action' ] = 'Cancel';
		$result            = Trmp::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'MPNo' ] == '--' ) {
			return $this->redirect( [ 'index' ] );
		} else {
			$model = Trmp::findOne( $result[ 'MPNo' ] );
			return $this->redirect( [ 'trmp/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}

    public function actionDelete()
    {
        /** @var Trmp $model */
        $request = Yii::$app->request;
        if ($request->isAjax){
            $model = $this->findModelBase64('Trmp', $_POST['id']);
            $_POST['MPNo'] = $model->MPNo;
            $_POST['Action'] = 'Delete';
            $result = Trmp::find()->callSP($_POST);
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            $id = $request->get('id');
            $model = $this->findModelBase64('Trmp', $id);
            $_POST['MPNo'] = $model->MPNo;
            $_POST['Action'] = 'Delete';
            $result = Trmp::find()->callSP($_POST);
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['MPNo'] == '--') {
                return $this->redirect(['index']);
            } else {
                $model = Trmp::findOne($result['MPNo']);
                return $this->redirect(['trmp/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
            }
        }
    }

    public function actionDetail() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $requestData                 = \Yii::$app->request->post();
        $primaryKeys                 = Trmp::getTableSchema()->primaryKey;
        if ( isset( $requestData[ 'oper' ] ) ) {
            /* operation : create, edit or delete */
            $oper = $requestData[ 'oper' ];
            /** @var Trmp $model */
            $model = null;
            if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
                $model = $this->findModelBase64( 'trmp', $requestData[ 'id' ] );
            }
            switch ( $oper ) {
                case 'add'  :
                case 'edit' :
                    if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
                        $requestData[ 'Action' ]  = 'Insert';
                        $requestData[ 'MPAutoN' ] = 0;
                    } else {
                        $requestData[ 'Action' ] = 'Update';
                        if ( $model != null ) {
                            $requestData[ 'MPNoOLD' ]   = $model->MPNoOLD;
                            $requestData[ 'MPAutoNOLD' ] = $model->MPAutoNOLD;
                            $requestData[ 'NoAccountOLD' ] = $model->NoAccountOLD;
                        }
                    }
                    $result = Trmp::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
                    break;
                case 'batch' :
                    break;
                case 'del'  :
                    $requestData             = array_merge( $requestData, $model->attributes );
                    $requestData[ 'Action' ] = 'Delete';
                    if ( $model != null ) {
                        $requestData[ 'MPNoOLD' ]   = $model->MPNoOLD;
                        $requestData[ 'MPAutoNOLD' ] = $model->MPAutoNOLD;
                        $requestData[ 'NoAccountOLD' ] = $model->NoAccountOLD;
                    }
                    $result = Trmp::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
                    break;
            }
        } else {
            for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
                $primaryKeys[ $i ] = 'Trmp.' . $primaryKeys[ $i ];
            }
            $query     = ( new \yii\db\Query() )
                ->select( new Expression(
                    'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
							    ttgeneralledgerhd.NoGL, ttgeneralledgerhd.TglGL, ttgeneralledgerit.NoAccount, traccount.NamaAccount, ttgeneralledgerit.DebetGL, ttgeneralledgerit.KreditGL'
                ) )
                ->from( 'ttgeneralledgerhd' )
                ->join( 'INNER JOIN', 'ttgeneralledgerit', 'ttgeneralledgerhd.NoGL = ttgeneralledgerit.NoGL' )
                ->join( 'INNER JOIN', 'traccount', 'ttgeneralledgerit.NoAccount = traccount.NoAccount' )
                ->join( 'INNER JOIN', 'trmp', 'trmp.MPNo = LEFT(ttgeneralledgerhd.GLLink, 8)' )
                ->where( [ 'trmp.MPNo' => $requestData[ 'MPNo' ] ] );
            $rowsCount = $query->count();
            $rows      = $query->all();
            /* encode row id */
            for ( $i = 0; $i < count( $rows ); $i ++ ) {
                $rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
            }
            $response = [
                'rows'      => $rows,
                'rowsCount' => $rowsCount
            ];
        }
        return $response;
    }


    public function actionHitungMp()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $saldoawal = $_POST['MPSaldoAwal'];
        $umur = $_POST['MPUmur'];
        $angsuran = $saldoawal / $umur;
        $MPNext = $angsuran;
        $MPAwal = $saldoawal - ( $angsuran * ($umur - 1));
        $res =['MPNext' => $MPNext,'MPAwal' => $MPAwal];
        return $res;
    }

}
