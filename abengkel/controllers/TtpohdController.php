<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\components\TUi;
use abengkel\models\Tdbaranglokasi;
use abengkel\models\Tdlokasi;
use abengkel\models\Tdsupplier;
use abengkel\models\Ttpohd;
use abengkel\models\Ttpoit;
use abengkel\models\Ttsrhd;
use common\components\Api;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtpohdController implements the CRUD actions for Ttpohd model.
 */
class TtpohdController extends AbengkelController {
	public function actions() {
		$colGrid    = null;
		$sqlraw     = null;
        $where      = [
            [
                'op'        => 'AND',
                'condition' => "(PONo NOT LIKE '%=%')",
                'params'    => []
            ]
        ];
		$queryRawPK = null;
		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			switch ( $r ) {
				case 'ttpohd/select':
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$sqlraw     = "SELECT ttpohd.*, IFNULL(tdsupplier.SupNama, '--') AS SupNama, 
       								IFNULL(ttPShd.PSTgl, DATE('1900-01-01')) AS PSTgl
								FROM ttpohd INNER JOIN ttpoit ON ttpohd.PONo = ttpoit.PONo
								  LEFT OUTER JOIN ttPShd ON ttpohd.PSNo = ttPShd.PSNo
								  LEFT OUTER JOIN tdsupplier ON ttpohd.SupKode = tdsupplier.SupKode
								WHERE ttpoit.POQty > ttpoit.PSQtys AND ttpohd.PONo NOT LIKE '%=%'
								 {$whereRaw}
                              GROUP BY ttPOhd.PONo ORDER BY ttPOhd.PONo";
					$queryRawPK = [ 'PONo' ];
					$colGrid    = Ttpohd::colGridPick();
					break;
			}
		}
		return [
			'td' => [
				'class'      => \abengkel\components\TdAction::class,
				'model'      => Ttpohd::class,
				'queryRaw'   => $sqlraw,
				'queryRawPK' => $queryRawPK,
                'where'      => $where,
                'colGrid'    => $colGrid,
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionSelect() {
		$urlDetail    = Url::toRoute( [ 'ttpoit/index', 'jenis' => 'FillItemPOPS' ] );
		$this->layout = 'select-mode';
		return $this->render( 'select',
			[
				'arr'       => [
					'cmbTxt'    => [
						'PONo'         => 'No PO',
						'SupKode'      => 'Kode Supplier',
						'LokasiKode'   => 'Lokasi',
						'PSKeterangan' => 'Keterangan',
						'KodeTrans'    => 'Kode Trans',
						'NoGL'         => 'No JT',
						'UserID'       => 'User ID',
					],
					'cmbTgl'    => [
						'POTgl' => 'Tgl PO',
					],
					'cmbNum'    => [
						'POTotal' => 'Total',
					],
					'sortname'  => "POTgl",
					'sortorder' => 'desc'
				],
				'title'     => 'Daftar Purchase Order / Order Pembelian',
				'options'   => [ 'colGrid' => Ttpohd::colGridPick(), 'mode' => self::MODE_SELECT_DETAIL_MULTISELECT ],
				'urlDetail' => $urlDetail
			] );
	}
	/**
	 * Lists all Ttpohd models.
	 * @return mixed
	 */
	public function actionIndex() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttpohd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'index', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttpohd/td' ] )
		] );
	}
	/**
	 * Creates a new Ttpohd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$LokasiKodeCmb       = Tdlokasi::find()->where( [ 'LokasiStatus' => 'A', 'LEFT(LokasiNomor,1)' => '7' ] )->orderBy( 'LokasiStatus, LokasiKode' )->one();
		$KarKodeCmb          = Tdsupplier::find()->where( "Supstatus = 'A' OR Supstatus = 1" )->orderBy( 'Supstatus, SupKode' )->one();
		$model               = new Ttpohd();
		$model->PONo         = General::createTemporaryNo( 'PO', 'Ttpohd', 'PONo' );
		$model->POTgl        = date( 'Y-m-d H:i:s' );
		$model->SupKode      = ( $KarKodeCmb != null ) ? $KarKodeCmb->SupKode : '--';
		$model->POKeterangan = "--";
		$model->LokasiKode   = ( $LokasiKodeCmb != null ) ? $LokasiKodeCmb->LokasiKode : '--';
		$model->POSubTotal   = 0;
		$model->PODiscFinal  = 0;
		$model->POTotalPajak = 0;
		$model->POBiayaKirim = 0;
		$model->POTotal      = 0;
		$model->PSNo         = "--";
		$model->SONo         = "--";
		$model->PosKode      = $this->LokasiKode();
		$model->Cetak        = "Belum";
		$model->KodeTrans    = "TC12";
		$model->UserID       = $this->UserID();
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttpohd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTBeli               = Ttpohd::find()->ttpohdFillByNo()
		                               ->where( [ 'ttpohd.PONo' => $model->PONo ] )->asArray( true )->one();
		$dsTBeli[ 'NoGL' ]     = '--';
		$dsTBeli[ 'SOTgl' ]    = date( 'Y-m-d H:i:s' );
		$dsTBeli[ 'PSTgl' ]    = date( 'Y-m-d H:i:s' );
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTBeli[ 'PONoView' ] = $result[ 'PONo' ];
		return $this->render( 'create', [
			'model'   => $model,
			'dsTBeli' => $dsTBeli,
			'id'      => $id
		] );
	}
	/**
	 * Updates an existing Ttpohd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$index = \yii\helpers\Url::toRoute( [ 'ttpohd/index' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$model   = $this->findModelBase64( 'Ttpohd', $id );
		$dsTBeli = Ttpohd::find()->ttpohdFillByNo()->where( [ 'ttpohd.PONo' => $model->PONo ] )
		                 ->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post             = array_merge( $dsTBeli, \Yii::$app->request->post() );
			$post[ 'POTgl' ]  = $post[ 'POTgl' ] . ' ' . $post[ 'POJam' ];
			$post[ 'TglAcc' ]  = $post[ 'TglAcc' ] . ' ' . $post[ 'AccJam' ];
			$post[ 'TglRelease' ]  = $post[ 'TglRelease' ] . ' ' . $post[ 'ReleaseJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttpohd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttpohd::findOne( $result[ 'PONo' ] );
            if ( $result[ 'PONo' ] == '--' || $model == null) {
                if ( $model === null ) {
                    Yii::$app->session->addFlash( 'warning', 'PONo : ' . $result[ 'PONo' ]. ' tidak ditemukan.' );
                }
                return $this->redirect( $index);
            }
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'PONoView' ] = $result[ 'PONo' ];
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				return $this->redirect( [ 'ttpohd/update', 'action' => 'display', 'id' => $id ] );
			} else {
				return $this->render( 'update', [
					'model'   => $model,
					'dsTBeli' => $post,
					'id'      => $id,
				] );
			}
		}
		$dsTBeli[ 'NoGL' ]   = '--';
		$dsTBeli[ 'Action' ] = 'Load';
		$result              = Ttpohd::find()->callSP( $dsTBeli );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTBeli[ 'PONoView' ] = $_GET[ 'PONoView' ] ?? $result[ 'PONo' ];
		return $this->render( 'update', [
			'model'   => $model,
			'dsTBeli' => $dsTBeli,
			'id'      => $id
		] );
	}
	/**
	 * Deletes an existing Ttpohd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
//	public function actionDelete() {
//		/** @var Ttpohd $model */
//		$model             = $this->findModelBase64( 'Ttpohd', $_POST[ 'id' ] );
//		$_POST[ 'PONo' ]   = $model->PONo;
//		$_POST[ 'Action' ] = 'Delete';
//		$result            = Ttpohd::find()->callSP( $_POST );
//		if ( $result[ 'status' ] == 0 ) {
//			return $this->responseSuccess( $result[ 'keterangan' ] );
//		} else {
//			return $this->responseFailed( $result[ 'keterangan' ] );
//		}
//	}

    public function actionDelete()
    {
        /** @var Ttpohd $model */
        $request = Yii::$app->request;
        if ($request->isAjax){
            $model = $this->findModelBase64('Ttpohd', $_POST['id']);
            $_POST['PONo'] = $model->PONo;
            $_POST['Action'] = 'Delete';
            $result = Ttpohd::find()->callSP($_POST);
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            $id = $request->get('id');
            $model = $this->findModelBase64('Ttpohd', $id);
            $_POST['PONo'] = $model->PONo;
            $_POST['Action'] = 'Delete';
            $result = Ttpohd::find()->callSP($_POST);
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['PONo'] == '--') {
                return $this->redirect(['index']);
            } else {
                $model = Ttpohd::findOne($result['PONo']);
                return $this->redirect(['ttpohd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
            }
        }
    }

	public function actionCancel( $id ) {
		/** @var Ttpohd $model */
		$model             = $this->findModelBase64( 'Ttpohd', $id );
		$_POST[ 'PONo' ]   = $model->PONo;
		$_POST[ 'POJam' ]  = $_POST[ 'POTgl' ] . ' ' . $_POST[ 'POJam' ];
		$_POST[ 'Action' ] = 'Cancel';
		$result            = Ttpohd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'PONo' ] == '--' ) {
			return $this->redirect( [ 'index' ] );
		} else {
			$model = Ttpohd::findOne( $result[ 'PONo' ] );
			return $this->redirect( [ 'ttpohd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client            = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response          = $client->post( Yii::$app->params['H2'][$_POST['db']]['host_report'] . '/abengkel/fpohd', [
			'body' => Json::encode( $_POST )
		] );

		return Yii::$app->response->sendContentAsFile( $response->getBody(), $_POST['PONo'].'.pdf', [
			'inline'   => true,
			'mimeType' => 'application/pdf'
		]);

        // $html                 = $response->getBody();
        // return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	/**
	 * Finds the Ttpohd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttpohd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttpohd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}


    public function actionImport()
    {
        $this->layout = 'select-mode';
        return $this->render('import', [
            'colGrid' => Ttpohd::colGridImportItems(),
            'mode' => self::MODE_SELECT_DETAIL_MULTISELECT,
        ]);
    }

    public function actionImportItems()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $fromTime = Yii::$app->formatter->asDate('-90 day', 'yyyy-MM-dd');
        $toTime = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
		$addParam = [];
        if (isset($_POST['query'])) {
            $query = Json::decode($_POST['query'], true);
            $fromTime = $query['tgl1'] ?? $fromTime;
            $toTime = $query['tgl2'] ?? $toTime;
			if($query['txt1'] !== ''){
				$addParam = array_merge_recursive($addParam,[$query['cmbTxt1']=> $query['txt1']]);
			}
        }
        $api = new Api();
        $mainDealer = $api->getMainDealer();
        $arrHasil = [];
        switch ($mainDealer) {
            case API::MD_HSO:
                $arrParams = [
                    'fromTime' => $fromTime . ' 00:00:00',
                    'toTime' => $toTime . ' 23:59:59',
                    'dealerId' => $api->getDealerId(),
                ];

                break;
            case API::MD_ANPER:
                $arrParams = [
                    'fromTime' => $fromTime . ' 00:00:00',
                    'toTime' => $toTime . ' 23:59:59',
                    'dealerId' => $api->getDealerId(),
                    'requestServerTime' => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                    'requestServertimezone' => date('T'),
                    'requestServerTimestamp' => $api->getCurrUnixTime(),
                ];
                break;
			default:
				$arrParams = [
					'fromTime'               => $fromTime . ' 00:00:00',
					'toTime'                 => $toTime . ' 23:59:59',
					'dealerId'               => $api->getDealerId(),
					'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
					'requestServertimezone'  => date('T'),
					'requestServerTimestamp' => $api->getCurrUnixTime(),
				];
        }
		$arrParams = array_merge_recursive($arrParams,$addParam);
		$filterResult = [];
		if($query['txt2'] !== ''){
			$filterResult = [
				'key' => $query['cmbTxt2'],
				'value' => $query['txt2'],
			];
		}
        $hasil = $api->call(Api::TYPE_PRSL, Json::encode($arrParams),$filterResult);
        $arrHasil = Json::decode($hasil);
        if (!isset($arrHasil['data'])) {
            return [
                'rows' => [],
                'rowsCount' => 0
            ];
        }
        $_SESSION['ttpohd']['import'] = $arrHasil['data'];
        return [
            'rows' => $arrHasil['data'],
            'rowsCount' => sizeof($arrHasil['data'])
        ];
    }

    public function actionImportDetails()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $key = array_search($_POST['noSO'], array_column($_SESSION['ttpohd']['import'], 'noSO'));
        return [
            'rows' => $_SESSION['ttpohd']['import'][$key]['parts'],
            'rowsCount' => sizeof($_SESSION['ttpohd']['import'][$key]['parts'])
        ];
    }

	public function actionImportLoop(){
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$respon = [];
		$base64 = urldecode(base64_decode($requestData['header']));
		parse_str($base64, $header);
		$requestData['data'][0]['header']['data']['Action'] = 'ImporBefore';
		$requestData['data'][0]['header']['data']['NoTrans'] = $header['PONo'];
		$result = Api::callPRSL($requestData['data'][0]['header']['data']);
		// $isSameID = $result['SSNo'] == $header['SSNo'];
		Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		$model = Ttpohd::findOne($result['NoTrans']);
		$id = $this->generateIdBase64FromModel($model);
		$respon['id'] = $id;
		foreach ($requestData['data'] as $item) {
			$item['data']['Action'] = 'Impor';
			$item['data']['NoTrans'] = $model->PONo;
			$result = Api::callPRSLit($item['data']);
			Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		}
		$requestData['data'][0]['header']['data']['Action'] = 'ImporAfter';
		$result = Api::callPRSL($requestData['data'][0]['header']['data']);
		return $respon;
	}
}
