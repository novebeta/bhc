<?php

namespace abengkel\controllers;

use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\components\TdAction;
use abengkel\components\TUi;
use abengkel\models\Tdcustomer;
use common\components\General;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * TdcustomerController implements the CRUD actions for Tdcustomer model.
 */
class TdcustomerController extends AbengkelController
{

    public function beforeAction($action)
    {
        $GLOBALS['MenuText'] = 'Konsumen';
        switch ($action->id) {
            case 'create':
                $this->isAllow = Menu::akses($GLOBALS['MenuText'], Menu::ADD);
                break;
            case 'update':
                $this->isAllow = Menu::akses($GLOBALS['MenuText'], Menu::UPDATE);
                break;
            case 'delete':
                $this->isAllow = Menu::akses($GLOBALS['MenuText'], Menu::DELETE);
                break;
            case 'index':
                $this->isAllow = Menu::akses($GLOBALS['MenuText'], Menu::DISPLAY);
                break;
            case 'td':
            case 'cancel':
                $this->isAllow = 1;
                break;
        }
//		if($this->isAllow == 0) throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        return true;
    }

    public function actions()
    {
        return [
            'td' => [
                'class' => TdAction::class,
                'model' => Tdcustomer::class,
                'joinWith' => [
                    'ahass' => [
                        'type' => 'INNER JOIN'
                    ]
                ]
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tdcustomer models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSelect()
    {
        $this->layout = 'select-mode';
        return $this->render('index', [
            'mode' => self::MODE_SELECT
        ]);
    }

    /**
     * Finds the Tdcustomer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param string $id
     *
     * @return Tdcustomer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tdcustomer::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new Tdcustomer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        TUi::$actionMode = Menu::ADD;
        $model = new Tdcustomer();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['tdcustomer/update', 'id' => $this->generateIdBase64FromModel($model), 'action' => 'display']);
        } else {
            Yii::$app->session->addFlash('warning', Html::errorSummary($model));
        }
        return $this->render('create', [
            'model' => $model,
            'id' => 'new'
        ]);
    }

    /**
     * Updates an existing Tdcustomer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param string $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $action = $_GET['action'];
        switch ($action) {
            case 'create':
            case 'update':
                TUi::$actionMode = Menu::UPDATE;
                break;
            case 'view':
                TUi::$actionMode = Menu::VIEW;
                break;
            case 'display':
                TUi::$actionMode = Menu::DISPLAY;
                break;
        }
        $model = $this->findModelBase64('Tdcustomer', $id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['tdcustomer/update', 'id' => $this->generateIdBase64FromModel($model), 'action' => 'display']);
        } else {
            Yii::$app->session->addFlash('warning', Html::errorSummary($model));
        }
        return $this->render('update', [
            'model' => $model,
            'id' => $this->generateIdBase64FromModel($model),
        ]);
    }
    /**
     * Deletes an existing Tdcustomer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param string $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
//	public function actionDelete( $id ) {
//		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//		$model                       = $model = $this->findModelBase64( 'Tdcustomer', $id );
//		if ( $model != null ) {
//			$result[ 'status' ] = $model->delete();
//		}
//		if ( $result[ 'status' ] !== false ) {
//			return $this->responseSuccess( 'Konsumen berhasil dihapus' );
//		} else {
//			return $this->responseFailed( 'Konsumen gagal dihapus' );
//		}
//	}

    public function actionDelete()
    {
        /** @var Tdcustomer $model */
        $request = Yii::$app->request;
        if ($request->isAjax) {
            $model = $this->findModelBase64('Tdcustomer', $_POST['id']);
            if ($model != null) {
                $result['status'] = $model->delete();
            }
            if ($result['status'] !== false) {
                $model = Tdcustomer::find()->one();
                return $this->redirect(['tdcustomer/update', 'id' => $this->generateIdBase64FromModel($model), 'action' => 'display']);
            } else {
                return $this->responseFailed('Konsumen gagal dihapus');
            }
        }
    }


    public function actionFindCombo()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return Tdcustomer::find()->combo($_POST);
    }

    public function actionFindById($id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return Tdcustomer::find()
            ->where('MotorNoPolisi = :MotorNoPolisi', [':MotorNoPolisi' => $id])
            ->asArray()->one();
    }

    public function actionCancel($id, $action)
    {
        $model = Tdcustomer::findOne($id);
        if ($model == null) {
            $model = Tdcustomer::find()->one();
            if ($model == null) {
                return $this->redirect(['tdcustomer/index']);
            }
        }
        return $this->redirect(['tdcustomer/update', 'id' => $this->generateIdBase64FromModel($model), 'action' => 'display']);
    }

    public function actionEdit($id)
    {
        $model = $this->findModelBase64('Tdcustomer', $id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($this->isPopupForm()) {
                $data = Tdcustomer::find()
                    ->where('MotorNoPolisi = :MotorNoPolisi', [':MotorNoPolisi' => $model->MotorNoPolisi])
                    ->asArray()->one();
                return $this->responseToPopupForm($data);
            } else {
                return $this->redirect(['tdcustomer/update', 'id' => $this->generateIdBase64FromModel, 'action' => 'display']);
            }
        }
        if ($this->isPopupForm()) {
            $this->setLayoutForPopupForm();
        }
        return $this->render('edit', [
            'model' => $model,
            'id' => $this->generateIdBase64FromModel($model),
        ]);
    }

    public function actionLoadCustomer()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $query = General::cCmd("Select tdcustomer.*, tdahass.AHASSNama  FROM tdcustomer 
    	LEFT JOIN tdahass ON tdahass.AHASSNomor = tdcustomer.AHASSNomor WHERE MotorNoPolisi = :MotorNoPolisi", [
            ':MotorNoPolisi' => $_POST['Tdcustomer']['MotorNoPolisi']
        ])->queryOne();
        return $query;
    }

    public function actionSaveCustomer()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = Tdcustomer::findOne($_POST['Tdcustomer']['MotorNoPolisi']);
        $msg = 'Berhasil update konsumen';
        if ($_POST['oper'] == 'Add' && $model == null) {
            $model = new Tdcustomer;
            $msg = 'Berhasil tambah konsumen';
        }
        $model->load($_POST);
        if (!$model->save()) {
            return [
                'status' => false,
                'msg' => Html::errorSummary($model)
            ];
        }
        return [
            'status' => true,
            'msg' => $msg
        ];
    }
}
