<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\models\Ttucit;
use common\components\Custom;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtucitController implements the CRUD actions for Ttucit model.
 */
class TtucitController extends AbengkelController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttucit::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
            /** @var Ttucit $model */
            $model = null;
            if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
                $model = $this->findModelBase64( 'ttucit', $requestData[ 'id' ] );
            }
            switch ( $oper ) {
                case 'add'  :
                case 'edit' :
                    if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
                        $requestData[ 'Action' ] = 'Insert';
                        $requestData[ 'UCAuto' ] = 0;
                    } else {
                        $requestData[ 'Action' ] = 'Update';
                        if ( $model != null ) {
                            $requestData[ 'UCNoOLD' ]   = $model->UCNo;
                            $requestData[ 'UCAutoOLD' ] = $model->UCAuto;
                            $requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
                        }
                    }
                    $result = Ttucit::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
                    break;
                case 'batch' :
                    break;
                case 'del'  :
                    $requestData             = array_merge( $requestData, $model->attributes );
                    $requestData[ 'Action' ] = 'Delete';
                    if ( $model != null ) {
                        $requestData[ 'UCNoLD' ]   = $model->UCNo;
                        $requestData[ 'UCAutoOLD' ] = $model->UCAuto;
                        $requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
                    }
                    $result = Ttucit::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
                    break;
            }
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'ttucit.' . $primaryKeys[ $i ];
			}
			$query     = ( new \yii\db\Query() )
				->select( new Expression(
					'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
								ttucit.UCNo, ttucit.UCAuto, ttucit.BrgKode, ttucit.UCQty, ttucit.UCHrgBeliLama, ttucit.UCHrgBeliBaru, ttucit.UCHrgJualLama, ttucit.UCHrgJualBaru, tdbarang.BrgNama, tdbarang.BrgSatuan, 
                         ttucit.UCQty * ttucit.UCHrgBeliLama AS JumHargaBeliLama, ttucit.UCQty * ttucit.UCHrgBeliBaru AS JumHargaBeliBaru, ttucit.UCQty * ttucit.UCHrgBeliBaru - ttucit.UCQty * ttucit.UCHrgBeliLama AS SelisihHargaBeli, 
                         ttucit.UCQty * ttucit.UCHrgJualLama AS JumHargaJualLama, ttucit.UCQty * ttucit.UCHrgJualBaru AS JumHargaJualBaru, ttucit.UCQty * ttucit.UCHrgJualBaru - ttucit.UCQty * ttucit.UCHrgJualLama AS SelisihHargaJual, 
                         tdbarang.BrgGroup'
				) )
				->from( 'ttucit' )
				->join( 'INNER JOIN', 'tdbarang', 'ttucit.BrgKode = tdbarang.BrgKode' )
				->where( "(ttucit.UCNo = :UCNo)", [ ':UCNo' => $requestData[ 'UCNo' ] ] );
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	protected function findModel( $UCNo, $UCAuto ) {
		if ( ( $model = Ttucit::findOne( [ 'UCNo' => $UCNo, 'UCAuto' => $UCAuto ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
