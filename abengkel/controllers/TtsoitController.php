<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\models\Ttsoit;
use Yii;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtsoitController implements the CRUD actions for Ttsoit model.
 */
class TtsoitController extends AbengkelController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttsoit models.
	 * @return mixed
	 */
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttsoit::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
            /** @var Ttsoit $model */
            $model = null;
            if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
                $model = $this->findModelBase64( 'ttsoit', $requestData[ 'id' ] );
            }
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
                if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'Action' ] = 'Insert';
                        $requestData[ 'SOAuto' ] = 0;
					} else {
						$requestData[ 'Action' ] = 'Update';
                        if ( $model != null ) {
                                $requestData[ 'SONoOLD' ]   = $model->SONo;
                                $requestData[ 'SOAutoOLD' ] = $model->SOAuto;
                                $requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
                            }
					}
					$result = Ttsoit::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
					break;
                case 'batch' :
                    break;
				case 'del'  :
                    $requestData             = array_merge( $requestData, $model->attributes );
                    $requestData[ 'Action' ] = 'Delete';
                    if ( $model != null ) {
                        $requestData[ 'SONoLD' ]   = $model->SONo;
                        $requestData[ 'SOAutoOLD' ] = $model->SOAuto;
                        $requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
                    }
					$result = Ttsoit::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
					break;
			}
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'Ttsoit.' . $primaryKeys[ $i ];
			}

            $jenis = $_GET['jenis'] ?? ' ';
            switch ( $jenis ) {
                case 'FillItemSOPO':
                    $query = (new \yii\db\Query())
                        ->select(new Expression(
                            'CONCAT(' . implode(',"||",', $primaryKeys) . ') AS id,
								ttsoit.SONo, ttsoit.SOAuto, ttsoit.BrgKode, (ttsoit.SOQty - ttsoit.POQtys )AS SOQty, ttsoit.SOHrgBeli, ttsoit.SOHrgJual, ttsoit.SODiscount, ttsoit.SOPajak, tdbarang.BrgNama, tdbarang.BrgSatuan, 0 AS Disc, 
                                ttsoit.SOQty * ttsoit.SOHrgJual - ttsoit.SODiscount AS Jumlah, tdbarang.BrgGroup, ttsohd.LokasiKode, ttsoit.PONo'
                        ))
                        ->from('ttsoit')
                        ->join('INNER JOIN', 'tdbarang', 'ttsoit.BrgKode = tdbarang.BrgKode')
                        ->join('INNER JOIN', 'ttsohd', 'ttsohd.SONo = ttsoit.SONo')
                        ->where(['ttsoit.SONo' => $requestData['SONo']])
                        ->orderBy('ttsoit.SONo, ttsoit.SOAuto');
                    break;
                default:
                    $query = (new \yii\db\Query())
                        ->select(new Expression(
                            'CONCAT(' . implode(',"||",', $primaryKeys) . ') AS id,
								ttsoit.SONo, ttsoit.SOAuto, ttsoit.BrgKode, ttsoit.SOQty, ttsoit.SOHrgBeli, ttsoit.SOHrgJual, ttsoit.SODiscount, ttsoit.SOPajak, tdbarang.BrgNama, tdbarang.BrgSatuan, IFNULL(ttsoit.SODiscount / (ttsoit.SOHrgJual * ttsoit.SOQty) 
                         * 100, 0) AS Disc, ttsoit.SOQty * ttsoit.SOHrgJual - ttsoit.SODiscount AS Jumlah, tdbarang.BrgGroup, ttsoit.SINo, ttsoit.PONo'
                        ))
                        ->from('Ttsoit')
                        ->join('INNER JOIN', 'tdbarang', 'ttsoit.BrgKode = tdbarang.BrgKode')
                        ->where(['ttsoit.SONo' => $requestData['SONo']])
                        ->orderBy('ttsoit.SONo, ttsoit.SOAuto');
            }

			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	/**
	 * Displays a single Ttsoit model.
	 *
	 * @param string $SONo
	 * @param integer $SOAuto
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $SONo, $SOAuto ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $SONo, $SOAuto ),
		] );
	}
	/**
	 * Finds the Ttsoit model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $SONo
	 * @param integer $SOAuto
	 *
	 * @return Ttsoit the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $SONo, $SOAuto ) {
		if ( ( $model = Ttsoit::findOne( [ 'SONo' => $SONo, 'SOAuto' => $SOAuto ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttsoit model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Ttsoit();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'SONo' => $model->SONo, 'SOAuto' => $model->SOAuto ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Ttsoit model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $SONo
	 * @param integer $SOAuto
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $SONo, $SOAuto ) {
		$model = $this->findModel( $SONo, $SOAuto );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'SONo' => $model->SONo, 'SOAuto' => $model->SOAuto ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Ttsoit model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $SONo
	 * @param integer $SOAuto
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $SONo, $SOAuto ) {
		$this->findModel( $SONo, $SOAuto )->delete();
		return $this->redirect( [ 'index' ] );
	}
}
