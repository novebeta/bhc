<?php
namespace abengkel\controllers;
use abengkel\components\AbengkelController;
use abengkel\models\Ttbmhd;
use abengkel\models\Ttbmit;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtbmitController implements the CRUD actions for Ttbmit model.
 */
class TtbmitController extends AbengkelController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttbmit models.
	 * @return mixed
	 */
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttbmit::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
			/** @var Ttbmit $model */
			$model = null;
			if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
				$model = $this->findModelBase64( 'ttbmit', $requestData[ 'id' ] );
			}
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'Action' ] = 'Insert';
						$requestData[ 'BMLink' ] = '--';
					} else {
						$requestData[ 'Action' ] = 'Update';
						if ( $model != null ) {
							$requestData[ 'BMNoOLD' ]   = $model->BMNo;
							$requestData[ 'BMLinkOLD' ] = $model->BMLink;
						}
					}
					$result = Ttbmit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'batch' :
					\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
					$respon                      = $this->responseFailed( 'Add Item' );
					$base64                      = urldecode( base64_decode( $requestData[ 'header' ] ) );
					parse_str( $base64, $header );
//					$header                      = $requestData[ 'header' ];
					$header[ 'Action' ]          = 'Loop';
					$result                      = Ttbmhd::find()->callSP( $header );
					foreach ( $requestData[ 'data' ] as $item ) {
						$items[ 'Action' ]  = 'Insert';
						$items[ 'BMNo' ]    = $header[ 'BMNo' ];
						$items[ 'BMLink' ]  = $item[ 'data' ][ 'NO' ];
						$items[ 'BMBayar' ] = $item[ 'data' ][ 'Sisa' ];
						$result             = Ttbmit::find()->callSP( $items );
					}
					$header[ 'Action' ] = 'LoopAfter';
					$result             = Ttbmhd::find()->callSP( $header );
					return $this->responseSuccess( $requestData );
					break;
				case 'del'  :
					$requestData             = array_merge( $requestData, $model->attributes );
					$requestData[ 'Action' ] = 'Delete';
					if ( $model != null ) {
						$requestData[ 'BMNoOLD' ]   = $model->BMNo;
						$requestData[ 'BMLinkOLD' ] = $model->BMLink;
					}
					$result = Ttbmit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
			}
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'Ttbmit.' . $primaryKeys[ $i ];
			}
			/** @var Ttbmhd $model */
			$model = Ttbmhd::find()->where( [ 'BMNo' => $requestData[ 'BMNo' ] ] )->one();
			$query = new Query();
			switch ( $model->KodeTrans ) {
				case 'SO' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,BMNo, BMLink, BMBayar,SOTgl AS TglLink, MotorNoPolisi AS BiayaNama,
			        CusNama, SOTotal AS Total, BMPaid.Paid - BMBayar AS Terbayar,  SOTotal - BMPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(SELECT ttBMit.*, ttSOhd.*, tdcustomer.CusNama  FROM ttBMit INNER JOIN ttSOhd ON BMLink = SONo INNER JOIN tdcustomer 
			              ON ttSOhd.MotorNoPolisi = tdcustomer.MotorNoPolisi WHERE ttBMit.BMNo = :BMNo) ttBMit" ) )
					      ->addParams( [ ':BMNo' => $model->BMNo ] )
					      ->innerJoin( "(SELECT KBMLINK, IFNULL(SUM(KBMBayar), 0) AS Paid FROM vtkBM  GROUP BY KBMLINK) BMPaid",
						      "ttBMit.BMLink = BMPaid.KBMLINK" );
					break;
				case 'SV' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,BMNo, BMLink, BMBayar,SDTgl AS TglLink, MotorNoPolisi AS BiayaNama,
			        CusNama, SDTotalBiaya AS Total, BMPaid.Paid - BMBayar AS Terbayar,  SDTotalBiaya - BMPaid.Paid AS Sisa ' ) )
					      ->from( new Expression( "(SELECT ttBMit.*, ttSDhd.*, tdcustomer.CusNama  FROM ttBMit INNER JOIN ttSDhd ON BMLink = SVNo INNER JOIN tdcustomer 
			              ON ttSDhd.MotorNoPolisi = tdcustomer.MotorNoPolisi WHERE ttBMit.BMNo = :BMNo) ttBMit" ) )
					      ->addParams( [ ':BMNo' => $model->BMNo ] )
					      ->innerJoin( "(SELECT KBMLINK, IFNULL(SUM(KBMBayar), 0) AS Paid FROM vtkBM  GROUP BY KBMLINK) BMPaid",
						      "ttBMit.BMLink = BMPaid.KBMLINK" );
					break;
				case 'SI' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,BMNo, BMLink, BMBayar,SITgl AS TglLink, MotorNoPolisi AS BiayaNama,
			        CusNama, SITotal AS Total, BMPaid.Paid - BMBayar AS Terbayar,  SITotal - BMPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(SELECT ttBMit.*, ttsihd.*,tdcustomer.CusNama FROM ttBMit 
                     INNER JOIN ttsihd ON BMLink = SINo 
                     INNER JOIN tdcustomer ON ttsihd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                     WHERE ttBMit.BMNo = :BMNo) ttBMit" ) )
					      ->addParams( [ ':BMNo' => $model->BMNo ] )
					      ->innerJoin( "(SELECT KBMLINK, IFNULL(SUM(KBMBayar), 0) AS Paid FROM vtkBM  GROUP BY KBMLINK) BMPaid",
						      "ttBMit.BMLink = BMPaid.KBMLINK" );
					break;
				case 'PR' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,BMNo, BMLink, BMBayar,PRTgl AS TglLink, SupKode AS BiayaNama,
			        CusNama, PRTotal AS Total, BMPaid.Paid - BMBayar AS Terbayar,  PRTotal - BMPaid.Paid AS Sisa ' ) )
					      ->from( new Expression( "(SELECT ttBMit.*, ttprhd.* ,tdsupplier.SupNama As CusNama FROM ttBMit 
                     INNER JOIN ttprhd ON BMLink = PRNo 
                     INNER JOIN tdsupplier ON ttprhd.SupKode = tdsupplier.SupKode  
                     WHERE ttBMit.BMNo = :BMNo) ttBMit" ) )
					      ->addParams( [ ':BMNo' => $model->BMNo ] )
					      ->innerJoin( "(SELECT KBMLINK, IFNULL(SUM(KBMBayar), 0) AS Paid FROM vtkBM  GROUP BY KBMLINK) BMPaid",
						      "ttBMit.BMLink = BMPaid.KBMLINK" );
					break;
				case 'SE' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,BMNo, BMLink, BMBayar,SETgl AS TglLink, MotorNoPolisi AS BiayaNama,
                    CusNama,SETotalBiaya AS Total, BMPaid.Paid - BMBayar AS Terbayar,  SETotalBiaya - BMPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(SELECT ttBMit.*, ttSEhd.*, tdcustomer.CusNama FROM ttBMit 
                     INNER JOIN ttSEhd ON BMLink = SENo INNER JOIN tdcustomer ON ttSEhd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                     WHERE ttBMit.BMNo = :BMNo) ttBMit" ) )
					      ->addParams( [ ':BMNo' => $model->BMNo ] )
					      ->innerJoin( "(SELECT KBMLINK, IFNULL(SUM(KBMBayar), 0) AS Paid FROM vtkbm  GROUP BY KBMLINK) BMPaid",
						      "ttBMit.BMLink = BMPaid.KBMLINK" );
					break;
			}
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	/**
	 * Displays a single Ttbmit model.
	 *
	 * @param string $BMNo
	 * @param string $BMLink
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $BMNo, $BMLink ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $BMNo, $BMLink ),
		] );
	}
	/**
	 * Creates a new Ttbmit model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Ttbmit();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'BMNo' => $model->BMNo, 'BMLink' => $model->BMLink ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Ttbmit model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $BMNo
	 * @param string $BMLink
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $BMNo, $BMLink ) {
		$model = $this->findModel( $BMNo, $BMLink );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'BMNo' => $model->BMNo, 'BMLink' => $model->BMLink ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Ttbmit model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $BMNo
	 * @param string $BMLink
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $BMNo, $BMLink ) {
		$this->findModel( $BMNo, $BMLink )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Ttbmit model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $BMNo
	 * @param string $BMLink
	 *
	 * @return Ttbmit the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $BMNo, $BMLink ) {
		if ( ( $model = Ttbmit::findOne( [ 'BMNo' => $BMNo, 'BMLink' => $BMLink ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
