<?php

namespace abengkel\controllers;

use abengkel\components\AbengkelController;
use abengkel\components\Menu;
use abengkel\models\Tdbaranglokasi;
use common\components\Custom;
use common\components\General;
use common\models\LoginForm;
use DateTime;
use SplFileObject;
use Yii;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class SiteController extends AbengkelController
{
    const keyNav = [
        'ttsdhd/daftar-servis-update' => ['SDNo'],
//        'ttsdhd/update?SD' => ['SDNo'],
        'ttsdhd/invoice-servis-update' => ['SDNo'],
//        'ttsdhd/update?SV' => ['SVNo'],
        'ttsehd/update' => ['SENo'],
        'ttcchd/update' => ['CCNo'],
        'ttckhd/update' => ['CKNo'],
        'ttsohd/update' => ['SONo'],
        'ttsihd/update?penjualanInternal' => ['SINo'],
        'ttsihd/update?invoicePenjualan' => ['SINo'],
        'ttsihd/penjualan-internal-update?penjualanInternal' => ['SINo'],
        'ttsihd/invoice-penjualan-update?invoicePenjualan' => ['SINo'],
        'ttsrhd/update' => ['SRNo'],
        'ttpohd/update' => ['PONo'],
        'ttpihd/update' => ['PINo'],
        'ttpshd/update' => ['PSNo'],
        'ttprhd/update' => ['PRNo'],
        'tttahd/update' => ['TANo'],
        'tttihd/update' => ['TINo'],
        'ttkmhd/kas-masuk-inden-update' => ['KMNo'],
        'ttkmhd/kas-masuk-pelunasan-leasing-update' => ['KMNo'],
        'ttkmhd/kas-masuk-uang-muka-kredit-update' => ['KMNo'],
        'ttkmhd/kas-masuk-bayar-piutang-kon-um-update' => ['KMNo'],
        'ttkmhd/kas-masuk-uang-muka-tunai-update' => ['KMNo'],
        'ttkmhd/kas-masuk-bayar-sisa-piutang-update' => ['KMNo'],
        'ttkmhd/kas-masuk-bbn-acc-update' => ['KMNo'],
        'ttkmhd/kas-masuk-dari-bank-update' => ['KMNo'],
        'ttkmhd/kas-masuk-umum-update' => ['KMNo'],
        'ttkmhd/kas-masuk-dari-bengkel-update' => ['KMNo'],
        'ttkmhd/kas-masuk-angsuran-karyawan-update' => ['KMNo'],
        'ttkmhd/kas-masuk-pos-dari-dealer-update' => ['KMNo'],
        'ttkmhd/kas-masuk-dealer-dari-pos-update' => ['KMNo'],
        'ttbkhd/bank-keluar-ke-kas-update' => ['BKNo'],
        'ttbkhd/bank-keluar-umum-update' => ['BKNo'],
        'ttbkhd/bank-keluar-pembelian-update' => ['BKNo'],
        'ttbkhd/bank-keluar-order-pengerjaan-luar-update'=> ['BKNo'],
        'ttbkhd/bank-keluar-retur-penjualan-update'=> ['BKNo'],
        'ttbkhd/transfer-antar-bank-update'=> ['BKNo'],
        'ttbmhd/bank-masuk-dari-h1-update'=> ['BMNo'],
        'ttbmhd/bank-masuk-umum-update'=> ['BMNo'],
        'ttbmhd/bank-masuk-dari-kas-update'=> ['BMNo'],
        'ttbmhd/bank-masuk-service-invoice-update'=> ['BMNo'],
        'ttbmhd/bank-masuk-penjualan-update'=> ['BMNo'],
        'ttbmhd/bank-masuk-estimasi-update'=> ['BMNo'],
        'ttbmhd/bank-masuk-order-penjualan-update'=> ['BMNo'],
        'ttbmhd/bank-masuk-retur-pembelian-update'=> ['BMNo'],
        'ttbmhd/bank-masuk-klaim-kpb-update'=> ['BMNo'],
        'ttbmhd/bank-masuk-klaim-c2-update'=> ['BMNo'],
        'ttkkhd/kas-keluar-umum-update'=> ['KKNo'],
        'ttkkhd/kas-keluar-ke-bank-update'=> ['KKNo'],
        'ttkkhd/kas-keluar-ke-h1-update'=> ['KKNo'],
        'ttkkhd/kas-keluar-pembelian-update'=> ['KKNo'],
        'ttkkhd/kas-keluar-order-pengerjaan-luar-update'=> ['KKNo'],
        'ttkkhd/kas-keluar-retur-penjualan-update'=> ['KKNo'],
        'ttkkhd/transfer-antar-kas-update'=> ['KKNo'],
        'ttkmhd/kas-masuk-service-invoice-update'=> ['KMNo'],
        'ttgeneralledgerhd/memorial-update'=> ['NoGL'],
        'ttgeneralledgerhd/transaksi-update'=> ['NoGL'],
        'ttgeneralledgerhd/pindahbuku-update'=> ['NoGL'],
        'trmp/update'=> ['MPNo'],
    ];
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        $this->isAllow = 1;
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionSendMessage()
    {
        return $this->render('send-message');
    }

    public function actionPreview()
    {
        $data = json_decode($_POST['data'],true);
        $msg = strtr($_POST['pesan'], $data) . "\r\n\r\n" . $_SESSION['_company']['nama'];
        return $this->responseSuccess("No. WA : " .$data['CusTelepon'] . "<br>" . $msg);
    }

    public function actionSend()
    {
        $outlet64 = $_COOKIE['_outlet'];
        $outlet         = base64_decode($outlet64);
        $json = General::send($outlet,$_SESSION['_company']['nama'],$_POST['pesan'], $_POST['data'], $_FILES['file-0'] ?? null);
        if ($json['status']) {
            return $this->responseSuccess($json['message']);
        } else {
            return $this->responseFailed($json['message']);
        }
    }

    public function actionReportRealtime()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return General::reportRealtime($_POST['from'],$_POST['to']);
    }

    public function actionTampil()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = General::cCmd($_POST['query'])->queryAll();
        $header = sizeof($result) > 0 ? array_keys($result[0]) : [];
        $colModel = [];
        foreach ($header as $item) {
            $colModel[] = ['name' => $item];
        }
        return [
            'header' => $header,
            'colModel' => $colModel,
            'data' => $result
        ];
    }

    public function actionScan(){
        return $this->render('scan');
    }

    public function actionWaktu()
    {
        $this->isAllow = 1;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->formatter->locale = 'id-ID';
        $time1 = new DateTime('@' . (Yii::$app->session->get('_loginTime')));
        $time2 = new DateTime();
        $interval = $time1->diff($time2);
        return [
            'tgl' => Yii::$app->formatter->asDatetime('now', 'eeee, dd MMMM yyyy - HH:mm:ss'),
            'duration' => $interval->format('%H:%I:%S')
        ];
    }

    public function actionBackupRestore()
    {
        return $this->render('backup-restore');
    }

    public function actionBackup()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $infoDB = [
            'host' => Custom::getDsnAttribute('host', Yii::$app->getDb()->dsn),
            'dbname' => Custom::getDsnAttribute('dbname', Yii::$app->getDb()->dsn),
            'username' => Yii::$app->getDb()->username,
            'password' => Yii::$app->getDb()->password,
        ];
        $nama = Yii::getAlias('@webroot') . "/backup/" . $_SESSION['_company']['nama'] . " " . date('Ymd Hi') . ".dmp";
        $nama_url = Yii::getAlias('@web') . "/backup/" . $_SESSION['_company']['nama'] . " " . date('Ymd Hi') . ".dmp";
        $logFile = Yii::getAlias('@runtime') . "/" . $infoDB['dbname'] . ".log";
        if (file_exists($logFile)) {
            unlink($logFile);
        }
        session_write_close();
//		sleep( 5 );
        $output = shell_exec("mysqldump -h " . $infoDB['host'] . " -u" . $infoDB['username'] .
            " -p" . $infoDB['password'] . " --routines --databases " .
            $infoDB['dbname'] . " --verbose 2> " . $logFile . " > '" . $nama . "'");
        return [
            'status' => true,
            'url' => htmlentities($nama_url)
        ];
    }

    public function actionMonitorBackup()
    {
        $filename = Yii::getAlias('@runtime') . "/" . Custom::getDsnAttribute('dbname', Yii::$app->getDb()->dsn) . ".log";
        $filenametmp = Yii::getAlias('@runtime') . "/" . Custom::getDsnAttribute('dbname', Yii::$app->getDb()->dsn) . "_tmp.log";
        shell_exec('\cp "' . $filename . '" ' . ' "' . $filenametmp . '"');
//		$output   = shell_exec( 'cat ' . $filename );
//		$output   = file_get_contents($filename);
        if (!file_exists($filenametmp)) {
            return "==EOL==";
        }
        $file = new SplFileObject($filenametmp, "r");
        if (!$file->isFile()) {
            return "==EOL==";
        }
        $file->flock(LOCK_SH);
        $file->seek($_GET['line']);
        if ($file->valid()) {
            return $file->current();
        } else {
            return "==EOL==";
        }
    }

    public function actionMonitorRestore()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return \Yii::$app->db->createCommand("SHOW PROCESSLIST")->queryAll();
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if(Yii::$app->session->get( '_nav' ) == null || Yii::$app->session->get( '_company' ) == null){
            return $this->redirect( Custom::urllgn( 'site/dealer' ) );
        }
        return $this->render('index');
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionTestError()
    {
        $rArr = General::cCmd("SELECT REPLACE(MenuText,'&','') AS Id,
			                  KodeAkses,
			                  REPLACE(MenuInduk,'&','') AS MenuInduk,
			                  REPLACE(MenuText,'&','') AS MenuText,
			                  MenuNo,
			                  '0' AS MenuStatus,
			                  '0' AS MenuTambah,
			                  '0' AS MenuEdit,
			                  '0' AS MenuHapus,
			                  '0' AS MenuCetak,
			                  '0' AS MenuView FROM `tuakses` WHERE KodeAkses = 'Super' AND MenuText != '-'")->queryAll();
        return json_encode($rArr);
//		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//		throw new \yii\web\HttpException( 500, 'The requested Item could not be found.' );
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionCheckStockTransaksi()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $PosKode = Menu::getUserLokasi()['PosKode'];
        if (strtolower(substr($PosKode, 0, 3)) === 'pos') {
            $result = General::cCmd("SELECT IFNULL(tdbaranglokasi.LokasiKode, 'Kosong') AS LokasiKode, SaldoQty
				FROM tdbaranglokasi 
  				INNER JOIN tdlokasi ON tdbaranglokasi.LokasiKode = tdlokasi.LokasiKode 
				WHERE saldoqty > 0 AND brgkode = :BrgKode 
  				AND (LokasiInduk = :PosKode OR LokasiInduk = 'Lokal') 
				ORDER BY lokasinomor;", [':BrgKode' => $_POST['BrgKode'], ':PosKode' => $PosKode])
                ->queryOne();
        } else {
            $result = General::cCmd("SELECT IFNULL(tdbaranglokasi.LokasiKode, 'Kosong') AS LokasiKode, SaldoQty
				FROM tdbaranglokasi 
  				INNER JOIN tdlokasi ON tdbaranglokasi.LokasiKode = tdlokasi.LokasiKode 
				WHERE saldoqty > 0 AND brgkode = :BrgKode 
  				AND tdbaranglokasi.LokasiKode NOT LIKE 'POS%'
				AND tdbaranglokasi.LokasiKode NOT LIKE 'Penerimaan%'
				ORDER BY lokasinomor;", [':BrgKode' => $_POST['BrgKode']])
                ->queryOne();
        }
        if ($result === false) {
            return [
                'LokasiKode' => 'Kosong',
                'SaldoQty' => 0
            ];
        } else {
            return $result;
        }
    }

    public function actionCheckStock()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $res = Tdbaranglokasi::find()
            ->where('BrgKode = :BrgKode AND LokasiKode = :LokasiKode', ['BrgKode' => $_POST['BrgKode'], 'LokasiKode' => $_POST['LokasiKode']])
            ->asArray()->one();
        if ($res == null) {
            $res['LokasiKode'] = $_POST['LokasiKode'];
            $res['SaldoQtyReal'] = 0;
        }
        return $res;
    }

    public function actionPushInv2()
    {
        return $this->render('push-inv2');
    }
    public function actionDetails() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $comm      = General::cCmd("SELECT
                      ttsdhd.ZnoWorkOrder,
                      REPLACE(ttsdhd.ZnoWorkOrder, 'WO', 'NJB') AS noNJB,
                      ttsdhd.SDTgl AS tanggalNJB,
                      ttsdhd.SDTotalNonGratis AS totalHargaNJB,
                      REPLACE(ttsdhd.ZnoWorkOrder, 'WO', 'NJB') AS noNSC,
                      ttsdhd.SDTgl AS tanggalNSC,
                      ttsdhd.SDTotalNonGratis AS totalHargaNSC,
                      IFNULL(SA.ZhondaId,'--') AS HondaidSA,
                      IFNULL(Mekanik.ZhondaId,'--') AS HondaidMekanik
                    FROM ttsdhd
                    LEFT OUTER JOIN tdkaryawan Mekanik ON ttsdhd.KarKode = Mekanik.KarKode
                    LEFT OUTER JOIN tdkaryawan SA ON ttsdhd.KarKodeSA = SA.KarKode
                    WHERE ttsdhd.SVTgl BETWEEN '2021-01-01' AND '2021-12-31';");
        $rowsArray = $comm->queryAll();
        for ( $i = 0; $i < count( $rowsArray ); $i ++ ) {
            $rowsArray[ $i ][ 'ZnoWorkOrder' ] = $rowsArray[ $i ][ 'ZnoWorkOrder' ];
        }
        $response = [
            'rows'      => $rowsArray,
            'rowsCount' => sizeof( $rowsArray )
        ];
        return $response;

    }

    public function actionNavigation(){
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $post             = \Yii::$app->request->post();
        $_nav = Yii::$app->session['_navSearch'];
        if($post['oper'] == 'update'){
            $_nav['linkBack'] = $post['linkBack'];
            $_nav['linkNext'] = $post['linkNext'];
            $_nav['current'] = $post['current'];
        }else{
            $sqlraw           = "";
            $params           = [':nilai'=> '%'.$post['inSearch'].'%'];
            switch($post['url']){
                case 'ttsdhd/daftar-servis-update':
                $sqlraw = "SELECT ttsdhd.SDNo
                        FROM ttsdhd 
                        LEFT JOIN tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (ttsdhd.SDNo NOT LIKE '%=%')
                        ORDER BY SDTgl";
                    break;
                case 'ttsdhd/invoice-servis-update':
                    $sqlraw = "SELECT ttsdhd.SVNo
                        FROM ttsdhd 
                        LEFT JOIN tdcustomer ON ttsdhd.MotorNoPolisi = tdcustomer.MotorNoPolisi 
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (ttsdhd.SVNo NOT LIKE '%=%')
                        ORDER BY SVTgl";
                    break;
                case 'ttsehd/update':
                    $sqlraw = "SELECT ttsehd.SENo
                        FROM ttsehd
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (ttsehd.SENo NOT LIKE '%=%')
                        ORDER BY `SETgl`";
                    break;
                case 'ttckhd/update':
                    $sqlraw = "SELECT ttckhd.CKNo
                        FROM ttckhd
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (ttckhd.CKNo NOT LIKE '%=%')
                        ORDER BY 'CKTgl'";
                    break;
                case 'ttcchd/update':
                    $sqlraw = "SELECT ttcchd.CCNo
                        FROM ttcchd 
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (ttcchd.CCNo NOT LIKE '%=%')
                        ORDER BY 'CCTgl'";
                    break;
                case 'ttsohd/update':
                    $sqlraw = "SELECT ttsohd.SONo
                        FROM ttsohd 
                        INNER JOIN tdcustomer ON ttsohd.MotorNoPolisi = tdcustomer.MotorNoPolisi
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (ttsohd.SONo NOT LIKE '%=%')
                        ORDER BY 'SOTgl'";
                    break;
                case 'ttsihd/invoice-penjualan-update?invoicePenjualan':
                    $sqlraw = "SELECT ttsihd.SINo FROM ttsihd WHERE ({$post['cmbSearch']} LIKE :nilai) AND (ttsihd.SINo NOT LIKE '%=%')";
                    break;
                case 'ttsihd/update?invoicePenjualan':
                    $sqlraw = "SELECT ttsohd.SONo
                        FROM ttsihd 
                        INNER JOIN tdcustomer  ON ttsihd.MotorNoPolisi = tdcustomer.MotorNoPolisi
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (ttsihd.SINo NOT LIKE '%=%')
                        ORDER BY 'SITgl'";
                    break;
                case 'ttsihd/penjualan-internal-update?penjualanInternal':
                    $sqlraw = "SELECT ttsihd.SINo FROM ttsihd WHERE ({$post['cmbSearch']} LIKE :nilai) AND (ttsihd.SINo NOT LIKE '%=%')";
                    break;
                case 'ttsihd/update?penjualanInternal':
                    $sqlraw = "SELECT ttsohd.SONo
                        FROM ttsihd 
                        INNER JOIN tdcustomer  ON ttsihd.MotorNoPolisi = tdcustomer.MotorNoPolisi
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (ttsihd.SINo NOT LIKE '%=%')
                        ORDER BY 'SITgl'";
                    break;
                case 'ttsrhd/update':
                    $sqlraw = "SELECT ttsrhd.SRNo  
                        FROM ttsrhd 
                        INNER JOIN tdcustomer ON ttsrhd.MotorNoPolisi = tdcustomer.MotorNoPolisi
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (ttsrhd.SRNo NOT LIKE '%=%')
                        ORDER BY 'SRTgl'";
                    break;
                case 'ttpohd/update':
                    $sqlraw = "SELECT ttpohd.PONo  
                        FROM ttpohd 
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (ttpohd.PONo NOT LIKE '%=%')
                        ORDER BY 'POTgl'";
                    break;
                case 'ttpihd/update':
                    $sqlraw = "SELECT ttpihd.PINo  
                        FROM ttpihd 
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (ttpihd.PINo NOT LIKE '%=%')
                        ORDER BY 'PITgl'";
                    break;
                case 'ttpshd/update':
                    $sqlraw = "SELECT ttpshd.PSNo  
                        FROM ttpshd 
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (ttpshd.PSNo NOT LIKE '%=%')
                        ORDER BY 'PSTgl'";
                    break;
                case 'ttprhd/update':
                    $sqlraw = "SELECT ttprhd.PRNo  
                        FROM ttprhd 
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (ttprhd.PRNo NOT LIKE '%=%')
                        ORDER BY 'PRTgl'";
                    break;
                case 'tttihd/update':
                    $sqlraw = "SELECT tttihd.TINo  
                        FROM tttihd 
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (tttihd.TINo NOT LIKE '%=%')
                        ORDER BY 'TITgl'";
                    break;
                case 'tttahd/update':
                    $sqlraw = "SELECT tttahd.TANo  
                        FROM tttahd 
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (tttahd.TANo NOT LIKE '%=%')
                        ORDER BY 'TATgl'";
                    break;
                case 'ttkmhd/kas-masuk-inden-update':
                    $sqlraw = "SELECT ttkmhd.KMNo
                    FROM `ttkmhd` 
                    WHERE (ttkmhd.KMJenis LIKE 'Inden' AND ttkmhd.KMNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkmhd/kas-masuk-pelunasan-leasing-update':
                    $sqlraw = "SELECT ttkmhd.KMNo
                    FROM `ttkmhd` 
                    WHERE (ttkmhd.KMJenis LIKE 'Leasing' AND ttkmhd.KMNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkmhd/kas-masuk-uang-muka-kredit-update':
                    $sqlraw = "SELECT ttkmhd.KMNo
                    FROM `ttkmhd` 
                    WHERE (ttkmhd.KMJenis LIKE 'Kredit' AND ttkmhd.KMNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkmhd/kas-masuk-bayar-piutang-kon-um-update':
                    $sqlraw = "SELECT ttkmhd.KMNo
                    FROM `ttkmhd` 
                    WHERE (ttkmhd.KMJenis LIKE 'Piutang UM' AND ttkmhd.KMNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkmhd/kas-masuk-uang-muka-tunai-update':
                    $sqlraw = "SELECT ttkmhd.KMNo
                    FROM `ttkmhd` 
                    WHERE ((ttkmhd.KMJenis LIKE 'Tunai' AND ttkmhd.KMNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkmhd/kas-masuk-bayar-sisa-piutang-update':
                    $sqlraw = "SELECT ttkmhd.KMNo
                    FROM `ttkmhd` 
                    WHERE ((ttkmhd.KMJenis LIKE 'Piutang Sisa' AND ttkmhd.KMNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkmhd/kas-masuk-bbn-acc-update':
                    $sqlraw = "SELECT ttkmhd.KMNo
                    FROM `ttkmhd` 
                    WHERE (ttkmhd.KMJenis LIKE 'BBN Acc' AND ttkmhd.KMNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkmhd/kas-masuk-dari-bank-update':
                    $sqlraw = "SELECT ttkmhd.KMNo
                    FROM `ttkmhd` 
                    WHERE  ((ttkmhd.KMJenis LIKE 'KM Dari Bank' AND ttkmhd.KMNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkmhd/kas-masuk-umum-update':
                    $sqlraw = "SELECT ttkmhd.KMNo
                    FROM `ttkmhd` 
                    WHERE   ((ttkmhd.KMJenis LIKE 'Umum' AND ttkmhd.KMNo NOT LIKE '%=%'))  AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkmhd/kas-masuk-dari-bengkel-update':
                    $sqlraw = "SELECT ttkmhd.KMNo
                    FROM `ttkmhd` 
                    WHERE ((ttkmhd.KMJenis LIKE 'KM Dari Bengkel' AND ttkmhd.KMNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkmhd/kas-masuk-angsuran-karyawan-update':
                    $sqlraw = "SELECT ttkmhd.KMNo
                    FROM `ttkmhd` 
                    WHERE ((ttkmhd.KMJenis LIKE 'Angsuran Karyawan' AND ttkmhd.KMNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkmhd/kas-masuk-pos-dari-dealer-update':
                    $sqlraw = "SELECT ttkmhd.KMNo
                    FROM `ttkmhd` 
                    WHERE ((ttkmhd.KMJenis LIKE 'KM Pos Dari Dealer' AND ttkmhd.KMNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkmhd/kas-masuk-dealer-dari-pos-update':
                    $sqlraw = "SELECT ttkmhd.KMNo
                    FROM `ttkmhd` 
                    WHERE ((ttkmhd.KMJenis LIKE 'KM Dealer Dari Pos' AND ttkmhd.KMNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case stristr ($_nav['url'],'ttbkhd/'):
                        $sqlraw = "SELECT ttbkhd.BKNo FROM `ttbkhd` WHERE  ({$post['cmbSearch']} LIKE :nilai)";
                    break;
                case stristr ($_nav['url'],'ttbmhd/'):
                    $sqlraw = "SELECT ttbmhd.BMNo FROM `ttbmhd` WHERE  ({$post['cmbSearch']} LIKE :nilai)";
                    break;
                case stristr ($_nav['url'],'ttkkhd/'):
                    $sqlraw = "SELECT ttkkhd.KKNo FROM `ttkkhd` WHERE  ({$post['cmbSearch']} LIKE :nilai)";
                    break;
                case 'ttkmhd/kas-masuk-service-invoice-update':
                    $sqlraw = "SELECT ttkmhd.KMNo FROM `ttkmhd` WHERE  ({$post['cmbSearch']} LIKE :nilai)";
                    break;
                case 'trmp/update':
                    $sqlraw = "SELECT trmp.MPNo FROM `trmp` WHERE  ({$post['cmbSearch']} LIKE :nilai)";
                    break;
                case stristr ($_nav['url'],'ttgeneralledgerhd/'):
                    $sqlraw = "SELECT ttgeneralledgerhd.NoGL FROM `ttgeneralledgerhd` WHERE  ({$post['cmbSearch']} LIKE :nilai)";
                    break;
            }
            $result = \Yii::$app->db->createCommand( $sqlraw, $params)->queryAll();
            if($result == null && stristr ($_nav['url'],'ttkmhd/')){
                $sqlraw = "SELECT ttkmhd.KMNo FROM `ttkmhd` WHERE  ({$post['cmbSearch']} LIKE :nilai)";
                $result = \Yii::$app->db->createCommand( $sqlraw, $params)->queryAll();
            }
            $_nav = [
                'url'=> $post['url'],
                'cmbSearch'=> $post['cmbSearch'],
                'inSearch'=> $post['inSearch'],
                'data'=> $result,
                'linkBack'=> -1,
                'linkNext'=> 1,
                'current'=> 0,
                'total'=> count($result),
            ];
        }
        Yii::$app->session['_navSearch'] = $_nav;
        $this->generateCurrentID();
        return Yii::$app->session['_navSearch'];
    }


    function generateCurrentID(){
        $_nav = Yii::$app->session['_navSearch'];
        $currentUrl = '';
        if(!empty($_nav['data'][$_nav['current']])){
            $currentData = $_nav['data'][$_nav['current']];
            $arrKey = [];
            foreach(self::keyNav[$_nav['url']] as $key){
                $arrKey[] = $currentData[$key];
            }
            $currentUrl = base64_encode(implode( '||', $arrKey));
        }
        $_nav['currentUrl'] = $currentUrl;
        Yii::$app->session['_navSearch'] = $_nav;
    }

}
