<?php
namespace abengkel\components;
use yii\db\ActiveRecord;
use yii\helpers\Url;
class TdUi {
	/**
	 * @param string $model
	 *
	 * @param string $menuText
	 * @param array $options
	 *
	 * @return string
	 */
	static function mainGridJs( $model, $menuText = '', $options = [] ) {
		$grid_id      = "jqGrid";
		$gridPager_id = "jqGridPager";
		$colGrid      = [];
		$class        = explode( "\\", $model::className() );
		$class        = end( $class );
		$class        = strtolower( $class );
		$url          = Url::toRoute( [ $class . '/td' ] );
		$url_add      = Url::toRoute( [ $class . '/create', 'action' => 'create' ] );
		$url_update   = Url::toRoute( [ $class . '/update', 'action' => 'update' ] );
		$url_view     = Url::toRoute( [ $class . '/update', 'action' => 'view' ] );
		$url_print    = Url::toRoute( [ $class . '/print', 'action' => 'print' ] );
		$url_delete   = Url::toRoute( [ $class . '/delete' ] );;
		$mode             = '';
		$mode_multiSelect = 0;
		$mode_showDetail  = 0;
		$readOnly         = 0;
		if ( isset( $options[ 'mode' ] ) ) {
			$arrConfig        = explode( ',', $options[ 'mode' ] );
			$mode             = $arrConfig[ 0 ];
			$mode_multiSelect = in_array( 'multi-select', $arrConfig ) ? 1 : 0;
			$mode_showDetail  = in_array( 'show-detail', $arrConfig ) || $mode === 'select-detail' ? 1 : 0;
			$readOnly         = $mode === 'select' || $mode === 'select-detail' ? 1 : 0;
		}
		if ( isset( $options[ 'grid_id' ] ) ) {
			$grid_id = $options[ 'grid_id' ];
		}
		if ( isset( $options[ 'gridPager_id' ] ) ) {
			$gridPager_id = $options[ 'gridPager_id' ];
		}
		if ( isset( $options[ 'url' ] ) ) {
			$url = $options[ 'url' ];
		}
		if ( isset( $options[ 'url_add' ] ) ) {
			$url_add = $options[ 'url_add' ];
		}
		if ( isset( $options[ 'url_update' ] ) ) {
			$url_update = $options[ 'url_update' ];
			$url_view   = str_replace( '&action=update', '&action=view', $url_update );
		}
		if ( isset( $options[ 'url_delete' ] ) ) {
			$url_delete = $options[ 'url_delete' ];
		}
		if ( isset( $options[ 'url_view' ] ) ) {
			$url_view = $options[ 'url_view' ];
		}
		$button_add     = Menu::akses( $menuText, Menu::ADD ) ? "grid.navButtonAdd('#" . $gridPager_id . "',{
		   caption:' Tambah', 
		   buttonicon:'glyphicon glyphicon-plus', 
		   onClickButton: function(){ 
		      window.location = '$url_add';
		   }, 
	        position:'last'
			})" : '';
		$button_update  = Menu::akses( $menuText, Menu::UPDATE ) ?
			"<a href=\"$url_update&id='+id+'\" title=\"Edit\" aria-label=\"Update\" data-pjax=\"0\"><span class=\"glyphicon glyphicon-edit\"></span></a>" : "";
		$button_delete  = Menu::akses( $menuText, Menu::DELETE ) ?
			"<a href=\"$url_delete&id='+id+'\" rowId=\"'+id+'\" title=\"Hapus\" aria-label=\"Delete\" data-pjax=\"0\" data-confirm=\"Are you sure you want to delete this item?\" data-method=\"post\"><span class=\"glyphicon glyphicon-trash\"></span></a>" : "";
		$button_view    = Menu::akses( $menuText, Menu::VIEW ) ?
			"<a href=\"$url_view&id='+id+'\" title=\"View\" aria-label=\"View\" data-pjax=\"0\"><span class=\"fa fa-eye\"></span></a>" : "";
		$button_print   = Menu::akses( $menuText, Menu::VIEW ) ?
			"<a href=\"$url_print&id='+id+'\" title=\"Cetak\" aria-label=\"Print\" data-pjax=\"0\"><span class=\"glyphicon glyphicon-print\"></span></a>" : "";
		$cols           = [];
		$gridColsProses = '';
		if ( isset( $options[ 'btnAdd_Disabled' ] ) && $options[ 'btnAdd_Disabled' ] ) {
			$button_add = "";
		}
		if ( isset( $options[ 'btnUpdate_Disabled' ] ) && $options[ 'btnUpdate_Disabled' ] ) {
			$button_update = "";
		}
		if ( isset( $options[ 'btnDelete_Disabled' ] ) && $options[ 'btnDelete_Disabled' ] ) {
			$button_delete = "";
		}
		if ( isset( $options[ 'btnView_Disabled' ] ) && $options[ 'btnView_Disabled' ] ) {
			$button_view = "";
		}
		if ( isset( $options[ 'btnPrint_Disabled' ] ) && $options[ 'btnPrint_Disabled' ] ) {
			$button_print = "";
		}
		if ( ! $readOnly && ( $button_view != '' || $button_update != '' || $button_delete != '' || $button_print != '' ) ) {
			$gridColsProses = " {
                 label: 'Proses', 
                 name: 'actions', 
                 sortable: false,
                 width: 75, 
                 align: 'center',
                 frozen:true,
                 formatter: function(cellvalue, options, rowObject) {
                     let id = btoa(options.rowId);
                   return '$button_view $button_update $button_delete';
                 }
             },";
		}
		$model = new $model;
		if ( isset( $options[ 'colGrid' ] ) ) {
			$colGrid = $options[ 'colGrid' ];
		} else {
			$colGrid = $model::colGrid();
		}
		foreach ( $colGrid as $filed => $label ) {
			$is_array = is_array( $label );
			$col      = [];
			if ( $is_array ) {
				$col = $label;
			} else {
				$col[ 'name' ]  = $filed;
				$col[ 'label' ] = $label;
			}
			$cols[] = json_encode( $col );
		}
		$gridCols = implode( ', ', $cols );
		$gridCols = preg_replace( '/[\'"](func\w+)[\'"]/', '$1', $gridCols );
		$script   = <<< JS

var mode = '$mode',
    mode_multiSelect = $mode_multiSelect,
    mode_showDetail = $mode_showDetail,
    gridFn = window.util.app.dialogListData.gridFn;

    gridFn.var.mode = mode;
    gridFn.var.multiSelect = mode_multiSelect;
    gridFn.var.showDetail = mode_showDetail;

jQuery(function ($) {
    var readOnly = $readOnly;
        /* grid rowNum */
        // gridRowNum = mode === 'select' && !mode_showDetail ? 10 : 
        //              (mode_showDetail ? 5  : 
        //              15); /* default rowNum (page index) */
        // gridRowNum = mode === '' ? 15 : (!mode_showDetail ? 10 : 5);
        gridRowNum = 15;                        
        gridMinHeight = mode === '' ? 380 : (!mode_showDetail ? 300 : 120);
        /* grid height when Filter #2 is inactive */
	    gridMaxHeight = gridMinHeight + 34;
        
        ajaxRowOpt = {
            success : function(response, status, error) {
                $("#$grid_id").trigger("reloadGrid");
                $.notify({message: response.message},{type: response.success ? 'success' : 'warning'});
            },
            error : function(response, status, error) {
                $.notify({message: error + '<br> ' + response.responseJSON.message},{type: 'danger'});
            }
        };
    
    var funcEditTypeCombo = function(el){
        $(el).datepicker({
            format: "dd-mm-yyyy",
            language: "id",
            daysOfWeekHighlighted: "0",
            autoclose: true,
            todayHighlight: true
        }); 
    };
    
    var funcFormaterDate = function(cellValue, options, rowObject){
        if(!cellValue) return '';
        var dt = new Date(cellValue),
            d = dt.getDate(),
            m = dt.getMonth() + 1,
            y = dt.getFullYear();
        return (d<10?'0':'') + d + "-" + (m<10?'0':'') + m + "-" + y;
    };
    
    // $.jgrid.formatter.number.decimalSeparator = ',';
    // $.jgrid.formatter.number.thousandsSeparator = '.';
    // $.jgrid.defaults.width = 1000;
    // $.jgrid.defaults.responsive = true;
    $.jgrid.defaults.styleUI = 'Bootstrap';
    $.jgrid.defaults.pgtext = "{0} of {1}";
    var url_string = $(location).attr('href');
    var url = new URL(url_string);
	var c = decodeURIComponent(url.searchParams.get("r"));
    var pageKey = c;
    // console.log(pageKey);    
    var _searchCookie = (window === window.parent) ? Lockr.get('F.'+pageKey)||1 : 1;
   // console.log(_searchCookie);  
    const search={
       cmbTxt1: $('#filter-text-cmb1').val(),
        txt1 : $('#filter-text-val1').val(),
        cmbTxt2 : $('#filter-text-cmb2').val(),
        txt2 : $('#filter-text-val2').val(),
        check : $('#checked-filter').is(":checked") ? 'on' : 'off',
        cmbTgl1 : $('#filter-tgl-cmb1').val(),
        tgl1 : $('#filter-tgl-val1').val(),
        tgl2 : $('#filter-tgl-val2').val(),
        cmbNum1 : $('#filter-num-cmb1').val(),
        cmbNum2 : $('#filter-num-cmb2').val(),
        num1 : $('#filter-num-val').val(),
        r: c,
	    urlParams: util.url.getParams(url_string)
    };
    
     if(_searchCookie !== 1){
         search.cmbTxt1 =_searchCookie.cmbTxt1;
         search.txt1 =_searchCookie.txt1;
         search.cmbTxt2 =_searchCookie.cmbTxt2;
         search.txt2 =_searchCookie.txt2;
         search.check =_searchCookie.check;
         search.cmbTgl1 =_searchCookie.cmbTgl1;
         search.tgl1 =_searchCookie.tgl1;
         search.tgl2 =_searchCookie.tgl2;
         search.cmbNum1 =_searchCookie.cmbNum1;
         search.cmbNum2 =_searchCookie.cmbNum2;
         search.num1 =_searchCookie.num1;
    }   
     
     const queryString = window.location.search;
     const urlParams = new URLSearchParams(queryString);
     const cmbTxt1 = urlParams.get('cmbTxt1');
     const txt1 = urlParams.get('txt1');
     const cmbTxt2 = urlParams.get('cmbTxt2');
     const txt2 = urlParams.get('txt2');
     const check = urlParams.get('check');
     const cmbTgl1 = urlParams.get('cmbTgl1');
     const tgl1 = urlParams.get('tgl1');
     const tgl2 = urlParams.get('tgl2');
     const cmbNum1 = urlParams.get('cmbNum1');
     const cmbNum2 = urlParams.get('cmbNum2');
     const num1 = urlParams.get('num1');
     
     if(cmbTxt1){
         search.cmbTxt1 = cmbTxt1;
     }
     if(txt1){
         search.txt1 = txt1;
     }
     if(cmbTxt2){
         search.cmbTxt2 = cmbTxt2;
     }
     if(txt2){
         search.txt2 = txt2;
     }
     if(check){
         search.check = check;
     }
     if(cmbTgl1){
         search.cmbTgl1 = cmbTgl1;
     }
     if(tgl1){
         search.tgl1 = tgl1;
     }
     if(tgl2){
         search.tgl2 = tgl2;
     }
     if(cmbNum1){
         search.cmbNum1 = cmbNum1;
     }
     if(cmbNum2){
         search.cmbNum2 = cmbNum2;
     }
     if(num1){
         search.num1 = num1;
     }
     
     $('#filter-text-cmb1').val(search.cmbTxt1);
     $('#filter-text-val1').val(search.txt1);
     $('#filter-text-cmb2').val(search.cmbTxt2);
     $('#filter-text-val2').val(search.txt2);
     $('#filter-num-cmb1').val(search.cmbNum1);
	 $('#filter-num-cmb2').val(search.cmbNum2);
	 $('#filter-num-val').utilNumberControl().val(search.num1);
	 $('#filter-tgl-cmb1').val(search.cmbTgl1);    	
	 let _tgl1 = util.date.sql2sDate(search.tgl1)
	 let _tgl2 = util.date.sql2sDate(search.tgl2)
	 $('#filter-tgl-val1-disp').datepicker('update', _tgl1);
	 $('#filter-tgl-val2-disp').datepicker('update', _tgl2);
	 let _check = search.check == 'on';
	 $("#checked-filter").prop("checked", _check); 
	 $("#checked-filter").trigger('change'); 
    
     
    var dataPost = JSON.stringify(search);
    function createGrid(){
	        var grid = $("#$grid_id");
	        var grid_param = {
	        url: '$url',
	        mtype: "POST",
	        datatype: "json",
	        jsonReader: {
	            root: 'rows',
	            page: 'currentPage',
	            total: 'totalPages',
	            records: 'rowsCount',
	            id: 'rowId',
	            repeatitems: false
	        },
	        rownumbers: true,
	        colModel: [
	            $gridColsProses
	            $gridCols
	        ],        
	        multiSort: true,	        
	        sortname: setcmb.sortname,
	        sortorder: setcmb.sortorder,
	        search: true,
	        postData: {
			    query: dataPost
			},
	        altclass: 'row-zebra',
	        altRows: true,
	        pager: "#$gridPager_id",
	        viewrecords: true,
	        rowNum: gridRowNum,
	        rowList:[15,30,45,60,75],
	        width: 780,
	        height: gridMinHeight,
	        autowidth:false, 
	        shrinkToFit:false,
	        styleUI : 'Bootstrap',
	        // toppager:true,
	        ajaxRowOptions : ajaxRowOpt,
	        page: Lockr.get('I.'+pageKey)||1,
	        loadComplete: function(d){
	            // console.log(d);
	            //document.cookie = pageKey+"="+$("#$grid_id").getGridParam('page')+";path=/";	 
	            const search={
		           cmbTxt1: $('#filter-text-cmb1').val(),
		            txt1 : $('#filter-text-val1').val(),
		            cmbTxt2 : $('#filter-text-cmb2').val(),
		            txt2 : $('#filter-text-val2').val(),
		            check : $('#checked-filter').is(":checked") ? 'on' : 'off',
		            cmbTgl1 : $('#filter-tgl-cmb1').val(),
		            tgl1 : $('#filter-tgl-val1').val(),
		            tgl2 : $('#filter-tgl-val2').val(),
		            cmbNum1 : $('#filter-num-cmb1').val(),
		            cmbNum2 : $('#filter-num-cmb2').val(),
		            num1 : $('#filter-num-val').val(),
		            r: c,
				    urlParams: util.url.getParams(url_string)
		        };	            
	            // if(window === window.parent){
		            Lockr.set('I.'+pageKey,d.currentPage);
		            Lockr.set('F.'+pageKey,search);
	            // }
    			dataPost = JSON.stringify(search);  
    			if (grid.getGridParam().multiselect){    			    
    			    if (window.multiSelectID !== undefined && window.multiSelectID[grid.getGridParam().id] !== undefined){
    			        var rows = window.multiSelectID[grid.getGridParam().id];
    			        for (var i = 0; i < rows.length; i++) {
                        	grid.setSelection(rows[i].id,false);
                        	grid.setRowData(rows[i].id, rows[i].data);
    			        }
    			    }
    			}
    			// var dataIDs = grid.getDataIDs();
    			// if (dataIDs.length > 0 && dataIDs[0] !== 'norecs'){
    			// 	grid.setSelection(dataIDs[0], true);
    			// }
	        },
	        gridComplete:function() {
	           $('a[data-confirm]').on('click',function(ev) {
			        var href = $(this).attr('href');
			        var rowId = $(this).attr('rowId');
			        
			        bootbox.confirm({
                        title: 'Konfirmasi',
                        message: "Anda yakin ingin menghapus record ini ?",
                        size: "small",
                        buttons: {
                            confirm: {
                                label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                                className: 'btn btn-warning'
                            },
                            cancel: {
                                label: '<span class="fa fa-ban"></span> Cancel',
                                className: 'btn btn-default'
                            }
                        },
                        callback: function (result) {
                            if(result) {
                                $.ajax({
						            url:  href, //this is the submit URL
						            type: 'POST', //or POST
						            data: {
						                'oper':'del',
						                'id': rowId    
						            },
						            success : function(data){
						                $.notify({message: data.message},{type: data.success ? 'success' : 'warning'});
						                 $('#$grid_id').trigger( 'reloadGrid' );
						            }
						        });
                            }
                        }
                    });
			        return false;
			    });
	        },
	        multiselect: gridFn.header.multiSelect(),
	        onSelectRow: gridFn.header.onSelectRow,
	        onSelectAll: gridFn.header.onSelectAll,
	        ondblClickRow: gridFn.header.ondblClickRow
	    };
		if (window.gridParam_$grid_id !== undefined){
			grid_param = Object.assign(grid_param, window.gridParam_$grid_id);
		}
		if (typeof window.grid_before_init_$grid_id !== "undefined"){
			window.grid_before_init_$grid_id(grid,grid_param);
		}
		grid.jqGrid(grid_param).navGrid('#$gridPager_id',{
	        edit:false,add:false,del:false,search:false,refresh:false
	    }).bindKeys();
	        
        grid.jqGrid("setLabel", "rn", "No",{'text-align':'center'});
        
	    if(!readOnly) {
            grid.jqGrid("setLabel", "actions", "Proses",{'text-align':'center'});
            // grid.jqGrid('getGridParam', 'colModel').forEach(item => {
            //    grid.jqGrid("setLabel", item.name, "",{'text-align':item.align});
            // });
            $button_add
	    }
	    
	    gridFn.set(grid);
	    
	    /* grid height */
        $('#checked-filter').change(function() {
            grid.setGridHeight(this.checked ? gridMinHeight : gridMaxHeight);      
        });
	    
        /* grid width */
	    function resizeGrid() {
	        $('#$grid_id')
	        .setGridWidth($('.panel-body').width(),false);
	        // .setGridHeight($(window).height() - $("#jqGrid").offset().top);
	    }
	    // resizeGrid();
	    $('.panel-body').resize(resizeGrid);
    }
    
	var idPK = decodeURIComponent(url.searchParams.get("id"));
	if(idPK != "null"){
	    $.ajax({
			  type: "POST",
			  url: '$url',
			  data: {
			      query : dataPost,
			      rows: gridRowNum,
			      sidx: setcmb.sortname,
	              sord: setcmb.sortorder,
	              id: idPK
			  },
			  success: function(d){
			       Lockr.set('I.'+pageKey,d.page);
			       createGrid();
			  },		
			  datatype: "json",		
		});
    }else{
			createGrid();      
    }
    
});
JS;
		return $script;
	}
}