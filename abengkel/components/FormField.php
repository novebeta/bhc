<?php
namespace abengkel\components;
use abengkel\models\Tdahass;
use abengkel\models\Tdarea;
use abengkel\models\Tddealer;
use abengkel\models\Tdjasagroup;
use abengkel\models\Tdkaryawan;
use abengkel\models\Tdlokasi;
use abengkel\models\Tdmotortype;
use abengkel\models\Tdpos;
use abengkel\models\Tdprogramhd;
use abengkel\models\Tdsupplier;
use abengkel\models\Traccount;
use abengkel\models\Ttqshd;
use abengkel\models\Ttsdhd;
use kartik\datecontrol\DateControl;
use kartik\number\NumberControl;
use kartik\widgets\Select2;
use kartik\widgets\TimePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
class FormField {
    /* --- LABEL --- */
    private static $select2_js_formatDropDown = <<< JS
function(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-10" style="text-align: left">' + result.id + '</div>' +
           '<div class="col-xs-14">' + result.text + '</div>' +
           '</div>';
}
JS;
    private static $select2_js_matcherDropDown = <<< JS
function(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
JS;
    /* --- COMBO --- */
    static function label( $text, $style = '' ) {
        if ( ! $style ) {
            $style = "margin: 0; padding: 6px 0;";
        }
        return '<label class="control-label" style="' . $style . '">' . $text . '</label>';
    }
    static function combo( $tipe, $params = [] ) {
        $extraOptions = isset( $params[ 'extraOptions' ] ) ? $params[ 'extraOptions' ] : [];
        $simpleCombo  = isset( $params[ 'extraOptions' ][ 'simpleCombo' ] ) ? $params[ 'extraOptions' ][ 'simpleCombo' ] : false;
        $options      = isset( $params[ 'options' ] ) ? $params[ 'options' ] : [];
        $config       = isset( $params[ 'config' ] ) ? $params[ 'config' ] : [];
        /* non model */
        $name = isset( $params[ 'name' ] ) ? $params[ 'name' ] : $tipe;
        /* by model */
        $form       = isset( $params[ 'form' ] ) ? $params[ 'form' ] : null;
        $model      = isset( $params[ 'model' ] ) ? $params[ 'model' ] : null;
        $label      = isset( $params[ 'label' ] ) ? $params[ 'label' ] : null;
        $labelStyle = isset( $params[ 'labelStyle' ] ) ? $params[ 'labelStyle' ] : null; // replace null with default style
        /* default options */
        if ( array_key_exists( 'class', $options ) ) {
            if ( ! strpos( $options[ 'class' ], 'form-control' ) ) {
                $options[ 'class' ] .= ' form-control';
            }
        } else {
            $options[ 'class' ] = 'form-control';
        }
        $config = array_merge( [
            'options'       => $options,
            'theme'         => Select2::THEME_DEFAULT,
            'pluginOptions' => [
                'dropdownAutoWidth' => true,
            ],
        ], $config );
        if ( ! $simpleCombo ) {
            $config[ 'pluginOptions' ][ 'templateResult' ]    = new JsExpression( self::$select2_js_formatDropDown );
            $config[ 'pluginOptions' ][ 'templateSelection' ] = new JsExpression( self::$select2_js_formatDropDown );
            $config[ 'pluginOptions' ][ 'matcher' ]           = new JsExpression( self::$select2_js_matcherDropDown );
            $config[ 'pluginOptions' ][ 'escapeMarkup' ]      = new JsExpression( "function(m) { return m; }" );
        }
        $allOption = isset( $params[ 'extraOptions' ][ 'allOption' ] ) ? $params[ 'extraOptions' ][ 'allOption' ] : false;
        switch ( $tipe ) {
            case 'JasaGroup' :
                $altLabel         = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'JasaGroup' ];
                $altOrder         = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'JasaGroup' => SORT_ASC ];
                $altCondition     = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
                $altParams        = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
                $config[ 'data' ] = ArrayHelper::map( Tdjasagroup::find()->JasaGroup( 'JasaGroup', $altLabel, $altOrder, [ "condition" => $altCondition, "params" => $altParams ], $allOption ), "value", 'label' );
                break;
            case 'LokasiInduk' :
                $altLabel         = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'LokasiKode' ];
                $altOrder         = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'LokasiKode' => SORT_ASC ];
                $altCondition     = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
                $altParams        = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
	            if(!isset($config[ 'data' ])) $config[ 'data' ] = ArrayHelper::map( Tdlokasi::find()->LokasiInduk( 'LokasiKode', $altLabel, $altOrder, [ "condition" => $altCondition, "params" => $altParams ], $allOption ), "value", 'label' );
                break;
	        case 'KodePos' :
		        $altLabel         = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'KodePos' ];
		        $altOrder         = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'KodePos' => SORT_ASC ];
		        $altCondition     = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
		        $altParams        = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
		        if(!isset($config[ 'data' ])) $config[ 'data' ] = ArrayHelper::map( Tdarea::find()->select2( 'KodePos', $altLabel, $altOrder, [ 'condition' => $altCondition, 'params' => $altParams ], $allOption ), "value", 'label' );
		        break;
            case 'KarKode' :
                $altLabel         = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'KarKode' ];
                $altOrder         = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'KarKode' => SORT_ASC ];
                $altCondition     = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
                $altParams        = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
	            if(!isset($config[ 'data' ])) $config[ 'data' ] = ArrayHelper::map( Tdkaryawan::find()->select2( 'KarKode', $altLabel, $altOrder, [ "condition" => $altCondition, "params" => $altParams ], $allOption ), "value", 'label' );
                break;
            case 'LokasiKode' :
                $altLabel         = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'LokasiKode' ];
                $altOrder         = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'LokasiKode' => SORT_ASC ];
                $altCondition     = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
                $altParams        = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
	            if(!isset($config[ 'data' ])) $config[ 'data' ] = ArrayHelper::map( Tdlokasi::find()->select2( 'LokasiKode', $altLabel, $altOrder, [ "condition" => $altCondition, "params" => $altParams ], $allOption ), "value", 'label' );
                break;
            case 'PosKode' :
                $altLabel         = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'PosKode' ];
                $altOrder         = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'PosKode' => SORT_ASC ];
                $altCondition     = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
                $altParams        = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
                if(!isset($config[ 'data' ])) $config[ 'data' ] = ArrayHelper::map( Tdpos::find()->select2( 'PosKode', $altLabel, $altOrder, [ "condition" => $altCondition, "params" => $altParams ], $allOption ), "value", 'label' );
                break;
            case 'SupKode' :
                $altLabel         = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'SupKode' ];
                $altOrder         = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'SupKode' => SORT_ASC ];
                $altCondition     = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
                $altParams        = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
	            if(!isset($config[ 'data' ])) $config[ 'data' ] = ArrayHelper::map( Tdsupplier::find()->select2( 'SupKode', $altLabel, $altOrder, [ "condition" => $altCondition, "params" => $altParams ], $allOption ), "value", 'label' );
                break;
            case 'NoAccount' :
                $altLabel         = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'NoAccount' ];
                $altOrder         = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'NoAccount' => SORT_ASC ];
                $altCondition     = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
                $altParams        = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
	            if(!isset($config[ 'data' ])) $config[ 'data' ] = ArrayHelper::map( Traccount::find()->NoAccount( 'NoAccount', $altLabel, $altOrder, [ "condition" => $altCondition, "params" => $altParams ], $allOption ), "value", 'label' );
                break;
            case 'DealerKode' :
                $altLabel         = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'DealerKode' ];
                $altOrder         = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'DealerKode' => SORT_ASC ];
                $altCondition     = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
                $altParams        = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
	            if(!isset($config[ 'data' ])) $config[ 'data' ] = ArrayHelper::map( Tddealer::find()->select2( 'DealerKode', $altLabel, $altOrder, [ "condition" => $altCondition, "params" => $altParams ], $allOption ), "value", 'label' );
                break;
            case 'AHASSNomor' :
                $altLabel         = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'AHASSNomor' ];
                $altOrder         = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'AHASSNomor' => SORT_ASC ];
                $altCondition     = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
                $altParams        = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
                $config[ 'data' ] = ArrayHelper::map( Tdahass::find()->select2( 'AHASSNomor', $altLabel, $altOrder, [ "condition" => $altCondition, "params" => $altParams ], $allOption ), "value", 'label' );
                break;
            case 'Program' :
                $altLabel         = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'PrgNama' ];
                $altOrder         = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : ['PrgTglAkhir' => SORT_DESC];
                $altCondition     = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
                $altParams        = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
	            if(!isset($config[ 'data' ])) $config[ 'data' ] = ArrayHelper::map( Tdprogramhd::find()->select2( 'PrgNama', $altLabel, $altOrder, [ "condition" => $altCondition, "params" => $altParams ], $allOption ), "value", 'label' );
                break;
            case 'Provinsi' :
                $urlTdArea        = Url::toRoute( 'tdarea/find' );
                $config[ 'data' ] = ArrayHelper::map( Tdarea::find()->select2( 'Provinsi', [ 'Provinsi' ], [ 'Provinsi' => SORT_ASC ],[ "condition" => '', "params" => [] ], $allOption ), "value", 'label' );
                if ( isset( $extraOptions[ 'KabupatenId' ] ) ) {
                    $config[ 'pluginEvents' ][ 'change' ] = new \yii\web\JsExpression( "function(e) {
                    $('#" . $extraOptions[ 'KabupatenId' ] . "').utilSelect2().loadRemote('" . $urlTdArea . "', {  mode: 'combo', field: 'Kabupaten', Provinsi: this.value }, true,
                        function(){ 
                            if(window.afterLoadKabupaten !== undefined && typeof window.afterLoadKabupaten === 'function'){
                                window.afterLoadKabupaten();
                            }
                            $('#" . $extraOptions[ 'KabupatenId' ] . "').trigger( 'change' ); 
                        });
                    }" );
                }
                break;
            case 'Kabupaten' :
                $urlTdArea        = Url::toRoute( 'tdarea/find' );
                $config[ 'data' ] = ArrayHelper::map( Tdarea::find()->select2( 'Kabupaten', [ 'Kabupaten' ], [ 'Kabupaten' => SORT_ASC ],[ "condition" => '', "params" => [] ], $allOption ), "value", 'label' );
                if ( isset( $extraOptions[ 'KecamatanId' ] ) ) {
                    $config[ 'pluginEvents' ][ 'change' ] = new \yii\web\JsExpression( "function(e) {
                    $('#" . $extraOptions[ 'KecamatanId' ] . "').utilSelect2().loadRemote('" . $urlTdArea . "', {  mode: 'combo', field: 'Kecamatan', Kabupaten: this.value }, true,
                    function(){ 
                            if(window.afterLoadKecamatan !== undefined && typeof window.afterLoadKecamatan === 'function'){
                                window.afterLoadKecamatan();
                            }
                            $('#" . $extraOptions[ 'KecamatanId' ] . "').trigger( 'change' ); 
                        });
                    }" );
                }
                break;
            case 'Kecamatan' :
                $urlTdArea        = Url::toRoute( 'tdarea/find' );
	            if(!isset($config[ 'data' ])) $config[ 'data' ] = ArrayHelper::map( Tdarea::find()->select2( 'Kecamatan', [ 'Kecamatan' ], [ 'Kecamatan' => SORT_ASC ],[ "condition" => '', "params" => [] ], $allOption ), "value", 'label' );
                if ( isset( $extraOptions[ 'KelurahanId' ] ) ) {
                    $config[ 'pluginEvents' ][ 'change' ] = new \yii\web\JsExpression( "function(e) {
                    $('#" . $extraOptions[ 'KelurahanId' ] . "').utilSelect2().loadRemote('" . $urlTdArea . "', {  mode: 'combo', field: 'Kelurahan', Kecamatan: this.value }, true,
                    function(){
                            if(window.afterLoadKelurahan !== undefined && typeof window.afterLoadKelurahan === 'function'){
                                window.afterLoadKelurahan();
                            } 
                            $('#" . $extraOptions[ 'KelurahanId' ] . "').trigger( 'change' ) 
                        });
                    }" );
                }
                break;
            case 'Kelurahan' :
                $urlTdArea        = Url::toRoute( 'tdarea/find' );
	            if(!isset($config[ 'data' ])) $config[ 'data' ] = ArrayHelper::map( Tdarea::find()->select2( 'Kelurahan', [ 'Kelurahan' ], [ 'Kelurahan' => SORT_ASC ],[ "condition" => '', "params" => [] ], $allOption ), "value", 'label' );
                if ( isset( $extraOptions[ 'KodePosId' ] ) ) {
                    $config[ 'pluginEvents' ][ 'change' ] = new \yii\web\JsExpression( "function(e) {
                    $('#" . $extraOptions[ 'KodePosId' ] . "').utilSelect2().loadRemote('" . $urlTdArea . "', {  mode: 'combo', field: 'KodePos', Kelurahan: this.value }, true,
                    function(){
                            if(window.afterLoadKodePos !== undefined && typeof window.afterLoadKodePos === 'function'){
                                window.afterLoadKodePos();
                            } 
                            $('#" . $extraOptions[ 'KodePosId' ] . "').trigger( 'change' ) 
                        });
                    }" );
                }
                break;
            case 'SDNo' :
                $altLabel         = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'SDNo' ];
                $altOrder         = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'SDNo' => SORT_ASC ];
                $altCondition     = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
                $altParams        = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
                $config[ 'data' ] = ArrayHelper::map( Ttsdhd::find()->select2( 'SDNo', $altLabel, $altOrder, [ 'condition' => $altCondition, 'params' => $altParams ], $allOption ), "value", 'label' );
                break;
            case 'SDNoUrut' :
                $altLabel         = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'SDNoUrut' ];
                $altOrder         = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'SDNoUrut' => SORT_ASC ];
                $altCondition     = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
                $altParams        = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
                $config[ 'data' ] = ArrayHelper::map( Ttsdhd::find()->select2( 'SDNoUrut', $altLabel, $altOrder, [ 'condition' => $altCondition, 'params' => $altParams ], $allOption ), "value", 'label' );
                break;
            case 'MotorNoPolisi' :
                $altLabel         = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'MotorNoPolisi' ];
                $altOrder         = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'MotorNoPolisi' => SORT_ASC ];
                $altCondition     = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
                $altParams        = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
                $config[ 'data' ] = ArrayHelper::map( Ttsdhd::find()->select2( 'MotorNoPolisi', $altLabel, $altOrder, [ 'condition' => $altCondition, 'params' => $altParams ], $allOption ), "value", 'label' );
                break;
            case 'SDPembawaMotor' :
                $altLabel         = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'SDPembawaMotor' ];
                $altOrder         = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'SDPembawaMotor' => SORT_ASC ];
                $altCondition     = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
                $altParams        = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
                $config[ 'data' ] = ArrayHelper::map( Ttsdhd::find()->select2( 'SDPembawaMotor', $altLabel, $altOrder, [ 'condition' => $altCondition, 'params' => $altParams ], $allOption ), "value", 'label' );
                break;
            case 'PKBNo' :
                $altLabel         = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'PKBNo' ];
                $altOrder         = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'PKBNo' => SORT_ASC ];
                $altCondition     = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
                $altParams        = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
                $config[ 'data' ] = ArrayHelper::map( Ttsdhd::find()->select2( 'PKBNo', $altLabel, $altOrder, [ 'condition' => $altCondition, 'params' => $altParams ], $allOption ), "value", 'label' );
                break;
            case 'QSTujuan' :
                $altLabel         = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'QSTujuan' ];
                $altOrder         = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'QSTujuan' => SORT_ASC ];
                $altCondition     = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
                $altParams        = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
                $config[ 'data' ] = ArrayHelper::map( Ttqshd::find()->select2( 'QSTujuan', $altLabel, $altOrder, [ 'condition' => $altCondition, 'params' => $altParams ], $allOption ), "value", 'label' );
                break;
            case 'MotorType' :
                $config[ 'data' ] = ArrayHelper::map( Tdmotortype::find()->select2( 'MotorType', [ 'MotorType' ], [ 'MotorType' => SORT_ASC ], [ "condition" => '', "params" => [] ],$allOption ), "value", 'label' );
                break;
            case 'MotorNama' :
                $config[ 'data' ] = ArrayHelper::map( Tdmotortype::find()->select2( 'MotorNama', [ 'MotorNama' ], [ 'MotorNama' => SORT_ASC ], [ "condition" => '', "params" => [] ],$allOption ), "value", 'label' );
                break;
            case 'DaftarKarKodeSA' :
                $altLabel         = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'KarKode' ];
                $altOrder         = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'KarStatus' => SORT_ASC ];
                $altCondition     = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
                $altParams        = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
                if(!isset($config[ 'data' ])) $config[ 'data' ] = ArrayHelper::map( Tdkaryawan::find()->daftarkarkode( 'KarKode', $altLabel, $altOrder, [ "condition" => $altCondition, "params" => $altParams ], $allOption ), "value", 'label' );
                break;
            case 'BrgGroup' :
                $config[ 'data' ] = [
                    'Accesories'=>	'Accesories',
                    'Battery'=>	'Battery',
                    'Oli'	=>'Oli',
                    'Part'	=>'Honda Genuine Part',
                    'Plastic Part'=>	'Plastic Part',
                    'Special Tools'	=>'Special Tools',
                    'Tire' =>	'Ban',
                    'XLokal' =>'Lokal',
                ];
                break;
            case 'BrgStatus' :
            case 'CusStatus' :
	        case 'MotorStatus' :
                $config[ 'data' ] = [
                    'A'=>	'AKTIF',
                    'N'=>	'NON AKTIF',
                ];
                break;
            case 'LokasiStatus' :
                $config[ 'data' ] = [
                    'A'=>	'AKTIF',
                    'N'=>	'NON AKTIF',
                    '1'=>	'DEALER',
                ];
                break;
            case 'LokasiJenis' :
                $config[ 'data' ] = [
                    'SubLokasi'=>	'SubLokasi',
                    'MainLokasi'=>	'MainLokasi',
                ];
                break;
            case 'BrgSatuan' :
                $config[ 'data' ] = [
                    'PCS'=>	'PCS',
                    'SET'=>	'SET',
                ];
                break;
            case 'CaraBayar' :
                $config[ 'data' ] = [
                    'Tunai'         =>	'Tunai',
                    'Transfer'      =>	'Transfer',
                    'Kartu Debit'   =>	'Kartu Debit',
                    'Kartu Kredit'  =>	'Kartu Kredit',
                    'Kas Kredit'    =>	'Kas Kredit',
                ];
                break;
            case 'PosJenis' :
                $config[ 'data' ] = [
                    'Mandiri'=>	'Mandiri',
                    'Non Mandiri'=>	'Non Mandiri',
                ];
                break;
            case 'DealerStatus' :
                $config[ 'data' ] = [
                    'A'=>	'AKTIF',
                    'N'=>	'NON AKTIF',
                    '1'=>	'Dealer Utama',
                ];
                break;
            case 'PIJenis' :
	        case 'JenisServis' :
                $config[ 'data' ] = [
                    'Tunai'=>	'Tunai',
                    'Kredit'=>	'Kredit',
                ];
                break;
            case 'AHASSJenisLayanan' :
                $config[ 'data' ] = [
                    'H12'=>	'H12',
                    'H123'=> 'H123',
                    'H1'=> 'H1',
                    'H23'=> 'H23',
                    'H2'=> 'H2',
                ];
                break;
            case 'AHASSJenisDealer' :
                $config[ 'data' ] = [
                    'Regular'=>	'Regular',
                    'Wing'=>	'Wing',
                    'Big Wing'=>	'Big Wing',
                ];
                break;
//            case 'MotorType' :
//                $config[ 'data' ] = [
//                    'Cub Low' => 'Cub Low',
//                    'Cub Mid' => 'Cub Mid',
//                    'Cub High' => 'Cub High',
//                    'AT Low' => 'AT Low',
//                    'AT Mid' => 'AT Mid',
//                    'AT High' => 'AT High',
//                    'Sport Low' => 'Sport Low',
//                    'Sport Mid' => 'Sport Mid',
//                    'Sport High' => 'Sport High',
//                ];
//                break;
            case 'StokIP' :
                $config[ 'data' ] = [
                    'Lokal'=>	'Lokal',
                    'Penerimaan1'=>	'Penerimaan1',
                    'HotLine'=>	'HotLine',
                    'Robbing'=>	'Robbing',
                ];
                break;
            case 'MotorCC' :
                $config[ 'data' ] = [
                    '110'=>	'110',
                    '125'=>	'125',
                    '150'=>	'150',
                    '250'=>	'250',
                    '100'=>	'100',
                ];
                break;
            case 'CusStatusRumah' :
                $config[ 'data' ] = [
                    'Rumah Sendiri' => 'Sendiri',
                    'Rumah Ortu'    => 'Ortu',
                    'Rumah Sewa'    => 'Sewa',
                ];
                break;
            case 'CusSex' :
                $config[ 'data' ] = [
                    'Perempuan' => 'Perempuan',
                    'Laki-Laki' => 'Laki-Laki',
                ];
                break;
            case 'StatusMP' :
                $config[ 'data' ] = [
                    'Open' => 'Open',
                    'Close' => 'Close',
                ];
                break;
            case 'CusAgama' :
                $config[ 'data' ] = [
                    'Islam'     => 'Islam',
                    'Kristen'   => 'Kristen',
                    'Katholik'  => 'Katholik',
                    'Hindu'     => 'Hindu',
                    'Budha'     => 'Budha',
                    'Lain-Lain' => 'Lain-Lain',
                ];
                break;
            case 'CusPekerjaan' :
                $config[ 'data' ] = [
                    'Wiraswasta' => 'Wiraswasta',
                    'Karyawan Swasta' => 'Karyawan Swasta',
                    'Belum/Tidak Bekerja' => 'Belum/Tidak Bekerja',
                    'Biarawan' => 'Biarawan',
                    'Biarawati' => 'Biarawati',
                    'Bidan' => 'Bidan',
                    'Bupati' => 'Bupati',
                    'Buruh Bangunan' => 'Buruh Bangunan',
                    'Buruh Harian Lepas' => 'Buruh Harian Lepas',
                    'Buruh Nelayan/Perikanan' => 'Buruh Nelayan/Perikanan',
                    'Buruh Peternakan' => 'Buruh Peternakan',
                    'Buruh Tani/Perkebunan' => 'Buruh Tani/Perkebunan',
                    'Dokter' => 'Dokter',
                    'Dosen' => 'Dosen',
                    'Duta Besar' => 'Duta Besar',
                    'Gubernur' => 'Gubernur',
                    'Guru' => 'Guru',
                    'Ibu Rumah Tangga' => 'Ibu Rumah Tangga',
                    'Imam Mesjid' => 'Imam Mesjid',
                    'Industri' => 'Industri',
                    'Juru Masak' => 'Juru Masak',
                    'Karyawan BUMD' => 'Karyawan BUMD',
                    'Karyawan BUMN' => 'Karyawan BUMN',
                    'Karyawan Honorer' => 'Karyawan Honorer',
                    'Kepala Desa' => 'Kepala Desa',
                    'Kepolisian RI' => 'Kepolisian RI',
                    'Konstruksi' => 'Konstruksi',
                    'Konsultan' => 'Konsultan',
                    'Lainnya' => 'Lainnya',
                    'Mahasiswa/Pelajar' => 'Mahasiswa/Pelajar',
                    'Mekanik' => 'Mekanik',
                    'Mengurus Rumah Tangga' => 'Mengurus Rumah Tangga',
                    'Nelayan/Perikanan' => 'Nelayan/Perikanan',
                    'Notaris' => 'Notaris',
                    'Ojek' => 'Ojek',
                    'Paraji' => 'Paraji',
                    'Paranormal' => 'Paranormal',
                    'Pastor' => 'Pastor',
                    'Pedagang' => 'Pedagang',
                    'Pegawai Negeri Sipil' => 'Pegawai Negeri Sipil',
                    'Pekebun' => 'Pekebun',
                    'Pelajar/Mahasiswa' => 'Pelajar/Mahasiswa',
                    'Pelaut' => 'Pelaut',
                    'Pembantu Rumah Tangga' => 'Pembantu Rumah Tangga',
                    'Penata Busana' => 'Penata Busana',
                    'Penata Rambut' => 'Penata Rambut',
                    'Penata Rias' => 'Penata Rias',
                    'Pendeta' => 'Pendeta',
                    'Peneliti' => 'Peneliti',
                    'Pengacara' => 'Pengacara',
                    'Pengrajin' => 'Pengrajin',
                    'Pensiunan' => 'Pensiunan',
                    'Penterjemah' => 'Penterjemah',
                    'Penyiar Radio' => 'Penyiar Radio',
                    'Penyiar Televisi' => 'Penyiar Televisi',
                    'Perancang Busana' => 'Perancang Busana',
                    'Perangkat Desa' => 'Perangkat Desa',
                    'Perawat' => 'Perawat',
                    'Perdagangan' => 'Perdagangan',
                    'Petani' => 'Petani',
                    'Peternak' => 'Peternak',
                    'Petugas Keamanan' => 'Petugas Keamanan',
                    'Pialang' => 'Pialang',
                    'Pilot' => 'Pilot',
                    'Presiden' => 'Presiden',
                    'Promotor Acara' => 'Promotor Acara',
                    'Psikiater' => 'Psikiater',
                    'Psikolog' => 'Psikolog',
                    'Seniman' => 'Seniman',
                    'Sopir' => 'Sopir',
                    'Tabib' => 'Tabib',
                    'Teknisi' => 'Teknisi',
                    'Tentara Nasional Indonesia' => 'Tentara Nasional Indonesia',
                    'Transportasi' => 'Transportasi',
                    'Tukang Batu' => 'Tukang Batu',
                    'Tukang Cukur' => 'Tukang Cukur',
                    'Tukang Gigi' => 'Tukang Gigi',
                    'Tukang Jahit' => 'Tukang Jahit',
                    'Tukang Kayu' => 'Tukang Kayu',
                    'Tukang Las/Pandai Besi' => 'Tukang Las/Pandai Besi',
                    'Tukang Listrik' => 'Tukang Listrik',
                    'Tukang Sol Sepatu' => 'Tukang Sol Sepatu',
                    'Ustadz/Mubaligh' => 'Ustadz/Mubaligh',
                    'Wakil Bupati' => 'Wakil Bupati',
                    'Wakil Gubernur' => 'Wakil Gubernur',
                    'Wakil Presiden' => 'Wakil Presiden',
                    'Wakil Walikota' => 'Wakil Walikota',
                    'Walikota' => 'Walikota',
                    'Wartawan' => 'Wartawan',
                    'Akuntan' => 'Akuntan',
                    'Anggota BPK' => 'Anggota BPK',
                    'Anggota DPD' => 'Anggota DPD',
                    'Anggota DPRD Kabupaten/Kota' => 'Anggota DPRD Kabupaten/Kota',
                    'Anggota DPRD Provinsi' => 'Anggota DPRD Provinsi',
                    'Anggota DPR-RI' => 'Anggota DPR-RI',
                    'Anggota Kabinet/Kementerian' => 'Anggota Kabinet/Kementerian',
                    'Anggota Mahkamah Konstitusi' => 'Anggota Mahkamah Konstitusi',
                    'Apoteker' => 'Apoteker',
                    'Arsitek' => 'Arsitek',
                ];
                break;
            case 'CusPendidikan' :
                $config[ 'data' ] = [
                    'Tidak Tamat SD'  => 'Tidak Tamat SD',
                    'SD'              => 'SD',
                    'SLTP/SMP'        => 'SLTP/SMP',
                    'SLTA/SMU'        => 'SLTA/SMU',
                    'Akademi/Diploma' => 'Akademi/Diploma',
                    'Sarjana'         => 'Sarjana',
                    'Pasca Sarjana'   => 'Pasca Sarjana',
                ];
                break;
            case 'CusPengeluaran' :
                $config[ 'data' ] = [
                    '<Rp 700.000,-'                     => '<Rp 700.000,-',
                    'Rp 700.001,- s/d Rp 1.000.000,-'   => 'Rp 700.001,- s/d Rp 1.000.000,-',
                    'Rp 1.000.001,- s/d Rp 1.500.000,-' => 'Rp 1.000.001,- s/d Rp 1.500.000,-',
                    'Rp 1.500.001,- s/d Rp 2.000.000,-' => 'Rp 1.500.001,- s/d Rp 2.000.000,-',
                    'Rp 2.000.001,- s/d Rp 3.000.000,-' => 'Rp 2.000.001,- s/d Rp 3.000.000,-',
                    'Rp 3.000.001,- s/d Rp 4.000.000,-' => 'Rp 3.000.001,- s/d Rp 4.000.000,-',
                    '>Rp 4.000.000,-'                   => '>Rp 4.000.000,-'
                ];
                break;
            case 'CusStatusHP' :
                $config[ 'data' ] = [
                    'Prabayar'   => 'Prabayar',
                    'Pascabayar' => 'Pascabayar',
                ];
                break;
            case 'CusKodeKons' :
                $config[ 'data' ] = [
                    'Individual Customer (Regular)'     => 'Individual Customer (Regular)',
                    'Individual Customer (Joint Promo)' => 'Individual Customer (Joint Promo)',
                    'Individual Customer (Kolektif)'    => 'Individual Customer (Kolektif)',
                    'Group Customer'                    => 'Group Customer',
                ];
                break;
            case 'DKMerk' :
                $config[ 'data' ] = [
                    'Honda'                 => 'Honda',
                    'Yamaha'                => 'Yamaha',
                    'Suzuki'                => 'Suzuki',
                    'Kawasaki'              => 'Kawasaki',
                    'Motor Lain'            => 'Motor Lain',
                    'Belum pernah memiliki' => 'Belum pernah memiliki',
                ];
                break;
            case 'DKJenis' :
                $config[ 'data' ] = [
                    'Sport'                 => 'Sport',
                    'Cub(Bebek)'            => 'Cub(Bebek)',
                    'AT(Automatic)'         => 'AT(Automatic)',
                    'Belum pernah memiliki' => 'Belum pernah memiliki',
                ];
                break;
            case 'DKMotorUntuk' :
                $config[ 'data' ] = [
                    'Berdagang'              => 'Berdagang',
                    'Pemakaian jarak dekat'  => 'Pemakaian jarak dekat',
                    'Ke Sekolah / ke Kampus' => 'Ke Sekolah / ke Kampus',
                    'Rekreasi / Olah raga'   => 'Rekreasi / Olah raga',
                    'Kebutuhan Keluarga'     => 'Kebutuhan Keluarga',
                    'Bekerja'                => 'Bekerja',
                ];
                break;
            case 'DKMotorOleh' :
                $config[ 'data' ] = [
                    'Saya sendiri'             => 'Saya sendiri',
                    'Anak'                     => 'Anak',
                    'Pasangan (suami / istri)' => 'Pasangan (suami / istri)',
                ];
                break;
            case 'TujuanUpdate' :
                $config[ 'data' ] = [
                    'STNKTglBayar'             => 'Tgl Ajukan Faktur',
                    'FakturAHMTgl'             => 'Tgl Jadi Faktur',
                    'FakturAHMTglAmbil'        => 'Tgl Jadi Faktur',
                    'STNKTglMasuk'             => 'Tgl Ajukan STNK',
                    'STNKTglJadi'              => 'Tgl Jadi STNK',
                    'STNKTglAmbil'             => 'Tgl Ambil STNK',
                    'Tgl Jadi Notice'          => 'NoticeTglJadi',
                    'Tgl Ambil Notice'         => 'NoticeTglAmbil',
                    'PlatTgl'                  => 'Tgl Jadi Plat',
                    'PlatTglAmbil'             => 'Tgl Ambil Plat',
                    'BPKBTglJadi'              => 'Tgl Jadi BPKB',
                    'BPKBTglAmbil'             => 'Tgl Ambil BPKB',
                    'SRUTTglJadi'              => 'Tgl Jadi SRUT',
                    'SRUTTglAmbil'             => 'Tgl Ambil SRUT',
                ];
                break;
            case 'TC' :
                $config[ 'data' ] = [
                    'TC11'=>	'TC11 - Penjualan Servis',
                    'TC13'=>	'TC13 - Pengeluaran eks Claim C2',
                    'TC14'=>	'TC14 - Pengeluaran eks KPB',
                ];
                break;
            case 'SOTC' :
                $config[ 'data' ] = [
                    'SO'=>	'SO - Sales Order',
                ];
                break;
            case 'POTC' :
                $config[ 'data' ] = [
                    'POHO'=>	'POHO - Purchase Order Hotline',
                    'PORG'=>	'PORG - Purchase Order Reguler',
                ];
                break;
            case 'PSTC' :
                $config[ 'data' ] = [
                    'TC01'=>	'TC01 - Penerimaan dari Supplier MD',
                    'TC02'=>	'TC02 - Penerimaan dari Supplier Lokal',
                    'TC03'=>	'TC03 - Penerimaan Part Robbing',
                    'TC05'=>	'TC05 - Penerimaan dari Dealer Lain',
                    'TC06'=>	'TC06 - Penerimaan Olie KPB',
                    'TC07'=>	'TC07 - Penerimaan Part HO',
                    'TC08'=>	'TC08 - Penerimaan Part Claim',
                    'TC09'=>	'TC09 - Penerimaan Kekurangan Part/Oli',
                    'TC10'=>	'TC10 - Penerimaan Lain Lain',

                ];
                break;
            case 'TCPI' :
                $config[ 'data' ] = [
                    'PIPR'=>	'PIPR',
                    'PIOL'=>	'PIOL',
                ];
                break;
            case 'TCTI' :
                $config[ 'data' ] = [
                    'TI'=>	'TI',
                ];
                break;
            case 'TCTA' :
                $config[ 'data' ] = [
                    'TC04'=>	'TC04 - Penerimaan akibat selisih stock opname (plus)',
                    'TC16'=>	'TC16 - Pengeluaran akibat selisih stock opname (minus)',
                ];
                break;
            case 'SRTC' :
                $config[ 'data' ] = [
                    'SR'=>	'SR - Sales Retur',
                ];
                break;
            case 'KodeTransInvoicePenjualan' :
                if(!isset($config[ 'data' ])) $config[ 'data' ] = [
                    'TC12'=>	'TC12 - Penjualan Counter',
                    'TC20'=>	'TC20 - Pengeluaran Program H1',
                    'TC21'=>	'TC21 - Pengeluaran Program Leasing H1',
                    'TC22'=>	'TC22 - Penjualan Online',
                    'TC23' => 'TC23 - Penjualan Progam Accesories'
                ];
                break;
            case 'KodeTransPenjualanInternal' :
                if(!isset($config[ 'data' ])) $config[ 'data' ] = [
                    'TC17'=>	'TC17 - Pengeluaran ke Dealer Lain'
                ];
                break;
            case 'KodeTrans' :
	            if(!isset($config[ 'data' ])) $config[ 'data' ] = [
                    'TC12'=>	'TC12 - Penjualan Counter',
                    'TC17'=>	'TC17 - Pengeluaran ke Dealer Lain',
                    'TC20'=>	'TC20 - Pengeluaran Program H1',
                    'TC21'=>	'TC21 - Pengeluaran Program Leasing H1',
                    'TC22'=>	'TC22 - Penjualan Online',
                    'TC23' => 'TC23 - Penjualan Progam Accesories'
                ];
                break;
            case 'PenjualanTCPenjualanInternal' :
                if(!isset($config[ 'data' ])) $config[ 'data' ] = [
                    'TC15'=>	'TC15 - Pengeluaran Keperluan Internal',
                    'TC18'=>	'TC18 - Pengembalian Part Robbing ke H1',
                    'TC41' => 'TC41 - Pemindahan part dari H1 - Modal',
                    'TC99'=>	'TC99 - Pengeluaran Lain Lain'
                ];
                break;
            case 'PenjualanTC' :
	            if(!isset($config[ 'data' ])) $config[ 'data' ] = [
                    'TC15'=>	'TC15 - Pengeluaran Keperluan Internal',
                    'TC99'=>	'TC99 - Pengeluaran Lain Lain',
                    'TC18'=>	'TC18 - Pengembalian Part Robbing ke H1',
                    'TC23' => 'TC23 - Penjualan Progam Accesories'
                ];
                break;
            case 'SDTC' :
		    case 'Hubungan' :
	            if(!isset($config[ 'data' ])) $config[ 'data' ] = [
                    'TC11'=>	'TC11 - Penjualan Servis',
                    'TC13'=>	'TC13 - Pengeluaran eks Claim C2',
                    'TC14'=>	'TC14 - Pengeluaran eks KPB',
                ];
                break;
            case 'SETC' :
                $config[ 'data' ] = [
                    'SE'    =>	'SE - Service Estimation',
                ];
                break;
            case 'ServisStatus' :
                $config[ 'data' ] = [
                    'Menunggu'=>	'Menunggu',
                    'Diproses'=>	'Diproses',
                    'Istirahat'=>	'Istirahat',
                    'Selesai'=>	'Selesai',
                    'Lunas'=>	'Lunas',
                ];
                break;
            case 'ASS' :
                $config[ 'data' ] = [
                    'ASS1'=>	'ASS1',
                    'ASS2'=>	'ASS2',
                    'ASS3'=>	'ASS3',
                    'ASS4'=>	'ASS4',
                    'CS'=>	'CS',
                    'HR'=>	'HR',
                    'LS'=>	'LS',
                    'LR'=>	'LR',
                    'JR'=>	'JR',
                    'CLAIMC2'=>	'CLAIMC2',
                    'OPL'=>	'OPL',
                    'OR+'=>	'OR+',
                    'Other'=>	'Other',
                ];
                break;
            case 'HubunganPembawaMotor' :
                $config[ 'data' ] = [
                    'Pemilik'               =>	'Pemilik',
                    'Saya sendiri'          =>	'Saya sendiri',
                    'Anak'                  =>	'Anak',
                    'Pasangan (suami/istri)'=>	'Pasangan (suami/istri)',
                    'Lain-lain'             =>	'Lain-lain',
                    'Teman'                 =>	'Teman',
                    'Atasan-Bawahan'        =>	'Atasan-Bawahan',
                    'Suami'                 =>	'Suami',
                    'Istri'                 =>	'Istri',
                    'Ayah'                  =>	'Ayah',
                    'Ibu'                   =>	'Ibu',
                    'Kakak'                 =>	'Kakak',
                    'Adik'                  =>	'Adik',
                    'Saudara'               =>	'Saudara',
                    'Paman'                 =>	'Paman',
                    'Bibi'                  =>	'Bibi',
                ];
                break;
            case 'SDAlasanServis' :
                $config[ 'data' ] = [
                    'Ingat Sendiri'         =>	'Ingat Sendiri',
                    'Iklan'                 =>	'Iklan',
                    'Sms'                   =>	'Sms',
                    'Stiker Remainder'      =>	'Stiker Remainder',
                    'Telepon'               =>	'Telepon',
                    'Walk in non Promo'     =>	'Walk in non Promo',
                    'Pit Express in house'  =>	'Pit Express in house',
                    'Pit Express Tenda'     =>	'Pit Express Tenda',
                    'AHASS Event'           =>	'AHASS Event',
                    'SV. Door to doos'      =>	'SV. Door to doos',
                    'SV. GC/Public Area'    =>	'SV. GC/Public Area',
                    'SV. Joint Dealer'      =>	'SV. Joint Dealer',
                    'SV. Pos Servis'        =>	'SV. Pos Servis',
                ];
                break;
            default:
                break;
        }
        if ( $form ) {
            $combo = $form->field( $model, $name,
                [ 'template' => ( $label ? '{label}{input}' : '{input}' ) ] )
                ->widget( Select2::class, $config );
            if ( $label ) {
                $labelParams = [];
                if ( $labelStyle ) {
                    $labelParams[ 'style' ] = $labelStyle;
                }
                $combo->label( $label, $labelParams );
            } else {
                $combo->label( false );
            }
        } else {
            $config = array_merge( $config, [ 'name' => $name, ] );
            $combo  = Select2::widget( $config );
        }
        return $combo;
    }
	static function integerInput( $params = [] ) {
		$default = [
			'config' =>[
				'maskedInputOptions'=>[
					'integerDigits'=> 5,
					'digits' => 0 ,
				]
			]
		];
		return self::numberInput(array_merge_recursive($default,$params));
	}
	static function decimalInput( $params = [] ) {
		$default = [
			'config' =>[
				'maskedInputOptions'=>[
					'integerDigits'=> 14,
					'digits' => 2 ,
				]
			]
		];
		return self::numberInput(array_merge_recursive($default,$params));
	}
    /* --- NUMBER INPUT --- */
    static function numberInput( $params = [] ) {
        $config = isset( $params[ 'config' ] ) ? $params[ 'config' ] : [];
        $name   = isset( $params[ 'name' ] ) ? $params[ 'name' ] : null;
        $form   = isset( $params[ 'form' ] ) ? $params[ 'form' ] : null;
        /* default configs */
        $config[ 'maskedInputOptions' ] = isset( $config[ 'maskedInputOptions' ] ) ? array_merge( [ 'groupSeparator' => '.', 'radixPoint' => ',', 'digits' => 2, 'digitsOptional'=> false ], $config[ 'maskedInputOptions' ] ) :
	        [ 'groupSeparator' => '.', 'radixPoint' => ',', 'digits' => 2 , 'digitsOptional'=> false];
        if ( array_key_exists( 'displayOptions', $config ) ) {
            if ( array_key_exists( 'class', $config[ 'displayOptions' ] ) && ! strpos( $config[ 'displayOptions' ][ 'class' ], 'form-control' ) ) {
                $config[ 'displayOptions' ][ 'class' ] .= ' form-control';
            }
        } else {
            $config[ 'displayOptions' ] = [ 'class' => 'form-control' ];
        }
	    $config['maskedInputOptions']['removeMaskOnSubmit'] = true;
	    $config['maskedInputOptions']['autoGroup'] = true;
	    $config['maskedInputOptions']['autoUnmask'] = true;
	    $config['maskedInputOptions']['allowMinus'] = true;
        if ( $form ) {
            /* by model */
            $model      = isset( $params[ 'model' ] ) ? $params[ 'model' ] : null;
            $label      = isset( $params[ 'label' ] ) ? $params[ 'label' ] : null;
            $labelStyle = isset( $params[ 'labelStyle' ] ) ? $params[ 'labelStyle' ] : null; // replace null with default style
            $field      = $form->field( $model, $name,
                [ 'template' => ( $label ? '{label}{input}' : '{input}' ) ] )
                ->widget( NumberControl::class, $config );
            if ( $label ) {
                $labelParams = [];
                if ( $labelStyle ) {
                    $labelParams[ 'style' ] = $labelStyle;
                }
                $field->label( $label, $labelParams );
            } else {
                $field->label( false );
            }
        } else {
            /* non model */
            if ( ! isset( $config[ 'id' ] ) ) {
                $config[ 'id' ] = $name;
            }
            $config = array_merge( $config, [ 'name' => $name, ] );
            $field  = NumberControl::widget( $config );
        }
        return $field;
    }
    /* --- DATE INPUT --- */
	static function timeInput( $params = [] ) {
		$config = isset( $params[ 'config' ] ) ? $params[ 'config' ] : [];
		$name   = isset( $params[ 'name' ] ) ? $params[ 'name' ] : null;
		$form   = isset( $params[ 'form' ] ) ? $params[ 'form' ] : null;
		if(isset($params['config']['value'])){
			$config['value'] = date('H:i:s', strtotime($params['config']['value']));
		}
		$config[ 'pluginOptions' ] = isset( $config[ 'pluginOptions' ] ) ? array_merge( ['showSeconds' => true, 'showMeridian' => false, 'template' => false], $config[ 'pluginOptions' ] ) :
			[ 'showSeconds' => true, 'showMeridian' => false, 'template' => false];
		$config['addon'] = '';
		if ( $form ) {
			/* by model */
			$model      = isset( $params[ 'model' ] ) ? $params[ 'model' ] : null;
			$label      = isset( $params[ 'label' ] ) ? $params[ 'label' ] : null;
			$labelStyle = isset( $params[ 'labelStyle' ] ) ? $params[ 'labelStyle' ] : null; // replace null with default style
			$field      = $form->field( $model, $name,
				[ 'template' => ( $label ? '{label}{input}' : '{input}' ) ] )
			                   ->widget( TimePicker::className(), $config );
			if ( $label ) {
				$labelParams = [];
				if ( $labelStyle ) {
					$labelParams[ 'style' ] = $labelStyle;
				}
				$field->label( $label, $labelParams );
			} else {
				$field->label( false );
			}
		} else {
			/* non model */
			$config = array_merge( $config, [ 'name' => $name, ] );
			$field  = TimePicker::widget( $config );
		}
		return $field;
	}
    static function dateInput( $params = [] ) {
        $config = isset( $params[ 'config' ] ) ? $params[ 'config' ] : [];
        $name   = isset( $params[ 'name' ] ) ? $params[ 'name' ] : null;
        $form   = isset( $params[ 'form' ] ) ? $params[ 'form' ] : null;
        /* default configs */
		if(isset($config['readonly']) && $config['readonly']){
			$config['disabled'] = true;
		}
        if ( ! array_key_exists( 'type', $config ) ) {
            $config[ 'type' ] = DateControl::FORMAT_DATE;
        }
	    $config[ 'pluginOptions' ] = isset( $config[ 'pluginOptions' ] ) ? array_merge( [ 'todayHighlight' => true,'todayBtn' => true,'autoclose' => true, ], $config[ 'pluginOptions' ] ) :
		    [ 'todayHighlight' => true,'todayBtn' => true,'autoclose' => true];
        if ( $form ) {
            /* by model */
            $model      = isset( $params[ 'model' ] ) ? $params[ 'model' ] : null;
            $label      = isset( $params[ 'label' ] ) ? $params[ 'label' ] : null;
            $labelStyle = isset( $params[ 'labelStyle' ] ) ? $params[ 'labelStyle' ] : null; // replace null with default style
            $field      = $form->field( $model, $name,
                [ 'template' => ( $label ? '{label}{input}' : '{input}' ) ] )
                ->widget( DateControl::class, $config );
            if ( $label ) {
                $labelParams = [];
                if ( $labelStyle ) {
                    $labelParams[ 'style' ] = $labelStyle;
                }
                $field->label( $label, $labelParams );
            } else {
                $field->label( false );
            }
        } else {
            /* non model */
            $config = array_merge( $config, [ 'name' => $name, ] );
            $field  = DateControl::widget( $config );
        }
        return $field;
    }
}