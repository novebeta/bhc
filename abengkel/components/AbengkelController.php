<?php
namespace abengkel\components;
use common\components\Custom;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
class AbengkelController extends Controller {
	public $isAllow = 0;

	public function beforeAction( $action ) {
		try {
			if ( ! parent::beforeAction( $action ) ) {
				return false;
			}
			if(Yii::$app->session->get( '_nav' ) == null){
				return $this->redirect( Custom::urllgn( 'site/dealer' ) );
			}
			if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] === 'create' ) {
				TUi::$actionMode = Menu::ADD;
			}
			if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] === 'update' ) {
				TUi::$actionMode = Menu::UPDATE;
			}
			if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] === 'view' ) {
				TUi::$actionMode = Menu::VIEW;
			}
			if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] === 'display' ) {
				TUi::$actionMode = Menu::DISPLAY;
			}
			if ( ! Yii::$app->user->isGuest ) {
				if ( Yii::$app->session[ 'userSessionTimeout' ] < time() ) {
					return $this->redirect( Custom::urllgn( 'site/logout' ) );
				} else {
					if ( $action->id != 'waktu' ) {
						Yii::$app->session->set( 'userSessionTimeout', time() + Yii::$app->params[ 'sessionTimeoutSeconds' ] );
					}
					return true;
				}
			} else {
				return $this->redirect( Custom::urllgn( 'site/logout' ) );
			}
		} catch ( BadRequestHttpException $e ) {
			return false;
		}
	}
	public function render($view, $params = []){
		if(YII_DEBUG){
			Yii::debug(VarDumper::dumpAsString($params));
		}
		return parent::render($view,$params);
	}
	protected function findModelBase64( $modelName, $id ) {
		try {
			$className  = '\abengkel\models\\' . $modelName;
			$modelObj   = Yii::createObject( [
				'class' => $className,
			] );
			$primaryKey = $modelObj::getTableSchema()->primaryKey;
			$fields     = $primaryKey;
			$condition  = [];
			$val        = explode( "||", base64_decode( $id ) );
			for ( $i = 0; $i < count( $fields ) && $i < count( $val ); $i ++ ) {
				$condition[ $fields[ $i ] ] = $val[ $i ];
			}
			return $modelObj::findOne( $condition );
		} catch ( InvalidConfigException $e ) {
			return null;
		}
	}
	/**
	 * @param yii\db\ActiveRecord $model
	 *
	 * @return string
	 */
	protected function generateIdBase64FromModel( $model ) {
		if($model == null){
			$message = "<h3>Data tidak ditemukan.</h3>";
			$message .= "</br>Controller : ".Yii::$app->controller->id;
			$message .= "</br>Action : ".Yii::$app->controller->action->id;
			throw new UserException($message);
		}
		$modelName   = get_class( $model );
		$primaryKeys = $modelName::getTableSchema()->primaryKey;
		return base64_encode( implode( '||', $model->getAttributes( $primaryKeys ) ) );
	}
	protected function decodeIdBase64( $modelName, $id ) {
		$className  = '\abengkel\models\\' . $modelName;
		$modelObj   = Yii::createObject( [
			'class' => $className,
		] );
		$primaryKey = $modelObj::getTableSchema()->primaryKey;
		$fields     = $primaryKey;
		$condition  = [];
		$val        = explode( "||", base64_decode( $id ) );
		for ( $i = 0; $i < count( $fields ) && $i < count( $val ); $i ++ ) {
			$condition[ $fields[ $i ] ] = $val[ $i ];
		}
		return $condition;
	}
	/**
	 * @param yii\db\ActiveRecord $model
	 *
	 * @return string
	 */
	protected function modelErrorSummary( $model ) {
		return \yii\helpers\Html::errorSummary( $model );
	}
	/**
	 * @param bool $success
	 * @param string $message
	 * @param array $data
	 *
	 * @return array
	 */
	protected function responseJson( $success, $message, $data = [] ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$res = [
			'success' => $success,
			'message' => $message,
		];
		return count( $data ) ? array_merge( $res, $data ) : $res;
	}
	/**
	 * @param string $message
	 * @param array $data
	 *
	 * @return array
	 */
	protected function responseSuccess( $message, $data = [] ) {
		return $this->responseJson( true, $message, $data );
	}
	/**
	 * @param string $message
	 *
	 * @return array
	 */
	protected function responseFailed( $message ) {
		return $this->responseJson( false, $message );
	}
	/**
	 * @param array $data
	 *
	 * @return array
	 */
	protected function responseDataJson( $data = [], $count = false ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return [
			'success'   => true,
			'rows'      => $data,
			'rowsCount' => $count ? $count : count( $data )
		];
	}
	protected function KodeAkses() {
		return Menu::getUserLokasi()[ 'KodeAkses' ];
	}
	protected function UserID() {
		return Menu::getUserLokasi()[ 'UserID' ];
	}
	protected function LokasiKode() {
		return Menu::getUserLokasi()[ 'PosKode' ];
	}
	const MODE_SELECT = 'select';
	const MODE_SELECT_MULTISELECT = 'select,multi-select';
	const MODE_SELECT_SHOW_DETAIL = 'select,show-detail';
	const MODE_SELECT_SHOW_DETAIL_MULTISELECT = 'select,show-detail,multi-select';
	const MODE_SELECT_DETAIL = 'select-detail';
	const MODE_SELECT_DETAIL_MULTISELECT = 'select-detail,multi-select';
	/**
	 * @return bool
	 */
	protected function isPopupForm() {
		return ( isset( $_GET[ 'mode' ] ) && $_GET[ 'mode' ] === 'popup-form' );
	}
	protected function setLayoutForPopupForm() {
		if ( isset( $_GET[ 'mode' ] ) && $_GET[ 'mode' ] === 'popup-form' ) {
			$this->layout = $_GET[ 'mode' ];
		}
	}
	/**
	 * @param array|string $data
	 *
	 * @return string
	 */
	protected function responseToPopupForm( $data ) {
		$this->layout = 'popup-form-return';
		return $this->renderContent( is_array( $data ) ? json_encode( $data ) : ( is_string( $data ) ? "'" . $data . "'" : $data ) );
	}
}