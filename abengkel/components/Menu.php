<?php

namespace abengkel\components;

use common\components\Custom;
use common\models\User;
use Yii;
use yii\base\Component;
use yii\db\Query;
use yii\helpers\ArrayHelper;

define("ATTRIBUTE", [
    'System'                        => ['class' => 'fa fa-cogs', 'url' => '#'],
    'Login Dealer'                  => ['class' => 'fa fa-check-circle', 'url' => Custom::urllgn('site/dealer&action=display')],
    'Scan'                          => ['class' => '', 'url' => Custom::url('site/scan&action=update')],
    'Broadcast Message'                  => ['class' => '', 'url' => Custom::url('site/send-message&action=update')],
    'Hak Akses'                     => ['class' => 'fa fa-check-circle', 'url' => Custom::url('tuakses/index&action=display')],
    'User Password'                 => ['class' => 'fa fa-check-circle', 'url' => Custom::url('tuser/index&action=display')],
    'User Profile'                  => ['class' => 'fa fa-check-circle', 'url' => Custom::url('tuser/update&action=display')],
    'Dealer Profile'                => ['class' => 'fa fa-check-circle', 'url' => Custom::url('tzcompany/update&action=display')],
    'Backup - Restore'              => ['class' => '', 'url' => Custom::url('site/backup-restore&action=display')],
    'Re-Login'                      => ['class' => 'fa fa-check-circle', 'url' => Custom::urllgn('site/logout&action=display')],
    'Keluar'                        => ['class' => 'fa fa-check-circle', 'url' => Custom::urllgn('site/blank&action=display')],
    'Data'                          => ['class' => 'fa fa-database', 'url' => '#'],
    'Jasa'                          => ['class' => 'fa fa-check-circle', 'url' => Custom::url('tdjasa/index&action=display')],
    'Spare Part'                    => ['class' => 'fa fa-check-circle', 'url' => Custom::url('tdbarang/index&action=display')],
    'Update Standar Cost'           => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttuchd/index&action=display')],
    'Update Harga Jual (HET)'       => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttuhhd/index&action=display')],
    'Update Harga Jasa'             => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttujhd/index&action=display')],
    'Gudang'                        => ['class' => 'fa fa-check-circle', 'url' => Custom::url('tdlokasi/index&action=display')],
    'POS'                           => ['class' => 'fa fa-check-circle', 'url' => Custom::url('tdpos/index&action=display')],
    'Dealer'                        => ['class' => 'fa fa-check-circle', 'url' => Custom::url('tddealer/index&action=display')],
    'AHASS'                         => ['class' => 'fa fa-check-circle', 'url' => Custom::url('tdahass/index&action=display')],
    'Supplier'                      => ['class' => 'fa fa-check-circle', 'url' => Custom::url('tdsupplier/index&action=display')],
    'Karyawan'                      => ['class' => 'fa fa-check-circle', 'url' => Custom::url('tdkaryawan/index&action=display')],
    'Konsumen'                      => ['class' => 'fa fa-check-circle', 'url' => Custom::url('tdcustomer/index&action=display')],
    'Type Motor'                    => ['class' => 'fa fa-check-circle', 'url' => Custom::url('tdmotortype/index&action=display')],
    'Area'                          => ['class' => 'fa fa-check-circle', 'url' => Custom::url('tdarea/index&action=display')],
    'Program'                       => ['class' => 'fa fa-check-circle', 'url' => Custom::url('tdprogramhd/index&action=display')],
    'Servis'                        => ['class' => 'fa fa-user-cog', 'url' => '#'],
    '[SD] Daftar Servis'            => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttsdhd/daftar-servis&action=display')],
    '[SV] Invoice Servis'           => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttsdhd/invoice-servis&action=display')],
    '[SE] Estimasi Servis'          => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttsehd/index&action=display')],
    '[CK] Klaim KPB'                => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttckhd/index&action=display')],
    '[CC] Klaim C2'                 => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttcchd/index&action=display')],
    '[AN] Panggil Antrian'          => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttsdhd/panggil-antrian&action=update')],
    '[SM] Start Stop Mekanik'       => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttsdhd/start-stop-mekanik&action=update')],
    '[LA] List Antrian'             => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttsdhd/list-antrian&action=display')],
    'Penjualan'                     => ['class' => 'fa fa-chart-line', 'url' => '#'],
    '[SO] Order Penjualan'          => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttsohd/index&action=display')],
    '[SI] Invoice Penjualan'        => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttsihd/invoice-penjualan&action=display')],
    '[SI] Penjualan Internal'       => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttsihd/penjualan-internal&action=display')],
    '[SI] Mutasi Internal'          => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttsihd/mutasi-internal&action=display')],
    '[SR] Retur Penjualan'          => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttsrhd/index&action=display')],
    'Pembelian'                     => ['class' => 'fa fa-shopping-cart', 'url' => '#'],
    '[PO] Order Pembelian'          => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttpohd/index&action=display')],
    '[PS] Penerimaan Barang'        => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttpshd/index&action=display')],
    '[PI] Invoice Pembelian'        => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttpihd/index&action=display')],
    '[PR] Retur Pembelian'          => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttprhd/index&action=display')],
    'Mutasi'                        => ['class' => 'fa fa-sync', 'url' => '#'],
    '[TI] Transfer'                 => ['class' => 'fa fa-check-circle', 'url' => Custom::url('tttihd/index&action=display')],
    '[TA] Penyesuaian'              => ['class' => 'fa fa-check-circle', 'url' => Custom::url('tttahd/index&action=display')],
    'Mutasi Eksternal ke Dealer'    => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttsd/mutasi-eksternal-dealer&action=display')],
    'Invoice Eksternal'             => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttsd/invoice-eksternal&action=display')],
    'Penerimaan dari Dealer'        => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttpd/penerimaan-dealer&action=display')],
    'Invoice Penerimaan'            => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttpd/invoice-penerimaan&action=display')],
    'Keuangan'                      => ['class' => 'fa fa-file-invoice-dollar', 'url' => '#'],
    'Kas Masuk Umum'                => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttkmhd/kas-masuk-umum&action=display')],
    'Kas Masuk Dari Bank'           => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttkmhd/kas-masuk-dari-bank&action=display')],
    'KM SV - Servis'                => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttkmhd/kas-masuk-service-invoice&action=display')],
    'KM SI - Penjualan'             => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttkmhd/kas-masuk-sales-invoice&action=display')],
    'KM SE - Estimasi Servis'       => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttkmhd/kas-masuk-estimasi-servis&jenis=kas&action=display')],
    'KM SO - Order Penjualan'       => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttkmhd/kas-masuk-order-penjualan&action=display')],
    'KM PR - Retur Pembelian'       => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttkmhd/kas-masuk-retur-pembelian&action=display')],
    'Kas Keluar Umum'               => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttkkhd/kas-keluar-umum&action=display')],
    'Kas Keluar Ke Bank'            => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttkkhd/kas-keluar-ke-bank&action=display')],
    'Kas Keluar Ke H1'              => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttkkhd/kas-keluar-ke-h1&action=display')],
    'KK PI - Pembelian'             => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttkkhd/kas-keluar-pembelian&action=display')],
    'KK PL - Order Pengerjaan Luar' => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttkkhd/kas-keluar-order-pengerjaan-luar&action=display')],
    'KK SR - Retur Penjualan'       => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttkkhd/kas-keluar-retur-penjualan&action=display')],
    'Transfer Antar Kas'            => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttkkhd/transfer-antar-kas&action=display')],
    'Bank Masuk Dari H1'            => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttbmhd/bank-masuk-dari-h1&action=display')],
    'Bank Masuk Umum'               => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttbmhd/bank-masuk-umum&action=display')],
    'Bank Masuk Dari Kas'           => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttbmhd/bank-masuk-dari-kas&action=display')],
    'BM SV - Servis'                => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttbmhd/bank-masuk-service-invoice&action=display')],
    'BM SI - Penjualan'             => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttbmhd/bank-masuk-penjualan&action=display')],
    'BM SE - Estimasi'              => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttbmhd/bank-masuk-estimasi&action=display')],
    'BM SO - Order Penjualan'       => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttbmhd/bank-masuk-order-penjualan&action=display')],
    'BM PR - Retur Pembelian'       => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttbmhd/bank-masuk-retur-pembelian&action=display')],
    'BM Klaim KPB'                  => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttbmhd/bank-masuk-klaim-kpb&action=display')],
    'BM Klaim C2'                   => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttbmhd/bank-masuk-klaim-c2&action=display')],
    'Bank Keluar Umum'              => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttbkhd/bank-keluar-umum&action=display')],
    'Bank Keluar Ke Kas'            => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttbkhd/bank-keluar-ke-kas&action=display')],
    'BK PI - Pembelian'             => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttbkhd/bank-keluar-pembelian&action=display')],
    'BK PL - Order Pengerjaan Luar' => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttbkhd/bank-keluar-order-pengerjaan-luar&action=display')],
    'BK SR - Retur Penjualan'       => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttbkhd/bank-keluar-retur-penjualan&action=display')],
    'Transfer Antar Bank'           => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttbkhd/transfer-antar-bank&action=display')],
    'Push INV2'                     => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'site/push-inv2&action=display' ) ],
    'Akuntansi'                     => ['class' => 'fa fa-calculator', 'url' => '#'],
    'Jurnal Memorial'               => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttgeneralledgerhd/memorial&action=display')],
    'Jurnal Adjustment'             => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttgeneralledgerhd/adjustment&action=display')],
    'Jurnal Transaksi'              => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttgeneralledgerhd/transaksi&action=display')],
    'Jurnal Pemindah Bukuan'        => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttgeneralledgerhd/pindahbuku&action=display')],
    'Data Perkiraan'                => ['class' => 'fa fa-check-circle', 'url' => Custom::url('traccount/data-perkiraan&action=display')],
    'Edit Perkiraan'                => ['class' => 'fa fa-check-circle', 'url' => Custom::url('traccount/index&action=display')],
    'Neraca Awal'                   => ['class' => 'fa fa-check-circle', 'url' => Custom::url('traccount/neraca-awal&action=display')],
    'Tutup Buku'                    => ['class' => 'fa fa-check-circle', 'url' => Custom::url('tsaldoaccount/index&action=update')],
    'Validasi Jurnal'               => ['class' => 'fa fa-check-circle', 'url' => Custom::url('ttgeneralledgerhd/validasi-jurnal&action=update')],
    'Memorial Penyusutan'           => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'trmp/index&action=display' ) ],
    'Dashboard'                     => ['class' => 'glyphicon glyphicon-dashboard', 'url' => '#'],
    'Dashboard Akunting'            => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/dashboard-akunting&action=display')],
//    'Dashboard Marketing'           => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/dashboard-marketing&action=display')],
    'Dashboard Marketing'           => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/dashboard-cdb&action=display')],
    'Dashboard Akunting Konsolidasi'       => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/dashboard-akukonsol&action=display' ) ],
    'Laporan Dashboard'             => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/dashboard-cdb&action=display' ) ],
    'Marketing'                     => ['class' => 'fa fa-poll', 'url' => '#'],
    'Laporan'                       => ['class' => 'fa fa-book-reader', 'url' => '#'],
    'Daftar [SD] Servis Draft'      => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/daftar-servis-draft&action=display')],
    'Daftar [SV] Invoice Servis'    => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/daftar-invoice-servis&action=display')],
    'Daftar [SE] Estimasi Servis'   => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/daftar-estimasi-servis&action=display')],
    'Daftar [CK] Klaim KPB'         => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/daftar-klaim-kpb&action=display')],
    'Daftar [CC] Klaim C2'          => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/daftar-klaim-c2&action=display')],
    'Daftar [PL] Picking List'      => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/daftar-picking-list&action=display')],
    'Daftar [SO] Order Penjualan'   => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/daftar-order-penjualan&action=display')],
    'Daftar [SI] Invoice Penjualan' => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/daftar-invoice-penjualan&action=display')],
    'Daftar [SR] Retur Penjualan'   => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/daftar-retur-penjualan&action=display')],
    'Daftar [PO] Order Pembelian'   => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/daftar-order-pembelian&action=display')],
    'Daftar [PS] Penerimaan Barang' => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/daftar-penerimaan-barang&action=display')],
    'Daftar [PI] Invoice Pembelian' => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/daftar-invoice-pembelian&action=display')],
    'Daftar [PR] Retur Pembelian'   => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/daftar-retur-pembelian&action=display')],
    'Daftar [TI] Transfer'          => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/daftar-transfer&action=display')],
    'Daftar [TA] Penyesuaian'       => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/daftar-penyesuaian&action=display')],
    'Daftar Kas'                    => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/daftar-kas&action=display')],
    'Daftar Bank'                   => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/daftar-bank&action=display')],
    'Jurnal Umum'                   => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/jurnal-umum&action=display')],
    'Neraca Percobaan'              => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/neraca-percobaan&action=display')],
    'Neraca dan Rugi Laba'          => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/neraca-rugi-laba&action=display')],
    'Laporan Harian'                => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/laporan-harian&action=display')],
    'Laporan Bulanan'               => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/laporan-bulanan&action=display')],
    'Laporan Piutang'               => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/laporan-piutang&action=display')],
    'Laporan Hutang'                => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/laporan-hutang&action=display')],
    'Item Stock'                    => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/item-stock&action=display')],
    'Kartu Stock'                   => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/kartu-stock&action=display')],
    'Item Grade'                   => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/item-grade&action=display')],
    'Daftar Pivot Tabel Mekanik'    => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/daftar-pivot-tabel-mekanik&action=display')],
    'Daftar RLH Mekanik'            => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/daftar-rlh-mekanik&action=display')],
    'Daftar Start Stop Mekanik'     => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/daftar-start-stop-mekanik&action=display')],
    'Laporan Query'                 => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/laporan-query&action=display')],
    'Laporan Data'                  => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/laporan-data&action=display')],
    'Laporan User Log'              => ['class' => 'fa fa-check-circle', 'url' => Custom::url('report/laporan-user-log&action=display')],
    //    'Validasi'                      => [ 'class' => 'fa fa-unlink', 'url' => '#' ],
]);
class Menu extends Component
{
    const ADD     = 'MenuTambah';
    const UPDATE  = 'MenuEdit';
    const DELETE  = 'MenuHapus';
    const print   = 'MenuCetak';
    const VIEW    = 'MenuView';
    const DISPLAY = 'MenuDisplay';
    public static function akses($menuText, $akses)
    {
        $akses_ = Yii::$app->session->get('_akses');
        if ($akses === self::DISPLAY) {
            return (array_key_exists($menuText, $akses_) ? 1 : 0);
        }
        return ArrayHelper::getValue($akses_, [$menuText, $akses], 0);
    }
    public static function val($menu, $tipe)
    {
        return ArrayHelper::getValue(ATTRIBUTE, "$menu.$tipe", ($tipe == 'url' ? '#' : ''));
    }
    public static function getMenu()
    {
        $id = \Yii::$app->user->id;
        /** @var User $user */
        $user      = User::findOne($id);
        $kodeAkses = $user->KodeAkses;
        try {
            $arr = Yii::$app->db->createCommand("select
				REPLACE(MenuInduk,'&','') AS MenuInduk,
				REPLACE(MenuText,'&','') AS MenuText,
				MenuTambah, MenuEdit,MenuHapus,MenuCetak,MenuView
				FROM tuakses WHERE KodeAkses = :kodeAkses AND MenuStatus = 1
				ORDER BY MenuInduk,MenuNo;", [':kodeAkses' => $kodeAkses])
                ->queryAll();
            if ($arr == null) {
                return '';
            }
            array_unshift($arr, [
                'MenuInduk'  => 'System',
                'MenuText'   => 'Scan',
                'MenuTambah' => '1',
                'MenuEdit'   => '1',
                'MenuHapus'  => '1',
                'MenuCetak'  => '1',
                'MenuView'   => '1',
            ]);
            $result = ArrayHelper::map($arr, 'MenuTambah', 'MenuView', 'MenuText');
            foreach ($arr as $value) {
                $result[$value['MenuText']] = $value;
            }
            Yii::$app->session->set('_akses', $result);
            $menu              = [];
            $menu['System']    = self::createMenu('System', $arr);
            $menu['Data']      = self::createMenu('Data', $arr);
            $menu['Servis']    = self::createMenu('Servis', $arr);
            $menu['Penjualan'] = self::createMenu('Penjualan', $arr);
            $menu['Pembelian'] = self::createMenu('Pembelian', $arr);
            $menu['Mutasi']    = self::createMenu('Mutasi', $arr);
            $menu['Keuangan']  = self::createMenu('Keuangan', $arr);
            $menu['Akuntansi'] = self::createMenu('Akuntansi', $arr);
            $menu['Marketing'] = self::createMenu('Marketing', $arr);
            $menu['Dashboard'] = self::createMenu('Dashboard', $arr);
            $menu['Laporan']   = self::createMenu('Laporan', $arr);
            //            $menu['Validasi']  = self::createMenu( 'Validasi', $arr );
            $menuBuilder = '';
            foreach ($menu as $k => $item) {
                if (isset($result[$k]) && $item != '') {
                    //                    if ( $item != '' ) {
                    $menuBuilder .= '<li class="treeview">
                                <a href="' . self::val($k, 'url') . '">
                                    <i class="' . self::val($k, 'class') . '"></i> <span>' . $k . '</span>
                                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i></span>
                                </a>
                                <ul class="treeview-menu">' . $item . '</ul>
                           </li>';
                    //                    }
                }
            }
            return $menuBuilder;
        } catch (Exception $e) {
        }
    }
    public static function createMenu($parent, &$arr)
    {
        $menu = '';
        foreach ($arr as $k => $m) {
            if ($m['MenuText'] === 'Saldo Kas - Bank') {
                continue;
            }
            if ($m['MenuInduk'] == $parent) {
                $child = self::createMenu($m['MenuText'], $arr);
                if ($child == '') {
                    if ($m['MenuText'] == '-') {
                        $menu .= '<li class="divider"></li>';
                    } else {
                        $menu .= '<li><a href="' . self::val($m['MenuText'], 'url') . '"><i class="fa fa-check-circle"></i> ' . $m['MenuText'] . '</a></li>';
                    }
                } else {
                    $menu .= '<li class="treeview">
                                <a href="' . self::val($k, 'url') . '">
                                    <i class="' . self::val($k, 'class') . '"></i> <span>' . $m['MenuText'] . '</span>
                                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i></span>
                                </a>
                                <ul class="treeview-menu">' . $child . '</ul>
                           </li>';
                }
                unset($arr['$k']);
            }
        }
        return $menu;
    }
    public static function getUserLokasi()
    {
        $q = new Query();
        return $q->select("UserID, KodeAkses, PosKode, PosNama, PosAlamat, PosTelepon, PosNomor, PosStatus, NoAccount")
            ->from("tuser")
            ->leftJoin("tdpos", "tuser.LokasiKode = tdpos.PosKode")
            ->where(['UserID' => Yii::$app->user->id])
            ->one();
    }

    public static function showOnlyHakAkses($Akses = [], $returnTrue = null, $returnFalse = null)
    {
        if (in_array_insensitive(self::getUserLokasi()['KodeAkses'], $Akses)) {
            if ($returnTrue !== null) {
                return $returnTrue;
            } else {
                return true;
            }
        } else {
            if ($returnFalse !== null) {
                return $returnFalse;
            } else {
                return false;
            }
        }
    }
}
