<?php
namespace common\components;
use aunit\models\Tzcompany;
use common\models\User;
use DateTime;
use Yii;
use yii\base\Component;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Url;
/**
 *
 * @property array $company
 */
class Custom extends Component {
	static public function getDsnAttribute( $name, $dsn ) {
		if ( preg_match( '/' . $name . '=([^;]*)/', $dsn, $match ) ) {
			return $match[ 1 ];
		} else {
			return null;
		}
	}
	static public function createSearch( $options ) {
		$opt = array_merge( [
			'class' => 'form-control',
		], $options );
		return Html::dropDownList( 'cabang', null, [
			'right'   => 'Right',
			'left'    => 'Left',
			'both'    => 'Both',
			'exactly' => 'Exactly',
			'not'     => 'Not',
		], $opt );
	}
	static public function getCompany() {
		/** @var Tzcompany $comp */
		$comp = Tzcompany::find()->one();
		if ( $comp != null ) {
			return [
				'nama'    => $comp->PerusahaanNama,
				'slogan'  => $comp->PerusahaanSlogan,
				'alamat'  => $comp->PerusahaanAlamat,
				'telepon' => $comp->PerusahaanTelepon,
			];
		}
		return [
			'nama'    => '',
			'slogan'  => '',
			'alamat'  => '',
			'telepon' => '',
		];
	}
	static public function getKodeAkses() {
		/** @var User $user */
		$user = User::findOne( Yii::$app->user->id );
		return $user->KodeAkses;
	}
	static function Encript( string $MyString ) {
		$s = self::DoXor( $MyString );
		$s = self::Stretch( $s );
		return $s;
	}
	/** BEGIN - SECTION ENCRIPTION */
	static function DoXor( $mstrText ) {
		$lngC = $intB = $lngN = 0;
		for ( $lngN = 1; ( $lngN <= strlen( $mstrText ) ); $lngN ++ ) {
			$lngC                      = ord( substr( $mstrText, ( $lngN - 1 ), 1 ) );
			$intB                      = Intval( ( 3 * 256 ) );
			$mstrText[ ( $lngN - 1 ) ] = Chr( $lngC ^ $intB );
		}
		return $mstrText;
	}
	static function Stretch( $mstrText ) {
		$lngC = $lngN = $lngJ = $lngK = $lngA = 0;
		$strB = '';
		$lngA = strlen( $mstrText );
		$strB = str_repeat( ' ', $lngA + floor( ( $lngA + 2 ) / 3 ) );
		for ( $lngN = 1; ( $lngN <= $lngA ); $lngN ++ ) {
			$lngC              = ord( substr( $mstrText, ( $lngN - 1 ), 1 ) );
			$lngJ              = ( $lngJ + 1 );
			$strB[ ( $lngJ ) ] = Chr( ( $lngC & 63 ) + 59 );
			switch ( ( $lngN % 3 ) ) {
				case 1:
					$lngK = $lngK | ( floor( $lngC / 64 ) * 16 );
					break;
				case 2:
					$lngK = $lngK | ( floor( $lngC / 64 ) * 4 );
					break;
				case 0:
					$lngK          = $lngK | floor( $lngC / 64 );
					$lngJ          = $lngJ + 1;
					$strB[ $lngJ ] = Chr( $lngK + 59 );
					$lngK          = 0;
					break;
			}
		}
		if ( ( $lngA % 3 ) ) {
			$lngJ          = $lngJ + 1;
			$strB[ $lngJ ] = Chr( $lngK + 59 );
		}
		return trim( $strB );
	}
	static function Decript( string $MyString ) {
		$s = self::Shrink( $MyString );
		$s = self::DoXor( $s );
		return $s;
	}
	static function Shrink( $mstrText ) {
		$lngC = $lngD = $lngE = $lngA = $lngB = $lngN = $lngJ = $lngK = 0;
		$strB = '';
		$lngA = strlen( $mstrText );
		$lngB = $lngA - 1 - floor( ( $lngA - 1 ) / 4 );
		$strB = str_repeat( ' ', $lngB );
		for ( $lngN = 1; ( $lngN <= $lngB ); $lngN ++ ) {
			$lngJ = ( $lngJ + 1 );
			$lngC = ord( substr( $mstrText, $lngJ - 1, 1 ) ) - 59;
			switch ( ( $lngN % 3 ) ) {
				case 1:
					$lngK = ( $lngK + 4 );
					if ( ( $lngK > $lngA ) ) {
						$lngK = $lngA;
					}
					$lngE = ord( substr( $mstrText, $lngK - 1, 1 ) ) - 59;
					$lngD = ( floor( $lngE / 16 ) & 3 ) * 64;
					break;
				case 2:
					$lngD = ( floor( $lngE / 4 ) & 3 ) * 64;
					break;
				case 0:
					$lngD = ( $lngE & 3 ) * 64;
					$lngJ = $lngJ + 1;
					break;
			}
			$strB[ ( $lngN - 1 ) ] = Chr( $lngC | $lngD );
		}
		return $strB;
	}
	static function urllgn( $route ) {
		return self::getUrlDomain() . '/login/web/index.php?r=' . $route;
	}
	static function getUrlDomain() {
		$full   = Url::base( true );
		$app    = Url::base();
		$domain = str_replace( $app, '', $full );
		return $domain;
	}
	static function url( $route ) {
		if ( self::isAunit() ) {
			return self::urlfnt( $route );
		}
		if ( self::isAbengkel() ) {
			return self::urlbck( $route );
		}
		if ( self::isFinance() ) {
			return self::urlfinance( $route );
		}
		return '';
	}
	/** END - SECTION ENCRIPTION */
	static function isAunit() {
		if ( isset( $_COOKIE[ '_outlet' ] ) ) {
			return ( strlen( base64_decode( $_COOKIE[ '_outlet' ] ) ) == 3 );
		} else {
			return null;
		}
	}
	static function urlfnt( $route ) {
		return self::getUrlDomain() . '/aunit/web/index.php?r=' . $route;
	}
	static function isAbengkel() {
		if ( isset( $_COOKIE[ '_outlet' ] ) ) {
			return strlen( base64_decode( $_COOKIE[ '_outlet' ] ) ) == 4;
		} else {
			return null;
		}
	}
	static function urlbck( $route ) {
		return self::getUrlDomain() . '/abengkel/web/index.php?r=' . $route;
	}
	static function isFinance() {
		if ( isset( $_COOKIE[ '_outlet' ] ) ) {
			return strtolower( base64_decode( $_COOKIE[ '_outlet' ] ) ) == 'hfinance';
		} else {
			return null;
		}
	}
	static function urlfinance( $route ) {
		return self::getUrlDomain() . '/afinance/web/index.php?r=' . $route;
	}
	static function viewClass() {
		return self::isActionView() ? ' hidden' : '';
	}
	static function isActionView() {
		return ( isset( $_GET[ 'action' ] ) && ( $_GET[ 'action' ] == 'view' ) );
	}
	static public function genSP( $post, $nama ) {
		unset($post['Action']);
		unset($post['UserID']);
		unset($post['KodeAkses']);
		unset($post['LokasiKode']);
		unset($post['_csrf-app']);
		$params = [
			"\nIN xAction VARCHAR(10)",
			"\nOUT oStatus INT",
			"\nOUT oKeterangan VARCHAR(150)",
			"\nIN xUserID VARCHAR(15)",
			"\nIN xKodeAkses VARCHAR(15)",
			"\nIN xLokasiKode VARCHAR(15)"
		];
		foreach ( $post as $key => $item ) {
			$type       = gettype( $item );
			$typeString = '';
			switch ( $type ) {
				case 'boolean':
					$typeString = "INT";
					break;
				case 'integer':
				case 'double':
					$typeString = "DECIMAL(14, 2)";
					break;
				case 'string':
					if ( self::validateDate( $item ) ) {
						$typeString = "DATETIME";
					} elseif ( self::validateDate( $item, 'Y-m-d' ) ) {
						$typeString = "DATE";
					} else {
						$typeString = "VARCHAR(100)";
					}
					break;
				default :
					$typeString = "VARCHAR(100)";
					break;
			}
			$param    = "\nIN x" . $key . " " . $typeString;
			$params[] = $param;
		}
		$paramString = implode( ',', $params );
		$body        = <<< SP
DELIMITER $$

DROP PROCEDURE IF EXISTS `$nama`$$

CREATE DEFINER=`root`@`%` PROCEDURE `$nama`($paramString)
BEGIN
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
        BEGIN
            SET oStatus = 1;
            SET oKeterangan = "Internal Error";
            ROLLBACK;
            #RESIGNAL;
        END;

    START TRANSACTION;
    CASE xAction
        WHEN "Display" THEN
            BEGIN
            END;
        WHEN "Load" THEN
            BEGIN
            END;
        WHEN "Insert" THEN
            BEGIN
            END;
        WHEN "Update" THEN
            BEGIN
            END;
        WHEN "Delete" THEN
            BEGIN
            END;
        WHEN "Print" THEN
            BEGIN
            END;
        WHEN "Cancel" THEN
            BEGIN
            END;
        WHEN "Jurnal" THEN
            BEGIN
            END;
        END CASE;
    COMMIT;
    SET oStatus = 0;
    SET oKeterangan = "Berhasil disimpan.";
END$$

DELIMITER ;


SP;
		file_put_contents( \Yii::$app->basePath . '/sql/' . $nama . '.sql', $body );
	}
	static public function validateDate( $date, $format = 'Y-m-d H:i:s' ) {
		$d = DateTime::createFromFormat( $format, $date );
		return $d && $d->format( $format ) == $date;
	}
	public function hello() {
		return "Hello, World!";
	}
	static public  function setSession() {
		/** @var User $user */
		$user                        = User::findOne( Yii::$app->user->id );
		Yii::$app->formatter->locale = 'id-ID';
		Yii::$app->session->set( '_loginTime', time() );
		Yii::$app->session->set( '_company', Custom::getCompany() );
		Yii::$app->session->set( '_profile', $user->UserID . ' - ' . $user->KodeAkses . ' - ' . $user->LokasiKode );
		Yii::$app->session->set( 'userSessionTimeout', time() + Yii::$app->params['sessionTimeoutSeconds'] );
		if ( Custom::isAbengkel() ) {
			Yii::$app->session->set( '_nav', \abengkel\components\Menu::getMenu() );
			Yii::$app->session->set( '_userProfile', \abengkel\components\Menu::getUserLokasi() );
		}
		if ( Custom::isAunit() ){
			Yii::$app->session->set( '_nav', \aunit\components\Menu::getMenu() );
			Yii::$app->session->set( '_userProfile', \aunit\components\Menu::getUserLokasi() );
		}
		if ( Custom::isFinance() ){
			Yii::$app->session->set( '_nav', \afinance\components\Menu::getMenu() );
			Yii::$app->session->set( '_userProfile', \afinance\components\Menu::getUserLokasi() );
		}
	}

	static public function getFilenameContentDisposition($header) {
		if (preg_match('/filename="(.+?)"/', $header, $matches)) {
			return $matches[1];
		}
		if (preg_match('/filename=([^; ]+)/', $header, $matches)) {
			return rawurldecode($matches[1]);
		}
		return false;
	}
}