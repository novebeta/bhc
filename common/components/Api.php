<?php

namespace common\components;

use Yii;
use DateTime;
use yii\base\Component;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
//php -dxdebug.mode=debug -dxdebug.start_with_request=yes ./yii api/uinb cwb
class Api extends Component
{
    const MD_HSO   = 'HSO';
    const MD_ANPER = 'ANPER';
    const MD_MPM = 'MPM';
    const MD_DAM = 'DAM';
    const MD_DAW = 'DAW';
    const MD_DAMH2 = 'DAMH2';
    const MD_DAWH2 = 'DAWH2';

    const TYPE_UINB = 'uinb';
    const TYPE_BAST = 'bast';
    const TYPE_PRSP = 'prsp';
    const TYPE_LSNG = 'lsng';
    const TYPE_SPK  = 'spk';
    const TYPE_DOCH = 'doch';
    const TYPE_PINB = 'pinb';
    const TYPE_PRSL = 'prsl';
    const TYPE_PKB  = 'pkb';
    const TYPE_INV1  = 'inv1';
    const TYPE_INV1_READ  = 'inv1_read';
    const TYPE_INV2  = 'inv2';
    const TYPE_INV2_READ  = 'inv2_read';
    const TYPE_DPHLO  = 'dphlo';
    const TYPE_UNPAIDHLO  = 'unpaidhlo';
    const TYPE_CDB  = 'cdb';
    const TYPE_REMAININGSTOCK  = 'remainingstock';

    const TYPE_UINB_ADD = 'uinbAdd';
    const TYPE_BAST_ADD = 'bashAdd';
    const TYPE_PRSP_ADD = 'prspAdd';
    const TYPE_LSNG_ADD = 'lsngAdd';
    const TYPE_SPK_ADD  = 'spkAdd';
    const TYPE_DOCH_ADD = 'dochAdd';
    const TYPE_PINB_ADD = 'pinbAdd';
    const TYPE_PRSL_ADD = 'prslAdd';
    const TYPE_PKB_ADD  = 'pkbAdd';
    const TYPE_CDB_ADD  = 'cdbAdd';
    const TYPE_BOOK_ADD  = 'bookAdd';
    const TYPE_LEADS_ADD  = 'leadsAdd';

    const DOC_ADD_TYPE = [
        self::TYPE_UINB_ADD => 'UINB/Add',
        self::TYPE_BAST_ADD => 'BAST/Add',
        self::TYPE_PRSP_ADD => 'PRSP/Add',
        self::TYPE_LSNG_ADD => 'LSNG/Add',
        self::TYPE_SPK_ADD  => 'SPK/Add',
        self::TYPE_DOCH_ADD => 'DOCH/Add',
        self::TYPE_PINB_ADD => 'PINB/Add',
        self::TYPE_PRSL_ADD => 'PRSL/Add',
        self::TYPE_PKB_ADD  => 'PKB/Add',
        self::TYPE_CDB_ADD  => 'CDB/Add',
        self::TYPE_BOOK_ADD  => 'Book/Add',
        self::TYPE_LEADS_ADD  => 'Leads/Add',
    ];
    /**
     * Api constructor.
     */
    private $mainDealer;

    private $api_key;
    private $secret_key;
    private $dealerId;
    private $curr_unix_time;

    /**
     * @return mixed
     */
    public function getCurrUnixTime()
    {
        return $this->curr_unix_time;
    }

    static public function array_except($array, $removeKeys){
        $keys = array_keys($removeKeys);
        foreach($keys as $key){
            unset($array[$key]);
        }
        return $array;
    }

    /**
     * @param mixed $curr_unix_time
     */
    public function setCurrUnixTime($curr_unix_time): void
    {
        $this->curr_unix_time = $curr_unix_time;
    }
    private $ch;
    private $header;
    private $outlet;

    public function __construct($outlet = null)
    {
        global $argv;
        if (empty($argv)){
            $outlet = $_COOKIE['_outlet'];
        }
        $this->curr_unix_time = time();
        $this->outlet         = base64_decode($outlet);
        $paramOutlet = array_merge(Yii::$app->params['H1'],Yii::$app->params['H2']);
        $mainDealer           = $paramOutlet[$this->outlet]['mainDealer']['kode'];
        $this->mainDealer     = $mainDealer;
        $this->dealerId       = $paramOutlet[$this->outlet]['mainDealer']['dealerId'];
        $this->api_key        = $paramOutlet[$this->outlet]['mainDealer']['api_key'];
        $this->secret_key     = $paramOutlet[$this->outlet]['mainDealer']['secret_key'];
        parent::__construct();
    }

    public function callApi($params, $url, $post_raw_json){
        $curl = curl_init();
        $unixTime = time();
        $header = [
            'Content-Type:application/json',
            'Accept:application/json',
            'DGI-API-Key:' . $params['api_key'],
            'X-Request-Time:' . $unixTime,
            'DGI-API-Token:' . hash('sha256', $params['api_key'] . $params['secret_key'] . $unixTime), # hash token agar tidak bisa terbaca (one way), token ini digunakan untuk keperluan validasi
        ];
        curl_setopt_array($curl, [
            CURLOPT_URL            => $url, # URL endpoint
            CURLOPT_HTTPHEADER     => $header, # HTTP Headers
            CURLOPT_RETURNTRANSFER => 1, # return hasil curl_exec ke variabel, tidak langsung dicetak
            CURLOPT_FOLLOWLOCATION => 1, # atur flag followlocation untuk mengikuti bila ada url redirect di server penerima tetap difollow
            CURLOPT_CONNECTTIMEOUT => 60, # set connection timeout ke 60 detik, untuk mencegah request gantung saat server mati
            CURLOPT_TIMEOUT        => 60, # set timeout ke 120 detik, untuk mencegah request gantung saat server hang
            CURLOPT_POST           => 1, # set method request menjadi POST
            CURLOPT_POSTFIELDS     => $post_raw_json, # attached post data dalam bentuk JSON String,
            // CURLOPT_VERBOSE        => Yii::$app->request->isConsoleRequest, # mode debug
            // CURLOPT_HEADER         => Yii::$app->request->isConsoleRequest, # cetak header
            // CURLINFO_HEADER_OUT    => Yii::$app->request->isConsoleRequest,
        ]);
        $resp = curl_exec($curl);
        Yii::info($resp, 'API POS');
        if (curl_errno($curl) == false) {
            if(curl_getinfo($curl, CURLINFO_HTTP_CODE) == '200' && self::isJson($resp)){
                $arr = Json::decode($resp);
                curl_close($curl);
                return $arr;
            }
            echo $resp;
        }else{
            echo $resp;
        }
        curl_close($curl);
        return false;
    }

    public function getDealerId()
    {
        return $this->dealerId;
    }

    public function getMainDealer()
    {
        return $this->mainDealer;
    }

    private function buildHeader()
    {
        switch ($this->mainDealer) {
            case Api::MD_HSO:
            case Api::MD_ANPER:
            case Api::MD_MPM:
            case Api::MD_DAM:
            case Api::MD_DAW:
            case Api::MD_DAMH2:
            case Api::MD_DAWH2:
                $this->header = [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    'DGI-API-Key:' . $this->api_key,
                    'X-Request-Time:' . $this->curr_unix_time,
                    'DGI-API-Token:' . hash('sha256', $this->api_key . $this->secret_key . $this->curr_unix_time), # hash token agar tidak bisa terbaca (one way), token ini digunakan untuk keperluan validasi
                ];
                break;
        }
    }

    private function setOption($url, $post_raw_json)
    {
        $this->buildHeader();
        curl_setopt_array($this->ch, [
            CURLOPT_URL            => $url, # URL endpoint
            CURLOPT_HTTPHEADER     => $this->header, # HTTP Headers
            CURLOPT_RETURNTRANSFER => 1, # return hasil curl_exec ke variabel, tidak langsung dicetak
            CURLOPT_FOLLOWLOCATION => 1, # atur flag followlocation untuk mengikuti bila ada url redirect di server penerima tetap difollow
            CURLOPT_CONNECTTIMEOUT => 60, # set connection timeout ke 60 detik, untuk mencegah request gantung saat server mati
            CURLOPT_TIMEOUT        => 60, # set timeout ke 120 detik, untuk mencegah request gantung saat server hang
            CURLOPT_POST           => 1, # set method request menjadi POST
            CURLOPT_POSTFIELDS     => $post_raw_json, # attached post data dalam bentuk JSON String,
            // CURLOPT_VERBOSE        => Yii::$app->request->isConsoleRequest, # mode debug
            // CURLOPT_HEADER         => Yii::$app->request->isConsoleRequest, # cetak header
            // CURLINFO_HEADER_OUT    => Yii::$app->request->isConsoleRequest,
        ]);
    }

    static public function isJson($string) {
        json_decode($string);
        return json_last_error() === JSON_ERROR_NONE;
     }

    public function call($type, $params, $filterResult = [],$saveResult = true)
    {
        $this->ch = curl_init();
        $url      = Yii::$app->params['mainDealer'][$this->mainDealer]['base_url'] . Yii::$app->params['mainDealer'][$this->mainDealer][$type];
        // echo "url:$url";
        $this->setOption($url, $params);
        $resp = curl_exec($this->ch);
        $arrPOS = [];
        switch ($type) {
            case Api::TYPE_UINB:
                $paramOutlet = array_merge(Yii::$app->params['H1'],Yii::$app->params['H2']);
                if(!empty($paramOutlet[$this->outlet]['mainDealer']['POS'])){
                    $arrParams = Json::decode($params);
                    $arrParams['dealerId'] = $paramOutlet[$this->outlet]['mainDealer']['POS']['dealerId'];
                    $retPos = $this->callApi($paramOutlet[$this->outlet]['mainDealer']['POS'],$url,Json::encode($arrParams));
                    if($retPos !== false && is_array($retPos)){
                        $arrPOS = $retPos['data'];
                    }
                }
                break;
        }
        Yii::info($type, 'API TYPE');
        Yii::info($this->header, 'API PARAMS');
        Yii::info($params, 'API PARAMS');
        // var_dump($resp);die();
        # validasi curl request tidak error
        $this->saveDebugInfo($this->outlet,$type,$resp);
        if (curl_errno($this->ch) == false) {
            # jika curl berhasil
            $http_code = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            Yii::info($resp,'API RESULT');
            if ($http_code == 200) {
                # http code === 200 berarti request sukses (harap pastikan server penerima mengirimkan http_code 200 jika berhasil)
                if(self::isJson($resp)){
                    $arr = Json::decode($resp);
                    if(!empty($arrPOS)){
                        $arr['data'] = \array_merge_recursive($arr['data'],$arrPOS);
                    }
                    // var_dump($arr['data']);die();
                    switch ($type) {
                        case Api::TYPE_PRSP:
                            if($this->mainDealer == 'HSO'){
                                $sumberProspect = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.sumberProspect");
                                $metodeFollowUp = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.metodeFollowUp");
                                $testRidePreference = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.testRidePreference");
                                $statusFollowUpProspecting = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.statusFollowUpProspecting");
                                $statusProspect = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.statusProspect");
                                for ($i = 0; $i < sizeof($arr['data'] ?? []); $i++) {
                                    if(!empty($arr['data'][$i]['metodeFollowUp']) && !empty($metodeFollowUp[$arr['data'][$i]['metodeFollowUp']])){
                                        $arr['data'][$i]['metodeFollowUp'] = $metodeFollowUp[$arr['data'][$i]['metodeFollowUp']];
                                    }
                                    if(!empty($arr['data'][$i]['sumberProspect']) && !empty($sumberProspect[$arr['data'][$i]['sumberProspect']])){
                                        $arr['data'][$i]['sumberProspect'] = $sumberProspect[$arr['data'][$i]['sumberProspect']];
                                    }
                                    if(!empty($arr['data'][$i]['testRidePreference']) && !empty($testRidePreference[$arr['data'][$i]['testRidePreference']])){
                                        $arr['data'][$i]['testRidePreference'] = $testRidePreference[$arr['data'][$i]['testRidePreference']];
                                    }
                                    if(!empty($arr['data'][$i]['statusFollowUpProspecting']) && !empty($statusFollowUpProspecting[$arr['data'][$i]['statusFollowUpProspecting']])){
                                        $arr['data'][$i]['statusFollowUpProspecting'] = $statusFollowUpProspecting[$arr['data'][$i]['statusFollowUpProspecting']];
                                    }
                                    if(!empty($arr['data'][$i]['statusProspect']) && !empty($statusProspect[$arr['data'][$i]['statusProspect']])){
                                        $arr['data'][$i]['statusProspect'] = $statusProspect[$arr['data'][$i]['statusProspect']];
                                    }
                                    if(Yii::$app->request->isConsoleRequest && !empty($arr['data'][$i]['idProspect'])){
                                        $this->callAdditionalApi(Api::TYPE_PRSP_ADD,['documentNo' => $arr['data'][$i]['idProspect']]);
                                    }
                                }
                                // \var_dump($arr['data']);die();
                            }
                            break;
                        case Api::TYPE_SPK:
                            if($this->mainDealer == 'HSO'){
                                $statusSPK = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.statusSPK");
                                $tipePembayaran = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.tipePembayaran");
                                for ($i = 0; $i < sizeof($arr['data'] ?? []); $i++) {
                                    if(!empty($arr['data'][$i]['statusSPK'])){
                                        $arr['data'][$i]['statusSPK'] = $statusSPK[$arr['data'][$i]['statusSPK']];
                                    }
                                    for ($x = 0; $x < count($arr['data'][$i]['unit'] ?? []); $x++) {
                                        if(!empty($arr['data'][$i]['unit'][$x]['tipePembayaran'])){
                                            $arr['data'][$i]['unit'][$x]['tipePembayaran'] = $tipePembayaran[$arr['data'][$i]['unit'][$x]['tipePembayaran']];
                                        }
                                    }
                                    if(Yii::$app->request->isConsoleRequest && !empty($arr['data'][$i]['idSpk'])){
                                        $this->callAdditionalApi(Api::TYPE_SPK_ADD,['documentNo' => $arr['data'][$i]['idSpk']]);
                                        $this->callAdditionalApi(Api::TYPE_CDB_ADD,['documentNo' => $arr['data'][$i]['idSpk']]);
                                    }
                                }
                            }
                            break;
                        case Api::TYPE_DOCH:
                            if($this->mainDealer == 'HSO'){
                                $statusFakturSTNK = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.statusFakturSTNK");
                                $jenisIdPenerimaBPKB = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.jenisIdPenerimaBPKB");
                                $jenisIdPenerimaSTNK = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.jenisIdPenerimaSTNK");
                                for ($i = 0; $i < sizeof($arr['data'] ?? []); $i++) {
                                    for ($x = 0; $x < count($arr['data'][$i]['unit'] ?? []); $x++) {
                                        if(!empty($arr['data'][$i]['unit'][$x]['statusFakturSTNK'])){
                                            $arr['data'][$i]['unit'][$x]['statusFakturSTNK'] = $statusFakturSTNK[$arr['data'][$i]['unit'][$x]['statusFakturSTNK']];
                                        }
                                        if(!empty($arr['data'][$i]['unit'][$x]['jenisIdPenerimaBPKB'])){
                                            $arr['data'][$i]['unit'][$x]['jenisIdPenerimaBPKB'] = $jenisIdPenerimaBPKB[$arr['data'][$i]['unit'][$x]['jenisIdPenerimaBPKB']];
                                        }
                                        if(!empty($arr['data'][$i]['unit'][$x]['jenisIdPenerimaSTNK'])){
                                            $arr['data'][$i]['unit'][$x]['jenisIdPenerimaSTNK'] = $jenisIdPenerimaSTNK[$arr['data'][$i]['unit'][$x]['jenisIdPenerimaSTNK']];
                                        }
                                    }
                                    if(Yii::$app->request->isConsoleRequest && !empty($arr['data'][$i]['idSPK'])){
                                        $this->callAdditionalApi(Api::TYPE_DOCH_ADD,['documentNo' => $arr['data'][$i]['idSPK']]);
                                    }
                                }
                            }
                            break;
                        case Api::TYPE_UINB:
                            if($this->mainDealer == 'HSO'){
                                $statusShippingList = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.statusShippingList");
                                $statusRFS = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.statusRFS");
                                // \var_dump($statusShippingList);die();
                                for ($i = 0; $i < sizeof($arr['data'] ?? []); $i++) {
                                    if(!empty($arr['data'][$i]['statusShippingList'])){
                                        $arr['data'][$i]['statusShippingList'] = $statusShippingList[$arr['data'][$i]['statusShippingList']];
                                    }
                                    for ($x = 0; $x < count($arr['data'][$i]['unit'] ?? []); $x++) {
                                        $arr['data'][$i]['unit'][$x]['noMesin'] = str_replace(' ','',$arr['data'][$i]['unit'][$x]['noMesin']);
                                        if(!empty($arr['data'][$i]['unit'][$x]['statusRFS'])){
                                            $arr['data'][$i]['unit'][$x]['statusRFS'] = $statusRFS[$arr['data'][$i]['unit'][$x]['statusRFS']];
                                        }
                                    }
                                    if(Yii::$app->request->isConsoleRequest && !empty($arr['data'][$i]['noShippingList'])){
                                        $this->callAdditionalApi(Api::TYPE_UINB_ADD,['documentNo' => $arr['data'][$i]['noShippingList']]);
                                    }
                                }
                            }
                            break;
                        case Api::TYPE_PINB:
                            if($this->mainDealer == 'HSO'){
                                $jenisOrder = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.jenisOrder");
                                // \var_dump($statusShippingList);die();
                                for ($i = 0; $i < sizeof($arr['data'] ?? []); $i++) {
                                    for ($x = 0; $x < count($arr['data'][$i]['unit'] ?? []); $x++) {
                                        if(!empty($arr['data'][$i]['unit'][$x]['statusRFS'])){
                                            $arr['data'][$i]['unit'][$x]['jenisOrder'] = $jenisOrder[$arr['data'][$i]['unit'][$x]['jenisOrder']];
                                        }
                                    }
                                    $noShippingList = $arr['data'][$i]['noShippingList'];
                                    if(Yii::$app->request->isConsoleRequest && !empty($noShippingList) && str_contains($noShippingList, '-SLP-')){
                                        $this->callAdditionalApi(Api::TYPE_PINB_ADD,['documentNo' => $noShippingList]);
                                    }
                                }
                            }
                            break;
                        case Api::TYPE_PKB:
                            if($this->mainDealer == 'HSO'){
                                $tipeComingCustomer = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.tipeComingCustomer");
                                $hubunganDenganPemilik = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.hubunganDenganPemilik");
                                $asalUnitEntry = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.asalUnitEntry");
                                $jenisPIT = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.jenisPIT");
                                $setUpPembayaran = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.setUpPembayaran");
                                $konfirmasiPekerjaanTambahan = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.konfirmasiPekerjaanTambahan");
                                $statusWorkOrder = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.statusWorkOrder");
                                for ($i = 0; $i < sizeof($arr['data'] ?? []); $i++) {
                                    if(!empty($arr['data'][$i]['tipeComingCustomer']) && !empty($tipeComingCustomer[$arr['data'][$i]['tipeComingCustomer']])){
                                        $arr['data'][$i]['tipeComingCustomer'] = $tipeComingCustomer[$arr['data'][$i]['tipeComingCustomer']];
                                    }
                                    if(!empty($arr['data'][$i]['hubunganDenganPemilik'])){
                                        $arr['data'][$i]['hubunganDenganPemilik'] = $hubunganDenganPemilik[$arr['data'][$i]['hubunganDenganPemilik']];
                                    }
                                    if(!empty($arr['data'][$i]['asalUnitEntry']) && !empty($asalUnitEntry[$arr['data'][$i]['asalUnitEntry']])){
                                        $arr['data'][$i]['asalUnitEntry'] = $asalUnitEntry[$arr['data'][$i]['asalUnitEntry']];
                                    }
                                    if(!empty($arr['data'][$i]['jenisPIT']) && !empty($jenisPIT[$arr['data'][$i]['jenisPIT']])){
                                        $arr['data'][$i]['jenisPIT'] = $jenisPIT[$arr['data'][$i]['jenisPIT']];
                                    }
                                    if(!empty($arr['data'][$i]['setUpPembayaran']) && !empty($setUpPembayaran[$arr['data'][$i]['setUpPembayaran']])){
                                        $arr['data'][$i]['setUpPembayaran'] = $setUpPembayaran[$arr['data'][$i]['setUpPembayaran']];
                                    }
                                    if(!empty($arr['data'][$i]['konfirmasiPekerjaanTambahan']) && !empty($konfirmasiPekerjaanTambahan[$arr['data'][$i]['konfirmasiPekerjaanTambahan']])){
                                        $arr['data'][$i]['konfirmasiPekerjaanTambahan'] = $konfirmasiPekerjaanTambahan[$arr['data'][$i]['konfirmasiPekerjaanTambahan']];
                                    }
                                    if(!empty($arr['data'][$i]['statusWorkOrder']) && !empty($statusWorkOrder[$arr['data'][$i]['statusWorkOrder']])){
                                        $arr['data'][$i]['statusWorkOrder'] = $statusWorkOrder[$arr['data'][$i]['statusWorkOrder']];
                                    }
                                    if(Yii::$app->request->isConsoleRequest && !empty($arr['data'][$i]['noWorkOrder'])){
                                        $this->callAdditionalApi(Api::TYPE_PKB_ADD,['documentNo' => $arr['data'][$i]['noWorkOrder']]);
                                    }
                                }
                            }
                            break;
                        case Api::TYPE_PRSL:
                                if($this->mainDealer == 'HSO'){
                                    for ($i = 0; $i < sizeof($arr['data'] ?? []); $i++) {
                                        if(Yii::$app->request->isConsoleRequest && !empty($arr['data'][$i]['noSO'])){
                                            $this->callAdditionalApi(Api::TYPE_PRSL_ADD,['documentNo' => $arr['data'][$i]['noSO']]);
                                        }
                                    }
                                }
                                break;
                        case Api::TYPE_CDB:
                                if($this->mainDealer == 'HSO'){
                                    for ($i = 0; $i < sizeof($arr['data'] ?? []); $i++) {
                                        if(Yii::$app->request->isConsoleRequest && !empty($arr['data'][$i]['idSpk'])){
                                            $this->callAdditionalApi(Api::TYPE_CDB_ADD,['documentNo' => $arr['data'][$i]['idSpk']]);
                                        }
                                    }
                                }
                                break;
                        // case Api::TYPE_DPHLO:
                        //         if($this->mainDealer == 'HSO'){
                        //             for ($i = 0; $i < sizeof($arr['data'] ?? []); $i++) {
                        //                 if(Yii::$app->request->isConsoleRequest && !empty($arr['data'][$i]['noWorkOrder'])){
                        //                     $this->callAdditionalApi(Api::TYPE_DPHL,['documentNo' => $arr['data'][$i]['noWorkOrder']]);
                        //                 }
                        //             }
                        //         }
                        //         break;
                        case Api::TYPE_LSNG:
                                if($this->mainDealer == 'HSO'){
                                    for ($i = 0; $i < sizeof($arr['data'] ?? []); $i++) {
                                        if(Yii::$app->request->isConsoleRequest && !empty($arr['data'][$i]['idSPK'])){
                                            $this->callAdditionalApi(Api::TYPE_LSNG_ADD,['documentNo' => $arr['data'][$i]['idSPK']]);
                                        }
                                    }
                                }
                                break;
                        case Api::TYPE_BAST:
                                if($this->mainDealer == 'HSO'){
                                    for ($i = 0; $i < sizeof($arr['data'] ?? []); $i++) {
                                        for ($x = 0; $x < sizeof($arr['data'][$i]['detail'] ?? []); $x++) {
                                            if(Yii::$app->request->isConsoleRequest && !empty($arr['data'][$i]['detail'][$x]['idSPK'])){
                                                $this->callAdditionalApi(Api::TYPE_BAST_ADD,['documentNo' => $arr['data'][$i]['detail'][$x]['idSPK']]);
                                            }
                                        }
                                    }
                                }
                                break;
                        case Api::TYPE_INV1_READ:
                            if($this->mainDealer == 'HSO'){
                                $tipePembayaran = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.tipePembayaranInv1");
                                $caraBayar = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.caraBayar");
                                $status = ArrayHelper::getValue(Yii::$app->params, "LoV.{$this->mainDealer}.statusInv1");
                                // \var_dump($statusShippingList);die();
                                for ($i = 0; $i < sizeof($arr['data'] ?? []); $i++) {
                                    if(!empty($arr['data'][$i]['tipePembayaran'])){
                                        $arr['data'][$i]['tipePembayaran'] = $tipePembayaran[$arr['data'][$i]['tipePembayaran']];
                                    }
                                    if(!empty($arr['data'][$i]['caraBayar'])){
                                        $arr['data'][$i]['caraBayar'] = $caraBayar[$arr['data'][$i]['caraBayar']];
                                    }
                                    if(!empty($arr['data'][$i]['status'])){
                                        $arr['data'][$i]['status'] = $status[$arr['data'][$i]['status']];
                                    }
                                }
                            }
                            break;
                        case self::TYPE_UINB_ADD:
                        case self::TYPE_BAST_ADD:
                        case self::TYPE_PRSP_ADD:
                        case self::TYPE_LSNG_ADD:
                        case self::TYPE_SPK_ADD:
                        case self::TYPE_DOCH_ADD:
                        case self::TYPE_PINB_ADD:
                        case self::TYPE_PRSL_ADD:
                        case self::TYPE_PKB_ADD:
                        case self::TYPE_CDB_ADD:
                        case self::TYPE_BOOK_ADD:
                        case self::TYPE_LEADS_ADD:
                            $arrParams = Json::decode($params);
                            if($saveResult && !empty($arrParams['documentNo'])){
                                $arrTmp = json_encode($arr);
                                $arr = [
                                    'data' => [
                                        [
                                            'documentNo' => $arrParams['documentNo'],
                                            'Response' => $arrTmp
                                        ]
                                    ],
                                ];
                            }
                            break;
                    }
                    // \var_dump($arr['data']);die();
                    //  save api result to db
                    if($saveResult){
                        $this->saveApiResult($this->outlet,$type,$arr);
                    }
                    if (!empty($filterResult) && isset($filterResult['key']) && isset($filterResult['value'])) {
                        //filter
                        if ($filterResult['value'] !== '' && !empty($arr['data'])) {
                            // cek arr header
                            if (array_key_exists($filterResult['key'], $arr['data'][0])) {
                                $arr['data'] = array_values(array_filter($arr['data'], function ($var) use ($filterResult) {
                                    return strpos(strtolower($var[$filterResult['key']]), strtolower($filterResult['value'])) !== false;
                                }));
                            } else {
                                // cek arr header
                                $newArr = [];
                                foreach ($arr['data'] as $header) {
                                    $isFound = false;
                                    foreach ($header as $item) {
                                        if (is_array($item) && !empty($item) && array_key_exists($filterResult['key'], $item[0])) {
                                            $arrResultDetils = array_filter($item, function ($var) use ($filterResult) {
                                                return strpos(strtolower($var[$filterResult['key']]), strtolower($filterResult['value'])) !== false;
                                            });
                                            if (!empty($arrResultDetils)) {
                                                $isFound = true;
                                            }
                                        }
                                    }
                                    if($isFound){
                                        $newArr[] = $header;
                                    }
                                }
                                $arr['data'] = $newArr;
                            }
                        }
                    }
                    $resp = Json::encode($arr);
                    return $resp;
                }else{
                    echo $resp;
                }
            }
        } else {
            # jika curl error (contoh: request timeout)
            # Daftar kode error : https://curl.haxx.se/libcurl/c/libcurl-errors.html
            echo "Error while sending request, reason:" . curl_error($this->ch);
        }
        # tutup CURL
        curl_close($this->ch);
    }

    public function callAdditionalApi($type, $params, $saveResult = false){
        $docType = Api::DOC_ADD_TYPE[$type];
        $params = array_merge(['dealerId'=>$this->dealerId, 'documentType' => $docType],$params);
        $jsonParams = Json::encode($params);
        $this->call($type, $jsonParams, $saveResult);
    }

    private function saveDebugInfo($key,$type,$arrHasil){
        $msg = '';
        try{
            $db_arr   = Yii::$app->components['db'];
            $connection = new \yii\db\Connection([
                'dsn'      => $db_arr['dsn'] . 'dbname=' . $key . ';',
                'username' => $db_arr['username'],
                'password' => $db_arr['password'],
            ]);
            $connection->open();
            $recordLogs = [];
            $interface = Yii::$app->request->isConsoleRequest ? 'CLI' : "WEB";
            $recordLogs[] = [
                'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>$arrHasil,'LogTabel'=>$type,'NoTrans'=>$interface
            ];
            $commandLog = $connection->createCommand()->batchInsert('tuserlog', [
                'LogTglJam','UserID','LogJenis','LogNama','LogTabel','NoTrans'
            ], $recordLogs);
            $commandLog->execute();
        }catch(Exception $e){
            $msg .= "Error : ".$e->getMessage();
        }
    }

    private function saveApiResult($key,$type,$arrHasil){
        $msg = '';
        try{
            $db_arr   = Yii::$app->components['db'];
            $connection = new \yii\db\Connection([
                'dsn'      => $db_arr['dsn'] . 'dbname=' . $key . ';',
                'username' => $db_arr['username'],
                'password' => $db_arr['password'],
            ]);
            $connection->open();
            $recordLogs = [];
            $interface = Yii::$app->request->isConsoleRequest ? 'CLI' : "WEB";
            $recordLogs[] = [
                'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>json_encode($arrHasil),'LogTabel'=>$type,'NoTrans'=>$interface
            ];
            if (!isset($arrHasil['data']) || !is_array($arrHasil['data'])) {
                $msg = "Outlet : " . $key . ", Data : 0";
            } elseif (is_array($arrHasil['data'])) {
                $count = count($arrHasil['data']);
                $msg = "Outlet : " . $key . ", Data : " . $count;
                if($count != 0){
                        switch($type){
                            case self::TYPE_BAST:
                                $dataFields = [
                                        'deliveryDocumentId',  'idDriver', 'tanggalPengiriman', 'statusDeliveryDocument','dealerid','createdTimehd','modifiedTimehd',
                                        'noSO','idSPK', 'noMesin', 'noRangka', 'idCustomer', 'waktuPengiriman','checklistKelengkapan',
                                        'lokasiPengiriman', 'latitude', 'longitude', 'namaPenerima', 'noKontakPenerima','createdTime','modifiedTime'
                                ];
                                $dataTemplate = array_fill_keys($dataFields, null);
                                $dataTemplate = (array_change_key_case($dataTemplate, CASE_LOWER));
                                $record = [];
                                foreach($arrHasil['data'] as $data){
                                    $data = (array_change_key_case($data, CASE_LOWER));
                                    $details = $data['detail'];
                                    $DateTime = DateTime::createFromFormat('d/m/Y', $data['tanggalpengiriman']);
                                    $data['tanggalpengiriman'] = ($DateTime === false)? $data['tanggalpengiriman']:$DateTime->format('Y-m-d');
                                    unset($data['detail']);
                                    if(!empty($data['createdtime'])){
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['createdtime']);
                                        $data['createdtimehd'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
                                        unset($data['createdtime']);
                                    }
                                    if(!empty($data['modifiedtime'])){
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['modifiedtime']);
                                        $data['modifiedtimehd'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
                                        unset($data['modifiedtime']);
                                    }
                                    foreach($details as $detil){
                                        $detil = (array_change_key_case($detil, CASE_LOWER));
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $detil['createdtime']);
                                        $detil['createdtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
                                        if(!empty($detil['modifiedtime'])){
                                            $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $detil['modifiedtime']);
                                            $detil['modifiedtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
                                        }
                                        $realData = array_merge($dataTemplate,$data,$detil);
                                        $diffKeys = array_diff_key($realData, $dataTemplate);
                                        $finalArrays = self::array_except($realData, $diffKeys);
                                        $record[] = array_values($finalArrays);
                                    }
                                }
                                $command = $connection->createCommand()->batchInsert(
                                    'txbast', $dataFields, $record
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE tanggalPengiriman=VALUES(tanggalPengiriman),idDriver=VALUES(idDriver),statusDeliveryDocument=VALUES(statusDeliveryDocument)'
                                .",noMesin=VALUES(noMesin),noRangka=VALUES(noRangka),idCustomer=VALUES(idCustomer),waktuPengiriman=VALUES(waktuPengiriman)"
                                .",dealerid=VALUES(dealerid),createdTimehd=VALUES(createdTimehd),modifiedTimehd=VALUES(modifiedTimehd)"
                                .",checklistKelengkapan=VALUES(checklistKelengkapan),lokasiPengiriman=VALUES(lokasiPengiriman),latitude=VALUES(latitude),longitude=VALUES(longitude)"
                                .",namaPenerima=VALUES(namaPenerima),noKontakPenerima=VALUES(noKontakPenerima),createdTime=VALUES(createdTime),modifiedtime=VALUES(modifiedtime)";
                                $command->setRawSql($sql);
                                // echo $sql;
                                $aff = $command->execute();
                                $msg .= "Row Affected : ".$aff;
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txbast','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_DOCH:
                                $dataFields = [
                                    'idSO',  'idSPK','dealerID','createdTimehd','modifiedTimehd', 
                                    'nomorRangka', 'nomorFakturSTNK', 'tanggalPengajuanSTNKKeBiro', 'statusFakturSTNK',
                                    'nomorSTNK', 'tanggalPenerimaanSTNKDariBiro', 'platNomor', 'nomorBPKB', 'tanggalPenerimaanBPKBDariBiro','tanggalTerimaSTNKOlehKonsumen',
                                    'tanggalTerimaBPKBOlehKonsumen', 'namaPenerimaBPKB','namaPenerimaSTNK', 'jenisIdPenerimaBPKB','jenisIdPenerimaSTNK',
                                    'noIdPenerimaBPKB','noIdPenerimaSTNK', 'createdTime', 'modifiedTime'
                                ];
                                $dataTemplate = array_fill_keys($dataFields, null);
                                $dataTemplate = (array_change_key_case($dataTemplate, CASE_LOWER));
                                $record = [];
                                foreach($arrHasil['data'] as $data){
                                    $data = (array_change_key_case($data, CASE_LOWER));
                                    $details = $data['unit'];
                                    unset($data['unit']);
                                    if(!empty($data['createdtime'])){
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['createdtime']);
                                        $data['createdTimehd'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                        unset($data['createdtime']);
                                    }
                                    if(!empty($data['modifiedtime'])){
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['modifiedtime']);
                                        $data['modifiedTimehd'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                        unset($data['modifiedtime']);
                                    }
                                    foreach($details as $detil){
                                        $detil = (array_change_key_case($detil, CASE_LOWER));
                                        $DateTime = DateTime::createFromFormat('d/m/Y', $detil['tanggalpengajuanstnkkebiro']);
                                        $detil['tanggalpengajuanstnkkebiro'] = ($DateTime === false)? $detil['tanggalpengajuanstnkkebiro']:$DateTime->format('Y-m-d');
                                        $DateTime = DateTime::createFromFormat('d/m/Y', $detil['tanggalterimastnkolehkonsumen']);
                                        $detil['tanggalterimastnkolehkonsumen'] = ($DateTime === false)? $detil['tanggalterimastnkolehkonsumen']:$DateTime->format('Y-m-d');
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $detil['createdtime']);
                                        $detil['createdtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                        if(!empty($detil['modifiedtime'])){
                                            $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $detil['modifiedtime']);
                                            $detil['modifiedtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                        }
                                        $realData = array_merge($dataTemplate,$data,$detil);
                                        $diffKeys = array_diff_key($realData, $dataTemplate);
                                        // var_dump($realData);die();return;
                                        $finalArrays = self::array_except($realData, $diffKeys);
                                        $record[] = array_values($finalArrays);
                                    }
                                }
                                $command = $connection->createCommand()->batchInsert(
                                    'txdoch', $dataFields, $record
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE idSPK=VALUES(idSPK),dealerID=VALUES(dealerID),createdTimehd=VALUES(createdTimehd),modifiedTimehd=VALUES(modifiedTimehd)'
                                .",nomorRangka=VALUES(nomorRangka),tanggalPengajuanSTNKKeBiro=VALUES(tanggalPengajuanSTNKKeBiro),statusFakturSTNK=VALUES(statusFakturSTNK)"
                                .",nomorSTNK=VALUES(nomorSTNK),tanggalPenerimaanSTNKDariBiro=VALUES(tanggalPenerimaanSTNKDariBiro),platNomor=VALUES(platNomor),nomorBPKB=VALUES(nomorBPKB)"
                                .",tanggalPenerimaanBPKBDariBiro=VALUES(tanggalPenerimaanBPKBDariBiro),tanggalTerimaSTNKOlehKonsumen=VALUES(tanggalTerimaSTNKOlehKonsumen)"
                                .",tanggalTerimaBPKBOlehKonsumen=VALUES(tanggalTerimaBPKBOlehKonsumen),namaPenerimaSTNK=VALUES(namaPenerimaSTNK),jenisIdPenerimaSTNK=VALUES(jenisIdPenerimaSTNK)"
                                .",noIdPenerimaSTNK=VALUES(noIdPenerimaSTNK),namaPenerimaBPKB=VALUES(namaPenerimaBPKB),jenisIdPenerimaBPKB=VALUES(jenisIdPenerimaBPKB)"
                                .",noIdPenerimaBPKB=VALUES(noIdPenerimaBPKB),createdTime=VALUES(createdTime),modifiedTime=VALUES(modifiedTime)";
                                $command->setRawSql($sql);
                                // echo $sql;
                                $aff = $command->execute();
                                $msg .= "Row Affected : ".$aff;
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txdoch','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_LSNG:
                                $dataFields = [
                                    'idDokumenPengajuan',  'idSPK', 'jumlahDP', 'tenor', 'jumlahCicilan','tanggalPengajuan', 'idFinanceCompany',
                                    'namaFinanceCompany', 'idPOFinanceCompany', 'tanggalPembuatanPO','tanggalPengirimanPOFinanceCompany', 'createdTime',
                                    'dealerID', 'modifiedTime'
                                ];
                                $dataTemplate = array_fill_keys($dataFields, null);
                                $dataTemplate = (array_change_key_case($dataTemplate, CASE_LOWER));
                                $record = [];
                                foreach($arrHasil['data'] as $data){
                                    $data = (array_change_key_case($data, CASE_LOWER));
                                    $DateTime = DateTime::createFromFormat('d/m/Y', $data['tanggalpengajuan']);
                                    $data['tanggalpengajuan'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
                                    $DateTime = DateTime::createFromFormat('d/m/Y', $data['tanggalpembuatanpo']);
                                    $data['tanggalpembuatanpo'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
                                    $DateTime = DateTime::createFromFormat('d/m/Y', $data['tanggalpengirimanpofinancecompany']);
                                    $data['tanggalpengirimanpofinancecompany'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
                                    $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['createdtime']);
                                    $data['createdtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                    if($data['modifiedtime']){
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['modifiedtime']);
                                        $data['modifiedtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                    }
                                    $realData = array_merge($dataTemplate,$data);
                                    $diffKeys = array_diff_key($realData, $dataTemplate);
                                    // var_dump($realData);die();return;
                                    $finalArrays = self::array_except($realData, $diffKeys);
                                    $record[] = array_values($finalArrays);
                                }
                                $command = $connection->createCommand()->batchInsert(
                                    'txlsng', $dataFields, $record
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE idSPK=VALUES(idSPK),jumlahDP=VALUES(jumlahDP),tenor=VALUES(tenor),jumlahCicilan=VALUES(jumlahCicilan)'
                                .",tanggalPengajuan=VALUES(tanggalPengajuan),idFinanceCompany=VALUES(idFinanceCompany),namaFinanceCompany=VALUES(namaFinanceCompany)"
                                .",dealerID=VALUES(dealerID),modifiedTime=VALUES(modifiedTime)"
                                .",idPOFinanceCompany=VALUES(idPOFinanceCompany),tanggalPembuatanPO=VALUES(tanggalPembuatanPO),tanggalPengirimanPOFinanceCompany=VALUES(tanggalPengirimanPOFinanceCompany),createdTime=VALUES(createdTime)";
                                $command->setRawSql($sql);
                                $aff = $command->execute();
                                $msg .= "Row Affected : ".$aff;
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txlsng','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_UINB:
                                $dataFields = [
                                    'noShippingList',  'tanggalTerima', 'mainDealerId', 'dealerId', 'noInvoice','statusShippingList', 'createdTimehd', 'modifiedTimehd',
                                    'kodeTipeUnit', 'kodeWarna', 'kuantitasTerkirim', 'kuantitasDiterima','noMesin', 'noRangka','statusRFS','poId','kelengkapanUnit',
                                    'noGoodsReceipt','docNRFSId','createdTime','modifiedTime','Warna','Type'
                                ];
                                $dataTemplate = array_fill_keys($dataFields, null);
                                $dataTemplate = (array_change_key_case($dataTemplate, CASE_LOWER));
                                $record = [];
                                foreach($arrHasil['data'] as $data){
                                    $data = (array_change_key_case($data, CASE_LOWER));
                                    $details = $data['unit'];
                                    unset($data['unit']);
                                    $DateTime = DateTime::createFromFormat('d/m/Y', $data['tanggalterima']);
                                    $data['tanggalterima'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
									if(!empty($data['createdtime'])){
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['createdtime']);
                                        $data['createdtimehd'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                        unset($data['createdtime']);
                                    }
                                    if(!empty($data['modifiedtime'])){
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['modifiedtime']);
                                        $data['modifiedtimehd'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                        unset($data['modifiedtime']);
                                    }
                                    foreach($details as $detil){
                                        $detil = (array_change_key_case($detil, CASE_LOWER));
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $detil['createdtime']);
                                        $detil['createdtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                        if(!empty($detil['modifiedtime'])){
                                            $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $detil['modifiedtime']);
                                            $detil['modifiedtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                        }
                                        $realData = array_merge($dataTemplate,$data,$detil);
                                        $diffKeys = array_diff_key($realData, $dataTemplate);
                                        // var_dump($realData);die();return;
                                        $finalArrays = self::array_except($realData, $diffKeys);
                                        $record[] = array_values($finalArrays);
                                    }
                                }
                                $command = $connection->createCommand()->batchInsert(
                                    'txuinb',$dataFields, $record
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE tanggalTerima=VALUES(tanggalTerima),mainDealerId=VALUES(mainDealerId),dealerId=VALUES(dealerId),noInvoice=VALUES(noInvoice)'
                                .",statusShippingList=VALUES(statusShippingList),createdtimehd=VALUES(createdtimehd),modifiedtimehd=VALUES(modifiedtimehd)"
                                .",kodeTipeUnit=VALUES(kodeTipeUnit),kodeWarna=VALUES(kodeWarna),kuantitasTerkirim=VALUES(kuantitasTerkirim),kuantitasDiterima=VALUES(kuantitasDiterima)"
                                .",statusRFS=VALUES(statusRFS),poId=VALUES(poId),kelengkapanUnit=VALUES(kelengkapanUnit),noGoodsReceipt=VALUES(noGoodsReceipt)"
                                .",docNRFSId=VALUES(docNRFSId),createdTime=VALUES(createdTime),modifiedtime=VALUES(modifiedtime)";
                                $command->setRawSql($sql);
                                // echo $sql;
                                $aff = $command->execute();
                                $msg .= "Row Affected : ".$aff;
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txuinb','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_SPK:
                                $dataFields = [
                                    'idSpk',  'idProspect', 'namaCustomer', 'noKtp', 'alamat','kodePropinsi', 
                                    'kodeKota', 'kodeKecamatan', 'kodeKelurahan', 'kodePos', 'noKontak', 
                                    'namaBPKB','noKTPBPKB', 'alamatBPKB','kodePropinsiBPKB','kodeKotaBPKB',
                                    'kodeKecamatanBPKB','kodeKelurahanBPKB','kodePosBPKB','latitude','longitude',
                                    'NPWP','noKK','alamatKK','kodePropinsiKK','kodeKotaKK','kodeKecamatanKK',
                                    'kodeKelurahanKK','kodePosKK','fax','email','idSalesPeople','idEvent',
                                    'tanggalPesanan','statusSPK','dealerid','createdTime','modifiedTime',
                                    'kodeTipeUnit','kodeWarna', 'quantity','hargaJual','diskon','amountPPN','fakturPajak',
                                    'tipePembayaran','jumlahTandaJadi','tanggalPengiriman','idSalesProgram','idApparel',
                                    'createdTimeUnit','modifiedTimeUnit',
                                    'anggotaKK','createdTimeKK','modifiedTimeKK'
                                ];
                                $dataTemplate = array_fill_keys($dataFields, null);
                                $dataTemplate = (array_change_key_case($dataTemplate, CASE_LOWER));
                                $record = [];
                                foreach($arrHasil['data'] as $data){
                                    $data = (array_change_key_case($data, CASE_LOWER));
                                    $unit = $data['unit'];
                                    $dataAnggotaKeluarga = $data['dataanggotakeluarga'];
                                    $countUnit = $countDataAnggotaKeluarga = 0;
                                    if(!empty($unit) && is_array($unit)){
                                        $countUnit = count($unit);
                                    }
                                    if(!empty($dataAnggotaKeluarga) && is_array($dataAnggotaKeluarga)){
                                        $countDataAnggotaKeluarga = count($dataAnggotaKeluarga);
                                    }
                                    $loop = ($countUnit >= $countDataAnggotaKeluarga) ? $countUnit : $countDataAnggotaKeluarga;
                                    unset($data['unit']);
                                    unset($data['dataanggotakeluarga']);
                                    $DateTime = DateTime::createFromFormat('d/m/Y', $data['tanggalpesanan']);
                                    $data['tanggalpesanan'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
                                    $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['createdtime']);
                                    $data['createdtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
									if(!empty($data['modifiedtime'])){
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['modifiedtime']);
                                        $data['modifiedtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                    }
                                    for($i = 0;$i<$loop;$i++){
                                        $unitTmp =  ['kodeTipeUnit'=>null,'kodeWarna'=>null, 'quantity'=>null,'hargaJual'=>null,'diskon'=>null,'kodePPN'=>null,
                                        'downpayment'=>null,'fakturPajak'=>null,'tipePembayaran'=>null,'jumlahTandaJadi'=>null,'tanggalPengiriman'=>null,'idSalesProgram'=>null,
                                        'idApparel'=>null,'createdTimeUnit'=>null];
                                        $dataAnggotaKeluargaTmp = ['anggotaKK'=>null,'createdTimeKK'=>null];
                                        $unitTmp = (array_change_key_case($unitTmp, CASE_LOWER));
                                        $dataAnggotaKeluargaTmp = (array_change_key_case($dataAnggotaKeluargaTmp, CASE_LOWER));
                                        if(!empty($unit[$i]) && is_array($unit[$i])){
                                            $unit[$i] = (array_change_key_case($unit[$i], CASE_LOWER));
                                            $DateTime = DateTime::createFromFormat('d/m/Y', $unit[$i]['tanggalpengiriman']);
                                            $unit[$i]['tanggalpengiriman'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
                                            $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $unit[$i]['createdtime']);
                                            $unit[$i]['createdtimeunit'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                            unset($unit[$i]['createdtime']);
                                            if(!empty($unit[$i]['modifiedtime'])){
                                                $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $unit[$i]['modifiedtime']);
                                                $unit[$i]['modifiedtimeunit'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                                unset($unit[$i]['modifiedtime']);
                                            }
                                            $unitTmp = $unit[$i];
                                        }
                                        if(!empty($dataAnggotaKeluarga[$i]) && is_array($dataAnggotaKeluarga[$i])){
                                            $dataAnggotaKeluarga[$i] = (array_change_key_case($dataAnggotaKeluarga[$i], CASE_LOWER));
                                            $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $dataAnggotaKeluarga[$i]['createdtime']);
                                            $dataAnggotaKeluarga[$i]['createdtimekk'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                            unset($dataAnggotaKeluarga[$i]['createdtime']);
                                            if(!empty($dataAnggotaKeluarga[$i]['modifiedtime'])){
                                                $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $dataAnggotaKeluarga[$i]['modifiedtime']);
                                                $dataAnggotaKeluarga[$i]['modifiedtimekk'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                                unset($dataAnggotaKeluarga[$i]['modifiedtime']);
                                            }
                                            $dataAnggotaKeluargaTmp = $dataAnggotaKeluarga[$i];
                                        }
                                        $dataTemplate = (array_change_key_case($dataTemplate, CASE_LOWER));
                                        $realData = array_merge($dataTemplate,$data,$unitTmp,$dataAnggotaKeluargaTmp);
                                        $diffKeys = array_diff_key($realData, $dataTemplate);
                                        $finalArrays = self::array_except($realData, $diffKeys);
                                        $record[] = array_values($finalArrays);
                                    }
                                }
                                $command = $connection->createCommand()->batchInsert(
                                    'txspk',$dataFields, $record
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE idProspect=VALUES(idProspect),namaCustomer=VALUES(namaCustomer),noKtp=VALUES(noKtp),alamat=VALUES(alamat)'
                                .",kodePropinsi=VALUES(kodePropinsi),kodeKota=VALUES(kodeKota),kodeKecamatan=VALUES(kodeKecamatan),alamatBPKB=VALUES(alamatBPKB)"
                                .",kodePropinsiBPKB=VALUES(kodePropinsiBPKB),kodeKotaBPKB=VALUES(kodeKotaBPKB),kodeKecamatanBPKB=VALUES(kodeKecamatanBPKB),kodeKelurahanBPKB=VALUES(kodeKelurahanBPKB)"
                                .",kodePosBPKB=VALUES(kodePosBPKB),latitude=VALUES(latitude),longitude=VALUES(longitude),NPWP=VALUES(NPWP),noKK=VALUES(noKK),alamatKK=VALUES(alamatKK)"
                                .",kodePropinsiKK=VALUES(kodePropinsiKK),kodeKotaKK=VALUES(kodeKotaKK),kodeKecamatanKK=VALUES(kodeKecamatanKK),kodeKelurahanKK=VALUES(kodeKelurahanKK),kodePosKK=VALUES(kodePosKK)"
                                .",fax=VALUES(fax),email=VALUES(email),idSalesPeople=VALUES(idSalesPeople),idEvent=VALUES(idEvent),tanggalPesanan=VALUES(tanggalPesanan),statusSPK=VALUES(statusSPK)"
                                .",dealerid=VALUES(dealerid),createdTime=VALUES(createdTime),modifiedTime=VALUES(modifiedTime)"
                                .",kodeTipeUnit=VALUES(kodeTipeUnit),kodeWarna=VALUES(kodeWarna),quantity=VALUES(quantity),hargaJual=VALUES(hargaJual),diskon=VALUES(diskon)"
                                .",amountPPN=VALUES(amountPPN),fakturPajak=VALUES(fakturPajak),tipePembayaran=VALUES(tipePembayaran),jumlahTandaJadi=VALUES(jumlahTandaJadi)"
                                .",tanggalPengiriman=VALUES(tanggalPengiriman),idSalesProgram=VALUES(idSalesProgram),idApparel=VALUES(idApparel),createdTimeUnit=VALUES(createdTimeUnit)"
                                .",anggotaKK=VALUES(anggotaKK),createdTimeKK=VALUES(createdTimeKK),modifiedtimekk=VALUES(modifiedtimekk)";
                                $command->setRawSql($sql);
                                // echo $sql;
                                $aff = $command->execute();
                                $msg .= "Row Affected : ".$aff;
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txspk','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_PRSP:
                                $dataFields = [
                                    'idProspect',  'sumberProspect', 'tanggalProspect', 'taggingProspect', 'namaLengkap','noKontak', 'noKtp',
                                    'alamat', 'kodePropinsi', 'kodeKota', 'kodeKecamatan','kodeKelurahan', 'kodePos','alamatKantor','kodePropinsiKantor','kodeKotaKantor',
                                    'kodeKecamatanKantor','kodeKelurahanKantor','kodePosKantor','latitude','longitude','kodePekerjaan','noKontakKantor',
                                    'tanggalAppointment','waktuAppointment','metodeFollowUp','testRidePreference','statusFollowUpProspecting','statusProspect','idSalesPeople', 'idEvent','dealerid','createdTimehd','modifiedTimehd',
                                    //unit
                                    'kodeTipeUnit','salesProgramId','createdTime','modifiedTime'
                                ];
                                $dataTemplate = array_fill_keys($dataFields, null);
                                $dataTemplate = (array_change_key_case($dataTemplate, CASE_LOWER));
                                $record = [];
                                foreach($arrHasil['data'] as $data){
                                    $data = (array_change_key_case($data, CASE_LOWER));
                                    $details = $data['unit'];
                                    if(count($details) != 0){
                                        unset($data['unit']);
                                        $DateTime = DateTime::createFromFormat('d/m/Y', $data['tanggalprospect']);
                                        $data['tanggalprospect'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
                                        $DateTime = DateTime::createFromFormat('d/m/Y', $data['tanggalappointment']);
                                        $data['tanggalappointment'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
										if(!empty($data['createdtime'])){
                                            $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['createdtime']);
                                            $data['createdtimehd'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
                                            unset($data['createdtime']);
                                        }
                                        if(!empty($data['modifiedtime'])){
                                            $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['modifiedtime']);
                                            $data['modifiedtimehd'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
                                            unset($data['modifiedtime']);
                                        }
                                        foreach($details as $detil){
                                            $detil = (array_change_key_case($detil, CASE_LOWER));
                                            $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $detil['createdtime']);
                                            $detil['createdtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                            if(!empty($detil['modifiedtime'])){
                                                $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $detil['modifiedtime']);
                                                $detil['modifiedtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
                                            }
                                            $realData = array_merge($dataTemplate,$data,$detil);
                                            $diffKeys = array_diff_key($realData, $dataTemplate);
                                            // var_dump((object)['template'=>$dataTemplate,'data'=>$data,'dtl'=>$detil]);die();return;
                                            $finalArrays = self::array_except($realData, $diffKeys);
                                            $record[] = array_values($finalArrays);
                                        }
                                    }
                                }
                                $aff = 0;
                                if(count($record) != 0){
                                    $command = $connection->createCommand()->batchInsert(
                                        'txprsp', $dataFields, $record
                                    );
                                    $sql = $command->getRawSql();
                                    // echo $sql;
                                    $sql .= ' ON DUPLICATE KEY UPDATE sumberProspect=VALUES(sumberProspect),tanggalProspect=VALUES(tanggalProspect),taggingProspect=VALUES(taggingProspect),namaLengkap=VALUES(namaLengkap)'
                                    .",noKontak=VALUES(noKontak),noKtp=VALUES(noKtp),alamat=VALUES(alamat),kodePropinsi=VALUES(kodePropinsi)"
                                    .",kodeKota=VALUES(kodeKota),kodeKecamatan=VALUES(kodeKecamatan),kodeKelurahan=VALUES(kodeKelurahan),kodePos=VALUES(kodePos)"
                                    .",alamatKantor=VALUES(alamatKantor),kodePropinsiKantor=VALUES(kodePropinsiKantor),kodeKotaKantor=VALUES(kodeKotaKantor),kodeKecamatanKantor=VALUES(kodeKecamatanKantor)"
                                    .",kodeKelurahanKantor=VALUES(kodeKelurahanKantor),kodePosKantor=VALUES(kodePosKantor),latitude=VALUES(latitude),longitude=VALUES(longitude)"
                                    .",kodePekerjaan=VALUES(kodePekerjaan),noKontakKantor=VALUES(noKontakKantor),tanggalAppointment=VALUES(tanggalAppointment),waktuAppointment=VALUES(waktuAppointment)"
                                    .",metodeFollowUp=VALUES(metodeFollowUp),testRidePreference=VALUES(testRidePreference),statusFollowUpProspecting=VALUES(statusFollowUpProspecting),statusProspect=VALUES(statusProspect),idSalesPeople=VALUES(idSalesPeople),idEvent=VALUES(idEvent)"
                                    .",dealerid=VALUES(dealerid),createdtimehd=VALUES(createdtimehd),modifiedtimehd=VALUES(modifiedtimehd)"
                                    .",kodeTipeUnit=VALUES(kodeTipeUnit),salesProgramId=VALUES(salesProgramId),createdTime=VALUES(createdTime),modifiedtime=VALUES(modifiedtime)";
                                    $command->setRawSql($sql);
                                    $aff = $command->execute();
                                }
                                $msg .= "Row Affected : ".$aff;
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txprsp','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_PINB:
                                $dataFields = [
                                    'noPenerimaan',  'tglPenerimaan', 'noShippingList','dealerId','createdTimehd','modifiedTimehd', 
                                    'noPO', 'jenisOrder','idWarehouse', 'partsNumber','kuantitas', 'uom', 'createdTime','modifiedTime'
                                ];
                                $dataTemplate = array_fill_keys($dataFields, null);
                                $dataTemplate = (array_change_key_case($dataTemplate, CASE_LOWER));
                                $record = [];
                                foreach($arrHasil['data'] as $data){
                                    $data = (array_change_key_case($data, CASE_LOWER));
                                    $details = $data['po'];
                                    if(!empty($data['createdtime'])){
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['createdtime']);
                                        $data['createdTimehd'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                        unset($data['createdtime']);
                                    }
                                    if(!empty($data['modifiedtime'])){
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['modifiedtime']);
                                        $data['modifiedTimehd'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                        unset($data['modifiedtime']);
                                    }
                                    if(count($details) != 0){
                                        unset($data['po']);
                                        $DateTime = DateTime::createFromFormat('d/m/Y', $data['tglpenerimaan']);
                                        $data['tglpenerimaan'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
                                        foreach($details as $detil){
                                            $detil = (array_change_key_case($detil, CASE_LOWER));
                                            $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $detil['createdtime']);
                                            $detil['createdtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                            if(!empty($detil['modifiedtime'])){
                                                $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $detil['modifiedtime']);
                                                $detil['modifiedtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                            }
                                            $realData = array_merge($dataTemplate,$data,$detil);
                                            $diffKeys = array_diff_key($realData, $dataTemplate);
                                            // var_dump($realData);die();return;
                                            $finalArrays = self::array_except($realData, $diffKeys);
                                            $record[] = array_values($finalArrays);
                                        }
                                    }
                                }
                                $command = $connection->createCommand()->batchInsert(
                                    'txpinb', $dataFields, $record
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE tglPenerimaan=VALUES(tglPenerimaan),noShippingList=VALUES(noShippingList)'
                                .",dealerId=VALUES(dealerId),createdTimehd=VALUES(createdTimehd),modifiedTimehd=VALUES(modifiedTimehd)"
                                .",noPO=VALUES(noPO),jenisOrder=VALUES(jenisOrder),idWarehouse=VALUES(idWarehouse),kuantitas=VALUES(kuantitas),uom=VALUES(uom)"
                                .",createdTime=VALUES(createdTime),modifiedTime=VALUES(modifiedTime)";
                                $command->setRawSql($sql);
                                // echo $sql;
                                $aff = $command->execute();
                                $msg .= "Row Affected : ".$aff;
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txpinb','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_PKB:
                                $dataFields = [
                                    'noWorkOrder',  'noSAForm', 'tanggalServis', 'waktuPKB', 'noPolisi','noRangka', 'noMesin',
                                    'kodeTipeUnit', 'tahunMotor', 'informasiBensin', 'kmTerakhir', 'tipeComingCustomer', 'namaPemilik',
                                    'alamatPemilik', 'kodePropinsiPemilik', 'kodeKotaPemilik', 'kodeKecamatanPemilik','kodeKelurahanPemilik',
                                    'kodePosPemilik', 'alamatPembawa', 'kodePropinsiPembawa', 'kodeKotaPembawa','kodeKecamatanPembawa',
                                    'kodeKelurahanPembawa', 'kodePosPembawa', 'namaPembawa', 'noTelpPembawa','hubunganDenganPemilik',
                                    'keluhanKonsumen', 'rekomendasiSA', 'hondaIdSA', 'hondaIdMekanik','saranMekanik','asalUnitEntry',
                                    'idPIT', 'jenisPIT', 'waktuPendaftaran', 'waktuSelesai','totalFRT','setUpPembayaran','catatanTambahan',
                                    'konfirmasiPekerjaanTambahan', 'noBukuClaimC2', 'noWorkOrderJobReturn', 'totalBiayaService','waktuPekerjaan',
                                    'statusWorkOrder','createdTime','modifiedTime','dealerid'
                                ];
                                $dataTemplate = array_fill_keys($dataFields, null);
                                $dataTemplate = (array_change_key_case($dataTemplate, CASE_LOWER));

                                $dataFieldParts = [
                                    'noWorkOrder',  'idJob', 'partsNumber', 'kuantitas', 'hargaParts','promoIdParts','discPartsAmount','discPartsPercentage',
                                    'Ppn','totalHargaParts','uangMuka','createdTime','modifiedTime'
                                ];
                                $dataTemplateParts = array_fill_keys($dataFieldParts, null);
                                $dataTemplateParts = (array_change_key_case($dataTemplateParts, CASE_LOWER));


                                $dataFieldServices = [
                                    'noWorkOrder',  'idJob', 'namaPekerjaan', 'jenisPekerjaan', 'biayaService',
                                    'promoIdJasa', 'discServiceAmount', 'discServicePercentage', 'totalHargaServis','createdTime','modifiedTime'
                                ];
                                $dataTemplateServices = array_fill_keys($dataFieldServices, null);
                                $dataTemplateServices = (array_change_key_case($dataTemplateServices, CASE_LOWER));

                                $record = $recordParts = $recordServices = [];
                                foreach($arrHasil['data'] as $data){
                                    $data = (array_change_key_case($data, CASE_LOWER));
                                    $parts = $data['parts'];
                                    unset($data['parts']);
                                    $services = $data['services'];
                                    unset($data['services']);
                                    $DateTime = DateTime::createFromFormat('d/m/Y', $data['tanggalservis']);
                                    $data['tanggalservis'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
                                    $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['waktupkb']);
                                    $data['waktupkb'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                    $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['waktupendaftaran']);
                                    $data['waktupendaftaran'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                    $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['waktuselesai']);
                                    $data['waktuselesai'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                    $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['createdtime']);
                                    $data['createdtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                    if(!empty($data['modifiedtime'])){
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['modifiedtime']);
                                        $data['modifiedtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                    }
                                    $realData = array_merge($dataTemplate,$data);
                                    $diffKeys = array_diff_key($realData, $dataTemplate);
                                    // var_dump($realData);die();return;
                                    $finalArrays = self::array_except($realData, $diffKeys);
                                    $record[] = array_values($finalArrays);
                                    foreach((array)$parts as $detil){
                                        $detil = (array_change_key_case($detil, CASE_LOWER));
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $detil['createdtime']);
                                        $detil['createdtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                        if(!empty($detil['modifiedtime'])){
                                            $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $detil['modifiedtime']);
                                            $detil['modifiedtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                        }
                                        $detil = ['noworkorder'=> $data['noworkorder']] + $detil;
                                        $realData = array_merge($dataTemplateParts,$detil);
                                        $diffKeys = array_diff_key($realData, $dataTemplateParts);
                                        // var_dump($detil);die();return;
                                        $finalArrays = self::array_except($realData, $diffKeys);
                                        $recordParts[] = array_values($finalArrays);
                                    }
                                    foreach((array)$services as $detil){
                                        $detil = (array_change_key_case($detil, CASE_LOWER));
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $detil['createdtime']);
                                        $detil['createdtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                        if(!empty($detil['modifiedtime'])){
                                            $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $detil['modifiedtime']);
                                            $detil['modifiedtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                        }
                                        $detil = ['noworkorder'=> $data['noworkorder']] + $detil;
                                        $realData = array_merge($dataTemplateServices, $detil);
                                        $diffKeys = array_diff_key($realData, $dataTemplateServices);
                                        // var_dump($realData);die();return;
                                        $finalArrays = self::array_except($realData, $diffKeys);
                                        $recordServices[] = array_values($finalArrays);
                                    }
                                }
                                $command = $connection->createCommand()->batchInsert(
                                    'txpkbhd',$dataFields,$record
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE noSAForm=VALUES(noSAForm),tanggalServis=VALUES(tanggalServis),waktuPKB=VALUES(waktuPKB)'
                                .",noPolisi=VALUES(noPolisi),noRangka=VALUES(noRangka),noMesin=VALUES(noMesin),kodeTipeUnit=VALUES(kodeTipeUnit)"
                                .",tahunMotor=VALUES(tahunMotor),informasiBensin=VALUES(informasiBensin),kmTerakhir=VALUES(kmTerakhir),tipeComingCustomer=VALUES(tipeComingCustomer)"
                                .",namaPemilik=VALUES(namaPemilik),alamatPemilik=VALUES(alamatPemilik),kodePropinsiPemilik=VALUES(kodePropinsiPemilik),kodeKotaPemilik=VALUES(kodeKotaPemilik)"
                                .",kodeKecamatanPemilik=VALUES(kodeKecamatanPemilik),kodeKelurahanPemilik=VALUES(kodeKelurahanPemilik),kodePosPemilik=VALUES(kodePosPemilik),alamatPembawa=VALUES(alamatPembawa)"
                                .",kodePropinsiPembawa=VALUES(kodePropinsiPembawa),kodeKotaPembawa=VALUES(kodeKotaPembawa),kodeKecamatanPembawa=VALUES(kodeKecamatanPembawa)"
                                .",kodeKelurahanPembawa=VALUES(kodeKelurahanPembawa),kodePosPembawa=VALUES(kodePosPembawa),namaPembawa=VALUES(namaPembawa),noTelpPembawa=VALUES(noTelpPembawa)"
                                .",hubunganDenganPemilik=VALUES(hubunganDenganPemilik),keluhanKonsumen=VALUES(keluhanKonsumen),rekomendasiSA=VALUES(rekomendasiSA),hondaIdSA=VALUES(hondaIdSA)"
                                .",hondaIdMekanik=VALUES(hondaIdMekanik),saranMekanik=VALUES(saranMekanik),asalUnitEntry=VALUES(asalUnitEntry),idPIT=VALUES(idPIT),jenisPIT=VALUES(jenisPIT)"
                                .",waktuPendaftaran=VALUES(waktuPendaftaran),waktuSelesai=VALUES(waktuSelesai),totalFRT=VALUES(totalFRT),setUpPembayaran=VALUES(setUpPembayaran)"
                                .",catatanTambahan=VALUES(catatanTambahan),konfirmasiPekerjaanTambahan=VALUES(konfirmasiPekerjaanTambahan),noBukuClaimC2=VALUES(noBukuClaimC2)"
                                .",noWorkOrderJobReturn=VALUES(noWorkOrderJobReturn),totalBiayaService=VALUES(totalBiayaService),waktuPekerjaan=VALUES(waktuPekerjaan)"
                                .",statusWorkOrder=VALUES(statusWorkOrder),createdTime=VALUES(createdTime),modifiedTime=VALUES(modifiedTime),dealerid=VALUES(dealerid)";
                                $command->setRawSql($sql);
                                // echo $sql;
                                $aff = $command->execute();
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txpkbhd','NoTrans'=>$interface
                                ];
                                if(!empty($recordParts) && is_array($recordParts)){
                                    $command = $connection->createCommand()->batchInsert(
                                        'txpkbitbarang',$dataFieldParts, $recordParts
                                    );
                                    $sql = $command->getRawSql();
                                    $sql .= ' ON DUPLICATE KEY UPDATE kuantitas=VALUES(kuantitas),hargaParts=VALUES(hargaParts),promoIdParts=VALUES(promoIdParts)'
                                    .",discPartsAmount=VALUES(discPartsAmount),discPartsPercentage=VALUES(discPartsPercentage),Ppn=VALUES(Ppn)"
                                    .",totalHargaParts=VALUES(totalHargaParts),uangMuka=VALUES(uangMuka),createdTime=VALUES(createdTime),modifiedTime=VALUES(modifiedTime)";
                                    $command->setRawSql($sql);
                                    // echo $sql;
                                    $aff = $command->execute();
                                    $recordLogs[] = [
                                        'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txpkbitbarang','NoTrans'=>$interface
                                    ];
                                }
                                if(!empty($recordServices) && is_array($recordServices)){
                                    $command = $connection->createCommand()->batchInsert(
                                        'txpkbitjasa',$dataFieldServices,$recordServices
                                    );
                                    $sql = $command->getRawSql();
                                    $sql .= ' ON DUPLICATE KEY UPDATE jenisPekerjaan=VALUES(jenisPekerjaan),biayaService=VALUES(biayaService),promoIdJasa=VALUES(promoIdJasa)'
                                    .",discServiceAmount=VALUES(discServiceAmount),discServicePercentage=VALUES(discServicePercentage),totalHargaServis=VALUES(totalHargaServis)"
                                    .",createdTime=VALUES(createdTime),modifiedTime=VALUES(modifiedTime)";
                                    $command->setRawSql($sql);
                                    // echo $sql;
                                    $aff = $command->execute();
                                    $recordLogs[] = [
                                        'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txpkbitjasa','NoTrans'=>$interface
                                    ];
                                }
                                break;
                            case self::TYPE_PRSL:
                                $dataFields = [
                                    'noSO',  'tglSO', 'idCustomer', 'namaCustomer', 'discSO','totalHargaSO','dealerId', 'createdTimehd','modifiedTimehd',
                                    'partsNumber','kuantitas', 'hargaParts', 'promoIdParts', 'discAmount', 'discPercentage', 'ppn',
                                    'totalHargaParts', 'uangMuka', 'bookingIdReference', 'createdTime', 'modifiedTime'
                                ];
                                $dataTemplate = array_fill_keys($dataFields, null);
                                $dataTemplate = (array_change_key_case($dataTemplate, CASE_LOWER));
                                $record = [];
                                foreach($arrHasil['data'] as $data){
                                    $data = (array_change_key_case($data, CASE_LOWER));
                                    $details = $data['parts'];
                                    if(count($details) != 0){
                                        unset($data['parts']);
                                        $DateTime = DateTime::createFromFormat('d/m/Y', $data['tglso']);
                                        $data['tglso'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
                                        if(!empty($data['createdtime'])){
                                            $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['createdtime']);
                                            $data['createdTimehd'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                            unset($data['createdtime']);
                                        }
                                        if(!empty($data['modifiedtime'])){
                                            $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['modifiedtime']);
                                            $data['modifiedTimehd'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                            unset($data['modifiedtime']);
                                        }
                                        foreach($details as $detil){
                                            $detil = (array_change_key_case($detil, CASE_LOWER));
                                            $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $detil['createdtime']);
                                            $detil['createdtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                            if(!empty($detil['modifiedtime'])){
                                                $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $detil['modifiedtime']);
                                                $detil['modifiedtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                            }
                                            $realData = array_merge($dataTemplate,$data,$detil);
                                            $diffKeys = array_diff_key($realData, $dataTemplate);
                                            $finalArrays = self::array_except($realData, $diffKeys);
                                            $record[] = array_values($finalArrays);
                                        }
                                    }
                                }
                                $command = $connection->createCommand()->batchInsert(
                                    'txprsl',$dataFields, $record
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE tglSO=VALUES(tglSO),idCustomer=VALUES(idCustomer),namaCustomer=VALUES(namaCustomer),discSO=VALUES(discSO)'
                                .",totalHargaSO=VALUES(totalHargaSO),dealerId=VALUES(dealerId),createdTimehd=VALUES(createdTimehd),modifiedTimehd=VALUES(modifiedTimehd)"
                                .",kuantitas=VALUES(kuantitas),hargaParts=VALUES(hargaParts),promoIdParts=VALUES(promoIdParts),discAmount=VALUES(discAmount),discPercentage=VALUES(discPercentage)"
                                .",ppn=VALUES(ppn),totalHargaParts=VALUES(totalHargaParts),uangMuka=VALUES(uangMuka),bookingIdReference=VALUES(bookingIdReference),createdTime=VALUES(createdTime)"
                                .",modifiedtime=VALUES(modifiedtime)";
                                $command->setRawSql($sql);
                                // echo $sql;
                                $aff = $command->execute();
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txprsl','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_DPHLO:
                                $dataFields = [
                                    'noInvoiceUangJaminan',  'idHLODocument', 'noWorkOrder',
                                    'idCustomer', 'dealerId', 'createdTime', 'modifiedTime', 'parts'
                                ];
                                $dataTemplate = array_fill_keys($dataFields, null);
                                $dataTemplate = (array_change_key_case($dataTemplate, CASE_LOWER));
                                $record = [];
                                foreach($arrHasil['data'] as $data){
                                    $data = (array_change_key_case($data, CASE_LOWER));
                                    if(!empty($data['createdtime'])){
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['createdtime']);
                                        $data['createdtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                    }
                                    if(!empty($data['modifiedtime'])){
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['modifiedtime']);
                                        $data['modifiedtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                    }
                                    $data['parts'] = json_encode($data['parts']);
                                    $realData = array_merge($dataTemplate,$data);
                                    $diffKeys = array_diff_key($realData, $dataTemplate);
                                    // var_dump($realData);die();return;
                                    $finalArrays = self::array_except($realData, $diffKeys);
                                    $record[] = array_values($finalArrays);
                                }
                                $command = $connection->createCommand()->batchInsert(
                                    'txdphlo',$dataFields, $record
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE idHLODocument=VALUES(idHLODocument),noWorkOrder=VALUES(noWorkOrder),idCustomer=VALUES(idCustomer)'
                                .",dealerId=VALUES(dealerId),createdTime=VALUES(createdTime),modifiedTime=VALUES(modifiedTime),parts=VALUES(parts)";
                                $command->setRawSql($sql);
                                // echo $sql;
                                $aff = $command->execute();
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txdphlo','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_INV2_READ:
                                $dataFields = [
                                    'noWorkOrder',  'noNJB', 'tanggalNJB', 'totalHargaNJB', 'noNSC',
                                    'tanggalNSC', 'totalHargaNSC', 'hondaIdSA', 'hondaIdMekanik', 'dealerId',
                                    'createdTime', 'modifiedTime', 'njb', 'nsc'
                                ];
                                $dataTemplate = array_fill_keys($dataFields, null);
                                $dataTemplate = (array_change_key_case($dataTemplate, CASE_LOWER));
                                $record = [];
                                // var_dump($arrHasil);die();return;
                                foreach($arrHasil['data'] as $data){
                                    $data = (array_change_key_case($data, CASE_LOWER));
                                    // var_dump($data);die();return;
                                    $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['tanggalnjb']);
                                    $data['tanggalnjb'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
                                    $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['tanggalnsc']);
                                    $data['tanggalnsc'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
                                    if(!empty($data['createdtime'])){
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['createdtime']);
                                        $data['createdtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                    }
                                    if(!empty($data['modifiedtime'])){
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['modifiedtime']);
                                        $data['modifiedtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                    }
                                    $data['njb'] = json_encode($data['njb']);
                                    $data['nsc'] = json_encode($data['nsc']);
                                    $realData = array_merge($dataTemplate,$data);
                                    $diffKeys = array_diff_key($realData, $dataTemplate);
                                    // var_dump($realData);die();return;
                                    $finalArrays = self::array_except($realData, $diffKeys);
                                    $record[] = array_values($finalArrays);
                                }
                                $command = $connection->createCommand()->batchInsert(
                                    'txinv2',$dataFields, $record
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE noNJB=VALUES(noNJB),tanggalNJB=VALUES(tanggalNJB),totalHargaNJB=VALUES(totalHargaNJB)'
                                .",noNSC=VALUES(noNSC),tanggalNSC=VALUES(tanggalNSC),totalHargaNSC=VALUES(totalHargaNSC),hondaIdSA=VALUES(hondaIdSA),njb=VALUES(njb),nsc=VALUES(nsc)"
                                .",hondaIdMekanik=VALUES(hondaIdMekanik),dealerId=VALUES(dealerId),createdTime=VALUES(createdTime),modifiedTime=VALUES(modifiedTime)";
                                $command->setRawSql($sql);
                                // echo $sql;
                                $aff = $command->execute();
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txinv2','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_INV1_READ:
                                $dataFields = [
                                    'idInvoice',  'idSPK', 'idCustomer', 'amount', 'tipePembayaran',
                                    'caraBayar', 'status', 'note', 'dealerId', 'createdTime', 'modifiedTime'
                                ];
                                $dataTemplate = array_fill_keys($dataFields, null);
                                $dataTemplate = (array_change_key_case($dataTemplate, CASE_LOWER));
                                $record = [];
                                // var_dump($arrHasil);die();return;
                                foreach($arrHasil['data'] as $data){
                                    $data = (array_change_key_case($data, CASE_LOWER));
                                    // var_dump($data);die();return;
                                    if(!empty($data['createdtime'])){
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['createdtime']);
                                        $data['createdtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                    }
                                    if(!empty($data['modifiedtime'])){
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['modifiedtime']);
                                        $data['modifiedtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                    }
                                    $realData = array_merge($dataTemplate,$data);
                                    $diffKeys = array_diff_key($realData, $dataTemplate);
                                    // var_dump($realData);die();return;
                                    $finalArrays = self::array_except($realData, $diffKeys);
                                    $record[] = array_values($finalArrays);
                                }
                                $command = $connection->createCommand()->batchInsert(
                                    'txinv2',$dataFields, $record
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE idSPK=VALUES(idSPK),idCustomer=VALUES(idCustomer),amount=VALUES(amount)'
                                .",tipePembayaran=VALUES(tipePembayaran),caraBayar=VALUES(caraBayar),status=VALUES(status),note=VALUES(note)"
                                .",dealerId=VALUES(dealerId),createdTime=VALUES(createdTime),modifiedTime=VALUES(modifiedTime)";
                                $command->setRawSql($sql);
                                // echo $sql;
                                $aff = $command->execute();
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txinv2','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_CDB:
                                $dataFields = [
                                    'idSpk',  'idSalesPeople', 'idEvent', 'tanggalPesanan', 'statusSPK',
                                    'tanggalPengiriman', 'tipePembayaran', 'dealerId', 'createdTime', 'modifiedTime', 'unit'
                                ];
                                $dataTemplate = array_fill_keys($dataFields, null);
                                $dataTemplate = (array_change_key_case($dataTemplate, CASE_LOWER));
                                $record = [];
                                // var_dump($arrHasil);die();return;
                                foreach($arrHasil['data'] as $data){
                                    $data = (array_change_key_case($data, CASE_LOWER));
                                    // var_dump($data);die();return;
                                    if(!empty($data['tanggalpesanan'])){
                                        $DateTime = DateTime::createFromFormat('d/m/Y', $data['tanggalpesanan']);
                                        $data['tanggalpesanan'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
                                    }
                                    if(!empty($data['tanggalpengiriman'])){
                                        $DateTime = DateTime::createFromFormat('d/m/Y', $data['tanggalpengiriman']);
                                        $data['tanggalpengiriman'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
                                    }
                                    if(!empty($data['createdtime'])){
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['createdtime']);
                                        $data['createdtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                    }
                                    if(!empty($data['modifiedtime'])){
                                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['modifiedtime']);
                                        $data['modifiedtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                                    }
                                    $data['unit'] = json_encode($data['unit']);
                                    $realData = array_merge($dataTemplate,$data);
                                    $diffKeys = array_diff_key($realData, $dataTemplate);
                                    // var_dump($realData);die();return;
                                    $finalArrays = self::array_except($realData, $diffKeys);
                                    $record[] = array_values($finalArrays);
                                }
                                $command = $connection->createCommand()->batchInsert(
                                    'txcdb',$dataFields, $record
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE idSalesPeople=VALUES(idSalesPeople),idEvent=VALUES(idEvent),tanggalPesanan=VALUES(tanggalPesanan)'
                                .",statusSPK=VALUES(statusSPK),tanggalPengiriman=VALUES(tanggalPengiriman),tipePembayaran=VALUES(tipePembayaran),dealerId=VALUES(dealerId)"
                                .",unit=VALUES(unit),createdTime=VALUES(createdTime),modifiedTime=VALUES(modifiedTime)";
                                $command->setRawSql($sql);
                                // echo $sql;
                                $aff = $command->execute();
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txcdb','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_UINB_ADD:
                                $command = $connection->createCommand()->batchInsert(
                                    'txuinbav03',['documentNo','Response'], $arrHasil['data']
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE Response=VALUES(Response)';
                                $command->setRawSql($sql);
                                $aff = $command->execute();
                                $recordLogs = [];
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txuinbav03','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_BAST_ADD:
                                $command = $connection->createCommand()->batchInsert(
                                    'txbastav03',['documentNo','Response'], $arrHasil['data']
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE Response=VALUES(Response)';
                                $command->setRawSql($sql);
                                $aff = $command->execute();
                                $recordLogs = [];
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txbastav03','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_PRSP_ADD:
                                $command = $connection->createCommand()->batchInsert(
                                    'txprspav03',['documentNo','Response'], $arrHasil['data']
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE Response=VALUES(Response)';
                                $command->setRawSql($sql);
                                $aff = $command->execute();
                                $recordLogs = [];
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txprspav03','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_LSNG_ADD:
                                $command = $connection->createCommand()->batchInsert(
                                    'txlsngav03',['documentNo','Response'], $arrHasil['data']
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE Response=VALUES(Response)';
                                $command->setRawSql($sql);
                                $aff = $command->execute();
                                $recordLogs = [];
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txlsngav03','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_SPK_ADD:
                                $command = $connection->createCommand()->batchInsert(
                                    'txspkav03',['documentNo','Response'], $arrHasil['data']
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE Response=VALUES(Response)';
                                $command->setRawSql($sql);
                                $aff = $command->execute();
                                $recordLogs = [];
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txspkav03','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_DOCH_ADD:
                                $command = $connection->createCommand()->batchInsert(
                                    'txdochav03',['documentNo','Response'], $arrHasil['data']
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE Response=VALUES(Response)';
                                $command->setRawSql($sql);
                                $aff = $command->execute();
                                $recordLogs = [];
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txdochav03','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_PINB_ADD:
                                $command = $connection->createCommand()->batchInsert(
                                    'txpinbav03',['documentNo','Response'], $arrHasil['data']
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE Response=VALUES(Response)';
                                $command->setRawSql($sql);
                                $aff = $command->execute();
                                $recordLogs = [];
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txpinbav03','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_PRSL_ADD:
                                $command = $connection->createCommand()->batchInsert(
                                    'txprslav03',['documentNo','Response'], $arrHasil['data']
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE Response=VALUES(Response)';
                                $command->setRawSql($sql);
                                $aff = $command->execute();
                                $recordLogs = [];
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txprslav03','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_PKB_ADD:
                                $command = $connection->createCommand()->batchInsert(
                                    'txpkbav03',['documentNo','Response'], $arrHasil['data']
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE Response=VALUES(Response)';
                                $command->setRawSql($sql);
                                $aff = $command->execute();
                                $recordLogs = [];
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txpkbav03','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_CDB_ADD:
                                $command = $connection->createCommand()->batchInsert(
                                    'txcdbav03',['documentNo','Response'], $arrHasil['data']
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE Response=VALUES(Response)';
                                $command->setRawSql($sql);
                                $aff = $command->execute();
                                $recordLogs = [];
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txcdbav03','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_BOOK_ADD:
                                $command = $connection->createCommand()->batchInsert(
                                    'txbookav03',['documentNo','Response'], $arrHasil['data']
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE Response=VALUES(Response)';
                                $command->setRawSql($sql);
                                $aff = $command->execute();
                                $recordLogs = [];
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txbookav03','NoTrans'=>$interface
                                ];
                                break;
                            case self::TYPE_LEADS_ADD:
                                $command = $connection->createCommand()->batchInsert(
                                    'txleadskav03',['documentNo','Response'], $arrHasil['data']
                                );
                                $sql = $command->getRawSql();
                                $sql .= ' ON DUPLICATE KEY UPDATE Response=VALUES(Response)';
                                $command->setRawSql($sql);
                                $aff = $command->execute();
                                $recordLogs = [];
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>"Row Affected : ".$aff,'LogTabel'=>'txleadskav03','NoTrans'=>$interface
                                ];
                                break;
                        }
                    
                }
            } else {
                $msg = "Outlet : " . $key . ", Data : undefined";
            }
            if(Yii::$app->request->isConsoleRequest){
                echo $msg;
            }else{
                Yii::info($msg, 'API RESULTS');
            }
            $commandLog = $connection->createCommand()->batchInsert('tuserlog', [
                'LogTglJam','UserID','LogJenis','LogNama','LogTabel','NoTrans'
            ], $recordLogs);
            $commandLog->execute();
        }catch(Exception $e){
            $msg .= "Error : ".$e->getMessage();
        }
    }

    private function getWarna()
    {
        $dbDealer = Yii::$app->params['mainDealer'][$this->mainDealer]['db'];
        $arrWarna = General::cCmd("SELECT KodeWarna,DeskripsiWarna FROM $dbDealer.tdwarnamtr;")->queryAll();
        return ArrayHelper::map($arrWarna, 'KodeWarna', 'DeskripsiWarna');
    }
    private function getType()
    {
        $dbDealer = Yii::$app->params['mainDealer'][$this->mainDealer]['db'];
        $arr      = General::cCmd("SELECT KodeType,DeskripsiType FROM $dbDealer.tdtypemtr;")->queryAll();
        return ArrayHelper::map($arr, 'KodeType', 'DeskripsiType');
    }

    static public function array_search_m($val,$haystack){
        if(is_array($val)){
            $searchResult = false;
            foreach($haystack as $key => &$item){
                $checkIndex = true;
                foreach($val as $valKey => $valItem){
                    if($valItem != $item[$valKey]){
                        $checkIndex = false;
                        break;
                    }
                }
                if($checkIndex){
                    return $key;
                }
            }
            return $searchResult;
        }else{
            return array_search($val, $haystack);
        }
    }

    static public function callSPK($post)
    {
        $post['UserID']            = $post[ 'UserID' ] ?? \aunit\components\Menu::getUserLokasi()['UserID'];
        $post['KodeAkses']         = \aunit\components\Menu::getUserLokasi()['KodeAkses'];
        $post['LokasiKode']        = \aunit\components\Menu::getUserLokasi()['LokasiKode'];
        $post['NoTrans']           = $post['NoTrans'] ?? '';
        $post['idSpk']             = $post['idSpk'] ?? '';
        $post['idProspect']        = $post['idProspect'] ?? '';
        $post['namaCustomer']      = $post['namaCustomer'] ?? '';
        $post['noKtp']             = $post['noKtp'] ?? '';
        $post['alamat']            = $post['alamat'] ?? '';
        $post['kodePropinsi']      = $post['kodePropinsi'] ?? '';
        $post['kodeKota']          = $post['kodeKota'] ?? '';
        $post['kodeKecamatan']     = $post['kodeKecamatan'] ?? '';
        $post['kodeKelurahan']     = $post['kodeKelurahan'] ?? '';
        $post['kodePos']           = $post['kodePos'] ?? '';
        $post['noKontak']          = $post['noKontak'] ?? '';
        $post['namaBPKB']          = $post['namaBPKB'] ?? '';
        $post['noKTPBPKB']         = $post['noKTPBPKB'] ?? '';
        $post['alamatBPKB']        = $post['alamatBPKB'] ?? '';
        $post['kodePropinsiBPKB']  = $post['kodePropinsiBPKB'] ?? '';
        $post['kodeKotaBPKB']      = $post['kodeKotaBPKB'] ?? '';
        $post['kodeKecamatanBPKB'] = $post['kodeKecamatanBPKB'] ?? '';
        $post['kodeKelurahanBPKB'] = $post['kodeKelurahanBPKB'] ?? '';
        $post['kodePosBPKB']       = $post['kodePosBPKB'] ?? '';
        $post['latitude']          = $post['latitude'] ?? '';
        $post['longitude']         = $post['longitude'] ?? '';
        $post['NPWP']              = $post['NPWP'] ?? '';
        $post['noKK']              = $post['noKK'] ?? '';
        $post['alamatKK']          = $post['alamatKK'] ?? '';
        $post['kodePropinsiKK']    = $post['kodePropinsiKK'] ?? '';
        $post['kodeKotaKK']        = $post['kodeKotaKK'] ?? '';
        $post['kodeKecamatanKK']   = $post['kodeKecamatanKK'] ?? '';
        $post['kodeKelurahanKK']   = $post['kodeKelurahanKK'] ?? '';
        $post['kodePosKK']         = $post['kodePosKK'] ?? '';
        $post['fax']               = $post['fax'] ?? '';
        $post['email']             = $post['email'] ?? '';
        $post['idSalesPeople']     = $post['idSalesPeople'] ?? '';
        $post['idEvent']           = $post['idEvent'] ?? '';
        $post['tanggalPesanan']    = $post['tanggalPesanan'] ?? '';
        $post['statusSPK']         = $post['statusSPK'] ?? '';
        $post['createdTime']       = $post['createdTime'] ?? '';
        try {
            General::cCmd("SET @NoTrans = ?;")->bindParam(1, $post['NoTrans'])->execute();
            General::cCmd("CALL ispkhd(?,@Status,@Keterangan,?,?,?,@NoTrans,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);")
                ->bindParam(1, $post['Action'])
                ->bindParam(2, $post['UserID'])
                ->bindParam(3, $post['KodeAkses'])
                ->bindParam(4, $post['LokasiKode'])
//                ->bindParam( 5,    $post['NoTrans'] )
                ->bindParam(5, $post['idSpk'])
                ->bindParam(6, $post['idProspect'])
                ->bindParam(7, $post['namaCustomer'])
                ->bindParam(8, $post['noKtp'])
                ->bindParam(9, $post['alamat'])
                ->bindParam(10, $post['kodePropinsi'])
                ->bindParam(11, $post['kodeKota'])
                ->bindParam(12, $post['kodeKecamatan'])
                ->bindParam(13, $post['kodeKelurahan'])
                ->bindParam(14, $post['kodePos'])
                ->bindParam(15, $post['noKontak'])
                ->bindParam(16, $post['namaBPKB'])
                ->bindParam(17, $post['noKTPBPKB'])
                ->bindParam(18, $post['alamatBPKB'])
                ->bindParam(19, $post['kodePropinsiBPKB'])
                ->bindParam(20, $post['kodeKotaBPKB'])
                ->bindParam(21, $post['kodeKecamatanBPKB'])
                ->bindParam(22, $post['kodeKelurahanBPKB'])
                ->bindParam(23, $post['kodePosBPKB'])
                ->bindParam(24, $post['latitude'])
                ->bindParam(25, $post['longitude'])
                ->bindParam(26, $post['NPWP'])
                ->bindParam(27, $post['noKK'])
                ->bindParam(28, $post['alamatKK'])
                ->bindParam(29, $post['kodePropinsiKK'])
                ->bindParam(30, $post['kodeKotaKK'])
                ->bindParam(31, $post['kodeKecamatanKK'])
                ->bindParam(32, $post['kodeKelurahanKK'])
                ->bindParam(33, $post['kodePosKK'])
                ->bindParam(34, $post['fax'])
                ->bindParam(35, $post['email'])
                ->bindParam(36, $post['idSalesPeople'])
                ->bindParam(37, $post['idEvent'])
                ->bindParam(38, $post['tanggalPesanan'])
                ->bindParam(39, $post['statusSPK'])
                ->bindParam(40, $post['createdTime'])
                ->execute();
            $exe = General::cCmd("SELECT @NoTrans,@Status,@Keterangan;")->queryOne();
        } catch (Exception $e) {
            return [
                'NoTrans'    => $post['NoTrans'],
                'status'     => 1,
                'keterangan' => $e->getMessage(),
            ];
        }
        $NoTrans    = $exe['@NoTrans'] ?? $post['NoTrans'];
        $status     = $exe['@Status'] ?? 1;
        $keterangan = $exe['@Keterangan'] ?? 'Action : [' . $post['Action'] . '] Keterangan NULL';
        return [
            'NoTrans'    => $NoTrans,
            'status'     => $status,
            'keterangan' => str_replace(" ", "&nbsp;", $keterangan),
        ];
    }

    static public function callSPKitmotor($post)
    {
        $post['UserID']            = $post[ 'UserID' ] ?? \aunit\components\Menu::getUserLokasi()['UserID'];
        $post['KodeAkses']         = \aunit\components\Menu::getUserLokasi()['KodeAkses'];
        $post['LokasiKode']        = \aunit\components\Menu::getUserLokasi()['LokasiKode'];
        $post['NoTrans']           = $post['NoTrans'] ?? '';
        $post['kodeTipeUnit']      = $post['kodeTipeUnit'] ?? '';
        $post['kodeWarna']         = $post['kodeWarna'] ?? '';
        $post['quantity']          = $post['quantity'] ?? '';
        $post['hargaJual']         = $post['hargaJual'] ?? '';
        $post['diskon']            = $post['diskon'] ?? '';
        $post['kodePPN']           = $post['kodePPN'] ?? '';
        $post['fakturPajak']       = $post['fakturPajak'] ?? '';
        $post['tipePembayaran']    = $post['tipePembayaran'] ?? '';
        $post['jumlahTandaJadi']   = $post['jumlahTandaJadi'] ?? '';
        $post['tanggalPengiriman'] = $post['tanggalPengiriman'] ?? '';
        $post['idSalesProgram']    = $post['idSalesProgram'] ?? '';
        $post['idApparel']         = $post['idApparel'] ?? '';
        $post['createdTime']       = $post['createdTime'] ?? '';
        try {
            General::cCmd("SET @NoTrans = ?;")->bindParam(1, $post['NoTrans'])->execute();
            General::cCmd("CALL ispkitmotor(?,@Status,@Keterangan,?,?,?,@NoTrans,?,?,?,?,?,?,?,?,?,?,?,?,?);")
                ->bindParam(1, $post['Action'])
                ->bindParam(2, $post['UserID'])
                ->bindParam(3, $post['KodeAkses'])
                ->bindParam(4, $post['LokasiKode'])
//                ->bindParam( 5,    $post['NoTrans'] )
                ->bindParam(5, $post['kodeTipeUnit'])
                ->bindParam(6, $post['kodeWarna'])
                ->bindParam(7, $post['quantity'])
                ->bindParam(8, $post['hargaJual'])
                ->bindParam(9, $post['diskon'])
                ->bindParam(10, $post['kodePPN'])
                ->bindParam(11, $post['fakturPajak'])
                ->bindParam(12, $post['tipePembayaran'])
                ->bindParam(13, $post['jumlahTandaJadi'])
                ->bindParam(14, $post['tanggalPengiriman'])
                ->bindParam(15, $post['idSalesProgram'])
                ->bindParam(16, $post['idApparel'])
                ->bindParam(17, $post['createdTime'])
                ->execute();
            $exe = General::cCmd("SELECT @NoTrans,@Status,@Keterangan;")->queryOne();
        } catch (Exception $e) {
            return [
                'NoTrans'    => $post['NoTrans'],
                'status'     => 1,
                'keterangan' => $e->getMessage(),
            ];
        }
        $NoTrans    = $exe['@NoTrans'] ?? $post['NoTrans'];
        $status     = $exe['@Status'] ?? 1;
        $keterangan = $exe['@Keterangan'] ?? 'Action : [' . $post['Action'] . '] Keterangan NULL';
        return [
            'NoTrans'    => $NoTrans,
            'status'     => $status,
            'keterangan' => str_replace(" ", "&nbsp;", $keterangan),
        ];
    }

    static public function callSPKitkk($post)
    {
        $post['UserID']      = $post[ 'UserID' ] ?? \aunit\components\Menu::getUserLokasi()['UserID'];
        $post['KodeAkses']   = \aunit\components\Menu::getUserLokasi()['KodeAkses'];
        $post['LokasiKode']  = \aunit\components\Menu::getUserLokasi()['LokasiKode'];
        $post['NoTrans']     = $post['NoTrans'] ?? '';
        $post['anggotaKK']   = $post['anggotaKK'] ?? '';
        $post['createdTime'] = $post['createdTime'] ?? '';
        try {
            General::cCmd("SET @NoTrans = ?;")->bindParam(1, $post['NoTrans'])->execute();
            General::cCmd("CALL ispkitkk(?,@Status,@Keterangan,?,?,?,@NoTrans,?,?);")
                ->bindParam(1, $post['Action'])
                ->bindParam(2, $post['UserID'])
                ->bindParam(3, $post['KodeAkses'])
                ->bindParam(4, $post['LokasiKode'])
//                ->bindParam( 5,    $post['NoTrans'] )
                ->bindParam(5, $post['anggotaKK'])
                ->bindParam(6, $post['createdTime'])
                ->execute();
            $exe = General::cCmd("SELECT @NoTrans,@Status,@Keterangan;")->queryOne();
        } catch (Exception $e) {
            return [
                'NoTrans'    => $post['NoTrans'],
                'status'     => 1,
                'keterangan' => $e->getMessage(),
            ];
        }
        $NoTrans    = $exe['@NoTrans'] ?? $post['NoTrans'];
        $status     = $exe['@Status'] ?? 1;
        $keterangan = $exe['@Keterangan'] ?? 'Action : [' . $post['Action'] . '] Keterangan NULL';
        return [
            'NoTrans'    => $NoTrans,
            'status'     => $status,
            'keterangan' => str_replace(" ", "&nbsp;", $keterangan),
        ];
    }

    static public function callBAST($post)
    {
        $post['UserID']                 = \aunit\components\Menu::getUserLokasi()['UserID'];
        $post['KodeAkses']              = \aunit\components\Menu::getUserLokasi()['KodeAkses'];
        $post['LokasiKode']             = \aunit\components\Menu::getUserLokasi()['LokasiKode'];
        $post['NoTrans']                = $post['NoTrans'] ?? '';
        $post['deliveryDocumentId']     = $post['deliveryDocumentId'] ?? '';
        $post['tanggalPengiriman']      = $post['tanggalPengiriman'] ?? '';
        $post['idDriver']               = $post['idDriver'] ?? '';
        $post['statusDeliveryDocument'] = $post['statusDeliveryDocument'] ?? '';
        try {
            General::cCmd("SET @NoTrans = ?;")->bindParam(1, $post['NoTrans'])->execute();
            General::cCmd("CALL ibasthd(?,@Status,@Keterangan,?,?,?,@NoTrans,?,?,?,?);")
                ->bindParam(1, $post['Action'])
                ->bindParam(2, $post['UserID'])
                ->bindParam(3, $post['KodeAkses'])
                ->bindParam(4, $post['LokasiKode'])
//                ->bindParam( 5,    $post['NoTrans'] )
                ->bindParam(5, $post['deliveryDocumentId'])
                ->bindParam(6, $post['tanggalPengiriman'])
                ->bindParam(7, $post['idDriver'])
                ->bindParam(8, $post['statusDeliveryDocument'])
                ->execute();
            $exe = General::cCmd("SELECT @NoTrans,@Status,@Keterangan;")->queryOne();
        } catch (Exception $e) {
            return [
                'NoTrans'    => $post['NoTrans'],
                'status'     => 1,
                'keterangan' => $e->getMessage(),
            ];
        }
        $NoTrans    = $exe['@NoTrans'] ?? $post['NoTrans'];
        $status     = $exe['@Status'] ?? 1;
        $keterangan = $exe['@Keterangan'] ?? 'Action : [' . $post['Action'] . '] Keterangan NULL';
        return [
            'NoTrans'    => $NoTrans,
            'status'     => $status,
            'keterangan' => str_replace(" ", "&nbsp;", $keterangan),
        ];
    }

    static public function callBASTit($post)
    {
        $post['UserID']          = \aunit\components\Menu::getUserLokasi()['UserID'];
        $post['KodeAkses']       = \aunit\components\Menu::getUserLokasi()['KodeAkses'];
        $post['LokasiKode']      = \aunit\components\Menu::getUserLokasi()['LokasiKode'];
        $post['NoTrans']         = $post['NoTrans'] ?? '';
        $post['noSO']            = $post['noSO'] ?? '';
        $post['idSPK']           = $post['idSPK'] ?? '';
        $post['noMesin']         = $post['noMesin'] ?? '';
        $post['noRangka']        = $post['noRangka'] ?? '';
        $post['idCustomer']      = $post['idCustomer'] ?? '';
        $post['waktuPengiriman'] = $post['waktuPengiriman'] ?? '';

        $post['checklistKelengkapan'] = $post['checklistKelengkapan'] ?? '';
        $post['lokasiPengiriman']     = $post['lokasiPengiriman'] ?? '';
        $post['latitude']             = $post['latitude'] ?? '';
        $post['longitude']            = $post['longitude'] ?? '';
        $post['namaPenerima']         = $post['namaPenerima'] ?? '';
        $post['noKontakPenerima']     = $post['noKontakPenerima'] ?? '';
        $post['createdTime']          = $post['createdTime'] ?? '';
        try {
            General::cCmd("SET @NoTrans = ?;")->bindParam(1, $post['NoTrans'])->execute();
            General::cCmd("CALL ibastit(?,@Status,@Keterangan,?,?,?,@NoTrans,?,?,?,?,?,?,?,?,?,?,?,?,?);")
                ->bindParam(1, $post['Action'])
                ->bindParam(2, $post['UserID'])
                ->bindParam(3, $post['KodeAkses'])
                ->bindParam(4, $post['LokasiKode'])
//                ->bindParam( 5,    $post['NoTrans'] )
                ->bindParam(5, $post['noSO'])
                ->bindParam(6, $post['idSPK'])
                ->bindParam(7, $post['noMesin'])
                ->bindParam(8, $post['noRangka'])
                ->bindParam(9, $post['idCustomer'])
                ->bindParam(10, $post['waktuPengiriman'])
                ->bindParam(11, $post['checklistKelengkapan'])
                ->bindParam(12, $post['lokasiPengiriman'])
                ->bindParam(13, $post['latitude'])
                ->bindParam(14, $post['longitude'])
                ->bindParam(15, $post['namaPenerima'])
                ->bindParam(16, $post['noKontakPenerima'])
                ->bindParam(17, $post['createdTime'])
                ->execute();
            $exe = General::cCmd("SELECT @NoTrans,@Status,@Keterangan;")->queryOne();
        } catch (Exception $e) {
            return [
                'NoTrans'    => $post['NoTrans'],
                'status'     => 1,
                'keterangan' => $e->getMessage(),
            ];
        }
        $NoTrans    = $exe['@NoTrans'] ?? $post['NoTrans'];
        $status     = $exe['@Status'] ?? 1;
        $keterangan = $exe['@Keterangan'] ?? 'Action : [' . $post['Action'] . '] Keterangan NULL';
        return [
            'NoTrans'    => $NoTrans,
            'status'     => $status,
            'keterangan' => str_replace(" ", "&nbsp;", $keterangan),
        ];
    }

    static public function callINV($post)
    {
        $post['UserID']         = \aunit\components\Menu::getUserLokasi()['UserID'];
        $post['KodeAkses']      = \aunit\components\Menu::getUserLokasi()['KodeAkses'];
        $post['LokasiKode']     = \aunit\components\Menu::getUserLokasi()['LokasiKode'];
        $post['NoTrans']        = $post['NoTrans'] ?? '';
        $post['idInvoice']      = $post['idInvoice'] ?? '';
        $post['idSPK']          = $post['idSPK'] ?? '';
        $post['idCustomer']     = $post['idCustomer'] ?? '';
        $post['Amount']         = $post['Amount'] ?? '';
        $post['tipePembayaran'] = $post['tipePembayaran'] ?? '';
        $post['caraBayar']      = $post['caraBayar'] ?? '';
        $post['Status']         = $post['Status'] ?? '';
        $post['Note']           = $post['Note'] ?? '';
        $post['createdTime']    = $post['createdTime'] ?? '';
        try {
            General::cCmd("SET @NoTrans = ?;")->bindParam(1, $post['NoTrans'])->execute();
            General::cCmd("CALL iinvhd(?,@Status,@Keterangan,?,?,?,@NoTrans,?,?,?,?,?,?,?,?,?);")
                ->bindParam(1, $post['Action'])
                ->bindParam(2, $post['UserID'])
                ->bindParam(3, $post['KodeAkses'])
                ->bindParam(4, $post['LokasiKode'])
//                ->bindParam( 5,    $post['NoTrans'] )
                ->bindParam(5, $post['noSO'])
                ->bindParam(6, $post['idSPK'])
                ->bindParam(7, $post['idCustomer'])
                ->bindParam(8, $post['Amount'])
                ->bindParam(9, $post['tipePembayaran'])
                ->bindParam(10, $post['caraBayar'])
                ->bindParam(11, $post['Status'])
                ->bindParam(12, $post['Note'])
                ->bindParam(13, $post['createdTime'])
                ->execute();
            $exe = General::cCmd("SELECT @NoTrans,@Status,@Keterangan;")->queryOne();
        } catch (Exception $e) {
            return [
                'NoTrans'    => $post['NoTrans'],
                'status'     => 1,
                'keterangan' => $e->getMessage(),
            ];
        }
        $NoTrans    = $exe['@NoTrans'] ?? $post['NoTrans'];
        $status     = $exe['@Status'] ?? 1;
        $keterangan = $exe['@Keterangan'] ?? 'Action : [' . $post['Action'] . '] Keterangan NULL';
        return [
            'NoTrans'    => $NoTrans,
            'status'     => $status,
            'keterangan' => str_replace(" ", "&nbsp;", $keterangan),
        ];
    }

    static public function callLSNG($post)
    {
        $post['UserID']                            = \aunit\components\Menu::getUserLokasi()['UserID'];
        $post['KodeAkses']                         = \aunit\components\Menu::getUserLokasi()['KodeAkses'];
        $post['LokasiKode']                        = \aunit\components\Menu::getUserLokasi()['LokasiKode'];
        $post['NoTrans']                           = $post['NoTrans'] ?? '';
        $post['idDokumenPengajuan']                = $post['idDokumenPengajuan'] ?? '';
        $post['idSPK']                             = $post['idSPK'] ?? '';
        $post['jumlahDP']                          = $post['jumlahDP'] ?? '';
        $post['tenor']                             = $post['tenor'] ?? '';
        $post['jumlahCicilan']                     = $post['jumlahCicilan'] ?? '';
        $post['tanggalPengajuan']                  = $post['tanggalPengajuan'] ?? '';
        $post['idFinanceCompany']                  = $post['idFinanceCompany'] ?? '';
        $post['namaFinanceCompany']                = $post['namaFinanceCompany'] ?? '';
        $post['idPOFinanceCompany']                = $post['idPOFinanceCompany'] ?? '';
        $post['tanggalPembuatanPO']                = $post['tanggalPembuatanPO'] ?? '';
        $post['tanggalPengirimanPOFinanceCompany'] = $post['tanggalPengirimanPOFinanceCompany'] ?? '';
        $post['createdTime']                       = $post['createdTime'] ?? '';
        try {
            General::cCmd("SET @NoTrans = ?;")->bindParam(1, $post['NoTrans'])->execute();
            General::cCmd("CALL ilsnghd(?,@Status,@Keterangan,?,?,?,@NoTrans,?,?,?,?,?,?,?,?,?,?,?,?);")
                ->bindParam(1, $post['Action'])
                ->bindParam(2, $post['UserID'])
                ->bindParam(3, $post['KodeAkses'])
                ->bindParam(4, $post['LokasiKode'])
//                ->bindParam( 5,    $post['NoTrans'] )
                ->bindParam(5, $post['idDokumenPengajuan'])
                ->bindParam(6, $post['idSPK'])
                ->bindParam(7, $post['jumlahDP'])
                ->bindParam(8, $post['tenor'])
                ->bindParam(9, $post['jumlahCicilan'])
                ->bindParam(10, $post['tanggalPengajuan'])
                ->bindParam(11, $post['idFinanceCompany'])
                ->bindParam(12, $post['namaFinanceCompany'])
                ->bindParam(13, $post['idPOFinanceCompany'])
                ->bindParam(14, $post['tanggalPembuatanPO'])
                ->bindParam(15, $post['tanggalPengirimanPOFi nanceCompany'])
                ->bindParam(16, $post['createdTime'])
                ->execute();
            $exe = General::cCmd("SELECT @NoTrans,@Status,@Keterangan;")->queryOne();
        } catch (Exception $e) {
            return [
                'NoTrans'    => $post['NoTrans'],
                'status'     => 1,
                'keterangan' => $e->getMessage(),
            ];
        }
        $NoTrans    = $exe['@NoTrans'] ?? $post['NoTrans'];
        $status     = $exe['@Status'] ?? 1;
        $keterangan = $exe['@Keterangan'] ?? 'Action : [' . $post['Action'] . '] Keterangan NULL';
        return [
            'NoTrans'    => $NoTrans,
            'status'     => $status,
            'keterangan' => str_replace(" ", "&nbsp;", $keterangan),
        ];
    }

    static public function callDOCH($post)
    {
        $post['UserID']     = \aunit\components\Menu::getUserLokasi()['UserID'];
        $post['KodeAkses']  = \aunit\components\Menu::getUserLokasi()['KodeAkses'];
        $post['LokasiKode'] = \aunit\components\Menu::getUserLokasi()['LokasiKode'];
        $post['NoTrans']    = $post['NoTrans'] ?? '';
        $post['idSO']       = $post['idSO'] ?? '';
        $post['idSPK']      = $post['idSPK'] ?? '';
        try {
            General::cCmd("SET @NoTrans = ?;")->bindParam(1, $post['NoTrans'])->execute();
            General::cCmd("CALL idochhd(?,@Status,@Keterangan,?,?,?,@NoTrans,?,?);")
                ->bindParam(1, $post['Action'])
                ->bindParam(2, $post['UserID'])
                ->bindParam(3, $post['KodeAkses'])
                ->bindParam(4, $post['LokasiKode'])
//                ->bindParam( 5,    $post['NoTrans'] )
                ->bindParam(5, $post['idSO'])
                ->bindParam(6, $post['idSPK'])
                ->execute();
            $exe = General::cCmd("SELECT @NoTrans,@Status,@Keterangan;")->queryOne();
        } catch (Exception $e) {
            return [
                'NoTrans'    => $post['NoTrans'],
                'status'     => 1,
                'keterangan' => $e->getMessage(),
            ];
        }
        $NoTrans    = $exe['@NoTrans'] ?? $post['NoTrans'];
        $status     = $exe['@Status'] ?? 1;
        $keterangan = $exe['@Keterangan'] ?? 'Action : [' . $post['Action'] . '] Keterangan NULL';
        return [
            'NoTrans'    => $NoTrans,
            'status'     => $status,
            'keterangan' => str_replace(" ", "&nbsp;", $keterangan),
        ];
    }

    static public function callDOCHit($post)
    {
        $post['UserID']                        = \aunit\components\Menu::getUserLokasi()['UserID'];
        $post['KodeAkses']                     = \aunit\components\Menu::getUserLokasi()['KodeAkses'];
        $post['LokasiKode']                    = \aunit\components\Menu::getUserLokasi()['LokasiKode'];
        $post['NoTrans']                       = $post['NoTrans'] ?? '';
        $post['nomorFakturSTNK']               = $post['nomorFakturSTNK'] ?? '';
        $post['tanggalPengajuanSTNKKeBiro']    = $post['tanggalPengajuanSTNKKeBiro'] ?? '';
        $post['statusFakturSTNK']              = $post['statusFakturSTNK'] ?? '';
        $post['nomorSTNK']                     = $post['nomorSTNK'] ?? '';
        $post['tanggalPenerimaanSTNKDariBiro'] = $post['tanggalPenerimaanSTNKDariBiro'] ?? '';
        $post['platNomor']                     = $post['platNomor'] ?? '';
        $post['nomorBPKB']                     = $post['nomorBPKB'] ?? '';
        $post['tanggalPenerimaanBPKBDariBiro'] = $post['tanggalPenerimaanBPKBDariBiro'] ?? '';
        $post['tanggalTerimaSTNKOlehKonsumen'] = $post['tanggalTerimaSTNKOlehKonsumen'] ?? '';
        $post['tanggalTerimaBPKBOlehKonsumen'] = $post['tanggalTerimaBPKBOlehKonsumen'] ?? '';
        $post['namaPenerima']                  = $post['namaPenerima'] ?? '';
        $post['jenisIdPenerima']               = $post['jenisIdPenerima'] ?? '';
        $post['noIdPenerima']                  = $post['noIdPenerima'] ?? '';
        $post['createdTime']                   = $post['createdTime'] ?? '';
        try {
            General::cCmd("SET @NoTrans = ?;")->bindParam(1, $post['NoTrans'])->execute();
            General::cCmd("CALL idochit(?,@Status,@Keterangan,?,?,?,@NoTrans,?,?,?,?,?,?,?,?,?,?,?,?,?,?);")
                ->bindParam(1, $post['Action'])
                ->bindParam(2, $post['UserID'])
                ->bindParam(3, $post['KodeAkses'])
                ->bindParam(4, $post['LokasiKode'])
//                ->bindParam( 5,    $post['NoTrans'] )
                ->bindParam(5, $post['nomorFakturSTNK'])
                ->bindParam(6, $post['tanggalPengajuanSTNKKeBiro'])
                ->bindParam(7, $post['statusFakturSTNK'])
                ->bindParam(8, $post['nomorSTNK'])
                ->bindParam(9, $post['tanggalPenerimaanSTNKDariBiro'])
                ->bindParam(10, $post['platNomor'])
                ->bindParam(11, $post['nomorBPKB'])
                ->bindParam(12, $post['tanggalPenerimaanBPKBDariBiro'])
                ->bindParam(13, $post['tanggalTerimaSTNKOlehKonsumen'])
                ->bindParam(14, $post['tanggalTerimaBPKBOlehKonsumen'])
                ->bindParam(15, $post['namaPenerima'])
                ->bindParam(16, $post['jenisIdPenerima'])
                ->bindParam(17, $post['noIdPenerima'])
                ->bindParam(18, $post['createdTime'])
                ->execute();
            $exe = General::cCmd("SELECT @NoTrans,@Status,@Keterangan;")->queryOne();
        } catch (Exception $e) {
            return [
                'NoTrans'    => $post['NoTrans'],
                'status'     => 1,
                'keterangan' => $e->getMessage(),
            ];
        }
        $NoTrans    = $exe['@NoTrans'] ?? $post['NoTrans'];
        $status     = $exe['@Status'] ?? 1;
        $keterangan = $exe['@Keterangan'] ?? 'Action : [' . $post['Action'] . '] Keterangan NULL';
        return [
            'NoTrans'    => $NoTrans,
            'status'     => $status,
            'keterangan' => str_replace(" ", "&nbsp;", $keterangan),
        ];
    }

    static public function callUINB($post)
    {
        $post['UserID']             = \aunit\components\Menu::getUserLokasi()['UserID'];
        $post['KodeAkses']          = \aunit\components\Menu::getUserLokasi()['KodeAkses'];
        $post['LokasiKode']         = \aunit\components\Menu::getUserLokasi()['LokasiKode'];
        $post['NoTrans']            = $post['NoTrans'] ?? '';
        $post['noShippingList']     = $post['noShippingList'] ?? '';
        $post['tanggalTerima']      = $post['tanggalTerima'] ?? '';
        $post['mainDealerId']       = $post['mainDealerId'] ?? '';
        $post['dealerId']           = $post['dealerId'] ?? '';
        $post['noInvoice']          = $post['noInvoice'] ?? '';
        $post['statusShippingList'] = $post['statusShippingList'] ?? '';
        try {
            General::cCmd("SET @NoTrans = ?;")->bindParam(1, $post['NoTrans'])->execute();
            General::cCmd("CALL iuinbhd(?,@Status,@Keterangan,?,?,?,@NoTrans,?,?,?,?,?,?);")
                ->bindParam(1, $post['Action'])
                ->bindParam(2, $post['UserID'])
                ->bindParam(3, $post['KodeAkses'])
                ->bindParam(4, $post['LokasiKode'])
//                ->bindParam( 5,    $post['NoTrans'] )
                ->bindParam(5, $post['noShippingList'])
                ->bindParam(6, $post['tanggalTerima'])
                ->bindParam(7, $post['mainDealerId'])
                ->bindParam(8, $post['dealerId'])
                ->bindParam(9, $post['noInvoice'])
                ->bindParam(10, $post['statusShippingList'])
                ->execute();
            $exe = General::cCmd("SELECT @NoTrans,@Status,@Keterangan;")->queryOne();
        } catch (Exception $e) {
            return [
                'NoTrans'    => $post['NoTrans'],
                'status'     => 1,
                'keterangan' => $e->getMessage(),
            ];
        }
        $NoTrans    = $exe['@NoTrans'] ?? $post['NoTrans'];
        $status     = $exe['@Status'] ?? 1;
        $keterangan = $exe['@Keterangan'] ?? 'Action : [' . $post['Action'] . '] Keterangan NULL';
        return [
            'NoTrans'    => $NoTrans,
            'status'     => $status,
            'keterangan' => str_replace(" ", "&nbsp;", $keterangan),
        ];
    }

    static public function callUINBit($post)
    {
        $post['UserID']            = \aunit\components\Menu::getUserLokasi()['UserID'];
        $post['KodeAkses']         = \aunit\components\Menu::getUserLokasi()['KodeAkses'];
        $post['LokasiKode']        = \aunit\components\Menu::getUserLokasi()['LokasiKode'];
        $post['NoTrans']           = $post['NoTrans'] ?? '';
        $post['kodeTipeUnit']      = $post['kodeTipeUnit'] ?? '';
        $post['kodeWarna']         = $post['kodeWarna'] ?? '';
        $post['kuantitasTerkirim'] = $post['kuantitasTerkirim'] ?? '';
        $post['kuantitasDiterima'] = $post['kuantitasDiterima'] ?? '';
        $post['noMesin']           = $post['noMesin'] ?? '';
        $post['noRangka']          = $post['noRangka'] ?? '';
        $post['statusRFS']         = $post['statusRFS'] ?? '';
        $post['poId']              = $post['poId'] ?? '';
        $post['kelengkapanUnit']   = $post['kelengkapanUnit'] ?? '';
        $post['noGoodsReceipt']    = $post['noGoodsReceipt'] ?? '';
        $post['docNRFSId']         = $post['docNRFSId'] ?? '';
        $post['createdTime']       = $post['createdTime'] ?? '';
        try {
            General::cCmd("SET @kodeTipeUnit = ?;")->bindParam(1, $post['kodeTipeUnit'])->execute();
            General::cCmd("CALL iuinbit(?,@Status,@Keterangan,?,?,?,@NoTrans,?,?,?,?,?,?,?,?,?,?,?,?);")
                ->bindParam(1, $post['Action'])
                ->bindParam(2, $post['UserID'])
                ->bindParam(3, $post['KodeAkses'])
                ->bindParam(4, $post['LokasiKode'])
//                ->bindParam( 5,    $post['NoTrans'] )
                ->bindParam(5, $post['kodeTipeUnit'])
                ->bindParam(6, $post['kodeWarna'])
                ->bindParam(7, $post['kuantitasTerkirim'])
                ->bindParam(8, $post['kuantitasDiterima'])
                ->bindParam(9, $post['noMesin'])
                ->bindParam(10, $post['noRangka'])
                ->bindParam(11, $post['statusRFS'])
                ->bindParam(12, $post['poId'])
                ->bindParam(13, $post['kelengkapanUnit'])
                ->bindParam(14, $post['noGoodsReceipt'])
                ->bindParam(15, $post['docNRFSId'])
                ->bindParam(16, $post['createdTime'])
                ->execute();
            $exe = General::cCmd("SELECT @NoTrans,@Status,@Keterangan;")->queryOne();
        } catch (Exception $e) {
            return [
                'NoTrans'    => $post['NoTrans'],
                'status'     => 1,
                'keterangan' => $e->getMessage(),
            ];
        }
        $NoTrans    = $exe['@NoTrans'] ?? $post['NoTrans'];
        $status     = $exe['@Status'] ?? 1;
        $keterangan = $exe['@Keterangan'] ?? 'Action : [' . $post['Action'] . '] Keterangan NULL';
        return [
            'NoTrans'    => $NoTrans,
            'status'     => $status,
            'keterangan' => str_replace(" ", "&nbsp;", $keterangan),
        ];
    }

    static public function callPRSP($post)
    {
        $post['UserID']                    = \aunit\components\Menu::getUserLokasi()['UserID'];
        $post['KodeAkses']                 = \aunit\components\Menu::getUserLokasi()['KodeAkses'];
        $post['LokasiKode']                = \aunit\components\Menu::getUserLokasi()['LokasiKode'];
        $post['NoTrans']                   = $post['NoTrans'] ?? '';
        $post['idProspect']                = $post['idProspect'] ?? '';
        $post['sumberProspect']            = $post['sumberProspect'] ?? '';
        $post['tanggalProspect']           = $post['tanggalProspect'] ?? '';
        $post['taggingProspect']           = $post['taggingProspect'] ?? '';
        $post['namaLengkap']               = $post['namaLengkap'] ?? '';
        $post['noKontak']                  = $post['noKontak'] ?? '';
        $post['noKtp']                     = $post['noKtp'] ?? '';
        $post['Alamat']                    = $post['Alamat'] ?? '';
        $post['kodePropinsi']              = $post['kodePropinsi'] ?? '';
        $post['kodeKota']                  = $post['kodeKota'] ?? '';
        $post['kodeKecamatan']             = $post['kodeKecamatan'] ?? '';
        $post['kodeKelurahan']             = $post['kodeKelurahan'] ?? '';
        $post['kodePos']                   = $post['kodePos'] ?? '';
        $post['alamatKantor']              = $post['alamatKantor'] ?? '';
        $post['kodePropinsiKantor']        = $post['kodePropinsiKantor'] ?? '';
        $post['kodeKotaKantor']            = $post['kodeKotaKantor'] ?? '';
        $post['kodeKecamatanKantor']       = $post['kodeKecamatanKantor'] ?? '';
        $post['KodeKelurahanKantor']       = $post['KodeKelurahanKantor'] ?? '';
        $post['kodePosKantor']             = $post['kodePosKantor'] ?? '';
        $post['Latitude']                  = $post['Latitude'] ?? '';
        $post['Longitude']                 = $post['Longitude'] ?? '';
        $post['kodePekerjaan']             = $post['kodePekerjaan'] ?? '';
        $post['noKontakKantor']            = $post['noKontakKantor'] ?? '';
        $post['tanggalAppointment']        = $post['tanggalAppointment'] ?? '';
        $post['waktuAppointment']          = $post['waktuAppointment'] ?? '';
        $post['metodeFollowUp']            = $post['metodeFollowUp'] ?? '';
        $post['testRidePreference']        = $post['testRidePreference'] ?? '';
        $post['statusFollowUpProspecting'] = $post['statusFollowUpProspecting'] ?? '';
        $post['statusProspect']            = $post['statusProspect'] ?? '';
        $post['idSalesPeople']             = $post['idSalesPeople'] ?? '';
        $post['idEvent']                   = $post['idEvent'] ?? '';

        try {
            General::cCmd("SET @NoTrans = ?;")->bindParam(1, $post['NoTrans'])->execute();
            General::cCmd("CALL iprsphd(?,@Status,@Keterangan,?,?,?,@NoTrans,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);")
                ->bindParam(1, $post['Action'])
                ->bindParam(2, $post['UserID'])
                ->bindParam(3, $post['KodeAkses'])
                ->bindParam(4, $post['LokasiKode'])
//                ->bindParam( 5,    $post['NoTrans'] )
                ->bindParam(5, $post['idProspect'])
                ->bindParam(6, $post['sumberProspect'])
                ->bindParam(7, $post['tanggalProspect'])
                ->bindParam(8, $post['taggingProspect'])
                ->bindParam(9, $post['namaLengkap'])
                ->bindParam(10, $post['noKontak'])
                ->bindParam(11, $post['noKtp'])
                ->bindParam(12, $post['Alamat'])
                ->bindParam(13, $post['kodePropinsi'])
                ->bindParam(14, $post['kodeKota'])
                ->bindParam(15, $post['kodeKecamatan'])
                ->bindParam(16, $post['kodeKelurahan'])
                ->bindParam(17, $post['kodePos'])
                ->bindParam(18, $post['alamatKantor'])
                ->bindParam(19, $post['kodePropinsiKantor'])
                ->bindParam(20, $post['kodeKotaKantor'])
                ->bindParam(21, $post['kodeKecamatanKantor'])
                ->bindParam(22, $post['KodeKelurahanKantor'])
                ->bindParam(23, $post['kodePosKantor'])
                ->bindParam(24, $post['Latitude'])
                ->bindParam(25, $post['Longitude'])
                ->bindParam(26, $post['kodePekerjaan'])
                ->bindParam(27, $post['noKontakKantor'])
                ->bindParam(28, $post['tanggalAppointment'])
                ->bindParam(29, $post['waktuAppointment'])
                ->bindParam(30, $post['metodeFollowUp'])
                ->bindParam(31, $post['testRidePreference'])
                ->bindParam(32, $post['statusFollowUpProspecting'])
                ->bindParam(33, $post['statusProspect'])
                ->bindParam(34, $post['idSalesPeople'])
                ->bindParam(35, $post['idEvent'])
                ->execute();
            $exe = General::cCmd("SELECT @NoTrans,@Status,@Keterangan;")->queryOne();
        } catch (Exception $e) {
            return [
                'NoTrans'    => $post['NoTrans'],
                'status'     => 1,
                'keterangan' => $e->getMessage(),
            ];
        }
        $NoTrans    = $exe['@NoTrans'] ?? $post['NoTrans'];
        $status     = $exe['@Status'] ?? 1;
        $keterangan = $exe['@Keterangan'] ?? 'Action : [' . $post['Action'] . '] Keterangan NULL';
        return [
            'NoTrans'    => $NoTrans,
            'status'     => $status,
            'keterangan' => str_replace(" ", "&nbsp;", $keterangan),
        ];
    }

    static public function callPRSPit($post)
    {
        $post['UserID']         = \aunit\components\Menu::getUserLokasi()['UserID'];
        $post['KodeAkses']      = \aunit\components\Menu::getUserLokasi()['KodeAkses'];
        $post['LokasiKode']     = \aunit\components\Menu::getUserLokasi()['LokasiKode'];
        $post['NoTrans']        = $post['NoTrans'] ?? '';
        $post['kodeTipeUnit']   = $post['kodeTipeUnit'] ?? '';
        $post['salesProgramId'] = $post['salesProgramId'] ?? '';
        $post['createdTime']    = $post['createdTime'] ?? '';
        try {
            General::cCmd("SET @NoTrans = ?;")->bindParam(1, $post['NoTrans'])->execute();
            General::cCmd("CALL iprspit(?,@Status,@Keterangan,?,?,?,@NoTrans,?,?,?);")
                ->bindParam(1, $post['Action'])
                ->bindParam(2, $post['UserID'])
                ->bindParam(3, $post['KodeAkses'])
                ->bindParam(4, $post['LokasiKode'])
//                ->bindParam( 5,    $post['NoTrans'] )
                ->bindParam(5, $post['kodeTipeUnit'])
                ->bindParam(6, $post['salesProgramId'])
                ->bindParam(7, $post['createdTime'])
                ->execute();
            $exe = General::cCmd("SELECT @NoTrans,@Status,@Keterangan;")->queryOne();
        } catch (Exception $e) {
            return [
                'NoTrans'    => $post['NoTrans'],
                'status'     => 1,
                'keterangan' => $e->getMessage(),
            ];
        }
        $NoTrans    = $exe['@NoTrans'] ?? $post['NoTrans'];
        $status     = $exe['@Status'] ?? 1;
        $keterangan = $exe['@Keterangan'] ?? 'Action : [' . $post['Action'] . '] Keterangan NULL';
        return [
            'NoTrans'    => $NoTrans,
            'status'     => $status,
            'keterangan' => str_replace(" ", "&nbsp;", $keterangan),
        ];
    }

    //ABENGKEL
    static public function callPINB($post)
    {
        $post['UserID']         = \abengkel\components\Menu::getUserLokasi()['UserID'];
        $post['KodeAkses']      = \abengkel\components\Menu::getUserLokasi()['KodeAkses'];
        $post['PosKode']        = \abengkel\components\Menu::getUserLokasi()['PosKode'];
        $post['NoTrans']        = $post['NoTrans'] ?? '';
        $post['noPenerimaan']   = $post['noPenerimaan'] ?? '';
        $post['tglPenerimaan']  = $post['tglPenerimaan'] ?? '';
        $post['noShippingList'] = $post['noShippingList'] ?? '';
        try {
            General::cCmd("SET @NoTrans = ?;")->bindParam(1, $post['NoTrans'])->execute();
            General::cCmd("CALL ipinbhd(?,@Status,@Keterangan,?,?,?,@NoTrans,?,?,?);")
                ->bindParam(1, $post['Action'])
                ->bindParam(2, $post['UserID'])
                ->bindParam(3, $post['KodeAkses'])
                ->bindParam(4, $post['PosKode'])
//                ->bindParam( 5,    $post['NoTrans'] )
                ->bindParam(5, $post['noPenerimaan'])
                ->bindParam(6, $post['tglPenerimaan'])
                ->bindParam(7, $post['noShippingList'])
                ->execute();
            $exe = General::cCmd("SELECT @NoTrans,@Status,@Keterangan;")->queryOne();
        } catch (Exception $e) {
            return [
                'NoTrans'    => $post['NoTrans'],
                'status'     => 1,
                'keterangan' => $e->getMessage(),
            ];
        }
        $NoTrans    = $exe['@NoTrans'] ?? $post['NoTrans'];
        $status     = $exe['@Status'] ?? 1;
        $keterangan = $exe['@Keterangan'] ?? 'Action : [' . $post['Action'] . '] Keterangan NULL';
        return [
            'NoTrans'    => $NoTrans,
            'status'     => $status,
            'keterangan' => str_replace(" ", "&nbsp;", $keterangan),
        ];
    }

    static public function callPINBit($post)
    {
        $post['UserID']      = \abengkel\components\Menu::getUserLokasi()['UserID'];
        $post['KodeAkses']   = \abengkel\components\Menu::getUserLokasi()['KodeAkses'];
        $post['PosKode']     = \abengkel\components\Menu::getUserLokasi()['PosKode'];
        $post['NoTrans']     = $post['NoTrans'] ?? '';
        $post['noPO']        = $post['noPO'] ?? '';
        $post['jenisOrder']  = $post['jenisOrder'] ?? '';
        $post['idWarehouse'] = $post['idWarehouse'] ?? '';
        $post['partsNumber'] = $post['partsNumber'] ?? '';
        $post['kuantitas']   = $post['kuantitas'] ?? '';
        $post['Uom']         = $post['uom'] ?? '';
        $post['createdTime'] = $post['createdTime'] ?? '';
        try {
            General::cCmd("SET @NoTrans = ?;")->bindParam(1, $post['NoTrans'])->execute();
            General::cCmd("CALL ipinbit(?,@Status,@Keterangan,?,?,?,@NoTrans,?,?,?,?,?,?,?);")
                ->bindParam(1, $post['Action'])
                ->bindParam(2, $post['UserID'])
                ->bindParam(3, $post['KodeAkses'])
                ->bindParam(4, $post['PosKode'])
//                ->bindParam( 5,    $post['NoTrans'] )
                ->bindParam(5, $post['noPO'])
                ->bindParam(6, $post['jenisOrder'])
                ->bindParam(7, $post['idWarehouse'])
                ->bindParam(8, $post['partsNumber'])
                ->bindParam(9, $post['kuantitas'])
                ->bindParam(10, $post['Uom'])
                ->bindParam(11, $post['createdTime'])
                ->execute();
            $exe = General::cCmd("SELECT @NoTrans,@Status,@Keterangan;")->queryOne();
        } catch (Exception $e) {
            return [
                'NoTrans'    => $post['NoTrans'],
                'status'     => 1,
                'keterangan' => $e->getMessage(),
            ];
        }
        $NoTrans    = $exe['@NoTrans'] ?? $post['NoTrans'];
        $status     = $exe['@Status'] ?? 1;
        $keterangan = $exe['@Keterangan'] ?? 'Action : [' . $post['Action'] . '] Keterangan NULL';
        return [
            'NoTrans'    => $NoTrans,
            'status'     => $status,
            'keterangan' => str_replace(" ", "&nbsp;", $keterangan),
        ];
    }

    static public function callPRSL($post)
    {
        $post['UserID']       = \abengkel\components\Menu::getUserLokasi()['UserID'];
        $post['KodeAkses']    = \abengkel\components\Menu::getUserLokasi()['KodeAkses'];
        $post['PosKode']      = \abengkel\components\Menu::getUserLokasi()['PosKode'];
        $post['NoTrans']      = $post['NoTrans'] ?? '';
        $post['noSO']         = $post['noSO'] ?? '';
        $post['tglSO']        = $post['tglSO'] ?? '';
        $post['idCustomer']   = $post['idCustomer'] ?? '';
        $post['namaCustomer'] = $post['namaCustomer'] ?? '';
        $post['discSO']       = $post['discSO'] ?? '';
        $post['totalHargaSO'] = $post['totalHargaSO'] ?? '';
        try {
            General::cCmd("SET @NoTrans = ?;")->bindParam(1, $post['NoTrans'])->execute();
            General::cCmd("CALL iprslhd(?,@Status,@Keterangan,?,?,?,@NoTrans,?,?,?,?,?,?);")
                ->bindParam(1, $post['Action'])
                ->bindParam(2, $post['UserID'])
                ->bindParam(3, $post['KodeAkses'])
                ->bindParam(4, $post['PosKode'])
//                ->bindParam( 5,    $post['NoTrans'] )
                ->bindParam(5, $post['noSO'])
                ->bindParam(6, $post['tglSO'])
                ->bindParam(7, $post['idCustomer'])
                ->bindParam(8, $post['namaCustomer'])
                ->bindParam(9, $post['discSO'])
                ->bindParam(10, $post['totalHargaSO'])
                ->execute();
            $exe = General::cCmd("SELECT @NoTrans,@Status,@Keterangan;")->queryOne();
        } catch (Exception $e) {
            return [
                'NoTrans'    => $post['NoTrans'],
                'status'     => 1,
                'keterangan' => $e->getMessage(),
            ];
        }
        $NoTrans    = $exe['@NoTrans'] ?? $post['NoTrans'];
        $status     = $exe['@Status'] ?? 1;
        $keterangan = $exe['@Keterangan'] ?? 'Action : [' . $post['Action'] . '] Keterangan NULL';
        return [
            'NoTrans'    => $NoTrans,
            'status'     => $status,
            'keterangan' => str_replace(" ", "&nbsp;", $keterangan),
        ];
    }

    static public function callPRSLit($post)
    {
        $post['UserID']             = \abengkel\components\Menu::getUserLokasi()['UserID'];
        $post['KodeAkses']          = \abengkel\components\Menu::getUserLokasi()['KodeAkses'];
        $post['PosKode']            = \abengkel\components\Menu::getUserLokasi()['PosKode'];
        $post['NoTrans']            = $post['NoTrans'] ?? '';
        $post['partsNumber']        = $post['partsNumber'] ?? '';
        $post['kuantitas']          = $post['kuantitas'] ?? '';
        $post['hargaParts']         = $post['hargaParts'] ?? '';
        $post['promoIdParts']       = $post['promoIdParts'] ?? '';
        $post['discAmount']         = $post['discAmount'] ?? '';
        $post['discPercentage']     = $post['discPercentage'] ?? '';
        $post['Ppn']                = $post['ppn'] ?? '';
        $post['totalHargaParts']    = $post['totalHargaParts'] ?? '';
        $post['uangMuka']           = $post['uangMuka'] ?? '';
        $post['bookingIdReference'] = $post['bookingIdReference'] ?? '';
        $post['createdTime']        = $post['createdTime'] ?? '';
        try {
            General::cCmd("SET @NoTrans = ?;")->bindParam(1, $post['NoTrans'])->execute();
            General::cCmd("CALL iprslit(?,@Status,@Keterangan,?,?,?,@NoTrans,?,?,?,?,?,?,?,?,?,?,?);")
                ->bindParam(1, $post['Action'])
                ->bindParam(2, $post['UserID'])
                ->bindParam(3, $post['KodeAkses'])
                ->bindParam(4, $post['PosKode'])
//                ->bindParam( 5,    $post['NoTrans'] )
                ->bindParam(5, $post['partsNumber'])
                ->bindParam(6, $post['kuantitas'])
                ->bindParam(7, $post['hargaParts'])
                ->bindParam(8, $post['promoIdParts'])
                ->bindParam(9, $post['discAmount'])
                ->bindParam(10, $post['discPercentage'])
                ->bindParam(11, $post['Ppn'])
                ->bindParam(12, $post['totalHargaParts'])
                ->bindParam(13, $post['uangMuka'])
                ->bindParam(14, $post['bookingIdReference'])
                ->bindParam(15, $post['createdTime'])
                ->execute();
            $exe = General::cCmd("SELECT @NoTrans,@Status,@Keterangan;")->queryOne();
        } catch (Exception $e) {
            return [
                'NoTrans'    => $post['NoTrans'],
                'status'     => 1,
                'keterangan' => $e->getMessage(),
            ];
        }
        $NoTrans    = $exe['@NoTrans'] ?? $post['NoTrans'];
        $status     = $exe['@Status'] ?? 1;
        $keterangan = $exe['@Keterangan'] ?? 'Action : [' . $post['Action'] . '] Keterangan NULL';
        return [
            'NoTrans'    => $NoTrans,
            'status'     => $status,
            'keterangan' => str_replace(" ", "&nbsp;", $keterangan),
        ];
    }

    static public function callPKB($post)
    {
        $post['UserID']                      = \abengkel\components\Menu::getUserLokasi()['UserID'];
        $post['KodeAkses']                   = \abengkel\components\Menu::getUserLokasi()['KodeAkses'];
        $post['PosKode']                     = \abengkel\components\Menu::getUserLokasi()['PosKode'];
        $post['NoTrans']                     = $post['NoTrans'] ?? '';
        $post['noWorkOrder']                 = $post['noWorkOrder'] ?? '';
        $post['noSAForm']                    = $post['noSAForm'] ?? '';
        $post['tanggalServis']               = $post['tanggalServis'] ?? '';
        $post['waktuPKB']                    = $post['waktuPKB'] ?? '';
        $post['noPolisi']                    = $post['noPolisi'] ?? '';
        $post['noRangka']                    = $post['noRangka'] ?? '';
        $post['noMesin']                     = $post['noMesin'] ?? '';
        $post['kodeTipeUnit']                = $post['kodeTipeUnit'] ?? '';
        $post['tahunMotor']                  = $post['tahunMotor'] ?? '';
        $post['informasiBensin']             = $post['informasiBensin'] ?? '';
        $post['kmTerakhir']                  = $post['kmTerakhir'] ?? '';
        $post['tipeComingCustomer']          = $post['tipeComingCustomer'] ?? '';
        $post['namaPemilik']                 = $post['namaPemilik'] ?? '';
        $post['alamatPemilik']               = $post['alamatPemilik'] ?? '';
        $post['kodePropinsiPemilik']         = $post['kodePropinsiPemilik'] ?? '';
        $post['kodeKotaPemilik']             = $post['kodeKotaPemilik'] ?? '';
        $post['kodeKecamatanPemilik']        = $post['kodeKecamatanPemilik'] ?? '';
        $post['kodeKelurahanPemilik']        = $post['kodeKelurahanPemilik'] ?? '';
        $post['kodePosPemilik']              = $post['kodePosPemilik'] ?? '';
        $post['alamatPembawa']               = $post['alamatPembawa'] ?? '';
        $post['kodePropinsiPembawa']         = $post['kodePropinsiPembawa'] ?? '';
        $post['kodeKotaPembawa']             = $post['kodeKotaPembawa'] ?? '';
        $post['kodeKecamatanPembawa']        = $post['kodeKecamatanPembawa'] ?? '';
        $post['kodeKelurahanPembawa']        = $post['kodeKelurahanPembawa'] ?? '';
        $post['kodePosPembawa']              = $post['kodePosPembawa'] ?? '';
        $post['namaPembawa']                 = $post['namaPembawa'] ?? '';
        $post['noTelpPembawa']               = $post['noTelpPembawa'] ?? '';
        $post['hubunganDenganPemilik']       = $post['hubunganDenganPemilik'] ?? '';
        $post['keluhanKonsumen']             = $post['keluhanKonsumen'] ?? '';
        $post['rekomendasiSA']               = $post['rekomendasiSA'] ?? '';
        $post['hondaIdSA']                   = $post['hondaIdSA'] ?? '';
        $post['hondaIdMekanik']              = $post['hondaIdMekanik'] ?? '';
        $post['saranMekanik']                = $post['saranMekanik'] ?? '';
        $post['asalUnitEntry']               = $post['asalUnitEntry'] ?? '';
        $post['idPIT']                       = $post['idPIT'] ?? '';
        $post['jenisPIT']                    = $post['jenisPIT'] ?? '';
        $post['waktuPendaftaran']            = $post['waktuPendaftaran'] ?? '';
        $post['waktuSelesai']                = $post['waktuSelesai'] ?? '';
        $post['totalFRT']                    = $post['totalFRT'] ?? '';
        $post['setUpPembayaran']             = $post['setUpPembayaran'] ?? '';
        $post['catatanTambahan']             = $post['catatanTambahan'] ?? '';
        $post['konfirmasiPekerjaanTambahan'] = $post['konfirmasiPekerjaanTambahan'] ?? '';
        $post['noBukuClaimC2']               = $post['noBukuClaimC2'] ?? '';
        $post['noWorkOrderJobReturn']        = $post['noWorkOrderJobReturn'] ?? '';
        $post['totalBiayaService']           = $post['totalBiayaService'] ?? '';
        $post['waktuPekerjaan']              = $post['waktuPekerjaan'] ?? '';
        $post['statusWorkOrder']             = $post['statusWorkOrder'] ?? '';
        $post['createdTime']                 = $post['createdTime'] ?? '';
        try {
            General::cCmd("SET @NoTrans = ?;")->bindParam(1, $post['NoTrans'])->execute();
            General::cCmd("CALL ipkbhd(?,@Status,@Keterangan,?,?,?,@NoTrans,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);")
                ->bindParam(1, $post['Action'])
                ->bindParam(2, $post['UserID'])
                ->bindParam(3, $post['KodeAkses'])
                ->bindParam(4, $post['PosKode'])
//                ->bindParam( 5,    $post['NoTrans'] )
                ->bindParam(5, $post['noWorkOrder'])
                ->bindParam(6, $post['noSAForm'])
                ->bindParam(7, $post['tanggalServis'])
                ->bindParam(8, $post['waktuPKB'])
                ->bindParam(9, $post['noPolisi'])
                ->bindParam(10, $post['noRangka'])
                ->bindParam(11, $post['noMesin'])
                ->bindParam(12, $post['kodeTipeUnit'])
                ->bindParam(13, $post['tahunMotor'])
                ->bindParam(14, $post['informasiBensin'])
                ->bindParam(15, $post['kmTerakhir'])
                ->bindParam(16, $post['tipeComingCustomer'])
                ->bindParam(17, $post['namaPemilik'])
                ->bindParam(18, $post['alamatPemilik'])
                ->bindParam(19, $post['kodePropinsiPemilik'])
                ->bindParam(20, $post['kodeKotaPemilik'])
                ->bindParam(21, $post['kodeKecamatanPemilik'])
                ->bindParam(22, $post['kodeKelurahanPemilik'])
                ->bindParam(23, $post['kodePosPemilik'])
                ->bindParam(24, $post['alamatPembawa'])
                ->bindParam(25, $post['kodePropinsiPembawa'])
                ->bindParam(26, $post['kodeKotaPembawa'])
                ->bindParam(27, $post['kodeKecamatanPembawa'])
                ->bindParam(28, $post['kodeKelurahanPembawa'])
                ->bindParam(29, $post['kodePosPembawa'])
                ->bindParam(30, $post['namaPembawa'])
                ->bindParam(31, $post['noTelpPembawa'])
                ->bindParam(32, $post['hubunganDenganPemilik'])
                ->bindParam(33, $post['keluhanKonsumen'])
                ->bindParam(34, $post['rekomendasiSA'])
                ->bindParam(35, $post['hondaIdSA'])
                ->bindParam(36, $post['hondaIdMekanik'])
                ->bindParam(37, $post['saranMekanik'])
                ->bindParam(38, $post['asalUnitEntry'])
                ->bindParam(39, $post['idPIT'])
                ->bindParam(40, $post['jenisPIT'])
                ->bindParam(41, $post['waktuPendaftaran'])
                ->bindParam(42, $post['waktuSelesai'])
                ->bindParam(43, $post['totalFRT'])
                ->bindParam(44, $post['setUpPembayaran'])
                ->bindParam(45, $post['catatanTambahan'])
                ->bindParam(46, $post['konfirmasiPekerjaanTambahan'])
                ->bindParam(47, $post['noBukuClaimC2'])
                ->bindParam(48, $post['noWorkOrderJobReturn'])
                ->bindParam(49, $post['totalBiayaService'])
                ->bindParam(50, $post['waktuPekerjaan'])
                ->bindParam(51, $post['statusWorkOrder'])
                ->bindParam(52, $post['createdTime'])
                ->execute();
            $exe = General::cCmd("SELECT @NoTrans,@Status,@Keterangan;")->queryOne();
        } catch (Exception $e) {
            return [
                'NoTrans'    => $post['NoTrans'],
                'status'     => 1,
                'keterangan' => $e->getMessage(),
            ];
        }
        $NoTrans    = $exe['@NoTrans'] ?? $post['NoTrans'];
        $status     = $exe['@Status'] ?? 1;
        $keterangan = $exe['@Keterangan'] ?? 'Action : [' . $post['Action'] . '] Keterangan NULL';
        return [
            'NoTrans'    => $NoTrans,
            'status'     => $status,
            'keterangan' => str_replace(" ", "&nbsp;", $keterangan),
        ];
    }

    static public function callPKBitjasa($post)
    {
        $post['UserID']         = \abengkel\components\Menu::getUserLokasi()['UserID'];
        $post['KodeAkses']      = \abengkel\components\Menu::getUserLokasi()['KodeAkses'];
        $post['PosKode']        = \abengkel\components\Menu::getUserLokasi()['PosKode'];
        $post['NoTrans']        = $post['NoTrans'] ?? '';
        $post['idJob']          = $post['idJob'] ?? '';
        $post['namaPekerjaan']  = $post['namaPekerjaan'] ?? '';
        $post['jenisPekerjaan'] = $post['jenisPekerjaan'] ?? '';
        $post['biayaService']   = $post['biayaService'] ?? '';
        $post['createdTime']    = $post['createdTime'] ?? '';
        try {
            General::cCmd("SET @NoTrans = ?;")->bindParam(1, $post['NoTrans'])->execute();
            General::cCmd("CALL ipkbitjasa(?,@Status,@Keterangan,?,?,?,@NoTrans,?,?,?,?,?);")
                ->bindParam(1, $post['Action'])
                ->bindParam(2, $post['UserID'])
                ->bindParam(3, $post['KodeAkses'])
                ->bindParam(4, $post['PosKode'])
//                ->bindParam( 5,    $post['NoTrans'] )
                ->bindParam(5, $post['idJob'])
                ->bindParam(6, $post['namaPekerjaan'])
                ->bindParam(7, $post['jenisPekerjaan'])
                ->bindParam(8, $post['biayaService'])
                ->bindParam(9, $post['createdTime'])
                ->execute();
            $exe = General::cCmd("SELECT @NoTrans,@Status,@Keterangan;")->queryOne();
        } catch (Exception $e) {
            return [
                'NoTrans'    => $post['NoTrans'],
                'status'     => 1,
                'keterangan' => $e->getMessage(),
            ];
        }
        $NoTrans    = $exe['@NoTrans'] ?? $post['NoTrans'];
        $status     = $exe['@Status'] ?? 1;
        $keterangan = $exe['@Keterangan'] ?? 'Action : [' . $post['Action'] . '] Keterangan NULL';
        return [
            'NoTrans'    => $NoTrans,
            'status'     => $status,
            'keterangan' => str_replace(" ", "&nbsp;", $keterangan),
        ];
    }

    static public function callPKBitbarang($post)
    {
        $post['UserID']      = \abengkel\components\Menu::getUserLokasi()['UserID'];
        $post['KodeAkses']   = \abengkel\components\Menu::getUserLokasi()['KodeAkses'];
        $post['PosKode']     = \abengkel\components\Menu::getUserLokasi()['PosKode'];
        $post['NoTrans']     = $post['NoTrans'] ?? '';
        $post['idJob']       = $post['idJob'] ?? '';
        $post['partsNumber'] = $post['partsNumber'] ?? '';
        $post['kuantitas']   = $post['kuantitas'] ?? '';
        $post['hargaParts']  = $post['hargaParts'] ?? '';
        $post['createdTime'] = $post['createdTime'] ?? '';
        try {
            General::cCmd("SET @NoTrans = ?;")->bindParam(1, $post['NoTrans'])->execute();
            General::cCmd("CALL ipkbitbarang(?,@Status,@Keterangan,?,?,?,@NoTrans,?,?,?,?,?);")
                ->bindParam(1, $post['Action'])
                ->bindParam(2, $post['UserID'])
                ->bindParam(3, $post['KodeAkses'])
                ->bindParam(4, $post['PosKode'])
//                ->bindParam( 5,    $post['NoTrans'] )
                ->bindParam(5, $post['idJob'])
                ->bindParam(6, $post['partsNumber'])
                ->bindParam(7, $post['kuantitas'])
                ->bindParam(8, $post['hargaParts'])
                ->bindParam(9, $post['createdTime'])
                ->execute();
            $exe = General::cCmd("SELECT @NoTrans,@Status,@Keterangan;")->queryOne();
        } catch (Exception $e) {
            return [
                'NoTrans'    => $post['NoTrans'],
                'status'     => 1,
                'keterangan' => $e->getMessage(),
            ];
        }
        $NoTrans    = $exe['@NoTrans'] ?? $post['NoTrans'];
        $status     = $exe['@Status'] ?? 1;
        $keterangan = $exe['@Keterangan'] ?? 'Action : [' . $post['Action'] . '] Keterangan NULL';
        return [
            'NoTrans'    => $NoTrans,
            'status'     => $status,
            'keterangan' => str_replace(" ", "&nbsp;", $keterangan),
        ];
    }

}
