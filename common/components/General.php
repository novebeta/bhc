<?php
namespace common\components;

use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use yii\helpers\Url;

class General
{
    const FontColorMaroon = 'maroon';
    const FontColorLime   = 'green';
    const FontColorPink   = 'magenta';
    const FontColorYellow = '#FF9900';
    const BackColorLime   = 'lime';
    const BackColorPink   = 'pink';
    const BackColorYellow = 'yellow';
    /**
     * Create Temporary Reference
     *
     * @param $code
     * @param $tableName
     * @param $fieldName
     *
     * @return string
     */
    public static function createTemporaryNo($code, $tableName, $fieldName)
    {
        return self::createNo($code, $tableName, $fieldName, true);
    }
    /**
     * Create Reference
     *
     * @param $code
     * @param $tableName
     * @param $fieldName
     * @param bool $temporary
     *
     * @return string
     */
    public static function createNo($code, $tableName, $fieldName, $temporary = false)
    {
        $yNow = date("y");
        $sign = $temporary ? '=' : '-';
//        $sign = '-';
        $maxNo = (new \yii\db\Query())
            ->from($tableName)
//            ->where( "$fieldName LIKE :Notrans" ,[':Notrans' => $code.$yNow.$sign.'%'])
            ->where("$fieldName REGEXP :Notrans", [':Notrans' => '^' . $code . $yNow . $sign . '[0-9]{5}$'])
            ->max("$fieldName");
        if ($maxNo && preg_match("/$code\d{2}" . $sign . "(\d+)/", $maxNo, $result)) {
            [$all, $counter] = $result;
            $digitCount      = strlen($counter);
            $val             = (int) $counter;
            $nextCounter     = sprintf('%0' . $digitCount . 'd', ++$val);
        } else {
            $nextCounter = "00001";
        }
        return $code . $yNow . $sign . $nextCounter; // ? str_replace( '-', '=', $nextRef ) : $nextRef;
    }
    public static function generateTahun($start, $end)
    {
        $jml    = $end - $start;
        $result = [];
        for ($i = 0; $i <= $jml; $i++) {
            $tahun          = $start + $i;
            $result[$tahun] = $tahun;
        }
        return $result;
    }
    /**
     * @param mix format harus yyyy-mm-dd atau sesuai dengan sql format
     * @param string $format
     *
     * @return string
     */
    public static function asDate($date, $format = 'Y-m-d')
    {
        try {
            if ($date == null || $date == '' || $date == '--') {
                return $date;
            }
            $timestamp = strtotime($date);
            return date($format, $timestamp);
        } catch (InvalidArgumentException $e) {
            return $date;
        }
    }
    /**
     * @param mix format harus yyyy-mm-dd atau sesuai dengan sql format
     * @param string $format
     *
     * @return string
     */
    public static function asDatetime($date, $format = 'Y-m-d H:i:s')
    {
        try {
            $timestamp = strtotime($date);
            return date($format, $timestamp);
        } catch (InvalidArgumentException $e) {
            return $date;
        } catch (InvalidConfigException $e) {
            return $date;
        }
    }
    /**
     * Creates a command for execution.
     *
     * @param string $sql the SQL statement to be executed
     * @param array $params the parameters to be bound to the SQL statement
     *
     * @return \yii\db\Command the DB command
     */
    public static function cCmd($sql = null, $params = [])
    {
        $query = \Yii::$app->db->createCommand($sql, $params);
//        $rawSql = \Yii::$app->db->createCommand( $sql, $params )->rawSql;
        return $query;
    }
    public static function labelGL($NoGL, $label = 'JT')
    {
        $gl        = null;
        $KodeAkses = Yii::$app->session->get('_userProfile')['KodeAkses'];
        if (Custom::isAbengkel()) {
            $gl = \abengkel\models\Ttgeneralledgerhd::findOne(['NoGL' => $NoGL]);
        }
        if (Custom::isAunit()) {
            $gl = \aunit\models\Ttgeneralledgerhd::findOne(['NoGL' => $NoGL]);
        }
        if (Custom::isFinance()) {
            $gl = \afinance\models\Ttgeneralledgerhd::findOne(['NoGL' => $NoGL]);
        }
        if ($gl == null) {
            return '<label class="control-label" style="margin: 0; padding: 6px 0;">' . $label . '</label>';
        }
        $modelName      = get_class($gl);
        $primaryKeys    = $modelName::getTableSchema()->primaryKey;
        $id             = base64_encode(implode('||', $gl->getAttributes($primaryKeys)));
        $urlParam[]     = 'ttgeneralledgerhd/transaksi-update';
        $urlParam['id'] = $id;
//        $urlParam[ 'action' ] = ( $KodeAkses === 'Admin' || $KodeAkses === 'Auditor' ) ? 'update' : 'view';
        $urlParam['action'] = 'display';
        $url                = Url::toRoute($urlParam);
        return '<a href="' . $url . '" target="_blank"><label class="control-label" style="margin: 0; padding: 6px 0;">' . $label . '</label></a>';
    }

    public static function send($outlet, $company_nama, $pesan, $data, $image = null)
    {
        $data = json_decode($data,true);
        $msg = strtr($pesan, $data);
        if(!empty($company_nama)){
            $msg .= "\r\n\r\n" . $company_nama;
        }
        $url_api = 'api/v2/send-message';
        $curl  = curl_init();
        // $outlet64 = $_COOKIE['_outlet'];
        // $outlet         = base64_decode($outlet64);
        $paramOutlet = array_merge(Yii::$app->params['H1'],Yii::$app->params['H2']);
        $wa_url           = $paramOutlet[$outlet ]['wa_url'];
        $wa_token         = $paramOutlet[$outlet ]['wa_token'];
        $token = $wa_token;        
        $params  = [
            'phone'   => $data['CusTelepon'],
            'message' => $msg,
        ];
        if($image != null){
            $path_parts = pathinfo($image["name"]);
            $extension = $path_parts['extension'];
            $target_image = "/uploads/".basename($image["name"]);
            $target_image = str_replace(strtolower('.'.$extension),strtolower('_'.$outlet.'.'.$extension),strtolower(str_replace(' ','_',$target_image)));
            $target_file = Yii::getAlias('@webroot').$target_image;
            $filename = $image['tmp_name'];
            move_uploaded_file($filename,$target_file);
            $hostName = $_SERVER['HTTP_HOST']; 
            $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';
            $image_url = $protocol.'://'.$hostName.Yii::getAlias('@web').$target_image;
            // Yii::info($image_url );
            // $handle = fopen($filename, "r");
            // $file = fread($handle, filesize($filename));
            $params = [
                'phone'     => $data['CusTelepon'],
                'secret'    => false, // or true
                'retry'     => false, // or true
                'isGroup'   => false, // or true
                // 'file'      => base64_encode($file),
                // 'data'      => json_encode($image)
            ];
            $url_api = '';
            switch ($extension) {
                case 'jpg':
                case 'gif':
                case 'jpeg':
                case 'png':
                    $url_api = 'api/v2/send-image';
                    $params['image'] = $image_url;
                    break;
                case 'doc':
                case 'docx':
                case 'pdf':
                case 'odt':
                case 'csv':
                case 'ppt':
                case 'pptx':
                case 'xls':
                case 'xlsx':
                case 'txt':
                    $url_api = 'api/v2/send-document';
                    $params['document'] = $image_url;
                    break;
                case 'mp4':
                case 'mpeg':
                    $url_api = 'api/v2/send-video';
                    $params['video'] = $image_url;
                    break;
              }
              $params['caption'] = $msg;
        }
        curl_setopt($curl, CURLOPT_HTTPHEADER,[
                "Authorization: $token",
                "Content-Type: application/json"
        ]);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(['data'=>[$params]]) );
        curl_setopt($curl, CURLOPT_URL, $wa_url . $url_api);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($curl);
        curl_close($curl);
        Yii::info($params, 'SEND MSG RESULT');
        Yii::info($result, 'SEND MSG RESULT');
        return Json::decode($result, true);
    }

	public static function reportRealtime($from,$to)
    {
        $curl = curl_init();
        $outlet64 = $_COOKIE['_outlet'];
        $outlet         = base64_decode($outlet64);
        $paramOutlet = array_merge(Yii::$app->params['H1'],Yii::$app->params['H2']);
        $wa_token           = $paramOutlet[$outlet ]['wa_token'];
        $token = $wa_token;
        $url_api = 'api/report-realtime?';
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array(
                "Authorization: $token",
            )
        );
        $url_api .= "from=".$from."&to=".$to;
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, Yii::$app->params['wa_url'] . $url_api);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($curl);
        $resultArr = Json::Decode($result);
        curl_close($curl);
        $filtered = [];
        $header = ['No Whatsapp', 'Pesan', 'Status'];
        $colModel = [
            ['name' => 'to','label'=>'No Whatsapp'],
            ['name' => 'message','label'=>'Pesan'],
            ['name' => 'status','label' => 'Status']
        ];
        foreach ($resultArr['data'] as $item) {
            if (substr($item['message'], -1 * strlen($_SESSION['_company']['nama'])) == $_SESSION['_company']['nama']) {
                $dateFrom = date('Y-m-d', strtotime($from));
                $dateTo = date('Y-m-d', strtotime($to));
                $msgDate = date('Y-m-d', strtotime($item['date']['created']));
                if($msgDate >= $dateFrom && $msgDate <= $dateTo){
                    $filtered[] = [
                        'to'=> $item['phone']['to'],
                        'message'=> str_replace("\r\n\r\n" . $_SESSION['_company']['nama'],'',$item['message']),
                        'status'=> $item['status'],
                    ];
                }
            }
        }
        return [
            'header' => $header,
            'colModel' => $colModel,
            'data' => $filtered
        ];
    }
}
