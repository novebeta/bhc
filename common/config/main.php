<?php
use kartik\datecontrol\Module;
$common = [
    'aliases'    => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'timeZone'   => $GLOBALS['timezone'],
    'container'  => [
        'definitions' => [
            'yii\widgets\ActiveForm'   => ['options' => ['autocomplete' => 'off']],
            'yii\bootstrap\ActiveForm' => ['options' => ['autocomplete' => 'off']],
        ],
    ],
    'modules'    => [
        //        'login' => [
        //            'class' => 'modules\login\Module',
        //        ],
        'gridview'    => [
            'class' => '\kartik\grid\Module',
            // other module settings
        ],
        'datecontrol' => [
            'class'              => 'kartik\datecontrol\Module',
            // format settings for displaying each date attribute
            'displaySettings'    => [
                Module::FORMAT_DATE     => 'dd/MM/yyyy',
                Module::FORMAT_TIME     => 'HH:mm:ss',
                Module::FORMAT_DATETIME => 'dd/MM/yyyy HH:mm:ss',
            ],
            // format settings for saving each date attribute
            'saveSettings'       => [
                'date'     => 'php:Y-m-d',
                'time'     => 'php:H:i:s',
                'datetime' => 'php:Y-m-d H:i:s',
            ],
            'displayTimezone'    => $GLOBALS['timezone'],
            // automatically use kartik\widgets for each of the above formats
            'autoWidget'         => true,
            'autoWidgetSettings' => [
                Module::FORMAT_DATE     => ['type' => 1, 'pluginOptions' => ['todayHighlight' => true, 'todayBtn' => true, 'autoclose' => true]], // example
                Module::FORMAT_DATETIME => [], // setup if needed
                Module::FORMAT_TIME     => [], // setup if needed
            ],
            'ajaxConversion'     => false,
        ],
    ],
    'components' => [
        'formatter'    => [
            'class'             => 'yii\i18n\Formatter',
            'dateFormat'        => 'yyyy-MM-dd',
            'decimalSeparator'  => ',',
            'thousandSeparator' => '.',
            'timeZone'          => $GLOBALS['timezone'],
            'language'          => 'id-ID',
            'locale'            => 'id-ID',
            //            'currencyCode'      => 'IDR',
        ],
        // 'session'      => [
        //     'class'      => 'common\components\DbSession',
        //     'db'         => ['dsn' => 'sqlite:@common/runtime/data.sqlite'],
        //     'timeout'    => 3600 * 30 * 24, //session expire
        //     'useCookies' => true,
        // ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class' => 'sergeymakinen\yii\slacklog\Target',
                    'levels' => ['error'],
                    'except' => [
                        'yii\web\HttpException:*',
                    ],
                    'enabled' => false,
                    'webhookUrl' => 'https://hooks.slack.com/services/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',
                    'username' => 'Fire Alarm Bot',
                    'iconEmoji' => ':poop:',
                ],
            ],
        ],
        'urlManager'   => [
            'enablePrettyUrl' => false,
            'showScriptName'  => false,
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'bundles'         => [
                'kartik\form\ActiveFormAsset'        => [
                    'bsDependencyEnabled' => true, // do not load bootstrap assets for a specific asset bundle
                ],
                'yii\web\JqueryAsset'                => [
                    'sourcePath' => null, // do not publish the bundle
                    //                        'basePath'   => '@webroot/jquery',
                    //                        'baseUrl'    => '@web/jquery'
                    'js'         => [
                        'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js',
                    ],
                ],
                'yii\widgets\MaskedInputAsset'       => [
                    'sourcePath' => null, // do not publish the bundle
                    'js'         => [
                        'https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6-beta.29/jquery.inputmask.min.js',
                    ],
                ],
                'yii\bootstrap\BootstrapAsset'       => [
                    'sourcePath' => null, // do not publish the bundle
                    'basePath'   => '@webroot/bootstrap',
                    'baseUrl'    => '@web/bootstrap',
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'sourcePath' => null, // do not publish the bundle
                    'basePath'   => '@webroot/bootstrap',
                    'baseUrl'    => '@web/bootstrap',
                ],
                'dmstr\web\AdminLteAsset'            => [
                    'sourcePath' => null, // do not publish the bundle
                    'basePath'   => '@webroot/adminlte',
                    'baseUrl'    => '@web/adminlte',
                    'depends'    => [
                        'rmrevin\yii\fontawesome\AssetBundle',
                        'yii\web\YiiAsset',
                        'yii\bootstrap\BootstrapAsset',
                        'yii\bootstrap\BootstrapPluginAsset',
                    ],
                ],
            ],
        ],
    ],
];
global $argv;
if (empty($argv)) {
    $common = array_merge_recursive($common, [
        'components' => [
            'user'    => [
                'identityClass'   => 'common\models\User',
                'enableAutoLogin' => true,
                'identityCookie'  => ['name' => '_identity', 'httpOnly' => true, 'domain' => '.bhc', 'path' => '/'],
                'loginUrl'        => getBaseUrl() . '/login/web/index.php?r=site/index',
                //            'as loginOnce'    => [  // <- bagian ini yg penting
                //                                    'class'         => 'common\components\LoginOnce',
                //                                    'kickLogedUser' => true,
                //                                    'cookieKey' => 'PHPSESSID'
                //            ]
            ],
            'request' => [
                'csrfParam' => '_csrf-app',
            ],
        ],
    ]);
}
return $common;
