<?php
function getBaseUrl() {
	// output: /myproject/index.php
	$currentPath = $_SERVER[ 'PHP_SELF' ];
	// output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index )
	$pathInfo = pathinfo( $currentPath );
	// output: localhost
	$hostName = $_SERVER[ 'HTTP_HOST' ];
	// output: http://
	$protocol = strtolower( substr( $_SERVER[ "SERVER_PROTOCOL" ], 0, 5 ) ) == 'https' ? 'https' : 'http';
	// return: http://localhost/myproject/
	return $protocol . '://' . $hostName . $pathInfo[ 'dirname' ] . "/";
}
//Yii::setAlias('@app', dirname(dirname(__DIR__)));
Yii::setAlias( '@common', dirname( __DIR__ ) );
Yii::setAlias( '@aunit', dirname( dirname( __DIR__ ) ) . '/aunit' );
Yii::setAlias( '@abengkel', dirname( dirname( __DIR__ ) ) . '/abengkel' );
Yii::setAlias( '@afinance', dirname( dirname( __DIR__ ) ) . '/afinance' );
Yii::setAlias( '@login', dirname( dirname( __DIR__ ) ) . '/login' );
//Yii::setAlias( '@api', dirname( dirname( __DIR__ ) ) . '/api' );
Yii::setAlias( '@console', dirname( dirname( __DIR__ ) ) . '/console' );
function in_array_insensitive($needle, $haystack) {
	$needle = strtolower($needle);
	foreach($haystack as $k => $v) {
		$haystack[$k] = strtolower($v);
	}
	return in_array($needle, $haystack);
}
function array_find($needle, array $haystack)
{
    foreach ($haystack as $key => $value) {
        if (false !== stripos($value, $needle)) {
            return $key;
        }
    }
    return false;
}
function array_find_key($needle, array $haystack, $exact = true)
{
    foreach ($haystack as $key => $value) {
		if($exact){
			if (strtolower($key) == strtolower($needle)) {
				return $key;
			}
		}else{
			if (false !== stripos($value, $key)) {
				return $key;
			}
		}
    }
    return false;
}