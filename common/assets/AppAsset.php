<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace common\assets;
use yii\web\AssetBundle;
/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle {
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'css/site.css',
//		'https://use.fontawesome.com/releases/v5.3.1/css/all.css',
		'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css',
	];
	public $js = [
		'jquery-slimscroll/jquery.slimscroll.min.js',
	];
	public $depends = [
		'dmstr\web\AdminLteAsset',
		'aunit\assets\BootboxAsset',
	];
}
