<?php
namespace common\models;
use Yii;
use yii\base\Model;
use yii\db\Exception;
/**
 * Login form
 */
class LoginForm extends Model {
	public $username;
	public $password;
//	public $cabang;
	public $rememberMe = true;
	private $_user;
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			// username and password are both required
			[ [ 'username', 'password'], 'required' ],
			// rememberMe must be a boolean value
			[ 'rememberMe', 'boolean' ],
			// password is validated by validatePassword()
			[ 'password', 'validatePassword' ],
		];
	}
	/**
	 * Validates the password.
	 * This method serves as the inline validation for password.
	 *
	 * @param string $attribute the attribute currently being validated
	 * @param array $params the additional name-value pairs given in the rule
	 */
	public function validatePassword( $attribute, $params ) {
		if ( ! $this->hasErrors() ) {
			$user = $this->getUser();
			if ( ! $user || ! $user->validatePassword( $this->password ) ) {
				$this->addError( $attribute, 'Incorrect username or password.' );
			}
		}
	}
	/**
	 * Logs in a user using the provided username and password.
	 *
	 * @return bool whether the user is logged in successfully
	 */
	public function login() {
		if ( $this->validate() ) {
			return Yii::$app->user->login( $this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0 );
		}
		return false;
	}
	/**
	 * Finds user by [[username]]
	 *
	 * @return User|null
	 */
	protected function getUser() {
		if ( $this->_user === null ) {
			$this->_user = User::findIdentity( $this->username );
		}
		return $this->_user;
	}
	public function validateDatabase() {
		$typeLoginArr = []; // 1 = Bengkel, 2 Unit
		$where[]   = 'AND';
		$where[]   = 'length(schema_name) = 3 OR (length(schema_name) = 4 AND schema_name like "B%") OR schema_name = "hfinance"';
		$where[]   = [ 'not in', 'schema_name', Yii::$app->params['db_exclude'] ];
		$cabangAll = ( new \yii\db\Query() )
			->select( 'SCHEMA_NAME' )
			->from( 'information_schema.schemata' )
			->where( $where )
			->all();
		$sqlCmd    = "";
		foreach ( $cabangAll as $ca_cbg ) {
			$cabang = $ca_cbg['SCHEMA_NAME'];
			$sqlTest = "SELECT UPPER('$cabang') AS db
				FROM $cabang.tuser 
				WHERE $cabang.tuser.UserStatus = 'A' AND $cabang.tuser.UserID = :user AND $cabang.tuser.UserPass = :pass";
			try {
				$test = Yii::$app->db->createCommand( $sqlTest )
				             ->bindParam( ':user', $this->username, \PDO::PARAM_STR )
				             ->bindParam( ':pass', $this->password, \PDO::PARAM_STR )
				             ->queryScalar();
				if($test != false){
					if ( $sqlCmd != '' ) {
						$sqlCmd .= " UNION ALL ";
					}
					$sqlCmd .= $sqlTest;
				}
			} catch ( Exception $e ) {
			}
		}
		try {
			if ( $sqlCmd == '' ) {
				$this->addError( 'username', 'UserID tidak memiliki akses.' );
				return false;
			}
			$s_cbg = Yii::$app->db->createCommand( $sqlCmd )
			                      ->bindParam( ':user', $this->username, \PDO::PARAM_STR )
			                      ->bindParam( ':pass', $this->password, \PDO::PARAM_STR )
			                      ->queryAll();
			if ( $s_cbg == null ) {
				$this->addError( 'password', 'Incorrect username or password.' );
				return false;
			} else {
				foreach ( $s_cbg as $cabang ) {
					$dbname = $cabang['db'];
					if(strlen($dbname) == 3 && strtolower($dbname[0]) != 'b' && !in_array(1,$typeLoginArr)){
						$typeLoginArr[] = 1;
					}elseif(strtolower($dbname[0]) == 'b' && !in_array(2,$typeLoginArr)){
						$typeLoginArr[] = 2;
					}
				}
				sort($typeLoginArr);
				setcookie('_userType', base64_encode(implode(',',$typeLoginArr)), 0,'/');
				\Yii::$app->db->close(); // make sure it clean
				\Yii::$app->db->dsn = Yii::$app->db->dsn . 'dbname=' . $s_cbg[0]['db'];
				setcookie( '_outlet', base64_encode($s_cbg[0]['db']), 0,'/');
				return true;
			}
		} catch ( Exception $e ) {
			return false;
		}
	}
}
