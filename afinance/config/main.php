<?php
$params = array_merge(
	require __DIR__ . '/../../common/config/params.php',
	require __DIR__ . '/../../common/config/params-local.php',
	require __DIR__ . '/params.php',
	require __DIR__ . '/params-local.php'
);
return [
	'id'                  => 'app-afinance',
	'name'                => 'HSYS – Unit',
	'basePath'            => dirname( __DIR__ ),
	'bootstrap'           => [ 'log' ],
	'controllerNamespace' => 'afinance\controllers',
	'components'          => [

	],
//	'as access' => [
//		'class' => \yii\filters\AccessControl::className(),
//		'rules' => [
//			[
//				'actions' => ['login', 'error','debug/*'],
//				'allow' => true,
//			],
//			[
//				'actions' => ['logout', 'index'],
//				'allow' => true,
//				'roles' => ['@'],
//			],
//		],
//	],
	'params'              => $params,
];
