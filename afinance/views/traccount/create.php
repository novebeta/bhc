<?php
/* @var $this yii\web\View */
/* @var $model afinance\models\Traccount */
$this->title                   = 'Create Data Perkiraan';
$this->params['breadcrumbs'][] = [ 'label' => 'Data Perkiraan', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="traccount-create">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
