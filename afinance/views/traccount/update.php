<?php
/* @var $this yii\web\View */
/* @var $model afinance\models\Traccount */
$this->title                   = 'Update Data Perkiraan: ' . $model->NoAccount;
$this->params['breadcrumbs'][] = [ 'label' => 'Data Perkiraan', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->NoAccount, 'url' => [ 'view', 'id' => $model->NoAccount ] ];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="traccount-update">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
