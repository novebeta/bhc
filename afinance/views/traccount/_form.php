<?php
use afinance\models\Traccount;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model afinance\models\Traccount */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-md-12 col-xl-12 col-lg-12">
    <div class="box box-primary ">
		<?php $form = ActiveForm::begin( [
			'class' => 'form-horizontal'
		] ); ?>
        <div class="box-body ">
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'NoAccount' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'NoAccount' )->textInput( [
						'maxlength' => true,
					] )->label( false) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'NamaAccount' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'NamaAccount' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'JenisAccount' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'JenisAccount' )->dropDownList( [
						'Header' => 'Header',
						'Detail' => 'Detail',
					], [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'StatusAccount' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'StatusAccount' )->dropDownList( [
						'A' => 'AKTIF',
						'N' => 'NON AKTIF',
					], [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'NoParent' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'NoParent' )->widget( Select2::classname(), [
						'value'         => $model->NoParent,
						'options'       => [ 'placeholder' => 'Filter as you type ...' ],
						'pluginOptions' => [ 'highlight' => true ],
						'data'          => Traccount::find()->header()
					] )->label( false ) ?>
                </div>
            </div>
            <div class="box-footer">
                <div class="pull-right">-
					<?= Html::submitButton( 'Simpan', [ 'class' => 'btn btn-success' ] ) ?>
                    <a href="<?= Url::to( [ 'traccount/index' ] ) ?>" class="btn btn-warning" role="button">Batal</a>
                </div>
            </div>
			<?php ActiveForm::end(); ?>
        </div>
    </div>
