<?php
use afinance\assets\AppAsset;
/* @var $this yii\web\View */
/* @var $searchModel afinance\models\TraccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
AppAsset::register( $this );
$this->title                   = 'Edit Perkiraan';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'traccount.NoAccount'   => 'Nomor Perkiraan',
		'traccount.NamaAccount' => 'Nama Perkiraan',
		'traccount.JenisAccount'  => 'Jenis',
		'traccount.StatusAccount' => 'Status',
		'traccount.NoParent'      => 'Induk',
	],
	'cmbTgl'    => [
	],
	'cmbNum'    => [
//		'OpeningBalance' => 'Opening Balance',
//		'LevelAccount' => 'Level Account',
	],
	'sortname'  => "traccount.StatusAccount asc,traccount.NoAccount asc,traccount.NamaAccount",
	'sortorder' => 'asc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \afinance\components\TdUi::mainGridJs( \afinance\models\Traccount::className(), 'Edit Perkiraan' ), \yii\web\View::POS_READY );