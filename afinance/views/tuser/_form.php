<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model afinance\models\Tuser */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-md-12 col-xl-12 col-lg-12">
    <div class="box box-primary ">
		<?php $form = ActiveForm::begin( [
			'class' => 'form-horizontal'
		] ); ?>
        <div class="box-body ">
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'UserID' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'UserID' )->textInput( [
						'maxlength' => true,
						'disabled'  => true
					] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'KodeAkses' ) ?>:</label>
                <div class="col-sm-12">
					<?= $form->field( $model, 'KodeAkses' )->textInput( [
						'maxlength' => true,
						'disabled'  => true
					] )->label( false ) ?>
                </div>
                <label class="control-label col-sm-2"><?= $model->getAttributeLabel( 'UserStatus' ) ?>:</label>
                <div class="col-sm-4">
		            <?= $form->field( $model, 'UserStatus' )->textInput( [
			            'maxlength' => true,
			            'disabled'  => true
		            ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">

            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'LokasiKode' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'LokasiKode' )->textInput( [
						'maxlength' => true,
						'disabled'  => true
					] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'UserNama' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'UserNama' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'UserAlamat' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'UserAlamat' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'UserTelepon' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'UserTelepon' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'UserKeterangan' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'UserKeterangan' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6">Password Lama:</label>
                <div class="col-sm-18">
                    <div class="form-group">
						<?= Html::passwordInput( 'old_pass', null, [ 'class' => 'form-control' ] ); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6">Password Baru:</label>
                <div class="col-sm-18">
                    <div class="form-group">
						<?= Html::passwordInput( 'new_pass', null, [ 'class' => 'form-control' ] ); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6">Konfirmasi Password:</label>
                <div class="col-sm-18">
                    <div class="form-group">
						<?= Html::passwordInput( 'confirm_pass', null, [ 'class' => 'form-control' ] ); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="pull-right">-
			    <?= Html::submitButton( 'Simpan', [ 'class' => 'btn btn-success' ] ) ?>
                <a href="<?= Url::to( [ 'site/index' ] ) ?>" class="btn btn-warning" role="button">Batal</a>
            </div>
        </div>
	    <?php ActiveForm::end(); ?>
    </div>
</div>
