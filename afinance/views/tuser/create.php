<?php
/* @var $this yii\web\View */
/* @var $model afinance\models\Tuser */
$this->title                   = 'User Profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tuser-create">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
