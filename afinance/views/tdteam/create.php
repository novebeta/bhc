<?php
/* @var $this yii\web\View */
/* @var $model afinance\models\Tdteam */
$this->title                   = 'Create Team';
$this->params['breadcrumbs'][] = [ 'label' => 'Team', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdteam-create">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
