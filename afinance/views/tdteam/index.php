<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel afinance\models\TdteamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
use afinance\assets\AppAsset;
AppAsset::register($this);
$this->title = 'Team';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'TeamKode'       => 'Kode',
		'TeamNama'       => 'Nama',
		'TeamAlamat'     => 'Alamat',
		'TeamTelepon'    => 'Telepon',
		'TeamKeterangan' => 'Keterangan',
		'TeamStatus'     => 'Status',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
	'sortname'  => "TeamStatus asc,TeamKode asc,TeamNama",
	'sortorder' => 'asc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \afinance\components\TdUi::mainGridJs( \afinance\models\Tdteam::className() ,'Team'), \yii\web\View::POS_READY );