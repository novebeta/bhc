<?php
use kartik\number\NumberControl;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model afinance\models\Tdteam */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-md-12 col-xl-12 col-lg-12">
    <div class="box box-primary ">
		<?php $form = ActiveForm::begin( [
			'class' => 'form-horizontal'
		] ); ?>
        <div class="box-body ">
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'TeamKode' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'TeamKode' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'TeamNo' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'TeamNo' )->widget( NumberControl::classname(), [
						'maskedInputOptions' => [
							'allowMinus' => false
						],
					] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'TeamNama' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'TeamNama' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'TeamStatus' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'TeamStatus' )->dropDownList( [
						'A' => 'AKTIF',
						'N' => 'NON AKTIF',
					], [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'TeamAlamat' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'TeamAlamat' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'TeamTelepon' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'TeamTelepon' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'TeamKeterangan' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'TeamKeterangan' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="pull-right">-
				<?= Html::submitButton( 'Simpan', [ 'class' => 'btn btn-success' ] ) ?>
                <a href="<?= Url::to( [ 'tdteam/index' ] ) ?>" class="btn btn-warning" role="button">Batal</a>
            </div>
        </div>
		<?php ActiveForm::end(); ?>
    </div>
</div>
