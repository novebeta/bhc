<?php
/* @var $this yii\web\View */
/* @var $model afinance\models\Tdteam */
$this->title                   = 'Update Team: ' . $model->TeamKode;
$this->params['breadcrumbs'][] = [ 'label' => 'Team', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->TeamKode, 'url' => [ 'view', 'id' => $model->TeamKode ] ];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tdteam-update">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
