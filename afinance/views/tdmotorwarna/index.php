<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel afinance\models\TdmotorwarnaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
use afinance\assets\AppAsset;
AppAsset::register($this);
$this->title = 'Type Warna Motor';
$this->params['breadcrumbs'][] = $this->title;
?>    <div class="box box-primary">
    <table id="jqGrid"></table>
    <div id="jqGridPager"></div>
</div>
<?php

$this->registerJs( \afinance\components\TdUi::mainGridJs( \afinance\models\Tdmotorwarna::className() ) );
