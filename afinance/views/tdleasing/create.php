<?php
/* @var $this yii\web\View */
/* @var $model afinance\models\Tdleasing */
$this->title                   = 'Create Leasing';
$this->params['breadcrumbs'][] = [ 'label' => 'Leasing', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdleasing-create">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
