<?php
use afinance\models\Tdmotortype;
use kartik\datecontrol\DateControl;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model afinance\models\Tdleasing */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-md-12 col-xl-12 col-lg-12">
    <div class="box box-primary ">
		<?php $form = ActiveForm::begin( [
			'class' => 'form-horizontal'
		] ); ?>
        <div class="box-body ">
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'LeaseKode' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'LeaseKode' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'LeaseNama' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'LeaseNama' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'LeaseContact' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'LeaseContact' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'LeaseAlamat' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'LeaseAlamat' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'LeaseKota' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'LeaseKota' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'LeaseTelepon' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'LeaseTelepon' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'LeaseFax' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'LeaseFax' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'LeaseEmail' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'LeaseEmail' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'LeaseKeterangan' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'LeaseKeterangan' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'LeaseStatus' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'LeaseStatus' )->dropDownList([
					        'A' => 'AKTIF',
					        'N' => 'NON AKTIF',
                    ], [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="pull-right">-
				<?= Html::submitButton( 'Simpan', [ 'class' => 'btn btn-success' ] ) ?>
                <a href="<?= Url::to( [ 'tdleasing/index' ] ) ?>" class="btn btn-warning" role="button">Batal</a>
            </div>
        </div>
		<?php ActiveForm::end(); ?>
    </div>
</div>
