<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel afinance\models\TdleasingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
use afinance\assets\AppAsset;
AppAsset::register($this);
$this->title = 'Leasing';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'LeaseKode' => 'Kode',
		'LeaseNama' => 'Nama',
		'LeaseContact' => 'Kontak',
		'LeaseAlamat' => 'Alamat',
		'LeaseKota' => 'Kota',
		'LeaseTelepon' => 'Telepon',
		'LeaseFax' => 'Fax',
		'LeaseEmail' => 'Email',
		'LeaseKeterangan' => 'Keterangan',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
	'sortname'  => "LeaseStatus asc,LeaseKode asc,LeaseNama",
	'sortorder' => 'asc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \afinance\components\TdUi::mainGridJs( \afinance\models\Tdleasing::className() ,'Leasing'), \yii\web\View::POS_READY );