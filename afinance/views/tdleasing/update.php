<?php
/* @var $this yii\web\View */
/* @var $model afinance\models\Tdleasing */
$this->title                   = 'Update Leasing: ' . $model->LeaseKode;
$this->params['breadcrumbs'][] = [ 'label' => 'Leasing', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->LeaseKode, 'url' => [ 'view', 'id' => $model->LeaseKode ] ];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tdleasing-update">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
