<?php
use afinance\models\Tdsale;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model afinance\models\Tdlokasi */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-md-12 col-xl-12 col-lg-12">
    <div class="box box-primary ">
		<?php $form = ActiveForm::begin( [
			'class' => 'form-horizontal'
		] ); ?>
        <div class="box-body ">
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'LokasiKode' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'LokasiKode' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'LokasiNomor' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'LokasiNomor' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'LokasiJenis' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'LokasiJenis' )->dropDownList( [
						'Lokasi' => 'Lokasi',
						'Kas'    => 'Kas',
					], [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'NoAccount' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'NoAccount' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'LokasiStatus' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'LokasiStatus' )->dropDownList( [
						'A' => 'AKTIF',
						'N' => 'NON AKTIF',
						'1' => 'DEALER',
					], [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'LokasiNama' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'LokasiNama' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'LokasiPetugas' ) ?>:</label>
                <div class="col-sm-18">
	                <?= $form->field( $model, 'LokasiPetugas' )->widget( Select2::classname(), [
		                'value'         => $model->LokasiPetugas,
		                'options'       => [ 'placeholder' => 'Filter as you type ...' ],
		                'pluginOptions' => [ 'highlight' => true ],
		                'data'          => Tdsale::find()->petugas()
	                ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'LokasiAlamat' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'LokasiAlamat' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'LokasiTelepon' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'LokasiTelepon' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="pull-right">-
				<?= Html::submitButton( 'Simpan', [ 'class' => 'btn btn-success' ] ) ?>
                <a href="<?= Url::to( [ 'tdbbn/index' ] ) ?>" class="btn btn-warning" role="button">Batal</a>
            </div>
        </div>
		<?php ActiveForm::end(); ?>
    </div>
</div>
