<?php
/* @var $this yii\web\View */
/* @var $model afinance\models\Tdlokasi */
$this->title                   = 'Create Warehouse / Pos';
$this->params['breadcrumbs'][] = [ 'label' => 'Warehouse / Pos', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdlokasi-create">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
