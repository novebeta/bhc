<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel afinance\models\TdlokasiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
use afinance\assets\AppAsset;
AppAsset::register($this);
$this->title = 'Warehouse / Pos';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'LokasiKode' => 'Kode',
		'LokasiNama' => 'Nama',
		'LokasiPetugas' => 'Petugas',
		'LokasiAlamat' => 'Alamat',
		'LokasiStatus' => 'Status',
		'LokasiTelepon' => 'Telepon',
		'LokasiNomor' => 'Nomor',
		'NoAccount' => 'No Account',
		'LokasiJenis' => 'Jenis',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
	'sortname'  => "LokasiStatus asc,LokasiKode asc,LokasiNama",
	'sortorder' => 'asc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \afinance\components\TdUi::mainGridJs( \afinance\models\Tdlokasi::className(),'Warehouse / Pos' ), \yii\web\View::POS_READY );