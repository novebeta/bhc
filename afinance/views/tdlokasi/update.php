<?php
/* @var $this yii\web\View */
/* @var $model afinance\models\Tdlokasi */
$this->title                   = 'Update Warehouse / Pos: ' . $model->LokasiKode;
$this->params['breadcrumbs'][] = [ 'label' => 'Warehouse / Pos', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->LokasiKode, 'url' => [ 'view', 'id' => $model->LokasiKode ] ];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tdlokasi-update">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
