<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model afinance\models\Tdbbn */

$this->title = 'Create Tarif BBN';
$this->params['breadcrumbs'][] = ['label' => 'Tarif BBN', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdbbn-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
