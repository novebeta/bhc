<?php
use afinance\models\Tdmotortype;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model afinance\models\Tdbbn */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-md-12 col-xl-12 col-lg-12">
    <div class="box box-primary ">
		<?php $form = ActiveForm::begin( [
//			'class' => 'form-horizontal'
		] ); ?>
        <div class="box-body ">
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'Kabupaten' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'Kabupaten' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'MotorType' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'MotorType' )->widget( Select2::classname(), [
						'value'         => $model->MotorType,
						'options'       => [ 'placeholder' => 'Filter as you type ...' ],
						'pluginOptions' => [ 'highlight' => true ],
						'data'          => Tdmotortype::find()->select2()
					] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'BBNBiaya' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'BBNBiaya' )->widget( NumberControl::classname(), [
						'maskedInputOptions' => [
							'allowMinus' => false
						],
					] )->label( false ) ?>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="pull-right">-
				<?= Html::submitButton( 'Simpan', [ 'class' => 'btn btn-success' ] ) ?>
                <a href="<?= Url::to( [ 'tdbbn/index' ] ) ?>" class="btn btn-warning" role="button">Batal</a>
            </div>
        </div>
		<?php ActiveForm::end(); ?>
    </div>
</div>