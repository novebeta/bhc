<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model afinance\models\Tdbbn */

$this->title = 'Update Tarif BBN: ' . $model->Kabupaten;
$this->params['breadcrumbs'][] = ['label' => 'Tarif BBN', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Kabupaten, 'url' => ['view', 'Kabupaten' => $model->Kabupaten, 'MotorType' => $model->MotorType]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tdbbn-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
