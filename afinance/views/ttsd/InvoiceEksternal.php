<?php
use afinance\assets\AppAsset;
use afinance\models\Ttsd;
/* @var $this yii\web\View */
/* @var $searchModel afinance\models\TtsdSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Invoice Eksternal';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
//		'SSNo'       => 'No SS',
//		'DONo' => 'No DO',
	],
	'cmbTgl'    => [
//		'SSTgl' => 'Tgl SS'
	],
	'cmbNum'    => [
	],
	'sortname'  => "SDTgl",
	'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \afinance\components\TdUi::mainGridJs( \afinance\models\Ttsd::className(),
	'Invoice Eksternal', [
		'colGrid' => Ttsd::colGridInvoiceEksternal()
	] ), \yii\web\View::POS_READY );