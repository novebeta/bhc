<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model afinance\models\Ttsd */

$this->title = 'Update Ttsd: ' . $model->SDNo;
$this->params['breadcrumbs'][] = ['label' => 'Ttsds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->SDNo, 'url' => ['view', 'id' => $model->SDNo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ttsd-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
