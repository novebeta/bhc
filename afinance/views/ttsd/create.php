<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model afinance\models\Ttsd */

$this->title = 'Create Ttsd';
$this->params['breadcrumbs'][] = ['label' => 'Ttsds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttsd-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
