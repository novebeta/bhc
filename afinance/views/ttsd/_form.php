<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model afinance\models\Ttsd */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ttsd-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'SDNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SDTgl')->textInput() ?>

    <?= $form->field($model, 'LokasiKode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DealerKode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SDMemo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SDTotal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'UserID')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SDJam')->textInput() ?>

    <?= $form->field($model, 'NoGL')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
