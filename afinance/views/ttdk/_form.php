<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model afinance\models\Ttdk */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ttdk-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'DKNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKTgl')->textInput() ?>

    <?= $form->field($model, 'SalesKode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusKode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'LeaseKode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKJenis')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKMemo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKLunas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'INNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'UserID')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKHPP')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKHarga')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKDPTotal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKDPLLeasing')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKDPTerima')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKDPInden')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKNetto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ProgramNama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ProgramSubsidi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'BBN')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Jaket')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PrgSubsSupplier')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PrgSubsDealer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PrgSubsFincoy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PotonganHarga')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ReturHarga')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PotonganKhusus')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NamaHadiah')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PotonganAHM')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKTenor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKAngsuran')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKMerkSblmnya')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKJenisSblmnya')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKMotorUntuk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKMotorOleh')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKMediator')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKTOP')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKPOLeasing')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SPKNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'InsentifSales')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TeamKode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKTglTagih')->textInput() ?>

    <?= $form->field($model, 'DKPONo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKPOTgl')->textInput() ?>

    <?= $form->field($model, 'BBNPlus')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ROCount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKScheme')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKSCP')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKPengajuanBBN')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKScheme2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'JAPrgSubsSupplier')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'JAPrgSubsDealer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'JAPrgSubsFincoy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'JADKHarga')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'JADKDPTerima')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'JADKDPLLeasing')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'JABBN')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'JAReturHarga')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'JAPotonganHarga')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'JAInsentifSales')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'JAPotonganKhusus')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKSurveyor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NoBukuServis')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
