<?php
use afinance\assets\AppAsset;
use afinance\models\Ttdk;
/* @var $this yii\web\View */
/* @var $searchModel afinance\models\TtdkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Pengajuan Berkas';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
//		'SSNo'       => 'No SS',
//		'DONo' => 'No DO',
	],
	'cmbTgl'    => [
//		'SSTgl' => 'Tgl SS'
	],
	'cmbNum'    => [
	],
	'sortname'  => "DKTgl",
	'sortorder' => 'desc',
	'where'     => [
		"tmotor.SKNo <> '--'"
	]
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \afinance\components\TdUi::mainGridJs( \afinance\models\Ttdk::className(),
	'Pengajuan Berkas', [
		'colGrid' => Ttdk::colGridPengajuanBerkas()
	] ), \yii\web\View::POS_READY );
