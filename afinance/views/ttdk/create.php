<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model afinance\models\Ttdk */

$this->title = 'Create Ttdk';
$this->params['breadcrumbs'][] = ['label' => 'Ttdks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttdk-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
