<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model afinance\models\Ttdk */

$this->title = 'Update Ttdk: ' . $model->DKNo;
$this->params['breadcrumbs'][] = ['label' => 'Ttdks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->DKNo, 'url' => ['view', 'id' => $model->DKNo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ttdk-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
