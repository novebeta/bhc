<?php
use afinance\models\Ttdk;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel afinance\models\TtdkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Konsumen Kredit';
$this->params['breadcrumbs'][] = $this->title;
$arr = [
	'cmbTxt'    => [
//		'SSNo'       => 'No SS',
//		'DONo' => 'No DO',
	],
	'cmbTgl'    => [
//		'SSTgl' => 'Tgl SS'
	],
	'cmbNum'    => [
	],
	'sortname'  => "DKTgl",
	'sortorder' => 'desc',
	'where'     => [
		"DKJenis = 'Kredit'"
	]
];
use afinance\assets\AppAsset;
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \afinance\components\TdUi::mainGridJs( \afinance\models\Ttdk::className(),
	'Data Konsumen Kredit',[
		'colGrid' => Ttdk::colGridDataKonsumenKredit()
	]  ), \yii\web\View::POS_READY );
