<?php
/* @var $this yii\web\View */
/* @var $model afinance\models\Tdsupplier */
$this->title                   = 'Update Supplier: ' . $model->SupKode;
$this->params['breadcrumbs'][] = [ 'label' => 'Supplier', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->SupKode, 'url' => [ 'view', 'id' => $model->SupKode ] ];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tdsupplier-update">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
