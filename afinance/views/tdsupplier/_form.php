<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model afinance\models\Tdsupplier */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-md-12 col-xl-12 col-lg-12">
    <div class="box box-primary ">
		<?php $form = ActiveForm::begin( [
			'class' => 'form-horizontal'
		] ); ?>
        <div class="box-body ">
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'SupKode' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'SupKode' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'SupNama' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'SupNama' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'SupContact' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'SupContact' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'SupAlamat' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'SupAlamat' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'SupKota' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'SupKota' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'SupTelepon' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'SupTelepon' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'SupFax' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'SupFax' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'SupEmail' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'SupEmail' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'SupKeterangan' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'SupKeterangan' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'SupStatus' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'SupStatus' )->dropDownList( [
						'A' => 'AKTIF',
						'N' => 'NON AKTIF',
						'1' => 'Supplier Utama',
					], [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="pull-right">-
				<?= Html::submitButton( 'Simpan', [ 'class' => 'btn btn-success' ] ) ?>
                <a href="<?= Url::to( [ 'tdsupplier/index' ] ) ?>" class="btn btn-warning" role="button">Batal</a>
            </div>
        </div>
		<?php ActiveForm::end(); ?>
    </div>
</div>
