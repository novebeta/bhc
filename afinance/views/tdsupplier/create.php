<?php
/* @var $this yii\web\View */
/* @var $model afinance\models\Tdsupplier */
$this->title                   = 'Create Supplier';
$this->params['breadcrumbs'][] = [ 'label' => 'Supplier', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdsupplier-create">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
