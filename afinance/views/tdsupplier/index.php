<?php
use afinance\assets\AppAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel afinance\models\TdsupplierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
AppAsset::register( $this );
$this->title                   = 'Supplier';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'SupKode'       => 'Kode',
		'SupNama'       => 'Nama',
		'SupContact'    => 'Kontak',
		'SupAlamat'     => 'Alamat',
		'SupKota'       => 'Kota',
		'SupTelepon'    => 'Telepon',
		'SupFax'        => 'Fax',
		'SupEmail'      => 'Email',
		'SupKeterangan' => 'Keterangan',
		'SupStatus'     => 'Status',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
	'sortname'  => "SupStatus asc,SupKode asc,SupNama",
	'sortorder' => 'asc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \afinance\components\TdUi::mainGridJs( \afinance\models\Tdsupplier::className() ,'Supplier'), \yii\web\View::POS_READY );
