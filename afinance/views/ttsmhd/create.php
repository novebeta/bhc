<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model afinance\models\Ttsmhd */

$this->title = 'Create Ttsmhd';
$this->params['breadcrumbs'][] = ['label' => 'Ttsmhds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttsmhd-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
