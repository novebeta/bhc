<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel afinance\models\TtsmhdSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ttsmhds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttsmhd-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ttsmhd', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'SMNo',
            'SMTgl',
            'LokasiAsal',
            'LokasiTujuan',
            'SMMemo',
            //'SMTotal',
            //'UserID',
            //'SMJam',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
