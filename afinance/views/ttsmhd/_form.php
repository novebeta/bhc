<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model afinance\models\Ttsmhd */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ttsmhd-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'SMNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SMTgl')->textInput() ?>

    <?= $form->field($model, 'LokasiAsal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'LokasiTujuan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SMMemo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SMTotal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'UserID')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SMJam')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
