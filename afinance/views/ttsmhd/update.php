<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model afinance\models\Ttsmhd */

$this->title = 'Update Ttsmhd: ' . $model->SMNo;
$this->params['breadcrumbs'][] = ['label' => 'Ttsmhds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->SMNo, 'url' => ['view', 'id' => $model->SMNo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ttsmhd-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
