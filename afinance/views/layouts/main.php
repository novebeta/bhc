<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this \yii\web\View */
/* @var $content string */
if ( Yii::$app->controller->action->id === 'login' ) {
	/**
	 * Do not use this code in your template. Remove it.
	 * Instead, use the code  $this->layout = '//main-login'; in your controller.
	 */
	echo $this->render(
		'main-login',
		[ 'content' => $content ]
	);
} else {
	common\assets\AppAsset::register( $this );
//	dmstr\web\AdminLteAsset::register( $this );
//	$directoryAsset = Yii::$app->assetManager->getPublishedUrl( '@vendor/almasaeed2010/adminlte/dist' );
	$directoryAsset = Url::base( true );
	?>
	<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="refresh" content="<?php echo Yii::$app->params['sessionTimeoutSeconds']; ?>;"/>
		<?= Html::csrfMetaTags() ?>
        <title><?= Html::encode( $this->title ) ?></title>
		<?php $this->head() ?>
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <script>
            var AdminLTEOptions = {
                sidebarExpandOnHover: true,
                enableBoxRefresh: true,
                enableBSToppltip: true
            };
        </script>
        <style>
            a .sidebar-toggle {
                height: 50px;
            }

            .container-fluid {
                padding-right: 0;
                padding-left: 0;
                padding-bottom: 2px;
            }

            .divider {
                height: 1px;
                width: 97%;
                display: block; /* for use on default inline elements like span */
                margin: 9px 0;
                overflow: hidden;
                /*background-color: #8aa4af;*/
                /*box-shadow: 0 0 10px 1px #8aa4af;*/
                background-image: -webkit-linear-gradient(left, #707070, #e3e3e3, #707070);
                background-image: -moz-linear-gradient(left, #707070, #e3e3e3, #707070);
                background-image: -ms-linear-gradient(left, #707070, #e3e3e3, #707070);
                background-image: -o-linear-gradient(left, #707070, #e3e3e3, #707070);
            }

            /*.divider::after{*/
            /*    display: block;*/
            /*    height: 10px;*/
            /*    content: '';*/
            /*}*/

            .panel {
                margin-bottom: 0px;
            }

            .panel-body {
                padding: 10px;
            }

            .content {
                min-height: 150px;
                padding: 5px 10px 5px 10px;
            }

            .content-header {
                padding-top: 5px;
            }

            .content-header > .breadcrumb {
                top: 5px;
            }

            .table-striped > tbody > tr:nth-of-type(odd) {
                background-color: #dff2fa;
            }

            .ui-jqgrid .ui-jqgrid-labels th.ui-th-column {
                background-color: #ffd26b;
                background-image: none
            }

            .ui-jqgrid .ui-jqgrid-btable tbody tr.jqgrow td.jqgrid-rownum {
                background-color: transparent;
                background-image: none;
            }

            /*.ui-jqgrid .ui-jqgrid-bdiv {*/
            /*    overflow-x:hidden !important;*/
            /*    overflow-y:auto !important;*/
            /*}*/

            .ui-jqgrid .loading, .loading_pivot {
                border: none;
                background-color: transparent;
                top: 35%;
            }

            .ui-pg-div {
                color: #00a65a;
            }

            .material-switch > input[type="checkbox"] {
                display: none;
            }

            .material-switch > label {
                cursor: pointer;
                height: 0px;
                position: relative;
                width: 40px;
            }

            label {
                margin-top: 5px;
                margin-bottom: 0;
            }

            .material-switch > label::before {
                background: rgb(0, 0, 0);
                box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
                border-radius: 8px;
                content: '';
                height: 16px;
                margin-top: -8px;
                position: absolute;
                opacity: 0.3;
                transition: all 0.4s ease-in-out;
                width: 40px;
            }

            .material-switch > label::after {
                background: rgb(255, 255, 255);
                border-radius: 16px;
                box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
                content: '';
                height: 24px;
                left: -4px;
                margin-top: -8px;
                position: absolute;
                top: -4px;
                transition: all 0.3s ease-in-out;
                width: 24px;
            }

            .material-switch > input[type="checkbox"]:checked + label::before {
                background: inherit;
                opacity: 0.5;
            }

            .material-switch > input[type="checkbox"]:checked + label::after {
                background: inherit;
                left: 20px;
            }

            /* General Links */
            a {
                color: #00a65a;
            }
            a:hover,
            a:active,
            a:focus {
                outline: none;
                text-decoration: none;
                color: #00f383;
            }

        </style>
    </head>
    <body class="hold-transition skin-green fixed sidebar-mini">
	<?php $this->beginBody() ?>
    <div class="wrapper">
		<?= $this->render(
			'header.php',
			[ 'directoryAsset' => $directoryAsset ]
		) ?>

		<?= $this->render(
			'left.php',
			[ 'directoryAsset' => $directoryAsset ]
		)
		?>

		<?= $this->render(
			'content.php',
			[ 'content' => $content, 'directoryAsset' => $directoryAsset ]
		) ?>
    </div>
	<?php $this->endBody() ?>
    </body>
    </html>
	<?php $this->endPage() ?>
<?php } ?>
<script>
    window.FontAwesomeConfig = {searchPseudoElements: true};
    $(function () {
        var url = $(location).attr('href');
        $('ul.sidebar-menu a').filter(function () {
            return decodeURIComponent(this.href) == decodeURIComponent(url);
        }).parent().addClass('active');
        $('ul.treeview-menu a').filter(function () {
            return decodeURIComponent(this.href) == decodeURIComponent(url);
        }).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');
        $('#sidebar-form').on('submit', function (e) {
            e.preventDefault();
        });
        $('.sidebar-menu li.active').data('lte.pushmenu.active', true);
        $('#search-input').on('keyup', function () {
            var term = $('#search-input').val().trim();
            if (term.length === 0) {
                $('.sidebar-menu li').each(function () {
                    $(this).show(0);
                    $(this).removeClass('active');
                    if ($(this).data('lte.pushmenu.active')) {
                        $(this).addClass('active');
                    }
                });
                return;
            }
            $('.sidebar-menu li').each(function () {
                if ($(this).text().toLowerCase().indexOf(term.toLowerCase()) === -1) {
                    $(this).hide(0);
                    $(this).removeClass('pushmenu-search-found', false);
                    if ($(this).is('.treeview')) {
                        $(this).removeClass('active');
                    }
                } else {
                    $(this).show(0);
                    $(this).addClass('pushmenu-search-found');
                    if ($(this).is('.treeview')) {
                        $(this).addClass('active');
                    }
                    var parent = $(this).parents('li').first();
                    if (parent.is('.treeview')) {
                        parent.show(0);
                    }
                }
                if ($(this).is('.header')) {
                    $(this).show();
                }
            });
            $('.sidebar-menu li.pushmenu-search-found.treeview').each(function () {
                $(this).find('.pushmenu-search-found').show(0);
            });
        });
        (function longPolling(){
            $.ajax({
                type: 'POST',
                url: '<?=Url::to(['site/waktu'])?>',
                dataType: "json",
                timeout: 1000,
                success: function(d, statusText, jqXHR){
                    $("#timer").html(d.tgl);
                    $("#duration").html(d.duration);
                },
            }).done(function(data, statusText, jqXHR){
                setTimeout(longPolling, 1000);
            });
        })();
    });
</script>