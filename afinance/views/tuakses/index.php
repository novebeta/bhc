<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel afinance\models\TuaksesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tuakses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tuakses-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Tuakses', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'KodeAkses',
            'MenuInduk',
            'MenuText',
            'MenuNo',
            'MenuStatus:boolean',
            //'MenuTambah:boolean',
            //'MenuEdit:boolean',
            //'MenuHapus:boolean',
            //'MenuCetak:boolean',
            //'MenuView:boolean',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
