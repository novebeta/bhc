<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model afinance\models\Ttfb */

$this->title = 'Update Ttfb: ' . $model->FBNo;
$this->params['breadcrumbs'][] = ['label' => 'Ttfbs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->FBNo, 'url' => ['view', 'id' => $model->FBNo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ttfb-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
