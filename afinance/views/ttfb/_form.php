<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model afinance\models\Ttfb */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ttfb-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'FBNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FBTgl')->textInput() ?>

    <?= $form->field($model, 'SSNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SupKode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FBTermin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FBTglTempo')->textInput() ?>

    <?= $form->field($model, 'FBPSS')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FBPtgLain')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FBTotal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FBMemo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FBLunas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'UserID')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NoGL')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FPNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FPTgl')->textInput() ?>

    <?= $form->field($model, 'DOTgl')->textInput() ?>

    <?= $form->field($model, 'FBExtraDisc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FBPosting')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FBPtgMD')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FBJam')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
