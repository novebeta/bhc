<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model afinance\models\Ttfb */

$this->title = 'Create Ttfb';
$this->params['breadcrumbs'][] = ['label' => 'Ttfbs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttfb-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
