<?php
use kartik\datecontrol\DateControl;
/* @var $this yii\web\View */
/* @var $model afinance\models\TdbbnSearch */
/* @var $form yii\widgets\ActiveForm */
?>
    <div id="findModal" class="modal modal-danger fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="form-inline" id="top-menu">
                    <div class="input-group">
                        <input id="txt-find-id" type="search" class="form-control" style="width: 235px"
                               aria-describedby="basic-addon1">
                        <span class="input-group-btn">
                            <button id="find-up" class="btn btn-default" type="button">
                                <span class="fa fa-angle-up" aria-hidden="true"></span>
                            </button>
                            <button id="find-down" class="btn btn-default" type="button">
                                <span class="fa fa-angle-down" aria-hidden="true"></span>
                            </button>
                            <button class="btn btn-default" type="button" data-dismiss="modal">
                                <span class="fa fa-close" aria-hidden="true"></span>
                            </button>
                        </span>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <form id="filter-bottom-form">
        <div class="form-inline" style="margin-bottom: 3px" id="top-menu">
            <select id="filter-text-cmb1" class="form-control cmbTxt chg">
            </select>
            <input type="text" id="filter-text-val1" class="form-control blur" aria-label="Search">
            <input type="text" readonly class="form-control" value="Dan" style="width: 50px">
            <select id="filter-text-cmb2" class="form-control cmbTxt chg">
            </select>
            <input type="text" id="filter-text-val2" class="form-control blur" aria-label="Search">
            <div class="material-switch form-control">
                <input id="checked-filter" type="checkbox" class="chg" checked/>
                <label for="checked-filter" class="label-success"></label>
                <span id="span-checked-filter" style="margin-left: 5px">Filter #2 Aktif</span>
            </div>
            <button id="lbl-find-id" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
        </div>
        <div class="form-inline inline-filter2" style="margin-bottom: 3px">
            <select id="filter-tgl-cmb1" class="form-control filter2 chg">
                <option value="">Tanggal</option>
            </select>
			<? try {
				echo DateControl::widget( [
					'name'          => 'tgl1',
					'type'          => DateControl::FORMAT_DATE,
					'value'         => Yii::$app->formatter->asDate( '-90 day' ),
					'options'       => [
						'id'    => 'filter-tgl-val1',
						'class' => 'filter2 dt',
					],
					'pluginOptions' => [
						'autoclose' => true,
//						'format'    => 'dd/mm/yyyy'
					],
					'pluginEvents'  => [
						"changeDate" => "function(e) { $('#filter-bottom-form').submit(); }",
					]
				] );
			} catch ( \yii\base\InvalidConfigException $e ) {
			} ?>
            -
			<? try {
				echo DateControl::widget( [
					'name'          => 'tgl2',
					'type'          => DateControl::FORMAT_DATE,
					'value'         => Yii::$app->formatter->asDate( 'now' ),
					'options'       => [
						'id'    => 'filter-tgl-val2',
						'class' => 'filter2 dt',
					],
					'pluginOptions' => [
						'autoclose' => true,
//						'format'    => 'dd/mm/yyyy'
					],
					'pluginEvents'  => [
						"changeDate" => "function(e) { $('#filter-bottom-form').submit(); }",
					]
				] );
			} catch ( \yii\base\InvalidConfigException $e ) {
			} ?>
            <select id="filter-num-cmb1" class="form-control filter2 chg">
                <option value="" selected>Jumlah</option>
            </select>
            <select id="filter-num-cmb2" class="form-control filter2 chg" style="width: 60px;">
                <option value=">">&gt;</option>
                <option value="=">&gt;=</option>
                <option value="<">&lt;</option>
                <option value="<=">&lt;=</option>
                <option value="=">=</option>
                <option value="<>">&lt;&gt;</option>
            </select>
            <input id="filter-num-val" type="number" class="form-control blur" placeholder="Nominal" value="0">
            <!--            <div class="input-group">-->
            <!--                <span class="input-group-addon" id="basic-addon1">Find</span>-->
            <!--                <input id="txt-find-id" type="text" class="form-control" aria-describedby="basic-addon1">-->
            <!--            </div>-->
            <button id="btn-find-id" class="btn btn-default" type="button">
                <span class="fa fa-binoculars"></span>
            </button>
        </div>
    </form>
<?php
$script = <<< JS
jQuery(function ($) {
    $("#checked-filter").change(function() {
        if(this.checked){
            $("#span-checked-filter").text("Filter #2 Aktif");    
            $(".inline-filter2").show();
        }else{
            $("#span-checked-filter").text("Filter #2 Non Aktif");
            $(".inline-filter2").hide();
        }
        // $(".filter2").prop('disabled', !this.checked);
        
    });    
    var option = $('.cmbTxt');
    option.find('option').remove().end();
    for (let val in setcmb['cmbTxt']) {
        option.append('<option value="' + val + '">' + setcmb['cmbTxt'][val] + '</option>');
    }
    $('#filter-text-cmb2').prop("selectedIndex", 1);
    if(Object.keys(setcmb['cmbTgl']).length > 0){
        var option = $('#filter-tgl-cmb1');
        option.find('option').remove().end();
        for (let val in setcmb['cmbTgl']) {
            option.append('<option value="' + val + '">' + setcmb['cmbTgl'][val] + '</option>');
        }
    }
    if(Object.keys(setcmb['cmbNum']).length > 0){
        var option = $('#filter-num-cmb1');
        option.find('option').remove().end();
        for (let val in setcmb['cmbNum']) {
            option.append('<option value="' + val + '">' + setcmb['cmbNum'][val] + '</option>');
        }
    }
    
    function submitFilter(){
        // console.log($('#checked-filter').is(":checked"));
        var url_string = $(location).attr('href');
        var url = new URL(url_string);
        var c = decodeURIComponent(url.searchParams.get("r"));
        const search={
            cmbTxt1: $('#filter-text-cmb1').val(),
            txt1 : $('#filter-text-val1').val(),
            cmbTxt2 : $('#filter-text-cmb2').val(),
            txt2 : $('#filter-text-val2').val(),
            check : $('#checked-filter').is(":checked") ? 'on' : 'off',
            cmbTgl1 : $('#filter-tgl-cmb1').val(),
            tgl1 : $('#filter-tgl-val1').val(),
            tgl2 : $('#filter-tgl-val2').val(),
            cmbNum1 : $('#filter-num-cmb1').val(),
            cmbNum2 : $('#filter-num-cmb2').val(),
            num1 : $('#filter-num-val').val(),
            r: c
        };
         $('#jqGrid')
         .jqGrid('setGridParam',{
             sortname: setcmb.sortname,
             sortorder: setcmb.sortorder,
             postData: {
                 query: JSON.stringify(search)
             }
         }).trigger("reloadGrid");
         $('#gbox_grid .s-ico').css('display','none');
         $('#gbox_grid #jqgh_grid_id .s-ico').css('display','');
         $('#gbox_grid #jqgh_grid_id .s-ico .ui-icon-triangle-1-s').removeClass('ui-state-disabled');
    }
    
    function findString (back) {
        var str = $('#txt-find-id').val();
         if (parseInt(navigator.appVersion)<4) return;
         var strFound;
         if (window.find) {
        
          // CODE FOR BROWSERS THAT SUPPORT window.find
        
          strFound=self.find(str,0,back);
          // if (!strFound) {
          //  strFound=self.find(str,0,1);
          //  while (self.find(str,0,1)) continue;
          // }
         }
         else if (navigator.appName.indexOf("Microsoft")!=-1) {
        
          // EXPLORER-SPECIFIC CODE
        
          if (TRange!=null) {
           TRange.collapse(false);
           strFound=TRange.findText(str);
           if (strFound) TRange.select();
          }
          if (TRange==null || strFound==0) {
           TRange=self.document.body.createTextRange();
           strFound=TRange.findText(str);
           if (strFound) TRange.select();
          }
         }
         else if (navigator.appName=="Opera") {
          alert ("Opera browsers not supported, sorry...")
          return;
         }
         if (!strFound) alert ("String '"+str+"' not found!")
         return;
    }
    
    $('#filter-bottom-form').on('submit', function(e){
		e.preventDefault();
		submitFilter();
    });
    $('#btn-find-id').on('click', function(e){
		e.preventDefault();
		$('#findModal').modal();
    });
    $('#find-up').on('click', function(e){
		e.preventDefault();
		findString(1);
    });
    $('#find-down').on('click', function(e){
		e.preventDefault();
		findString(0);
    });
    
    $('.blur').on('blur', function(e){
		submitFilter();
    });
    $('.chg').on('change', function(e){
		setTimeout(submitFilter(),500);
    });
    $('.blur').keydown(function (e){
        if(e.keyCode == 13){
            submitFilter();
        }
    });
    
});
JS;
$this->registerJs( $script );
?>