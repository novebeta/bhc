<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model afinance\models\Ttin */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ttin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'INNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'INTgl')->textInput() ?>

    <?= $form->field($model, 'SalesKode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusKode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'LeaseKode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'INJenis')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'INMemo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'INDP')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'UserID')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'INHarga')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MotorType')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MotorWarna')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKNo')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
