<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model afinance\models\Ttin */

$this->title = 'Create Ttin';
$this->params['breadcrumbs'][] = ['label' => 'Ttins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttin-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
