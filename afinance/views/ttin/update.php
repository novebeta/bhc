<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model afinance\models\Ttin */

$this->title = 'Update Ttin: ' . $model->INNo;
$this->params['breadcrumbs'][] = ['label' => 'Ttins', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->INNo, 'url' => ['view', 'id' => $model->INNo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ttin-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
