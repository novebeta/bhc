<?php
/* @var $this yii\web\View */
/* @var $model afinance\models\Tzcompany */
$this->title = 'Profile Dealer';
$this->params['breadcrumbs'][] = [ 'label' => 'Profile Dealer'];
?>
<div class="tzcompany-update">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
