<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ttss */

$this->title = 'Create Ttss';
$this->params['breadcrumbs'][] = ['label' => 'Ttsses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttss-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
