<?php
/* @var $this yii\web\View */
/* @var $searchModel app\models\TtssQuery */
/* @var $dataProvider yii\data\ActiveDataProvider */
use afinance\assets\AppAsset;
AppAsset::register( $this );
$this->title                   = 'Penerimaan Barang';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'SSNo' => 'No SS',
		'DONo' => 'No DO',
	],
	'cmbTgl'    => [
		'SSTgl' => 'Tgl SS'
	],
	'cmbNum'    => [
	],
	'sortname'  => "SSTgl desc,SupNama",
	'sortorder' => 'asc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \afinance\components\TdUi::mainGridJs( \afinance\models\Ttss::className(),
	'Penerimaan Barang' ), \yii\web\View::POS_READY );