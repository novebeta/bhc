<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ttss */
/* @var $form yii\widgets\ActiveForm */

use afinance\assets\AppAsset;
AppAsset::register($this);
?>
<div class="ttss-form">
    <div class="box box-primary">
        <?php $form = ActiveForm::begin(); ?>

        <div class="box-body ">

            <table id="detailGrid"></table>
            <div id="detailGridPager"></div>
        </div>

        <div class="box-footer">
            <div class="pull-right">-
                <?= Html::submitButton( 'Simpan', [ 'class' => 'btn btn-success' ] ) ?>
                <a href="<?= Url::to( [ 'ttgeneralledgerhd/memorial' ] ) ?>" class="btn btn-warning" role="button">Batal</a>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

<!-- confirm delete dialog -->
<div class="modal fade" id="confirmDeleteRowModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">Hapus data terpilih?</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="confirmDeleteRowModal_btnDelete">Hapus</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<?php
$script = <<< JS
$(document).ready(function () {
    var mydata = [
    {id:"12345",name:"Desktop Computer",note:"note",stock:"Yes",ship:"FE"},
    {id:"23456",name:"Laptop",note:"Long text ",stock:"Yes",ship:"IN"},
    {id:"34567",name:"LCD Monitor",note:"note3",stock:"Yes",ship:"TN"},
    {id:"45678",name:"Speakers",note:"note",stock:"No",ship:"AR"},
    {id:"56789",name:"Laser Printer",note:"note2",stock:"Yes",ship:"FE"},
    {id:"67890",name:"Play Station",note:"note3",stock:"No", ship:"FE"},
    {id:"76543",name:"Mobile Telephone",note:"note",stock:"Yes",ship:"AR"},
    {id:"87654",name:"Server",note:"note2",stock:"Yes",ship:"TN"},
    {id:"98765",name:"Matrix Printer",note:"note3",stock:"No", ship:"FE"}
    ];
    
    $('.date')
        .datepicker({format: "dd/mm/yyyy"})
        .width('130px');
    
    $('.clockpicker')
        .clockpicker({align: 'left', autoclose: true, 'default': 'now'})
        .width('100px');
    
    /* variables and functions */
    var grid = $("#detailGrid"),
        confirmDeleteModal = $('#confirmDeleteRowModal'),
        confirmDeleteModal_btnDelete = $("#confirmDeleteRowModal_btnDelete"),
        getSelectedRowId = function() { return grid.getGridParam('selrow'); },
        addRow = function() {
            grid.jqGrid('addRow', {
                initdata : {},
                position :"first",
                useDefValues : false,
                useFormatter : false,
                addRowParams : {extraparam:{}}
            });
            $('#jEditButton_jqg1').click();
        },
        deleteRow = function(rowId) {
            grid.jqGrid('delRowData', rowId);
            confirmDeleteModal.modal('hide');
        },
        actionsFormater = function(cellVal,options,rowObject) {
            var confirmDialog = "$('#confirmDeleteRowModal').modal({ backdrop: 'static', keyboard: true, show: true });";
            return '' +
             '<div style="margin-left:8px;">' +
              '<div title="Sunting data terpilih" style="float: left; cursor: pointer;" class="ui-pg-div ui-inline-edit" id="jEditButton_jqg1" onclick="$.fn.fmatter.rowactions.call(this,\'edit\');" onmouseover="$(this).addClass(\'active\');" onmouseout="$(this).removeClass(\'active\');">' +
               '<a class="glyphicon glyphicon-edit"></a>' +
              '</div>' +
              '<div title="Hapus baris terpilih" style="float: left; cursor: pointer;" class="ui-pg-div ui-inline-del" id="jDeleteButton_jqg1" onclick="' + confirmDialog + '" onmouseover="$(this).addClass(\'active\');" onmouseout="$(this).removeClass(\'active\');">' +
               '<a class="glyphicon glyphicon-trash"></a>' +
              '</div>' +
              '<div title="Simpan" style="float: left; display: none; cursor: pointer;" class="ui-pg-div ui-inline-save" id="jSaveButton_jqg1" onclick="$.fn.fmatter.rowactions.call(this,\'save\');" onmouseover="$(this).addClass(\'active\');" onmouseout="$(this).removeClass(\'active\');">' +
               '<a class="glyphicon glyphicon-floppy-disk"></a>' +
              '</div>' +
              '<div title="Batal" style="float: left; display: none; cursor: pointer;" class="ui-pg-div ui-inline-cancel" id="jCancelButton_jqg1" onclick="$.fn.fmatter.rowactions.call(this,\'cancel\');" onmouseover="$(this).addClass(\'active\');" onmouseout="$(this).removeClass(\'active\');">' +
               '<a class="glyphicon glyphicon-remove-circle"></a>' +
              '</div></div>';  
        };
    
    grid.jqGrid({
        datatype: 'local',
        data: mydata,
        editurl: 'clientArray',
        colModel: [
            { label: "Actions", name: "actions", width: 37, formatter: actionsFormater },
            {name:'id',index:'id', width:90, sorttype:"int", editable: true},
            {name:'name',index:'name', width:150,editable: true, editoptions:{size:"20",maxlength:"30"}},
            {name:'stock',index:'stock', width:60, editable: true, edittype:"checkbox",editoptions: {value:"Yes:No"}},
            {name:'ship',index:'ship', width:90, editable: true, edittype:"select",formatter:'select', editoptions:{value:"FE:FedEx;IN:InTime;TN:TNT;AR:ARAMEX"}},                 
            {name:'note',index:'note', width:200, sortable:false,editable: true}
        ],
        
        responsive: false,
        styleUI: 'Bootstrap',
        width: 780,
        height: 180,
        rowNum: 20,
        rownumbers: true,
        autowidth: true,
        shrinkToFit : true,
        
        pager: null,
        pgbuttons: false,
        pgtext: null,
        viewrecords: true
    });
    
    /* Add new row button */
    $("#detailGrid_actions")
    .empty()
    .css('text-align', 'center')
    .append('<button id="detailGrid_btnAdd" type="button" class="btn btn-primary btn-xs" style="cursor: pointer;" title="Tambah data baru"><span class="glyphicon glyphicon-plus"></span></button>');
    
    $("#detailGrid_btnAdd").click(addRow);
    confirmDeleteModal_btnDelete.click(function() {
        var rowId = getSelectedRowId();
        console.log(rowId);
        deleteRow(rowId);
    });
    
    /* Grid resize */
    function resizeGrid() {
        $('#detailGrid').setGridWidth($('.ttss-form').width());
    }
    
    resizeGrid();
    $('.box.box-primary').resize(resizeGrid);
})
JS;

$this->registerJs($script);



$css = <<< CSS
    .ui-jqgrid .ui-jqgrid-bdiv { background-color: white; }
CSS;

$this->registerCss($css);