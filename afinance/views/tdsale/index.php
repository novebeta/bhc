<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel afinance\models\TdsaleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
use afinance\assets\AppAsset;
AppAsset::register($this);
$this->title = 'Sales';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'SalesKode' => 'Kode',
		'SalesNama' => 'Nama',
		'SalesAlamat' => 'Alamat',
		'SalesTelepon' => 'Telepon',
		'SalesStatus' => 'Status',
		'SalesKeterangan' => 'Keterangan',
		'SalesKodeAstra' => 'Kode Astra',
		'TeamKode' => 'Team Kode',
		'SalesHSOid' => 'HSO ID',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
	'sortname'  => "SalesStatus asc,SalesKode asc,SalesNama",
	'sortorder' => 'asc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \afinance\components\TdUi::mainGridJs( \afinance\models\Tdsale::className(),'Sales' ), \yii\web\View::POS_READY );