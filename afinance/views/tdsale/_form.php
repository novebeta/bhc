<?php
use afinance\models\Tdteam;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model afinance\models\Tdsale */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col-md-12 col-xl-12 col-lg-12">
    <div class="box box-primary ">
		<?php $form = ActiveForm::begin( [
			'class' => 'form-horizontal'
		] ); ?>
        <div class="box-body ">
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'SalesKode' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'SalesKode' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'SalesKodeAstra' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'SalesKodeAstra' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'SalesHSOid' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'SalesHSOid' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'SalesNama' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'SalesNama' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'TeamKode' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'TeamKode' )->widget( Select2::classname(), [
						'value'         => $model->TeamKode,
						'options'       => [ 'placeholder' => 'Filter as you type ...' ],
						'pluginOptions' => [ 'highlight' => true ],
						'data'          => Tdteam::find()->select2()
					] )->label( false ) ?>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'SalesAlamat' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'SalesAlamat' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'SalesTelepon' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'SalesTelepon' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'SalesKeterangan' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'SalesKeterangan' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'SalesStatus' ) ?>:</label>
                <div class="col-sm-18">
			        <?= $form->field( $model, 'SalesStatus' )->dropDownList( [
				        'A' => 'AKTIF',
				        'N' => 'NON AKTIF',
			        ], [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="pull-right">-
				<?= Html::submitButton( 'Simpan', [ 'class' => 'btn btn-success' ] ) ?>
                <a href="<?= Url::to( [ 'tdsales/index' ] ) ?>" class="btn btn-warning" role="button">Batal</a>
            </div>
        </div>
		<?php ActiveForm::end(); ?>
    </div>
</div>
