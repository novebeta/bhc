<?php
/* @var $this yii\web\View */
/* @var $model afinance\models\Tdsale */
$this->title                   = 'Update Sales: ' . $model->SalesKode;
$this->params['breadcrumbs'][] = [ 'label' => 'Sales', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->SalesKode, 'url' => [ 'view', 'id' => $model->SalesKode ] ];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tdsale-update">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
