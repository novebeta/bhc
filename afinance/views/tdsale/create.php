<?php
/* @var $this yii\web\View */
/* @var $model afinance\models\Tdsale */
$this->title                   = 'Create Sales';
$this->params['breadcrumbs'][] = [ 'label' => 'Sales', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdsale-create">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
