$("#tipe-id").change(function () {
    var tipe = this.value;
    var option = $('#sort-id');
    option.find('option').remove().end();
    for (let val in setcmb[tipe].sort) {
        option.append('<option value="' + val + '">' + setcmb[tipe].sort[val] + '</option>');
    }
    option.prop("selectedIndex", 0);
    var option1 = $('#kode1-id');
    var option2 = $('#kode2-id');
    option1.find('option').remove().end();
    option2.find('option').remove().end();
    option1.append('<option value="">Semua</option>');
    option2.append('<option value="">Semua</option>');
    for (let val in setcmb[tipe].cmb) {
        option1.append('<option value="' + setcmb[tipe].cmb[val].value + '">' + setcmb[tipe].cmb[val].label + '</option>');
        option2.append('<option value="' + setcmb[tipe].cmb[val].value + '">' + setcmb[tipe].cmb[val].label + '</option>');
    }
    option1.prop("selectedIndex", 0);
    option2.prop("selectedIndex", 0);
});

$("#tipe-id").trigger('change');