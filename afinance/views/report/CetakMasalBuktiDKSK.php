<?php
use afinance\models\Tdarea;
use afinance\models\Tdbbn;
use afinance\models\Tdcustomer;
use afinance\models\Tddealer;
use afinance\models\Tdleasing;
use afinance\models\Tdlokasi;
use afinance\models\Tdmotortype;
use afinance\models\Tdmotorwarna;
use afinance\models\Tdprogram;
use afinance\models\Tdsale;
use afinance\models\Tdsupplier;
use afinance\models\Tuser;
use kartik\date\DatePicker;
use yii\helpers\Html;

$this->title                   = 'Daftar Kas Masuk';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;
?>
    <div class="laporan-data col-md-16" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
            <div class="box-body">
                <div class="container-fluid">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label class="control-label col-sm-7">Laporan</label>
                            <div class="col-sm-17">
								<?= Html::dropDownList( 'tipe', null, [
									'tdmotortype'  => 'Type Motor',
									'tdmotorharga' => 'Harga Motor',
									'tdmotorWarna' => 'Warna Motor',
									'tdlokasi'     => 'WareHouse',
									'tdSupplier'   => 'Supplier',
									'tddealer'     => 'Dealer',
									'tdSales'      => 'Sales',
									'tdCustomer'   => 'Konsumen',
									'tdleasing'    => 'Leasing',
									'tdprogram'    => 'Program',
									'tdarea'       => 'Area',
									'tdbbn'        => 'BBN',
									'tuser'        => 'User',
									'tuakses'      => 'Hak Akses',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            <label class="control-label col-sm-7">Format</label>
                            <div class="col-sm-17">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <button type="submit" formtarget="_blank" class="btn btn-primary pull-right">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">

            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
$arr = [
	'tdmotortype'  => [
		'cmb'  => Tdmotortype::find()->combo(),
		'sort' => [
			'MotorType'  => 'Type Motor',
			'TypeStatus' => 'Status',
		]
	],
	'tdmotorharga' => [
		'cmb'  => Tdmotortype::find()->combo(),
		'sort' => [
			'MotorType'  => 'Type Motor',
			'TypeStatus' => 'Status',
		]
	],
	'tdmotorWarna' => [
		'cmb'  => Tdmotorwarna::find()->combo(),
		'sort' => [
			'MotorWarna' => 'Warna Motor'
		]
	],
	'tdlokasi'     => [
		'cmb'  => Tdlokasi::find()->combo(),
		'sort' => [
			'LokasiKode' => 'Lokasi'
		]
	],
	'tdSupplier'   => [
		'cmb'  => Tdsupplier::find()->combo(),
		'sort' => [
			'SupKode' => 'Kode Supplier'
		]
	],
	'tddealer'     => [
		'cmb'  => Tddealer::find()->combo(),
		'sort' => [
			'DealerKode' => 'Kode Dealer'
		]
	],
	'tdSales'      => [
		'cmb'  => Tdsale::find()->combo(),
		'sort' => [
			'SalesKode' => 'Kode Sales'
		]
	],
	'tdCustomer'   => [
		'cmb'  => Tdcustomer::find()->combo(),
		'sort' => [
			'CusKode' => 'Kode Customer'
		]
	],
	'tdleasing'    => [
		'cmb'  => Tdleasing::find()->combo(),
		'sort' => [
			'LeaseKode' => 'Kode Leasing'
		]
	],
	'tdprogram'    => [
		'cmb'  => Tdprogram::find()->combo(),
		'sort' => [
			'ProgramNama' => 'Nama Progam'
		]
	],
	'tuser'        => [
		'cmb'  => Tuser::find()->combo(),
		'sort' => [
			'UserID' => 'UserID'
		]
	],
	'tdarea'       => [
		'cmb'  => Tdarea::find()->combo(),
		'sort' => [
			'Kabupaten' => 'Kabupaten'
		]
	],
	'tdbbn'        => [
		'cmb'  => Tdbbn::find()->combo(),
		'sort' => [
			'Kabupaten' => 'Kabupaten',
			'MotorType' => 'Tipe Motor',
		]
	]
];
$this->registerJsVar( 'setcmb', $arr );
$this->registerJs( $this->render( 'LaporanData.js' ) );