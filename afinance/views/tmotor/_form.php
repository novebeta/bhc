<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model afinance\models\Tmotor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tmotor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'MotorAutoN')->textInput() ?>

    <?= $form->field($model, 'MotorNoMesin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MotorNoRangka')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MotorType')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MotorWarna')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MotorTahun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MotorMemo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DealerKode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SSNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FBNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FBHarga')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DKNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SKNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PDNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SDNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'BPKBNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'BPKBTglJadi')->textInput() ?>

    <?= $form->field($model, 'BPKBTglAmbil')->textInput() ?>

    <?= $form->field($model, 'STNKNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'STNKTglMasuk')->textInput() ?>

    <?= $form->field($model, 'STNKTglBayar')->textInput() ?>

    <?= $form->field($model, 'STNKTglJadi')->textInput() ?>

    <?= $form->field($model, 'STNKTglAmbil')->textInput() ?>

    <?= $form->field($model, 'STNKNama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'STNKAlamat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PlatNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PlatTgl')->textInput() ?>

    <?= $form->field($model, 'PlatTglAmbil')->textInput() ?>

    <?= $form->field($model, 'FakturAHMNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FakturAHMTgl')->textInput() ?>

    <?= $form->field($model, 'FakturAHMTglAmbil')->textInput() ?>

    <?= $form->field($model, 'FakturAHMNilai')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'RKNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'RKHarga')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SKHarga')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SDHarga')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MMHarga')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NoticeNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NoticeTglJadi')->textInput() ?>

    <?= $form->field($model, 'NoticeTglAmbil')->textInput() ?>

    <?= $form->field($model, 'NoticePenerima')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'STNKPenerima')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'BPKBPenerima')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PlatPenerima')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SSHarga')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SRUTNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SRUTTglJadi')->textInput() ?>

    <?= $form->field($model, 'SRUTTglAmbil')->textInput() ?>

    <?= $form->field($model, 'SRUTPenerima')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
