<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model afinance\models\Tmotor */

$this->title = 'Create Tmotor';
$this->params['breadcrumbs'][] = ['label' => 'Tmotors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tmotor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
