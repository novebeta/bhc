<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model afinance\models\Tmotor */

$this->title = 'Update Tmotor: ' . $model->MotorAutoN;
$this->params['breadcrumbs'][] = ['label' => 'Tmotors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->MotorAutoN, 'url' => ['view', 'MotorAutoN' => $model->MotorAutoN, 'MotorNoMesin' => $model->MotorNoMesin]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tmotor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
