<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel afinance\models\TmotorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Motor';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'TeamKode'       => 'Kode',
		'TeamNama'       => 'Nama',
		'TeamAlamat'     => 'Alamat',
		'TeamTelepon'    => 'Telepon',
		'TeamKeterangan' => 'Keterangan',
		'TeamStatus'     => 'Status',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \afinance\components\TdUi::mainGridJs( \afinance\models\Tmotor::className() ), \yii\web\View::POS_READY );