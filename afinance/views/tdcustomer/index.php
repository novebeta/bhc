<?php
/* @var $this yii\web\View */
use afinance\assets\AppAsset;
AppAsset::register( $this );
$this->title                   = 'Konsumen';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'CusKode'      => 'Kode',
		'CusKTP'       => 'Ktp',
		'CusNama'      => 'Nama',
		'CusAlamat'    => 'Alamat',
		'CusRT'        => 'Rt',
		'CusRW'        => 'Rw',
		'CusProvinsi'  => 'Provinsi',
		'CusKabupaten' => 'Kabupaten',
		'CusKecamatan' => 'Kecamatan',
		'CusKelurahan' => 'Kelurahan',
		'CusKodePos'   => 'Kode Pos',
		'CusTelepon'   => 'Telepon',
		'CusSex'       => 'Sex',
		'CusTempatLhr' => 'Tempat Lhr',
		'CusAgama'       => 'Agama',
		'CusPekerjaan'   => 'Pekerjaan',
		'CusPendidikan'  => 'Pendidikan',
		'CusPengeluaran' => 'Pengeluaran',
		'CusKeterangan'    => 'Keterangan',
		'CusEmail'         => 'Email',
		'CusKodeKons'      => 'Kode Kons',
		'CusStatusHP'      => 'Status Hp',
		'CusStatusRumah'   => 'Status Rumah',
		'CusPembeliNama'   => 'Pembeli Nama',
		'CusPembeliKTP'    => 'Pembeli Ktp',
		'CusPembeliAlamat' => 'Pembeli Alamat',
		'CusKK'            => 'Kk',
		'CusTelepon2'      => 'Telepon2',
	],
	'cmbTgl' => [
		'CusTglBeli' => 'Tgl Beli',
		'CusTglLhr'  => 'Tgl Lhr',
	],
	'cmbNum' => [
	],
	'sortname'  => "CusKode asc,CusKTP asc,CusTglBeli",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \afinance\components\TdUi::mainGridJs( \afinance\models\Tdcustomer::className() ,'Konsumen'), \yii\web\View::POS_READY );