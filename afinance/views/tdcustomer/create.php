<?php
/* @var $this yii\web\View */
/* @var $model afinance\models\Tdcustomer */
$this->title                   = 'Create Konsumen';
$this->params['breadcrumbs'][] = [ 'label' => 'Konsumen', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdcustomer-create">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
