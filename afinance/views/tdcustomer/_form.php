<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model afinance\models\Tdcustomer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tdcustomer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'CusKode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusKTP')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusNama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusAlamat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusRT')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusRW')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusProvinsi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusKabupaten')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusKecamatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusKelurahan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusKodePos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusTelepon')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusSex')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusTempatLhr')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusTglLhr')->textInput() ?>

    <?= $form->field($model, 'CusAgama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusPekerjaan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusPendidikan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusPengeluaran')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusTglBeli')->textInput() ?>

    <?= $form->field($model, 'CusKeterangan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusEmail')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusKodeKons')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusStatusHP')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusStatusRumah')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusPembeliNama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusPembeliKTP')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusPembeliAlamat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusKK')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CusTelepon2')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
