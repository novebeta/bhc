<?php
/* @var $this yii\web\View */
/* @var $model afinance\models\Tdcustomer */
$this->title                   = 'Update Konsumen: ' . $model->CusKode;
$this->params['breadcrumbs'][] = [ 'label' => 'Konsumen', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->CusKode, 'url' => [ 'view', 'id' => $model->CusKode ] ];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tdcustomer-update">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
