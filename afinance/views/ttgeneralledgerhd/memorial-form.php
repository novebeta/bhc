<?php

use common\components\Custom;
use kartik\datecontrol\DateControl;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model afinance\models\Ttgeneralledgerhd */
/* @var $form yii\widgets\ActiveForm */

use afinance\assets\AppAsset;
AppAsset::register($this);
$params = '&id='.$id.'&action='.$action;
?>

<div class="ttgeneralledgerhd-form">
    <div class="box box-primary" style="flex: 1 1 auto">
        <?php $form = ActiveForm::begin(['action' => Custom::url(\Yii::$app->controller->id.'/memorial-update'.$params ),'options' => ['method' => 'post']]); ?>

        <div class="box-body">
            <div class="row-no-gutters">
                <div class="form-inline col-md-14">
                    <div style="margin-bottom: 3px;">
<!--                    <div class="col-md-10" style="background-color: lightblue;">-->
                        <?= $form->field($model, 'NoGL', ['template' => '{label}{input}'])
                            ->textInput(['maxlength' => true, 'placeholder' => 'No Jurnal'])
                            ->label('No Jurnal&nbsp') ?>
<!--                    </div>-->
<!--                    <div class="col-md-10" style="background-color: lightblue;">-->
                        <?= $form->field($model, 'TglGL', ['template' => '{label}{input}'])
                            ->widget(DateControl::className(), [
                                'type'          => DateControl::FORMAT_DATE,
                                'pluginOptions' => [ 'autoclose' => true ]
                            ])->label('Tanggal Jurnal&nbsp') ?>
<!--                    </div>-->
                    </div>
                    <div style="margin-bottom: 3px;">
<!--                    <div class="col-md-24" style="background-color: indianred;">-->
                        <?= $form->field($model, 'MemoGL', ['template' => '{label}{input}'])
                            ->textarea(['maxlength' => true, 'placeholder' => 'Memo Jurnal', 'cols' => 80, 'rows' => 3])
                            ->label('Memo &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp &nbsp') ?>
<!--                    </div>-->
                    </div>
                </div>
                <div class="form-inline col-md-10">
                    <div style="margin-bottom: 3px;">
                        <?= $form->field($model, 'GLValid', ['template' => '{label}<div class="input-group"><input type="text" class="form-control" value="Belum"><span class="input-group-addon">{input}</span></div>'])
                            ->checkbox([], false)
                            ->label('Validasi Jurnal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;') ?>
                    </div>
                    <div style="margin-bottom: 3px;">
                        <?= $form->field($model, 'GLLink', ['template' => '{label}{input}'])
                            ->textInput(['maxlength' => true])
                            ->label('Link Transaksi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;') ?>
                    </div>
                    <div style="margin-bottom: 3px;">
                        <div class="form-group">
                            <label class="control-label" for="selisih">Hutang - Piutang &nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-info-sign"></i></button>
                                </span>
                                <input type="text" class="form-control" placeholder="...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i></button>
                                    <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i></button>
                                </span>
                            </div>
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row col-md-24" style="margin-bottom: 3px;">
                <table id="detailGrid"></table>
                <div id="detailGridPager"></div>
            </div>

            <div class="form-inline pull-right">
                <div class="form-group">
                    <label class="control-label" for="selisih">Selisih &nbsp;</label>
                    <input type="text" id="selisih" class="form-control" name="selisih" readonly>
                    <div class="help-block"></div>
                </div>
            <?= $form->field($model, 'TotalDebetGL')->textInput(['maxlength' => true, 'readonly'=>''])
                ->label('Total &nbsp') ?>
            <?= $form->field($model, 'TotalKreditGL')->textInput(['maxlength' => true, 'readonly'=>''])->label(false) ?>
            </div>
        </div>
        <div class="box-footer">
            <div class="pull-right">
<!--                <div class="btn-group btn-group-justified">-->
                    <?= Html::submitButton( 'Simpan', [ 'class' => 'btn btn-info' ] ) ?>
                    <a href="<?= Custom::url(\Yii::$app->controller->id.'/memorial-print'.$params ) ?>" class="btn btn-warning" role="button">Print</a>
                    <a href="<?= Custom::url(\Yii::$app->controller->id.'/memorial-cancel'.$params ) ?>" class="btn btn-danger" role="button">Batal</a>
<!--                </div>-->
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php
$dataCOA = Yii::$app->db->createCommand('SELECT CONCAT(NoAccount, ":", NoAccount) AS `coa` FROM `traccount` WHERE JenisAccount = "Detail"')->queryColumn();
$dataCOA = implode(';', $dataCOA);

$dataCOA2 = Yii::$app->db->createCommand('SELECT CONCAT(NoAccount, ":", NamaAccount) AS `coa` FROM `traccount` WHERE JenisAccount = "Detail"')->queryColumn();
$dataCOA2 = implode(';', $dataCOA2);

$this->registerJs(\afinance\components\TUi::detailGridJs(\afinance\models\Ttgeneralledgerit::className(), [
    'url' => Custom::url(\Yii::$app->controller->id.'/memorial-detail'.$params ),
    'height' => 220,
    'extraParams' => [
        'NoGL' => $action === 'create' ? "" : $model->NoGL,
        'TglGL' => $model->TglGL
    ],
    'colModel' => [
        [
            'name' => 'NoAccount',
            'label' => 'Nomor',
            'editable' => true,
            'editor' => [
                'type' => 'select2',
                'data' => $dataCOA
            ]
        ],
        [
            'name' => 'NamaAccount',
            'label' => 'Nama Perkiraan',
            'width' => 200,
            'editable' => true,
            'editor' => [
                'type' => 'select2',
                'data' => $dataCOA2
            ]
        ],
        [
            'name' => 'KeteranganGL',
            'label' => 'Keterangan',
            'width' => 270,
            'editable' => true
        ],
        [
            'name' => 'DebetGL',
            'label' => 'Debet',
            'editable' => true,
            'align' => "right",
            'formatter' => 'integer',
            'formatoptions' => ['decimalSeparator' => ".", 'decimalPlaces' => 2, 'thousandsSeparator' => ","]
        ],
        [
            'name' => 'KreditGL',
            'label' => 'Kredit',
            'editable' => true,
            'align' => "right",
            'formatter' => 'integer',
            'formatoptions' => ['decimalSeparator' => ".", 'decimalPlaces' => 2, 'thousandsSeparator' => ","]
        ]
    ],
    'funcAfterLoadGrid' => <<< JS
    function(grid, response) {
        var totalDebet = 0,
            totalKredit = 0;
        
        for(var i = 0; i < response.rows.length; i++) {
            totalDebet += parseFloat(response.rows[i].DebetGL);
            totalKredit += parseFloat(response.rows[i].KreditGL);
        }
        
        $('#selisih').val(totalDebet-totalKredit);
        $('#ttgeneralledgerhd-totaldebetgl').val(totalDebet);
        $('#ttgeneralledgerhd-totalkreditgl').val(totalKredit);
    }
JS

]));