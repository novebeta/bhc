<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model afinance\models\Ttgeneralledgerhd */

$this->title = 'Create Jurnal Memorial';
$this->params['breadcrumbs'][] = ['label' => 'Jurnal Memorial', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttgeneralledgerhd-create">

    <?= $this->render('memorial-form', [
        'model' => $model,
        'id' => $id,
        'action' => 'create'
    ]) ?>

</div>
