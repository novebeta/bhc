<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model afinance\models\Ttgeneralledgerhd */

$this->title = 'Update Jurnal Memorial: ' . $model->NoGL;
$this->params['breadcrumbs'][] = ['label' => 'Jurnal Memorial', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NoGL, 'url' => ['view', 'NoGL' => $model->NoGL, 'TglGL' => $model->TglGL]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ttgeneralledgerhd-update">

    <?= $this->render('memorial-form', [
        'model' => $model,
        'id' => $id,
        'action' => 'update'
    ]) ?>

</div>
