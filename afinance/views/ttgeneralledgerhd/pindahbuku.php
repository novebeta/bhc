<?php
use common\components\Custom;
use afinance\assets\AppAsset;
/* @var $this yii\web\View */
/* @var $searchModel afinance\models\TtgeneralledgerhdSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
AppAsset::register( $this );
$this->title                   = 'Jurnal Pemindahan Bukuan';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'NoGL'    => 'No Jurnal',
		'MemoGL'  => 'Memo Jurnal',
		'GLLink'  => 'No Transaksi',
		'HPLink'  => 'HP Link',
		'GLValid' => 'Validasi',
	],
	'cmbTgl'    => [
		'TglGL' => 'Tgl Jurnal',
	],
	'cmbNum'    => [
		'TotalDebetGL'  => 'Debet',
		'TotalKreditGL' => 'Kredit',
	],
	'sortname'  => "TglGL",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \afinance\components\TdUi::mainGridJs( \afinance\models\Ttgeneralledgerhd::className(), 'Jurnal Pemindah Bukuan', [
	'url_add'    => Custom::url( 'ttgeneralledgerhd/create' ),
	'url_update' => Custom::url( 'ttgeneralledgerhd/update' ),
	'url_delete' => Custom::url( 'ttgeneralledgerhd/td' ),
] ), \yii\web\View::POS_READY );
