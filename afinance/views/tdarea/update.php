<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model afinance\models\Tdarea */

$this->title = 'Update Area: ' . $model->Provinsi;
$this->params['breadcrumbs'][] = ['label' => 'Area', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Provinsi, 'url' => ['view', 'Provinsi' => $model->Provinsi, 'Kabupaten' => $model->Kabupaten, 'Kecamatan' => $model->Kecamatan, 'Kelurahan' => $model->Kelurahan, 'KodePos' => $model->KodePos]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tdarea-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
