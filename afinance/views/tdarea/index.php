<?php
/* @var $this yii\web\View */
use afinance\assets\AppAsset;
AppAsset::register( $this );
$this->title                   = 'Area';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'Provinsi'       => 'Provinsi',
		'Kabupaten'      => 'Kabupaten',
		'Kecamatan'      => 'Kecamatan',
		'Kelurahan'      => 'Kelurahan',
		'KodePos'        => 'Kode Pos',
		'AreaStatus'     => 'Area Status',
		'ProvinsiAstra'  => 'Provinsi Astra',
		'KabupatenAstra' => 'Kabupaten Astra',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
	'sortname'  => "AreaStatus asc,Provinsi asc,Kabupaten",
	'sortorder' => 'asc'
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \afinance\components\TdUi::mainGridJs( \afinance\models\Tdarea::className(),'Area' ), \yii\web\View::POS_READY );
//$script = <<< JS
//$(document).ready(function () {
//
//    function resizeGrid() {
//        $('#jqGrid')
//        .setGridWidth($('.box.box-primary').width())
//        .setGridHeight($(window).height() - $("#jqGrid").offset().top - 200);
//    }
//
//    resizeGrid();
//
//    $('.box.box-primary').resize(resizeGrid);
//
//});
//
//JS;
//$this->registerJs( $script, \yii\web\View::POS_READY );