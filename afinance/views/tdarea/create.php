<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model afinance\models\Tdarea */

$this->title = 'Create Area';
$this->params['breadcrumbs'][] = ['label' => 'Area', 'url' => ['tdarea/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdarea-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
