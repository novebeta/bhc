<?php
/* @var $this yii\web\View */
/* @var $model afinance\models\Tddealer */
$this->title                   = 'Update Dealer: ' . $model->DealerKode;
$this->params['breadcrumbs'][] = [ 'label' => 'Dealer', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->DealerKode, 'url' => [ 'view', 'id' => $model->DealerKode ] ];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tddealer-update">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
