<?php
/* @var $this yii\web\View */
/* @var $model afinance\models\Tddealer */
$this->title                   = 'Create Dealer';
$this->params['breadcrumbs'][] = [ 'label' => 'Dealer', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tddealer-create">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
