<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model afinance\models\Tddealer */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-md-12 col-xl-12 col-lg-12">
    <div class="box box-primary ">
		<?php $form = ActiveForm::begin( [
			'class' => 'form-horizontal'
		] ); ?>
        <div class="box-body ">
            <div class="form-group">
                <label class="control-label col-sm-4"><?= $model->getAttributeLabel( 'DealerKode' ) ?>:</label>
                <div class="col-sm-4">
					<?= $form->field( $model, 'DealerKode' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
                <label class="control-label col-sm-4"><?= $model->getAttributeLabel( 'DealerKodeDMain' ) ?>:</label>
                <div class="col-sm-4">
		            <?= $form->field( $model, 'DealerKodeDMain' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
                <label class="control-label col-sm-4"><?= $model->getAttributeLabel( 'DealerKodeDAstra' ) ?>:</label>
                <div class="col-sm-4">
		            <?= $form->field( $model, 'DealerKodeDAstra' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4"><?= $model->getAttributeLabel( 'DealerStatus' ) ?>:</label>
                <div class="col-sm-20">
					<?= $form->field( $model, 'DealerStatus' )->dropDownList( [
						'A' => 'AKTIF',
						'N' => 'NON AKTIF',
						'1' => 'Dealer Utama',
					], [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4"><?= $model->getAttributeLabel( 'DealerNama' ) ?>:</label>
                <div class="col-sm-20">
					<?= $form->field( $model, 'DealerNama' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4"><?= $model->getAttributeLabel( 'DealerContact' ) ?>:</label>
                <div class="col-sm-20">
					<?= $form->field( $model, 'DealerContact' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4"><?= $model->getAttributeLabel( 'DealerAlamat' ) ?>:</label>
                <div class="col-sm-20">
					<?= $form->field( $model, 'DealerAlamat' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4"><?= $model->getAttributeLabel( 'DealerKota' ) ?>:</label>
                <div class="col-sm-20">
					<?= $form->field( $model, 'DealerKota' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4"><?= $model->getAttributeLabel( 'DealerTelepon' ) ?>:</label>
                <div class="col-sm-8">
					<?= $form->field( $model, 'DealerTelepon' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
                <label class="control-label col-sm-2"><?= $model->getAttributeLabel( 'DealerFax' ) ?>:</label>
                <div class="col-sm-10">
		            <?= $form->field( $model, 'DealerFax' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4"><?= $model->getAttributeLabel( 'DealerProvinsi' ) ?>:</label>
                <div class="col-sm-20">
					<?= $form->field( $model, 'DealerProvinsi' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4"><?= $model->getAttributeLabel( 'DealerKeterangan' ) ?>:</label>
                <div class="col-sm-20">
					<?= $form->field( $model, 'DealerKeterangan' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4"><?= $model->getAttributeLabel( 'DealerEmail' ) ?>:</label>
                <div class="col-sm-20">
					<?= $form->field( $model, 'DealerEmail' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="pull-right">-
				<?= Html::submitButton( 'Simpan', [ 'class' => 'btn btn-success' ] ) ?>
                <a href="<?= Url::to( [ 'tdbbn/index' ] ) ?>" class="btn btn-warning" role="button">Batal</a>
            </div>
        </div>
		<?php ActiveForm::end(); ?>
    </div>
</div>
