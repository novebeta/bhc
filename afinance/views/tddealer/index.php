<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel afinance\models\TddealerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
use afinance\assets\AppAsset;
AppAsset::register($this);
$this->title = 'Dealer';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'DealerKode' => 'Kode',
		'DealerNama' => 'Nama',
		'DealerContact' => 'Kontak',
		'DealerAlamat' => 'Alamat',
		'DealerKota' => 'Kota',
		'DealerProvinsi' => 'Provinsi',
		'DealerTelepon' => 'Telepon',
		'DealerFax' => 'Fax',
		'DealerEmail' => 'Email',
		'DealerKeterangan' => 'Keterangan',
		'DealerStatus' => 'Status',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
	'sortname'  => "DealerStatus asc,DealerKode asc,DealerNama",
	'sortorder' => 'asc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \afinance\components\TdUi::mainGridJs( \afinance\models\Tddealer::className() ,'Dealer'), \yii\web\View::POS_READY );