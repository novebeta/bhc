<?php
/* @var $this yii\web\View */
/* @var $model afinance\models\Tdprogram */
$this->title                   = 'Update Program: ' . $model->ProgramNama;
$this->params['breadcrumbs'][] = [ 'label' => 'Program', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->ProgramNama,
                                   'url'   => [
	                                   'view',
	                                   'ProgramNama' => $model->ProgramNama,
	                                   'MotorType'   => $model->MotorType
                                   ]
];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tdprogram-update">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
