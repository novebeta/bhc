<?php
/* @var $this yii\web\View */
/* @var $model afinance\models\Tdprogram */
$this->title                   = 'Create Program';
$this->params['breadcrumbs'][] = [ 'label' => 'Program', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdprogram-create">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
