<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel afinance\models\TdprogramSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
use afinance\assets\AppAsset;
AppAsset::register($this);
$this->title = 'Program';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'ProgramNama' => 'Nama Program',
		'MotorType' => 'Type Motor',
	],
	'cmbTgl' => [
		'PrgTglAwal' => 'Awal',
		'PrgTglAkhir' => 'Akhir',
	],
	'cmbNum' => [
		'PrgSubsSupplier' => 'Subsidi Supplier',
		'PrgSubsDealer' => 'Subsidi Dealer',
		'PrgSubsTotal' => 'Subsidi Total'
	],
	'sortname'  => "ProgramNama asc,MotorType asc,PrgTglAwal desc,PrgSubsTotal",
	'sortorder' => 'asc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \afinance\components\TdUi::mainGridJs( \afinance\models\Tdprogram::className() ,'Program'), \yii\web\View::POS_READY );