<?php
use afinance\models\Tdmotortype;
use kartik\datecontrol\DateControl;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model afinance\models\Tdprogram */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-md-12 col-xl-12 col-lg-12">
    <div class="box box-primary ">
		<?php $form = ActiveForm::begin( [
			'class' => 'form-horizontal'
		] ); ?>
        <div class="box-body ">
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'ProgramNama' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'ProgramNama' )->textInput( [ 'maxlength' => true ] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'MotorType' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'MotorType' )->widget( Select2::classname(), [
						'value'         => $model->MotorType,
						'options'       => [ 'placeholder' => 'Filter as you type ...' ],
						'pluginOptions' => [ 'highlight' => true ],
						'data'          => Tdmotortype::find()->select2()
					] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'PrgTglAwal' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'PrgTglAwal' )->widget( DateControl::classname(), [
						'type'           => DateControl::FORMAT_DATE,
						'value'         => Yii::$app->formatter->asDate( 'now' ),
						'widgetOptions'  => [
							'pluginOptions' => [
								'autoclose' => true
							]
						]
					] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'PrgTglAkhir' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'PrgTglAkhir' )->widget( DateControl::classname(), [
						'type'           => DateControl::FORMAT_DATE,
						'value'         => Yii::$app->formatter->asDate( 'now' ),
						'widgetOptions'  => [
							'pluginOptions' => [
								'autoclose' => true
							]
						]
					] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'PrgSubsSupplier' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'PrgSubsSupplier' )->widget( NumberControl::classname(), [
						'maskedInputOptions' => [
							'allowMinus' => false
						],
					] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'PrgSubsDealer' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'PrgSubsDealer' )->widget( NumberControl::classname(), [
						'maskedInputOptions' => [
							'allowMinus' => false
						],
					] )->label( false ) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6"><?= $model->getAttributeLabel( 'PrgSubsTotal' ) ?>:</label>
                <div class="col-sm-18">
					<?= $form->field( $model, 'PrgSubsTotal' )->widget( NumberControl::classname(), [
						'maskedInputOptions' => [
							'allowMinus' => false
						],
					] )->label( false ) ?>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="pull-right">-
				<?= Html::submitButton( 'Simpan', [ 'class' => 'btn btn-success' ] ) ?>
                <a href="<?= Url::to( [ 'tdprogram/index' ] ) ?>" class="btn btn-warning" role="button">Batal</a>
            </div>
        </div>
		<?php ActiveForm::end(); ?>
    </div>
</div>
