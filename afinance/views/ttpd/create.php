<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model afinance\models\Ttpd */

$this->title = 'Create Ttpd';
$this->params['breadcrumbs'][] = ['label' => 'Ttpds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttpd-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
