<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model afinance\models\Ttpd */

$this->title = 'Update Ttpd: ' . $model->PDNo;
$this->params['breadcrumbs'][] = ['label' => 'Ttpds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->PDNo, 'url' => ['view', 'id' => $model->PDNo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ttpd-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
