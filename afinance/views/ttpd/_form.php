<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model afinance\models\Ttpd */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ttpd-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'PDNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PDTgl')->textInput() ?>

    <?= $form->field($model, 'SDNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DealerKode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'LokasiKode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PDMemo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PDTotal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'UserID')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PDJam')->textInput() ?>

    <?= $form->field($model, 'NoGL')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
