<?php
namespace afinance\controllers;
use afinance\components\AfinanceController;
use afinance\components\TdAction;
use afinance\models\Tdsupplier;

use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use common\components\Custom;
/**
 * TdsupplierController implements the CRUD actions for Tdsupplier model.
 */
class TdsupplierController extends AfinanceController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actions() {
		return [
			'td' => [
				'class' => TdAction::className(),
				'model' => Tdsupplier::className(),
			],
		];
	}
	/**
	 * Lists all Tdsupplier models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index' );
	}
	/**
	 * Displays a single Tdsupplier model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Creates a new Tdsupplier model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Tdsupplier();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'index','id'=> $this->generateIdBase64FromModel($model)  ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Tdsupplier model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$model = $this->findModelBase64( 'Tdsupplier', $id );
		if ( $model->load( Yii::$app->request->post() ) ) {
			if ( ! $model->save() ) {
				Yii::$app->getSession()->setFlash( 'error', Html::errorSummary( $model ) );
				return $this->render( 'update', [
					'model' => $model,
				] );
			}
			return $this->redirect( [ 'tdsupplier/index' ,'id'=> $this->generateIdBase64FromModel($model)  ] );
		}
		return $this->render( 'update', [
			'model' => $model,
            'id'    => $this->generateIdBase64FromModel($model),
		] );
	}
	/**
	 * Deletes an existing Tdsupplier model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Tdsupplier model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Tdsupplier the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Tdsupplier::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
