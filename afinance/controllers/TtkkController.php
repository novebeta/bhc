<?php

namespace afinance\controllers;

use afinance\components\AfinanceController;
use afinance\components\TdAction;
use Yii;
use afinance\models\Ttkk;

use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TtkkController implements the CRUD actions for Ttkk model.
 */
class TtkkController extends AfinanceController
{
	public function actions() {
		$colGrid  = null;
		$joinWith = [];
		$join     = [];
		$where    = [];
		if ( isset( $_POST['query'] ) ) {
			$r = Json::decode( $_POST['query'], true )['r'];
			switch ( $r ) {
				case 'ttkk/pengajuan-bayar-bbn-kas':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'Bayar BBN')",
							'params'    => []
						],
						[
							'op'        => 'AND',
							'condition' => "(LEFT(ttkk.KKNo,2) = 'KP')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-subsidi-dealer-2':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'SubsidiDealer2')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-retur-harga':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'Retur Harga')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-insentif-sales':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'Insentif Sales')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-potongan-khusus':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'Potongan Khusus')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-bank-unit':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'KK Ke Bank')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-umum':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'Umum')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-bank-bengkel':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'KK Ke Bengkel')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-pinjaman-karyawan':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'Pinjaman Karyawan')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-dealer-pos':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'KK Dealer Ke Pos')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-pos-dealer':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'KK Pos Ke Dealer')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-bayar-hutang-unit': // Kas Keluar Bayar Hutang
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'Bayar Unit')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-bayar-hutang-bbn': // Kas Keluar Bayar Hutang
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'Bayar BBN')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-bayar-bbn-progresif': // Kas Keluar Bayar Hutang
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'BBN Plus')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-bayar-bbn-acc': // Kas Keluar Bayar Hutang
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'BBN Acc')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-transfer':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis = 'Kas Transfer')",
							'params'    => []
						],
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis = 'Kas Transfer')",
							'params'    => []
						]
					];
					$join[]  = [
						'type'   => 'INNER JOIN',
						'table'  => "ttkm",
						'on'     => 'ttkk.KKLInk = ttkm.KMLink',
						'params' => []
					];
					break;
				case 'ttkk/kwitansi-kas-bayar-bbn': // Keuangan kwitansi cetak awal
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'Bayar BBN')",
							'params'    => []
						]
					];
					break;
			}
		}
		return [
			'td' => [
				'class'    => TdAction::className(),
				'model'    => Ttkk::className(),
				'colGrid'  => $colGrid,
				'joinWith' => $joinWith,
				'join'     => $join,
				'where'    => $where
			],
		];
	}
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
	public function actionPengajuanBayarBbnKas() {

		return $this->render( 'PengajuanBayarBBNKas' );
	}
    public function actionPengajuanBayarBbnKasCreate() {
//        return $this->create('Pengajuan Bayar BBN Kas', 'pengajuan-bayar-bbn-kas', 'Pengajuan Bayar BBN Via Kas');
        $title = 'Pengajuan Bayar BBN Via Kas';
        $model = new Ttkk();
        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'view', 'id' => $model->KMNo ] );
        }
        return $this->render( 'kas-keluar-create', [
            'model' => $model,
            'title' => $title
        ] );
    }

    /**
     * Lists all Ttkk models.
     * @return mixed
     */
    public function actionKasKeluarSubsidiDealer2()
    {



        return $this->render('KasKeluarSubsidiDealer2');
    }

            /**
             * @return string
             */
            public function actionKasKeluarSubsidiDealer2Create() {
                return $this->create('SubsidiDealer2', 'kas-keluar-subsidi-dealer-2', 'KAS KELUAR - Subsidi Dealer 2');
            }

            /**
             * @param $id
             * @param $action
             * @return string|\yii\web\Response
             * @throws NotFoundHttpException
             */
            public function actionKasKeluarSubsidiDealer2Update($id ) {
                return $this->update($id, 'kas-keluar-subsidi-dealer-2', 'KAS KELUAR - Subsidi Dealer 2');
            }

    /**
     * Lists all Ttkk models.
     * @return mixed
     */
    public function actionKasKeluarReturHarga()
    {



        return $this->render('KasKeluarReturHarga');
    }

            /**
             * @return string
             */
            public function actionKasKeluarReturHargaCreate() {
                return $this->create('Retur Harga', 'kas-keluar-retur-harga', 'KAS KELUAR - Retur Harga');
            }

            /**
             * @param $id
             * @param $action
             * @return string|\yii\web\Response
             * @throws NotFoundHttpException
             */
            public function actionKasKeluarReturHargaUpdate($id ) {
                return $this->update($id, 'kas-keluar-retur-harga', 'KAS KELUAR - Retur Harga');
            }

    /**
     * Lists all Ttkk models.
     * @return mixed
     */
    public function actionKasKeluarInsentifSales()
    {



        return $this->render('KasKeluarInsentifSales');
    }

            /**
             * @return string
             */
            public function actionKasKeluarInsentifSalesCreate() {
                return $this->create('Insentif Sales', 'kas-keluar-insentif-sales', 'KAS KELUAR - Insentif Sales');
            }

            /**
             * @param $id
             * @param $action
             * @return string|\yii\web\Response
             * @throws NotFoundHttpException
             */
            public function actionKasKeluarInsentifSalesUpdate($id ) {
                return $this->update($id, 'kas-keluar-insentif-sales', 'KAS KELUAR - Insentif Sales');
            }

    /**
     * Lists all Ttkk models.
     * @return mixed
     */
    public function actionKasKeluarPotonganKhusus()
    {



        return $this->render('KasKeluarPotonganKhusus');
    }

            /**
             * @return string
             */
            public function actionKasKeluarPotonganKhususCreate() {
                return $this->create('Potongan Khusus', 'kas-keluar-potongan-khusus', 'KAS KELUAR - Potongan Khusus');
            }

            /**
             * @param $id
             * @param $action
             * @return string|\yii\web\Response
             * @throws NotFoundHttpException
             */
            public function actionKasKeluarPotonganKhususUpdate($id ) {
                return $this->update($id, 'kas-keluar-potongan-khusus', 'KAS KELUAR - Potongan Khusus');
            }

    /**
     * Lists all Ttkk models.
     * @return mixed
     */
    public function actionKasKeluarBankUnit()
    {



        return $this->render('KasKeluarBankUnit');
    }

            /**
             * @return string
             */
            public function actionKasKeluarBankUnitCreate() {
                return $this->create('KK Ke Bank', 'kas-keluar-bank-unit', 'KAS KELUAR - Ke Bank Unit - H1');
            }

            /**
             * @param $id
             * @param $action
             * @return string|\yii\web\Response
             * @throws NotFoundHttpException
             */
            public function actionKasKeluarBankUnitUpdate($id ) {
                return $this->update($id, 'kas-keluar-potongan-khusus', 'KAS KELUAR - Ke Bank Unit - H1');
            }

    /**
     * Lists all Ttkk models.
     * @return mixed
     */
    public function actionKasKeluarUmum()
    {



        return $this->render('KasKeluarUmum');
    }

            /**
             * @return string
             */
            public function actionKasKeluarUmumCreate() {
                $model = new Ttkk();
                if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
                    return $this->redirect( [ 'view', 'id' => $model->KKNo ] );
                }
                return $this->render( 'kas-keluar-umum-create', [
                    'model' => $model,
                ] );
            }

            /**
             * @param $id
             * @param $action
             * @return string|\yii\web\Response
             * @throws NotFoundHttpException
             */
            public function actionKasKeluarUmumUpdate($id ) {
                return $this->update($id, 'kas-keluar-umum', 'KAS KELUAR - Umum');
            }

    /**
     * Lists all Ttkk models.
     * @return mixed
     */
    public function actionKasKeluarBankBengkel()
    {



        return $this->render('KasKeluarBankBengkel');
    }

            /**
             * @return string
             */
            public function actionKasKeluarBankBengkelCreate() {
                return $this->create('KK Ke Bengkel', 'kas-keluar-bank-bengkel', 'KAS KELUAR - Ke Bank Bengkel - H2');
            }

            /**
             * @param $id
             * @param $action
             * @return string|\yii\web\Response
             * @throws NotFoundHttpException
             */
            public function actionKasKeluarBankBengkelUpdate($id ) {
                return $this->update($id, 'kas-keluar-bank-bengkel', 'KAS KELUAR - Ke Bank Bengkel - H2');
            }

    /**
     * Lists all Ttkk models.
     * @return mixed
     */
    public function actionKasKeluarPinjamanKaryawan()
    {



        return $this->render('KasKeluarPinjamanKaryawan');
    }

            /**
             * @return string
             */
            public function actionKasKeluarPinjamanKaryawanCreate() {
                return $this->create('Pinjaman Karyawan', 'kas-keluar-pinjaman-karyawan', 'KAS KELUAR - Pinjaman Karyawan');
            }

            /**
             * @param $id
             * @param $action
             * @return string|\yii\web\Response
             * @throws NotFoundHttpException
             */
            public function actionKasKeluarPinjamanKaryawanUpdate($id ) {
                return $this->update($id, 'kas-keluar-pinjaman-karyawan', 'KAS KELUAR - Pinjaman Karyawan');
            }

    /**
     * Lists all Ttkk models.
     * @return mixed
     */
    public function actionKasKeluarDealerPos()
    {



        return $this->render('KasKeluarDealerPos');
    }

            /**
             * @return string
             */
            public function actionKasKeluarDealerPosCreate() {
                return $this->create('KK Dealer Ke Pos', 'kas-keluar-dealer-pos', 'KAS KELUAR - Dealer Ke Pos');
            }

            /**
             * @param $id
             * @param $action
             * @return string|\yii\web\Response
             * @throws NotFoundHttpException
             */
            public function actionKasKeluarDealerPosUpdate($id ) {
                return $this->update($id, 'kas-keluar-dealer-pos', 'KAS KELUAR - Dealer Ke Pos');
            }

    /**
     * Lists all Ttkk models.
     * @return mixed
     */
    public function actionKasKeluarBayarHutangUnit()
    {



        return $this->render('KasKeluarBayarHutangUnit');
    }

            /**
             * @return string
             */
            public function actionKasKeluarBayarHutangUnitCreate() {
                $title = 'KAS KELUAR - Bayar Hutang Unit';
                $model = new Ttkk();
                if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
                    return $this->redirect( [ 'view', 'id' => $model->KMNo ] );
                }
                return $this->render( 'kas-keluar-create', [
                    'model' => $model,
                    'title' => $title
                ] );
            }

            /**
             * @param $id
             * @param $action
             * @return string|\yii\web\Response
             * @throws NotFoundHttpException
             */
            public function actionKasKeluarBayarHutangUnitUpdate($id ) {
                return $this->update($id, 'kas-keluar-bayar-hutang-unit', 'KAS KELUAR - Bayar Hutang Unit');
            }

    /**
     * Lists all Ttkk models.
     * @return mixed
     */
    public function actionKasKeluarBayarHutangBbn()
    {



        return $this->render('KasKeluarBayarHutangBbn');
    }

            /**
             * @return string
             */
            public function actionKasKeluarBayarHutangBbnCreate() {
                $title = 'KAS KELUAR - Bayar Hutang BBN';
                $model = new Ttkk();
                if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
                    return $this->redirect( [ 'view', 'id' => $model->KMNo ] );
                }
                return $this->render( 'kas-keluar-create', [
                    'model' => $model,
                    'title' => $title
                ] );
            }

            /**
             * @param $id
             * @param $action
             * @return string|\yii\web\Response
             * @throws NotFoundHttpException
             */
            public function actionKasKeluarBayarHutangBbnUpdate($id ) {
                return $this->update($id, 'kas-keluar-bayar-hutang-bbn', 'KAS KELUAR - Bayar Hutang BBN');
            }

    /**
     * Lists all Ttkk models.
     * @return mixed
     */
    public function actionKasKeluarBayarBbnProgresif()
    {



        return $this->render('KasKeluarBayarBbnProgresif');
    }

            /**
             * @return string
             */
            public function actionKasKeluarBayarBbnProgresifCreate() {
                $title = 'KAS KELUAR - Bayar Hutang BBN Progresif';
                $model = new Ttkk();
                if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
                    return $this->redirect( [ 'view', 'id' => $model->KMNo ] );
                }
                return $this->render( 'kas-keluar-create', [
                    'model' => $model,
                    'title' => $title
                ] );
            }

            /**
             * @param $id
             * @param $action
             * @return string|\yii\web\Response
             * @throws NotFoundHttpException
             */
            public function actionKasKeluarBayarBbnProgresifUpdate($id ) {
                return $this->update($id, 'kas-keluar-bayar-bbn-progresif', 'KAS KELUAR - BBN Progresif');
            }

    /**
     * Lists all Ttkk models.
     * @return mixed
     */
    public function actionKasKeluarBayarBbnAcc()
    {



        return $this->render('KasKeluarBayarBbnAcc');
    }

            /**
             * @return string
             */
            public function actionKasKeluarBayarBbnAccCreate() {
                $title = 'KAS KELUAR - Bayar Hutang BBN Acc ';
                $model = new Ttkk();
                if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
                    return $this->redirect( [ 'view', 'id' => $model->KMNo ] );
                }
                return $this->render( 'kas-keluar-create', [
                    'model' => $model,
                    'title' => $title
                ] );
            }

            /**
             * @param $id
             * @param $action
             * @return string|\yii\web\Response
             * @throws NotFoundHttpException
             */
            public function actionKasKeluarBayarBbnAccUpdate($id ) {
                return $this->update($id, 'kas-keluar-bayar-bbn-acc', 'KAS KELUAR - Bayar BBN Acc');
            }

    /**
     * Lists all Ttkk models.
     * @return mixed
     */
    public function actionKasKeluarPosDealer()
    {



        return $this->render('KasKeluarPosDealer');
    }

            /**
             * @return string
             */
            public function actionKasKeluarPosDealerCreate() {
                return $this->create('KK Pos Ke Dealer', 'kas-keluar-pos-dealer', 'KAS KELUAR - Pos Ke Dealer');
            }

            /**
             * @param $id
             * @param $action
             * @return string|\yii\web\Response
             * @throws NotFoundHttpException
             */
            public function actionKasKeluarPosDealerUpdate($id ) {
                return $this->update($id, 'kas-keluar-pos-dealer', 'KAS KELUAR - Pos Ke Dealer');
            }


    /**
     * Lists all Ttkk models.
     * @return mixed
     */
    public function actionKasTransfer()
    {



        return $this->render('KasTransfer');
    }

            /**
             * @return string
             */
            public function actionKasTransferCreate() {
                $model            = new Ttkk();
                if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
                    return $this->redirect( [ 'kas-transfer' ] );
                }

                $model->KKNo      = Ttkk::generateKKNo( true );
                $model->KKTgl     = date( "Y-m-d" );
                $model->KKJam     = date( "Y-m-d H:i:s" );
                $model->KKJenis   = 'Kas Transfer';

                return $this->render( 'create', [
                    'model' => $model,
                    'view'  => 'kas-transfer',
                    'title'  => 'Kas Transfer',
                    'templateForm'  => '_kasTransferform'
                ] );
            }

            /**
             * @param $id
             * @param $action
             * @return string|\yii\web\Response
             * @throws NotFoundHttpException
             */
            public function actionKasTransferUpdate($id ) {
                $model = $this->findModelBase64( 'Ttkk', $id );
                if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
                    return $this->redirect( [ 'kas-transfer' ] );
                }
                return $this->render( 'update', [
                    'model' => $model,
                    'id'    => $id,
                    'view'  => 'kas-transfer',
                    'title'  => 'Kas Transfer',
                    'templateForm'  => '_kasTransferform'
                ] );
            }


	/**
	 * Lists all Ttkk models.
	 * @return mixed
	 */
	public function actionKwitansiKasBayarBbn()
	{



		return $this->render('KwitansiKasBayarBbn');
	}
    /**
     * Displays a single Ttkk model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @param $KKJenis
     * @param $view
     * @param $title
     * @return string
     */
    private function create($KKJenis, $view, $title) {
        $model = new Ttkk();
        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ $view ] );
        }

        $model->KKNo      = Ttkk::generateKKNo( true );
        $model->KKTgl     = date( "Y-m-d" );
        $model->KKJam     = date( "Y-m-d H:i:s" );
        $model->KKJenis   = $KKJenis;

        return $this->render( 'create', [
            'model' => $model,
            'view'  => $view,
            'title'  => $title
        ] );
    }

    /**
     * @param $id
     * @param $view
     * @param $title
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    private function update($id, $view, $title) {
        $model = $this->findModelBase64( 'Ttkk', $id );
        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ $view ] );
        }
        return $this->render( 'update', [
            'model' => $model,
            'id'    => $id,
            'view'  => $view,
            'title'  => $title
        ] );
    }

    /**
     * Deletes an existing Ttkk model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ttkk model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Ttkk the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ttkk::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
