<?php
namespace afinance\controllers;
use afinance\components\AfinanceController;
use afinance\components\TdAction;
use afinance\models\Tddealer;

use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use common\components\Custom;
/**
 * TddealerController implements the CRUD actions for Tddealer model.
 */
class TddealerController extends AfinanceController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actions() {
		return [
			'td' => [
				'class' => TdAction::className(),
				'model' => Tddealer::className(),
			],
		];
	}
	/**
	 * Lists all Tddealer models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index');
	}
	/**
	 * Displays a single Tddealer model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Creates a new Tddealer model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Tddealer();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'index','id'=> $this->generateIdBase64FromModel($model)  ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Tddealer model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$model = $this->findModelBase64( 'Tddealer', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'index','id'=> $this->generateIdBase64FromModel($model)  ] );
		}
		return $this->render( 'update', [
			'model' => $model,
            'id'    => $this->generateIdBase64FromModel($model),
		] );
	}
	/**
	 * Deletes an existing Tddealer model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Tddealer model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Tddealer the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Tddealer::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
