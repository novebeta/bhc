<?php
namespace afinance\controllers;
use afinance\components\AfinanceController;
use afinance\components\TdAction;
use afinance\models\Tmotor;
use afinance\models\Ttsd;

use afinance\models\Tvmotorlokasi;
use common\components\Custom;
use common\components\General;
use Yii;
use yii\base\UserException;
use yii\db\Exception;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
/**
 * TtsdController implements the CRUD actions for Ttsd model.
 */
class TtsdController extends AfinanceController {
	public function actions() {
		$colGrid  = null;
		$joinWith = [
			'supplier' => [
				'type' => 'LEFT JOIN'
			],
			'lokasi'   => [
				'type' => 'LEFT JOIN'
			]
		];
		$join     = [];
		$where    = [];
		if ( isset( $_POST[ 'query' ] ) ) {
			$r = Json::decode( $_POST[ 'query' ], true )[ 'r' ];
			switch ( $r ) {
				case 'ttsd/mutasi-eksternal-dealer':
					$colGrid  = Ttsd::colGridMutasiEksternalDealer();
					$joinWith = [
						'lokasi' => [
							'type' => 'LEFT JOIN'
						],
						'dealer' => [
							'type' => 'LEFT JOIN'
						],
					];
					$where    = [
						[
							'op'        => 'AND',
							'condition' => "LEFT(SDNo,2) = 'ME'",
							'params'    => []
						]
					];
					break;
				case 'ttsd/invoice-eksternal':
					$colGrid  = Ttsd::colGridInvoiceEksternal();
					$joinWith = [
						'lokasi' => [
							'type' => 'INNER JOIN'
						],
						'dealer' => [
							'type' => 'INNER JOIN'
						],
					];
					$where    = [
						[
							'op'        => 'AND',
							'condition' => "LEFT(SDNo,2) = 'ME'",
							'params'    => []
						]
					];
					break;
				case 'ttsd/retur-beli' || 'ttsd/invoice-retur-beli':
					$colGrid   = Ttsd::colGridReturBeli();
					$joinWith  = [
						'lokasi'   => [
							'type' => 'LEFT JOIN'
						],
						'supplier' => [
							'type' => 'LEFT JOIN'
						],
					];
					$condition = "LEFT(SDNo,2) = 'SR'";
					if ( $r == 'ttsd/invoice-retur-beli' ) {
						$condition .= " AND INSTR(`SDNo`, '=') = 0";
					}
					$where = [
						[
							'op'        => 'AND',
							'condition' => $condition,
							'params'    => []
						]
					];
					break;
			}
		}
		return [
			'td' => [
				'class'    => TdAction::className(),
				'model'    => Ttsd::className(),
				'colGrid'  => $colGrid,
				'joinWith' => $joinWith,
				'where'    => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttsd models.
	 * @return mixed
	 */
	public function actionReturBeliCreate() {
		$model             = new Ttsd();
		$model->SDNo       = General::createTemporaryNo( "SR", 'ttsd', 'SDNo' );
		$model->SDTgl      = date( 'Y-m-d' );
		$model->SDMemo     = '--';
		$model->SDTotal    = 0;
		$model->LokasiKode = $this->LokasiKode();
		$model->UserID     = $this->UserID();
		$model->save();
		$id     = $this->generateIdBase64FromModel( $model );
		$dsBeli = Ttsd::find()->dsTMutasiReturBeli()->where( [ 'ttsd.SDNo' => $model->SDNo ] )->asArray( true )->one();
		return $this->render( 'create', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'jenis'  => "SuratReturBeli",
			'id'     => $id
		] );
	}
	/**
	 * Lists all Ttsd models.
	 *
	 * @param $id
	 * @param $action
	 *
	 * @return mixed
	 * @throws Exception
	 * @throws NotFoundHttpException
	 */
	public function actionReturBeliUpdate( $id, $action ) {
		/** @var Ttsd $model */
		$model = $this->findModelBase64( 'Ttsd', $id );
		if ( $model == null ) {
			$model = new Ttsd;
		}
		$dsBeli = Ttsd::find()->dsTMutasiReturBeli()->where( [ 'ttsd.SDNo' => $model->SDNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$transaction = Ttsd::getDb()->beginTransaction();
			try {
				$this->prosesUpdateReturBeli( $model, $action );
				$transaction->commit();
				return $this->redirect( [ 'ttsd/retur-beli', 'id' => $this->generateIdBase64FromModel( $model ) ] );
			} catch ( UserException $e ) {
				$transaction->rollBack();
				Yii::$app->session->setFlash( 'error', $e->getMessage() );
				return $this->render( 'update', [
					'model'  => $model,
					'dsBeli' => $dsBeli,
					'jenis'  => "SuratReturBeli",
					'id'     => $id
				] );
			}
		}
		return $this->render( 'update', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'jenis'  => "SuratReturBeli",
			'id'     => $id
		] );
	}
	/**
	 * @param Ttsd $model
	 * @param $action
	 *
	 * @throws Exception
	 */
	protected function prosesUpdateReturBeli( $model, $action ) {
		$post          = Yii::$app->request->post();
		$model->UserID = $this->UserID();
		unset( $post[ 'UserID' ] );
		$SubTotal = 0;
		$detil    = Json::decode( $post[ 'detil' ] );
		foreach ( $detil as $row ) {
			$SubTotal += floatval( $row[ 'FBHarga' ] );
			Yii::$app->db
				->createCommand( "Update TMotor SET SDNo = :SDNo WHERE MotorNoMesin = :MotorNoMesin 
                                 AND MotorAutoN = :MotorAutoN", [
					':SDNo'         => $model->SDNo,
					':MotorNoMesin' => $row[ 'MotorNoMesin' ],
					':MotorAutoN'   => $row[ 'MotorAutoN' ],
				] )->execute();
		}
		$post[ 'SDTotal' ] = $SubTotal;
		if ( $action != 'create' ) {
			Tvmotorlokasi::deleteAll( [ 'NoTrans' => $model->SDNo ] );
		}
		$model->load( $post, '' );
		$model->DealerKode = $post[ 'SupKode' ];
		if ( strpos( $model->SDNo, '=' ) !== false ) {
			$model->SDNo = General::createNo( "SR", 'ttsd', 'SDNo' );
			Tmotor::updateAll( [ 'SDNo' => $model->SDNo ], [ 'SDNo' => $model->SDNo ] );
		}
		$model->save();
		$model->refresh();
		Yii::$app->db
			->createCommand( "INSERT INTO tvmotorlokasi (MotorAutoN, MotorNoMesin, LokasiKode, NoTrans, Jam, Kondisi)
						SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttSD.LokasiKode  AS LokasiKode, ttSD.SDNo AS NoTrans, ttSD.SDJam AS Jam, 'OUT' AS Kondisi
						FROM tmotor INNER JOIN ttSD ON tmotor.SDNo = ttSD.SDNo WHERE ttSD.SDNo = :SDNo", [
				':SDNo' => $model->SDNo
			] )->execute();
	}
	/**
	 * Lists all Ttsd models.
	 *
	 * @param $id
	 * @param $action
	 *
	 * @return mixed
	 * @throws Exception
	 * @throws NotFoundHttpException
	 */
	public function actionInvoiceReturBeliUpdate( $id, $action ) {
		/** @var Ttsd $model */
		$model = $this->findModelBase64( 'Ttsd', $id );
		if ( $model == null ) {
			$model = new Ttsd;
		}
		$dsBeli = Ttsd::find()->dsTMutasiReturBeli()->where( [ 'ttsd.SDNo' => $model->SDNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$transaction = Ttsd::getDb()->beginTransaction();
			try {
				$this->prosesUpdateReturBeli( $model, $action );
				$transaction->commit();
				return $this->redirect( [ 'ttsd/invoice-retur-beli', 'id' => $this->generateIdBase64FromModel( $model ) ] );
			} catch ( UserException $e ) {
				$transaction->rollBack();
				Yii::$app->session->setFlash( 'error', $e->getMessage() );
				return $this->render( 'update', [
					'model'  => $model,
					'dsBeli' => $dsBeli,
					'jenis'  => "InvoiceReturBeli",
					'id'     => $id
				] );
			}
		}
		return $this->render( 'update', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'jenis'  => "InvoiceReturBeli",
			'id'     => $id
		] );
	}
	/**
	 * Lists all Ttsd models.
	 * @return mixed
	 */
	public function actionReturBeli() {
		return $this->render( 'retur-beli' );
	}
	/**
	 * Lists all Ttsd models.
	 * @return mixed
	 */
	public function actionInvoiceReturBeli() {
		return $this->render( 'invoice-retur-beli' );
	}
	/**
	 * Lists all Ttsd models.
	 * @return mixed
	 */
	public function actionMutasiEksternalDealer() {
		return $this->render( 'MutasiEksternalDealer' );
	}
	/**
	 * Lists all Ttsd models.
	 * @return mixed
	 */
	public function actionInvoiceEksternal() {

		return $this->render( 'InvoiceEksternal' );
	}
	/**
	 * Lists all Ttsd models.
	 * @return mixed
	 */
	public function actionPenerimaanDealer() {

		return $this->render( 'invoice-retur-beli' );
	}
	/**
	 * Lists all Ttsd models.
	 * @return mixed
	 */
	public function actionInvoicePenerimaan() {

		return $this->render( 'invoice-retur-beli' );
	}
	/**
	 * Displays a single Ttsd model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Finds the Ttsd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttsd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttsd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttsd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Ttsd();
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Ttsd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$model = $this->findModel( $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'id' => $model->SDNo ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Ttsd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
	public function actionMutasiEksternalDealerCreate() {
		$model             = new Ttsd();
		$model->SDNo       = General::createTemporaryNo( "ME", 'ttsd', 'SDNo' );
		$model->SDTgl      = date( 'Y-m-d' );
		$model->SDMemo     = '--';
		$model->SDTotal    = 0;
		$model->LokasiKode = $this->LokasiKode();
		$model->UserID     = $this->UserID();
		$model->save();
		$id     = $this->generateIdBase64FromModel( $model );
		$dsBeli = Ttsd::find()->dsTMutasiReturBeli()->where( [ 'ttsd.SDNo' => $model->SDNo ] )->asArray( true )->one();
		return $this->render( 'mutasi-eksternal-dealer-create', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'jenis'  => "MutasiEksternal",
			'id'     => $id
		] );
	}
	public function actionMutasiEksternalDealerUpdate( $id, $action ) {
		/** @var Ttsd $model */
		$model = $this->findModelBase64( 'Ttsd', $id );
		if ( $model == null ) {
			$model = new Ttsd;
		}
		$dsBeli = Ttsd::find()->dsTMutasiReturBeli()->where( [ 'ttsd.SDNo' => $model->SDNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$transaction = Ttsd::getDb()->beginTransaction();
			try {
				$this->prosesUpdateReturBeli( $model, $action );
				$transaction->commit();
				return $this->redirect( [ 'ttsd/retur-beli', 'id' => $this->generateIdBase64FromModel( $model ) ] );
			} catch ( UserException $e ) {
				$transaction->rollBack();
				Yii::$app->session->setFlash( 'error', $e->getMessage() );
				return $this->render( 'mutasi-eksternal-dealer-update', [
					'model'  => $model,
					'dsBeli' => $dsBeli,
					'jenis'  => "MutasiEksternal",
					'id'     => $id
				] );
			}
		}
		return $this->render( 'mutasi-eksternal-dealer-update', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'jenis'  => "MutasiEksternal",
			'id'     => $id
		] );
	}
	public function actionInvoiceEksternalDealerUpdate( $id, $action ) {
		/** @var Ttsd $model */
		$model = $this->findModelBase64( 'Ttsd', $id );
		if ( $model == null ) {
			$model = new Ttsd;
		}
		$dsBeli = Ttsd::find()->dsTMutasiReturBeli()->where( [ 'ttsd.SDNo' => $model->SDNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$transaction = Ttsd::getDb()->beginTransaction();
			try {
				$this->prosesUpdateReturBeli( $model, $action );
				$transaction->commit();
				return $this->redirect( [ 'ttsd/retur-beli', 'id' => $this->generateIdBase64FromModel( $model ) ] );
			} catch ( UserException $e ) {
				$transaction->rollBack();
				Yii::$app->session->setFlash( 'error', $e->getMessage() );
				return $this->render( 'invoice-eksternal-dealer-update', [
					'model'  => $model,
					'dsBeli' => $dsBeli,
					'jenis'  => "InvoiceEksternal",
					'id'     => $id
				] );
			}
		}
		return $this->render( 'invoice-eksternal-dealer-update', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'jenis'  => "InvoiceEksternal",
			'id'     => $id
		] );
	}
	public function actionInvoiceReturBeliCreate() {
		$model = new Ttsd();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'id' => $model->SDNo ] );
		}
		return $this->render( 'invoice-retur-beli-update', [
			'model' => $model,
		] );
	}
	/**
	 * @return array
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDetail() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Tmotor::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper  = $requestData[ 'oper' ];
			$model = $this->findModelBase64( 'tmotor', $requestData[ 'id' ] );
			switch ( $oper ) {
				case 'add'  :
					$model->attributes = $requestData;
					$response          = $model->save() ?
						$this->responseSuccess( 'Data berhasil disimpan.' ) :
						$this->responseFailed( $this->modelErrorSummary( $model ) );
					break;
				case 'edit' :
					$model->attributes = $requestData;
//					if ( $oper === 'add' ) {
//						if ( $requestData[ 'SSNo' ] == '--' ) {
//							$count = ( new \yii\db\Query() )
//								->from( Tmotor::tableName() )
//								->where( [ 'FBNo' => $requestData[ 'FBNo' ] ] )
//								->max( 'MotorAutoN' );
//							if ( ! $count ) {
//								$count = 0;
//							}
//							$model->MotorAutoN = ++ $count;
//						} else {
//							$model->MotorAutoN = 0;
//						}
//					}
					$response = $model->save() ?
						$this->responseSuccess( 'Data berhasil disimpan.' ) :
						$this->responseFailed( $this->modelErrorSummary( $model ) );
					break;
				case 'del'  :
					$response = $model->delete() ?
						$this->responseSuccess( 'Data berhasil dihapus.' ) :
						$this->responseFailed( $this->modelErrorSummary( $model ) );
					break;
			}
		} else {
//			/* operation : read */
//			$query = ( new \yii\db\Query() )
//				->select( [
//					'CONCAT(' . implode( ",'||',", $primaryKeys ) . ') AS id',
//					'tmotor.*'
//				] )
//				->from( Tmotor::tableName() )
//				->where( [
//					'FBNo' => $requestData[ 'FBNo' ]
//				] )
//				->orderBy( 'MotorAutoN' );
			$query2 = ( new \yii\db\Query() )
				->select( new Expression(
					'CONCAT(' . implode( ',"||",', [ 'tmotor.MotorAutoN', 'tmotor.MotorNoMesin' ] ) . ') AS id,
								tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, 
								tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.FBHarga, tdmotortype.MotorNama'
				) )
				->from( 'tmotor' )
				->join( 'LEFT OUTER JOIN', 'tdmotortype', 'tmotor.MotorType = tdmotortype.MotorType' )
				->join( 'INNER JOIN', 'ttsd', 'tmotor.SDNo = ttsd.SDNo' );
			if ( isset( $_GET[ 'jenis' ] ) ) {
				switch ( $_GET[ 'jenis' ] ) {
					case 'SuratReturBeli':
					case 'InvoiceReturBeli':
					case 'MutasiEksternal':
						$query2->where( [
							'ttsd.SDNo' => $requestData[ 'SDNo' ],
						] );
						$query = $query2;
						break;
					case 'FillMotorGridAddEdit':
						$query1 = ( new \yii\db\Query() )
							->select( new Expression(
								'CONCAT(' . implode( ',"||",', [ 'tmotor.MotorAutoN', 'tmotor.MotorNoMesin' ] ) . ') AS id,
								tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, 
								tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.FBHarga, tdmotortype.MotorNama'
							) )
							->from( 'tmotor' )
							->join( 'INNER JOIN', 'vmotorLastlokasi', '(tmotor.MotorNoMesin = vmotorLastlokasi.MotorNoMesin AND tmotor.MotorAutoN = vmotorLastlokasi.MotorAutoN)' )
							->join( 'LEFT OUTER JOIN', 'tdmotortype', 'tmotor.MotorType = tdmotortype.MotorType' )
							->join( 'LEFT OUTER JOIN', 'ttdk', 'tmotor.DKNo = ttdk.DKNo' )
							->join( 'LEFT OUTER JOIN', 'ttsd', 'tmotor.SDNo = ttsd.SDNo' )
							->where( "(ttdk.DKNo IS NULL AND ttsd.SDNo IS NULL)
							AND (vmotorLastlokasi.LokasiKode = :LokasiKode
							AND vmotorLastlokasi.Kondisi = 'IN'
							AND vmotorLastlokasi.LokasiKode <> 'Repair'
							AND vmotorLastlokasi.Jam <= :Jam)", [
								':LokasiKode' => $requestData[ 'LokasiKode' ],
								':Jam'        => $requestData[ 'Jam' ] . ' ' . date( 'H:i' ) ] );
						$query2->where( [
							'ttsd.SDNo' => $requestData[ 'SDNo' ],
						] );
//						if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'update' ) {
//							$query = $query2;
//						} else {
//							$query = $query1->union( $query2, true );
//						}
						$query = $query1->union( $query2, true );
						break;
				}
			}
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
}
