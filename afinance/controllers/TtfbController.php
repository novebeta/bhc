<?php
namespace afinance\controllers;
use afinance\components\AfinanceController;
use afinance\components\TdAction;
use afinance\models\Tmotor;
use afinance\models\Ttfb;
use afinance\models\Ttss;
use common\components\Custom;
use common\components\General;
use Yii;
use yii\base\UserException;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtfbController implements the CRUD actions for Ttfb model.
 */
class TtfbController extends AfinanceController {
	public function actions() {
		$queryRaw   = null;
		$queryRawPK = null;
		if ( isset( $_POST[ 'query' ] ) ) {
			$urlParams = Json::decode( $_POST[ 'query' ], true )[ 'urlParams' ];
			if ( isset( $urlParams[ 'jenis' ] ) ) {
				switch ( $urlParams[ 'jenis' ] ) {
					case 'SS':
						$queryRawPK = [ "FBNo" ];
						$queryRaw   = "SELECT ttfb.FBNo, ttfb.FBTgl, ttfb.SSNo, ttfb.SupKode, ttfb.FBTermin, ttfb.FBTglTempo, ttfb.FBPSS,
					ttfb.FBPtgLain, ttfb.FBTotal, ttfb.FBMemo, ttfb.FBLunas, ttfb.UserID, 
					IFNULL(tdsupplier.SupNama, '--') AS SupNama, IFNULL(ttss.SSTgl, DATE('1900-01-01')) AS SSTgl,
					IFNULL(tmotor.FBJum, 0) AS FBJum, IFNULL(tmotor.FBSubTotal, 0) AS FBSubTotal 
					FROM ttfb
					LEFT OUTER JOIN ttss ON ttfb.SSNo = ttss.SSNo
					LEFT OUTER JOIN tdsupplier ON ttfb.SupKode = tdsupplier.SupKode
					LEFT OUTER JOIN 
						(SELECT COUNT(MotorType) AS FBJum, SUM(FBHarga) AS FBSubTotal, FBNo FROM tmotor tmotor_1 Where SSNo = '--' GROUP BY FBNo ) tmotor
					ON ttfb.FBNo = tmotor.FBNo";
						break;
				}
			}
		}
		return [
			'td' => [
				'class'      => TdAction::className(),
				'model'      => Ttfb::className(),
				'queryRaw'   => $queryRaw,
				'queryRawPK' => $queryRawPK,
				'joinWith'   => [
					'suratSupp' => [
						'type' => 'LEFT OUTER JOIN'
					],
					'supplier'  => [
						'type' => 'LEFT OUTER JOIN'
					],
				],
				'join'       => [
					[
						'type'   => 'LEFT OUTER JOIN',
						'table'  => '(SELECT COUNT(MotorType) AS FBJum, SUM(FBHarga) AS FBSubTotal, FBNo FROM tmotor tmotor_1 GROUP BY FBNo) tmotor',
						'on'     => 'ttfb.FBNo = tmotor.FBNo',
						'params' => []
					]
				]
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttfb models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index', [
			'mode' => '',
			'url_add'=>Url::toRoute( [ 'ttfb/create' ] )
		] );
	}
	/**
	 * Lists all Ttfb models.
	 * @return mixed
	 */
	public function actionSelect() {
		$this->layout = 'select-mode';
		return $this->render( 'index', [
			'mode' => self::MODE_SELECT_DETAIL_MULTISELECT,
			'url_add'=>Url::toRoute( [ 'ttfb/td', 'jenis' => $_GET['jenis'] ] )
		] );
	}
	/**
	 * Displays a single Ttfb model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Finds the Ttfb model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttfb the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttfb::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttfb model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model        = new Ttfb();
		$id           = $this->generateIdBase64FromModel( $model );
		$model->FBNo  = General::createTemporaryNo("FB",'ttfb','FBNo');
		$model->FBTgl = date( 'Y-m-d' );
		$model->FBJam = date( 'Y-m-d H:i:s' );
		$model->DOTgl = date( 'Y-m-d' );
		$model->SSNo = '--';
		$model->FBMemo = '--';
		$model->FBTermin = 0;
		$model->FBPSS = 0;
		$model->FBPtgLain = 0;
		$model->FBTotal = 0;
		$model->FBExtraDisc = 0;
		$model->FBPtgMD = 0;
		$model->FBLunas = "Belum";
		$model->UserID = $this->UserID();
		$model->save();
		$dsBeli            = Ttfb::find()->dsTBeliFillByNo()->where( [ 'ttfb.FBNo' => $model->FBNo ] )->asArray( true )->one();
		$dsBeli['FBJum'] = 0;
		$dsBeli['FBSubTotal'] = 0;
		$dsBeli['FBPtgTotal'] = 0;
		$dsBeli['FPTgl'] = "1900-01-01";
		$dsBeli['FPNo'] = "--";
		return $this->render( 'create', [
			'model' => $model,
			'dsBeli' => $dsBeli,
			'id'    => $id
		] );
	}
	/**
	 * Updates an existing Ttfb model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 * @param string $action
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id, $action ) {
		/** @var Ttfb $model */
		$model = $this->findModelBase64( 'Ttfb', $id );
		if ( $model == null ) {
			$model = new Ttfb;
		}
		$dsBeli = Ttfb::find()->dsTBeliFillByNo()->where( [  'ttfb.FBNo' => $model->FBNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			if ( Yii::$app->request->isAjax ) {
				\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			}
			$transaction = Ttfb::getDb()->beginTransaction();
			$post        = Yii::$app->request->post();
			try {
				$detil = Json::decode( $_POST[ 'detil' ] );
			} catch ( UserException $e ) {
				$transaction->rollBack();
				if ( Yii::$app->request->isAjax ) {
					return [ 'success' => false, 'msg' => $e->getMessage() ];
				} else {
					Yii::$app->session->setFlash( 'error', $e->getMessage() );
					return $this->render( 'update', [
						'model'  => $model,
						'dsBeli' => $post,
						'id'     => $id
					] );
				}
			}
		}
		return $this->render( 'update', [
			'model' => $model,
			'dsBeli' => $dsBeli,
			'id'    => $id
		] );
	}
	/**
	 * Deletes an existing Ttfb model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * @return array
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDetail() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Tmotor::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper  = $requestData[ 'oper' ];
			$model = $oper === 'add' ? new Tmotor() :
				$this->findModelBase64( 'tmotor', $requestData[ 'id' ] );
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					$model->attributes = $requestData;
					if ( $oper === 'add' ) {
						$count = ( new \yii\db\Query() )
							->from( Tmotor::tableName() )
							->where( [ 'FBNo' => $requestData[ 'FBNo' ] ] )
							->max( 'MotorAutoN' );
						if ( ! $count ) {
							$count = 0;
						}
						$model->MotorAutoN = ++ $count;
					}
					$response = $model->save() ?
						$this->responseSuccess( 'Data berhasil disimpan.' ) :
						$this->responseFailed( $this->modelErrorSummary( $model ) );
					break;
				case 'del'  :
					$response = $model->delete() ?
						$this->responseSuccess( 'Data berhasil dihapus.' ) :
						$this->responseFailed( $this->modelErrorSummary( $model ) );
					break;
			}
		} else {
			/* operation : read */
			$query = ( new \yii\db\Query() )
				->select( [
					'CONCAT(' . implode( ",'||',", $primaryKeys ) . ') AS id',
					'tmotor.*'
				] )
				->from( Tmotor::tableName() )
				->where( [
					'FBNo' => $requestData[ 'FBNo' ]
				] )
				->orderBy( 'MotorAutoN' );
			if ( isset( $_GET[ 'jenis' ] ) ) {
				switch ($_GET[ 'jenis' ]){
					case 'SS':
						$query = ( new \yii\db\Query() )
							->select(new Expression(
								'CONCAT(' . implode( ',"||",', $primaryKeys ). ') AS id,
								BPKBNo, BPKBTglJadi, DKNo, FBHarga, FBNo, FakturAHMNo, FakturAHMTgl,
								MotorNoMesin, MotorNoRangka, MotorTahun, MotorType, MotorWarna, PDNo, PlatNo,
								PlatTgl, SDNo, SKNo, SSNo, STNKAlamat, STNKNama, STNKNo, STNKTglBayar, STNKTglJadi, STNKTglMasuk, MotorAutoN'
							))
							->from('tmotor')
							->where( [
								'FBNo' => $requestData[ 'FBNo' ],
								'SSNo'=> '--'
							] )
							->orderBy( 'SSNo DESC, MotorType, MotorTahun, MotorWarna' );
						break;
				}
			}
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	/**
	 * @param $id
	 * @param $action
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionCancel( $id, $action ) {
		/** @var Ttfb $model */
		$model = $this->findModelBase64( 'Ttfb', $id );
		if ( $action == 'create' && $model != null && strpos( $model->FBNo, '=' ) !== false ) {
			/* delete data detail  */
			Tmotor::deleteAll( "FBNo = :FBNo AND  (SSNo == '--' OR SSNo == '' OR SSNo IS NULL)", [
				':FBNo' => $model->FBNo
			] );
			Tmotor::updateAll( ['FBNo'=>'--'],"FBNo = :FBNo AND (SSNo <> '--' OR SSNo <> '')", [
				':FBNo' => $model->FBNo
			] );
			/* delete data header */
			$model->delete();
		}
		if ( Yii::$app->request->isAjax ) {
			return '';
		}
		return $this->redirect( [ 'index' ] );
	}
}
