<?php

namespace afinance\controllers;

use afinance\components\TdAction;
use Yii;
use afinance\models\Ttpbhd;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TtpbhdController implements the CRUD actions for Ttpbhd model.
 */
class TtpbhdController extends Controller
{
	public function actions() {
		$colGrid  = null;
		$joinWith = [
			'suratJalan'      => [
				'type' => 'LEFT OUTER JOIN'
			],
			'rLokasiAsal lasal'      => [
				'type' => 'LEFT OUTER JOIN'
			],
			'rLokasiTujuan ltujuan' => [
				'type' => 'LEFT OUTER JOIN'
			]
		];
		$join     = [];
		$where    = [];
//		if ( isset( $_POST['query'] ) ) {
//			$r = Json::decode( $_POST['query'], true )['r'];
//			switch ( $r ) {
//				case 'ttsmhd/mutasi-internal-pos':
//					$colGrid = Ttsmhd::colGridMutasiInternalPos();
//					$where = [
//						[
//							'op'        => 'AND',
//							'condition' => "LEFT(ttsmhd.SMNo,2) = 'MI'",
//							'params'    => []
//						]
//					];
//					break;
//				case 'ttsmhd/mutasi-repair-in':
//					$colGrid = Ttsmhd::colGridMutasiRepairIn();
//					$where = [
//						[
//							'op'        => 'AND',
//							'condition' => "LEFT(ttsmhd.SMNo,2) = 'RI'",
//							'params'    => []
//						]
//					];
//					break;
//				case 'ttsmhd/mutasi-repair-out':
//					$colGrid = Ttsmhd::colGridMutasiRepairOut();
//					$where = [
//						[
//							'op'        => 'AND',
//							'condition' => "LEFT(ttsmhd.SMNo,2) = 'RO'",
//							'params'    => []
//						]
//					];
//					break;
//			}
//		}
		return [
			'td' => [
				'class'    => TdAction::className(),
				'model'    => Ttpbhd::className(),
//				'colGrid'  => $colGrid,
				'joinWith' => $joinWith,
				'join'     => $join,
				'where'    => $where,
			],
		];
	}
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ttpbhd models.
     * @return mixed
     */
    public function actionIndex()
    {



        return $this->render('index');
    }

    /**
     * Displays a single Ttpbhd model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ttpbhd model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ttpbhd();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->PBNo]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ttpbhd model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->PBNo]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ttpbhd model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ttpbhd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Ttpbhd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ttpbhd::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
