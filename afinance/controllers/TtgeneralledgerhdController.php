<?php
namespace afinance\controllers;
use afinance\components\AfinanceController;
use afinance\components\TdAction;
use afinance\models\Ttgeneralledgerhd;

use afinance\models\Ttgeneralledgerit;
use Yii;
use yii\db\Exception;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
/**
 * TtgeneralledgerhdController implements the CRUD actions for Ttgeneralledgerhd model.
 */
class TtgeneralledgerhdController extends AfinanceController {
	public function actions() {
		$where = [];
		if ( isset( $_POST['query'] ) ) {
			$r = Json::decode( $_POST['query'], true )['r'];
			switch ( $r ) {
				case 'ttgeneralledgerhd/memorial':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "KodeTrans = 'GL'",
							'params'    => []
						]
					];
					break;
				case 'ttgeneralledgerhd/transaksi':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "KodeTrans <> 'GL' AND KodeTrans <> 'JA' AND KodeTrans <> 'JP'",
							'params'    => []
						]
					];
					break;
				case 'ttgeneralledgerhd/adjustment':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "KodeTrans = 'JA'",
							'params'    => []
						]
					];
					break;
				case 'ttgeneralledgerhd/pindahbuku':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "KodeTrans = 'JP'",
							'params'    => []
						]
					];
					break;
				default:
//					$where = [
//						[
//							'op'        => 'AND',
//							'condition' => "KodeTrans <> 'GL' AND KodeTrans <> 'JA' AND KodeTrans <> 'JP'",
//							'params'    => []
//						]
//					];
//					break;
			}
		}
		return [
			'td' => [
				'class' => TdAction::className(),
				'model' => Ttgeneralledgerhd::className(),
				'where' => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttgeneralledgerhd models.
	 * @return mixed
	 */
	public function actionIndex() {

		return $this->render( 'index' );
	}
	/**
	 * Lists all Ttgeneralledgerhd models.
	 * @return mixed
	 */
	public function actionMemorial() {

		return $this->render( 'memorial' );
	}
	/**
	 * @return string
	 */
	public function actionMemorialCreate() {
		return $this->create( 'GL', 'memorial', 'Jurnal Memorial' );
	}
	/**
	 * @param $id
	 * @param $action
	 *
	 * @return string|\yii\web\Response
	 * @throws NotFoundHttpException
	 */
	public function actionMemorialUpdate( $id, $action ) {
		return $this->update( $id, $action, 'GL', 'memorial', 'Jurnal Memorial' );
	}
	/**
	 * @param $id
	 * @param $action
	 *
	 * @return \yii\web\Response
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionMemorialCancel( $id, $action ) {
		return $this->cancel( $id, $action, 'memorial' );
	}
	/**
	 * Lists all Ttgeneralledgerhd models.
	 * @return mixed
	 */
	public function actionTransaksi() {

		return $this->render( 'transaksi' );
	}
	/**
	 * @return string
	 */
	public function actionTransaksiCreate() {
		return $this->create( 'JT', 'transaksi', 'Jurnal Transaksi' );
	}
	/**
	 * @return string
	 */
	public function actionValidasiJurnal() {
		if ( isset( $_POST['mode'] ) ) {
			Ttgeneralledgerhd::updateAll( [
				'GLValid' => $_POST['mode']
			], "TglGL BETWEEN :dtpTgl1 AND :dtpTgl2", [
				':dtpTgl1' => $_POST[':dtpTgl1'],
				':dtpTgl2' => $_POST[':dtpTgl2'],
			] );
		}
		$last = Ttgeneralledgerhd::find()->GetLastValidasi();
		return $this->render( 'validasi-jurnal', [
			'last' => $last
		] );
	}
	/**
	 * @param $id
	 * @param $action
	 *
	 * @return string|\yii\web\Response
	 * @throws NotFoundHttpException
	 */
	public function actionTransaksiUpdate( $id, $action ) {
		return $this->update( $id, $action, 'JT', 'transaksi', 'Jurnal Transaksi' );
	}
	/**
	 * @param $id
	 * @param $action
	 *
	 * @return \yii\web\Response
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionTransaksiCancel( $id, $action ) {
		return $this->cancel( $id, $action, 'transaksi' );
	}
	/**
	 * Lists all Ttgeneralledgerhd models.
	 * @return mixed
	 */
	public function actionAdjustment() {

		return $this->render( 'adjustment' );
	}
	/**
	 * @return string
	 */
	public function actionAdjustmentCreate() {
		return $this->create( 'JA', 'adjustment', 'Jurnal Adjustment' );
	}
	/**
	 * @param $id
	 * @param $action
	 *
	 * @return string|\yii\web\Response
	 * @throws NotFoundHttpException
	 */
	public function actionAdjustmentUpdate( $id, $action ) {
		return $this->update( $id, $action,'JA', 'adjustment', 'Jurnal Adjustment' );
	}
	/**
	 * @param $id
	 * @param $action
	 *
	 * @return \yii\web\Response
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionAdjustmentCancel( $id, $action ) {
		return $this->cancel( $id, $action, 'adjustment' );
	}
	/**
	 * Lists all Ttgeneralledgerhd models.
	 * @return mixed
	 */
	public function actionPindahbuku() {

		return $this->render( 'pindahbuku' );
	}
	/**
	 * @return string
	 */
	public function actionPindahbukuCreate() {
		return $this->create( 'JP', 'pindahbuku', 'Jurnal Pemindah Bukuan' );
	}
	/**
	 * @param $id
	 * @param $action
	 *
	 * @return string|\yii\web\Response
	 * @throws NotFoundHttpException
	 */
	public function actionPindahbukuUpdate( $id, $action ) {
		return $this->update( $id, $action, 'JP','pindahbuku', 'Jurnal Pemindah Bukuan' );
	}
	/**
	 * @param $id
	 * @param $action
	 *
	 * @return \yii\web\Response
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionPindahbukuCancel( $id, $action ) {
		return $this->cancel( $id, $action, 'pindahbuku' );
	}

    public function actionValidate( $id, $action ) {
	    /*
	        file : frmJurnalTransaksi
	    */

	    /* #Region " General Procedure & Variabel " */
	    $Number = 'MM10-00001';
	    $NumberLama = 'MM10-00001';

        $MyGLLink = '';
        $MyKodeTrans = '';
        $MyNumber = '';

        $PiutSubsLease = '';
        $PiutLeasing = '';

        /* preparation */
        $requestData = \Yii::$app->request->post('Ttgeneralledgerhd');
        $MyKodeTrans = substr($requestData['NoGL'], 0, 2);

        /* btn_Save_Click */
        try {
            if(($requestData['GLLink'] == '--' || $requestData['GLLink'] == $Number) && $MyKodeTrans === 'JA')
                throw new Exception('Silahkan Memilih Link Transaksi');

            if($MyKodeTrans === 'GL')
                Ttgeneralledgerhd::CekMMCOAKasBank($requestData['NoGL'], $requestData['TglGL']);

            /* CekDataGrid() <= di browser */

            /*"Total Debet dan Total Kredit harus sama" <= di browser*/

            if(!Ttgeneralledgerhd::CekTglTransaksi($requestData['TglGL']))
                throw new Exception('Tanggal tidak valid.');

            /*'cek Transaksi lain yang menggunakan JT ini, kalo ada update tabel itu NoGL = '--'*/
            /*
             * CekNoGLdiTransaksi();
             * todo: source code di-comment?
             * */

            /*
             * Aktif_Pasif(true)
             * todo: Aktif_Pasif(true) apakah saat render page?
             * */

            /*
             * SetHPLink(Number) 'beri nilai di GLHPLink
             * todo: apakah sudah dilakukan secara otomatis?
             * */

            /*
             * Fill(Number, Tanggal)
             * todo: Fill(Number, Tanggal) apakah saat render page?
             * */

            $cekHPLink = Ttgeneralledgerhd::CekHPLink($requestData['NoGL'], $requestData['TglGL']);
            $JumlahHP = $cekHPLink['JumlahHP'];
            $MyPesan0 = $cekHPLink['MyPesan0'];
            if($JumlahHP > 0 && $requestData['HPLink']) {
                $MyPesan0 = 'Silahkan melakukan link pelunasan hutang piutang berikut ini : ' . $MyPesan0;
                throw new Exception($MyPesan0);
            }

//            if()

            return $this->responseSuccess('validated.');
        }catch (\Exception $e) {
            return $this->responseFailed($e->getMessage());
        }
    }
	/**
	 * Displays a single Ttgeneralledgerhd model.
	 *
	 * @param string $NoGL
	 * @param string $TglGL
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $NoGL, $TglGL ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $NoGL, $TglGL ),
		] );
	}
	/**
	 * @return array
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDetail() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttgeneralledgerit::getTableSchema()->primaryKey;
		if ( isset( $requestData['oper'] ) ) {
			/* operation : create, edit or delete */
			$oper  = $requestData['oper'];
			$model = $oper === 'add' ? new Ttgeneralledgerit() :
				$this->findModelBase64( 'Ttgeneralledgerit', $requestData['id'] );
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					$model->attributes = $requestData;
					if ( $oper === 'add' ) {
						$count = ( new \yii\db\Query() )
							->from( 'ttgeneralledgerit' )
							->where( [
								'NoGL'  => $requestData['NoGL'],
								'TglGL' => $requestData['TglGL']
							] )->max( 'GLAutoN' );
						if ( ! $count ) {
							$count = 0;
						}
						$model->GLAutoN = ++ $count;
					}
					$response = $model->save() ?
						$this->responseSuccess( 'Data berhasil disimpan.' ) :
						$this->responseFailed( $this->modelErrorSummary( $model ) );
					break;
				case 'del'  :
					$response = $model->delete() ?
						$this->responseSuccess( 'Data berhasil dihapus.' ) :
						$this->responseFailed( $this->modelErrorSummary( $model ) );
					break;
			}
		} else {
			/* operation : read */
			$query     = ( new \yii\db\Query() )
				->select( [
					'CONCAT(' . implode( ",'||',", $primaryKeys ) . ') AS id',
					'ttgeneralledgerit.*',
					'traccount.NamaAccount NamaAccount'
				] )
				->from( 'ttgeneralledgerit' )
				->leftJoin( 'traccount', 'traccount.NoAccount = ttgeneralledgerit.NoAccount' )
				->where( [
					'NoGL'  => $requestData['NoGL'],
					'TglGL' => $requestData['TglGL']
				] )
                ->orderBy('GLAutoN');;
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ]['id'] = base64_encode( $rows[ $i ]['id'] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	/**
	 * Deletes an existing Ttgeneralledgerhd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $NoGL
	 * @param string $TglGL
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionMemorialDelete( $NoGL, $TglGL ) {
		$this->findModel( $NoGL, $TglGL )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * @param $kodeTrans
	 * @param $view
	 *
	 * @return string
	 */
	private function create( $kodeTrans, $view, $title ) {
		$model            = new Ttgeneralledgerhd();
		$model->NoGL      = Ttgeneralledgerhd::generateNoGl( $kodeTrans, true );
		$model->TglGL     = date( "Y-m-d" );
		$model->KodeTrans = $kodeTrans;
		$model->GLValid   = 'Belum';
		$model->save();
		$id = $this->generateIdBase64FromModel( $model );
		return $this->render( 'create', [
			'model' => $model,
			'id'    => $id,
            'kodeTrans' => $kodeTrans,
            'actionPrefix' => $view,
            'title' => $title,
		] );
	}
	/**
	 * @param $id
	 * @param $action
	 * @param $view
	 *
	 * @return string|\yii\web\Response
	 * @throws NotFoundHttpException
	 */
	private function update( $id, $action, $kodeTrans, $view, $title ) {
		$model = $this->findModelBase64( 'Ttgeneralledgerhd', $id );
		$post  = Yii::$app->request->post();
		if ( $action === 'create' ) {
			$noGl = Ttgeneralledgerhd::generateNoGl( $model->KodeTrans );
			/* data detail : update NoGL  */
			$modelDetails = Ttgeneralledgerit::findAll( [
				'NoGL'  => $model->NoGL,
				'TglGL' => $model->TglGL
			] );
			foreach ( $modelDetails as $modelDetail ) {
				$modelDetail->NoGL = $noGl;
				$modelDetail->save();
			}
			/* data header : update NoGl */
			$post['Ttgeneralledgerhd']['NoGL'] = $noGl;
			/* Type 'GL', then GLLink refers to it's self */
			if ( $model->KodeTrans = 'GL' ) {
				$post['Ttgeneralledgerhd']['GLLink'] = $noGl;
			}
		}
		if ( array_key_exists( 'Ttgeneralledgerhd', $post ) ) {
			$post['Ttgeneralledgerhd']['GLValid'] = (int) $post['Ttgeneralledgerhd']['GLValid'] === 0 ? 'Belum' : 'Sudah';
		}
		Yii::$app->request->setBodyParams( $post );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ $view ] );
		}
		return $this->render( 'update', [
			'model' => $model,
			'id'    => $id,
            'kodeTrans' => $kodeTrans,
            'actionPrefix' => $view,
            'title' => $title
		] );
	}
	/**
	 * @param $id
	 * @param $action
	 * @param $view
	 *
	 * @return \yii\web\Response
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	private function cancel( $id, $action, $view ) {
		if ( $action === 'create' ) {
			$model = $this->findModelBase64( 'Ttgeneralledgerhd', $id );
			/* delete data detail  */
			$modelDetails = Ttgeneralledgerit::findAll( [
				'NoGL'  => $model->NoGL,
				'TglGL' => $model->TglGL
			] );
			foreach ( $modelDetails as $modelDetail ) {
				$modelDetail->delete();
			}
			/* delete data header */
			$model->delete();
		}
		return $this->redirect( [ $view ] );
	}
	/**
	 * Finds the Ttgeneralledgerhd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $NoGL
	 * @param string $TglGL
	 *
	 * @return Ttgeneralledgerhd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $NoGL, $TglGL ) {
		if ( ( $model = Ttgeneralledgerhd::findOne( [ 'NoGL' => $NoGL, 'TglGL' => $TglGL ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
