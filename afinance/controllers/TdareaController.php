<?php
namespace afinance\controllers;
use afinance\components\AfinanceController;
use afinance\components\TdAction;
use afinance\models\Tdarea;
use Yii;
use yii\web\NotFoundHttpException;
class TdareaController extends AfinanceController {
	public function actions() {
		return [
			'td' => [
				'class' => TdAction::className(),
				'model' => Tdarea::className(),
			],
		];
	}
	public function actionIndex() {
		return $this->render( 'index' );
	}
	/**
	 * Creates a new Tdarea model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Tdarea();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'tdarea/index','id'=> $this->generateIdBase64FromModel($model)  ] );
//			return $this->redirect( [
//				'view',
//				'Provinsi'  => $model->Provinsi,
//				'Kabupaten' => $model->Kabupaten,
//				'Kecamatan' => $model->Kecamatan,
//				'Kelurahan' => $model->Kelurahan,
//				'KodePos'   => $model->KodePos
//			] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Tdarea model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$model = $this->findModelBase64( 'Tdarea', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'tdarea/index','id'=> $this->generateIdBase64FromModel($model) ] );
//			return $this->redirect( [
//				'view',
//				'Provinsi'  => $model->Provinsi,
//				'Kabupaten' => $model->Kabupaten,
//				'Kecamatan' => $model->Kecamatan,
//				'Kelurahan' => $model->Kelurahan,
//				'KodePos'   => $model->KodePos
//			] );
		}
		return $this->render( 'update', [
			'model' => $model,
            'id'    => $this->generateIdBase64FromModel($model),
		] );
	}
    /**
     *
     */
    public function actionFind() {
        $params = \Yii::$app->request->post();
        $rows = [];

        if($params['mode'] === 'combo')
            $rows = Tdarea::find()->combo($params);

        return $this->responseDataJson($rows);
    }
	protected function findModel( $Provinsi, $Kabupaten, $Kecamatan, $Kelurahan, $KodePos ) {
		if ( ( $model = Tdarea::findOne( [
				'Provinsi'  => $Provinsi,
				'Kabupaten' => $Kabupaten,
				'Kecamatan' => $Kecamatan,
				'Kelurahan' => $Kelurahan,
				'KodePos'   => $KodePos
			] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
