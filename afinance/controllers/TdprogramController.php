<?php

namespace afinance\controllers;

use afinance\components\TdAction;
use afinance\models\Tdarea;
use Yii;
use afinance\models\Tdprogram;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TdprogramController implements the CRUD actions for Tdprogram model.
 */
class TdprogramController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
	public function actions()
	{
		return [
			'td' => [
				'class' => TdAction::className(),
				'model' => Tdprogram::className(),
			],
		];
	}
    /**
     * Lists all Tdprogram models.
     * @return mixed
     */
    public function actionIndex()
    {

        return $this->render('index');
    }

    /**
     * Displays a single Tdprogram model.
     * @param string $ProgramNama
     * @param string $MotorType
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($ProgramNama, $MotorType)
    {
        return $this->render('view', [
            'model' => $this->findModel($ProgramNama, $MotorType),
        ]);
    }

    /**
     * Creates a new Tdprogram model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tdprogram();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
	        return $this->redirect( [ 'index','id'=> $this->generateIdBase64FromModel($model)  ] );
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tdprogram model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $ProgramNama
     * @param string $MotorType
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($ProgramNama, $MotorType)
    {
        $model = $this->findModel($ProgramNama, $MotorType);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
	        return $this->redirect( [ 'index','id'=> $this->generateIdBase64FromModel($model)  ] );
        }

        return $this->render('update', [
            'model' => $model,
            'id'    => $this->generateIdBase64FromModel($model),
        ]);
    }

    /**
     * Deletes an existing Tdprogram model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $ProgramNama
     * @param string $MotorType
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($ProgramNama, $MotorType)
    {
        $this->findModel($ProgramNama, $MotorType)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tdprogram model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $ProgramNama
     * @param string $MotorType
     * @return Tdprogram the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($ProgramNama, $MotorType)
    {
        if (($model = Tdprogram::findOne(['ProgramNama' => $ProgramNama, 'MotorType' => $MotorType])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
