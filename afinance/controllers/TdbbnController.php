<?php
namespace afinance\controllers;
use afinance\components\AfinanceController;
use afinance\components\TdAction;
use afinance\models\Tdbbn;

use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TdbbnController implements the CRUD actions for Tdbbn model.
 */
class TdbbnController extends AfinanceController {
	public function actions() {
		return [
			'td' => [
				'class'    => TdAction::className(),
				'model'    => Tdbbn::className(),
				'joinWith' => [
					'motorTypes' => [
						'type' => 'INNER JOIN'
					]
				]
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Tdbbn models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index');
	}
	/**
	 * Displays a single Tdbbn model.
	 *
	 * @param string $Kabupaten
	 * @param string $MotorType
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $Kabupaten, $MotorType ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $Kabupaten, $MotorType ),
		] );
	}
	/**
	 * Creates a new Tdbbn model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Tdbbn();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'tdbbn/index','id'=> $this->generateIdBase64FromModel($model)  ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Tdbbn model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$model = $this->findModelBase64( 'Tdbbn', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'tdbbn/index','id'=> $this->generateIdBase64FromModel($model)  ] );
		}
		return $this->render( 'update', [
			'model' => $model,
            'id'    => $this->generateIdBase64FromModel($model),
		] );
	}
	/**
	 * Deletes an existing Tdbbn model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $Kabupaten
	 * @param string $MotorType
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete( $Kabupaten, $MotorType ) {
		$this->findModel( $Kabupaten, $MotorType )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Tdbbn model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $Kabupaten
	 * @param string $MotorType
	 *
	 * @return Tdbbn the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $Kabupaten, $MotorType ) {
		if ( ( $model = Tdbbn::findOne( [ 'Kabupaten' => $Kabupaten, 'MotorType' => $MotorType ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
