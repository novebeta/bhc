<?php
namespace afinance\controllers;
use afinance\components\AfinanceController;
use afinance\components\TdAction;
use afinance\models\Ttsk;
use common\components\Custom;
use common\components\General;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
/**
 * TtskController implements the CRUD actions for Ttsk model.
 */
class TtskController extends AfinanceController {
	public function actions() {
		$sql   = "";
		$where = [];
		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			switch ( $r ) {
				case 'ttsk/index':
					$sql = "SELECT ttsk.SKJam, ttsk.SKCetak, ttsk.SKPengirim, ttsk.SKNo, ttsk.SKTgl, ttsk.LokasiKode, ttsk.SKMemo, ttsk.UserID, ttdk.CusKode, tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, tdcustomer.CusTelepon, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, ttdk.DKNo, ttdk.DKHarga, tmotor.RKNo, ttsk.NoGL
				         FROM ttsk 
				         INNER JOIN tmotor ON ttsk.SKNo = tmotor.SKNo
				         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
				         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
				         UNION ALL
				         SELECT ttsk.SKJam, ttsk.SKCetak, ttsk.SKPengirim, ttsk.SKNo, ttsk.SKTgl, ttsk.LokasiKode, ttsk.SKMemo, ttsk.UserID, ttdk.CusKode, tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, tdcustomer.CusTelepon, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, ttdk.DKNo, ttdk.DKHarga, tmotor.RKNo, ttsk.NoGL
				         FROM ttsk 
				         INNER JOIN ttrk ON ttsk.SKNo = ttrk.SKNo 
				         INNER JOIN tmotor ON ttrk.RKNo = tmotor.RKNo 
				         INNER JOIN ttdk ON ttrk.DKNo = ttdk.DKNo 
				         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
				         WHERE (tmotor.SSNo <> '--' OR tmotor.FBNo <> '--' OR tmotor.PDNo <>'--') 
		         ";
					break;
				case 'ttsk/select':
					$SKNo = isset($urlParams[ 'SKNo' ] )? $urlParams[ 'SKNo' ] : '';
						$sql = "SELECT ttsk.*, tdcustomer.CusNama, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun,
					tmotor.MotorNoMesin, tmotor.MotorNoRangka, tdmotortype.MotorNama
					FROM ttsk 
					INNER JOIN tmotor ON ttsk.SKNo = tmotor.SKNo
					INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
					INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType
					INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
					LEFT OUTER JOIN ttrk ON ttsk.SKNo = ttrk.SKNo
					WHERE (ttrk.RKNo is NUll OR ttsk.SKNo = '$SKNo')
		         ";
					break;
				case 'ttsk/serah-terima-kolektif':
					$sql = "SELECT        '0' AS Cek, ttsk.SKNo, ttsk.SKTgl, ttdk.DKNo, ttdk.DKTgl, ttdk.LeaseKode, tdcustomer.CusNama, tdsales.SalesNama, tdmotortype.MotorNama, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorNoMesin, tmotor.FakturAHMNo, 
                         tmotor.STNKTglBayar, tmotor.FakturAHMTgl, tmotor.FakturAHMTglAmbil, tmotor.STNKNo, tmotor.STNKTglMasuk, tmotor.STNKTglJadi, tmotor.STNKTglAmbil, tmotor.STNKNama, tmotor.STNKAlamat, tmotor.STNKPenerima, 
                         tmotor.NoticeNo, tmotor.NoticeTglJadi, tmotor.NoticeTglAmbil, tmotor.NoticePenerima, tmotor.PlatNo, tmotor.PlatTgl, tmotor.PlatTglAmbil, tmotor.PlatPenerima, tmotor.BPKBNo, tmotor.BPKBTglJadi, tmotor.BPKBTglAmbil, 
                         tmotor.BPKBPenerima, tmotor.MotorAutoN, tmotor.SRUTNo, tmotor.SRUTTglJadi, tmotor.SRUTTglAmbil, tmotor.SRUTPenerima
						 FROM ttsk INNER JOIN
                         tmotor ON ttsk.SKNo = tmotor.SKNo INNER JOIN
                         ttdk ON ttdk.DKNo = tmotor.DKNo INNER JOIN
                         tdsales ON tdsales.SalesKode = ttdk.SalesKode INNER JOIN
                         tdmotortype ON tmotor.MotorType = tdmotortype.MotorType INNER JOIN
                         tdcustomer ON ttdk.CusKode = tdcustomer.CusKode";
					break;
			}
		}
		return [
			'td' => [
				'class'      => TdAction::className(),
				'model'      => Ttsk::className(),
				'queryRaw'   => $sql,
				'queryRawPK' => [ 'SKNo' ]
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttsk models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index', [
			'mode' => ''
		] );
	}
	/**
	 * Lists all Ttsk models.
	 * @return mixed
	 */
	public function actionSelect() {
		$this->layout = 'select-mode';
		return $this->render( 'index', [
			'mode' => self::MODE_SELECT
		] );
	}
	/**
	 * Lists all Ttsk models.
	 * @return mixed
	 */
	public function actionSerahTerimaKolektif() {
		return $this->render( 'serah-terima-kolektif' );
	}
	/**
	 * Displays a single Ttsk model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Finds the Ttsk model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttsk the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttsk::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttsk model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model             = new Ttsk();
		$model->SKNo       = General::createTemporaryNo( 'SK', 'ttsk', 'SKNo' );
		$model->SKTgl      = General::asDate( 'now', 'yyyy-MM-dd' );
		$model->SKJam      = General::asDatetime( 'now', 'yyyy-MM-dd HH:mm:ss' );
		$model->SKCetak    = "Belum";
		$model->UserID     = $this->UserID();
		$model->LokasiKode = $this->LokasiKode();
		$model->DKNo       = "--";
		$model->SKPengirim = "--";
		$model->save();
		$dsJual                   = $model::find()->dsTJualFillByNo()->where( [ 'ttsk.SKNo' => $model->SKNo ] )->asArray()->one();
		$dsJual[ 'NoBukuServis' ] = "--";
		$id                       = $this->generateIdBase64FromModel( $model );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'id' => $id ] );
		}
		return $this->render( 'create', [
			'model'  => $model,
			'dsJual' => $dsJual,
			'id'     => $id,
		] );
	}
	/**
	 * Updates an existing Ttsk model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		/** @var Ttsk $model */
		$model = $this->findModelBase64( 'ttsk', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'ttsk/index', 'id' => $id ] );
		}
		$dsJual = $model::find()->dsTJualFillByNo()->where( [ 'ttsk.SKNo' => $model->SKNo ] )->asArray()->one();
		return $this->render( 'update', [
			'model'  => $model,
			'dsJual' => $dsJual,
			'id'     => $id,
		] );
	}
	/**
	 * Deletes an existing Ttsk model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionCancel( $id ) {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'create' ) {
			$this->findModelBase64( 'ttsk', $id )->delete();
		}
		return $this->redirect( [ 'ttsk/index' ] );
	}
}
