<?php
namespace afinance\controllers;
use afinance\components\AfinanceController;
use afinance\models\ContactForm;
use afinance\models\PasswordResetRequestForm;
use afinance\models\ResendVerificationEmailForm;
use afinance\models\ResetPasswordForm;
use afinance\models\SignupForm;
use afinance\models\Tuser;
use afinance\models\VerifyEmailForm;
use common\components\Custom;
use DateTime;
use GuzzleHttp\Client;
use SplFileObject;
use Yii;
use yii\base\InvalidArgumentException;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
/**
 * Site controller
 */
class SiteController extends AfinanceController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['logout', 'signup'],
//                'rules' => [
//                    [
//                        'actions' => ['signup'],
//                        'allow' => true,
//                        'roles' => ['?'],
//                    ],
//                    [
//                        'actions' => ['logout'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'logout' => [ 'post' ],
				],
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function actions() {
		return [
			'error'   => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}
	public function actionDekrip() {
		/** @var Tuser[] $user */
		$user = Tuser::find()->all();
		$list = [];
		foreach ( $user as $u ) {
			$list[ $u->UserID ] = Custom::Decript( $u->UserPass );
		}
		$this->layout = '/plain';
		var_dump( $list );
		return $list;
	}
//    public function actionIndex(){
//	    $pdf = file_get_contents('http://192.168.56.101:8080/file/tdbbn/pdf');
//	    header('Content-Description: File Transfer');
//	    header('Content-Type: application/octet-stream');
//	    header('Content-Disposition: attachment; filename=tdbbn.pdf');
//	    ob_start();
//	    echo $pdf;
//	    return ob_get_clean();
//    }
	/**
	 * Displays homepage.
	 *
	 * @return mixed
	 */
	public function actionIndex() {
//	    $client = new Client();
//	    $response = $client->get('http://192.168.56.101:8080/file/tdbbn/pdf')
//	                       ->send();
		return $this->render( 'index' );
	}
	public function actionBackupRestore() {
		return $this->render( 'backup-restore' );
	}
	public function actionWaktu() {
		session_write_close();
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		Yii::$app->formatter->locale = 'id-ID';
		$time1                       = new DateTime( '@' . ( Yii::$app->session->get( '_loginTime' ) ) );
		$time2                       = new DateTime();
		$interval                    = $time1->diff( $time2 );
		return [
			'tgl'      => Yii::$app->formatter->asDatetime( 'now', 'eeee, dd MMMM yyyy - HH:mm:ss' ),
			'duration' => $interval->format( '%H:%I:%S' )
		];
	}
	public function actionTestReport() {
		$client   = new Client( [
			'headers' => [ 'Content-Type' => 'application/json' ]
		] );
		$response = $client->get(Yii::$app->params['H1'][$_POST['db']]['host_report'] . '/login/camellabs/camellabs' );
		return $response->getBody()->getContents();
	}
	public function actionBackup() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$infoDB                      = [
			'host'     => Custom::getDsnAttribute( 'host', Yii::$app->getDb()->dsn ),
			'dbname'   => Custom::getDsnAttribute( 'dbname', Yii::$app->getDb()->dsn ),
			'username' => Yii::$app->getDb()->username,
			'password' => Yii::$app->getDb()->password,
		];
		$nama                        = Yii::getAlias( '@webroot' ) . "/backup/" . $_SESSION['_company']['nama'] . " " . date( 'Ymd Hi' ) . ".dmp";
		$nama_url                    = Yii::getAlias( '@web' ) . "/backup/" . $_SESSION['_company']['nama'] . " " . date( 'Ymd Hi' ) . ".dmp";
		$logFile                     = Yii::getAlias( '@runtime' ) . "/" . $infoDB['dbname'] . ".log";
		if ( file_exists( $logFile ) ) {
			unlink( $logFile );
		}
		session_write_close();
//		sleep( 5 );
		$output = shell_exec( "mysqldump -h " . $infoDB['host'] . " -u" . $infoDB['username'] .
		                      " -p" . $infoDB['password'] . " --routines --databases " .
		                      $infoDB['dbname'] . " --verbose 2> " . $logFile . " > '" . $nama . "'" );
		return [
			'status' => true,
			'url'    => htmlentities( $nama_url )
		];
	}
	public function actionMonitorBackup() {
		$filename    = Yii::getAlias( '@runtime' ) . "/" . Custom::getDsnAttribute( 'dbname', Yii::$app->getDb()->dsn ) . ".log";
		$filenametmp = Yii::getAlias( '@runtime' ) . "/" . Custom::getDsnAttribute( 'dbname', Yii::$app->getDb()->dsn ) . "_tmp.log";
		shell_exec( '\cp "' . $filename . '" ' . ' "' . $filenametmp . '"' );
//		$output   = shell_exec( 'cat ' . $filename );
//		$output   = file_get_contents($filename);
		if ( ! file_exists( $filenametmp ) ) {
			return "==EOL==";
		}
		$file = new SplFileObject( $filenametmp, "r" );
		if ( ! $file->isFile() ) {
			return "==EOL==";
		}
		$file->flock( LOCK_SH );
		$file->seek( $_GET['line'] );
		if ( $file->valid() ) {
			return $file->current();
		} else {
			return "==EOL==";
		}
	}

	public function actionMonitorRestore(){
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return \Yii::$app->db->createCommand("SHOW PROCESSLIST")->queryAll();
	}

	/**
	 * Logs out the current user.
	 *
	 * @return mixed
	 */
//    public function actionLogout()
//    {
//        Yii::$app->user->logout();
//
//        return $this->goHome();
//    }
	/**
	 * Displays contact page.
	 *
	 * @return mixed
	 */
	public function actionContact() {
		$model = new ContactForm();
		if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) {
			if ( $model->sendEmail( Yii::$app->params['adminEmail'] ) ) {
				Yii::$app->session->setFlash( 'success', 'Thank you for contacting us. We will respond to you as soon as possible.' );
			} else {
				Yii::$app->session->setFlash( 'error', 'There was an error sending your message.' );
			}
			return $this->refresh();
		} else {
			return $this->render( 'contact', [
				'model' => $model,
			] );
		}
	}
	/**
	 * Displays about page.
	 *
	 * @return mixed
	 */
	public function actionAbout() {
		return $this->render( 'about' );
	}
	/**
	 * Signs user up.
	 *
	 * @return mixed
	 */
	public function actionSignup() {
		$model = new SignupForm();
		if ( $model->load( Yii::$app->request->post() ) && $model->signup() ) {
			Yii::$app->session->setFlash( 'success', 'Thank you for registration. Please check your inbox for verification email.' );
			return $this->goHome();
		}
		return $this->render( 'signup', [
			'model' => $model,
		] );
	}
	/**
	 * Requests password reset.
	 *
	 * @return mixed
	 */
	public function actionRequestPasswordReset() {
		$model = new PasswordResetRequestForm();
		if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) {
			if ( $model->sendEmail() ) {
				Yii::$app->session->setFlash( 'success', 'Check your email for further instructions.' );
				return $this->goHome();
			} else {
				Yii::$app->session->setFlash( 'error', 'Sorry, we are unable to reset password for the provided email address.' );
			}
		}
		return $this->render( 'requestPasswordResetToken', [
			'model' => $model,
		] );
	}
	/**
	 * Resets password.
	 *
	 * @param string $token
	 *
	 * @return mixed
	 * @throws BadRequestHttpException
	 */
	public function actionResetPassword( $token ) {
		try {
			$model = new ResetPasswordForm( $token );
		} catch ( InvalidArgumentException $e ) {
			throw new BadRequestHttpException( $e->getMessage() );
		}
		if ( $model->load( Yii::$app->request->post() ) && $model->validate() && $model->resetPassword() ) {
			Yii::$app->session->setFlash( 'success', 'New password saved.' );
			return $this->goHome();
		}
		return $this->render( 'resetPassword', [
			'model' => $model,
		] );
	}
	/**
	 * Verify email address
	 *
	 * @param string $token
	 *
	 * @return yii\web\Response
	 * @throws BadRequestHttpException
	 */
	public function actionVerifyEmail( $token ) {
		try {
			$model = new VerifyEmailForm( $token );
		} catch ( InvalidArgumentException $e ) {
			throw new BadRequestHttpException( $e->getMessage() );
		}
		if ( $user = $model->verifyEmail() ) {
			if ( Yii::$app->user->login( $user ) ) {
				Yii::$app->session->setFlash( 'success', 'Your email has been confirmed!' );
				return $this->goHome();
			}
		}
		Yii::$app->session->setFlash( 'error', 'Sorry, we are unable to verify your account with provided token.' );
		return $this->goHome();
	}
	/**
	 * Resend verification email
	 *
	 * @return mixed
	 */
	public function actionResendVerificationEmail() {
		$model = new ResendVerificationEmailForm();
		if ( $model->load( Yii::$app->request->post() ) && $model->validate() ) {
			if ( $model->sendEmail() ) {
				Yii::$app->session->setFlash( 'success', 'Check your email for further instructions.' );
				return $this->goHome();
			}
			Yii::$app->session->setFlash( 'error', 'Sorry, we are unable to resend verification email for the provided email address.' );
		}
		return $this->render( 'resendVerificationEmail', [
			'model' => $model
		] );
	}
}
