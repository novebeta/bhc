<?php
namespace afinance\controllers;
use afinance\components\AfinanceController;
use afinance\components\TdAction;
use afinance\models\Tddealer;
use afinance\models\Tmotor;
use afinance\models\Ttpd;

use common\components\General;
use Yii;
use yii\base\UserException;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
/**
 * TtpdController implements the CRUD actions for Ttpd model.
 */
class TtpdController extends AfinanceController {
	public function actions() {
		$colGrid  = null;
		$joinWith = [
			'dealer' => [
				'type' => 'INNER JOIN'
			],
			'lokasi' => [
				'type' => 'INNER JOIN'
			]
		];
		$join     = [];
		$where    = [
			[
				'op'        => 'AND',
				'condition' => "LEFT(PDNo,2) = 'PD'",
				'params'    => []
			]
		];
		if ( isset( $_POST[ 'query' ] ) ) {
			$r = Json::decode( $_POST[ 'query' ], true )[ 'r' ];
			switch ( $r ) {
				case 'ttpd/penerimaan-dealer':
					$colGrid = Ttpd::colGridPenerimaanDealer();
					break;
				case 'ttpd/invoice-penerimaan':
					$colGrid = Ttpd::colGridInvoicePenerimaan();
					break;
			}
		}
		return [
			'td' => [
				'class'    => TdAction::className(),
				'model'    => Ttpd::className(),
				'colGrid'  => $colGrid,
				'joinWith' => $joinWith,
				'where'    => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttpd models.
	 * @return mixed
	 */
	public function actionPenerimaanDealer() {

		return $this->render( 'PenerimaanDealer' );
	}
	/**
	 * Lists all Ttpd models.
	 * @return mixed
	 */
	public function actionInvoicePenerimaan() {

		return $this->render( 'InvoicePenerimaan' );
	}
	/**
	 * Displays a single Ttpd model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Finds the Ttpd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttpd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttpd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttpd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model             = new Ttpd();
		$model->PDNo       = General::createTemporaryNo( "PD", 'Ttpd', 'PDNo' );
		$model->PDTgl      = date( 'Y-m-d' );
		$model->SDNo       = '--';
		$model->PDMemo     = '--';
		$model->LokasiKode = $this->LokasiKode();
		$model->DealerKode = ( Tddealer::find()->one() )->DealerKode;
		$model->PDTotal    = 0;
		$model->UserID     = $this->UserID();
		$model->save();
		$dsBeli               = Ttpd::find()->dsTMutasiTtpd()->where( [ 'ttpd.PDNo' => $model->PDNo ] )->asArray( true )->one();
		$dsBeli[ 'PDNoView' ] = str_replace( '=', '-', $dsBeli[ 'PDNo' ] );
		$id                   = $this->generateIdBase64FromModel( $model );
		return $this->render( 'create', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'jenis'  => "MutasiEksternal",
			'id'     => $id
		] );
	}
	/**
	 * Updates an existing Ttpd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \yii\db\Exception
	 */
	public function actionUpdate( $id, $jenis, $action ) {
		/** @var Ttpd $model */
		$model = $this->findModelBase64( 'Ttpd', $id );
		if ( $model == null ) {
			$model = new Ttpd;
		}
		$dsBeli = Ttpd::find()->dsTMutasiTtpd()->where( [ 'ttpd.PDNo' => $model->PDNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post        = Yii::$app->request->post();
			$transaction = Ttpd::getDb()->beginTransaction();
			try {
				if ( Ttpd::find()->CekSDNo( "ttpd", "SDNo", $post[ 'SDNo' ],
					"Surat Jalan : " . $post[ 'SDNo' ] . " telah dipakai oleh PD Nomor : ", $post[ 'DealerKode' ], $post[ 'PDNo' ] ) ) {
					throw new UserException();
				}
				$post[ 'UserID' ] = $this->UserID();
				$SubTotal         = 0;
//				$detil    = Json::decode( $post[ 'detil' ] );
				$detil = Tmotor::find()->where( [ 'PDNo' => $post[ 'PDNo' ] ] )->asArray()->all();
				foreach ( $detil as &$row ) {
					$SubTotal            += floatval( $row[ 'SSHarga' ] );
					$row[ 'DealerKode' ] = $post[ 'DealerKode' ];
				}
				$post[ 'PDTotal' ] = $SubTotal;
				if ( $jenis == 'InvoicePenerimaan' ) {
					foreach ( $detil as &$row ) {
						if ( $row[ 'FBHarga' ] == $row[ 'SSHarga' ] ) {
							if ( $row[ 'SKHarga' ] == 0 || $row[ 'SKNo' ] == '--' ) {
								$row[ 'SKHarga' ] = $row[ 'SSHarga' ];
							}
							if ( $row[ 'SDHarga' ] == 0 || $row[ 'SDNo' ] == '--' ) {
								$row[ 'SDHarga' ] = $row[ 'SSHarga' ];
							}
							if ( $row[ 'RKHarga' ] == 0 || $row[ 'RKNo' ] == '--' ) {
								$row[ 'RKHarga' ] = $row[ 'SSHarga' ];
							}
						} else {
							if ( $row[ 'FBHarga' ] == 0 || $row[ 'FBNo' ] == '--' ) {
								$row[ 'FBHarga' ] = $row[ 'SSHarga' ];
							}
							if ( $row[ 'SKHarga' ] == 0 || $row[ 'SKNo' ] == '--' ) {
								$row[ 'SKHarga' ] = $row[ 'FBHarga' ];
							}
							if ( $row[ 'SDHarga' ] == 0 || $row[ 'SDNo' ] == '--' ) {
								$row[ 'SDHarga' ] = $row[ 'FBHarga' ];
							}
							if ( $row[ 'RKHarga' ] == 0 || $row[ 'RKNo' ] == '--' ) {
								$row[ 'RKHarga' ] = $row[ 'FBHarga' ];
							}
						}
					}
				} else {
					Ttpd::getDb()->createCommand( "DELETE FROM tvmotorlokasi WHERE NoTrans = :NoTrans", [
						':NoTrans' => $post[ 'PDNo' ]
					] )->execute();
					foreach ( $detil as &$row ) {
						if ( $row[ 'FBHarga' ] == 0 || $row[ 'FBNo' ] == '--' ) {
							$row[ 'FBHarga' ] = $row[ 'SSHarga' ];
						}
						if ( $row[ 'SDHarga' ] == 0 || $row[ 'SDNo' ] == '--' ) {
							$row[ 'SDHarga' ] = $row[ 'SSHarga' ];
						}
						if ( $row[ 'SKHarga' ] == 0 || $row[ 'SKNo' ] == '--' ) {
							$row[ 'SKHarga' ] = $row[ 'SSHarga' ];
						}
						if ( $row[ 'RKHarga' ] == 0 || $row[ 'RKNo' ] == '--' ) {
							$row[ 'RKHarga' ] = $row[ 'SSHarga' ];
						}
					}
				}
				Ttpd::getDb()->createCommand( "UPDATE tmotor JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
				SET tmotor.SSHarga = tdmotortype.MotorHrgBeli, SKHarga = IF(SKHarga = 0,tdmotortype.MotorHrgBeli,SKHarga), SDHarga = IF(SDHarga = 0,tdmotortype.MotorHrgBeli,SDHarga), RKHarga = IF(RKHarga = 0,tdmotortype.MotorHrgBeli,RKHarga)
				WHERE tmotor.SSHarga = 0 AND PDNo = :PDNo;
				UPDATE tmotor JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
				SET tmotor.FBHarga = tdmotortype.MotorHrgBeli, SKHarga = IF(SKHarga = 0,tdmotortype.MotorHrgBeli,SKHarga), SDHarga = IF(SDHarga = 0,tdmotortype.MotorHrgBeli,SDHarga), RKHarga = IF(RKHarga = 0,tdmotortype.MotorHrgBeli,RKHarga)
				WHERE tmotor.FBHarga = 0 AND PDNo = :PDNo;", [
					':PDNo' => $post[ 'PDNo' ]
				] )->execute();
				foreach ( $detil as $row ) {
					Ttpd::getDb()->createCommand()
					    ->update( 'tmotor', $row, [ 'MotorAutoN' => $row[ 'MotorAutoN' ], 'MotorNoMesin' => $row[ 'MotorNoMesin' ] ] )
					    ->execute();
				}
				$model->load($post,'');
				$model->save();
				if ( strpos( $model->PDNo, '=' ) !== false ) {
					$model->PDNo = General::createNo( "PD", 'ttpd', 'PDNo' );
					Tmotor::updateAll( [ 'PDNo' => $model->PDNo ], [ 'PDNo' => $post['PDNo'] ] );
				}
				$model->save();
				$model->refresh();
				Yii::$app->db
					->createCommand( "INSERT INTO tvmotorlokasi (MotorAutoN, MotorNoMesin, LokasiKode, NoTrans, Jam, Kondisi)
						SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttpd.LokasiKode  AS LokasiKode, ttpd.PDNo AS NoTrans, ttpd.PDJam AS Jam, 'IN' AS Kondisi
						FROM tmotor INNER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo WHERE ttpd.PDNo = :PDNo", [
						':PDNo' => $model->PDNo
					] )->execute();
				$transaction->commit();
				return $this->redirect( [ 'ttpd/' . ( $jenis == 'MutasiEksternal' ) ? 'penerimaan-dealer' : 'invoice-penerimaan', 'id' => $this->generateIdBase64FromModel( $model ) ] );
			} catch ( UserException $e ) {
				$transaction->rollBack();
				Yii::$app->session->setFlash( 'error', $e->getMessage() );
				return $this->render( 'update', [
					'model'  => $model,
					'dsBeli' => $dsBeli,
					'jenis'  => $jenis,
					'id'     => $id
				] );
			}
		}
		$dsBeli[ 'PDNoView' ] = str_replace( '=', '-', $dsBeli[ 'PDNo' ] );
		return $this->render( 'update', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'jenis'  => $jenis,
			'id'     => $id
		] );
	}
	/**
	 * Deletes an existing Ttpd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
	public function actionDetail() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = [ 'tmotor.MotorAutoN', 'tmotor.MotorNoMesin' ];
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper  = $requestData[ 'oper' ];
			$model = $this->findModelBase64( 'tmotor', $requestData[ 'id' ] );
			if ( $model == null ) {
				$model = new Tmotor();
			}
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					$model->attributes = $requestData;
					if ( $oper === 'add' ) {
						$JumMotor          = Ttpd::find()->CekMesinNo( $requestData[ 'MotorNoMesin' ], $requestData[ 'PDNo' ] );
						$model->MotorAutoN = $JumMotor;
						$model->save();
					}
					$response = $model->save() ?
						$this->responseSuccess( 'Data berhasil disimpan.' ) :
						$this->responseFailed( $this->modelErrorSummary( $model ) );
					break;
				case 'del'  :
					$response = $model->delete() ?
						$this->responseSuccess( 'Data berhasil dihapus.' ) :
						$this->responseFailed( $this->modelErrorSummary( $model ) );
					break;
			}
		} else {
			/* operation : read */
			$query = ( new \yii\db\Query() );
			if ( isset( $_GET[ 'jenis' ] ) ) {
				switch ( $_GET[ 'jenis' ] ) {
					case 'MutasiEksternal':
					case 'InvoicePenerimaan':
						$query = ( new \yii\db\Query() )
							->select( new Expression(
								'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
								tmotor.BPKBNo, tmotor.BPKBTglAmbil, tmotor.BPKBTglJadi, tmotor.DKNo, tmotor.DealerKode, tmotor.FBHarga, tmotor.FBNo, tmotor.FakturAHMNo, 
                         tmotor.FakturAHMTgl, tmotor.FakturAHMTglAmbil, tmotor.MotorAutoN, tmotor.MotorMemo, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorTahun, 
                         tmotor.MotorType, tmotor.MotorWarna, tmotor.PDNo, tmotor.PlatNo, tmotor.PlatTgl, tmotor.PlatTglAmbil, tmotor.SDNo, tmotor.SKNo, tmotor.SSNo, tmotor.STNKAlamat, 
                         tmotor.STNKNama, tmotor.STNKNo, tmotor.STNKTglAmbil, tmotor.STNKTglBayar, tmotor.STNKTglJadi, tmotor.STNKTglMasuk, tdmotortype.MotorNama, 
                         tmotor.FakturAHMNilai, tmotor.RKNo, tmotor.RKHarga, tmotor.SKHarga, tmotor.SDHarga, tmotor.MMHarga, tmotor.SSHarga' ) )
							->from( 'tmotor' )
							->join( 'LEFT OUTER JOIN', 'tdmotortype', 'tmotor.MotorType = tdmotortype.MotorType' )
							->where( [
								'PDNo' => $requestData[ 'PDNo' ]
							] )
							->orderBy( 'tmotor.MotorType, tmotor.MotorTahun, tmotor.MotorWarna' );
						break;
				}
			}
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
}
