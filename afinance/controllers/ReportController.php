<?php
namespace afinance\controllers;
use afinance\components\AfinanceController;
use GuzzleHttp\Client;
use Yii;
use yii\helpers\Json;
use yii\web\RangeNotSatisfiableHttpException;
class ReportController extends AfinanceController {
	const MIME = [
		'pdf' => 'application/pdf',
		'xls' => 'application/vnd.ms-excel',
		'doc' => 'application/msword',
		'rtf' => 'application/rtf',
	];
	public function actionDaftarSuratJalanSupplier() {
		return $this->prosesLaporan( 'aunit/rBeliSuratJalanSupplier', '@aunit/views/report/Pembelian/DaftarSuratJalanSupplier' );
	}
	protected function prosesLaporan( $target, $view ) {
		if ( isset( $_POST[ 'tipe' ] ) ) {
			unset( $_POST[ '_csrf-app' ] );
			$_POST         = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
			$_POST[ 'db' ] = base64_decode( $_COOKIE[ '_outlet' ] );
			$client        = new Client( [
				'headers' => [ 'Content-Type' => 'application/octet-stream' ]
			] );
			$response      = $client->post( Yii::$app->params[ 'host_report' ] . '/' . $target, [
				'body' => Json::encode($_POST)
			] );
			try {
				if ( $_POST[ 'filetype' ] == 'xlsr' ) {
					$_POST[ 'filetype' ] = 'xls';
				}
				return Yii::$app->response->sendContentAsFile(
					$response->getBody(),
					$_POST[ 'tipe' ] . '.' . $_POST[ 'filetype' ],
					[
						'inline'   => true,
						'mimeType' => $this::MIME[ $_POST[ 'filetype' ] ]
					]
				);
			} catch ( RangeNotSatisfiableHttpException $e ) {
			}
		}
		unset( $_POST );
		return $this->render( $view );
	}
	public function actionDaftarFakturBeli() {
		return $this->prosesLaporan( 'aunit/rBeliFakturBeli', '@aunit/views/report/Pembelian/DaftarFakturBeli' );
	}
	public function actionDaftarInden() {
		return $this->prosesLaporan( 'aunit/rJualInden', '@aunit/views/report/Penjualan/DaftarInden' );
	}
	public function actionDaftarDataKonsumen() {
		return $this->prosesLaporan( 'aunit/rJualDataKonsumen', '@aunit/views/report/Penjualan/DaftarDataKonsumen' );
	}
	public function actionDaftarSuratJalanKonsumen() {
		return $this->prosesLaporan( 'aunit/rJualSuratJalanKonsumen', '@aunit/views/report/Penjualan/DaftarSuratJalanKonsumen' );
	}
	public function actionDaftarReturKonsumen() {
		return $this->prosesLaporan( 'aunit/rJualReturKonsumen', '@aunit/views/report/Penjualan/DaftarReturKonsumen' );
	}
	public function actionCetakMasalBuktiDksk() {
		return $this->prosesLaporan( 'aunit/', '@aunit/views/report/Penjualan/CetakMasalBuktiDKSK' );
	}
	public function actionDaftarSuratJalanMutasiDealer() {
		return $this->prosesLaporan( 'aunit/rMutasiSDealer', '@aunit/views/report/Mutasi/DaftarSuratJalanMutasiDealer' );
	}
	public function actionDaftarSuratJalanMutasiPos() {
		return $this->prosesLaporan( 'aunit/rMutasiSPOS', '@aunit/views/report/Mutasi/DaftarSuratJalanMutasiPos' );
	}
	public function actionDaftarPenerimaanBarangDealer() {
		return $this->prosesLaporan( 'aunit/rMutasiPDealer', '@aunit/views/report/Mutasi/DaftarPenerimaanBarangDealer' );
	}
	public function actionDaftarPenerimaanBarangPos() {
		return $this->prosesLaporan( 'aunit/rMutasiPPos', '@aunit/views/report/Mutasi/DaftarPenerimaanBarangPos' );
	}
	public function actionDaftarKasMasuk() {
		return $this->prosesLaporan( 'aunit/rKeuanganKasMasuk', '@aunit/views/report/Keuangan/DaftarKasMasuk' );
	}
	public function actionDaftarKasKeluar() {
		return $this->prosesLaporan( 'aunit/rKeuanganKasKeluar', '@aunit/views/report/Keuangan/DaftarKasKeluar' );
	}
	public function actionDaftarBankMasuk() {
		return $this->prosesLaporan( 'aunit/rKeuanganBankMasuk', '@aunit/views/report/Keuangan/DaftarBankMasuk' );
	}
	public function actionDaftarBankKeluar() {
		return $this->prosesLaporan( 'aunit/rKeuanganBankKeluar', '@aunit/views/report/Keuangan/DaftarBankKeluar' );
	}
	public function actionDaftarSaldoKasBank() {
		return $this->prosesLaporan( 'aunit/rKeuanganSaldoKasBank', '@aunit/views/report/Keuangan/DaftarSaldoKasBank' );
	}
	public function actionJurnalUmum() {
		return $this->prosesLaporan( 'aunit/rGLJurnalUmum', '@aunit/views/report/Akuntansi/JurnalUmum.php' );
	}
	public function actionNeracaPercobaan() {
		return $this->prosesLaporan( 'aunit/rGLNeracaPercobaan', '@aunit/views/report/Akuntansi/NeracaPercobaan.php' );
	}
	public function actionNeracaRugiLaba() {
		return $this->prosesLaporan( 'aunit/rGLNeracaRugiLaba', '@aunit/views/report/Akuntansi/NeracaRugiLaba.php' );
	}
	public function actionLaporanHarian() {
		return $this->prosesLaporan( 'aunit/rGLHarian', '@aunit/views/report/Auditor/LaporanHarian' );
	}
	public function actionLaporanPiutang() {
		return $this->prosesLaporan( 'aunit/rGLPiutang', '@aunit/views/report/Auditor/LaporanPiutang' );
	}
	public function actionLaporanHutang() {
		return $this->prosesLaporan( 'aunit/rGLHutang', '@aunit/views/report/Auditor/LaporanHutang' );
	}
	public function actionDashboardAkuntansi() {
		return $this->prosesLaporan( 'aunit/', '@aunit/views/report/Auditor/DashboardAkuntansi' );
	}
	public function actionDaftarStockMotorAcct() {
		return $this->prosesLaporan( 'aunit/rGLStockMotor', '@aunit/views/report/Motor/DaftarStockMotorAcct' );
	}
	public function actionDaftarMotor() {
		return $this->prosesLaporan( 'aunit/rMotorDaftarMotor', '@aunit/views/report/Motor/DaftarMotor' );
	}
	public function actionDaftarStnkBpkb() {
		return $this->prosesLaporan( 'aunit/rMotorSTNKBPKB', '@aunit/views/report/Motor/DaftarSTNKBPKB' );
	}
	public function actionDaftarRiwayatMotor() {
		return $this->prosesLaporan( 'aunit/rMotorRiwayatMotor', '@aunit/views/report/Motor/DaftarRiwayatMotor' );
	}
	public function actionDaftarStockMotorFisik() {
		return $this->prosesLaporan( 'aunit/rMotorStockMotorFisik', '@aunit/views/report/Motor/DaftarStockMotorFisik' );
	}
	public function actionRekapHarian() {
		return $this->prosesLaporan( 'aunit/rMarketRekapHarian', '@aunit/views/report/Market/RekapHarian' );
	}
	public function actionRekapBulanan() {
		return $this->prosesLaporan( 'aunit/rMarketRekapBulanan', '@aunit/views/report/Market/RekapBulanan' );
	}
	public function actionRekapPivotTabel() {
		return $this->prosesLaporan( 'aunit/rMarketPivotTabel', '@aunit/views/report/Market/RekapPivotTabel' );
	}
	public function actionDashboard1() {
		return $this->prosesLaporan( 'aunit/rMarketDashboard1', '@aunit/views/report/Market/Dashboard1' );
	}
	public function actionDashboard2() {
		return $this->prosesLaporan( 'aunit/rMarketDashboard2', '@aunit/views/report/Market/Dashboard2' );
	}
	public function actionDashboard3() {
		return $this->prosesLaporan( 'aunit/rMarketDashboard3', '@aunit/views/report/Market/Dashboard3' );
	}
	public function actionLaporanAstra() {
		return $this->prosesLaporan( 'aunit/rAstra', '@aunit/views/report/Astra/LaporanAstra' );
	}
	public function actionLaporanQuery() {
		return $this->render( '@aunit/views/report/Astra/LaporanQuery' );
	}
	public function actionLaporanData() {
		return $this->prosesLaporan( 'aunit/rdata', '@aunit/views/report/Data/LaporanData' );
	}
	public function actionLaporanUserLog() {
		return $this->prosesLaporan( 'aunit/ruserlog', '@aunit/views/report/Data/LaporanUserLog' );
	}
	public function actionQuery() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$post                        = Yii::$app->request->post();
		$query                       = \Yii::$app->db->createCommand(base64_decode($post['q']))->queryAll();
		$columns                     = [];
		if ( sizeof( $query ) > 0 ) {
			$columns_arr = array_keys( $query[ 0 ] );
			foreach ($columns_arr as $item){
				$columns[] = ['text'=> $item,'datafield'=>$item];
			}
		}
		return [
			'columns' => $columns,
			'rows'    => $query
		];
	}
}








