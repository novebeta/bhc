<?php

namespace afinance\controllers;

use afinance\components\AfinanceController;
use afinance\models\Tmotor;
use Yii;
use afinance\models\Ttsmit;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TtsmitController implements the CRUD actions for Ttsmit model.
 */
class TtsmitController extends AfinanceController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
	/**
	 * Lists all Ttsmit models.
	 * @return mixed
	 * @throws \yii\base\InvalidConfigException
	 */
    public function actionIndex()
    {
	    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	    $requestData                 = \Yii::$app->request->post();
	    $primaryKeys = Ttsmit::getTableSchema()->primaryKey;
	    if ( isset( $requestData[ 'oper' ] ) ) {
		    /* operation : create, edit or delete */
		    $oper  = $requestData[ 'oper' ];
		    switch ( $oper ) {
			    case 'add'  :
				    $model = Tmotor::findOne([$requestData['MotorAutoN'],$requestData['MotorNoMesin']]);
				    $model->attributes = $requestData;
				    $response          = $model->save() ?
					    $this->responseSuccess( 'Data berhasil disimpan.' ) :
					    $this->responseFailed( $this->modelErrorSummary( $model ) );
				    break;
			    case 'edit' :
				    $model = $this->findModelBase64( 'tmotor', $requestData[ 'id' ] );
				    $model->attributes = $requestData;
//					if ( $oper === 'add' ) {
//						if ( $requestData[ 'SSNo' ] == '--' ) {
//							$count = ( new \yii\db\Query() )
//								->from( Tmotor::tableName() )
//								->where( [ 'FBNo' => $requestData[ 'FBNo' ] ] )
//								->max( 'MotorAutoN' );
//							if ( ! $count ) {
//								$count = 0;
//							}
//							$model->MotorAutoN = ++ $count;
//						} else {
//							$model->MotorAutoN = 0;
//						}
//					}
				    $response = $model->save() ?
					    $this->responseSuccess( 'Data berhasil disimpan.' ) :
					    $this->responseFailed( $this->modelErrorSummary( $model ) );
				    break;
			    case 'del'  :
				    $response = $model->delete() ?
					    $this->responseSuccess( 'Data berhasil dihapus.' ) :
					    $this->responseFailed( $this->modelErrorSummary( $model ) );
				    break;
		    }
	    } else {
		    for ($i = 0; $i < count($primaryKeys); $i++)
			    $primaryKeys[$i] = 'ttsmit.'.$primaryKeys[$i];
		    $query = ( new \yii\db\Query() )
			    ->select( new Expression(
				    'CONCAT(' . implode( ',"||",',$primaryKeys ) . ') AS id,
								ttsmit.SMNo,	ttsmit.MotorNoMesin,	ttsmit.MotorAutoN,	tmotor.MotorType,	tmotor.MotorTahun,
								tmotor.MotorWarna,	tmotor.MotorNoRangka,	tmotor.FBHarga,	tdmotortype.MotorNama'
			    ) )
			    ->from( 'ttsmit' )
			    ->join( 'INNER JOIN', 'tmotor', 'ttsmit.MotorNoMesin = tmotor.MotorNoMesin 
						AND ttsmit.MotorAutoN = tmotor.MotorAutoN' )
			    ->join( 'LEFT OUTER JOIN', 'tdmotortype', 'tmotor.MotorType = tdmotortype.MotorType' )
			    ->where(['ttsmit.SMNo'=>$requestData[ 'SMNo' ]]);
		    $rowsCount = $query->count();
		    $rows      = $query->all();
		    /* encode row id */
		    for ( $i = 0; $i < count( $rows ); $i ++ ) {
			    $rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
		    }
		    $response = [
			    'rows'      => $rows,
			    'rowsCount' => $rowsCount
		    ];
	    }
	    return $response;
    }

    /**
     * Displays a single Ttsmit model.
     * @param string $SMNo
     * @param string $MotorNoMesin
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($SMNo, $MotorNoMesin)
    {
        return $this->render('view', [
            'model' => $this->findModel($SMNo, $MotorNoMesin),
        ]);
    }

    /**
     * Creates a new Ttsmit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ttsmit();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'SMNo' => $model->SMNo, 'MotorNoMesin' => $model->MotorNoMesin]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ttsmit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $SMNo
     * @param string $MotorNoMesin
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($SMNo, $MotorNoMesin)
    {
        $model = $this->findModel($SMNo, $MotorNoMesin);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'SMNo' => $model->SMNo, 'MotorNoMesin' => $model->MotorNoMesin]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ttsmit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $SMNo
     * @param string $MotorNoMesin
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($SMNo, $MotorNoMesin)
    {
        $this->findModel($SMNo, $MotorNoMesin)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ttsmit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $SMNo
     * @param string $MotorNoMesin
     * @return Ttsmit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($SMNo, $MotorNoMesin)
    {
        if (($model = Ttsmit::findOne(['SMNo' => $SMNo, 'MotorNoMesin' => $MotorNoMesin])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
