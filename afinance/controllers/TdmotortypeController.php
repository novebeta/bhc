<?php

namespace afinance\controllers;

use afinance\components\AfinanceController;
use afinance\components\TdAction;
use afinance\models\Tdarea;
use Yii;
use afinance\models\Tdmotortype;

use yii\db\Query;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\Custom;

/**
 * TdmotortypeController implements the CRUD actions for Tdmotortype model.
 */
class TdmotortypeController extends AfinanceController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
	public function actions()
	{
		return [
			'td' => [
				'class' => TdAction::className(),
				'model' => Tdmotortype::className(),
			],
		];
	}

	/**
	 *
	 */
	public function actionFind() {
		$params = \Yii::$app->request->post();
		$rows = [];

		if($params['mode'] === 'number') {
			$rows = Tdmotortype::findOne( $params[ 'id' ] )->toArray();
		}

		return $this->responseDataJson($rows);
	}
    /**
     * Lists all Tdmotortype models.
     * @return mixed
     */
    public function actionIndex()
    {

        return $this->render('index');
    }

    /**
     * Displays a single Tdmotortype model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tdmotortype model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tdmotortype();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect( [ 'tdmotorwarna/index','id'=> $this->generateIdBase64FromModel($model)  ] );
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tdmotortype model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModelBase64('Tdmotortype',$id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect( [ 'tdmotorwarna/index','id'=> $this->generateIdBase64FromModel($model)  ] );
        }

        return $this->render('update', [
            'model' => $model,
            'id'    => $this->generateIdBase64FromModel($model),
        ]);
    }

    /**
     * Deletes an existing Tdmotortype model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

	public function actionMotorTypeList($q = null) {
		$query = new Query;
		$query->select('MotorType')
		      ->from('tdmotortype')
		      ->where('MotorType LIKE "%' . $q .'%"')
		      ->orderBy('MotorType');
		$command = $query->createCommand();
		$data = $command->queryAll();
		$out = [];
		foreach ($data as $d) {
			$out[] = ['value' => $d['MotorType']];
		}
		echo Json::encode($out);
	}

    /**
     * Finds the Tdmotortype model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Tdmotortype the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tdmotortype::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
