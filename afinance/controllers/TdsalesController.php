<?php
namespace afinance\controllers;
use afinance\components\AfinanceController;
use afinance\components\TdAction;
use afinance\models\Tdsales;

use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TdsaleController implements the CRUD actions for Tdsale model.
 */
class TdsalesController extends AfinanceController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actions() {
		return [
			'td' => [
				'class' => TdAction::className(),
				'model' => Tdsales::className(),
			],
		];
	}
	/**
	 * Lists all Tdsale models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index');
	}
	/**
	 * Displays a single Tdsale model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Finds the Tdsale model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Tdsales the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Tdsales::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Tdsale model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Tdsales();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'index', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Tdsale model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$model = $this->findModelBase64( 'Tdsale', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'index', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
		return $this->render( 'update', [
			'model' => $model,
			'id'    => $this->generateIdBase64FromModel( $model ),
		] );
	}
	/**
	 *
	 */
	public function actionFind() {
		$params = \Yii::$app->request->post();
		$rows   = [];
		if ( $params[ 'mode' ] === 'combo' ) {
			$value     = isset( $params[ 'value' ] ) ? $params[ 'value' ] : 'SalesKode';
			$label     = isset( $params[ 'label' ] ) ? $params[ 'label' ] : [ 'SalesNama' ];
			$order     = isset( $params[ 'order' ] ) ? $params[ 'order' ] : [];
			$condition = isset( $params[ 'condition' ] ) ? $params[ 'condition' ] : [];
			$params_cmb    = isset( $params[ 'params' ] ) ? $params[ 'params' ] : [];
			$allOption = isset( $params[ 'allOption' ] ) ? $params[ 'allOption' ] : false;
			$rows      = Tdsales::find()->select2( $value, $label, $order, [ 'condition' => $condition, 'params' => $params_cmb ], $allOption );
		}
		return $this->responseDataJson( $rows );
	}
	/**
	 * Deletes an existing Tdsale model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
}
