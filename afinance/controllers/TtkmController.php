<?php

namespace afinance\controllers;

use afinance\components\AfinanceController;
use afinance\components\TdAction;
use Yii;
use afinance\models\Ttkm;

use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TtkmController implements the CRUD actions for Ttkm model.
 */
class TtkmController extends AfinanceController
{
	public function actions() {
		$colGrid  = null;
		$joinWith = [];
		$join     = [];
		$where    = [];
		if ( isset( $_POST['query'] ) ) {
			$r = Json::decode( $_POST['query'], true )['r'];
			switch ( $r ) {
				case 'ttkm/kas-masuk-inden':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'Inden')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-pelunasan-leasing':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'Leasing')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-uang-muka-kredit':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'Kredit')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-bayar-piutang-kon-um':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'Piutang UM')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-uang-muka-tunai':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'Tunai')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-bayar-sisa-piutang':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'Piutang Sisa')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-bbn-progresif':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'BBN Plus')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-bbn-acc':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'BBN Acc')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-dari-bank':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'KM Dari Bank')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-umum':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'Umum')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-dari-bengkel':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'KM Dari Bengkel')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-angsuran-karyawan':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'Angsuran Karyawan')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-pos-dari-dealer':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'KM Pos Dari Dealer')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-dealer-dari-pos':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'KM Dealer Dari Pos')",
							'params'    => []
						]
					];
					break;
			}
		}
		return [
			'td' => [
				'class'    => TdAction::className(),
				'model'    => Ttkm::className(),
				'colGrid'  => $colGrid,
				'joinWith' => $joinWith,
				'join'     => $join,
				'where'    => $where
			],
		];
	}
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ttkm models.
     * @return mixed
     */
    public function actionIndex()
    {



        return $this->render('index');
    }
    /**
     * Lists all Ttkm models.
     * @return mixed
     */
    public function actionKasMasukInden()
    {



        return $this->render('KasMasukInden');
    }

        /**
         * @return string
         */
        public function actionKasMasukIndenCreate() {
            return $this->create('Inden', 'kas-masuk-inden', 'KAS MASUK - UM Inden');
        }

        /**
         * @param $id
         * @param $action
         * @return string|\yii\web\Response
         * @throws NotFoundHttpException
         */
        public function actionKasMasukIndenUpdate($id ) {
            return $this->update($id, 'kas-masuk-inden', 'KAS MASUK - UM Inden');
        }

    /**
     * Lists all Ttkm models.
     * @return mixed
     */
    public function actionKasMasukPelunasanLeasing()
    {



        return $this->render('KasMasukPelunasanLeasing');
    }

        /**
         * @return string
         */
        public function actionKasMasukPelunasanLeasingCreate() {
            return $this->create('Leasing', 'kas-masuk-pelunasan-leasing', 'KAS MASUK - Pelunasan Leasing');
        }

        /**
         * @param $id
         * @param $action
         * @return string|\yii\web\Response
         * @throws NotFoundHttpException
         */
        public function actionKasMasukPelunasanLeasingUpdate($id ) {
            return $this->update($id, 'kas-masuk-pelunasan-leasing', 'KAS MASUK - Pelunasan Leasing');
        }

    /**
     * Lists all Ttkm models.
     * @return mixed
     */
    public function actionKasMasukUangMukaKredit()
    {



        return $this->render('KasMasukUangMukaKredit');
    }

        /**
         * @return string
         */
        public function actionKasMasukUangMukaKreditCreate() {
            return $this->create('Kredit', 'kas-masuk-uang-muka-kredit', 'KAS MASUK - Uang Muka Kredit');
        }

        /**
         * @param $id
         * @param $action
         * @return string|\yii\web\Response
         * @throws NotFoundHttpException
         */
        public function actionKasMasukUangMukaKreditUpdate($id ) {
            return $this->update($id, 'kas-masuk-uang-muka-kredit', 'KAS MASUK - Uang Muka Kredit');
        }

    /**
     * Lists all Ttkm models.
     * @return mixed
     */
    public function actionKasMasukBayarPiutangKonUm()
    {



        return $this->render('KasMasukBayarPiutangKonUm');
    }

        /**
         * @return string
         */
        public function actionKasMasukBayarPiutangKonUmCreate() {
            return $this->create('Piutang UM', 'kas-masuk-bayar-piutang-kon-um', 'KAS MASUK - Piutang Konsumen UM');
        }

        /**
         * @param $id
         * @param $action
         * @return string|\yii\web\Response
         * @throws NotFoundHttpException
         */
        public function actionKasMasukBayarPiutangKonUmUpdate($id ) {
            return $this->update($id, 'kas-masuk-bayar-piutang-kon-um', 'KAS MASUK - Piutang Konsumen UM');
        }

    /**
     * Lists all Ttkm models.
     * @return mixed
     */
    public function actionKasMasukUangMukaTunai()
    {



        return $this->render('KasMasukUangMukaTunai');
    }

        /**
         * @return string
         */
        public function actionKasMasukUangMukaTunaiCreate() {
            return $this->create('Tunai', 'kas-masuk-uang-muka-tunai', 'KAS MASUK - Uang Muka Tunai');
        }

        /**
         * @param $id
         * @param $action
         * @return string|\yii\web\Response
         * @throws NotFoundHttpException
         */
        public function actionKasMasukUangMukaTunaiUpdate($id ) {
            return $this->update($id, 'kas-masuk-uang-muka-tunai', 'KAS MASUK - Uang Muka Tunai');
        }

    /**
     * Lists all Ttkm models.
     * @return mixed
     */
    public function actionKasMasukBayarSisaPiutang()
    {



        return $this->render('KasMasukBayarSisaPiutang');
    }

        /**
         * @return string
         */
        public function actionKasMasukBayarSisaPiutangCreate() {
            return $this->create('Piutang Sisa', 'kas-masuk-bayar-sisa-piutang', 'KAS MASUK - Bayar Sisa Piutang');
        }

        /**
         * @param $id
         * @param $action
         * @return string|\yii\web\Response
         * @throws NotFoundHttpException
         */
        public function actionKasMasukBayarSisaPiutangUpdate($id ) {
            return $this->update($id, 'kas-masuk-bayar-sisa-piutang', 'KAS MASUK - Bayar Sisa Piutang');
        }

    /**
     * Lists all Ttkm models.
     * @return mixed
     */
    public function actionKasMasukBbnProgresif()
    {



        return $this->render('KasMasukBbnProgresif');
    }

        /**
         * @return string
         */
        public function actionKasMasukBbnProgresifCreate() {
            return $this->create('BBN Plus', 'kas-masuk-bbn-progresif', 'KAS MASUK - BBN Progresif');
        }

        /**
         * @param $id
         * @param $action
         * @return string|\yii\web\Response
         * @throws NotFoundHttpException
         */
        public function actionKasMasukBbnProgresifUpdate($id ) {
            return $this->update($id, 'kas-masuk-bbn-progresif', 'KAS MASUK - BBN Progresif');
        }

    /**
     * Lists all Ttkm models.
     * @return mixed
     */
    public function actionKasMasukBbnAcc()
    {



        return $this->render('KasMasukBbnAcc');
    }

        /**
         * @return string
         */
        public function actionKasMasukBbnAccCreate() {
            return $this->create('BBN Acc', 'kas-masuk-bbn-acc', 'KAS MASUK - BBN Acc');
        }

        /**
         * @param $id
         * @param $action
         * @return string|\yii\web\Response
         * @throws NotFoundHttpException
         */
        public function actionKasMasukBbnAccUpdate($id ) {
            return $this->update($id, 'kas-masuk-bbn-acc', 'KAS MASUK - BBN Acc');
        }

    /**
     * Lists all Ttkm models.
     * @return mixed
     */
    public function actionKasMasukDariBank()
    {



        return $this->render('KasMasukDariBank');
    }

        /**
         * @return string
         */
        public function actionKasMasukDariBankCreate() {
            return $this->create('KM Dari Bank', 'kas-masuk-dari-bank', 'KAS MASUK - Dari Bank');
        }

        /**
         * @param $id
         * @param $action
         * @return string|\yii\web\Response
         * @throws NotFoundHttpException
         */
        public function actionKasMasukDariBankUpdate($id ) {
            return $this->update($id, 'kas-masuk-dari-bank', 'KAS MASUK - Dari Bank');
        }

    /**
     * Lists all Ttkm models.
     * @return mixed
     */
    public function actionKasMasukUmum()
    {



        return $this->render('KasMasukUmum');
    }

        /**
         * @return string
         */
        public function actionKasMasukUmumCreate() {
//            return $this->create('Umum', 'kas-masuk-umum', 'KAS MASUK - Umum');
            $model = new Ttkm();
            if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
                return $this->redirect( [ 'view', 'id' => $model->KMNo ] );
            }
            return $this->render( 'kas-masuk-umum-create', [
                'model' => $model,
            ] );
        }

        /**
         * @param $id
         * @param $action
         * @return string|\yii\web\Response
         * @throws NotFoundHttpException
         */
        public function actionKasMasukUmumUpdate($id ) {
            return $this->update($id, 'kas-masuk-umum', 'KAS MASUK - Umum');
        }

    /**
     * Lists all Ttkm models.
     * @return mixed
     */
    public function actionKasMasukDariBengkel()
    {



        return $this->render('KasMasukDariBengkel');
    }

        /**
         * @return string
         */
        public function actionKasMasukDariBengkelCreate() {
            return $this->create('KM Dari Bengkel', 'kas-masuk-dari-bengkel', 'KAS MASUK - Dari Bengkel');
        }

        /**
         * @param $id
         * @param $action
         * @return string|\yii\web\Response
         * @throws NotFoundHttpException
         */
        public function actionKasMasukDariBengkelUpdate($id ) {
            return $this->update($id, 'kas-masuk-dari-bengkel', 'KAS MASUK - Dari Bengkel');
        }

    /**
     * Lists all Ttkm models.
     * @return mixed
     */
    public function actionKasMasukAngsuranKaryawan()
    {



        return $this->render('KasMasukAngsuranKaryawan');
    }

        /**
         * @return string
         */
        public function actionKasMasukAngsuranKaryawanCreate() {
            return $this->create('Angsuran Karyawan', 'kas-masuk-angsuran-karyawan', 'KAS MASUK - Angsuran Karyawan');
        }

        /**
         * @param $id
         * @param $action
         * @return string|\yii\web\Response
         * @throws NotFoundHttpException
         */
        public function actionKasMasukAngsuranKaryawanUpdate($id ) {
            return $this->update($id, 'kas-masuk-angsuran-karyawan', 'KAS MASUK - Angsuran Karyawan');
        }

    /**
     * Lists all Ttkm models.
     * @return mixed
     */
    public function actionKasMasukPosDariDealer()
    {



        return $this->render('KasMasukPosDariDealer');
    }

        /**
         * @return string
         */
        public function actionKasMasukPosDariDealerCreate() {
            return $this->create('KM Pos Dari Dealer', 'kas-masuk-pos-dari-dealer', 'KAS MASUK - Pos Dari Dealer');
        }

        /**
         * @param $id
         * @param $action
         * @return string|\yii\web\Response
         * @throws NotFoundHttpException
         */
        public function actionKasMasukPosDariDealerUpdate($id ) {
            return $this->update($id, 'kas-masuk-pos-dari-dealer', 'KAS MASUK - Pos Dari Dealer');
        }

    /**
     * Lists all Ttkm models.
     * @return mixed
     */
    public function actionKasMasukDealerDariPos()
    {



        return $this->render('KasMasukDealerDariPos');
    }

        /**
         * @return string
         */
        public function actionKasMasukDealerDariPosCreate() {
            return $this->create('KM Dealer Dari Pos', 'kas-masuk-dealer-dari-pos', 'KAS MASUK - Dealer Dari Pos');
        }

        /**
         * @param $id
         * @param $action
         * @return string|\yii\web\Response
         * @throws NotFoundHttpException
         */
        public function actionKasMasukDealerDariPosUpdate($id ) {
            return $this->update($id, 'kas-masuk-dealer-dari-pos', 'KAS MASUK - Dealer Dari Pos');
        }


    /**
     * Displays a single Ttkm model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @param $KMJenis
     * @param $view
     * @param $title
     * @return string
     */
    private function create($KMJenis, $view, $title) {
        $model            = new Ttkm();
        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ $view ] );
        }

        $model->KMNo      = Ttkm::generateKMNo( true );
        $model->KMTgl     = date( "Y-m-d" );
        $model->KMJam     = date( "Y-m-d H:i:s" );
        $model->KMJenis   = $KMJenis;

        return $this->render( 'create', [
            'model' => $model,
            'view'  => $view,
            'title'  => $title
        ] );
    }

    /**
     * @param $id
     * @param $view
     * @param $title
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    private function update($id, $view, $title) {
        $model = $this->findModelBase64( 'Ttkm', $id );
        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ $view ] );
        }
        return $this->render( 'update', [
            'model' => $model,
            'id'    => $id,
            'view'  => $view,
            'title'  => $title
        ] );
    }

    /**
     * Deletes an existing Ttkm model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ttkm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Ttkm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ttkm::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
