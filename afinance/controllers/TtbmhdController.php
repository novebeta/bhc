<?php

namespace afinance\controllers;

use afinance\components\AfinanceController;
use afinance\components\TdAction;
use afinance\models\Ttbmit;
use Yii;
use afinance\models\Ttbmhd;

use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TtbmhdController implements the CRUD actions for Ttbmhd model.
 */
class TtbmhdController extends AfinanceController
{

	public function actions() {
		$colGrid  = null;
		$joinWith = [
			'leasing'      => [
				'type' => 'INNER JOIN'
			],
			'account' => [
				'type' => 'INNER JOIN'
			],
		];
		$join     = [];
		$where    = [];
		if ( isset( $_POST['query'] ) ) {
			$r = Json::decode( $_POST['query'], true )['r'];
			switch ( $r ) {
				case 'ttbmhd/bank-masuk-inden':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'Inden')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-leasing':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'Leasing')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-scheme':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'Scheme')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-uang-muka-kredit':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'Kredit')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-piutang-kons-um':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'Piutang UM')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-uang-muka-tunai':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'Tunai')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-sisa-piutang-kons':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'Piutang Sisa')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-bbn-progresif':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'BBN Plus')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-dari-kas':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'BM Dari Kas')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-umum':
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'Umum')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-penjualan-multi':
					$joinWith = [
						'account' => [
							'type' => 'INNER JOIN'
						],
					];
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'Penjualan Multi')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-penjualan-multi-pl':
					$joinWith = [
						'account' => [
							'type' => 'INNER JOIN'
						],
					];
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'Penjualan Multi')",
							'params'    => []
						]
					];
					break;
			}
		}
		return [
			'td' => [
				'class'    => TdAction::className(),
				'model'    => Ttbmhd::className(),
				'colGrid'  => $colGrid,
				'joinWith' => $joinWith,
				'join'     => $join,
				'where'    => $where
			],
		];
	}

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ttbmhd models.
     * @return mixed
     */
    public function actionBankMasukInden()
    {

        return $this->render('BankMasukInden');
    }

            /**
             * @return string
             */
            public function actionBankMasukIndenCreate() {
                return $this->create('Inden', 'bank-masuk-inden', 'BANK MASUK - Inden');
            }

            /**
             * @param $id
             * @param $action
             * @return string|\yii\web\Response
             * @throws NotFoundHttpException
             */
            public function actionBankMasukIndenUpdate($id, $action ) {
                return $this->update($id, $action, 'bank-masuk-inden', 'BANK MASUK - Inden');
            }

            /**
             * @param $id
             * @param $action
             * @return \yii\web\Response
             * @throws NotFoundHttpException
             * @throws \Throwable
             * @throws \yii\db\StaleObjectException
             */
            public function actionBankMasukIndenCancel($id, $action ) {
                return $this->cancel($id, $action, 'bank-masuk-inden');
            }

    /**
     * Lists all Ttbmhd models.
     * @return mixed
     */
    public function actionBankMasukLeasing()
    {

        return $this->render('BankMasukLeasing');
    }

            /**
             * @return string
             */
            public function actionBankMasukLeasingCreate() {
                return $this->create('Leasing', 'bank-masuk-leasing', 'BANK MASUK - Pelunasan Leasing');
            }

            /**
             * @param $id
             * @param $action
             * @return string|\yii\web\Response
             * @throws NotFoundHttpException
             */
            public function actionBankMasukLeasingUpdate($id, $action ) {
                return $this->update($id, $action, 'bank-masuk-leasing', 'BANK MASUK - Pelunasan Leasing');
            }

            /**
             * @param $id
             * @param $action
             * @return \yii\web\Response
             * @throws NotFoundHttpException
             * @throws \Throwable
             * @throws \yii\db\StaleObjectException
             */
            public function actionBankMasukLeasingCancel($id, $action ) {
                return $this->cancel($id, $action, 'bank-masuk-leasing');
            }

    /**
     * Lists all Ttbmhd models.
     * @return mixed
     */
    public function actionBankMasukScheme()
    {



        return $this->render('BankMasukScheme');
    }

            /**
             * @return string
             */
            public function actionBankMasukSchemeCreate() {
                return $this->create('Scheme', 'bank-masuk-scheme', 'BANK MASUK - Pelunasan Scheme');
            }

            /**
             * @param $id
             * @param $action
             * @return string|\yii\web\Response
             * @throws NotFoundHttpException
             */
            public function actionBankMasukSchemeUpdate($id, $action ) {
                return $this->update($id, $action, 'bank-masuk-scheme', 'BANK MASUK - Pelunasan Scheme');
            }

            /**
             * @param $id
             * @param $action
             * @return \yii\web\Response
             * @throws NotFoundHttpException
             * @throws \Throwable
             * @throws \yii\db\StaleObjectException
             */
            public function actionBankMasukSchemeCancel($id, $action ) {
                return $this->cancel($id, $action, 'bank-masuk-scheme');
            }

    /**
     * Lists all Ttbmhd models.
     * @return mixed
     */
    public function actionBankMasukUangMukaKredit()
    {



        return $this->render('BankMasukUangMukaKredit');
    }

            /**
             * @return string
             */
            public function actionBankMasukUangMukaKreditCreate() {
                return $this->create('Kredit', 'bank-masuk-uang-muka-kredit', 'BANK MASUK - Uang Muka Kredit');
            }

            /**
             * @param $id
             * @param $action
             * @return string|\yii\web\Response
             * @throws NotFoundHttpException
             */
            public function actionBankMasukUangMukaKreditUpdate($id, $action ) {
                return $this->update($id, $action, 'bank-masuk-uang-muka-kredit', 'BANK MASUK - Uang Muka Kredit');
            }

            /**
             * @param $id
             * @param $action
             * @return \yii\web\Response
             * @throws NotFoundHttpException
             * @throws \Throwable
             * @throws \yii\db\StaleObjectException
             */
            public function actionBankMasukUangMukaKreditCancel($id, $action ) {
                return $this->cancel($id, $action, 'bank-masuk-uang-muka-kredit');
            }

    /**
     * Lists all Ttbmhd models.
     * @return mixed
     */
    public function actionBankMasukUangMukaTunai()
    {



        return $this->render('BankMasukUangMukaTunai');
    }

    /**
     * @return string
     */
    public function actionBankMasukUangMukaTunaiCreate() {
        return $this->create('Kredit', 'bank-masuk-uang-muka-tunai', 'BANK MASUK - Uang Muka Tunai');
    }

    /**
     * Lists all Ttbmhd models.
     * @return mixed
     */
    public function actionBankMasukPiutangKonsUm()
    {



        return $this->render('BankMasukPiutangKonsUm');
    }

    public function actionBankMasukPiutangKonsUmCreate()
    {
        return $this->create('Bank Masuk Piutang Konsumen UM', 'bank-masuk-piutang-kons-um', 'BANK MASUK - Piutang Konsumen UM');
    }
    /**
     * Lists all Ttbmhd models.
     * @return mixed
     */
    public function actionBankMasukSisaPiutangKons()
    {



        return $this->render('BankMasukSisaPiutangKons');
    }
    public function actionBankMasukSisaPiutangKonsCreate()
    {
        return $this->create('Bank Masuk Sisa Piutang Kons', 'bank-masuk-sisa-piutang-kons', 'BANK MASUK - Sisa Piutang Kons');
    }
    /**
     * Lists all Ttbmhd models.
     * @return mixed
     */
    public function actionBankMasukBbnProgresif()
    {



        return $this->render('BankMasukBbnProgresif');
    }
    public function actionBankMasukBbnProgresifCreate()
    {
        return $this->create('Bank Masuk Bbn Progresif', 'bank-masuk-bbn-progresif', 'BANK MASUK - Bbn Progresif');
    }
    /**
     * Lists all Ttbmhd models.
     * @return mixed
     */
    public function actionBankMasukDariKas()
    {



        return $this->render('BankMasukDariKas');
    }
    public function actionBankMasukDariKasCreate() {
        return $this->create('Bank Masuk Dari Kas', 'bank-masuk-dari-kas', 'BANK MASUK DARI KAS');
    }
    /**
     * Lists all Ttbmhd models.
     * @return mixed
     */
    public function actionBankMasukUmum()
    {



        return $this->render('BankMasukUmum');
    }
    public function actionBankMasukUmumCreate() {
        $model = new Ttbmhd();
        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'ttbhmd/bank-masuk-umum', 'id' => $model->BMNo ] );
        }
        return $this->render( 'bank-masuk-umum-create', [
            'model' => $model,
        ] );
    }
    /**
     * Lists all Ttbmhd models.
     * @return mixed
     */
    public function actionBankMasukPenjualanMulti()
    {



        return $this->render('BankMasukPenjualanMulti');
    }
    public function actionBankMasukPenjualanMultiCreate() {
        $model = new Ttbmhd();
        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'ttbhmd/bank-masuk-penjualan-multi', 'id' => $model->BMNo ] );
        }
        return $this->render( 'bank-masuk-penjualan-multi-create', [
            'model' => $model,
        ] );
    }
    /**
     * Lists all Ttbmhd models.
     * @return mixed
     */
    public function actionBankMasukPenjualanMultiPl()
    {



        return $this->render('BankMasukPenjualanMultiPl');
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function actionDetail() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $requestData                 = \Yii::$app->request->post();
        $primaryKeys = Ttbmit::getTableSchema()->primaryKey;
        if ( isset( $requestData['oper'] ) ) {
            /* operation : create, edit or delete */
            $oper = $requestData['oper'];
            $model = $oper === 'add' ? new Ttbmit() :
                $this->findModelBase64( 'Ttbmit', $requestData['id'] );
            switch ( $oper ) {
                case 'add'  :
                case 'edit' :
                    $model->attributes = $requestData;
                    $response = $model->save() ?
                        $this->responseSuccess( 'Data berhasil disimpan.' ) :
                        $this->responseFailed( $this->modelErrorSummary( $model ) );
                    break;
                case 'del'  :
                    $response = $model->delete() ?
                        $this->responseSuccess( 'Data berhasil dihapus.' ) :
                        $this->responseFailed( $this->modelErrorSummary( $model ) );
                    break;
            }
        } else {
            /* operation : read */
            $query = ( new \yii\db\Query() )
                ->select( [
                    'CONCAT(' . implode( ",'||',", $primaryKeys ) . ') AS id',
                    'ttbmit.*',
//                    'traccount.NamaAccount NamaAccount'
                ] )
                ->from( 'ttbmit' )
//                ->leftJoin( 'traccount', 'traccount.NoAccount = ttbmit.NoAccount' )
                ->where( [
                    'BMNo'  => $requestData['BMNo'],
//                    'DKNo' => $requestData['DKNo']
                ] );
            $rowsCount = $query->count();
            $rows      = $query->all();
            /* encode row id */
            for ( $i = 0; $i < count( $rows ); $i ++ ) {
                $rows[ $i ]['id'] = base64_encode( $rows[ $i ]['id'] );
            }
            $response = [
                'rows'      => $rows,
                'rowsCount' => $rowsCount
            ];
        }
        return $response;
    }

    /**
     * Displays a single Ttbmhd model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @param $KKJenis
     * @param $view
     * @param $title
     * @return string
     */
    private function create($BMJenis, $view, $title) {
        $model            = new Ttbmhd();
        $model->BMJenis   = $BMJenis;
        $model->save();
        $id = $this->generateIdBase64FromModel( $model );
        return $this->render( 'create', [
            'model' => $model,
            'id'    => $id,
            'view'  => $view,
            'title'  => $title
        ] );
    }

    /**
     * @param $id
     * @param $action
     * @param $view
     * @param $title
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    private function update($id, $action, $view, $title) {
        $model = $this->findModelBase64( 'Ttbmhd', $id );
        $post = Yii::$app->request->post();
        if ( $action === 'create' ) {
            $bmNo = Ttbmhd::generateReference();
            /* data detail : update BMNo  */
            $modelDetails = Ttbmit::findAll( [
                'BMNo'  => $model->BMNo
            ] );
            foreach ( $modelDetails as $modelDetail ) {
                $modelDetail->BMNo = $bmNo;
                $modelDetail->save();
            }
            /* data header : update BMNo */
            $post['Ttbmhd']['BMNo'] = $bmNo;
        }

        Yii::$app->request->setBodyParams($post);
        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ $view ] );
        }
        return $this->render( 'update', [
            'model' => $model,
            'id'    => $id,
            'view'  => $view,
            'title'  => $title
        ] );
    }

    /**
     * @param $id
     * @param $action
     * @param $view
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    private function cancel($id, $action, $view) {
        if ( $action === 'create' ) {
            $model = $this->findModelBase64( 'Ttbmhd', $id );
            /* delete data detail  */
            $modelDetails = Ttbmit::findAll( [
                'BMNo'  => $model->BMNo
            ] );
            foreach ( $modelDetails as $modelDetail ) {
                $modelDetail->delete();
            }
            /* delete data header */
            $model->delete();
        }
        return $this->redirect( [ $view ] );
    }

    /**
     * Deletes an existing Ttbmhd model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ttbmhd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Ttbmhd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ttbmhd::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionCreate() {
        $model = new Ttbmhd();
        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'view', 'id' => $model->BMNo ] );
        }
        return $this->render( 'create', [
            'model' => $model,
        ] );
    }
}
