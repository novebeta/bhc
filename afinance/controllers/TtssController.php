<?php
namespace afinance\controllers;
use afinance\components\AfinanceController;
use afinance\components\TdAction;
use afinance\models\Tmotor;
use afinance\models\Ttss;
use common\components\Custom;
use common\components\General;
use common\components\Reference;
use Yii;
use yii\base\UserException;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
class TtssController extends AfinanceController {
	public function actions() {
		return [
			'td' => [
				'class'    => TdAction::className(),
				'model'    => Ttss::className(),
				'joinWith' => [
					'fBNos'    => [
						'type' => 'LEFT OUTER JOIN'
					],
					'supplier' => [
						'type' => 'LEFT OUTER JOIN'
					],
					'lokasi'   => [
						'type' => 'LEFT OUTER JOIN'
					],
				],
				'join'     => [
					[
						'type'   => 'LEFT OUTER JOIN',
						'table'  => '(SELECT COUNT(tmotor.MotorType) AS SSJum, tmotor.SSNo FROM tmotor GROUP BY SSNo) tmotor',
						'on'     => 'ttss.SSNo = tmotor.SSNo',
						'params' => []
					]
				]
			],
		];
	}
	public function actionIndex() {
		return $this->render( 'index', [
			'mode' => '' ,
			'url_add'=>Url::toRoute( [ 'ttss/td' ] )
		] );
	}

	/**
	 * Lists all Ttfb models.
	 * @return mixed
	 */
	public function actionSelect() {
		$this->layout = 'select-mode';
		return $this->render( 'index', [
			'mode' => self::MODE_SELECT_DETAIL_MULTISELECT,
			'url_add'=>Url::toRoute( [ 'ttss/td', 'jenis' => $_GET['jenis'] ] )
		] );
	}
	/**
	 * Displays a single Ttss model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Finds the Ttss model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttss the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttss::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttss model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 * @throws \Throwable
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionCreate() {
		$model             = new Ttss();
		$model->SSNo       = General::createTemporaryNo( 'SS', 'ttss', 'SSNo' );
		$model->SSNoTerima = General::createTemporaryNo( 'PS', 'ttss', 'SSNoTerima' );
		$model->SSTgl      = Yii::$app->formatter->asDate( 'now', 'yyyy-MM-dd' );
		$model->FBNo       = '--';
		$model->SSMemo     = '--';
		$model->DONo       = '--';
		$model->UserID     = $this->UserID();
		$model->save();
		$id                = $this->generateIdBase64FromModel( $model );
		$dsBeli            = Ttss::find()->dsTBeliFillByNo()->where( [ 'ttss.SSNo' => $model->SSNo ] )->asArray( true )->one();
//		$dsBeli[ 'detil' ] = Json::encode( Tmotor::find()->where( [ 'SSNo' => $model->SSNo ] )->asArray()->all() );
//		$model->delete();
		return $this->render( 'create', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'id'     => $id
		] );
	}
	/**
	 * Updates an existing Ttss model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws UserException
	 * @throws \yii\db\Exception
	 */
	public function actionUpdate( $id ) {
		/** @var Ttss $model */
		$model = $this->findModelBase64( 'Ttss', $id );
		if ( $model == null ) {
			$model = new Ttss;
		}
		$dsBeli = Ttss::find()->dsTBeliFillByNo()->where( [ 'ttss.SSNo' => $model->SSNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			if ( Yii::$app->request->isAjax ) {
				\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			}
			$transaction = Ttss::getDb()->beginTransaction();
			$post        = Yii::$app->request->post();
			try {
				$detil = Json::decode( $_POST[ 'detil' ] );
				if ( ! Yii::$app->request->isAjax ) {
					$msg = $this->CekDataGrid( $detil );
					if ( $msg != '' ) {
						throw new UserException( $msg );
					}
				}
				foreach ( $detil as $row ) {
					$JumMotor            = Ttss::find()->CekMesinNo( $row[ 'MotorNoMesin' ], $post[ 'SSNo' ] );
					$row[ 'MotorAutoN' ] = $JumMotor;
					$row[ 'SSNo' ]       = $post[ 'SSNo' ];
					$tmotor              = Tmotor::findOne( [ 'MotorNoMesin' => $row[ 'MotorNoMesin' ], 'MotorAutoN' => $row[ 'MotorAutoN' ] ] );
					if ( $tmotor == null ) {
						$tmotor       = new Tmotor;
						$tmotor->FBNo = '--';
						$tmotor->SDNo = '--';
						$tmotor->SKNo = '--';
						$tmotor->RKNo = '--';
					}
					if ( $tmotor->FBHarga == 0 || $tmotor->FBNo == '--' ) {
						$tmotor->FBHarga = $row[ 'SSHarga' ];
					}
					if ( $tmotor->SDHarga == 0 || $tmotor->SDNo == '--' ) {
						$tmotor->SDHarga = $row[ 'SSHarga' ];
					}
					if ( $tmotor->SKHarga == 0 || $tmotor->SKNo == '--' ) {
						$tmotor->SKHarga = $row[ 'SSHarga' ];
					}
					if ( $tmotor->RKHarga == 0 || $tmotor->RKNo == '--' ) {
						$tmotor->RKHarga = $row[ 'SSHarga' ];
					}
					$tmotor->load( $row, '' );
					$tmotor->save();
				}
				$model->load( $post, '' );
				if ( ! $model->save() ) {
					throw new UserException( Html::errorSummary( $model ) );
				}
				if ( ! Yii::$app->request->isAjax ) {
					if ( strpos( $post[ 'SSNoTerima' ], '=' ) !== false ) {
						$model->SSNoTerima = General::createNo( 'PS', 'ttss', 'SSNoTerima' );
					}
					if ( strpos( $post[ 'SSNoLama' ], '=' ) !== false && $post[ 'SSNoLama' ] == $post[ 'SSNo' ] ) {
						$model->SSNo = General::createNo( 'SS', 'ttss', 'SSNo' );
					}
					if ( $model->isAttributeChanged( 'SSNoTerima' ) || $model->isAttributeChanged( 'SSNo' ) ) {
						$model->save();
						Tmotor::updateAll( [ 'SSNo' => $model->SSNo ], [ 'SSNo' => $post[ 'SSNoLama' ] ] );
					}
				}
				$transaction->commit();
				if ( Yii::$app->request->isAjax ) {
					return [ 'success' => true, 'msg' => '' ];
				} else {
					return $this->redirect( [ 'ttss/index', 'id' => $this->generateIdBase64FromModel( $model ) ] );
				}
			} catch ( UserException $e ) {
				$transaction->rollBack();
				if ( Yii::$app->request->isAjax ) {
					return [ 'success' => false, 'msg' => $e->getMessage() ];
				} else {
					Yii::$app->session->setFlash( 'error', $e->getMessage() );
					return $this->render( 'update', [
						'model'  => $model,
						'dsBeli' => $post,
						'id'     => $id
					] );
				}
			}
//		}
//		Yii::$app->request->setBodyParams( $post );
//		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
//			return $this->redirect( [ 'index' ] );
		}
//		$dsBeli[ 'detil' ] = Json::encode( Tmotor::find()->where( [ 'SSNo' => $model->SSNo ] )->asArray()->all() );
		return $this->render( 'update', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'id'     => $id
		] );
	}
	protected function CekDataGrid( $detil ) {
		$pesanNoMesin  = [];
		$pesanNoRangka = [];
		$pesanHarga    = [];
		foreach ( $detil as $row ) {
			if ( $row[ 'SSHarga' ] == 0 ) {
				$pesanHarga[] = $row[ 'MotorType' ];
			}
			if ( strlen( $row[ 'MotorNoMesin' ] ) < 12 ) {
				$pesanNoMesin[] = $row[ 'MotorNoMesin' ];
			}
			if ( strlen( $row[ 'MotorNoRangka' ] ) < 17 ) {
				$pesanNoRangka[] = $row[ 'MotorNoRangka' ];
			}
		}
		$msg = '';
		if ( sizeof( $pesanNoMesin ) > 0 ) {
			$msg = "Berikut No Mesin yang kurang dari 12 karakter : " . implode( ', ', $pesanNoMesin );
		}
		if ( sizeof( $pesanNoRangka ) > 0 ) {
			$msg = "Berikut No Rangka yang kurang dari 17 karakter : " . implode( ', ', $pesanNoRangka );
		}
		if ( sizeof( $pesanHarga ) > 0 ) {
			$msg = "Berikut Type Motor yang belum memiliki HPP : " . implode( ', ', $pesanHarga ) . "\n Silahkan memberi nilai HPP melalui Menu Data - Type & Warna Motor";
		}
		return $msg;
	}
	/**
	 * Deletes an existing Ttss model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete( $id ) {
		$model = $model = $this->findModelBase64( 'Ttss', $id );
		/* delete data detail  */
		Tmotor::deleteAll( "SSNo = :SSNo AND  (FBNo <> '--' OR FBNo <> '' OR FBNo IS NULL)", [
			':SSNo' => $model->SSNo
		] );
		Tmotor::updateAll( ['SSNo'=>'--'],"SSNo = :SSNo AND (FBNo <> '--' OR FBNo <> '' OR FBNo IS NULL)", [
			':SSNo' => $model->SSNo
		] );
		$model->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * @return array
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDetail() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Tmotor::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper  = $requestData[ 'oper' ];
			$model = $oper === 'add' ? new Tmotor() :
				$this->findModelBase64( 'tmotor', $requestData[ 'id' ] );
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					$model->attributes = $requestData;
					if ( $oper === 'add' ) {
						$count = ( new \yii\db\Query() )
							->from( Tmotor::tableName() )
							->where( [ 'SSNo' => $requestData[ 'SSNo' ] ] )
							->max( 'MotorAutoN' );
						if ( ! $count ) {
							$count = 0;
						}
						$model->MotorAutoN = ++ $count;
					}
					$response = $model->save() ?
						$this->responseSuccess( 'Data berhasil disimpan.' ) :
						$this->responseFailed( $this->modelErrorSummary( $model ) );
					break;
				case 'del'  :
					$response = $model->delete() ?
						$this->responseSuccess( 'Data berhasil dihapus.' ) :
						$this->responseFailed( $this->modelErrorSummary( $model ) );
					break;
			}
		} else {
			/* operation : read */
			$query     = ( new \yii\db\Query() )
				->select( [
					'CONCAT(' . implode( ",'||',", $primaryKeys ) . ') AS id',
					'tmotor.*'
				] )
				->from( Tmotor::tableName() )
				->where( [
					'SSNo' => $requestData[ 'SSNo' ]
				] )
				->orderBy( 'MotorAutoN' );
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	/**
	 * @param $id
	 * @param $action
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionCancel( $id, $action ) {
		/** @var Ttss $model */
		$model = $this->findModelBase64( 'Ttss', $id );
		if ( $action == 'create' && $model != null && strpos( $model->SSNoTerima, '=' ) !== false ) {
			/* delete data detail  */
			Tmotor::deleteAll( "SSNo = :SSNo AND  (FBNo <> '--' OR FBNo <> '' OR FBNo IS NULL)", [
				':SSNo' => $model->SSNo
			] );
			Tmotor::updateAll( ['SSNo'=>'--'],"SSNo = :SSNo AND (FBNo <> '--' OR FBNo <> '' OR FBNo IS NULL)", [
				':SSNo' => $model->SSNo
			] );
			/* delete data header */
			$model->delete();
		}
		if ( Yii::$app->request->isAjax ) {
			return '';
		}
		return $this->redirect( [ 'index' ] );
	}
}
