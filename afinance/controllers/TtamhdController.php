<?php

namespace afinance\controllers;

use afinance\components\TdAction;
use afinance\models\Ttamit;
use Yii;
use afinance\models\Ttamhd;

use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TtamhdController implements the CRUD actions for Ttamhd model.
 */
class TtamhdController extends \afinance\components\AfinanceController
{
	public function actions() {
		return [
			'td' => [
				'class'    => TdAction::className(),
				'model'    => Ttamhd::className(),
			],
		];
	}
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ttamhd models.
     * @return mixed
     */
    public function actionIndex()
    {

        return $this->render('index');
    }

    /**
     * Displays a single Ttamhd model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function actionDetail() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $requestData                 = \Yii::$app->request->post();
        $primaryKeys = Ttamit::getTableSchema()->primaryKey;
        if ( isset( $requestData['oper'] ) ) {
            /* operation : create, edit or delete */
            $oper = $requestData['oper'];
            $model = $oper === 'add' ? new Ttamit() :
                $this->findModelBase64( 'Ttamit', $requestData['id'] );
            switch ( $oper ) {
                case 'add'  :
                case 'edit' :
                    $model->attributes = $requestData;
                    if ( $oper === 'add' ) {
                        $count = ( new \yii\db\Query() )
                            ->from( 'ttamit' )
                            ->where( [
                                'AMNo'  => $requestData['AMNo']
                            ] )->max( 'MotorAutoN' );
                        if ( ! $count ) {
                            $count = 0;
                        }
                        $model->MotorAutoN = ++ $count;
                    }
                    $response = $model->save() ?
                        $this->responseSuccess( 'Data berhasil disimpan.' ) :
                        $this->responseFailed( $this->modelErrorSummary( $model ) );
                    break;
                case 'del'  :
                    $response = $model->delete() ?
                        $this->responseSuccess( 'Data berhasil dihapus.' ) :
                        $this->responseFailed( $this->modelErrorSummary( $model ) );
                    break;
            }
        } else {
            /* operation : read */
            for ($i = 0; $i < count($primaryKeys); $i++)
                $primaryKeys[$i] = 'ttamit.'.$primaryKeys[$i];

            $query = ( new \yii\db\Query() )
                ->select( [
                    'CONCAT(' . implode( ",'||',", $primaryKeys ) . ') AS id',
                    'ttamit.*',
                    'tsaldomotor.MotorTahun',
                    'tsaldomotor.MotorNoRangka',
                    'tsaldomotor.MotorType',
                    'IFNULL(tsaldomotor.MotorHarga, 0) AS MotorHarga',
                ] )
                ->from( 'ttamit' )
                ->leftJoin('tsaldomotor', 'tsaldomotor.MotorNoMesin = ttamit.MotorNoMesin')
                ->where( [
                    'AMNo'  => $requestData['AMNo']
                ] )
                ->orderBy('MotorAutoN');
            $rowsCount = $query->count();
            $rows      = $query->all();
            /* encode row id */
            for ( $i = 0; $i < count( $rows ); $i ++ ) {
                $rows[ $i ]['id'] = base64_encode( $rows[ $i ]['id'] );
            }
            $response = [
                'rows'      => $rows,
                'rowsCount' => $rowsCount
            ];
        }
        return $response;
    }

    /**
     * Creates a new Ttamhd model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ttamhd();
        $model->AMNo      = Ttamhd::generateReference(true );
        $model->MotorTahun = date('Y');
        $model->save();
        $id = $this->generateIdBase64FromModel( $model );
        return $this->render('create', [
            'model' => $model,
            'id'    => $id
        ]);
    }

    /**
     * Updates an existing Ttamhd model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @param string $action
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $action)
    {
        $model = $this->findModelBase64( 'Ttamhd', $id );
        $post = Yii::$app->request->post();
        if ( $action === 'create' ) {
            $AMNo = Ttamhd::generateReference();
            /* data detail : update AMNo  */
            Ttamit::updateAll(
                [ 'AMNo'  => $AMNo ],
                'AMNo = :AMNo', [ ':AMNo'  => $model->AMNo ]
            );

            /* data header : update AMNo */
            $post['Ttamhd']['AMNo'] = $AMNo;
        }

        Yii::$app->request->setBodyParams($post);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect( [ 'index' ] );
        }

        return $this->render('update', [
            'model' => $model,
            'id'    => $id
        ]);
    }

    /**
     * Deletes an existing Ttamhd model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @param string $action
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCancel($id, $action)
    {
        if ( $action === 'create' ) {
            $model = $this->findModelBase64( 'Ttamhd', $id );
            /* delete data detail  */
            Ttamit::deleteAll( 'AMNo = :AMNo', [ ':AMNo'  => $model->AMNo ] );
            /* delete data header */
            $model->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing Ttamhd model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ttamhd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Ttamhd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ttamhd::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
