<?php
namespace afinance\controllers;
use afinance\components\AfinanceController;
use afinance\components\TdAction;
use afinance\models\Tdlokasi;
use afinance\models\Tmotor;
use afinance\models\Ttsmhd;

use common\components\General;
use Yii;
use yii\base\UserException;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
/**
 * TtsmhdController implements the CRUD actions for Ttsmhd model.
 */
class TtsmhdController extends AfinanceController {
	public function actions() {
		$colGrid  = null;
		$joinWith = [
			'rLokasiAsal lasal'     => [
				'type' => 'LEFT OUTER JOIN'
			],
			'rLokasiTujuan ltujuan' => [
				'type' => 'LEFT OUTER JOIN'
			]
		];
		$join     = [];
		$where    = [];
		if ( isset( $_POST[ 'query' ] ) ) {
			$r = Json::decode( $_POST[ 'query' ], true )[ 'r' ];
			switch ( $r ) {
				case 'ttsmhd/mutasi-internal-pos':
					$colGrid = Ttsmhd::colGridMutasiInternalPos();
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "LEFT(ttsmhd.SMNo,2) = 'MI'",
							'params'    => []
						]
					];
					break;
				case 'ttsmhd/mutasi-repair-in':
					$colGrid = Ttsmhd::colGridMutasiRepairIn();
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "LEFT(ttsmhd.SMNo,2) = 'RI'",
							'params'    => []
						]
					];
					break;
				case 'ttsmhd/mutasi-repair-out':
					$colGrid = Ttsmhd::colGridMutasiRepairOut();
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "LEFT(ttsmhd.SMNo,2) = 'RO'",
							'params'    => []
						]
					];
					break;
			}
		}
		return [
			'td' => [
				'class'    => TdAction::className(),
				'model'    => Ttsmhd::className(),
				'colGrid'  => $colGrid,
				'joinWith' => $joinWith,
				'join'     => $join,
				'where'    => $where,
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttsmhd models.
	 * @return mixed
	 */
	public function actionMutasiInternalPos() {

		return $this->render( 'MutasiInternalPos' );
	}
	/**
	 * Lists all Ttsmhd models.
	 * @return mixed
	 */
	public function actionMutasiRepairIn() {

		return $this->render( 'MutasiRepairIn' );
	}
	/**
	 * Lists all Ttsmhd models.
	 * @return mixed
	 */
	public function actionMutasiRepairOut() {

		return $this->render( 'MutasiRepairOut' );
	}
	/**
	 * Creates a new Ttsmhd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param $jenis
	 *
	 * @return mixed
	 */
	public function actionCreate( $jenis ) {
		$model               = new Ttsmhd();
		$model->SMNo         = $this->createNumber( $jenis ,true);
		$model->SMTgl        = date( 'Y-m-d' );
		$model->SMMemo       = '--';
		$model->LokasiAsal   = ( Tdlokasi::find()->one() )->LokasiKode;
		$model->LokasiTujuan = ( Tdlokasi::find()->one() )->LokasiKode;
		$model->SMTotal      = 0;
		$model->UserID       = $this->UserID();
		$model->save();
		$dsBeli               = Ttsmhd::find()->GetDataByNo()->where( [ 'ttsmhd.SMNo' => $model->SMNo ] )->asArray( true )->one();
		$dsBeli[ 'SMNoView' ] = str_replace( '=', '-', $dsBeli[ 'SMNo' ] );
		$id                   = $this->generateIdBase64FromModel( $model );
		return $this->render( 'create', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'jenis'  => $jenis,
			'id'     => $id
		] );
	}
	protected function createNumber( $jenis, $tmp = false ) {
		switch ( $jenis ) {
			case "MutasiPOS":
				return General::createNo( "MI", 'Ttsmhd', 'SMNo', $tmp );
				break;
			case "RepairIN":
				return General::createNo( "RI", 'Ttsmhd', 'SMNo', $tmp );
				break;
			case "RepairOUT":
				return General::createNo( "RO", 'Ttsmhd', 'SMNo', $tmp );
				break;
		}
	}
	/**
	 * Updates an existing Ttsmhd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 * @param $jenis
	 * @param $action
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \yii\db\Exception
	 */
	public function actionUpdate( $id, $jenis, $action ) {
		/** @var Ttsmhd $model */
		$model                = $this->findModelBase64( 'Ttsmhd', $id );
		$dsBeli               = Ttsmhd::find()->GetDataByNo()->where( [ 'ttsmhd.SMNo' => $model->SMNo ] )->asArray( true )->one();
		$dsBeli[ 'SMNoView' ] = str_replace( '=', '-', $dsBeli[ 'SMNo' ] );
		$id                   = $this->generateIdBase64FromModel( $model );
		if ( Yii::$app->request->isPost ) {
			$transaction = Ttsmhd::getDb()->beginTransaction();
			$post = Yii::$app->request->post();
			try {
				$post[ 'UserID' ] = $this->UserID();
				$SubTotal         = 0;
				$detil = Tmotor::find()->where( [ 'SMNo' => $post[ 'SMNo' ] ] )->asArray()->all();
				foreach ( $detil as &$row ) {
					$SubTotal            += floatval( $row[ 'FBHarga' ] );
				}
				$post[ 'SMJam' ] = $post[ 'SMTgl' ]. ' ' . date( 'H:i' );
				$model->load($post,'');
				$model->save();
				if ( strpos( $model->SMNo, '=' ) !== false ) {
					$model->SMNo = $this->createNumber( $jenis ,false);
					Tmotor::updateAll( [ 'SMNo' => $model->SMNo ], [ 'SMNo' => $post['SMNo'] ] );
				}
				$model->save();
				$transaction->commit();
				switch ( $jenis ) {
					case "MutasiPOS":
						return $this->redirect( [ 'ttsmhd/mutasi-internal-pos', 'id' => $id ] );
						break;
					case "RepairIN":
						return $this->redirect( [ 'ttsmhd/mutasi-repair-in', 'id' => $id ] );
						break;
					case "RepairOUT":
						return $this->redirect( [ 'ttsmhd/mutasi-repair-out', 'id' => $id ] );
						break;
				}
			} catch ( UserException $e ) {
				$transaction->rollBack();
				Yii::$app->session->setFlash( 'error', $e->getMessage() );
				return $this->render( 'update', [
					'model'  => $model,
					'dsBeli' => $dsBeli,
					'jenis'  => $jenis,
					'id'     => $id
				] );
			}
		}
		return $this->render( 'update', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'jenis'  => $jenis,
			'id'     => $id
		] );
	}
	public function actionFillMotorGridAddEdit( $jenis ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$SMNo                        = $requestData[ 'SMNo' ];
		$LokasiKode                  = $requestData[ 'LokasiKode' ];
		$Jam                         = $requestData[ 'Jam' ] . ' ' . date( 'H:i' );
		$query2                      = ( new \yii\db\Query() )
			->select( new Expression(
				'tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType,
				 tmotor.MotorWarna, tmotor.MotorTahun, tmotor.FBHarga, tdmotortype.MotorNama'
			) )
			->from( 'tmotor' )
			->join( 'LEFT OUTER JOIN', 'tdmotortype', 'tmotor.MotorType = tdmotortype.MotorType' )
			->join( 'INNER JOIN', 'ttsmit', 'tmotor.MotorNoMesin = ttsmit.MotorNoMesin AND  ttsmit.MotorAutoN = tmotor.MotorAutoN' )
			->where( [ 'ttsmit.SMNo' => $SMNo ] );
		$query1                      = new \yii\db\Query();
		switch ( $jenis ) {
			case 'MutasiPOS':
				$query1->select( 'tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType,
				 tmotor.MotorWarna, tmotor.MotorTahun, tmotor.FBHarga, tdmotortype.MotorNama' )
				       ->from( 'tmotor' )
				       ->join( 'LEFT OUTER JOIN', 'tdmotortype', 'tmotor.MotorType = tdmotortype.MotorType' )
				       ->join( 'INNER JOIN', 'vmotorLastlokasi', '(tmotor.MotorNoMesin = vmotorLastlokasi.MotorNoMesin AND tmotor.MotorAutoN = vmotorLastlokasi.MotorAutoN)' )
				       ->where( "(vmotorLastlokasi.LokasiKode = :LokasiKode AND vmotorLastlokasi.LokasiKode <> 'Repair' 
					AND vmotorLastlokasi.Kondisi = 'IN' AND vmotorLastlokasi.Jam <= :Jam )", [
					       ':LokasiKode' => $LokasiKode,
					       ':Jam'        => $Jam
				       ] )->orderBy( 'tmotor.MotorType, MotorWarna, MotorTahun' );
				break;
			case 'RepairIN':
				$query1->select( 'tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType, 
						tmotor.MotorWarna, tmotor.MotorTahun, tmotor.FBHarga, tdmotortype.MotorNama' )
				       ->from( 'tmotor' )
				       ->join( 'LEFT OUTER JOIN', 'tdmotortype', 'tmotor.MotorType = tdmotortype.MotorType' )
				       ->join( 'INNER JOIN', 'vmotorLastlokasi', '(tmotor.MotorNoMesin = vmotorLastlokasi.MotorNoMesin AND tmotor.MotorAutoN = vmotorLastlokasi.MotorAutoN)' )
				       ->where( "(vmotorLastlokasi.LokasiKode = :LokasiKode AND vmotorLastlokasi.LokasiKode <> 'Repair' 
							AND vmotorLastlokasi.Kondisi = 'IN' AND vmotorLastlokasi.Jam <= :Jam )", [
					       ':LokasiKode' => $LokasiKode,
					       ':Jam'        => $Jam
				       ] )->orderBy( 'tmotor.MotorType, MotorWarna, MotorTahun' );
				break;
			case 'RepairOUT':
				$query1->select( 'tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType, 
						tmotor.MotorWarna, tmotor.MotorTahun, tmotor.FBHarga, tdmotortype.MotorNama' )
				       ->from( 'tmotor' )
				       ->join( 'LEFT OUTER JOIN', 'tdmotortype', 'tmotor.MotorType = tdmotortype.MotorType' )
				       ->join( 'INNER JOIN', 'vmotorLastlokasi', '(tmotor.MotorNoMesin = vmotorLastlokasi.MotorNoMesin AND tmotor.MotorAutoN = vmotorLastlokasi.MotorAutoN)' )
				       ->where( "(vmotorLastlokasi.LokasiKode = :LokasiKode AND vmotorLastlokasi.LokasiKode = 'Repair' 
							AND vmotorLastlokasi.Kondisi = 'IN' AND vmotorLastlokasi.Jam <= :Jam )", [
					       ':LokasiKode' => $LokasiKode,
					       ':Jam'        => $Jam
				       ] )->orderBy( 'tmotor.MotorType, MotorWarna, MotorTahun' );
				break;
		}
		$query     = $query1->union( $query2, true );
		$rowsCount = $query->count();
		$rows      = $query->all();
		/* encode row id */
//		for ( $i = 0; $i < count( $rows ); $i ++ ) {
//			$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
//		}
		$response = [
			'rows'      => $rows,
			'rowsCount' => $rowsCount
		];
		return $response;
	}
	/**
	 * Deletes an existing Ttsmhd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Ttsmhd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttsmhd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttsmhd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	public function actionCancel( $id, $jenis, $action ) {
//		if ( $action === 'create' ) {
//			$model = $this->findModelBase64( 'Ttamhd', $id );
//			/* delete data detail  */
//			Ttamit::deleteAll( 'AMNo = :AMNo', [ ':AMNo'  => $model->AMNo ] );
//			/* delete data header */
//			$model->delete();
//		}
		switch ( $jenis ) {
			case "MutasiPOS":
				return $this->redirect( [ 'ttsmhd/mutasi-internal-pos' ] );
				break;
			case "RepairIN":
				return $this->redirect( [ 'ttsmhd/mutasi-repair-in' ] );
				break;
			case "RepairOUT":
				return $this->redirect( [ 'ttsmhd/mutasi-repair-out' ] );
				break;
		}
	}
}
