<?php
namespace afinance\controllers;
use afinance\components\AfinanceController;
use afinance\components\TdAction;
use afinance\models\Tdmotorwarna;

use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TdmotorwarnaController implements the CRUD actions for Tdmotorwarna model.
 */
class TdmotorwarnaController extends AfinanceController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actions() {
		return [
			'td' => [
				'class' => TdAction::className(),
				'model' => Tdmotorwarna::className(),
			],
		];
	}
	/**
	 * Lists all Tdmotorwarna models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index');
	}
	/**
	 * Displays a single Tdmotorwarna model.
	 *
	 * @param string $MotorType
	 * @param string $MotorWarna
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $MotorType, $MotorWarna ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $MotorType, $MotorWarna ),
		] );
	}
	/**
	 * Creates a new Tdmotorwarna model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Tdmotorwarna();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'tdmotorwarna/index','id'=> $this->generateIdBase64FromModel($model)  ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Tdmotorwarna model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $MotorType
	 * @param string $MotorWarna
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$model = $this->findModelBase64( 'Tdmotorwarna', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'tdmotorwarna/index','id'=> $this->generateIdBase64FromModel($model)  ] );
		}
		return $this->render( 'update', [
			'model' => $model,
            'id'    => $this->generateIdBase64FromModel($model),
		] );
	}
	/**
	 * Deletes an existing Tdmotorwarna model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $MotorType
	 * @param string $MotorWarna
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $MotorType, $MotorWarna ) {
		$this->findModel( $MotorType, $MotorWarna )->delete();
		return $this->redirect( [ 'index' ] );
	}
    /**
     *
     */
    public function actionFind() {
        $params = \Yii::$app->request->post();
        $rows = [];

        if($params['mode'] === 'combo')
            $rows = Tdmotorwarna::find()->combo($params);

        return $this->responseDataJson($rows);
    }
	/**
	 * Finds the Tdmotorwarna model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $MotorType
	 * @param string $MotorWarna
	 *
	 * @return Tdmotorwarna the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $MotorType, $MotorWarna ) {
		if ( ( $model = Tdmotorwarna::findOne( [ 'MotorType' => $MotorType, 'MotorWarna' => $MotorWarna ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
