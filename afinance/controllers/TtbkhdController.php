<?php
namespace afinance\controllers;
use afinance\components\TdAction;
use afinance\models\Ttbkhd;

use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use afinance\components\AfinanceController;

/**
 * TtbkhdController implements the CRUD actions for Ttbkhd model.
 */
class TtbkhdController extends AfinanceController {
	public function actions() {
		$colGrid  = null;
		$joinWith = [
			'supplier' => [
				'type' => 'INNER JOIN'
			],
			'account'  => [
				'type' => 'INNER JOIN'
			],
		];
		$join     = [];
		$where    = [];
		if ( isset( $_POST['query'] ) ) {
			$r = Json::decode( $_POST['query'], true )['r'];
			switch ( $r ) {
				case 'ttbkhd/bank-keluar-subsidi-dealer-2':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'SubsidiDealer2')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-keluar-retur-harga':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'Retur Harga')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-keluar-insentif-sales':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'Insentif Sales')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-keluar-potongan-khusus':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'Potongan Khusus')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-keluar-bayar-hutang-unit':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'Bayar Unit')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-keluar-bayar-hutang-bbn':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'Bayar BBN')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-keluar-bayar-bbn-progresif':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'BBN Plus')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-keluar-kas':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'BK Ke Kas')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-keluar-umum':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'Umum')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-transfer':
					$colGrid  = Ttbkhd::colGridBankTransfer();
					$joinWith = [];
					$where    = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'Bank Transfer')",
							'params'    => []
						],
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'Bank Transfer')",
							'params'    => []
						]
					];
					$join     = [
						[
							'type'   => 'INNER JOIN',
							'table'  => "ttbkit",
							'on'     => 'ttbkhd.BKNo = ttbkit.BKNo',
							'params' => []
						],
						[
							'type'   => 'INNER JOIN',
							'table'  => "ttbmit",
							'on'     => 'ttbkit.FBNo = ttbmit.DKNo',
							'params' => []
						],
						[
							'type'   => 'INNER JOIN',
							'table'  => "ttbmhd",
							'on'     => 'ttbmhd.BMNo = ttbmit.BMNo',
							'params' => []
						],
						[
							'type'   => 'INNER JOIN',
							'table'  => "traccount traccountbm",
							'on'     => 'ttbmhd.NoAccount = traccountbm.NoAccount',
							'params' => []
						],
						[
							'type'   => 'INNER JOIN',
							'table'  => "traccount traccountbk",
							'on'     => 'ttbkhd.NoAccount = traccountbk.NoAccount',
							'params' => []
						]
					];
					break;
				case 'ttbkhd/pengajuan-bayar-bbn-bank':
					$colGrid = Ttbkhd::colGridPengajuanBayarBBNBank();
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'Bayar BBN')",
							'params'    => []
						],
						[
							'op'        => 'AND',
							'condition' => "LEFT(ttbkhd.BKNo,2) = ‘BP’",
							'params'    => []
						],
					];
					break;
				case 'ttbkhd/kwitansi-bank-bayar-bbn':
//					$colGrid  = Ttbkhd::colGridKwitansiBankBayarBbn();
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'Bayar BBN')",
							'params'    => []
						]
					];
					break;
			}
		}
		return [
			'td' => [
				'class'    => TdAction::className(),
				'model'    => Ttbkhd::className(),
				'colGrid'  => $colGrid,
				'joinWith' => $joinWith,
				'join'     => $join,
				'where'    => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttbkhd models.
	 * @return mixed
	 */
	public function actionBankKeluarSubsidiDealer2() {
		return $this->render( 'BankKeluarSubsidiDealer2' );
	}
    public function actionBankKeluarSubsidiDealer2Create() {
        return $this->create('Bank Keluar Subsidi Dealer2', 'bank-keluar-subsidi-dealer2', 'BANK KELUAR - Subsidi Dealer2');
    }
	/**
	 * Lists all Ttbkhd models.
	 * @return mixed
	 */
	public function actionBankKeluarReturHarga() {
		return $this->render( 'BankKeluarReturHarga');
	}
    public function actionBankKeluarReturHargaCreate() {
        return $this->create('Bank Keluar Retur Harga', 'bank-keluar-retur-harga', 'BANK KELUAR - Retur Harga');
    }
	/**
	 * Lists all Ttbkhd models.
	 * @return mixed
	 */
	public function actionBankKeluarInsentifSales() {
		return $this->render( 'BankKeluarInsentifSales');
	}
    public function actionBankKeluarInsentifSalesCreate() {
        return $this->create('Bank Keluar Insentif Sales', 'bank-keluar-insentif-sales', 'BANK KELUAR - Insentif Sales');
    }
	/**
	 * Lists all Ttbkhd models.
	 * @return mixed
	 */
	public function actionBankKeluarPotonganKhusus() {
		return $this->render( 'BankKeluarPotonganKhusus' );
	}
    public function actionBankKeluarPotonganKhususCreate() {
        return $this->create('Bank Keluar Potongan Khusus', 'bank-keluar-potongan-khusus', 'BANK KELUAR - Potongan Khusus');
    }
	/**
	 * Lists all Ttbkhd models.
	 * @return mixed
	 */
	public function actionBankKeluarBayarHutangUnit() {
		return $this->render( 'BankKeluarBayarHutangUnit');
	}
    public function actionBankKeluarBayarHutangUnitCreate() {
        return $this->create('Bank Keluar Bayar Hutang Unit', 'bank-keluar-bayar-hutang-unit', 'BANK KELUAR - Bayar Hutang Unit');
    }
	/**
	 * Lists all Ttbkhd models.
	 * @return mixed
	 */
	public function actionBankKeluarBayarHutangBbn() {
		return $this->render( 'BankKeluarBayarHutangBbn');
	}
    public function actionBankKeluarBayarHutangBbnCreate() {
        return $this->create('Bank Keluar Bayar Hutang Bbn', 'bank-keluar-bayar-hutang-bbn', 'BANK KELUAR - Bayar Hutang BBN');
    }
	/**
	 * Lists all Ttbkhd models.
	 * @return mixed
	 */
	public function actionBankKeluarBayarBbnProgresif() {
		return $this->render( 'BankKeluarBayarBbnProgresif');
	}
    public function actionBankKeluarBayarBbnProgresifCreate() {
        return $this->create('Bank Keluar Bayar BBN Progresif', 'bank-keluar-bayar-bbn-progresif', 'BANK KELUAR - Bayar BBN Progresif');
    }
	/**
	 * Lists all Ttbkhd models.
	 * @return mixed
	 */
	public function actionBankKeluarKas() {
		return $this->render( 'BankKeluarKas' );
	}

    public function actionBankKeluarKasCreate() {
        $model = new Ttbkhd();
        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'view', 'id' => $model->BKNo ] );
        }
        return $this->render( 'bank-keluar-kas-create', [
            'model' => $model,
        ] );
    }
	/**
	 * Lists all Ttbkhd models.
	 * @return mixed
	 */
	public function actionBankKeluarUmum() {
		return $this->render( 'BankKeluarUmum' );
	}
    public function actionBankKeluarUmumCreate() {
        $model = new Ttbkhd();
        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'view', 'id' => $model->BKNo ] );
        }
        return $this->render( 'bank-keluar-umum-create', [
            'model' => $model,
        ] );
    }
	/**
	 * Lists all Ttbkhd models.
	 * @return mixed
	 */
	public function actionBankTransfer() {
		return $this->render( 'BankTransfer' );
	}

    public function actionBankTransferCreate() {
        $model            = new Ttbkhd();
        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'bank-transfer' ] );
        }

        $model->BKNo      = Ttbkhd::generateBKNo( true );
        $model->BKTgl     = date( "Y-m-d" );
        $model->BKJam     = date( "Y-m-d H:i:s" );
        $model->BKJenis   = 'Bank Transfer';

        return $this->render( 'create', [
            'model' => $model,
            'view'  => 'bank-transfer',
            'title'  => 'Bank Transfer',
            'templateForm'  => '_bank-transfer-form'
        ] );
    }

	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
	public function actionPengajuanBayarBbnBank() {
		return $this->render( 'PengajuanBayarBBNBank' );
	}

    public function actionPengajuanBayarBbnBankCreate() {
//        $title = 'Pengajuan Bayar BBN Via Bank';
//        $model = new Ttbkhd();
//        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
//            return $this->redirect( [ 'view', 'id' => $model->BKNo ] );
//        }
//        return $this->render( 'create', [
//            'model' => $model,
//            'title' => $title
//        ] );
        return $this->create('Bank Keluar Bayar BBN Bank', 'bank-keluar-bayar-bbn-bank', 'BANK KELUAR - Bayar BBN Via Bank');
    }
	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
	public function actionKwitansiBankBayarBbn() {
		return $this->render( 'KwitansiBankBayarBbn');
	}
	/**
	 * Displays a single Ttbkhd model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Creates a new Ttbkhd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Ttbkhd();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'id' => $model->BKNo ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Ttbkhd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$model = $this->findModel( $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'id' => $model->BKNo ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Ttbkhd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Ttbkhd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttbkhd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttbkhd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}

//    public function actionPengajuanBayarBbnBankCreate() {
//        $model = new Ttbkhd();
//        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
//            return $this->redirect( [ 'view', 'id' => $model->BKNo ] );
//        }
//        return $this->render( 'create', [
//            'model' => $model,
//        ] );
//    }

    private function create($BKJenis, $view, $title) {
        $model            = new Ttbkhd();
        $model->BKJenis   = $BKJenis;
        $model->save();
        $id = $this->generateIdBase64FromModel( $model );
        return $this->render( 'create', [
            'model' => $model,
            'id'    => $id,
            'view'  => $view,
            'title'  => $title
        ] );
    }
}
