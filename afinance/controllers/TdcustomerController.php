<?php
namespace afinance\controllers;
use afinance\components\AfinanceController;
use afinance\components\TdAction;
use afinance\models\Tdcustomer;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TdcustomerController implements the CRUD actions for Tdcustomer model.
 */
class TdcustomerController extends AfinanceController {
	public function actions() {
		return [
			'td' => [
				'class' => TdAction::className(),
				'model' => Tdcustomer::className()
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttfb models.
	 * @return mixed
	 */
	public function actionSelect() {
		$this->layout = 'select-mode';
		return $this->render( 'index', [
			'mode' => self::MODE_SELECT
		] );
	}
	/**
	 * Lists all Tdcustomer models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index', [
			'mode' => ''
		] );
	}
	/**
	 * Displays a single Tdcustomer model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Finds the Tdcustomer model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Tdcustomer the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Tdcustomer::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Tdcustomer model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Tdcustomer();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'index', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Tdcustomer model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$model = $this->findModelBase64( 'Tdcustomer', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'index', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
		return $this->render( 'update', [
			'model' => $model,
			'id'    => $this->generateIdBase64FromModel( $model ),
		] );
	}
	/**
	 * Deletes an existing Tdcustomer model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
}
