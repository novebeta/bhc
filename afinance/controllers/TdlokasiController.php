<?php

namespace afinance\controllers;

use afinance\components\AfinanceController;
use afinance\components\TdAction;
use afinance\models\Tdarea;
use Yii;
use afinance\models\Tdlokasi;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TdlokasiController implements the CRUD actions for Tdlokasi model.
 */
class TdlokasiController extends AfinanceController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
	public function actions()
	{
		return [
			'td' => [
				'class' => TdAction::className(),
				'model' => Tdlokasi::className(),
				'joinWith' => [
					'sales' => [
						'type' => 'LEFT OUTER JOIN'
					]
				]
			],
		];
	}
    /**
     * Lists all Tdlokasi models.
     * @return mixed
     */
    public function actionIndex()
    {

        return $this->render('index');
    }

    /**
     * Displays a single Tdlokasi model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tdlokasi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tdlokasi();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect( [ 'tdlokasi/index','id'=> $this->generateIdBase64FromModel($model)  ] );
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tdlokasi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModelBase64('Tdlokasi',$id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect( [ 'tdlokasi/index','id'=> $this->generateIdBase64FromModel($model)  ] );
        }

        return $this->render('update', [
            'model' => $model,
            'id'    => $this->generateIdBase64FromModel($model),
        ]);
    }

    /**
     * Deletes an existing Tdlokasi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tdlokasi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Tdlokasi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tdlokasi::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
