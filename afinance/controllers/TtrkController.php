<?php
namespace afinance\controllers;
use afinance\components\AfinanceController;
use afinance\components\TdAction;
use afinance\models\Tmotor;
use afinance\models\Ttrk;

use common\components\Custom;
use common\components\General;
use Yii;
use yii\base\UserException;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtrkController implements the CRUD actions for Ttrk model.
 */
class TtrkController extends AfinanceController {
	public function actions() {
		$colGrid  = null;
		$joinWith = [
			'motor'              => [
				'type' => 'INNER JOIN'
			],
			'suratJalanKonsumen' => [
				'type' => 'INNER JOIN'
			],
			'lokasi'             => [
				'type' => 'INNER JOIN'
			],
			'dataKonsumen'       => [
				'type' => 'INNER JOIN'
			],
			'konsumen'           => [
				'type' => 'INNER JOIN'
			]
		];
		$join     = [];
		$where    = [];
//		if ( isset( $_POST['query'] ) ) {
//			$r = Json::decode( $_POST['query'], true )['r'];
//			switch ( $r ) {
//				case 'ttsd/mutasi-eksternal-dealer':
//					$colGrid  = Ttsd::colGridMutasiEksternalDealer();
//					$joinWith = [
//						'lokasi' => [
//							'type' => 'INNER JOIN'
//						],
//						'dealer' => [
//							'type' => 'INNER JOIN'
//						],
//					];
//					break;
//				case 'ttsd/invoice-eksternal':
//					$colGrid  = Ttsd::colGridInvoiceEksternal();
//					$joinWith = [
//						'lokasi' => [
//							'type' => 'INNER JOIN'
//						],
//						'dealer' => [
//							'type' => 'INNER JOIN'
//						],
//					];
//					break;
//			}
//		}
		return [
			'td' => [
				'class'    => TdAction::className(),
				'model'    => Ttrk::className(),
				'colGrid'  => $colGrid,
				'joinWith' => $joinWith
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttrk models.
	 * @return mixed
	 */
	public function actionIndex() {

		return $this->render( 'index' );
	}
	/**
	 * Displays a single Ttrk model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Finds the Ttrk model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttrk the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttrk::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttrk model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model               = new Ttrk();
		$model->RKNo         = General::createTemporaryNo( "RK", 'ttrk', 'RKNo' );
		$model->RKTgl        = date( 'Y-m-d' );
		$model->RKMemo       = '--';
		$model->DKNo         = '--';
		$model->SKNo         = '--';
		$model->MotorNoMesin = '--';
		$model->LokasiKode   = $this->LokasiKode();
		$model->UserID       = $this->UserID();
		$model->save();
		$dsJual               = $model::find()->dsTJualFillByNo()->where( [ 'ttrk.RKNo' => $model->RKNo ] )->asArray()->one();
		$dsJual[ 'RKNoView' ] = str_replace( '=', '-', $dsJual[ 'RKNo' ] );
		$id                   = $this->generateIdBase64FromModel( $model );
		return $this->render( 'create', [
			'model'  => $model,
			'dsJual' => $dsJual,
			'id'     => $id,
		] );
	}
	/**
	 * Updates an existing Ttrk model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @param $action
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \yii\db\Exception
	 */
	public function actionUpdate( $id , $action) {
		/** @var Ttrk $model */
		$model  = $this->findModelBase64( 'Ttrk', $id );
		$MySKNoAsal = $model->SKNo;
		$MyDKNoAsal = $model->DKNo;
		$dsJual = $model::find()->dsTJualFillByNo()->where( [ 'ttrk.RKNo' => $model->RKNo ] )->asArray()->one();
		$id     = $this->generateIdBase64FromModel( $model );
		if ( Yii::$app->request->isPost ) {
			$post        = Yii::$app->request->post();
			$transaction = Yii::$app->db->beginTransaction();
			try {
//				$post[ 'RKNo' ]
				$post[ 'UserID' ] = $this->UserID();
				$model->load($post,'');
				$model->save();
				$model->refresh();
				if($action == 'create'){
					Ttrk::find()->InsertMotorRKNo($model->DKNo,$model->SKNo,$model->RKNo,$model->MotorNoMesin);
					Ttrk::find()->SetPemutihanByRK($model->RKTgl,$model->SKNo);
				}else{
					if($MySKNoAsal != $model->SKNo){
						Ttrk::find()->HapusMotorRKNo($MyDKNoAsal,$MySKNoAsal,$model->RKNo);
						Ttrk::find()->InsertMotorRKNo($model->DKNo,$model->SKNo,$model->RKNo,$model->MotorNoMesin);
						Ttrk::find()->DelPemutihanByRK($model->RKTgl,$MySKNoAsal);
						Ttrk::find()->SetPemutihanByRK($model->RKTgl,$model->SKNo);
					}
					Ttrk::find()->EditDelTvMotorLokasi($model->RKNo);
				}
				Ttrk::find()->PerbaikiSalahNoSinRKSK($model->RKNo);
				if ( strpos( $model->RKNo, '=' ) !== false ) {
					$model->RKNo = General::createNo( "RK", 'ttrk', 'RKNo' );
					Tmotor::updateAll( [ 'RKNo' => $model->RKNo ], [ 'RKNo' => $post['RKNo'] ] );
				}
				$model->save();
				$model->refresh();
				Yii::$app->db
					->createCommand( "INSERT INTO tvmotorlokasi (MotorAutoN, MotorNoMesin, LokasiKode, NoTrans, Jam, Kondisi)
						SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttRK.LokasiKode  AS LokasiKode, ttRK.RKNo AS NoTrans, ttRK.RKJam AS Jam, 'IN' AS Kondisi
						FROM tmotor INNER JOIN ttRK ON tmotor.RKNo = ttRK.RKNo WHERE ttRK.RKNo = :RKNo", [
						':RKNo' => $model->RKNo
					] )->execute();
				$transaction->commit();
				return $this->redirect( ['ttrk/index', 'id' => $id ] );
			} catch ( UserException $e ) {
				$transaction->rollBack();
				Yii::$app->session->setFlash( 'error', $e->getMessage() );
				return $this->render( 'update', [
					'model'  => $model,
					'dsJual' => $dsJual,
					'id'     => $id,
				] );
			}
		}
		$dsJual[ 'RKNoView' ] = str_replace( '=', '-', $dsJual[ 'RKNo' ] );
		$dsJual[ 'SKTgl' ]    = General::asDate( $dsJual[ 'SKTgl' ] );
		$dsJual[ 'DKTgl' ]    = General::asDate( $dsJual[ 'DKTgl' ] );
		return $this->render( 'update', [
			'model'  => $model,
			'dsJual' => $dsJual,
			'id'     => $id,
		] );
	}
	/**
	 * Deletes an existing Ttrk model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
}
