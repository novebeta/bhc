<?php
namespace afinance\controllers;
use afinance\components\AfinanceController;
use afinance\components\TdAction;
use afinance\models\Tdcustomer;
use afinance\models\Tdsales;
use afinance\models\Tmotor;
use afinance\models\Ttdk;

use afinance\models\Ttsk;
use common\components\General;
use Yii;
use yii\db\Exception;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
/**
 * TtdkController implements the CRUD actions for Ttdk model.
 */
class TtdkController extends AfinanceController {
	public function actions() {
		$colGrid  = null;
		$joinWith = [
			'motor'      => [
				'type' => 'LEFT OUTER JOIN'
			],
			'motorTypes' => [
				'type' => 'LEFT OUTER JOIN'
			],
			'customer'   => [
				'type' => 'INNER JOIN'
			]
		];
		$join     = [];
		$where    = [];
		if ( isset( $_POST[ 'query' ] ) ) {
			$r = Json::decode( $_POST[ 'query' ], true )[ 'r' ];
			switch ( $r ) {
				case 'ttdk/data-konsumen-kredit':
					$colGrid = Ttdk::colGridDataKonsumenKredit();
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "DKJenis = 'Kredit'",
							'params'    => []
						]
					];
					break;
				case 'ttdk/data-konsumen-tunai':
					$colGrid = Ttdk::colGridDataKonsumenTunai();
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "DKJenis = 'Tunai'",
							'params'    => []
						]
					];
					break;
				case 'ttdk/select':
					$colGrid                     = Ttdk::colGridDataKonsumenTunai();
					unset($joinWith['motor']);
					$joinWith[ 'suratKonsumen' ] = [
						'type' => 'LEFT OUTER JOIN'
					];
					$where                       = [
						[
							'op'        => 'AND',
							'condition' => "(ttsk.SKNo is NUll )",
							'params'    => []
						]
					];
					break;
				case 'ttdk/data-konsumen-status':
					$colGrid = Ttdk::colGridDataKonsumenStatus();
					break;
				case 'ttdk/stnk-bpkb':
					$colGrid = Ttdk::colGridStnkBpkb();
					break;
				case 'ttdk/data-portal':
					$colGrid = Ttdk::colGridDataPortal();
					break;
				case 'ttdk/pengajuan-berkas':
					$colGrid = Ttdk::colGridPengajuanBerkas();
					break;
				case 'ttdk/kwitansi-kirim-tagih':
					$colGrid = Ttdk::colGridKwitansiKirimTagih();
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(DKJenis LIKE '%')",
							'params'    => []
						]
					];
					break;
				case 'ttdk/kwitansi-um-konsumen':
					$colGrid = Ttdk::colGridKwitansiUmKonsumen();
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(DKJenis LIKE '%')",
							'params'    => []
						]
					];
					break;
				case 'ttdk/kwitansi-pelunasan-leasing':
					$colGrid = Ttdk::colGridKwitansiPelunasanLeasing();
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(DKJenis LIKE '%')",
							'params'    => []
						]
					];
					break;
			}
		}
		return [
			'td' => [
				'class'    => TdAction::className(),
				'model'    => Ttdk::className(),
				'colGrid'  => $colGrid,
				'joinWith' => $joinWith,
				'join'     => $join,
				'where'    => $where,
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
	public function actionDataKonsumenKredit() {

		return $this->render( 'DataKonsumenKredit' );
	}
	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
	public function actionDataKonsumenTunai() {

		return $this->render( 'DataKonsumenTunai' );
	}

	/**
	 * Lists all Ttfb models.
	 * @return mixed
	 */
	public function actionSelect() {
		$this->layout = 'select-mode';
		return $this->render( 'DataKonsumenSelect', [
			'mode' => self::MODE_SELECT
		] );
	}
	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
	public function actionDataKonsumenStatus() {
		return $this->render( 'DataKonsumenStatus' );
	}
	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
	public function actionStnkBpkb() {

		return $this->render( 'StnkBpkb' );
	}
	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
	public function actionDataPortal() {

		return $this->render( 'DataPortal' );
	}
	/**
	 * Updates an existing Ttdk model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDataPortalUpdate( $id ) {
		/** @var Ttdk $model */
		$model = $this->findModelBase64( 'Ttdk', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'DataPortal', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
		$dsJual = $model->find()->dsJual()->where( [ 'ttdk.DKNo' => $model->DKNo ] )->asArray( true )->one();
		return $this->render( 'DataPortal-update', [
			'model'  => $model,
			'dsJual' => $dsJual,
			'id'     => $this->generateIdBase64FromModel( $model ),
		] );
	}
	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
	public function actionKwitansiKirimTagih() {

		return $this->render( 'KwitansiKirimTagih' );
	}
	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
	public function actionKwitansiUmKonsumen() {

		return $this->render( 'KwitansiUmKonsumen' );
	}
	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
	public function actionKwitansiPelunasanLeasing() {

		return $this->render( 'KwitansiPelunasanLeasing' );
	}
	/**
	 * Creates a new Ttdk model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 * @throws \yii\base\InvalidConfigException
	 */
	public function actionDataKonsumenKreditCreate() {
		$model          = new Ttdk();
		$model->DKNo    = General::createTemporaryNo( 'DK', 'Ttdk', 'DKNo' );
		$model->DKTgl   = Yii::$app->formatter->asDate( 'now', 'yyyy-MM-dd' );
		$model->DKJenis = 'Kredit';
		$model->save();
		$model->refresh();
		$dsTJualDatKonFill                       = $model->find()->dsTJualFillByNo()->where( "(ttdk.DKNo = :DKNo) AND (ttdk.DKJenis LIKE :DKJenis)", [
			':DKNo'    => $model->DKNo,
			':DKJenis' => 'Kredit',
		] )->asArray( true )->one();
		$dsTJualDatKonFill[ 'CusKode' ]          = '--';
		$dsTJualDatKonFill[ 'SPKNo' ]            = '--';
		$dsTJualDatKonFill[ 'DKLunas' ]          = 'Belum';
		$dsTJualDatKonFill[ 'INNo' ]             = '--';
		$dsTJualDatKonFill[ 'DKHPP' ]            = 0;
		$dsTJualDatKonFill[ 'DKHarga' ]          = 0;
		$dsTJualDatKonFill[ 'DKDPTotal' ]        = 0;
		$dsTJualDatKonFill[ 'DKDPLLeasing' ]     = 0;
		$dsTJualDatKonFill[ 'DKDPTerima' ]       = 0;
		$dsTJualDatKonFill[ 'DKDPInden' ]        = 0;
		$dsTJualDatKonFill[ 'DKNetto' ]          = 0;
		$dsTJualDatKonFill[ 'ProgramNama' ]      = '';
		$dsTJualDatKonFill[ 'ProgramSubsidi' ]   = 0;
		$dsTJualDatKonFill[ 'BBN' ]              = 0;
		$dsTJualDatKonFill[ 'BBNPlus' ]          = 0;
		$dsTJualDatKonFill[ 'Jaket' ]            = 0;
		$dsTJualDatKonFill[ 'PrgSubsSupplier' ]  = 0;
		$dsTJualDatKonFill[ 'PrgSubsDealer' ]    = 0;
		$dsTJualDatKonFill[ 'PrgSubsFincoy' ]    = 0;
		$dsTJualDatKonFill[ 'PotonganHarga' ]    = 0;
		$dsTJualDatKonFill[ 'ReturHarga' ]       = 0;
		$dsTJualDatKonFill[ 'InsentifSales' ]    = 0;
		$dsTJualDatKonFill[ 'PotonganKhusus' ]   = 0;
		$dsTJualDatKonFill[ 'PotonganAHM' ]      = 0;
		$dsTJualDatKonFill[ 'NamaHadiah' ]       = "";
		$dsTJualDatKonFill[ 'DKTenor' ]          = 0;
		$dsTJualDatKonFill[ 'DKAngsuran' ]       = 0;
		$dsTJualDatKonFill[ 'DKMerkSblmnya' ]    = "";
		$dsTJualDatKonFill[ 'DKJenisSblmnya' ]   = "";
		$dsTJualDatKonFill[ 'DKMotorUntuk' ]     = "";
		$dsTJualDatKonFill[ 'DKMotorOleh' ]      = "";
		$dsTJualDatKonFill[ 'DKSurveyor' ]       = "";
		$dsTJualDatKonFill[ 'DKTOP' ]            = 7;
		$dsTJualDatKonFill[ 'DKMediator' ]       = "--";
		$dsTJualDatKonFill[ 'DKPOLeasing' ]      = "--";
		$dsTJualDatKonFill[ 'CusPembeliNama' ]   = "--";
		$dsTJualDatKonFill[ 'CusPembeliKTP' ]    = "--";
		$dsTJualDatKonFill[ 'CusPembeliAlamat' ] = "--";
		$dsTJualDatKonFill[ 'CusKK' ]            = "--";
		$dsTJualDatKonFill[ 'DKTglTagih' ]       = "1900-01-01";
		$dsTJualDatKonFill[ 'DKPOTgl' ]          = "1900-01-01";
		$dsTJualDatKonFill[ 'DKPONo' ]           = "--";
		$dsTJualDatKonFill[ 'ROCount' ]          = 0;
		$dsTJualDatKonFill[ 'DKSCP' ]            = 0;
		$dsTJualDatKonFill[ 'DKScheme' ]         = 0;
		$dsTJualDatKonFill[ 'DKScheme2' ]        = 0;
		$model->load( $dsTJualDatKonFill, '' );
		$model->save();
		return $this->render( 'DataKonsumenKredit-create', [
			'model'             => $model,
			'dsTJualDatKonFill' => $dsTJualDatKonFill,
		] );
	}
	/**
	 * Updates an existing Ttdk model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDataKonsumenKreditUpdate( $id ) {
		/** @var Ttdk $model */
		$model             = $this->findModelBase64( 'Ttdk', $id );
		$dsTJualDatKonFill = $model->find()->dsTJualFillByNo()->where( "(ttdk.DKNo = :DKNo) AND (ttdk.DKJenis LIKE :DKJenis)", [
			':DKNo'    => $model->DKNo,
			':DKJenis' => 'Kredit',
		] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$transaction = Ttdk::getDb()->beginTransaction();
			try {
				$post = Yii::$app->request->post();
				$this->save( $dsTJualDatKonFill, $model );
				$transaction->commit();
			} catch ( Exception $e ) {
				return $this->render( 'DataKonsumenKredit-create', [
					'model'             => $model,
					'dsTJualDatKonFill' => $post,
					'id'                => $this->generateIdBase64FromModel( $model ),
				] );
			}
			return $this->redirect( [ 'ttdk/data-konsumen-kredit', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
		return $this->render( 'DataKonsumenKredit-update', [
			'model'             => $model,
			'dsTJualDatKonFill' => $dsTJualDatKonFill,
			'id'                => $this->generateIdBase64FromModel( $model ),
		] );
	}
	protected function save( $dsTJualDatKonFill, $model ) {
		/** @var Ttdk $model */
		$action = $_GET[ 'action' ];
		if ( $action == 'create' ) {
			$model->DKNo = General::createNo( 'DK', 'Ttdk', 'DKNo' );
			$model->save();
			$model->refresh();
			$post[ 'DKNo' ] = $model->DKNo;
		}
		/** @var Ttsk $ttsk */
		$a    = $post[ 'DKTgl' ];
		$ttsk = Ttsk::findOne( $post[ 'SKNo' ] );
		if ( $ttsk != null ) {
			$a = $ttsk->SKTgl;
		}
		if ( $post[ 'DKTgl' ] > $a ) {
			$post[ 'DKTgl' ] = $a;
		}
		$Motor             = Ttdk::find()->ComboMotorAddEdit( true, [
			'DKTgl'        => $post[ 'DKTgl' ],
			'MotorNoMesin' => $post[ 'MotorNoMesin' ],
		] )->asArray()->one();
		$MyAutoN           = ( $Motor == null ) ? 0 : $Motor[ 'MotorAutoN' ];
		$post[ 'ROCount' ] = Ttdk::find()->GetRO( $post[ 'DKNo' ], $post[ 'CusKTP' ], $post[ 'CusPembeliKTP' ] );
		$post[ 'DKHPP' ]   = Ttdk::find()->UpdateHargaBeli( $post[ 'MotorNoMesin' ], $MyAutoN );
		if ( $dsTJualDatKonFill[ 'MotorNoMesin' ] != $post[ 'MotorNoMesin' ] || $dsTJualDatKonFill[ 'MotorAutoN' ] != $MyAutoN ) {
			Ttdk::find()->UpdateMotorDKNo( $post[ 'MotorNoMesin' ], $MyAutoN, $post[ 'DKNo' ], $post[ 'SKNo' ] );
		}
		$customer = Tdcustomer::findOne( $post[ 'CusKode' ] );
		if ( $customer == null ) {
			$customer          = new Tdcustomer;
			$customer->CusKode = General::createNo( 'KS', 'tdcustomer', 'CusKode' );
			unset( $post[ 'CusKode' ] );
		}
		$customer->load( $post, '' );
		$customer->save();
		$model->load( $post, '' );
		$model->save();
		if ( $action == 'create' ) {
			Ttdk::find()->EditSTNK( $post[ 'CusNama' ], [
				implode( ", ", [ $post[ 'CusAlamat' ], $post[ 'CusKelurahan' ], $post[ 'CusKecamatan' ], $post[ 'CusKabupaten' ] ] )
			], $post[ 'MotorNoMesin' ], $MyAutoN );
		}
	}
	public function actionDataKonsumenValidate() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$success                     = true;
		$msg                         = Tdsales::find()->CekPiutangKonsSales( $_POST[ 'DKTgl' ], $_POST[ 'SalesKode' ] );
		if ( $msg != '' ) {
			return [ 'success' => false, 'msg' => $msg ];
		}
		$tmotor = Tmotor::find()->where( [ 'MotorNoMesin' => $_POST[ 'MotorNoMesin' ] ] )->one();
		if ( $tmotor != null ) {
			if ( $tmotor->DKNo != '--' && $tmotor->DKNo != $_POST[ 'DKNo' ] ) {
				return [ 'success' => false, 'msg' => "Motor Telah di pakai oleh No Data Konsumen : " . $tmotor->DKNo ];
			}
		}
		$ttdk = Ttdk::find()->where( [ 'INNo' => $_POST[ 'INNo' ] ] )->andWhere( [ '<>', 'DKNo', $_POST[ 'DKNo' ] ] )->one();
		if ( $ttdk != null ) {
			if ( $ttdk->DKNo != '--' && $ttdk->DKNo != $_POST[ 'DKNo' ] ) {
				return [ 'success' => false, 'msg' => "No Inden telah dipakai oleh No Data Konsumen : " . $ttdk->DKNo ];
			}
		}
		return [ 'success' => $success, 'msg' => '' ];
	}
	/**
	 * Updates an existing Ttdk model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDataKonsumenTunaiUpdate( $id ) {
		/** @var Ttdk $model */
		$model             = $this->findModelBase64( 'Ttdk', $id );
		$dsTJualDatKonFill = $model->find()->dsTJualFillByNo()->where( "(ttdk.DKNo = :DKNo) AND (ttdk.DKJenis LIKE :DKJenis)", [
			':DKNo'    => $model->DKNo,
			':DKJenis' => 'Tunai',
		] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$transaction = Ttdk::getDb()->beginTransaction();
			try {
				$post = Yii::$app->request->post();
				$this->save( $dsTJualDatKonFill, $model );
				$transaction->commit();
			} catch ( Exception $e ) {
				return $this->render( 'DataKonsumenTunai-create', [
					'model'             => $model,
					'dsTJualDatKonFill' => $post,
					'id'                => $this->generateIdBase64FromModel( $model ),
				] );
			}
			return $this->redirect( [ 'ttdk/data-konsumen-tunai', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
		return $this->render( 'DataKonsumenTunai-update', [
			'model'             => $model,
			'dsTJualDatKonFill' => $dsTJualDatKonFill,
			'id'                => $this->generateIdBase64FromModel( $model ),
		] );
	}
	/**
	 * Creates a new Ttdk model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 * @throws \yii\base\InvalidConfigException
	 */
	public function actionDataKonsumenTunaiCreate() {
		$model          = new Ttdk();
		$model->DKNo    = General::createTemporaryNo( 'DK', 'Ttdk', 'DKNo' );
		$model->DKTgl   = Yii::$app->formatter->asDate( 'now', 'yyyy-MM-dd' );
		$model->DKJenis = 'Tunai';
		$model->save();
		$model->refresh();
		$dsTJualDatKonFill                       = $model->find()->dsTJualFillByNo()->where( "(ttdk.DKNo = :DKNo) AND (ttdk.DKJenis LIKE :DKJenis)", [
			':DKNo'    => $model->DKNo,
			':DKJenis' => 'Tunai',
		] )->asArray( true )->one();
		$dsTJualDatKonFill[ 'CusKode' ]          = '--';
		$dsTJualDatKonFill[ 'SPKNo' ]            = '--';
		$dsTJualDatKonFill[ 'DKLunas' ]          = 'Belum';
		$dsTJualDatKonFill[ 'INNo' ]             = '--';
		$dsTJualDatKonFill[ 'DKHPP' ]            = 0;
		$dsTJualDatKonFill[ 'DKHarga' ]          = 0;
		$dsTJualDatKonFill[ 'DKDPTotal' ]        = 0;
		$dsTJualDatKonFill[ 'DKDPLLeasing' ]     = 0;
		$dsTJualDatKonFill[ 'DKDPTerima' ]       = 0;
		$dsTJualDatKonFill[ 'DKDPInden' ]        = 0;
		$dsTJualDatKonFill[ 'DKNetto' ]          = 0;
		$dsTJualDatKonFill[ 'ProgramNama' ]      = '';
		$dsTJualDatKonFill[ 'ProgramSubsidi' ]   = 0;
		$dsTJualDatKonFill[ 'BBN' ]              = 0;
		$dsTJualDatKonFill[ 'BBNPlus' ]          = 0;
		$dsTJualDatKonFill[ 'Jaket' ]            = 0;
		$dsTJualDatKonFill[ 'PrgSubsSupplier' ]  = 0;
		$dsTJualDatKonFill[ 'PrgSubsDealer' ]    = 0;
		$dsTJualDatKonFill[ 'PrgSubsFincoy' ]    = 0;
		$dsTJualDatKonFill[ 'PotonganHarga' ]    = 0;
		$dsTJualDatKonFill[ 'ReturHarga' ]       = 0;
		$dsTJualDatKonFill[ 'InsentifSales' ]    = 0;
		$dsTJualDatKonFill[ 'PotonganKhusus' ]   = 0;
		$dsTJualDatKonFill[ 'PotonganAHM' ]      = 0;
		$dsTJualDatKonFill[ 'NamaHadiah' ]       = "";
		$dsTJualDatKonFill[ 'DKTenor' ]          = 0;
		$dsTJualDatKonFill[ 'DKAngsuran' ]       = 0;
		$dsTJualDatKonFill[ 'DKMerkSblmnya' ]    = "";
		$dsTJualDatKonFill[ 'DKJenisSblmnya' ]   = "";
		$dsTJualDatKonFill[ 'DKMotorUntuk' ]     = "";
		$dsTJualDatKonFill[ 'DKMotorOleh' ]      = "";
		$dsTJualDatKonFill[ 'DKSurveyor' ]       = "";
		$dsTJualDatKonFill[ 'DKTOP' ]            = 1;
		$dsTJualDatKonFill[ 'DKMediator' ]       = "--";
		$dsTJualDatKonFill[ 'DKPOLeasing' ]      = "--";
		$dsTJualDatKonFill[ 'CusPembeliNama' ]   = "--";
		$dsTJualDatKonFill[ 'CusPembeliKTP' ]    = "--";
		$dsTJualDatKonFill[ 'CusPembeliAlamat' ] = "--";
		$dsTJualDatKonFill[ 'CusKK' ]            = "--";
		$dsTJualDatKonFill[ 'DKTglTagih' ]       = "1900-01-01";
		$dsTJualDatKonFill[ 'DKPOTgl' ]          = "1900-01-01";
		$dsTJualDatKonFill[ 'DKPONo' ]           = "--";
		$dsTJualDatKonFill[ 'ROCount' ]          = 0;
		$dsTJualDatKonFill[ 'DKSCP' ]            = 0;
		$dsTJualDatKonFill[ 'DKScheme' ]         = 0;
		$dsTJualDatKonFill[ 'DKScheme2' ]        = 0;
		$model->load( $dsTJualDatKonFill, '' );
		$model->save();
		return $this->render( 'DataKonsumenTunai-create', [
			'model'             => $model,
			'dsTJualDatKonFill' => $dsTJualDatKonFill,
		] );
	}
	/**
	 * Deletes an existing Ttdk model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Ttdk model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttdk the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttdk::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	public function actionDataKonsumenStatusCreate() {
		$model = new Ttdk();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'ttdk/data-konsumen-status', 'id' => $model->DKNo ] );
		}
		return $this->render( 'data-konsumen-status-create', [
			'model' => $model,
		] );
	}
	public function actionDataKonsumenStatusUpdate( $id ) {
		/** @var Ttdk $model */
		$model = $this->findModelBase64( 'Ttdk', $id );
		if ( Yii::$app->request->isPost ) {
			$transaction = Ttdk::getDb()->beginTransaction();
			try {
				$model->DKPONo     = $_POST[ 'DKPONo' ];
				$model->DKPOTgl    = $_POST[ 'DKPOTgl' ];
				$model->DKTglTagih = $_POST[ 'DKTglTagih' ];
				$model->DKMemo     = $_POST[ 'DKMemo' ];
				$model->save();
				$cus                   = Tdcustomer::findOne( $_POST[ 'CusKode' ] );
				$cus->CusPembeliNama   = $_POST[ 'CusPembeliNama' ];
				$cus->CusPembeliKTP    = $_POST[ 'CusPembeliKTP' ];
				$cus->CusPembeliAlamat = $_POST[ 'CusPembeliAlamat' ];
				$cus->save();
				$transaction->commit();
			} catch ( Exception $e ) {
				return $this->render( 'data-konsumen-status-update', [
					'model'             => $model,
					'dsTJualDatKonFill' => $_POST,
					'id'                => $this->generateIdBase64FromModel( $model ),
				] );
			}
			return $this->redirect( [ 'ttdk/data-konsumen-tunai', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
		$dsJual              = $model->find()->dsJualStatusKonsumen()->where( [ 'ttdk.DKNo' => $model->DKNo ] )->asArray( true )->one();
		$txtNPrgSubsSupplier = floatval( $dsJual[ 'PrgSubsSupplier' ] );
		$txtNPrgSubsDealer   = floatval( $dsJual[ 'PrgSubsDealer' ] );
		$txtNPrgSubsFincoy   = floatval( $dsJual[ 'PrgSubsFincoy' ] );
		$txtNDKDPLeasing     = floatval( $dsJual[ 'DKDPLLeasing' ] );
		$txtNDKDPInden       = floatval( $dsJual[ 'DKDPInden' ] );
		$txtNDKDPTerima      = floatval( $dsJual[ 'DKDPTerima' ] );
		$txtNDPPO            = $txtNPrgSubsSupplier + $txtNPrgSubsDealer + $txtNPrgSubsFincoy + $txtNDKDPLeasing + $txtNDKDPInden + $txtNDKDPTerima;
		$txtNUMPKBayar       = "0.00";
		$txtUMPKNoBukti      = "--";
		$txtUMPKTglBukti     = "--";
		$txtNUMIndenBayar    = "0.00";
		$txtUMIndenNoBukti   = "--";
		$txtUMIndenTglBukti  = "--";
		$txtNUMDBayar        = "0.00";
		$txtUMDNoBukti       = "--";
		$txtUMDTglBukti      = "--";
		$txtNDKNettoBayar    = "0.00";
		$txtDKNettoNoBukti   = "--";
		$txtDKNettoTglBukti  = "--";
		$txtNRHBayar         = "0.00";
		$txtRHNoBukti        = "--";
		$txtRHTglBukti       = "--";
		$txtNSD2Bayar        = "0.00";
		$txtSD2NoBukti       = "--";
		$txtSD2TglBukti      = "--";
		$txtNISBayar         = "0.00";
		$txtISNoBukti        = "--";
		$txtISTglBukti       = "--";
		$txtNPKBayar         = "0.00";
		$txtPKNoBukti        = "--";
		$txtPKTglBukti       = "--";
		$txtNPKSisa          = "0.00";
		$txtNBBNBayar        = "0.00";
		$txtBBNNoBukti       = "--";
		$txtBBNTglBukti      = "--";
		$txtNBBNSisa         = "0.00";
		$txtNBBNPlusBayar    = "0.00";
		$txtBBNPlusNoBukti   = "--";
		$txtBBNPlusTglBukti  = "--";
		$txtNBBNPlusSisa     = "0.00";
		$Nominal             = 0;
		$JenisDK             = $dsJual[ 'DKJenis' ];
		$INNo                = $dsJual[ 'INNo' ];
		if ( $INNo == '--' ) {
			$INNo = '';
		}
		$foundRows    = Ttdk::find()->SelectKMBMDK( $dsJual[ 'DKNo' ], $INNo );
		$foundRowsCol = array_column( $foundRows, 'KMJenis' );
		$i            = array_search( 'Piutang UM', $foundRowsCol );
		if ( $i != null ) {
			$txtNUMPKBayar   = floatval( $foundRows[ $i ][ 'KMNominal' ] );
			$txtUMPKNoBukti  = $foundRows[ $i ][ 'KMNo' ];
			$txtUMPKTglBukti = $foundRows[ $i ][ 'KMTgl' ];
		}
		$txtNUMPKSisa = $txtNDKDPLeasing + $txtNUMPKBayar;
		$i            = array_search( 'Inden', $foundRowsCol );
		if ( $i != null ) {
			$txtNUMIndenBayar   = floatval( $foundRows[ $i ][ 'KMNominal' ] );
			$txtUMIndenNoBukti  = $foundRows[ $i ][ 'KMNo' ];
			$txtUMIndenTglBukti = $foundRows[ $i ][ 'KMTgl' ];
		}
		$txtNUMIndenSisa = $txtNDKDPInden + $txtNUMIndenBayar;
		$txtNBBNPlus     = floatval( $dsJual[ 'BBNPlus' ] );
		if ( $txtNBBNPlus != 0 ) {
			$i = array_search( 'BBN Plus', $foundRowsCol );
			if ( $i != null ) {
				$txtNBBNPlusBayar   = floatval( $foundRows[ $i ][ 'KMNominal' ] );
				$txtBBNPlusNoBukti  = $foundRows[ $i ][ 'KMNo' ];
				$txtBBNPlusTglBukti = $foundRows[ $i ][ 'KMTgl' ];
			}
			$txtNBBNPlusSisa = $txtNBBNPlus - $txtNBBNPlusBayar;
		}
		$i = array_search( 'Kredit', $foundRowsCol );
		if ( $i != null ) {
			$txtNUMDBayar   = floatval( $foundRows[ $i ][ 'KMNominal' ] );
			$txtUMDNoBukti  = $foundRows[ $i ][ 'KMNo' ];
			$txtUMDTglBukti = $foundRows[ $i ][ 'KMTgl' ];
		}
		$i = array_search( 'Tunai', $foundRowsCol );
		if ( $i != null ) {
			$txtNUMDBayar   = floatval( $foundRows[ $i ][ 'KMNominal' ] );
			$txtUMDNoBukti  = $foundRows[ $i ][ 'KMNo' ];
			$txtUMDTglBukti = $foundRows[ $i ][ 'KMTgl' ];
		}
		$txtNUMDSisa = $txtNDKDPTerima - $txtNUMDBayar;
		$i           = array_search( 'Leasing', $foundRowsCol );
		if ( $i != null ) {
			$txtNDKNettoBayar   = floatval( $foundRows[ $i ][ 'KMNominal' ] );
			$txtDKNettoNoBukti  = $foundRows[ $i ][ 'KMNo' ];
			$txtDKNettoTglBukti = $foundRows[ $i ][ 'KMTgl' ];
		}
		$i = array_search( 'Piutang Sisa', $foundRowsCol );
		if ( $i != null ) {
			$txtNDKNettoBayar   = floatval( $foundRows[ $i ][ 'KMNominal' ] );
			$txtDKNettoNoBukti  = $foundRows[ $i ][ 'KMNo' ];
			$txtDKNettoTglBukti = $foundRows[ $i ][ 'KMTgl' ];
		}
		$txtNDKNetto     = floatval( $dsJual[ 'DKNetto' ] );
		$txtNDKNettoSisa = $txtNDKNetto - $txtNDKNettoBayar;
		$foundRows       = Ttdk::find()->SelectKKBKDK( $dsJual[ 'DKNo' ] );
		$foundRowsCol    = array_column( $foundRows, 'KKJenis' );
		$i               = array_search( 'Retur Harga', $foundRowsCol );
		if ( $i != null ) {
			$txtNRHBayar   = floatval( $foundRows[ $i ][ 'KKNominal' ] );
			$txtRHNoBukti  = $foundRows[ $i ][ 'KKNo' ];
			$txtRHTglBukti = $foundRows[ $i ][ 'KKTgl' ];
		}
		$txtNReturHarga = floatval( $dsJual[ 'ReturHarga' ] );
		$txtNRHSisa     = $txtNReturHarga + $txtNRHBayar;
		$i              = array_search( 'SubsidiDealer2', $foundRowsCol );
		if ( $i != null ) {
			$txtNSD2Bayar   = floatval( $foundRows[ $i ][ 'KKNominal' ] );
			$txtSD2NoBukti  = $foundRows[ $i ][ 'KKNo' ];
			$txtSD2TglBukti = $foundRows[ $i ][ 'KKTgl' ];
		}
		$txtNPotonganHarga = floatval( $dsJual[ 'PotonganHarga' ] );
		$txtNSD2Sisa       = $txtNPotonganHarga + $txtNSD2Bayar;
		$i                 = array_search( 'Insentif Sales', $foundRowsCol );
		if ( $i != null ) {
			$txtNISBayar   = floatval( $foundRows[ $i ][ 'KKNominal' ] );
			$txtISNoBukti  = $foundRows[ $i ][ 'KKNo' ];
			$txtISTglBukti = $foundRows[ $i ][ 'KKTgl' ];
		}
		$txtNInsentifSales = floatval( $dsJual[ 'InsentifSales' ] );
		$txtNISSisa        = $txtNInsentifSales + $txtNISBayar;
		$i                 = array_search( 'Bayar BBN', $foundRowsCol );
		if ( $i != null ) {
			$txtNBBNBayar   = floatval( $foundRows[ $i ][ 'KKNominal' ] );
			$txtBBNNoBukti  = $foundRows[ $i ][ 'KKNo' ];
			$txtBBNTglBukti = $foundRows[ $i ][ 'KKTgl' ];
		}
		$txtNBBN     = floatval( $dsJual[ 'BBN' ] );
		$txtNBBNSisa = $txtNBBN + $txtNBBNBayar;
		$i           = array_search( 'Potongan Khusus', $foundRowsCol );
		if ( $i != null ) {
			$txtNPKBayar   = floatval( $foundRows[ $i ][ 'KKNominal' ] );
			$txtPKNoBukti  = $foundRows[ $i ][ 'KKNo' ];
			$txtPKTglBukti = $foundRows[ $i ][ 'KKTgl' ];
		}
		$txtNPotonganKhusus          = floatval( $dsJual[ 'PotonganKhusus' ] );
		$txtNPKSisa                  = $txtNPotonganKhusus + $txtNPKBayar;
		$dsJual[ 'DPPO' ]            = $txtNDPPO;
		$dsJual[ 'UMPKBayar' ]       = $txtNUMPKBayar;
		$dsJual[ 'UMPKNoBukti' ]     = $txtUMPKNoBukti;
		$dsJual[ 'UMPKTglBukti' ]    = $txtUMPKTglBukti;
		$dsJual[ 'UMPKSisa' ]        = $txtNUMPKSisa;
		$dsJual[ 'UMIndenSisa' ]     = $txtNUMIndenSisa;
		$dsJual[ 'UMIndenBayar' ]    = $txtNUMIndenBayar;
		$dsJual[ 'UMIndenNoBukti' ]  = $txtUMIndenNoBukti;
		$dsJual[ 'UMIndenTglBukti' ] = $txtUMIndenTglBukti;
		$dsJual[ 'BBNPlusBayar' ]    = $txtNBBNPlusBayar;
		$dsJual[ 'BBNPlusNoBukti' ]  = $txtBBNPlusNoBukti;
		$dsJual[ 'BBNPlusTglBukti' ] = $txtBBNPlusTglBukti;
		$dsJual[ 'BBNPlusSisa' ]     = $txtNBBNPlusSisa;
		$dsJual[ 'UMDBayar' ]        = $txtNUMDBayar;
		$dsJual[ 'UMDNoBukti' ]      = $txtUMDNoBukti;
		$dsJual[ 'UMDTglBukti' ]     = $txtUMDTglBukti;
		$dsJual[ 'UMDSisa' ]         = $txtNUMDSisa;
		$dsJual[ 'DKNettoBayar' ]    = $txtNDKNettoBayar;
		$dsJual[ 'DKNettoNoBukti' ]  = $txtDKNettoNoBukti;
		$dsJual[ 'DKNettoTglBukti' ] = $txtDKNettoTglBukti;
		$dsJual[ 'DKNettoSisa' ]     = $txtNDKNettoSisa;
		$dsJual[ 'RHBayar' ]         = $txtNRHBayar;
		$dsJual[ 'RHNoBukti' ]       = $txtRHNoBukti;
		$dsJual[ 'RHTglBukti' ]      = $txtRHTglBukti;
		$dsJual[ 'RHSisa' ]          = $txtNRHSisa;
		$dsJual[ 'SD2Bayar' ]        = $txtNSD2Bayar;
		$dsJual[ 'SD2NoBukti' ]      = $txtSD2NoBukti;
		$dsJual[ 'SD2TglBukti' ]     = $txtSD2TglBukti;
		$dsJual[ 'SD2Sisa' ]         = $txtNSD2Sisa;
		$dsJual[ 'ISBayar' ]         = $txtNISBayar;
		$dsJual[ 'ISNoBukti' ]       = $txtISNoBukti;
		$dsJual[ 'ISTglBukti' ]      = $txtISTglBukti;
		$dsJual[ 'ISSisa' ]          = $txtNISSisa;
		$dsJual[ 'BBNBayar' ]        = $txtNBBNBayar;
		$dsJual[ 'BBNNoBukti' ]      = $txtBBNNoBukti;
		$dsJual[ 'BBNTglBukti' ]     = $txtBBNTglBukti;
		$dsJual[ 'BBNSisa' ]         = $txtNBBNSisa;
		$dsJual[ 'PKBayar' ]         = $txtNPKBayar;
		$dsJual[ 'PKNoBukti' ]       = $txtPKNoBukti;
		$dsJual[ 'PKTglBukti' ]      = $txtPKTglBukti;
		$dsJual[ 'PKSisa' ]          = $txtNPKSisa;
		return $this->render( 'data-konsumen-status-update', [
			'model'             => $model,
			'dsTJualDatKonFill' => $dsJual,
			'id'                => $this->generateIdBase64FromModel( $model ),
		] );
	}
	public function actionStnkBpkbCreate() {
		$model = new Ttdk();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'ttdk/stnk-bpkb', 'id' => $model->DKNo ] );
		}
		return $this->render( 'stnk-bpkb-create', [
			'model' => $model,
		] );
	}
	public function actionStnkBpkbUpdate( $id ) {
		/** @var Tmotor $model */
		$model = $this->findModelBase64( 'Tmotor', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'ttdk/data-konsumen-tunai', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
		$dsJual = $model->find()->dsTJualFillByNo()->where( "(tmotor.DKNo = :DKNo)", [
			':DKNo' => $model->DKNo,
		] )->asArray( true )->one();
		return $this->render( 'stnk-bpkb-update', [
			'model'  => $model,
			'dsJual' => $dsJual,
			'id'     => $this->generateIdBase64FromModel( $model ),
		] );
	}
	public function actionKwitansiKirimTagihCreate() {
		$model = new Ttdk();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'ttdk/kwitansi-kirim-tagih', 'id' => $model->DKNo ] );
		}
		return $this->render( 'kwitansi-kirim-tagih-create', [
			'model' => $model,
		] );
	}
	public function actionKwitansiUMKonsumenCreate() {
		return $this->create( 'Kwitansi UM Konsumen', 'kwitansi-um-konsumen', 'DK - Kwitansi UM Konsumen' );
	}
}
