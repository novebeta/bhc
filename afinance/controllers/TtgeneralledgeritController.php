<?php

namespace afinance\controllers;

use Yii;
use afinance\models\Ttgeneralledgerit;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TtgeneralledgeritController implements the CRUD actions for Ttgeneralledgerit model.
 */
class TtgeneralledgeritController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ttgeneralledgerit models.
     * @return mixed
     */
    public function actionIndex()
    {



        return $this->render('index');
    }

    /**
     * Displays a single Ttgeneralledgerit model.
     * @param string $NoGL
     * @param string $TglGL
     * @param integer $GLAutoN
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($NoGL, $TglGL, $GLAutoN)
    {
        return $this->render('view', [
            'model' => $this->findModel($NoGL, $TglGL, $GLAutoN),
        ]);
    }

    /**
     * Creates a new Ttgeneralledgerit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ttgeneralledgerit();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'NoGL' => $model->NoGL, 'TglGL' => $model->TglGL, 'GLAutoN' => $model->GLAutoN]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ttgeneralledgerit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $NoGL
     * @param string $TglGL
     * @param integer $GLAutoN
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($NoGL, $TglGL, $GLAutoN)
    {
        $model = $this->findModel($NoGL, $TglGL, $GLAutoN);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'NoGL' => $model->NoGL, 'TglGL' => $model->TglGL, 'GLAutoN' => $model->GLAutoN]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ttgeneralledgerit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $NoGL
     * @param string $TglGL
     * @param integer $GLAutoN
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($NoGL, $TglGL, $GLAutoN)
    {
        $this->findModel($NoGL, $TglGL, $GLAutoN)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ttgeneralledgerit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $NoGL
     * @param string $TglGL
     * @param integer $GLAutoN
     * @return Ttgeneralledgerit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($NoGL, $TglGL, $GLAutoN)
    {
        if (($model = Ttgeneralledgerit::findOne(['NoGL' => $NoGL, 'TglGL' => $TglGL, 'GLAutoN' => $GLAutoN])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
