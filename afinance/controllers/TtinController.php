<?php
namespace afinance\controllers;
use afinance\components\AfinanceController;
use afinance\components\TdAction;
use afinance\models\Tdcustomer;
use afinance\models\Ttin;

use common\components\General;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
/**
 * TtinController implements the CRUD actions for Ttin model.
 */
class TtinController extends AfinanceController {
	public function actions() {
		$colGrid  = null;
		$joinWith = [
			'leasing'  => [
				'type' => 'INNER JOIN'
			],
			'sales'    => [
				'type' => 'INNER JOIN'
			],
			'customer' => [
				'type' => 'INNER JOIN'
			],
		];
		$join     = [];
		$where    = [];
		if ( isset( $_POST[ 'query' ] ) ) {
			$r = Json::decode( $_POST[ 'query' ], true )[ 'r' ];
			switch ( $r ) {
				case 'ttin/index':
					$colGrid                    = Ttin::colGrid();
					$joinWith[ 'dataKonsumen' ] = [
						'type' => 'LEFT OUTER JOIN'
					];
					break;
				case 'ttin/kas-masuk-inden':
					$colGrid = Ttin::colGridMutasiInternalPos();
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "LEFT(ttsmhd.SMNo,2) = 'MI'",
							'params'    => []
						]
					];
					$join    = [
						[
							'type'   => 'LEFT OUTER JOIN',
							'table'  => "(SELECT vtkm.KMLink, vtkm.KMJenis, SUM(vtkm.KMNominal) AS Inden FROM vtkm WHERE vtkm.KMJenis = 'Inden' AND vtkm.KMNo <> '\" & Number & \"' GROUP BY vtkm.KMLink, vtkm.KMJenis) ttkM",
							'on'     => 'ttin.INNo = ttkM.KMLINK',
							'params' => []
						]
					];
					break;
			}
		}
		return [
			'td' => [
				'class'    => TdAction::className(),
				'model'    => Ttin::className(),
				'colGrid'  => $colGrid,
				'joinWith' => $joinWith,
				'join'     => $join,
				'where'    => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttin models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index', [
			'mode' => ''
		] );
	}
	/**
	 * Finds the Ttin model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttin the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttin::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttin model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model        = new Ttin();
		$model->INNo  = General::createNo( 'IN', 'Ttin', 'INNo' );
		$model->INTgl = Yii::$app->formatter->asDate( 'now', 'yyyy-MM-dd' );
		$model->save();
		$model->refresh();
		$dsJual = $model->find()->dsTJual()->where( " (ttin.INNo = :INNo)", [
			':INNo' => $model->INNo,
		] )->asArray( true )->one();
		$model->delete();
		$dsJual[ 'CusTglLhr' ] = Yii::$app->formatter->asDate( '-9000 day', 'yyyy-MM-dd' );
		$dsJual[ 'INDP' ]      = 0;
		$dsJual[ 'CusKode' ]      = '--';
		return $this->render( 'create', [
			'model'  => $model,
			'dsJual' => $dsJual,
		] );
	}
	/**
	 * Updates an existing Ttin model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		if(Yii::$app->request->isPost){
			$post = Yii::$app->request->post();
			$customer = Tdcustomer::findOne($post['CusKode']);
			if($customer == null){
				$customer = new Tdcustomer;
				$customer->CusKode = General::createNo('KS','tdcustomer','CusKode');
				unset($post['CusKode']);
			}
			$customer->load($post,'');
			$customer->save();
			$inden = Ttin::findOne($post['INNo']);
			if($inden == null){
				$inden = new Ttin;
				$inden->INNo  = General::createNo( 'IN', 'Ttin', 'INNo' );
				unset($post['INNo']);
			}
			$inden->load($post,'');
			$inden->save();
			$id = $inden->INNo;
			return $this->redirect( [ 'ttin/index', 'id' => base64_encode($id) ] );
		}
		/** @var Ttin $model */
		$model = $this->findModelBase64( 'Ttin', $id );
		$dsJual = $model->find()->dsTJual()->where( " (ttin.INNo = :INNo)", [
			':INNo' => $model->INNo,
		] )->asArray( true )->one();
		return $this->render( 'update', [
			'model'  => $model,
			'dsJual' => $dsJual,
		] );
	}
	/**
	 * Deletes an existing Ttin model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}

	/**
	 * Lists all Ttfb models.
	 * @return mixed
	 */
	public function actionSelect() {
		$this->layout = 'select-mode';
		return $this->render( 'index', [
			'mode' => self::MODE_SELECT
		] );
	}
}
