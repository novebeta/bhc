<?php
namespace afinance\controllers;
use afinance\components\AfinanceController;
use afinance\components\TdAction;
use afinance\models\Tdcustomer;
use afinance\models\Ttabhd;

use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtabhdController implements the CRUD actions for Ttabhd model.
 */
class TtabhdController extends AfinanceController {
	public function actions() {
		return [
			'td' => [
				'class' => TdAction::className(),
				'model' => Ttabhd::className(),
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttabhd models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index');
	}
	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
//	public function actionPengajuanBerkas() {
//
//		$dataProvider = $searchModel->search( Yii::$app->request->queryParams );
//		return $this->render( 'PengajuanBerkas', [
//			'searchModel'  => $searchModel,
//			'dataProvider' => $dataProvider,
//		] );
//	}
	/**
	 * Displays a single Ttabhd model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Finds the Ttabhd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttabhd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttabhd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttabhd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Ttabhd();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'id' => $model->ABNo ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Ttabhd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id, $action ) {
//		$model = $this->findModel( $id );
//		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
//			return $this->redirect( [ 'view', 'id' => $model->ABNo ] );
//		}
//		return $this->render( 'update', [
//			'model' => $model,
//		] );
        $model = $this->findModelBase64( 'Ttabhd', $id );
        $post  = Yii::$app->request->post();
        if ( $action === 'create' ) {
            $ref = \common\components\General::createNo('SS', Ttabhd::tableName(), 'ABNo');

            /* data detail : update Reference  */
            /* data header : update Reference */
            $post['Ttabhd']['ABNo'] = $ref;
        }
        Yii::$app->request->setBodyParams( $post );
        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'index' ] );
        }
        return $this->render( 'update', [
            'model' => $model,
            'id'    => $id
        ] );
	}
	/**
	 * Deletes an existing Ttabhd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
}
