<?php

namespace afinance\controllers;

use afinance\components\TdAction;
use Yii;
use afinance\models\Tsaldomotor;

use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TsaldomotorController implements the CRUD actions for Tsaldomotor model.
 */
class TsaldomotorController extends \afinance\components\AfinanceController
{
	public function actions() {
		return [
			'td' => [
				'class'    => TdAction::className(),
				'model'    => Tsaldomotor::className(),
				'useHaving' => true,
				'grupBy' => ['NoTrans','TglTrans']
			],
		];
	}
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tsaldomotor models.
     * @return mixed
     */
    public function actionIndex()
    {

        return $this->render('index');
    }

    /**
     * Displays a single Tsaldomotor model.
     * @param string $NoTrans
     * @param string $TglTrans
     * @param integer $MotorAutoN
     * @param string $MotorNoMesin
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($NoTrans, $TglTrans, $MotorAutoN, $MotorNoMesin)
    {
        return $this->render('view', [
            'model' => $this->findModel($NoTrans, $TglTrans, $MotorAutoN, $MotorNoMesin),
        ]);
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function actionDetail() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $requestData                 = \Yii::$app->request->post();
        $primaryKeys = Tsaldomotor::getTableSchema()->primaryKey;
        if ( isset( $requestData['oper'] ) ) {
            /* operation : create, edit or delete */
            $oper = $requestData['oper'];
            $model = $oper === 'add' ? new Tsaldomotor() :
                $this->findModelBase64( 'Tsaldomotor', $requestData['id'] );
            switch ( $oper ) {
                case 'add'  :
                case 'edit' :
                    $model->attributes = $requestData;
                    if ( $oper === 'add' ) {
                        $count = ( new \yii\db\Query() )
                            ->from( 'tsaldomotor' )
                            ->where( [
                                'NoTrans'  => $requestData['NoTrans'],
                                'TglTrans' => $requestData['TglTrans']
                            ] )->max( 'MotorAutoN' );
                        if ( ! $count ) {
                            $count = 0;
                        }
                        $model->MotorAutoN = ++ $count;
                    }
                    $response = $model->save() ?
                        $this->responseSuccess( 'Data berhasil disimpan.' ) :
                        $this->responseFailed( $this->modelErrorSummary( $model ) );
                    break;
                case 'del'  :
                    $response = $model->delete() ?
                        $this->responseSuccess( 'Data berhasil dihapus.' ) :
                        $this->responseFailed( $this->modelErrorSummary( $model ) );
                    break;
            }
        } else {
            /* operation : read */
            for ($i = 0; $i < count($primaryKeys); $i++)
                $primaryKeys[$i] = 'tsaldomotor.'.$primaryKeys[$i];

            $query = ( new \yii\db\Query() )
                ->select( [
                    'CONCAT(' . implode( ",'||',", $primaryKeys ) . ') AS id',
                    'tsaldomotor.*'
                ] )
                ->from( 'tsaldomotor' )
                ->where( [
                    'NoTrans'  => $requestData['NoTrans'],
                    'TglTrans' => $requestData['TglTrans']
                ] )
                ->orderBy('MotorAutoN');
            $rowsCount = $query->count();
            $rows      = $query->all();
            /* encode row id */
            for ( $i = 0; $i < count( $rows ); $i ++ ) {
                $rows[ $i ]['id'] = base64_encode( $rows[ $i ]['id'] );
            }
            $response = [
                'rows'      => $rows,
                'rowsCount' => $rowsCount
            ];
        }
        return $response;
    }

    /**
     * Creates a new Tsaldomotor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tsaldomotor();
        $model->NoTrans = Tsaldomotor::generateReference(true);
        $model->TglTrans = date('Y-m-d');

        $id = $this->generateIdBase64FromModel( $model );
        return $this->render('create', [
            'model' => $model,
            'id'    => $id
        ]);
    }

    /**
     * Updates an existing Tsaldomotor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @param string $action
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $action)
    {
        if ( $action === 'update' )
            $model = $this->findModelBase64( 'Tsaldomotor', $id );

        $post = Yii::$app->request->post();
        if(isset($post['Tsaldomotor'])) {

            if ( $action === 'create' )
                $post['Tsaldomotor']['NoTrans'] = Tsaldomotor::generateReference();

            $arrId = $this->decodeIdBase64('Tsaldomotor', $id );

            /* data detail : update NoTrans, TglTrans */
            Tsaldomotor::updateAll(
                [
                    'NoTrans'  => $post['Tsaldomotor']['NoTrans'],
                    'TglTrans'  => $post['Tsaldomotor']['TglTrans']
                ],
                'NoTrans = :NoTrans AND TglTrans = :TglTrans',
                [ ':NoTrans'  => $arrId['NoTrans'], ':TglTrans'  => $arrId['TglTrans'] ]
            );

            return $this->redirect( [ 'index' ] );
        }

        return $this->render('update', [
            'model' => $model,
            'id'    => $id
        ]);
    }

    /**
     * Deletes an existing Tsaldomotor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @param string $action
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCancel($id, $action)
    {
        if ( $action === 'create' ) {
            $arrId = $this->decodeIdBase64('Tsaldomotor', $id );

            /* delete data detail  */
            Tsaldomotor::deleteAll( 'NoTrans = :NoTrans AND TglTrans = :TglTrans', [ ':NoTrans'  => $arrId['NoTrans'], ':TglTrans'  => $arrId['TglTrans'] ] );
        }

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing Tsaldomotor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $NoTrans
     * @param string $TglTrans
     * @param integer $MotorAutoN
     * @param string $MotorNoMesin
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($NoTrans, $TglTrans, $MotorAutoN, $MotorNoMesin)
    {
        $this->findModel($NoTrans, $TglTrans, $MotorAutoN, $MotorNoMesin)->delete();

        return $this->redirect(['index']);
    }

//    public function actionFind()
//    {
//        $post = Yii::$app->request->post();
//        $query =  Tsaldomotor::find();
//
//        if(isset($post['MotorType']))
//            $query->where('MotorType = :MotorType', [ ':MotorType' => $post['MotorType'] ]);
//
//        return [
//            'results' => $query->all(),
//            'count' => $query->count()
//        ];
//    }

    /**
     * Finds the Tsaldomotor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $NoTrans
     * @param string $TglTrans
     * @param integer $MotorAutoN
     * @param string $MotorNoMesin
     * @return Tsaldomotor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($NoTrans, $TglTrans, $MotorAutoN, $MotorNoMesin)
    {
        if (($model = Tsaldomotor::findOne(['NoTrans' => $NoTrans, 'TglTrans' => $TglTrans, 'MotorAutoN' => $MotorAutoN, 'MotorNoMesin' => $MotorNoMesin])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
