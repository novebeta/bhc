<?php

namespace afinance\assets;

use yii\web\AssetBundle;

/**
 * Main afinance application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/ui.jqgrid-bootstrap.css',
        'css/bootstrap-datepicker.min.css',
        'css/bootstrap-clockpicker.min.css',
        'css/animate.css',
        'select2/css/select2.min.css',
    ];
    public $js = [
//    	'js/grid.locale-en.js',
    	'jqgrid/js/grid.locale-id.js',
    	'jqgrid/js/jquery.jqGrid.min.js',
    	'js/jquery.resize.js',
    	'js/bootstrap-datepicker.min.js',
        'js/bootstrap-datepicker.id.min.js',
        'js/bootstrap-clockpicker.min.js',
    	'js/bootstrap-notify.min.js',
        'select2/js/select2.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
