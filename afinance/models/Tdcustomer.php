<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "tdcustomer".
 *
 * @property string $CusKode
 * @property string $CusKTP
 * @property string $CusNama
 * @property string $CusAlamat
 * @property string $CusRT
 * @property string $CusRW
 * @property string $CusProvinsi
 * @property string $CusKabupaten
 * @property string $CusKecamatan
 * @property string $CusKelurahan
 * @property string $CusKodePos
 * @property string $CusTelepon
 * @property string $CusSex
 * @property string $CusTempatLhr
 * @property string $CusTglLhr
 * @property string $CusAgama
 * @property string $CusPekerjaan
 * @property string $CusPendidikan
 * @property string $CusPengeluaran
 * @property string $CusTglBeli
 * @property string $CusKeterangan
 * @property string $CusEmail
 * @property string $CusKodeKons
 * @property string $CusStatusHP
 * @property string $CusStatusRumah
 * @property string $CusPembeliNama
 * @property string $CusPembeliKTP
 * @property string $CusPembeliAlamat
 * @property string $CusKK
 * @property string $CusTelepon2
 */
class Tdcustomer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdcustomer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CusKode'], 'required'],
            [['CusTglLhr', 'CusTglBeli'], 'safe'],
            [['CusKode', 'CusStatusHP'], 'string', 'max' => 10],
            [['CusKTP', 'CusPembeliKTP'], 'string', 'max' => 18],
            [['CusNama', 'CusKabupaten', 'CusKecamatan', 'CusKelurahan', 'CusEmail', 'CusPembeliNama'], 'string', 'max' => 50],
            [['CusAlamat'], 'string', 'max' => 75],
            [['CusRT', 'CusRW'], 'string', 'max' => 3],
            [['CusProvinsi', 'CusTelepon', 'CusTempatLhr', 'CusTelepon2'], 'string', 'max' => 30],
            [['CusKodePos'], 'string', 'max' => 7],
            [['CusSex'], 'string', 'max' => 12],
            [['CusAgama', 'CusStatusRumah'], 'string', 'max' => 15],
            [['CusPekerjaan', 'CusPengeluaran', 'CusKodeKons'], 'string', 'max' => 35],
            [['CusPendidikan'], 'string', 'max' => 20],
            [['CusKeterangan'], 'string', 'max' => 100],
            [['CusPembeliAlamat'], 'string', 'max' => 125],
            [['CusKK'], 'string', 'max' => 16],
            [['CusKode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CusKode' => 'Kode',
            'CusKTP' => 'KTP',
            'CusNama' => 'Nama',
            'CusAlamat' => 'Alamat',
            'CusRT' => 'RT',
            'CusRW' => 'RW',
            'CusProvinsi' => 'Provinsi',
            'CusKabupaten' => 'Kabupaten',
            'CusKecamatan' => 'Kecamatan',
            'CusKelurahan' => 'Kelurahan',
            'CusKodePos' => 'Kode Pos',
            'CusTelepon' => 'Telepon',
            'CusSex' => 'Sex',
            'CusTempatLhr' => 'Tempat Lhr',
            'CusTglLhr' => 'Tgl Lhr',
            'CusAgama' => 'Agama',
            'CusPekerjaan' => 'Pekerjaan',
            'CusPendidikan' => 'Pendidikan',
            'CusPengeluaran' => 'Pengeluaran',
            'CusTglBeli' => 'Tgl Beli',
            'CusKeterangan' => 'Keterangan',
            'CusEmail' => 'Email',
            'CusKodeKons' => 'Kode Kons',
            'CusStatusHP' => 'Status Hp',
            'CusStatusRumah' => 'Status Rumah',
            'CusPembeliNama' => 'Pembeli Nama',
            'CusPembeliKTP' => 'Pembeli Ktp',
            'CusPembeliAlamat' => 'Pembeli Alamat',
            'CusKK' => 'Kk',
            'CusTelepon2' => 'Telepon2',
        ];
    }

	public static function colGrid()
	{
		return [
			'CusKode' => 'Kode',
			'CusNama' => 'Nama',
			'CusAlamat' => 'Alamat',
			'CusTelepon' => 'Telepon',
			'CusKabupaten' => 'Kabupaten',
			'CusKecamatan' => 'Kecamatan',
			'CusKelurahan' => 'Kelurahan',
		];
	}

    /**
     * {@inheritdoc}
     * @return TdcustomerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdcustomerQuery(get_called_class());
    }
}
