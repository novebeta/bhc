<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttsd]].
 *
 * @see Ttsd
 */
class TtsdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttsd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttsd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
