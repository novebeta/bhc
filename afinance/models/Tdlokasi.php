<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "tdlokasi".
 *
 * @property string $LokasiKode
 * @property string $LokasiNama
 * @property string $LokasiPetugas
 * @property string $LokasiAlamat
 * @property string $LokasiStatus
 * @property string $LokasiTelepon
 * @property string $LokasiNomor
 * @property string $NoAccount
 * @property string $LokasiJenis
 */
class Tdlokasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdlokasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['LokasiKode'], 'required'],
            [['LokasiKode'], 'string', 'max' => 15],
            [['LokasiNama'], 'string', 'max' => 35],
            [['LokasiPetugas', 'NoAccount'], 'string', 'max' => 10],
            [['LokasiAlamat'], 'string', 'max' => 75],
            [['LokasiStatus'], 'string', 'max' => 1],
            [['LokasiTelepon'], 'string', 'max' => 30],
            [['LokasiNomor'], 'string', 'max' => 2],
            [['LokasiJenis'], 'string', 'max' => 6],
            [['LokasiKode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'LokasiKode' => 'Kode',
            'LokasiNama' => 'Nama',
            'LokasiPetugas' => 'Petugas',
            'LokasiAlamat' => 'Alamat',
            'LokasiStatus' => 'Status',
            'LokasiTelepon' => 'Telepon',
            'LokasiNomor' => 'No Urut',
            'NoAccount' => 'COA',
            'LokasiJenis' => 'Jenis',
        ];
    }

	public static function colGrid()
	{
		return [
			'LokasiKode' => 'Kode',
			'LokasiNama' => 'Nama',
			'LokasiAlamat' => 'Alamat',
			'LokasiPetugas' => 'Petugas',
			'LokasiStatus' => 'Status',
		];
	}

    /**
     * {@inheritdoc}
     * @return TdlokasiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdlokasiQuery(get_called_class());
    }
}
