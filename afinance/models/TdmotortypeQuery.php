<?php
namespace afinance\models;
use phpDocumentor\Reflection\Types\Self_;
use phpDocumentor\Reflection\Types\This;
use yii\helpers\ArrayHelper;
/**
 * This is the ActiveQuery class for [[Tdmotortype]].
 *
 * @see Tdmotortype
 */
class TdmotortypeQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Tdmotortype[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Tdmotortype|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function combo() {
		return $this->select( [ "CONCAT(MotorType,' - ',MotorNama) as label", "MotorType as value" ] )
		            ->orderBy( 'MotorType' )
		            ->asArray()
		            ->all();
	}
	public function select2() {
		return ArrayHelper::map( self::combo(), 'value', 'label' );
	}
}
