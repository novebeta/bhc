<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Tddealer]].
 *
 * @see Tddealer
 */
class TddealerQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tddealer[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tddealer|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	public function combo() {
		return $this->select( [ "CONCAT(DealerKode,' - ',DealerNama) as label", "DealerKode as value" ] )
		            ->orderBy( 'DealerKode' )
		            ->asArray()
		            ->all();
	}
}
