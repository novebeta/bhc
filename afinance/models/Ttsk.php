<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttsk".
 *
 * @property string $SKNo
 * @property string $SKTgl
 * @property string $LokasiKode
 * @property string $SKMemo
 * @property string $UserID
 * @property string $SKJam
 * @property string $NoGL
 * @property string $SKCetak
 * @property string $SKPengirim
 * @property string $DKNo
 */
class Ttsk extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttsk';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SKNo'], 'required'],
            [['SKTgl', 'SKJam'], 'safe'],
            [['SKNo', 'NoGL', 'DKNo'], 'string', 'max' => 10],
            [['LokasiKode', 'UserID'], 'string', 'max' => 15],
            [['SKMemo'], 'string', 'max' => 100],
            [['SKCetak'], 'string', 'max' => 5],
            [['SKPengirim'], 'string', 'max' => 50],
            [['SKNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SKNo' => 'Sk No',
            'SKTgl' => 'Sk Tgl',
            'LokasiKode' => 'Lokasi Kode',
            'SKMemo' => 'Sk Memo',
            'UserID' => 'User ID',
            'SKJam' => 'Sk Jam',
            'NoGL' => 'No Gl',
            'SKCetak' => 'Sk Cetak',
            'SKPengirim' => 'Sk Pengirim',
            'DKNo' => 'Dk No',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtskQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtskQuery(get_called_class());
    }
}
