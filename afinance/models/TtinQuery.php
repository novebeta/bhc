<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttin]].
 *
 * @see Ttin
 */
class TtinQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttin[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttin|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
