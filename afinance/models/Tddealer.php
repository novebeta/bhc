<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "tddealer".
 *
 * @property string $DealerKode
 * @property string $DealerNama
 * @property string $DealerContact
 * @property string $DealerAlamat
 * @property string $DealerKota
 * @property string $DealerProvinsi
 * @property string $DealerTelepon
 * @property string $DealerFax
 * @property string $DealerEmail
 * @property string $DealerKeterangan
 * @property string $DealerStatus
 * @property string $DealerKodeDAstra
 * @property string $DealerKodeDMain
 */
class Tddealer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tddealer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DealerKode'], 'required'],
            [['DealerKode'], 'string', 'max' => 10],
            [['DealerNama', 'DealerContact', 'DealerKota', 'DealerProvinsi', 'DealerFax'], 'string', 'max' => 50],
            [['DealerAlamat', 'DealerTelepon'], 'string', 'max' => 75],
            [['DealerEmail'], 'string', 'max' => 25],
            [['DealerKeterangan'], 'string', 'max' => 100],
            [['DealerStatus'], 'string', 'max' => 1],
            [['DealerKodeDAstra'], 'string', 'max' => 5],
            [['DealerKodeDMain'], 'string', 'max' => 3],
            [['DealerKode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'DealerKode' => 'Kode',
            'DealerNama' => 'Nama',
            'DealerContact' => 'Contact',
            'DealerAlamat' => 'Alamat',
            'DealerKota' => 'Kota',
            'DealerProvinsi' => 'Provinsi',
            'DealerTelepon' => 'Telepon',
            'DealerFax' => 'Fax',
            'DealerEmail' => 'Email',
            'DealerKeterangan' => 'Keterangan',
            'DealerStatus' => 'Status',
            'DealerKodeDAstra' => 'Kode Astra',
            'DealerKodeDMain' => 'Main Dealer',
        ];
    }

	public static function colGrid()
	{
		return [
			'DealerKode' => 'Kode',
			'DealerNama' => 'Nama',
			'DealerAlamat' => 'Alamat',
			'DealerKota' => 'Kota',
			'DealerProvinsi' => 'Provinsi',
			'DealerTelepon' => 'Telepon',
			'DealerContact' => 'Kontak',
			'DealerFax' => 'Fax',
		];
	}

    /**
     * {@inheritdoc}
     * @return TddealerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TddealerQuery(get_called_class());
    }
}
