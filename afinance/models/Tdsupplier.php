<?php
namespace afinance\models;
/**
 * This is the model class for table "tdsupplier".
 *
 * @property string $SupKode
 * @property string $SupNama
 * @property string $SupContact
 * @property string $SupAlamat
 * @property string $SupKota
 * @property string $SupTelepon
 * @property string $SupFax
 * @property string $SupEmail
 * @property string $SupKeterangan
 * @property string $SupStatus
 */
class Tdsupplier extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'tdsupplier';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'SupKode' ], 'required' ],
			[ [ 'SupKode' ], 'string', 'max' => 10 ],
			[ [ 'SupNama', 'SupContact', 'SupFax' ], 'string', 'max' => 50 ],
			[ [ 'SupAlamat', 'SupTelepon' ], 'string', 'max' => 75 ],
			[ [ 'SupKota', 'SupEmail' ], 'string', 'max' => 25 ],
			[ [ 'SupKeterangan' ], 'string', 'max' => 100 ],
			[ [ 'SupStatus' ], 'string', 'max' => 1 ],
			[ [ 'SupKode' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'SupKode'       => 'Kode',
			'SupNama'       => 'Nama',
			'SupContact'    => 'Kontak',
			'SupAlamat'     => 'Alamat',
			'SupKota'       => 'Kota',
			'SupTelepon'    => 'Telepon',
			'SupFax'        => 'Fax',
			'SupEmail'      => 'Email',
			'SupKeterangan' => 'Keterangan',
			'SupStatus'     => 'Status',
		];
	}

	public static function colGrid()
	{
		return [
			'SupKode'       => 'Kode',
			'SupNama'       => 'Nama',
			'SupAlamat'     => 'Alamat',
			'SupKota'       => 'Kota',
			'SupContact'    => 'Kontak',
			'SupTelepon'    => 'Telepon',
			'SupEmail'      => 'Email'
		];
	}

	/**
	 * {@inheritdoc}
	 * @return TdsupplierQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TdsupplierQuery( get_called_class() );
	}
}
