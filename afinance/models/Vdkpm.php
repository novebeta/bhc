<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "vdkpm".
 *
 * @property string $DKNo
 * @property string $DKTgl
 * @property string $Jenis
 * @property string $KeteranganJenis
 * @property string $CusKode
 * @property string $Awal
 */
class Vdkpm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vdkpm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DKTgl'], 'safe'],
            [['Awal'], 'number'],
            [['DKNo', 'CusKode'], 'string', 'max' => 10],
            [['Jenis'], 'string', 'max' => 14],
            [['KeteranganJenis'], 'string', 'max' => 26],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'DKNo' => 'Dk No',
            'DKTgl' => 'Dk Tgl',
            'Jenis' => 'Jenis',
            'KeteranganJenis' => 'Keterangan Jenis',
            'CusKode' => 'Cus Kode',
            'Awal' => 'Awal',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VdkpmQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VdkpmQuery(get_called_class());
    }
}
