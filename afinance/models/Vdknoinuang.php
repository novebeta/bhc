<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "vdknoinuang".
 *
 * @property string $NoTrans
 * @property string $DKNo
 */
class Vdknoinuang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vdknoinuang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NoTrans'], 'string', 'max' => 12],
            [['DKNo'], 'string', 'max' => 18],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'NoTrans' => 'No Trans',
            'DKNo' => 'Dk No',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VdknoinuangQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VdknoinuangQuery(get_called_class());
    }
}
