<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttbmit]].
 *
 * @see Ttbmit
 */
class TtbmitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttbmit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttbmit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
