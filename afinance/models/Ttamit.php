<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttamit".
 *
 * @property string $AMNo
 * @property string $MotorNoMesin
 * @property int $MotorAutoN
 */
class Ttamit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttamit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AMNo', 'MotorNoMesin'], 'required'],
            [['MotorAutoN'], 'integer'],
            [['AMNo'], 'string', 'max' => 10],
            [['MotorNoMesin'], 'string', 'max' => 25],
            [['AMNo', 'MotorNoMesin'], 'unique', 'targetAttribute' => ['AMNo', 'MotorNoMesin']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AMNo' => 'Am No',
            'MotorNoMesin' => 'Motor No Mesin',
            'MotorAutoN' => 'Motor Auto N',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtamitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtamitQuery(get_called_class());
    }
}
