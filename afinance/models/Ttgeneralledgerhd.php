<?php
namespace afinance\models;
/**
 * This is the model class for table "ttgeneralledgerhd".
 *
 * @property string $NoGL
 * @property string $TglGL
 * @property string $KodeTrans SS,FB,DK,SK,SM,PB,SD,PD,KK,KM,BK,BM
 * @property string $MemoGL
 * @property string $TotalDebetGL
 * @property string $TotalKreditGL
 * @property string $GLLink
 * @property string $UserID
 * @property string $HPLink
 * @property string $GLValid
 */
class Ttgeneralledgerhd extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttgeneralledgerhd';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'TglGL' ], 'required' ],
			[ [ 'TglGL' ], 'safe' ],
			[ [ 'TotalDebetGL', 'TotalKreditGL' ], 'number' ],
			[ [ 'NoGL' ], 'string', 'max' => 10 ],
			[ [ 'KodeTrans' ], 'string', 'max' => 2 ],
			[ [ 'MemoGL' ], 'string', 'max' => 300 ],
			[ [ 'GLLink', 'HPLink' ], 'string', 'max' => 18 ],
			[ [ 'UserID' ], 'string', 'max' => 15 ],
			[ [ 'GLValid' ], 'string', 'max' => 5 ],
			[ [ 'NoGL', 'TglGL' ], 'unique', 'targetAttribute' => [ 'NoGL', 'TglGL' ] ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'NoGL'          => 'No Jurnal',
			'TglGL'         => 'Tgl Jurnal',
			'KodeTrans'     => 'SS,FB,DK,SK,SM,PB,SD,PD,KK,KM,BK,BM',
			'MemoGL'        => 'Memo Jurnal',
			'TotalDebetGL'  => 'Debet',
			'TotalKreditGL' => 'Kredit',
			'GLLink'        => 'No Transaksi',
			'UserID'        => 'User ID',
			'HPLink'        => 'HP Link',
			'GLValid'       => 'Validasi',
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public static function colGrid() {
		return [
			'NoGL'          => 'No Jurnal',
			'TglGL'         => 'Tgl Jurnal',
			'MemoGL'        => 'Memo Jurnal',
			'TotalDebetGL'  => [
				'label'     => 'Debet',
				'name'      => 'TotalDebetGL',
				'formatter' => 'number',
				'align'     => 'right'
			],
			'TotalKreditGL' => [
				'label'     => 'Kredit',
				'name'      => 'TotalKreditGL',
				'formatter' => 'number',
				'align'     => 'right'
			],
			'GLLink'        => 'No Transaksi',
			'HPLink'        => 'HP Link',
			'GLValid'       => 'Validasi',
		];
	}
	/**
	 * {@inheritdoc}
	 * @return TtgeneralledgerhdQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TtgeneralledgerhdQuery( get_called_class() );
	}
}
