<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttbmitpm]].
 *
 * @see Ttbmitpm
 */
class TtbmitpmQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttbmitpm[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttbmitpm|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
