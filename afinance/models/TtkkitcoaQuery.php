<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttkkitcoa]].
 *
 * @see Ttkkitcoa
 */
class TtkkitcoaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttkkitcoa[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttkkitcoa|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
