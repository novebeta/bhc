<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "vglmotor".
 *
 * @property string $MotorType
 * @property string $MotorWarna
 * @property string $MotorTahun
 * @property int $MotorAutoN
 * @property string $MotorNoMesin
 * @property string $MotorNoRangka
 * @property string $KodeTrans
 * @property string $NoTrans
 * @property string $TglTrans
 * @property string $Harga
 * @property string $MMHarga
 * @property string $NoGL
 * @property string $STATUS
 */
class Vglmotor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vglmotor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['MotorTahun', 'Harga', 'MMHarga'], 'number'],
            [['MotorAutoN'], 'integer'],
            [['TglTrans'], 'safe'],
            [['MotorType'], 'string', 'max' => 50],
            [['MotorWarna'], 'string', 'max' => 35],
            [['MotorNoMesin', 'MotorNoRangka'], 'string', 'max' => 25],
            [['KodeTrans'], 'string', 'max' => 2],
            [['NoTrans'], 'string', 'max' => 18],
            [['NoGL'], 'string', 'max' => 10],
            [['STATUS'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'MotorType' => 'Motor Type',
            'MotorWarna' => 'Motor Warna',
            'MotorTahun' => 'Motor Tahun',
            'MotorAutoN' => 'Motor Auto N',
            'MotorNoMesin' => 'Motor No Mesin',
            'MotorNoRangka' => 'Motor No Rangka',
            'KodeTrans' => 'Kode Trans',
            'NoTrans' => 'No Trans',
            'TglTrans' => 'Tgl Trans',
            'Harga' => 'Harga',
            'MMHarga' => 'Mm Harga',
            'NoGL' => 'No Gl',
            'STATUS' => 'Status',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VglmotorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VglmotorQuery(get_called_class());
    }
}
