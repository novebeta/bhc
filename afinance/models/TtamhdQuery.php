<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttamhd]].
 *
 * @see Ttamhd
 */
class TtamhdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttamhd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttamhd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
