<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttdkprg".
 *
 * @property string $DKNo
 * @property string $ProgramNama
 * @property string $PrgSubsSupplier
 * @property string $PrgSubsDealer
 * @property string $PrgSubsFincoy
 * @property string $ProgramSubsidi
 */
class Ttdkprg extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttdkprg';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DKNo', 'ProgramNama'], 'required'],
            [['PrgSubsSupplier', 'PrgSubsDealer', 'PrgSubsFincoy', 'ProgramSubsidi'], 'number'],
            [['DKNo'], 'string', 'max' => 10],
            [['ProgramNama'], 'string', 'max' => 25],
            [['DKNo', 'ProgramNama'], 'unique', 'targetAttribute' => ['DKNo', 'ProgramNama']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'DKNo' => 'Dk No',
            'ProgramNama' => 'Program Nama',
            'PrgSubsSupplier' => 'Prg Subs Supplier',
            'PrgSubsDealer' => 'Prg Subs Dealer',
            'PrgSubsFincoy' => 'Prg Subs Fincoy',
            'ProgramSubsidi' => 'Program Subsidi',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtdkprgQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtdkprgQuery(get_called_class());
    }
}
