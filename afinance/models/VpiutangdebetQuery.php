<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Vpiutangdebet]].
 *
 * @see Vpiutangdebet
 */
class VpiutangdebetQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Vpiutangdebet[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Vpiutangdebet|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
