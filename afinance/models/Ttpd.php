<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttpd".
 *
 * @property string $PDNo
 * @property string $PDTgl
 * @property string $SDNo
 * @property string $DealerKode
 * @property string $LokasiKode
 * @property string $PDMemo
 * @property string $PDTotal
 * @property string $UserID
 * @property string $PDJam
 * @property string $NoGL
 */
class Ttpd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttpd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PDNo'], 'required'],
            [['PDTgl', 'PDJam'], 'safe'],
            [['PDTotal'], 'number'],
            [['PDNo', 'SDNo', 'DealerKode', 'NoGL'], 'string', 'max' => 10],
            [['LokasiKode', 'UserID'], 'string', 'max' => 15],
            [['PDMemo'], 'string', 'max' => 100],
            [['PDNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PDNo' => 'Pd No',
            'PDTgl' => 'Pd Tgl',
            'SDNo' => 'Sd No',
            'DealerKode' => 'Dealer Kode',
            'LokasiKode' => 'Lokasi Kode',
            'PDMemo' => 'Pd Memo',
            'PDTotal' => 'Pd Total',
            'UserID' => 'User ID',
            'PDJam' => 'Pd Jam',
            'NoGL' => 'No Gl',
        ];
    }


	public static function colGrid() {
		return [
			'PDNo' => 'Pd No',
			'PDTgl' => 'Pd Tgl',
			'SDNo' => 'Sd No',
			'tddealer.DealerKode' => [
				'label' => 'DealerKode',
				'width' => 105,
				'name'  => 'DealerKode'
			],
			'tdlokasi.LokasiNama' => [
				'label' => 'LokasiNama',
				'width' => 105,
				'name'  => 'LokasiNama'
			],
		];
	}
	public static function colGridInvoicePenerimaan() {
		return [
			'PDNo' => 'Pd No',
			'PDTgl' => 'Pd Tgl',
			'tddealer.DealerKode' => [
				'label' => 'DealerKode',
				'width' => 105,
				'name'  => 'DealerKode'
			],
			'tdlokasi.LokasiNama' => [
				'label' => 'LokasiNama',
				'width' => 105,
				'name'  => 'LokasiNama'
			],
		];
	}
	public static function colGridPenerimaanDealer() {
		return [
			'PDNo' => 'Pd No',
			'PDTgl' => 'Pd Tgl',
			'SDNo' => 'Sd No',
			'tddealer.DealerKode' => [
				'label' => 'DealerKode',
				'width' => 105,
				'name'  => 'DealerKode'
			],
			'tdlokasi.LokasiNama' => [
				'label' => 'LokasiNama',
				'width' => 105,
				'name'  => 'LokasiNama'
			],
		];
	}

	public function getLokasi() {
		return $this->hasOne( Tdlokasi::className(), [ 'LokasiKode' => 'LokasiKode' ] );
	}
	public function getDealer() {
		return $this->hasOne( Tddealer::className(), [ 'DealerKode' => 'DealerKode' ] );
	}

    /**
     * {@inheritdoc}
     * @return TtpdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtpdQuery(get_called_class());
    }
}
