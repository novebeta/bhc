<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttmsalestarget".
 *
 * @property string $SalesTgl
 * @property string $SalesKode
 * @property string $TeamKode
 * @property string $SalesQty
 */
class Ttmsalestarget extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttmsalestarget';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SalesTgl', 'SalesKode', 'TeamKode'], 'required'],
            [['SalesTgl'], 'safe'],
            [['SalesQty'], 'number'],
            [['SalesKode'], 'string', 'max' => 10],
            [['TeamKode'], 'string', 'max' => 15],
            [['SalesTgl', 'SalesKode', 'TeamKode'], 'unique', 'targetAttribute' => ['SalesTgl', 'SalesKode', 'TeamKode']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SalesTgl' => 'Sales Tgl',
            'SalesKode' => 'Sales Kode',
            'TeamKode' => 'Team Kode',
            'SalesQty' => 'Sales Qty',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtmsalestargetQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtmsalestargetQuery(get_called_class());
    }
}
