<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttpbhd".
 *
 * @property string $PBNo
 * @property string $PBTgl
 * @property string $SMNo
 * @property string $LokasiAsal
 * @property string $LokasiTujuan
 * @property string $PBMemo
 * @property string $PBTotal
 * @property string $UserID
 * @property string $PBJam
 */
class Ttpbhd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttpbhd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PBNo'], 'required'],
            [['PBTgl', 'PBJam'], 'safe'],
            [['PBTotal'], 'number'],
            [['PBNo', 'SMNo'], 'string', 'max' => 10],
            [['LokasiAsal', 'LokasiTujuan', 'UserID'], 'string', 'max' => 15],
            [['PBMemo'], 'string', 'max' => 100],
            [['PBNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PBNo' => 'Pb No',
            'PBTgl' => 'Pb Tgl',
            'SMNo' => 'Sm No',
            'LokasiAsal' => 'Lokasi Asal',
            'LokasiTujuan' => 'Lokasi Tujuan',
            'PBMemo' => 'Pb Memo',
            'PBTotal' => 'Pb Total',
            'UserID' => 'User ID',
            'PBJam' => 'Pb Jam',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtpbhdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtpbhdQuery(get_called_class());
    }
}
