<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttrk]].
 *
 * @see Ttrk
 */
class TtrkQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttrk[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttrk|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
