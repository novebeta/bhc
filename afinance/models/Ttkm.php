<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttkm".
 *
 * @property string $KMNo
 * @property string $KMTgl
 * @property string $KMMemo
 * @property string $KMNominal
 * @property string $KMJenis DP, Tunai, Umum
 * @property string $KMPerson
 * @property string $KMLink DKNo, BKNo
 * @property string $UserID
 * @property string $LokasiKode
 * @property string $KMJam
 * @property string $NoGL
 */
class Ttkm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttkm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KMNo'], 'required'],
            [['KMTgl', 'KMJam'], 'safe'],
            [['KMNominal'], 'number'],
            [['KMNo', 'KMLink'], 'string', 'max' => 12],
            [['KMMemo'], 'string', 'max' => 100],
            [['KMJenis'], 'string', 'max' => 20],
            [['KMPerson'], 'string', 'max' => 50],
            [['UserID', 'LokasiKode'], 'string', 'max' => 15],
            [['NoGL'], 'string', 'max' => 10],
            [['KMNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'KMNo' => 'Km No',
            'KMTgl' => 'Km Tgl',
            'KMMemo' => 'Km Memo',
            'KMNominal' => 'Km Nominal',
            'KMJenis' => 'DP, Tunai, Umum',
            'KMPerson' => 'Km Person',
            'KMLink' => 'DKNo, BKNo',
            'UserID' => 'User ID',
            'LokasiKode' => 'Lokasi Kode',
            'KMJam' => 'Km Jam',
            'NoGL' => 'No Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtkmQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtkmQuery(get_called_class());
    }
}
