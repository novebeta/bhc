<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Vmotorsalahjam]].
 *
 * @see Vmotorsalahjam
 */
class VmotorsalahjamQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Vmotorsalahjam[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Vmotorsalahjam|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
