<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "tdcekfisik".
 *
 * @property string $Kabupaten
 * @property string $Biaya
 * @property string $COADebet
 * @property string $COAKredit
 */
class Tdcekfisik extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdcekfisik';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Kabupaten'], 'required'],
            [['Biaya'], 'number'],
            [['Kabupaten'], 'string', 'max' => 50],
            [['COADebet', 'COAKredit'], 'string', 'max' => 10],
            [['Kabupaten'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Kabupaten' => 'Kabupaten',
            'Biaya' => 'Biaya',
            'COADebet' => 'Coa Debet',
            'COAKredit' => 'Coa Kredit',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TdcekfisikQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdcekfisikQuery(get_called_class());
    }
}
