<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttmmarketshare]].
 *
 * @see Ttmmarketshare
 */
class TtmmarketshareQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttmmarketshare[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttmmarketshare|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
