<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttmdo".
 *
 * @property string $DONo
 * @property string $DOTgl
 * @property string $MotorType
 * @property string $DOQty
 */
class Ttmdo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttmdo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DONo', 'DOTgl', 'MotorType'], 'required'],
            [['DOTgl'], 'safe'],
            [['DOQty'], 'number'],
            [['DONo'], 'string', 'max' => 10],
            [['MotorType'], 'string', 'max' => 25],
            [['DONo', 'DOTgl', 'MotorType'], 'unique', 'targetAttribute' => ['DONo', 'DOTgl', 'MotorType']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'DONo' => 'Do No',
            'DOTgl' => 'Do Tgl',
            'MotorType' => 'Motor Type',
            'DOQty' => 'Do Qty',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtmdoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtmdoQuery(get_called_class());
    }
}
