<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttkk".
 *
 * @property string $KKNo
 * @property string $KKTgl
 * @property string $KKMemo
 * @property string $KKNominal
 * @property string $KKJenis Subsidi2, ReturHarga, Umum, BBN, Jaket, Subsidi
 * @property string $KKPerson
 * @property string $KKLink
 * @property string $UserID
 * @property string $LokasiKode
 * @property string $KKJam
 * @property string $NoGL
 */
class Ttkk extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttkk';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KKNo'], 'required'],
            [['KKTgl', 'KKJam'], 'safe'],
            [['KKNominal'], 'number'],
            [['KKNo', 'KKLink'], 'string', 'max' => 12],
            [['KKMemo'], 'string', 'max' => 100],
            [['KKJenis'], 'string', 'max' => 20],
            [['KKPerson'], 'string', 'max' => 50],
            [['UserID', 'LokasiKode'], 'string', 'max' => 15],
            [['NoGL'], 'string', 'max' => 10],
            [['KKNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'KKNo' => 'Kk No',
            'KKTgl' => 'Kk Tgl',
            'KKMemo' => 'Kk Memo',
            'KKNominal' => 'Kk Nominal',
            'KKJenis' => 'Subsidi2, ReturHarga, Umum, BBN, Jaket, Subsidi',
            'KKPerson' => 'Kk Person',
            'KKLink' => 'Kk Link',
            'UserID' => 'User ID',
            'LokasiKode' => 'Lokasi Kode',
            'KKJam' => 'Kk Jam',
            'NoGL' => 'No Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtkkQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtkkQuery(get_called_class());
    }
}
