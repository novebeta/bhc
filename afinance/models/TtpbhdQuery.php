<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttpbhd]].
 *
 * @see Ttpbhd
 */
class TtpbhdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttpbhd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttpbhd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
