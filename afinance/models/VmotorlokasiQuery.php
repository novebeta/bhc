<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Vmotorlokasi]].
 *
 * @see Vmotorlokasi
 */
class VmotorlokasiQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Vmotorlokasi[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Vmotorlokasi|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
