<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Vtbanksaldo]].
 *
 * @see Vtbanksaldo
 */
class VtbanksaldoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Vtbanksaldo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Vtbanksaldo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
