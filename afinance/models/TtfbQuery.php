<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttfb]].
 *
 * @see Ttfb
 */
class TtfbQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttfb[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttfb|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
