<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttmpo".
 *
 * @property string $PONo
 * @property string $POTgl
 * @property string $MotorType
 * @property string $POQty
 */
class Ttmpo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttmpo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PONo', 'POTgl', 'MotorType'], 'required'],
            [['POTgl'], 'safe'],
            [['POQty'], 'number'],
            [['PONo'], 'string', 'max' => 10],
            [['MotorType'], 'string', 'max' => 50],
            [['PONo', 'POTgl', 'MotorType'], 'unique', 'targetAttribute' => ['PONo', 'POTgl', 'MotorType']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PONo' => 'Po No',
            'POTgl' => 'Po Tgl',
            'MotorType' => 'Motor Type',
            'POQty' => 'Po Qty',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtmpoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtmpoQuery(get_called_class());
    }
}
