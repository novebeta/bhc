<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttmpo]].
 *
 * @see Ttmpo
 */
class TtmpoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttmpo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttmpo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
