<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "tvgltrans".
 *
 * @property string $TransNo
 * @property string $TransTgl
 * @property string $TransJam
 * @property string $NoGL
 */
class Tvgltran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tvgltrans';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['TransNo'], 'required'],
            [['TransTgl', 'TransJam'], 'safe'],
            [['TransNo'], 'string', 'max' => 18],
            [['NoGL'], 'string', 'max' => 10],
            [['TransNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'TransNo' => 'Trans No',
            'TransTgl' => 'Trans Tgl',
            'TransJam' => 'Trans Jam',
            'NoGL' => 'No Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TvgltranQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TvgltranQuery(get_called_class());
    }
}
