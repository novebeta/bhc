<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "tvhutangkredit".
 *
 * @property string $GLLink
 * @property string $NoGL
 * @property string $TglGL
 * @property string $KodeTrans
 * @property int $GLAutoN
 * @property string $NoAccount
 * @property string $NamaAccount
 * @property string $MemoGL
 * @property string $KeteranganGL
 * @property string $KreditGL
 * @property string $HPLink
 */
class Tvhutangkredit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tvhutangkredit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['GLLink', 'NoGL', 'TglGL', 'KodeTrans', 'GLAutoN', 'NoAccount'], 'required'],
            [['TglGL'], 'safe'],
            [['GLAutoN'], 'integer'],
            [['KreditGL'], 'number'],
            [['GLLink'], 'string', 'max' => 18],
            [['NoGL', 'NoAccount'], 'string', 'max' => 10],
            [['KodeTrans'], 'string', 'max' => 2],
            [['NamaAccount', 'KeteranganGL'], 'string', 'max' => 150],
            [['MemoGL'], 'string', 'max' => 300],
            [['HPLink'], 'string', 'max' => 15],
            [['GLLink', 'NoGL', 'TglGL', 'KodeTrans', 'GLAutoN', 'NoAccount'], 'unique', 'targetAttribute' => ['GLLink', 'NoGL', 'TglGL', 'KodeTrans', 'GLAutoN', 'NoAccount']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'GLLink' => 'Gl Link',
            'NoGL' => 'No Gl',
            'TglGL' => 'Tgl Gl',
            'KodeTrans' => 'Kode Trans',
            'GLAutoN' => 'Gl Auto N',
            'NoAccount' => 'No Account',
            'NamaAccount' => 'Nama Account',
            'MemoGL' => 'Memo Gl',
            'KeteranganGL' => 'Keterangan Gl',
            'KreditGL' => 'Kredit Gl',
            'HPLink' => 'Hp Link',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TvhutangkreditQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TvhutangkreditQuery(get_called_class());
    }
}
