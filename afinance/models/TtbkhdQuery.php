<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttbkhd]].
 *
 * @see Ttbkhd
 */
class TtbkhdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttbkhd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttbkhd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
