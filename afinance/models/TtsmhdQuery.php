<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttsmhd]].
 *
 * @see Ttsmhd
 */
class TtsmhdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttsmhd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttsmhd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
