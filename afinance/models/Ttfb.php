<?php
namespace afinance\models;
/**
 * This is the model class for table "ttfb".
 *
 * @property string $FBNo
 * @property string $FBTgl
 * @property string $SSNo
 * @property string $SupKode
 * @property string $FBTermin
 * @property string $FBTglTempo
 * @property string $FBPSS
 * @property string $FBPtgLain
 * @property string $FBTotal
 * @property string $FBMemo
 * @property string $FBLunas
 * @property string $UserID
 * @property string $NoGL
 * @property string $FPNo
 * @property string $FPTgl
 * @property string $DOTgl
 * @property string $FBExtraDisc
 * @property string $FBPosting
 * @property string $FBPtgMD
 * @property string $FBJam
 */
class Ttfb extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttfb';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'FBNo' ], 'required' ],
			[ [ 'FBTgl', 'FBTglTempo', 'FPTgl', 'DOTgl', 'FBJam' ], 'safe' ],
			[ [ 'FBTermin', 'FBPSS', 'FBPtgLain', 'FBTotal', 'FBExtraDisc', 'FBPtgMD' ], 'number' ],
			[ [ 'FBNo', 'SSNo' ], 'string', 'max' => 18 ],
			[ [ 'SupKode', 'NoGL' ], 'string', 'max' => 10 ],
			[ [ 'FBMemo' ], 'string', 'max' => 100 ],
			[ [ 'FBLunas' ], 'string', 'max' => 5 ],
			[ [ 'UserID', 'FBPosting' ], 'string', 'max' => 15 ],
			[ [ 'FPNo' ], 'string', 'max' => 30 ],
			[ [ 'FBNo' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'FBNo'        => 'Fb No',
			'FBTgl'       => 'Fb Tgl',
			'SSNo'        => 'Ss No',
			'SupKode'     => 'Sup Kode',
			'FBTermin'    => 'Fb Termin',
			'FBTglTempo'  => 'Fb Tgl Tempo',
			'FBPSS'       => 'Fbpss',
			'FBPtgLain'   => 'Fb Ptg Lain',
			'FBTotal'     => 'Fb Total',
			'FBMemo'      => 'Fb Memo',
			'FBLunas'     => 'Fb Lunas',
			'UserID'      => 'User ID',
			'NoGL'        => 'No Gl',
			'FPNo'        => 'Fp No',
			'FPTgl'       => 'Fp Tgl',
			'DOTgl'       => 'Do Tgl',
			'FBExtraDisc' => 'Fb Extra Disc',
			'FBPosting'   => 'Fb Posting',
			'FBPtgMD'     => 'Fb Ptg Md',
			'FBJam'       => 'Fb Jam',
		];
	}
	public static function colGrid() {
		return [
			'ttfb.FBNo'                                       => [
				'label' => 'No FB',
				'width' => 105,
				'name'  => 'FBNo'
			],
			'FBTgl'                                           => [
				'label' => 'Tgl FB',
				'width' => 175,
				'name'  => 'FBTgl'
			],
			'ttfb.SSNo'                                       => [
				'label' => 'No Srt Jln',
				'width' => 175,
				'name'  => 'SSNo'
			],
			"IFNULL(ttss.SSTgl, DATE('1900-01-01')) AS SSTgl" => [
				'label' => 'Tgl SS',
				'width' => 175,
				'name'  => 'SSTgl'
			],
			"IFNULL(tmotor.FBJum, 0) AS FBJum"                => [
				'label' => 'Jumlah',
				'width' => 175,
				'name'  => 'FBJum'
			],
			"FBPSS"                                           => [
				'label' => 'PPS',
				'width' => 175,
				'name'  => 'FBPSS',
			],
			'FBPtgLain'                                       => [
				'label' => 'Ptg Lain2',
				'width' => 175,
				'name'  => 'FBPtgLain'
			],
			"FBTotal"                                         => [
				'label' => 'Total',
				'width' => 175,
				'name'  => 'FBTotal'
			],
			"ttfb.SupKode"                                    => [
				'label' => 'Kode Sup',
				'width' => 175,
				'name'  => 'SupKode'
			],
			"IFNULL(tdsupplier.SupNama, '--') AS SupNama"     => [
				'label' => 'Nama Supplier',
				'width' => 175,
				'name'  => 'SupNama'
			],
			"ttfb.FBMemo"                                     => [
				'label' => 'Keterangan',
				'width' => 175,
				'name'  => 'FBMemo'
			],
			"ttfb.NoGL"                                       => [
				'label' => 'No GL',
				'width' => 175,
				'name'  => 'NoGL'
			],
			"ttfb.UserID"                                     => [
				'label' => 'UserID',
				'width' => 175,
				'name'  => 'UserID'
			],
		];
	}
	public function getSuratSupp() {
		return $this->hasOne( Ttss::className(), [ 'SSNo' => 'SSNo' ] );
	}
	public function getSupplier() {
		return $this->hasOne( Tdsupplier::className(), [ 'SupKode' => 'SupKode' ] );
	}
	/**
	 * {@inheritdoc}
	 * @return TtfbQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TtfbQuery( get_called_class() );
	}
}

//"SELECT ttfb.FBNo, ttfb.FBTgl, ttfb.SSNo, ttfb.SupKode, ttfb.FBTermin, ttfb.FBTglTempo, ttfb.FBPSS, " + _
//         "ttfb.FBPtgLain, ttfb.FBTotal, ttfb.FBMemo, ttfb.FBLunas, ttfb.UserID, " + _
//         "IFNULL(tdsupplier.SupNama, '--') AS SupNama, IFNULL(ttss.SSTgl, DATE('1900-01-01')) AS SSTgl, " + _
//         "IFNULL(tmotor.FBJum, 0) AS FBJum, IFNULL(tmotor.FBSubTotal, 0) AS FBSubTotal, ttfb.NoGL " + _
//         "FROM ttfb " + _
//         "LEFT OUTER JOIN ttss ON ttfb.SSNo = ttss.SSNo " + _
//         "LEFT OUTER JOIN tdsupplier ON ttfb.SupKode = tdsupplier.SupKode " + _
//         "LEFT OUTER JOIN " + _
//         "(SELECT COUNT(MotorType) AS FBJum, SUM(FBHarga) AS FBSubTotal, FBNo FROM tmotor tmotor_1 GROUP BY FBNo) tmotor " + _
//         "ON ttfb.FBNo = tmotor.FBNo " + _
