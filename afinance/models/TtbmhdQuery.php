<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttbmhd]].
 *
 * @see Ttbmhd
 */
class TtbmhdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttbmhd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttbmhd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
