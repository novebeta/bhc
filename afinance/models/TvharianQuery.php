<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Tvharian]].
 *
 * @see Tvharian
 */
class TvharianQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tvharian[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tvharian|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
