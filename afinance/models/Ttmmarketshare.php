<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttmmarketshare".
 *
 * @property string $Tanggal
 * @property string $Provinsi
 * @property string $Kabupaten
 * @property string $Kecamatan
 * @property string $Honda
 * @property string $Semua
 */
class Ttmmarketshare extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttmmarketshare';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Tanggal', 'Provinsi', 'Kabupaten', 'Kecamatan'], 'required'],
            [['Tanggal'], 'safe'],
            [['Honda', 'Semua'], 'number'],
            [['Provinsi', 'Kabupaten', 'Kecamatan'], 'string', 'max' => 50],
            [['Tanggal', 'Provinsi', 'Kabupaten', 'Kecamatan'], 'unique', 'targetAttribute' => ['Tanggal', 'Provinsi', 'Kabupaten', 'Kecamatan']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Tanggal' => 'Tanggal',
            'Provinsi' => 'Provinsi',
            'Kabupaten' => 'Kabupaten',
            'Kecamatan' => 'Kecamatan',
            'Honda' => 'Honda',
            'Semua' => 'Semua',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtmmarketshareQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtmmarketshareQuery(get_called_class());
    }
}
