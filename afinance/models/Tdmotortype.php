<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "tdmotortype".
 *
 * @property string $MotorType
 * @property string $TypeStatus
 * @property string $MotorHrgBeli
 * @property string $MotorHrgJual
 * @property string $MotorNoMesin
 * @property string $MotorNoRangka
 * @property string $MotorNama
 * @property string $MotorKategori
 * @property string $MotorCC
 * @property string $TglUpdate
 * @property string $KodeAHM
 */
class Tdmotortype extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdmotortype';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['MotorType'], 'required'],
            [['MotorHrgBeli', 'MotorHrgJual', 'MotorCC'], 'number'],
            [['TglUpdate'], 'safe'],
            [['MotorType'], 'string', 'max' => 50],
            [['TypeStatus'], 'string', 'max' => 1],
            [['MotorNoMesin'], 'string', 'max' => 5],
            [['MotorNoRangka'], 'string', 'max' => 8],
            [['MotorNama'], 'string', 'max' => 100],
            [['MotorKategori'], 'string', 'max' => 15],
            [['KodeAHM'], 'string', 'max' => 3],
            [['MotorType'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'MotorType' => 'Motor Type',
            'TypeStatus' => 'Type Status',
            'MotorHrgBeli' => 'Motor Hrg Beli',
            'MotorHrgJual' => 'Motor Hrg Jual',
            'MotorNoMesin' => 'Motor No Mesin',
            'MotorNoRangka' => 'Motor No Rangka',
            'MotorNama' => 'Motor Nama',
            'MotorKategori' => 'Motor Kategori',
            'MotorCC' => 'Motor Cc',
            'TglUpdate' => 'Tgl Update',
            'KodeAHM' => 'Kode Ahm',
        ];
    }

	public static function colGrid()
	{
		return [
			'MotorType' => 'Type Motor',
			'MotorNama' => 'Nama Motor',
			'MotorNoMesin' => 'No Mesin',
			'MotorNoRangka' => 'No Rangka',
			'TypeStatus' => 'Status',
			'MotorKategori' => 'Kategori',
			'MotorCC' => 'Motor CC',
		];
	}

    /**
     * {@inheritdoc}
     * @return TdmotortypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdmotortypeQuery(get_called_class());
    }
}
