<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttkmitcoa]].
 *
 * @see Ttkmitcoa
 */
class TtkmitcoaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttkmitcoa[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttkmitcoa|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
