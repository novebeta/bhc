<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttdkprg]].
 *
 * @see Ttdkprg
 */
class TtdkprgQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttdkprg[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttdkprg|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
