<?php
namespace afinance\models;
/**
 * This is the model class for table "ttdk".
 *
 * @property string $DKNo
 * @property string $DKTgl
 * @property string $SalesKode
 * @property string $CusKode
 * @property string $LeaseKode
 * @property string $DKJenis
 * @property string $DKMemo
 * @property string $DKLunas
 * @property string $INNo
 * @property string $UserID
 * @property string $DKHPP
 * @property string $DKHarga
 * @property string $DKDPTotal
 * @property string $DKDPLLeasing
 * @property string $DKDPTerima
 * @property string $DKDPInden
 * @property string $DKNetto
 * @property string $ProgramNama
 * @property string $ProgramSubsidi
 * @property string $BBN
 * @property string $Jaket
 * @property string $PrgSubsSupplier
 * @property string $PrgSubsDealer
 * @property string $PrgSubsFincoy
 * @property string $PotonganHarga Subsidi Dealer 2
 * @property string $ReturHarga Komisi
 * @property string $PotonganKhusus
 * @property string $NamaHadiah
 * @property string $PotonganAHM
 * @property string $DKTenor
 * @property string $DKAngsuran
 * @property string $DKMerkSblmnya
 * @property string $DKJenisSblmnya
 * @property string $DKMotorUntuk
 * @property string $DKMotorOleh
 * @property string $DKMediator
 * @property string $DKTOP
 * @property string $DKPOLeasing
 * @property string $SPKNo
 * @property string $InsentifSales
 * @property string $TeamKode
 * @property string $DKTglTagih
 * @property string $DKPONo
 * @property string $DKPOTgl
 * @property string $BBNPlus
 * @property string $ROCount
 * @property string $DKScheme
 * @property string $DKSCP
 * @property string $DKPengajuanBBN
 * @property string $DKScheme2
 * @property string $JAPrgSubsSupplier
 * @property string $JAPrgSubsDealer
 * @property string $JAPrgSubsFincoy
 * @property string $JADKHarga
 * @property string $JADKDPTerima
 * @property string $JADKDPLLeasing
 * @property string $JABBN
 * @property string $JAReturHarga
 * @property string $JAPotonganHarga
 * @property string $JAInsentifSales
 * @property string $JAPotonganKhusus
 * @property string $DKSurveyor
 * @property string $NoBukuServis
 */
class Ttdk extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttdk';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'DKNo' ], 'required' ],
			[ [ 'DKTgl', 'DKTglTagih', 'DKPOTgl' ], 'safe' ],
			[
				[
					'DKHPP',
					'DKHarga',
					'DKDPTotal',
					'DKDPLLeasing',
					'DKDPTerima',
					'DKDPInden',
					'DKNetto',
					'ProgramSubsidi',
					'BBN',
					'Jaket',
					'PrgSubsSupplier',
					'PrgSubsDealer',
					'PrgSubsFincoy',
					'PotonganHarga',
					'ReturHarga',
					'PotonganKhusus',
					'PotonganAHM',
					'DKTenor',
					'DKAngsuran',
					'DKTOP',
					'InsentifSales',
					'BBNPlus',
					'ROCount',
					'DKScheme',
					'DKSCP',
					'DKScheme2',
					'JAPrgSubsSupplier',
					'JAPrgSubsDealer',
					'JAPrgSubsFincoy',
					'JADKHarga',
					'JADKDPTerima',
					'JADKDPLLeasing',
					'JABBN',
					'JAReturHarga',
					'JAPotonganHarga',
					'JAInsentifSales',
					'JAPotonganKhusus'
				],
				'number'
			],
			[ [ 'DKNo', 'SalesKode', 'CusKode', 'LeaseKode', 'INNo' ], 'string', 'max' => 10 ],
			[ [ 'DKJenis' ], 'string', 'max' => 6 ],
			[ [ 'DKMemo' ], 'string', 'max' => 100 ],
			[ [ 'DKLunas', 'DKPengajuanBBN' ], 'string', 'max' => 5 ],
			[ [ 'UserID', 'SPKNo', 'TeamKode' ], 'string', 'max' => 15 ],
			[ [ 'ProgramNama', 'DKMerkSblmnya', 'DKJenisSblmnya', 'DKMotorUntuk', 'DKMotorOleh', 'DKPOLeasing' ], 'string', 'max' => 25 ],
			[ [ 'NamaHadiah', 'DKMediator', 'DKSurveyor' ], 'string', 'max' => 50 ],
			[ [ 'DKPONo', 'NoBukuServis' ], 'string', 'max' => 20 ],
			[ [ 'DKNo' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'DKNo'              => 'Dk No',
			'DKTgl'             => 'Dk Tgl',
			'SalesKode'         => 'Sales Kode',
			'CusKode'           => 'Cus Kode',
			'LeaseKode'         => 'Lease Kode',
			'DKJenis'           => 'Dk Jenis',
			'DKMemo'            => 'Dk Memo',
			'DKLunas'           => 'Dk Lunas',
			'INNo'              => 'In No',
			'UserID'            => 'User ID',
			'DKHPP'             => 'Dkhpp',
			'DKHarga'           => 'Dk Harga',
			'DKDPTotal'         => 'Dkdp Total',
			'DKDPLLeasing'      => 'Dkdpl Leasing',
			'DKDPTerima'        => 'Dkdp Terima',
			'DKDPInden'         => 'Dkdp Inden',
			'DKNetto'           => 'Dk Netto',
			'ProgramNama'       => 'Program Nama',
			'ProgramSubsidi'    => 'Program Subsidi',
			'BBN'               => 'Bbn',
			'Jaket'             => 'Jaket',
			'PrgSubsSupplier'   => 'Prg Subs Supplier',
			'PrgSubsDealer'     => 'Prg Subs Dealer',
			'PrgSubsFincoy'     => 'Prg Subs Fincoy',
			'PotonganHarga'     => 'Subsidi Dealer 2',
			'ReturHarga'        => 'Komisi',
			'PotonganKhusus'    => 'Potongan Khusus',
			'NamaHadiah'        => 'Nama Hadiah',
			'PotonganAHM'       => 'Potongan Ahm',
			'DKTenor'           => 'Dk Tenor',
			'DKAngsuran'        => 'Dk Angsuran',
			'DKMerkSblmnya'     => 'Dk Merk Sblmnya',
			'DKJenisSblmnya'    => 'Dk Jenis Sblmnya',
			'DKMotorUntuk'      => 'Dk Motor Untuk',
			'DKMotorOleh'       => 'Dk Motor Oleh',
			'DKMediator'        => 'Dk Mediator',
			'DKTOP'             => 'Dktop',
			'DKPOLeasing'       => 'Dkpo Leasing',
			'SPKNo'             => 'Spk No',
			'InsentifSales'     => 'Insentif Sales',
			'TeamKode'          => 'Team Kode',
			'DKTglTagih'        => 'Dk Tgl Tagih',
			'DKPONo'            => 'Dkpo No',
			'DKPOTgl'           => 'Dkpo Tgl',
			'BBNPlus'           => 'Bbn Plus',
			'ROCount'           => 'Ro Count',
			'DKScheme'          => 'Dk Scheme',
			'DKSCP'             => 'Dkscp',
			'DKPengajuanBBN'    => 'Dk Pengajuan Bbn',
			'DKScheme2'         => 'Dk Scheme2',
			'JAPrgSubsSupplier' => 'Ja Prg Subs Supplier',
			'JAPrgSubsDealer'   => 'Ja Prg Subs Dealer',
			'JAPrgSubsFincoy'   => 'Ja Prg Subs Fincoy',
			'JADKHarga'         => 'Jadk Harga',
			'JADKDPTerima'      => 'Jadkdp Terima',
			'JADKDPLLeasing'    => 'Jadkdpl Leasing',
			'JABBN'             => 'Jabbn',
			'JAReturHarga'      => 'Ja Retur Harga',
			'JAPotonganHarga'   => 'Ja Potongan Harga',
			'JAInsentifSales'   => 'Ja Insentif Sales',
			'JAPotonganKhusus'  => 'Ja Potongan Khusus',
			'DKSurveyor'        => 'Dk Surveyor',
			'NoBukuServis'      => 'No Buku Servis',
		];
	}
	public static function colGrid() {
		return [
			'ttdk.DKNo'             => [
				'label' => 'DKNo',
				'width' => 105,
				'name'  => 'DKNo'
			],
			'tdmotortype.MotorType' => [
				'label' => 'MotorType',
				'width' => 105,
				'name'  => 'MotorType'
			],
			'ttdk.CusKode'          => [
				'label' => 'CusKode',
				'width' => 105,
				'name'  => 'CusKode'
			]
		];
	}
	public static function colGridDataKonsumenStatus() {
		return [
			'ttdk.DKNo'             => [
				'label' => 'DKNo',
				'width' => 105,
				'name'  => 'DKNo'
			],
			'tdmotortype.MotorType' => [
				'label' => 'MotorType',
				'width' => 105,
				'name'  => 'MotorType'
			],
			'ttdk.CusKode'          => [
				'label' => 'CusKode',
				'width' => 105,
				'name'  => 'CusKode'
			]
		];
	}
	public static function colGridStnkBpkb() {
		return [
			'ttdk.DKNo'             => [
				'label' => 'DKNo',
				'width' => 105,
				'name'  => 'DKNo'
			],
			'tdmotortype.MotorType' => [
				'label' => 'MotorType',
				'width' => 105,
				'name'  => 'MotorType'
			],
			'ttdk.CusKode'          => [
				'label' => 'CusKode',
				'width' => 105,
				'name'  => 'CusKode'
			]
		];
	}
	public static function colGridDataPortal() {
		return [
			'ttdk.DKNo'             => [
				'label' => 'DKNo',
				'width' => 105,
				'name'  => 'DKNo'
			],
			'tdmotortype.MotorType' => [
				'label' => 'MotorType',
				'width' => 105,
				'name'  => 'MotorType'
			],
			'ttdk.CusKode'          => [
				'label' => 'CusKode',
				'width' => 105,
				'name'  => 'CusKode'
			]
		];
	}
	public static function colGridPengajuanBerkas() {
		return [
			'ttdk.DKNo'             => [
				'label' => 'DKNo',
				'width' => 105,
				'name'  => 'DKNo'
			],
			'tdmotortype.MotorType' => [
				'label' => 'MotorType',
				'width' => 105,
				'name'  => 'MotorType'
			],
			'ttdk.CusKode'          => [
				'label' => 'CusKode',
				'width' => 105,
				'name'  => 'CusKode'
			]
		];
	}
	public static function colGridPengajuanBayarBBNBank() {
		return [
			'ttdk.DKNo'             => [
				'label' => 'DKNo',
				'width' => 105,
				'name'  => 'DKNo'
			],
			'tdmotortype.MotorType' => [
				'label' => 'MotorType',
				'width' => 105,
				'name'  => 'MotorType'
			],
			'ttdk.CusKode'          => [
				'label' => 'CusKode',
				'width' => 105,
				'name'  => 'CusKode'
			]
		];
	}
	public static function colGridPengajuanBayarBBNKas() {
		return [
			'ttdk.DKNo'             => [
				'label' => 'DKNo',
				'width' => 105,
				'name'  => 'DKNo'
			],
			'tdmotortype.MotorType' => [
				'label' => 'MotorType',
				'width' => 105,
				'name'  => 'MotorType'
			],
			'ttdk.CusKode'          => [
				'label' => 'CusKode',
				'width' => 105,
				'name'  => 'CusKode'
			]
		];
	}
	public static function colGridDataKonsumenKredit() {
		return [
			'ttdk.DKNo'             => [
				'label' => 'DKNo',
				'width' => 105,
				'name'  => 'DKNo'
			],
			'tdmotortype.MotorType' => [
				'label' => 'MotorType',
				'width' => 105,
				'name'  => 'MotorType'
			],
			'ttdk.CusKode'          => [
				'label' => 'CusKode',
				'width' => 105,
				'name'  => 'CusKode'
			]
		];
	}
	public static function colGridDataKonsumenTunai() {
		return [
			'ttdk.DKNo'             => [
				'label' => 'DKNo',
				'width' => 105,
				'name'  => 'DKNo'
			],
			'tdmotortype.MotorType' => [
				'label' => 'MotorType',
				'width' => 105,
				'name'  => 'MotorType'
			],
			'ttdk.CusKode'          => [
				'label' => 'CusKode',
				'width' => 105,
				'name'  => 'CusKode'
			]
		];
	}
	public function getMotor() {
		return $this->hasOne( Tmotor::className(), [ 'DKNo' => 'DKNo' ] );
	}
	public function getMotorTypes() {
		return $this->hasOne( Tdmotortype::className(), [ 'MotorType' => 'MotorType' ] )
		            ->via( 'motor' );
	}
	public function getCustomer() {
		return $this->hasOne( Tdcustomer::className(), [ 'CusKode' => 'CusKode' ] );
	}
	/**
	 * {@inheritdoc}
	 * @return TtdkQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TtdkQuery( get_called_class() );
	}
}
//
//"SELECT ttdk.DKNo, ttdk.DKTgl, IFNULL(tdcustomer.CusNama,'--') AS CusNama, " + _
//         "IFNULL(tmotor.MotorType,'--') AS MotorType, " + _
//         "IFNULL(tmotor.MotorWarna,'--') AS MotorWarna, " + _
//         "IFNULL(tmotor.MotorTahun,'--') AS MotorTahun, " + _
//         "IFNULL(tmotor.MotorNoMesin,'--') AS MotorNoMesin, " + _
//         "IFNULL(tmotor.MotorNoRangka,'--') AS MotorNoRangka, " + _
//         "IFNULL(tdmotortype.MotorNama,'--') AS MotorNama " + _
//         "FROM ttdk " + _
//         "INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo " + _
//         "INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType " + _
//         "INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode "
//SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, " + _
//         "tmotor.MotorNoMesin, tmotor.MotorNoRangka, tdmotortype.MotorNama " + _
//         "FROM ttdk " + _
//         "INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo " + _
//         "INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType " + _
//         "INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode