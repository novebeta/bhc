<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttkkit]].
 *
 * @see Ttkkit
 */
class TtkkitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttkkit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttkkit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
