<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Tdlokasi]].
 *
 * @see Tdlokasi
 */
class TdlokasiQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdlokasi[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdlokasi|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	public function combo() {
		return $this->select([ "CONCAT(LokasiKode,' - ',LokasiNama) as label", "LokasiKode as value" ] )
//		            ->groupBy( 'MotorWarna' )
		            ->orderBy( 'LokasiKode' )
		            ->asArray()
		            ->all();
	}
}
