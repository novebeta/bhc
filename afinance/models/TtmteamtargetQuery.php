<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttmteamtarget]].
 *
 * @see Ttmteamtarget
 */
class TtmteamtargetQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttmteamtarget[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttmteamtarget|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
