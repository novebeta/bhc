<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttgeneralledgerhd]].
 *
 * @see Ttgeneralledgerhd
 */
class TtgeneralledgerhdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttgeneralledgerhd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttgeneralledgerhd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
