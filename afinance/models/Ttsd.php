<?php
namespace afinance\models;
/**
 * This is the model class for table "ttsd".
 *
 * @property string $SDNo
 * @property string $SDTgl
 * @property string $LokasiKode
 * @property string $DealerKode
 * @property string $SDMemo
 * @property string $SDTotal
 * @property string $UserID
 * @property string $SDJam
 * @property string $NoGL
 */
class Ttsd extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttsd';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'SDNo' ], 'required' ],
			[ [ 'SDTgl', 'SDJam' ], 'safe' ],
			[ [ 'SDTotal' ], 'number' ],
			[ [ 'SDNo', 'DealerKode', 'NoGL' ], 'string', 'max' => 10 ],
			[ [ 'LokasiKode', 'UserID' ], 'string', 'max' => 15 ],
			[ [ 'SDMemo' ], 'string', 'max' => 100 ],
			[ [ 'SDNo' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'SDNo'       => 'Sd No',
			'SDTgl'      => 'Sd Tgl',
			'LokasiKode' => 'Lokasi Kode',
			'DealerKode' => 'Dealer Kode',
			'SDMemo'     => 'Sd Memo',
			'SDTotal'    => 'Sd Total',
			'UserID'     => 'User ID',
			'SDJam'      => 'Sd Jam',
			'NoGL'       => 'No Gl',
		];
	}
	public static function colGrid() {
		return [
			'SDNo'                => [
				'label' => 'No SD',
				'width' => 105,
				'name'  => 'SDNo'
			],
			'SDTgl'               => [
				'label' => 'Tgl SD',
				'width' => 105,
				'name'  => 'SDTgl'
			],
			'ttsd.LokasiKode'     => [
				'label' => 'Lokasi',
				'width' => 105,
				'name'  => 'LokasiKode'
			],
			'tdlokasi.LokasiNama' => [
				'label' => 'Asal',
				'width' => 105,
				'name'  => 'LokasiNama'
			],
			'DealerKode'          => [
				'label' => 'Dealer',
				'width' => 105,
				'name'  => 'DealerKode'
			],
			'tdsupplier.SupNama'  => [
				'label' => 'Supplier Nama',
				'width' => 105,
				'name'  => 'SupNama'
			],
			'SDTotal'             => [
				'label' => 'Total',
				'width' => 105,
				'name'  => 'SDTotal'
			],
			'SDMemo'              => [
				'label' => 'Keterangan',
				'width' => 105,
				'name'  => 'SDMemo'
			],
			'NoGL'                => [
				'label' => 'No GL',
				'width' => 105,
				'name'  => 'NoGL'
			],
			'UserID'              => [
				'label' => 'User ID',
				'width' => 105,
				'name'  => 'UserID'
			],
		];
	}
	public static function colGridMutasiEksternalDealer() {
		return [
			'ttsd.SDNo'           => [
				'label' => 'SDNo',
				'width' => 105,
				'name'  => 'SDNo'
			],
			'tdlokasi.LokasiNama' => [
				'label' => 'LokasiNama',
				'width' => 105,
				'name'  => 'LokasiNama'
			],
			'tddealer.DealerKode' => [
				'label' => 'DealerKode',
				'width' => 105,
				'name'  => 'DealerKode'
			]
		];
	}
	public static function colGridInvoiceEksternal() {
		return [
			'ttsd.SDNo'           => [
				'label' => 'SDNo',
				'width' => 105,
				'name'  => 'SDNo'
			],
			'tdlokasi.LokasiNama' => [
				'label' => 'LokasiNama',
				'width' => 105,
				'name'  => 'LokasiNama'
			],
			'tddealer.DealerKode' => [
				'label' => 'DealerKode',
				'width' => 105,
				'name'  => 'DealerKode'
			]
		];
	}
	public function getSupplier() {
		return $this->hasOne( Tdsupplier::className(), [ 'SupKode' => 'DealerKode' ] );
	}
	public function getLokasi() {
		return $this->hasOne( Tdlokasi::className(), [ 'LokasiKode' => 'LokasiKode' ] );
	}
	public function getDealer() {
		return $this->hasOne( Tddealer::className(), [ 'DealerKode' => 'DealerKode' ] );
	}
	/**
	 * {@inheritdoc}
	 * @return TtsdQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TtsdQuery( get_called_class() );
	}
}
//
//"SELECT ttsd.SDNo, ttsd.SDTgl, ttsd.LokasiKode, ttsd.DealerKode, ttsd.SDMemo, ttsd.SDTotal, ttsd.UserID, tdlokasi.LokasiNama, " + _
//         "tdsupplier.SupNama, ttsd.NoGL FROM  ttsd " + _
//         "INNER JOIN tdlokasi ON ttsd.LokasiKode = tdlokasi.LokasiKode " + _
//         "INNER JOIN tdsupplier ON ttsd.DealerKode = tdsupplier.SupKode " + _