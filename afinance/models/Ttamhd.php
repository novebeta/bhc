<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttamhd".
 *
 * @property string $AMNo
 * @property string $AMTgl
 * @property string $AMJam
 * @property string $AMMemo
 * @property string $AMTotal
 * @property string $NoGL
 * @property string $UserID
 * @property string $MotorTahun
 * @property string $FBNo
 */
class Ttamhd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttamhd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AMNo'], 'required'],
            [['AMTgl', 'AMJam'], 'safe'],
            [['AMTotal', 'MotorTahun'], 'number'],
            [['AMNo', 'NoGL'], 'string', 'max' => 10],
            [['AMMemo'], 'string', 'max' => 300],
            [['UserID'], 'string', 'max' => 15],
            [['FBNo'], 'string', 'max' => 18],
            [['AMNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AMNo' => 'Am No',
            'AMTgl' => 'Am Tgl',
            'AMJam' => 'Am Jam',
            'AMMemo' => 'Am Memo',
            'AMTotal' => 'Am Total',
            'NoGL' => 'No Gl',
            'UserID' => 'User ID',
            'MotorTahun' => 'Motor Tahun',
            'FBNo' => 'Fb No',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtamhdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtamhdQuery(get_called_class());
    }
}
