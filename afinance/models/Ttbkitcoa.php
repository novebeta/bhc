<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttbkitcoa".
 *
 * @property string $BKNo
 * @property int $BKAutoN
 * @property string $NoAccount
 * @property string $BKBayar
 * @property string $BKKeterangan
 */
class Ttbkitcoa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttbkitcoa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BKNo', 'BKAutoN', 'NoAccount'], 'required'],
            [['BKAutoN'], 'integer'],
            [['BKBayar'], 'number'],
            [['BKNo'], 'string', 'max' => 12],
            [['NoAccount'], 'string', 'max' => 10],
            [['BKKeterangan'], 'string', 'max' => 150],
            [['BKNo', 'BKAutoN', 'NoAccount'], 'unique', 'targetAttribute' => ['BKNo', 'BKAutoN', 'NoAccount']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'BKNo' => 'Bk No',
            'BKAutoN' => 'Bk Auto N',
            'NoAccount' => 'No Account',
            'BKBayar' => 'Bk Bayar',
            'BKKeterangan' => 'Bk Keterangan',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtbkitcoaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtbkitcoaQuery(get_called_class());
    }
}
