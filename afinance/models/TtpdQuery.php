<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttpd]].
 *
 * @see Ttpd
 */
class TtpdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttpd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttpd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
