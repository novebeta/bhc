<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "tdleasing".
 *
 * @property string $LeaseKode
 * @property string $LeaseNama
 * @property string $LeaseContact
 * @property string $LeaseAlamat
 * @property string $LeaseKota
 * @property string $LeaseTelepon
 * @property string $LeaseFax
 * @property string $LeaseEmail
 * @property string $LeaseKeterangan
 * @property string $LeaseStatus
 * @property string $LeaseNo
 */
class Tdleasing extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdleasing';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['LeaseKode'], 'required'],
            [['LeaseNo'], 'number'],
            [['LeaseKode'], 'string', 'max' => 10],
            [['LeaseNama', 'LeaseContact', 'LeaseKota', 'LeaseFax'], 'string', 'max' => 50],
            [['LeaseAlamat', 'LeaseTelepon'], 'string', 'max' => 75],
            [['LeaseEmail'], 'string', 'max' => 25],
            [['LeaseKeterangan'], 'string', 'max' => 100],
            [['LeaseStatus'], 'string', 'max' => 1],
            [['LeaseKode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
	        'LeaseKode' => 'Kode',
	        'LeaseNama' => 'Nama',
	        'LeaseContact' => 'Kontak',
	        'LeaseAlamat' => 'Alamat',
	        'LeaseKota' => 'Kota',
	        'LeaseTelepon' => 'Telepon',
	        'LeaseFax' => 'Fax',
	        'LeaseEmail' => 'Email',
	        'LeaseKeterangan' => 'Keterangan',
            'LeaseStatus' => 'Status',
            'LeaseNo' => 'Lease No',
        ];
    }

	public static function colGrid()
	{
		return [
			'LeaseNama' => 'Nama',
			'LeaseAlamat' => 'Alamat',
			'LeaseKota' => 'Kota',
			'LeaseContact' => 'Kontak',
			'LeaseTelepon' => 'Telepon',
			'LeaseEmail' => 'Email',
		];
	}

    /**
     * {@inheritdoc}
     * @return TdleasingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdleasingQuery(get_called_class());
    }
}
