<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttmo]].
 *
 * @see Ttmo
 */
class TtmoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttmo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttmo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
