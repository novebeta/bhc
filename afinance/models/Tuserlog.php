<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "tuserlog".
 *
 * @property string $LogTglJam
 * @property string $UserID
 * @property string $LogJenis
 * @property string $LogNama
 * @property string $LogTabel
 * @property string $NoTrans
 */
class Tuserlog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tuserlog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['LogTglJam'], 'safe'],
            [['UserID'], 'string', 'max' => 15],
            [['LogJenis'], 'string', 'max' => 6],
            [['LogNama'], 'string', 'max' => 300],
            [['LogTabel'], 'string', 'max' => 50],
            [['NoTrans'], 'string', 'max' => 18],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'LogTglJam' => 'Log Tgl Jam',
            'UserID' => 'User ID',
            'LogJenis' => 'Log Jenis',
            'LogNama' => 'Log Nama',
            'LogTabel' => 'Log Tabel',
            'NoTrans' => 'No Trans',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TuserlogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TuserlogQuery(get_called_class());
    }
}
