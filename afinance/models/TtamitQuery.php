<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttamit]].
 *
 * @see Ttamit
 */
class TtamitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttamit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttamit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
