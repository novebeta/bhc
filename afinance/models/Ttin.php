<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttin".
 *
 * @property string $INNo
 * @property string $INTgl
 * @property string $SalesKode
 * @property string $CusKode
 * @property string $LeaseKode
 * @property string $INJenis Tunai / Kredit
 * @property string $INMemo
 * @property string $INDP
 * @property string $UserID
 * @property string $INHarga
 * @property string $MotorType
 * @property string $MotorWarna
 * @property string $DKNo
 */
class Ttin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['INNo'], 'required'],
            [['INTgl'], 'safe'],
            [['INDP', 'INHarga'], 'number'],
            [['INNo', 'SalesKode', 'CusKode', 'LeaseKode', 'DKNo'], 'string', 'max' => 10],
            [['INJenis'], 'string', 'max' => 6],
            [['INMemo'], 'string', 'max' => 100],
            [['UserID'], 'string', 'max' => 15],
            [['MotorType'], 'string', 'max' => 50],
            [['MotorWarna'], 'string', 'max' => 35],
            [['INNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'INNo' => 'In No',
            'INTgl' => 'In Tgl',
            'SalesKode' => 'Sales Kode',
            'CusKode' => 'Cus Kode',
            'LeaseKode' => 'Lease Kode',
            'INJenis' => 'Tunai / Kredit',
            'INMemo' => 'In Memo',
            'INDP' => 'Indp',
            'UserID' => 'User ID',
            'INHarga' => 'In Harga',
            'MotorType' => 'Motor Type',
            'MotorWarna' => 'Motor Warna',
            'DKNo' => 'Dk No',
        ];
    }

	public static function colGrid()
	{
		return [
			'ttin.LeaseKode' => [
				'label' => 'LeaseKode',
				'width' => 105,
				'name'  => 'LeaseKode'
			],
			'ttin.SalesKode' => [
				'label' => 'SalesKode',
				'width' => 105,
				'name'  => 'SalesKode'
			],
			'ttin.CusKode' => [
				'label' => 'CusKode',
				'width' => 105,
				'name'  => 'CusKode'
			],
			'ttin.INNo' => [
				'label' => 'INNo',
				'width' => 105,
				'name'  => 'INNo'
			],
		];
	}

	public function getLeasing() {
		return $this->hasOne( Tdleasing::className(), [ 'LeaseKode' => 'LeaseKode' ] );
	}
	public function getSales() {
		return $this->hasOne( Tdsale::className(), [ 'SalesKode' => 'SalesKode' ] );
	}
	public function getCustomer() {
		return $this->hasOne( Tdcustomer::className(), [ 'CusKode' => 'CusKode' ] );
	}
	public function getDataKonsumen() {
		return $this->hasOne( Ttdk::className(), [ 'INNo' => 'INNo' ] );
	}
    /**
     * {@inheritdoc}
     * @return TtinQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtinQuery(get_called_class());
    }
}

//SELECT ttin.INNo, ttin.INTgl, ttin.SalesKode, ttin.CusKode, ttin.LeaseKode, ttin.INJenis, ttin.INMemo, " + _
//         "ttin.INDP, ttin.UserID, IFNULL(tdcustomer.CusNama, '--') AS CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, " + _
//         "tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, IFNULL(tdcustomer.CusKabupaten, '--') AS CusKabupaten, tdcustomer.CusTelepon,tdcustomer.CusTelepon2, " + _
//         "IFNULL(tdsales.SalesNama, '--') AS SalesNama, IFNULL(tdleasing.LeaseNama, '--') AS LeaseNama, ttin.INHarga, " + _
//         "ttin.MotorType, ttin.MotorWarna, ttin.DKNo FROM ttin " + _
//         "INNER JOIN tdleasing ON ttin.LeaseKode = tdleasing.LeaseKode  " + _
//         "INNER JOIN tdsales ON ttin.SalesKode = tdsales.SalesKode  " + _
//         "INNER JOIN tdcustomer ON ttin.CusKode = tdcustomer.CusKode  " + _
//         "LEFT OUTER JOIN ttdk ON ttin.INNo = ttdk.INNo
