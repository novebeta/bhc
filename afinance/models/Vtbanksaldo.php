<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "vtbanksaldo".
 *
 * @property string $BankNo
 * @property string $BankTgl
 * @property string $BankJam
 * @property string $BankMemo
 * @property string $BankDebet
 * @property string $BankKredit
 * @property string $BankJenis
 * @property string $BankSupLease
 * @property string $BankCekNo
 * @property string $BankCekTempo
 * @property string $BankPerson
 * @property string $BankAC
 * @property string $NoAccount
 * @property string $UserID
 * @property string $NoGL
 */
class Vtbanksaldo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vtbanksaldo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BankTgl', 'BankJam', 'BankCekTempo'], 'safe'],
            [['BankDebet', 'BankKredit'], 'number'],
            [['BankNo'], 'string', 'max' => 12],
            [['BankMemo'], 'string', 'max' => 100],
            [['BankJenis'], 'string', 'max' => 20],
            [['BankSupLease', 'NoAccount', 'NoGL'], 'string', 'max' => 10],
            [['BankCekNo', 'BankAC'], 'string', 'max' => 25],
            [['BankPerson'], 'string', 'max' => 50],
            [['UserID'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'BankNo' => 'Bank No',
            'BankTgl' => 'Bank Tgl',
            'BankJam' => 'Bank Jam',
            'BankMemo' => 'Bank Memo',
            'BankDebet' => 'Bank Debet',
            'BankKredit' => 'Bank Kredit',
            'BankJenis' => 'Bank Jenis',
            'BankSupLease' => 'Bank Sup Lease',
            'BankCekNo' => 'Bank Cek No',
            'BankCekTempo' => 'Bank Cek Tempo',
            'BankPerson' => 'Bank Person',
            'BankAC' => 'Bank Ac',
            'NoAccount' => 'No Account',
            'UserID' => 'User ID',
            'NoGL' => 'No Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VtbanksaldoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VtbanksaldoQuery(get_called_class());
    }
}
