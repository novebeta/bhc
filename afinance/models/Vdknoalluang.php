<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "vdknoalluang".
 *
 * @property string $NoTrans
 * @property string $DKno
 * @property string $Nominal
 * @property string $TglTrans
 * @property string $Jenis
 * @property string $NoGL
 * @property string $JamTrans
 */
class Vdknoalluang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vdknoalluang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nominal'], 'number'],
            [['TglTrans', 'JamTrans'], 'safe'],
            [['Jenis'], 'string'],
            [['NoTrans'], 'string', 'max' => 12],
            [['DKno'], 'string', 'max' => 18],
            [['NoGL'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'NoTrans' => 'No Trans',
            'DKno' => 'D Kno',
            'Nominal' => 'Nominal',
            'TglTrans' => 'Tgl Trans',
            'Jenis' => 'Jenis',
            'NoGL' => 'No Gl',
            'JamTrans' => 'Jam Trans',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VdknoalluangQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VdknoalluangQuery(get_called_class());
    }
}
