<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttabhd".
 *
 * @property string $ABNo
 * @property string $ABTgl
 * @property string $ABMemo
 * @property string $UserID
 */
class Ttabhd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttabhd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ABNo'], 'required'],
            [['ABTgl'], 'safe'],
            [['ABNo'], 'string', 'max' => 10],
            [['ABMemo'], 'string', 'max' => 100],
            [['UserID'], 'string', 'max' => 15],
            [['ABNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ABNo' => 'Ab No',
            'ABTgl' => 'Ab Tgl',
            'ABMemo' => 'Ab Memo',
            'UserID' => 'User ID',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtabhdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtabhdQuery(get_called_class());
    }
}
