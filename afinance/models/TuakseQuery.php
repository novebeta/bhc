<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Tuakse]].
 *
 * @see Tuakse
 */
class TuakseQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tuakse[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tuakse|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
