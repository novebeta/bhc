<?php

namespace afinance\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use afinance\models\Ttss;

/**
 * TtssQuery represents the model behind the search form of `app\models\Ttss`.
 */
class TtssQuery extends Ttss
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SSNo', 'SSTgl', 'FBNo', 'SupKode', 'LokasiKode', 'SSMemo', 'UserID', 'SSJam', 'SSNoTerima', 'DONo', 'NoGL'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ttss::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'SSTgl' => $this->SSTgl,
            'SSJam' => $this->SSJam,
        ]);

        $query->andFilterWhere(['like', 'SSNo', $this->SSNo])
            ->andFilterWhere(['like', 'FBNo', $this->FBNo])
            ->andFilterWhere(['like', 'SupKode', $this->SupKode])
            ->andFilterWhere(['like', 'LokasiKode', $this->LokasiKode])
            ->andFilterWhere(['like', 'SSMemo', $this->SSMemo])
            ->andFilterWhere(['like', 'UserID', $this->UserID])
            ->andFilterWhere(['like', 'SSNoTerima', $this->SSNoTerima])
            ->andFilterWhere(['like', 'DONo', $this->DONo])
            ->andFilterWhere(['like', 'NoGL', $this->NoGL]);

        return $dataProvider;
    }
}
