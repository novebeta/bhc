<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "tdprogram".
 *
 * @property string $ProgramNama
 * @property string $MotorType
 * @property string $PrgSubsSupplier
 * @property string $PrgSubsDealer
 * @property string $PrgSubsFincoy
 * @property string $PrgSubsTotal
 * @property string $PrgTglAwal
 * @property string $PrgTglAkhir
 */
class Tdprogram extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdprogram';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ProgramNama', 'MotorType'], 'required'],
            [['PrgSubsSupplier', 'PrgSubsDealer', 'PrgSubsFincoy', 'PrgSubsTotal'], 'number'],
            [['PrgTglAwal', 'PrgTglAkhir'], 'safe'],
            [['ProgramNama'], 'string', 'max' => 25],
            [['MotorType'], 'string', 'max' => 50],
            [['ProgramNama', 'MotorType'], 'unique', 'targetAttribute' => ['ProgramNama', 'MotorType']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
	        'ProgramNama' => 'Nama Program',
	        'MotorType' => 'Type Motor',
	        'PrgTglAwal' => 'Awal',
	        'PrgTglAkhir' => 'Akhir',
	        'PrgSubsSupplier' => 'Subsidi Supplier',
	        'PrgSubsDealer' => 'Subsidi Dealer',
	        'PrgSubsFincoy' => 'Subsidi Fincoy',
	        'PrgSubsTotal' => 'Subsidi Total'
        ];
    }

	public static function colGrid()
	{
		return [
			'ProgramNama' => 'Nama Program',
			'MotorType' => 'Type Motor',
			'PrgTglAwal' => 'Awal',
			'PrgTglAkhir' => 'Akhir',
			'PrgSubsSupplier' => 'Subsidi Supplier',
			'PrgSubsDealer' => 'Subsidi Dealer',
			'PrgSubsTotal' => 'Subsidi Total',
		];
	}

    /**
     * {@inheritdoc}
     * @return TdprogramQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdprogramQuery(get_called_class());
    }
}
