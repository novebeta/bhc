<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Tdsupplier]].
 *
 * @see Tdsupplier
 */
class TdsupplierQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdsupplier[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdsupplier|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	public function combo() {
		return $this->select( [ "CONCAT(SupKode,' - ',SupNama) as label", "SupKode as value" ] )
		            ->orderBy( 'SupKode' )
		            ->asArray()
		            ->all();
	}
}
