<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "vtkm".
 *
 * @property string $KMNo
 * @property string $KMLINK
 * @property string $KMJenis
 * @property string $Via
 * @property string $KMNominal
 * @property string $KMTgl
 * @property string $KMjam
 * @property string $NoGL
 */
class Vtkm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vtkm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KMNominal'], 'number'],
            [['KMTgl', 'KMjam'], 'safe'],
            [['KMNo', 'KMLINK'], 'string', 'max' => 12],
            [['KMJenis'], 'string', 'max' => 20],
            [['Via'], 'string', 'max' => 2],
            [['NoGL'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'KMNo' => 'Km No',
            'KMLINK' => 'Kmlink',
            'KMJenis' => 'Km Jenis',
            'Via' => 'Via',
            'KMNominal' => 'Km Nominal',
            'KMTgl' => 'Km Tgl',
            'KMjam' => 'K Mjam',
            'NoGL' => 'No Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VtkmQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VtkmQuery(get_called_class());
    }
}
