<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttabit".
 *
 * @property string $ABNo
 * @property string $DKNo
 */
class Ttabit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttabit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ABNo', 'DKNo'], 'required'],
            [['ABNo', 'DKNo'], 'string', 'max' => 10],
            [['ABNo', 'DKNo'], 'unique', 'targetAttribute' => ['ABNo', 'DKNo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ABNo' => 'Ab No',
            'DKNo' => 'Dk No',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtabitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtabitQuery(get_called_class());
    }
}
