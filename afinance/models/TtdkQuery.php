<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttdk]].
 *
 * @see Ttdk
 */
class TtdkQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttdk[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttdk|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
