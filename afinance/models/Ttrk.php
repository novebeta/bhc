<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttrk".
 *
 * @property string $RKNo
 * @property string $RKTgl
 * @property string $LokasiKode
 * @property string $RKMemo
 * @property string $UserID
 * @property string $RKJam
 * @property string $DKNo
 * @property string $SKNo
 * @property string $MotorNoMesin
 * @property string $NoGL
 */
class Ttrk extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttrk';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['RKNo'], 'required'],
            [['RKTgl', 'RKJam'], 'safe'],
            [['RKNo', 'DKNo', 'SKNo', 'NoGL'], 'string', 'max' => 10],
            [['LokasiKode', 'UserID'], 'string', 'max' => 15],
            [['RKMemo'], 'string', 'max' => 100],
            [['MotorNoMesin'], 'string', 'max' => 25],
            [['RKNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'RKNo' => 'Rk No',
            'RKTgl' => 'Rk Tgl',
            'LokasiKode' => 'Lokasi Kode',
            'RKMemo' => 'Rk Memo',
            'UserID' => 'User ID',
            'RKJam' => 'Rk Jam',
            'DKNo' => 'Dk No',
            'SKNo' => 'Sk No',
            'MotorNoMesin' => 'Motor No Mesin',
            'NoGL' => 'No Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtrkQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtrkQuery(get_called_class());
    }
}
