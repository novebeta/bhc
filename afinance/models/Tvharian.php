<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "tvharian".
 *
 * @property string $SKNo
 * @property string $SKTgl
 * @property string $SKJam
 * @property string $LokasiKode
 * @property string $NoGL
 * @property string $TglGL
 * @property string $DKNo
 * @property string $DKJenis
 * @property string $LeaseKode
 * @property string $DKHarga 3010100 Harga Jual
 * @property string $DKHPP 5000000 HPP
 * @property string $BBN 6010110 BBN
 * @property string $ReturHarga 6010200 Retur Harga / Komisi
 * @property string $Jaket 6010300 Jaket
 * @property string $PotonganHarga 3020300 Subisidi Dealer 2
 * @property string $PrgSubsDealer 3020200 Net Subsidi Dealer 1
 * @property string $PrgSubsSupplier 1130408 Subsidi Supplier AHM SD1
 * @property string $PrgSubsFincoy 1130300 Piutang Leasing
 * @property string $ProgramSubsidi SubsDealer+SubsSupplier+SubsFincoy
 * @property string $DKDPLLeasing 2140500 + 1130100 Piutang Kons UM 
 * @property string $DKDPInden 2140500 + 1130100 UM Inden 
 * @property string $DKDPTerima 2140500 + 1130100 UM Diterima 
 * @property string $DKDPTotal DPLLeasing+DPInden+DPTerima
 * @property string $DKNetto DKHarga-ProgramSubsidi-DPTotal
 * @property string $CusKode
 * @property string $CusNama
 * @property string $KKNo
 * @property string $KKTgl
 * @property string $KKJam
 * @property int $MotorAutoN
 * @property string $MotorNoMesin
 * @property string $MotorNoRangka
 * @property string $MotorType
 * @property string $DKNettoHTK
 * @property string $DKNettoPK
 * @property string $DKDP2140500HTK
 * @property string $DKDPLLeasing2140500
 * @property string $DKDPInden2140500
 * @property string $DKDPTerima2140500
 * @property string $DKDP1130100PK
 * @property string $DKDPLLeasing1130100
 * @property string $DKDPInden1130100
 * @property string $DKDPTerima1130100
 * @property string $DKDPPKTerbayarTotal
 * @property string $DKDPPKTerbayarLeasing
 * @property string $DKDPPKTerbayarInden
 * @property string $DKDPPKTerbayarTerima
 * @property string $DKNettoKMPKons
 * @property string $DKNettoKMPLease
 * @property string $InsentifSales
 * @property string $Scheme
 */
class Tvharian extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tvharian';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SKNo'], 'required'],
            [['SKTgl', 'SKJam', 'TglGL', 'KKTgl', 'KKJam'], 'safe'],
            [['DKHarga', 'DKHPP', 'BBN', 'ReturHarga', 'Jaket', 'PotonganHarga', 'PrgSubsDealer', 'PrgSubsSupplier', 'PrgSubsFincoy', 'ProgramSubsidi', 'DKDPLLeasing', 'DKDPInden', 'DKDPTerima', 'DKDPTotal', 'DKNetto', 'DKNettoHTK', 'DKNettoPK', 'DKDP2140500HTK', 'DKDPLLeasing2140500', 'DKDPInden2140500', 'DKDPTerima2140500', 'DKDP1130100PK', 'DKDPLLeasing1130100', 'DKDPInden1130100', 'DKDPTerima1130100', 'DKDPPKTerbayarTotal', 'DKDPPKTerbayarLeasing', 'DKDPPKTerbayarInden', 'DKDPPKTerbayarTerima', 'DKNettoKMPKons', 'DKNettoKMPLease', 'InsentifSales', 'Scheme'], 'number'],
            [['MotorAutoN'], 'integer'],
            [['SKNo', 'NoGL', 'DKNo', 'LeaseKode', 'CusKode'], 'string', 'max' => 10],
            [['LokasiKode'], 'string', 'max' => 15],
            [['DKJenis'], 'string', 'max' => 6],
            [['CusNama'], 'string', 'max' => 75],
            [['KKNo'], 'string', 'max' => 12],
            [['MotorNoMesin', 'MotorNoRangka'], 'string', 'max' => 25],
            [['MotorType'], 'string', 'max' => 50],
            [['SKNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SKNo' => 'Sk No',
            'SKTgl' => 'Sk Tgl',
            'SKJam' => 'Sk Jam',
            'LokasiKode' => 'Lokasi Kode',
            'NoGL' => 'No Gl',
            'TglGL' => 'Tgl Gl',
            'DKNo' => 'Dk No',
            'DKJenis' => 'Dk Jenis',
            'LeaseKode' => 'Lease Kode',
            'DKHarga' => '3010100 Harga Jual',
            'DKHPP' => '5000000 HPP',
            'BBN' => '6010110 BBN',
            'ReturHarga' => '6010200 Retur Harga / Komisi',
            'Jaket' => '6010300 Jaket',
            'PotonganHarga' => '3020300 Subisidi Dealer 2',
            'PrgSubsDealer' => '3020200 Net Subsidi Dealer 1',
            'PrgSubsSupplier' => '1130408 Subsidi Supplier AHM SD1',
            'PrgSubsFincoy' => '1130300 Piutang Leasing',
            'ProgramSubsidi' => 'SubsDealer+SubsSupplier+SubsFincoy',
            'DKDPLLeasing' => '2140500 + 1130100 Piutang Kons UM ',
            'DKDPInden' => '2140500 + 1130100 UM Inden ',
            'DKDPTerima' => '2140500 + 1130100 UM Diterima ',
            'DKDPTotal' => 'DPLLeasing+DPInden+DPTerima',
            'DKNetto' => 'DKHarga-ProgramSubsidi-DPTotal',
            'CusKode' => 'Cus Kode',
            'CusNama' => 'Cus Nama',
            'KKNo' => 'Kk No',
            'KKTgl' => 'Kk Tgl',
            'KKJam' => 'Kk Jam',
            'MotorAutoN' => 'Motor Auto N',
            'MotorNoMesin' => 'Motor No Mesin',
            'MotorNoRangka' => 'Motor No Rangka',
            'MotorType' => 'Motor Type',
            'DKNettoHTK' => 'Dk Netto Htk',
            'DKNettoPK' => 'Dk Netto Pk',
            'DKDP2140500HTK' => 'Dkdp2140500 Htk',
            'DKDPLLeasing2140500' => 'Dkdpl Leasing2140500',
            'DKDPInden2140500' => 'Dkdp Inden2140500',
            'DKDPTerima2140500' => 'Dkdp Terima2140500',
            'DKDP1130100PK' => 'Dkdp1130100 Pk',
            'DKDPLLeasing1130100' => 'Dkdpl Leasing1130100',
            'DKDPInden1130100' => 'Dkdp Inden1130100',
            'DKDPTerima1130100' => 'Dkdp Terima1130100',
            'DKDPPKTerbayarTotal' => 'Dkdppk Terbayar Total',
            'DKDPPKTerbayarLeasing' => 'Dkdppk Terbayar Leasing',
            'DKDPPKTerbayarInden' => 'Dkdppk Terbayar Inden',
            'DKDPPKTerbayarTerima' => 'Dkdppk Terbayar Terima',
            'DKNettoKMPKons' => 'Dk Netto Kmp Kons',
            'DKNettoKMPLease' => 'Dk Netto Kmp Lease',
            'InsentifSales' => 'Insentif Sales',
            'Scheme' => 'Scheme',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TvharianQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TvharianQuery(get_called_class());
    }
}
