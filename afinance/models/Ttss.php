<?php
namespace afinance\models;
/**
 * This is the model class for table "ttss".
 *
 * @property string $SSNo
 * @property string $SSTgl
 * @property string $FBNo
 * @property string $SupKode
 * @property string $LokasiKode
 * @property string $SSMemo
 * @property string $UserID
 * @property string $SSJam
 * @property string $SSNoTerima
 * @property string $DONo
 * @property string $NoGL
 */
class Ttss extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttss';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'SSNo' ], 'required' ],
			[ [ 'SSTgl', 'SSJam' ], 'safe' ],
			[ [ 'SSNo', 'FBNo', 'DONo' ], 'string', 'max' => 18 ],
			[ [ 'SupKode', 'SSNoTerima', 'NoGL' ], 'string', 'max' => 10 ],
			[ [ 'LokasiKode', 'UserID' ], 'string', 'max' => 15 ],
			[ [ 'SSMemo' ], 'string', 'max' => 100 ],
			[ [ 'SSNo' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'SSNo'       => 'No SS',
			'SSTgl'      => 'Tgl SS',
			'FBNo'       => 'Fb No',
			'SupKode'    => 'Kode Sup',
			'LokasiKode' => 'Lokasi Kode',
			'SSMemo'     => 'Ss Memo',
			'UserID'     => 'User ID',
			'SSJam'      => 'Ss Jam',
			'SSNoTerima' => 'Ss No Terima',
			'DONo'       => 'Do No',
			'NoGL'       => 'No Gl',
		];
	}
	public static function colGrid() {
		return [
			'ttss.SSNo'                                       => [
				'label' => 'No SS',
				'width' => 105,
				'name'  => 'SSNo'
			],
			'SSTgl'                                           => [
				'label' => 'Tgl SS',
				'width' => 175,
				'name'  => 'SSTgl'
			],
			'DONo'                                            => [
				'label' => 'No DO',
				'width' => 175,
				'name'  => 'DONo'
			],
			'ttss.FBNo'                                       => [
				'label' => 'No Faktur',
				'width' => 175,
				'name'  => 'FBNo'
			],
			"IFNULL(ttfb.FBTgl, DATE('1900-01-01')) AS FBTgl" => [
				'label' => 'Tgl Faktur',
				'width' => 175,
				'name'  => 'FBTgl'
			],
			"IFNULL(tmotor.SSJum, 0) AS SSJum"                => [
				'label' => 'Jumlah',
				'width' => 175,
				'name'  => 'SSJum'
			],
			"tdlokasi.LokasiKode"                             => [
				'label' => 'Kode Lokasi',
				'width' => 175,
				'name'  => 'LokasiKode',
			],
			"IFNULL(tdlokasi.LokasiNama, '--') AS LokasiNama" => [
				'label' => 'Nama Lokasi',
				'width' => 175,
				'name'  => 'LokasiNama',
			],
			'tdsupplier.SupKode'                              => [
				'label' => 'Kode Sup',
				'width' => 175,
				'name'  => 'SupKode'
			],
			"IFNULL(tdsupplier.SupNama, '--') AS SupNama"     => [
				'label' => 'Nama Supplier',
				'width' => 175,
				'name'  => 'SupNama'
			],
			"SSMemo"                                          => [
				'label' => 'Keterangan',
				'width' => 175,
				'name'  => 'SSMemo'
			],
			"ttss.UserID"                                     => [
				'label' => 'UserID',
				'width' => 175,
				'name'  => 'ttss.UserID'
			],
		];
	}
	public function getFBNos() {
		return $this->hasMany( Ttfb::className(), [ 'FBNo' => 'FBNo' ] );
	}
	public function getSupplier() {
		return $this->hasOne( Tdsupplier::className(), [ 'SupKode' => 'SupKode' ] );
	}
	public function getLokasi() {
		return $this->hasOne( Tdlokasi::className(), [ 'LokasiKode' => 'LokasiKode' ] );
	}
	public function getMotor() {
		return $this->hasOne( Tmotor::className(), [ 'SSNo' => 'SSNo' ] );
	}
	/**
	 * {@inheritdoc}
	 * @return TtssQuery the active query used by this AR class.
	 */
//	public static function find() {
//		return new TtssQuery( get_called_class() );
//	}
}
//
//"SELECT ttss.SSNo, ttss.SSTgl,ttss.DONo,  ttss.FBNo, ttss.SupKode, " + _
//         "ttss.LokasiKode, ttss.SSMemo, ttss.UserID, " + _
//         "IFNULL(tdsupplier.SupNama, '--') AS SupNama," + _
//         "IFNULL(tdlokasi.LokasiNama, '--') AS LokasiNama, " + _
//         "IFNULL(ttfb.FBTgl, DATE('1900-01-01')) AS FBTgl, " + _
//         "IFNULL(tmotor.SSJum, 0) AS SSJum " + _
//         "FROM ttss " + _
//         "LEFT OUTER JOIN ttfb  ON ttss.FBNo = ttfb.FBNo " + _
//         "LEFT OUTER JOIN tdsupplier ON ttss.SupKode = tdsupplier.SupKode " + _
//         "LEFT OUTER JOIN tdlokasi ON ttss.LokasiKode = tdlokasi.LokasiKode " + _
//         "LEFT OUTER JOIN (SELECT COUNT(tmotor.MotorType) AS SSJum, tmotor.SSNo FROM tmotor GROUP BY SSNo) tmotor ON ttss.SSNo = tmotor.SSNo "