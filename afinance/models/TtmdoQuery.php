<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttmdo]].
 *
 * @see Ttmdo
 */
class TtmdoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttmdo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttmdo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
