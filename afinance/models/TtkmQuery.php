<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttkm]].
 *
 * @see Ttkm
 */
class TtkmQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttkm[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttkm|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
