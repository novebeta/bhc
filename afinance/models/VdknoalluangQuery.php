<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Vdknoalluang]].
 *
 * @see Vdknoalluang
 */
class VdknoalluangQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Vdknoalluang[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Vdknoalluang|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
