<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "tsaldobank".
 *
 * @property string $SaldoBankTgl
 * @property string $NoAccount
 * @property string $SaldoBankAwal
 * @property string $SaldoBankMasuk
 * @property string $SaldoBankKeluar
 * @property string $SaldoBankSaldo
 */
class Tsaldobank extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tsaldobank';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SaldoBankTgl', 'NoAccount'], 'required'],
            [['SaldoBankTgl'], 'safe'],
            [['SaldoBankAwal', 'SaldoBankMasuk', 'SaldoBankKeluar', 'SaldoBankSaldo'], 'number'],
            [['NoAccount'], 'string', 'max' => 10],
            [['SaldoBankTgl', 'NoAccount'], 'unique', 'targetAttribute' => ['SaldoBankTgl', 'NoAccount']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SaldoBankTgl' => 'Saldo Bank Tgl',
            'NoAccount' => 'No Account',
            'SaldoBankAwal' => 'Saldo Bank Awal',
            'SaldoBankMasuk' => 'Saldo Bank Masuk',
            'SaldoBankKeluar' => 'Saldo Bank Keluar',
            'SaldoBankSaldo' => 'Saldo Bank Saldo',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TsaldobankQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TsaldobankQuery(get_called_class());
    }
}
