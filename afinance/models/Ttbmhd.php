<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttbmhd".
 *
 * @property string $BMNo
 * @property string $BMTgl
 * @property string $BMMemo
 * @property string $BMNominal
 * @property string $BMJenis Leasing, Tunai, Umum
 * @property string $LeaseKode
 * @property string $BMCekNo
 * @property string $BMCekTempo
 * @property string $BMPerson
 * @property string $BMAC
 * @property string $NoAccount
 * @property string $UserID
 * @property string $BMJam
 * @property string $NoGL
 */
class Ttbmhd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttbmhd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BMNo'], 'required'],
            [['BMTgl', 'BMCekTempo', 'BMJam'], 'safe'],
            [['BMNominal'], 'number'],
            [['BMNo'], 'string', 'max' => 12],
            [['BMMemo'], 'string', 'max' => 100],
            [['BMJenis'], 'string', 'max' => 20],
            [['LeaseKode', 'NoAccount', 'NoGL'], 'string', 'max' => 10],
            [['BMCekNo', 'BMAC'], 'string', 'max' => 25],
            [['BMPerson'], 'string', 'max' => 50],
            [['UserID'], 'string', 'max' => 15],
            [['BMNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'BMNo' => 'Bm No',
            'BMTgl' => 'Bm Tgl',
            'BMMemo' => 'Bm Memo',
            'BMNominal' => 'Bm Nominal',
            'BMJenis' => 'Leasing, Tunai, Umum',
            'LeaseKode' => 'Lease Kode',
            'BMCekNo' => 'Bm Cek No',
            'BMCekTempo' => 'Bm Cek Tempo',
            'BMPerson' => 'Bm Person',
            'BMAC' => 'Bmac',
            'NoAccount' => 'No Account',
            'UserID' => 'User ID',
            'BMJam' => 'Bm Jam',
            'NoGL' => 'No Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtbmhdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtbmhdQuery(get_called_class());
    }
}
