<?php
namespace afinance\models;
use yii\helpers\ArrayHelper;
/**
 * This is the ActiveQuery class for [[Tdteam]].
 *
 * @see Tdteam
 */
class TdteamQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Tdteam[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Tdteam|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function combo() {
		return $this->select( [ "TeamKode as label", "TeamKode as value" ] )
		            ->orderBy( 'TeamKode' )
		            ->asArray()
		            ->all();
	}
	public function select2() {
		return ArrayHelper::map( self::combo(), 'value', 'label' );
	}
}
