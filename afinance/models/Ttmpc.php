<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttmpc".
 *
 * @property string $PCNo
 * @property string $PCTgl
 * @property string $MotorType
 * @property string $PCQty
 */
class Ttmpc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttmpc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PCNo', 'PCTgl', 'MotorType'], 'required'],
            [['PCTgl'], 'safe'],
            [['PCQty'], 'number'],
            [['PCNo'], 'string', 'max' => 10],
            [['MotorType'], 'string', 'max' => 50],
            [['PCNo', 'PCTgl', 'MotorType'], 'unique', 'targetAttribute' => ['PCNo', 'PCTgl', 'MotorType']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PCNo' => 'Pc No',
            'PCTgl' => 'Pc Tgl',
            'MotorType' => 'Motor Type',
            'PCQty' => 'Pc Qty',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtmpcQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtmpcQuery(get_called_class());
    }
}
