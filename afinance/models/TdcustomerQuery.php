<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Tdcustomer]].
 *
 * @see Tdcustomer
 */
class TdcustomerQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdcustomer[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdcustomer|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
	public function combo() {
		return $this->select( [ "CONCAT(CusKode,' - ',CusNama) as label", "CusKode as value" ] )
		            ->orderBy( 'CusKode' )
		            ->asArray()
		            ->all();
	}
}
