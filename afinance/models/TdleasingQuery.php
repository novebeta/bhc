<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Tdleasing]].
 *
 * @see Tdleasing
 */
class TdleasingQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdleasing[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdleasing|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	public function combo() {
		return $this->select( [ "CONCAT(LeaseKode,' - ',LeaseNama) as label", "LeaseKode as value" ] )
		            ->orderBy( 'LeaseKode' )
		            ->asArray()
		            ->all();
	}
}
