<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "vdkpmbayar".
 *
 * @property string $DKNo
 * @property string $Jenis
 * @property string $Paid
 */
class Vdkpmbayar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vdkpmbayar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Paid'], 'number'],
            [['DKNo'], 'string', 'max' => 18],
            [['Jenis'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'DKNo' => 'Dk No',
            'Jenis' => 'Jenis',
            'Paid' => 'Paid',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VdkpmbayarQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VdkpmbayarQuery(get_called_class());
    }
}
