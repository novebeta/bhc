<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttabhd]].
 *
 * @see Ttabhd
 */
class TtabhdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttabhd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttabhd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
