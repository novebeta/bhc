<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttmo".
 *
 * @property string $MotorNoMesin
 * @property string $MOTgl
 * @property string $MONominal
 */
class Ttmo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttmo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['MotorNoMesin', 'MOTgl'], 'required'],
            [['MOTgl'], 'safe'],
            [['MONominal'], 'number'],
            [['MotorNoMesin'], 'string', 'max' => 25],
            [['MotorNoMesin', 'MOTgl'], 'unique', 'targetAttribute' => ['MotorNoMesin', 'MOTgl']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'MotorNoMesin' => 'Motor No Mesin',
            'MOTgl' => 'Mo Tgl',
            'MONominal' => 'Mo Nominal',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtmoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtmoQuery(get_called_class());
    }
}
