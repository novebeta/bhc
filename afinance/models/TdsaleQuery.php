<?php
namespace afinance\models;
use yii\helpers\ArrayHelper;
/**
 * This is the ActiveQuery class for [[Tdsale]].
 *
 * @see Tdsales
 */
class TdsaleQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Tdsale[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Tdsale|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function combo() {
		return $this->select( [ "CONCAT(SalesKode,' - ',SalesNama) as label", "SalesKode as value" ] )
		            ->orderBy( 'SalesKode' )
		            ->asArray()
		            ->all();
	}
	public function petugas() {
		return ArrayHelper::map( $this->select( [ "CONCAT(SalesKode,' - ',SalesNama) as label", "SalesKode as value" ] )
		                              ->where( [ 'SalesStatus' => 'A' ] )
		                              ->orderBy( 'SalesStatus, SalesKode' )
		                              ->asArray()
		                              ->all(), 'value', 'label' );
	}
}
