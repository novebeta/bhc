<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Tsaldounit]].
 *
 * @see Tsaldounit
 */
class TsaldounitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tsaldounit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tsaldounit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
