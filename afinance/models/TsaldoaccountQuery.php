<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Tsaldoaccount]].
 *
 * @see Tsaldoaccount
 */
class TsaldoaccountQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tsaldoaccount[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tsaldoaccount|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
