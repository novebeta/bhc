<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttabit]].
 *
 * @see Ttabit
 */
class TtabitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttabit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttabit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
