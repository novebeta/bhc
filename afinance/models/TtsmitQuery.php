<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttsmit]].
 *
 * @see Ttsmit
 */
class TtsmitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttsmit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttsmit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
