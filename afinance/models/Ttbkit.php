<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttbkit".
 *
 * @property string $BKNo
 * @property string $FBNo
 * @property string $BKBayar
 */
class Ttbkit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttbkit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BKNo', 'FBNo'], 'required'],
            [['BKBayar'], 'number'],
            [['BKNo'], 'string', 'max' => 12],
            [['FBNo'], 'string', 'max' => 18],
            [['BKNo', 'FBNo'], 'unique', 'targetAttribute' => ['BKNo', 'FBNo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'BKNo' => 'Bk No',
            'FBNo' => 'Fb No',
            'BKBayar' => 'Bk Bayar',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtbkitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtbkitQuery(get_called_class());
    }
}
