<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "vdknoalluangnogl".
 *
 * @property string $NoTrans
 * @property string $DKNo
 * @property string $Nominal
 * @property string $TglTrans
 * @property string $Jenis
 * @property string $NoGL
 * @property string $JamTrans
 */
class Vdknoalluangnogl extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vdknoalluangnogl';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nominal'], 'number'],
            [['TglTrans', 'JamTrans'], 'safe'],
            [['Jenis'], 'string'],
            [['NoTrans'], 'string', 'max' => 12],
            [['DKNo'], 'string', 'max' => 18],
            [['NoGL'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'NoTrans' => 'No Trans',
            'DKNo' => 'Dk No',
            'Nominal' => 'Nominal',
            'TglTrans' => 'Tgl Trans',
            'Jenis' => 'Jenis',
            'NoGL' => 'No Gl',
            'JamTrans' => 'Jam Trans',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VdknoalluangnoglQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VdknoalluangnoglQuery(get_called_class());
    }
}
