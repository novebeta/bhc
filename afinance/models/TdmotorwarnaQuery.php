<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Tdmotorwarna]].
 *
 * @see Tdmotorwarna
 */
class TdmotorwarnaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdmotorwarna[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdmotorwarna|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
	public function combo() {
		return $this->select('MotorWarna as label,MotorWarna as value' )
			->groupBy( 'MotorWarna' )
		            ->orderBy( 'MotorWarna' )
		            ->asArray()
		            ->all();
	}
}
