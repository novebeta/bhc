<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Vdknoinuang]].
 *
 * @see Vdknoinuang
 */
class VdknoinuangQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Vdknoinuang[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Vdknoinuang|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
