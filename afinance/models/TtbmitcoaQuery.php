<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttbmitcoa]].
 *
 * @see Ttbmitcoa
 */
class TtbmitcoaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttbmitcoa[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttbmitcoa|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
