<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttkkit".
 *
 * @property string $KKNo
 * @property string $FBNo
 * @property string $KKBayar
 */
class Ttkkit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttkkit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KKNo', 'FBNo'], 'required'],
            [['KKBayar'], 'number'],
            [['KKNo'], 'string', 'max' => 12],
            [['FBNo'], 'string', 'max' => 18],
            [['KKNo', 'FBNo'], 'unique', 'targetAttribute' => ['KKNo', 'FBNo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'KKNo' => 'Kk No',
            'FBNo' => 'Fb No',
            'KKBayar' => 'Kk Bayar',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtkkitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtkkitQuery(get_called_class());
    }
}
