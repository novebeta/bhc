<?php
namespace afinance\models;
/**
 * This is the ActiveQuery class for [[Tdarea]].
 *
 * @see Tdarea
 */
class TdareaQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Tdarea[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Tdarea|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}

	public function combo() {
		return $this->select('Kabupaten as label,Kabupaten as value' )
		            ->groupBy( 'Kabupaten' )
		            ->orderBy( 'Kabupaten' )
		            ->asArray()
		            ->all();
	}
}
