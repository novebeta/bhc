<?php
namespace afinance\models;
/**
 * This is the ActiveQuery class for [[Tdbbn]].
 *
 * @see Tdbbn
 */
class TdbbnQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Tdbbn[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Tdbbn|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function combo() {
		return $this->select('Kabupaten as label,Kabupaten as value' )
		            ->groupBy( 'Kabupaten' )
		            ->orderBy( 'Kabupaten' )
		            ->asArray()
		            ->all();
	}
}
