<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Ttsk]].
 *
 * @see Ttsk
 */
class TtskQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttsk[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttsk|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
