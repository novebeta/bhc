<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Tmotor]].
 *
 * @see Tmotor
 */
class TmotorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tmotor[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tmotor|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
