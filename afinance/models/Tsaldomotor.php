<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "tsaldomotor".
 *
 * @property string $NoTrans
 * @property string $TglTrans
 * @property int $MotorAutoN
 * @property string $MotorNoMesin
 * @property string $MotorWarna
 * @property string $MotorTahun
 * @property string $MotorNoRangka
 * @property string $MotorType
 * @property string $MotorHarga
 */
class Tsaldomotor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tsaldomotor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NoTrans', 'TglTrans', 'MotorAutoN', 'MotorNoMesin'], 'required'],
            [['TglTrans'], 'safe'],
            [['MotorAutoN'], 'integer'],
            [['MotorTahun', 'MotorHarga'], 'number'],
            [['NoTrans'], 'string', 'max' => 10],
            [['MotorNoMesin', 'MotorNoRangka', 'MotorType'], 'string', 'max' => 25],
            [['MotorWarna'], 'string', 'max' => 35],
            [['NoTrans', 'TglTrans', 'MotorAutoN', 'MotorNoMesin'], 'unique', 'targetAttribute' => ['NoTrans', 'TglTrans', 'MotorAutoN', 'MotorNoMesin']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'NoTrans' => 'No Trans',
            'TglTrans' => 'Tgl Trans',
            'MotorAutoN' => 'Motor Auto N',
            'MotorNoMesin' => 'Motor No Mesin',
            'MotorWarna' => 'Motor Warna',
            'MotorTahun' => 'Motor Tahun',
            'MotorNoRangka' => 'Motor No Rangka',
            'MotorType' => 'Motor Type',
            'MotorHarga' => 'Motor Harga',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TsaldomotorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TsaldomotorQuery(get_called_class());
    }
}
