<?php

namespace afinance\models;

/**
 * This is the ActiveQuery class for [[Tdmotorahm]].
 *
 * @see Tdmotorahm
 */
class TdmotorahmQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdmotorahm[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdmotorahm|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
