<?php
namespace afinance\models;
use yii\helpers\ArrayHelper;
/**
 * This is the ActiveQuery class for [[Traccount]].
 *
 * @see Traccount
 */
class TraccountQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Traccount[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Traccount|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function combo() {
		return $this->select( [ "CONCAT(NoAccount,' - ',NamaAccount) as label", "NoAccount as value" ] )
		            ->orderBy( 'NoAccount' )
		            ->asArray()
		            ->all();
	}
	public function header() {
		return ArrayHelper::map( $this->select( [
			"CONCAT(NoAccount,' - ',NamaAccount) as label",
			"NoAccount as value"
		] )
		                              ->where( [ 'StatusAccount' => 'A', 'JenisAccount' => 'Header' ] )
		                              ->orderBy( 'NoAccount' )
		                              ->asArray()
		                              ->all(), 'value', 'label' );
	}
}
