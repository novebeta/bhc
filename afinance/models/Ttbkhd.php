<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttbkhd".
 *
 * @property string $BKNo
 * @property string $BKTgl
 * @property string $BKMemo
 * @property string $BKNominal
 * @property string $BKJenis Beli, BBN, Umum
 * @property string $SupKode
 * @property string $BKCekNo
 * @property string $BKCekTempo
 * @property string $BKPerson
 * @property string $BKAC
 * @property string $NoAccount
 * @property string $UserID
 * @property string $BKJam
 * @property string $NoGL
 */
class Ttbkhd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttbkhd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BKNo'], 'required'],
            [['BKTgl', 'BKCekTempo', 'BKJam'], 'safe'],
            [['BKNominal'], 'number'],
            [['BKNo'], 'string', 'max' => 12],
            [['BKMemo'], 'string', 'max' => 100],
            [['BKJenis'], 'string', 'max' => 20],
            [['SupKode', 'NoAccount', 'NoGL'], 'string', 'max' => 10],
            [['BKCekNo', 'BKAC'], 'string', 'max' => 25],
            [['BKPerson'], 'string', 'max' => 50],
            [['UserID'], 'string', 'max' => 15],
            [['BKNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'BKNo' => 'Bk No',
            'BKTgl' => 'Bk Tgl',
            'BKMemo' => 'Bk Memo',
            'BKNominal' => 'Bk Nominal',
            'BKJenis' => 'Beli, BBN, Umum',
            'SupKode' => 'Sup Kode',
            'BKCekNo' => 'Bk Cek No',
            'BKCekTempo' => 'Bk Cek Tempo',
            'BKPerson' => 'Bk Person',
            'BKAC' => 'Bkac',
            'NoAccount' => 'No Account',
            'UserID' => 'User ID',
            'BKJam' => 'Bk Jam',
            'NoGL' => 'No Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtbkhdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtbkhdQuery(get_called_class());
    }
}
