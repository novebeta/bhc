<?php

namespace afinance\models;

use Yii;

/**
 * This is the model class for table "ttsmhd".
 *
 * @property string $SMNo
 * @property string $SMTgl
 * @property string $LokasiAsal
 * @property string $LokasiTujuan
 * @property string $SMMemo
 * @property string $SMTotal
 * @property string $UserID
 * @property string $SMJam
 */
class Ttsmhd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttsmhd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SMNo'], 'required'],
            [['SMTgl', 'SMJam'], 'safe'],
            [['SMTotal'], 'number'],
            [['SMNo'], 'string', 'max' => 10],
            [['LokasiAsal', 'LokasiTujuan', 'UserID'], 'string', 'max' => 15],
            [['SMMemo'], 'string', 'max' => 100],
            [['SMNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SMNo' => 'Sm No',
            'SMTgl' => 'Sm Tgl',
            'LokasiAsal' => 'Lokasi Asal',
            'LokasiTujuan' => 'Lokasi Tujuan',
            'SMMemo' => 'Sm Memo',
            'SMTotal' => 'Sm Total',
            'UserID' => 'User ID',
            'SMJam' => 'Sm Jam',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtsmhdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtsmhdQuery(get_called_class());
    }
}
