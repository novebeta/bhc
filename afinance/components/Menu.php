<?php
namespace afinance\components;
use common\components\Custom;
use common\models\User;
use Yii;
use yii\base\Component;
use yii\db\Query;
use yii\helpers\ArrayHelper;
define( "ATTRIBUTE", [
	'System'                               => [ 'class' => 'fa fa-cogs', 'url' => '#' ],
	'Login Dealer'                         => [ 'class' => '', 'url' => Custom::urllgn( 'site/dealer' ) ],
	'User Profile'                         => [ 'class' => '', 'url' => Custom::url( 'tuser/update' ) ],
	'Dealer Profile'                       => [ 'class' => '', 'url' => Custom::url( 'tzcompany/update' ) ],
	'Re-Login'                             => [ 'class' => '', 'url' => Custom::urllgn( 'site/logout' ) ],
	'Keluar'                               => [ 'class' => '', 'url' => Custom::urllgn( 'site/blank' ) ],
	'Data'                                 => [ 'class' => 'fa fa-database', 'url' => '#' ],
	'Type  Warna Motor'                    => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tdmotorwarna/index' ) ],
	'Motor'                                => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tdmotortype/index' ) ],
	'Warehouse / Pos'                      => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tdlokasi/index' ) ],
	'Supplier'                             => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tdsupplier/index' ) ],
	'Dealer'                               => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tddealer/index' ) ],
	'Team'                                 => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tdteam/index' ) ],
	'Sales'                                => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tdsales/index' ) ],
	'Konsumen'                             => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tdcustomer/index' ) ],
	'Leasing'                              => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tdleasing/index' ) ],
	'Program'                              => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tdprogram/index' ) ],
	'Tarif BBN'                            => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tdbbn/index' ) ],
	'Area'                                 => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tdarea/index' ) ],
	'Pembelian'                            => [ 'class' => 'fa fa-shopping-cart', 'url' => '#' ],
	'Penerimaan Barang'                    => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttss/index' ) ],
	'Faktur Beli'                          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttfb/index' ) ],
	'Retur Beli'                           => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttsd/retur-beli' ) ],
	'Invoice Retur Beli'                   => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttsd/invoice-retur-beli' ) ],
	'Penjualan'                            => [ 'class' => 'fa fa-chart-line', 'url' => '#' ],
	'Inden'                                => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttin/index' ) ],
	'Data Konsumen Kredit'                 => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/data-konsumen-kredit' ) ],
	'Data Konsumen Tunai'                  => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/data-konsumen-tunai' ) ],
	'Data Konsumen Status'                 => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/data-konsumen-status' ) ],
	'STNK - BPKB'                          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/stnk-bpkb' ) ],
	'Data Portal'                          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/data-portal' ) ],
	'Pengajuan Berkas'                     => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/pengajuan-berkas' ) ],
	'Pengajuan Bayar BBN via Bank'         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/pengajuan-bayar-bbn-bank' ) ],
	'Pengajuan Bayar BBN via Kas'          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/pengajuan-bayar-bbn-kas' ) ],
	'Mutasi'                               => [ 'class' => 'fa fa-sync', 'url' => '#' ],
	'Mutasi Eksternal ke Dealer'           => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttsd/mutasi-eksternal-dealer' ) ],
	'Invoice Eksternal'                    => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttsd/invoice-eksternal' ) ],
	'Penerimaan dari Dealer'               => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttpd/penerimaan-dealer' ) ],
	'Invoice Penerimaan'                   => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttpd/invoice-penerimaan' ) ],
	'Keuangan'                             => [ 'class' => 'fa fa-file-invoice-dollar', 'url' => '#' ],
	'Akuntansi'                            => [ 'class' => 'fa fa-calculator', 'url' => '#' ],
	'Jurnal Memorial'                      => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttgeneralledgerhd/memorial' ) ],
	'Jurnal Adjustment'                    => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttgeneralledgerhd/adjustment' ) ],
	'Jurnal Transaksi'                     => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttgeneralledgerhd/transaksi' ) ],
	'Jurnal Pemindah Bukuan'               => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttgeneralledgerhd/pindahbuku' ) ],
	'Edit Perkiraan'                       => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'traccount/index' ) ],
	'Marketing'                            => [ 'class' => 'fa fa-poll', 'url' => '#' ],
	'Laporan'                              => [ 'class' => 'fa fa-book-reader', 'url' => '#' ],
	'Daftar Surat Jalan Supplier'          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-surat-jalan-supplier' ) ],
	'Daftar Faktur Beli'                   => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-faktur-beli' ) ],
	'Daftar Inden'                         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-inden' ) ],
	'Daftar Data Konsumen'                 => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-data-konsumen' ) ],
	'Daftar Surat Jalan Konsumen'          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-surat-jalan-konsumen' ) ],
	'Daftar Retur Konsumen'                => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-retur-konsumen' ) ],
	'Cetak Masal Bukti DK - SK'            => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/cetak-masal-bukti-dksk' ) ],
	'Daftar Surat Jalan Mutasi ke Dealer'  => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-surat-jalan-mutasi-dealer' ) ],
	'Daftar Surat Jalan Mutasi ke Pos'     => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-surat-jalan-mutasi-pos' ) ],
	'Daftar Penerimaan Barang dari Dealer' => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-penerimaan-barang-dealer' ) ],
	'Daftar Penerimaan Barang dari Pos'    => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-penerimaan-barang-pos' ) ],
	'Daftar Kas Masuk'                     => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-kas-masuk' ) ],
	'Daftar Kas Keluar'                    => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-kas-keluar' ) ],
	'Daftar Bank Masuk'                    => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-bank-masuk' ) ],
	'Daftar Bank Keluar'                   => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-bank-keluar' ) ],
	'Daftar Saldo Kas - Bank'              => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-saldo-kas-bank' ) ],
	'Jurnal Umum'                          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/jurnal-umum' ) ],
	'Neraca Percobaan'                     => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/neraca-percobaan' ) ],
	'Neraca dan Rugi Laba'                 => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/neraca-rugi-laba' ) ],
	'Laporan Harian'                       => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/laporan-harian' ) ],
	'Laporan Piutang'                      => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/laporan-piutang' ) ],
	'Laporan Hutang'                       => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/laporan-hutang' ) ],
	'Dashboard Akuntansi'                  => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/dashboard-akuntansi' ) ],
	'Daftar Stock Motor Acct'              => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-stock-motor-acct' ) ],
	'Daftar Motor'                         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-motor' ) ],
	'Daftar STNK dan BPKB'                 => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-stnk-bpkb' ) ],
	'Daftar Riwayat Motor'                 => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-riwayat-motor' ) ],
	'Daftar Stock Motor Fisik'             => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-stock-motor-fisik' ) ],
	'Rekap Harian'                         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/rekap-harian' ) ],
	'Rekap Bulanan'                        => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/rekap-bulanan' ) ],
	'Rekap Pivot Tabel'                    => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/rekap-pivot-tabel' ) ],
	'Dashboard 1'                          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/dashboard-1' ) ],
	'Dashboard 2'                          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/dashboard-2' ) ],
	'Dashboard 3'                          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/dashboard-3' ) ],
	'Laporan Astra'                        => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/laporan-astra' ) ],
	'Laporan Query'                        => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/laporan-query' ) ],
	'Laporan Data'                         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/laporan-data' ) ],
	'Laporan User Log'                     => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/laporan-user-log' ) ],
	'Validasi'                             => [ 'class' => 'fa fa-unlink', 'url' => '#' ],
] );
class Menu extends Component {
	const ADD = 'MenuTambah';
	const UPDATE = 'MenuEdit';
	const DELETE = 'MenuHapus';
	const PRINT = 'MenuCetak';
	const VIEW = 'MenuView';
	//define("ATTRIBUTE","");
	static function akses( $menuText, $akses ) {
		$akses_ = Yii::$app->session->get( '_akses' );
		return ArrayHelper::getValue( $akses_, [ $menuText, $akses ], 0 );
	}
	static function val( $menu, $tipe ) {
		return ArrayHelper::getValue( ATTRIBUTE, "$menu.$tipe", ( $tipe == 'url' ? '#' : '' ) );
	}
	static function getMenu() {
		$id = \Yii::$app->user->id;
		/** @var User $user */
		$user      = User::findOne( $id );
		$kodeAkses = $user->KodeAkses;
		try {
			$arr = Yii::$app->db->createCommand( "select  
				REPLACE(MenuInduk,'&','') AS MenuInduk, 
				REPLACE(MenuText,'&','') AS MenuText, 
				MenuTambah, MenuEdit,MenuHapus,MenuCetak,MenuView
				FROM tuakses WHERE KodeAkses = :kodeAkses AND MenuStatus = 1				
				ORDER BY MenuInduk,MenuNo;", [ ':kodeAkses' => $kodeAkses ] )
			                    ->queryAll();
			if ( $arr == null ) {
				return '';
			}
			$result = ArrayHelper::map( $arr, 'MenuTambah', 'MenuView', 'MenuText' );
			foreach ( $arr as $value ) {
				$result[ $value['MenuText'] ] = $value;
			}
			Yii::$app->session->set( '_akses', $result );
			$menu              = [];
			$menu['System']    = self::createMenu( 'System', $arr );
			$menu['Data']      = self::createMenu( 'Data', $arr );
			$menu['Pembelian'] = self::createMenu( 'Pembelian', $arr );
			$menu['Penjualan'] = self::createMenu( 'Penjualan', $arr );
			$menu['Mutasi']    = self::createMenu( 'Mutasi', $arr );
			$menu['Keuangan']  = self::createMenu( 'Keuangan', $arr );
			$menu['Akuntansi'] = self::createMenu( 'Akuntansi', $arr );
			$menu['Marketing'] = self::createMenu( 'Marketing', $arr );
			$menu['Laporan']   = self::createMenu( 'Laporan', $arr );
			$menu['Validasi']  = self::createMenu( 'Validasi', $arr );
			$menuBuilder       = '';
			foreach ( $menu as $k => $item ) {
				if ( isset( $result[ $k ] ) && $item != '' ) {
//					if ( $item != '' ) {
					$menuBuilder .= '<li class="treeview">
                                <a href="' . self::val( $k, 'url' ) . '">
                                    <i class="' . self::val( $k, 'class' ) . '"></i> <span>' . $k . '</span>
                                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i></span>
                                </a>
                                <ul class="treeview-menu">' . $item . '</ul>
                           </li>';
//					}
				}
			}
			return $menuBuilder;
		} catch ( Exception $e ) {
		}
	}
	static function createMenu( $parent, &$arr ) {
		$menu = '';
		foreach ( $arr as $k => $m ) {
			if ( $m['MenuInduk'] == $parent ) {
				$child = self::createMenu( $m['MenuText'], $arr );
				if ( $child == '' ) {
					if ( $m['MenuText'] == '-' ) {
						$menu .= '<li class="divider"></li>';
					} else {
						$menu .= '<li><a href="' . self::val( $m['MenuText'], 'url' ) . '"><i class="fa fa-check-circle"></i> ' . $m['MenuText'] . '</a></li>';
					}
				} else {
					$menu .= '<li class="treeview">
                                <a href="' . self::val( $k, 'url' ) . '">
                                    <i class="' . self::val( $k, 'class' ) . '"></i> <span>' . $m['MenuText'] . '</span>
                                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i></span>
                                </a>
                                <ul class="treeview-menu">' . $child . '</ul>
                           </li>';
				}
				unset( $arr['$k'] );
			}
		}
		return $menu;
	}
	static function getUserLokasi() {
		$q = new Query();
		return $q->select( "UserID, KodeAkses, PosKode, PosNama, PosAlamat, PosTelepon, PosNomor, PosStatus, NoAccount" )
		         ->from( "tuser" )
		         ->leftJoin( "tdpos", "tuser.LokasiKode = tdpos.PosKode" )
		         ->where( [ 'UserID' => Yii::$app->user->id ] )
		         ->one();
	}
}