<?php

namespace afinance\components;


use common\components\Custom;
use yii\db\ActiveRecord;
use yii\helpers\Url;

class TUi
{
    /**
     * @param string|array $items
     * @return string
     */
    static function renderItems($items = []) {
        if(!is_array($items)) return $items;
        $script = "";
        foreach ($items as $item)
            $script .= self::renderItem($item);
        return $script;
    }

    /**
     * @param string|array $item
     * @return string
     */
    private static function renderItem($item) {
        if(!is_array($item)) return $item;
        elseif (array_key_exists('template', $item))
            return self::renderInput($item);

        $tag =  "div";
        $attributes = [];
        $class = "";
        $items = "";
        $innerHtml = "";
        foreach ($item as $k => $v) {
            switch ($k) {
                case 'tag':
                    $tag = $v; break;
                case 'items':
                    $items = self::renderItems($v); break;
                case 'class':
                case 'responsive':
                    $class .= " $v"; break;
                case 'text':
                case 'innerHtml':
                    $innerHtml = $v; break;
                default:
                    $attributes[] = "$k='$v'";
            }
        }
        $is_inputTag = $tag === 'input';
        return "<$tag class='$class' ".implode(" ", $attributes).">". ($is_inputTag? "" : "$innerHtml$items</$tag>");
    }

    public static $fieldCounter = 0;

    /**
     * @param array $item
     * @return string
     */
    private static function renderInput($item) {
        $is_label = $item['template'] === 'label';
        $begin = "<div class='form-group' style='padding-right:0 !important; padding-left:0 !important;'>";
        $end = "</div>";

        if(isset($item['responsive']) && $item['responsive']) {
            $begin = "<div class='form-group ".$item['responsive']."' style='padding-right:0 !important; padding-left:0 !important;'>";
        }

        if(!isset($item['id'])) $item['id'] = "field".self::$fieldCounter++;
        if(!isset($item['label'])) $item['label'] = "";
        if(!$is_label)
            $begin .= "<label class='".($item['label'] ? "control-label" : "sr-only")."' for='".$item['id']."'>".$item['label']."</label>";

        $attributes = [];
        $class = $is_label ? "" : "form-control";
        $innerHtml = "";
        foreach ($item as $k => $v) {
            switch ($k) {
                case 'items':
                case 'template':
                case 'tag':
                case 'responsive':
                case 'label':
                    continue;
                case 'class':
                    $class .= " $v"; break;
                case 'text':
                case 'innerHtml':
                    $innerHtml = $v; break;
                default:
                    $attributes[] = "$k='$v'";
            }
        }

        $field = "";
        switch ($item['template']) {
            case 'label':
                $field = "<label class='$class' ".implode(" ", $attributes).">$innerHtml</label>";
                break;
            case 'text':
                $field = "<input type='".$item['template']."' class='$class' ".implode(" ", $attributes).">";
                break;
            case 'date':
                $field = "<div class='input-group date' data-date-format='".(isset($item['format'])? $item['format'] : "dd/mm/yyyy")."'>
                            <input  type='text' class='$class' ".implode(" ", $attributes).">
                            <div class='input-group-addon' >
                                <span class='glyphicon glyphicon-th'></span>
                            </div>
                        </div>";
                break;
            case 'button':
                $field = "<button type='".$item['template']."' class='$class' ".implode(" ", $attributes).">$innerHtml</button>";
                break;
        }

        return $begin.$field.$end;
    }

    public static $jsSign = '~func~';

    /**
     * @param ActiveRecord $model
     * @param array $cols
     * @return string|string[]|null
     */
    private static function generateGridCols($model, $cols = []){
        $colModels = [];
        $dbCols = $model->getTableSchema()->columns;
        $dbColLabels = $model->attributeLabels();

        /* jika kosong, maka akan menampilkan semua kolom */
        if(!count($cols))
            foreach ($dbCols as $field => $v)
                $cols[] = [
                    'name' => $field,
                    'index' => $field,
                    'editable' => true,
                    'colMenu' => true,
                ];

        foreach ($cols as $col) {
            $colModel = [];

            foreach ($col as $k => $v)
                if(!in_array($k, ['name','index','label','sorttype','editable','renderer',
                    'editor','edittype','template','editoptions','editoptions']))
                    $colModel[$k] = $v;

            $field = array_key_exists('name', $col) ? $col['name'] : $col['index'];

            $colModel['name'] = array_key_exists('name', $col) ? $col['name'] : $field;
            $colModel['index'] = array_key_exists('index', $col) ? $col['index'] : $field;

            if (array_key_exists('label', $col))
                $colModel['label'] = $col['label'];
            elseif (array_key_exists($field, $dbColLabels))
                $colModel['label'] = $dbColLabels[$field];

            if (array_key_exists('sorttype', $col))
                $colModel['sorttype'] = $col['sorttype'];
            elseif (array_key_exists($field, $dbCols))
                $colModel['sorttype'] = $dbCols[$field]->type;

            if (array_key_exists('width', $col))
                $colModel['width'] = $col['width'];

            /* jika kolom dapat menerima input */
            if(array_key_exists('editable', $col) && $col['editable']) {
                $colModel['editable'] = true;
                $colModel = array_merge($colModel, self::colEditor($col, array_key_exists($field, $dbCols) ? $dbCols[$field] : null));
            }

            $colModels[] = json_encode($colModel);
        }

        $stringColModels = implode(', ', $colModels);
        $stringColModels = preg_replace('/[\'"]'.self::$jsSign.'/', '', $stringColModels);
        $stringColModels = preg_replace('/'.self::$jsSign.'[\'"]/', '', $stringColModels);
        return $stringColModels;
    }

    /**
     * @param $col
     * @param $dbCol
     * @return array
     */
    private static function colEditor($col, $dbCol) {
            $colEditor = [];

            $editrules = [];
            $editoptions = [];

            if($dbCol)
                $editrules['required'] = !$dbCol->allowNull;

            if(array_key_exists('editor', $col)) {
                $editor = $col['editor'];
                /*
                 * custom editor attr
                 * type
                 * data
                 * placeholder
                 */
                switch ($editor['type']) {
                    case 'select2':
                        $colEditor['edittype'] = 'select';
                        $editoptions['value'] = $editor['data'];
                        $editoptions['dataInit'] = <<< JS
                            function(element) { $(element).select2(); }
JS;
                        $editoptions['dataInit'] = self::$jsSign.$editoptions['dataInit'].self::$jsSign;
                        break;
                }

            } else if (!array_key_exists('edittype', $col)) {
                /* Jika editor tidak didefinisikan */
                $dbType = '';
                if($dbCol && preg_match('/^[a-zA-Z]+/', $dbCol->dbType, $output_array))
                    $dbType = $output_array[0];

                switch ($dbType) {
                    case 'varchar':
                    case 'char':
                        $colEditor['edittype'] = 'text';
                        $editoptions['maxlength'] = $dbCol->size;
                        break;
                    case 'text':
                    case 'tinytext':
                    case 'mediumtext':
                    case 'longtext':
                        $colEditor['edittype'] = 'textarea';
                        break;
                    case 'date':
                        $colEditor['edittype'] = 'text';
                        $colEditor['formatter'] = self::$jsSign.'renderDate_ddmmyyyy'.self::$jsSign;
                        $editoptions['dataInit'] = self::$jsSign.'funcEditTypeDate'.self::$jsSign;
                        break;
                    case 'smallint':
                    case 'mediumint':
                    case 'int':
                    case 'integer':
                    case 'bigint':
                    case 'decimal':
                    case 'dec':
                    case 'float':
                    case 'double':
                    case 'real':
                        $colEditor['edittype'] = 'text';
                        $colEditor['template'] = 'number';
                        $editoptions['type'] = 'number';
                        $editoptions['pattern'] = '[0-9]+([\.|,][0-9]+)?';
                        $editrules['number'] = true;
                        break;
                    case 'enum':
                        $colEditor['edittype'] = 'select';
                        $editoptions['value'] = $dbCol->enumValues;
                        break;
                    default :
                        $colEditor['edittype'] = 'text';
                }
            }

            if(count($editrules)) $colEditor['editrules'] = $editrules;
            if(count($editoptions)) $colEditor['editoptions'] = $editoptions;

            return $colEditor;
    }

    /**
     * @param $model
     * @param array $options
     * @return string
     */
    static function detailGridJs($model, $options = []) {
        /* load options */
        $url = array_key_exists('url', $options) ? $options['url'] : Custom::url(\Yii::$app->controller->id.'/detail');
        $cols = array_key_exists('colModel', $options) ? $options['colModel'] : [];
        $height = array_key_exists('height', $options) ? $options['height'] : 220;
        $shrinkToFit = array_key_exists('shrinkToFit',$options) ? $options['shrinkToFit'] : 0;

        /* serialize extraParams */
        $extraParamsJson = json_encode($options['extraParams']);
        $extraParams = '';
        if(array_key_exists('extraParams',$options) && is_array($options['extraParams']) && count($options['extraParams'])) {
            foreach ($options['extraParams'] as $k => $v)
                $extraParams .= "&$k=$v";
        }

        $funcAfterLoadGrid = array_key_exists('funcAfterLoadGrid', $options) ? $options['funcAfterLoadGrid'] : 'function(grid, response) {}';

        $model = new $model;
        $gridCols = self::generateGridCols($model, $cols);

        return <<< JS
jQuery(function ($) {
    
    /* Append UI Element */
    $("body").append(
        '<div class="modal fade" id="confirmDeleteRowModal" tabindex="-1" role="dialog">' +
            '<div class="modal-dialog modal-sm" role="document">' +
                '<div class="modal-content">' +
                    '<div class="modal-body">Hapus data terpilih?</div>' +
                    '<div class="modal-footer">' +
                        '<button type="button" class="btn btn-primary" id="confirmDeleteRowModal_btnDelete">Hapus</button>' +
                        '<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>'
    );
    
    /* variables and functions */
    var grid = $("#detailGrid"),
        confirmDeleteModal = $('#confirmDeleteRowModal'),
        confirmDeleteModal_btnDelete = $("#confirmDeleteRowModal_btnDelete"),
        
        //getSelectedRowId = function() { return grid.getGridParam('selrow'); },
        addRow = function() {
            grid.jqGrid('addRow', {
                initdata : {},
                position :"first",
                useDefValues : false,
                useFormatter : false,
                addRowParams : {extraparam:{}}
            });
            $('#jEditButton_jqg1').click();
        },
        deleteRow = function(rowId) {
            /* hide confirm dialog */
            confirmDeleteModal.modal('hide');
            
            /* delete row request */
            $.ajax({
                url: '$url',
                method:"POST",
                data: { oper: 'del', id: rowId },
                success:function(response) {
                    console.log(response);
                    if(response.success) {
                        /* remove listener */
                        confirmDeleteModal_btnDelete.unbind();
                        
                        /* remove row from grid */
                        grid.jqGrid('delRowData', rowId);
                    } else {
                        window.confirmDeleteRow(rowId);
                    }
                },
                error:function(response){
                    console.log(response);
                    alert("error");
                }
          });
        },
        actionsFormatter = function(cellVal,options,rowObject) {
            return '' +
             '<div style="margin-left:8px;">' +
              '<div title="Sunting data terpilih" style="float: left; cursor: pointer;" class="ui-pg-div ui-inline-edit" id="jEditButton_jqg1" onclick="$.fn.fmatter.rowactions.call(this,\'edit\');" onmouseover="$(this).addClass(\'active\');" onmouseout="$(this).removeClass(\'active\');">' +
               '<a class="glyphicon glyphicon-edit"></a>' +
              '</div>' +
              '<div title="Hapus baris terpilih" style="float: left; cursor: pointer;" class="ui-pg-div ui-inline-del" id="jDeleteButton_jqg1" onclick="window.confirmDeleteRow(\''+options.rowId+'\');" onmouseover="$(this).addClass(\'active\');" onmouseout="$(this).removeClass(\'active\');">' +
               '<a class="glyphicon glyphicon-trash"></a>' +
              '</div>' +
              '<div title="Simpan" style="float: left; display: none; cursor: pointer;" class="ui-pg-div ui-inline-save" id="jSaveButton_jqg1" onclick="$.fn.fmatter.rowactions.call(this,\'save\');" onmouseover="$(this).addClass(\'active\');" onmouseout="$(this).removeClass(\'active\');">' +
               '<a class="glyphicon glyphicon-floppy-disk"></a>' +
              '</div>' +
              '<div title="Batal" style="float: left; display: none; cursor: pointer;" class="ui-pg-div ui-inline-cancel" id="jCancelButton_jqg1" onclick="$.fn.fmatter.rowactions.call(this,\'cancel\');" onmouseover="$(this).addClass(\'active\');" onmouseout="$(this).removeClass(\'active\');">' +
               '<a class="glyphicon glyphicon-remove-circle"></a>' +
              '</div></div>';
        },
        
        funcEditTypeDate = function(el){
            $(el).datepicker({
                format: "dd-mm-yyyy",
                language: "id",
                daysOfWeekHighlighted: "0",
                autoclose: true,
                todayHighlight: true
            }); 
        },
        renderDate_ddmmyyyy = function(cellValue, options, rowObject){
            if(!cellValue) return '';
            var dt = new Date(cellValue),
                d = dt.getDate(),
                m = dt.getMonth() + 1,
                y = dt.getFullYear();
            return (d<10?'0':'') + d + "-" + (m<10?'0':'') + m + "-" + y;
        },
        
        reloadGrid = function(){
            $.ajax({
                url: '$url',
                method:"POST",
                data: $extraParamsJson,
                success: function(response) {
                    //console.log(response);
                    
                    grid.jqGrid('clearGridData')
                        .jqGrid('setGridParam', { datatype: 'local', data: response.rows })
                        .trigger('reloadGrid')
                        .jqGrid('setGridParam', { datatype: 'json' });
                    
                    funcAfterLoadGrid(grid, response);
                }
            })
        },
        funcAfterLoadGrid =$funcAfterLoadGrid,
        
        ajaxGridOpt = {
            beforeSend : function(jqXHR, ajaxOptions) {
                ajaxOptions.data += '$extraParams';
            },
            success : function(response, status, error) {
                
            },
            error : function(response, status, error) {
                
            }
        },
        ajaxRowOpt = {
            beforeSend : function(jqXHR, ajaxOptions) {
                ajaxOptions.data += '$extraParams';
            },
            success : function(response, status, error) {
                // grid.trigger("reloadGrid");
                reloadGrid();
            },
            error : function(response, status, error) {
                $.notify(
                    {
                        message: error + '<br> ' + response.responseJSON.message
                    },
                    {
                        type: 'danger',
                        mouse_over: 'pause',
                        newest_on_top: true,
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    }
                );
            }
        };
    
    /* Global function */
    window.confirmDeleteRow = function(rowId) {
        $('#confirmDeleteRowModal').modal({ backdrop: 'static', keyboard: true, show: true });
        confirmDeleteModal_btnDelete.click(function() { deleteRow(rowId); });
    };
    
    grid.jqGrid({
        url: '$url',
        mtype: "POST",
        datatype: "local",
        jsonReader: {
            root: 'rows',
            records: 'rowsCount',
            id: 'rowId',
            repeatitems: false,
            page:  function(obj) { return 1; },
            total: function(obj) { return 1; }
        },
        editurl: '$url',
        // ajaxGridOptions: ajaxGridOpt,
        ajaxRowOptions : ajaxRowOpt,
        
        colModel: [
            { label: "Actions", name: "actions", width: 65, formatter: actionsFormatter},
            { label: "rowId", name: "rowId", index: "rowId", hidden: true},
            $gridCols
        ],
        
        responsive: false,
        styleUI: 'Bootstrap',
        width: 780,
        height: $height,
        rowNum: 20,
        rownumbers: true,
        autowidth: true,
        shrinkToFit : $shrinkToFit,
        
        pager: '#detailGridPager',
        pgbuttons: false,
        pgtext: null,
        viewrecords: false
        
    }) // end of jqGrid
    .navGrid('#detailGridPager',{ edit:false,add:false,del:false,search:false,refresh:false })
    .jqGrid("setLabel", "rn", "No")
    .navButtonAdd('#detailGridPager',{
       caption:' Tambah',
       buttonicon:'glyphicon glyphicon-plus',
       onClickButton: addRow,
       position:'last'
	});
    
    // confirmDeleteModal_btnDelete.click(function() {
    //     var rowId = getSelectedRowId();
    //     deleteRow(rowId);
    // });
    
    /* Grid resize */
    function resizeGrid() {
        $('#detailGrid').setGridWidth($('.box-body').width());
    }
    
    resizeGrid();
    $('.box-body').resize(resizeGrid);
    
    reloadGrid();
    
}); // end of JQuery

JS;
    }
}