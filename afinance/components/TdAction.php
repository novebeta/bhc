<?php
namespace afinance\components;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
class TdAction extends Action {
	/** @var string|ActiveRecord $model */
	public $model;
	public $colGrid;
	public $join = [];
	public $joinWith = [];
	public $grupBy = [];
	public $where = [];
	/** @var array */
	public $requestData = [];
	/**
	 * @return array
	 * @throws InvalidConfigException
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function run() {
		$this->requestData = \Yii::$app->request->post();
		foreach ( $this->requestData as $k => $v ) {
			if ( preg_match( '/^(\d{2})(-\d{2}-)(\d{4})$/', $v, $output_array ) ) {
				$this->requestData[ $k ] = $output_array[3] . $output_array[2] . $output_array[1];
			}
		}
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		if ( isset( $this->requestData['oper'] ) )
			switch ( $this->requestData['oper'] ) {
				case 'add':
					return $this->addAction();
				case 'edit':
					return $this->editAction();
				case 'del':
					return $this->delAction();
			}
		else {
			return $this->readAction();
		}
	}
	/**
	 * @return array
	 */
	protected function addAction() {
		$model             = new $this->model;
		$model->attributes = $this->requestData;
		if ( $model->save() ) {
			return $this->responseSuccess( 'Data baru berhasil ditambahkan.' );
		} else {
			return $this->responseFailed( $this->modelErrorSummary( $model ) );
		}
	}
	/**
	 * @return array
	 * @throws InvalidConfigException
	 * @throws NotFoundHttpException
	 */
	protected function editAction() {
		$model = $this->findModel( $this->requestData['id'] );;
		$model->attributes = $this->requestData;
		if ( $model->save() ) {
			return $this->responseSuccess( 'Perubahan data berhasil disimpan.' );
		} else {
			return $this->responseFailed( $this->modelErrorSummary( $model ) );
		}
	}
	/**
	 * @return array
	 * @throws InvalidConfigException
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	protected function delAction() {
		$model = $this->findModel( $this->requestData['id'] );
		if ( $model->delete() ) {
			return $this->responseSuccess( 'Data telah dihapus.' );
		} else {
			return $this->responseFailed( $this->modelErrorSummary( $model ) );
		}
	}
	/**
	 * @return array
	 * @throws InvalidConfigException
	 */
	protected function readAction() {
		$model = new $this->model;
		if ( $this->colGrid == null ) {
			$this->colGrid = $model::colGrid();
		}
		$query  = new ActiveQuery( $this->model );
		$pk     = $this->modelPrimaryKey();
		$fields = count( $pk ) ? $pk : $this->modelAttributes();
		array_walk( $fields, function ( &$value, $key ) {
			$value = $this->model::getTableSchema()->name . "." . $value;
		} );
		$query->select( array_merge( [
			'CONCAT(' . implode( ",'||',", $fields ) . ') AS rowId'
		], array_keys( $this->colGrid ) ) );
		foreach ( $this->joinWith as $k => $relasi ) {
			$query->joinWith( $k, false, $relasi['type'] );
		}
		foreach ( $this->join as $j ) {
			$query->join( $j['type'], $j['table'], $j['on'], $j['params'] );
		}
		$query->groupBy( $this->grupBy );
//		$orderBy = "";
		if ( $this->requestData['sidx'] != null ) {
			$orderBy = $this->requestData['sidx'] . ' ' . $this->requestData['sord'];
			$query->orderBy( $orderBy );
		}
		$query->offset( ( $this->requestData['page'] - 1 ) * $this->requestData['rows'] )
		      ->limit( $this->requestData['rows'] );
		if ( isset( $this->requestData['query'] ) ) {
			$q = Json::decode( $this->requestData['query'], true );
//			if ( isset( $q['where'] ) ) {
//				foreach ( $q['where'] as $condition ) {
//					$query->andWhere( $condition );
//				}
//			}
			foreach ( $this->where as $c ) {
				if ( $c['op'] == 'AND' ) {
					$query->andWhere( $c['condition'], $c['params'] );
				}
				if ( $c['op'] == 'OR' ) {
					$query->orWhere( $c['condition'], $c['params'] );
				}
			}
			if ( $q['txt1'] != '' ) {
				$query->andWhere( $q['cmbTxt1'] . " LIKE '%" . $q['txt1'] . "%'" );
			}
			if ( $q['txt2'] != '' ) {
				$query->andWhere( $q['cmbTxt2'] . " LIKE '%" . $q['txt2'] . "%'" );
			}
			if ( $q['check'] == 'on' ) {
				if ( $q['cmbTgl1'] != '' ) {
					$query->andWhere( $q['cmbTgl1'] . " >= '" . $q['tgl1'] . "'" );
					$query->andWhere( $q['cmbTgl1'] . " <= '" . $q['tgl2'] . "'" );
				}
				if ( $q['cmbNum1'] != '' && $q['num1'] != '' ) {
					$query->andWhere( $q['cmbNum1'] . " " . $q['cmbNum2'] . " " . $q['num1'] );
				}
			}
		}
		$rowsCount = $query->count();
		$rows      = $query->asArray()->all();
		return [
			'rows'        => $rows,
			'rowsCount'   => $rowsCount,
			'currentPage' => $this->requestData['page'],
			'totalPages'  => ceil( $rowsCount / $this->requestData['rows'] )
		];
	}
	/**
	 * @param $rowId
	 *
	 * @return ActiveRecord|null
	 * @throws InvalidConfigException
	 * @throws NotFoundHttpException
	 */
	protected function findModel( $rowId ) {
		$primaryKey = $this->modelPrimaryKey();
		$fields     = count( $primaryKey ) ? $primaryKey : $this->modelAttributes();
		$condition  = [];
		$val        = explode( "||", base64_decode( $rowId ) );
		for ( $i = 0; $i < count( $fields ) && $i < count( $val ); $i ++ ) {
			$condition[ $fields[ $i ] ] = $val[ $i ];
		}
		$model = $this->model::findOne( $condition );
		if ( $model ) {
			return $model;
		}
		throw new NotFoundHttpException( 'Data tidak ditemukan.' );
	}
	/**
	 * @return array
	 */
	private function modelAttributes() {
		$model      = new $this->model;
		$attributes = [];
		foreach ( $model->attributeLabels() as $field => $label ) {
			$attributes[] = $field;
		}
		return $attributes;
	}
	/**
	 * @return string[]
	 * @throws InvalidConfigException
	 */
	private function modelPrimaryKey() {
		return $this->model::getTableSchema()->primaryKey;
	}
	/**
	 * @param ActiveRecord $model
	 *
	 * @return string
	 */
	private function modelErrorSummary( $model ) {
		return implode( '. ', $model->getFirstErrors() );
	}
	/**
	 * @param string $message
	 *
	 * @return array
	 */
	private function responseSuccess( $message ) {
		return [
			'success' => true,
			'message' => $message,
		];
	}
	/**
	 * @param string $message
	 *
	 * @return array
	 */
	private function responseFailed( $message ) {
		return [
			'success' => false,
			'message' => $message,
		];
	}
}