<?php
namespace afinance\components;
use common\components\Custom;
use afinance\components\Menu;
use yii\db\ActiveRecord;
class TdUi {
	/**
	 * @param ActiveRecord $model
	 *
	 * @param string $menuText
	 * @param array $options
	 *
	 * @return string
	 */
	static function mainGridJs( $model, $menuText = '', $options = [] ) {
		$colGrid    = [];
		$class      = explode( "\\", $model::className() );
		$class      = end( $class );
		$class      = strtolower( $class );
		$url        = Custom::url( $class . '/td' );
		$url_add    = Custom::url( $class . '/create' );
		$url_update = Custom::url( $class . '/update' );
		$url_view   = Custom::url( $class . '/view' );
		$url_print  = Custom::url( $class . '/print' );
		$url_delete = $url;
		if ( isset( $options['url'] ) ) {
			$url = $options['url'];
		}
		if ( isset( $options['url_add'] ) ) {
			$url_add = $options['url_add'];
		}
		if ( isset( $options['url_update'] ) ) {
			$url_update = $options['url_update'];
		}
		if ( isset( $options['url_delete'] ) ) {
			$url_delete = $options['url_delete'];
		}
		$button_add     = Menu::akses( $menuText, Menu::ADD ) ? "grid.navButtonAdd('#jqGridPager',{
		   caption:' Tambah', 
		   buttonicon:'glyphicon glyphicon-plus', 
		   onClickButton: function(){ 
		      window.location = '$url_add';
		   }, 
	        position:'last'
			})" : '';
		$button_update  = Menu::akses( $menuText, Menu::UPDATE ) ?
			"<a href=\"$url_update&id='+id+'&action=update\" title=\"Edit\" aria-label=\"Update\" data-pjax=\"0\"><span class=\"glyphicon glyphicon-edit\"></span></a>" : "";
		$button_delete  = Menu::akses( $menuText, Menu::DELETE ) ?
			"<a href=\"$url_delete\" rowId=\"'+id+'\" title=\"Hapus\" aria-label=\"Delete\" data-pjax=\"0\" data-confirm=\"Are you sure you want to delete this item?\" data-method=\"post\"><span class=\"glyphicon glyphicon-trash\"></span></a>" : "";
		$button_view    = Menu::akses( $menuText, Menu::VIEW ) ?
			"<a href=\"$url_view&id='+id+'\" title=\"Lihat\" aria-label=\"View\" data-pjax=\"0\"><span class=\"glyphicon glyphicon-view\"></span></a>" : "";
		$button_print   = Menu::akses( $menuText, Menu::VIEW ) ?
			"<a href=\"$url_print&id='+id+'\" title=\"Cetak\" aria-label=\"Print\" data-pjax=\"0\"><span class=\"glyphicon glyphicon-print\"></span></a>" : "";
		$cols           = [];
		$gridColsProses = '';
		if ( $button_update != '' || $button_delete != '' || $button_print != '' ) {
			$gridColsProses = " {
                 label: 'Proses', 
                 name: 'actions', 
                 sortable: false,
                 width: 75, 
                 align: 'center',
                 frozen:true,
                 formatter: function(cellvalue, options, rowObject) {
                     let id = btoa(options.rowId);
                   return '$button_update $button_delete';
                 }
             },";
		}
		$model = new $model;
		if ( isset( $options['colGrid'] ) ) {
			$colGrid = $options['colGrid'];
		} else {
			$colGrid = $model::colGrid();
		}
		foreach ( $colGrid as $filed => $label ) {
			$is_array = is_array( $label );
			$col      = [];
			if ( $is_array ) {
				$col = $label;
			} else {
				$col['name']  = $filed;
				$col['label'] = $label;
			}
			$cols[] = json_encode( $col );
		}
		$gridCols = implode( ', ', $cols );
		$gridCols = preg_replace( '/[\'"](func\w+)[\'"]/', '$1', $gridCols );
		$script   = <<< JS
jQuery(function ($) {
    var ajaxRowOpt = {
            success : function(response, status, error) {
                $("#jqGrid").trigger("reloadGrid");
                
                $.notify(
                    {
                        message: response.message 
                    },
                    {
                        type: response.success ? 'success' : 'warning',
                        mouse_over: 'pause',
                        newest_on_top: true,
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    }
                );
            },
            error : function(response, status, error) {
                $.notify(
                    {
                        message: error + '<br> ' + response.responseJSON.message
                    },
                    {
                        type: 'danger',
                        mouse_over: 'pause',
                        newest_on_top: true,
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    }
                );
            }
        };
    
    var funcEditTypeCombo = function(el){
        $(el).datepicker({
            format: "dd-mm-yyyy",
            language: "id",
            daysOfWeekHighlighted: "0",
            autoclose: true,
            todayHighlight: true
        }); 
    };
    
    var funcFormaterDate = function(cellValue, options, rowObject){
        if(!cellValue) return '';
        var dt = new Date(cellValue),
            d = dt.getDate(),
            m = dt.getMonth() + 1,
            y = dt.getFullYear();
        return (d<10?'0':'') + d + "-" + (m<10?'0':'') + m + "-" + y;
    };
    
    // $.jgrid.formatter.number.decimalSeparator = ',';
    // $.jgrid.formatter.number.thousandsSeparator = '.';
    // $.jgrid.defaults.width = 1000;
    // $.jgrid.defaults.responsive = true;
    $.jgrid.defaults.styleUI = 'Bootstrap';
    $.jgrid.defaults.pgtext = "{0} of {1}";
    var url_string = $(location).attr('href');
    var url = new URL(url_string);
	var c = decodeURIComponent(url.searchParams.get("r"));
    var pageKey = c;
    // console.log(pageKey);
    var grid = $("#jqGrid"); 
//    var url_add = '$url_add';
//    var url_update = '$url_update';
//    var url_delete = '$url_delete';
    grid.jqGrid({
        url: '$url',
        mtype: "POST",
        datatype: "json",
        jsonReader: {
            root: 'rows',
            page: 'currentPage',
            total: 'totalPages',
            records: 'rowsCount',
            id: 'rowId',
            repeatitems: false
        },
        rownumbers: true,
        colModel: [
            $gridColsProses
            $gridCols
        ],        
        multiSort: true,
        sortname: setcmb.sortname,
        sortorder: setcmb.sortorder,
        search: true,
        postData: {
		    query: function () {
		       const search={
		           cmbTxt1: '',
		            txt1 : '',
		            cmbTxt2 : '',
		            txt2 : '',
		            check :'on',
		            cmbTgl1 : $('#filter-tgl-cmb1').val(),
		            tgl1 : $('#filter-tgl-val1').val(),
		            tgl2 : $('#filter-tgl-val2').val(),
		            cmbNum1 : '',
		            cmbNum2 : '',
		            num1 : '',
		            r : c
		        };
            	return JSON.stringify(search);
		    }
		},
        altclass: 'row-zebra',
        altRows: true,
        pager: "#jqGridPager",
        viewrecords: true,
        rowNum: 15,
        rowList:[15,30,45,60,75],
        width: 780,
        height: 375,
        autowidth:false, 
        shrinkToFit:false,
        styleUI : 'Bootstrap',
        // toppager:true,
        ajaxRowOptions : ajaxRowOpt,
        page: (document.cookie.match(new RegExp("(?:.*;)?\s*"+pageKey+"\s*=\s*([^;]+)(?:.*)?$"))||[,1])[1],
        loadComplete: function(){
            document.cookie = pageKey+"="+$("#jqGrid").getGridParam('page')+";path=/"; 
        },
        gridComplete:function() {
           $('a[data-confirm]').on('click',function(ev) {
		        var href = $(this).attr('href');
		        var rowId = $(this).attr('rowId');
		        if (!$('#dataConfirmModal').length) {
		            $('body').append('<div id="dataConfirmModal" class="modal bootstrap-dialog type-warning fade size-normal in" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true">' +
		            '<div class="modal-dialog"><div class="modal-content">' +
		             '<div class="modal-header"><div class="bootstrap-dialog-header"><div class="bootstrap-dialog-close-button" style="display: none;"><button class="close" data-dismiss="modal" aria-label="close">×</button></div><div class="bootstrap-dialog-title" id="w3_title">Confirmation</div></div></div>' +
		              '<form id="delete-form" method="post"><div class="modal-body"><div class="bootstrap-dialog-body"><div class="bootstrap-dialog-message">Are you sure you want to delete this item?</div></div></div>' +
		               '<div class="modal-footer"><div class="bootstrap-dialog-footer"><div class="bootstrap-dialog-footer-buttons"><button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><span class="fa fa-ban"></span>  Cancel</button><button class="btn btn-warning" type="submit" ><span class="glyphicon glyphicon-ok"></span> Ok</button></div></div></div></form></div></div></div>');
		        } 
		        $('#dataConfirmModal').find('.modal-body').text($(this).attr('data-confirm'));
		        // $('#dataConfirmOK').attr('href', href);
		        $('#dataConfirmModal').modal({show:true});
		          $('#delete-form').on('submit', function(e){
			        e.preventDefault();			        
			        // $.notifyClose();
			        $.ajax({
			            url:  href, //this is the submit URL
			            type: 'POST', //or POST
			            data: {
			                'oper':'del',
			            	'id': rowId    
			            },
			            success : function(data){
			                 $.notify({
                        		message: data.message 
                    		},
                    		{
		                        type: data.success ? 'success' : 'warning',
		                        allow_duplicates: false,
		                        mouse_over: 'pause',
		                        newest_on_top: true,
		                        placement: {
		                            from: "top",
		                            align: "right"
		                        },
		                        animate: {
		                            enter: 'animated fadeInDown',
		                            exit: 'animated fadeOutUp'
		                        }
                    		});
			                 $('#jqGrid').trigger( 'reloadGrid' );
			            }
			        });
			        
			        $('#dataConfirmModal').modal('hide');
			    });
		        return false;
		    });
        }
    }).navGrid('#jqGridPager',{
        edit:false,add:false,del:false,search:false,refresh:false
    }).bindKeys();
    grid.jqGrid("setLabel", "rn", "No",{'text-align':'center'});
    grid.jqGrid("setLabel", "actions", "Proses",{'text-align':'center'});
    // grid.jqGrid('getGridParam', 'colModel').forEach(item => {
    //    grid.jqGrid("setLabel", item.name, "",{'text-align':item.align});
    // });
    $button_add
     function resizeGrid() {
        $('#jqGrid')
        .setGridWidth($('.panel-body').width(),false);
        // .setGridHeight($(window).height() - $("#jqGrid").offset().top);
    }
    resizeGrid();
    $('.panel-body').resize(resizeGrid);
	
});
JS;
		return $script;
	}
}