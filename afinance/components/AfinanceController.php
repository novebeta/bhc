<?php
namespace afinance\components;
use common\components\Custom;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
class AfinanceController extends Controller {
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => [ '@' ],
					],
				],
			],
		];
	}
	public function beforeAction( $action ) {
		try {
			if ( ! parent::beforeAction( $action ) ) {
				return false;
			}
			if ( ! Yii::$app->user->isGuest ) {
				if ( Yii::$app->session['userSessionTimeout'] < time() ) {
					return $this->redirect( Custom::urllgn( 'site/logout' ) );
				} else {
					if($action->id != 'waktu') {
						Yii::$app->session->set( 'userSessionTimeout', time() + Yii::$app->params['sessionTimeoutSeconds'] );
					}
					return true;
				}
			} else {
				return $this->redirect( Custom::urllgn( 'site/logout' ) );
			}
		} catch ( BadRequestHttpException $e ) {
			return false;
		}
	}
	protected function findModelBase64( $modelName, $id ) {
		try {
			$className = '\afinance\models\\'.$modelName;
			$modelObj = Yii::createObject([
				'class' => $className,
			]);
			$primaryKey = $modelObj::getTableSchema()->primaryKey;
			$fields     = $primaryKey;
			$condition  = [];
			$val        = explode( "||", base64_decode( $id ) );
			for ( $i = 0; $i < count( $fields ) && $i < count( $val ); $i ++ ) {
				$condition[ $fields[ $i ] ] = $val[ $i ];
			}
			$model = $modelObj::findOne( $condition );
			if ( $model ) {
				return $model;
			}
			throw new NotFoundHttpException( 'Data tidak ditemukan.' );
		} catch ( InvalidConfigException $e ) {
		}
	}

    /**
     * @param yii\db\ActiveRecord $model
     * @return string
     */
    protected function generateIdBase64FromModel($model){
        $modelName = get_class($model);
        $primaryKeys = $modelName::getTableSchema()->primaryKey;
        return base64_encode(implode('||', $model->getAttributes($primaryKeys)));
    }

    /**
     * @param yii\db\ActiveRecord $model
     *
     * @return string
     */
    protected function modelErrorSummary( $model ) {
        return implode( '. ', $model->getFirstErrors() );
    }
    /**
     * @param string $message
     *
     * @return array
     */
    protected function responseSuccess( $message ) {
        return [
            'success' => true,
            'message' => $message,
        ];
    }
    /**
     * @param string $message
     *
     * @return array
     */
    protected function responseFailed( $message ) {
        return [
            'success' => false,
            'message' => $message,
        ];
    }protected function KodeAkses(){
	return Menu::getUserLokasi()[ 'KodeAkses' ];
}
	protected function UserID(){
		return Menu::getUserLokasi()[ 'UserID' ];
	}
	protected function LokasiKode(){
		return Menu::getUserLokasi()[ 'PosKode' ];
	}
}