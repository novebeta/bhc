<?php
$main = [
    'components' => [
	    'formatter' => [
		    'dateFormat' => 'dd.MM.yyyy',
		    'decimalSeparator' => ',',
		    'thousandSeparator' => '.',
//		    'currencyCode' => 'EUR',
	    ],
	    'user' => [
		    'loginUrl' => 'http://localhost:81/login/web/index.php?r=site/index',
	    ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=127.0.0.1;port=3307;',
            'username' => 'root',
            'password' => 'sa',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    ],
];

$main['components']['db']['dsn'] =  $main['components']['db']['dsn'].(isset($_COOKIE['_outlet'])?'dbname='.base64_decode($_COOKIE['_outlet']).';':'');
return $main;