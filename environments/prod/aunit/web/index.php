<?php
$server_name = $_SERVER['SERVER_NAME'];
if ( ! in_array( $_SERVER['SERVER_PORT'], [ 80, 443 ] ) ) {
	$port = ":$_SERVER[SERVER_PORT]";
} else {
	$port = '';
}
if ( ! empty( $_SERVER['HTTPS'] ) && ( strtolower( $_SERVER['HTTPS'] ) == 'on' || $_SERVER['HTTPS'] == '1' ) ) {
	$scheme = 'https';
} else {
	$scheme = 'http';
}
if ( isset( $_COOKIE['_outlet'] )  && isset( $_COOKIE['PHPSESSID'] )) {
	switch ( strlen( base64_decode( $_COOKIE['_outlet'] ) ) ) {
		case 4 :
			header( "Location: " . $scheme . '://' . $server_name . $port . '/abengkel/web/index.php?r=site/index' );
			die();
			break;
		case 3 :
			break;
		default :
			header( "Location: " . $scheme . '://' . $server_name . $port . '/afinance/web/index.php?r=site/index' );
			die();
			break;
	}
} else {
	session_start();
	session_destroy();
	setcookie( 'PHPSESSID', '', time() - 1000 ,'/');
	header( "Location: " . $scheme . '://' . $server_name . $port . '/login/web/index.php?r=site/index' );
	die();
}
defined('YII_DEBUG') or define('YII_DEBUG', false);
defined('YII_ENV') or define('YII_ENV', 'prod');
define('YII_ENABLE_ERROR_HANDLER', true);
define('YII_ENABLE_EXCEPTION_HANDLER', true);
error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT);
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/../../common/config/bootstrap.php';
require __DIR__ . '/../config/bootstrap.php';

$config = yii\helpers\ArrayHelper::merge(
    require __DIR__ . '/../../common/config/main.php',
    require __DIR__ . '/../../common/config/main-local.php',
    require __DIR__ . '/../config/main.php',
    require __DIR__ . '/../config/main-local.php'
);

(new yii\web\Application($config))->run();
