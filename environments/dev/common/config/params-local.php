<?php
return [
	'host_report' => 'http://localhost:8080/',
	'db_exclude'=>[
		'information_schema',
		'mysql',
		'performance_schema',
		'HCONSOL',
		'HFinance',
		'h_dashboard',
		'hmds-core',
		'hmds_log',
		'hvys_log',
		'kdashboard',
		'mailbot',
		'sampledb',
		'test',
		'updatehsys',
		'web-report'
	]
];
