<?php
return [
    'adminEmail' => 'admin@example.com',
    'LoV'=> [
        'HSO' => [
            'sumberProspect' => [
                '0001' => 'Pameran',
                '0002' => 'Showroom Event',
                '0003' => 'Roadshow',
                '0004' => 'Walk in',
                '0005' => 'Customer RO H1',
                '0006' => 'Customer RO H23',
                '0007' => 'Website',
                '0008' => 'Social media',
                '0009' => 'External parties',
                '0010' => 'Mobile Apps MD/Dealer',
                '0011' => 'Refferal',
                '0012' => 'Contact Center',
                '9999' => 'Others'
            ],
            'metodeFollowUp'=> [
                '1' => 'SMS (WA/Line)',
                '2' => 'Call',
                '3' => 'Visit',
                '4' =>'Direct Touch'
            ],
            'testRidePreference' => [
                '2' => 'No',
                '1' => 'Yes',
                '0' => 'No'
            ],
            'statusFollowUpProspecting'=>[
                '1' => 'Planned',
                '2' => 'Done'
            ],
            'statusProspect'=>[
                '1' => 'Low',
                '2' => 'Medium',
                '3' => 'Hot',
                '4' => 'Deal',
                '5' => 'Not Deal'
            ],
            'statusSPK'=>[
                '1' => 'Open',
                '2' => 'Indent',
                '3' => 'Complete',
                '4'=> 'Cancelled'
            ],
            'tipePembayaran'=>[
                '1' => 'Cash',
                '2' => 'Credit'
            ],
            'statusFakturSTNK'=>[
                '1' => 'Mohon Faktur',
                '2' => 'Sudah diserahkan ke Biro Jasa',
                '3' => 'STNK selesai',
                '4' => 'STNK diserahkan ke konsumen',
                '5' => 'BPKB selesai',
                '6' => 'BPKB diserahkan ke konsumen',
                '7' => 'Plat Nopol selesai',
                '8' => 'Plat Nopol diserahkan ke konsumen'
            ],
            'jenisIdPenerimaBPKB'=>[
                '1' => 'KTP',
                '2' => 'TDP',
                '3' => 'Kitas',
                '4' => 'Passport'
            ],
            'jenisIdPenerimaSTNK'=>[
                '1' => 'KTP',
                '2' => 'TDP',
                '3' => 'Kitas',
                '4' => 'Passport'
            ],
            'statusShippingList'=>[
                '1' => 'Sudah diterima',
                '0' => 'Belum diterima'
            ],
            'statusRFS'=>[
                '1' => 'RFS',
                '0' => 'NRFS'
            ],
            'jenisOrder'=>[
                '1' => 'fix',
                '2' => 'regular',
                '3' => 'hotline',
                '4' => 'urgent'
            ],
            'tipePembayaranInv1'=>[
                '1' => 'Credit',
                '2' => 'Cash'
            ],
            'caraBayar'=>[
                '1' => 'Cash',
                '2' => 'Transfer'
            ],
            'statusInv1'=>[
                '1' => 'New',
                '2' => 'Process',
                '3' => 'Accepted',
                '4' => 'Close',
            ],
            'tipeComingCustomer'=>[
                '1' => 'Milik',
                '2' => 'Bawa',
                '3' => 'Pakai',
            ],
            'hubunganDenganPemilik'=>[
                '1' => 'Orang Tua',
                '2' => 'Anak',
                '3' => 'Saudara',
                '4' => 'Karyawan',
                '5' => 'Teman',
                '6' => 'Suami/Istri',
                '0' => 'Lainnya'
            ],
            'asalUnitEntry'=>[
                'VPS' => 'Pos Service',
                'SVJD' => 'Join Dealer Activity',
                'SVGC' => 'Group Customer',
                'SVPA' => 'Public Area',
                'SVER' => 'Emergency',
                'PE' => 'Pit Express',
                'RM' => 'Reminder',
                'AE01' => 'AHASS Event yang di Develop oleh AHM',
                'AE02' => 'AHASS Event yang di Develop oleh MD',
                'AE03' => 'AHASS Event yang di Develop oleh AHASS',
                'NP' => 'Non – Promotion'
            ],
            'jenisPIT' => [
                '1' => 'Reguler',
                '2' => 'Fast Track',
                '3' => 'Booking',
                '4' => 'PIT Express'
            ],
            'setUpPembayaran' => [
                '1' => 'Cash',
                '2' => 'TOP',
            ],
            'konfirmasiPekerjaanTambahan' => [
                '1' => 'Yes',
                '0' => 'No',
            ],
            'statusWorkOrder' => [
                '1' => 'Start',
                '2' => 'Pause',
                '3' => 'Pending',
                '4' => 'Finish',
                '5' => 'Cancel',
            ]
        ]
    ]
];
