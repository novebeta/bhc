<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "vmotorlastlokasi".
 *
 * @property int $MotorAutoN
 * @property string $MotorNoMesin
 * @property string $LokasiKode
 * @property string $NoTrans
 * @property string $Jam
 * @property string $Kondisi
 */
class Vmotorlastlokasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vmotorlastlokasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['MotorAutoN', 'MotorNoMesin', 'NoTrans', 'Jam', 'Kondisi'], 'required'],
            [['MotorAutoN'], 'integer'],
            [['Jam'], 'safe'],
            [['MotorNoMesin'], 'string', 'max' => 25],
            [['LokasiKode'], 'string', 'max' => 15],
            [['NoTrans'], 'string', 'max' => 18],
            [['Kondisi'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'MotorAutoN' => 'Motor Auto N',
            'MotorNoMesin' => 'Motor No Mesin',
            'LokasiKode' => 'Lokasi Kode',
            'NoTrans' => 'No Trans',
            'Jam' => 'Jam',
            'Kondisi' => 'Kondisi',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VmotorlastlokasiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VmotorlastlokasiQuery(get_called_class());
    }
}
