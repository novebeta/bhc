<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "tdleasing".
 *
 * @property string $LeaseKode
 * @property string $LeaseNama
 * @property string $LeaseContact
 * @property string $LeaseAlamat
 * @property string $LeaseKota
 * @property string $LeaseTelepon
 * @property string $LeaseFax
 * @property string $LeaseEmail
 * @property string $LeaseKeterangan
 * @property string $LeaseStatus
 * @property string $LeaseNo
 * @property string $LeaseMO
 */
class Tdleasing extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdleasing';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['LeaseKode'], 'required'],
            [['LeaseNo','LeaseMO'], 'number'],
            [['LeaseKode'], 'string', 'max' => 10],
            [['LeaseNama', 'LeaseContact', 'LeaseKota', 'LeaseFax'], 'string', 'max' => 50],
            [['LeaseAlamat', 'LeaseTelepon'], 'string', 'max' => 75],
            [['LeaseEmail'], 'string', 'max' => 25],
            [['LeaseKeterangan'], 'string', 'max' => 100],
            [['LeaseStatus'], 'string', 'max' => 1],
            [['LeaseKode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
	        'LeaseKode' => 'Kode',
	        'LeaseNama' => 'Nama',
	        'LeaseContact' => 'Kontak',
	        'LeaseAlamat' => 'Alamat',
	        'LeaseKota' => 'Kota',
	        'LeaseTelepon' => 'Telepon',
	        'LeaseFax' => 'Fax',
	        'LeaseEmail' => 'Email',
	        'LeaseKeterangan' => 'Keterangan',
            'LeaseStatus' => 'Status',
            'LeaseNo' => 'Lease No',
            'LeaseMO' => 'Lease Mo',
        ];
    }

	public static function colGrid()
	{
		return [
            'LeaseKode' => ['label' => 'Kode','width' => 70,'name' => 'LeaseKode'],
            'LeaseNama' => ['label' => 'Nama','width' => 265,'name' => 'LeaseNama'],
            'LeaseAlamat' => ['label' => 'Alamat','width' => 170,'name' => 'LeaseAlamat'],
            'LeaseKota' => ['label' => 'Kota','width' => 80,'name' => 'LeaseKota'],
            'LeaseContact' => ['label' => 'Kontak','width' => 100,'name' => 'LeaseContact'],
            'LeaseTelepon' => ['label' => 'Telepon','width' => 90,'name' => 'LeaseTelepon'],
            'LeaseEmail' => ['label' => 'Email','width' => 100,'name' => 'LeaseEmail'],
            'LeaseStatus' => ['label' => 'Status','width' => 55,'name' => 'LeaseStatus'],
            'LeaseMO' => ['label' => 'MO','width' => 55,'name' => 'LeaseMo'],
		];
	}

    /**
     * {@inheritdoc}
     * @return TdleasingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdleasingQuery(get_called_class());
    }
}
