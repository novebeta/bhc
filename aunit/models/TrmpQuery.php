<?php

namespace aunit\models;

use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;

/**
 * This is the ActiveQuery class for [[Ttmphd]].
 *
 * @see Trmp
 */
class TrmpQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Trmp[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Trmp|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function rmFillByNo() {
        return $this->select( new Expression( "*"));
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]       = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]    = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'LokasiKode' ]   = Menu::getUserLokasi()[ 'LokasiKode' ];
        $post[ 'MPNo' ]         = $post[ 'MPNo' ] ?? '--';
        $post[ 'MPNoBaru' ]     = $post[ 'MPNoView' ] ?? '--';
        $post[ 'MPTgl' ]        = date('Y-m-d', strtotime($post[ 'MPTgl' ] ?? date( 'Y-m-d' )));
        $post[ 'MPMemo' ]         = $post[ 'MPMemo' ] ?? '--';
        $post[ 'MPSaldoAwal' ]      = floatval( $post[ 'MPSaldoAwal' ] ?? 0 );
        $post[ 'MPUmur' ]      = floatval( $post[ 'MPUmur' ] ?? 0 );
        $post[ 'MPAwal' ]      = floatval( $post[ 'MPAwal' ] ?? 0 );
        $post[ 'MPNext' ]      = floatval( $post[ 'MPNext' ] ?? 0 );
        $post[ 'MPCOADebet' ]   = $post[ 'MPCOADebet' ] ?? '--';
        $post[ 'MPCOAKredit' ]   = $post[ 'MPCOAKredit' ] ?? '--';
        $post[ 'MPTglMulai' ]        = date('Y-m-d', strtotime($post[ 'MPTglMulai' ] ?? date( 'Y-m-d' )));
        $post[ 'MPSaldoNow' ]      = floatval( $post[ 'MPSaldoNow' ] ?? 0 );
        $post[ 'MPStatus' ] = $post[ 'MPStatus' ] ?? '--';
        try {
        General::cCmd( "SET @MPNo = ?;" )->bindParam( 1, $post[ 'MPNo'] )->execute();
        General::cCmd( "CALL frmp(?,@Status,@Keterangan,?,?,?,@MPNo,?,?,?,?,?,?,?,?,?,?,?,?);" )
            ->bindParam( 1, $post[ 'Action' ] )
            ->bindParam( 2, $post[ 'UserID' ] )
            ->bindParam( 3, $post[ 'KodeAkses' ] )
            ->bindParam( 4, $post[ 'LokasiKode' ] )
            ->bindParam( 5, $post[ 'MPNoBaru' ] )
            ->bindParam( 6, $post[ 'MPTgl' ] )
            ->bindParam( 7, $post[ 'MPMemo' ] )
            ->bindParam( 8, $post[ 'MPSaldoAwal' ] )
            ->bindParam( 9, $post[ 'MPUmur' ] )
            ->bindParam( 10, $post[ 'MPAwal' ] )
            ->bindParam( 11, $post[ 'MPNext' ] )
            ->bindParam( 12, $post[ 'MPCOADebet' ] )
            ->bindParam( 13, $post[ 'MPCOAKredit' ] )
            ->bindParam( 14, $post[ 'MPTglMulai' ] )
            ->bindParam( 15, $post[ 'MPSaldoNow' ] )
            ->bindParam( 16, $post[ 'MPStatus' ] )
            ->execute();
        $exe = General::cCmd( "SELECT @MPNo,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'MPNo'       => $post[ 'MPNo' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $MPNo = $exe[ '@MPNo' ] ?? $post['MPNo'];
        $status = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : ['.$post['Action'].'] Keterangan NULL';
        return [
            'MPNo'       => $MPNo,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
