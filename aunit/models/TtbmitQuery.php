<?php

namespace aunit\models;

use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttbmit]].
 *
 * @see Ttbmit
 */
class TtbmitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttbmit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttbmit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
    public function callSP( $post ) {
        $post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'LokasiKode' ] = Menu::getUserLokasi()[ 'LokasiKode' ];
        $post[ 'BMNo' ]       = $post[ 'BMNo' ] ?? '--';
        $post[ 'DKNo' ]       = $post[ 'DKNo' ] ?? '--';
        $post[ 'BMBayar' ]    = floatval( $post[ 'BMBayar' ] ?? 0 );
        $post[ 'BMNoOLD' ]    = $post[ 'BMNoOLD' ] ?? '--';
        $post[ 'DKNoOLD' ]    = $post[ 'DKNoOLD' ] ?? '--';
        try {
            General::cCmd( "CALL fbmit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'LokasiKode' ] )
                ->bindParam( 5, $post[ 'BMNo' ] )
                ->bindParam( 6, $post[ 'DKNo' ] )
                ->bindParam( 7, $post[ 'BMBayar' ] )
                ->bindParam( 8, $post[ 'BMNoOLD' ] )
                ->bindParam( 9, $post[ 'DKNoOLD' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
