<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "tsaldokas".
 *
 * @property string $SaldoKasTgl
 * @property string $LokasiKode
 * @property string $SaldoKasAwal
 * @property string $SaldoKasMasuk
 * @property string $SaldoKasKeluar
 * @property string $SaldoKasSaldo
 */
class Tsaldoka extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tsaldokas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SaldoKasTgl', 'LokasiKode'], 'required'],
            [['SaldoKasTgl'], 'safe'],
            [['SaldoKasAwal', 'SaldoKasMasuk', 'SaldoKasKeluar', 'SaldoKasSaldo'], 'number'],
            [['LokasiKode'], 'string', 'max' => 15],
            [['SaldoKasTgl', 'LokasiKode'], 'unique', 'targetAttribute' => ['SaldoKasTgl', 'LokasiKode']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SaldoKasTgl' => 'Saldo Kas Tgl',
            'LokasiKode' => 'Lokasi Kode',
            'SaldoKasAwal' => 'Saldo Kas Awal',
            'SaldoKasMasuk' => 'Saldo Kas Masuk',
            'SaldoKasKeluar' => 'Saldo Kas Keluar',
            'SaldoKasSaldo' => 'Saldo Kas Saldo',
        ];
    }

	public static function colGrid()
	{
		return [
			'SaldoKasTgl' => 'Saldo Kas Tgl',
			'LokasiKode' => 'Lokasi Kode',
			'SaldoKasAwal' => 'Saldo Kas Awal',
			'SaldoKasMasuk' => 'Saldo Kas Masuk',
			'SaldoKasKeluar' => 'Saldo Kas Keluar',
			'SaldoKasSaldo' => 'Saldo Kas Saldo',
		];
	}

    /**
     * {@inheritdoc}
     * @return TsaldokaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TsaldokaQuery(get_called_class());
    }
}
