<?php

namespace aunit\models;
use yii\helpers\ArrayHelper;

/**
 * This is the ActiveQuery class for [[Tdcustomer]].
 *
 * @see Tdcustomer
 */
class TdcustomerQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdcustomer[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdcustomer|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
	public function combo($where = null) {
		$q =  $this->select( [ "CONCAT(CusKode,' - ',CusNama) as label", "CusKode as value" ] )
		            ->orderBy( 'CusKode' );
		if ( $where != null ) {
			$q->where( 'CusTglBeli BETWEEN :from AND :to', [
				':from' => $where[ 'from' ],
				':to'   => $where[ 'to' ],
			] );
		}
		return $q->asArray()
		         ->all();
	}
    public function ktp() {
        return ArrayHelper::map( $this->select( [ "CONCAT(CusKTP) as label", "CusKTP as value" ] )
            ->orderBy( 'CusKTP' )
            ->asArray()
            ->all(), 'value', 'label' );
    }
}
