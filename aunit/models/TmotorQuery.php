<?php
namespace aunit\models;
use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
/**
 * This is the ActiveQuery class for [[Tmotor]].
 *
 * @see Tmotor
 */
class TmotorQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Tmotor|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function combo( $params = [] ) {
		$field = isset( $params[ 'field' ] ) ? $params[ 'field' ] : 'MotorNoMesin';
		$this->select( "$field as label, $field as value" )
		     ->groupBy( $field )
		     ->orderBy( $field );
		if ( isset( $params[ 'MotorType' ] ) ) {
			$this->where( 'MotorType = :MotorType', [ ':MotorType' => $params[ 'MotorType' ] ] );
		}
		return $this->asArray()
		            ->all();
	}
	/**
	 * {@inheritdoc}
	 * @return Tmotor[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	public function nomesin() {
		return ArrayHelper::map( $this->select( [ "CONCAT(MotorNoMesin) as label", "MotorNoMesin as value" ] )
		                              ->orderBy( 'MotorNoMesin' )
		                              ->asArray()
		                              ->all(), 'value', 'label' );
	}
	public function norangka() {
		return ArrayHelper::map( $this->select( [ "CONCAT(MotorNoRangka) as label", "MotorNoRangka as value" ] )
		                              ->orderBy( 'MotorNoRangka' )
		                              ->asArray()
		                              ->all(), 'value', 'label' );
	}
	public function select2( $value, $label = [ 'MotorType' ], $orderBy = [], $where = [ 'condition' => null, 'params' => [] ], $allOption = false ) {
		$option =
			$this->select( [ "CONCAT(" . implode( ",'||',", $label ) . ") as label", "$value as value" ] )
			     ->orderBy( $orderBy )
			     ->where( $where[ 'condition' ], $where[ 'params' ] )
			     ->asArray()
			     ->all();
		if ( $allOption ) {
			return array_merge( [ [ 'value' => '%', 'label' => 'Semua' ] ], $option );
		} else {
			return $option;
		}
	}
	public function dsTJualFillByNo() {
		return $this->select( new Expression( "tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorMemo, tmotor.DealerKode, tmotor.SSNo, tmotor.FBNo, tmotor.FBHarga, tmotor.DKNo, 
                         tmotor.SKNo, tmotor.PDNo, tmotor.SDNo, tmotor.BPKBNo, tmotor.BPKBTglJadi, tmotor.BPKBTglAmbil, tmotor.STNKNo, tmotor.STNKTglMasuk, tmotor.STNKTglBayar, tmotor.STNKTglJadi, tmotor.STNKTglAmbil, 
                         tmotor.STNKNama, tmotor.STNKAlamat, tmotor.PlatNo, tmotor.PlatTgl, tmotor.PlatTglAmbil, tmotor.FakturAHMNo, tmotor.FakturAHMTgl, tmotor.FakturAHMTglAmbil, tmotor.RKNo, tmotor.FakturAHMNilai, tmotor.SKHarga, 
                         tmotor.SDHarga, tmotor.MMHarga, tmotor.RKHarga, tmotor.NoticeNo, tmotor.NoticeTglJadi, tmotor.NoticeTglAmbil, tmotor.NoticePenerima, tmotor.STNKPenerima, tmotor.BPKBPenerima, tmotor.PlatPenerima, tmotor.SSHarga, 
                         ttdk.DKTgl, tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, tdcustomer.CusTelepon, ttdk.DKHarga, 
                         ttdk.LeaseKode, tdsales.SalesNama, tdcustomer.CusKodeKons, tmotor.SRUTNo, tmotor.SRUTTglJadi, tmotor.SRUTTglAmbil, tmotor.SRUTPenerima, tdcustomer.CusTelepon2, tmotor.NoticeNilai, tmotor.NoticeDenda, ttdk.ZidSpk, ttdk.ZidProspect,tmotor.NoticeTglPrint" ) )
		            ->join( "LEFT JOIN", "ttdk", "tmotor.DKNo = ttdk.DKNo" )
		            ->join( "LEFT JOIN", "tdcustomer", "ttdk.CusKode = tdcustomer.CusKode" )
		            ->join( "LEFT JOIN", "tdsales", "ttdk.SalesKode = tdsales.SalesKode" );
	}
	public function callSP_SD( $post ) {
		$post[ 'UserID' ]          = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]       = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]      = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'SDNo' ]            = $post[ 'SDNo' ] ?? '--';
		$post[ 'MotorType' ]       = $post[ 'MotorType' ] ?? '--';
		$post[ 'MotorTahun' ]      = $post[ 'MotorTahun' ] ?? '--';
		$post[ 'MotorWarna' ]      = $post[ 'MotorWarna' ] ?? '--';
		$post[ 'MotorNoMesin' ]    = strtoupper( $post[ 'MotorNoMesin' ] ?? '--' );
		$post[ 'MotorNoRangka' ]   = strtoupper( $post[ 'MotorNoRangka' ] ?? '--' );
		$post[ 'SDHarga' ]         = $post[ 'SDHarga' ] ?? 0;
		$post[ 'MotorAutoN' ]      = $post[ 'MotorAutoN' ] ?? 0;
		$post[ 'MotorAutoNOLD' ]   = $post[ 'MotorAutoNOLD' ] ?? 0;
		$post[ 'MotorNoMesinOLD' ] = $post[ 'MotorNoMesinOLD' ] ?? '--';
		try {
			General::cCmd( "CALL fsdmotor(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'SDNo' ] )
			       ->bindParam( 6, $post[ 'MotorType' ] )
			       ->bindParam( 7, $post[ 'MotorTahun' ] )
			       ->bindParam( 8, $post[ 'MotorWarna' ] )
			       ->bindParam( 9, $post[ 'MotorNoMesin' ] )
			       ->bindParam( 10, $post[ 'MotorNoRangka' ] )
			       ->bindParam( 11, $post[ 'SDHarga' ] )
			       ->bindParam( 12, $post[ 'MotorAutoN' ] )
			       ->bindParam( 13, $post[ 'MotorAutoNOLD' ] )
			       ->bindParam( 14, $post[ 'MotorNoMesinOLD' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
	public function callSP_SS( $post ) {
		$post[ 'UserID' ]          = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]       = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]      = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'SSNo' ]            = $post[ 'SSNo' ] ?? '--';
		$post[ 'MotorType' ]       = $post[ 'MotorType' ] ?? '--';
		$post[ 'MotorTahun' ]      = $post[ 'MotorTahun' ] ?? 1900;
		$post[ 'MotorWarna' ]      = $post[ 'MotorWarna' ] ?? '--';
		$post[ 'MotorNoMesin' ]    = strtoupper( $post[ 'MotorNoMesin' ] ?? '--' );
		$post[ 'MotorNoRangka' ]   = strtoupper( $post[ 'MotorNoRangka' ] ?? '--' );
		$post[ 'FBNo' ]            = $post[ 'FBNo' ] ?? '--';
		$post[ 'SSHarga' ]         = $post[ 'SSHarga' ] ?? 0;
		$post[ 'MotorAutoN' ]      = $post[ 'MotorAutoN' ] ?? 0;
		$post[ 'MotorAutoNOLD' ]   = $post[ 'MotorAutoNOLD' ] ?? 0;
		$post[ 'MotorNoMesinOLD' ] = $post[ 'MotorNoMesinOLD' ] ?? '--';
		try {
			General::cCmd( "CALL fssmotor(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'SSNo' ] )
			       ->bindParam( 6, $post[ 'MotorType' ] )
			       ->bindParam( 7, $post[ 'MotorTahun' ] )
			       ->bindParam( 8, $post[ 'MotorWarna' ] )
			       ->bindParam( 9, $post[ 'MotorNoMesin' ] )
			       ->bindParam( 10, $post[ 'MotorNoRangka' ] )
			       ->bindParam( 11, $post[ 'FBNo' ] )
			       ->bindParam( 12, $post[ 'SSHarga' ] )
			       ->bindParam( 13, $post[ 'MotorAutoN' ] )
			       ->bindParam( 14, $post[ 'MotorAutoNOLD' ] )
			       ->bindParam( 15, $post[ 'MotorNoMesinOLD' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
	public function callSP_FB( $post ) {
		$post[ 'UserID' ]          = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]       = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]      = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'FBNo' ]            = $post[ 'FBNo' ] ?? '--';
		$post[ 'MotorType' ]       = $post[ 'MotorType' ] ?? '--';
		$post[ 'MotorTahun' ]      = $post[ 'MotorTahun' ] ?? '--';
		$post[ 'MotorWarna' ]      = $post[ 'MotorWarna' ] ?? '--';
		$post[ 'MotorNoMesin' ]    = strtoupper( $post[ 'MotorNoMesin' ] ?? '--' );
		$post[ 'MotorNoRangka' ]   = strtoupper( $post[ 'MotorNoRangka' ] ?? '--' );
		$post[ 'SSNo' ]            = $post[ 'SSNo' ] ?? '--';
		$post[ 'FBHarga' ]         = $post[ 'FBHarga' ] ?? 0;
		$post[ 'MotorAutoN' ]      = $post[ 'MotorAutoN' ] ?? 0;
		$post[ 'MotorAutoNOLD' ]   = $post[ 'MotorAutoNOLD' ] ?? 0;
		$post[ 'MotorNoMesinOLD' ] = $post[ 'MotorNoMesinOLD' ] ?? '--';
		try {
			General::cCmd( "CALL ffbmotor(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'FBNo' ] )
			       ->bindParam( 6, $post[ 'MotorType' ] )
			       ->bindParam( 7, $post[ 'MotorTahun' ] )
			       ->bindParam( 8, $post[ 'MotorWarna' ] )
			       ->bindParam( 9, $post[ 'MotorNoMesin' ] )
			       ->bindParam( 10, $post[ 'MotorNoRangka' ] )
			       ->bindParam( 11, $post[ 'SSNo' ] )
			       ->bindParam( 12, $post[ 'FBHarga' ] )
			       ->bindParam( 13, $post[ 'MotorAutoN' ] )
			       ->bindParam( 14, $post[ 'MotorAutoNOLD' ] )
			       ->bindParam( 15, $post[ 'MotorNoMesinOLD' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
//		$status = 0;
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
	public function callSP_PD( $post ) {
		$post[ 'UserID' ]          = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]       = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]      = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'PDNo' ]            = $post[ 'PDNo' ] ?? '--';
		$post[ 'MotorType' ]       = $post[ 'MotorType' ] ?? '--';
		$post[ 'MotorTahun' ]      = $post[ 'MotorTahun' ] ?? '--';
		$post[ 'MotorWarna' ]      = $post[ 'MotorWarna' ] ?? '--';
		$post[ 'MotorNoMesin' ]    = strtoupper( $post[ 'MotorNoMesin' ] ?? '--' );
		$post[ 'MotorNoRangka' ]   = strtoupper( $post[ 'MotorNoRangka' ] ?? '--' );
		$post[ 'SSHarga' ]         = $post[ 'SSHarga' ] ?? 0;
		$post[ 'MotorAutoN' ]      = $post[ 'MotorAutoN' ] ?? 0;
		$post[ 'MotorAutoNOLD' ]   = $post[ 'MotorAutoNOLD' ] ?? 0;
		$post[ 'MotorNoMesinOLD' ] = $post[ 'MotorNoMesinOLD' ] ?? '--';
		try {
			General::cCmd( "CALL fpdmotor(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'PDNo' ] )
			       ->bindParam( 6, $post[ 'MotorType' ] )
			       ->bindParam( 7, $post[ 'MotorTahun' ] )
			       ->bindParam( 8, $post[ 'MotorWarna' ] )
			       ->bindParam( 9, $post[ 'MotorNoMesin' ] )
			       ->bindParam( 10, $post[ 'MotorNoRangka' ] )
			       ->bindParam( 11, $post[ 'SSHarga' ] )
			       ->bindParam( 12, $post[ 'MotorAutoN' ] )
			       ->bindParam( 13, $post[ 'MotorAutoNOLD' ] )
			       ->bindParam( 14, $post[ 'MotorNoMesinOLD' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
}
