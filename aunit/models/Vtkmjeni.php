<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "vtkmjenis".
 *
 * @property string $KMNo
 * @property string $KMTgl
 * @property string $KasGroup
 * @property string $KMJenis
 * @property string $KMPerson
 * @property string $MotorType
 * @property string $KMNominal
 * @property string $LeaseKode
 * @property string $KMMemo
 * @property string $NoGL
 */
class Vtkmjeni extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vtkmjenis';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KMTgl'], 'safe'],
            [['KMNominal'], 'number'],
            [['KMNo'], 'string', 'max' => 12],
            [['KasGroup'], 'string', 'max' => 14],
            [['KMJenis'], 'string', 'max' => 20],
            [['KMPerson'], 'string', 'max' => 50],
            [['MotorType'], 'string', 'max' => 150],
            [['LeaseKode', 'NoGL'], 'string', 'max' => 10],
            [['KMMemo'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'KMNo' => 'Km No',
            'KMTgl' => 'Km Tgl',
            'KasGroup' => 'Kas Group',
            'KMJenis' => 'Km Jenis',
            'KMPerson' => 'Km Person',
            'MotorType' => 'Motor Type',
            'KMNominal' => 'Km Nominal',
            'LeaseKode' => 'Lease Kode',
            'KMMemo' => 'Km Memo',
            'NoGL' => 'No Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VtkmjeniQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VtkmjeniQuery(get_called_class());
    }
}
