<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "generalledgernobalance".
 *
 * @property string $NoGL
 */
class Generalledgernobalance extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'generalledgernobalance';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NoGL'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'NoGL' => 'No Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return GeneralledgernobalanceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GeneralledgernobalanceQuery(get_called_class());
    }
}
