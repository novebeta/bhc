<?php

namespace aunit\models;

use yii\helpers\ArrayHelper;
/**
 * This is the ActiveQuery class for [[Tuakses]].
 *
 * @see Tuakses
 */
class TuaksesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tuakses[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tuakses|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	public function combo() {
		return $this->select( [ "KodeAkses as label", "KodeAkses as value" ] )
		            ->orderBy( 'KodeAkses' )
		            ->asArray()
		            ->all();
	}

    public function combodata() {
        return $this->select('KodeAkses as label,KodeAkses as value' )
            ->groupBy( 'KodeAkses' )
            ->orderBy( 'KodeAkses' )
            ->asArray()
            ->all();
    }

	public function select2() {
		return ArrayHelper::map( self::combo(), 'value', 'label' );
	}
}
