<?php

namespace aunit\models;
/**
 * This is the model class for table "ttpihd".
 *
 * @property string $PINo
 * @property string $PITgl
 * @property string $PIJenis
 * @property string $PSNo
 * @property string $KodeTrans
 * @property string $PosKode
 * @property string $SupKode
 * @property string $LokasiKode
 * @property string $PINoRef
 * @property string $PITerm
 * @property string $PITglTempo
 * @property string $PISubTotal
 * @property string $PIDiscFinal
 * @property string $PITotalPajak
 * @property string $PIBiayaKirim
 * @property string $PITotal
 * @property string $PITerbayar
 * @property string $PIKeterangan
 * @property string $NoGL
 * @property string $Cetak
 * @property string $UserID
 */
class Ttpihd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'twpihd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PINo'], 'required'],
            [['PITgl', 'PITglTempo'], 'safe'],
            [['PITerm', 'PISubTotal', 'PIDiscFinal', 'PITotalPajak', 'PIBiayaKirim', 'PITotal', 'PITerbayar'], 'number'],
            [['PINo', 'PSNo', 'NoGL'], 'string', 'max' => 10],
            [['PIJenis'], 'string', 'max' => 6],
            [['KodeTrans'], 'string', 'max' => 4],
            [['PosKode'], 'string', 'max' => 20],
            [['SupKode', 'LokasiKode', 'UserID'], 'string', 'max' => 15],
            [['PINoRef'], 'string', 'max' => 35],
            [['PIKeterangan'], 'string', 'max' => 150],
            [['Cetak'], 'string', 'max' => 5],
            [['PINo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PINo'         => 'Pi No',
            'PITgl'        => 'Pi Tgl',
            'PIJenis'      => 'Pi Jenis',
            'PSNo'         => 'Ps No',
            'KodeTrans'    => 'Kode Trans',
            'PosKode'      => 'Pos Kode',
            'SupKode'      => 'Sup Kode',
            'LokasiKode'   => 'Lokasi Kode',
            'PINoRef'      => 'Pi No Ref',
            'PITerm'       => 'Pi Term',
            'PITglTempo'   => 'Pi Tgl Tempo',
            'PISubTotal'   => 'Pi Sub Total',
            'PIDiscFinal'  => 'Pi Disc Final',
            'PITotalPajak' => 'Pi Total Pajak',
            'PIBiayaKirim' => 'Pi Biaya Kirim',
            'PITotal'      => 'Pi Total',
            'PITerbayar'   => 'Pi Terbayar',
            'PIKeterangan' => 'Pi Keterangan',
            'NoGL'         => 'No Gl',
            'Cetak'        => 'Cetak',
            'UserID'       => 'User ID',
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
		$col  = [
		    'PINo'         => ['width' => 85,'label' => 'No PI','name'  => 'PINo'],
            'PITgl'        => ['width' => 115,'label' => 'Tgl PI','name'  => 'PITgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y H:i:s']],
            'PIJenis'      => ['width' => 50,'label' => 'Jenis','name'  => 'PIJenis'],
            'PSNo'         => ['width' => 85,'label' => 'No PS','name'  => 'PSNo'],
            'KodeTrans'    => ['width' => 60,'label' => 'TC','name'  => 'KodeTrans'],
            'SupKode'      => ['width' => 70,'label' => 'Supplier','name'  => 'SupKode'],
            'LokasiKode'   => ['width' => 85,'label' => 'Lokasi','name'  => 'LokasiKode'],
            'PINoRef'      => ['width' => 105,'label' => 'No REf','name'  => 'PINoRef'],
            'PITotal'      => ['width' => 100,'label' => 'Total','name'  => 'PITotal','formatter' => 'number', 'align' => 'right'],
            'PITerbayar'   => ['width' => 100,'label' => 'Terbayar','name'  => 'PITerbayar','formatter' => 'number', 'align' => 'right'],
		];
		$col['(PITotal-PITerbayar) AS PIStatus'] = ['label' => 'Sisa','width' => 80,'name'  => 'PIStatus','formatter' => 'number', 'align' => 'right'];
		$col['NoGL'] = ['label' => 'NoGL','width' => 70,'name'  => 'NoGL'];
		$col['Cetak'] = ['label' => 'Cetak','width' => 50,'name'  => 'Cetak'];
		$col['UserID'] = ['label' => 'UserID','width' => 80,'name'  => 'UserID'];
		$col['PIKeterangan'] = ['label' => 'Keterangan','width' => 300,'name'  => 'PIKeterangan'];
		$col['PosKode'] = ['label' => 'Pos','width' => 90,'name'  => 'PosKode'];
		return $col;
    }

    /**
     * {@inheritdoc}
     * @return TtpihdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtpihdQuery(get_called_class());
    }
}
