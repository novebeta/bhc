<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Tuser]].
 *
 * @see Tuser
 */
class TuserQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tuser[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tuser|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	public function combo() {
		return $this->select( [ "CONCAT(UserID,' - ',UserNama) as label", "UserID as value" ] )
		            ->orderBy( 'UserID' )
		            ->asArray()
		            ->all();
	}
}
