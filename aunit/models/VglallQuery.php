<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Vglall]].
 *
 * @see Vglall
 */
class VglallQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Vglall[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Vglall|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
