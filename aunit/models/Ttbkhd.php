<?php
namespace aunit\models;
/**
 * This is the model class for table "ttbkhd".
 *
 * @property string $BKNo
 * @property string $BKTgl
 * @property string $BKMemo
 * @property string $BKNominal
 * @property string $BKJenis Beli, BBN, Umum
 * @property string $SupKode
 * @property string $BKCekNo
 * @property string $BKCekTempo
 * @property string $BKPerson
 * @property string $BKAC
 * @property string $NoAccount
 * @property string $UserID
 * @property string $BKJam
 * @property string $NoGL
 * @property string $BKDoc
 */
class Ttbkhd extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttbkhd';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'BKNo' ], 'required' ],
			[ [ 'BKTgl', 'BKCekTempo', 'BKJam' ], 'safe' ],
			[ [ 'BKNominal', 'BKDoc' ], 'number' ],
			[ [ 'BKNo' ], 'string', 'max' => 12 ],
			[ [ 'BKMemo' ], 'string', 'max' => 100 ],
			[ [ 'BKJenis' ], 'string', 'max' => 20 ],
			[ [ 'SupKode', 'NoAccount', 'NoGL' ], 'string', 'max' => 10 ],
			[ [ 'BKCekNo', 'BKAC' ], 'string', 'max' => 25 ],
			[ [ 'BKPerson' ], 'string', 'max' => 50 ],
			[ [ 'UserID' ], 'string', 'max' => 15 ],
			[ [ 'BKNo' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'BKNo'       => 'Bk No',
			'BKTgl'      => 'Bk Tgl',
			'BKMemo'     => 'Bk Memo',
			'BKNominal'  => 'Bk Nominal',
			'BKJenis'    => 'Beli, BBN, Umum',
			'SupKode'    => 'Sup Kode',
			'BKCekNo'    => 'Bk Cek No',
			'BKCekTempo' => 'Bk Cek Tempo',
			'BKPerson'   => 'Bk Person',
			'BKAC'       => 'Bkac',
			'NoAccount'  => 'No Account',
			'UserID'     => 'User ID',
			'BKJam'      => 'Bk Jam',
			'NoGL'       => 'No Gl',
			'BKDoc'      => 'BKDoc',
		];
	}
	//BKCekTempo, 
	public static function colGrid() {
		return [
			'ttBKhd.BKNo'              => [ 'width' => 85, 'label' => 'No BK', 'name' => 'BKNo' ],
			'BKTgl'                    => [ 'width' => 75, 'label' => 'Tgl BK', 'name' => 'BKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'BKJam'                    => [ 'width' => 55, 'label' => 'Jam', 'name' => 'BKJam', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'H:i:s' ] ],
			'BKJenis'                  => [ 'width' => 80, 'label' => 'Jenis', 'name' => 'BKJenis' ],
			'ttBKhd.NoAccount'         => [ 'width' => 70, 'label' => 'Kode Bank', 'name' => 'NoAccount' ],
			'traccount.NamaAccount'    => [ 'width' => 225, 'label' => 'Nama Bank', 'name' => 'NamaAccount' ],
			'ttBKhd.SupKode'           => [ 'width' => 75, 'label' => 'Supplier', 'name' => 'SupKode' ],
			'BKPerson'                 => [ 'width' => 172, 'label' => 'Terima Dari', 'name' => 'BKPerson' ],
			'ttbkhd.BKNominal AS Sisa' => [ 'width' => 100, 'label' => 'Nominal', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
			'NoGL'                     => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
			'BKCekNo'                  => [ 'width' => 70, 'label' => 'No Cek', 'name' => 'BKCekNo' ],
			'BKAC'                     => [ 'width' => 70, 'label' => 'AC', 'name' => 'BKAC' ],
			'BKCekTempo'               => [ 'width' => 70, 'label' => 'AC Tempo', 'name' => 'BKCekTempo', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'UserID'                   => [ 'width' => 80, 'label' => 'User', 'name' => 'UserID' ],
			'BKMemo'                   => [ 'width' => 500, 'label' => 'Keterangan', 'name' => 'BKMemo' ],
			'SupNama'                  => [ 'width' => 175, 'label' => 'Nama Sup', 'name' => 'SupNama' ],
		];
	}
	public static function colGridBankTransfer() {
		return [
			'FBNo'          => [ 'width' => 85, 'label' => 'No BT', 'name' => 'FBNo' ],
			'BKTgl'         => [ 'width' => 80, 'label' => 'Tgl BT', 'name' => 'BKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'BKNo'          => [ 'width' => 85, 'label' => 'No BK', 'name' => 'BKNo' ],
			'BMNo'          => [ 'width' => 85, 'label' => 'No BM', 'name' => 'BMNo' ],
			'BKNamaAccount' => [ 'width' => 150, 'label' => 'Bank Keluar', 'name' => 'BKNamaAccount' ],
			'BMNamaAccount' => [ 'width' => 150, 'label' => 'Bank Masuk', 'name' => 'BMNamaAccount' ],
			'BKNominal'     => [ 'width' => 110, 'label' => 'Nominal', 'name' => 'BKNominal', 'formatter' => 'number', 'align' => 'right' ],
			'BKPerson'      => [ 'width' => 225, 'label' => 'Pengirim', 'name' => 'BKPerson' ],
			'BMPerson'      => [ 'width' => 225, 'label' => 'Penerima', 'name' => 'BMPerson' ],
			'BKNoGL'        => [ 'width' => 70, 'label' => 'No GL BK', 'name' => 'BKNoGL' ],
			'BMNoGL'        => [ 'width' => 70, 'label' => 'No GL BM', 'name' => 'BMNoGL' ],
			'UserID'        => [ 'width' => 80, 'label' => 'User', 'name' => 'UserID' ],
		];
	}
	public static function colGridPengajuanBayarBBNBank() {
		return [
			'BKNo'                  => [ 'width' => 85, 'label' => 'No BK', 'name' => 'BKNo' ],
			'BKTgl'                 => [ 'width' => 80, 'label' => 'Tgl BK', 'name' => 'BKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'ttbkhd.NoAccount'      => [ 'width' => 70, 'label' => 'No Account', 'name' => 'NoAccount' ],
			'traccount.NamaAccount' => [ 'width' => 150, 'label' => 'Nama Bank', 'name' => 'NamaAccount' ],
			'BKPerson'              => [ 'width' => 110, 'label' => 'Terima Dari', 'name' => 'BKPerson' ],
			'BKNominal'             => [ 'width' => 100, 'label' => 'Nominal', 'name' => 'BKNominal', 'formatter' => 'number', 'align' => 'right' ],
			'BKMemo'                => [ 'width' => 250, 'label' => 'Keterangan', 'name' => 'BKMemo' ],
			'ttbkhd.SupKode'        => [ 'width' => 100, 'label' => 'Kode Sup', 'name' => 'SupKode' ],
			'SupNama'               => [ 'width' => 175, 'label' => 'Nama Sup', 'name' => 'SupNama' ],
			'BKJenis'               => [ 'width' => 50, 'label' => 'Jenis', 'name' => 'BKJenis' ],
			'BKCekNo'               => [ 'width' => 70, 'label' => 'No Cek', 'name' => 'BKCekNo' ],
			'BKAC'                  => [ 'width' => 70, 'label' => 'AC', 'name' => 'BKAC' ],
			'NoGL'                  => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
			'UserID'                => [ 'width' => 80, 'label' => 'User', 'name' => 'UserID' ],
		];
	}
	public function getAccount() { return $this->hasOne( Traccount::className(), [ 'NoAccount' => 'NoAccount' ] ); }
	public function getSupplier() { return $this->hasOne( Tdsupplier::className(), [ 'SupKode' => 'SupKode' ] ); }
	public static function find() { return new TtbkhdQuery( get_called_class() ); }
	public static function generateBKNo( $BankKode, $temporary = false ) {
		$sign    = $temporary ? '=' : '-';
		$kode    = 'BK';
		$kBank   = substr( $BankKode, - 2 );
		$yNow    = date( "y" );
		$maxNoGl = ( new \yii\db\Query() )
			->from( self::tableName() )
			->where( "BKNo LIKE '$kode$kBank$yNow$sign%'" )
			->max( 'BKNo' );
		if ( $maxNoGl && preg_match( "/$kode\d{4}$sign(\d+)/", $maxNoGl, $result ) ) {
			[ $all, $counter ] = $result;
			$digitCount  = strlen( $counter );
			$val         = (int) $counter;
			$nextCounter = sprintf( '%0' . $digitCount . 'd', ++ $val );
		} else {
			$nextCounter = "00001";
		}
		return "$kode$kBank$yNow$sign$nextCounter";
	}
	public static function getRoute( $BKJenis, $param = [] ) {
		$index  = [];
		$update = [];
		$create = [];
		switch ( $BKJenis ) {
			case 'SubsidiDealer2' :
				$index  = [ 'ttbkhd/bank-keluar-subsidi-dealer-2' ];
				$update = [ 'ttbkhd/bank-keluar-subsidi-dealer-2-update' ];
				$create = [ 'ttbkhd/bank-keluar-subsidi-dealer-2-create' ];
				break;
			case 'Retur Harga' :
				$index  = [ 'ttbkhd/bank-keluar-retur-harga' ];
				$update = [ 'ttbkhd/bank-keluar-retur-harga-update' ];
				$create = [ 'ttbkhd/bank-keluar-retur-harga-create' ];
				break;
			case 'Insentif Sales' :
				$index  = [ 'ttbkhd/bank-keluar-insentif-sales' ];
				$update = [ 'ttbkhd/bank-keluar-insentif-sales-update' ];
				$create = [ 'ttbkhd/bank-keluar-insentif-sales-create' ];
				break;
			case 'Potongan Khusus' :
				$index  = [ 'ttbkhd/bank-keluar-potongan-khusus' ];
				$update = [ 'ttbkhd/bank-keluar-potongan-khusus-update' ];
				$create = [ 'ttbkhd/bank-keluar-potongan-khusus-create' ];
				break;
			case 'Bayar Unit' :
				$index  = [ 'ttbkhd/bank-keluar-bayar-hutang-unit' ];
				$update = [ 'ttbkhd/bank-keluar-bayar-hutang-unit-update' ];
				$create = [ 'ttbkhd/bank-keluar-bayar-hutang-unit-create' ];
				break;
			case 'Bayar BBN' :
				$index  = [ 'ttbkhd/bank-keluar-bayar-hutang-bbn' ];
				$update = [ 'ttbkhd/bank-keluar-bayar-hutang-bbn-update' ];
				$create = [ 'ttbkhd/bank-keluar-bayar-hutang-bbn-create' ];
				break;
			case 'BBN Plus' :
				$index  = [ 'ttbkhd/bank-keluar-bayar-bbn-progresif' ];
				$update = [ 'ttbkhd/bank-keluar-bayar-bbn-progresif-update' ];
				$create = [ 'ttbkhd/bank-keluar-bayar-bbn-progresif-create' ];
				break;
			case 'BK Ke Kas' :
				$index  = [ 'ttbkhd/bank-keluar-kas' ];
				$update = [ 'ttbkhd/bank-keluar-kas-update' ];
				$create = [ 'ttbkhd/bank-keluar-kas-create' ];
				break;
			case 'Umum' :
				$index  = [ 'ttbkhd/bank-keluar-umum' ];
				$update = [ 'ttbkhd/bank-keluar-umum-update' ];
				$create = [ 'ttbkhd/bank-keluar-umum-create' ];
				break;
			case 'Bank Transfer' :
				$index  = [ 'ttbkhd/bank-transfer' ];
				$update = [ 'ttbkhd/bank-transfer-update' ];
				$create = [ 'ttbkhd/bank-transfer-create' ];
				break;
		}
		return [
			'index'  => \yii\helpers\Url::toRoute( array_merge( $index, $param ) ),
			'update' => \yii\helpers\Url::toRoute( array_merge( $update, $param ) ),
			'create' => \yii\helpers\Url::toRoute( array_merge( $create, $param ) ),
		];
	}
    public static function colGridBayarHutangBbn() {
        return [
			'ttBKhd.BKDoc' 	=> [ 'width' => 30, 'label' => 'Doc', 'name' => 'BKDoc' ],
            'ttBKhd.BKNo'              => [ 'width' => 85, 'label' => 'No BK', 'name' => 'BKNo' ],
            'BKTgl'                    => [ 'width' => 75, 'label' => 'Tgl BK', 'name' => 'BKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
            'BKMemo'                   => [ 'width' => 500, 'label' => 'Keterangan', 'name' => 'BKMemo' ],
            'BKPerson'                 => [ 'width' => 172, 'label' => 'Terima Dari', 'name' => 'BKPerson' ],
            'ttbkhd.BKNominal AS Sisa' => [ 'width' => 100, 'label' => 'Nominal', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
            'BKJam'                    => [ 'width' => 55, 'label' => 'Jam', 'name' => 'BKJam', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'H:i:s' ] ],
            'BKJenis'                  => [ 'width' => 80, 'label' => 'Jenis', 'name' => 'BKJenis' ],
            'ttBKhd.NoAccount'         => [ 'width' => 70, 'label' => 'Kode Bank', 'name' => 'NoAccount' ],
            'traccount.NamaAccount'    => [ 'width' => 225, 'label' => 'Nama Bank', 'name' => 'NamaAccount' ],
            'ttBKhd.SupKode'           => [ 'width' => 75, 'label' => 'Supplier', 'name' => 'SupKode' ],
            'NoGL'                     => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
            'BKCekNo'                  => [ 'width' => 70, 'label' => 'No Cek', 'name' => 'BKCekNo' ],
            'BKAC'                     => [ 'width' => 70, 'label' => 'AC', 'name' => 'BKAC' ],
            'BKCekTempo'               => [ 'width' => 70, 'label' => 'AC Tempo', 'name' => 'BKCekTempo', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
            'UserID'                   => [ 'width' => 80, 'label' => 'User', 'name' => 'UserID' ],
            'SupNama'                  => [ 'width' => 175, 'label' => 'Nama Sup', 'name' => 'SupNama' ],
        ];
    }
}
