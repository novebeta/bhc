<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "vtkassaldo".
 *
 * @property string $KasNo
 * @property string $KasTgl
 * @property string $KasJam
 * @property string $KasMemo
 * @property string $KasDebet
 * @property string $KasKredit
 * @property string $KasJenis
 * @property string $KasPerson
 * @property string $KasLink
 * @property string $UserID
 * @property string $LokasiKode
 * @property string $NoGL
 */
class Vtkassaldo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vtkassaldo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KasTgl', 'KasJam'], 'safe'],
            [['KasDebet', 'KasKredit'], 'number'],
            [['KasNo', 'KasLink'], 'string', 'max' => 12],
            [['KasMemo'], 'string', 'max' => 100],
            [['KasJenis'], 'string', 'max' => 20],
            [['KasPerson'], 'string', 'max' => 50],
            [['UserID', 'LokasiKode'], 'string', 'max' => 15],
            [['NoGL'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'KasNo' => 'Kas No',
            'KasTgl' => 'Kas Tgl',
            'KasJam' => 'Kas Jam',
            'KasMemo' => 'Kas Memo',
            'KasDebet' => 'Kas Debet',
            'KasKredit' => 'Kas Kredit',
            'KasJenis' => 'Kas Jenis',
            'KasPerson' => 'Kas Person',
            'KasLink' => 'Kas Link',
            'UserID' => 'User ID',
            'LokasiKode' => 'Lokasi Kode',
            'NoGL' => 'No Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VtkassaldoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VtkassaldoQuery(get_called_class());
    }
}
