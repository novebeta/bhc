<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "ttgeneralledgerit".
 *
 * @property string $NoGL
 * @property string $TglGL
 * @property int $GLAutoN
 * @property string $NoAccount
 * @property string $KeteranganGL
 * @property string $DebetGL
 * @property string $KreditGL
 */
class Ttgeneralledgerit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttgeneralledgerit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NoGL', 'TglGL', 'GLAutoN'], 'required'],
            [['TglGL'], 'safe'],
            [['GLAutoN'], 'integer'],
            [['DebetGL', 'KreditGL'], 'number'],
            [['NoGL', 'NoAccount'], 'string', 'max' => 10],
            [['KeteranganGL'], 'string', 'max' => 150],
            [['NoGL', 'TglGL', 'GLAutoN'], 'unique', 'targetAttribute' => ['NoGL', 'TglGL', 'GLAutoN']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'NoGL' => 'No Gl',
            'TglGL' => 'Tgl Gl',
            'GLAutoN' => 'Gl Auto N',
            'NoAccount' => 'No Account',
            'KeteranganGL' => 'Keterangan Gl',
            'DebetGL' => 'Debet Gl',
            'KreditGL' => 'Kredit Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtgeneralledgeritQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtgeneralledgeritQuery(get_called_class());
    }
}
