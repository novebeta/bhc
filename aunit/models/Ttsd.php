<?php
namespace aunit\models;
/**
 * This is the model class for table "ttsd".
 *
 * @property string $SDNo
 * @property string $SDTgl
 * @property string $LokasiKode
 * @property string $DealerKode
 * @property string $SDMemo
 * @property string $SDTotal
 * @property string $UserID
 * @property string $SDJam
 * @property string $NoGL
 */
class Ttsd extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttsd';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'SDNo' ], 'required' ],
			[ [ 'SDTgl', 'SDJam' ], 'safe' ],
			[ [ 'SDTotal' ], 'number' ],
			[ [ 'SDNo', 'DealerKode', 'NoGL' ], 'string', 'max' => 10 ],
			[ [ 'LokasiKode', 'UserID' ], 'string', 'max' => 15 ],
			[ [ 'SDMemo' ], 'string', 'max' => 100 ],
			[ [ 'SDNo' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'SDNo'       => 'Sd No',
			'SDTgl'      => 'Sd Tgl',
			'LokasiKode' => 'Lokasi Kode',
			'DealerKode' => 'Dealer Kode',
			'SDMemo'     => 'Sd Memo',
			'SDTotal'    => 'Sd Total',
			'UserID'     => 'User ID',
			'SDJam'      => 'Sd Jam',
			'NoGL'       => 'No Gl',
		];
	}
	public static function colGrid() {
		return [
			'SDNo'       => [ 'width' => 95, 'label' => 'No SD', 'name' => 'SDNo' ],
			'SDTgl'      => [ 'width' => 70, 'label' => 'Tgl SD', 'name' => 'SDTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'SDJam'      => [ 'width' => 50, 'label' => 'Jam', 'name' => 'SDJam', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'H:i:s' ] ],
			'LokasiNama' => [ 'width' => 200, 'label' => 'Lokasi Asal', 'name' => 'LokasiNama' ],
			'DealerNama' => [ 'width' => 200, 'label' => 'Dealer Tujuan', 'name' => 'DealerNama' ],
			'SupNama'    => [ 'width' => 200, 'label' => 'Supplier Nama', 'name' => 'SupNama' ],
			'SDTotal'    => [ 'width' => 100, 'label' => 'Total', 'name' => 'SDTotal', 'formatter' => 'number', 'align' => 'right' ],
			'SDMemo'     => [ 'width' => 250, 'label' => 'Keterangan', 'name' => 'SDMemo' ],
			'NoGL'       => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
			'UserID'     => [ 'width' => 70, 'label' => 'User ID', 'name' => 'UserID' ],
		];
	}
	public static function colGridReturBeli() {
		return [
			'ttsd.SDNo'       => [ 'width' => 95, 'label' => 'No SD', 'name' => 'SDNo' ],
			'ttsd.SDTgl'      => [ 'width' => 70, 'label' => 'Tgl SD', 'name' => 'SDTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'ttsd.SDJam'      => [ 'width' => 50, 'label' => 'Jam', 'name' => 'SDJam', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'H:i:s' ] ],
			'ttsd.LokasiKode' => [ 'width' => 100, 'label' => 'Kode Lokasi', 'name' => 'LokasiKode' ],
			'LokasiNama'      => [ 'width' => 175, 'label' => 'Lokasi Asal', 'name' => 'LokasiNama' ],
			'SupNama'         => [ 'width' => 150, 'label' => 'Supplier Nama', 'name' => 'SupNama' ],
			'ttsd.SDTotal'    => [ 'width' => 100, 'label' => 'Total', 'name' => 'SDTotal', 'formatter' => 'number', 'align' => 'right' ],
			'ttsd.SDMemo'     => [ 'width' => 248, 'label' => 'Keterangan', 'name' => 'SDMemo' ],
			'ttsd.NoGL'       => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
			'ttsd.UserID'     => [ 'width' => 70, 'label' => 'User ID', 'name' => 'UserID' ],
		];
	}
	public static function colGridInvoiceReturBeli() {
		return [
			'REPLACE(SDNo,"SR","IR") AS SDNoInvoice' => [ 'width' => 75, 'label' => 'SDNoInvoice', 'name' => 'SDNoInvoice' ],
			'ttsd.SDTgl'                             => [ 'width' => 70, 'label' => 'Tgl SD', 'name' => 'SDTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'ttsd.SDJam'                             => [ 'width' => 50, 'label' => 'Jam', 'name' => 'SDJam', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'H:i:s' ] ],
			'ttsd.LokasiKode'                        => [ 'width' => 100, 'label' => 'Kode Lokasi', 'name' => 'LokasiKode' ],
			'LokasiNama'                             => [ 'width' => 175, 'label' => 'Lokasi Asal', 'name' => 'LokasiNama' ],
			'SupNama'                                => [ 'width' => 150, 'label' => 'Dealer Tujuan', 'name' => 'SupNama' ],
			'ttsd.SDTotal'                           => [ 'width' => 100, 'label' => 'Total', 'name' => 'SDTotal', 'formatter' => 'number', 'align' => 'right' ],
			'ttsd.SDMemo'                            => [ 'width' => 248, 'label' => 'Keterangan', 'name' => 'SDMemo' ],
			'ttsd.NoGL'                              => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
			'ttsd.UserID'                            => [ 'width' => 70, 'label' => 'User ID', 'name' => 'UserID' ],
		];
	}
	public static function colGridMutasiEksternalDealer() {
		return [
			'ttsd.SDNo'       => [ 'width' => 95, 'label' => 'No SD', 'name' => 'SDNo' ],
			'ttsd.SDTgl'      => [ 'width' => 70, 'label' => 'Tgl SD', 'name' => 'SDTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'ttsd.SDJam'      => [ 'width' => 50, 'label' => 'Jam', 'name' => 'SDJam', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'H:i:s' ] ],
			'ttsd.LokasiKode' => [ 'width' => 100, 'label' => 'Kode Lokasi', 'name' => 'LokasiKode' ],
			'LokasiNama'      => [ 'width' => 175, 'label' => 'Lokasi Asal', 'name' => 'LokasiNama' ],
			'ttsd.DealerKode' => [ 'width' => 70, 'label' => 'Kode Dealer', 'name' => 'DealerKode' ],
			'DealerNama'      => [ 'width' => 175, 'label' => 'Dealer Tujuan', 'name' => 'DealerNama' ],
			'ttsd.SDMemo'     => [ 'width' => 210, 'label' => 'Keterangan', 'name' => 'SDMemo' ],
			'ttsd.NoGL'       => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
			'ttsd.UserID'     => [ 'width' => 70, 'label' => 'User ID', 'name' => 'UserID' ],
		];
	}
	public static function colGridMutasiEksternalDealerSelect() {
		return [
			'SDNo'       => [ 'width' => 95, 'label' => 'No SD', 'name' => 'SDNo' ],
			'SDTgl'      => [ 'width' => 70, 'label' => 'Tgl SD', 'name' => 'SDTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'SDJam'      => [ 'width' => 50, 'label' => 'Jam', 'name' => 'SDJam', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'H:i:s' ] ],
			'LokasiKode' => [ 'width' => 100, 'label' => 'Kode Lokasi', 'name' => 'LokasiKode' ],
			'LokasiNama' => [ 'width' => 175, 'label' => 'Lokasi Asal', 'name' => 'LokasiNama' ],
			'DealerKode' => [ 'width' => 70, 'label' => 'Kode Dealer', 'name' => 'DealerKode' ],
			'DealerNama' => [ 'width' => 175, 'label' => 'Dealer Tujuan', 'name' => 'DealerNama' ],
			'SDMemo'     => [ 'width' => 210, 'label' => 'Keterangan', 'name' => 'SDMemo' ],
			'NoGL'       => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
			'UserID'     => [ 'width' => 70, 'label' => 'User ID', 'name' => 'UserID' ],
		];
	}
	public static function colGridInvoiceEksternal() {
		return [
			'REPLACE(SDNo,"ME","IE") AS SDNoInvoice' => [ 'width' => 75, 'label' => 'SDNoInvoice', 'name' => 'SDNoInvoice' ],
			//			'SDNo'                => ['width' => 95,'label' => 'No SD','name'  => 'SDNo'],
			'ttsd.SDTgl'                             => [ 'width' => 70, 'label' => 'Tgl SD', 'name' => 'SDTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'ttsd.SDJam'                             => [ 'width' => 50, 'label' => 'Jam', 'name' => 'SDJam', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'H:i:s' ] ],
			'ttsd.LokasiKode'                        => [ 'width' => 100, 'label' => 'Kode Lokasi', 'name' => 'LokasiKode' ],
			'LokasiNama'                             => [ 'width' => 175, 'label' => 'Lokasi Asal', 'name' => 'LokasiNama' ],
			'ttsd.DealerKode'                        => [ 'width' => 70, 'label' => 'Kode Dealer', 'name' => 'DealerKode' ],
			'DealerNama'                             => [ 'width' => 175, 'label' => 'Dealer Tujuan', 'name' => 'DealerNama' ],
			'ttsd.SDTotal'                           => [ 'width' => 100, 'label' => 'Total', 'name' => 'SDTotal', 'formatter' => 'number', 'align' => 'right' ],
			'ttsd.SDMemo'                            => [ 'width' => 210, 'label' => 'Keterangan', 'name' => 'SDMemo' ],
			'ttsd.NoGL'                              => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
			'ttsd.UserID'                            => [ 'width' => 70, 'label' => 'User ID', 'name' => 'UserID' ],
		];
	}
	public function getSupplier() {
		return $this->hasOne( Tdsupplier::className(), [ 'SupKode' => 'DealerKode' ] );
	}
	public function getLokasi() {
		return $this->hasOne( Tdlokasi::className(), [ 'LokasiKode' => 'LokasiKode' ] );
	}
	public function getDealer() {
		return $this->hasOne( Tddealer::className(), [ 'DealerKode' => 'DealerKode' ] );
	}
	/**
	 * {@inheritdoc}
	 * @return TtsdQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TtsdQuery( get_called_class() );
	}
}
//
//"SELECT ttsd.SDNo, ttsd.SDTgl, ttsd.LokasiKode, ttsd.DealerKode, ttsd.SDMemo, ttsd.SDTotal, ttsd.UserID, tdlokasi.LokasiNama, " + _
//         "tdsupplier.SupNama, ttsd.NoGL FROM  ttsd " + _
//         "INNER JOIN tdlokasi ON ttsd.LokasiKode = tdlokasi.LokasiKode " + _
//         "INNER JOIN tdsupplier ON ttsd.DealerKode = tdsupplier.SupKode " + _
