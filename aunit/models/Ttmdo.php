<?php
namespace aunit\models;
/**
 * This is the model class for table "ttmdo".
 *
 * @property string $DONo
 * @property string $DOTgl
 * @property string $MotorType
 * @property string $DOQty
 */
class Ttmdo extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttmdo';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'DONo', 'DOTgl', 'MotorType' ], 'required' ],
			[ [ 'DOTgl' ], 'safe' ],
			[ [ 'DOQty' ], 'number' ],
			[ [ 'DONo' ], 'string', 'max' => 10 ],
			[ [ 'MotorType' ], 'string', 'max' => 25 ],
			[ [ 'DONo', 'DOTgl', 'MotorType' ], 'unique', 'targetAttribute' => [ 'DONo', 'DOTgl', 'MotorType' ] ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'DONo'      => 'Do No',
			'DOTgl'     => 'Do Tgl',
			'MotorType' => 'Motor Type',
			'DOQty'     => 'Do Qty',
		];
	}
	/**
	 * {@inheritdoc}
	 * SELECT        ttmdo.DONo, ttmdo.DOTgl, ttmdo.MotorType, ttmdo.DOQty, tdmotortype.MotorNama
	 * FROM            ttmdo INNER JOIN
	 * tdmotortype ON ttmdo.MotorType = tdmotortype.MotorType
	 */
	public static function colGrid() {
		return [
			'ttmdo.MotorType'       => [ 'width' => 50, 'label' => 'Type', 'name' => 'MotorType' ],
			'tdmotortype.MotorNama' => [ 'width' => 90, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
			'ttmdo.DOQty'           => [ 'width' => 90, 'label' => 'Qty', 'name' => 'DOQty' ],
		];
	}
	public function getMotorTypes() {
		return $this->hasOne( Tdmotortype::className(), [ 'MotorType' => 'MotorType' ] );
	}
	/**
	 * {@inheritdoc}
	 * @return TtmdoQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TtmdoQuery( get_called_class() );
	}
}
