<?php
namespace aunit\models;
use aunit\components\Menu;
/**
 * This is the model class for table "ttkk".
 *
 * @property string $KKNo
 * @property string $KKTgl
 * @property string $KKMemo
 * @property string $KKNominal
 * @property string $KKJenis Subsidi2, ReturHarga, Umum, BBN, Jaket, Subsidi
 * @property string $KKPerson
 * @property string $KKLink
 * @property string $UserID
 * @property string $LokasiKode
 * @property string $KKJam
 * @property string $NoGL
 * @property string $KKDoc
 */
class Ttkk extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function colGrid() {
		return [
            'KKDoc' 	=> [ 'width' => 30, 'label' => 'Doc', 'name' => 'KKDoc' ],
			'KKNo'       => [ 'width' => 95, 'label' => 'No KK', 'name' => 'KKNo' ],
			'KKTgl'      => [ 'width' => 80, 'label' => 'Tgl KK', 'name' => 'KKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
            'KKJam'      => [ 'width' => 50, 'label' => 'Jam', 'name' => 'KKJam', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'H:i:s' ] ],
            'KKJenis'    => [ 'width' => 80, 'label' => 'Jenis', 'name' => 'KKJenis' ],
            'KKPerson'   => [ 'width' => 175, 'label' => 'Penerima', 'name' => 'KKPerson' ],
            'KKLink'     => [ 'width' => 90, 'label' => 'No Transaksi', 'name' => 'KKLink' ],
            'KKNominal'  => [ 'width' => 90, 'label' => 'Nominal', 'name' => 'KKNominal', 'formatter' => 'number', 'align' => 'right' ],
            'KKMemo'     => [ 'width' => 280, 'label' => 'Keterangan', 'name' => 'KKMemo' ],
			'NoGL'       => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
			'UserID'     => [ 'width' => 70, 'label' => 'User ID', 'name' => 'UserID' ],
			'LokasiKode' => [ 'width' => 100, 'label' => 'LokasiKode', 'name' => 'LokasiKode' ],
		];
	}
	public static function colGridTransferKas() {
		return [
			'KKLink'      => [ 'width' => 70, 'label' => 'No KT', 'name' => 'KKLink' ],
			'KKTgl'       => [ 'width' => 115, 'label' => 'Tgl KK', 'name' => 'KKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y H:i:s' ] ],
			'KKNo'        => [ 'width' => 80, 'label' => 'No KK', 'name' => 'KKNo' ],
			'KMNo'        => [ 'width' => 80, 'label' => 'No KM', 'name' => 'KMNo' ],
			'KKPerson'    => [ 'width' => 140, 'label' => 'Pengirim', 'name' => 'KKPerson' ],
			'KMPerson'    => [ 'width' => 140, 'label' => 'Penerima', 'name' => 'KMPerson' ],
			'KKNominal'   => [ 'width' => 100, 'label' => 'Nominal', 'name' => 'KKNominal', 'formatter' => 'number', 'align' => 'right' ],
			'KKMemo'      => [ 'width' => 300, 'label' => 'Keterangan', 'name' => 'KKMemo' ],
			'NoGL'        => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
			'ttkk.UserID' => [ 'width' => 80, 'label' => 'UserID', 'name' => 'UserID' ],
			'KKJenis'     => [ 'width' => 80, 'label' => 'Jenis', 'name' => 'KKJenis' ],
		];
	}
	public static function colGridSelectKMDariBengkel() {
		return [
			'KKNo'      => [ 'width' => 95, 'label' => 'No KK', 'name' => 'KKNo' ],
			'KKTgl'     => [ 'width' => 80, 'label' => 'Tgl KK', 'name' => 'KKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'KKJenis'   => [ 'width' => 80, 'label' => 'Jenis', 'name' => 'KKJenis' ],
			'KKPerson'  => [ 'width' => 175, 'label' => 'Penerima', 'name' => 'KKPerson' ],
			'KodeTrans' => [ 'width' => 90, 'label' => 'TC', 'name' => 'KodeTrans' ],
			'KKNominal' => [ 'width' => 90, 'label' => 'Nominal', 'name' => 'KKNominal', 'formatter' => 'number', 'align' => 'right' ],
			'Sisa' => [ 'width' => 90, 'label' => 'Nominal', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right','hidden'=>true ],
			'KKMemo'    => [ 'width' => 280, 'label' => 'Keterangan', 'name' => 'KKMemo' ],
		];
	}
	public static function colGridSelectKMOrderKK() {
		return [
			'KKNo'      => [ 'width' => 95, 'label' => 'No KK', 'name' => 'KKNo' ],
			'KKTgl'     => [ 'width' => 80, 'label' => 'Tgl KK', 'name' => 'KKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'KKMemo'    => [ 'width' => 280, 'label' => 'Keterangan', 'name' => 'KKMemo' ],
			'KKNominal' => [ 'width' => 90, 'label' => 'Nominal', 'name' => 'KKNominal', 'formatter' => 'number', 'align' => 'right' ],
			'KKJenis'   => [ 'width' => 80, 'label' => 'Jenis', 'name' => 'KKJenis' ],
			'KKPerson'  => [ 'width' => 175, 'label' => 'Penerima', 'name' => 'KKPerson' ],
			'KKLink'    => [ 'width' => 90, 'label' => 'Tujuan', 'name' => 'KKLink' ],
			'Sisa'      => [ 'width' => 90, 'hidden' => true, 'label' => 'Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
		];
	}
	public static function colGridKasAcc() {
		return [
			'Dealer'        => [ 'width' => 80, 'label' => 'Dealer', 'name' => 'Dealer' ],
			'LokasiKode'        => [ 'width' => 80, 'label' => 'LokasiKode', 'name' => 'LokasiKode' ],
			'KKNo'        => [ 'width' => 80, 'label' => 'No KK', 'name' => 'KKNo'],
			'KKJam'       => [ 'width' => 115, 'label' => 'KKJam', 'name' => 'KKJam', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y H:i:s' ] ],
			'KKNominal'   => [ 'width' => 100, 'label' => 'Nominal', 'name' => 'KKNominal', 'formatter' => 'number', 'align' => 'right' ],
			'KKPerson'    => [ 'width' => 140, 'label' => 'KKPerson', 'name' => 'KKPerson' ],
			'KKLink'      => [ 'width' => 70, 'label' => 'KKLink', 'name' => 'KKLink' ],
			'KKMemo'      => [ 'width' => 300, 'label' => 'Keterangan', 'name' => 'KKMemo' ],
			'UserID' 	  => [ 'width' => 80, 'label' => 'UserID', 'name' => 'UserID' ],
			'KKAcc'      => [ 'width' => 80, 'label' => 'KKAcc', 'name' => 'KKAcc' ],
		];
	}
    public static function colGridBayarHutangBbn() {
        return [
			'KKDoc' 	=> [ 'width' => 30, 'label' => 'Doc', 'name' => 'KKDoc' ],
            'KKNo'       => [ 'width' => 95, 'label' => 'No KK', 'name' => 'KKNo' ],
            'KKTgl'      => [ 'width' => 80, 'label' => 'Tgl KK', 'name' => 'KKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
            'KKNominal'  => [ 'width' => 90, 'label' => 'Nominal', 'name' => 'KKNominal', 'formatter' => 'number', 'align' => 'right' ],
            'KKMemo'     => [ 'width' => 280, 'label' => 'Keterangan', 'name' => 'KKMemo' ],
            'KKPerson'   => [ 'width' => 175, 'label' => 'Penerima', 'name' => 'KKPerson' ],
            'KKJam'      => [ 'width' => 50, 'label' => 'Jam', 'name' => 'KKJam', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'H:i:s' ] ],
            'KKJenis'    => [ 'width' => 80, 'label' => 'Jenis', 'name' => 'KKJenis' ],
            'KKLink'     => [ 'width' => 90, 'label' => 'No Transaksi', 'name' => 'KKLink' ],
            'NoGL'       => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
            'UserID'     => [ 'width' => 70, 'label' => 'User ID', 'name' => 'UserID' ],
            'LokasiKode' => [ 'width' => 100, 'label' => 'LokasiKode', 'name' => 'LokasiKode' ],
        ];
    }
	/**
	 * {@inheritdoc}
	 * @return TtkkQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TtkkQuery( get_called_class() );
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'KKNo' ], 'required' ],
			[ [ 'KKTgl', 'KKJam' ], 'safe' ],
			[ [ 'KKNominal','KKDoc' ], 'number' ],
			// [ [ 'KKNo', 'KKLink' ], 'string', 'max' => 12 ],
			[ [ 'KKMemo' ], 'string', 'max' => 100 ],
			[ [ 'KKJenis' ], 'string', 'max' => 20 ],
			[ [ 'KKPerson' ], 'string', 'max' => 50 ],
			[ [ 'UserID', 'LokasiKode' ], 'string', 'max' => 15 ],
			[ [ 'NoGL' ], 'string', 'max' => 10 ],
			[ [ 'KKNo' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'KKNo'       => 'Kk No',
			'KKTgl'      => 'Kk Tgl',
			'KKMemo'     => 'Kk Memo',
			'KKNominal'  => 'Kk Nominal',
			'KKJenis'    => 'Subsidi2, ReturHarga, Umum, BBN, Jaket, Subsidi',
			'KKPerson'   => 'Kk Person',
			'KKLink'     => 'Kk Link',
			'UserID'     => 'User ID',
			'LokasiKode' => 'Lokasi Kode',
			'KKJam'      => 'Kk Jam',
			'NoGL'       => 'No Gl',
			'KKDoc'      => 'KKDoc',
		];
	}
	public function getAccount() { return $this->hasOne( Traccount::className(), [ 'NoAccount' => 'NoAccount' ] ); }
//    public function getLokasi() {return $this->hasOne( Traccount::className(), [ 'LokasiKode' => 'LokasiKode' ] );}
	/**
	 * @return bool
	 */
	/**
	 * @param bool $temporary
	 *
	 * @return mixed|string
	 */
	public static function generateKKNo( $temporary = false ) {
		$sign    = $temporary ? '=' : '-';
		$kode    = 'KK';
		$yNow    = date( "y" );
		$locNo   = Menu::getUserLokasi()[ 'LokasiNomor' ];
		$maxNoGl = ( new \yii\db\Query() )
			->from( self::tableName() )
			->where( "KKNo LIKE '$kode$locNo$yNow$sign%'" )
			->max( 'KKNo' );
		if ( $maxNoGl && preg_match( "/$kode\d{4}$sign(\d+)/", $maxNoGl, $result ) ) {
			[ $all, $counter ] = $result;
			$digitCount  = strlen( $counter );
			$val         = (int) $counter;
			$nextCounter = sprintf( '%0' . $digitCount . 'd', ++ $val );
		} else {
			$nextCounter = "00001";
		}
		return "$kode$locNo$yNow$sign$nextCounter";
	}
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttkk';
	}

    public static function getRoute( $KKJenis, $param = []) {
        $index  = [];
        $update = [];
        $create = [];
        switch ( $KKJenis ) {
            case 'Bayar BBN' :
                $index = [ 'ttkk/kas-keluar-bayar-hutang-bbn' ];
                $update = [ 'ttkk/kas-keluar-bayar-hutang-bbn-update' ];
                $create = [ 'ttkk/kas-keluar-bayar-hutang-bbn-create' ];
                break;
            case 'SubsidiDealer2' :
                $index = [ 'ttkk/kas-keluar-subsidi-dealer-2' ];
                $update = [ 'ttkk/kas-keluar-subsidi-dealer-2-update' ];
                $create = [ 'ttkk/kas-keluar-subsidi-dealer-2-create' ];
                break;
            case 'Retur Harga' :
                $index = [ 'ttkk/kas-keluar-retur-harga' ];
                $update = [ 'ttkk/kas-keluar-retur-harga-update' ];
                $create = [ 'ttkk/kas-keluar-retur-harga-create' ];
                break;
            case 'Insentif Sales' :
                $index = [ 'ttkk/kas-keluar-insentif-sales' ];
                $update = [ 'ttkk/kas-keluar-insentif-sales-update' ];
                $create = [ 'ttkk/kas-keluar-insentif-sales-create' ];
                break;
            case 'Potongan Khusus' :
                $index = [ 'ttkk/kas-keluar-potongan-khusus' ];
                $update = [ 'ttkk/kas-keluar-potongan-khusus-update' ];
                $create = [ 'ttkk/kas-keluar-potongan-khusus-create' ];
                break;
            case 'KK Ke Bank' :
                $index = [ 'ttkk/kas-keluar-bank-unit' ];
                $update = [ 'ttkk/kas-keluar-bank-unit-update' ];
                $create = [ 'ttkk/kas-keluar-bank-unit-create' ];
                break;
            case 'Umum' :
                $index = [ 'ttkk/kas-keluar-umum' ];
                $update = [ 'ttkk/kas-keluar-umum-update' ];
                $create = [ 'ttkk/kas-keluar-umum-create' ];
                break;
            case 'KK Ke Bengkel' :
                $index = [ 'ttkk/kas-keluar-bank-bengkel' ];
                $update = [ 'ttkk/kas-keluar-bank-bengkel-update' ];
                $create = [ 'ttkk/kas-keluar-bank-bengkel-create' ];
                break;
            case 'Pinjaman Karyawan' :
                $index = [ 'ttkk/kas-keluar-pinjaman-karyawan' ];
                $update = [ 'ttkk/kas-keluar-pinjaman-karyawan-update' ];
                $create = [ 'ttkk/kas-keluar-pinjaman-karyawan-create' ];
                break;
            case 'KK Dealer Ke Pos' :
                $index = [ 'ttkk/kas-keluar-dealer-pos' ];
                $update = [ 'ttkk/kas-keluar-dealer-pos-update' ];
                $create = [ 'ttkk/kas-keluar-dealer-pos-create' ];
                break;
            case 'KK Pos Ke Dealer' :
                $index = [ 'ttkk/kas-keluar-pos-dealer' ];
                $update = [ 'ttkk/kas-keluar-pos-dealer-update' ];
                $create = [ 'ttkk/kas-keluar-pos-dealer-create' ];
                break;
            case 'Bayar Unit' :
                $index = [ 'ttkk/kas-keluar-bayar-hutang-unit' ];
                $update = [ 'ttkk/kas-keluar-bayar-hutang-unit-update' ];
                $create = [ 'ttkk/kas-keluar-bayar-hutang-unit-create' ];
                break;
            case 'BBN Plus' :
                $index = [ 'ttkk/kas-keluar-bayar-bbn-progresif' ];
                $update = [ 'ttkk/kas-keluar-bayar-bbn-progresif-update' ];
                $create = [ 'ttkk/kas-keluar-bayar-bbn-progresif-create' ];
                break;
            case 'BBN Acc' :
                $index = [ 'ttkk/kas-keluar-bayar-bbn-acc' ];
                $update = [ 'ttkk/kas-keluar-bayar-bbn-acc-update' ];
                $create = [ 'ttkk/kas-keluar-bayar-bbn-acc-create' ];
                break;
            case 'Kas Transfer' :
                $index = [ 'ttkk/kas-transfer' ];
                $update = [ 'ttkk/kas-transfer-update' ];
                $create = [ 'ttkk/kas-transfer-create' ];
                break;
        }
        return [
            'index'  => \yii\helpers\Url::toRoute( array_merge( $index, $param ) ),
            'update' => \yii\helpers\Url::toRoute( array_merge( $update, $param ) ),
            'create' => \yii\helpers\Url::toRoute( array_merge( $create, $param ) ),
        ];
    }

    public static function colGridKasKeluarUmum() {
        return [
			'KKDoc' 	=> [ 'width' => 30, 'label' => 'Doc', 'name' => 'KKDoc' ],
            'KKNo'       => [ 'width' => 95, 'label' => 'No KK', 'name' => 'KKNo' ],
            'KKTgl'      => [ 'width' => 80, 'label' => 'Tgl KK', 'name' => 'KKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
            'KKNominal'  => [ 'width' => 90, 'label' => 'Nominal', 'name' => 'KKNominal', 'formatter' => 'number', 'align' => 'right' ],
            'KKMemo'     => [ 'width' => 280, 'label' => 'Keterangan', 'name' => 'KKMemo' ],
            'KKPerson'   => [ 'width' => 175, 'label' => 'Penerima', 'name' => 'KKPerson' ],
            'KKJam'      => [ 'width' => 50, 'label' => 'Jam', 'name' => 'KKJam', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'H:i:s' ] ],
            'KKJenis'    => [ 'width' => 80, 'label' => 'Jenis', 'name' => 'KKJenis' ],
            'KKLink'     => [ 'width' => 90, 'label' => 'No Transaksi', 'name' => 'KKLink' ],
            'NoGL'       => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
            'UserID'     => [ 'width' => 70, 'label' => 'User ID', 'name' => 'UserID' ],
            'LokasiKode' => [ 'width' => 100, 'label' => 'LokasiKode', 'name' => 'LokasiKode' ],
        ];
    }
}
