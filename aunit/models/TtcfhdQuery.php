<?php

namespace aunit\models;

use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
/**
 * This is the ActiveQuery class for [[Ttcfhd]].
 *
 * @see Ttcfhd
 */
class TtcfhdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttcfhd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttcfhd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function callSP( $post ) {
		$post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ] = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'CFNo' ]       = $post[ 'CFNo' ] ?? '--';
		$post[ 'CFNoBaru' ]   = $post[ 'CFNo' ];
		$post[ 'CFTgl' ]      = date('Y-m-d H:i:s', strtotime($post[ 'CFTgl' ] ?? date( 'Y-m-d H:i:s' )));
		$post[ 'CFMemo' ]     = $post[ 'CFMemo' ] ?? '--';
		$post[ 'LokasiKodeKu' ] = $post[ 'LokasiKodeKu' ] ?? '--';
		$post[ 'SalesKode' ]  = $post[ 'SalesKode' ] ?? '--';
		$post[ 'StockBuku' ]  = floatval( $post[ 'StockBuku' ] ?? 0 );
		$post[ 'StockFisik' ] = floatval( $post[ 'StockFisik' ] ?? 0 );
		$post[ 'NilaiBuku' ]  = floatval( $post[ 'NilaiBuku' ] ?? 0 );
		$post[ 'NilaiFisik' ] = floatval( $post[ 'NilaiFisik' ] ?? 0 );
		try {
			General::cCmd( "SET @CFNo = ?;" )->bindParam( 1, $post[ 'CFNo' ] )->execute();
			General::cCmd( "CALL fcfhd(?,@Status,@Keterangan,?,?,?,@CFNo,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'CFNoBaru' ] )
			       ->bindParam( 6, $post[ 'CFTgl' ] )
			       ->bindParam( 7, $post[ 'CFMemo' ] )
			       ->bindParam( 8, $post[ 'LokasiKodeKu' ] )
			       ->bindParam( 9, $post[ 'SalesKode' ] )
			       ->bindParam( 10, $post[ 'StockBuku' ] )
			       ->bindParam( 11, $post[ 'StockFisik' ] )
			       ->bindParam( 12, $post[ 'NilaiBuku' ] )
			       ->bindParam( 13, $post[ 'NilaiFisik' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @CFNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'CFNo'       => $post[ 'CFNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$CFNo       = $exe[ '@CFNo' ] ?? $post[ 'CFNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'CFNo'       => $CFNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
}
