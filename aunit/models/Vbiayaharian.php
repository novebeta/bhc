<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "vbiayaharian".
 *
 * @property string $KKNo
 * @property string $KKTgl
 * @property string $KKJenis
 * @property string $Keterangan
 * @property string $CusNama
 * @property string $KKNominal
 * @property string $NoGL
 */
class Vbiayaharian extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vbiayaharian';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KKTgl'], 'safe'],
            [['KKNominal'], 'number'],
            [['KKNo'], 'string', 'max' => 12],
            [['KKJenis'], 'string', 'max' => 20],
            [['Keterangan'], 'string', 'max' => 100],
            [['CusNama'], 'string', 'max' => 50],
            [['NoGL'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'KKNo' => 'Kk No',
            'KKTgl' => 'Kk Tgl',
            'KKJenis' => 'Kk Jenis',
            'Keterangan' => 'Keterangan',
            'CusNama' => 'Cus Nama',
            'KKNominal' => 'Kk Nominal',
            'NoGL' => 'No Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VbiayaharianQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VbiayaharianQuery(get_called_class());
    }
}
