<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "ttpbit".
 *
 * @property string $PBNo
 * @property string $MotorNoMesin
 * @property int $MotorAutoN
 */
class Ttpbit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttpbit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PBNo', 'MotorNoMesin'], 'required'],
            [['MotorAutoN'], 'integer'],
            [['PBNo'], 'string', 'max' => 10],
            [['MotorNoMesin'], 'string', 'max' => 25],
            [['PBNo', 'MotorNoMesin'], 'unique', 'targetAttribute' => ['PBNo', 'MotorNoMesin']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PBNo' => 'Pb No',
            'MotorNoMesin' => 'Motor No Mesin',
            'MotorAutoN' => 'Motor Auto N',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtpbitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtpbitQuery(get_called_class());
    }
}
