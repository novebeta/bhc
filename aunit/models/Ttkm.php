<?php
namespace aunit\models;
use aunit\components\Menu;
/**
 * This is the model class for table "ttkm".
 *
 * @property string $KMNo
 * @property string $KMTgl
 * @property string $KMMemo
 * @property string $KMNominal
 * @property string $KMJenis DP, Tunai, Umum
 * @property string $KMPerson
 * @property string $KMLink DKNo, BKNo
 * @property string $UserID
 * @property string $LokasiKode
 * @property string $KMJam
 * @property string $NoGL
 */
class Ttkm extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttkm';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'KMNo' ], 'required' ],
			[ [ 'KMTgl', 'KMJam' ], 'safe' ],
			[ [ 'KMNominal' ], 'number' ],
			[ [ 'KMNo', 'KMLink' ], 'string', 'max' => 12 ],
			[ [ 'KMMemo' ], 'string', 'max' => 100 ],
			[ [ 'KMJenis' ], 'string', 'max' => 20 ],
			[ [ 'KMPerson' ], 'string', 'max' => 50 ],
			[ [ 'UserID', 'LokasiKode' ], 'string', 'max' => 15 ],
			[ [ 'NoGL' ], 'string', 'max' => 10 ],
			[ [ 'KMNo' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'KMNo'       => 'Km No',
			'KMTgl'      => 'Km Tgl',
			'KMMemo'     => 'Km Memo',
			'KMNominal'  => 'Km Nominal',
			'KMJenis'    => 'DP, Tunai, Umum',
			'KMPerson'   => 'Km Person',
			'KMLink'     => 'DKNo, BKNo',
			'UserID'     => 'User ID',
			'LokasiKode' => 'Lokasi Kode',
			'KMJam'      => 'Km Jam',
			'NoGL'       => 'No Gl',
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public static function colGrid() {
		return [
			'KMNo'       => [ 'width' => 95, 'label' => 'No KM', 'name' => 'KMNo' ],
			'KMTgl'      => [ 'width' => 80, 'label' => 'Tgl KM', 'name' => 'KMTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'KMJam'      => [ 'width' => 50, 'label' => 'Jam', 'name' => 'KMJam', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'H:i:s' ] ],
			'KMJenis'    => [ 'width' => 80, 'label' => 'Jenis', 'name' => 'KMJenis' ],
			'KMPerson'   => [ 'width' => 175, 'label' => 'Penerima', 'name' => 'KMPerson' ],
			'KMLink'     => [ 'width' => 90, 'label' => 'No Transaksi', 'name' => 'KMLink' ],
			'KMNominal'  => [ 'width' => 90, 'label' => 'Nominal', 'name' => 'KMNominal', 'formatter' => 'number', 'align' => 'right' ],
			'KMMemo'     => [ 'width' => 280, 'label' => 'Keterangan', 'name' => 'KMMemo' ],
			'NoGL'       => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
			'UserID'     => [ 'width' => 70, 'label' => 'UserID', 'name' => 'UserID' ],
			'LokasiKode' => [ 'width' => 100, 'label' => 'LokasiKode', 'name' => 'LokasiKode' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public static function colGridKKkeBengkel() {
		return [
			'KMNo'       => [ 'width' => 95, 'label' => 'No KM', 'name' => 'KMNo' ],
			'KMTgl'      => [ 'width' => 80, 'label' => 'Tgl KM', 'name' => 'KMTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'KMJam'      => [ 'width' => 50, 'label' => 'Jam', 'name' => 'KMJam', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'H:i:s' ] ],
			'KMJenis'    => [ 'width' => 80, 'label' => 'Jenis', 'name' => 'KMJenis' ],
			'KMPerson'   => [ 'width' => 175, 'label' => 'Penerima', 'name' => 'KMPerson' ],
			'KMLink'     => [ 'width' => 90, 'label' => 'No Transaksi', 'name' => 'KMLink' ],
			'KMNominal'  => [ 'width' => 90, 'label' => 'Nominal', 'name' => 'KMNominal', 'formatter' => 'number', 'align' => 'right' ],
			'Sisa'       => [ 'width' => 90, 'label' => 'Sisa', 'name' => 'Sisa', 'hidden' => true ],
			'KMMemo'     => [ 'width' => 280, 'label' => 'Keterangan', 'name' => 'KMMemo' ],
			'NoGL'       => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
			'UserID'     => [ 'width' => 70, 'label' => 'UserID', 'name' => 'UserID' ],
			'LokasiKode' => [ 'width' => 100, 'label' => 'LokasiKode', 'name' => 'LokasiKode' ],
		];
	}
	
	public static function colGridKMWA() {
		return [
			'KMNo'       => [ 'width' => 95, 'label' => 'No KM', 'name' => 'KMNo' ],
			'KMTgl'      => [ 'width' => 80, 'label' => 'Tgl KM', 'name' => 'KMTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'KMJenis'    => [ 'width' => 80, 'label' => 'Jenis', 'name' => 'KMJenis' ],
			'CusTelepon'   => [ 'width' => 175, 'label' => 'CusTelepon', 'name' => 'CusTelepon' ],
			'KMPerson'   => [ 'width' => 175, 'label' => 'Penerima', 'name' => 'KMPerson' ],
			'KMLink'     => [ 'width' => 90, 'label' => 'No Transaksi', 'name' => 'KMLink' ],
			'MotorNama'     => [ 'width' => 90, 'label' => 'MotorNama', 'name' => 'MotorNama' ],
			'MotorWarna'     => [ 'width' => 90, 'label' => 'MotorWarna', 'name' => 'MotorWarna' ],
			'Konsumen'     => [ 'width' => 280, 'label' => 'Konsumen', 'name' => 'Konsumen' ],
			'KMStatus'       => [ 'width' => 70, 'label' => 'KMStatus', 'name' => 'KMStatus' ],
			'KMNominal'  => [ 'width' => 90, 'label' => 'Nominal', 'name' => 'KMNominal', 'formatter' => 'number', 'align' => 'right' ],
		];
	}
	/**
	 * @param bool $temporary
	 *
	 * @return mixed|string
	 */
	public static function generateKMNo( $temporary = false ) {
		$sign    = $temporary ? '=' : '-';
		$kode    = 'KM';
		$yNow    = date( "y" );
		$locNo   = Menu::getUserLokasi()[ 'LokasiNomor' ];
		$maxNoGl = ( new \yii\db\Query() )
			->from( self::tableName() )
			->where( "KMNo LIKE '$kode$locNo$yNow$sign%'" )
			->max( 'KMNo' );
		if ( $maxNoGl && preg_match( "/$kode\d{4}$sign(\d+)/", $maxNoGl, $result ) ) {
			[ $all, $counter ] = $result;
			$digitCount  = strlen( $counter );
			$val         = (int) $counter;
			$nextCounter = sprintf( '%0' . $digitCount . 'd', ++ $val );
		} else {
			$nextCounter = "00001";
		}
		return "$kode$locNo$yNow$sign$nextCounter";
	}
	/**
	 * {@inheritdoc}
	 * @return TtkmQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TtkmQuery( get_called_class() );
	}
	public static function getRoute( $KMJenis, $param = [] ) {
		$index  = [];
		$update = [];
		$create = [];
		switch ( $KMJenis ) {
			case 'KM Pos Dari Dealer' :
				$index  = [ 'ttkm/kas-masuk-pos-dari-dealer' ];
				$update = [ 'ttkm/kas-masuk-pos-dari-dealer-update' ];
				$create = [ 'ttkm/kas-masuk-pos-dari-dealer-create' ];
				break;
			case 'KM Dealer Dari Pos' :
				$index  = [ 'ttkm/kas-masuk-dealer-dari-pos' ];
				$update = [ 'ttkm/kas-masuk-dealer-dari-pos-update' ];
				$create = [ 'ttkm/kas-masuk-dealer-dari-pos-create' ];
				break;
			case 'Inden' :
				$index  = [ 'ttkm/kas-masuk-inden' ];
				$update = [ 'ttkm/kas-masuk-inden-update' ];
				$create = [ 'ttkm/kas-masuk-inden-create' ];
				break;
			case 'Leasing' :
				$index  = [ 'ttkm/kas-masuk-pelunasan-leasing' ];
				$update = [ 'ttkm/kas-masuk-pelunasan-leasing-update' ];
				$create = [ 'ttkm/kas-masuk-pelunasan-leasing-create' ];
				break;
			case 'Kredit' :
				$index  = [ 'ttkm/kas-masuk-uang-muka-kredit' ];
				$update = [ 'ttkm/kas-masuk-uang-muka-kredit-update' ];
				$create = [ 'ttkm/kas-masuk-uang-muka-kredit-create' ];
				break;
			case 'Piutang UM' :
				$index  = [ 'ttkm/kas-masuk-bayar-piutang-kon-um' ];
				$update = [ 'ttkm/kas-masuk-bayar-piutang-kon-um-update' ];
				$create = [ 'ttkm/kas-masuk-bayar-piutang-kon-um-create' ];
				break;
			case 'Tunai' :
				$index  = [ 'ttkm/kas-masuk-uang-muka-tunai' ];
				$update = [ 'ttkm/kas-masuk-uang-muka-tunai-update' ];
				$create = [ 'ttkm/kas-masuk-uang-muka-tunai-create' ];
				break;
			case 'Piutang Sisa' :
				$index  = [ 'ttkm/kas-masuk-bayar-sisa-piutang' ];
				$update = [ 'ttkm/kas-masuk-bayar-sisa-piutang-update' ];
				$create = [ 'ttkm/kas-masuk-bayar-sisa-piutang-create' ];
				break;
			case 'BBN Plus' :
				$index  = [ 'ttkm/kas-masuk-bbn-progresif' ];
				$update = [ 'ttkm/kas-masuk-bbn-progresif-update' ];
				$create = [ 'ttkm/kas-masuk-bbn-progresif-create' ];
				break;
			case 'BBN Acc' :
				$index  = [ 'ttkm/kas-masuk-bbn-acc' ];
				$update = [ 'ttkm/kas-masuk-bbn-acc-update' ];
				$create = [ 'ttkm/kas-masuk-bbn-acc-create' ];
				break;
			case 'KM Dari Bank' :
				$index  = [ 'ttkm/kas-masuk-dari-bank' ];
				$update = [ 'ttkm/kas-masuk-dari-bank-update' ];
				$create = [ 'ttkm/kas-masuk-dari-bank-create' ];
				break;
			case 'Umum' :
				$index  = [ 'ttkm/kas-masuk-umum' ];
				$update = [ 'ttkm/kas-masuk-umum-update' ];
				$create = [ 'ttkm/kas-masuk-umum-create' ];
				break;
			case 'KM Dari Bengkel' :
				$index  = [ 'ttkm/kas-masuk-dari-bengkel' ];
				$update = [ 'ttkm/kas-masuk-dari-bengkel-update' ];
				$create = [ 'ttkm/kas-masuk-dari-bengkel-create' ];
				break;
			case 'Angsuran Karyawan' :
				$index  = [ 'ttkm/kas-masuk-angsuran-karyawan' ];
				$update = [ 'ttkm/kas-masuk-angsuran-karyawan-update' ];
				$create = [ 'ttkm/kas-masuk-angsuran-karyawan-create' ];
				break;
		}
		return [
			'index'  => \yii\helpers\Url::toRoute( array_merge( $index, $param ) ),
			'update' => \yii\helpers\Url::toRoute( array_merge( $update, $param ) ),
			'create' => \yii\helpers\Url::toRoute( array_merge( $create, $param ) ),
		];
	}
}
