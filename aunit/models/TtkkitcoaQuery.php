<?php

namespace aunit\models;

use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
/**
 * This is the ActiveQuery class for [[Ttkkitcoa]].
 *
 * @see Ttkkitcoa
 */
class TtkkitcoaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttkkitcoa[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttkkitcoa|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }


	public function callSP( $post ) {
		$post[ 'UserID' ]       = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]    = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]   = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'KKNo' ]         = $post[ 'KKNo' ] ?? '--';
		$post[ 'KKAutoN' ]      = floatval( $post[ 'KKAutoN' ] ?? 0 );
		$post[ 'NoAccount' ]    = $post[ 'NoAccount' ] ?? '--';
		$post[ 'KKKeterangan' ] = $post[ 'KKKeterangan' ] ?? '--';
		$post[ 'KKBayar' ]      = floatval( $post[ 'KKBayar' ] ?? 0 );
		$post[ 'KKAutoNOLD' ]   = floatval( $post[ 'KKAutoNOLD' ] ?? 0 );
		$post[ 'NoAccountOLD' ] = $post[ 'NoAccountOLD' ] ?? '--';
		try {
			General::cCmd( "CALL fkkitcoa(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'KKNo' ] )
			       ->bindParam( 6, $post[ 'KKAutoN' ] )
			       ->bindParam( 7, $post[ 'NoAccount' ] )
			       ->bindParam( 8, $post[ 'KKKeterangan' ] )
			       ->bindParam( 9, $post[ 'KKBayar' ] )
			       ->bindParam( 10, $post[ 'KKAutoNOLD' ] )
			       ->bindParam( 11, $post[ 'NoAccountOLD' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'status'     => $status,
			'keterangan' => str_replace(" ","&nbsp;",$keterangan)
		];
	}
}
