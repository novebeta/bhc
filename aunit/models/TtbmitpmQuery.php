<?php
namespace aunit\models;
use aunit\components\Menu;
use common\components\General;
use Exception;
/**
 * This is the ActiveQuery class for [[Ttbmitpm]].
 *
 * @see Ttbmitpm
 */
class TtbmitpmQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttbmitpm[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttbmitpm|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ] = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'BMNo' ]       = $post[ 'BMNo' ] ?? '--';
		$post[ 'DKNo' ]       = $post[ 'DKNo' ] ?? '--';
		$post[ 'BMJenis' ]    = $post[ 'BMJenis' ] ?? '--';
		$post[ 'BMBayar' ]    = floatval( $post[ 'BMBayar' ] ?? 0 );
		$post[ 'BMNoOLD' ]    = $post[ 'BMNoOLD' ] ?? '--';
		$post[ 'DKNoOLD' ]    = $post[ 'DKNoOLD' ] ?? '--';
		$post[ 'BMJenisOLD' ] = $post[ 'BMJenisOLD' ] ?? '--';
		try {
			$rows = General::cCmd( "CALL fbmitpm(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'BMNo' ] )
			       ->bindParam( 6, $post[ 'DKNo' ] )
			       ->bindParam( 7, $post[ 'BMJenis' ] )
			       ->bindParam( 8, $post[ 'BMBayar' ] )
			       ->bindParam( 9, $post[ 'BMNoOLD' ] )
			       ->bindParam( 10, $post[ 'DKNoOLD' ] )
			       ->bindParam( 11, $post[ 'BMJenisOLD' ] )
			       ->queryAll();
			$exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'status'     => $status,
			'rows'     => $rows,
			'rowsCount'     => count($rows),
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
}
