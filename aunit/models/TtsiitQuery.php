<?php
namespace aunit\models;
use common\components\General;
use aunit\components\Menu;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttsiit]].
 *
 * @see Ttsiit
 */
class TtsiitQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttsiit[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttsiit|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'SINo' ]          = $post[ 'SINo' ] ?? '--';
		$post[ 'SIAuto' ]        = floatval( $post[ 'SIAuto' ] ?? 0 );
		$post[ 'BrgKode' ]       = $post[ 'BrgKode' ] ?? '--';
		$post[ 'SIQty' ]         = floatval( $post[ 'SIQty' ] ?? 0 );
		$post[ 'SIHrgBeli' ]     = floatval( $post[ 'SIHrgBeli' ] ?? 0 );
		$post[ 'SIHrgJual' ]     = floatval( $post[ 'SIHrgJual' ] ?? 0 );
		$post[ 'SIDiscount' ]    = floatval( $post[ 'SIDiscount' ] ?? 0 );
		$post[ 'SIPajak' ]       = floatval( $post[ 'SIPajak' ] ?? 0 );
		$post[ 'SONo' ]          = $post[ 'SONo' ] ?? '--';
		$post[ 'SIPickingNo' ]   = $post[ 'SIPickingNo' ] ?? '--';
		$post[ 'SIPickingDate' ] = date('Y-m-d H:i:s', strtotime($post[ 'SIPickingDate' ] ?? date( 'Y-m-d H:i:s' )));
		$post[ 'Cetak' ]         = $post[ 'Cetak' ] ?? 'Belum';
        $post[ 'SINoOLD' ]       = $post[ 'SINoOLD' ] ?? '--';
        $post[ 'SIAutoOLD' ]     = floatval( $post[ 'SIAutoOLD' ] ?? 0 );
        $post[ 'BrgKodeOLD' ]    = $post[ 'BrgKodeOLD' ] ?? '--';
        try {
		General::cCmd( "CALL fsiit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
		       ->bindParam( 1, $post[ 'Action' ] )
		       ->bindParam( 2, $post[ 'UserID' ] )
		       ->bindParam( 3, $post[ 'KodeAkses' ] )
		       ->bindParam( 4, $post[ 'PosKode' ] )
		       ->bindParam( 5, $post[ 'SINo' ] )
		       ->bindParam( 6, $post[ 'SIAuto' ] )
		       ->bindParam( 7, $post[ 'BrgKode' ] )
		       ->bindParam( 8, $post[ 'SIQty' ] )
		       ->bindParam( 9, $post[ 'SIHrgBeli' ] )
		       ->bindParam( 10, $post[ 'SIHrgJual' ] )
		       ->bindParam( 11, $post[ 'SIDiscount' ] )
		       ->bindParam( 12, $post[ 'SIPajak' ] )
		       ->bindParam( 13, $post[ 'SONo' ] )
		       ->bindParam( 14, $post[ 'SIPickingNo' ] )
		       ->bindParam( 15, $post[ 'SIPickingDate' ] )
		       ->bindParam( 16, $post[ 'Cetak' ] )
               ->bindParam( 17, $post[ 'SINoOLD' ] )
               ->bindParam( 18, $post[ 'SIAutoOLD' ] )
               ->bindParam( 19, $post[ 'BrgKodeOLD' ] )
		       ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'SINo'       => $post[ 'SINo' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $SINo       = $exe[ '@SINo' ] ?? $post[ 'SINo' ];
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'SINo'       => $SINo,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
	}
}
