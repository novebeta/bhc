<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "tdvariabel".
 *
 * @property string $Nama
 * @property string $Nilai
 */
class Tdvariabel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdvariabel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nama'], 'required'],
            [['Nilai'], 'number'],
            [['Nama'], 'string', 'max' => 15],
            [['Nama'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Nama' => 'Nama',
            'Nilai' => 'Nilai',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TdvariabelQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdvariabelQuery(get_called_class());
    }
}
