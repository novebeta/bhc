<?php

namespace aunit\models;

use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttpsit]].
 *
 * @see Ttpsit
 */
class TtpsitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttpsit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttpsit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
	public function callSP( $post ) {
		$post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'PosKode' ]    = Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'PSNo' ]       = $post[ 'PSNo' ] ?? '--';
		$post[ 'PSAuto' ]     = floatval( $post[ 'PSAuto' ] ?? 0 );
		$post[ 'BrgKode' ]    = $post[ 'BrgKode' ] ?? '--';
		$post[ 'PSQty' ]      = floatval( $post[ 'PSQty' ] ?? 0 );
		$post[ 'PSHrgBeli' ]  = floatval( $post[ 'PSHrgBeli' ] ?? 0 );
		$post[ 'PSHrgJual' ]  = floatval( $post[ 'PSHrgJual' ] ?? 0 );
		$post[ 'PSDiscount' ] = floatval( $post[ 'PSDiscount' ] ?? 0 );
		$post[ 'PSPajak' ]    = floatval( $post[ 'PSPajak' ] ?? 0 );
		$post[ 'PONo' ]       = $post[ 'PONo' ] ?? '--';
		$post[ 'PINo' ]       = $post[ 'PINo' ] ?? '--';
		$post[ 'PIQtyS' ]     = floatval( $post[ 'PIQtyS' ] ?? 0 );
        $post[ 'PSNoOLD' ]    = $post[ 'PSNoOLD' ] ?? '--';
        $post[ 'PSAutoOLD' ]  = floatval( $post[ 'PSAutoOLD' ] ?? 0 );
        $post[ 'BrgKodeOLD' ] = $post[ 'BrgKodeOLD' ] ?? '--';
        try {
            General::cCmd( "CALL fpsit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'PSNo' ] )
                ->bindParam( 6, $post[ 'PSAuto' ] )
                ->bindParam( 7, $post[ 'BrgKode' ] )
                ->bindParam( 8, $post[ 'PSQty' ] )
                ->bindParam( 9, $post[ 'PSHrgBeli' ] )
                ->bindParam( 10, $post[ 'PSHrgJual' ] )
                ->bindParam( 11, $post[ 'PSDiscount' ] )
                ->bindParam( 12, $post[ 'PSPajak' ] )
                ->bindParam( 13, $post[ 'PONo' ] )
                ->bindParam( 14, $post[ 'PINo' ] )
                ->bindParam( 15, $post[ 'PIQtyS' ] )
                ->bindParam( 16, $post[ 'PSNoOLD' ] )
                ->bindParam( 17, $post[ 'PSAutoOLD' ] )
                ->bindParam( 18, $post[ 'BrgKodeOLD' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
	}
}
