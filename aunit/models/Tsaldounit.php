<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "tsaldounit".
 *
 * @property string $NoAccount
 * @property string $TglUnit
 * @property string $JumUnit
 */
class Tsaldounit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tsaldounit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NoAccount'], 'required'],
            [['TglUnit'], 'safe'],
            [['JumUnit'], 'number'],
            [['NoAccount'], 'string', 'max' => 10],
            [['NoAccount'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'NoAccount' => 'No Account',
            'TglUnit' => 'Tgl Unit',
            'JumUnit' => 'Jum Unit',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TsaldounitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TsaldounitQuery(get_called_class());
    }
}
