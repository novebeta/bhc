<?php
namespace aunit\models;
/**
 * This is the model class for table "twsdhd".
 *
 * @property string $SDNo
 * @property string $SVNo
 * @property string $SENo
 * @property string $NoGLSD
 * @property string $NoGLSV
 * @property string $SDTgl
 * @property string $SVTgl
 * @property string $KodeTrans
 * @property string $ASS
 * @property string $PosKode
 * @property string $KarKode
 * @property string $PrgNama
 * @property string $PKBNo
 * @property string $MotorNoPolisi
 * @property string $LokasiKode
 * @property string $SDNoUrut
 * @property string $SDJenis
 * @property string $SDTOP
 * @property string $SDDurasi
 * @property string $SDJamMasuk
 * @property string $SDJamSelesai
 * @property string $SDJamDikerjakan
 * @property string $SDTotalWaktu
 * @property string $SDStatus Menunggu, Diproses, Selesai, Lunas
 * @property string $SDKmSekarang
 * @property string $SDKmBerikut
 * @property string $SDKeluhan
 * @property string $SDKeterangan
 * @property string $SDPembawaMotor
 * @property string $SDHubunganPembawa
 * @property string $SDAlasanServis
 * @property string $SDTotalJasa
 * @property string $SDTotalQty
 * @property string $SDTotalPart
 * @property string $SDTotalGratis
 * @property string $SDTotalNonGratis
 * @property string $SDDiscFinal
 * @property string $SDTotalPajak
 * @property string $SDUangMuka
 * @property string $SDTotalBiaya
 * @property string $SDKasKeluar
 * @property string $SDTerbayar
 * @property string $KlaimKPB
 * @property string $KlaimC2
 * @property string $Cetak
 * @property string $UserID
 */
class Ttsdhd extends \yii\db\ActiveRecord {
	public static function tableName() {
		return 'twsdhd';
	}
	public function rules() {
		return [
			[ [ 'SDNo', 'SVNo' ], 'required' ],
			[ [ 'SDTgl', 'SVTgl', 'SDJamMasuk', 'SDJamSelesai', 'SDJamDikerjakan' ], 'safe' ],
			[
				[
					'SDTOP',
					'SDDurasi',
					'SDTotalWaktu',
					'SDKmSekarang',
					'SDKmBerikut',
					'SDTotalJasa',
					'SDTotalQty',
					'SDTotalPart',
					'SDTotalGratis',
					'SDTotalNonGratis',
					'SDDiscFinal',
					'SDTotalPajak',
					'SDUangMuka',
					'SDTotalBiaya',
					'SDKasKeluar',
					'SDTerbayar'
				],
				'number'
			],
			[ [ 'SDNo', 'SVNo', 'SENo', 'NoGLSD', 'NoGLSV', 'ASS', 'KarKode' ], 'string', 'max' => 10 ],
			[ [ 'KodeTrans' ], 'string', 'max' => 4 ],
			[ [ 'PosKode', 'PKBNo' ], 'string', 'max' => 20 ],
			[ [ 'PrgNama', 'SDHubunganPembawa' ], 'string', 'max' => 25 ],
			[ [ 'MotorNoPolisi' ], 'string', 'max' => 12 ],
			[ [ 'LokasiKode', 'SDStatus', 'SDAlasanServis', 'UserID' ], 'string', 'max' => 15 ],
			[ [ 'SDNoUrut' ], 'string', 'max' => 3 ],
			[ [ 'SDJenis' ], 'string', 'max' => 6 ],
			[ [ 'SDKeluhan', 'SDKeterangan' ], 'string', 'max' => 150 ],
			[ [ 'SDPembawaMotor' ], 'string', 'max' => 75 ],
			[ [ 'KlaimKPB', 'KlaimC2', 'Cetak' ], 'string', 'max' => 5 ],
			[ [ 'SDNo', 'SVNo' ], 'unique', 'targetAttribute' => [ 'SDNo', 'SVNo' ] ],
		];
	}
	public function attributeLabels() {
		return [
			'SDNo'              => 'Sd No',
			'SVNo'              => 'Sv No',
			'SENo'              => 'Se No',
			'NoGLSD'            => 'No Glsd',
			'NoGLSV'            => 'No Glsv',
			'SDTgl'             => 'Sd Tgl',
			'SVTgl'             => 'Sv Tgl',
			'KodeTrans'         => 'Kode Trans',
			'ASS'               => 'Ass',
			'PosKode'           => 'Pos Kode',
			'KarKode'           => 'Kar Kode',
			'PrgNama'           => 'Prg Nama',
			'PKBNo'             => 'Pkb No',
			'MotorNoPolisi'     => 'Motor No Polisi',
			'LokasiKode'        => 'Lokasi Kode',
			'SDNoUrut'          => 'Sd No Urut',
			'SDJenis'           => 'Sd Jenis',
			'SDTOP'             => 'Sdtop',
			'SDDurasi'          => 'Sd Durasi',
			'SDJamMasuk'        => 'Sd Jam Masuk',
			'SDJamSelesai'      => 'Sd Jam Selesai',
			'SDJamDikerjakan'   => 'Sd Jam Dikerjakan',
			'SDTotalWaktu'      => 'Sd Total Waktu',
			'SDStatus'          => 'Sd Status',
			'SDKmSekarang'      => 'Sd Km Sekarang',
			'SDKmBerikut'       => 'Sd Km Berikut',
			'SDKeluhan'         => 'Sd Keluhan',
			'SDKeterangan'      => 'Sd Keterangan',
			'SDPembawaMotor'    => 'Sd Pembawa Motor',
			'SDHubunganPembawa' => 'Sd Hubungan Pembawa',
			'SDAlasanServis'    => 'Sd Alasan Servis',
			'SDTotalJasa'       => 'Sd Total Jasa',
			'SDTotalQty'        => 'Sd Total Qty',
			'SDTotalPart'       => 'Sd Total Part',
			'SDTotalGratis'     => 'Sd Total Gratis',
			'SDTotalNonGratis'  => 'Sd Total Non Gratis',
			'SDDiscFinal'       => 'Sd Disc Final',
			'SDTotalPajak'      => 'Sd Total Pajak',
			'SDUangMuka'        => 'Sd Uang Muka',
			'SDTotalBiaya'      => 'Sd Total Biaya',
			'SDKasKeluar'       => 'Sd Kas Keluar',
			'SDTerbayar'        => 'Sd Terbayar',
			'KlaimKPB'          => 'Klaim Kpb',
			'KlaimC2'           => 'Klaim C2',
			'Cetak'             => 'Cetak',
			'UserID'            => 'User ID',
		];
	}
	public static function colGrid() {
		return [
			'SDNo'                     => [ 'width' => 70, 'label' => 'No SD', 'name' => 'SDNo' ],
			'SDTgl'                    => [ 'width' => 115, 'label' => 'Tgl SD', 'name' => 'SDTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y H:i:s' ] ],
			'MotorNoPolisi'            => [ 'width' => 90, 'label' => 'No Polisi', 'name' => 'MotorNoPolisi' ],
			'SVNo'                     => [ 'width' => 70, 'label' => 'No SV', 'name' => 'SVNo' ],
			'CusNama'                  => [ 'width' => 200, 'label' => 'Nama Konsumen', 'name' => 'CusNama' ],
			'MotorNama'                => [ 'width' => 150, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
			'PKBNo'                    => [ 'width' => 60, 'label' => 'No PKB', 'name' => 'PKBNo' ],
			'SDStatus'                 => [ 'width' => 60, 'label' => 'Status', 'name' => 'SDStatus' ],
			'KodeTrans'                => [ 'width' => 50, 'label' => 'TC', 'name' => 'KodeTrans' ],
			'KarKode'                  => [ 'width' => 71, 'label' => 'Mekanik', 'name' => 'KarKode' ],
			'NoGLSD'                   => [ 'width' => 70, 'label' => 'No GL SD', 'name' => 'NoGLSD' ],
			'NoGLSV'                   => [ 'width' => 70, 'label' => 'No GL SV', 'name' => 'NoGLSV' ],
			'Cetak'                    => [ 'width' => 50, 'label' => 'Cetak', 'name' => 'Cetak' ],
			'UserID'                   => [ 'width' => 70, 'label' => 'UserID', 'name' => 'UserID' ],
		];
	}
	public static function colGridDaftarServis() {
		$arr = [
			'SDNo'                     => [ 'width' => 70, 'label' => 'No SD', 'name' => 'SDNo' ],
			'SDTgl'                    => [ 'width' => 115, 'label' => 'Tgl SD', 'name' => 'SDTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y H:i:s' ] ],

			'SVNo'                     => [ 'width' => 70, 'label' => 'No SV', 'name' => 'SVNo' ],

			'PKBNo'                    => [ 'width' => 60, 'label' => 'No PKB', 'name' => 'PKBNo' ],
			'SDStatus'                 => [ 'width' => 60, 'label' => 'Status', 'name' => 'SDStatus' ],
			'KodeTrans'                => [ 'width' => 50, 'label' => 'TC', 'name' => 'KodeTrans' ],
			'KarKode'                  => [ 'width' => 80, 'label' => 'Mekanik', 'name' => 'KarKode' ],
			'NoGLSD'                   => [ 'width' => 70, 'label' => 'No GL SD', 'name' => 'NoGLSD' ],
			'NoGLSV'                   => [ 'width' => 70, 'label' => 'No GL SV', 'name' => 'NoGLSV' ],
			'SDTotalBiaya'             => [ 'width' => 100, 'label' => 'Total', 'name' => 'SDTotalBiaya', 'formatter' => 'number', 'align' => 'right' ],
			'SDTerbayar'               => [ 'width' => 100, 'label' => 'Terbayar', 'name' => 'SDTerbayar', 'formatter' => 'number', 'align' => 'right' ],
			'ASS'                      => [ 'width' => 70, 'label' => 'ASS', 'name' => 'ASS' ],
			'KlaimKPB'                 => [ 'width' => 50, 'label' => 'KlaimKPB', 'name' => 'KlaimKPB' ],
			'KlaimC2'                  => [ 'width' => 50, 'label' => 'KlaimC2', 'name' => 'KlaimC2' ],
			'SDNoUrut'                 => [ 'width' => 50, 'label' => 'NoUrut', 'name' => 'SDNoUrut' ],
			'PosKode'                  => [ 'width' => 90, 'label' => 'Pos', 'name' => 'PosKode' ],
			'Cetak'                    => [ 'width' => 50, 'label' => 'Cetak', 'name' => 'Cetak' ],
			'UserID'                   => [ 'width' => 80, 'label' => 'UserID', 'name' => 'UserID' ],
			'SDKeterangan'             => [ 'width' => 300, 'label' => 'Keterangan', 'name' => 'SDKeterangan' ],
		];
		if ( \aunit\components\Menu::showOnlyHakAkses( [ 'warehouse', 'gudang' ] ) ) {
			$arr[ 'PLCetak' ] = [ 'width' => 50, 'label' => 'PLCetak', 'name' => 'PLCetak' ];
		}
		return $arr;
	}
	public static function colGridInvoiceServis() {
		$arr = [
			'SDNo'                     => [ 'width' => 70, 'label' => 'No SD', 'name' => 'SDNo' ],
			'SDTgl'                    => [ 'width' => 115, 'label' => 'Tgl SD', 'name' => 'SDTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y H:i:s' ] ],

			'SVNo'                     => [ 'width' => 70, 'label' => 'No Inv', 'name' => 'SVNo' ],

			'PKBNo'                    => [ 'width' => 60, 'label' => 'No PKB', 'name' => 'PKBNo' ],
			'SDStatus'                 => [ 'width' => 60, 'label' => 'Status', 'name' => 'SDStatus' ],
			'KodeTrans'                => [ 'width' => 50, 'label' => 'TC', 'name' => 'KodeTrans' ],
			'KarKode'                  => [ 'width' => 80, 'label' => 'Mekanik', 'name' => 'KarKode' ],
			'NoGLSD'                   => [ 'width' => 70, 'label' => 'No GL SD', 'name' => 'NoGLSD' ],
			'NoGLSV'                   => [ 'width' => 70, 'label' => 'No GL SV', 'name' => 'NoGLSV' ],
			'SDTotalBiaya'             => [ 'width' => 100, 'label' => 'Total', 'name' => 'SDTotalBiaya', 'formatter' => 'number', 'align' => 'right' ],
			'SDTerbayar'               => [ 'width' => 100, 'label' => 'Terbayar', 'name' => 'SDTerbayar', 'formatter' => 'number', 'align' => 'right' ],
			'ASS'                      => [ 'width' => 70, 'label' => 'ASS', 'name' => 'ASS' ],
			'KlaimKPB'                 => [ 'width' => 50, 'label' => 'KlaimKPB', 'name' => 'KlaimKPB' ],
			'KlaimC2'                  => [ 'width' => 50, 'label' => 'KlaimC2', 'name' => 'KlaimC2' ],
			'SDNoUrut'                 => [ 'width' => 50, 'label' => 'NoUrut', 'name' => 'SDNoUrut' ],
			'PosKode'                  => [ 'width' => 90, 'label' => 'Pos', 'name' => 'PosKode' ],
			'Cetak'                    => [ 'width' => 50, 'label' => 'Cetak', 'name' => 'Cetak' ],
			'UserID'                   => [ 'width' => 80, 'label' => 'UserID', 'name' => 'UserID' ],
			'SDKeterangan'             => [ 'width' => 300, 'label' => 'Keterangan', 'name' => 'SDKeterangan' ],
		];
		if ( \aunit\components\Menu::showOnlyHakAkses( [ 'warehouse', 'gudang' ] ) ) {
			$arr[ 'PLCetak' ] = [ 'width' => 50, 'label' => 'PLCetak', 'name' => 'PLCetak' ];
		}
		return $arr;
	}
	public static function colGridInvoiceServisSelect() {
		return [
			'SVNo'                     => [ 'width' => 70, 'label' => 'No Servis', 'name' => 'SVNo' ],
			'SVTgl'                    => [ 'width' => 115, 'label' => 'Tgl Servis', 'name' => 'SVTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y H:i:s' ] ],
			'SDTotalBiaya'             => [ 'width' => 100, 'label' => 'Total', 'name' => 'SDTotalBiaya', 'formatter' => 'number', 'align' => 'right' ],
			'SDTerbayar'               => [ 'width' => 100, 'label' => 'Terbayar', 'name' => 'SDTerbayar', 'formatter' => 'number', 'align' => 'right' ],
		];
	}
	public static function colGridSD() {
		return [
			'SDNo'                     => [ 'width' => 70, 'label' => 'No SD', 'name' => 'SDNo' ],
			'SDTgl'                    => [ 'width' => 115, 'label' => 'Tgl SD', 'name' => 'SDTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y H:i:s' ] ],
			'MotorNoPolisi'            => [ 'width' => 90, 'label' => 'No Polisi', 'name' => 'MotorNoPolisi' ],
			'CusNama'                  => [ 'width' => 200, 'label' => 'Nama Konsumen', 'name' => 'CusNama' ],
			'LokasiKode'               => [ 'width' => 200, 'label' => 'Lokasi', 'name' => 'LokasiKode' ],
			'KodeTrans'                => [ 'width' => 50, 'label' => 'SD', 'name' => 'KodeTrans' ],
			'KarKode'                  => [ 'width' => 50, 'label' => 'Karyawan', 'name' => 'KarKode' ],
			'SDTerbayar'               => [ 'width' => 50, 'label' => 'Uang Muka', 'name' => 'SDTerbayar' ],
			'SDKeterangan'             => [ 'width' => 50, 'label' => 'Keterangan', 'name' => 'SDKeterangan' ],
		];
	}
	public static function colGridKlaim() {
		return [
			'NO'                       => [ 'width' => 70, 'label' => 'No Servis', 'name' => 'SDNo' ],
			'SDTgl'                    => [ 'width' => 115, 'label' => 'Tgl Servis', 'name' => 'SDTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y H:i:s' ] ],
			'MotorNama'                => [ 'width' => 90, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
			'MotorNoPolisi'            => [ 'width' => 90, 'label' => 'No Polisi', 'name' => 'MotorNoPolisi' ],
			'CusNama'                  => [ 'width' => 200, 'label' => 'Nama Konsumen', 'name' => 'CusNama' ],
			'SDTotalJasa'              => [ 'width' => 50, 'label' => 'Total Jasa', 'name' => 'SDTotalJasa' ],
			'SDTotalPart'              => [ 'width' => 50, 'label' => 'Total Part', 'name' => 'SDTotalPart' ],
			'SDTotal'                  => [ 'width' => 50, 'label' => 'Total', 'name' => 'SDTotalBiaya' ],
		];
	}
	public function getCustomer() {
//		return $this->hasOne( Tdcustomer::className(), [ 'CusKode' => 'CusKode' ] );
	}
	public function getDetilBarang() {
		return $this->hasOne( Ttsditbarang::className(), [ 'SDNo' => 'SDNo' ] );
	}
	public static function find() {
		return new TtsdhdQuery( get_called_class() );
	}
	public static function getRoute( $SDJenis, $param = [] ) {
		$index  = [];
		$update = [];
		$create = [];
		switch ( $SDJenis ) {
			case 'SD' :
				$index  = [ 'ttsdhd/daftar-servis' ];
				$update = [ 'ttsdhd/daftar-servis-update' ];
				$create = [ 'ttsdhd/daftar-servis-create' ];
				break;
			case 'SV' :
				$index  = [ 'ttsdhd/invoice-servis' ];
				$update = [ 'ttsdhd/invoice-servis-update' ];
				$create = [ 'ttsdhd/invoice-servis-create' ];
				break;
		}
		return [
			'index'  => \yii\helpers\Url::toRoute( array_merge( $index, $param ) ),
			'update' => \yii\helpers\Url::toRoute( array_merge( $update, $param ) ),
			'create' => \yii\helpers\Url::toRoute( array_merge( $create, $param ) ),
		];
	}
}
