<?php
namespace aunit\models;
use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttbkhd]].
 *
 * @see Ttbkhd
 */
class TtbkhdQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttbkhd[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttbkhd|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ] = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'BKNoBaru' ]   = $post[ 'BKNoView' ] ?? '--';
		$post[ 'BKNo' ]       = $post[ 'BKNo' ] ?? '--';
		$post[ 'BKTgl' ]      = date('Y-m-d', strtotime($post[ 'BKTgl' ] ?? date( 'Y-m-d' )));
		$post[ 'BKMemo' ]     = $post[ 'BKMemo' ] ?? '--';
		$post[ 'BKNominal' ]  = floatval( $post[ 'BKNominal' ] ?? 0 );
		$post[ 'BKJenis' ]    = $post[ 'BKJenis' ] ?? '--';
		$post[ 'SupKode' ]    = $post[ 'SupKode' ] ?? '--';
		$post[ 'BKCekNo' ]    = $post[ 'BKCekNo' ] ?? '--';
		$post[ 'BKCekTempo' ] = date( "Y-m-d", strtotime( ($post[ 'BKCekTempo' ] ?? date( 'Y-m-d' )) ) );
		$post[ 'BKPerson' ]   = $post[ 'BKPerson' ] ?? '--';
		$post[ 'BKAC' ]       = $post[ 'BKAC' ] ?? '--';
		$post[ 'NoAccount' ]  = $post[ 'NoAccount' ] ?? '--';
		$post[ 'BKJam' ]      = date('Y-m-d H:i:s', strtotime($post[ 'BKJam' ] ?? date( 'Y-m-d H:i:s' )));
		$post[ 'NoGL' ]       = $post[ 'NoGL' ] ?? '--';
		try {
			General::cCmd( "SET @BKNo = ?;" )->bindParam( 1, $post[ 'BKNo' ] )->execute();
			General::cCmd( "CALL fbkhd(?,@Status,@Keterangan,?,?,?,@BKNo,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'BKNoBaru' ] )
			       ->bindParam( 6, $post[ 'BKTgl' ] )
			       ->bindParam( 7, $post[ 'BKMemo' ] )
			       ->bindParam( 8, $post[ 'BKNominal' ] )
			       ->bindParam( 9, $post[ 'BKJenis' ] )
			       ->bindParam( 10, $post[ 'SupKode' ] )
			       ->bindParam( 11, $post[ 'BKCekNo' ] )
			       ->bindParam( 12, $post[ 'BKCekTempo' ] )
			       ->bindParam( 13, $post[ 'BKPerson' ] )
			       ->bindParam( 14, $post[ 'BKAC' ] )
			       ->bindParam( 15, $post[ 'NoAccount' ] )
			       ->bindParam( 16, $post[ 'BKJam' ] )
			       ->bindParam( 17, $post[ 'NoGL' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @BKNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'BKNo'       => $post[ 'BKNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$BKNo       = $exe[ '@BKNo' ] ?? $post[ 'BKNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'BKNo'       => $BKNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
	public function callSPTransfer( $post ) {
		$post[ 'UserID' ]      = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]   = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]  = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'BTNo' ]        = $post[ 'BTNo' ] ?? '--';
		$post[ 'BTNoBaru' ]    = $post[ 'BKNoBaru' ] ?? '--';
		$post[ 'BTTgl' ]       = $post[ 'BTTgl' ] ?? date( 'Y-m-d' );
		$post[ 'BTJam' ]       = $post[ 'BTJam' ] ?? date( 'Y-m-d H:i:s' );
		$post[ 'BTMemo' ]      = $post[ 'BKMemo' ] ?? '--';
		$post[ 'BTNominal' ]   = $post[ 'BKNominal' ] ?? 0;
		$post[ 'BTJenis' ]     = $post[ 'BKJenis' ] ?? '--';
		$post[ 'BKCekNo' ]     = $post[ 'BKCekNo' ] ?? '--';
		$post[ 'BKCekTempo' ]  = $post[ 'BKCekTempo' ] ?? date( 'Y-m-d' );
		$post[ 'BKPerson' ]    = $post[ 'BKPerson' ] ?? '--';
		$post[ 'BKAC' ]        = $post[ 'BKAC' ] ?? '--';
		$post[ 'BKNoAccount' ] = $post[ 'BKNoAccount' ] ?? '--';
		$post[ 'BKNoGL' ]      = $post[ 'BKNoGL' ] ?? '--';
		$post[ 'BMCekTempo' ]  = $post[ 'BMCekTempo' ] ?? date( 'Y-m-d' );
		$post[ 'BMPerson' ]    = $post[ 'BMPerson' ] ?? '--';
		$post[ 'BMAC' ]        = $post[ 'BMAC' ] ?? '--';
		$post[ 'BMNoAccount' ] = $post[ 'BMNoAccount' ] ?? '--';
		$post[ 'BMNoGL' ]      = $post[ 'BMNoGL' ] ?? '--';
		$post[ 'BKNo' ]      = $post[ 'BKNo' ] ?? '--';
		$post[ 'BMNo' ]      = $post[ 'BMNo' ] ?? '--';
		try {
			General::cCmd( "SET @BTNo = ?;" )->bindParam( 1, $post[ 'BTNo' ] )->execute();
			General::cCmd( "CALL fbt(?,@Status,@Keterangan,?,?,?,@BTNo,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'BTNoBaru' ] )
			       ->bindParam( 6, $post[ 'BTTgl' ] )
			       ->bindParam( 7, $post[ 'BTJam' ] )
			       ->bindParam( 8, $post[ 'BTMemo' ] )
			       ->bindParam( 9, $post[ 'BTNominal' ] )
			       ->bindParam( 10, $post[ 'BTJenis' ] )
			       ->bindParam( 11, $post[ 'BKCekNo' ] )
			       ->bindParam( 12, $post[ 'BKCekTempo' ] )
			       ->bindParam( 13, $post[ 'BKPerson' ] )
			       ->bindParam( 14, $post[ 'BKAC' ] )
			       ->bindParam( 15, $post[ 'BKNoAccount' ] )
			       ->bindParam( 16, $post[ 'BKNoGL' ] )
			       ->bindParam( 17, $post[ 'BMCekNo' ] )
			       ->bindParam( 18, $post[ 'BMCekTempo' ] )
			       ->bindParam( 19, $post[ 'BMPerson' ] )
			       ->bindParam( 20, $post[ 'BMAC' ] )
			       ->bindParam( 21, $post[ 'BMNoAccount' ] )
			       ->bindParam( 22, $post[ 'BMNoGL' ] )
			       ->bindParam( 23, $post[ 'BKNo' ] )
			       ->bindParam( 24, $post[ 'BMNo' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @BTNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'BTNo'       => $post[ 'BTNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$BTNo       = $exe[ '@BTNo' ] ?? $post[ 'BTNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'BTNo'       => $BTNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
	public function callSPPengajuan( $post ) {
		$post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ] = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'BKNo' ]       = $post[ 'BKNo' ] ?? '--';
		$post[ 'BKNoBaru' ]   = $post[ 'BKNo' ];
		$post[ 'BKTgl' ]      = $post[ 'BKTgl' ] ?? date( 'Y-m-d' );
		$post[ 'BKMemo' ]     = $post[ 'BKMemo' ] ?? '--';
		$post[ 'BKNominal' ]  = $post[ 'BKNominal' ] ?? 0;
		$post[ 'BKJenis' ]    = $post[ 'BKJenis' ] ?? '--';
		$post[ 'SupKode' ]    = $post[ 'SupKode' ] ?? '--';
		$post[ 'BKCekNo' ]    = $post[ 'BKCekNo' ] ?? '--';
		$post[ 'BKCekTempo' ] = $post[ 'BKCekTempo' ] ?? date( 'Y-m-d' );
		$post[ 'BKPerson' ]   = $post[ 'BKPerson' ] ?? '--';
		$post[ 'BKAC' ]       = $post[ 'BKAC' ] ?? '--';
		$post[ 'NoAccount' ]  = $post[ 'NoAccount' ] ?? '--';
		$post[ 'BKJam' ]      = $post[ 'BKJam' ] ?? date( 'Y-m-d H:i:s' );
		$post[ 'NoGL' ]       = $post[ 'NoGL' ] ?? '--';
		try {
			General::cCmd( "SET @BKNo = ?;" )->bindParam( 1, $post[ 'BKNo' ] )->execute();
			General::cCmd( "CALL fbphd(?,@Status,@Keterangan,?,?,?,@BKNo,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'BKNoBaru' ] )
			       ->bindParam( 6, $post[ 'BKTgl' ] )
			       ->bindParam( 7, $post[ 'BKMemo' ] )
			       ->bindParam( 8, $post[ 'BKNominal' ] )
			       ->bindParam( 9, $post[ 'BKJenis' ] )
			       ->bindParam( 10, $post[ 'SupKode' ] )
			       ->bindParam( 11, $post[ 'BKCekNo' ] )
			       ->bindParam( 12, $post[ 'BKCekTempo' ] )
			       ->bindParam( 13, $post[ 'BKPerson' ] )
			       ->bindParam( 14, $post[ 'BKAC' ] )
			       ->bindParam( 15, $post[ 'NoAccount' ] )
			       ->bindParam( 16, $post[ 'BKJam' ] )
			       ->bindParam( 17, $post[ 'NoGL' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @BKNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'BKNo'       => $post[ 'BKNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$BKNo       = $exe[ '@BKNo' ] ?? $post[ 'BKNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'BKNo'       => $BKNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
	public function ttbkFillByNo() {
		return $this->select( new Expression( "ttbkhd.BKAC, ttbkhd.BKCekNo, ttbkhd.BKCekTempo, ttbkhd.BKJam, ttbkhd.BKJenis, ttbkhd.BKMemo, 
            ttbkhd.BKNo, ttbkhd.BKNominal, ttbkhd.BKPerson, ttbkhd.BKTgl, ttbkhd.NoAccount, ttbkhd.NoGL, 
            ttbkhd.SupKode, ttbkhd.UserID, IFNULL(traccount.NamaAccount, '--') AS NamaAccount, 
            IFNULL(tdsupplier.SupNama, '--') AS SupNama " ) )
		            ->join( "LEFT OUTER JOIN", "tdsupplier", "ttbkhd.SupKode = tdsupplier.SupKode " )
		            ->join( "LEFT OUTER JOIN", "traccount", "ttbkhd.NoAccount = traccount.NoAccount" );
	}
	public function transferBank( $BTNo ) {
		$BK = General::cCmd( "SELECT ttbkhd.BKNo, ttbkhd.BKTgl, ttbkhd.BKMemo, ttbkhd.BKNominal, ttbkhd.BKJenis, ttbkhd.SupKode, 
		      ttbkhd.BKCekNo, ttbkhd.BKPerson, ttbkhd.BKCekTempo, ttbkhd.BKAC, ttbkhd.NoAccount AS BKNoAccount, ttbkhd.UserID, ttbkhd.BKJam, ttbkhd.NoGL AS BKNoGL, 
		      ttbkit.FBNo AS SupNama, traccount.NamaAccount 
		      FROM ttbkhd 
		      INNER JOIN ttbkit ON (ttbkhd.BKNo = ttbkit.BKNo)
		      INNER JOIN traccount ON (ttbkhd.NoAccount = traccount.NoAccount) 
		      WHERE  (FBNo = :FBNo)", [ ':FBNo' => $BTNo ] )->queryOne();
		$BM = General::cCmd( "SELECT ttbmhd.BMNo, BMTgl, BMMemo, BMNominal, BMJenis, LeaseKode, BMCekNo, BMCekTempo, BMPerson, BMAC, 
		      ttbmhd.NoAccount AS BMNoAccount, UserID, BMJam, ttbmhd.NoGL AS BMNoGL,DKNo, BMBayar, traccount.NamaAccount 
		      FROM ttbmhd 
		      INNER JOIN ttbmit ON (ttbmhd.BMNo = ttbmit.BMNo) 
		      INNER JOIN traccount ON (ttbmhd.NoAccount = traccount.NoAccount) 
		      WHERE  (DKNo = :DKNo)", [ ':DKNo' => $BTNo ] )->queryOne();

        // Ensure bk & bm variables are arrays before merging
        $BK = is_array($BK) ? $BK : [];
        $BM = is_array($BM) ? $BM : [];
		return array_merge( $BK, $BM );
	}
}
