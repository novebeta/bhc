<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Vmotorlastlokasi]].
 *
 * @see Vmotorlastlokasi
 */
class VmotorlastlokasiQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Vmotorlastlokasi[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Vmotorlastlokasi|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
