<?php
namespace aunit\models;
/**
 * This is the model class for table "ttpd".
 *
 * @property string $PDNo
 * @property string $PDTgl
 * @property string $SDNo
 * @property string $DealerKode
 * @property string $LokasiKode
 * @property string $PDMemo
 * @property string $PDTotal
 * @property string $UserID
 * @property string $PDJam
 * @property string $NoGL
 */
class Ttpd extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttpd';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'PDNo' ], 'required' ],
			[ [ 'PDTgl', 'PDJam' ], 'safe' ],
			[ [ 'PDTotal' ], 'number' ],
			[ [ 'PDNo', 'SDNo', 'DealerKode', 'NoGL' ], 'string', 'max' => 10 ],
			[ [ 'LokasiKode', 'UserID' ], 'string', 'max' => 15 ],
			[ [ 'PDMemo' ], 'string', 'max' => 100 ],
			[ [ 'PDNo' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'PDNo'       => 'Pd No',
			'PDTgl'      => 'Pd Tgl',
			'SDNo'       => 'Sd No',
			'DealerKode' => 'Dealer Kode',
			'LokasiKode' => 'Lokasi Kode',
			'PDMemo'     => 'Pd Memo',
			'PDTotal'    => 'Pd Total',
			'UserID'     => 'User ID',
			'PDJam'      => 'Pd Jam',
			'NoGL'       => 'No Gl',
		];
	}
	public static function colGrid() {
		return [
			'PDNo'                => [ 'width' => 80, 'label' => 'No PD', 'name' => 'PDNo' ],
			'PDTgl'               => [ 'width' => 70, 'label' => 'Tgl PD', 'name' => 'PDTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'PDJam'               => [ 'width' => 50, 'label' => 'Jam', 'name' => 'PDJam', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'H:i:s' ] ],
			'Tddealer.DealerNama' => [ 'width' => 150, 'label' => 'Dealer', 'name' => 'DealerNama' ],
			'tdlokasi.LokasiKode' => [ 'width' => 100, 'label' => 'LokasiKode', 'name' => 'LokasiKode' ],
			'ttpd.NoGL'           => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
			'ttpd.UserID'         => [ 'width' => 70, 'label' => 'UserID', 'name' => 'UserID' ],
		];
	}
	public static function colGridInvoicePenerimaan() {
		return [
			'REPLACE(PDNo,"PD","IP") AS PDNoInvoice' => [ 'width' => 75, 'label' => 'PDNoInvoice', 'name' => 'PDNoInvoice' ],
			'PDTgl'                   => [ 'width' => 70, 'label' => 'Tgl PD', 'name' => 'PDTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'PDJam'                   => [ 'width' => 50, 'label' => 'Jam', 'name' => 'PDJam', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'H:i:s' ] ],
			'ttpd.DealerKode'		  => [ 'width' => 60, 'label' => 'Dealer', 'name' => 'DealerKode' ],
			'Tddealer.DealerNama'     => [ 'width' => 200, 'label' => 'Dealer Asal', 'name' => 'DealerNama' ],
			'tdlokasi.LokasiKode'     => [ 'width' => 115, 'label' => 'Lokasi Tujuan', 'name' => 'LokasiKode' ],
			'ttpd.PDTotal'            => [ 'width' => 95, 'label' => 'Total', 'name' => 'PDTotal', 'formatter' => 'number', 'align' => 'right' ],
			'REPLACE(SDNo,"ME","IE") AS SDNoInvoice ' => [ 'width' => 75, 'label' => 'SDNoInvoice', 'name' => 'SDNoInvoice' ],
			'ttpd.PDMemo'             => [ 'width' => 150, 'label' => 'Keterangan', 'name' => 'PDMemo' ],
			'ttpd.NoGL'               => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
			'ttpd.UserID'             => [ 'width' => 80, 'label' => 'UserID', 'name' => 'UserID' ],
//			'PDNo'                    => [ 'width' => 75, 'label' => 'No PD', 'name' => 'PDNo' ],
//			'SDNo'                    => [ 'width' => 75, 'label' => 'No SD', 'name' => 'SDNo' ],
			'ttpd.PDMemo'             => [ 'width' => 200, 'label' => 'Keterangan', 'name' => 'PDMemo' ],
			'ttpd.NoGL'               => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
			'ttpd.UserID'             => [ 'width' => 70, 'label' => 'UserID', 'name' => 'UserID' ],
		];
	}
	public static function colGridPenerimaanDealer() {
		return [
			'PDNo'                => [ 'width' => 95, 'label' => 'No PD', 'name' => 'PDNo' ],
			'PDTgl'               => [ 'width' => 90, 'label' => 'Tgl PD', 'name' => 'PDTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'PDJam'               => [ 'width' => 50, 'label' => 'Jam', 'name' => 'PDJam', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'H:i:s' ] ],
			'ttpd.DealerKode'     => [ 'width' => 60, 'label' => 'Dealer', 'name' => 'DealerKode' ],
			'Tddealer.DealerNama' => [ 'width' => 200, 'label' => 'Dealer Asal', 'name' => 'DealerNama' ],
			'tdlokasi.LokasiKode' => [ 'width' => 120, 'label' => 'Lokasi Tujuan', 'name' => 'LokasiKode' ],
			'ttpd.SDNo'           => [ 'width' => 75, 'label' => 'No SD', 'name' => 'SDNo' ],
			'ttpd.PDMemo'         => [ 'width' => 250, 'label' => 'Keterangan', 'name' => 'PDMemo' ],
			'ttpd.NoGL'           => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
			'ttpd.UserID'         => [ 'width' => 80, 'label' => 'UserID', 'name' => 'UserID' ],
		];
	}

	public function getLokasi() {
		return $this->hasOne( Tdlokasi::className(), [ 'LokasiKode' => 'LokasiKode' ] );
	}
	public function getDealer() {
		return $this->hasOne( Tddealer::className(), [ 'DealerKode' => 'DealerKode' ] );
	}
	/**
	 * {@inheritdoc}
	 * @return TtpdQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TtpdQuery( get_called_class() );
	}
}
