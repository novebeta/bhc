<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Tvmotorlokasi]].
 *
 * @see Tvmotorlokasi
 */
class TvmotorlokasiQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tvmotorlokasi[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tvmotorlokasi|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
