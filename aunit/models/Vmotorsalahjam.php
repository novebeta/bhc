<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "vmotorsalahjam".
 *
 * @property string $MotorNoMesin
 * @property string $MotorType
 * @property string $JamKeluar
 * @property string $JamMasuk
 * @property string $NoKeluar
 * @property string $NoMasuk
 * @property string $IDKeluar
 * @property string $IDMasuk
 */
class Vmotorsalahjam extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vmotorsalahjam';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['JamKeluar', 'JamMasuk'], 'safe'],
            [['MotorNoMesin'], 'string', 'max' => 25],
            [['MotorType'], 'string', 'max' => 50],
            [['NoKeluar'], 'string', 'max' => 10],
            [['NoMasuk'], 'string', 'max' => 18],
            [['IDKeluar', 'IDMasuk'], 'string', 'max' => 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'MotorNoMesin' => 'Motor No Mesin',
            'MotorType' => 'Motor Type',
            'JamKeluar' => 'Jam Keluar',
            'JamMasuk' => 'Jam Masuk',
            'NoKeluar' => 'No Keluar',
            'NoMasuk' => 'No Masuk',
            'IDKeluar' => 'Id Keluar',
            'IDMasuk' => 'Id Masuk',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VmotorsalahjamQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VmotorsalahjamQuery(get_called_class());
    }
}
