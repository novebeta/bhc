<?php

namespace aunit\models;

use common\components\General;
use Yii;
use yii\base\Model;
use common\models\User;
use Exception;
use OSS\OssClient;
use OSS\Core\OssException;
use aunit\models\Ttkk;
use aunit\models\Ttbkhd;
use aunit\models\Ttdk;
use aunit\models\Tttgeneralledgerhd;
use aunit\models\Ttsk;
use aunit\models\Tmotor;


/**
 * Signup form
 */
class Attachment extends Model
{
    private const URL = 'attachment/index';
    private const Delimiter = '/';
    private const Maxkeys = 1000;
    private const ThumbnailParam = 'x-oss-process=image/resize,m_fill,h_320,w_320,Q_5';
    private const CompressParam = 'image/resize,w_1080/format,jpg/quality,q_90';
    private const Pattern = '/.+(\d{2})(-|\+).+/';
    private const MimePattern = '/^(data:(image|application)\/([a-zA-Z0-9]+);base64,)[a-zA-Z0-9\/\+=]+$/';
    private const FLAMP = [
        'ttkk'=> 'KKDoc',
        'ttbkhd'=> 'BKDoc',
        'ttdk'=> 'DKDoc',
        'tttgeneralledgerhd'=> 'GLDoc',
        'ttsk'=> 'SKDoc',
        'tmotor'=> 'MotorDoc',
    ];
    private const FLAMP_PK = [
        'ttkk'=> 'KKNo',
        'ttbkhd'=> 'BKNo',
        'ttdk'=> 'DKNo',
        'tttgeneralledgerhd'=> 'NoGL',
        'ttsk'=> 'SKNo',
        'tmotor'=> 'DKNo',
    ];
    private $dbName;
    private $bucket;
    private $ossClient;
    private $endPoint;

    public $model;
    public $table;
    public $tableId;
    public $redirectUrl;
    public $titleId;
    
    public $isAllowDelete;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['table', 'trim'],
            ['table', 'required'],

            ['tableId', 'trim'],
            ['tableId', 'required']
        ];
    }

    function __construct()
    {
        if (Yii::$app instanceof \yii\web\Application){
            try {
                $this->dbName = Yii::$app->db->createCommand("SELECT DATABASE()")->queryScalar();
                $this->endPoint = Yii::$app->params['OSS']['OSS_ENDPOINT'];
                $this->ossClient = new OssClient(Yii::$app->params['OSS']['OSS_ACCESS_KEY_ID'], Yii::$app->params['OSS']['OSS_ACCESS_KEY_SECRET'], $this->endPoint, false);
                $this->isAllowDelete = function(){
                    $command = General::cCmd("SELECT GLValid FROM ttgeneralledgerhd WHERE GLLink = :GLLink LIMIT 1;",[':GLLink'=>$this->tableId]);
                    $result = $command->queryScalar();
                    if(!empty($result) && $result == 'Sudah'){
                        return ['result'=>false,'msg'=>'Tidak bisa di hapus karena GL sudah valid.'];
                    }
                    return ['result'=>true,'msg'=>''];
                };
            } catch (OssException $e) {
                throw $e;
            }
        }
    }

    function base64url_encode($data){
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    public function getOptions(){
        preg_match(Self::Pattern, $this->tableId, $matches);
        $twoNumbers = $matches[1];
        $this->bucket = Yii::$app->params['OSS']['OSS_BUCKET'].$twoNumbers;
        return [
            'prefix'=>"{$this->dbName}/{$this->table}/{$this->tableId}/",
            'delimiter' => Self::Delimiter,
            'max-keys' => Self::Maxkeys,
        ];
    }

    /**
     * Get the value of ossClient
     */
    public function getOssClient()
    {
        return $this->ossClient;
    }

    public function generateParams()
    {
        $session = Yii::$app->session;
        $session->set($this->table.$this->tableId, $this->model);
        return base64_encode(json_encode([
            'table' => $this->table, 'tableId' => $this->tableId, 'redirectUrl' => $this->redirectUrl, 'titleId' => $this->titleId
        ]));
    }

    public function loadParams($id)
    {
        $jsonParams = base64_decode($id);
        $params = json_decode($jsonParams,true);
        $this->table = $params['table'];
        $this->tableId = $params['tableId'];
        $this->redirectUrl = $params['redirectUrl'];
        $this->titleId = $params['titleId'];
        $session = Yii::$app->session;
        $this->model = $session->get($this->table.$this->tableId);
    }

    public function getRedirectUrl()
    {
        return  [self::URL, 'id' => $this->generateParams()];
    }    

    public function upload($img, $filename = null)
    {
        try {
            $options =$this->getOptions();
            $prefix = $options['prefix'];
            preg_match(Self::MimePattern, $img, $matches);
            $mime = strtoupper($matches[3]);
            $img = str_replace($matches[1], '', $img);
            $img = str_replace(' ', '+', $img);
            $data = base64_decode($img);
            if(empty($filename)){
                $listObjectInfo = $this->ossClient->listObjectsV2($this->bucket, $options);
                $listFiles = [];
                foreach ($listObjectInfo->getObjectList() as $objInfo){
                    $path = pathinfo($objInfo->getKey());
                    $strFilename = $path['filename'];
                    $arrFilename = explode('-',$strFilename);
                    $strLastArr = end($arrFilename);
                    if($strLastArr === false && !empty($strFilename)){
                        $listFiles[] = floatval($strFilename);
                    }else{
                        $listFiles[] = floatval($strLastArr);
                    }
                }
                rsort($listFiles);
                $filename = 1;
                if(!empty($listFiles)){
                    $filename = $listFiles[0];
                    $filename++;
                }
                $filename = strtoupper($this->dbName.'-'.$this->tableId.'-'. str_pad($filename,2,0,STR_PAD_LEFT).".".$mime);
            }
            $this->ossClient->putObject($this->bucket, $prefix.$filename, $data);
            if($mime != 'PDF'){
                $newPrefix = ($mime == 'JPG') ? "_TMP":'';
                $newFileName = str_replace($newPrefix.'.'.$mime, '.JPG', $filename);
                $process = self::CompressParam.
                '|sys/saveas'.
                ',o_'.$this->base64url_encode($prefix.$newFileName).
                ',b_'.$this->base64url_encode($this->bucket);
                $this->ossClient->processObject($this->bucket, $prefix.$filename, $process);
                $this->ossClient->deleteObject($this->bucket, $prefix.$filename);
                if($newPrefix != ''){
                    $this->ossClient->copyObject($this->bucket, $prefix.$newFileName,$this->bucket, $prefix.$filename);
                    $this->ossClient->deleteObject($this->bucket, $prefix.$filename);
                }
            }
            
        } catch (OssException $e) {
            throw $e;
        }
    }

    public function delete($basename){
        try {
            $allowDelete = $this->isAllowDelete->__invoke();
            if($allowDelete['result']){
                $options =$this->getOptions();
                $prefix = $options['prefix'];
                $this->ossClient->deleteObject($this->bucket, $prefix.$basename);
            }
            return $allowDelete;
        } catch (OssException $e) {
            throw $e;
        }
    }

    private function updateTotalLampiran($countObj){
        if(!empty($this->model)){
            $className = (new \ReflectionClass($this->model))->getShortName();
            $className = strtolower($className);
            $this->model->attributes = [self::FLAMP[$className]=>$countObj];
            $this->model->save();
        }
    }

    public function getListAttachment(){
        try {
            $options =$this->getOptions();
            $listObjectInfo = $this->ossClient->listObjectsV2($this->bucket, $options);
            $listFiles = [];
            $countObj = 0;
            foreach ($listObjectInfo->getObjectList() as $objInfo){
                $modified = $objInfo->getLastModified();
                $strTime = strtotime($modified);
                $path = $objInfo->getKey();
                $pathInfo = pathinfo($path);
                $path = \urlencode($path);
                $pathInfo['tableId'] = $this->tableId;
                $baseUrl = "https://{$this->bucket}.{$this->endPoint}/$path?v=$strTime";
                $thumb = "https://{$this->bucket}.{$this->endPoint}/$path?v=$strTime&".self::ThumbnailParam;
                $listFiles[] = [
                    'pathInfo' => $pathInfo,
                    'thumb' => $thumb,
                    'url' => $baseUrl,
                ];
                $countObj++;
            }
            $this->updateTotalLampiran($countObj);            
            return $listFiles;
        } catch (OssException $e) {
            throw $e;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function singleUpdateDoc($prefix){
        if (Yii::$app instanceof \yii\console\Application){
            try{
                $listArrayLamp = [];
                $dbName = str_replace('/','',$prefix);
                $this->endPoint = Yii::$app->params['OSS']['OSS_ENDPOINT'];
                $this->ossClient = new OssClient(Yii::$app->params['OSS']['OSS_ACCESS_KEY_ID'], Yii::$app->params['OSS']['OSS_ACCESS_KEY_SECRET'], $this->endPoint, false);
                $this->bucket = Yii::$app->params['OSS']['OSS_BUCKET'].date('y');
                $listTable = $this->ossClient->listObjectsV2($this->bucket, [
                    'prefix'=>$prefix,
                    'delimiter' => Self::Delimiter,
                    'max-keys' => Self::Maxkeys,
                ]);
                $tableList = $listTable->getPrefixList();
                if (!empty($tableList)) {
                    foreach ($tableList as $tableInfo) {
                        $table = $tableInfo->getPrefix();
                        $dataTable = $this->ossClient->listObjectsV2($this->bucket, [
                            'prefix'=>$table,
                            'delimiter' => Self::Delimiter,
                            'max-keys' => Self::Maxkeys,
                        ]);
                        $dataList = $dataTable->getPrefixList();
                        if (!empty($dataList)) {
                            foreach ($dataList as $dataInfo) {
                                $data = $dataInfo->getPrefix();
                                $dataNumber = $this->ossClient->listObjectsV2($this->bucket, [
                                    'prefix'=>$data,
                                    'delimiter' => Self::Delimiter,
                                    'max-keys' => Self::Maxkeys,
                                ]);
                                $docNum = $dataNumber->getObjectList();
                                $tmpArry = [
                                    'prefix'=> $dataNumber->getPrefix(),
                                    'total'=> 0
                                ];
                                if (!empty($docNum)) {
                                    $tmpArry['total'] = count($docNum);
                                }
                                $listArrayLamp[] = $tmpArry;
                            }
                        }
                    }
                }
                if(!empty($listArrayLamp)){
                    $db_arr   = Yii::$app->components['db'];
                    $connection = new \yii\db\Connection([
                        'dsn'      => $db_arr['dsn'] . 'dbname=' . $dbName . ';',
                        'username' => $db_arr['username'],
                        'password' => $db_arr['password'],
                    ]);
                    $connection->open();
                    $transaction = $connection->beginTransaction();
                    try {
                        $recordLogs = [];
                        foreach ($listArrayLamp as $obj) {
                            list($dbName, $table, $tableId) = explode('/', $obj['prefix']);
                            $table = strtolower($table);
                            if(key_exists($table,Attachment::FLAMP) && key_exists($table,Attachment::FLAMP_PK) && !empty($tableId)){
                                $aff = $connection->createCommand()->update($table, [Attachment::FLAMP[$table] => $obj['total']], [Attachment::FLAMP_PK[$table] => $tableId])->execute();
                                $recordLogs[] = [
                                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'OSS','LogNama'=>$obj['prefix']." : ".$obj['total']." affected rows {$aff}",'LogTabel'=>$table,'NoTrans'=>$tableId
                                ];
                            }
                        }
                        $commandLog = $connection->createCommand()->batchInsert('tuserlog', [
                            'LogTglJam','UserID','LogJenis','LogNama','LogTabel','NoTrans'
                        ], $recordLogs);
                        $commandLog->execute();
                        $transaction->commit();
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                }
                // print_r($listArrayLamp);
            }catch (OssException $e) {
                echo $e->getMessage()."\n";
            } catch (Exception $e) {
                echo $e->getMessage()."\n";
            }
        }
    }

    public function updateDoc(){
        if (Yii::$app instanceof \yii\console\Application){
            try{
                echo "Start Update\n";
                $this->endPoint = Yii::$app->params['OSS']['OSS_ENDPOINT'];
                $this->ossClient = new OssClient(Yii::$app->params['OSS']['OSS_ACCESS_KEY_ID'], Yii::$app->params['OSS']['OSS_ACCESS_KEY_SECRET'], $this->endPoint, false);
                $this->bucket = Yii::$app->params['OSS']['OSS_BUCKET'].date('y');
                $listObjectInfo = $this->ossClient->listObjectsV2($this->bucket, [
                    'prefix'=>"",
                    'delimiter' => Self::Delimiter,
                    'max-keys' => Self::Maxkeys,
                ]);
                // printf("Bucket Name: %s". "\n",$listObjectInfo->getBucketName());
                // printf("Prefix: %s". "\n",$listObjectInfo->getPrefix());
                // printf("Next Continuation Token: %s". "\n",$listObjectInfo->getNextContinuationToken());
                // printf("Continuation Token: %s". "\n",$listObjectInfo->getContinuationToken());
                // printf("Max Keys: %s". "\n",$listObjectInfo->getMaxKeys());
                // printf("Key Count: %s". "\n",$listObjectInfo->getKeyCount());
                // printf("Delimiter: %s". "\n",$listObjectInfo->getDelimiter());
                // $listObjectInfo = $ossClient->listObjects($bucket, $options);
                // $objectList = $listObjectInfo->getObjectList();
                // if (!empty($objectList)) {
                //     print("objectList:\n");
                //     foreach ($objectList as $objectInfo) {
                //         print($objectInfo->getKey() . "\n");
                //     }
                // }
                $dir = realpath(Yii::getAlias('@app').'/..');
                $prefixList = $listObjectInfo->getPrefixList();
                $path = $dir . '/yii oss/single-update-doc';
                $command = $path . ' %s > /dev/null &';
                if (!empty($prefixList)) {
                    foreach ($prefixList as $prefixInfo) {
                        $dbName = $prefixInfo->getPrefix();
                        $time = date('Y-m-d H:i:s');
                        print("$dbName $time \n");
                        $cmd = sprintf($command, $dbName);
                        exec($cmd);
                    }
                }
            }catch (OssException $e) {
                echo $e->getMessage()."\n";
            } catch (Exception $e) {
                echo $e->getMessage()."\n";
            }
        }
    }
}