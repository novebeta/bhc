<?php
namespace aunit\models;
/**
 * This is the model class for table "ttdk".
 *
 * @property string $DKNo
 * @property string $DKTgl
 * @property string $SalesKode
 * @property string $CusKode
 * @property string $LeaseKode
 * @property string $DKJenis
 * @property string $DKMemo
 * @property string $DKLunas
 * @property string $INNo
 * @property string $UserID
 * @property string $DKHPP
 * @property string $DKHarga
 * @property string $DKDPTotal
 * @property string $DKDPLLeasing
 * @property string $DKDPTerima
 * @property string $DKDPInden
 * @property string $DKNetto
 * @property string $ProgramNama
 * @property string $ProgramSubsidi
 * @property string $BBN
 * @property string $Jaket
 * @property string $PrgSubsSupplier
 * @property string $PrgSubsDealer
 * @property string $PrgSubsFincoy
 * @property string $PotonganHarga Subsidi Dealer 2
 * @property string $ReturHarga Komisi
 * @property string $PotonganKhusus
 * @property string $NamaHadiah
 * @property string $PotonganAHM
 * @property string $DKTenor
 * @property string $DKAngsuran
 * @property string $DKMerkSblmnya
 * @property string $DKJenisSblmnya
 * @property string $DKMotorUntuk
 * @property string $DKMotorOleh
 * @property string $DKMediator
 * @property string $DKTOP
 * @property string $DKPOLeasing
 * @property string $SPKNo
 * @property string $InsentifSales
 * @property string $TeamKode
 * @property string $DKTglTagih
 * @property string $DKPONo
 * @property string $DKPOTgl
 * @property string $BBNPlus
 * @property string $ROCount
 * @property string $DKScheme
 * @property string $DKSCP
 * @property string $DKPengajuanBBN
 * @property string $DKScheme2
 * @property string $JAPrgSubsSupplier
 * @property string $JAPrgSubsDealer
 * @property string $JAPrgSubsFincoy
 * @property string $JADKHarga
 * @property string $JADKDPTerima
 * @property string $JADKDPLLeasing
 * @property string $JABBN
 * @property string $JAReturHarga
 * @property string $JAPotonganHarga
 * @property string $JAInsentifSales
 * @property string $JAPotonganKhusus
 * @property string $DKSurveyor
 * @property Tdcustomer $customer
 * @property Tdmotortype $motorTypes
 * @property Tmotor $motor
 * @property string $NoBukuServis
 * @property string $DKDoc
 */
class Ttdk extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttdk';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'DKNo' ], 'required' ],
			[ [ 'DKTgl', 'DKTglTagih', 'DKPOTgl' ], 'safe' ],
			[
				[
					'DKHPP',
					'DKHarga',
					'DKDPTotal',
					'DKDPLLeasing',
					'DKDPTerima',
					'DKDPInden',
					'DKNetto',
					'ProgramSubsidi',
					'BBN',
					'Jaket',
					'PrgSubsSupplier',
					'PrgSubsDealer',
					'PrgSubsFincoy',
					'PotonganHarga',
					'ReturHarga',
					'PotonganKhusus',
					'PotonganAHM',
					'DKTenor',
					'DKAngsuran',
					'DKTOP',
					'InsentifSales',
					'BBNPlus',
					'ROCount',
					'DKScheme',
					'DKSCP',
					'DKScheme2',
					'JAPrgSubsSupplier',
					'JAPrgSubsDealer',
					'JAPrgSubsFincoy',
					'JADKHarga',
					'JADKDPTerima',
					'JADKDPLLeasing',
					'JABBN',
					'JAReturHarga',
					'JAPotonganHarga',
					'JAInsentifSales',
					'JAPotonganKhusus',
					'DKDoc'
				],
				'number'
			],
			[ [ 'DKNo', 'SalesKode', 'CusKode', 'LeaseKode', 'INNo' ], 'string', 'max' => 10 ],
			[ [ 'DKJenis' ], 'string', 'max' => 6 ],
			[ [ 'DKMemo' ], 'string', 'max' => 100 ],
			[ [ 'DKLunas', 'DKPengajuanBBN' ], 'string', 'max' => 5 ],
			[ [ 'UserID', 'SPKNo', 'TeamKode' ], 'string', 'max' => 15 ],
			[ [ 'ProgramNama', 'DKMerkSblmnya', 'DKJenisSblmnya', 'DKMotorUntuk', 'DKMotorOleh', 'DKPOLeasing' ], 'string', 'max' => 25 ],
			[ [ 'NamaHadiah', 'DKMediator', 'DKSurveyor' ], 'string', 'max' => 50 ],
			[ [ 'DKPONo', 'NoBukuServis' ], 'string', 'max' => 20 ],
			[ [ 'DKNo' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'DKNo'              => 'Dk No',
			'DKTgl'             => 'Dk Tgl',
			'SalesKode'         => 'Sales Kode',
			'CusKode'           => 'Cus Kode',
			'LeaseKode'         => 'Lease Kode',
			'DKJenis'           => 'Dk Jenis',
			'DKMemo'            => 'Dk Memo',
			'DKLunas'           => 'Dk Lunas',
			'INNo'              => 'In No',
			'UserID'            => 'User ID',
			'DKHPP'             => 'Dkhpp',
			'DKHarga'           => 'Dk Harga',
			'DKDPTotal'         => 'Dkdp Total',
			'DKDPLLeasing'      => 'Dkdpl Leasing',
			'DKDPTerima'        => 'Dkdp Terima',
			'DKDPInden'         => 'Dkdp Inden',
			'DKNetto'           => 'Dk Netto',
			'ProgramNama'       => 'Program Nama',
			'ProgramSubsidi'    => 'Program Subsidi',
			'BBN'               => 'Bbn',
			'Jaket'             => 'Jaket',
			'PrgSubsSupplier'   => 'Prg Subs Supplier',
			'PrgSubsDealer'     => 'Prg Subs Dealer',
			'PrgSubsFincoy'     => 'Prg Subs Fincoy',
			'PotonganHarga'     => 'Subsidi Dealer 2',
			'ReturHarga'        => 'Komisi',
			'PotonganKhusus'    => 'Potongan Khusus',
			'NamaHadiah'        => 'Nama Hadiah',
			'PotonganAHM'       => 'Potongan Ahm',
			'DKTenor'           => 'Dk Tenor',
			'DKAngsuran'        => 'Dk Angsuran',
			'DKMerkSblmnya'     => 'Dk Merk Sblmnya',
			'DKJenisSblmnya'    => 'Dk Jenis Sblmnya',
			'DKMotorUntuk'      => 'Dk Motor Untuk',
			'DKMotorOleh'       => 'Dk Motor Oleh',
			'DKMediator'        => 'Dk Mediator',
			'DKTOP'             => 'Dktop',
			'DKPOLeasing'       => 'Dkpo Leasing',
			'SPKNo'             => 'Spk No',
			'InsentifSales'     => 'Insentif Sales',
			'TeamKode'          => 'Team Kode',
			'DKTglTagih'        => 'Dk Tgl Tagih',
			'DKPONo'            => 'Dkpo No',
			'DKPOTgl'           => 'Dkpo Tgl',
			'BBNPlus'           => 'Bbn Plus',
			'ROCount'           => 'Ro Count',
			'DKScheme'          => 'Dk Scheme',
			'DKSCP'             => 'Dkscp',
			'DKPengajuanBBN'    => 'Dk Pengajuan Bbn',
			'DKScheme2'         => 'Dk Scheme2',
			'JAPrgSubsSupplier' => 'Ja Prg Subs Supplier',
			'JAPrgSubsDealer'   => 'Ja Prg Subs Dealer',
			'JAPrgSubsFincoy'   => 'Ja Prg Subs Fincoy',
			'JADKHarga'         => 'Jadk Harga',
			'JADKDPTerima'      => 'Jadkdp Terima',
			'JADKDPLLeasing'    => 'Jadkdpl Leasing',
			'JABBN'             => 'Jabbn',
			'JAReturHarga'      => 'Ja Retur Harga',
			'JAPotonganHarga'   => 'Ja Potongan Harga',
			'JAInsentifSales'   => 'Ja Insentif Sales',
			'JAPotonganKhusus'  => 'Ja Potongan Khusus',
			'DKSurveyor'        => 'Dk Surveyor',
			'NoBukuServis'      => 'No Buku Servis',
			'DKDoc'             => 'DKDoc',
		];
	}
	public static function colGrid() {
		return [
			'ttdk.DKNo'            => [ 'width' => 80, 'label' => 'No DatKons', 'name' => 'DKNo' ],
			'ttdk.DKTgl'           => [ 'width' => 70, 'label' => 'Tgl DK', 'name' => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'tdcustomer.CusNama'   => [ 'label' => 'Nama Konsumen', 'width' => 200, 'name' => 'CusNama' ],
			'tmotor.MotorType'     => [ 'label' => 'Type Motor', 'width' => 220, 'name' => 'MotorType' ],
			'tmotor.MotorWarna'    => [ 'label' => 'Warna Motor', 'width' => 120, 'name' => 'MotorWarna' ],
			'tmotor.MotorNoMesin'  => [ 'label' => 'No Mesin', 'width' => 100, 'name' => 'MotorNoMesin' ],
			'tmotor.MotorNoRangka' => [ 'label' => 'No Rangka', 'width' => 140, 'name' => 'MotorNoRangka' ],
		];
	}
	public static function colGridDataKonsumenStatus() {
		return [
			'ttdk.DKNo'            => [ 'width' => 80, 'label' => 'No DatKons', 'name' => 'DKNo' ],
			'ttdk.DKTgl'           => [ 'width' => 70, 'label' => 'Tgl DK', 'name' => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'tdcustomer.CusNama'   => [ 'label' => 'Nama Konsumen', 'width' => 190, 'name' => 'CusNama' ],
			'tmotor.MotorType'     => [ 'label' => 'Type Motor', 'width' => 230, 'name' => 'MotorType' ],
			'tmotor.MotorNama'     => [ 'label' => 'Nama Motor', 'width' => 230, 'name' => 'MotorNama' ],
			'tmotor.MotorWarna'    => [ 'label' => 'Warna Motor', 'width' => 129, 'name' => 'MotorWarna' ],
			'tmotor.MotorNoMesin'  => [ 'label' => 'No Mesin', 'width' => 100, 'name' => 'MotorNoMesin' ],
			'tmotor.MotorNoRangka' => [ 'label' => 'No Rangka', 'width' => 140, 'name' => 'MotorNoRangka' ],
			'SalesNama'            => [ 'label' => 'Sales', 'width' => 120, 'name' => 'SalesNama' ],
		];
	}
	public static function colGridStnkBpkb() {
		return [
			'tmotor.MotorDoc' 	=> [ 'width' => 30, 'label' => 'Doc', 'name' => 'MotorDoc' ],
			'ttdk.DKNo'             => [ 'width' => 80, 'label' => 'No DatKons', 'name' => 'DKNo' ],
			'ttdk.DKTgl'            => [ 'width' => 70, 'label' => 'Tgl DK', 'name' => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'tdcustomer.CusNama'    => [ 'label' => 'Nama Konsumen', 'width' => 190, 'name' => 'CusNama' ],
			'STNKNama'              => [ 'label' => 'Nama STNK', 'width' => 190, 'name' => 'STNKNama' ],
			'tdcustomer.CusAlamat'  => [ 'label' => 'Alamat', 'width' => 190, 'name' => 'CusAlamat' ],
			'tdsales.SalesNama'     => [ 'label' => 'Sales', 'width' => 120, 'name' => 'SalesNama' ],
			'tmotor.PlatNo'         => [ 'label' => 'No Plat', 'width' => 129, 'name' => 'PlatNo' ],
			'tdmotortype.MotorNama' => [ 'label' => 'Nama Motor', 'width' => 240, 'name' => 'MotorNama' ],
			'tmotor.MotorNoMesin'   => [ 'label' => 'No Mesin', 'width' => 100, 'name' => 'MotorNoMesin' ],
			'tmotor.MotorNoRangka'  => [ 'label' => 'No Rangka', 'width' => 140, 'name' => 'MotorNoRangka' ],
			'ttdk.LeaseKode'        => [ 'label' => 'Leasing', 'width' => 70, 'name' => 'LeaseKode' ],
		];
	}
	public static function colGridDataPortal() {
		return [
			'ttdk.DKNo'            => [ 'width' => 80, 'label' => 'No DatKons', 'name' => 'DKNo' ],
			'ttdk.DKTgl'           => [ 'width' => 70, 'label' => 'Tgl DK', 'name' => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'tdcustomer.CusNama'   => [ 'label' => 'Nama Konsumen', 'width' => 190, 'name' => 'CusNama' ],
			'tmotor.MotorType'     => [ 'label' => 'Type Motor', 'width' => 230, 'name' => 'MotorType' ],
			'tmotor.MotorWarna'    => [ 'label' => 'Warna Motor', 'width' => 129, 'name' => 'MotorWarna' ],
			'tmotor.MotorNoMesin'  => [ 'label' => 'No Mesin', 'width' => 100, 'name' => 'MotorNoMesin' ],
			'tmotor.MotorNoRangka' => [ 'label' => 'No Rangka', 'width' => 140, 'name' => 'MotorNoRangka' ],
		];
	}
	public static function colGridKwitansiKirimTagih() {
		return [
			'ttdk.DKNo'             => [ 'width' => 80, 'label' => 'No DatKons', 'name' => 'DKNo' ],
			'ttdk.DKTgl'            => [ 'width' => 70, 'label' => 'Tgl DK', 'name' => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'tdcustomer.CusNama'    => [ 'label' => 'Nama Konsumen', 'width' => 190, 'name' => 'CusNama' ],
			'tmotor.MotorType'      => [ 'label' => 'Type Motor', 'width' => 230, 'name' => 'MotorType' ],
			'tmotor.MotorWarna'     => [ 'label' => 'Warna Motor', 'width' => 129, 'name' => 'MotorWarna' ],
			'tmotor.MotorNoMesin'   => [ 'label' => 'No Mesin', 'width' => 100, 'name' => 'MotorNoMesin' ],
			'tmotor.MotorNoRangka'  => [ 'label' => 'No Rangka', 'width' => 140, 'name' => 'MotorNoRangka' ],
			'tdmotortype.MotorNama' => [ 'label' => 'Nama Motor', 'width' => 240, 'name' => 'MotorNama' ],
			'ttdk.LeaseKode'        => [ 'label' => 'Leasing', 'width' => 70, 'name' => 'LeaseKode' ],
			'ttdk.TeamKode'         => [ 'label' => 'Team', 'width' => 100, 'name' => 'TeamKode' ],
			'ttdk.SalesKode'        => [ 'label' => 'Sales', 'width' => 80, 'name' => 'SalesKode' ],
			'ttdk.SPKNo'            => [ 'label' => 'No SPK', 'width' => 80, 'name' => 'SPKNo' ],
			'tmotor.SKNo'           => [ 'label' => 'No SK', 'width' => 80, 'name' => 'SKNo' ],
			'ttdk.DKHarga'          => [ 'label' => 'Harga', 'width' => 80, 'name' => 'DKHarga', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.DKDPTotal'        => [ 'label' => 'DP Total', 'width' => 80, 'name' => 'DKDPTotal', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.ProgramSubsidi'   => [ 'label' => 'Subsidi', 'width' => 80, 'name' => 'ProgramSubsidi', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.DKNetto'          => [ 'label' => 'Piutang', 'width' => 80, 'name' => 'DKNetto', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.BBN'              => [ 'label' => 'BBN', 'width' => 80, 'name' => 'BBN', 'formatter' => 'number', 'align' => 'right' ],
			'DKPengajuanBBN'        => [ 'label' => 'Aju BBN', 'width' => 60, 'name' => 'DKPengajuanBBN' ],
			'ttdk.DKMediator'       => [ 'label' => 'Mediator', 'width' => 85, 'name' => 'DKMediator' ],
			'ttdk.ReturHarga'       => [ 'label' => 'Retur Harga', 'width' => 80, 'name' => 'ReturHarga', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.UserID'           => [ 'label' => 'UserID', 'width' => 85, 'name' => 'UserID' ],
			'ttdk.DKPONo'           => [ 'label' => 'No PO', 'width' => 110, 'name' => 'DKPONo' ],
			'ttdk.DKPOTgl'          => [ 'width' => 70, 'label' => 'Tgl PO', 'name' => 'DKPOTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'ttdk.DKTglTagih'       => [ 'width' => 70, 'label' => 'Tgl Tagih', 'name' => 'DKTglTagih', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'ttdk.DKMemo'           => [ 'label' => 'Keterangan', 'width' => 200, 'name' => 'DKMemo' ],
		];
	}
	public static function colGridKwitansiKirimTagihKg() {
		return [
			'ttkg.KGNo'             => [ 'width' => 80, 'label' => 'KGNo', 'name' => 'KGNo' ],
			'ttkg.KGTgl'            => [ 'width' => 70, 'label' => 'KGTgl', 'name' => 'KGTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'tdcustomer.CusNama'    => [ 'label' => 'CusNama', 'width' => 190, 'name' => 'CusNama' ],
			'ttkg.DKNo'             => [ 'width' => 80, 'label' => 'DKNo', 'name' => 'DKNo' ],
			'ttdk.DKTgl'            => [ 'width' => 70, 'label' => 'DKTgl', 'name' => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'ttkg.KGJenis'             => [ 'width' => 80, 'label' => 'KGJenis', 'name' => 'KGJenis' ],
			'ttkg.KGNominal'          => [ 'label' => 'KGNominal', 'width' => 80, 'name' => 'KGNominal', 'formatter' => 'number', 'align' => 'right' ],
			'ttkg.KGPengirim'   => [ 'label' => 'KGPengirim', 'width' => 85, 'name' => 'KGPengirim' ],
			'ttkg.KGPenerima'   => [ 'label' => 'KGPenerima', 'width' => 85, 'name' => 'KGPenerima' ],
			'ttkg.KGMemo'           => [ 'label' => 'KGMemo', 'width' => 85, 'name' => 'KGMemo' ],
			'ttkg.KGStatus'           => [ 'label' => 'KGStatus', 'width' => 85, 'name' => 'KGStatus' ],
			'ttkg.UserID'           => [ 'label' => 'UserID', 'width' => 85, 'name' => 'UserID' ],
			"CONCAT(tdcustomer.CusAlamat, 'RT/RW : ', tdcustomer.CusRT, '/', tdcustomer.CusRW)"      => [ 'label' => 'CusAlamat', 'width' => 85, 'name' => 'CusAlamat' ],
			"CONCAT(tdcustomer.CusKelurahan, ', ', tdcustomer.CusKecamatan, ', ', tdcustomer.CusKabupaten, ', ', tdcustomer.CusProvinsi)"      => [ 'label' => 'CusArea', 'width' => 85, 'name' => 'CusArea' ],
			"CONCAT(tdmotortype.MotorNama, ' - ', tmotor.MotorWarna, ' - ', tdmotortype.MotorCC, 'CC', ' - ', tmotor.MotorTahun)"      => [ 'label' => 'TypeMotor', 'width' => 85, 'name' => 'TypeMotor' ],
			"CONCAT(tmotor.MotorType, ' - ', tmotor.MotorNoMesin, ' - ', tmotor.MotorNoRangka)"      => [ 'label' => 'NoSinNoKa', 'width' => 85, 'name' => 'NoSinNoKa' ],
		];
	}
	public static function colGridKwitansiUmKonsumen() {
		return [
			'ttdk.DKNo'             => [ 'width' => 80, 'label' => 'No DatKons', 'name' => 'DKNo' ],
			'ttdk.DKTgl'            => [ 'width' => 70, 'label' => 'Tgl DK', 'name' => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'tdcustomer.CusNama'    => [ 'label' => 'Nama Konsumen', 'width' => 190, 'name' => 'CusNama' ],
			'tmotor.MotorType'      => [ 'label' => 'Type Motor', 'width' => 230, 'name' => 'MotorType' ],
			'tmotor.MotorWarna'     => [ 'label' => 'Warna Motor', 'width' => 129, 'name' => 'MotorWarna' ],
			'tmotor.MotorNoMesin'   => [ 'label' => 'No Mesin', 'width' => 100, 'name' => 'MotorNoMesin' ],
			'tmotor.MotorNoRangka'  => [ 'label' => 'No Rangka', 'width' => 140, 'name' => 'MotorNoRangka' ],
			'tdmotortype.MotorNama' => [ 'label' => 'Nama Motor', 'width' => 240, 'name' => 'MotorNama' ],
			'ttdk.LeaseKode'        => [ 'label' => 'Leasing', 'width' => 70, 'name' => 'LeaseKode' ],
			'ttdk.TeamKode'         => [ 'label' => 'Team', 'width' => 100, 'name' => 'TeamKode' ],
			'ttdk.SalesKode'        => [ 'label' => 'Sales', 'width' => 80, 'name' => 'SalesKode' ],
			'ttdk.SPKNo'            => [ 'label' => 'No SPK', 'width' => 80, 'name' => 'SPKNo' ],
			'tmotor.SKNo'           => [ 'label' => 'No SK', 'width' => 80, 'name' => 'SKNo' ],
			'ttdk.DKHarga'          => [ 'label' => 'Harga', 'width' => 80, 'name' => 'DKHarga', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.DKDPTotal'        => [ 'label' => 'DP Total', 'width' => 80, 'name' => 'DKDPTotal', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.ProgramSubsidi'   => [ 'label' => 'Subsidi', 'width' => 80, 'name' => 'ProgramSubsidi', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.DKNetto'          => [ 'label' => 'Piutang', 'width' => 80, 'name' => 'DKNetto', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.BBN'              => [ 'label' => 'BBN', 'width' => 80, 'name' => 'BBN', 'formatter' => 'number', 'align' => 'right' ],
			'DKPengajuanBBN'        => [ 'label' => 'Aju BBN', 'width' => 60, 'name' => 'DKPengajuanBBN' ],
			'ttdk.DKMediator'       => [ 'label' => 'Mediator', 'width' => 85, 'name' => 'DKMediator' ],
			'ttdk.ReturHarga'       => [ 'label' => 'Retur Harga', 'width' => 80, 'name' => 'ReturHarga', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.UserID'           => [ 'label' => 'UserID', 'width' => 85, 'name' => 'UserID' ],
			'ttdk.DKPONo'           => [ 'label' => 'No PO', 'width' => 110, 'name' => 'DKPONo' ],
			'ttdk.DKPOTgl'          => [ 'width' => 70, 'label' => 'Tgl PO', 'name' => 'DKPOTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'ttdk.DKTglTagih'       => [ 'width' => 70, 'label' => 'Tgl Tagih', 'name' => 'DKTglTagih', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'ttdk.DKMemo'           => [ 'label' => 'Keterangan', 'width' => 200, 'name' => 'DKMemo' ],
		];
	}
	public static function colGridKwitansiPelunasanLeasing() {
		return [
			'ttdk.DKNo'             => [ 'width' => 80, 'label' => 'No DatKons', 'name' => 'DKNo' ],
			'ttdk.DKTgl'            => [ 'width' => 70, 'label' => 'Tgl DK', 'name' => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'tdcustomer.CusNama'    => [ 'label' => 'Nama Konsumen', 'width' => 190, 'name' => 'CusNama' ],
			'tmotor.MotorType'      => [ 'label' => 'Type Motor', 'width' => 230, 'name' => 'MotorType' ],
			'tmotor.MotorWarna'     => [ 'label' => 'Warna Motor', 'width' => 129, 'name' => 'MotorWarna' ],
			'tmotor.MotorNoMesin'   => [ 'label' => 'No Mesin', 'width' => 100, 'name' => 'MotorNoMesin' ],
			'tmotor.MotorNoRangka'  => [ 'label' => 'No Rangka', 'width' => 140, 'name' => 'MotorNoRangka' ],
			'tdmotortype.MotorNama' => [ 'label' => 'Nama Motor', 'width' => 240, 'name' => 'MotorNama' ],
			'ttdk.LeaseKode'        => [ 'label' => 'Leasing', 'width' => 70, 'name' => 'LeaseKode' ],
			'ttdk.TeamKode'         => [ 'label' => 'Team', 'width' => 100, 'name' => 'TeamKode' ],
			'ttdk.SalesKode'        => [ 'label' => 'Sales', 'width' => 80, 'name' => 'SalesKode' ],
			'ttdk.SPKNo'            => [ 'label' => 'No SPK', 'width' => 80, 'name' => 'SPKNo' ],
			'tmotor.SKNo'           => [ 'label' => 'No SK', 'width' => 80, 'name' => 'SKNo' ],
			'ttdk.DKHarga'          => [ 'label' => 'Harga', 'width' => 80, 'name' => 'DKHarga', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.DKDPTotal'        => [ 'label' => 'DP Total', 'width' => 80, 'name' => 'DKDPTotal', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.ProgramSubsidi'   => [ 'label' => 'Subsidi', 'width' => 80, 'name' => 'ProgramSubsidi', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.DKNetto'          => [ 'label' => 'Piutang', 'width' => 80, 'name' => 'DKNetto', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.BBN'              => [ 'label' => 'BBN', 'width' => 80, 'name' => 'BBN', 'formatter' => 'number', 'align' => 'right' ],
			'DKPengajuanBBN'        => [ 'label' => 'Aju BBN', 'width' => 60, 'name' => 'DKPengajuanBBN' ],
			'ttdk.DKMediator'       => [ 'label' => 'Mediator', 'width' => 85, 'name' => 'DKMediator' ],
			'ttdk.ReturHarga'       => [ 'label' => 'Retur Harga', 'width' => 80, 'name' => 'ReturHarga', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.UserID'           => [ 'label' => 'UserID', 'width' => 85, 'name' => 'UserID' ],
			'ttdk.DKPONo'           => [ 'label' => 'No PO', 'width' => 110, 'name' => 'DKPONo' ],
			'ttdk.DKPOTgl'          => [ 'width' => 70, 'label' => 'Tgl PO', 'name' => 'DKPOTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'ttdk.DKTglTagih'       => [ 'width' => 70, 'label' => 'Tgl Tagih', 'name' => 'DKTglTagih', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'ttdk.DKMemo'           => [ 'label' => 'Keterangan', 'width' => 200, 'name' => 'DKMemo' ],
		];
	}
	public static function colGridKwitansi() {
		return [
			'ttdk.DKNo'             => [ 'width' => 80, 'label' => 'No DatKons', 'name' => 'DKNo' ],
			'ttdk.DKTgl'            => [ 'width' => 70, 'label' => 'Tgl DK', 'name' => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'tdcustomer.CusNama'    => [ 'label' => 'Nama Konsumen', 'width' => 190, 'name' => 'CusNama' ],
			'tmotor.MotorType'      => [ 'label' => 'Type Motor', 'width' => 230, 'name' => 'MotorType' ],
			'tmotor.MotorWarna'     => [ 'label' => 'Warna Motor', 'width' => 129, 'name' => 'MotorWarna' ],
			'tmotor.MotorNoMesin'   => [ 'label' => 'No Mesin', 'width' => 100, 'name' => 'MotorNoMesin' ],
			'tmotor.MotorNoRangka'  => [ 'label' => 'No Rangka', 'width' => 140, 'name' => 'MotorNoRangka' ],
			'tdmotortype.MotorNama' => [ 'label' => 'Nama Motor', 'width' => 240, 'name' => 'MotorNama' ],
			'ttdk.LeaseKode'        => [ 'label' => 'Leasing', 'width' => 70, 'name' => 'LeaseKode' ],
			'ttdk.TeamKode'         => [ 'label' => 'Team', 'width' => 100, 'name' => 'TeamKode' ],
			'ttdk.SalesKode'        => [ 'label' => 'Sales', 'width' => 80, 'name' => 'SalesKode' ],
			'ttdk.SPKNo'            => [ 'label' => 'No SPK', 'width' => 80, 'name' => 'SPKNo' ],
			'tmotor.SKNo'           => [ 'label' => 'No SK', 'width' => 80, 'name' => 'SKNo' ],
			'ttdk.DKHarga'          => [ 'label' => 'Harga', 'width' => 80, 'name' => 'DKHarga', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.DKDPTotal'        => [ 'label' => 'DP Total', 'width' => 80, 'name' => 'DKDPTotal', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.ProgramSubsidi'   => [ 'label' => 'Subsidi', 'width' => 80, 'name' => 'ProgramSubsidi', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.DKNetto'          => [ 'label' => 'Piutang', 'width' => 80, 'name' => 'DKNetto', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.BBN'              => [ 'label' => 'BBN', 'width' => 80, 'name' => 'BBN', 'formatter' => 'number', 'align' => 'right' ],
			'DKPengajuanBBN'        => [ 'label' => 'Aju BBN', 'width' => 60, 'name' => 'DKPengajuanBBN' ],
			'ttdk.DKMediator'       => [ 'label' => 'Mediator', 'width' => 85, 'name' => 'DKMediator' ],
			'ttdk.ReturHarga'       => [ 'label' => 'Retur Harga', 'width' => 80, 'name' => 'ReturHarga', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.UserID'           => [ 'label' => 'UserID', 'width' => 85, 'name' => 'UserID' ],
			'ttdk.DKPONo'           => [ 'label' => 'No PO', 'width' => 110, 'name' => 'DKPONo' ],
			'ttdk.DKPOTgl'          => [ 'width' => 70, 'label' => 'Tgl PO', 'name' => 'DKPOTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'ttdk.DKTglTagih'       => [ 'width' => 70, 'label' => 'Tgl Tagih', 'name' => 'DKTglTagih', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'ttdk.DKMemo'           => [ 'label' => 'Keterangan', 'width' => 200, 'name' => 'DKMemo' ],
		];
	}
	public static function colGridDataKonsumenKredit() {
		return [
			'ttdk.DKDoc' 		   => [ 'width' => 30, 'label' => 'Doc', 'name' => 'DKDoc' ],
			'ttdk.DKNo'            => [ 'width' => 80, 'label' => 'No DatKons', 'name' => 'DKNo' ],
			'ttdk.DKTgl'           => [ 'width' => 70, 'label' => 'Tgl DK', 'name' => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'tdcustomer.CusNama'   => [ 'label' => 'Nama Konsumen', 'width' => 190, 'name' => 'CusNama' ],
			'tmotor.MotorType'     => [ 'label' => 'Type Motor', 'width' => 230, 'name' => 'MotorType' ],
			'tmotor.MotorWarna'    => [ 'label' => 'Warna Motor', 'width' => 129, 'name' => 'MotorWarna' ],
			'tmotor.MotorNoMesin'  => [ 'label' => 'No Mesin', 'width' => 100, 'name' => 'MotorNoMesin' ],
			'tmotor.MotorNoRangka' => [ 'label' => 'No Rangka', 'width' => 140, 'name' => 'MotorNoRangka' ],
		];
	}
	public static function colGridDataKonsumenTunai() {
		return [
			'ttdk.DKDoc' 		   => [ 'width' => 30, 'label' => 'Doc', 'name' => 'DKDoc' ],
			'ttdk.DKNo'            => [ 'width' => 80, 'label' => 'No DatKons', 'name' => 'DKNo' ],
			'ttdk.DKTgl'           => [ 'width' => 70, 'label' => 'Tgl DK', 'name' => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'tdcustomer.CusNama'   => [ 'label' => 'Nama Konsumen', 'width' => 190, 'name' => 'CusNama' ],
			'tmotor.MotorType'     => [ 'label' => 'Type Motor', 'width' => 230, 'name' => 'MotorType' ],
			'tmotor.MotorWarna'    => [ 'label' => 'Warna Motor', 'width' => 129, 'name' => 'MotorWarna' ],
			'tmotor.MotorNoMesin'  => [ 'label' => 'No Mesin', 'width' => 100, 'name' => 'MotorNoMesin' ],
			'tmotor.MotorNoRangka' => [ 'label' => 'No Rangka', 'width' => 140, 'name' => 'MotorNoRangka' ],
		];
	}
	public static function colGridDataKonBayarBBNKas() {
		return [
			'ttdk.DKNo'          => [ 'width' => 80, 'label' => 'No DatKons', 'name' => 'DKNo' ],
			'ttdk.DKTgl'         => [ 'width' => 70, 'label' => 'Tgl DK', 'name' => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'tdcustomer.CusNama' => [ 'label' => 'Nama Konsumen', 'width' => 105, 'name' => 'CusNama' ],
			'ABNo' => [ 'label' => 'ABNo', 'width' => 75, 'name' => 'ABNo' ],
			'tmotor.MotorType'   => [ 'label' => 'Type', 'width' => 165, 'name' => 'MotorType' ],
			'MotorNoMesin'       => [ 'label' => 'MotorNoMesin', 'width' => 100, 'name' => 'MotorNoMesin' ],
			'ttdk.BBN'           => [ 'label' => 'BBN', 'width' => 80, 'name' => 'BBN', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.Terbayar'      => [ 'label' => 'Terbayar', 'width' => 80, 'name' => 'Terbayar', 'formatter' => 'number', 'align' => 'right' ],
			'ttdk.KKBayar'       => [ 'label' => 'Bayar', 'width' => 80, 'name' => 'KKBayar', 'formatter' => 'number', 'align' => 'right', 'editable' => true, 'formatoptions' => [ 'decimalPlaces' => 0 ] ],
			'ttdk.Sisa'          => [ 'label' => 'Sisa', 'width' => 80, 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
		];
	}
	public static function colGridPengajuanBerkas() {
		return [
			'ttdk.DKNo'             => [
				'label' => 'DKNo',
				'width' => 75,
				'name'  => 'DKNo'
			],
			'tdmotortype.MotorType' => [
				'label' => 'MotorType',
				'width' => 105,
				'name'  => 'MotorType'
			],
			'ttdk.CusKode'          => [
				'label' => 'CusKode',
				'width' => 105,
				'name'  => 'CusKode'
			]
		];
	}
	public static function colGridSelectOrderBBN() {
		return [
			'ttdk.DKNo'         => [
				'label' => 'No DatKon',
				'width' => 75,
				'name'  => 'DKNo'
			],
			'ttdk.DKTgl'        => [
				'label' => 'Tgl DK',
				'width' => 105,
				'name'  => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ]
			],
			'ttdk.CusNama'      => [
				'label' => 'Nama Konsumen',
				'width' => 105,
				'name'  => 'CusNama'
			],
			'ttdk.CusKabupaten' => [
				'label' => 'Kabupaten',
				'width' => 105,
				'name'  => 'CusKabupaten'
			],
			'ttdk.MotorType'    => [
				'label' => 'CusKabupaten',
				'width' => 105,
				'name'  => 'MotorType'
			],
			'ttdk.MotorNoMesin' => [
				'label' => 'No Mesin',
				'width' => 105,
				'name'  => 'MotorNoMesin'
			],
			'ttdk.BBN'          => [
				'label' => 'BBN',
				'width' => 105,
				'name'  => 'BBN', 'formatter' => 'number', 'align' => 'right'
			]
		];
	}
	public static function colGridPengajuanBayarBBNBank() {
		return [
			'DKNo'         => [
				'label' => 'DKNo',
				'width' => 75,
				'name'  => 'DKNo'
			],
			'DKTgl'        => [
				'label' => 'DKTgl',
				'width' => 75,
				'name'  => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ]
			],
			'CusNama'      => [
				'label' => 'CusNama',
				'width' => 105,
				'name'  => 'CusNama'
			],
			'ABNo'      => [
				'label' => 'ABNo',
				'width' => 75,
				'name'  => 'ABNo'
			],
			'MotorType'    => [
				'label' => 'MotorType',
				'width' => 165,
				'name'  => 'MotorType'
			],
			'MotorNoMesin' => [
				'label' => 'MotorNoMesin',
				'width' => 100,
				'name'  => 'MotorNoMesin'
			],
			'BBN'          => [
				'label' => 'BBN',
				'width' => 100,
				'name'  => 'BBN', 'formatter' => 'number', 'align' => 'right'
			],
			'Terbayar'     => [
				'label' => 'Terbayar',
				'width' => 100,
				'name'  => 'Terbayar', 'formatter' => 'number', 'align' => 'right'
			],
			'BKBayar'      => [ 'label' => 'Bayar', 'width' => 80, 'name' => 'BKBayar', 'formatter' => 'number', 'align' => 'right', 'editable' => true, 'formatoptions' => [ 'decimalPlaces' => 0 ] ],
			'Sisa'         => [
				'label' => 'Sisa',
				'width' => 80,
				'name'  => 'Sisa', 'formatter' => 'number', 'align' => 'right'
			]
		];
	}
	public static function colGridPengajuanBayarBBNKas() {
		return self::colGridPengajuanBayarBBNBank();
	}
	public static function colGridKKBayarBBNBank() {
		return [
			'DKNo'         => [
				'label' => 'DKNo',
				'width' => 75,
				'name'  => 'DKNo'
			],
			'DKTgl'        => [
				'label' => 'DKTgl',
				'width' => 105,
				'name'  => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ]
			],
			'CusNama'      => [
				'label' => 'CusNama',
				'width' => 105,
				'name'  => 'CusNama'
			],
			'MotorType'    => [
				'label' => 'MotorType',
				'width' => 105,
				'name'  => 'MotorType'
			],
			'MotorTahun'   => [
				'label' => 'MotorTahun',
				'width' => 105,
				'name'  => 'MotorTahun'
			],
			'MotorNoMesin' => [
				'label' => 'MotorNoMesin',
				'width' => 105,
				'name'  => 'MotorNoMesin'
			],
			'BBN'          => [
				'label' => 'BBN',
				'width' => 105,
				'name'  => 'BBN', 'formatter' => 'number', 'align' => 'right'
			],
			'Terbayar'     => [
				'label' => 'Terbayar',
				'width' => 105,
				'name'  => 'Terbayar', 'formatter' => 'number', 'align' => 'right'
			],
			'KKBayar'      => [ 'label' => 'Bayar', 'width' => 80, 'name' => 'KKBayar', 'formatter' => 'number', 'align' => 'right', 'editable' => true, 'formatoptions' => [ 'decimalPlaces' => 0 ] ],
			'Sisa'         => [
				'label' => 'Sisa',
				'width' => 105,
				'name'  => 'Sisa', 'formatter' => 'number', 'align' => 'right'
			],
		];
	}
	public static function colGridKKBayarBBNPlus() {
		return [
			'DKNo'         => [
				'label' => 'DKNo',
				'width' => 75,
				'name'  => 'DKNo'
			],
			'DKTgl'        => [
				'label' => 'DKTgl',
				'width' => 75,
				'name'  => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ]
			],
			'CusNama'      => [
				'label' => 'CusNama',
				'width' => 105,
				'name'  => 'CusNama'
			],
			'MotorType'    => [
				'label' => 'MotorType',
				'width' => 105,
				'name'  => 'MotorType'
			],
			'MotorNoMesin' => [
				'label' => 'MotorNoMesin',
				'width' => 105,
				'name'  => 'MotorNoMesin'
			],
			'BBN'          => [
				'label' => 'BBN',
				'width' => 105,
				'name'  => 'BBN', 'formatter' => 'number', 'align' => 'right'
			],
			'Terbayar'     => [
				'label' => 'Terbayar',
				'width' => 105,
				'name'  => 'Terbayar', 'formatter' => 'number', 'align' => 'right'
			],
			'KKBayar'      => [ 'label' => 'Bayar', 'width' => 80, 'name' => 'KKBayar', 'formatter' => 'number', 'align' => 'right', 'editable' => true, 'formatoptions' => [ 'decimalPlaces' => 0 ] ],
			'Sisa'         => [
				'label' => 'Sisa',
				'width' => 105,
				'name'  => 'Sisa', 'formatter' => 'number', 'align' => 'right'
			],
		];
	}
	public static function colGridKKBayarBBNAcc() {
		return [
			'DKNo'         => [
				'label' => 'DKNo',
				'width' => 75,
				'name'  => 'DKNo'
			],
			'DKTgl'        => [
				'label' => 'DKTgl',
				'width' => 75,
				'name'  => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ]
			],
			'CusNama'      => [
				'label' => 'CusNama',
				'width' => 105,
				'name'  => 'CusNama'
			],
			'MotorType'    => [
				'label' => 'MotorType',
				'width' => 105,
				'name'  => 'MotorType'
			],
			'MotorNoMesin' => [
				'label' => 'MotorNoMesin',
				'width' => 105,
				'name'  => 'MotorNoMesin'
			],
			'BBN'          => [
				'label' => 'BBN',
				'width' => 105,
				'name'  => 'BBN', 'formatter' => 'number', 'align' => 'right'
			],
			'Terbayar'     => [
				'label' => 'Terbayar',
				'width' => 105,
				'name'  => 'Terbayar', 'formatter' => 'number', 'align' => 'right'
			],
			'KKBayar'      => [ 'label' => 'Bayar', 'width' => 80, 'name' => 'KKBayar', 'formatter' => 'number', 'align' => 'right', 'editable' => true, 'formatoptions' => [ 'decimalPlaces' => 0 ] ],
			'Sisa'         => [
				'label' => 'Sisa',
				'width' => 105,
				'name'  => 'Sisa', 'formatter' => 'number', 'align' => 'right'
			],
		];
	}
	public static function colGridKKOrderDK() {
		return [
			'DKNo'         => [
				'label' => 'DKNo',
				'width' => 75,
				'name'  => 'DKNo'
			],
			'DKTgl'        => [
				'label' => 'DKTgl',
				'width' => 105,
				'name'  => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ]
			],
			'CusNama'      => [
				'label' => 'CusNama',
				'width' => 105,
				'name'  => 'CusNama'
			],
			'MotorType'    => [
				'label' => 'MotorType',
				'width' => 105,
				'name'  => 'MotorType'
			],
			'MotorNoMesin' => [
				'label' => 'No Mesin',
				'width' => 105,
				'name'  => 'MotorNoMesin'
			],
			'MotorWarna'   => [
				'label' => 'Warna Motor',
				'width' => 105,
				'name'  => 'MotorWarna'
			],
			'Awal'         => [
				'label' => 'Awal',
				'width' => 105,
				'name'  => 'Awal', 'formatter' => 'number', 'align' => 'right'
			],
			'Terbayar'     => [
				'label' => 'Terbayar',
				'width' => 105,
				'name'  => 'Terbayar', 'formatter' => 'number', 'align' => 'right'
			],
			'Sisa'         => [
				'label' => 'Sisa',
				'width' => 105,
				'name'  => 'Sisa', 'formatter' => 'number', 'align' => 'right'
			],
		];
	}
	public static function colGridBMOrderDK() {
		return [
			'DKNo'         => [
				'label' => 'DKNo',
				'width' => 75,
				'name'  => 'DKNo'
			],
			'DKTgl'        => [
				'label' => 'DKTgl',
				'width' => 75,
				'name'  => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ]
			],
			'CusNama'      => [
				'label' => 'CusNama',
				'width' => 105,
				'name'  => 'CusNama'
			],
			'MotorType'    => [
				'label' => 'MotorType',
				'width' => 105,
				'name'  => 'MotorType'
			],
			'MotorNoMesin' => [
				'label' => 'No Mesin',
				'width' => 105,
				'name'  => 'MotorNoMesin'
			],
			'NilaiAwal'    => [
				'label' => 'Nilai Awal',
				'width' => 105,
				'name'  => 'NilaiAwal', 'formatter' => 'number', 'align' => 'right'
			],
			'Terbayar'     => [
				'label' => 'Terbayar',
				'width' => 105,
				'name'  => 'Terbayar', 'formatter' => 'number', 'align' => 'right'
			],
			'BMBayar'      => [ 'label' => 'Bayar', 'width' => 80, 'name' => 'BMBayar', 'formatter' => 'number', 'align' => 'right', 'editable' => true, 'formatoptions' => [ 'decimalPlaces' => 0 ] ],
			'Sisa'         => [
				'label' => 'Sisa',
				'width' => 105,
				'name'  => 'Sisa', 'formatter' => 'number', 'align' => 'right'
			],
		];
	}
	public static function colGridBMMulti() {
		return [
			'DKNo'      => [
				'label' => 'DKNo',
				'width' => 75,
				'name'  => 'DKNo'
			],
			'DKTgl'     => [
				'label' => 'DKTgl',
				'width' => 105,
				'name'  => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ]
			],
			'Jenis'     => [
				'label' => 'Jenis',
				'width' => 105,
				'name'  => 'Jenis'
			],
			'CusNama'   => [
				'label' => 'CusNama',
				'width' => 105,
				'name'  => 'CusNama'
			],
			'MotorType' => [
				'label' => 'MotorType',
				'width' => 105,
				'name'  => 'MotorType'
			],
			'Awal'      => [
				'label' => 'Awal',
				'width' => 105,
				'name'  => 'NilaiAwal', 'formatter' => 'number', 'align' => 'right'
			],
			'Terbayar'  => [
				'label' => 'Terbayar',
				'width' => 105,
				'name'  => 'Terbayar', 'formatter' => 'number', 'align' => 'right'
			],
			'BMBayar'   => [
				'label'         => 'Bayar',
				'width'         => 80,
				'name'          => 'BMBayar',
				'formatter'     => 'number',
				'align'         => 'right',
				'editable'      => true,
				'formatoptions' => [ 'decimalPlaces' => 0 ]
			],
			'Sisa'      => [
				'label' => 'Sisa',
				'width' => 105,
				'name'  => 'Sisa', 'formatter' => 'number', 'align' => 'right'
			],
		];
	}
	public static function colGridBKOrderPenjualan() {
		return [
			'DKNo'         => [
				'label' => 'DKNo',
				'width' => 75,
				'name'  => 'DKNo'
			],
			'DKTgl'        => [
				'label' => 'DKTgl',
				'width' => 105,
				'name'  => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ]
			],
			'CusNama'      => [
				'label' => 'CusNama',
				'width' => 105,
				'name'  => 'CusNama'
			],
			'MotorType'    => [
				'label' => 'MotorType',
				'width' => 105,
				'name'  => 'MotorType'
			],
			'MotorTahun'   => [
				'label' => 'MotorTahun',
				'width' => 105,
				'name'  => 'MotorTahun'
			],
			'MotorNoMesin' => [
				'label' => 'MotorNoMesin',
				'width' => 105,
				'name'  => 'MotorNoMesin'
			],
			'Awal'         => [
				'label' => 'Awal',
				'width' => 105,
				'name'  => 'Awal', 'formatter' => 'number', 'align' => 'right'
			],
			'Terbayar'     => [
				'label' => 'Terbayar',
				'width' => 105,
				'name'  => 'Terbayar', 'formatter' => 'number', 'align' => 'right'
			],
			'BKBayar'      => [ 'label' => 'Bayar', 'width' => 80, 'name' => 'BKBayar', 'formatter' => 'number', 'align' => 'right', 'editable' => true, 'formatoptions' => [ 'decimalPlaces' => 0 ] ],
			'Sisa'         => [
				'label' => 'Sisa',
				'width' => 105,
				'name'  => 'Sisa', 'formatter' => 'number', 'align' => 'right'
			],
		];
	}

    public static function colGridImportDkItems()
    {
        return [
            'idSpk' => ['label' => 'Id SPK', 'width' => 95,'name' => 'idSpk'],
            'idProspect' => ['label' => 'Id Prospect','width' => 130,  'name' => 'idProspect'],
            'namaCustomer' => ['label' => 'Nama Customer', 'width' => 150,  'name' => 'namaCustomer'],
            'noKtp' => ['label' => 'No KTP','width' => 120, 'name' => 'noKtp'],
            'alamat' => ['label' => 'Alamat','width' => 255, 'name' => 'alamat'],
            'idEvent' => ['label' => 'Id Event','width' => 90, 'name' => 'idEvent'],
            'statusSPK' => ['label' => 'Status SPK','width' => 90, 'name' => 'statusSPK'],
            'kodePropinsi' => ['label' => 'Kode Propinsi','width' => 90, 'name' => 'kodePropinsi'],
            'kodeKota' => ['label' => 'Kode Kota', 'width' => 90, 'name' => 'kodeKota'],
            'kodeKecamatan' => ['label' => 'Kode Kecamatan','width' => 100, 'name' => 'kodeKecamatan'],
            'kodeKelurahan' => ['label' => 'Kode Kelurahan','width' => 100, 'name' => 'kodeKelurahan'],
            'kodePos' => ['label' => 'Kode Pos','width' => 80, 'name' => 'kodePos'],
            'noKontak' => ['label' => 'No Kontak','width' => 90, 'name' => 'noKontak'],
            'namaBPKB' => ['label' => 'Nama BPKB','width' => 150, 'name' => 'namaBPKB'],
            'noKTPBPKB' => ['label' => 'No Ktp BPKB','width' => 120, 'name' => 'noKTPBPKB'],
            'alamatBPKB' => ['label' => 'Alamat BPKB','width' => 200, 'name' => 'alamatBPKB'],
            'kodePropinsiBPKB' => ['label' => 'Kode Propinsi BPKB','width' => 120, 'name' => 'kodePropinsiBPKB'],
            'kodeKotaBPKB' => ['label' => 'Kode Kota BPKB','width' => 120, 'name' => 'kodeKotaBPKB'],
            'kodeKecamatanBPKB' => ['label' => 'Kode Kecamatan BPKB','width' => 120, 'name' => 'kodeKecamatanBPKB'],
            'kodeKelurahanBPKB' => ['label' => 'Kode Kelurahan BPKB','width' => 120, 'name' => 'kodeKelurahanBPKB'],
            'kodePosBPKB' => ['label' => 'Kode Pos BPKB','width' => 90, 'name' => 'kodePosBPKB'],
            'latitude' => ['label' => 'Latitude','width' => 80, 'name' => 'latitude'],
            'longitude' => ['label' => 'Longitude','width' => 80, 'name' => 'longitude'],
            'NPWP' => ['label' => 'NPWP','width' => 80, 'name' => 'NPWP'],
            'noKK' => ['label' => 'No KK','width' => 80, 'name' => 'noKK'],
            'alamatKK' => ['label' => 'Alamat KK','width' => 200, 'name' => 'alamatKK'],
            'kodePropinsiKK' => ['label' => 'Kode Propinsi KK','width' => 130, 'name' => 'kodePropinsiKK'],
            'kodeKotaKK' => ['label' => 'Kode Kota KK','width' => 100, 'name' => 'kodeKotaKK'],
            'kodeKecamatanKK' => ['label' => 'Kode Kecamatan KK','width' => 110, 'name' => 'kodeKecamatanKK'],
            'kodeKelurahanKK' => ['label' => 'Kode Kelurahan KK','width' => 110, 'name' => 'kodeKelurahanKK'],
            'kodePosKK' => ['label' => 'Kode Pos KK','width' => 90, 'name' => 'kodePosKK'],
            'fax' => ['label' => 'Fax','width' => 80, 'name' => 'fax'],
            'email' => ['label' => 'Email','width' => 80, 'name' => 'email'],
            'idSalesPeople' => ['label' => 'Id Sales People','width' => 100, 'name' => 'idSalesPeople'],

            'tanggalPesanan' => ['label' => 'Tgl Pesanan','width' => 90, 'name' => 'tanggalPesanan'],

            'createdTime' => ['label' => 'Create Time', 'width' => 130, 'name' => 'createdTime'],
        ];
    }

    public static function colGridImportDKStatusItems()
    {
        return [
            'idDokumenPengajuan' => ['label' => 'Id Dokumen Pengajuan','width' => 135, 'name' => 'idDokumenPengajuan'],
            'idSPK' => ['label' => 'Id SPK','width' => 130, 'name' => 'idSPK'],
            'jumlahDP' => ['label' => 'Jumlah DP','width' => 80, 'name' => 'jumlahDP', 'formatter' => 'number', 'align' => 'right'],
            'tenor' => ['label' => 'Tenor','width' => 60, 'name' => 'tenor'],
            'jumlahCicilan' => ['label' => 'Jumlah Cicilan','width' => 90, 'name' => 'jumlahCicilan', 'formatter' => 'number', 'align' => 'right'],
            'tanggalPengajuan' => ['label' => 'Tgl Pengajuan','width' => 90, 'name' => 'tanggalPengajuan'],
            'tanggalPembuatanPO' => ['label' => 'Tgl Buat PO','width' => 100, 'name' => 'tanggalPembuatanPO'],
            'tanggalPengirimanPOFinanceCompany' => ['label' => 'Tgl Kirim PO FinCoy','width' => 120, 'name' => 'tanggalPengirimanPOFinanceCompany'],
            'idFinanceCompany' => ['label' => 'Id FinCoy','width' => 90, 'name' => 'idFinanceCompany'],
            'namaFinanceCompany' => ['label' => 'Nama FinCoy','width' => 200, 'name' => 'namaFinanceCompany'],
            'idPOFinanceCompany' => ['label' => 'Id PO FinCoy','width' => 100, 'name' => 'idPOFinanceCompany'],
            'createdTime' => ['label' => 'Create Time','width' => 125, 'name' => 'createdTime'],
        ];
    }
    public static function colGridImportDKStnkItems()
    {
        return [
            'idSO' => ['label' => 'ID SO', 'width' => 120, 'name' => 'idSO'],
            'idSPK' => ['label' => 'ID SPK', 'width' => 90, 'name' => 'idSPK'],
        ];
    }


	public static function colGridListTransactions() {
		return [
			'NoTrans' => ['width' => 125, 'name' => 'NoTrans'],
            'DKno' => ['width' => 125, 'name' => 'DKno'],
            'Nominal' => ['width' => 125, 'name' => 'Nominal'],
            'TglTrans' => ['width' => 125, 'name' => 'TglTrans'],
            'Jenis' => ['width' => 125, 'name' => 'Jenis'],
            'NoGL' => ['width' => 125, 'name' => 'NoGL'],
            'JamTrans' => ['width' => 125, 'name' => 'JamTrans'],
		];
	}

	public function getMotor() {
		return $this->hasOne( Tmotor::class, [ 'DKNo' => 'DKNo' ] );
	}
	public function getSales() {
		return $this->hasOne( Tdsales::class, [ 'SalesKode' => 'SalesKode' ] );
	}
	public function getMotorTypes() {
		return $this->hasOne( Tdmotortype::class, [ 'MotorType' => 'MotorType' ] )
		            ->via( 'motor' );
	}
	public function getCustomer() {
		return $this->hasOne( Tdcustomer::class, [ 'CusKode' => 'CusKode' ] );
	}
	public function getSuratKonsumen() {
		return $this->hasOne( Ttsk::class, [ 'SKNo' => 'SKNo' ] )
		            ->via( 'motor' );
	}
	public function getData( $param = null ) {
		return $this->find()->dsTJualFillByNo()->where( " (ttdk.DKNo = :DKNo)", [
			':DKNo' => $this->DKNo,
		] )->asArray( true )->one();
	}
	/**
	 * {@inheritdoc}
	 * @return TtdkQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TtdkQuery( get_called_class() );
	}
}