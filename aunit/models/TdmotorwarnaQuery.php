<?php

namespace aunit\models;
use yii\helpers\ArrayHelper;

/**
 * This is the ActiveQuery class for [[Tdmotorwarna]].
 *
 * @see Tdmotorwarna
 */
class TdmotorwarnaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdmotorwarna[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdmotorwarna|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param array $params
     * @return array|Tdmotorwarna[]
     */
    public function combo($params = []) {
		$this->select('MotorWarna as label,MotorWarna as value' )
             ->groupBy( 'MotorWarna' )
             ->orderBy( 'MotorWarna' );

		if(isset($params['MotorType']))
            $this->where('MotorType = "'.$params['MotorType'].'"');

        return $this->asArray()
                    ->all();
	}
    public function motorwarna() {
        return ArrayHelper::map( $this->select( [ "CONCAT(MotorWarna) as label", "MotorWarna as value" ] )
            ->orderBy( 'MotorWarna' )
            ->asArray()
            ->all(), 'value', 'label' );
    }

	public function select2( $value, $label = [ 'MotorWarna' ], $orderBy = [], $where = [ 'condition' => null, 'params' => [] ] , $allOption = false ) {
		$option =
			$this->select( [ "CONCAT(" . implode( ",'||',", $label ) . ") as label", "$value as value" ] )
			     ->orderBy( $orderBy )
			     ->where( $where[ 'condition' ], $where[ 'params' ] )
			     ->asArray()
			     ->all();
		if ( $allOption ) {
			return array_merge( [ [ 'value' => '%', 'label' => 'Semua' ] ], $option );
		} else {
			return $option;
		}
	}
}
