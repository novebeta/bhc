<?php

namespace aunit\models;

use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
/**
 * This is the ActiveQuery class for [[Tsaldoaccount]].
 *
 * @see Tsaldoaccount
 */
class TsaldoaccountQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tsaldoaccount[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tsaldoaccount|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $TglDari
     * @param $TglSampai
     * @return array|Tsaldoaccount[]
     */
    public function listTutupBuku($TglDari, $TglSampai)
    {
        return Tsaldoaccount::find()
            ->select([ 'TglSaldo', 'DATE_FORMAT(TglSaldo, "%d/%m/%Y") TglSaldoFormatted' ])
            ->from(Tsaldoaccount::tableName())
            ->where(
                'TglSaldo >= :TglDari AND TglSaldo <= :TglSampai',
                [ ':TglDari' => $TglDari, ':TglSampai' => $TglSampai ]
            )
            ->groupBy('TglSaldo')
            ->orderBy('TglSaldo')
            ->asArray()
            ->all();
    }

	public function callSP( $post ) {
		$post[ 'UserID' ]       = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]    = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]   = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'Tanggal' ]      = date('Y-m-d', strtotime($post[ 'Tanggal' ] ?? date( 'Y-m-d' )));
		try {
			General::cCmd( "CALL fglTutupBuku(?,@Status,@Keterangan,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'Tanggal' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'status'     => $status,
			'keterangan' => str_replace(" ","&nbsp;",$keterangan)
		];
	}
}
