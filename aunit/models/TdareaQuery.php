<?php
namespace aunit\models;
use yii\helpers\ArrayHelper;
/**
 * This is the ActiveQuery class for [[Tdarea]].
 *
 * @see Tdarea
 */
class TdareaQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Tdarea[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Tdarea|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}

    /**
     * @param array $params
     * @return array|Tdarea[]
     */
    public function combo($params = []) {
        $field = isset($params['field']) ? $params['field'] : 'Kabupaten';
		$this->select("$field as label, $field as value" )
		            ->groupBy( $field )
		            ->orderBy( $field );

        if(isset($params['Provinsi']))
            $this->where('Provinsi = "'.$params['Provinsi'].'"');

        if(isset($params['Kabupaten']))
            $this->where('Kabupaten = "'.$params['Kabupaten'].'"');

        if(isset($params['Kecamatan']))
            $this->where('Kecamatan = "'.$params['Kecamatan'].'"');

        if(isset($params['Kelurahan']))
            $this->where('Kelurahan = "'.$params['Kelurahan'].'"');

        if(isset($params['KodePos']))
            $this->where('KodePos = "'.$params['KodePos'].'"');

        return $this->asArray()
            ->all();
	}

    public function kabupaten() {
        return ArrayHelper::map( $this->select( [ "CONCAT(Kabupaten) as label", "Kabupaten as value" ] )
            ->orderBy( 'Kabupaten' )
            ->asArray()
            ->all(), 'value', 'label' );
    }

    public function kecamatan() {
        return ArrayHelper::map( $this->select( [ "CONCAT(Kecamatan) as label", "Kecamatan as value" ] )
            ->orderBy( 'Kecamatan' )
            ->asArray()
            ->all(), 'value', 'label' );
    }

    public function kelurahan() {
        return ArrayHelper::map( $this->select( [ "CONCAT(Kelurahan) as label", "Kelurahan as value" ] )
            ->orderBy( 'Kelurahan' )
            ->asArray()
            ->all(), 'value', 'label' );
    }
    public function KodePos() {
        return ArrayHelper::map( $this->select( [ "CONCAT(KodePos) as label", "KodePos as value" ] )
            ->orderBy( 'KodePos' )
            ->asArray()
            ->all(), 'value', 'label' );
    }


	public function select2( $value, $label, $orderBy, $where = [ 'condition' => null, 'params' => [] ] , $allOption = false) {
		$option =
			$this->select( [ "CONCAT(" . implode( ",'||',", $label ) . ") as label", "$value as value" ] )
			     ->orderBy( $orderBy )
				->groupBy( $value )
			     ->where( $where[ 'condition' ], $where[ 'params' ] )
			     ->asArray()
			     ->all();
		if ( $allOption ) {
			return array_merge( [ [ 'value' => '%', 'label' => 'Semua' ] ], $option );
		} else {
			return $option;
		}
	}


}
