<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "ttglhpit".
 *
 * @property string $NoGL
 * @property string $TglGL
 * @property string $GLLink
 * @property string $NoAccount
 * @property string $HPJenis
 * @property string $HPNilai
 */
class Ttglhpit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttglhpit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NoGL', 'TglGL', 'GLLink', 'NoAccount', 'HPNilai'], 'required'],
            [['TglGL'], 'safe'],
            [['HPNilai'], 'number'],
            [['NoGL', 'NoAccount'], 'string', 'max' => 10],
            [['GLLink'], 'string', 'max' => 30],
            [['HPJenis'], 'string', 'max' => 7],
            [['NoGL', 'TglGL', 'GLLink', 'NoAccount', 'HPNilai'], 'unique', 'targetAttribute' => ['NoGL', 'TglGL', 'GLLink', 'NoAccount', 'HPNilai']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'NoGL' => 'No Gl',
            'TglGL' => 'Tgl Gl',
            'GLLink' => 'Gl Link',
            'NoAccount' => 'No Account',
            'HPJenis' => 'Hp Jenis',
            'HPNilai' => 'Hp Nilai',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtglhpitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtglhpitQuery(get_called_class());
    }
}
