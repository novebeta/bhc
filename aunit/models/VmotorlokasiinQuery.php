<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Vmotorlokasiin]].
 *
 * @see Vmotorlokasiin
 */
class VmotorlokasiinQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Vmotorlokasiin[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Vmotorlokasiin|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
