<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Tuserlogdb]].
 *
 * @see Tuserlogdb
 */
class TuserlogdbQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tuserlogdb[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tuserlogdb|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
