<?php

namespace aunit\models;

/**
 * This is the model class for table "ttpiit".
 *
 * @property string $PINo
 * @property int $PIAuto
 * @property string $BrgKode
 * @property string $PIQty
 * @property string $PIHrgBeli Standard Cost
 * @property string $PIHrgJual Harga Beli Real
 * @property string $PIDiscount
 * @property string $PIPajak
 * @property string $PSNo
 * @property string $LokasiKode
 */
class Ttpiit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttpiit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PINo', 'PIAuto'], 'required'],
            [['PIAuto'], 'integer'],
            [['PIQty', 'PIHrgBeli', 'PIHrgJual', 'PIDiscount', 'PIPajak'], 'number'],
            [['PINo', 'PSNo'], 'string', 'max' => 10],
            [['BrgKode'], 'string', 'max' => 20],
            [['LokasiKode'], 'string', 'max' => 15],
            [['PINo', 'PIAuto'], 'unique', 'targetAttribute' => ['PINo', 'PIAuto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PINo'       => 'Pi No',
            'PIAuto'     => 'Pi Auto',
            'BrgKode'    => 'Brg Kode',
            'PIQty'      => 'Pi Qty',
            'PIHrgBeli'  => 'Pi Hrg Beli',
            'PIHrgJual'  => 'Pi Hrg Jual',
            'PIDiscount' => 'Pi Discount',
            'PIPajak'    => 'Pi Pajak',
            'PSNo'       => 'Ps No',
            'LokasiKode' => 'Lokasi Kode',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtpiitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtpiitQuery(get_called_class());
    }
}
