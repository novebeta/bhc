<?php

namespace aunit\models;

use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
/**
 * This is the ActiveQuery class for [[Ttcfit]].
 *
 * @see Ttcfit
 */
class TtcfitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttcfit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttcfit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function callSP( $post ) {
		$post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ] = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'CFNo' ]       = $post[ 'CFNo' ] ?? '--';
		$post[ 'FBHarga' ]    = floatval( $post[ 'FBHarga' ] ?? 0 );
		$post[ 'MotorAutoN' ]     = $post[ 'MotorAutoN' ]?? 0;
		$post[ 'MotorNoMesin' ]       = $post[ 'MotorNoMesin' ] ?? '--';
		$post[ 'SPKNo' ]       = $post[ 'SPKNo' ] ?? '--';
		$post[ 'MotorAutoNOLD' ]     = $post[ 'MotorAutoNOLD' ] ?? 0;
		$post[ 'MotorNoMesinOLD' ]       = $post[ 'MotorNoMesinOLD' ] ?? '--';
		try {
			General::cCmd( "SET @CFNo = ?;" )->bindParam( 1, $post[ 'CFNo' ] )->execute();
			General::cCmd( "CALL fcfit(?,@Status,@Keterangan,?,?,?,@CFNo,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'FBHarga' ] )
			       ->bindParam( 6, $post[ 'MotorAutoN' ] )
			       ->bindParam( 7, $post[ 'MotorNoMesin' ] )
			       ->bindParam( 8, $post[ 'SPKNo' ] )
			       ->bindParam( 9, $post[ 'MotorAutoNOLD' ] )
			       ->bindParam( 10, $post[ 'MotorNoMesinOLD' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @CFNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'CFNo'       => $post[ 'CFNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$CFNo       = $exe[ '@CFNo' ] ?? $post[ 'CFNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'CFNo'       => $CFNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
}
