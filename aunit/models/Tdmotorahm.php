<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "tdmotorahm".
 *
 * @property string $KodeAHM
 * @property string $MotorNoMesin
 */
class Tdmotorahm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdmotorahm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KodeAHM'], 'string', 'max' => 3],
            [['MotorNoMesin'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'KodeAHM' => 'Kode Ahm',
            'MotorNoMesin' => 'Motor No Mesin',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TdmotorahmQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdmotorahmQuery(get_called_class());
    }
}
