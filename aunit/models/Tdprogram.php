<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "tdprogram".
 *
 * @property string $ProgramNama
 * @property string $MotorType
 * @property string $PrgSubsSupplier
 * @property string $PrgSubsDealer
 * @property string $PrgSubsFincoy
 * @property string $PrgSubsTotal
 * @property string $PrgTglAwal
 * @property string $PrgTglAkhir
 */
class Tdprogram extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdprogram';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ProgramNama', 'MotorType'], 'required'],
            [['PrgSubsSupplier', 'PrgSubsDealer', 'PrgSubsFincoy', 'PrgSubsTotal'], 'number'],
            [['PrgTglAwal', 'PrgTglAkhir'], 'safe'],
            [['ProgramNama'], 'string', 'max' => 25],
            [['MotorType'], 'string', 'max' => 50],
            [['ProgramNama', 'MotorType'], 'unique', 'targetAttribute' => ['ProgramNama', 'MotorType']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
	        'ProgramNama' => 'Nama Program',
	        'MotorType' => 'Type Motor',
	        'PrgTglAwal' => 'Awal',
	        'PrgTglAkhir' => 'Akhir',
	        'PrgSubsSupplier' => 'Subsidi Supplier',
	        'PrgSubsDealer' => 'Subsidi Dealer',
	        'PrgSubsFincoy' => 'Subsidi Fincoy',
	        'PrgSubsTotal' => 'Subsidi Total'
        ];
    }

	public static function colGrid()
	{
		return [
            'ProgramNama' => [
                'label' => 'Nama Program',
                'width' => 200,
                'name' => 'ProgramNama'
            ],
            'MotorType' => [
                'label' => 'Type Motor',
                'width' => 200,
                'name' => 'MotorType'
            ],
            'PrgTglAwal' => [
                'label' => 'Awal',
                'width' => 100,
                'name' => 'PrgTglAwal'
            ],
            'PrgTglAkhir' => [
                'label' => 'Akhir',
                'width' => 90,
                'name' => 'PrgTglAkhir'
            ],
            'PrgSubsSupplier' => [
                'label' => 'Subsidi Supplier',
                'width' => 110,
                'name' => 'PrgSubsSupplier'
            ],
            'PrgSubsDealer' => [
                'label' => 'Subsidi Dealer',
                'width' => 110,
                'name' => 'PrgSubsDealer'
            ],
            'PrgSubsTotal' => [
                'label' => 'Subsidi Total',
                'width' => 110,
                'name' => 'PrgSubsTotal'
            ],
		];
	}

    /**
     * {@inheritdoc}
     * @return TdprogramQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdprogramQuery(get_called_class());
    }
}
