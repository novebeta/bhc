<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Tvpiutangdebet]].
 *
 * @see Tvpiutangdebet
 */
class TvpiutangdebetQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tvpiutangdebet[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tvpiutangdebet|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
