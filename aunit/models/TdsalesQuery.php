<?php

namespace aunit\models;

use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the ActiveQuery class for [[Tdsale]].
 *
 * @see Tdsales
 */
class TdsalesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/
    /**
     * {@inheritdoc}
     * @return Tdsales|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function combo()
    {
        return $this->select(["CONCAT(SalesKode,' - ',SalesNama) as label", "SalesKode as value"])
            ->orderBy('SalesKode')
            ->asArray()
            ->all();
    }

    /**
     * {@inheritdoc}
     * @return Tdsales[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    public function petugas()
    {
        return ArrayHelper::map($this->select(["CONCAT(SalesNama) as label", "SalesKode as value"])
            ->where(['SalesStatus' => 'A'])
            ->orderBy('SalesStatus, SalesKode')
            ->asArray()
            ->all(), 'value', 'label');
    }

    public function select2($value, $label = ['SalesNama'], $orderBy = [], $where = ['condition' => null, 'params' => []], $allOption = false)
    {
        $option =
            $this->select(["$value as value", "CONCAT(" . implode(",'||',", $label) . ") as label", 'TeamKode'])
                ->orderBy($orderBy)
                ->where($where['condition'], $where['params'])
                ->asArray()
                ->all();
        if ($allOption) {
            return array_merge([['value' => '%', 'label' => 'Semua']], $option);
        } else {
            return $option;
        }
    }

    public function CekPiutangKonsSales($dtpDKTgl, $SalesKode)
    {
        $TglAwal = date_create($dtpDKTgl);
        date_sub($TglAwal, date_interval_create_from_date_string('360 days'));
        $TglAwal = $TglAwal->format('Y-m-d');
        $pesan = '';
        $Individu = 10;
        $Kolektif = 15;
        $tdteam = Tdteam::find()->where(['TeamStatus' => 'A'])->one();
        $Individu = $tdteam->TeamIndividu;
        $Kolektif = $tdteam->TeamKolektif;
        $TglAkhir = date_create($dtpDKTgl);
        date_sub($TglAkhir, date_interval_create_from_date_string($Individu . ' days'));
        $TglAkhir = $TglAkhir->format('Y-m-d');
        $sqlcomm = Yii::$app->db->createCommand("SELECT SKNo, SKTgl, SalesKode, DebetGL, Bayar, DebetGL - Bayar AS Sisa FROM 
        (SELECT ttsk.SKNo, ttsk.SKTgl, SalesKode, SUM(DebetGL) AS DebetGL FROM tmotor
        INNER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo
        INNER JOIN ttdk ON ttdk.DKNo = tmotor.DKNo
        INNER JOIN ttgeneralledgerhd ON ttsk.NoGL = ttgeneralledgerhd.NoGL
        INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL
        INNER JOIN tdcustomer ON tdcustomer.CusKode = ttdk.CusKode
        WHERE SalesKode = :MySalesKode AND ttgeneralledgerit.NoAccount = '11060100' AND ttsk.SKTGL BETWEEN :MyTgl1 AND :MyTgl2
        AND CusKodeKons IN (
        'Individual Customer (Regular)', 
        'Kolektif Customer (Leasing)',
        'Individual Customer (Kolektif)',
        'Individual Customer (Joint Promo)'
        ) 
        GROUP BY SKNo) ttsk
        LEFT OUTER JOIN 
        (SELECT GLLink,  SUM(HPNilai) AS Bayar FROM ttglhpit WHERE  HPJenis = 'Piutang' AND NoAccount = '11060100' AND TGlGL >= :MyTgl1 GROUP BY GLLink) ttglhpit
        ON ttglhpit.GLLink = ttsk.SKNo
        WHERE DebetGL - Bayar > 0", [':MyTgl1' => $TglAwal, ':MyTgl2' => $TglAkhir, ':MySalesKode' => $SalesKode]);
                $SKNoArr = $sqlcomm->queryColumn();
                $pesan .= implode(', ', $SKNoArr);
                $TglAkhir = date_create($dtpDKTgl);
                date_sub($TglAkhir, date_interval_create_from_date_string($Kolektif . ' days'));
                $TglAkhir = $TglAkhir->format('Y-m-d');
                $sqlcomm = Yii::$app->db->createCommand("SELECT SKNo, SKTgl, SalesKode, DebetGL, Bayar, DebetGL - Bayar AS Sisa FROM 
        (SELECT ttsk.SKNo, ttsk.SKTgl, SalesKode, SUM(DebetGL) AS DebetGL FROM tmotor
        INNER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo
        INNER JOIN ttdk ON ttdk.DKNo = tmotor.DKNo
        INNER JOIN ttgeneralledgerhd ON ttsk.NoGL = ttgeneralledgerhd.NoGL
        INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL
        INNER JOIN tdcustomer ON tdcustomer.CusKode = ttdk.CusKode
        WHERE SalesKode = :MySalesKode AND ttgeneralledgerit.NoAccount = '11060100' AND ttsk.SKTGL BETWEEN :MyTgl1 AND :MyTgl2
        AND CusKodeKons IN (
        'Kolektif Customer (Bank)',
        'Kolektif Customer (Koperasi)',
        'Group Customer'
        ) 
        GROUP BY SKNo) ttsk
        LEFT OUTER JOIN 
        (SELECT GLLink,  SUM(HPNilai) AS Bayar FROM ttglhpit WHERE  HPJenis = 'Piutang' AND NoAccount = '11060100' AND TGlGL >= :MyTgl1 GROUP BY GLLink) ttglhpit
        ON ttglhpit.GLLink = ttsk.SKNo
        WHERE DebetGL - Bayar > 0", [':MyTgl1' => $TglAwal, ':MyTgl2' => $TglAkhir, ':MySalesKode' => $SalesKode]);
                $SKNoArr = $sqlcomm->queryColumn();
                $pesan .= implode(', ', $SKNoArr);
                $sales = Tdsales::findOne($SalesKode);
                if ($sales != null && $sales->SalesPass == 1) {
                    $pesan = '';
                }
                return $pesan;
            }

    public function GetJoinData() {
        return $this->select( new Expression( "
        SalesKode, SalesNama, SalesAlamat as CusAlamat, SalesTelepon, SalesStatus, SalesKeterangan"));
    }
}
