<?php

namespace aunit\models;

use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttsmhd]].
 *
 * @see Ttsmhd
 */
class TtsmhdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttsmhd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttsmhd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	public function GetDataByNo(){
		return $this->select( new Expression("ttsmhd.LokasiAsal, ttsmhd.LokasiTujuan, ttsmhd.SMMemo, ttsmhd.SMNo, 
							ttsmhd.SMTgl, ttsmhd.SMTotal, ttsmhd.UserID, IFNULL(tdlokasi.LokasiNama, '--') AS LokasiNamaAsal, 
							IFNULL(tdlokasi_1.LokasiNama, '--') AS LokasiNamaTujuan, ttsmhd.SMJam, IFNULL(ttpbhd.PBNo, '--') AS PBNo"))
		            ->from("ttsmhd")
		            ->join("LEFT OUTER JOIN","tdlokasi","ttsmhd.LokasiAsal = tdlokasi.LokasiKode")
		            ->join("LEFT OUTER JOIN","tdlokasi AS tdlokasi_1","ttsmhd.LokasiTujuan = tdlokasi_1.LokasiKode")
		            ->join("LEFT OUTER JOIN","ttpbhd","ttsmhd.SMNo = ttpbhd.SMNo");
	}

    public function callSP( $post, $jenis) {
    	$spArr = [
    		'MutasiPOS' => 'fsmhd',
    		'RepairIN' => 'frihd',
    		'RepairOUT'=> 'frohd',
	    ];
        $post[ 'UserID' ]       = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]    = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'LokasiKode' ]   = Menu::getUserLokasi()[ 'LokasiKode' ];
        $post[ 'SMNo' ]         = $post[ 'SMNo' ] ?? '--';
        $post[ 'SMNoView' ]     = $post[ 'SMNoView' ] ?? '--';
        $post[ 'SMTgl' ]        = date('Y-m-d', strtotime($post[ 'SMTgl' ] ?? date( 'Y-m-d' )));
        $post[ 'LokasiAsal' ]   = $post[ 'LokasiAsal' ] ?? '--';
        $post[ 'LokasiTujuan' ] = $post[ 'LokasiTujuan' ] ?? '--';
        $post[ 'SMMemo' ]       = $post[ 'SMMemo' ] ?? '--';
        $post[ 'SMTotal' ]      = floatval( $post[ 'SMTotal' ] ?? 0 );
        $post[ 'SMJam' ]        = date('Y-m-d H:i:s', strtotime($post[ 'SMJam' ] ?? date( 'Y-m-d H:i:s' )));
        try {
        General::cCmd( "SET @SMNo = ?;" )->bindParam( 1, $post[ 'SMNo' ] )->execute();
        General::cCmd( "CALL ".$spArr[$jenis]."(?,@Status,@Keterangan,?,?,?,@SMNo,?,?,?,?,?,?,?);" )
            ->bindParam( 1, $post[ 'Action' ] )
            ->bindParam( 2, $post[ 'UserID' ] )
            ->bindParam( 3, $post[ 'KodeAkses' ] )
            ->bindParam( 4, $post[ 'LokasiKode' ] )
            ->bindParam( 5, $post[ 'SMNoView' ] )
            ->bindParam( 6, $post[ 'SMTgl' ] )
            ->bindParam( 7, $post[ 'LokasiAsal' ] )
            ->bindParam( 8, $post[ 'LokasiTujuan' ] )
            ->bindParam( 9, $post[ 'SMMemo' ] )
            ->bindParam( 10, $post[ 'SMTotal' ] )
            ->bindParam( 11, $post[ 'SMJam' ] )
            ->execute();
        $exe = General::cCmd( "SELECT @SMNo,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'SMNo'       => $post[ 'SMNo' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $SMNo = $exe[ '@SMNo' ] ?? $post['SMNo'];
        $status = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : ['.$post['Action'].'] Keterangan NULL';
        return [
            'SMNo'       => $SMNo,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
