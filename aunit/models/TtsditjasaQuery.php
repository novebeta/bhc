<?php

namespace aunit\models;

use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttsditjasa]].
 *
 * @see Ttsditjasa
 */
class TtsditjasaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttsditjasa[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttsditjasa|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]           = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]        = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]          = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'SDNo' ]             = $post[ 'SDNo' ] ?? '--';
        $post[ 'SDAuto' ]           = floatval( $post[ 'SDAuto' ] ?? 0 );
        $post[ 'JasaKode' ]         = $post[ 'JasaKode' ] ?? '--';
        $post[ 'SDWaktuKerja' ]     = floatval( $post[ 'SDWaktuKerja' ] ?? 0 );
        $post[ 'SDWaktuSatuan' ]    = $post[ 'SDWaktuSatuan' ] ?? '--';
        $post[ 'SDWaktuKerjaMenit'] = floatval( $post[ 'SDWaktuKerjaMenit' ] ?? 0 );
        $post[ 'SDHargaBeli' ]      = floatval( $post[ 'SDHargaBeli' ] ?? 0 );
        $post[ 'SDHrgJual' ]        = floatval( $post[ 'SDHrgJual' ] ?? 0 );
        $post[ 'SDJualDisc' ]       = floatval( $post[ 'SDJualDisc' ] ?? 0 );
        $post[ 'SDPajak' ]          = floatval( $post[ 'SDPajak' ] ?? 0 );
        $post[ 'SDNoOLD' ]          = $post[ 'SDNoOLD' ] ?? '--';
        $post[ 'SDAutoOLD' ]        = floatval( $post[ 'SDAutoOLD' ] ?? 0 );
        $post[ 'JasaKodeOLD' ]      = $post[ 'JasaKodeOLD' ] ?? '--';
        try {
            General::cCmd( "CALL fsditjasa(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'SDNo' ] )
                ->bindParam( 6, $post[ 'SDAuto' ] )
                ->bindParam( 7, $post[ 'JasaKode' ] )
                ->bindParam( 8, $post[ 'SDWaktuKerja' ] )
                ->bindParam( 9, $post[ 'SDWaktuSatuan' ] )
                ->bindParam( 10, $post[ 'SDWaktuKerjaMenit' ] )
                ->bindParam( 11, $post[ 'SDHrgBeli' ] )
                ->bindParam( 12, $post[ 'SDHrgJual' ] )
                ->bindParam( 13, $post[ 'SDJualDisc' ] )
                ->bindParam( 14, $post[ 'SDPajak' ] )
                ->bindParam( 15, $post[ 'SDNoOLD' ] )
                ->bindParam( 16, $post[ 'SDAutoOLD' ] )
                ->bindParam( 17, $post[ 'JasaKodeOLD' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
