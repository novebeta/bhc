<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "tzcompany".
 *
 * @property string $PerusahaanNama
 * @property string $PerusahaanSlogan
 * @property string $PerusahaanAlamat
 * @property string $PerusahaanTelepon
 * @property string $PrinterDotMatrix
 * @property string $PrinterInkJet
 * @property string $PrinterView
 * @property string $ReleaseDate
 */
class Tzcompany extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tzcompany';
    }

	public static function primaryKey()
	{
		return ['PerusahaanNama'];
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ReleaseDate'], 'safe'],
            [['PerusahaanNama', 'PerusahaanSlogan', 'PerusahaanTelepon', 'PrinterDotMatrix', 'PrinterInkJet', 'PrinterView'], 'string', 'max' => 50],
            [['PerusahaanAlamat'], 'string', 'max' => 75],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PerusahaanNama' => 'Perusahaan Nama',
            'PerusahaanSlogan' => 'Perusahaan Slogan',
            'PerusahaanAlamat' => 'Perusahaan Alamat',
            'PerusahaanTelepon' => 'Perusahaan Telepon',
            'PrinterDotMatrix' => 'Printer Dot Matrix',
            'PrinterInkJet' => 'Printer Ink Jet',
            'PrinterView' => 'Printer View',
            'ReleaseDate' => 'Release Date',
        ];
    }
}
