<?php
namespace aunit\models;
use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
/**
 * This is the ActiveQuery class for [[Ttin]].
 *
 * @see Ttin
 */
class TtinQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttin[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttin|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function combo() {
		return ArrayHelper::map( $this->select( [ "CONCAT(INNo) as label", "INNo as value" ] )
		                              ->orderBy( 'INNo' )
		                              ->asArray()
		                              ->all(), 'value', 'label' );
	}
	public function dsTJual() {
		return $this->select( new Expression( "ttin.INNo, ttin.INTgl, ttin.SalesKode, ttin.CusKode, ttin.LeaseKode, ttin.INJenis, ttin.INMemo, ttin.INDP, ttin.UserID, IFNULL(tdsales.SalesNama, '--') AS SalesNama, IFNULL(tdleasing.LeaseNama, '--') AS LeaseNama, ttin.INHarga,
                         ttin.MotorType, ttin.MotorWarna, ttin.DKNo, IFNULL(tdcustomer.CusKTP, '--') AS CusKTP, IFNULL(tdcustomer.CusNama, '--') AS CusNama, IFNULL(tdcustomer.CusAlamat, '--') AS CusAlamat, IFNULL(tdcustomer.CusRT, '--')
                         AS CusRT, IFNULL(tdcustomer.CusRW, '--') AS CusRW, IFNULL(tdcustomer.CusProvinsi, '--') AS CusProvinsi, IFNULL(tdcustomer.CusKabupaten, '') AS CusKabupaten, IFNULL(tdcustomer.CusKecamatan, '--') AS CusKecamatan,
                         IFNULL(tdcustomer.CusKelurahan, '--') AS CusKelurahan, IFNULL(tdcustomer.CusKodePos, '--') AS CusKodePos, IFNULL(tdcustomer.CusTelepon, '--') AS CusTelepon, IFNULL(tdcustomer.CusSex, '--') AS CusSex,
                         IFNULL(tdcustomer.CusTempatLhr, '--') AS CusTempatLhr, IFNULL(tdcustomer.CusTglLhr, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS CusTglLhr, IFNULL(tdcustomer.CusAgama, '--') AS CusAgama,
                         IFNULL(tdcustomer.CusPekerjaan, '--') AS CusPekerjaan, IFNULL(tdcustomer.CusPendidikan, '--') AS CusPendidikan, IFNULL(tdcustomer.CusPengeluaran, '--') AS CusPengeluaran, IFNULL(tdcustomer.CusEmail, '--') AS CusEmail,
                         IFNULL(tdcustomer.CusKodeKons, '--') AS CusKodeKons, IFNULL(tdcustomer.CusStatusHP, '--') AS CusStatusHP, IFNULL(tdcustomer.CusStatusRumah, '--') AS CusStatusRumah, tdcustomer.CusTelepon2, tdcustomer.CusKK, tdcustomer.CusNPWP" ) )
		            ->join( "LEFT OUTER JOIN", "tdleasing", "ttin.LeaseKode = tdleasing.LeaseKode" )
		            ->join( "LEFT OUTER JOIN", "tdsales", "ttin.SalesKode = tdsales.SalesKode" )
		            ->join( "LEFT OUTER JOIN", "tdcustomer", "ttin.CusKode = tdcustomer.CusKode" )
		            ->join( "LEFT OUTER JOIN", "ttdk", "ttin.INNo = ttdk.INNo" );
	}
	public function GetJoinData() {
		$subqueryjoin = ( new \yii\db\Query() )
			->select( new Expression(
				'vtkm.KMLink, vtkm.KMJenis, SUM(vtkm.KMNominal) AS Inden'
			) )
			->from( 'vtkm' )
			->groupBy( 'vtkm.KMLink, vtkm.KMJenis' );
		return $this->select( new Expression( "ttin.INNo,
                        ttin.INTgl,
                        ttin.SalesKode,
                        ttin.CusKode,
                        ttin.LeaseKode,
                        ttin.INJenis,
                        ttin.DKNo,
                        ttin.INMemo,
                        ttin.INDP,
                        ttin.UserID,
                        IFNULL(tdcustomer.CusNama, '--') AS CusNama,
                        tdcustomer.CusAlamat,
                        tdcustomer.CusRT,
                        tdcustomer.CusRW,
                        tdcustomer.CusKelurahan,
                        tdcustomer.CusKecamatan,
                        IFNULL(
                            tdcustomer.CusKabupaten,
                            '--'
                        ) AS CusKabupaten,
                        tdcustomer.CusTelepon,
                        IFNULL(tdsales.SalesNama, '--') AS SalesNama,
                        IFNULL(tdleasing.LeaseNama, '--') AS LeaseNama,
                        ttin.INHarga,
                        ttin.MotorType,
                        ttin.MotorWarna,
                        IFNULL(Inden, 0) AS IndenSudah,
                        (ttin.INDP - IFNULL(Inden, 0)) AS IndenSisa" ) )
		            ->join( "INNER JOIN", "tdleasing", "ttin.LeaseKode = tdleasing.LeaseKode" )
		            ->join( "INNER JOIN", "tdsales", "ttin.SalesKode = tdsales.SalesKode" )
		            ->join( "INNER JOIN", "tdcustomer", "ttin.CusKode = tdcustomer.CusKode" )
		            ->leftJoin( [ 'ttkM' => $subqueryjoin ], 'ttin.INNo = ttkM.KMLINK' );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]         = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]      = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]     = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'INNo' ]           = $post[ 'INNo' ] ?? '--';
		$post[ 'INNoBaru' ]       = $post[ 'INNo' ];
		$post[ 'INTgl' ]          = date('Y-m-d', strtotime($post[ 'INTgl' ] ?? date( 'Y-m-d' )));
		$post[ 'INDP' ]           = floatval( $post[ 'INDP' ] ?? 0 );
		$post[ 'INHarga' ]        = floatval( $post[ 'INHarga' ] ?? 0 );
		$post[ 'DKNo' ]           = $post[ 'DKNo' ] ?? '--';
		$post[ 'SalesKode' ]      = $post[ 'SalesKode' ] ?? '--';
		$post[ 'CusKode' ]        = $post[ 'CusKode' ] ?? '--';
		$post[ 'CusNama' ]        = $post[ 'CusNama' ] ?? '--';
		$post[ 'CusKTP' ]         = $post[ 'CusKTP' ] ?? '--';
		$post[ 'CusKabupaten' ]   = $post[ 'CusKabupaten' ] ?? '--';
		$post[ 'CusAlamat' ]      = $post[ 'CusAlamat' ] ?? '--';
		$post[ 'CusRT' ]          = $post[ 'CusRT' ] ?? '--';
		$post[ 'CusRW' ]          = $post[ 'CusRW' ] ?? '--';
		$post[ 'CusKecamatan' ]   = $post[ 'CusKecamatan' ] ?? '--';
		$post[ 'CusTelepon' ]     = $post[ 'CusTelepon' ] ?? '--';
		$post[ 'CusStatusHP' ]    = $post[ 'CusStatusHP' ] ?? '--';
		$post[ 'CusKodePos' ]     = $post[ 'CusKodePos' ] ?? '--';
		$post[ 'CusKelurahan' ]   = $post[ 'CusKelurahan' ] ?? '--';
		$post[ 'CusEmail' ]       = $post[ 'CusEmail' ] ?? '--';
		$post[ 'CusStatusRumah' ] = $post[ 'CusStatusRumah' ] ?? '--';
		$post[ 'CusKodeKons' ]    = $post[ 'CusKodeKons' ] ?? '--';
		$post[ 'CusSex' ]         = $post[ 'CusSex' ] ?? '--';
		$post[ 'CusTempatLhr' ]   = $post[ 'CusTempatLhr' ] ?? '--';
		$post[ 'CusTglLhr' ]      = date('Y-m-d', strtotime($post[ 'CusTglLhr' ] ?? date( 'Y-m-d' )));
		$post[ 'CusAgama' ]       = $post[ 'CusAgama' ] ?? '--';
		$post[ 'CusPekerjaan' ]   = $post[ 'CusPekerjaan' ] ?? '--';
		$post[ 'CusPendidikan' ]  = $post[ 'CusPendidikan' ] ?? '--';
		$post[ 'CusPengeluaran' ] = $post[ 'CusPengeluaran' ] ?? '--';
		$post[ 'CusKK' ]          = $post[ 'CusKK' ] ?? '--';
		$post[ 'CusNPWP' ]        = $post[ 'CusNPWP' ] ?? '--';
		$post[ 'CusTelepon2' ]    = $post[ 'CusTelepon2' ] ?? '--';
		$post[ 'LeaseKode' ]      = $post[ 'LeaseKode' ] ?? '--';
		$post[ 'INJenis' ]        = $post[ 'INJenis' ] ?? '--';
		$post[ 'INMemo' ]         = $post[ 'INMemo' ] ?? '--';
		$post[ 'MotorType' ]      = $post[ 'MotorType' ] ?? '--';
		$post[ 'MotorWarna' ]     = $post[ 'MotorWarna' ] ?? '--';
		$post[ 'INJenis' ]        = $post[ 'INJenis' ] ?? 'Tunai';
		try {
			General::cCmd( "SET @INNo = ?;" )->bindParam( 1, $post[ 'INNo' ] )->execute();
			General::cCmd( "CALL fin(?,@Status,@Keterangan,?,?,?,@INNo,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'INNoBaru' ] )
			       ->bindParam( 6, $post[ 'SalesKode' ] )
			       ->bindParam( 7, $post[ 'DKNo' ] )
			       ->bindParam( 8, $post[ 'INTgl' ] )
			       ->bindParam( 9, $post[ 'LeaseKode' ] )
			       ->bindParam( 10, $post[ 'CusNama' ] )
			       ->bindParam( 11, $post[ 'CusKode' ] )
			       ->bindParam( 12, $post[ 'CusKTP' ] )
			       ->bindParam( 13, $post[ 'CusKabupaten' ] )
			       ->bindParam( 14, $post[ 'CusAlamat' ] )
			       ->bindParam( 15, $post[ 'CusRT' ] )
			       ->bindParam( 16, $post[ 'CusRW' ] )
			       ->bindParam( 17, $post[ 'CusKecamatan' ] )
			       ->bindParam( 18, $post[ 'CusTelepon' ] )
			       ->bindParam( 19, $post[ 'CusStatusHP' ] )
			       ->bindParam( 20, $post[ 'CusKodePos' ] )
			       ->bindParam( 21, $post[ 'CusKelurahan' ] )
			       ->bindParam( 22, $post[ 'CusEmail' ] )
			       ->bindParam( 23, $post[ 'CusStatusRumah' ] )
			       ->bindParam( 24, $post[ 'CusKodeKons' ] )
			       ->bindParam( 25, $post[ 'CusSex' ] )
			       ->bindParam( 26, $post[ 'CusTempatLhr' ] )
			       ->bindParam( 27, $post[ 'CusTglLhr' ] )
			       ->bindParam( 28, $post[ 'CusAgama' ] )
			       ->bindParam( 29, $post[ 'CusPekerjaan' ] )
			       ->bindParam( 30, $post[ 'CusPendidikan' ] )
			       ->bindParam( 31, $post[ 'CusPengeluaran' ] )
			       ->bindParam( 32, $post[ 'CusKK' ] )
			       ->bindParam( 33, $post[ 'CusNPWP' ] )
			       ->bindParam( 34, $post[ 'CusTelepon2' ] )
			       ->bindParam( 35, $post[ 'MotorType' ] )
			       ->bindParam( 36, $post[ 'MotorWarna' ] )
			       ->bindParam( 37, $post[ 'INHarga' ] )
			       ->bindParam( 38, $post[ 'INMemo' ] )
			       ->bindParam( 39, $post[ 'INDP' ] )
			       ->bindParam( 40, $post[ 'INJenis' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @INNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'INNo'       => $post[ 'INNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$INNo       = $exe[ '@INNo' ] ?? $post[ 'INNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'INNo'       => $INNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
}


//SELECT        ttin.INNo, ttin.INTgl, ttin.SalesKode, ttin.CusKode, ttin.LeaseKode, ttin.INJenis, ttin.INMemo, ttin.INDP, ttin.UserID, IFNULL(tdsales.SalesNama, '--') AS SalesNama, IFNULL(tdleasing.LeaseNama, '--') AS LeaseNama, ttin.INHarga,
//                         ttin.MotorType, ttin.MotorWarna, ttin.DKNo, IFNULL(tdcustomer.CusKTP, '--') AS CusKTP, IFNULL(tdcustomer.CusNama, '--') AS CusNama, IFNULL(tdcustomer.CusAlamat, '--') AS CusAlamat, IFNULL(tdcustomer.CusRT, '--')
//                         AS CusRT, IFNULL(tdcustomer.CusRW, '--') AS CusRW, IFNULL(tdcustomer.CusProvinsi, '--') AS CusProvinsi, IFNULL(tdcustomer.CusKabupaten, '') AS CusKabupaten, IFNULL(tdcustomer.CusKecamatan, '--') AS CusKecamatan,
//                         IFNULL(tdcustomer.CusKelurahan, '--') AS CusKelurahan, IFNULL(tdcustomer.CusKodePos, '--') AS CusKodePos, IFNULL(tdcustomer.CusTelepon, '--') AS CusTelepon, IFNULL(tdcustomer.CusSex, '--') AS CusSex,
//                         IFNULL(tdcustomer.CusTempatLhr, '--') AS CusTempatLhr, IFNULL(tdcustomer.CusTglLhr, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS CusTglLhr, IFNULL(tdcustomer.CusAgama, '--') AS CusAgama,
//                         IFNULL(tdcustomer.CusPekerjaan, '--') AS CusPekerjaan, IFNULL(tdcustomer.CusPendidikan, '--') AS CusPendidikan, IFNULL(tdcustomer.CusPengeluaran, '--') AS CusPengeluaran, IFNULL(tdcustomer.CusEmail, '--') AS CusEmail,
//                         IFNULL(tdcustomer.CusKodeKons, '--') AS CusKodeKons, IFNULL(tdcustomer.CusStatusHP, '--') AS CusStatusHP, IFNULL(tdcustomer.CusStatusRumah, '--') AS CusStatusRumah, tdcustomer.CusTelepon2
//FROM            ttin LEFT OUTER JOIN
//                         tdleasing ON ttin.LeaseKode = tdleasing.LeaseKode LEFT OUTER JOIN
//                         tdsales ON ttin.SalesKode = tdsales.SalesKode LEFT OUTER JOIN
//                         tdcustomer ON ttin.CusKode = tdcustomer.CusKode LEFT OUTER JOIN
//                         ttdk ON ttin.INNo = ttdk.INNo
//WHERE        (ttin.INNo = :INNo)