<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "tuserlogdb".
 *
 * @property string $NoTrans
 * @property string $LogTglJam
 * @property string $LogTabel
 * @property string $LogJenis
 * @property string $LogTrans
 * @property string $LogGL
 * @property string $LogMemo
 * @property string $LogNominal
 * @property string $UserID
 */
class Tuserlogdb extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tuserlogdb';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['LogTglJam'], 'safe'],
            [['LogNominal'], 'number'],
            [['NoTrans'], 'string', 'max' => 18],
            [['LogTabel'], 'string', 'max' => 25],
            [['LogJenis'], 'string', 'max' => 10],
            [['LogTrans', 'LogGL'], 'string', 'max' => 20],
            [['LogMemo'], 'string', 'max' => 300],
            [['UserID'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'NoTrans' => 'No Trans',
            'LogTglJam' => 'Log Tgl Jam',
            'LogTabel' => 'Log Tabel',
            'LogJenis' => 'Log Jenis',
            'LogTrans' => 'Log Trans',
            'LogGL' => 'Log Gl',
            'LogMemo' => 'Log Memo',
            'LogNominal' => 'Log Nominal',
            'UserID' => 'User ID',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TuserlogdbQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TuserlogdbQuery(get_called_class());
    }
}
