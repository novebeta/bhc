<?php
namespace aunit\models;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
use aunit\components\Menu;
/**
 * This is the ActiveQuery class for [[Ttsrhd]].
 *
 * @see Ttsrhd
 */
class TtsrhdQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttsrhd[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttsrhd|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function ttsrhdFillByNo() {
		return $this->select( new Expression( "ttsrhd.SRNo, ttsrhd.SRTgl, ttsrhd.MotorNoPolisi, ttsrhd.LokasiKode, ttsrhd.KarKode, ttsrhd.SRKeterangan, ttsrhd.SRSubTotal, 
            ttsrhd.SRDiscFinal, ttsrhd.SRTotalPajak, ttsrhd.SRBiayaKirim, ttsrhd.SRTotal, ttsrhd.UserID, tdkaryawan.KarNama, tdmotortype.MotorNama,
            tdcustomer.MotorWarna, tdcustomer.CusNama, ttsrhd.NoGL, ttsrhd.KodeTrans, ttsrhd.SINo, ttsrhd.PosKode, ttsrhd.Cetak, ttsrhd.SRTerbayar" ) )
		            ->join( "INNER JOIN", "tdkaryawan", "ttsrhd.KarKode = tdkaryawan.KarKode" )
		            ->join( "LEFT OUTER JOIN", "tdcustomer", "ttsrhd.MotorNoPolisi = tdcustomer.MotorNoPolisi " )
		            ->join( "LEFT OUTER JOIN", "tdmotortype", "tdcustomer.MotorType = tdmotortype.MotorType" );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
//		$post[ 'PosKode' ]      = Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'SRNo' ]          = $post[ 'SRNo' ] ?? '--';
		$post[ 'SRNoBaru' ]      = $post[ 'SRNoView' ] ?? '--';
		$post[ 'SRTgl' ]         = date('Y-m-d H:i:s', strtotime($post[ 'SRTgl' ] ?? date( 'Y-m-d H:i:s' )));
		$post[ 'SINo' ]          = $post[ 'SINo' ] ?? '--';
		$post[ 'KodeTrans' ]     = $post[ 'KodeTrans' ] ?? 'TC12';
		$post[ 'SRSubTotal' ]    = floatval( $post[ 'SRSubTotal' ] ?? 0 );
		$post[ 'LokasiKode' ]    = $post[ 'LokasiKode' ] ?? '--';
		$post[ 'KarKode' ]       = $post[ 'KarKode' ] ?? '--';
		$post[ 'MotorNoPolisi' ] = $post[ 'MotorNoPolisi' ] ?? 'UMUM';
		$post[ 'SRDiscFinal' ]   = floatval( $post[ 'SRDiscFinal' ] ?? 0 );
		$post[ 'SRTotalPajak' ]  = floatval( $post[ 'SRTotalPajak' ] ?? 0 );
		$post[ 'SRBiayaKirim' ]  = floatval( $post[ 'SRBiayaKirim' ] ?? 0 );
		$post[ 'SRTotal' ]       = floatval( $post[ 'SRTotal' ] ?? 0 );
		$post[ 'SRTerbayar' ]    = floatval( $post[ 'SRTerbayar' ] ?? 0 );
		$post[ 'SRKeterangan' ]  = $post[ 'SRKeterangan' ] ?? '--';
		$post[ 'NoGL' ]          = $post[ 'NoGL' ] ?? '--';
		$post[ 'Cetak' ]         = $post[ 'Cetak' ] ?? 'Belum';
        try {
            General::cCmd( "SET @SRNo = ?;" )->bindParam( 1, $post[ 'SRNo' ] )->execute();
            General::cCmd( "CALL fsrhd(?,@Status,@Keterangan,?,?,?,@SRNo,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                   ->bindParam( 1, $post[ 'Action' ] )
                   ->bindParam( 2, $post[ 'UserID' ] )
                   ->bindParam( 3, $post[ 'KodeAkses' ] )
                   ->bindParam( 4, $post[ 'PosKode' ] )
                   ->bindParam( 5, $post[ 'SRNoBaru' ] )
                   ->bindParam( 6, $post[ 'SRTgl' ] )
                   ->bindParam( 7, $post[ 'SINo' ] )
                   ->bindParam( 8, $post[ 'KodeTrans' ] )
                   ->bindParam( 9, $post[ 'SRSubTotal' ] )
                   ->bindParam( 10, $post[ 'LokasiKode' ] )
                   ->bindParam( 11, $post[ 'KarKode' ] )
                   ->bindParam( 12, $post[ 'MotorNoPolisi' ] )
                   ->bindParam( 13, $post[ 'SRDiscFinal' ] )
                   ->bindParam( 14, $post[ 'SRTotalPajak' ] )
                   ->bindParam( 15, $post[ 'SRBiayaKirim' ] )
                   ->bindParam( 16, $post[ 'SRTotal' ] )
                   ->bindParam( 17, $post[ 'SRTerbayar' ] )
                   ->bindParam( 18, $post[ 'SRKeterangan' ] )
                   ->bindParam( 19, $post[ 'NoGL' ] )
                   ->bindParam( 20, $post[ 'Cetak' ] )
                   ->execute();
            $exe = General::cCmd( "SELECT @SRNo,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'SRNo'       => $post[ 'SRNo' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $SRNo       = $exe[ '@SRNo' ] ?? $post[ 'SRNo' ];
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
            'SRNo'       => $SRNo,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
		];
	}
}
