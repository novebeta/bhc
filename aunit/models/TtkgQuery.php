<?php

namespace aunit\models;
use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;

/**
 * This is the ActiveQuery class for [[Ttkg]].
 *
 * @see Ttkg
 */
class TtkgQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttkg[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttkg|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function dsTUang() {
		return $this->select( new Expression( "ttkg.KGNo, ttkg.KGTgl, ttkg.DKNo, ttdk.DKTgl, ttkg.KGJenis, ttkg.KGNominal, ttkg.KGPengirim, ttkg.KGPenerima, ttkg.KGMemo, ttkg.KGStatus, ttkg.UserID, tdcustomer.CusNama, CONCAT(tdcustomer.CusAlamat, 'RT/RW : ', 
        tdcustomer.CusRT, '/', tdcustomer.CusRW) AS CusAlamat, CONCAT(tdcustomer.CusKelurahan, ', ', tdcustomer.CusKecamatan, ', ', tdcustomer.CusKabupaten, ', ', tdcustomer.CusProvinsi) AS CusArea, 
        CONCAT(tdmotortype.MotorNama, ' - ', tmotor.MotorWarna, ' - ', tdmotortype.MotorCC, 'CC', ' - ', tmotor.MotorTahun) AS TypeMotor, CONCAT(tmotor.MotorType, ' - ', tmotor.MotorNoMesin, ' - ', tmotor.MotorNoRangka) 
        AS NoSinNoKa, tmotor.STNKNama, tmotor.STNKAlamat" ) )
		            ->join( "LEFT OUTER JOIN", "ttdk", "ttdk.DKNo = ttkg.DKNo" )
		            ->join( "LEFT OUTER JOIN", "tdcustomer", "ttdk.CusKode = tdcustomer.CusKode" )
		            ->join( "LEFT OUTER JOIN", "tmotor", "ttdk.DKNo = tmotor.DKNo" )
		            ->join( "LEFT OUTER JOIN", "tdmotortype", "tdmotortype.MotorType = tmotor.MotorType" );
	}

    public function callSP( $post ) {
		$post[ 'UserID' ]           = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]        = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]       = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'KGNo' ]             = $post[ 'KGNo' ] ?? '--';
		$post[ 'KGNoBaru' ]         = $post[ 'KGNoBaru' ] ?? '--';
		$post[ 'KGTgl' ]            = date('Y-m-d H:i:s', strtotime($post[ 'KGTgl' ] ?? date( 'Y-m-d' )));
		$post[ 'DKNo' ]             = $post[ 'DKNo' ] ?? '--';
		$post[ 'KGJenis' ]          = $post[ 'KGJenis' ] ?? '--';
		$post[ 'KGNominal' ]        = $post[ 'KGNominal' ] ?? 0;
		$post[ 'KGPengirim' ]       = $post[ 'KGPengirim' ] ?? '--';
		$post[ 'KGPenerima' ]       = $post[ 'KGPenerima' ] ?? '--';
		$post[ 'KGMemo' ]           = $post[ 'KGMemo' ] ?? '--';
		$post[ 'KGStatus' ]         = $post[ 'KGStatus' ] ?? '--';
		try {
			General::cCmd( "SET @KGNo = ?;" )->bindParam( 1, $post[ 'KGNo' ] )->execute();
			General::cCmd( "CALL fkg(?,@Status,@Keterangan,?,?,?,@KGNo,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'KGNoBaru' ] )
			       ->bindParam( 6, $post[ 'KGTgl' ] )
			       ->bindParam( 7, $post[ 'DKNo' ] )
			       ->bindParam( 8, $post[ 'KGJenis' ] )
			       ->bindParam( 9, $post[ 'KGNominal' ] )
			       ->bindParam( 10, $post[ 'KGPengirim' ] )
			       ->bindParam( 11, $post[ 'KGPenerima' ] )
			       ->bindParam( 12, $post[ 'KGMemo' ] )
			       ->bindParam( 13, $post[ 'KGStatus' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @KGNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'KGNo'       => $post[ 'KGNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$KGNo       = $exe[ '@KGNo' ] ?? $post[ 'KGNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'KGNo'       => $KGNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
}