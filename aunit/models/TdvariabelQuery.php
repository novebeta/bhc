<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Tdvariabel]].
 *
 * @see Tdvariabel
 */
class TdvariabelQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdvariabel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdvariabel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
