<?php
namespace aunit\models;
use yii\db\Exception;
use yii\db\Query;
/**
 * This is the model class for table "ttgeneralledgerhd".
 *
 * @property string $NoGL
 * @property string $TglGL
 * @property string $KodeTrans SS,FB,DK,SK,SM,PB,SD,PD,KK,KM,BK,BM
 * @property string $MemoGL
 * @property string $TotalDebetGL
 * @property string $TotalKreditGL
 * @property string $GLLink
 * @property string $UserID
 * @property string $HPLink
 * @property string $GLValid
 * @property string $GLDoc
 */
class Ttgeneralledgerhd extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttgeneralledgerhd';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'NoGL', 'TglGL' ], 'required' ],
			[ [ 'TglGL' ], 'safe' ],
			[ [ 'TotalDebetGL', 'TotalKreditGL','GLDoc' ], 'number' ],
			[ [ 'NoGL' ], 'string', 'max' => 10 ],
			[ [ 'KodeTrans' ], 'string', 'max' => 2 ],
			[ [ 'MemoGL' ], 'string', 'max' => 300 ],
			[ [ 'GLLink', 'HPLink' ], 'string', 'max' => 18 ],
			[ [ 'UserID' ], 'string', 'max' => 15 ],
			[ [ 'GLValid' ], 'string', 'max' => 5 ],
			[ [ 'NoGL', 'TglGL' ], 'unique', 'targetAttribute' => [ 'NoGL', 'TglGL' ] ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'NoGL'          => 'No Jurnal',
			'TglGL'         => 'Tgl Jurnal',
			'KodeTrans'     => 'SS,FB,DK,SK,SM,PB,SD,PD,KK,KM,BK,BM',
			'MemoGL'        => 'Memo Jurnal',
			'TotalDebetGL'  => 'Debet',
			'TotalKreditGL' => 'Kredit',
			'GLLink'        => 'No Transaksi',
			'UserID'        => 'User ID',
			'HPLink'        => 'HP Link',
			'GLValid'       => 'Status',
			'GLDoc'         => 'GLDoc',
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public static function colGrid() {
		return [
			'NoGL'          => [ 'width' => 70, 'label' => 'No Jurnal', 'name' => 'NoGL' ],
			'TglGL'         => [ 'width' => 70, 'label' => 'Tgl Jurnal', 'name' => 'TglGL', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'MemoGL'        => [ 'width' => 360, 'label' => 'Memo Jurnal', 'name' => 'MemoGL' ],
			'TotalDebetGL'  => [ 'width' => 100, 'label' => 'Debet', 'name' => 'TotalDebetGL', 'formatter' => 'number', 'align' => 'right' ],
			'TotalKreditGL' => [ 'width' => 100, 'label' => 'Kredit', 'name' => 'TotalKreditGL', 'formatter' => 'number', 'align' => 'right' ],
			'GLLink'        => [ 'width' => 90, 'label' => 'No Transaksi', 'name' => 'GLLink' ],
			'HPLink'        => [ 'width' => 90, 'label' => 'HP Link', 'name' => 'HPLink' ],
			'GLValid'       => [ 'width' => 50, 'label' => 'Validasi', 'name' => 'GLValid' ],
		];
	}

	public static function colGridMemorial() {
		return [
			'GLDoc' 		=> [ 'width' => 30, 'label' => 'Doc', 'name' => 'GLDoc' ],
			'NoGL'          => [ 'width' => 70, 'label' => 'No Jurnal', 'name' => 'NoGL' ],
			'TglGL'         => [ 'width' => 70, 'label' => 'Tgl Jurnal', 'name' => 'TglGL', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'MemoGL'        => [ 'width' => 360, 'label' => 'Memo Jurnal', 'name' => 'MemoGL' ],
			'TotalDebetGL'  => [ 'width' => 100, 'label' => 'Debet', 'name' => 'TotalDebetGL', 'formatter' => 'number', 'align' => 'right' ],
			'TotalKreditGL' => [ 'width' => 100, 'label' => 'Kredit', 'name' => 'TotalKreditGL', 'formatter' => 'number', 'align' => 'right' ],
			'GLLink'        => [ 'width' => 90, 'label' => 'No Transaksi', 'name' => 'GLLink' ],
			'HPLink'        => [ 'width' => 90, 'label' => 'HP Link', 'name' => 'HPLink' ],
			'GLValid'       => [ 'width' => 50, 'label' => 'Validasi', 'name' => 'GLValid' ],
		];
	}

	public static function colGridSearchTrans() {
		return [
			'DKNo'   => [ 'width' => 70, 'label' => 'No DatKons', 'name' => 'DKNo' ],
			'DKTgl'       => [ 'width' => 70, 'label' => 'Tgl DK', 'name' => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'CusNama' => [ 'width' => 160, 'label' => 'Nama Konsumen', 'name' => 'CusNama' ],
			'LeaseKode'      => [ 'width' => 90, 'label' => 'Leasing', 'name' => 'LeaseKode' ],
			'MotorType'      => [ 'width' => 90, 'label' => 'Type Motor', 'name' => 'MotorType' ],
			'MotorWarna'      => [ 'width' => 90, 'label' => 'Warna Motor', 'name' => 'MotorWarna' ],
			'MotorNoMesin'        => [ 'width' => 100, 'label' => 'No Mesin', 'name' => 'MotorNoMesin' ],
			'MotorNoRangka'      => [ 'width' => 135, 'label' => 'No Rangka', 'name' => 'MotorNoRangka' ],
			'Nominal'     => [ 'width' => 90, 'label' => 'Nominal', 'name' => 'Nominal', 'formatter' => 'number', 'align' => 'right' ],
		];
	}
	public static function colGridSelectMMHutang() {
		return [
			'NoAccount'   => [ 'width' => 70, 'label' => 'No Account', 'name' => 'NoAccount' ],
			'NamaAccount' => [ 'width' => 225, 'label' => 'Nama Account', 'name' => 'NamaAccount' ],
			'GLLink'      => [ 'width' => 90, 'label' => 'No Transaksi', 'name' => 'GLLink' ],
			'HPNilai'     => [ 'width' => 100, 'label' => 'HPNilai', 'name' => 'HPNilai', 'formatter' => 'number', 'align' => 'right' ],
			'NoGL'        => [ 'width' => 70, 'label' => 'No Jurnal', 'name' => 'NoGL' ],
			'TglGL'       => [ 'width' => 70, 'label' => 'Tgl Jurnal', 'name' => 'TglGL', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'MemoGL'      => [ 'width' => 360, 'label' => 'Memo Jurnal', 'name' => 'MemoGL' ],
			'HPLink'      => [ 'width' => 90, 'label' => 'HP Link', 'name' => 'HPLink' ],
		];
	}
	public static function colGridShowHutangPiutang() {
		return [
			'NoAccount'   => [ 'width' => 70, 'label' => 'No Account', 'name' => 'NoAccount' ],
			'NamaAccount' => [ 'width' => 150, 'label' => 'Nama Account', 'name' => 'NamaAccount' ],
			'GLLink'      => [ 'width' => 90, 'label' => 'No Transaksi', 'name' => 'GLLink' ],
			'TglGL'       => [ 'width' => 70, 'label' => 'Tgl Jurnal', 'name' => 'TglGL', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'NoGL'        => [ 'width' => 70, 'label' => 'No Jurnal', 'name' => 'NoGL' ],
			'MemoGL'      => [ 'width' => 360, 'label' => 'Memo Jurnal', 'name' => 'MemoGL' ],
			'HPNilai'     => [ 'width' => 100, 'label' => 'HPNilai', 'name' => 'HPNilai', 'formatter' => 'number', 'align' => 'right' ],
		];
	}
	public static function colGridSelectPiutang() {
		return [
			'tvpiutangdebet.GLLink'                                => [ 'width' => 70, 'label' => 'No Trans', 'name' => 'GLLink' ],
			'tvpiutangdebet.TglGL'                                 => [ 'width' => 70, 'label' => 'Tgl Jurnal', 'name' => 'TglGL', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
//			'tvpiutangdebet.NoAccount'                             => [ 'width' => 90, 'label' => 'No Account', 'name' => 'NoAccount' ],
//			'tvpiutangdebet.NamaAccount'                           => [ 'width' => 70, 'label' => 'Nama Account', 'name' => 'NamaAccount' ],
			'tvpiutangdebet.MemoGL'                                => [ 'width' => 360, 'label' => 'Memo', 'name' => 'MemoGL' ],
			'DebetGL'                                              => [ 'width' => 100, 'label' => 'Piutang Total', 'name' => 'DebetGL', 'formatter' => 'number', 'align' => 'right' ],
			'IFNULL(tvpiutangbayar.SumKreditGL,0) AS Lunas'        => [ 'width' => 100, 'label' => 'Piutang Lunas', 'name' => 'Lunas', 'formatter' => 'number', 'align' => 'right' ],
			'DebetGL-IFNULL(tvpiutangbayar.SumKreditGL,0) AS Sisa' => [ 'width' => 100, 'label' => 'Piutang Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
			'tvpiutangdebet.NoGL'                                  => [ 'width' => 225, 'label' => 'No Jurnal', 'name' => 'NoGL' ],
		];
	}
	public static function colGridSelectHutang() {
		return [
			'tvhutangkredit.GLLink'                                 => [ 'width' => 70, 'label' => 'No Trans', 'name' => 'GLLink' ],
			'tvhutangkredit.TglGL'                                  => [ 'width' => 70, 'label' => 'Tgl Jurnal', 'name' => 'TglGL', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
//			'tvpiutangdebet.NoAccount'                              => [ 'width' => 90, 'label' => 'No Account', 'name' => 'NoAccount' ],
//			'tvpiutangdebet.NamaAccount'                            => [ 'width' => 70, 'label' => 'Nama Account', 'name' => 'NamaAccount' ],
			'tvhutangkredit.MemoGL'                                 => [ 'width' => 360, 'label' => 'Memo', 'name' => 'MemoGL' ],
			'KreditGL'                                              => [ 'width' => 100, 'label' => 'Hutang Total', 'name' => 'KreditGL', 'formatter' => 'number', 'align' => 'right' ],
			'IFNULL(tvhutangbayar.SumDebetGL,0) AS Lunas'           => [ 'width' => 100, 'label' => 'Hutang Lunas', 'name' => 'Lunas', 'formatter' => 'number', 'align' => 'right' ],
			'KreditGL - IFNULL(tvhutangbayar.SumDebetGL,0) AS Sisa' => [ 'width' => 100, 'label' => 'Hutang Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
			'tvpiutangdebet.NoGL'                                   => [ 'width' => 225, 'label' => 'No Jurnal', 'name' => 'NoGL' ],
		];
	}
	/**
	 * {@inheritdoc}
	 * @return TtgeneralledgerhdQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TtgeneralledgerhdQuery( get_called_class() );
	}
	/**
	 * @param $kodeTrans
	 * @param bool $temporary
	 *
	 * @return mixed|string
	 */
	public static function generateNoGl( $kodeTrans, $temporary = false ) {
		/*
		 * format : MMyy-00001
		 * temporary number use = instead rather than -
		 */
		switch ( $kodeTrans ) {
			case 'GL':
				$kode = 'MM';
				break;
			default:
				$kode = $kodeTrans;
		}
		$yNow    = date( "y" );
		$maxNoGl = ( new \yii\db\Query() )
			->from( self::tableName() )
			->where( "NoGL LIKE '$kode$yNow-%'" )
			->max( 'NoGL' );
		if ( $maxNoGl && preg_match( "/$kode\d{2}-(\d+)/", $maxNoGl, $result ) ) {
			[ $all, $counter ] = $result;
			$digitCount  = strlen( $counter );
			$val         = (int) $counter;
			$nextCounter = sprintf( '%0' . $digitCount . 'd', ++ $val );
		} else {
			$nextCounter = "00001";
		}
		$nextNoGl = "$kode$yNow-$nextCounter";
		return $temporary ? str_replace( '-', '=', $nextNoGl ) : $nextNoGl;
	}
	/**
	 * @param $NoGL
	 * @param $TglGL
	 *
	 * @return bool
	 * @throws Exception
	 */
	public static function CekMMCOAKasBank( $NoGL, $TglGL ) {
		$Hasil       = false;
		$MyNoAccount = ( new Query() )
			->select( 'CONCAT("\'",NoAccount, "\'")' )
			->from( 'ttgeneralledgerit' )
			->where( 'NoGL = :NoGL AND TglGL = :TglGL', [ ':NoGL' => $NoGL, ':TglGL' => $TglGL ] )
			->column();
		$MyNoAccount = implode( ',', $MyNoAccount );
		if ( $MyNoAccount ) {
			$A = ( new Query() )
				->select( 'ttgeneralledgerit.NoAccount' )
				->from( 'ttgeneralledgerit' )
				->innerJoin( 'traccount', 'traccount.NoAccount = ttgeneralledgerit.NoAccount' )
				->where( "Noparent IN ('11020000','11030000','11010000','11010300') AND LEFT(NoGL,2) = 'MM' AND ttgeneralledgerit.NoAccount IN ($MyNoAccount)" )
				->scalar();
			if ( $A && $A !== '--' ) {
				// $Hasil = true;
				throw new Exception( 'COA Kas dan Bank tidak boleh digunakan pada Jurnal Memorial' );
			}
		}
		return $Hasil;
	}
	/**
	 * @param $TglGL
	 *
	 * @return bool
	 * @throws Exception
	 */
	public static function CekTglTransaksi( $TglGL ) {
		$now              = date( "Y-m-d" );
		$fistDayLastMonth = date_create( $now . ' first day of last month' );
		$Hasil            = false;
		$MyTglGL          = strtotime( $TglGL );
		$MyTglNow         = strtotime( $now );
		$AwalBulanLalu    = strtotime( $fistDayLastMonth->format( 'Y-m-d' ) );
		$AwalBulanNow     = date( "Y-m-01" );
		$BulanNow5Hari    = date( "Y-m-06" );
		if ( $MyTglNow <= $BulanNow5Hari && $MyTglGL >= $AwalBulanLalu ) /*(kurang dari = tgl 5 dan lebih dari = awal bulan lalu)*/ {
			$Hasil = true;
		} /*Boleh Edit*/
		if ( $MyTglNow > $BulanNow5Hari && $MyTglGL >= $AwalBulanNow ) /*(Lbh dari tgl 5 dan lebih dari = awal bulan skrg)*/ {
			$Hasil = true;
		} /*Boleh Edit*/
		if ( $MyTglNow <= $BulanNow5Hari && $MyTglGL < $AwalBulanLalu ) { /*(kurang dari = tgl 5 dan kurang dari awal bulan lalu)*/
			$Hasil = false; /*Tidak Boleh Edit*/
			throw new Exception( 'Tanggal Jurnal : ' . date( "d/m/Y" ) . '  <  ' . date( "d/m/Y", $AwalBulanLalu ) );
		}
		if ( $MyTglNow > $BulanNow5Hari && $MyTglGL < $AwalBulanNow ) { /*(Lbh dari tgl 5 dan kurang dari awal bulan skrg)*/
			$Hasil = false; /*Tidak Boleh Edit*/
			throw new Exception( 'Tanggal Jurnal : ' . date( "d/m/Y" ) . '  <  ' . date( "d/m/Y", $AwalBulanNow ) );
		}
		$BKodeAkses = ''; // todo: get hakAkses
		if ( $BKodeAkses = "Admin" || $BKodeAkses = "Auditor" ) {
			$Hasil = true; /*Boleh Edit*/
		}
		return $Hasil;
	}
	/**
	 * @param $NoGL
	 * @param $TglGL
	 *
	 * @return array
	 */
	public static function CekHPLink( $NoGL, $TglGL ) {
		/* 'Pelunasan Hutang Piutang */
		$JumlahHP         = 0;
		$MyPesan0         = '';
		$TyJenisHP        = [];
		$TyNoAccountHP    = [];
		$TyNamaAccountHP  = [];
		$TyNilaiAccountHP = [];
		$dataDetails      = ( new Query() )
			->select( [
				'ttgeneralledgerit.*',
				'traccount.NamaAccount NamaAccount'
			] )
			->from( 'ttgeneralledgerit' )
			->leftJoin( 'traccount', 'traccount.NoAccount = ttgeneralledgerit.NoAccount' )
			->where( 'NoGL = :NoGL AND TglGL = :TglGL', [ ':NoGL' => $NoGL, ':TglGL' => $TglGL ] )
			->all();
		foreach ( $dataDetails as $data ) {
			if ( $data[ 'KreditGL' ] > 0 && in_array( substr( $data[ 'NoAccount' ], 0, 4 ),
					[ '1106', '1107', '1108', '1109', '1110', '1111', '1112', '1113', '1114' ] ) ) {
				/* Ada Pelunasan Piutang (KM,BM,MM) */
				$JenisHP = 'Piutang';
				$JumlahHP ++;
				$TyJenisHP[]        = 'Piutang';
				$TyNoAccountHP[]    = $data[ 'NoAccount' ];
				$TyNamaAccountHP[]  = $data[ 'NamaAccount' ];
				$TyNilaiAccountHP[] = $data[ 'KreditGL' ];
				$MyPesan0           .= "* " . $JenisHP . " - " . $data[ 'NoAccount' ] . " - " . $data[ 'NamaAccount' ] . " (K) -> "
				                       . number_format( $data[ 'KreditGL' ], 2, ".", "," ) . '<br/>';
			}
			if ( $data[ 'DebetGL' ] > 0 && in_array( substr( $data[ 'NoAccount' ], 0, 2 ), [] ) ) {
				/* If DsJurnal.ttgeneralledgerit(i).NoAccount.Substring(0, 2) = "21" And DsJurnal.ttgeneralledgerit(i).DebetGL > 0 Then
				   Ada Pelunasan Hutang (KK,BK,MM) */
				$JenisHP = 'Hutang';
				$JumlahHP ++;
				$TyJenisHP[]        = 'Hutang';
				$TyNoAccountHP[]    = $data[ 'NoAccount' ];
				$TyNamaAccountHP[]  = $data[ 'NamaAccount' ];
				$TyNilaiAccountHP[] = $data[ 'KreditGL' ];
				$MyPesan0           .= "* " . $JenisHP . " - " . $data[ 'NoAccount' ] . " - " . $data[ 'NamaAccount' ] . " (D) -> "
				                       . number_format( $data[ 'DebetGL' ], 2, ".", "," ) . '<br/>';
			}
		}
		return [ 'JumlahHP' => $JumlahHP, 'MyPesan0' => $MyPesan0 ];
	}
}
