<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "vtkbsaldo".
 *
 * @property string $KBNo
 * @property string $Tgl
 * @property string $Jam
 * @property string $Memo
 * @property string $Debet
 * @property string $Kredit
 * @property string $Jenis
 * @property string $Person
 * @property string $UserID
 * @property string $Link
 * @property string $Kode
 * @property string $BankAC
 * @property string $NoAccount
 * @property string $BankCekNo
 * @property string $BankCekTempo
 * @property string $NoGL
 */
class Vtkbsaldo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vtkbsaldo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Tgl', 'Jam', 'BankCekTempo'], 'safe'],
            [['Debet', 'Kredit'], 'number'],
            [['KBNo', 'Link'], 'string', 'max' => 12],
            [['Memo'], 'string', 'max' => 100],
            [['Jenis'], 'string', 'max' => 20],
            [['Person'], 'string', 'max' => 50],
            [['UserID', 'Kode'], 'string', 'max' => 15],
            [['BankAC', 'BankCekNo'], 'string', 'max' => 25],
            [['NoAccount', 'NoGL'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'KBNo' => 'Kb No',
            'Tgl' => 'Tgl',
            'Jam' => 'Jam',
            'Memo' => 'Memo',
            'Debet' => 'Debet',
            'Kredit' => 'Kredit',
            'Jenis' => 'Jenis',
            'Person' => 'Person',
            'UserID' => 'User ID',
            'Link' => 'Link',
            'Kode' => 'Kode',
            'BankAC' => 'Bank Ac',
            'NoAccount' => 'No Account',
            'BankCekNo' => 'Bank Cek No',
            'BankCekTempo' => 'Bank Cek Tempo',
            'NoGL' => 'No Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VtkbsaldoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VtkbsaldoQuery(get_called_class());
    }
}
