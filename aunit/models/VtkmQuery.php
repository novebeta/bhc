<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Vtkm]].
 *
 * @see Vtkm
 */
class VtkmQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Vtkm[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Vtkm|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
