<?php
namespace aunit\models;
use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
class TtskQuery extends \yii\db\ActiveQuery {
	public function all( $db = null ) {
		return parent::all( $db );
	}
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function dsTJualFillByNo() {
		return $this->select( new Expression( "ttsk.SKNo, ttsk.SKTgl, ttsk.LokasiKode, ttsk.SKMemo, ttsk.UserID, ttdk.CusKode, tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusKelurahan, tdcustomer.CusKecamatan,
                         tdcustomer.CusKabupaten, tdcustomer.CusTelepon, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, ttdk.DKNo, ttdk.DKHarga, tdlokasi.LokasiNama, ttsk.SKJam,
                         tmotor.FBHarga, ttdk.DKTgl, ttdk.DKJenis, tdcustomer.CusKTP, tdcustomer.CusKodePos, tmotor.RKNo, tmotor.PlatNo, ttsk.NoGL, ttdk.LeaseKode, tdmotortype.MotorNama, tdmotortype.MotorKategori,
                         tdcustomer.CusPembeliNama, tdcustomer.CusPembeliKTP, tdcustomer.CusPembeliAlamat, tdsales.SalesNama, ttdk.TeamKode, ttdk.SPKNo, IFNULL(tmotor.MotorAutoN, 0) AS MotorAutoN, ttdk.BBN, ttsk.SKCetak,
                         ttsk.SKPengirim, IFNULL(ttdk.NoBukuServis, '--') AS NoBukuServis, tmotor.PlatNo AS EXPR1, ttsk.SalesKodeSK, ttsk.ZdeliveryDocumentId, ttsk.ZlokasiPengiriman, ttsk.Zlatitude, ttsk.Zlongitude, ttsk.ZnamaPenerima, ttsk.ZnoKontakPenerima" ) )
		            ->from( "tdsales" )
		            ->join( "INNER JOIN", "ttdk", "tdsales.SalesKode = ttdk.SalesKode" )
		            ->join( "RIGHT OUTER JOIN", "ttsk" )
		            ->join( "LEFT OUTER JOIN", "tdlokasi", "ttsk.LokasiKode = tdlokasi.LokasiKode" )
		            ->join( "LEFT OUTER JOIN", "tmotor", "ttsk.SKNo = tmotor.SKNo" )
		            ->join( "LEFT OUTER JOIN", "tdmotortype", "tmotor.MotorType = tdmotortype.MotorType ON ttdk.DKNo = tmotor.DKNo" )
		            ->join( "LEFT OUTER JOIN", "tdcustomer", "ttdk.CusKode = tdcustomer.CusKode" );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ] = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'SKNo' ]       = $post[ 'SKNo' ] ?? '--';
		$post[ 'SKNoBaru' ]   = $post[ 'SKNoView' ] ?? '--';
		$post[ 'DKNo' ]       = $post[ 'DKNo' ] ?? '--';
		$post[ 'DKTgl' ]      = date('Y-m-d', strtotime($post[ 'DKTgl' ] ?? date( 'Y-m-d' )));
		if ( $post[ 'DKTgl' ] == '' || $post[ 'DKTgl' ] == '--' ) {
			$post[ 'DKTgl' ] = date('Y-m-d', strtotime($post[ 'DKTgl' ] ?? date( 'Y-m-d' )));
		}
		$post[ 'DKJenis' ]          = $post[ 'DKJenis' ] ?? '--';
		$post[ 'SPKNo' ]            = $post[ 'SPKNo' ] ?? '--';
		$post[ 'SKTgl' ]            = date('Y-m-d', strtotime($post[ 'SKTgl' ] ?? date( 'Y-m-d' )));
		$post[ 'SKJam' ]            = date('Y-m-d H:i:s', strtotime($post[ 'SKJam' ] ?? date( 'Y-m-d H:i:s' )));
		$post[ 'SKCetak' ]          = $post[ 'SKCetakSKCetak' ] ?? '--';
		$post[ 'SalesNama' ]        = $post[ 'SalesNama' ] ?? '--';
		$post[ 'TeamKode' ]         = $post[ 'TeamKode' ] ?? '--';
		$post[ 'LeaseKode' ]        = $post[ 'LeaseKode' ] ?? '--';
		$post[ 'SKMemo' ]           = $post[ 'SKMemo' ] ?? '--';
		$post[ 'NoBukuServis' ]     = $post[ 'NoBukuServis' ] ?? '--';
		$post[ 'CusNama' ]          = $post[ 'CusNama' ] ?? '--';
		$post[ 'CusKTP' ]           = $post[ 'CusKTP' ] ?? '--';
		$post[ 'CusKabupaten' ]     = $post[ 'CusKabupaten' ] ?? '--';
		$post[ 'CusAlamat' ]        = $post[ 'CusAlamat' ] ?? '--';
		$post[ 'CusRT' ]            = $post[ 'CusRT' ] ?? '--';
		$post[ 'CusRW' ]            = $post[ 'CusRW' ] ?? '--';
		$post[ 'CusKecamatan' ]     = $post[ 'CusKecamatan' ] ?? '--';
		$post[ 'CusTelepon' ]       = $post[ 'CusTelepon' ] ?? '--';
		$post[ 'CusKodePos' ]       = $post[ 'CusKodePos' ] ?? '--';
		$post[ 'CusKelurahan' ]     = $post[ 'CusKelurahan' ] ?? '--';
		$post[ 'CusPembeliNama' ]   = $post[ 'CusPembeliNama' ] ?? '--';
		$post[ 'CusPembeliKTP' ]    = $post[ 'CusPembeliKTP' ] ?? '--';
		$post[ 'CusPembeliAlamat' ] = $post[ 'CusPembeliAlamat' ] ?? '--';
		$post[ 'MotorType' ]        = $post[ 'MotorType' ] ?? '--';
		$post[ 'MotorTahun' ]       = floatval( $post[ 'MotorTahun' ] ?? 1900 );
		$post[ 'MotorWarna' ]       = $post[ 'MotorWarna' ] ?? '--';
		$post[ 'SKPengirim' ]       = $post[ 'SKPengirim' ] ?? '--';
		$post[ 'MotorNoMesin' ]     = $post[ 'MotorNoMesin' ] ?? '--';
		$post[ 'MotorNoRangka' ]    = $post[ 'MotorNoRangka' ] ?? '--';
		$post[ 'DKHarga' ]          = floatval( $post[ 'DKHarga' ] ?? 0 );
		$post[ 'MotorNama' ]        = $post[ 'MotorNama' ] ?? '--';
		$post[ 'BBN' ]              = floatval( $post[ 'BBN' ] ?? 0 );
		$post[ 'MotorKategori' ]    = $post[ 'MotorKategori' ] ?? '--';
		$post[ 'RKNo' ]             = $post[ 'RKNo' ] ?? '--';
		$post[ 'NoGL' ]             = $post[ 'NoGL' ] ?? '--';
		$post[ 'LokasiKodeSK' ]     = $post[ 'LokasiKodeSK' ] ?? '--';
		$post[ 'SalesKodeSK' ]     = $post[ 'SalesKodeSK' ] ?? '--';
		$post[ 'ZdeliveryDocumentId' ]     = $post[ 'ZdeliveryDocumentId' ] ?? '--';
		$post[ 'ZlokasiPengiriman' ]     = $post[ 'ZlokasiPengiriman' ] ?? '--';
		$post[ 'Zlatitude' ]     = $post[ 'Zlatitude' ] ?? '--';
		$post[ 'Zlongitude' ]     = $post[ 'Zlongitude' ] ?? '--';
		$post[ 'ZnamaPenerima' ]     = $post[ 'ZnamaPenerima' ] ?? '--';
		$post[ 'ZnoKontakPenerima' ]     = $post[ 'ZnoKontakPenerima' ] ?? '--';
		try {
			General::cCmd( "SET @SKNo = ?;" )->bindParam( 1, $post[ 'SKNo' ] )->execute();
			General::cCmd( "CALL fsk(?,@Status,@Keterangan,?,?,?,@SKNo,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'SKNoBaru' ] )
			       ->bindParam( 6, $post[ 'DKNo' ] )
			       ->bindParam( 7, $post[ 'DKTgl' ] )
			       ->bindParam( 8, $post[ 'DKJenis' ] )
			       ->bindParam( 9, $post[ 'SPKNo' ] )
			       ->bindParam( 10, $post[ 'SKTgl' ] )
			       ->bindParam( 11, $post[ 'SKJam' ] )
			       ->bindParam( 12, $post[ 'SKCetak' ] )
			       ->bindParam( 13, $post[ 'SalesNama' ] )
			       ->bindParam( 14, $post[ 'TeamKode' ] )
			       ->bindParam( 15, $post[ 'LeaseKode' ] )
			       ->bindParam( 16, $post[ 'SKMemo' ] )
			       ->bindParam( 17, $post[ 'NoBukuServis' ] )
			       ->bindParam( 18, $post[ 'CusNama' ] )
			       ->bindParam( 19, $post[ 'CusKTP' ] )
			       ->bindParam( 20, $post[ 'CusKabupaten' ] )
			       ->bindParam( 21, $post[ 'CusAlamat' ] )
			       ->bindParam( 22, $post[ 'CusRT' ] )
			       ->bindParam( 23, $post[ 'CusRW' ] )
			       ->bindParam( 24, $post[ 'CusKecamatan' ] )
			       ->bindParam( 25, $post[ 'CusTelepon' ] )
			       ->bindParam( 26, $post[ 'CusKodePos' ] )
			       ->bindParam( 27, $post[ 'CusKelurahan' ] )
			       ->bindParam( 28, $post[ 'CusPembeliNama' ] )
			       ->bindParam( 29, $post[ 'CusPembeliKTP' ] )
			       ->bindParam( 30, $post[ 'CusPembeliAlamat' ] )
			       ->bindParam( 31, $post[ 'MotorType' ] )
			       ->bindParam( 32, $post[ 'MotorTahun' ] )
			       ->bindParam( 33, $post[ 'MotorWarna' ] )
			       ->bindParam( 34, $post[ 'SKPengirim' ] )
			       ->bindParam( 35, $post[ 'MotorNoMesin' ] )
			       ->bindParam( 36, $post[ 'MotorNoRangka' ] )
			       ->bindParam( 37, $post[ 'DKHarga' ] )
			       ->bindParam( 38, $post[ 'MotorNama' ] )
			       ->bindParam( 39, $post[ 'BBN' ] )
			       ->bindParam( 40, $post[ 'MotorKategori' ] )
			       ->bindParam( 41, $post[ 'RKNo' ] )
			       ->bindParam( 42, $post[ 'NoGL' ] )
			       ->bindParam( 43, $post[ 'LokasiKodeSK' ] )
			       ->bindParam( 44, $post[ 'SalesKodeSK' ] )
			       ->bindParam( 45, $post[ 'ZdeliveryDocumentId' ] )
			       ->bindParam( 46, $post[ 'ZlokasiPengiriman' ] )
			       ->bindParam( 47, $post[ 'Zlatitude' ] )
			       ->bindParam( 48, $post[ 'Zlongitude' ] )
			       ->bindParam( 49, $post[ 'ZnamaPenerima' ] )
			       ->bindParam( 50, $post[ 'ZnoKontakPenerima' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @SKNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'SKNo'       => $post[ 'SKNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$SKNo       = $exe[ '@SKNo' ] ?? $post[ 'SKNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'SKNo'       => $SKNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
	public function callSP_MO( $post ) {
		$post[ 'UserID' ]       = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]    = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]   = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'MONo' ]         = $post[ 'MONo' ] ?? '--';
		$post[ 'MOAsal' ]   = $post[ 'MOAsal' ] ?? '--';
		$post[ 'MOTujuan' ] = $post[ 'MOTujuan' ] ?? '--';
		try {
			General::cCmd( "SET @MONo = ?;" )->bindParam( 1, $post[ 'MONo' ] )->execute();
			General::cCmd( "CALL fmo(?,@Status,@Keterangan,?,?,?,@MONo,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'MOAsal' ] )
			       ->bindParam( 6, $post[ 'MOTujuan' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @MONo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'SKNo'       => $post[ 'MONo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$MONo       = $exe[ '@MONo' ] ?? $post[ 'MONo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'MONo'       => $MONo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
	public function callSP_PrintKwitansi( $post ) {
		$post[ 'UserID' ]       = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'Bayar' ]        = $post[ 'Bayar' ] ?? 0;
		$post[ 'SKNo' ]         = $post[ 'SKNo' ] ?? '--';
		try {
			General::cCmd( "SET @Bayar = ?;" )->bindParam( 1, $post[ 'Bayar' ] )->execute();
			General::cCmd( "CALL cPiutangDPKonsumen(@Status,@Keterangan,@Bayar,?,?);" )
			       ->bindParam( 1, $post[ 'SKNo' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @Bayar,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'Bayar'       => $post[ 'Bayar' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$Bayar       = $exe[ '@Bayar' ] ?? $post[ 'Bayar' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Keterangan NULL';
		return [
			'Bayar'       => $Bayar,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
}