<?php

namespace aunit\models;

use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
/**
 * This is the ActiveQuery class for [[Ttpbit]].
 *
 * @see Ttpbit
 */
class TtpbitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttpbit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttpbit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }


	public function callSP( $post ) {
		$post[ 'UserID' ]          = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]       = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]      = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'PBNo' ]            = $post[ 'PBNo' ] ?? '--';
		$post[ 'MotorType' ]       = $post[ 'MotorType' ] ?? '--';
		$post[ 'MotorTahun' ]      = floatval( $post[ 'MotorTahun' ] ?? 1900 );
		$post[ 'MotorWarna' ]      = $post[ 'MotorWarna' ] ?? '--';
		$post[ 'MotorNoMesin' ]    = $post[ 'MotorNoMesin' ] ?? '--';
		$post[ 'MotorNoRangka' ]   = $post[ 'MotorNoRangka' ] ?? '--';
		$post[ 'MotorAutoN' ]      = floatval( $post[ 'MotorAutoN' ] ?? 0 );
		$post[ 'MotorAutoNOLD' ]   = floatval( $post[ 'MotorAutoNOLD' ] ?? 0 );
		$post[ 'MotorNoMesinOLD' ] = $post[ 'MotorNoMesinOLD' ] ?? '--';
		try {
			General::cCmd( "CALL fpbit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'PBNo' ] )
			       ->bindParam( 6, $post[ 'MotorType' ] )
			       ->bindParam( 7, $post[ 'MotorTahun' ] )
			       ->bindParam( 8, $post[ 'MotorWarna' ] )
			       ->bindParam( 9, $post[ 'MotorNoMesin' ] )
			       ->bindParam( 10, $post[ 'MotorNoRangka' ] )
			       ->bindParam( 11, $post[ 'MotorAutoN' ] )
			       ->bindParam( 12, $post[ 'MotorAutoNOLD' ] )
			       ->bindParam( 13, $post[ 'MotorNoMesinOLD' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'status'     => $status,
			'keterangan' => str_replace(" ","&nbsp;",$keterangan)
		];
	}
}
