<?php
namespace aunit\models;
use aunit\components\Menu;
use common\components\General;
use Yii;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttrk]].
 *
 * @see Ttrk
 */
class TtrkQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttrk[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttrk|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function dsTJualFillByNo() {
		return $this->select( new Expression( "ttsk.SKTgl, ttdk.CusKode, tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusKelurahan, 
                         tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, tdcustomer.CusTelepon, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoRangka, 
                         ttdk.DKHarga, tdlokasi.LokasiNama, tmotor.FBHarga, ttdk.DKTgl, ttrk.RKNo, ttrk.RKTgl, ttrk.LokasiKode, ttrk.RKMemo, ttrk.UserID, ttrk.RKJam, ttrk.DKNo, 
                         ttrk.MotorNoMesin, ttrk.SKNo, tdcustomer.CusKTP, tdcustomer.CusKodePos, ttrk.NoGL, tmotor.MotorAutoN, tmotor.SKHarga, tmotor.SDHarga, tmotor.MMHarga, 
                         tmotor.RKHarga, tmotor.SSHarga" ) )
		            ->join( "LEFT OUTER JOIN", "tmotor", "ttrk.RKNo = tmotor.RKNo" )
		            ->join( "LEFT OUTER JOIN", "ttsk", "ttrk.SKNo = ttsk.SKNo" )
		            ->join( "LEFT OUTER JOIN", "tdlokasi", "ttrk.LokasiKode = tdlokasi.LokasiKode" )
		            ->join( "LEFT OUTER JOIN", "ttdk", "ttrk.DKNo = ttdk.DKNo" )
		            ->join( "LEFT OUTER JOIN", "tdcustomer", "ttdk.CusKode = tdcustomer.CusKode" );
	}
	public function InsertMotorRKNo( $MyDKNo, $MySKNo, $RKNo, $MyMesinNo ) {
		$MyMotorAutoN = $this->GetMesinNo( $MyMesinNo, $RKNo );
		Yii::$app->db
			->createCommand( "INSERT INTO tmotor (MotorAutoN, MotorNoMesin, MotorNoRangka, MotorType, MotorWarna, MotorTahun, DealerKode,
                    	FBHarga, SKHarga, SDHarga, MMHarga, RKHarga, SSHarga, RKNo)
                    	SELECT :MyMotorAutoN, MotorNoMesin, MotorNoRangka, MotorType, MotorWarna, MotorTahun, DealerKode, 
                    	FBHarga, FBHarga, FBHarga, 0, FBHarga, FBHarga, :RKNo FROM  tmotor WHERE SKNo = :SKNo", [
				':MyMotorAutoN' => $MyMotorAutoN,
				':RKNo'         => $RKNo,
				':SKNo'         => $MySKNo
			] )->execute();
		Yii::$app->db
			->createCommand( "Update tmotor SET FakturAHMNo = :FakturAHMNo WHERE SKNo = :SKNo AND DKNo = :DKNo", [
				':FakturAHMNo' => $RKNo,
				':SKNo'        => $MySKNo,
				':DKNo'        => $MyDKNo
			] )->execute();
	}
	public function GetMesinNo( $MyMesinNo, $RKNo ) {
		$A = Yii::$app->db
			->createCommand( "Select IFNULL(MAX(MotorAutoN) + 1,0) FROM tmotor WHERE  MotorNoMesin = :MotorNoMesin AND RKNo <> :RKNo", [
				':MotorNoMesin' => $MyMesinNo,
				':RKNo'         => $RKNo
			] )->queryScalar();
		if ( $A == null ) {
			return 0;
		}
		return $A;
	}
	public function DelPemutihanByRK( $RKNo, $MySKNo ) {
		Yii::$app->db
			->createCommand( "UPDATE tmotor SET BPKBTglJadi = :RKTglInit WHERE BPKBTglJadi = :RKTgl AND SKNo = :SKNo;
				UPDATE tmotor SET BPKBTglAmbil = :RKTglInit, BPKBPenerima = '--' WHERE BPKBTglAmbil = :RKTgl AND SKNo = :SKNo;
				UPDATE tmotor SET STNKTglMasuk = :RKTglInit WHERE STNKTglMasuk = :RKTgl AND SKNo = :SKNo;
				UPDATE tmotor SET STNKTglBayar = :RKTglInit WHERE STNKTglBayar = :RKTgl AND SKNo = :SKNo;
				UPDATE tmotor SET STNKTglJadi = :RKTglInit WHERE STNKTglJadi = :RKTgl AND SKNo = :SKNo;
				UPDATE tmotor SET STNKTglAmbil = :RKTglInit, STNKPenerima = '--'  WHERE STNKTglAmbil = :RKTgl AND SKNo = :SKNo;
				UPDATE tmotor SET PlatTgl = :RKTglInit WHERE PlatTgl = :RKTgl AND SKNo = :SKNo;
				UPDATE tmotor SET PlatTglAmbil = :RKTglInit, PlatPenerima = '--' WHERE PlatTglAmbil = :RKTgl AND SKNo = :SKNo;
				UPDATE tmotor SET FakturAHMTgl = :RKTglInit WHERE FakturAHMTgl = :RKTgl AND SKNo = :SKNo;
				UPDATE tmotor SET FakturAHMTglAmbil = :RKTglInit WHERE FakturAHMTglAmbil = :RKTgl AND SKNo = :SKNo;
				UPDATE tmotor SET NoticeTglJadi = :RKTglInit WHERE NoticeTglJadi = :RKTgl AND SKNo = :SKNo;
				UPDATE tmotor SET NoticeTglAmbil = :RKTglInit, NoticePenerima = '--'  WHERE NoticeTglAmbil = :RKTgl AND SKNo = :SKNo;
				UPDATE tmotor SET FakturAHMNo = '--' WHERE SKNo = :SKNo", [
				':RKTglInit' => '1900-01-01',
				':RKTgl'     => $RKNo,
				':SKNo'      => $MySKNo
			] )->execute();
	}
	public function SetPemutihanByRK( $RKNo, $MySKNo ) {
		Yii::$app->db
			->createCommand( "UPDATE tmotor SET BPKBTglJadi = :RKTgl WHERE BPKBTglJadi = '1900-01-01' AND SKNo = :SKNo;
				UPDATE tmotor SET BPKBTglAmbil = :RKTgl, BPKBPenerima = 'Retur' WHERE BPKBTglAmbil = '1900-01-01' AND SKNo = :SKNo;
				UPDATE tmotor SET STNKTglMasuk = :RKTgl WHERE STNKTglMasuk = '1900-01-01' AND SKNo = :SKNo;
				UPDATE tmotor SET STNKTglBayar = :RKTgl WHERE STNKTglBayar = '1900-01-01' AND SKNo = :SKNo;
				UPDATE tmotor SET STNKTglJadi = :RKTgl WHERE STNKTglJadi = '1900-01-01' AND SKNo = :SKNo;
				UPDATE tmotor SET STNKTglAmbil = :RKTgl, STNKPenerima = 'Retur'  WHERE STNKTglAmbil = '1900-01-01' AND SKNo = :SKNo;
				UPDATE tmotor SET PlatTgl = :RKTgl WHERE PlatTgl = '1900-01-01' AND SKNo = :SKNo;
				UPDATE tmotor SET PlatTglAmbil = :RKTgl, PlatPenerima = 'Retur' WHERE PlatTglAmbil = '1900-01-01' AND SKNo = :SKNo;
				UPDATE tmotor SET FakturAHMTgl = :RKTgl WHERE FakturAHMTgl = '1900-01-01' AND SKNo = :SKNo;
				UPDATE tmotor SET FakturAHMTglAmbil = :RKTgl WHERE FakturAHMTglAmbil = '1900-01-01' AND SKNo = :SKNo;
				UPDATE tmotor SET NoticeTglJadi = :RKTgl WHERE NoticeTglJadi = '1900-01-01' AND SKNo = :SKNo;
				UPDATE tmotor SET NoticeTglAmbil = :RKTgl, NoticePenerima = 'Retur'  WHERE NoticeTglAmbil = '1900-01-01' AND SKNo = :SKNo;", [
				':RKTgl' => $RKNo,
				':SKNo'  => $MySKNo
			] )->execute();
	}
	public function HapusMotorRKNo( $MyDKNo, $MySKNo, $RKNo ) {
		Yii::$app->db
			->createCommand( "Update tmotor SET FakturAHMNo = '--' WHERE SKNo = :SKNo OR DKNo = :DKNo;
				DELETE FROM tmotor WHERE RKNo = :RKNo;", [
				':SKNo' => $MySKNo,
				':DKNo' => $MyDKNo,
				':RKNo' => $RKNo
			] )->execute();
	}
	public function EditDelTvMotorLokasi( $RKNo ) {
		Yii::$app->db
			->createCommand( "DELETE FROM tvmotorlokasi WHERE NoTrans = :RKNo;", [
				':RKNo' => $RKNo
			] )->execute();
	}
	public function PerbaikiSalahNoSinRKSK( $RKNo ) {
		$SQLReader = Yii::$app->db
			->createCommand( "SELECT  tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.DKNo, tmotor.SKNo, tmotor.RKNo, ttsk.SKTgl, ttrk.RKTgl
			FROM tmotor 
			INNER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo
			INNER JOIN ttrk ON ttrk.RKNo = tmotor.RKno
			WHERE ttsk.SKTgl < ttrk.RKtgl AND ttrk.RKNo = :RKNo
			", [
				':RKNo' => $RKNo
			] )->queryOne();
		if ( $SQLReader != null ) {
			$SQLReader1 = Yii::$app->db
				->createCommand( "SELECT MotorAutoN, MotorNoMesin FROM tmotor WHERE DKNo = '--' AND SKNo = '--' AND MotorNoMesin = :MotorNoMesin", [
					':MotorNoMesin' => $SQLReader[ 'MotorNoMesin' ]
				] )->queryOne();
			Yii::$app->db
				->createCommand( "UPDATE tmotor SET DKNo = :DKNo, SKNo = :SKNo WHERE MotorAutoN = :MotorAutoNSeharusnya AND MotorNoMesin = :MotorNoMesinSeharusnya;
					UPDATE tmotor SET DKNo = '--', SKNo = '--' WHERE MotorAutoN = :MotorAutoN AND MotorNoMesin = :MotorNoMesin;", [
					':DKNo'                   => $SQLReader[ 'DKNo' ],
					':SKNo'                   => $SQLReader[ 'SKNo' ],
					':MotorAutoNSeharusnya'   => $SQLReader1[ 'MotorAutoN' ],
					':MotorNoMesinSeharusnya' => $SQLReader1[ 'MotorNoMesin' ],
					':MotorAutoN'             => $SQLReader[ 'MotorNoMesin' ],
					':MotorNoMesin'           => $SQLReader[ 'MotorNoMesin' ]
				] )->execute();
		}
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]    = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'RKNo' ]          = $post[ 'RKNo' ] ?? '--';
		$post[ 'RKNoBaru' ]      = $post[ 'RKNoView' ] ?? '--';
		$post[ 'CusKode' ]       = $post[ 'CusKode' ] ?? '--';
		$post[ 'SKNo' ]          = $post[ 'SKNo' ] ?? '--';
		$post[ 'SKTgl' ]         = $post[ 'SKTgl' ] ?? ! empty( $post[ 'SKTgl' ] ) ?: date( 'Y-m-d' );
		$post[ 'DKNo' ]          = $post[ 'DKNo' ] ?? '--';
		$post[ 'DKTgl' ]         = $post[ 'DKTgl' ] ?? ! empty( $post[ 'DKTgl' ] ) ?: date( 'Y-m-d' );
		$post[ 'RKTgl' ]         = $post[ 'RKTgl' ] ?? ! empty( $post[ 'RKTgl' ] ) ?: date( 'Y-m-d' );
		$post[ 'RKJam' ]         = $post[ 'RKJam' ] ?? ! empty( $post[ 'RKJam' ] ) ?: date( 'Y-m-d H:i:s' );
		$post[ 'RKMemo' ]        = $post[ 'RKMemo' ] ?? '--';
		$post[ 'CusNama' ]       = $post[ 'CusNama' ] ?? '--';
		$post[ 'CusKTP' ]        = $post[ 'CusKTP' ] ?? '--';
		$post[ 'CusKabupaten' ]  = $post[ 'CusKabupaten' ] ?? '--';
		$post[ 'CusAlamat' ]     = $post[ 'CusAlamat' ] ?? '--';
		$post[ 'CusRT' ]         = $post[ 'CusRT' ] ?? '--';
		$post[ 'CusRW' ]         = $post[ 'CusRW' ] ?? '--';
		$post[ 'CusKecamatan' ]  = $post[ 'CusKecamatan' ] ?? '--';
		$post[ 'CusTelepon' ]    = $post[ 'CusTelepon' ] ?? '--';
		$post[ 'CusKodePos' ]    = $post[ 'CusKodePos' ] ?? '--';
		$post[ 'CusKelurahan' ]  = $post[ 'CusKelurahan' ] ?? '--';
		$post[ 'MotorType' ]     = $post[ 'MotorType' ] ?? '--';
		$post[ 'MotorTahun' ]    = floatval( $post[ 'MotorTahun' ] ?? 1900 );
		$post[ 'MotorWarna' ]    = $post[ 'MotorWarna' ] ?? '--';
		$post[ 'DKHarga' ]       = floatval( $post[ 'DKHarga' ] ?? 0.00 );
		$post[ 'MotorNoMesin' ]  = $post[ 'MotorNoMesin' ] ?? '--';
		$post[ 'MotorNoRangka' ] = $post[ 'MotorNoRangka' ] ?? '--';
		$post[ 'NoGL' ]          = $post[ 'NoGL' ] ?? '--';
		$post[ 'LokasiKodeRK' ]  = $post[ 'LokasiKodeRK' ] ?? '--';
		try {
			General::cCmd( "SET @RKNo = ?;" )->bindParam( 1, $post[ 'RKNo' ] )->execute();
			General::cCmd( "CALL frk(?,@Status,@Keterangan,?,?,?,@RKNo,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'RKNoBaru' ] )
			       ->bindParam( 6, $post[ 'CusKode' ] )
			       ->bindParam( 7, $post[ 'SKNo' ] )
			       ->bindParam( 8, $post[ 'SKTgl' ] )
			       ->bindParam( 9, $post[ 'DKNo' ] )
			       ->bindParam( 10, $post[ 'DKTgl' ] )
			       ->bindParam( 11, $post[ 'RKTgl' ] )
			       ->bindParam( 12, $post[ 'RKJam' ] )
			       ->bindParam( 13, $post[ 'RKMemo' ] )
			       ->bindParam( 14, $post[ 'CusNama' ] )
			       ->bindParam( 15, $post[ 'CusKTP' ] )
			       ->bindParam( 16, $post[ 'CusKabupaten' ] )
			       ->bindParam( 17, $post[ 'CusAlamat' ] )
			       ->bindParam( 18, $post[ 'CusRT' ] )
			       ->bindParam( 19, $post[ 'CusRW' ] )
			       ->bindParam( 20, $post[ 'CusKecamatan' ] )
			       ->bindParam( 21, $post[ 'CusTelepon' ] )
			       ->bindParam( 22, $post[ 'CusKodePos' ] )
			       ->bindParam( 23, $post[ 'CusKelurahan' ] )
			       ->bindParam( 24, $post[ 'MotorType' ] )
			       ->bindParam( 25, $post[ 'MotorTahun' ] )
			       ->bindParam( 26, $post[ 'MotorWarna' ] )
			       ->bindParam( 27, $post[ 'DKHarga' ] )
			       ->bindParam( 28, $post[ 'MotorNoMesin' ] )
			       ->bindParam( 29, $post[ 'MotorNoRangka' ] )
			       ->bindParam( 30, $post[ 'NoGL' ] )
			       ->bindParam( 31, $post[ 'LokasiKodeRK' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @RKNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'RKNo'       => $post[ 'RKNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$RKNo       = $exe[ '@RKNo' ] ?? $post[ 'RKNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'RKNo'       => $RKNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
}
