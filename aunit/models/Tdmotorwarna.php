<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "tdmotorwarna".
 *
 * @property string $MotorType
 * @property string $MotorWarna
 */
class Tdmotorwarna extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdmotorwarna';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['MotorType', 'MotorWarna'], 'required'],
            [['MotorType'], 'string', 'max' => 50],
            [['MotorWarna'], 'string', 'max' => 35],
            [['MotorType', 'MotorWarna'], 'unique', 'targetAttribute' => ['MotorType', 'MotorWarna']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'MotorType' => 'Motor Type',
            'MotorWarna' => 'Motor Warna',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
        return [
            'MotorType' => [
                'label' => 'Type Motor',
                'width' => 200,
                'name' => 'MotorType'
            ],
            'MotorWarna' => [
                'label' => 'Warna Motor',
                'width' => 300,
                'name' => 'MotorWarna'
            ],
        ];
    }

	public function getMotorType() {
		return $this->hasOne( Tdmotortype::className(), [ 'MotorType' => 'MotorType' ] );
	}

    /**
     * {@inheritdoc}
     * @return TdmotorwarnaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdmotorwarnaQuery(get_called_class());
    }
}
