<?php

namespace aunit\models;

/**
 * This is the model class for table "ttprit".
 *
 * @property string $PRNo
 * @property int $PRAuto
 * @property string $BrgKode
 * @property string $PRQty
 * @property string $PRHrgBeli Standard Cost
 * @property string $PRHrgJual HargaBeliReal
 * @property string $PRDiscount
 * @property string $PRPajak
 * @property string $LokasiKode
 */
class Ttprit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttprit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PRNo', 'PRAuto'], 'required'],
            [['PRAuto'], 'integer'],
            [['PRQty', 'PRHrgBeli', 'PRHrgJual', 'PRDiscount', 'PRPajak'], 'number'],
            [['PRNo'], 'string', 'max' => 10],
            [['BrgKode'], 'string', 'max' => 20],
            [['LokasiKode'], 'string', 'max' => 15],
            [['PRNo', 'PRAuto'], 'unique', 'targetAttribute' => ['PRNo', 'PRAuto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PRNo' => 'Pr No',
            'PRAuto' => 'Pr Auto',
            'BrgKode' => 'Brg Kode',
            'PRQty' => 'Pr Qty',
            'PRHrgBeli' => 'Pr Hrg Beli',
            'PRHrgJual' => 'Pr Hrg Jual',
            'PRDiscount' => 'Pr Discount',
            'PRPajak' => 'Pr Pajak',
            'LokasiKode' => 'Lokasi Kode',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtpritQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtpritQuery(get_called_class());
    }
}
