<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Vhutangbayar]].
 *
 * @see Vhutangbayar
 */
class VhutangbayarQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Vhutangbayar[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Vhutangbayar|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
