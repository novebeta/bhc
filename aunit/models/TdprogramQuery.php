<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Tdprogram]].
 *
 * @see Tdprogram
 */
class TdprogramQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdprogram[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdprogram|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	public function combo() {
		return $this->select( [ "ProgramNama as label", "ProgramNama as value" ] )
		            ->orderBy( 'ProgramNama' )
		            ->asArray()
		            ->all();
	}
}
