<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Vbiayaharian]].
 *
 * @see Vbiayaharian
 */
class VbiayaharianQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Vbiayaharian[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Vbiayaharian|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
