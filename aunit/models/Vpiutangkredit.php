<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "vpiutangkredit".
 *
 * @property string $GLLink
 * @property string $NoGL
 * @property string $TglGL
 * @property string $KodeTrans SS,FB,DK,SK,SM,PB,SD,PD,KK,KM,BK,BM
 * @property int $GLAutoN
 * @property string $NoAccount
 * @property string $NamaAccount
 * @property string $MemoGL
 * @property string $KeteranganGL
 * @property string $KreditGL
 * @property string $HPLINk
 */
class Vpiutangkredit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vpiutangkredit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['TglGL'], 'safe'],
            [['GLAutoN'], 'required'],
            [['GLAutoN'], 'integer'],
            [['KreditGL'], 'number'],
            [['GLLink', 'HPLINk'], 'string', 'max' => 18],
            [['NoGL', 'NoAccount'], 'string', 'max' => 10],
            [['KodeTrans'], 'string', 'max' => 2],
            [['NamaAccount', 'KeteranganGL'], 'string', 'max' => 150],
            [['MemoGL'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'GLLink' => 'Gl Link',
            'NoGL' => 'No Gl',
            'TglGL' => 'Tgl Gl',
            'KodeTrans' => 'SS,FB,DK,SK,SM,PB,SD,PD,KK,KM,BK,BM',
            'GLAutoN' => 'Gl Auto N',
            'NoAccount' => 'No Account',
            'NamaAccount' => 'Nama Account',
            'MemoGL' => 'Memo Gl',
            'KeteranganGL' => 'Keterangan Gl',
            'KreditGL' => 'Kredit Gl',
            'HPLINk' => 'Hpli Nk',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VpiutangkreditQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VpiutangkreditQuery(get_called_class());
    }
}
