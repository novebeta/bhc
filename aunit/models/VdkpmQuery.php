<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Vdkpm]].
 *
 * @see Vdkpm
 */
class VdkpmQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Vdkpm[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Vdkpm|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
