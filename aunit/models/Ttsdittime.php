<?php

namespace aunit\models;

/**
 * This is the model class for table "ttsdittime".
 *
 * @property string $SDNo
 * @property string $KarKode
 * @property string $Nomor
 * @property string $Start
 * @property string $Finish
 * @property string $Status
 */
class Ttsdittime extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttsdittime';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SDNo', 'KarKode', 'Nomor', 'Start', 'Finish'], 'required'],
            [['Nomor'], 'number'],
            [['Start', 'Finish'], 'safe'],
            [['SDNo', 'KarKode'], 'string', 'max' => 10],
            [['Status'], 'string', 'max' => 30],
            [['SDNo', 'KarKode', 'Nomor', 'Start', 'Finish'], 'unique', 'targetAttribute' => ['SDNo', 'KarKode', 'Nomor', 'Start', 'Finish']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SDNo' => 'Sd No',
            'KarKode' => 'Kar Kode',
            'Nomor' => 'Nomor',
            'Start' => 'Start',
            'Finish' => 'Finish',
            'Status' => 'Status',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtsdittimeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtsdittimeQuery(get_called_class());
    }
}
