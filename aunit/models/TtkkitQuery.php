<?php

namespace aunit\models;

use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttkkit]].
 *
 * @see Ttkkit
 */
class TtkkitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttkkit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttkkit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'LokasiKode' ] = Menu::getUserLokasi()[ 'LokasiKode' ];
        $post[ 'KKNo' ]       = $post[ 'KKNo' ] ?? '--';
        $post[ 'FBNo' ]       = $post[ 'FBNo' ] ?? '--';
        $post[ 'KKBayar' ]    = floatval( $post[ 'KKBayar' ] ?? 0 );
        $post[ 'KKNoOLD' ]    = $post[ 'KKNoOLD' ] ?? '--';
        $post[ 'FBNoOLD' ]    = $post[ 'FBNoOLD' ] ?? '--';
        try {
            General::cCmd( "CALL fkkit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'LokasiKode' ] )
                ->bindParam( 5, $post[ 'KKNo' ] )
                ->bindParam( 6, $post[ 'FBNo' ] )
                ->bindParam( 7, $post[ 'KKBayar' ] )
                ->bindParam( 8, $post[ 'KKNoOLD' ] )
                ->bindParam( 9, $post[ 'FBNoOLD' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
    public function callSPPengajuan( $post ) {
        $post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'LokasiKode' ] = Menu::getUserLokasi()[ 'LokasiKode' ];
        $post[ 'KKNo' ]       = $post[ 'KKNo' ] ?? '--';
        $post[ 'FBNo' ]       = $post[ 'FBNo' ] ?? '--';
        $post[ 'KKBayar' ]    = $post[ 'KKBayar' ] ?? 0;
        $post[ 'KKNoOLD' ]    = $post[ 'KKNoOLD' ] ?? '--';
        $post[ 'FBNoOLD' ]  = $post[ 'FBNoOLD' ] ?? '--';
        try {
            General::cCmd( "CALL fkpit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'LokasiKode' ] )
                ->bindParam( 5, $post[ 'KKNo' ] )
                ->bindParam( 6, $post[ 'FBNo' ] )
                ->bindParam( 7, $post[ 'KKBayar' ] )
                ->bindParam( 8, $post[ 'KKNoOLD' ] )
                ->bindParam( 9, $post[ 'FBNoOLD' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
