<?php
namespace aunit\models;
use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
/**
 * This is the ActiveQuery class for [[Ttamhd]].
 *
 * @see Ttamhd
 */
class TtamhdQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttamhd[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttamhd|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ] = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'AMNo' ]       = $post[ 'AMNo' ] ?? '--';
		$post[ 'AMNoBaru' ]   = $post[ 'AMNo' ];
		$post[ 'AMTgl' ]      = date('Y-m-d', strtotime($post[ 'AMTgl' ] ?? date( 'Y-m-d' )));
		$post[ 'AMJam' ]      = date('Y-m-d H:i:s', strtotime($post[ 'AMJam' ] ?? date( 'Y-m-d H:i:s' )));
		$post[ 'AMMemo' ]     = $post[ 'AMMemo' ] ?? '--';
		$post[ 'AMTotal' ]    = floatval( $post[ 'AMTotal' ] ?? 0 );
		$post[ 'NoGL' ]       = $post[ 'NoGL' ] ?? '--';
		$post[ 'MotorTahun' ] = $post[ 'MotorTahun' ] ?? 1900;
		$post[ 'FBNo' ]       = $post[ 'FBNo' ] ?? '--';
		try {
			General::cCmd( "SET @AMNo = ?;" )->bindParam( 1, $post[ 'AMNo' ] )->execute();
			General::cCmd( "CALL famhd(?,@Status,@Keterangan,?,?,?,@AMNo,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'AMNoBaru' ] )
			       ->bindParam( 6, $post[ 'AMTgl' ] )
			       ->bindParam( 7, $post[ 'AMJam' ] )
			       ->bindParam( 8, $post[ 'AMMemo' ] )
			       ->bindParam( 9, $post[ 'AMTotal' ] )
			       ->bindParam( 10, $post[ 'NoGL' ] )
			       ->bindParam( 11, $post[ 'MotorTahun' ] )
			       ->bindParam( 12, $post[ 'FBNo' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @AMNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'AMNo'       => $post[ 'AMNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$AMNo       = $exe[ '@AMNo' ] ?? $post[ 'AMNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'AMNo'       => $AMNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
}
