<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "tdlokasi".
 *
 * @property string $LokasiKode
 * @property string $LokasiNama
 * @property string $LokasiPetugas
 * @property string $LokasiAlamat
 * @property string $LokasiStatus
 * @property string $LokasiTelepon
 * @property string $LokasiNomor
 * @property string $NoAccount
 * @property string $LokasiJenis
 */
class Tdlokasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdlokasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['LokasiKode'], 'required'],
            [['LokasiKode'], 'string', 'max' => 15],
            [['LokasiNama'], 'string', 'max' => 35],
            [['LokasiPetugas', 'NoAccount'], 'string', 'max' => 10],
            [['LokasiAlamat'], 'string', 'max' => 75],
            [['LokasiStatus'], 'string', 'max' => 1],
            [['LokasiTelepon'], 'string', 'max' => 30],
            [['LokasiNomor'], 'string', 'max' => 2],
            [['LokasiJenis'], 'string', 'max' => 6],
            [['LokasiKode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'LokasiKode' => 'Kode',
            'LokasiNama' => 'Nama',
            'LokasiPetugas' => 'Petugas',
            'LokasiAlamat' => 'Alamat',
            'LokasiStatus' => 'Status',
            'LokasiTelepon' => 'Telepon',
            'LokasiNomor' => 'No Urut',
            'NoAccount' => 'COA',
            'LokasiJenis' => 'Jenis',
        ];
    }

    public static function colGrid()
    {
        return [
            'LokasiKode' => ['width' => 100, 'label' => 'Kode Warehouse', 'name' => 'LokasiKode'],
            'LokasiNama' => ['width' => 250, 'label' => 'Nama Warehouse', 'name' => 'LokasiNama'],
            'LokasiAlamat' => ['width' => 355, 'label' => 'Alamat', 'name' => 'LokasiAlamat'],
            'LokasiTelepon' => ['width' => 100, 'label' => 'Telepon', 'name' => 'LokasiTelepon'],
            'LokasiStatus' => ['width' => 55, 'label' => 'Status', 'name' => 'LokasiStatus'],
            'LokasiPetugas' => ['width' => 70, 'label' => 'Petugas', 'name' => 'LokasiPetugas'],
            'SalesNama' => ['width' => 70, 'label' => 'Nama Petugas', 'name' => 'SalesNama'],
        ];
    }

    public function getSales()
    {
        return $this->hasOne(Tdsales::className(), ['SalesKode' => 'LokasiPetugas']);
    }

    /**
     * {@inheritdoc}
     * @return TdlokasiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdlokasiQuery(get_called_class());
    }
}
