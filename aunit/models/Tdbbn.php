<?php
namespace aunit\models;
/**
 * This is the model class for table "tdbbn".
 *
 * @property string $Kabupaten
 * @property string $MotorType
 * @property Tdmotortype $motorType
 * @property mixed $motorTypes
 * @property string $BBNBiaya
 * @property string $BBNTahunLalu [decimal(11,2)]
 */
class Tdbbn extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'tdbbn';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'Kabupaten', 'MotorType' ], 'required' ],
			[ [ 'BBNBiaya' ], 'number' ],
			[ [ 'BBNTahunLalu' ], 'number' ],
			[ [ 'Kabupaten' ], 'string', 'max' => 35 ],
			[ [ 'MotorType' ], 'string', 'max' => 50 ],
			[ [ 'Kabupaten', 'MotorType'], 'unique', 'targetAttribute' => [ 'Kabupaten', 'MotorType' ] ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'Kabupaten' => 'Kabupaten',
			'MotorType' => 'Type Motor',
			'BBNBiaya'  => 'Tarif BBN',
			'BBNTahunLalu'=> 'BBN Tahun Lalu',
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public static  function colGrid() {
		return [
			'Kabupaten'             => ['label' => 'Kabupaten','width' => 150,'name'  => 'Kabupaten'],
			'tdmotortype.MotorType' => ['label' => 'Type Motor','width' => 260,'name'  => 'MotorType'],
			'tdmotortype.MotorNama' => ['label' => 'Motor Nama','width' => 259,'name'  => 'MotorNama'],
			'BBNBiaya'              => ['label' => 'Tarif BBN','width' => 100,'name' => 'BBNBiaya','formatter' => 'number','align' => 'right'],
			'BBNTahunLalu'              => ['label' => 'BBN Tahun lalu','width' => 100,'name' => 'BBNTahunLalu','formatter' => 'number','align'     => 'right'],
		];
	}
	public function getMotorTypes() {
		return $this->hasOne( Tdmotortype::className(), [ 'MotorType' => 'MotorType' ] );
	}
	/**
	 * {@inheritdoc}
	 * @return TdbbnQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TdbbnQuery( get_called_class() );
	}
}
