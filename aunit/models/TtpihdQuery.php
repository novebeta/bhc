<?php

namespace aunit\models;

use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttpihd]].
 *
 * @see Ttpihd
 */
class TtpihdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttpihd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttpihd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function ttpihdFillByNo() {
        return $this->select( new Expression( "ttpihd.PINo,
                            ttpihd.PITgl,
                            ttpihd.SupKode,
                            ttpihd.PINoRef,
                            ttpihd.LokasiKode,
                            ttpihd.PIKeterangan,
                            ttpihd.PISubTotal,
                            ttpihd.PIDiscFinal,
                            ttpihd.PITotalPajak,
                            ttpihd.PIBiayaKirim,
                            ttpihd.PITotal,
                            ttpihd.PITerm,
                            ttpihd.PITglTempo,
                            ttpihd.UserID,
                            tdsupplier.SupNama,
                            ttpihd.NoGL,
                            ttpihd.KodeTrans,
                            ttpihd.PSNo,
                            IFNULL(
                                ttpshd.PSTgl,
                                STR_TO_DATE('01/01/1900', '%m/%d/%Y')
                            ) AS PSTgl,
                            ttpihd.PosKode,
                            ttpihd.Cetak,
                            ttpihd.PIJenis,
                            ttpihd.PITerbayar" ) )
            ->join( "LEFT OUTER JOIN", "tdsupplier", "ttpihd.SupKode = tdsupplier.SupKode" )
            ->join( "LEFT OUTER JOIN", "ttpshd", "ttpihd.PSNo = ttpshd.PSNo" )
            ->groupBy("ttpihd.PINo, ttpihd.PIJenis, ttpihd.PITerbayar");
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
//        $post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'PINo' ]          = $post[ 'PINo' ] ?? '--';
        $post[ 'PINoBaru' ]      = $post[ 'PINoView' ] ?? '--';
        $post[ 'PITgl' ]         = date('Y-m-d H:i:s', strtotime($post[ 'PITgl' ] ?? date( 'Y-m-d H:i:s' )));
        $post[ 'PIJenis' ]       = $post[ 'PIJenis' ] ?? '--';
        $post[ 'PSNo' ]          = $post[ 'PSNo' ] ?? '--';
        $post[ 'KodeTrans' ]     = $post[ 'KodeTrans' ] ?? '--';
        $post[ 'LokasiKode' ]    = $post[ 'LokasiKode' ] ?? '--';
        $post[ 'SupKode' ]       = $post[ 'SupKode' ] ?? '--';
        $post[ 'PINoRef' ]       = $post[ 'PINoRef' ] ?? '--';
        $post[ 'PITerm' ]        = floatval( $post[ 'PITerm' ] ?? 0 );
        $post[ 'PITglTempo' ]    = date('Y-m-d H:i:s', strtotime($post[ 'PITglTempo' ] ?? date( 'Y-m-d H:i:s' )));
        $post[ 'PISubTotal' ]    = floatval( $post[ 'PISubTotal' ] ?? 0 );
        $post[ 'PIDiscFinal' ]   = floatval( $post[ 'PIDiscFinal' ] ?? 0 );
        $post[ 'PITotalPajak' ]  = floatval( $post[ 'PITotalPajak' ] ?? 0 );
        $post[ 'PIBiayaKirim' ]  = floatval( $post[ 'PIBiayaKirim' ] ?? 0 );
        $post[ 'PITotal' ]       = floatval( $post[ 'PITotal' ] ?? 0 );
        $post[ 'PITerbayar' ]    = floatval( $post[ 'PITerbayar' ] ?? 0 );
        $post[ 'PIKeterangan' ]  = $post[ 'PIKeterangan' ] ?? '--';
        $post[ 'NoGL' ]          = $post[ 'NoGL' ] ?? '--';
        $post[ 'Cetak' ]         = $post[ 'Cetak' ] ?? 'Belum';
        try {
            General::cCmd( "SET @PINo = ?;" )->bindParam( 1, $post[ 'PINo' ] )->execute();
            General::cCmd( "CALL fpihd(?,@Status,@Keterangan,?,?,?,@PINo,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'PINoBaru' ] )
                ->bindParam( 6, $post[ 'PITgl' ] )
                ->bindParam( 7, $post[ 'PIJenis' ] )
                ->bindParam( 8, $post[ 'PSNo' ] )
                ->bindParam( 9, $post[ 'KodeTrans' ] )
                ->bindParam( 10, $post[ 'LokasiKode' ] )
                ->bindParam( 11, $post[ 'SupKode' ] )
                ->bindParam( 12, $post[ 'PINoRef' ] )
                ->bindParam( 13, $post[ 'PITerm' ] )
                ->bindParam( 14, $post[ 'PITglTempo' ] )
                ->bindParam( 15, $post[ 'PISubTotal' ] )
                ->bindParam( 16, $post[ 'PIDiscFinal' ] )
                ->bindParam( 17, $post[ 'PITotalPajak' ] )
                ->bindParam( 18, $post[ 'PIBiayaKirim' ] )
                ->bindParam( 19, $post[ 'PITotal' ] )
                ->bindParam( 20, $post[ 'PITerbayar' ] )
                ->bindParam( 21, $post[ 'PIKeterangan' ] )
                ->bindParam( 22, $post[ 'NoGL' ] )
                ->bindParam( 23, $post[ 'Cetak' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @PINo,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'PINo'       => $post[ 'PINo' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $PINo       = $exe[ '@PINo' ] ?? $post[ 'PINo' ];
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'PINo'       => $PINo,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
