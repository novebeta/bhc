<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "ttmteamtarget".
 *
 * @property string $TeamKode
 * @property string $TargetTgl
 * @property string $TargetQty
 */
class Ttmteamtarget extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttmteamtarget';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['TeamKode', 'TargetTgl'], 'required'],
            [['TargetTgl'], 'safe'],
            [['TargetQty'], 'number'],
            [['TeamKode'], 'string', 'max' => 15],
            [['TeamKode', 'TargetTgl'], 'unique', 'targetAttribute' => ['TeamKode', 'TargetTgl']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'TeamKode' => 'Team Kode',
            'TargetTgl' => 'Target Tgl',
            'TargetQty' => 'Target Qty',
        ];
    }



    /**
     * {@inheritdoc}
     * @return TtmteamtargetQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtmteamtargetQuery(get_called_class());
    }
}
