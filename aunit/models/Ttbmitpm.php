<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "ttbmitpm".
 *
 * @property string $BMNo
 * @property string $DKNo
 * @property string $BMJenis
 * @property string $BMBayar
 */
class Ttbmitpm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttbmitpm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BMNo', 'DKNo', 'BMJenis'], 'required'],
            [['BMBayar'], 'number'],
            [['BMNo', 'DKNo'], 'string', 'max' => 12],
            [['BMJenis'], 'string', 'max' => 20],
            [['BMNo', 'DKNo', 'BMJenis'], 'unique', 'targetAttribute' => ['BMNo', 'DKNo', 'BMJenis']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'BMNo' => 'Bm No',
            'DKNo' => 'Dk No',
            'BMJenis' => 'Bm Jenis',
            'BMBayar' => 'Bm Bayar',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtbmitpmQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtbmitpmQuery(get_called_class());
    }
}
