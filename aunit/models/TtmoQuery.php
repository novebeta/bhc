<?php
namespace aunit\models;
use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
/**
 * This is the ActiveQuery class for [[Ttmo]].
 *
 * @see Ttmo
 */
class TtmoQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttmo[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttmo|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]       = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]    = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]   = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'MONo' ]         = $post[ 'MONo' ] ?? '--';
		$post[ 'MONoBaru' ]     = $post[ 'MONoBaru' ] ?? '--';
		$post[ 'MOTgl' ]        = date('Y-m-d', strtotime($post[ 'MOTgl' ] ?? date( 'Y-m-d' )));
		$post[ 'MOJam' ]        = date('Y-m-d H:i:s', strtotime($post[ 'MOJam' ] ?? date( 'Y-m-d H:i:s' )));
		$post[ 'LokasiAsal' ]   = $post[ 'LokasiAsal' ] ?? '--';
		$post[ 'LokasiTujuan' ] = $post[ 'LokasiTujuan' ] ?? '--';
		$post[ 'SKNo' ]         = $post[ 'SKNo' ] ?? '--';
		$post[ 'MotorAutoN' ]   = $post[ 'MotorAutoN' ] ?? '--';
		$post[ 'MotorNoMesin' ] = $post[ 'MotorNoMesin' ] ?? '--';
		try {
			General::cCmd( "SET @MONo = ?;" )->bindParam( 1, $post[ 'MONo' ] )->execute();
			General::cCmd( "CALL fmo(?,@Status,@Keterangan,?,?,?,@MONo,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'MONoBaru' ] )
			       ->bindParam( 6, $post[ 'MOTgl' ] )
			       ->bindParam( 7, $post[ 'MOJam' ] )
			       ->bindParam( 8, $post[ 'LokasiAsal' ] )
			       ->bindParam( 9, $post[ 'LokasiTujuan' ] )
			       ->bindParam( 10, $post[ 'SKNo' ] )
			       ->bindParam( 11, $post[ 'MotorAutoN' ] )
			       ->bindParam( 12, $post[ 'MotorNoMesin' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @MONo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'MONo'       => $post[ 'MONo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$MONo       = $exe[ '@MONo' ] ?? $post[ 'MONo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'MONo'       => $MONo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
}
