<?php
namespace aunit\models;
use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * TtssQuery represents the model behind the search form of `app\models\Ttss`.
 */
class TtssQuery extends \yii\db\ActiveQuery {
	/*public function active()
   {
	   return $this->andWhere('[[status]]=1');
   }*/
	/**
	 * {@inheritdoc}
	 * @return Ttss[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttss|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function dsTBeliFillByNo() {
		return $this->select( new Expression( "ttss.SSNo, ttss.SSTgl, ttss.FBNo, ttss.SupKode, ttss.LokasiKode, ttss.SSMemo, ttss.UserID, 
IFNULL(tdsupplier.SupNama, '--') AS SupNama,
IFNULL(tdlokasi.LokasiNama, '--') AS LokasiNama, IFNULL(ttfb.FBTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS FBTgl, IFNULL(tmotor.SSJum, 0) AS SSJum, 
ttss.SSJam, ttss.SSNoTerima, ttss.DONo, ttss.NoGL, ttss.ZnoShippingList, ttss.ZnoInvoice, ttss.ZtanggalTerima" ) )
		            ->join( "LEFT OUTER JOIN", "ttfb", "ttss.FBNo = ttfb.FBNo" )
		            ->join( "LEFT OUTER JOIN", "tdsupplier", "ttss.SupKode = tdsupplier.SupKode" )
		            ->join( "LEFT OUTER JOIN", "tdlokasi", "ttss.LokasiKode = tdlokasi.LokasiKode" )
		            ->join( "LEFT OUTER JOIN", [ 'tmotor' => new Expression( "( SELECT COUNT( MotorType ) AS SSJum, SSNo FROM tmotor tmotor_1 GROUP BY SSNo )" ) ], "ttss.SSNo = tmotor.SSNo" );
	}
	public function CekMesinNo( $MyMesinNo, $SSNo ) {
		$A              = 0;
		$MyMotorNoMesin = null;
		try {
			$A = \Yii::$app->db->createCommand( "Select IFNULL(COUNT(MotorNoMesin),0) FROM tmotor 
						WHERE  MotorNoMesin = :MotorNoMesin AND SDNo <> :SDNo AND SKNo = '--' AND PDNo <> '--' ", [
				':MotorNoMesin' => $MyMesinNo,
				':SDNo'         => $SSNo
			] )->queryScalar();
			if ( $A > 0 ) {
				$MyMotorNoMesin = \Yii::$app->db->createCommand( "SELECT MotorNoMesin FROM vmotorlastlokasi 
						WHERE Kondisi = 'OUT' AND MotorNoMesin = :MotorNoMesin", [
					':MotorNoMesin' => $MyMesinNo
				] )->queryScalar();
				if ( $MyMotorNoMesin == null ) {
					$A = 0;
				}
			}
			return $A;
		} catch ( Exception $e ) {
		}
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]       = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]    = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]   = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'SSNo' ]         = $post[ 'SSNo' ] ?? '--';
		$post[ 'SSNoBaru' ]     = $post[ 'SSNoView' ] ?? '--';
		$post[ 'SSTgl' ]        = date('Y-m-d', strtotime($post[ 'SSTgl' ] ?? date( 'Y-m-d' )));
		$post[ 'SSJam' ]        = date('Y-m-d H:i:s', strtotime($post[ 'SSJam' ] ?? date( 'Y-m-d H:i:s' )));
		$post[ 'SSNoTerima' ]   = $post[ 'SSNoTerima' ] ?? '--';
		$post[ 'SupKode' ]      = $post[ 'SupKode' ] ?? '--';
		$post[ 'FBNo' ]         = $post[ 'FBNo' ] ?? '--';
		$post[ 'FBTgl' ]        = date('Y-m-d', strtotime($post[ 'FBTgl' ] ?? date( 'Y-m-d' )));
		$post[ 'DONo' ]         = $post[ 'DONo' ] ?? '--';
		$post[ 'LokasiKodeSS' ] = $post[ 'LokasiKodeSS' ] ?? '--';
		$post[ 'SSMemo' ]       = $post[ 'SSMemo' ] ?? '--';
		$post[ 'NoGL' ]         = $post[ 'NoGL' ] ?? '--';
		$post[ 'Cetak' ]        = $post[ 'Cetak' ] ?? '--';
		$post[ 'ZnoShippingList' ]        = $post[ 'ZnoShippingList' ] ?? '--';
		$post[ 'ZnoInvoice' ]        = $post[ 'ZnoInvoice' ] ?? '--';
		$post[ 'ZtanggalTerima' ]        = $post[ 'ZtanggalTerima' ] ?? '--';
		try {
			General::cCmd( "SET @SSNo = ?;" )->bindParam( 1, $post[ 'SSNo' ] )->execute();
			General::cCmd( "CALL fss(?,@Status,@Keterangan,?,?,?,@SSNo,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'SSNoBaru' ] )
			       ->bindParam( 6, $post[ 'SSTgl' ] )
			       ->bindParam( 7, $post[ 'SSJam' ] )
			       ->bindParam( 8, $post[ 'SSNoTerima' ] )
			       ->bindParam( 9, $post[ 'SupKode' ] )
			       ->bindParam( 10, $post[ 'FBNo' ] )
			       ->bindParam( 11, $post[ 'FBTgl' ] )
			       ->bindParam( 12, $post[ 'DONo' ] )
			       ->bindParam( 13, $post[ 'LokasiKodeSS' ] )
			       ->bindParam( 14, $post[ 'SSMemo' ] )
			       ->bindParam( 15, $post[ 'NoGL' ] )
			       ->bindParam( 16, $post[ 'Cetak' ] )
			       ->bindParam( 17, $post[ 'ZnoShippingList' ] )
			       ->bindParam( 18, $post[ 'ZnoInvoice' ] )
			       ->bindParam( 19, $post[ 'ZtanggalTerima' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @SSNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'SSNo'       => $post[ 'SSNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$SSNo = $exe[ '@SSNo' ] ?? $post['SSNo'];
		$status = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : ['.$post['Action'].'] Keterangan NULL';
		return [
			'SSNo'       => $SSNo,
			'status'     => $status,
			'keterangan' => str_replace(" ","&nbsp;",$keterangan)
		];
	}
}
