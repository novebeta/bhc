<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "ttamhd".
 *
 * @property string $AMNo
 * @property string $AMTgl
 * @property string $AMJam
 * @property string $AMMemo
 * @property string $AMTotal
 * @property string $NoGL
 * @property string $UserID
 * @property string $MotorTahun
 * @property string $FBNo
 */
class Ttamhd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttamhd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AMNo'], 'required'],
            [['AMTgl', 'AMJam'], 'safe'],
            [['AMTotal', 'MotorTahun'], 'number'],
            [['AMNo', 'NoGL'], 'string', 'max' => 10],
            [['AMMemo'], 'string', 'max' => 300],
            [['UserID'], 'string', 'max' => 15],
            [['FBNo'], 'string', 'max' => 18],
            [['AMNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AMNo'       => 'Am No',
            'AMTgl'      => 'Am Tgl',
            'AMJam'      => 'Am Jam',
            'AMMemo'     => 'Am Memo',
            'AMTotal'    => 'Am Total',
            'NoGL'       => 'No Gl',
            'UserID'     => 'User ID',
            'MotorTahun' => 'Motor Tahun',
            'FBNo'       => 'Fb No',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function colGrid()
    {
        return [
            'AMNo' => [
                'label' => 'No AM',
                'width' => 100,
                'name' => 'AMNo'
            ],
            'AMTgl' => [
                'label' => 'Tgl AM',
                'width' => 100,
                'name' => 'AMTgl'
            ],
            'AMJam' => [
                'label' => 'Jam',
                'width' => 110,
                'name' => 'AMJam'
            ],
            'AMTotal' => [
                'label' => 'Total',
                'width' => 90,
                'name' => 'AMTotal'
            ],
            'MotorTahun' => [
                'label' => 'Tahun',
                'width' => 80,
                'name' => 'MotorTahun'
            ],
            'NoGL' => [
                'label' => 'No GL',
                'width' => 90,
                'name' => 'NoGL'
            ],
            'AMMemo' => [
                'label' => 'AM Memo',
                'width' => 360,
                'name' => 'AMMemo'
            ],
//	        'AMNo' => 'Am No',
//	        'AMTgl' => 'Am Tgl',
//	        'AMJam' => 'Am Jam',
//	        'AMMemo' => 'Am Memo',
//	        'AMTotal' => 'Am Total',
//	        'NoGL' => 'No Gl',
//	        'UserID' => 'User ID',
//	        'MotorTahun' => 'Motor Tahun',
//	        'FBNo' => 'Fb No',
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->UserID = \yii::$app->user->id;
            $this->AMTgl = date("Y-m-d");
            $this->AMJam = date("Y-m-d H:i:s");
        }
        return parent::beforeValidate();
    }

    /**
     * @param bool $temporary
     * @return mixed|string
     */
    public static function generateReference($temporary = false)
    {
        $kode = 'AM';
        $yNow = date("y");
        $max = (new \yii\db\Query())
            ->from(self::tableName())
            ->where("AMNo LIKE '$kode$yNow-%'")
            ->max('AMNo');

        if ($max && preg_match("/$kode\d{2}-(\d+)/", $max, $result)) {
            list($all, $counter) = $result;
            $digitCount = strlen($counter);
            $val = (int)$counter;
            $nextCounter = sprintf('%0' . $digitCount . 'd', ++$val);
        } else {
            $nextCounter = "00001";
        }

        $nextRef = "$kode$yNow-$nextCounter";

        return $temporary ? str_replace('-', '=', $nextRef) : $nextRef;
    }

    /**
     * {@inheritdoc}
     * @return TtamhdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtamhdQuery(get_called_class());
    }
}
