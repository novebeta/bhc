<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "vmotorlastjam".
 *
 * @property int $MotorAutoN
 * @property string $MotorNoMesin
 * @property string $Jam
 */
class Vmotorlastjam extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vmotorlastjam';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['MotorAutoN'], 'integer'],
            [['Jam'], 'safe'],
            [['MotorNoMesin'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'MotorAutoN' => 'Motor Auto N',
            'MotorNoMesin' => 'Motor No Mesin',
            'Jam' => 'Jam',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VmotorlastjamQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VmotorlastjamQuery(get_called_class());
    }
}
