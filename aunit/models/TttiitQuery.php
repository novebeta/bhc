<?php

namespace aunit\models;

use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Tttiit]].
 *
 * @see Tttiit
 */
class TttiitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tttiit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tttiit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	public function callSP( $post ) {
		$post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
		$post[ 'TINo' ]          = $post[ 'TINo' ] ?? '--';
		$post[ 'TIAuto' ]        = floatval( $post[ 'TIAuto' ] ?? 0 );
		$post[ 'BrgKode' ]       = $post[ 'BrgKode' ] ?? '--';
		$post[ 'TIQty' ]         = floatval( $post[ 'TIQty' ] ?? 0 );
		$post[ 'TIHrgBeli' ]     = floatval( $post[ 'TIHrgBeli' ] ?? 0 );
		$post[ 'LokasiAsal' ]    = $post[ 'LokasiAsal' ] ?? '--';
		$post[ 'LokasiTujuan' ]  = $post[ 'LokasiTujuan' ] ?? '--';
        $post[ 'TINoOLD' ]       = $post[ 'TINoOLD' ] ?? '--';
        $post[ 'TIAutoOLD' ]     = floatval( $post[ 'TIAutoOLD' ] ?? 0 );
        $post[ 'BrgKodeOLD' ]    = $post[ 'BrgKodeOLD' ] ?? '--';
        try {
            General::cCmd( "CALL ftiit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'TINo' ] )
                ->bindParam( 6, $post[ 'TIAuto' ] )
                ->bindParam( 7, $post[ 'BrgKode' ] )
                ->bindParam( 8, $post[ 'TIQty' ] )
                ->bindParam( 9, $post[ 'TIHrgBeli' ] )
                ->bindParam( 10, $post[ 'LokasiAsal' ] )
                ->bindParam( 11, $post[ 'LokasiTujuan' ] )
                ->bindParam( 12, $post[ 'TINoOLD' ] )
                ->bindParam( 13, $post[ 'TIAutoOLD' ] )
                ->bindParam( 14, $post[ 'BrgKodeOLD' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
	}
}
