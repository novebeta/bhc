<?php
namespace aunit\models;
/**
 * This is the model class for table "ttrk".
 *
 * @property string $RKNo
 * @property string $RKTgl
 * @property string $LokasiKode
 * @property string $RKMemo
 * @property string $UserID
 * @property string $RKJam
 * @property string $DKNo
 * @property string $SKNo
 * @property string $MotorNoMesin
 * @property string $NoGL
 */
class Ttrk extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttrk';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'RKNo' ], 'required' ],
			[ [ 'RKTgl', 'RKJam' ], 'safe' ],
			[ [ 'RKNo', 'DKNo', 'SKNo', 'NoGL' ], 'string', 'max' => 10 ],
			[ [ 'LokasiKode', 'UserID' ], 'string', 'max' => 15 ],
			[ [ 'RKMemo' ], 'string', 'max' => 100 ],
			[ [ 'MotorNoMesin' ], 'string', 'max' => 25 ],
			[ [ 'RKNo' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'RKNo'         => 'Rk No',
			'RKTgl'        => 'Rk Tgl',
			'LokasiKode'   => 'Lokasi Kode',
			'RKMemo'       => 'Rk Memo',
			'UserID'       => 'User ID',
			'RKJam'        => 'Rk Jam',
			'DKNo'         => 'Dk No',
			'SKNo'         => 'Sk No',
			'MotorNoMesin' => 'Motor No Mesin',
			'NoGL'         => 'No Gl',
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public static function colGrid() {
		return [
			'ttrk.RKNo'         => ['width' => 80,'label' => 'No RK','name' => 'RKNo'],
			'ttrk.RKTgl'        => ['width' => 70,'label' => 'Tgl RK','name' => 'RKTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y']],
			'ttrk.RKJam'        => ['width' => 50,'label' => 'Jam','name' => 'RKJam', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'H:i:s']],
			'ttrk.LokasiKode'   => ['width' => 90,'label' => 'Lokasi','name'  => 'LokasiKode'],
			'Tdcustomer.CusNama'=> ['width' => 125,'label' => 'Nama Konsumen','name'  => 'CusNama'],
			'tmotor.MotorType'   => ['width' => 175,'label' => 'Motor Type','name'  => 'MotorType'],
			'tmotor.MotorWarna'   => ['width' => 100,'label' => 'Warna','name'  => 'MotorWarna'],
			'tmotor.MotorNoMesin'   => ['width' => 100,'label' => 'No Mesin','name'  => 'MotorNoMesin'],
			'ttrk.SKNo'   => ['width' => 75,'label' => 'No SK','name'  => 'SKNo'],
			'ttrk.DKNo'   => ['width' => 75,'label' => 'No DK','name'  => 'DKNo'],
			'ttrk.RKMemo'   => ['width' => 200,'label' => 'Keterangan','name'  => 'RKMemo'],
			'ttrk.NoGL'         => ['width' => 70,'label' => 'No GL','name'  => 'NoGL'],
			'ttrk.UserID'       => ['width' => 70,'label' => 'UserID','name'  => 'UserID'],
			'tdmotortype.MotorNama'=> ['width' => 200,'label' => 'Nama Motor','name'  => 'MotorNama'],
	];
	}

	public function getMotorTypes()
    {
        return $this->hasOne(Tdmotortype::className(), ['MotorType' => 'MotorType'])
            ->via('motor');
    }
	
	public function getLokasi() {
		return $this->hasOne( Tdlokasi::className(), [ 'LokasiKode' => 'LokasiKode' ] );
	}
	public function getMotor() {
		return $this->hasOne( Tmotor::className(), [ 'RKNo' => 'RKNo' ] );
	}

	public function getDataKonsumen() {
		return $this->hasOne( Ttdk::className(), [ 'DKNo' => 'DKNo' ] );
	}
	public function getSuratJalanKonsumen() {
		return $this->hasOne( Ttsk::className(), [ 'SKNo' => 'SKNo' ] );
	}

	public function getKonsumen() {
		return $this->hasOne( Tdcustomer::className(), [ 'CusKode' => 'CusKode' ] )
		            ->via( 'dataKonsumen' );
	}

	//Nama Motor
	public static function find() {
		return new TtrkQuery( get_called_class() );
	}
}
