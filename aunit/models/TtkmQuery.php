<?php

namespace aunit\models;

use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;

/**
 * This is the ActiveQuery class for [[Ttkm]].
 *
 * @see Ttkm
 */
class TtkmQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttkm[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttkm|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function ttkmFillByNo()
    {
        return $this->select(new Expression("KMNo, KMTgl, KMMemo, KMNominal, KMJenis, KMPerson, KMLink, UserID, LokasiKode, KMJam, NoGL"));
    }

    public function ttkmGetJoinData() {
        $subqueryjoin = ( new \yii\db\Query() )
            ->select( new Expression(
                'vtkm.KMLink, vtkm.KMJenis, SUM(vtkm.KMNominal) AS Inden'
            ) )
            ->from( 'vtkm' )
            ->groupBy( 'vtkm.KMLink, vtkm.KMJenis' );
        return $this->select( new Expression( "ttin.INNo,
                        ttin.INTgl,
                        ttin.SalesKode,
                        ttin.CusKode,
                        ttin.LeaseKode,
                        ttin.INJenis,
                        ttin.DKNo,
                        ttin.INMemo,
                        ttin.INDP,
                        ttin.UserID,
                        IFNULL(tdcustomer.CusNama, '--') AS CusNama,
                        tdcustomer.CusAlamat,
                        tdcustomer.CusRT,
                        tdcustomer.CusRW,
                        tdcustomer.CusKelurahan,
                        tdcustomer.CusKecamatan,
                        IFNULL(
                            tdcustomer.CusKabupaten,
                            '--'
                        ) AS CusKabupaten,
                        tdcustomer.CusTelepon,
                        IFNULL(tdsales.SalesNama, '--') AS SalesNama,
                        IFNULL(tdleasing.LeaseNama, '--') AS LeaseNama,
                        ttin.INHarga,
                        ttin.MotorType,
                        ttin.MotorWarna,
                        IFNULL(Inden, 0) AS IndenSudah,
                        (ttin.INDP - IFNULL(Inden, 0)) AS IndenSisa"))
            ->join( "INNER JOIN", "tdleasing", "ttin.LeaseKode = tdleasing.LeaseKode")
            ->join( "INNER JOIN", "tdsales", "ttin.SalesKode = tdsales.SalesKode")
            ->join( "INNER JOIN", "tdcustomer", "ttin.CusKode = tdcustomer.CusKode")
            ->leftJoin(['ttkM' => $subqueryjoin], 'ttin.INNo = ttkM.KMLINK');
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'LokasiKode' ]    = Menu::getUserLokasi()[ 'LokasiKode' ];
        $post[ 'KMNo' ]          = $post[ 'KMNo' ] ?? '--';
        $post[ 'KMNoBaru' ]      = $post[ 'KMNoView' ] ?? '--';
        $post[ 'KMTgl' ]         = date('Y-m-d', strtotime($post[ 'KMTgl' ] ?? date( 'Y-m-d' )));
        $post[ 'KMMemo' ]        = $post[ 'KMMemo' ] ?? '--';
        $post[ 'KMNominal' ]     = floatval( $post[ 'KMNominal' ] ?? 0 );
        $post[ 'KMJenis' ]       = $post[ 'KMJenis' ] ?? '--';
        $post[ 'KMPerson' ]      = $post[ 'KMPerson' ] ?? '--';
        $post[ 'KMLink' ]        = $post[ 'KMLink' ] ?? '--';
        $post[ 'KMJam' ]         = date('Y-m-d H:i:s', strtotime($post[ 'KMJam' ] ?? date( 'Y-m-d H:i:s' )));
        $post[ 'NoGL' ]          = $post[ 'NoGL' ] ?? '--';
        try {
        General::cCmd( "SET @KMNo = ?;" )->bindParam( 1, $post[ 'KMNo' ] )->execute();
        General::cCmd( "CALL fkm(?,@Status,@Keterangan,?,?,?,@KMNo,?,?,?,?,?,?,?,?,?);" )
            ->bindParam( 1, $post[ 'Action' ] )
            ->bindParam( 2, $post[ 'UserID' ] )
            ->bindParam( 3, $post[ 'KodeAkses' ] )
            ->bindParam( 4, $post[ 'LokasiKode' ] )
            ->bindParam( 5, $post[ 'KMNoBaru' ] )
            ->bindParam( 6, $post[ 'KMTgl' ] )
            ->bindParam( 7, $post[ 'KMMemo' ] )
            ->bindParam( 8, $post[ 'KMNominal' ] )
            ->bindParam( 9, $post[ 'KMJenis' ] )
            ->bindParam( 10, $post[ 'KMPerson' ] )
            ->bindParam( 11, $post[ 'KMLink' ] )
            ->bindParam( 12, $post[ 'KMJam' ] )
            ->bindParam( 13, $post[ 'NoGL' ] )
            ->execute();
        $exe = General::cCmd( "SELECT @KMNo,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'KMNo'       => $post[ 'KMNo' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $KMNo = $exe[ '@KMNo' ] ?? $post['KMNo'];
        $status = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : ['.$post['Action'].'] Keterangan NULL';
        return [
            'KMNo'       => $KMNo,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }

    public function GetJoinDataBengkel() {
        return $this->select( new Expression( "ttkm.KMNo, ttkm.KMTgl, ttkm.KMMemo, ttkm.KMNominal, ttkm.KMJenis, ttkm.KMPerson, 
                ttkm.KMLink, ttkm.UserID, ttkm.LokasiKode, ttkm.KMJam, ttkm.NoGL, ttkk.KKLink  "))
            ->join( "LEFT OUTER JOIN", "ttkk", "ttkk.KKLink = ttkm.KMNo ");
    }
}
