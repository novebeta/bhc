<?php

namespace aunit\models;

/**
 * This is the model class for table "ttsiit".
 *
 * @property string $SINo
 * @property int $SIAuto
 * @property string $BrgKode
 * @property string $SIQty
 * @property string $SIHrgBeli
 * @property string $SIHrgJual
 * @property string $SIDiscount
 * @property string $SIPajak
 * @property string $SONo
 * @property string $LokasiKode
 * @property string $SIPickingNo
 * @property string $SIPickingDate
 * @property string $Cetak
 */
class Ttsiit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttsiit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SINo', 'SIAuto'], 'required'],
            [['SIAuto'], 'integer'],
            [['SIQty', 'SIHrgBeli', 'SIHrgJual', 'SIDiscount', 'SIPajak'], 'number'],
            [['SIPickingDate'], 'safe'],
            [['SINo', 'SONo', 'SIPickingNo'], 'string', 'max' => 10],
            [['BrgKode'], 'string', 'max' => 20],
            [['LokasiKode'], 'string', 'max' => 15],
            [['Cetak'], 'string', 'max' => 5],
            [['SINo', 'SIAuto'], 'unique', 'targetAttribute' => ['SINo', 'SIAuto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SINo' => 'Si No',
            'SIAuto' => 'Si Auto',
            'BrgKode' => 'Brg Kode',
            'SIQty' => 'Si Qty',
            'SIHrgBeli' => 'Si Hrg Beli',
            'SIHrgJual' => 'Si Hrg Jual',
            'SIDiscount' => 'Si Discount',
            'SIPajak' => 'Si Pajak',
            'SONo' => 'So No',
            'LokasiKode' => 'Lokasi Kode',
            'SIPickingNo' => 'Si Picking No',
            'SIPickingDate' => 'Si Picking Date',
            'Cetak' => 'Cetak',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtsiitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtsiitQuery(get_called_class());
    }
}
