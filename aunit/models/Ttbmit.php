<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "ttbmit".
 *
 * @property string $BMNo
 * @property string $DKNo
 * @property string $BMBayar
 */
class Ttbmit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttbmit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BMNo', 'DKNo'], 'required'],
            [['BMBayar'], 'number'],
            [['BMNo', 'DKNo'], 'string', 'max' => 12],
            [['BMNo', 'DKNo'], 'unique', 'targetAttribute' => ['BMNo', 'DKNo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'BMNo' => 'Bm No',
            'DKNo' => 'Dk No',
            'BMBayar' => 'Bm Bayar',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtbmitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtbmitQuery(get_called_class());
    }
}
