<?php
namespace aunit\models;
use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttsd]].
 *
 * @see Ttsd
 */
class TtsdQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttsd[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttsd|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function dsTMutasiReturBeli() {
		return $this->select( new Expression( "ttsd.SDNo,ttsd.NoGL, ttsd.SDTgl, ttsd.LokasiKode, ttsd.DealerKode, ttsd.SDMemo, ttsd.SDTotal,
						ttsd.UserID, tdlokasi.LokasiNama, tdSupplier.SupNama As DealerNama, ttsd.SDJam " ) )
		            ->join( "LEFT JOIN", "tdlokasi", "ttsd.LokasiKode = tdlokasi.LokasiKode" )
		            ->join( "LEFT JOIN", "tdsupplier", "ttsd.DealerKode = tdsupplier.SupKode" );
	}
	public function dsTMutasiReturBeliTmotorFillBySD() {
		return $this->select( new Expression( "tmotor.BPKBNo, tmotor.BPKBTglAmbil, tmotor.BPKBTglJadi, tmotor.DKNo, tmotor.DealerKode, tmotor.FBHarga, tmotor.FBNo, tmotor.FakturAHMNo, 
                         tmotor.FakturAHMTgl, tmotor.FakturAHMTglAmbil, tmotor.MotorAutoN, tmotor.MotorMemo, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorTahun, 
                         tmotor.MotorType, tmotor.MotorWarna, tmotor.PDNo, tmotor.PlatNo, tmotor.PlatTgl, tmotor.PlatTglAmbil, tmotor.SDNo, tmotor.SKNo, tmotor.SSNo, tmotor.STNKAlamat, 
                         tmotor.STNKNama, tmotor.STNKNo, tmotor.STNKTglAmbil, tmotor.STNKTglBayar, tmotor.STNKTglJadi, tmotor.STNKTglMasuk, tdmotortype.MotorNama, 
                         tmotor.FakturAHMNilai, tmotor.RKNo, tmotor.RKHarga, tmotor.SKHarga, tmotor.SDHarga, tmotor.MMHarga, tmotor.SSHarga" ) )
		            ->from( "tmotor" )
		            ->join( "LEFT OUTER JOIN", "tdmotortype", "tmotor.MotorType = tdmotortype.MotorType" )
		            ->orderBy( "tmotor.MotorType, tmotor.MotorTahun, tmotor.MotorWarna" );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]       = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]    = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]   = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'SDNoBaru' ]     = $post[ 'SDNoView' ] ?? '--';
		$post[ 'SDTgl' ]        = date('Y-m-d', strtotime($post[ 'SDTgl' ] ?? date( 'Y-m-d' )));
		$post[ 'SDJam' ]        = date('Y-m-d H:i:s', strtotime($post[ 'SDJam' ] ?? date( 'Y-m-d H:i:s' )));
		$post[ 'SupKode' ]      = $post[ 'SupKode' ] ?? '--';
		$post[ 'LokasiKodeSD' ] = $post[ 'LokasiKodeSD' ] ?? '--';
		$post[ 'SDMemo' ]       = $post[ 'SDMemo' ] ?? '--';
		$post[ 'SDTotal' ]      = floatval( $post[ 'SDTotal' ] ?? 0 );
		$post[ 'NoGL' ]         = $post[ 'NoGL' ] ?? '--';
		$post[ 'Cetak' ]        = $post[ 'Cetak' ] ?? '--';
		$post[ 'KodeTrans' ]    = $post[ 'KodeTrans' ] ?? '--';
		try {
			General::cCmd( "SET @SDNo = ?;" )->bindParam( 1, $post[ 'SDNo' ] )->execute();
			General::cCmd( "CALL fsd(?,@Status,@Keterangan,?,?,?,@SDNo,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'SDNoBaru' ] )
			       ->bindParam( 6, $post[ 'SDTgl' ] )
			       ->bindParam( 7, $post[ 'SDJam' ] )
			       ->bindParam( 8, $post[ 'SupKode' ] )
			       ->bindParam( 9, $post[ 'LokasiKodeSD' ] )
			       ->bindParam( 10, $post[ 'SDMemo' ] )
			       ->bindParam( 11, $post[ 'SDTotal' ] )
			       ->bindParam( 12, $post[ 'NoGL' ] )
			       ->bindParam( 13, $post[ 'Cetak' ] )
			       ->bindParam( 14, $post[ 'KodeTrans' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @SDNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'SDNo'       => $post[ 'SDNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$SDNo       = $exe[ '@SDNo' ] ?? $post[ 'SDNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'SDNo'       => $SDNo,
			'status'     => $status,
			'keterangan' => str_replace(" ","&nbsp;",$keterangan)
		];
	}
	public function callSPJurnal( $post ) {
		$post[ 'UserID' ]       = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		try {
			General::cCmd( "SET @SDNo = ?;" )->bindParam( 1, $post[ 'SDNo' ] )->execute();
			General::cCmd( "CALL jsd(@SDNo,?);" )
			       ->bindParam( 1, $post[ 'UserID' ] )
			       ->execute();
		} catch ( Exception $e ) {
			return [
				'SDNo'       => $post[ 'SDNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$SDNo       = $post[ 'SDNo' ];
		$status     = 0;
		$keterangan = '';
		return [
			'SDNo'       => $SDNo,
			'status'     => $status,
			'keterangan' => str_replace(" ","&nbsp;",$keterangan)
		];
	}
}
