<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "ttmsalestarget".
 *
 * @property string $SalesTgl
 * @property string $SalesKode
 * @property string $TeamKode
 * @property string $SalesQty
 */
class Ttmsalestarget extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttmsalestarget';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SalesTgl', 'SalesKode', 'TeamKode'], 'required'],
            [['SalesTgl'], 'safe'],
            [['SalesQty'], 'number'],
            [['SalesKode'], 'string', 'max' => 10],
            [['TeamKode'], 'string', 'max' => 15],
            [['SalesTgl', 'SalesKode', 'TeamKode'], 'unique', 'targetAttribute' => ['SalesTgl', 'SalesKode', 'TeamKode']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SalesTgl' => 'Sales Tgl',
            'SalesKode' => 'Sales Kode',
            'TeamKode' => 'Team Kode',
            'SalesQty' => 'Sales Qty',
        ];
    }
	/**
	 * {@inheritdoc}
	 * SELECT        ttmsalestarget.SalesTgl, ttmsalestarget.SalesKode, ttmsalestarget.TeamKode, ttmsalestarget.SalesQty, tdsales.SalesNama
	FROM            ttmsalestarget INNER JOIN
	tdsales ON ttmsalestarget.SalesKode = tdsales.SalesKode
	ORDER BY ttmsalestarget.TeamKode, tdsales.SalesNama
	 */
	public static function colGrid() {
		return [
			'ttmsalestarget.SalesTgl'         => ['width' => 80,'label' => 'Tanggal','name' => 'SalesTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y'],
			'ttmsalestarget.TeamKode'        => ['width' => 70,'label' => 'Team','name' => 'TeamKode']],
			'ttmsalestarget.SalesKode'        => ['width' => 50,'label' => 'Kode','name' => 'SalesKode'],
			'tdsales.SalesNama'   => ['width' => 90,'label' => 'Nama Sales','name'  => 'SalesNama'],
			'ttmsalestarget.SalesQty'=> ['width' => 125,'label' => 'Qty','name'  => 'SalesQty'],
		];
	}

	public function getSales()
	{
		return $this->hasOne(Tdsales::className(), ['SalesKode' => 'SalesKode']);
	}
    /**
     * {@inheritdoc}
     * @return TtmsalestargetQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtmsalestargetQuery(get_called_class());
    }
}
