<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Vdknoalluangnogl]].
 *
 * @see Vdknoalluangnogl
 */
class VdknoalluangnoglQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Vdknoalluangnogl[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Vdknoalluangnogl|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
