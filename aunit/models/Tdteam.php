<?php
namespace aunit\models;
/**
 * This is the model class for table "tdteam".
 *
 * @property string $TeamNo
 * @property string $TeamKode
 * @property string $TeamNama
 * @property string $TeamAlamat
 * @property string $TeamTelepon
 * @property string $TeamKeterangan
 * @property string $TeamStatus
 * @property string $TeamPass
 * @property string $TeamIndividu
 * @property string $TeamKolektif
 */
class Tdteam extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'tdteam';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'TeamNo', 'TeamIndividu', 'TeamKolektif' ], 'number' ],
			[ [ 'TeamKode' ], 'required' ],
			[ [ 'TeamKode' ], 'string', 'max' => 15 ],
			[ [ 'TeamNama' ], 'string', 'max' => 50 ],
			[ [ 'TeamAlamat', 'TeamTelepon' ], 'string', 'max' => 75 ],
			[ [ 'TeamKeterangan' ], 'string', 'max' => 100 ],
			[ [ 'TeamStatus', 'TeamPass' ], 'string', 'max' => 1 ],
			[ [ 'TeamKode' ], 'unique' ],
		];
	}
	public static function colGrid() {
		return [
            'TeamKode'    => ['label' => 'Kode','width' => 140,'name' => 'TeamKode'],
            'TeamNama'    => ['label' => 'Nama','width' => 265,'name' => 'TeamNama'],
            'TeamAlamat'  => ['label' => 'Alamat','width' => 340,'name' => 'TeamAlamat'],
            'TeamTelepon' => ['label' => 'Telepon','width' => 125,'name' => 'TeamTelepon'],
            'TeamStatus'  => ['label' => 'Status','width' => 60,'name' => 'TeamStatus'],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
            'TeamNo'         => 'No Urut',
            'TeamKode'       => 'Kode',
            'TeamNama'       => 'Nama',
            'TeamAlamat'     => 'Alamat',
            'TeamTelepon'    => 'Telepon',
            'TeamKeterangan' => 'Keterangan',
            'TeamStatus'     => 'Status',
            'TeamPass'       => 'Team Pass',
            'TeamIndividu'   => 'Team Individu',
            'TeamKolektif'   => 'Team Kolektif',
		];
	}
	/**
	 * {@inheritdoc}
	 * @return TdteamQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TdteamQuery( get_called_class() );
	}
}
