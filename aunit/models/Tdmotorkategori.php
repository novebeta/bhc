<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "tdmotorkategori".
 *
 * @property string $MotorNo
 * @property string $MotorKategori
 * @property string $MotorGroup
 */
class Tdmotorkategori extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tdmotorkategori';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['MotorNo'], 'string', 'max' => 2],
            [['MotorKategori', 'MotorGroup'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'MotorNo' => 'Motor No',
            'MotorKategori' => 'Motor Kategori',
            'MotorGroup' => 'Motor Group',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TdmotorkategoriQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TdmotorkategoriQuery(get_called_class());
    }
}
