<?php

namespace aunit\models;

use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Tttihd]].
 *
 * @see Tttihd
 */
class TttihdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tttihd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tttihd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function tttihdFillByNo() {
        return $this->select( new Expression( "Cetak, KodeTrans, LokasiAsal, LokasiTujuan, NoGL, PSNo, 
                            PosKode, TIKeterangan, TINo, TITgl, TITotal, UserID "));
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
//        $post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'TINo' ]          = $post[ 'TINo' ] ?? '--';
        $post[ 'TINoBaru' ]      = $post[ 'TINoView' ] ?? '--';
        $post[ 'TITgl' ]         = date('Y-m-d H:i:s', strtotime($post[ 'TITgl' ] ?? date( 'Y-m-d H:i:s' )));
        $post[ 'KodeTrans' ]     = $post[ 'KodeTrans' ] ?? '--';
        $post[ 'LokasiKode' ]    = $post[ 'LokasiKode' ] ?? '--';
        $post[ 'LokasiAsal' ]    = $post[ 'LokasiAsal' ] ?? '--';
        $post[ 'LokasiTujuan' ]  = $post[ 'LokasiTujuan' ] ?? '--';
        $post[ 'PSNo' ]          = $post[ 'PSNo' ] ?? '--';
        $post[ 'TITotal' ]       = floatval( $post[ 'TITotal' ] ?? 0 );
        $post[ 'TIKeterangan' ]  = $post[ 'TIKeterangan' ] ?? '--';
        $post[ 'Cetak' ]         = $post[ 'Cetak' ] ?? 'Belum';
        $post[ 'NoGL' ]          = $post[ 'NoGL' ] ?? '--';
        try {
            General::cCmd( "SET @TINo = ?;" )->bindParam( 1, $post[ 'TINo' ] )->execute();
            General::cCmd( "CALL ftihd(?,@Status,@Keterangan,?,?,?,@TINo,?,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'TINoBaru' ] )
                ->bindParam( 6, $post[ 'TITgl' ] )
                ->bindParam( 7, $post[ 'KodeTrans' ] )
                ->bindParam( 8, $post[ 'LokasiKode' ] )
                ->bindParam( 9, $post[ 'LokasiAsal' ] )
                ->bindParam( 10, $post[ 'LokasiTujuan' ] )
                ->bindParam( 11, $post[ 'PSNo' ] )
                ->bindParam( 12, $post[ 'TITotal' ] )
                ->bindParam( 13, $post[ 'TIKeterangan' ] )
                ->bindParam( 14, $post[ 'Cetak' ] )
                ->bindParam( 15, $post[ 'NoGL' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @TINo,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'TINo'       => $post[ 'TINo' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $TINo       = $exe[ '@TINo' ] ?? $post[ 'TINo' ];
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'TINo'       => $TINo,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
