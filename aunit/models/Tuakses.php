<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "tuakses".
 *
 * @property string $KodeAkses
 * @property string $MenuInduk
 * @property string $MenuText
 * @property int $MenuNo
 * @property bool $MenuStatus
 * @property bool $MenuTambah
 * @property bool $MenuEdit
 * @property bool $MenuHapus
 * @property bool $MenuCetak
 * @property bool $MenuView
 */
class Tuakses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tuakses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KodeAkses', 'MenuInduk', 'MenuText', 'MenuNo'], 'required'],
            [['MenuNo'], 'integer'],
            [['MenuStatus', 'MenuTambah', 'MenuEdit', 'MenuHapus', 'MenuCetak', 'MenuView'], 'boolean'],
            [['KodeAkses'], 'string', 'max' => 15],
            [['MenuInduk', 'MenuText'], 'string', 'max' => 50],
            [['KodeAkses', 'MenuInduk', 'MenuText', 'MenuNo'], 'unique', 'targetAttribute' => ['KodeAkses', 'MenuInduk', 'MenuText', 'MenuNo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'KodeAkses' => 'Kode Akses',
            'MenuInduk' => 'Menu Induk',
            'MenuText' => 'Menu Text',
            'MenuNo' => 'Menu No',
            'MenuStatus' => 'Menu Status',
            'MenuTambah' => 'Menu Tambah',
            'MenuEdit' => 'Menu Edit',
            'MenuHapus' => 'Menu Hapus',
            'MenuCetak' => 'Menu Cetak',
            'MenuView' => 'Menu View',
        ];
    }


	public static function colGrid()
	{
		return [
			'KodeAkses' => 'Kode Akses',
		];
	}
    /**
     * {@inheritdoc}
     * @return TuaksesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TuaksesQuery(get_called_class());
    }
}
