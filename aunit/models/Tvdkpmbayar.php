<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "tvdkpmbayar".
 *
 * @property string $DKNo
 * @property string $Jenis
 * @property string $Paid
 */
class Tvdkpmbayar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tvdkpmbayar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DKNo', 'Jenis', 'Paid'], 'required'],
            [['Paid'], 'number'],
            [['DKNo'], 'string', 'max' => 18],
            [['Jenis'], 'string', 'max' => 20],
            [['DKNo', 'Jenis', 'Paid'], 'unique', 'targetAttribute' => ['DKNo', 'Jenis', 'Paid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'DKNo' => 'Dk No',
            'Jenis' => 'Jenis',
            'Paid' => 'Paid',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TvdkpmbayarQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TvdkpmbayarQuery(get_called_class());
    }
}
