<?php

namespace aunit\models;

use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Tttait]].
 *
 * @see Tttait
 */
class TttaitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tttait[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tttait|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'PosKode' ]       = Menu::getUserLokasi()[ 'PosKode' ];
        $post[ 'TANo' ]          = $post[ 'TANo' ] ?? '--';
        $post[ 'TAAuto' ]        = floatval( $post[ 'TAAuto' ] ?? 0 );
        $post[ 'BrgKode' ]       = $post[ 'BrgKode' ] ?? '--';
        $post[ 'TAQty' ]         = floatval( $post[ 'TAQty' ] ?? 0 );
        $post[ 'TAHrgBeli' ]     = floatval( $post[ 'TAHrgBeli' ] ?? 0 );
        $post[ 'TANoOLD' ]       = $post[ 'TANoOLD' ] ?? '--';
        $post[ 'TAAutoOLD' ]     = floatval( $post[ 'TAAutoOLD' ] ?? 0 );
        $post[ 'BrgKodeOLD' ]    = $post[ 'BrgKodeOLD' ] ?? '--';
        $post[ 'LokasiKode' ]    = $post[ 'LokasiKode' ] ?? '--';
        try {
            General::cCmd( "CALL ftait(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'PosKode' ] )
                ->bindParam( 5, $post[ 'TANo' ] )
                ->bindParam( 6, $post[ 'TAAuto' ] )
                ->bindParam( 7, $post[ 'BrgKode' ] )
                ->bindParam( 8, $post[ 'TAQty' ] )
                ->bindParam( 9, $post[ 'TAHrgBeli' ] )
                ->bindParam( 10, $post[ 'TANoOLD' ] )
                ->bindParam( 11, $post[ 'TAAutoOLD' ] )
                ->bindParam( 12, $post[ 'BrgKodeOLD' ] )
                ->bindParam( 13, $post[ 'LokasiKode' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
