<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "vtkk".
 *
 * @property string $KKNo
 * @property string $KKLINK
 * @property string $KKJenis
 * @property string $Via
 * @property string $KKNominal
 * @property string $KKTgl
 * @property string $KKjam
 * @property string $NoGL
 */
class Vtkk extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vtkk';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KKNominal'], 'number'],
            [['KKTgl', 'KKjam'], 'safe'],
            [['KKNo'], 'string', 'max' => 12],
            [['KKLINK'], 'string', 'max' => 18],
            [['KKJenis'], 'string', 'max' => 20],
            [['Via'], 'string', 'max' => 2],
            [['NoGL'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'KKNo' => 'Kk No',
            'KKLINK' => 'Kklink',
            'KKJenis' => 'Kk Jenis',
            'Via' => 'Via',
            'KKNominal' => 'Kk Nominal',
            'KKTgl' => 'Kk Tgl',
            'KKjam' => 'K Kjam',
            'NoGL' => 'No Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VtkkQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VtkkQuery(get_called_class());
    }
}
