<?php
namespace aunit\models;
use phpDocumentor\Reflection\Types\Self_;
use phpDocumentor\Reflection\Types\This;
use yii\helpers\ArrayHelper;
/**
 * This is the ActiveQuery class for [[Tdmotortype]].
 *
 * @see Tdmotortype
 */
class TdmotortypeQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Tdmotortype[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Tdmotortype|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function combo() {
		return $this->select( [ "CONCAT(MotorType,' - ',MotorNama) as label", "MotorType as value" ] )
		            ->orderBy( 'TypeStatus,MotorType' )
		            ->asArray()
		            ->all();
	}
	public function select2( $value, $label = [ 'MotorType' ], $orderBy = ['TypeStatus'], $where = [ 'condition' => null, 'params' => [] ] , $allOption = false ) {
		$option =
			$this->select( [ "CONCAT(" . implode( ",'||',", $label ) . ") as label", "$value as value" ] )
			     ->orderBy( $orderBy )
			     ->where( $where[ 'condition' ], $where[ 'params' ] )
			     ->asArray()
			     ->all();
		if ( $allOption ) {
			return array_merge( [ [ 'value' => '%', 'label' => 'Semua' ] ], $option );
		} else {
			return $option;
		}
	}
    public function comboSelect2( $namaAccount = false ) {
        return $this->select( [ 'MotorType AS id', 'MotorType AS text', 'tdmotortype.*' ] )
            ->orderBy( 'TypeStatus,MotorType' )
            ->asArray()
            ->all();
    }
    public function motortype() {
        return ArrayHelper::map( $this->select( [ "CONCAT(MotorType) as label", "MotorType as value" ] )
            ->orderBy( 'TypeStatus,MotorType' )
            ->asArray()
            ->all(), 'value', 'label' );
    }
    public function comboselect() {
        return ArrayHelper::map( $this->select( [ "CONCAT(MotorNama) as label", "MotorType as value" ] )
            ->orderBy( 'TypeStatus,MotorType' )
            ->asArray()
            ->all(), 'value', 'label' );
    }
}
