<?php

namespace aunit\models;

use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;

/**
 * This is the ActiveQuery class for [[Ttpbhd]].
 *
 * @see Ttpbhd
 */
class TtpbhdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttpbhd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttpbhd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function ttpbhdFillByNo() {
        return $this->select( new Expression( "ttpbhd.LokasiAsal, ttpbhd.LokasiTujuan, ttpbhd.PBMemo, ttpbhd.PBNo, ttpbhd.PBTgl, ttpbhd.PBTotal, ttpbhd.SMNo, ttpbhd.UserID, IFNULL(tdlokasi.LokasiNama, '--') 
                         AS LokasiNamaAsal, IFNULL(tdlokasi_1.LokasiNama, '--') AS LokasiNamaTujuan, IFNULL(ttsmhd.SMTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SMTgl, 
                         ttpbhd.PBJam"))
            ->join( "LEFT OUTER JOIN", "ttsmhd", "ttpbhd.SMNo = ttsmhd.SMNo")
            ->join( "LEFT OUTER JOIN", "tdlokasi", "ttpbhd.LokasiAsal = tdlokasi.LokasiKode")
            ->join( "LEFT OUTER JOIN", "tdlokasi tdlokasi_1", "ttpbhd.LokasiTujuan = tdlokasi_1.LokasiKode");
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]       = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]    = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'LokasiKode' ]   = Menu::getUserLokasi()[ 'LokasiKode' ];
        $post[ 'PBNo' ]         = $post[ 'PBNo' ] ?? '--';
        $post[ 'PBNoBaru' ]     = $post[ 'PBNoView' ] ?? '--';
        $post[ 'PBTgl' ]        = date('Y-m-d', strtotime($post[ 'PBTgl' ] ?? date( 'Y-m-d' )));
        $post[ 'SMNo' ]         = $post[ 'SMNo' ] ?? '--';
        $post[ 'LokasiAsal' ]   = $post[ 'LokasiAsal' ] ?? '--';
        $post[ 'LokasiTujuan' ] = $post[ 'LokasiTujuan' ] ?? '--';
        $post[ 'PBMemo' ]       = $post[ 'PBMemo' ] ?? '--';
        $post[ 'PBTotal' ]      = floatval( $post[ 'PBTotal' ] ?? 0 );
        $post[ 'PBJam' ]        = date('Y-m-d H:i:s', strtotime($post[ 'PBJam' ] ?? date( 'Y-m-d H:i:s' )));
        try {
        General::cCmd( "SET @PBNo = ?;" )->bindParam( 1, $post[ 'PBNo'] )->execute();
        General::cCmd( "CALL fpbhd(?,@Status,@Keterangan,?,?,?,@PBNo,?,?,?,?,?,?,?,?);" )
            ->bindParam( 1, $post[ 'Action' ] )
            ->bindParam( 2, $post[ 'UserID' ] )
            ->bindParam( 3, $post[ 'KodeAkses' ] )
            ->bindParam( 4, $post[ 'LokasiKode' ] )
            ->bindParam( 5, $post[ 'PBNoBaru' ] )
            ->bindParam( 6, $post[ 'PBTgl' ] )
            ->bindParam( 7, $post[ 'SMNo' ] )
            ->bindParam( 8, $post[ 'LokasiAsal' ] )
            ->bindParam( 9, $post[ 'LokasiTujuan' ] )
            ->bindParam( 10, $post[ 'PBMemo' ] )
            ->bindParam( 11, $post[ 'PBTotal' ] )
            ->bindParam( 12, $post[ 'PBJam' ] )
            ->execute();
        $exe = General::cCmd( "SELECT @PBNo,@Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'PBNo'       => $post[ 'PBNo' ],
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $PBNo = $exe[ '@PBNo' ] ?? $post['PBNo'];
        $status = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : ['.$post['Action'].'] Keterangan NULL';
        return [
            'PBNo'       => $PBNo,
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
