<?php
namespace aunit\models;
use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttbmhd]].
 *
 * @see Ttbmhd
 */
class TtbmhdQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttbmhd[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttbmhd|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ] = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'BMNo' ]       = $post[ 'BMNo' ] ?? '--';
		$post[ 'BMNoBaru' ]   = $post[ 'BMNoView' ] ?? '--';
		$post[ 'BMTgl' ]      = date('Y-m-d', strtotime($post[ 'BMTgl' ] ?? date( 'Y-m-d' )));
		$post[ 'BMKMemo' ]    = $post[ 'BMMemo' ] ?? '--';
		$post[ 'BMNominal' ]  = floatval( $post[ 'BMNominal' ] ?? 0 );
		$post[ 'BMJenis' ]    = $post[ 'BMJenis' ] ?? '--';
		$post[ 'LeaseKode' ]  = $post[ 'LeaseKode' ] ?? '--';
		$post[ 'BMCekNo' ]    = $post[ 'BMCekNo' ] ?? '--';
		$post[ 'BMCekTempo' ] = date('Y-m-d', strtotime($post[ 'BMCekTempo' ] ?? date( 'Y-m-d' )));
		$post[ 'BMPerson' ]   = $post[ 'BMPerson' ] ?? '--';
		$post[ 'BMAC' ]       = $post[ 'BMAC' ] ?? '--';
		$post[ 'NoAccount' ]  = $post[ 'NoAccount' ] ?? '--';
		$post[ 'BMJam' ]      = date('Y-m-d H:i:s', strtotime($post[ 'BMJam' ] ?? date( 'Y-m-d H:i:s' )));
		$post[ 'NoGL' ]       = $post[ 'NoGL' ] ?? '--';
		try {
			General::cCmd( "SET @BMNo = ?;" )->bindParam( 1, $post[ 'BMNo' ] )->execute();
			General::cCmd( "CALL fbmhd(?,@Status,@Keterangan,?,?,?,@BMNo,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'BMNoBaru' ] )
			       ->bindParam( 6, $post[ 'BMTgl' ] )
			       ->bindParam( 7, $post[ 'BMMemo' ] )
			       ->bindParam( 8, $post[ 'BMNominal' ] )
			       ->bindParam( 9, $post[ 'BMJenis' ] )
			       ->bindParam( 10, $post[ 'LeaseKode' ] )
			       ->bindParam( 11, $post[ 'BMCekNo' ] )
			       ->bindParam( 12, $post[ 'BMCekTempo' ] )
			       ->bindParam( 13, $post[ 'BMPerson' ] )
			       ->bindParam( 14, $post[ 'BMAC' ] )
			       ->bindParam( 15, $post[ 'NoAccount' ] )
			       ->bindParam( 16, $post[ 'BMJam' ] )
			       ->bindParam( 17, $post[ 'NoGL' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @BMNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'BMNo'       => $post[ 'BMNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$BMNo       = $exe[ '@BMNo' ] ?? $post[ 'BMNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'BMNo'       => $BMNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
	public function ttbmhdFillGetData() {
		return $this->select( new Expression( "ttbmhd.BMAC, ttbmhd.BMCekNo, ttbmhd.BMCekTempo, 
            ttbmhd.BMJenis, ttbmhd.BMMemo, ttbmhd.BMNo, ttbmhd.BMNominal, ttbmhd.BMPerson, ttbmhd.BMTgl, ttbmhd.LeaseKode, 
            ttbmhd.NoAccount, ttbmhd.UserID, IFNULL(tdleasing.LeaseNama, '--') AS LeaseNama, 
            IFNULL(traccount.NamaAccount, '--') AS NamaAccount, ttbmhd.BMJam, ttbmhd.NoGL " ) )
		            ->join( "LEFT OUTER JOIN", "tdleasing", "ttbmhd.LeaseKode = tdleasing.LeaseKode" )
		            ->join( "LEFT OUTER JOIN", "traccount", "ttbmhd.NoAccount = traccount.NoAccount" );
	}
	public function GetJoinData() {
		return $this->select( new Expression( "BMNo, BMTgl, BMMemo, BMNominal, BMJenis, ttbmhd.LeaseKode, 
                    BMCekNo, BMCekTempo, BMPerson, BMAC, ttbmhd.NoAccount, UserID, tdleasing.LeaseNama, traccount.NamaAccount " ) )
		            ->join( "INNER JOIN", "tdleasing", "ttbmhd.LeaseKode = tdleasing.LeaseKode" )
		            ->join( "INNER JOIN", "traccount", "ttbmhd.NoAccount = traccount.NoAccount" );
	}
}
