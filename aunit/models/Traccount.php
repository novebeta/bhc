<?php
namespace aunit\models;
/**
 * This is the model class for table "traccount".
 *
 * @property string $NoAccount
 * @property string $NamaAccount
 * @property string $OpeningBalance
 * @property string $JenisAccount
 * @property string $StatusAccount
 * @property string $NoParent
 * @property string $LevelAccount
 */
class Traccount extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'traccount';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'NoAccount' ], 'required' ],
			[ [ 'OpeningBalance', 'LevelAccount' ], 'number' ],
			[ [ 'NoAccount', 'NoParent' ], 'string', 'max' => 10 ],
			[ [ 'NamaAccount' ], 'string', 'max' => 150 ],
			[ [ 'JenisAccount' ], 'string', 'max' => 6 ],
			[ [ 'StatusAccount' ], 'string', 'max' => 1 ],
			[ [ 'NoAccount' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'NoAccount'      => 'Nomor Perkiraan',
			'NamaAccount'    => 'Nama Perkiraan',
			'OpeningBalance' => 'Opening Balance',
			'JenisAccount'   => 'Jenis',
			'StatusAccount'  => 'Status',
			'NoParent'       => 'No Parent',
			'LevelAccount'   => 'Level Account',
		];
	}
	public static function colGrid() {
		return [
			'traccount.NoAccount'     => [
				'label' => 'Nomor Perkiraan',
				'name'  => 'NoAccount',
                'width' => 190,
			],
			'traccount.NamaAccount'   =>[
				'label' => 'Nama Perkiraan',
				'name'  => 'NamaAccount',
                'width' => 310,
			],
			'traccount.JenisAccount'  => [
				'label' => 'Jenis',
				'name'  => 'JenisAccount',
                'width' => 130,
			],
			'traccount.NoParent' =>[
				'label' => 'Induk',
				'name'  => 'NoParent',
                'width' => 160,
			],
			'traccount.StatusAccount' =>[
				'label' => 'Status',
				'name'  => 'StatusAccount',
                'width' => 130,
			],
		];
	}
	public function getParent() {
		return $this->hasOne( Traccount::className(), [ 'NoAccount' => 'NoParent' ] )
		            ->from( Traccount::tableName() . ' parent' );
	}
	/**
	 * {@inheritdoc}
	 * @return TraccountQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TraccountQuery( get_called_class() );
	}
}
