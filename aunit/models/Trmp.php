<?php

namespace aunit\models;

use Yii;
/**
 * This is the model class for table "trmp".
 *
 * @property string $MPNo
 * @property string $MPTgl
 * @property string $MPMemo
 * @property string $MPSaldoAwal
 * @property string $MPUmur
 * @property string $MPAwal
 * @property string $MPNext
 * @property string $MPCOADebet
 * @property string $MPCOAKredit
 * @property string $MPTglMulai
 * @property string $MPSaldoNow
 * @property string $MPStatus
 * @property string $UserID
 */
class Trmp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trmp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['MPNo'], 'required'],
            [['MPTgl'], 'safe'],
            [['MPSaldoAwal', 'MPUmur', 'MPAwal', 'MPNext', 'MPSaldoNow'], 'number'],
            [['MPNo'], 'string', 'max' => 12],
            [['MPMemo'], 'string', 'max' => 200],
            [['MPNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'MPNo'         => 'Mp No',
            'MPTgl'        => 'Mp Tgl',
            'MPMemo'       => 'Mp Memo',
            'MPSaldoAwal'  => 'Saldo Awal',
            'MPUmur'       => 'Umur',
            'MPAwal'       => 'MP Awal',
            'MPNext'       => 'MP Next',
            'MPCOADebet'   => 'MP COA Debet',
            'MPCOAKredit'  => 'MP COA Kredit',
            'MPTglMulai'   => 'MP Tgl Mulai',
            'MPSaldoNow'   => 'MP Saldo Now',
            'MPStatus'     => 'MP Status',
            'UserID'       => 'USer ID',
        ];
    }

	/**
	 * {@inheritdoc}
	 */
	public static function colGrid() {
		return [
			'MPNo'                    => ['width' => 75,'label' => 'No MP','name' => 'MPNo'],
			'MPTgl'                   => ['width' => 100,'label' => 'Tgl MP','name' => 'MPTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y']],
			'MPMemo'             => ['width' => 300,'label' => 'Keterangan','name' => 'MPMemo'],
			'MPSaldoAwal'       => ['width' => 120,'label' => 'Saldo Awal','name' => 'MPSaldoAwal'],
			'MPUmur'     => ['width' => 120,'label' => 'Umur','name' => 'MPUmur'],
			'MPAwal'           => ['width' => 120,'label' => 'Awal','name' => 'MPAwal'],
            'MPSaldoNow' => ['width' => 120,'label' => 'Saldo Now','name' => 'MPSaldoNow'],
            			'MPNext'           => ['width' => 120,'label' => 'Next','name' => 'MPNext'],
		];
	}
    /**
     * {@inheritdoc}
     * @return TrmpQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TrmpQuery(get_called_class());
    }
}
