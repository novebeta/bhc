<?php

namespace aunit\models;
use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
/**
 * This is the ActiveQuery class for [[Ttamit]].
 *
 * @see Ttamit
 */
class TtamitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttamit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttamit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	public function callSP( $post ) {
		$post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ] = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'AMNo' ]   = $post[ 'AMNo' ] ?? '--';
		$post[ 'MotorNoMesin' ]   = $post[ 'MotorNoMesin' ] ?? '--';
		$post[ 'MotorAutoN' ]   = $post[ 'MotorAutoN' ] ?? '--';
		$post[ 'MotorHarga' ]    = floatval( $post[ 'MMHarga' ] ?? 0 );
		$post[ 'AMNoOLD' ]   = $post[ 'AMNoOLD' ] ?? '--';
        $post[ 'MotorAutoNOLD' ]   = $post[ 'MotorAutoNOLD' ] ?? '--';
		$post[ 'MotorNoMesinOLD' ]   = $post[ 'MotorNoMesinOLD' ] ?? '--';
		try {
			General::cCmd( "SET @AMNo = ?;" )->bindParam( 1, $post[ 'AMNo' ] )->execute();
			General::cCmd( "CALL famit(?,@Status,@Keterangan,?,?,?,@AMNo,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'MotorNoMesin' ] )
			       ->bindParam( 6, $post[ 'MotorAutoN' ] )
			       ->bindParam( 7, $post[ 'MotorHarga' ] )
			       ->bindParam( 8, $post[ 'AMNoOLD' ] )
			       ->bindParam( 9, $post[ 'MotorAutoNOLD' ] )
			       ->bindParam( 10, $post[ 'MotorNoMesinOLD' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @AMNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'AMNo'       => $post[ 'AMNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$AMNo       = $exe[ '@AMNo' ] ?? $post[ 'AMNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'AMNo'       => $AMNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
}
