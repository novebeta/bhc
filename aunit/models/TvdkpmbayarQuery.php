<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Tvdkpmbayar]].
 *
 * @see Tvdkpmbayar
 */
class TvdkpmbayarQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tvdkpmbayar[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tvdkpmbayar|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
