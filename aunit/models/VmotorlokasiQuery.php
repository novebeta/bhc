<?php

namespace aunit\models;

use common\components\General;
use yii\base\Exception;

/**
 * This is the ActiveQuery class for [[Vmotorlokasi]].
 *
 * @see Vmotorlokasi
 */
class VmotorlokasiQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Vmotorlokasi[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Vmotorlokasi|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    
    public function callSP() {
		$status = 0;
        try {
		General::cCmd( "CALL ctvmotorLokasi('oo',0);" )
		       ->execute();
        } catch ( Exception $e ) {
            $status = 1;
//            $keterangan = $e->getMessage();
        }
        return [
            'status'     => $status,
//            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
	}
}
