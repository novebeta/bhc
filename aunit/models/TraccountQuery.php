<?php
namespace aunit\models;
use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
/**
 * This is the ActiveQuery class for [[Traccount]].
 *
 * @see Traccount
 */
class TraccountQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Traccount[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Traccount|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function combo() {
		return $this->select( [ "CONCAT(NoAccount,' - ',NamaAccount) as label", "NoAccount as value" ] )
		            ->orderBy( 'NoAccount' )
		            ->asArray()
		            ->all();
	}
	public function header() {
		return ArrayHelper::map( $this->select( [
			"CONCAT(NoAccount,' - ',NamaAccount) as label",
			"NoAccount as value"
		] )
		                              ->where( [ 'StatusAccount' => 'A', 'JenisAccount' => 'Header' ] )
		                              ->orderBy( 'NoAccount' )
		                              ->asArray()
		                              ->all(), 'value', 'label' );
	}
	public function comboSelect2( $namaAccount = false ) {
		return $this->select( [ 'NoAccount', 'NamaAccount' ] )
		            ->where( 'JenisAccount = "Detail"' )
		            ->orderBy( 'NoAccount' )
		            ->asArray()
		            ->all();
	}
	public function getChild( $NoParent ) {
		$hasil  = Traccount::find()
		                   ->select(
			                   [
				                   '*',
				                   'NoAccount AS id',
				                   'IF(JenisAccount = "Header", "img/folder.png", "img/card.png") as icon',
				                   'CONCAT(NoAccount," ",NamaAccount) AS label'
			                   ] )
		                   ->where( [ 'NoParent' => $NoParent ] )
		                   ->asArray()
		                   ->all();
		$hasil1 = [];
		foreach ( $hasil as $row ) {
			$row[ 'value' ] = json_encode( $row );
			$hasil1[]       = $row;
			$hasil1         = array_merge( $hasil1, self::getChild( $row[ 'NoAccount' ] ) );
		}
		return $hasil1;
	}
	public function getAccount( $NoAccount ) {
		$aktiva            = Traccount::find()
		                              ->select(
			                              [
				                              '*',
				                              'NoAccount AS id',
				                              'IF(JenisAccount = "Header", "img/folder.png", "img/card.png") as icon',
				                              'CONCAT(NoAccount," ",NamaAccount) AS label'
			                              ] )
		                              ->where( [ 'NoAccount' => $NoAccount ] )->asArray()->one();
		$aktiva[ 'value' ] = json_encode( $aktiva );
		$child_aktiva      = self::getChild( $NoAccount );
		$child_aktiva[]    = $aktiva;
		return $child_aktiva;
	}
	public function select2() {
		return ArrayHelper::map( $this->select( [
			"CONCAT(NamaAccount) as label",
			"NoAccount as value"
		] )
		                              ->where( [ 'StatusAccount' => 'A', 'JenisAccount' => 'Header' ] )
		                              ->orderBy( 'NoAccount' )
		                              ->asArray()
		                              ->all(), 'value', 'label' );
	}
	public function NoAccount( $value, $label = [ 'NamaAccount' ], $orderBy = [ 'NamaAccount' ], $where = [ 'condition' => null, 'params' => [] ], $allOption = false ) {
		$option =
			$this->select( [ "CONCAT(" . implode( ",'||',", $label ) . ") as label", "$value as value" ] )
			     ->orderBy( $orderBy )
			     ->where( $where[ 'condition' ], $where[ 'params' ] )
			     ->asArray()
			     ->all();
		if ( $allOption ) {
			return array_merge( [ [ 'value' => '%', 'label' => 'Semua' ] ], $option );
		} else {
			return $option;
		}
	}
	public function bank( $convert = true ) {
		$arr = General::cCmd( "SELECT NoAccount as id, NamaAccount as text FROM traccount 
		WHERE (NoParent = '11020100' OR NoParent = '11020200' OR  NoParent = '11030000' )  
		  AND StatusAccount = 'A' 
		UNION SELECT NoAccount as id, NamaAccount  as text FROM traccount 
		WHERE (NoParent = '11020300')  AND StatusAccount = 'A'" )->queryAll();
		return $convert ? ArrayHelper::map( $arr, 'id', 'text' ) : $arr;
	}
	public function kas( $convert = true ) {
		$arr = General::cCmd( "SELECT NoAccount as id, NamaAccount as text FROM traccount 
		WHERE LEFT(NoAccount,5) = '11010'  AND JenisAccount = 'Detail' " )->queryAll();
		return $convert ? ArrayHelper::map( $arr, 'id', 'text' ) : $arr;
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]       = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]    = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]   = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'TglGL' ]        = date('Y-m-d', strtotime($post[ 'TglGL' ] ?? date( 'Y-m-d' )));
		$post[ 'NoAccount' ]    = $post[ 'NoAccount' ] ?? '--';
		$post[ 'KeteranganGL' ] = $post[ 'KeteranganGL' ] ?? '--';
		$post[ 'DebetGL' ]      = floatval( $post[ 'DebetGL' ] ?? 0 );
		$post[ 'KreditGL' ]     = floatval( $post[ 'KreditGL' ] ?? 0 );
		try {
			General::cCmd( "CALL fglneracaawal(?,@Status,@Keterangan,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'TglGL' ] )
			       ->bindParam( 6, $post[ 'NoAccount' ] )
			       ->bindParam( 7, $post[ 'KeteranganGL' ] )
			       ->bindParam( 8, $post[ 'DebetGL' ] )
			       ->bindParam( 9, $post[ 'KreditGL' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
}
