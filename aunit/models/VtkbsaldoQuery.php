<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Vtkbsaldo]].
 *
 * @see Vtkbsaldo
 */
class VtkbsaldoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Vtkbsaldo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Vtkbsaldo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
