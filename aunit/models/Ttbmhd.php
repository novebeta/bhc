<?php
namespace aunit\models;
/**
 * This is the model class for table "ttbmhd".
 *
 * @property string $BMNo
 * @property string $BMTgl
 * @property string $BMMemo
 * @property string $BMNominal
 * @property string $BMJenis Leasing, Tunai, Umum
 * @property string $LeaseKode
 * @property string $BMCekNo
 * @property string $BMCekTempo
 * @property string $BMPerson
 * @property string $BMAC
 * @property string $NoAccount
 * @property string $UserID
 * @property string $BMJam
 * @property string $NoGL
 */
class Ttbmhd extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttbmhd';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'BMNo' ], 'required' ],
			[ [ 'BMTgl', 'BMCekTempo', 'BMJam' ], 'safe' ],
			[ [ 'BMNominal' ], 'number' ],
			[ [ 'BMNo' ], 'string', 'max' => 12 ],
			[ [ 'BMMemo' ], 'string', 'max' => 100 ],
			[ [ 'BMJenis' ], 'string', 'max' => 20 ],
			[ [ 'LeaseKode', 'NoAccount', 'NoGL' ], 'string', 'max' => 10 ],
			[ [ 'BMCekNo', 'BMAC' ], 'string', 'max' => 25 ],
			[ [ 'BMPerson' ], 'string', 'max' => 50 ],
			[ [ 'UserID' ], 'string', 'max' => 15 ],
			[ [ 'BMNo' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'BMNo'       => 'Bm No',
			'BMTgl'      => 'Bm Tgl',
			'BMMemo'     => 'Bm Memo',
			'BMNominal'  => 'Bm Nominal',
			'BMJenis'    => 'Leasing, Tunai, Umum',
			'LeaseKode'  => 'Lease Kode',
			'BMCekNo'    => 'Bm Cek No',
			'BMCekTempo' => 'Bm Cek Tempo',
			'BMPerson'   => 'Bm Person',
			'BMAC'       => 'Bmac',
			'NoAccount'  => 'No Account',
			'UserID'     => 'User ID',
			'BMJam'      => 'Bm Jam',
			'NoGL'       => 'No Gl',
		];
	}
	//BMMemo, BMNominal, BMJenis, LeaseKode, BMCekNo, BMCekTempo, BMPerson, BMAC, NoAccount, UserID,  NoGL
	public static function colGrid() {
		return [
			'BMNo'                  => [ 'width' => 85, 'label' => 'No BM', 'name' => 'BMNo' ],
			'BMTgl'                 => [ 'width' => 75, 'label' => 'Tgl BM', 'name' => 'BMTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'BMJam'                 => [ 'width' => 55, 'label' => 'Jam', 'name' => 'BMJam', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'H:i:s' ] ],
			'BMJenis'               => [ 'width' => 80, 'label' => 'Jenis', 'name' => 'BMJenis' ],
			'ttbmhd.NoAccount'      => [ 'width' => 70, 'label' => 'No Account', 'name' => 'NoAccount' ],
			'traccount.NamaAccount' => [ 'width' => 225, 'label' => 'Nama Bank', 'name' => 'NamaAccount' ],
			'ttbmhd.LeaseKode'      => [ 'width' => 75, 'label' => 'Kode Lease', 'name' => 'LeaseKode' ],
			'BMPerson'              => [ 'width' => 172, 'label' => 'Terima Dari', 'name' => 'BMPerson' ],
			'BMNominal'             => [ 'width' => 100, 'label' => 'Nominal', 'name' => 'BMNominal', 'formatter' => 'number', 'align' => 'right' ],
			'NoGL'                  => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
			'BMCekNo'               => [ 'width' => 70, 'label' => 'No Cek', 'name' => 'BMCekNo' ],
			'BMAC'                  => [ 'width' => 70, 'label' => 'No A/C', 'name' => 'BMAC' ],
			'BMCekTempo'            => [ 'width' => 70, 'label' => 'AC Tempo', 'name' => 'BMCekTempo', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'UserID'                => [ 'width' => 70, 'label' => 'User', 'name' => 'UserID' ],
			'BMMemo'                => [ 'width' => 500, 'label' => 'Keterangan', 'name' => 'BMMemo' ],
		];
	}
	public function getLeasing() {
		return $this->hasOne( Tdleasing::className(), [ 'LeaseKode' => 'LeaseKode' ] );
	}
	public function getAccount() {
		return $this->hasOne( Traccount::className(), [ 'NoAccount' => 'NoAccount' ] );
	}
	/**
	 * {@inheritdoc}
	 * @return TtbmhdQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TtbmhdQuery( get_called_class() );
	}
	public static function generateBMNo( $BankKode, $temporary = false ) {
		$sign    = $temporary ? '=' : '-';
		$kode    = 'BM';
		$kBank   = substr( $BankKode, - 2 );
		$yNow    = date( "y" );
		$maxNoGl = ( new \yii\db\Query() )
			->from( self::tableName() )
			->where( "BMNo LIKE '$kode$kBank$yNow$sign%'" )
			->max( 'BMNo' );
		if ( $maxNoGl && preg_match( "/$kode\d{4}$sign(\d+)/", $maxNoGl, $result ) ) {
			[ $all, $counter ] = $result;
			$digitCount  = strlen( $counter );
			$val         = (int) $counter;
			$nextCounter = sprintf( '%0' . $digitCount . 'd', ++ $val );
		} else {
			$nextCounter = "00001";
		}
		return "$kode$kBank$yNow$sign$nextCounter";
	}
	public static function getRoute( $BMJenis, $param = []) {
		$index  = [];
		$update = [];
		$create = [];
		switch ( $BMJenis ) {
			case 'Inden' :
				$index = [ 'ttbmhd/bank-masuk-inden' ];
				$update = [ 'ttbmhd/bank-masuk-inden-update' ];
				$create = [ 'ttbmhd/bank-masuk-inden-create' ];
				break;
			case 'Leasing' :
				$index = [ 'ttbmhd/bank-masuk-leasing' ];
				$update = [ 'ttbmhd/bank-masuk-leasing-update' ];
				$create = [ 'ttbmhd/bank-masuk-leasing-create' ];
				break;
			case 'Scheme' :
				$index = [ 'ttbmhd/bank-masuk-scheme' ];
				$update = [ 'ttbmhd/bank-masuk-scheme-update' ];
				$create = [ 'ttbmhd/bank-masuk-scheme-create' ];
				break;
			case 'Kredit' :
				$index = [ 'ttbmhd/bank-masuk-uang-muka-kredit' ];
				$update = [ 'ttbmhd/bank-masuk-uang-muka-kredit-update' ];
				$create = [ 'ttbmhd/bank-masuk-uang-muka-kredit-create' ];
				break;
			case 'Piutang UM' :
				$index = [ 'ttbmhd/bank-masuk-piutang-kons-um' ];
				$update = [ 'ttbmhd/bank-masuk-piutang-kons-um-update' ];
				$create = [ 'ttbmhd/bank-masuk-piutang-kons-um-create' ];
				break;
			case 'Tunai' :
				$index = [ 'ttbmhd/bank-masuk-uang-muka-tunai' ];
				$update = [ 'ttbmhd/bank-masuk-uang-muka-tunai-update' ];
				$create = [ 'ttbmhd/bank-masuk-uang-muka-tunai-create' ];
				break;
			case 'Piutang Sisa' :
				$index = [ 'ttbmhd/bank-masuk-sisa-piutang-kons' ];
				$update = [ 'ttbmhd/bank-masuk-sisa-piutang-kons-update' ];
				$create = [ 'ttbmhd/bank-masuk-sisa-piutang-kons-create' ];
				break;
			case 'BBN Plus' :
				$index = [ 'ttbmhd/bank-masuk-bbn-progresif' ];
				$update = [ 'ttbmhd/bank-masuk-bbn-progresif-update' ];
				$create = [ 'ttbmhd/bank-masuk-bbn-progresif-create' ];
				break;
			case 'BM Dari Kas' :
				$index = [ 'ttbmhd/bank-masuk-dari-kas' ];
				$update = [ 'ttbmhd/bank-masuk-dari-kas-update' ];
				$create = [ 'ttbmhd/bank-masuk-dari-kas-create' ];
				break;
			case 'Umum' :
				$index = [ 'ttbmhd/bank-masuk-umum' ];
				$update = [ 'ttbmhd/bank-masuk-umum-update' ];
				$create = [ 'ttbmhd/bank-masuk-umum-create' ];
				break;
			case 'Penjualan Multi' :
				$index = [ 'ttbmhd/bank-masuk-penjualan-multi' ];
				$update = [ 'ttbmhd/bank-masuk-penjualan-multi-update' ];
				$create = [ 'ttbmhd/bank-masuk-penjualan-multi-create' ];
				break;
		}
		return [
			'index'  => \yii\helpers\Url::toRoute( array_merge( $index, $param ) ),
			'update' => \yii\helpers\Url::toRoute( array_merge( $update, $param ) ),
			'create' => \yii\helpers\Url::toRoute( array_merge( $create, $param ) ),
		];
	}
}
