<?php

namespace aunit\models;
/**
 * This is the model class for table "ttss".
 *
 * @property string $SSNo
 * @property string $SSTgl
 * @property string $FBNo
 * @property string $SupKode
 * @property string $LokasiKode
 * @property string $SSMemo
 * @property string $UserID
 * @property string $SSJam
 * @property string $SSNoTerima
 * @property string $DONo
 * @property string $NoGL
 */
class Ttss extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttss';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SSNo'], 'required'],
            [['SSTgl', 'SSJam'], 'safe'],
            [['SSNo', 'FBNo', 'DONo'], 'string', 'max' => 18],
            [['SupKode', 'SSNoTerima', 'NoGL'], 'string', 'max' => 10],
            [['LokasiKode', 'UserID'], 'string', 'max' => 15],
            [['SSMemo'], 'string', 'max' => 100],
            [['SSNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SSNo' => 'No SS',
            'SSTgl' => 'Tgl SS',
            'FBNo' => 'Fb No',
            'SupKode' => 'Kode Sup',
            'LokasiKode' => 'Lokasi Kode',
            'SSMemo' => 'Ss Memo',
            'UserID' => 'User ID',
            'SSJam' => 'Ss Jam',
            'SSNoTerima' => 'Ss No Terima',
            'DONo' => 'Do No',
            'NoGL' => 'No Gl',
        ];
    }

    public static function colGrid()
    {
        return [
            'ttss.SSNo' => ['width' => 125, 'label' => 'No SS', 'name' => 'SSNo'],
            'SSTgl' => ['width' => 70, 'label' => 'Tgl SS', 'name' => 'SSTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y']],
            'SSJam' => ['width' => 50, 'label' => 'Jam', 'name' => 'SSJam', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long", 'newformat' => 'H:i:s']],
            'DONo' => ['width' => 125, 'label' => 'No DO', 'name' => 'DONo'],
            'ttss.FBNo' => ['width' => 125, 'label' => 'No Faktur', 'name' => 'FBNo'],
            "IFNULL(ttfb.FBTgl, DATE('1900-01-01')) AS FBTgl" => ['width' => 70, 'label' => 'Tgl Faktur', 'name' => 'FBTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y']],
            "IFNULL(tmotor.SSJum, 0) AS SSJum" => ['width' => 50, 'label' => 'Jumlah', 'name' => 'SSJum', 'formatter' => 'integer', 'align' => 'right'],
            "tdlokasi.LokasiKode" => ['width' => 80, 'label' => 'Kode Lokasi', 'name' => 'LokasiKode',],
            "tdlokasi.LokasiNama" => ['width' => 80, 'label' => 'Nama Lokasi', 'name' => 'LokasiNama',],
            'tdsupplier.SupKode' => ['width' => 80, 'label' => 'Kode Sup', 'name' => 'SupKode'],
            'tdsupplier.SupNama' => ['width' => 80, 'label' => 'Nama Sup', 'name' => 'SupNama'],
            "SSMemo" => ['width' => 160, 'label' => 'Keterangan', 'name' => 'SSMemo'],
            "ttss.NoGL" => ['width' => 70, 'label' => 'No JT', 'name' => 'NoGL'],
            "ttss.UserID" => ['width' => 70, 'label' => 'UserID', 'name' => 'UserID'],
            "SSNoTerima" => ['width' => 70, 'label' => 'SSNoTerima', 'name' => 'SSNoTerima'],
            "tdlokasi.LokasiNama" => ['width' => 175, 'label' => 'Nama Lokasi', 'name' => 'LokasiNama',],
            'tdsupplier.SupNama' => ['width' => 125, 'label' => 'Nama Sup', 'name' => 'SupNama'],
        ];
    }

    public static function colGridImportItems()
    {
        return [
            'noShippingList' => ['width' => 120, 'label' => 'No Shipping', 'name' => 'noShippingList'],
            'noInvoice' => ['width' => 120, 'label' => 'No Invoice', 'name' => 'noInvoice'],
            'tanggalTerima' => ['width' => 90, 'label' => 'Tgl Terima', 'name' => 'tanggalTerima'],
            'statusShippingList' => ['width' => 80, 'label' => 'Status', 'name' => 'statusShippingList'],
            'mainDealerId' => ['width' => 100, 'label' => 'Main Dealer Id', 'name' => 'mainDealerId'],
            'dealerId' => ['width' => 70, 'label' => 'Dealer Id', 'name' => 'dealerId'],
            'createdTime' => ['width' => 70, 'label' => 'createdTime', 'name' => 'createdTime', 'hidden'=> true],
        ];
    }

    //FBTermin, FBPSS, FBPtgLain, FBTotal, DOTgl, FBExtraDisc, FBPtgMD, FPNo, FPTgl, FPHargaUnit, FPDiscount, FPDPP
    public function getFBNos()
    {
        return $this->hasMany(Ttfb::className(), ['FBNo' => 'FBNo']);
    }

    public function getSupplier()
    {
        return $this->hasOne(Tdsupplier::className(), ['SupKode' => 'SupKode']);
    }

    public function getLokasi()
    {
        return $this->hasOne(Tdlokasi::className(), ['LokasiKode' => 'LokasiKode']);
    }

    public function getMotor()
    {
        return $this->hasOne(Tmotor::className(), ['SSNo' => 'SSNo']);
    }

    public static function find()
    {
        return new TtssQuery(get_called_class());
    }
}
