<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Tuserlog]].
 *
 * @see Tuserlog
 */
class TuserlogQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tuserlog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tuserlog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
