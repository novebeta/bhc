<?php
namespace aunit\models;
/**
 * This is the model class for table "ttsmhd".
 *
 * @property string $SMNo
 * @property string $SMTgl
 * @property string $LokasiAsal
 * @property string $LokasiTujuan
 * @property string $SMMemo
 * @property string $SMTotal
 * @property string $UserID
 * @property string $SMJam
 */
class Ttsmhd extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttsmhd';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'SMNo' ], 'required' ],
			[ [ 'SMTgl', 'SMJam' ], 'safe' ],
			[ [ 'SMTotal' ], 'number' ],
			[ [ 'SMNo' ], 'string', 'max' => 10 ],
			[ [ 'LokasiAsal', 'LokasiTujuan', 'UserID' ], 'string', 'max' => 15 ],
			[ [ 'SMMemo' ], 'string', 'max' => 100 ],
			[ [ 'SMNo' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'SMNo'         => 'Sm No',
			'SMTgl'        => 'Sm Tgl',
			'LokasiAsal'   => 'Lokasi Asal',
			'LokasiTujuan' => 'Lokasi Tujuan',
			'SMMemo'       => 'Sm Memo',
			'SMTotal'      => 'Sm Total',
			'UserID'       => 'User ID',
			'SMJam'        => 'Sm Jam',
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public static function colGrid() {
		return [
			'SMNo'           => ['width' => 75,'label' => 'No SM/MI','name' => 'SMNo'],
			'SMTgl'          => ['width' => 75,'label' => 'Tgl SM/MI','name' => 'SMTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y']],
			'SMJam'          => ['width' => 50,'label' => 'Jam','name' => 'SMJam', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'H:i:s']],
			'LokasiAsal'     => ['width' => 100,'label' => 'Lokasi Asal','name' => 'LokasiAsal',],
			'LokasiTujuan'   => ['width' => 100,'label' => 'Lokasi Tujuan','name' => 'LokasiTujuan',],
			'SMMemo'         => ['width' => 250,'label' => 'Keterangan','name' => 'SMMemo',],
			'UserID'         => ['width' => 70,'label' => 'UserID','name'  => 'UserID'],
		];
	}
	//SMNo, SMTgl, LokasiAsal, LokasiTujuan, SMMemo, SMTotal, UserID, SMJam
	/**
	 * {@inheritdoc}
	 */
	public static function colGridMutasiInternalPos() {
		return [
			'SMNo'           => ['width' => 80,'label' => 'No SM/MI','name' => 'SMNo'],
			'SMTgl'          => ['width' => 80,'label' => 'Tgl SM/MI','name' => 'SMTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y']],
			'SMJam'          => ['width' => 60,'label' => 'Jam','name' => 'SMJam', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'H:i:s']],
			'LokasiAsal'     => ['width' => 125,'label' => 'Lokasi Asal','name' => 'LokasiAsal',],
			'LokasiTujuan'   => ['width' => 125,'label' => 'Lokasi Tujuan','name' => 'LokasiTujuan',],
			'SMMemo'         => ['width' => 380,'label' => 'Keterangan','name' => 'SMMemo',],
			'UserID'         => ['width' => 90,'label' => 'UserID','name'  => 'UserID'],
			'lasal.LokasiNama AS LokasiAsalNama'     => ['width' => 175,'label' => 'Lokasi Asal','name' => 'LokasiAsalNama'],
			'ltujuan.LokasiNama AS LokasiTujuanNama' => ['width' => 175,'label' => 'Lokasi Tujuan','name' => 'LokasiTujuanNama'],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public static function colGridMutasiRepairIn() {
		return [
			'SMNo'           => ['width' => 80,'label' => 'No SM/RI','name' => 'SMNo'],
			'SMTgl'          => ['width' => 80,'label' => 'Tgl SM/RI','name' => 'SMTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y']],
			'SMJam'          => ['width' => 60,'label' => 'Jam','name' => 'SMJam', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'H:i:s']],
			'LokasiAsal'     => ['width' => 125,'label' => 'Lokasi Asal','name' => 'LokasiAsal',],
			'LokasiTujuan'   => ['width' => 125,'label' => 'Lokasi Tujuan','name' => 'LokasiTujuan',],
			'SMMemo'         => ['width' => 380,'label' => 'Keterangan','name' => 'SMMemo',],
			'UserID'         => ['width' => 90,'label' => 'UserID','name'  => 'UserID'],
			'lasal.LokasiNama AS LokasiAsalNama'     => ['width' => 175,'label' => 'Lokasi Asal','name' => 'LokasiAsalNama'],
			'ltujuan.LokasiNama AS LokasiTujuanNama' => ['width' => 175,'label' => 'Lokasi Tujuan','name' => 'LokasiTujuanNama'],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public static function colGridMutasiRepairOut() {
		return [
			'SMNo'           => ['width' => 80,'label' => 'No SM/RO','name' => 'SMNo'],
			'SMTgl'          => ['width' => 80,'label' => 'Tgl SM/RO','name' => 'SMTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y']],
			'SMJam'          => ['width' => 60,'label' => 'Jam','name' => 'SMJam', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'H:i:s']],
			'LokasiAsal'     => ['width' => 125,'label' => 'Lokasi Asal','name' => 'LokasiAsal',],
			'LokasiTujuan'   => ['width' => 125,'label' => 'Lokasi Tujuan','name' => 'LokasiTujuan',],
			'SMMemo'         => ['width' => 380,'label' => 'Keterangan','name' => 'SMMemo',],
			'UserID'         => ['width' => 90,'label' => 'UserID','name'  => 'UserID'],
			'lasal.LokasiNama AS LokasiAsalNama'     => ['width' => 175,'label' => 'Lokasi Asal','name' => 'LokasiAsalNama'],
			'ltujuan.LokasiNama AS LokasiTujuanNama' => ['width' => 175,'label' => 'Lokasi Tujuan','name' => 'LokasiTujuanNama'],
		];
	}
	public function getRLokasiAsal() {
		return $this->hasOne( Tdlokasi::className(), [ 'LokasiKode' => 'LokasiAsal' ] );
	}
	public function getRLokasiTujuan() {
		return $this->hasOne( Tdlokasi::className(), [ 'LokasiKode' => 'LokasiTujuan' ] );
	}
//			'Tdlokasi.LokasiNama'  => ['width' => 100,'label' => 'Lokasi Asal','name' => 'LokasiNama',],

	/**
	 * {@inheritdoc}
	 * @return TtsmhdQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TtsmhdQuery( get_called_class() );
	}


	public static function getRoute( $jenis, $param = []) {
		$index  = [];
		$update = [];
		$create = [];
		switch ( $jenis ) {
			case 'MutasiPOS' :
				$index = [ 'ttsmhd/mutasi-internal-pos' ];
				$update = [ 'ttsmhd/update' ];
				$create = [ 'ttsmhd/mutasi-internal-pos-create' ];
				break;
			case 'RepairIN' :
				$index = [ 'ttsmhd/mutasi-repair-in' ];
				$update = [ 'ttsmhd/update'];
				$create = [ 'ttsmhd/mutasi-repair-in-create' ];
				break;
			case 'RepairOUT' :
				$index = [ 'ttsmhd/mutasi-repair-out' ];
				$update = [ 'ttsmhd/update' ];
				$create = [ 'ttsmhd/mutasi-repair-out-create' ];
				break;
		}
		return [
			'index'  => \yii\helpers\Url::toRoute( array_merge( $index, $param ) ),
			'update' => \yii\helpers\Url::toRoute( array_merge( $update, $param ) ),
			'create' => \yii\helpers\Url::toRoute( array_merge( $create, $param ) ),
		];
	}
}
