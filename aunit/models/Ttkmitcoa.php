<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "ttkmitcoa".
 *
 * @property string $KMNo
 * @property int $KMAutoN
 * @property string $NoAccount
 * @property string $KMBayar
 * @property string $KMKeterangan
 */
class Ttkmitcoa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttkmitcoa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KMNo', 'KMAutoN', 'NoAccount'], 'required'],
            [['KMAutoN'], 'integer'],
            [['KMBayar'], 'number'],
            [['KMNo'], 'string', 'max' => 12],
            [['NoAccount'], 'string', 'max' => 10],
            [['KMKeterangan'], 'string', 'max' => 150],
            [['KMNo', 'KMAutoN', 'NoAccount'], 'unique', 'targetAttribute' => ['KMNo', 'KMAutoN', 'NoAccount']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'KMNo' => 'Km No',
            'KMAutoN' => 'Km Auto N',
            'NoAccount' => 'No Account',
            'KMBayar' => 'Km Bayar',
            'KMKeterangan' => 'Km Keterangan',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtkmitcoaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtkmitcoaQuery(get_called_class());
    }
}
