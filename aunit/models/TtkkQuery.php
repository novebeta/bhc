<?php
namespace aunit\models;
use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttkk]].
 *
 * @see Ttkk
 */
class TtkkQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttkk[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttkk|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function ttkkTransfer() {
		$query = $this->select( new Expression( "ttkk.KKNo, ttkk.KKTgl, ttkk.KKMemo, ttkk.KKNominal, ttkk.KKJenis, ttkk.KKPerson, 
					ttkk.KKLink,ttkk.KKLink AS KTNo, ttkk.UserID, ttkk.LokasiKode AS LokasiKodeKK, ttkk.KKJam, ttkk.NoGL NoGLKK, ttkm.NoGL NoGLKM, ttkm.KMNo, 
					ttkm.KMTgl, ttkm.LokasiKode AS LokasiKodeKM, ttkm.KMMemo, ttkm.KMNominal, ttkm.KMJenis, ttkm.KMPerson, ttkm.KMLink,ttkm.KMJam" ) )
		              ->innerJoin( 'ttkm', 'ttkk.KKLInk = ttkm.KMLink' )
		              ->andWhere( "(ttkk.KKJenis = 'Kas Transfer' AND ttkm.KMJenis = 'Kas Transfer')" );
		return $query;
	}
	public function ttkkFillByNo() {
		return $this->select( new Expression( "KKNo, KKTgl, KKMemo, KKNominal, KKJenis, KKPerson, KKLink, UserID, LokasiKode, KKJam, NoGL" ) );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ] = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'KKNoBaru' ]   = $post[ 'KKNoView' ] ?? '--';
		$post[ 'KKNo' ]       = $post[ 'KKNo' ] ?? '--';
		$post[ 'KKTgl' ]      = date('Y-m-d', strtotime($post[ 'KKTgl' ] ?? date( 'Y-m-d' )));
		$post[ 'KKMemo' ]     = $post[ 'KKMemo' ] ?? '--';
		$post[ 'KKNominal' ]  = floatval( $post[ 'KKNominal' ] ?? 0 );
		$post[ 'KKJenis' ]    = $post[ 'KKJenis' ] ?? '--';
		$post[ 'KKPerson' ]   = $post[ 'KKPerson' ] ?? '--';
		$post[ 'KKLink' ]     = $post[ 'KKLink' ] ?? '--';
		$post[ 'KKJam' ]      = date('Y-m-d H:i:s', strtotime($post[ 'KKJam' ] ?? date( 'Y-m-d H:i:s' )));
		$post[ 'NoGL' ]       = $post[ 'NoGL' ] ?? '--';
		try {
			General::cCmd( "SET @KKNo = ?;" )->bindParam( 1, $post[ 'KKNo' ] )->execute();
			General::cCmd( "CALL fkk(?,@Status,@Keterangan,?,?,?,@KKNo,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'KKNoBaru' ] )
			       ->bindParam( 6, $post[ 'KKTgl' ] )
			       ->bindParam( 7, $post[ 'KKMemo' ] )
			       ->bindParam( 8, $post[ 'KKNominal' ] )
			       ->bindParam( 9, $post[ 'KKJenis' ] )
			       ->bindParam( 10, $post[ 'KKPerson' ] )
			       ->bindParam( 11, $post[ 'KKLink' ] )
			       ->bindParam( 12, $post[ 'KKJam' ] )
			       ->bindParam( 13, $post[ 'NoGL' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @KKNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'KKNo'       => $post[ 'KKNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$KKNo       = $exe[ '@KKNo' ] ?? $post[ 'KKNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'KKNo'       => $KKNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
	public function callSPTransfer( $post ) {
		$post[ 'UserID' ]         = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]      = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]     = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'KTNo' ]           = $post[ 'KTNo' ] ?? '--';
		$post[ 'KTNoBaru' ]       = $post[ 'KTNoBaru' ] ?? '--';
		$post[ 'KTTgl' ]          = $post[ 'KKTgl' ] ?? date( 'Y-m-d' );
		$post[ 'KTMemo' ]         = $post[ 'KKMemo' ] ?? '--';
		$post[ 'KTNominal' ]      = $post[ 'KKNominal' ] ?? 0;
		$post[ 'KTJenis' ]        = $post[ 'KKJenis' ] ?? '--';
		$post[ 'KKPerson' ]       = $post[ 'KKPerson' ] ?? '--';
		$post[ 'KTPersonAsal' ]   = $post[ 'KKPerson' ] ?? '--';
		$post[ 'KTPersonTujuan' ] = $post[ 'KMPerson' ] ?? '--';
		$post[ 'KKNo' ]           = $post[ 'KKNo' ] ?? '--';
		$post[ 'KMNo' ]           = $post[ 'KMNo' ] ?? '--';
		$post[ 'KTJam' ]          = $post[ 'KKJam' ] ?? date( 'Y-m-d H:i:s' );
		$post[ 'NoGLKK' ]         = $post[ 'NoGLKK' ] ?? '--';
		$post[ 'NoGLKM' ]         = $post[ 'NoGLKM' ] ?? '--';
		$post[ 'KTLokasiAsal' ]   = $post[ 'LokasiKodeKK' ] ?? '--';
		$post[ 'KTLokasiTujuan' ] = $post[ 'LokasiKodeKM' ] ?? '--';
		try {
			General::cCmd( "SET @KTNo = ?;" )->bindParam( 1, $post[ 'KTNo' ] )->execute();
			General::cCmd( "CALL fkt(?,@Status,@Keterangan,?,?,?,@KTNo,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'KTNoBaru' ] )
			       ->bindParam( 6, $post[ 'KTTgl' ] )
			       ->bindParam( 7, $post[ 'KTMemo' ] )
			       ->bindParam( 8, $post[ 'KTNominal' ] )
			       ->bindParam( 9, $post[ 'KTJenis' ] )
			       ->bindParam( 10, $post[ 'KTPersonAsal' ] )
			       ->bindParam( 11, $post[ 'KTPersonTujuan' ] )
			       ->bindParam( 12, $post[ 'KKNo' ] )
			       ->bindParam( 13, $post[ 'KMNo' ] )
			       ->bindParam( 14, $post[ 'KTJam' ] )
			       ->bindParam( 15, $post[ 'NoGLKK' ] )
			       ->bindParam( 16, $post[ 'NoGLKM' ] )
			       ->bindParam( 17, $post[ 'KTLokasiAsal' ] )
			       ->bindParam( 18, $post[ 'KTLokasiTujuan' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @KTNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'KTNo'       => $post[ 'KTNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$KTNo       = $exe[ '@KTNo' ] ?? $post[ 'KTNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'KTNo'       => $KTNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
	public function callSPPengajuan( $post ) {
		$post[ 'UserID' ]     = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]  = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ] = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'KKNo' ]       = $post[ 'KKNo' ] ?? '--';
		$post[ 'KKNoBaru' ]   = $post[ 'KKNoView' ] ?? '--';
		$post[ 'KKTgl' ]      = $post[ 'KKTgl' ] ?? date( 'Y-m-d' );
		$post[ 'KKMemo' ]     = $post[ 'KKMemo' ] ?? '--';
		$post[ 'KKNominal' ]  = $post[ 'KKNominal' ] ?? 0;
		$post[ 'KKJenis' ]    = $post[ 'KKJenis' ] ?? '--';
		$post[ 'KKPerson' ]   = $post[ 'KKPerson' ] ?? '--';
		$post[ 'KKLink' ]     = $post[ 'KKLink' ] ?? '--';
		$post[ 'KKJam' ]      = $post[ 'KKJam' ] ?? date( 'Y-m-d H:i:s' );
		$post[ 'NoGL' ]       = $post[ 'NoGL' ] ?? '--';
		try {
			General::cCmd( "SET @KKNo = ?;" )->bindParam( 1, $post[ 'KKNo' ] )->execute();
			General::cCmd( "CALL fkp(?,@Status,@Keterangan,?,?,?,@KKNo,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'KKNoBaru' ] )
			       ->bindParam( 6, $post[ 'KKTgl' ] )
			       ->bindParam( 7, $post[ 'KKMemo' ] )
			       ->bindParam( 8, $post[ 'KKNominal' ] )
			       ->bindParam( 9, $post[ 'KKJenis' ] )
			       ->bindParam( 10, $post[ 'KKPerson' ] )
			       ->bindParam( 11, $post[ 'KKLink' ] )
			       ->bindParam( 12, $post[ 'KKJam' ] )
			       ->bindParam( 13, $post[ 'NoGL' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @KKNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'KKNo'       => $post[ 'KKNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$KKNo       = $exe[ '@KKNo' ] ?? $post[ 'KKNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'KKNo'       => $KKNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
	public function GetJoinData() {
		return $this->select( new Expression( "tdlokasi.LokasiKode, tdlokasi.LokasiNama, tdlokasi.LokasiPetugas,
                tdlokasi.LokasiAlamat as CusAlamat, tdlokasi.LokasiStatus, tdlokasi.LokasiTelepon, tdlokasi.LokasiNomor, 
                ttkk.KKNo, ttkk.KKTgl, ttkk.KKPerson, ttkk.KKMemo " ) )
		            ->join( "INNER JOIN", "tdlokasi", "ttkk.LokasiKode = tdlokasi.LokasiKode " );
	}
}
