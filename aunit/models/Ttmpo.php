<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "ttmpo".
 *
 * @property string $PONo
 * @property string $POTgl
 * @property string $MotorType
 * @property string $POQty
 */
class Ttmpo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttmpo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PONo', 'POTgl', 'MotorType'], 'required'],
            [['POTgl'], 'safe'],
            [['POQty'], 'number'],
            [['PONo'], 'string', 'max' => 10],
            [['MotorType'], 'string', 'max' => 50],
            [['PONo', 'POTgl', 'MotorType'], 'unique', 'targetAttribute' => ['PONo', 'POTgl', 'MotorType']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PONo' => 'Po No',
            'POTgl' => 'Po Tgl',
            'MotorType' => 'Motor Type',
            'POQty' => 'Po Qty',
        ];
    }


	/**
	 * {@inheritdoc}
	 * SELECT        ttmdo.DONo, ttmdo.DOTgl, ttmdo.MotorType, ttmdo.DOQty, tdmotortype.MotorNama
	 * FROM            ttmdo INNER JOIN
	 * tdmotortype ON ttmdo.MotorType = tdmotortype.MotorType
	 */
	public static function colGrid() {
		return [
			'ttmpo.MotorType'       => [ 'width' => 50, 'label' => 'Type', 'name' => 'MotorType' ],
			'tdmotortype.MotorNama' => [ 'width' => 90, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
			'ttmpo.POQty'           => [ 'width' => 90, 'label' => 'Qty', 'name' => 'POQty' ],
		];
	}


	public function getMotorTypes() {
		return $this->hasOne( Tdmotortype::className(), [ 'MotorType' => 'MotorType' ] );
	}
    /**
     * {@inheritdoc}
     * @return TtmpoQuery the active query used by this AR class.
     * SELECT        ttmpo.PONo, ttmpo.POTgl, ttmpo.MotorType, ttmpo.POQty, tdmotortype.MotorNama
    FROM            ttmpo INNER JOIN
    tdmotortype ON ttmpo.MotorType = tdmotortype.MotorType
     */
    public static function find()
    {
        return new TtmpoQuery(get_called_class());
    }
}
