<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "ttcfhd".
 *
 * @property string $CFNo
 * @property string $CFTgl
 * @property string $CFMemo
 * @property string $LokasiKode
 * @property string $SalesKode
 * @property float $StockBuku
 * @property float $StockFisik
 * @property float $NilaiBuku
 * @property float $NilaiFisik
 * @property string $UserID
 */
class Ttcfhd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttcfhd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CFNo', 'CFTgl', 'CFMemo', 'LokasiKode', 'SalesKode', 'UserID'], 'required'],
            [['CFTgl'], 'safe'],
            [['StockBuku', 'StockFisik', 'NilaiBuku', 'NilaiFisik'], 'number'],
            [['CFNo', 'SalesKode'], 'string', 'max' => 10],
            [['CFMemo'], 'string', 'max' => 150],
            [['LokasiKode', 'UserID'], 'string', 'max' => 15],
            [['CFNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CFNo' => 'Cf No',
            'CFTgl' => 'Cf Tgl',
            'CFMemo' => 'Cf Memo',
            'LokasiKode' => 'Lokasi Kode',
            'SalesKode' => 'Sales Kode',
            'StockBuku' => 'Stock Buku',
            'StockFisik' => 'Stock Fisik',
            'NilaiBuku' => 'Nilai Buku',
            'NilaiFisik' => 'Nilai Fisik',
            'UserID' => 'User ID',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtcfhdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtcfhdQuery(get_called_class());
    }

    public static function colGrid()
    {
        return [
            'CFNo' => [
                'label' => 'No CF',
                'width' => 100,
                'name' => 'CFNo'
            ],
            'CFTgl' => [
                'label' => 'CFTgl',
                'width' => 150,
                'name' => 'CFTgl'
            ],
            'LokasiKode' => [
                'label' => 'LokasiKode',
                'width' => 110,
                'name' => 'LokasiKode'
            ],
            'SalesKode' => [
                'label' => 'SalesKode',
                'width' => 90,
                'name' => 'SalesKode'
            ],
            'SalesNama' => [
                'label' => 'Sales Nama',
                'width' => 80,
                'name' => 'SalesNama'
            ],
            'StockBuku' => [
                'label' => 'StockBuku',
                'width' => 90,
                'name' => 'StockBuku'
            ],
            'StockFisik' => [
                'label' => 'StockFisik',
                'width' => 90,
                'name' => 'StockFisik'
            ],
            'UserID' => [
                'label' => 'UserID',
                'width' => 90,
                'name' => 'UserID'
            ],
        ];
    }
}
