<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "ttcfit".
 *
 * @property string $CFNo
 * @property int $MotorAutoN
 * @property string $MotorNoMesin
 * @property float|null $FBHarga
 */
class Ttcfit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttcfit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CFNo', 'MotorAutoN', 'MotorNoMesin'], 'required'],
            [['MotorAutoN'], 'integer'],
            [['FBHarga'], 'number'],
            [['CFNo'], 'string', 'max' => 10],
            [['MotorNoMesin'], 'string', 'max' => 25],
            [['CFNo', 'MotorAutoN', 'MotorNoMesin'], 'unique', 'targetAttribute' => ['CFNo', 'MotorAutoN', 'MotorNoMesin']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CFNo' => 'Cf No',
            'MotorAutoN' => 'Motor Auto N',
            'MotorNoMesin' => 'Motor No Mesin',
            'FBHarga' => 'Fb Harga',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtcfitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtcfitQuery(get_called_class());
    }
}
