<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Tdcekfisik]].
 *
 * @see Tdcekfisik
 */
class TdcekfisikQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdcekfisik[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdcekfisik|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
