<?php
namespace aunit\models;
use Yii;
/**
 * This is the model class for table "ttsk".
 *
 * @property string $SKNo
 * @property string $SKTgl
 * @property string $LokasiKode
 * @property string $SKMemo
 * @property string $UserID
 * @property string $SKJam
 * @property string $NoGL
 * @property string $SKCetak
 * @property string $SKPengirim
 * @property string $DKNo
 * @property string $SKDoc
 */
class Ttsk extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttsk';
	}
	/**
	 * {@inheritdoc}
	 */
	public static function colGrid() {
		return [
			'SKDoc' 	=> [ 'width' => 30, 'label' => 'Doc', 'name' => 'SKDoc' ],
			'SKNo'          => [ 'width' => 80, 'label' => 'No SK', 'name' => 'SKNo' ],
			'SKTgl'         => [ 'width' => 70, 'label' => 'Tgl SK', 'name' => 'SKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
            'tdcustomer.CusNama'   => [ 'label' => 'Nama Konsumen', 'width' => 200, 'name' => 'CusNama' ],
			'MotorNama'     => [ 'width' => 210, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
			'MotorWarna'    => [ 'width' => 120, 'label' => 'Warna Motor', 'name' => 'MotorWarna' ],
			'MotorNoMesin'  => [ 'width' => 105, 'label' => 'No Mesin', 'name' => 'MotorNoMesin' ],
			'MotorNoRangka' => [ 'width' => 140, 'label' => 'No Rangka', 'name' => 'MotorNoRangka' ],
			"DKNo"          => [ 'width' => 70, 'label' => 'No DK', 'name' => 'DKNo' ],
		];
	}
	/**
	 * {@inheritdoc}
	 * @return TtskQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TtskQuery( get_called_class() );
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'SKNo' ], 'required' ],
			[ [ 'SKTgl', 'SKJam' ], 'safe' ],
			[ [ 'SKNo', 'NoGL', 'DKNo' ], 'string', 'max' => 10 ],
			[ [ 'LokasiKode', 'UserID' ], 'string', 'max' => 15 ],
			[ [ 'SKMemo' ], 'string', 'max' => 100 ],
			[ [ 'SKCetak' ], 'string', 'max' => 5 ],
			[ [ 'SKPengirim' ], 'string', 'max' => 50 ],
			[ [ 'SKNo' ], 'unique' ],
			[ [ 'SKDoc'], 'number' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'SKNo'       => 'Sk No',
			'SKTgl'      => 'Sk Tgl',
			'LokasiKode' => 'Lokasi Kode',
			'SKMemo'     => 'Sk Memo',
			'UserID'     => 'User ID',
			'SKJam'      => 'Sk Jam',
			'NoGL'       => 'No Gl',
			'SKCetak'    => 'Sk Cetak',
			'SKPengirim' => 'Sk Pengirim',
			'DKNo'       => 'Dk No',
			'SKDoc'      => 'SKDoc',
		];
	}


    public static function colGridImportItems()
    {
        return [
            'deliveryDocumentId' => ['width' => 150, 'label' => 'Delivery Document ID', 'name' => 'deliveryDocumentId'],
            'idDriver' => ['width' => 75, 'label' => 'Honda ID', 'name' => 'idDriver'],
            'tanggalPengiriman' => ['width' => 90, 'label' => 'Tgl Pengiriman', 'name' => 'tanggalPengiriman'],
            'statusDeliveryDocument' => ['width' => 150, 'label' => 'Status Delivery Document', 'name' => 'statusDeliveryDocument'],
        ];
    }

	public function getMotor() {
		return $this->hasOne( Tmotor::className(), [ 'SKNo' => 'SKNo' ] );
	}
	public function getData( $param = null ) {
		return Yii::$app->db->createCommand( "SELECT ttsk.SKJam, ttsk.SKCetak, ttsk.SKPengirim, ttsk.SKNo, ttsk.SKTgl, ttdk.DKTgl,
       ttsk.LokasiKode, ttsk.SKMemo, ttsk.UserID, ttdk.CusKode,tdcustomer.CusKTP, tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT,
       tdcustomer.CusRW, tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, tdcustomer.CusTelepon, tdcustomer.CusKodePos,
       tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, ttdk.DKNo, tdcustomer.CusKodePos,
       ttdk.DKHarga, tmotor.RKNo, '--' AS NoGL
		FROM ttsk 
		INNER JOIN tmotor ON ttsk.SKNo = tmotor.SKNo
		INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
		INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
		WHERE ttsk.SKNo = :SKNo", [ ':SKNo' => $param[ 'SKNo' ] ] )
		                    ->queryOne();
	}
}
