<?php
namespace aunit\models;
/**
 * This is the model class for table "ttfb".
 *
 * @property string $FBNo
 * @property string $FBTgl
 * @property string $SSNo
 * @property string $SupKode
 * @property string $FBTermin
 * @property string $FBTglTempo
 * @property string $FBPSS
 * @property string $FBPtgLain
 * @property string $FBTotal
 * @property string $FBMemo
 * @property string $FBLunas
 * @property string $UserID
 * @property string $NoGL
 * @property string $FPNo
 * @property string $FPTgl
 * @property string $DOTgl
 * @property string $FBExtraDisc
 * @property string $FBPosting
 * @property string $FBPtgMD
 * @property string $FBPPNm
 * @property string $FBJam
 */
class Ttfb extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttfb';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'FBNo' ], 'required' ],
			[ [ 'FBTgl', 'FBTglTempo', 'FPTgl', 'DOTgl', 'FBJam' ], 'safe' ],
			[ [ 'FBTermin', 'FBPSS', 'FBPtgLain', 'FBTotal', 'FBExtraDisc', 'FBPtgMD','FBPPNm' ], 'number' ],
			[ [ 'FBNo', 'SSNo' ], 'string', 'max' => 30 ],
			[ [ 'SupKode', 'NoGL' ], 'string', 'max' => 10 ],
			[ [ 'FBMemo' ], 'string', 'max' => 100 ],
			[ [ 'FBLunas' ], 'string', 'max' => 5 ],
			[ [ 'UserID', 'FBPosting' ], 'string', 'max' => 15 ],
			[ [ 'FPNo' ], 'string', 'max' => 30 ],
			[ [ 'FBNo' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'FBNo'        => 'Fb No',
			'FBTgl'       => 'Fb Tgl',
			'SSNo'        => 'Ss No',
			'SupKode'     => 'Sup Kode',
			'FBTermin'    => 'Fb Termin',
			'FBTglTempo'  => 'Fb Tgl Tempo',
			'FBPSS'       => 'Fbpss',
			'FBPtgLain'   => 'Fb Ptg Lain',
			'FBTotal'     => 'Fb Total',
			'FBMemo'      => 'Fb Memo',
			'FBLunas'     => 'Fb Lunas',
			'UserID'      => 'User ID',
			'NoGL'        => 'No Gl',
			'FPNo'        => 'Fp No',
			'FPTgl'       => 'Fp Tgl',
			'DOTgl'       => 'Do Tgl',
			'FBExtraDisc' => 'Fb Extra Disc',
			'FBPosting'   => 'Fb Posting',
			'FBPtgMD'     => 'Fb Ptg Md',
			'FBPPNm'     => 'Fb Ppn',
			'FBJam'       => 'Fb Jam',
		];
	}
	public static function colGrid() {
		return [
			'ttfb.FBNo'                                       => [ 'width' => 125, 'label' => 'No FB', 'name' => 'FBNo' ],
			'FBTgl'                                           => [ 'width' => 70, 'label' => 'Tgl FB', 'name' => 'FBTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'FBJam'                                           => [ 'width' => 50, 'label' => 'Jam', 'name' => 'FBJam', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'H:i:s' ] ],
			'ttfb.SSNo'                                       => [ 'width' => 125, 'label' => 'No SS', 'name' => 'SSNo' ],
			"IFNULL(ttss.SSTgl, DATE('1900-01-01')) AS SSTgl" => [ 'width' => 70, 'label' => 'Tgl SS', 'name' => 'SSTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			"IFNULL(tmotor.FBJum, 0) AS FBJum"                => [ 'width' => 50, 'label' => 'Jumlah', 'name' => 'FBJum', 'formatter' => 'integer', 'align' => 'right' ],
			"FBTotal"                                         => [ 'width' => 125, 'label' => 'Total', 'name' => 'FBTotal', 'formatter' => 'number', 'align' => 'right' ],
			'FBTglTempo'                                      => [ 'width' => 70, 'label' => 'Tgl Tempo', 'name' => 'FBTglTempo', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			"ttfb.SupKode"                                    => [ 'width' => 100, 'label' => 'Supplier', 'name' => 'SupKode' ],
			"ttfb.FBMemo"                                     => [ 'width' => 150, 'label' => 'Keterangan', 'name' => 'FBMemo' ],
			"ttfb.NoGL"                                       => [ 'width' => 70, 'label' => 'No GL', 'name' => 'NoGL' ],
			"ttfb.UserID"                                     => [ 'width' => 70, 'label' => 'UserID', 'name' => 'UserID' ],
		];
	}
	public static function colGridBankKeluarBayarHutangUnit() {
		return [
			'FBNo'                                       => [ 'width' => 125, 'label' => 'No FB', 'name' => 'FBNo' ],
			'FBTgl'                                      => [ 'width' => 70, 'label' => 'Tgl FB', 'name' => 'FBTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'SSNo'                                       => [ 'width' => 125, 'label' => 'No SS', 'name' => 'SSNo' ],
			"IFNULL(SSTgl, DATE('1900-01-01')) AS SSTgl" => [ 'width' => 70, 'label' => 'Tgl SS', 'name' => 'SSTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			"FBTotal"                                    => [ 'width' => 125, 'label' => 'FB Total', 'name' => 'FBTotal', 'formatter' => 'number', 'align' => 'right' ],
			"Terbayar"                                   => [ 'width' => 125, 'label' => 'Terbayar', 'name' => 'Terbayar', 'formatter' => 'number', 'align' => 'right' ],
			'BKBayar'   => [ 'label' => 'Bayar', 'width' => 80, 'name' => 'BKBayar', 'formatter' => 'number', 'align' => 'right', 'editable' => true , 'formatoptions' => [ 'decimalPlaces' => 0 ] ],
			"Sisa"                                       => [ 'width' => 125, 'label' => 'Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
		];
	}
	public static function colGridKasKeluarBayarHutangUnit() {
		return [
			'FBNo'                                       => [ 'width' => 125, 'label' => 'No FB', 'name' => 'FBNo' ],
			'FBTgl'                                      => [ 'width' => 70, 'label' => 'Tgl FB', 'name' => 'FBTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'SSNo'                                       => [ 'width' => 125, 'label' => 'No SS', 'name' => 'SSNo' ],
			"IFNULL(SSTgl, DATE('1900-01-01')) AS SSTgl" => [ 'width' => 70, 'label' => 'Tgl SS', 'name' => 'SSTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			"FBTotal"                                    => [ 'width' => 125, 'label' => 'FB Total', 'name' => 'FBTotal', 'formatter' => 'number', 'align' => 'right' ],
			"Terbayar"                                   => [ 'width' => 125, 'label' => 'Terbayar', 'name' => 'Terbayar', 'formatter' => 'number', 'align' => 'right' ],
			'KKBayar'   => [ 'label' => 'Bayar', 'width' => 80, 'name' => 'KKBayar', 'formatter' => 'number', 'align' => 'right', 'editable' => true , 'formatoptions' => [ 'decimalPlaces' => 0 ] ],
			"Sisa"                                       => [ 'width' => 125, 'label' => 'Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
		];
	}

    public static function colGridImportItems()
    {
        return [
            'noShippingList' => ['width' => 125, 'label' => 'No Shipping', 'name' => 'noShippingList'],
            'noInvoice' => ['width' => 125, 'label' => 'No Invoice', 'name' => 'noInvoice'],
            'tanggalTerima' => ['width' => 125, 'label' => 'Tgl Terima', 'name' => 'tanggalTerima'],
            'statusShippingList' => ['width' => 125, 'label' => 'Status', 'name' => 'statusShippingList'],
            'mainDealerId' => ['width' => 125, 'label' => 'mainDealerId', 'name' => 'mainDealerId'],
            'dealerId' => ['width' => 125, 'label' => 'dealerId', 'name' => 'dealerId'],
        ];
    }

	public function getSuratSupp() {
		return $this->hasOne( Ttss::className(), [ 'SSNo' => 'SSNo' ] );
	}
	public function getSupplier() {
		return $this->hasOne( Tdsupplier::className(), [ 'SupKode' => 'SupKode' ] );
	}
	/**
	 * @return bool
	 */
	public function beforeValidate() {
		if ( $this->isNewRecord ) {
			$this->FBNo   = self::generateReference( true );
			$this->UserID = \yii::$app->user->id;
		}
		return parent::beforeValidate();
	}
	/**
	 * @param bool $temporary
	 *
	 * @return mixed|string
	 */
	public static function generateReference( $temporary = false ) {
		$kode  = 'FB';
		$yNow  = date( "y" );
		$maxNo = ( new \yii\db\Query() )
			->from( self::tableName() )
			->where( "FBNo LIKE '$kode$yNow-%'" )
			->max( 'FBNo' );
		if ( $maxNo && preg_match( "/$kode\d{2}-(\d+)/", $maxNo, $result ) ) {
			[ $all, $counter ] = $result;
			$digitCount  = strlen( $counter );
			$val         = (int) $counter;
			$nextCounter = sprintf( '%0' . $digitCount . 'd', ++ $val );
		} else {
			$nextCounter = "00001";
		}
		$nextRef = "$kode$yNow-$nextCounter";
		return $temporary ? str_replace( '-', '=', $nextRef ) : $nextRef;
	}
	/**
	 * {@inheritdoc}
	 * @return TtfbQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TtfbQuery( get_called_class() );
	}
}

//"SELECT ttfb.FBNo, ttfb.FBTgl, ttfb.SSNo, ttfb.SupKode, ttfb.FBTermin, ttfb.FBTglTempo, ttfb.FBPSS, " + _
//         "ttfb.FBPtgLain, ttfb.FBTotal, ttfb.FBMemo, ttfb.FBLunas, ttfb.UserID, " + _
//         "IFNULL(tdsupplier.SupNama, '--') AS SupNama, IFNULL(ttss.SSTgl, DATE('1900-01-01')) AS SSTgl, " + _
//         "IFNULL(tmotor.FBJum, 0) AS FBJum, IFNULL(tmotor.FBSubTotal, 0) AS FBSubTotal, ttfb.NoGL " + _
//         "FROM ttfb " + _
//         "LEFT OUTER JOIN ttss ON ttfb.SSNo = ttss.SSNo " + _
//         "LEFT OUTER JOIN tdsupplier ON ttfb.SupKode = tdsupplier.SupKode " + _
//         "LEFT OUTER JOIN " + _
//         "(SELECT COUNT(MotorType) AS FBJum, SUM(FBHarga) AS FBSubTotal, FBNo FROM tmotor tmotor_1 GROUP BY FBNo) tmotor " + _
//         "ON ttfb.FBNo = tmotor.FBNo " + _
