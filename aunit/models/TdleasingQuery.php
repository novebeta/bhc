<?php
namespace aunit\models;
use yii\helpers\ArrayHelper;
/**
 * This is the ActiveQuery class for [[Tdleasing]].
 *
 * @see Tdleasing
 */
class TdleasingQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Tdleasing[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Tdleasing|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function combo() {
		return $this->select( [ "CONCAT(LeaseKode,' - ',LeaseNama) as label", "LeaseKode as value" ] )
		            ->orderBy( 'LeaseKode' )
		            ->asArray()
		            ->all();
	}
	public function code() {
		return ArrayHelper::map( $this->select( [ "CONCAT(LeaseKode) as label", "LeaseKode as value" ] )
		                              ->orderBy( 'LeaseKode' )
		                              ->asArray()
		                              ->all(), 'value', 'label' );
	}
	public function select2($value, $label = [ 'LeaseNama' ] , $orderBy = ['LeaseNama'], $where = [ 'condition' => null, 'params' => [] ], $allOption = false ) {
		$option =
			$this->select( [ "CONCAT(" . implode( ",'||',", $label ) . ") as label", "$value as value" ] )
			     ->orderBy( $orderBy )
			     ->where( $where[ 'condition' ], $where[ 'params' ] )
			     ->asArray()
			     ->all();
		if ( $allOption ) {
			return array_merge( [ [ 'value' => '%', 'label' => 'Semua' ] ], $option );
		} else {
			return $option;
		}
	}
}
