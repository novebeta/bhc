<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Tsaldobank]].
 *
 * @see Tsaldobank
 */
class TsaldobankQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tsaldobank[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tsaldobank|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
