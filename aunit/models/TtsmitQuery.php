<?php
namespace aunit\models;
use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttsmit]].
 *
 * @see Ttsmit
 */
class TtsmitQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttsmit[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttsmit|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function GetDataByNo() {
		return $this->select( new Expression( "ttsmit.SMNo, ttsmit.MotorNoMesin, ttsmit.MotorAutoN, tmotor.MotorType, 
					tmotor.MotorTahun, tmotor.MotorWarna, tmotor.MotorNoRangka, tmotor.FBHarga,tdmotortype.MotorNama" ) )
		            ->from( "ttsmit" )
		            ->join( "INNER JOIN", "tmotor", "ttsmit.MotorNoMesin = tmotor.MotorNoMesin AND ttsmit.MotorAutoN = tmotor.MotorAutoN" )
		            ->join( "LEFT OUTER JOIN", "tdmotortype", "tmotor.MotorType = tdmotortype.MotorType" );
	}
	public function callSP( $post, $jenis ) {
		$post[ 'UserID' ]          = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]       = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]      = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'SMNo' ]            = $post[ 'SMNo' ] ?? '--';
		$post[ 'MotorType' ]       = $post[ 'MotorType' ] ?? '--';
		$post[ 'MotorNama' ]       = $post[ 'MotorNama' ] ?? '--';
		$post[ 'MotorTahun' ]      = floatval( $post[ 'MotorTahun' ] ?? 1900 );
		$post[ 'MotorWarna' ]      = $post[ 'MotorWarna' ] ?? '--';
		$post[ 'MotorNoRangka' ]   = $post[ 'MotorNoRangka' ] ?? '--';
		$post[ 'MotorAutoN' ]      =  $post[ 'MotorAutoN' ] ?? 0 ;
        $post[ 'MotorNoMesin' ]    = $post[ 'MotorNoMesin' ] ?? '--';
		$post[ 'MotorAutoNOLD' ]   = floatval( $post[ 'MotorAutoNOLD' ] ?? 0 );
		$post[ 'MotorNoMesinOLD' ] = $post[ 'MotorNoMesinOLD' ] ?? '--';
		try {
            			switch ($jenis){
                        case "MutasiPOS":
                            General::cCmd( "CALL fsmit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                                ->bindParam( 1, $post[ 'Action' ] )
                                ->bindParam( 2, $post[ 'UserID' ] )
                                ->bindParam( 3, $post[ 'KodeAkses' ] )
                                ->bindParam( 4, $post[ 'LokasiKode' ] )
                                ->bindParam( 5, $post[ 'SMNo' ] )
                                ->bindParam( 6, $post[ 'MotorType' ] )
                                ->bindParam( 7, $post[ 'MotorNama' ] )
                                ->bindParam( 8, $post[ 'MotorTahun' ] )
                                ->bindParam( 9, $post[ 'MotorWarna' ] )
                                ->bindParam( 10, $post[ 'MotorNoRangka' ] )
                                ->bindParam( 11, $post[ 'MotorAutoN' ] )
                                ->bindParam( 12, $post[ 'MotorNoMesin' ] )
                                ->bindParam( 13, $post[ 'MotorAutoNOLD' ] )
                                ->bindParam( 14, $post[ 'MotorNoMesinOLD' ] )
                                ->execute();
                            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
                            break;
                        case "RepairIN":
                            General::cCmd( "CALL friit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                                ->bindParam( 1, $post[ 'Action' ] )
                                ->bindParam( 2, $post[ 'UserID' ] )
                                ->bindParam( 3, $post[ 'KodeAkses' ] )
                                ->bindParam( 4, $post[ 'LokasiKode' ] )
                                ->bindParam( 5, $post[ 'SMNo' ] )
                                ->bindParam( 6, $post[ 'MotorType' ] )
                                ->bindParam( 7, $post[ 'MotorNama' ] )
                                ->bindParam( 8, $post[ 'MotorTahun' ] )
                                ->bindParam( 9, $post[ 'MotorWarna' ] )
                                ->bindParam( 10, $post[ 'MotorNoRangka' ] )
                                ->bindParam( 11, $post[ 'MotorAutoN' ] )
                                ->bindParam( 12, $post[ 'MotorNoMesin' ] )
                                ->bindParam( 13, $post[ 'MotorAutoNOLD' ] )
                                ->bindParam( 14, $post[ 'MotorNoMesinOLD' ] )
                                ->execute();
                            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
                            break;
                            case "RepairOUT":
                                General::cCmd( "CALL froit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
                                    ->bindParam( 1, $post[ 'Action' ] )
                                    ->bindParam( 2, $post[ 'UserID' ] )
                                    ->bindParam( 3, $post[ 'KodeAkses' ] )
                                    ->bindParam( 4, $post[ 'LokasiKode' ] )
                                    ->bindParam( 5, $post[ 'SMNo' ] )
                                    ->bindParam( 6, $post[ 'MotorType' ] )
                                    ->bindParam( 7, $post[ 'MotorNama' ] )
                                    ->bindParam( 8, $post[ 'MotorTahun' ] )
                                    ->bindParam( 9, $post[ 'MotorWarna' ] )
                                    ->bindParam( 10, $post[ 'MotorNoRangka' ] )
                                    ->bindParam( 11, $post[ 'MotorAutoN' ] )
                                    ->bindParam( 12, $post[ 'MotorNoMesin' ] )
                                    ->bindParam( 13, $post[ 'MotorAutoNOLD' ] )
                                    ->bindParam( 14, $post[ 'MotorNoMesinOLD' ] )
                                    ->execute();
                                $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
                                break;
                    }

		} catch ( Exception $e ) {
			return [
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
}
