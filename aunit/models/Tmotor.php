<?php
namespace aunit\models;
/**
 * This is the model class for table "tmotor".
 *
 * @property int $MotorAutoN
 * @property string $MotorNoMesin
 * @property string $MotorNoRangka
 * @property string $MotorType
 * @property string $MotorWarna
 * @property string $MotorTahun
 * @property string $MotorMemo
 * @property string $DealerKode
 * @property string $SSNo
 * @property string $FBNo
 * @property string $FBHarga
 * @property string $DKNo
 * @property string $SKNo
 * @property string $PDNo
 * @property string $SDNo
 * @property string $BPKBNo
 * @property string $BPKBTglJadi
 * @property string $BPKBTglAmbil
 * @property string $STNKNo
 * @property string $STNKTglMasuk
 * @property string $STNKTglBayar
 * @property string $STNKTglJadi
 * @property string $STNKTglAmbil
 * @property string $STNKNama
 * @property string $STNKAlamat
 * @property string $PlatNo
 * @property string $PlatTgl
 * @property string $PlatTglAmbil
 * @property string $FakturAHMNo
 * @property string $FakturAHMTgl
 * @property string $FakturAHMTglAmbil
 * @property string $FakturAHMNilai
 * @property string $RKNo
 * @property string $RKHarga
 * @property string $SKHarga
 * @property string $SDHarga
 * @property string $MMHarga
 * @property string $NoticeNo
 * @property string $NoticeTglJadi
 * @property string $NoticeTglAmbil
 * @property string $NoticePenerima
 * @property string $STNKPenerima
 * @property string $BPKBPenerima
 * @property string $PlatPenerima
 * @property string $SSHarga
 * @property string $SRUTNo
 * @property string $SRUTTglJadi
 * @property string $SRUTTglAmbil
 * @property string $SRUTPenerima
 * @property string $NoticeNilai [decimal(11,2)]
 * @property string $NoticeDenda [decimal(11,2)]
 * @property string $MotorDoc
 */
class Tmotor extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'tmotor';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'MotorAutoN', 'MotorNoMesin' ], 'required' ],
			[ [ 'MotorAutoN' ], 'integer' ],
			[ [ 'MotorTahun', 'FBHarga', 'FakturAHMNilai', 'RKHarga', 'SKHarga', 'SDHarga', 'MMHarga', 'SSHarga', 'MotorDoc' ], 'number' ],
			[
				[
					'BPKBTglJadi',
					'BPKBTglAmbil',
					'STNKTglMasuk',
					'STNKTglBayar',
					'STNKTglJadi',
					'STNKTglAmbil',
					'PlatTgl',
					'PlatTglAmbil',
					'FakturAHMTgl',
					'FakturAHMTglAmbil',
					'NoticeTglJadi',
					'NoticeTglAmbil',
					'SRUTTglJadi',
					'SRUTTglAmbil'
				],
				'safe'
			],
			[ [ 'MotorNoMesin', 'MotorNoRangka' ], 'string', 'max' => 25 ],
			[ [ 'MotorType', 'STNKNama', 'NoticePenerima', 'STNKPenerima', 'BPKBPenerima', 'PlatPenerima', 'SRUTNo', 'SRUTPenerima' ], 'string', 'max' => 50 ],
			[ [ 'MotorWarna' ], 'string', 'max' => 35 ],
			[ [ 'MotorMemo' ], 'string', 'max' => 100 ],
			[ [ 'DealerKode', 'DKNo', 'SKNo', 'PDNo', 'SDNo', 'RKNo' ], 'string', 'max' => 10 ],
			[ [ 'SSNo', 'FBNo' ], 'string', 'max' => 30 ],
			[ [ 'BPKBNo', 'STNKNo', 'FakturAHMNo', 'NoticeNo' ], 'string', 'max' => 20 ],
			[ [ 'STNKAlamat' ], 'string', 'max' => 125 ],
			[ [ 'PlatNo' ], 'string', 'max' => 12 ],
			[ [ 'MotorAutoN', 'MotorNoMesin' ], 'unique', 'targetAttribute' => [ 'MotorAutoN', 'MotorNoMesin' ] ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'MotorAutoN'        => 'Motor Auto N',
			'MotorNoMesin'      => 'Motor No Mesin',
			'MotorNoRangka'     => 'Motor No Rangka',
			'MotorType'         => 'Motor Type',
			'MotorWarna'        => 'Motor Warna',
			'MotorTahun'        => 'Motor Tahun',
			'MotorMemo'         => 'Motor Memo',
			'DealerKode'        => 'Dealer Kode',
			'SSNo'              => 'Ss No',
			'FBNo'              => 'Fb No',
			'FBHarga'           => 'Fb Harga',
			'DKNo'              => 'Dk No',
			'SKNo'              => 'Sk No',
			'PDNo'              => 'Pd No',
			'SDNo'              => 'Sd No',
			'BPKBNo'            => 'Bpkb No',
			'BPKBTglJadi'       => 'Bpkb Tgl Jadi',
			'BPKBTglAmbil'      => 'Bpkb Tgl Ambil',
			'STNKNo'            => 'Stnk No',
			'STNKTglMasuk'      => 'Stnk Tgl Masuk',
			'STNKTglBayar'      => 'Stnk Tgl Bayar',
			'STNKTglJadi'       => 'Stnk Tgl Jadi',
			'STNKTglAmbil'      => 'Stnk Tgl Ambil',
			'STNKNama'          => 'Stnk Nama',
			'STNKAlamat'        => 'Stnk Alamat',
			'PlatNo'            => 'Plat No',
			'PlatTgl'           => 'Plat Tgl',
			'PlatTglAmbil'      => 'Plat Tgl Ambil',
			'FakturAHMNo'       => 'Faktur Ahm No',
			'FakturAHMTgl'      => 'Faktur Ahm Tgl',
			'FakturAHMTglAmbil' => 'Faktur Ahm Tgl Ambil',
			'FakturAHMNilai'    => 'Faktur Ahm Nilai',
			'RKNo'              => 'Rk No',
			'RKHarga'           => 'Rk Harga',
			'SKHarga'           => 'Sk Harga',
			'SDHarga'           => 'Sd Harga',
			'MMHarga'           => 'Mm Harga',
			'NoticeNo'          => 'Notice No',
			'NoticeTglJadi'     => 'Notice Tgl Jadi',
			'NoticeTglAmbil'    => 'Notice Tgl Ambil',
			'NoticePenerima'    => 'Notice Penerima',
			'STNKPenerima'      => 'Stnk Penerima',
			'BPKBPenerima'      => 'Bpkb Penerima',
			'PlatPenerima'      => 'Plat Penerima',
			'SSHarga'           => 'Ss Harga',
			'SRUTNo'            => 'Srut No',
			'SRUTTglJadi'       => 'Srut Tgl Jadi',
			'SRUTTglAmbil'      => 'Srut Tgl Ambil',
			'SRUTPenerima'      => 'Srut Penerima',
			'MotorDoc'          => 'MotorDoc',
		];
	}
	public static function colGrid() {
		return [
			'MotorNoMesin'  => [
				'label' => 'NoMesin',
				'width' => 130,
				'name'  => 'MotorNoMesin'
			],
			'MotorNoRangka' => [
				'label' => 'NoRangka',
				'width' => 130,
				'name'  => 'MotorNoRangka'
			],
			'MotorType'     => [
				'label' => 'Type',
				'width' => 110,
				'name'  => 'MotorType'
			],
			'MotorWarna'    => [
				'label' => 'Warna',
				'width' => 107,
				'name'  => 'MotorWarna'
			],
			'LokasiKode'    => [
				'label' => 'Lokasi',
				'width' => 100,
				'name'  => 'LokasiKode'
			],
			'NoTrans'          => [
				'label' => 'No Trans',
				'width' => 120,
				'name'  => 'NoTrans'
			],
			'Kondisi'       => [
				'label' => 'Kondisi',
				'width' => 110,
				'name'  => 'Kondisi'
			],
			'Jam'       => [
				'label' => 'Tanggal',
				'width' => 110,
				'name'  => 'Jam',
			],
			'CusNama'       => [
				'label' => 'CusNama',
				'width' => 110,
				'name'  => 'CusNama',
				'hidden' => true
			],
			'FakturAHMNo'    => [
				'label'  => 'FakturAHMNo',
				'width'  => 110,
				'name'   => 'FakturAHMNo',
				'hidden' => true
			],
			'PlatNo'    => [
				'label'  => 'PlatNo',
				'width'  => 110,
				'name'   => 'PlatNo',
				'hidden' => true
			],
			'BPKBNo'    => [
				'label'  => 'BPKBNo',
				'width'  => 110,
				'name'   => 'BPKBNo',
				'hidden' => true
			],
			'STNKNo'    => [
				'label'  => 'STNKNo',
				'width'  => 110,
				'name'   => 'STNKNo',
				'hidden' => true
			],
			'SKTgl'    => [
				'label'  => 'SKTgl',
				'width'  => 110,
				'name'   => 'SKTgl',
				'hidden' => true
			],
			'SKNo'    => [
				'label'  => 'SKNo',
				'width'  => 110,
				'name'   => 'SKNo',
				'hidden' => true
			],
			'SDTgl'    => [
				'label'  => 'SDTgl',
				'width'  => 110,
				'name'   => 'SDTgl',
				'hidden' => true
			],
			'SDNo'    => [
				'label'  => 'SDNo',
				'width'  => 110,
				'name'   => 'SDNo',
				'hidden' => true
			],
			'DKTgl'    => [
				'label'  => 'DKTgl',
				'width'  => 110,
				'name'   => 'DKTgl',
				'hidden' => true
			],
			'DKNo'    => [
				'label'  => 'DKNo',
				'width'  => 110,
				'name'   => 'DKNo',
				'hidden' => true
			],
			'INTgl'    => [
				'label'  => 'INTgl',
				'width'  => 110,
				'name'   => 'INTgl',
				'hidden' => true
			],
			'INNo'    => [
				'label'  => 'INNo',
				'width'  => 110,
				'name'   => 'INNo',
				'hidden' => true
			],
			'RKTgl'    => [
				'label'  => 'RKTgl',
				'width'  => 110,
				'name'   => 'RKTgl',
				'hidden' => true
			],
			'RKNo'    => [
				'label'  => 'RKNo',
				'width'  => 110,
				'name'   => 'RKNo',
				'hidden' => true
			],
			'PDTgl'    => [
				'label'  => 'PDTgl',
				'width'  => 110,
				'name'   => 'PDTgl',
				'hidden' => true
			],
			'PDNo'    => [
				'label'  => 'PDNo',
				'width'  => 110,
				'name'   => 'PDNo',
				'hidden' => true
			],
			'FBTgl'    => [
				'label'  => 'FBTgl',
				'width'  => 110,
				'name'   => 'FBTgl',
				'hidden' => true
			],
			'FBNo'    => [
				'label'  => 'FBNo',
				'width'  => 110,
				'name'   => 'FBNo',
				'hidden' => true
			],
			'SSTgl'    => [
				'label'  => 'SSTgl',
				'width'  => 110,
				'name'   => 'SSTgl',
				'hidden' => true
			],
			'SSNo'    => [
				'label'  => 'SSNo',
				'width'  => 110,
				'name'   => 'SSNo',
				'hidden' => true
			],
		];
	}
	public function getFakturBeli() {
		return $this->hasOne( Ttfb::className(), [ 'FBNo' => 'FBNo' ] );
	}
	public function getTerimaBarangSupplier() {
		return $this->hasOne( Ttss::className(), [ 'SSNo' => 'SSNo' ] );
	}
	public function getTerimaBarangDealer() {
		return $this->hasOne( Ttpd::className(), [ 'PDNo' => 'PDNo' ] );
	}
	public function getSuratJalanKonsumen() {
		return $this->hasOne( Ttsk::className(), [ 'SKNo' => 'SKNo' ] );
	}
	public function getMutasiDealerLain() {
		return $this->hasOne( Ttsd::className(), [ 'SDNo' => 'SDNo' ] );
	}
	public function getDataKonsumen() {
		return $this->hasOne( Ttdk::className(), [ 'DKNo' => 'DKNo' ] );
	}
	public function getReturKonsumen() {
		return $this->hasOne( Ttrk::className(), [ 'RKNo' => 'RKNo' ] );
	}
	public function getKonsumen() {
		return $this->hasOne( Tdcustomer::className(), [ 'CusKode' => 'CusKode' ] );
	}
	public function getInden() {
		return $this->hasOne( Ttin::className(), [ 'INNo' => 'INNo' ] );
	}
	/**
	 * {@inheritdoc}
	 * @return TmotorQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TmotorQuery( get_called_class() );
	}
}
//
//"SELECT " + _
//      "tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType, tmotor.MotorWarna, " + _
//      "tmotor.MotorTahun, tmotor.MotorMemo, tmotor.DealerKode, tmotor.SSNo, tmotor.FBNo, tmotor.FBHarga, " + _
//      "tmotor.DKNo, tmotor.SKNo, tmotor.PDNo, tmotor.SDNo, tmotor.BPKBNo, tmotor.BPKBTglJadi, tmotor.BPKBTglAmbil, " + _
//      "tmotor.STNKNo, tmotor.STNKTglMasuk, tmotor.STNKTglBayar, tmotor.STNKTglJadi, tmotor.STNKTglAmbil, tmotor.STNKNama, " + _
//      "tmotor.STNKAlamat, tmotor.PlatNo, tmotor.PlatTgl, tmotor.PlatTglAmbil, tmotor.FakturAHMNo, tmotor.FakturAHMTgl, tmotor.FakturAHMTglAmbil, " + _
//      "LokasiMotor.LokasiKode, LokasiMotor.NoTrans, LokasiMotor.Jam, LokasiMotor.Kondisi, " + _
//      "IFNULL(ttdk.DKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS DKTgl, IFNULL(ttss.SSTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SSTgl, IFNULL(ttfb.FBTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS FBTgl, " + _
//      "IFNULL(ttpd.PDTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS PDTgl, IFNULL(ttsd.SDTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SDTgl, IFNULL(ttsk.SKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SKTgl, " + _
//      "IFNULL(tdcustomer.CusNama, '--') AS CusNama, tmotor.RKNo, IFNULL(ttrk.RKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS RKTgl, IFNULL(ttin.INNo, '--') AS INNo, IFNULL(ttin.INTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS INTgl  " + _
//      "FROM tmotor " + _
//      "INNER JOIN " + _
//      "(SELECT MotorAutoN, MotorNoMesin, LokasiKode, NoTrans, Jam, Kondisi " + _
//      " FROM vmotorlastlokasi " + _
//      " WHERE vmotorlastlokasi.Kondisi LIKE '" & cboKondisi.SelectedValue & "%') LokasiMotor " + _
//      "ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN " + _
//      "LEFT OUTER JOIN ttss ON tmotor.SSNo = ttss.SSNo " + _
//      "LEFT OUTER JOIN ttfb ON tmotor.FBNo = ttfb.FBNo " + _
//      "LEFT OUTER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo " + _
//      "LEFT OUTER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo " + _
//      "LEFT OUTER JOIN ttsd ON tmotor.SDNo = ttsd.SDNo " + _
//      "LEFT OUTER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo " + _
//      "LEFT OUTER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo " + _
//      "LEFT OUTER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode " + _
//      "LEFT OUTER JOIN ttin ON ttdk.INNo = ttin.INNo "