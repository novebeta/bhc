<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "ttbkit".
 *
 * @property string $BKNo
 * @property string $FBNo
 * @property string $BKBayar
 */
class Ttbkit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttbkit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BKNo', 'FBNo'], 'required'],
            [['BKBayar'], 'number'],
            [['BKNo'], 'string', 'max' => 12],
            [['FBNo'], 'string', 'max' => 18],
            [['BKNo', 'FBNo'], 'unique', 'targetAttribute' => ['BKNo', 'FBNo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'BKNo' => 'Bk No',
            'FBNo' => 'Fb No',
            'BKBayar' => 'Bk Bayar',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtbkitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtbkitQuery(get_called_class());
    }


	public static function generateBTNo($BankKode,  $temporary = false ) {
		$sign    = $temporary ? '=' : '-';
		$kode    = 'BT';
		$kBank = substr($BankKode, -2);
		$yNow    = date( "y" );
		$maxNoGl = ( new \yii\db\Query() )
			->from( self::tableName() )
			->where( "FBNo LIKE '$kode$kBank$yNow$sign%'" )
			->max( 'FBNo' );
		if ( $maxNoGl && preg_match( "/$kode\d{4}$sign(\d+)/", $maxNoGl, $result ) ) {
			[ $all, $counter ] = $result;
			$digitCount  = strlen( $counter );
			$val         = (int) $counter;
			$nextCounter = sprintf( '%0' . $digitCount . 'd', ++ $val );
		} else {
			$nextCounter = "00001";
		}
		return "$kode$kBank$yNow$sign$nextCounter";
	}
}
