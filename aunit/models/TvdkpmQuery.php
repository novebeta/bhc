<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Tvdkpm]].
 *
 * @see Tvdkpm
 */
class TvdkpmQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tvdkpm[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tvdkpm|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
