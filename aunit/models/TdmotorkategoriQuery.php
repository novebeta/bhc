<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Tdmotorkategori]].
 *
 * @see Tdmotorkategori
 */
class TdmotorkategoriQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdmotorkategori[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdmotorkategori|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
