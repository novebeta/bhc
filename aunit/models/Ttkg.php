<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "ttkg".
 *
 * @property string $KGNo
 * @property string|null $KGTgl
 * @property string|null $DKNo
 * @property string|null $KGJenis UMPK, UMDP, UMIN
 * @property float|null $KGNominal
 * @property string|null $KGPengirim
 * @property string|null $KGPenerima
 * @property string|null $KGMemo
 * @property string|null $KGStatus
 * @property string|null $UserID
 */
class Ttkg extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttkg';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KGNo'], 'required'],
            [['KGTgl'], 'safe'],
            [['KGNominal'], 'number'],
            [['KGMemo'], 'string'],
            [['KGNo', 'DKNo'], 'string', 'max' => 10],
            [['KGJenis'], 'string', 'max' => 4],
            [['KGPengirim', 'KGPenerima'], 'string', 'max' => 50],
            [['KGStatus'], 'string', 'max' => 5],
            [['UserID'], 'string', 'max' => 15],
            [['KGNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'KGNo' => 'Kg No',
            'KGTgl' => 'Kg Tgl',
            'DKNo' => 'Dk No',
            'KGJenis' => 'Kg Jenis',
            'KGNominal' => 'Kg Nominal',
            'KGPengirim' => 'Kg Pengirim',
            'KGPenerima' => 'Kg Penerima',
            'KGMemo' => 'Kg Memo',
            'KGStatus' => 'Kg Status',
            'UserID' => 'User ID',
        ];
    }

    public static function colGrid() {
		return [
			'ttkg.KGNo'             => [ 'width' => 80, 'label' => 'KGNo', 'name' => 'KGNo' ],
			'ttkg.KGTgl'            => [ 'width' => 70, 'label' => 'KGTgl', 'name' => 'KGTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'tdcustomer.CusNama'    => [ 'label' => 'CusNama', 'width' => 190, 'name' => 'CusNama' ],
			'ttkg.DKNo'             => [ 'width' => 80, 'label' => 'DKNo', 'name' => 'DKNo' ],
			'ttdk.DKTgl'            => [ 'width' => 70, 'label' => 'DKTgl', 'name' => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'ttkg.KGJenis'             => [ 'width' => 80, 'label' => 'KGJenis', 'name' => 'KGJenis' ],
			'ttkg.KGNominal'          => [ 'label' => 'KGNominal', 'width' => 80, 'name' => 'KGNominal', 'formatter' => 'number', 'align' => 'right' ],
			'ttkg.KGPengirim'   => [ 'label' => 'KGPengirim', 'width' => 85, 'name' => 'KGPengirim' ],
			'ttkg.KGPenerima'   => [ 'label' => 'KGPenerima', 'width' => 85, 'name' => 'KGPenerima' ],
			'ttkg.KGMemo'           => [ 'label' => 'KGMemo', 'width' => 85, 'name' => 'KGMemo' ],
			'ttkg.KGStatus'           => [ 'label' => 'KGStatus', 'width' => 85, 'name' => 'KGStatus' ],
			'ttkg.UserID'           => [ 'label' => 'UserID', 'width' => 85, 'name' => 'UserID' ],
			"CONCAT(tdcustomer.CusAlamat, 'RT/RW : ', tdcustomer.CusRT, '/', tdcustomer.CusRW)"      => [ 'label' => 'CusAlamat', 'width' => 85, 'name' => 'CusAlamat' ],
			"CONCAT(tdcustomer.CusKelurahan, ', ', tdcustomer.CusKecamatan, ', ', tdcustomer.CusKabupaten, ', ', tdcustomer.CusProvinsi)"      => [ 'label' => 'CusArea', 'width' => 85, 'name' => 'CusArea' ],
			"CONCAT(tdmotortype.MotorNama, ' - ', tmotor.MotorWarna, ' - ', tdmotortype.MotorCC, 'CC', ' - ', tmotor.MotorTahun)"      => [ 'label' => 'TypeMotor', 'width' => 85, 'name' => 'TypeMotor' ],
			"CONCAT(tmotor.MotorType, ' - ', tmotor.MotorNoMesin, ' - ', tmotor.MotorNoRangka)"      => [ 'label' => 'NoSinNoKa', 'width' => 85, 'name' => 'NoSinNoKa' ],
		];
	}
    public static function colGridSelect() {
		return [
			'DKNo'             => [ 'width' => 80, 'label' => 'DKNo', 'name' => 'DKNo' ],
			'DKTgl'            => [ 'width' => 70, 'label' => 'DKTgl', 'name' => 'DKTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'CusNama'    => [ 'label' => 'CusNama', 'width' => 190, 'name' => 'CusNama' ],
			"MotorNama"      => [ 'label' => 'MotorNama', 'width' => 85, 'name' => 'MotorNama' ],
			'Jenis'             => [ 'width' => 80, 'label' => 'Jenis', 'name' => 'Jenis' ],
			'Nominal'          => [ 'label' => 'Nominal', 'width' => 80, 'name' => 'Nominal', 'formatter' => 'number', 'align' => 'right' ],
			'KGNo'             => [ 'width' => 80, 'label' => 'KGNo', 'name' => 'KGNo' ],
			'KGJenis'             => [ 'width' => 80, 'label' => 'KGJenis', 'name' => 'KGJenis' ],
		];
	}

    /**
     * {@inheritdoc}
     * @return TtkgQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtkgQuery(get_called_class());
    }
}
