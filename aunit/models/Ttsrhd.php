<?php
namespace aunit\models;
use common\models\User;
use Yii;
/**
 * This is the model class for table "ttsrhd".
 *
 * @property string $SRNo
 * @property string $SRTgl
 * @property string $SINo
 * @property string $KodeTrans
 * @property string $LokasiKode
 * @property string $SRSubTotal
 * @property string $PosKode
 * @property string $KarKode
 * @property string $MotorNoPolisi
 * @property string $SRDiscFinal
 * @property string $SRTotalPajak
 * @property string $SRBiayaKirim
 * @property string $SRTotal
 * @property string $SRTerbayar
 * @property string $SRKeterangan
 * @property string $NoGL
 * @property string $Cetak
 * @property string $UserID
 */
class Ttsrhd extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'twsrhd';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'SRNo' ], 'required' ],
			[ [ 'SRTgl' ], 'safe' ],
			[ [ 'SRSubTotal', 'SRDiscFinal', 'SRTotalPajak', 'SRBiayaKirim', 'SRTotal', 'SRTerbayar' ], 'number' ],
			[ [ 'SRNo', 'SINo', 'KarKode', 'NoGL' ], 'string', 'max' => 10 ],
			[ [ 'KodeTrans' ], 'string', 'max' => 4 ],
			[ [ 'LokasiKode', 'UserID' ], 'string', 'max' => 15 ],
			[ [ 'PosKode' ], 'string', 'max' => 20 ],
			[ [ 'MotorNoPolisi' ], 'string', 'max' => 12 ],
			[ [ 'SRKeterangan' ], 'string', 'max' => 150 ],
			[ [ 'Cetak' ], 'string', 'max' => 5 ],
			[ [ 'SRNo' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'SRNo'          => 'Sr No',
			'SRTgl'         => 'Sr Tgl',
			'SINo'          => 'Si No',
			'KodeTrans'     => 'Kode Trans',
			'LokasiKode'    => 'Lokasi Kode',
			'SRSubTotal'    => 'Sr Sub Total',
			'PosKode'       => 'Pos Kode',
			'KarKode'       => 'Kar Kode',
			'MotorNoPolisi' => 'Motor No Polisi',
			'SRDiscFinal'   => 'Sr Disc Final',
			'SRTotalPajak'  => 'Sr Total Pajak',
			'SRBiayaKirim'  => 'Sr Biaya Kirim',
			'SRTotal'       => 'Sr Total',
			'SRTerbayar'    => 'Sr Terbayar',
			'SRKeterangan'  => 'Sr Keterangan',
			'NoGL'          => 'No Gl',
			'Cetak'         => 'Cetak',
			'UserID'        => 'User ID',
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public static function colGrid() {
		$col  = [
            'SRNo'                                => ['width' => 90,'label' => 'No SR','name'  => 'SRNo'],
            'SRTgl'                               => ['width' => 115,'label' => 'Tgl SR','name'  => 'SRTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y H:i:s']],
//			'Ttsrhd.MotorNoPolisi'                       => ['width' => 90,'label' => 'No Polisi','name'  => 'MotorNoPolisi'],
//			'CusNama'                             => ['width' => 150,'label' => 'Konsumen','name'  => 'CusNama'],
//			'MotorNama'                           => ['width' => 210,'label' => 'Nama Motor','name'  => 'MotorNama'],
			'KarKode'                             => ['width' => 90,'label' => 'Karyawan','name'  => 'KarKode'],
            'SRTotal'                             => ['width' => 110,'label' => 'Total','name'  => 'SRTotal', 'formatter' => 'number', 'align' => 'right'],
		];
		$user = User::findOne( Yii::$app->user->id );
		if ( $user->KodeAkses != "WareHouse" && $user->KodeAkses != "Gudang" ) {
			$col['IF(SRTotal-SRTerbayar = 0,"Lunas","Belum") AS SRStatus'] = [
				'label' => 'Status',
				'width' => 85,
				'name'  => 'SRStatus'
			];
		}
		$col['NoGL'] = ['label' => 'NoGL','width' => 70,'name'  => 'NoGL'];
		$col['Cetak'] = ['label' => 'Cetak','width' => 50,'name'  => 'Cetak'];
		$col['UserID'] = ['label' => 'UserID','width' => 80,'name'  => 'UserID'];
		$col['SRTerbayar'] = ['label' => 'Terbayar','width' => 110,'name'  => 'SRTerbayar', 'formatter' => 'number', 'align' => 'right'];
		$col['SRKeterangan'] = ['label' => 'Keterangan','width' => 300,'name'  => 'SRKeterangan'];
		return $col;
	}
	public function getCustomer() {
//		return $this->hasOne( Tdcustomer::className(), [ 'MotorNoPolisi' => 'MotorNoPolisi' ] );
	}
		public function getDetil() {
		return $this->hasOne( Ttsrit::className(), [ 'SRNo' => 'SRNo' ] );
	}
	/**
	 * {@inheritdoc}
	 * @return TtsrhdQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TtsrhdQuery( get_called_class() );
	}
}
