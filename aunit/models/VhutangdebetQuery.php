<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Vhutangdebet]].
 *
 * @see Vhutangdebet
 */
class VhutangdebetQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Vhutangdebet[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Vhutangdebet|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
