<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "vpiutangbayar".
 *
 * @property string $HPLink
 * @property string $NoAccount
 * @property string $SumKreditGL
 */
class Vpiutangbayar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vpiutangbayar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['HPLink', 'NoAccount'], 'required'],
            [['SumKreditGL'], 'number'],
            [['HPLink'], 'string', 'max' => 18],
            [['NoAccount'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'HPLink' => 'Hp Link',
            'NoAccount' => 'No Account',
            'SumKreditGL' => 'Sum Kredit Gl',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VpiutangbayarQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VpiutangbayarQuery(get_called_class());
    }
}
