<?php
namespace aunit\models;
use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttfb]].
 *
 * @see Ttfb
 */
class TtfbQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttfb[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttfb|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function dsTBeliFillByNo() {
		return $this->select( new Expression( "ttfb.FBNo, ttfb.FBTgl, ttfb.SSNo, ttfb.SupKode, ttfb.FBTermin, ttfb.FBTglTempo, 
		ttfb.FBPSS, ttfb.FBPtgLain, ttfb.FBTotal, ttfb.FBMemo, ttfb.FBLunas, ttfb.UserID, IFNULL(tdsupplier.SupNama, '--') AS SupNama, 
		IFNULL(ttss.SSTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SSTgl, IFNULL(tmotor.FBJum, 0) AS FBJum, 
		IFNULL(tmotor.FBSubTotal, 0) AS FBSubTotal, ttfb.FBPSS + ttfb.FBPtgLain + ttfb.FBExtraDisc AS FBPtgTotal, ttfb.NoGL, ttfb.FPNo, 
                         ttfb.FPTgl, ttfb.DOTgl, ttfb.FBExtraDisc, ttfb.FBPosting, ttfb.FBPtgMD,ttfb.FBPPNm, ttfb.FBJam" ) )
		            ->join( "LEFT OUTER JOIN", "ttss", "ttfb.SSNo = ttss.SSNo" )
		            ->join( "LEFT OUTER JOIN", "tdsupplier", "ttfb.SupKode = tdsupplier.SupKode" )
		            ->join( "LEFT OUTER JOIN", [ 'tmotor' => new Expression( "(SELECT        COUNT(MotorType) AS FBJum, SUM(FBHarga) AS FBSubTotal, FBNo
                               FROM            tmotor tmotor_1
                               GROUP BY FBNo)" ) ], "ttfb.FBNo = tmotor.FBNo" );
	}
	public function CekNomorMotor( $MyMesinNo ) {
		try {
			return \Yii::$app->db->createCommand( "SELECT IFNULL(MAX(MotorAutoN),0) AS MyCount FROM tmotor 
							WHERE  MotorNoMesin = :MotorNoMesin AND PDNo = '--' AND RKNo = '--';", [
				':MotorNoMesin' => $MyMesinNo
			] )->queryScalar();
		} catch ( Exception $e ) {
			return null;
		}
	}
	public function UpdateHargaBeli( $FBNo, $Tgl ) {
		try {
			\Yii::$app->db->createCommand( "UPDATE tdmotortype JOIN (SELECT MotorType, MAX(FBHarga) AS FBHarga 
							FROM tmotor WHERE FBNo = :FBNo GROUP BY MotorType) FB 
							ON tdmotortype.MotorType = FB.MotorType
							SET tdmotortype.MotorHrgBeli = FB.FBHarga , 
							tdmotortype.TglUpdate = :Tgl 
							WHERE tdmotortype.TglUpdate <=  :Tgl", [
				':FBNo' => $FBNo,
				':Tgl'  => $Tgl,
			] )->execute();
		} catch ( Exception $e ) {
		}
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]      = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]   = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]  = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'FBNo' ]        = $post[ 'FBNo' ] ?? '--';
		$post[ 'FBNoBaru' ]    = $post[ 'FBNoView' ] ?? '--';
		$post[ 'FBTgl' ]       = date('Y-m-d', strtotime($post[ 'FBTgl' ] ?? date( 'Y-m-d' )));
		$post[ 'FBJam' ]       = date('Y-m-d H:i:s', strtotime($post[ 'SSJam' ] ?? date( 'Y-m-d H:i:s' )));
		$post[ 'SupKode' ]     = $post[ 'SupKode' ] ?? '--';
		$post[ 'SSNo' ]        = $post[ 'SSNo' ] ?? '--';
		$post[ 'SSTgl' ]       = date('Y-m-d', strtotime($post[ 'SSTgl' ] ?? date( 'Y-m-d' )));
		$post[ 'FBTermin' ]    = floatval( $post[ 'FBTermin' ] ?? 0 );
		$post[ 'FBTglTempo' ]  = date('Y-m-d', strtotime($post[ 'FBTglTempo' ] ?? date( 'Y-m-d' )));
		$post[ 'DONo' ]        = $post[ 'DONo' ] ?? '--';
		$post[ 'DOTgl' ]       = date('Y-m-d', strtotime($post[ 'DOTgl' ] ?? date( 'Y-m-d' )));
		$post[ 'FPNo' ]        = $post[ 'FPNo' ] ?? '--';
		$post[ 'FPTgl' ]       = date('Y-m-d', strtotime($post[ 'FPTgl' ] ?? date( 'Y-m-d' )));
		$post[ 'FBPtgMD' ]     = floatval( $post[ 'FBPtgMD' ] ?? 0 );
		$post[ 'FBSubTotal' ]  = floatval( $post[ 'FBSubTotal' ] ?? 0 );
		$post[ 'FBLunas' ]     = $post[ 'FBLunas' ] ?? '--';
		$post[ 'FBPtgLain' ]   = floatval( $post[ 'FBPtgLain' ] ?? 0 );
		$post[ 'FBPtgTotal' ]  = floatval( $post[ 'FBPtgTotal' ] ?? 0 );
		$post[ 'FBPosting' ]   = $post[ 'FBPosting' ] ?? '--';
		$post[ 'FBExtraDisc' ] = floatval( $post[ 'FBExtraDisc' ] ?? 0 );
		$post[ 'FBTotal' ]     = floatval( $post[ 'FBTotal' ] ?? 0 );
		$post[ 'FBMemo' ]      = $post[ 'FBMemo' ] ?? '--';
		$post[ 'NoGL' ]        = $post[ 'NoGL' ] ?? '--';
		$post[ 'Cetak' ]       = $post[ 'Cetak' ] ?? '--';
		$post[ 'FBPSS' ]       = floatval( $post[ 'FBPSS' ] ?? 0 );
		$post[ 'FBPPNm' ]       = floatval( $post[ 'FBPPNm' ] ?? 0 );
		try {
		General::cCmd( "SET @FBNo = ?;" )->bindParam( 1, $post[ 'FBNo' ] )->execute();
		General::cCmd( "CALL ffb(?,@Status,@Keterangan,?,?,?,@FBNo,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
		       ->bindParam( 1, $post[ 'Action' ] )
		       ->bindParam( 2, $post[ 'UserID' ] )
		       ->bindParam( 3, $post[ 'KodeAkses' ] )
		       ->bindParam( 4, $post[ 'LokasiKode' ] )
		       ->bindParam( 5, $post[ 'FBNoBaru' ] )
		       ->bindParam( 6, $post[ 'FBTgl' ] )
		       ->bindParam( 7, $post[ 'FBJam' ] )
		       ->bindParam( 8, $post[ 'SupKode' ] )
		       ->bindParam( 9, $post[ 'SSNo' ] )
		       ->bindParam( 10, $post[ 'SSTgl' ] )
		       ->bindParam( 11, $post[ 'FBTermin' ] )
		       ->bindParam( 12, $post[ 'FBTglTempo' ] )
		       ->bindParam( 13, $post[ 'DONo' ] )
		       ->bindParam( 14, $post[ 'DOTgl' ] )
		       ->bindParam( 15, $post[ 'FPNo' ] )
		       ->bindParam( 16, $post[ 'FPTgl' ] )
		       ->bindParam( 17, $post[ 'FBPtgMD' ] )
		       ->bindParam( 18, $post[ 'FBSubTotal' ] )
		       ->bindParam( 19, $post[ 'FBLunas' ] )
		       ->bindParam( 20, $post[ 'FBPtgLain' ] )
		       ->bindParam( 21, $post[ 'FBPtgTotal' ] )
		       ->bindParam( 22, $post[ 'FBPosting' ] )
		       ->bindParam( 23, $post[ 'FBExtraDisc' ] )
		       ->bindParam( 24, $post[ 'FBTotal' ] )
		       ->bindParam( 25, $post[ 'FBMemo' ] )
		       ->bindParam( 26, $post[ 'NoGL' ] )
		       ->bindParam( 27, $post[ 'Cetak' ] )
		       ->bindParam( 28, $post[ 'FBPSS' ] )
		       ->bindParam( 29, $post[ 'FBPPNm' ] )
		       ->execute();
		$exe = General::cCmd( "SELECT @FBNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'FBNo'       => $post[ 'FBNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$FBNo = $exe[ '@FBNo' ] ?? $post['FBNo'];
		$status = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : ['.$post['Action'].'] Keterangan NULL';
		return [
			'FBNo'       => $FBNo,
			'status'     => $status,
			'keterangan' => str_replace(" ","&nbsp;",$keterangan)
		];
	}
}
