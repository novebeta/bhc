<?php
namespace aunit\models;
/**
 * This is the model class for table "ttsihd".
 *
 * @property string $SINo
 * @property string $SITgl
 * @property string $SONo
 * @property string $SDNo
 * @property string $SIJenis
 * @property string $PosKode
 * @property string $KodeTrans
 * @property string $KarKode
 * @property string $PrgNama
 * @property string $MotorNoPolisi
 * @property string $LokasiKode
 * @property string $DealerKode
 * @property string $SISubTotal
 * @property string $SIDiscFinal
 * @property string $SITotalPajak
 * @property string $SIBiayaKirim
 * @property string $SIUangMuka
 * @property string $SITotal
 * @property string $SITOP
 * @property string $SITerbayar
 * @property string $SIKeterangan
 * @property string $NoGL
 * @property string $Cetak
 * @property string $UserID
 */
class Ttsihd extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'twsihd';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'SINo' ], 'required' ],
			[ [ 'SITgl' ], 'safe' ],
			[ [ 'SISubTotal', 'SIDiscFinal', 'SITotalPajak', 'SIBiayaKirim', 'SIUangMuka', 'SITotal', 'SITOP', 'SITerbayar' ], 'number' ],
			[ [ 'SINo', 'SONo', 'KarKode', 'DealerKode', 'NoGL' ], 'string', 'max' => 10 ],
			[ [ 'SDNo', 'PrgNama' ], 'string', 'max' => 25 ],
			[ [ 'SIJenis' ], 'string', 'max' => 6 ],
			[ [ 'PosKode' ], 'string', 'max' => 20 ],
			[ [ 'KodeTrans' ], 'string', 'max' => 4 ],
			[ [ 'MotorNoPolisi' ], 'string', 'max' => 12 ],
			[ [ 'LokasiKode', 'UserID' ], 'string', 'max' => 15 ],
			[ [ 'SIKeterangan' ], 'string', 'max' => 150 ],
			[ [ 'Cetak' ], 'string', 'max' => 5 ],
			[ [ 'SINo' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'SINo'          => 'Si No',
			'SITgl'         => 'Si Tgl',
			'SONo'          => 'So No',
			'SDNo'          => 'Sd No',
			'SIJenis'       => 'Si Jenis',
			'PosKode'       => 'Pos Kode',
			'KodeTrans'     => 'Kode Trans',
			'KarKode'       => 'Kar Kode',
			'PrgNama'       => 'Prg Nama',
			'MotorNoPolisi' => 'Motor No Polisi',
			'LokasiKode'    => 'Lokasi Kode',
			'DealerKode'    => 'Dealer Kode',
			'SISubTotal'    => 'Si Sub Total',
			'SIDiscFinal'   => 'Si Disc Final',
			'SITotalPajak'  => 'Si Total Pajak',
			'SIBiayaKirim'  => 'Si Biaya Kirim',
			'SIUangMuka'    => 'Si Uang Muka',
			'SITotal'       => 'Si Total',
			'SITOP'         => 'Sitop',
			'SITerbayar'    => 'Si Terbayar',
			'SIKeterangan'  => 'Si Keterangan',
			'NoGL'          => 'No Gl',
			'Cetak'         => 'Cetak',
			'UserID'        => 'User ID',
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public static function colGrid() {
		$col  = [
            'SINo'                                => ['width' => 90,'label' => 'No SI','name'  => 'SINo'],
            'SITgl'                               => ['width' => 115,'label' => 'Tgl SI','name'  => 'SITgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y H:i:s']],
			'MotorNoPolisi'                       => ['width' => 90,'label' => 'No Polisi','name'  => 'MotorNoPolisi'],
			'CusNama'                             => ['width' => 150,'label' => 'Konsumen','name'  => 'CusNama'],
			'MotorNama'                           => ['width' => 210,'label' => 'Nama Motor','name'  => 'MotorNama'],
			'KarKode'                             => ['width' => 95,'label' => 'Karyawan','name'  => 'KarKode'],
            'SITotal'                             => ['width' => 110,'label' => 'Total','name'  => 'SITotal', 'formatter' => 'number', 'align' => 'right'],
		];
		if ( \aunit\components\Menu::showOnlyHakAkses(['warehouse','gudang']) ) {
			$col['IF(SITotal-SITerbayar = 0,"Lunas","Belum") AS SIStatus'] = ['label' => 'Status','width' => 80,'name'  => 'SIStatus'];
			$col['PLCetak'] = ['label' => 'PLCetak','width' => 50,'name'  => 'PLCetak'];
		}
		$col['NoGL'] = ['label' => 'NoGL','width' => 70,'name'  => 'NoGL'];
		$col['Cetak'] = ['label' => 'Cetak','width' => 50,'name'  => 'Cetak'];
		$col['UserID'] = ['label' => 'UserID','width' => 80,'name'  => 'UserID'];
		$col['SITerbayar'] = ['label' => 'Terbayar','width' => 110,'name'  => 'SITerbayar', 'formatter' => 'number', 'align' => 'right'];
		$col['PosKode'] = ['label' => 'Pos','width' => 90,'name'  => 'PosKode'];
		$col['SIKeterangan'] = ['label' => 'Keterangan','width' => 300,'name'  => 'SIKeterangan'];

		return $col;
	}
	public function getCustomer() {
//		return $this->hasOne( Tdcustomer::className(), [ 'MotorNoPolisi' => 'MotorNoPolisi' ] );
	}
	public function getDetil() {
		return $this->hasOne( Ttsiit::className(), [ 'SINo' => 'SINo' ] );
	}
	/**
	 * {@inheritdoc}
	 * @return TtsihdQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TtsihdQuery( get_called_class() );
	}

    public static function getRoute( $jenis, $param = []) {
        $index  = [];
        $update = [];
        $create = [];
        switch ( $jenis ) {
            case 'TC12' :
            case 'TC17' :
            case 'TC20' :
            case 'TC21' :
            case 'TC22' :
                $index = [ 'ttsihd/invoice-penjualan' ];
                $update = [ 'ttsihd/invoice-penjualan-update' ];
                $create = [ 'ttsihd/invoice-penjualan-create' ];
                break;
            case 'TC15' :
            case 'TC99' :
            case 'TC18' :
                $index = [ 'ttsihd/penjualan-internal' ];
                $update = [ 'ttsihd/penjualan-internal-update' ];
                $create = [ 'ttsihd/penjualan-internal-create' ];
                break;
        }
        return [
            'index'  => \yii\helpers\Url::toRoute( array_merge( $index, $param ) ),
            'update' => \yii\helpers\Url::toRoute( array_merge( $update, $param ) ),
            'create' => \yii\helpers\Url::toRoute( array_merge( $create, $param ) ),
        ];
    }

    public static function colGridPick()
    {
        return [
            'SINo'                                => ['width' => 90,'label' => 'No SI','name'  => 'SINo'],
            'SITgl'                               => ['width' => 115,'label' => 'Tgl SI','name'  => 'SITgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y H:i:s']],
            'MotorNoPolisi'                       => ['width' => 90,'label' => 'No Polisi','name'  => 'MotorNoPolisi'],
            'CusNama'                             => ['width' => 150,'label' => 'Nama Konsumen','name'  => 'CusNama'],
            'LokasiKode'                          => ['width' => 150,'label' => 'Lokasi','name'  => 'LokasiKode'],
            'KodeTrans'                           => ['width' => 150,'label' => 'Kode','name'  => 'CusNama'],
            'SIKeterangan'                        => ['width' => 150,'label' => 'Keterangan','name'  => 'KodeTrans'],
            'KarKode'                             => ['width' => 150,'label' => 'Karyawan','name'  => 'KarKode'],
            'SITerbayar'                          => ['width' => 110,'label' => 'Uang Muka','name'  => 'SITerbayar', 'formatter' => 'number', 'align' => 'right'],
        ];
    }

}
