<?php
namespace aunit\models;
/**
 * This is the model class for table "tsaldomotor".
 *
 * @property string $NoTrans
 * @property string $TglTrans
 * @property int $MotorAutoN
 * @property string $MotorNoMesin
 * @property string $MotorWarna
 * @property string $MotorTahun
 * @property string $MotorNoRangka
 * @property string $MotorType
 * @property string $MotorHarga
 */
class Tsaldomotor extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'tsaldomotor';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'NoTrans', 'TglTrans', 'MotorAutoN', 'MotorNoMesin' ], 'required' ],
			[ [ 'TglTrans' ], 'safe' ],
			[ [ 'MotorAutoN' ], 'integer' ],
			[ [ 'MotorTahun', 'MotorHarga' ], 'number' ],
			[ [ 'NoTrans' ], 'string', 'max' => 10 ],
			[ [ 'MotorNoMesin', 'MotorNoRangka', 'MotorType' ], 'string', 'max' => 25 ],
			[ [ 'MotorWarna' ], 'string', 'max' => 35 ],
			[ [ 'NoTrans', 'TglTrans', 'MotorAutoN', 'MotorNoMesin' ], 'unique', 'targetAttribute' => [ 'NoTrans', 'TglTrans', 'MotorAutoN', 'MotorNoMesin' ] ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'NoTrans'       => 'No Trans',
			'TglTrans'      => 'Tgl Trans',
			'MotorAutoN'    => 'Motor Auto N',
			'MotorNoMesin'  => 'Motor No Mesin',
			'MotorWarna'    => 'Motor Warna',
			'MotorTahun'    => 'Motor Tahun',
			'MotorNoRangka' => 'Motor No Rangka',
			'MotorType'     => 'Motor Type',
			'MotorHarga'    => 'Motor Harga',
		];
	}
	public static function colGrid() {
		return [
            'NoTrans'          =>[
                'label' => 'No',
                'width' => 120,
                'name'  => 'NoTrans'
            ],
            'TglTrans'          =>[
                'label' => 'Tgl',
                'width' => 120,
                'name'  => 'TglTrans'
            ],


//			'NoTrans'                       => 'NoTrans',
//			'TglTrans'                      => 'Tgl Trans',
//			'SUM(MotorHarga) As MotorHarga' => [
//				'label' => 'MotorHarga',
//				'width' => 105,
//				'name'  => 'MotorHarga'
//			],
		];
	}

    /**
     * @param bool $temporary
     * @return mixed|string
     */
    public static function generateReference($temporary = false) {
        $kode = 'SM';
        $yNow = date("y");
        $maxNo = (new \yii\db\Query())
            ->from(self::tableName())
            ->where("NoTrans LIKE '$kode$yNow-%'")
            ->max('NoTrans');

        if($maxNo && preg_match("/$kode\d{2}-(\d+)/", $maxNo, $result)) {
            list($all, $counter) = $result;
            $digitCount = strlen($counter);
            $val = (int)$counter;
            $nextCounter = sprintf('%0'.$digitCount.'d', ++$val);
        } else {
            $nextCounter = "00001";
        }

        $nextRef = "$kode$yNow-$nextCounter";

        return $temporary ? str_replace('-', '=', $nextRef) : $nextRef;
    }
	/**
	 * {@inheritdoc}
	 * @return TsaldomotorQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TsaldomotorQuery( get_called_class() );
	}
}
