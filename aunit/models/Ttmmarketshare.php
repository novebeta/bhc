<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "ttmmarketshare".
 *
 * @property string $Tanggal
 * @property string $Provinsi
 * @property string $Kabupaten
 * @property string $Kecamatan
 * @property string $Honda
 * @property string $Semua
 */
class Ttmmarketshare extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttmmarketshare';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Tanggal', 'Provinsi', 'Kabupaten', 'Kecamatan'], 'required'],
            [['Tanggal'], 'safe'],
            [['Honda', 'Semua'], 'number'],
            [['Provinsi', 'Kabupaten', 'Kecamatan'], 'string', 'max' => 50],
            [['Tanggal', 'Provinsi', 'Kabupaten', 'Kecamatan'], 'unique', 'targetAttribute' => ['Tanggal', 'Provinsi', 'Kabupaten', 'Kecamatan']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Tanggal' => 'Tanggal',
            'Provinsi' => 'Provinsi',
            'Kabupaten' => 'Kabupaten',
            'Kecamatan' => 'Kecamatan',
            'Honda' => 'Honda',
            'Semua' => 'Semua',
        ];
    }


	/**
	 * {@inheritdoc}
	 * SELECT        Tanggal, Provinsi, Kabupaten, Kecamatan, Honda, Semua
	FROM            ttmmarketshare
	 */
	public static function colGrid() {
		return [
			'Tanggal'         => ['width' => 80,'label' => 'Tanggal','name' => 'Tanggal', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y'],
			                      'Provinsi'        => ['width' => 70,'label' => 'Provinsi','name' => 'Provinsi']],
			'Kabupaten'        => ['width' => 50,'label' => 'Kabupaten','name' => 'Kabupaten'],
			'Kecamatan'   => ['width' => 90,'label' => 'Kecamatan','name'  => 'Kecamatan'],
			'Honda'   => ['width' => 90,'label' => 'Honda','name'  => 'Honda'],
			'Semua'=> ['width' => 125,'label' => 'Semua','name'  => 'Semua'],
		];
	}

    /**
     * {@inheritdoc}
     * @return TtmmarketshareQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtmmarketshareQuery(get_called_class());
    }
}
