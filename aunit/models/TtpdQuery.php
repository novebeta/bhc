<?php
namespace aunit\models;
use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttpd]].
 *
 * @see Ttpd
 */
class TtpdQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttpd[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttpd|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function dsTMutasiTtpd() {
		return $this->select( new Expression( " ttpd.PDNo, ttpd.PDTgl, ttpd.SDNo, ttpd.DealerKode, ttpd.LokasiKode, 
						ttpd.PDMemo, ttpd.PDTotal, ttpd.UserID, tddealer.DealerNama, tdlokasi.LokasiNama, 
                         IFNULL(ttsd.SDTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SDTgl, ttpd.PDJam, ttpd.NoGL" ) )
		            ->join( "LEFT OUTER JOIN", "tddealer", "ttpd.DealerKode = tddealer.DealerKode" )
		            ->join( "LEFT OUTER JOIN", "tdlokasi", "ttpd.LokasiKode = tdlokasi.LokasiKode " )
		            ->join( "LEFT OUTER JOIN", "ttsd", "ttpd.SDNo = ttsd.SDNo" );
	}
	public function CekMesinNo( $MyMesinNo, $PDNo ) {
		$A              = 0;
		$MyMotorNoMesin = null;
		try {
			$A = \Yii::$app->db->createCommand( "Select IFNULL(COUNT(MotorNoMesin),0) FROM tmotor 
						WHERE  MotorNoMesin = :MotorNoMesin AND PDNo <> :PDNo AND SKNo = '--' AND SDNo <> '--' ", [
				':MotorNoMesin' => $MyMesinNo,
				':PDNo'         => $PDNo
			] )->queryScalar();
			if ( $A > 0 ) {
				$MyMotorNoMesin = \Yii::$app->db->createCommand( "SELECT MotorNoMesin FROM vmotorlastlokasi 
						WHERE Kondisi = 'OUT' AND MotorNoMesin = :MotorNoMesin", [
					':MotorNoMesin' => $MyMesinNo
				] )->queryScalar();
				if ( $MyMotorNoMesin == null ) {
					$A = 0;
				}
			}
			return $A;
		} catch ( Exception $e ) {
		}
	}
	public function CekSDNo( $MyTabel, $MyField, $MyNo, $MyPesan, $DealerKode, $PDNo ) {
		$Hasil = false;
		try {
			$A = \Yii::$app->db->createCommand( "SELECT PDNo FROM $MyTabel 
				WHERE $MyField = :MyNo AND DealerKode = :DealerKode AND PDNo <> :PDNo", [
				':MyNo'       => $MyNo,
				':DealerKode' => $DealerKode,
				':PDNo'       => $PDNo
			] )->queryScalar();
			if ( $A != null && $A != '--' ) {
				$Hasil = true;
				if ( $MyPesan != 'TidakAdaPesan' ) {
					\Yii::$app->session->setFlash( 'error', $MyPesan );
				}
			}
			return $Hasil;
		} catch ( Exception $e ) {
		}
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]       = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]    = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]   = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'PDNo' ]         = $post[ 'PDNo' ] ?? '--';
		$post[ 'PDNoBaru' ]     = $post[ 'PDNoBaru' ] ?? '--';
		$post[ 'PDTgl' ]        = date('Y-m-d', strtotime($post[ 'PDTgl' ] ?? date( 'Y-m-d' )));
		$post[ 'PDJam' ]        = date('Y-m-d H:i:s', strtotime($post[ 'PDJam' ] ?? date( 'Y-m-d H:i:s' )));
		$post[ 'DealerKode' ]   = $post[ 'DealerKode' ] ?? '--';
		$post[ 'SDNo' ]         = $post[ 'SDNo' ] ?? '--';
		$post[ 'PDMemo' ]       = $post[ 'PDMemo' ] ?? '--';
		$post[ 'JmlUnit' ]      = floatval( $post[ 'JmlUnit' ] ?? 0 );
		$post[ 'PDTotal' ]      = floatval( $post[ 'PDTotal' ] ?? 0 );
		$post[ 'NoGL' ]         = $post[ 'NoGL' ] ?? '--';
		$post[ 'LokasiKodePD' ] = $post[ 'LokasiKodePD' ] ?? '--';
		try {
			General::cCmd( "SET @PDNo = ?;" )->bindParam( 1, $post[ 'PDNo' ] )->execute();
			General::cCmd( "CALL fpd(?,@Status,@Keterangan,?,?,?,@PDNo,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'PDNoBaru' ] )
			       ->bindParam( 6, $post[ 'PDTgl' ] )
			       ->bindParam( 7, $post[ 'PDJam' ] )
			       ->bindParam( 8, $post[ 'DealerKode' ] )
			       ->bindParam( 9, $post[ 'SDNo' ] )
			       ->bindParam( 10, $post[ 'PDMemo' ] )
			       ->bindParam( 11, $post[ 'JmlUnit' ] )
			       ->bindParam( 12, $post[ 'PDTotal' ] )
			       ->bindParam( 13, $post[ 'NoGL' ] )
			       ->bindParam( 14, $post[ 'LokasiKodePD' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @PDNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'PDNo'       => $post[ 'PDNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$PDNo       = $exe[ '@PDNo' ] ?? $post[ 'PDNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'PDNo'       => $PDNo,
			'status'     => $status,
			'keterangan' => str_replace(" ","&nbsp;",$keterangan)
		];
	}
}
