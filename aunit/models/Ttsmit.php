<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "ttsmit".
 *
 * @property string $SMNo
 * @property string $MotorNoMesin
 * @property int $MotorAutoN
 */
class Ttsmit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttsmit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SMNo', 'MotorNoMesin'], 'required'],
            [['MotorAutoN'], 'integer'],
            [['SMNo'], 'string', 'max' => 10],
            [['MotorNoMesin'], 'string', 'max' => 25],
            [['SMNo', 'MotorNoMesin'], 'unique', 'targetAttribute' => ['SMNo', 'MotorNoMesin']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SMNo' => 'Sm No',
            'MotorNoMesin' => 'Motor No Mesin',
            'MotorAutoN' => 'Motor Auto N',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TtsmitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtsmitQuery(get_called_class());
    }
}
