<?php

namespace aunit\models;

use Yii;
/**
 * This is the model class for table "ttpbhd".
 *
 * @property string $PBNo
 * @property string $PBTgl
 * @property string $SMNo
 * @property string $LokasiAsal
 * @property string $LokasiTujuan
 * @property string $PBMemo
 * @property string $PBTotal
 * @property string $UserID
 * @property Ttsmhd $suratJalan
 * @property Tdlokasi $rLokasiTujuan
 * @property Tdlokasi $rLokasiAsal
 * @property string $PBJam
 */
class Ttpbhd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ttpbhd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PBNo'], 'required'],
            [['PBTgl', 'PBJam'], 'safe'],
            [['PBTotal'], 'number'],
            [['PBNo', 'SMNo'], 'string', 'max' => 10],
            [['LokasiAsal', 'LokasiTujuan', 'UserID'], 'string', 'max' => 15],
            [['PBMemo'], 'string', 'max' => 100],
            [['PBNo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PBNo'         => 'Pb No',
            'PBTgl'        => 'Pb Tgl',
            'SMNo'         => 'Sm No',
            'LokasiAsal'   => 'Lokasi Asal',
            'LokasiTujuan' => 'Lokasi Tujuan',
            'PBMemo'       => 'Pb Memo',
            'PBTotal'      => 'Pb Total',
            'UserID'       => 'User ID',
            'PBJam'        => 'Pb Jam',
        ];
    }

	/**
	 * {@inheritdoc}
	 */
	public static function colGrid() {
		return [
			'PBNo'                    => ['width' => 75,'label' => 'No PB/PP','name' => 'PBNo'],
			'PBTgl'                   => ['width' => 75,'label' => 'Tgl PB/PP','name' => 'PBTgl', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'd/m/Y']],
			'PBJam'                   => ['width' => 50,'label' => 'Jam','name' => 'PBJam', 'formatter' => 'date', 'formatoptions' => ['srcformat' => "ISO8601Long",'newformat' => 'H:i:s']],
			'ttpbhd.SMNo'             => ['width' => 80,'label' => 'No SM/MI','name' => 'SMNo'],
			'ttpbhd.LokasiAsal'       => ['width' => 120,'label' => 'Lokasi Asal','name' => 'LokasiAsal'],
			'ttpbhd.LokasiTujuan'     => ['width' => 120,'label' => 'Lokasi Tujuan','name' => 'LokasiTujuan'],
			'ttpbhd.PBMemo'           => ['width' => 330,'label' => 'Keterangan','name' => 'PBMemo'],
			'ttpbhd.UserID'           => ['width' => 90,'label' => 'UserID','name' => 'UserID'],
			'lasal.LokasiNama AS LokasiAsalNama'     => ['width' => 175,'label' => 'Lokasi Asal','name' => 'LokasiAsalNama'],
			'ltujuan.LokasiNama AS LokasiTujuanNama' => ['width' => 175,'label' => 'Lokasi Tujuan','name' => 'LokasiTujuanNama'],
		];
	}

	// ,  PBMemo, PBTotal,  

	public function getSuratJalan() {
		return $this->hasOne( Ttsmhd::className(), [ 'SMNo' => 'SMNo' ] );
	}
	public function getRLokasiAsal() {
		return $this->hasOne( Tdlokasi::className(), [ 'LokasiKode' => 'LokasiAsal' ] );
	}
	public function getRLokasiTujuan() {
		return $this->hasOne( Tdlokasi::className(), [ 'LokasiKode' => 'LokasiTujuan' ] );
	}
    /**
     * {@inheritdoc}
     * @return TtpbhdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TtpbhdQuery(get_called_class());
    }
}
