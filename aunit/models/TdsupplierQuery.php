<?php

namespace aunit\models;
use yii\helpers\ArrayHelper;
/**
 * This is the ActiveQuery class for [[Tdsupplier]].
 *
 * @see Tdsupplier
 */
class TdsupplierQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tdsupplier[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tdsupplier|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	public function combo() {
		return $this->select( [ "CONCAT(SupKode,' - ',SupNama) as label", "SupKode as value" ] )
		            ->orderBy( 'SupKode' )
		            ->asArray()
		            ->all();
	}

    public function select2($value, $label = [ 'SupKode' ], $orderBy = [], $where = [ 'condition' => null, 'params' => [] ] , $allOption = false) {
	    $option =
		    $this->select( [ "CONCAT(" . implode( ",'||',", $label ) . ") as label", "$value as value" ] )
		         ->orderBy( $orderBy )
		         ->where( $where[ 'condition' ], $where[ 'params' ] )
		         ->asArray()
		         ->all();
	    if ( $allOption ) {
		    return array_merge( [ [ 'value' => '%', 'label' => 'Semua' ] ], $option );
	    } else {
		    return $option;
	    }
    }


}
