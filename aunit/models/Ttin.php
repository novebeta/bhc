<?php
namespace aunit\models;
/**
 * This is the model class for table "ttin".
 *
 * @property string $INNo
 * @property string $INTgl
 * @property string $SalesKode
 * @property string $CusKode
 * @property string $LeaseKode
 * @property string $INJenis Tunai / Kredit
 * @property string $INMemo
 * @property string $INDP
 * @property string $UserID
 * @property string $INHarga
 * @property string $MotorType
 * @property string $MotorWarna
 * @property Tdcustomer $customer
 * @property mixed $sales
 * @property mixed $dataKonsumen
 * @property Tdleasing $leasing
 * @property string $DKNo
 */
class Ttin extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'ttin';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'INNo' ], 'required' ],
			[ [ 'INTgl' ], 'safe' ],
			[ [ 'INDP', 'INHarga' ], 'number' ],
			[ [ 'INNo', 'SalesKode', 'CusKode', 'LeaseKode', 'DKNo' ], 'string', 'max' => 10 ],
			[ [ 'INJenis' ], 'string', 'max' => 6 ],
			[ [ 'INMemo' ], 'string', 'max' => 100 ],
			[ [ 'UserID' ], 'string', 'max' => 15 ],
			[ [ 'MotorType' ], 'string', 'max' => 50 ],
			[ [ 'MotorWarna' ], 'string', 'max' => 35 ],
			[ [ 'INNo' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'INNo'       => 'In No',
			'INTgl'      => 'In Tgl',
			'SalesKode'  => 'Sales Kode',
			'CusKode'    => 'Cus Kode',
			'LeaseKode'  => 'Lease Kode',
			'INJenis'    => 'Tunai / Kredit',
			'INMemo'     => 'In Memo',
			'INDP'       => 'Indp',
			'UserID'     => 'User ID',
			'INHarga'    => 'In Harga',
			'MotorType'  => 'Motor Type',
			'MotorWarna' => 'Motor Warna',
			'DKNo'       => 'Dk No',
		];
	}
	//INNo, INTgl, SalesKode, CusKode, LeaseKode, INJenis, INMemo, INDP, UserID, INHarga, MotorType, MotorWarna, DKNo
	public static function colGrid() {
		return [
			'ttin.INNo'             => [ 'width' => 70, 'label' => 'No Inden', 'name' => 'INNo' ],
			'INTgl'                 => [ 'width' => 70, 'label' => 'Tgl IN', 'name' => 'INTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'tdcustomer.CusNama'    => [ 'width' => 200, 'label' => 'Nama Konsumen', 'name' => 'CusNama' ],
			'ttin.MotorType'        => [ 'width' => 265, 'label' => 'Type Motor', 'name' => 'MotorType' ],
			'ttin.MotorWarna'       => [ 'width' => 115, 'label' => 'Warna Motor', 'name' => 'MotorWarna' ],
			'INDP'                  => [ 'width' => 105, 'label' => 'Uang Muka', 'name' => 'INDP', 'formatter' => 'number', 'align' => 'right' ],
			'INHarga'               => [ 'width' => 105, 'label' => 'Harga', 'name' => 'INHarga', 'formatter' => 'number', 'align' => 'right' ],
			'tdsales.SalesNama'     => [ 'width' => 175, 'label' => 'Sales', 'name' => 'SalesNama' ],
			'Tdleasing.LeaseKode'   => [ 'width' => 60, 'label' => 'Leasing', 'name' => 'LeaseKode' ],
			'ttin.DKNo'             => [ 'width' => 70, 'label' => 'No DK', 'name' => 'DKNo' ],
			'ttin.UserID'           => [ 'width' => 80, 'label' => 'UserID', 'name' => 'UserID' ],
			'ttin.INMemo'           => [ 'width' => 200, 'label' => 'Keterangan', 'name' => 'INMemo' ],
			'Tdmotortype.MotorNama' => [ 'width' => 200, 'label' => 'Nama Motor', 'name' => 'MotorNama' ],
		];
	}
	public static function colGridSelectKMInden() {
		return [
			'INNo'       => [ 'width' => 70, 'label' => 'No Inden', 'name' => 'INNo' ],
			'INTgl'      => [ 'width' => 70, 'label' => 'Tgl IN', 'name' => 'INTgl', 'formatter' => 'date', 'formatoptions' => [ 'srcformat' => "ISO8601Long", 'newformat' => 'd/m/Y' ] ],
			'CusNama'    => [ 'width' => 200, 'label' => 'Nama Konsumen', 'name' => 'CusNama' ],
			'MotorType'  => [ 'width' => 165, 'label' => 'Type Motor', 'name' => 'MotorType' ],
			'DKNo'       => [ 'width' => 70, 'label' => 'No DK', 'name' => 'DKNo' ],
			'INDP'       => [ 'width' => 105, 'label' => 'Uang Muka', 'name' => 'INDP', 'formatter' => 'number', 'align' => 'right' ],
			'IndenSudah' => [ 'width' => 105, 'label' => 'Inden Terbayar', 'name' => 'IndenSudah', 'formatter' => 'number', 'align' => 'right' ],
			'Sisa'  => [ 'width' => 105, 'label' => 'Sisa', 'name' => 'Sisa', 'formatter' => 'number', 'align' => 'right' ],
		];
	}
	public function getMotorTypes() {
		return $this->hasOne( Tdmotortype::className(), [ 'MotorType' => 'MotorType' ] );
	}
	public function getLeasing() {
		return $this->hasOne( Tdleasing::className(), [ 'LeaseKode' => 'LeaseKode' ] );
	}
	public function getSales() {
		return $this->hasOne( Tdsales::className(), [ 'SalesKode' => 'SalesKode' ] );
	}
	public function getCustomer() {
		return $this->hasOne( Tdcustomer::className(), [ 'CusKode' => 'CusKode' ] );
	}
	public function getDataKonsumen() {
		return $this->hasOne( Ttdk::className(), [ 'INNo' => 'INNo' ] );
	}
	public function getData( $param = null ) {
		return $this->find()->dsTJual()->where( " (ttin.INNo = :INNo)", [
			':INNo' => $this->INNo,
		] )->asArray( true )->one();
	}
	/**
	 * {@inheritdoc}
	 * @return TtinQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TtinQuery( get_called_class() );
	}
}

//SELECT ttin.INNo, ttin.INTgl, ttin.SalesKode, ttin.CusKode, ttin.LeaseKode, ttin.INJenis, ttin.INMemo, " + _
//         "ttin.INDP, ttin.UserID, IFNULL(tdcustomer.CusNama, '--') AS CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, " + _
//         "tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, IFNULL(tdcustomer.CusKabupaten, '--') AS CusKabupaten, tdcustomer.CusTelepon,tdcustomer.CusTelepon2, " + _
//         "IFNULL(tdsales.SalesNama, '--') AS SalesNama, IFNULL(tdleasing.LeaseNama, '--') AS LeaseNama, ttin.INHarga, " + _
//         "ttin.MotorType, ttin.MotorWarna, ttin.DKNo FROM ttin " + _
//         "INNER JOIN tdleasing ON ttin.LeaseKode = tdleasing.LeaseKode  " + _
//         "INNER JOIN tdsales ON ttin.SalesKode = tdsales.SalesKode  " + _
//         "INNER JOIN tdcustomer ON ttin.CusKode = tdcustomer.CusKode  " + _
//         "LEFT OUTER JOIN ttdk ON ttin.INNo = ttdk.INNo
