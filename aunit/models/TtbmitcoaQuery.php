<?php
namespace aunit\models;
use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
/**
 * This is the ActiveQuery class for [[Ttbmitcoa]].
 *
 * @see Ttbmitcoa
 */
class TtbmitcoaQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttbmitcoa[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttbmitcoa|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]       = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]    = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]   = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'BMNo' ]         = $post[ 'BMNo' ] ?? '--';
		$post[ 'BMAutoN' ]      = floatval( $post[ 'BMAutoN' ] ?? 0 );
		$post[ 'NoAccount' ]    = $post[ 'NoAccount' ] ?? '--';
		$post[ 'BMKeterangan' ] = $post[ 'BMKeterangan' ] ?? '--';
		$post[ 'BMBayar' ]      = floatval( $post[ 'BMBayar' ] ?? 0 );
		$post[ 'BMAutoNOLD' ]   = floatval( $post[ 'BMAutoNOLD' ] ?? 0 );
		$post[ 'NoAccountOLD' ] = $post[ 'NoAccountOLD' ] ?? '--';
		try {
			General::cCmd( "CALL fbmitcoa(?,@Status,@Keterangan,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'BMNo' ] )
			       ->bindParam( 6, $post[ 'BMAutoN' ] )
			       ->bindParam( 7, $post[ 'NoAccount' ] )
			       ->bindParam( 8, $post[ 'BMBayar' ] )
			       ->bindParam( 9, $post[ 'BMKeterangan' ] )
			       ->bindParam( 10, $post[ 'BMAutoNOLD' ] )
			       ->bindParam( 11, $post[ 'NoAccountOLD' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
}
