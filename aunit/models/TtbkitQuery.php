<?php

namespace aunit\models;

use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;

/**
 * This is the ActiveQuery class for [[Ttbkit]].
 *
 * @see Ttbkit
 */
class TtbkitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ttbkit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ttbkit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function callSP( $post ) {
        $post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'LokasiKode' ]    = Menu::getUserLokasi()[ 'LokasiKode' ];
        $post[ 'BKNo' ]          = $post[ 'BKNo' ] ?? '--';
        $post[ 'FBNo' ]          = $post[ 'FBNo' ] ?? '--';
        $post[ 'BKBayar' ]       = floatval( $post[ 'BKBayar' ] ?? 0 );
        $post[ 'BKNoOLD' ]       = $post[ 'BKNoOLD' ] ?? '--';
        $post[ 'FBNoOLD' ]       = $post[ 'FBNoOLD' ] ?? '--';
        try {
            General::cCmd( "CALL fbkit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'LokasiKode' ] )
                ->bindParam( 5, $post[ 'BKNo' ] )
                ->bindParam( 6, $post[ 'FBNo' ] )
                ->bindParam( 7, $post[ 'BKBayar' ] )
                ->bindParam( 8, $post[ 'BKNoOLD' ] )
                ->bindParam( 9, $post[ 'FBNoOLD' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
    public function callSPPengajuan( $post ) {
        $post[ 'UserID' ]        = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
        $post[ 'KodeAkses' ]     = Menu::getUserLokasi()[ 'KodeAkses' ];
        $post[ 'LokasiKode' ]    = Menu::getUserLokasi()[ 'LokasiKode' ];
        $post[ 'BKNo' ]          = $post[ 'BKNo' ] ?? '--';
        $post[ 'FBNo' ]          = $post[ 'FBNo' ] ?? '--';
        $post[ 'BKBayar' ]       = $post[ 'BKBayar' ] ?? 0;
        $post[ 'BKNoOLD' ]       = $post[ 'BKNoOLD' ] ?? '--';
        $post[ 'FBNoOLD' ]     = $post[ 'FBNoOLD' ] ?? '--';
        try {
            General::cCmd( "CALL fbpit(?,@Status,@Keterangan,?,?,?,?,?,?,?,?);" )
                ->bindParam( 1, $post[ 'Action' ] )
                ->bindParam( 2, $post[ 'UserID' ] )
                ->bindParam( 3, $post[ 'KodeAkses' ] )
                ->bindParam( 4, $post[ 'LokasiKode' ] )
                ->bindParam( 5, $post[ 'BKNo' ] )
                ->bindParam( 6, $post[ 'FBNo' ] )
                ->bindParam( 7, $post[ 'BKBayar' ] )
                ->bindParam( 8, $post[ 'BKNoOLD' ] )
                ->bindParam( 9, $post[ 'FBNoOLD' ] )
                ->execute();
            $exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
        } catch ( Exception $e ) {
            return [
                'status'     => 1,
                'keterangan' => $e->getMessage()
            ];
        }
        $status     = $exe[ '@Status' ] ?? 1;
        $keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
        return [
            'status'     => $status,
            'keterangan' => str_replace(" ","&nbsp;",$keterangan)
        ];
    }
}
