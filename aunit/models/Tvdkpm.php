<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "tvdkpm".
 *
 * @property string $DKNo
 * @property string $DKTgl
 * @property string $Jenis
 * @property string $KeteranganJenis
 * @property string $CusKode
 * @property string $Awal
 */
class Tvdkpm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tvdkpm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DKNo', 'DKTgl', 'Jenis'], 'required'],
            [['DKTgl'], 'safe'],
            [['Awal'], 'number'],
            [['DKNo', 'CusKode'], 'string', 'max' => 10],
            [['Jenis'], 'string', 'max' => 20],
            [['KeteranganJenis'], 'string', 'max' => 35],
            [['DKNo', 'DKTgl', 'Jenis'], 'unique', 'targetAttribute' => ['DKNo', 'DKTgl', 'Jenis']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'DKNo' => 'Dk No',
            'DKTgl' => 'Dk Tgl',
            'Jenis' => 'Jenis',
            'KeteranganJenis' => 'Keterangan Jenis',
            'CusKode' => 'Cus Kode',
            'Awal' => 'Awal',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TvdkpmQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TvdkpmQuery(get_called_class());
    }
}
