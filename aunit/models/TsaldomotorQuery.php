<?php

namespace aunit\models;

/**
 * This is the ActiveQuery class for [[Tsaldomotor]].
 *
 * @see Tsaldomotor
 */
class TsaldomotorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tsaldomotor[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tsaldomotor|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
