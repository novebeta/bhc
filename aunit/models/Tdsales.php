<?php
namespace aunit\models;
/**
 * This is the model class for table "tdsales".
 *
 * @property string $SalesKode
 * @property string $SalesNama
 * @property string $SalesAlamat
 * @property string $SalesTelepon
 * @property string $SalesStatus
 * @property string $SalesKeterangan
 * @property string $SalesKodeAstra
 * @property string $TeamKode
 * @property string $SalesHSOid
 * @property string $SalesPass
 */
class Tdsales extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'tdsales';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'SalesKode' ], 'required' ],
			[ [ 'SalesKode' ], 'string', 'max' => 10 ],
			[ [ 'SalesNama' ], 'string', 'max' => 50 ],
			[ [ 'SalesAlamat', 'SalesTelepon' ], 'string', 'max' => 75 ],
			[ [ 'SalesStatus', 'SalesPass' ], 'string', 'max' => 1 ],
			[ [ 'SalesKeterangan' ], 'string', 'max' => 100 ],
			[ [ 'SalesKodeAstra' ], 'string', 'max' => 25 ],
			[ [ 'TeamKode', 'SalesHSOid' ], 'string', 'max' => 15 ],
			[ [ 'SalesKode' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'SalesKode'       => 'Kode',
			'SalesNama'       => 'Nama',
			'SalesAlamat'     => 'Alamat',
			'SalesTelepon'    => 'Telepon',
			'SalesStatus'     => 'Status',
			'SalesKeterangan' => 'Keterangan',
			'SalesKodeAstra'  => 'Kode Astra',
			'TeamKode'        => 'Team Kode',
			'SalesHSOid'      => 'HSO ID',
			'SalesPass'       => 'Sales Pass',
		];
	}
	public static function colGrid() {
		return [
            'SalesKode' => ['label' => 'Kode','width' => 60,'name' => 'SalesKode'],
            'SalesNama' => ['label' => 'Nama','width' => 150,'name' => 'SalesNama'],
            'SalesAlamat' => ['label' => 'Alamat','width' => 405,'name' => 'SalesAlamat'],
            'SalesTelepon' => ['label' => 'Telepon','width' => 100,'name' => 'SalesTelepon'],
            'SalesKeterangan' => ['label' => 'Keterangan','width' => 160,'name' => 'SalesKeterangan'],
            'SalesStatus' => ['label' => 'Status','width' => 55,'name' => 'SalesStatus'],
		];
	}
	/**
	 * {@inheritdoc}
	 * @return TdsalesQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TdsalesQuery( get_called_class() );
	}
}
