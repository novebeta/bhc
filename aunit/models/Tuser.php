<?php

namespace aunit\models;

use Yii;

/**
 * This is the model class for table "tuser".
 *
 * @property string $UserID
 * @property string $UserPass
 * @property string $KodeAkses
 * @property string $UserNama
 * @property string $UserAlamat
 * @property string $UserTelepon
 * @property string $UserKeterangan
 * @property string $UserStatus
 * @property string $LokasiKode
 */
class Tuser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tuser';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['UserID'], 'required'],
            [['UserID', 'KodeAkses', 'LokasiKode'], 'string', 'max' => 15],
            [['UserPass'], 'string', 'max' => 10],
            [['UserNama'], 'string', 'max' => 75],
            [['UserAlamat'], 'string', 'max' => 100],
            [['UserTelepon'], 'string', 'max' => 38],
            [['UserKeterangan'], 'string', 'max' => 150],
            [['UserStatus'], 'string', 'max' => 1],
            [['UserID'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'UserID' => 'User ID',
            'UserPass' => 'User Pass',
            'KodeAkses' => 'Grup Akses',
            'UserNama' => 'Nama',
            'UserAlamat' => 'Alamat',
            'UserTelepon' => 'No Telepon',
            'UserKeterangan' => 'Keterangan',
            'UserStatus' => 'Status',
            'LokasiKode' => 'Pos',
        ];
    }
    public static function colGrid()
    {
        return [
            'UserID' => 'User ID',
            'KodeAkses' => 'Kode Akses',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TuserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TuserQuery(get_called_class());
    }
}
