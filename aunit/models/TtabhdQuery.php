<?php
namespace aunit\models;
use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttabhd]].
 *
 * @see Ttabhd
 */
class TtabhdQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttabhd[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttabhd|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function dsTJualFillByNo() {
		return $this->select( new Expression( "ABMemo, ABNo, ABTgl, UserID" ) );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]       = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]    = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]   = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'ABNo' ]         = $post[ 'ABNo' ] ?? '--';
		$post[ 'ABNoBaru' ]     = $post[ 'ABNoView' ] ?? '--';
		$post[ 'ABTgl' ]        = date('Y-m-d', strtotime($post[ 'ABTgl' ] ?? date( 'Y-m-d' )));
		$post[ 'ABMemo' ]       = $post[ 'ABMemo' ] ?? '--';
		try {
			General::cCmd( "SET @ABNo = ?;" )->bindParam( 1, $post[ 'ABNo' ] )->execute();
			General::cCmd( "CALL fabhd(?,@Status,@Keterangan,?,?,?,@ABNo,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'ABNoBaru' ] )
			       ->bindParam( 6, $post[ 'ABTgl' ] )
			       ->bindParam( 7, $post[ 'ABMemo' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @ABNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'ABNo'       => $post[ 'ABNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$ABNo       = $exe[ '@ABNo' ] ?? $post[ 'ABNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'ABNo'       => $ABNo,
			'status'     => $status,
			'keterangan' => str_replace(" ","&nbsp;",$keterangan)
		];
	}
}
