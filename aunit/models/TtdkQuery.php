<?php
namespace aunit\models;
use aunit\components\Menu;
use common\components\General;
use yii\db\Exception;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[Ttdk]].
 *
 * @see Ttdk
 */
class TtdkQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Ttdk[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Ttdk|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function dsJual() {
		return $this->select( new Expression( "ttdk.DKNo, ttdk.DKTgl, ttdk.SalesKode, ttdk.CusKode, ttdk.LeaseKode, ttdk.DKJenis, ttdk.DKMemo, ttdk.DKLunas, ttdk.UserID, ttdk.DKHPP, ttdk.DKHarga, ttdk.DKDPTotal, ttdk.DKDPLLeasing, ttdk.DKDPTerima, ttdk.DKNetto, 
                         ttdk.ProgramNama, ttdk.ProgramSubsidi, ttdk.BBN, ttdk.Jaket, ttdk.PrgSubsSupplier, ttdk.PrgSubsDealer, ttdk.PotonganHarga, ttdk.ReturHarga, ttdk.PotonganKhusus, ttdk.NamaHadiah, IFNULL(tmotor.MotorNoMesin, '--') 
                         AS MotorNoMesin, IFNULL(tmotor.MotorAutoN, 0) AS MotorAutoN, IFNULL(tmotor.MotorType, '--') AS MotorType, IFNULL(tmotor.MotorWarna, '--') AS MotorWarna, IFNULL(tmotor.MotorNoRangka, '--') AS MotorNoRangka, 
                         tmotor.MotorTahun, IFNULL(tdsales.SalesNama, '--') AS SalesNama, IFNULL(tdleasing.LeaseNama, '--') AS LeaseNama, ttdk.INNo, ttdk.PrgSubsFincoy, IFNULL(tmotor.SKNo, '--') AS SKNo, ttdk.PotonganAHM, ttdk.DKDPInden, 
                         ttdk.DKTenor, ttdk.DKAngsuran, IFNULL(tdcustomer.CusKTP, '--') AS CusKTP, IFNULL(tdcustomer.CusNama, '') AS CusNama, IFNULL(tdcustomer.CusAlamat, '') AS CusAlamat, IFNULL(tdcustomer.CusRT, '') AS CusRT, 
                         IFNULL(tdcustomer.CusRW, '') AS CusRW, IFNULL(tdcustomer.CusProvinsi, '--') AS CusProvinsi, IFNULL(tdcustomer.CusKabupaten, '') AS CusKabupaten, IFNULL(tdcustomer.CusKecamatan, '--') AS CusKecamatan, 
                         IFNULL(tdcustomer.CusKelurahan, '--') AS CusKelurahan, IFNULL(tdcustomer.CusKodePos, '--') AS CusKodePos, IFNULL(tdcustomer.CusTelepon, '') AS CusTelepon, IFNULL(tdcustomer.CusSex, '--') AS CusSex, 
                         IFNULL(tdcustomer.CusTempatLhr, '') AS CusTempatLhr, IFNULL(tdcustomer.CusTglLhr, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS CusTglLhr, IFNULL(tdcustomer.CusAgama, '--') AS CusAgama, 
                         IFNULL(tdcustomer.CusPekerjaan, '--') AS CusPekerjaan, IFNULL(tdcustomer.CusPendidikan, '--') AS CusPendidikan, IFNULL(tdcustomer.CusPengeluaran, '--') AS CusPengeluaran, IFNULL(tdcustomer.CusEmail, '--') AS CusEmail, 
                         IFNULL(tdcustomer.CusKodeKons, '--') AS CusKodeKons, ttdk.DKMerkSblmnya, ttdk.DKJenisSblmnya, ttdk.DKMotorUntuk, ttdk.DKMotorOleh, ttdk.DKMediator, ttdk.DKTOP, ttdk.DKPOLeasing, ttdk.SPKNo, 
                         IFNULL(tdmotortype.MotorNama, '--') AS MotorNama, IFNULL(tdmotortype.MotorKategori, '--') AS MotorKategori, tdcustomer.CusStatusHP, tdcustomer.CusStatusRumah, tdcustomer.CusPembeliNama, tdcustomer.CusPembeliKTP, 
                         tdcustomer.CusPembeliAlamat, ttdk.InsentifSales, ttdk.TeamKode, IFNULL(tmotor.RKNo, '--') AS RKNo, ttdk.DKTglTagih, ttdk.DKPONo, ttdk.DKPOTgl, ttdk.BBNPlus, ttdk.ROCount, ttdk.DKScheme, ttdk.DKSCP, 
                         ttdk.DKPengajuanBBN, ttdk.JAPrgSubsSupplier, ttdk.JAPrgSubsDealer, ttdk.JAPrgSubsFincoy, ttdk.JADKHarga, ttdk.JADKDPTerima, ttdk.JADKDPLLeasing, ttdk.JABBN, ttdk.JAReturHarga, ttdk.JAPotonganHarga, 
                         ttdk.JAInsentifSales, ttdk.JAPotonganKhusus, ttdk.DKScheme2, tmotor.FakturAHMNo, ttdk.DKSurveyor, tdcustomer.CusKK, tdcustomer.CusTelepon2" ) )
		            ->join( "LEFT OUTER JOIN", "tmotor", "ttdk.DKNo = tmotor.DKNo" )
		            ->join( "LEFT OUTER JOIN", "tdmotortype", "tmotor.MotorType = tdmotortype.MotorType" )
		            ->join( "LEFT OUTER JOIN", "tdleasing", "ttdk.LeaseKode = tdleasing.LeaseKode" )
		            ->join( "LEFT OUTER JOIN", "tdsales", "ttdk.SalesKode = tdsales.SalesKode" )
		            ->join( "LEFT OUTER JOIN", "tdcustomer", "ttdk.CusKode = tdcustomer.CusKode" );
	}
	public function dsJualStatusKonsumen() {
		return $this->select( new Expression( "ttdk.DKNo, ttdk.DKTgl, ttdk.SalesKode, ttdk.CusKode, ttdk.LeaseKode, ttdk.DKJenis, ttdk.DKMemo, ttdk.DKLunas, ttdk.UserID, ttdk.DKHPP, 
                         ttdk.ProgramNama, ttdk.Jaket, ttdk.NamaHadiah, ttdk.INNo, ttdk.DKTenor, ttdk.DKAngsuran,   
                         (ttdk.PrgSubsSupplier + ttdk.JAPrgSubsSupplier) AS PrgSubsSupplier, (ttdk.PrgSubsDealer + ttdk.JAPrgSubsDealer) AS PrgSubsDealer, (ttdk.PrgSubsFincoy + ttdk.JAPrgSubsFincoy) AS PrgSubsFincoy, 
                         (ttdk.ProgramSubsidi + ttdk.JAPrgSubsSupplier + ttdk.JAPrgSubsDealer + ttdk.JAPrgSubsFincoy) AS ProgramSubsidi,
                         (ttdk.DKDPLLeasing+JADKDPLLeasing) AS DKDPLLeasing, (ttdk.DKDPTerima + ttdk.JADKDPTerima) AS DKDPTerima, (ttdk.DKDPTotal + ttdk.JADKDPTerima + JADKDPLLeasing) AS DKDPTotal, 
                         (ttdk.DKHarga+ttdk.JADKHarga) AS DKHarga, (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) AS DKNetto, 
                         (ttdk.BBN+ttdk.JABBN) AS BBN, 
                         (ttdk.ReturHarga+ttdk.JAReturHarga) AS ReturHarga, (ttdk.PotonganHarga+ttdk.JAPotonganHarga) AS PotonganHarga, (ttdk.InsentifSales+ttdk.JAInsentifSales) AS InsentifSales, (ttdk.PotonganKhusus+ttdk.JAPotonganKhusus) AS PotonganKhusus,
                         ttdk.JAPrgSubsSupplier, ttdk.JAPrgSubsDealer, ttdk.JAPrgSubsFincoy, 
                         ttdk.JADKHarga, ttdk.JADKDPTerima, ttdk.JADKDPLLeasing,
                         ttdk.JABBN, 
                         ttdk.JAReturHarga, ttdk.JAPotonganHarga, ttdk.JAInsentifSales, ttdk.JAPotonganKhusus,
                         ttdk.PotonganAHM, ttdk.DKDPInden, ttdk.BBNPlus, ttdk.DKTenor, ttdk.DKAngsuran, ttdk.DKSurveyor, 
                         ttdk.TeamKode, ttdk.DKTglTagih, ttdk.DKPONo, ttdk.DKPOTgl, ttdk.ROCount, ttdk.DKScheme,ttdk.DKScheme2,  ttdk.DKSCP, ttdk.DKPengajuanBBN,
                         ttdk.DKMerkSblmnya, ttdk.DKJenisSblmnya, ttdk.DKMotorUntuk, ttdk.DKMotorOleh, ttdk.DKMediator, ttdk.DKTOP, ttdk.DKPOLeasing, ttdk.SPKNo, 
                         tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, 
                         tmotor.SKNo, tmotor.RKNo, tmotor.FakturAHMNo,
                         MotorNama, MotorKategori, SalesNama, LeaseNama, 
                         CusKTP, CusNama, CusAlamat, CusRT, CusRW, CusProvinsi, CusKabupaten, CusKecamatan, CusKelurahan, CusKodePos, CusTelepon, CusSex, 
                         CusTempatLhr, CusTglLhr, CusAgama, CusPekerjaan, CusPendidikan, CusPengeluaran, CusEmail, CusKodeKons, 
                         tdcustomer.CusStatusHP, tdcustomer.CusStatusRumah, tdcustomer.CusPembeliNama, tdcustomer.CusPembeliKTP, tdcustomer.CusPembeliAlamat, tdcustomer.CusKK, ttdk.ZidSpk, ttdk.ZidProspect" ) )
		            ->join( "LEFT JOIN", "tmotor", "ttdk.DKNo = tmotor.DKNo" )
		            ->join( "LEFT JOIN", "tdmotortype", "tmotor.MotorType = tdmotortype.MotorType" )
		            ->join( "LEFT JOIN", "tdleasing", "ttdk.LeaseKode = tdleasing.LeaseKode" )
		            ->join( "LEFT JOIN", "tdsales", "ttdk.SalesKode = tdsales.SalesKode" )
		            ->join( "LEFT JOIN", "tdcustomer", "ttdk.CusKode = tdcustomer.CusKode" );
	}
	public function dsTJualFillByNo() {
		return $this->select( new Expression( "ttdk.DPTOP,ttdk.DKNo, ttdk.DKTgl, ttdk.SalesKode, ttdk.CusKode, ttdk.LeaseKode, ttdk.DKJenis, ttdk.DKMemo, ttdk.DKLunas, ttdk.UserID, ttdk.DKHPP, ttdk.DKHarga, ttdk.DKDPTotal, ttdk.DKDPLLeasing, ttdk.DKDPTerima, ttdk.DKNetto, 
                         ttdk.ProgramNama, ttdk.ProgramSubsidi, ttdk.BBN, ttdk.Jaket, ttdk.PrgSubsSupplier, ttdk.PrgSubsDealer, ttdk.PotonganHarga, ttdk.ReturHarga, ttdk.PotonganKhusus, ttdk.NamaHadiah, IFNULL(tmotor.MotorNoMesin, '--') 
                         AS MotorNoMesin, IFNULL(tmotor.MotorAutoN, 0) AS MotorAutoN, IFNULL(tmotor.MotorType, '--') AS MotorType, IFNULL(tmotor.MotorWarna, '--') AS MotorWarna, IFNULL(tmotor.MotorNoRangka, '--') AS MotorNoRangka, 
                         tmotor.MotorTahun, IFNULL(tdsales.SalesNama, '--') AS SalesNama, IFNULL(tdleasing.LeaseNama, '--') AS LeaseNama, ttdk.INNo, ttdk.PrgSubsFincoy, IFNULL(tmotor.SKNo, '--') AS SKNo, ttdk.PotonganAHM, ttdk.DKDPInden, 
                         ttdk.DKTenor, ttdk.DKAngsuran, IFNULL(tdcustomer.CusKTP, '--') AS CusKTP, IFNULL(tdcustomer.CusNama, '') AS CusNama, IFNULL(tdcustomer.CusAlamat, '') AS CusAlamat, IFNULL(tdcustomer.CusRT, '') AS CusRT, 
                         IFNULL(tdcustomer.CusRW, '') AS CusRW, IFNULL(tdcustomer.CusProvinsi, '--') AS CusProvinsi, IFNULL(tdcustomer.CusKabupaten, '') AS CusKabupaten, IFNULL(tdcustomer.CusKecamatan, '--') AS CusKecamatan, 
                         IFNULL(tdcustomer.CusKelurahan, '--') AS CusKelurahan, IFNULL(tdcustomer.CusKodePos, '--') AS CusKodePos, IFNULL(tdcustomer.CusTelepon, '') AS CusTelepon, IFNULL(tdcustomer.CusSex, '--') AS CusSex, 
                         IFNULL(tdcustomer.CusTempatLhr, '') AS CusTempatLhr, IFNULL(tdcustomer.CusTglLhr, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS CusTglLhr, IFNULL(tdcustomer.CusAgama, '--') AS CusAgama, 
                         IFNULL(tdcustomer.CusPekerjaan, '--') AS CusPekerjaan, IFNULL(tdcustomer.CusPendidikan, '--') AS CusPendidikan, IFNULL(tdcustomer.CusPengeluaran, '--') AS CusPengeluaran, IFNULL(tdcustomer.CusEmail, '--') AS CusEmail, 
                         IFNULL(tdcustomer.CusKodeKons, '--') AS CusKodeKons, ttdk.DKMerkSblmnya, ttdk.DKJenisSblmnya, ttdk.DKMotorUntuk, ttdk.DKMotorOleh, ttdk.DKMediator, ttdk.DKTOP, ttdk.DKPOLeasing, ttdk.SPKNo, 
                         IFNULL(tdmotortype.MotorNama, '--') AS MotorNama, IFNULL(tdmotortype.MotorKategori, '--') AS MotorKategori, tdcustomer.CusStatusHP, tdcustomer.CusStatusRumah, tdcustomer.CusPembeliNama, tdcustomer.CusPembeliKTP, 
                         tdcustomer.CusPembeliAlamat, ttdk.InsentifSales, ttdk.TeamKode, IFNULL(tmotor.FakturAHMNo, '--') AS RKNo, ttdk.DKTglTagih, ttdk.DKPONo, ttdk.DKPOTgl, ttdk.BBNPlus, ttdk.ROCount, ttdk.DKScheme, ttdk.DKSCP, 
                         ttdk.DKPengajuanBBN, ttdk.JAPrgSubsSupplier, ttdk.JAPrgSubsDealer, ttdk.JAPrgSubsFincoy, ttdk.JADKHarga, ttdk.JADKDPTerima, ttdk.JADKDPLLeasing, ttdk.JABBN, ttdk.JAReturHarga, ttdk.JAPotonganHarga, 
                         ttdk.JAInsentifSales, ttdk.JAPotonganKhusus, ttdk.DKScheme2, tmotor.FakturAHMNo, ttdk.DKSurveyor, tdcustomer.CusKK, tdcustomer.CusTelepon2, tdcustomer.CusNPWP, ttdk.ZidSpk, ttdk.ZidProspect" ) )
		            ->join( "LEFT OUTER JOIN", "tmotor", "ttdk.DKNo = tmotor.DKNo" )
		            ->join( "LEFT OUTER JOIN", "tdmotortype", "tmotor.MotorType = tdmotortype.MotorType" )
		            ->join( "LEFT OUTER JOIN", "tdleasing", "ttdk.LeaseKode = tdleasing.LeaseKode" )
		            ->join( "LEFT OUTER JOIN", "tdsales", "ttdk.SalesKode = tdsales.SalesKode" )
		            ->join( "LEFT OUTER JOIN", "tdcustomer", "ttdk.CusKode = tdcustomer.CusKode" );
	}
	public function ComboMotorAddEdit( $add, $params ) {
		$query = $this->select( new Expression( "tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorAutoN,
    	tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorTahun, 
    	ttdk.DKNo, ttsd.SDNo, tdmotortype.MotorHrgJual,tdmotortype.MotorNama, tdmotortype.MotorKategori" ) )
		              ->from( "tmotor" )
		              ->join( "LEFT OUTER JOIN", "ttdk", "tmotor.DKNo = ttdk.DKNo" )
		              ->join( "LEFT OUTER JOIN", "ttsd", "tmotor.SDNo = ttsd.SDNo" )
		              ->join( "LEFT OUTER JOIN", "ttfb", "tmotor.FBNo = ttfb.FBNo" )
		              ->join( "LEFT OUTER JOIN", "ttss", "tmotor.SSNo = ttss.SSNo" )
		              ->join( "INNER JOIN", "tdmotortype", "tmotor.MotorType = tdmotortype.MotorType" )
		              ->join( "INNER JOIN", "vmotorLastlokasi", " (tmotor.MotorNoMesin = vmotorLastlokasi.MotorNoMesin AND 
		              tmotor.MotorAutoN = vmotorLastlokasi.MotorAutoN)" );
		return $query;
	}
	public function GetRO( $MyDKNo, $MyCusKTP, $MyCusPembeliKTP ) {
		$query = $this->select( "IFNULL(COUNT(DKNo),0)" )
		              ->join( 'INNER JOIN', 'tdcustomer', 'ttdk.CusKode = tdcustomer.CusKode' );
		if ( $MyCusKTP <> "      -      -" && ( $MyCusPembeliKTP <> "      -      -" && $MyCusPembeliKTP <> "--" ) ) {
			$query->where( "(CusKTP = :CusKTP OR CusPembeliKTP = :CusPembeliKTP) AND  DKNo < :DKNo", [
				':CusKTP'        => $MyCusKTP,
				':CusPembeliKTP' => $MyCusPembeliKTP,
				':DKNo'          => $MyDKNo,
			] );
		} else {
			if ( $MyCusKTP <> "      -      -" && ( $MyCusPembeliKTP == "      -      -" || $MyCusPembeliKTP == "--" ) ) {
				$query->where( "(CusKTP = :CusKTP) AND  DKNo < :DKNo", [
					':CusKTP' => $MyCusKTP,
					':DKNo'   => $MyDKNo,
				] );
			}
			if ( $MyCusPembeliKTP <> "      -      -" && $MyCusPembeliKTP <> "--" && $MyCusKTP == "      -      -" ) {
				$query->where( "(CusKTP = :CusKTP) AND  DKNo < :DKNo", [
					':CusKTP' => $MyCusKTP,
					':DKNo'   => $MyDKNo,
				] );
			}
			if ( $MyCusKTP == "      -      -" && ( $MyCusPembeliKTP == "      -      -" || $MyCusPembeliKTP == "--" ) ) {
				$query = $this->select( new Expression( '0' ) );
			}
		}
		return $query->scalar();
	}
	public function UpdateHargaBeli( $MyNoMesin, $MyAutoN ) {
		try {
			$MyDKHPP = 0;
			$MyDKHPP = \Yii::$app->db->createCommand( "SELECT IFNULL(FBHarga,0) FROM tmotor 
			WHERE MotorAutoN = :MotorAutoN AND MotorNoMesin = :MotorNoMesin AND FBHarga = SSHarga ", [
				':MotorAutoN'   => $MyAutoN,
				':MotorNoMesin' => $MyNoMesin
			] )->queryScalar();
			if ( $MyDKHPP == 0 ) {
				$MyDKHPP = \Yii::$app->db->createCommand( "SELECT IFNULL(FBHarga,0) FROM tmotor 
    		INNER JOIN ttfb ON tmotor.FBNo = ttfb.FBNo INNER JOIN ttSs ON tmotor.SSNo = ttSs.SSNo 
			WHERE MotorAutoN = :MotorAutoN AND MotorNoMesin = :MotorNoMesin AND FBHarga <> SSHarga  AND FBTgl < SSTgl", [
					':MotorAutoN'   => $MyAutoN,
					':MotorNoMesin' => $MyNoMesin
				] )->queryScalar();
			}
			$SKTglLebihBesarFBtgl = true;
			$A                    = null;
			if ( $MyDKHPP > 0 ) {
				$A = \Yii::$app->db->createCommand( "SELECT tmotor.SKNo FROM tmotor INNER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo INNER JOIN ttFB ON ttFB.FBNo = tmotor.FBNo
				WHERE SKTgl < FBTgl AND MotorAutoN = :MotorAutoN AND MotorNoMesin = :MotorNoMesin ", [
					':MotorAutoN'   => $MyAutoN,
					':MotorNoMesin' => $MyNoMesin
				] )->queryScalar();
				if ( $A != null ) {
					$SKTglLebihBesarFBtgl = false;
				}
			}
			if ( $MyDKHPP > 0 && $SKTglLebihBesarFBtgl ) {
				\Yii::$app->db->createCommand( "UPDATE tmotor SET SKHarga = :SKHarga WHERE MotorAutoN = :MotorAutoN AND MotorNoMesin = :MotorNoMesin", [
					':SKHarga'      => $MyDKHPP,
					':MotorAutoN'   => $MyAutoN,
					':MotorNoMesin' => $MyNoMesin
				] )->execute();
			}
			if ( $MyDKHPP == 0 ) {
				$MyDKHPP = \Yii::$app->db->createCommand( "SELECT SKHarga FROM  tmotor WHERE MotorAutoN = :MotorAutoN AND MotorNoMesin = :MotorNoMesin", [
					':MotorAutoN'   => $MyAutoN,
					':MotorNoMesin' => $MyNoMesin
				] )->queryScalar();
			}
			$A = null;
			$A = \Yii::$app->db->createCommand( "SELECT PDNo FROM tmotor WHERE MotorAutoN = :MotorAutoN AND MotorNoMesin = :MotorNoMesin AND LEFT(PDNo,2) = 'PD'", [
				':MotorAutoN'   => $MyAutoN,
				':MotorNoMesin' => $MyNoMesin
			] )->queryScalar();
			if ( $A != null ) {
				\Yii::$app->db->createCommand( "UPDATE tmotor SET SKHarga = SSHarga, FBHarga = SSHarga, SDHarga = SSHarga, RKHarga = SSHarga 
				WHERE MotorAutoN = :MotorAutoN AND MotorNoMesin = :MotorNoMesin", [
					':MotorAutoN'   => $MyAutoN,
					':MotorNoMesin' => $MyNoMesin
				] )->execute();
				$MyDKHPP = \Yii::$app->db->createCommand( "SELECT SKHarga FROM  tmotor WHERE MotorAutoN = :MotorAutoN AND MotorNoMesin = :MotorNoMesin", [
					':MotorAutoN'   => $MyAutoN,
					':MotorNoMesin' => $MyNoMesin
				] )->queryScalar();
			}
		} catch ( Exception $e ) {
			throw new $e;
		}
		return $MyDKHPP;
	}
	public function UpdateMotorDKNo( $MyNoMesin, $MyAutoN, $Number, &$txtSKNo ) {
		try {
			if ( $this->HapusMotorSKDKNo( $Number, $txtSKNo ) ) {
				\Yii::$app->db->createCommand( "UPDATE tmotor SET DKNo = :DKNo WHERE MotorAutoN = :MotorAutoN AND MotorNoMesin = :MotorNoMesin", [
					':DKNo'         => $Number,
					':MotorAutoN'   => $MyAutoN,
					':MotorNoMesin' => $MyNoMesin
				] )->execute();
				if ( $txtSKNo != "--" ) {
					\Yii::$app->db->createCommand( "Update tmotor SET SKNo = :SKNo  WHERE MotorAutoN = :MotorAutoN AND MotorNoMesin = :MotorNoMesin;
						Update tvmotorlokasi SET MotorAutoN = :MotorAutoN , MotorNoMesin = :MotorNoMesin WHERE NoTrans = :SKNo;
						Update ttpbit SET MotorAutoN =:MotorAutoN , MotorNoMesin = :MotorNoMesin WHERE PBNo = :PBNo ; 
						Update ttsmit SET MotorAutoN =:MotorAutoN , MotorNoMesin = :MotorNoMesin WHERE SMNo = :PBNo ;
						Update tvmotorlokasi SET MotorAutoN =:MotorAutoN , MotorNoMesin = :MotorNoMesin WHERE NoTrans = :PBNo ;
						", [
						':SKNo'         => $txtSKNo,
						':MotorAutoN'   => $MyAutoN,
						':MotorNoMesin' => $MyNoMesin,
						':PBNo'         => str_replace( 'SK', 'MO', $txtSKNo )
					] )->execute();
				}
			}
		} catch ( Exception $e ) {
			throw new $e;
		}
	}
	public function HapusMotorSKDKNo( $Number, &$txtSKNo ) {
		try {
			$MySKNo  = null;
			$HasilSK = false;
			$Hapus   = false;
			$MySKNo  = \Yii::$app->db->createCommand( "Select SKNo FROM tmotor WHERE  DKNo = :DKNo", [
				':DKNo' => $Number,
			] )->queryScalar();
			if ( $MySKNo != null ) {
				if ( $MySKNo != "--" ) {
					$HasilSK = true;
				}
			}
			\Yii::$app->db->createCommand( "Update tmotor SET SKNo = '--' WHERE  DKNo = :DKNo", [
				':DKNo' => $Number,
			] )->execute();
			\Yii::$app->db->createCommand( "Update tmotor SET DKNo = '--' WHERE  DKNo = :DKNo", [
				':DKNo' => $Number,
			] )->execute();
			if ( $HasilSK ) {
				$txtSKNo = $MySKNo;
			} else {
				\Yii::$app->db->createCommand( "DELETE FROM ttsk  WHERE SKNo = :SKNo", [
					':SKNo' => $txtSKNo,
				] )->execute();
				$txtSKNo = "--";
			}
		} catch ( Exception $e ) {
			throw new $e;
		}
		$Hapus = true;
		return $Hapus;
	}
	public function EditSTNK( $CusNama, $STNKAlamat, $MyNoMesin, $MyAutoN ) {
		try {
			\Yii::$app->db->createCommand( "UPDATE tmotor SET  STNKNama = :CusNama, STNKAlamat = LEFT(:STNKAlamat,150) 
				WHERE MotorAutoN = :MotorAutoN AND MotorNoMesin = :MotorNoMesin ", [
				':CusNama'      => $CusNama,
				':STNKAlamat'   => $STNKAlamat,
				':MotorAutoN'   => $MyAutoN,
				':MotorNoMesin' => $MyNoMesin
			] )->execute();
		} catch ( Exception $e ) {
			throw new $e;
		}
	}
	public function SelectKMBMDK( $DKNo, $INNo ) {
		try {
			return \Yii::$app->db->createCommand( "SELECT  KMJenis,  SUM(KMNominal) AS KMNominal, MAX(KMNo) AS KMNo, MAX(KMTgl) AS KMTgl FROM
		      (SELECT KMNo, KMLINK , ttkm.KMJenis, 'KM' AS Via , KMNominal, KMTgl, KMJam, NoGL  FROM ttkm 
		      WHERE KMLink = :DKNo  UNION 
		      SELECT KMNo, KMLINK, ttkm.KMJenis, 'KM' AS Via, KMNominal, KMTgl, KMJam, NoGL FROM ttkm 
		      INNER JOIN ttin ON INNo = KMLink
		      WHERE DKNo = :DKNo  UNION 
		      SELECT ttbmit.BMNo AS KMNo , ttbmit.DKNo AS KMLINK, ttbmhd.BMJenis AS KMJenis, 'BM' AS Via, ttbmit.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, NoGL FROM ttbmhd INNER JOIN ttbmit ON ttbmhd.BMNo = ttbmit.BMNo  
		      WHERE ttbmit.DKNo = :DKNo UNION      
		      SELECT ttbmitpm.BMNo AS KMNo , ttbmitpm.DKNo AS KMLINK, ttbmitpm.BMJenis AS KMJenis, 'BM' AS Via, ttbmitpm.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, NoGL FROM ttbmhd INNER JOIN ttbmitpm ON ttbmhd.BMNo = ttbmitpm.BMNo 
		      WHERE ttbmitpm.BMJenis IN ('Piutang UM','Tunai','Kredit','Inden','Piutang Sisa','Leasing','Scheme') 
		      AND ttbmitpm.DKNo = :DKNo ) a
		      GROUP BY KMJenis", [
				':DKNo' => $DKNo
			] )->queryAll();
		} catch ( Exception $e ) {
			throw new $e;
		}
	}
	public function SelectKKBKDK( $DKNo ) {
		try {
			return \Yii::$app->db->createCommand( " SELECT  KKJenis,  SUM(KKNominal) AS KKNominal, MAX(KKNo) AS KKNo, MAX(KKTgl) AS KKTgl, MAX(KKjam) AS KKJam FROM
      (
         SELECT ttBKit.BKNo AS KKNo , ttBKit.FBNo AS KKLINK, ttBKhd.BKJenis AS KKJenis, 'BK' AS Via, ttBKit.BKBayar AS KKNominal, ttBKhd.BKTgl AS KKTgl, ttBKhd.BKJam AS KKjam, ttBKhd.NoGL
         FROM ttBKhd
         INNER JOIN ttBKit ON ttBKhd.BKNo = ttBKit.BKNo
         WHERE ttBKit.FBNo = :DKNo
         UNION
         SELECT ttbmitpm.BMNo AS KKNo , ttbmitpm.DKNo AS KKLINK, ttbmitpm.BMJenis AS KKJenis, 'BM' AS Via, ttbmitpm.BMBayar AS KKNominal, ttbmhd.BMTgl AS KKTgl, ttbmhd.BMJam AS KKjam, ttBmhd.NoGL
         FROM ttbmhd
         INNER JOIN ttbmitpm ON ttbmhd.BMNo = ttbmitpm.BMNo WHERE ttbmitpm.BMJenis IN ('Retur Harga','SubsidiDealer2','Insentif Sales')
         AND ttbmitpm.DKNo = :DKNo  
         UNION
         SELECT KKNo, KKLINK , ttKK.KKJenis, 'KK' AS Via , KKNominal, KKTgl, KKJam, NoGL  
         FROM ttKK
         WHERE KKLINK = :DKNo 
         UNION
         SELECT ttkk.KKNo, ttkkit.FBNo AS KKLink , ttKK.KKJenis, 'KK' AS Via , ttkkit.KKBayar AS KKNominal, KKTgl, KKJam, NoGL  
         FROM ttkk INNER JOIN ttkkit ON (ttkk.KKNo = ttkkit.KKNo)
         WHERE ttkkit.FBNo  = :DKNo 
      ) a
      GROUP BY KKJenis", [
				':DKNo' => $DKNo
			] )->queryAll();
		} catch ( Exception $e ) {
			throw new $e;
		}
	}
	public function SelectKMBMIN( $INNo ) {
		try {
			return \Yii::$app->db->createCommand( " SELECT  KMJenis,  SUM(KMNominal) AS KMNominal, MAX(KMNo) AS KMNo, MAX(KMTgl) AS KMTgl FROM
      (SELECT KMNo, KMLINK , ttkm.KMJenis, 'KM' AS Via , KMNominal, KMTgl, KMJam, NoGL  FROM ttkm 
      WHERE KMLink <> '' AND  ttkm.KMJenis = 'Inden' AND  KMLink = :INNo UNION 
      SELECT ttbmit.BMNo AS KMNo , ttbmit.DKNo AS KMLINK, ttbmhd.BMJenis AS KMJenis, 'BM' AS Via, ttbmit.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, NoGL FROM ttbmhd INNER JOIN ttbmit ON ttbmhd.BMNo = ttbmit.BMNo  
      WHERE ttbmit.DKNo <> '' AND  ttbmhd.BMJenis = 'Inden' AND  ttbmit.DKNo = :INNo UNION      
      SELECT ttbmitpm.BMNo AS KMNo , ttbmitpm.DKNo AS KMLINK, ttbmitpm.BMJenis AS KMJenis, 'BM' AS Via, ttbmitpm.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, NoGL FROM ttbmhd INNER JOIN ttbmitpm ON ttbmhd.BMNo = ttbmitpm.BMNo 
      WHERE ttbmitpm.BMJenis IN ('Piutang UM','Tunai','Kredit','Inden','Piutang Sisa','Leasing','Scheme') 
      AND ttbmitpm.DKNo  <> '' AND  ttbmitpm.BMJenis = 'Inden' AND  ttbmitpm.DKNo = :INNo) a
      GROUP BY KMJenis", [
				':INNo' => $INNo
			] )->queryAll();
		} catch ( Exception $e ) {
			throw new $e;
		}
	}
	public function GetJoinData() {
//        $subqueryjoin = ( new \yii\db\Query() )
//            ->select( new Expression(
//                'vtkm.KMLink, vtkm.KMJenis, IFNULL(SUM(vtkm.KMNominal), 0) AS Kredit'
//            ) )
//            ->from( 'vtkm' )
//            ->where(['vtkm.KMJenis' => 'Kredit'])
//            ->groupBy( 'vtkm.KMLink, vtkm.KMJenis' );
		return $this->select( new Expression( "ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama,ttdk.LeaseKode,
            CONCAT_WS(' - ',CusAlamat,'RT/RW :' ,CusRT,'/' ,CusRW,CusKelurahan,CusKecamatan,CusKabupaten) AS CusAlamat,
            CONCAT_WS(' - ',MotorType,MotorWarna,MotorTahun,MotorNoMesin,MotorNoRangka) AS MotorType,
            ttdk.DKDPTotal, ttdk.DKHarga, ttdk.ProgramSubsidi " ) )
		            ->join( "INNER JOIN", "tmotor", "ttdk.DKNo = tmotor.DKNo" )
		            ->join( "INNER JOIN", "tdcustomer", "ttdk.CusKode = tdcustomer.CusKode " );
	}
	public function TtdkGetJoinData() {
		return $this->select( new Expression( "ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tdmotortype.MotorNama, tmotor.MotorType, tmotor.MotorWarna,
                                tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tdcustomer.CusAlamat, tdcustomer.CusRT, 
                                tdcustomer.CusRW, tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, tdarea.Provinsi, 
                                ttdk.DKDPTotal, ttdk.DKHarga, ttdk.ProgramSubsidi, ttdk.LeaseKode " ) )
		            ->join( "INNER JOIN", "tmotor", "ttdk.DKNo = tmotor.DKNo" )
		            ->join( "INNER JOIN", "tdmotortype", "tdmotortype.MotorType = tmotor.MotorType " )
		            ->join( "INNER JOIN", "tdcustomer", "ttdk.CusKode = tdcustomer.CusKode " )
		            ->join( "INNER JOIN", "tdarea", "tdcustomer.CusKabupaten = tdarea.Kabupaten " );
	}
	public function callSP( $post ) {
		$post[ 'UserID' ]           = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]        = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]       = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'DKNo' ]             = $post[ 'DKNo' ] ?? '--';
		$post[ 'DKNoBaru' ]         = $post[ 'DKNoView' ] ?? '--';
		$post[ 'SalesKode' ]        = $post[ 'SalesKode' ] ?? '--';
		$post[ 'TeamKode' ]         = $post[ 'TeamKode' ] ?? '--';
		$post[ 'DKTgl' ]            = date('Y-m-d', strtotime($post[ 'DKTgl' ] ?? date( 'Y-m-d' )));
		$post[ 'INNo' ]             = $post[ 'INNo' ] ?? '--';
		$post[ 'LeaseKode' ]        = $post[ 'LeaseKode' ] ?? '--';
		$post[ 'SPKNo' ]            = $post[ 'SPKNo' ] ?? '--';
		$post[ 'DKSurveyor' ]       = $post[ 'DKSurveyor' ] ?? '--';
		$post[ 'SKNo' ]             = $post[ 'SKNo' ] ?? '--';
		$post[ 'CusNama' ]          = $post[ 'CusNama' ] ?? '--';
		$post[ 'CusKode' ]          = $post[ 'CusKode' ] ?? '--';
		$post[ 'CusKTP' ]           = $post[ 'CusKTP' ] ?? '--';
		$post[ 'CusKabupaten' ]     = $post[ 'CusKabupaten' ] ?? '--';
		$post[ 'CusAlamat' ]        = $post[ 'CusAlamat' ] ?? '--';
		$post[ 'CusRT' ]            = $post[ 'CusRT' ] ?? '--';
		$post[ 'CusRW' ]            = $post[ 'CusRW' ] ?? '--';
		$post[ 'CusKecamatan' ]     = $post[ 'CusKecamatan' ] ?? '--';
		$post[ 'CusTelepon' ]       = $post[ 'CusTelepon' ] ?? '--';
		$post[ 'CusStatusHP' ]      = $post[ 'CusStatusHP' ] ?? '--';
		$post[ 'CusKodePos' ]       = $post[ 'CusKodePos' ] ?? '--';
		$post[ 'CusKelurahan' ]     = $post[ 'CusKelurahan' ] ?? '--';
		$post[ 'CusStatusRumah' ]   = $post[ 'CusStatusRumah' ] ?? '--';
		$post[ 'CusKodeKons' ]      = $post[ 'CusKodeKons' ] ?? '--';
		$post[ 'CusSex' ]           = $post[ 'CusSex' ] ?? '--';
		$post[ 'CusTempatLhr' ]     = $post[ 'CusTempatLhr' ] ?? '--';
		$post[ 'CusTglLhr' ]        = date('Y-m-d', strtotime($post[ 'CusTglLhr' ] ?? date( 'Y-m-d' )));
		$post[ 'CusAgama' ]         = $post[ 'CusAgama' ] ?? '--';
		$post[ 'CusPekerjaan' ]     = $post[ 'CusPekerjaan' ] ?? '--';
		$post[ 'CusPendidikan' ]    = $post[ 'CusPendidikan' ] ?? '--';
		$post[ 'CusPengeluaran' ]   = $post[ 'CusPengeluaran' ] ?? '--';
		$post[ 'CusPembeliNama' ]   = $post[ 'CusPembeliNama' ] ?? '--';
		$post[ 'CusPembeliKTP' ]    = $post[ 'CusPembeliKTP' ] ?? '--';
		$post[ 'CusKK' ]            = $post[ 'CusKK' ] ?? '--';
		$post[ 'ROCount' ]          = floatval( $post[ 'ROCount' ] ?? 0 );
		$post[ 'CusPembeliAlamat' ] = $post[ 'CusPembeliAlamat' ] ?? '--';
		$post[ 'DKMerk' ]           = $post[ 'DKMerk' ] ?? '--';
		$post[ 'DKMotorUntuk' ]     = $post[ 'DKMotorUntuk' ] ?? '--';
		$post[ 'DKJenis' ]          = $post[ 'DKJenis' ] ?? '--';
		$post[ 'DKMotorOleh' ]      = $post[ 'DKMotorOleh' ] ?? '--';
		$post[ 'MotorType' ]        = $post[ 'MotorType' ] ?? '--';
		$post[ 'MotorTahun' ]       = floatval( $post[ 'MotorTahun' ] ?? 1900 );
		$post[ 'MotorWarna' ]       = $post[ 'MotorWarna' ] ?? '--';
		$post[ 'DKSCP' ]            = floatval( $post[ 'DKSCP' ] ?? 0 );
		$post[ 'MotorNoMesin' ]     = $post[ 'MotorNoMesin' ] ?? '--';
		$post[ 'MotorNoRangka' ]    = $post[ 'MotorNoRangka' ] ?? '--';
		$post[ 'DKScheme' ]         = floatval( $post[ 'DKScheme' ] ?? 0 );
		$post[ 'MotorKategori' ]    = $post[ 'MotorKategori' ] ?? '--';
		$post[ 'DKScheme2' ]        = floatval( $post[ 'DKScheme2' ] ?? 0 );
		$post[ 'ProgramNama' ]      = $post[ 'ProgramNama' ] ?? '--';
		$post[ 'DKPengajuanBBN' ]   = $post[ 'DKPengajuanBBN' ] ?? '--';
		$post[ 'NDPPO' ]            = floatval( $post[ 'NDPPO' ] ?? 0 );
		$post[ 'DKHarga' ]          = floatval( $post[ 'DKHarga' ] ?? 0 );
		$post[ 'PrgSubsSupplier' ]  = floatval( $post[ 'PrgSubsSupplier' ] ?? 0 );
		$post[ 'PrgSubsDealer' ]    = floatval( $post[ 'PrgSubsDealer' ] ?? 0 );
		$post[ 'PrgSubsFincoy' ]    = floatval( $post[ 'PrgSubsFincoy' ] ?? 0 );
		$post[ 'ProgramSubsidi' ]   = floatval( $post[ 'ProgramSubsidi' ] ?? 0 );
		$post[ 'DKDPLLeasing' ]     = floatval( $post[ 'DKDPLLeasing' ] ?? 0 );
		$post[ 'DKDPInden' ]        = floatval( $post[ 'DKDPInden' ] ?? 0 );
		$post[ 'DKDPTerima' ]       = floatval( $post[ 'DKDPTerima' ] ?? 0 );
		$post[ 'DKDPTotal' ]        = floatval( $post[ 'DKDPTotal' ] ?? 0 );
		$post[ 'ReturHarga' ]       = floatval( $post[ 'ReturHarga' ] ?? 0 );
		$post[ 'DKMediator' ]       = $post[ 'DKMediator' ] ?? '--';
		$post[ 'PotonganHarga' ]    = floatval( $post[ 'PotonganHarga' ] ?? 0 );
		$post[ 'DKNetto' ]          = floatval( $post[ 'DKNetto' ] ?? 0 );
		$post[ 'InsentifSales' ]    = floatval( $post[ 'InsentifSales' ] ?? 0 );
		$post[ 'BBN' ]              = floatval( $post[ 'BBN' ] ?? 0 );
		$post[ 'BBNPlus' ]          = floatval( $post[ 'BBNPlus' ] ?? 0 );
		$post[ 'DKTenor' ]          = floatval( $post[ 'DKTenor' ] ?? 0 );
		$post[ 'PotonganKhusus' ]   = floatval( $post[ 'PotonganKhusus' ] ?? 0 );
		$post[ 'Jaket' ]            = floatval( $post[ 'Jaket' ] ?? 0 );
		$post[ 'NamaHadiah' ]       = $post[ 'NamaHadiah' ] ?? '--';
		$post[ 'DKAngsuran' ]       = floatval( $post[ 'DKAngsuran' ] ?? 0 );
		$post[ 'DKMemo' ]           = $post[ 'DKMemo' ] ?? '--';
		$post[ 'Hadiah2' ]          = $post[ 'Hadiah2' ] ?? '--';
		$post[ 'DKTOP' ]            = floatval( $post[ 'DKTOP' ] ?? 0 );
		$post[ 'DKJenisSblmnya' ]   = $post[ 'DKJenisSblmnya' ] ?? '--';
		$post[ 'CusEmail' ]         = $post[ 'CusEmail' ] ?? '';
		$post[ 'CusTelepon2' ]      = $post[ 'CusTelepon2' ] ?? '';
		$post[ 'CusNPWP' ]          = $post[ 'CusNPWP' ] ?? '';
		$post[ 'MotorAutoN' ]       = $post[ 'MotorAutoN' ] ?? 0 ;
		$post[ 'ZidSpk' ]       = $post[ 'ZidSpk' ] ?? '--';
		$post[ 'ZidProspect' ]       = $post[ 'ZidProspect' ] ?? '--';
		$post[ 'DPTOP' ]       = $post[ 'DPTOP' ] ?? '--';
		try {
			General::cCmd( "SET @DKNo = ?;" )->bindParam( 1, $post[ 'DKNo' ] )->execute();
			General::cCmd( "CALL fdk(?,@Status,@Keterangan,?,?,?,@DKNo,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,
		?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'DKNoBaru' ] )
			       ->bindParam( 6, $post[ 'SalesKode' ] )
			       ->bindParam( 7, $post[ 'TeamKode' ] )
			       ->bindParam( 8, $post[ 'DKTgl' ] )
			       ->bindParam( 9, $post[ 'INNo' ] )
			       ->bindParam( 10, $post[ 'LeaseKode' ] )
			       ->bindParam( 11, $post[ 'SPKNo' ] )
			       ->bindParam( 12, $post[ 'DKSurveyor' ] )
			       ->bindParam( 13, $post[ 'SKNo' ] )
			       ->bindParam( 14, $post[ 'CusNama' ] )
			       ->bindParam( 15, $post[ 'CusKode' ] )
			       ->bindParam( 16, $post[ 'CusKTP' ] )
			       ->bindParam( 17, $post[ 'CusKabupaten' ] )
			       ->bindParam( 18, $post[ 'CusAlamat' ] )
			       ->bindParam( 19, $post[ 'CusRT' ] )
			       ->bindParam( 20, $post[ 'CusRW' ] )
			       ->bindParam( 21, $post[ 'CusKecamatan' ] )
			       ->bindParam( 22, $post[ 'CusTelepon' ] )
			       ->bindParam( 23, $post[ 'CusStatusHP' ] )
			       ->bindParam( 24, $post[ 'CusKodePos' ] )
			       ->bindParam( 25, $post[ 'CusKelurahan' ] )
			       ->bindParam( 26, $post[ 'CusStatusRumah' ] )
			       ->bindParam( 27, $post[ 'CusKodeKons' ] )
			       ->bindParam( 28, $post[ 'CusSex' ] )
			       ->bindParam( 29, $post[ 'CusTempatLhr' ] )
			       ->bindParam( 30, $post[ 'CusTglLhr' ] )
			       ->bindParam( 31, $post[ 'CusAgama' ] )
			       ->bindParam( 32, $post[ 'CusPekerjaan' ] )
			       ->bindParam( 33, $post[ 'CusPendidikan' ] )
			       ->bindParam( 34, $post[ 'CusPengeluaran' ] )
			       ->bindParam( 35, $post[ 'CusPembeliNama' ] )
			       ->bindParam( 36, $post[ 'CusPembeliKTP' ] )
			       ->bindParam( 37, $post[ 'CusKK' ] )
			       ->bindParam( 38, $post[ 'ROCount' ] )
			       ->bindParam( 39, $post[ 'CusPembeliAlamat' ] )
			       ->bindParam( 40, $post[ 'DKMerk' ] )
			       ->bindParam( 41, $post[ 'DKMotorUntuk' ] )
			       ->bindParam( 42, $post[ 'DKJenis' ] )
			       ->bindParam( 43, $post[ 'DKMotorOleh' ] )
			       ->bindParam( 44, $post[ 'MotorType' ] )
			       ->bindParam( 45, $post[ 'MotorTahun' ] )
			       ->bindParam( 46, $post[ 'MotorWarna' ] )
			       ->bindParam( 47, $post[ 'DKSCP' ] )
			       ->bindParam( 48, $post[ 'MotorNoMesin' ] )
			       ->bindParam( 49, $post[ 'MotorNoRangka' ] )
			       ->bindParam( 50, $post[ 'DKScheme' ] )
			       ->bindParam( 51, $post[ 'MotorNama' ] )
			       ->bindParam( 52, $post[ 'MotorKategori' ] )
			       ->bindParam( 53, $post[ 'DKScheme2' ] )
			       ->bindParam( 54, $post[ 'ProgramNama' ] )
			       ->bindParam( 55, $post[ 'DKPengajuanBBN' ] )
			       ->bindParam( 56, $post[ 'NDPPO' ] )
			       ->bindParam( 57, $post[ 'DKHarga' ] )
			       ->bindParam( 58, $post[ 'PrgSubsSupplier' ] )
			       ->bindParam( 59, $post[ 'PrgSubsDealer' ] )
			       ->bindParam( 60, $post[ 'PrgSubsFincoy' ] )
			       ->bindParam( 61, $post[ 'ProgramSubsidi' ] )
			       ->bindParam( 62, $post[ 'DKDPLLeasing' ] )
			       ->bindParam( 63, $post[ 'DKDPInden' ] )
			       ->bindParam( 64, $post[ 'DKDPTerima' ] )
			       ->bindParam( 65, $post[ 'DKDPTotal' ] )
			       ->bindParam( 66, $post[ 'ReturHarga' ] )
			       ->bindParam( 67, $post[ 'DKMediator' ] )
			       ->bindParam( 68, $post[ 'PotonganHarga' ] )
			       ->bindParam( 69, $post[ 'DKNetto' ] )
			       ->bindParam( 70, $post[ 'InsentifSales' ] )
			       ->bindParam( 71, $post[ 'BBN' ] )
			       ->bindParam( 72, $post[ 'BBNPlus' ] )
			       ->bindParam( 73, $post[ 'DKTenor' ] )
			       ->bindParam( 74, $post[ 'PotonganKhusus' ] )
			       ->bindParam( 75, $post[ 'Jaket' ] )
			       ->bindParam( 76, $post[ 'NamaHadiah' ] )
			       ->bindParam( 77, $post[ 'DKAngsuran' ] )
			       ->bindParam( 78, $post[ 'DKMemo' ] )
			       ->bindParam( 79, $post[ 'Hadiah2' ] )
			       ->bindParam( 80, $post[ 'DKTOP' ] )
			       ->bindParam( 81, $post[ 'DKJenisSblmnya' ] )
			       ->bindParam( 82, $post[ 'CusEmail' ] )
			       ->bindParam( 83, $post[ 'CusTelepon2' ] )
			       ->bindParam( 84, $post[ 'CusNPWP' ] )
			       ->bindParam( 85, $post[ 'MotorAutoN' ] )
			       ->bindParam( 86, $post[ 'ZidSpk' ] )
			       ->bindParam( 87, $post[ 'ZidProspect' ] )
			       ->bindParam( 88, $post[ 'DPTOP' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @DKNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'DKNo'       => $post[ 'DKNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$DKNo       = $exe[ '@DKNo' ] ?? $post[ 'DKNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'DKNo'       => $DKNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
	public function callSPStatus( $post ) {
		$post[ 'UserID' ]           = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]        = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]       = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'DKNo' ]             = $post[ 'DKNo' ] ?? '--';
		$post[ 'DKNoBaru' ]         = $post[ 'DKNoBaru' ] ?? '--';
		$post[ 'DKPONo' ]           = $post[ 'DKPONo' ] ?? '--';
		$post[ 'DKPOTgl' ]          = $post[ 'DKPOTgl' ] ?? date( 'Y-m-d' );
		$post[ 'DKTglTagih' ]       = $post[ 'DKTglTagih' ] ?? date( 'Y-m-d' );
		$post[ 'DKMemo' ]           = $post[ 'DKMemo' ] ?? '--';
		$post[ 'CusKode' ]          = $post[ 'CusKode' ] ?? '--';
		$post[ 'CusPembeliNama' ]   = $post[ 'CusPembeliNama' ] ?? '--';
		$post[ 'CusPembeliKTP' ]    = $post[ 'CusPembeliKTP' ] ?? '--';
		$post[ 'CusPembeliAlamat' ] = $post[ 'CusPembeliAlamat' ] ?? '--';
		try {
			General::cCmd( "SET @DKNo = ?;" )->bindParam( 1, $post[ 'DKNo' ] )->execute();
			General::cCmd( "CALL fdkstatus(?,@Status,@Keterangan,?,?,?,@DKNo,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'DKNoBaru' ] )
			       ->bindParam( 6, $post[ 'DKPONo' ] )
			       ->bindParam( 7, $post[ 'DKPOTgl' ] )
			       ->bindParam( 8, $post[ 'DKTglTagih' ] )
			       ->bindParam( 9, $post[ 'DKMemo' ] )
			       ->bindParam( 10, $post[ 'CusKode' ] )
			       ->bindParam( 11, $post[ 'CusPembeliNama' ] )
			       ->bindParam( 12, $post[ 'CusPembeliKTP' ] )
			       ->bindParam( 13, $post[ 'CusPembeliAlamat' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @DKNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'DKNo'       => $post[ 'DKNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$DKNo       = $exe[ '@DKNo' ] ?? $post[ 'DKNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'DKNo'       => $DKNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}

	public function callSPKG( $post ) {
		$post[ 'UserID' ]           = $post[ 'UserID' ] ?? Menu::getUserLokasi()[ 'UserID' ];
		$post[ 'KodeAkses' ]        = Menu::getUserLokasi()[ 'KodeAkses' ];
		$post[ 'LokasiKode' ]       = Menu::getUserLokasi()[ 'LokasiKode' ];
		$post[ 'KGNo' ]             = $post[ 'KGNo' ] ?? '--';
		$post[ 'KGNoBaru' ]         = $post[ 'KGNoBaru' ] ?? '--';
		$post[ 'KGTgl' ]            = date('Y-m-d', strtotime($post[ 'KGTgl' ] ?? date( 'Y-m-d' )));
		$post[ 'DKNo' ]             = $post[ 'DKNo' ] ?? '--';
		$post[ 'KGJenis' ]          = $post[ 'KGJenis' ] ?? '--';
		$post[ 'KGNominal' ]        = $post[ 'KGNominal' ] ?? 0;
		$post[ 'KGPengirim' ]       = $post[ 'KGPengirim' ] ?? '--';
		$post[ 'KGPenerima' ]       = $post[ 'KGPenerima' ] ?? '--';
		$post[ 'KGMemo' ]           = $post[ 'KGMemo' ] ?? '--';
		$post[ 'KGStatus' ]         = $post[ 'KGStatus' ] ?? '--';
		try {
			General::cCmd( "SET @KGNo = ?;" )->bindParam( 1, $post[ 'KGNo' ] )->execute();
			General::cCmd( "CALL fkg(?,@Status,@Keterangan,?,?,?,@KGNo,?,?,?,?,?,?,?,?,?);" )
			       ->bindParam( 1, $post[ 'Action' ] )
			       ->bindParam( 2, $post[ 'UserID' ] )
			       ->bindParam( 3, $post[ 'KodeAkses' ] )
			       ->bindParam( 4, $post[ 'LokasiKode' ] )
			       ->bindParam( 5, $post[ 'KGNoBaru' ] )
			       ->bindParam( 6, $post[ 'KGTgl' ] )
			       ->bindParam( 7, $post[ 'DKNo' ] )
			       ->bindParam( 8, $post[ 'KGJenis' ] )
			       ->bindParam( 9, $post[ 'KGNominal' ] )
			       ->bindParam( 10, $post[ 'KGPengirim' ] )
			       ->bindParam( 11, $post[ 'KGPenerima' ] )
			       ->bindParam( 12, $post[ 'KGMemo' ] )
			       ->bindParam( 13, $post[ 'KGStatus' ] )
			       ->execute();
			$exe = General::cCmd( "SELECT @KGNo,@Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'KGNo'       => $post[ 'KGNo' ],
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$KGNo       = $exe[ '@KGNo' ] ?? $post[ 'KGNo' ];
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [' . $post[ 'Action' ] . '] Keterangan NULL';
		return [
			'KGNo'       => $KGNo,
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		];
	}
}
