<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);
return [
    'id' => 'app-aunit',
    'name' => 'HSYS – Unit',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'aunit\controllers',
    'components' => [
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
    ],
//	'as access' => [
//		'class' => \yii\filters\AccessControl::className(),
//		'rules' => [
//			[
//				'actions' => ['login', 'error','debug/*'],
//				'allow' => true,
//			],
//			[
//				'actions' => ['logout', 'index'],
//				'allow' => true,
//				'roles' => ['@'],
//			],
//		],
//	],
    'params' => $params,
];
