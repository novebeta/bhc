<?php
namespace aunit\components;
use common\components\Custom;
use Yii;
use yii\bootstrap\Html;
use yii\db\ActiveRecord;
use yii\helpers\Url;
class TUi {
	public static $actionMode = Menu::DISPLAY;
	/**
	 * @param array $body
	 * @param array $footer
	 * @param array $components
	 */
	static function form( $body = [], $footer = [], $components = [], $options = [] ) {
		$defaultComponen__ = [
			":btnSave"   => Html::button( '<i class="glyphicon glyphicon-floppy-disk"> <br><br>Simpan</i>', [ 'class' => 'btn btn-info btn-primary ', 'id' => 'btnSave' . Custom::viewClass() ] ),
			":btnCancel" => Html::Button( '<i class="fa fa-ban"><br><br> Batal</i>', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px' ] ),
			":btnAdd"    => Html::button( '<i class="fa fa-plus-circle"><br><br> Tambah</i>', [ 'class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:60px;height:60px' ] ),
			":btnEdit"   => Html::button( '<i class="fa fa-edit"><br><br> Edit</i>', [ 'class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px' ] ),
			":btnDaftar" => Html::Button( '<i class="fa fa-list"><br><br> Daftar</i> ', [ 'class' => 'btn btn-primary ', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px' ] ),
			":btnPrint"  => Html::button( '<i class="glyphicon glyphicon-print"><br><br>  Print </i>', [ 'class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px' ] ),
			":btnJurnal" => Html::button( '<i class="fa fa-balance-scale"> <br><br> Jurnal </i>', [ 'class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px' ] ),
            ":btnDelete" => Html::button( '<i class="glyphicon glyphicon-trash"><br><br>Hapus</i> ', [ 'class' => 'btn bg-dark', 'id' => 'btnDelete', 'style' => 'margin-right:2px;width:60px;height:60px;background-color:grey;color:white' ] ),
            ":btnBukuServis"  => Html::button( '<i class="fa fa-vcard"></i> Buku Servis ', [ 'class' => 'btn bg-navy ', 'id' => 'btnBukuServis', 'style' => 'width:120px' ] ),
		];
		$url               = $options[ 'url_main' ] ?? '';
		$url_prefix        = $options[ 'url_prefix' ] ?? '';
		$url_extra 		   = $options[ 'url_extra' ] ?? [];
		unset($options[ 'url_extra' ]);
		$url_index         = $options[ 'url_index' ] ?? Url::toRoute( array_merge([ "{$url}/{$url_prefix}", 'id' => $options['url_id'] ?? '' ],$url_extra) );
		$url_add           = $options[ 'url_add' ] ?? Url::toRoute( array_merge([ "{$url}/{$url_prefix}create" ],$url_extra) );
		$url_update        = $options[ 'url_update' ] ?? Url::toRoute( array_merge([ "{$url}/{$url_prefix}update", 'action' => 'update', 'id' => $options['url_id'] ?? ''],$url_extra) );
		$url_print         = $options[ 'url_print' ] ?? Url::toRoute( array_merge([ "{$url}/{$url_prefix}print", 'id' => $options['url_id'] ?? '' ],$url_extra) );
		$url_cancel        = $options[ 'url_cancel' ] ?? Url::toRoute( array_merge([ "{$url}/{$url_prefix}cancel" , 'id' => $options['url_id'] ?? '','action' => $_GET[ 'action' ] ?? 'view' ],$url_extra) );
		$url_jurnal        = $options[ 'url_jurnal' ] ?? Url::toRoute( array_merge([ "{$url}/{$url_prefix}jurnal", 'id' => $options['url_id'] ?? '' ] ,$url_extra));
        $url_delete        = $options[ 'url_delete' ] ?? Url::toRoute( array_merge([ "{$url}/{$url_prefix}delete", 'id' => $options['url_id'] ?? '' ],$url_extra) );
		$jsBtnCancel       = $options[ 'jsBtnCancel' ] ?? true;
		$jsBtnAdd          = $options[ 'jsBtnAdd' ] ?? true;
		$jsBtnEdit         = $options[ 'jsBtnEdit' ] ?? true;
		$jsBtnDaftar       = $options[ 'jsBtnDaftar' ] ?? true;
		$jsBtnPrint        = $options[ 'jsBtnPrint' ] ?? true;
		$jsBtnJurnal       = $options[ 'jsBtnJurnal' ] ?? true;
        $jsBtnDelete       = $options[ 'jsBtnDelete' ] ?? true;
		$components = array_merge( $defaultComponen__, $components );
		$bodyHtml   = self::renderItems( $body );
		$footerHtml = self::renderItems( $footer );
		$footerHtml = self::renderButtonAction( $footerHtml , $options['_akses'] ?? null);
		foreach ( $components as $k => $v ) {
			$bodyHtml   = str_replace( $k, $v, $bodyHtml );
			$footerHtml = str_replace( $k, $v, $footerHtml );
		}
		$htmlFooter = count( $footer ) ? "<div class=\"box-footer\">$footerHtml</div>" : "";
		$options = count( $options ) ? $options : [
			'style' => "flex: 1 1 auto; margin-bottom: 0px;"
		];
		if ( array_key_exists( 'class', $options ) ) {
			$options[ 'class' ] = "box box-default " . $options[ 'class' ];
		} else {
			$options[ 'class' ] = "box box-default";
		}
		$htmlOptions = "";
		foreach ( $options as $k => $v ) {
			$htmlOptions .= " $k=\"$v\"";
		}
		echo <<< HTML
        <div $htmlOptions>
            <div class="box-body">$bodyHtml</div>
            $htmlFooter
        </div>
HTML;

		if ( self::$actionMode == Menu::VIEW || self::$actionMode == Menu::DISPLAY) {
			Yii::$app->getView()->registerJs( <<< JS
 		function blockAll(){
    		let form = $('form').not('#sidebar-form');
    		form.find('input,textarea,select').attr('disabled', true);
    		form.find('.box-body').find('button').attr('disabled',true);
 		}
 		//blockAll();
 		
JS
			);
		}

        if ( $jsBtnDelete ) {
            Yii::$app->getView()->registerJs( <<< JS
            $('#btnDelete').click(function (event) {	
                var form = $(this).parents('form:first');   
                bootbox.confirm({
                    title: 'Konfirmasi',
                    message: "Hapus data terpilih ?",
                    size: "small",
                    buttons: {
                        confirm: {
                            label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                            className: 'btn btn-warning'
                        },
                        cancel: {
                            label: '<span class="fa fa-ban"></span> Cancel',
                            className: 'btn btn-default'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            form.utilForm().submit("{$url_delete}","POST","");        
                        }
                    }
                });
            }); 
JS
            );
        }

	}
	/**
	 * @param string|array $items
	 *
	 * @return string
	 */
	static function renderItems( $items = [] ) {
		if ( ! is_array( $items ) ) {
			return $items;
		}
		if ( array_key_exists( 'items', $items )
		     || array_keys( $items ) !== range( 0, count( $items ) - 1 ) ) {
			return self::renderItem( $items );
		}
		$script = "";
		foreach ( $items as $item ) {
			$script .= self::renderItem( $item );
		}
		return $script;
	}
	private static function renderButtonAction( $html,$akses = null ) {
		if ( strpos( $html, ':btnAction' ) !== false ) {
			$hakAkses = $_SESSION['_akses'][$akses] ?? null;
			switch ( self::$actionMode ) {
				case Menu::ADD :
				case Menu::UPDATE :
                    $html = str_replace( ':btnBukuServis', '', $html );
//                    $html = str_replace( ':btnImport', '', $html );
					$html = str_replace( ':btnPrint', '', $html );
					$html = str_replace( ':btnJurnal', '', $html );
					$html = str_replace( ':btnAction', ':btnSave :btnCancel ', $html );
					break;
				case Menu::DISPLAY :
                    $html = str_replace( ':btnImport', '', $html );
					$defaultBtn = ':btnAdd :btnEdit :btnDelete';
					$btnAction = "";
					if($hakAkses == null){
						$btnAction = $defaultBtn;
					}else{
						$btnAction .= ($hakAkses['MenuTambah'] ?? false) ? ':btnAdd ' : '';
						$btnAction .= ($hakAkses['MenuEdit'] ?? false) ? ':btnEdit ' : '';
						$btnAction .= ($hakAkses['MenuHapus'] ?? false) ? ':btnDelete ' : '';
					}
					$html = str_replace( ':btnAction', $btnAction.':btnDaftar', $html );
					break;
				case Menu::VIEW :
                    $html = str_replace( ':btnImport', '', $html );
					$html = str_replace( ':btnAction', ':btnDaftar', $html );
					break;
			}
		}
		return $html;
	}


	/**
	 * @param string|array $item
	 *
	 * @return string
	 */
	private static function renderItem( $item ) {
		if ( ! is_array( $item ) ) {
			return $item;
		} elseif ( array_key_exists( 'template', $item ) ) {
			return self::renderInput( $item );
		}
		$tag        = "div";
		$attributes = [];
		$class      = "";
		$items      = "";
		$innerHtml  = "";
		foreach ( $item as $k => $v ) {
			switch ( $k ) {
				case 'tag':
					$tag = $v;
					break;
				case 'items':
					$items = self::renderItems( $v );
					break;
				case 'class':
				case 'responsive':
					$class .= " $v";
					break;
				case 'text':
				case 'html':
					$innerHtml = is_array( $v ) ? implode( " ", $v ) : $v;
					break;
				default:
					$attributes[] = "$k='$v'";
			}
		}
		$is_inputTag = $tag === 'input';
		return "<$tag class='$class' " . implode( " ", $attributes ) . ">" . ( $is_inputTag ? "" : "$innerHtml$items</$tag>" );
	}
	public static $fieldCounter = 0;
	/**
	 * @param array $item
	 *
	 * @return string
	 */
	private static function renderInput( $item ) {
		$is_label = $item[ 'template' ] === 'label';
		$begin    = "<div class='form-group' style='padding-right:0 !important; padding-left:0 !important;'>";
		$end      = "</div>";
		if ( isset( $item[ 'responsive' ] ) && $item[ 'responsive' ] ) {
			$begin = "<div class='form-group " . $item[ 'responsive' ] . "' style='padding-right:0 !important; padding-left:0 !important;'>";
		}
		if ( ! isset( $item[ 'id' ] ) ) {
			$item[ 'id' ] = "field" . self::$fieldCounter ++;
		}
		if ( ! isset( $item[ 'label' ] ) ) {
			$item[ 'label' ] = "";
		}
		if ( ! $is_label ) {
			$begin .= "<label class='" . ( $item[ 'label' ] ? "control-label" : "sr-only" ) . "' for='" . $item[ 'id' ] . "'>" . $item[ 'label' ] . "</label>";
		}
		$attributes = [];
		$class      = $is_label ? "" : "form-control";
		$innerHtml  = "";
		foreach ( $item as $k => $v ) {
			switch ( $k ) {
				case 'items':
				case 'template':
				case 'tag':
				case 'responsive':
				case 'label':
					break;
				case 'class':
					$class .= " $v";
					break;
				case 'text':
				case 'innerHtml':
					$innerHtml = $v;
					break;
				default:
					$attributes[] = "$k='$v'";
			}
		}
		$field = "";
		switch ( $item[ 'template' ] ) {
			case 'label':
				$field = "<label class='$class' " . implode( " ", $attributes ) . ">$innerHtml</label>";
				break;
			case 'text':
				$field = "<input type='" . $item[ 'template' ] . "' class='$class' " . implode( " ", $attributes ) . ">";
				break;
			case 'date':
				$field = "<div class='input-group date' data-date-format='" . ( isset( $item[ 'format' ] ) ? $item[ 'format' ] : "dd/mm/yyyy" ) . "'>
                            <input  type='text' class='$class' " . implode( " ", $attributes ) . ">
                            <div class='input-group-addon' >
                                <span class='glyphicon glyphicon-th'></span>
                            </div>
                        </div>";
				break;
			case 'button':
				$field = "<button type='" . $item[ 'template' ] . "' class='$class' " . implode( " ", $attributes ) . ">$innerHtml</button>";
				break;
		}
		return $begin . $field . $end;
	}
	public static $jsSign = '~func~';
	public static $jsAtTop = [];
	static function arrayToObjJS( $array, $parseJS = false ) {
		$string = json_encode( $array );
		$string = preg_replace( '/"([^"]+)"\s*:\s*/', '$1:', $string );
		return $parseJS ? self::parseJS( $string ) : $string;
	}
	static function scriptJS( $string, $clearSpace = false ) {
		$string = preg_replace( '/(")/', '``', $string );
		return self::$jsSign . ( $clearSpace ? self::clearSpaceJS( $string ) : $string ) . self::$jsSign;
	}
	static function parseJS( $string, $clearSpace = false ) {
		$string = preg_replace( '/[\'"]' . self::$jsSign . '/', '', $string );
		$string = preg_replace( '/' . self::$jsSign . '[\'"]/', '', $string );
		$string = preg_replace( '/(``)/', '"', $string );
		return $clearSpace ? self::clearSpaceJS( $string ) : $string;
	}
	static function clearSpaceJS( $string ) {
		return trim( preg_replace( '/\s+/', ' ', $string ) );
	}
	public static $europeanNumberFields = [];
	/**
	 * @param ActiveRecord $model
	 * @param array $cols
	 *
	 * @return string|string[]|null
	 */
	private static function generateGridCols( $model, $cols = [] ) {
		$colModels   = [];
		$dbCols      = $model->getTableSchema()->columns;
		$dbColLabels = $model->attributeLabels();
		/* jika kosong, maka akan menampilkan semua kolom */
		if ( ! count( $cols ) ) {
			foreach ( $dbCols as $field => $v ) {
				$cols[] = [
					'name'     => $field,
					'index'    => $field,
					'editable' => true,
					'colMenu'  => true,
				];
			}
		}
		foreach ( $cols as $col ) {
			$colModel = [];
			foreach ( $col as $k => $v ) {
				if ( ! in_array( $k, [ 'name', 'index', 'label', 'sorttype', 'editable', 'renderer',
				                       'editor', 'edittype', 'template', 'editoptions', 'editoptions' ] ) ) {
					$colModel[ $k ] = $v;
				}
			}
			$field = array_key_exists( 'name', $col ) ? $col[ 'name' ] : $col[ 'index' ];
			$colModel[ 'name' ]  = array_key_exists( 'name', $col ) ? $col[ 'name' ] : $field;
			$colModel[ 'index' ] = array_key_exists( 'index', $col ) ? $col[ 'index' ] : $field;
			if ( array_key_exists( 'label', $col ) ) {
				$colModel[ 'label' ] = $col[ 'label' ];
			} elseif ( array_key_exists( $field, $dbColLabels ) ) {
				$colModel[ 'label' ] = $dbColLabels[ $field ];
			}
			if ( array_key_exists( 'sorttype', $col ) ) {
				$colModel[ 'sorttype' ] = $col[ 'sorttype' ];
			} elseif ( array_key_exists( $field, $dbCols ) ) {
				$colModel[ 'sorttype' ] = $dbCols[ $field ]->type;
			}
			if ( array_key_exists( 'width', $col ) ) {
				$colModel[ 'width' ] = $col[ 'width' ];
			}
			/* jika kolom dapat menerima input */
			if ( array_key_exists( 'editable', $col ) && $col[ 'editable' ] ) {
				$colModel[ 'editable' ] = true;
				$colModel               = array_merge( $colModel, self::colEditor( $col, array_key_exists( $field, $dbCols ) ? $dbCols[ $field ] : null, $colModel ) );
			}
			$colModels[] = json_encode( $colModel );
		}
		$stringColModels = implode( ', ', $colModels );
		$stringColModels = str_replace( '\n', '', $stringColModels );
		return self::parseJS( $stringColModels, true );
	}
	/**
	 * @param $col
	 * @param $dbCol
	 * @param array $colModel
	 *
	 * @return array
	 */
	private static function colEditor( $col, $dbCol, $colModel ) {
		$colEditor = [];
		$editrules     = [];
		$editoptions   = [];
		$formatoptions = [];
		if ( $dbCol ) {
			$editrules[ 'required' ] = ! $dbCol->allowNull;
		}
		$onInitFunction = isset( $col[ 'onInit' ] ) ? "(" . $col[ 'onInit' ] . ")(grid.jqGrid('getRowData', grid.jqGrid('getGridParam', 'selrow')));" : '';
		if ( array_key_exists( 'editor', $col ) ) {
			$editor = $col[ 'editor' ];
			/*
			 * custom editor attr
			 * type
			 * data
			 * placeholder
			 * onchange
			 */
			switch ( $editor[ 'type' ] ) {
				case 'select2':
					$colEditor[ 'edittype' ] = 'select';
					$editorOptions = [];
					if ( array_key_exists( 'dropdownAutoWidth', $editor ) ) {
						$editorOptions[ 'dropdownAutoWidth' ] = $editor[ 'dropdownAutoWidth' ];
					}
					if ( array_key_exists( 'dataArray', $editor ) ) {
						$editorOptions[ 'data' ] = is_array( $editor[ 'dataArray' ] ) ? $editor[ 'dataArray' ] : [];
					}
					if ( array_key_exists( 'ajax', $editor ) ) {
						$editorOptions[ 'ajax' ] = $editor[ 'ajax' ];
					}
					if ( array_key_exists( 'data', $editor ) ) {
						$editoptions[ 'value' ] = is_array( $editor[ 'data' ] ) ? implode( ";", $editor[ 'data' ] ) : $editor[ 'data' ];
					}
					if ( array_key_exists( 'readonly', $editor ) ) {
						$editoptions[ 'readonly' ] = "readonly";
					}
					$editorOptions             = self::arrayToObjJS( $editorOptions, true );
					$editoptions[ 'dataInit' ] = <<< JS
                            function(element) { $(element).select2($editorOptions); $onInitFunction}
JS;
					$editoptions[ 'dataInit' ] = self::scriptJS( $editoptions[ 'dataInit' ], true );
					break;
				case 'numbercontrol':
					self::$europeanNumberFields[] = $colModel[ 'name' ];
					$colEditor[ 'edittype' ] = 'text';
					$editorOptions = [];
					foreach ( $editor as $k => $v ) {
						if ( $k == 'type' ) {
							continue;
						}
						$editorOptions[ $k ] = $v;
					}
					if ( ! count( $editorOptions ) ) {
						$editorOptions = [
							'alias'          => 'numeric',
							'digits'         => 2,
							'groupSeparator' => '.',
							'radixPoint'     => ',',
							'autoGroup'      => true,
							'autoUnmask'     => false
						];
					}
					$editorOptions             = self::arrayToObjJS( $editorOptions, true );
					$editoptions[ 'dataInit' ] = <<< JS
                            function(element) { $(element).attr('style', 'text-align: right;').inputmask($editorOptions); $onInitFunction}
JS;
					$editoptions[ 'dataInit' ] = self::scriptJS( $editoptions[ 'dataInit' ], true );
					$colEditor[ 'formatter' ] = 'number';
					$colEditor[ 'align' ]     = 'right';
					$formatoptions[ 'decimalSeparator' ]   = isset( $editor[ 'decimalSeparator' ] ) ? $editor[ 'decimalSeparator' ] : ",";
					$formatoptions[ 'thousandsSeparator' ] = isset( $editor[ 'thousandsSeparator' ] ) ? $editor[ 'thousandsSeparator' ] : ".";
					$formatoptions[ 'decimalPlaces' ]      = isset( $editor[ 'decimalPlaces' ] ) ? $editor[ 'decimalPlaces' ] : 2;
					break;
			}
		} else if ( ! array_key_exists( 'edittype', $col ) ) {
			/* Jika editor tidak didefinisikan */
			$dbType = '';
			if ( $dbCol && preg_match( '/^[a-zA-Z]+/', $dbCol->dbType, $output_array ) ) {
				$dbType = $output_array[ 0 ];
			}
			switch ( $dbType ) {
				case 'varchar':
				case 'char':
					$colEditor[ 'edittype' ]    = 'text';
					$editoptions[ 'maxlength' ] = $dbCol->size;
					break;
				case 'text':
				case 'tinytext':
				case 'mediumtext':
				case 'longtext':
					$colEditor[ 'edittype' ] = 'textarea';
					break;
				case 'date':
					$colEditor[ 'edittype' ]   = 'text';
					$colEditor[ 'formatter' ]  = self::scriptJS( 'renderDate_ddmmyyyy' );
					$editoptions[ 'dataInit' ] = self::scriptJS( 'funcEditTypeDate' );
					break;
				case 'smallint':
				case 'mediumint':
				case 'int':
				case 'integer':
				case 'bigint':
				case 'decimal':
				case 'dec':
				case 'float':
				case 'double':
				case 'real':
					self::$europeanNumberFields[] = $colModel[ 'name' ];
					$colEditor[ 'edittype' ]  = 'text';
					$colEditor[ 'template' ]  = 'number';
					$editoptions[ 'type' ]    = 'number';
					$editoptions[ 'pattern' ] = '[0-9]+([\.|,][0-9]+)?';
					$editrules[ 'number' ]    = true;
					break;
				case 'enum':
					$colEditor[ 'edittype' ] = 'select';
					$editoptions[ 'value' ]  = $dbCol->enumValues;
					break;
				default :
					$colEditor[ 'edittype' ] = 'text';
			}
		}
		/* listener */
		if ( array_key_exists( 'onchange', $col ) ) {
			$editoptions[ 'dataEvents' ] = [ [ 'type' => 'change', 'fn' => self::scriptJS( $col[ 'onchange' ], true ) ] ];
		}
		if ( count( $editrules ) ) {
			$colEditor[ 'editrules' ] = $editrules;
		}
		if ( count( $editoptions ) ) {
			$colEditor[ 'editoptions' ] = $editoptions;
		}
		if ( count( $formatoptions ) ) {
			$colEditor[ 'formatoptions' ] = $formatoptions;
		}
		return $colEditor;
	}
	/**
	 * @param $model
	 * @param array $options
	 *
	 * @return string
	 */
	static function detailGridJs( $model, $options = [] ) {
		/* load options */
		/* Element */
		$element      = array_key_exists( 'element', $options ) ? $options[ 'element' ] : "#detailGrid";
		$elementPager = array_key_exists( 'pager', $options ) ? $options[ 'pager' ] : "#detailGridPager";
		/* readonly : TRUE means add, edit and delete buttons are removed */
		$readOnly      = array_key_exists( 'readonly', $options ) && $options[ 'readonly' ];
		$actionCol     = $readOnly ? '' : '{ label: "Actions", name: "actions", width: 65, formatter: actionsFormatter},';
		$gridNavButton = $readOnly ? '' : ".navGrid('#detailGridPager',{ edit:false,add:false,del:false,search:false,refresh:false }).navButtonAdd('#detailGridPager',{ caption:' Tambah', buttonicon:'glyphicon glyphicon-plus', onClickButton: addRow, position:'last' })";
		$gridContainer = array_key_exists( 'gridContainer', $options ) ? $options[ 'gridContainer' ] : "$('.box-body')";
		$clientArray = array_key_exists( 'editurl', $options ) && $options[ 'editurl' ] == 'clientArray';
		$_url        = array_key_exists( 'url', $options ) ? $options[ 'url' ] : Custom::url( \Yii::$app->controller->id . '/detail' );
		$url         = ! $clientArray ? "url: '$_url'," : "";
		$editurl     = "editurl: '" . ( array_key_exists( 'editurl', $options ) ? $options[ 'editurl' ] : $_url ) . "',";
		$data        = array_key_exists( 'data', $options ) ?
			"data: " . ( is_array( $options[ 'data' ] ) ? self::scriptJS( self::arrayToObjJS( $options[ 'data' ] ) ) : $options[ 'data' ] ) . "," : "";
		$datatype    = array_key_exists( 'datatype', $options ) ? $options[ 'datatype' ] : "local";
		$cols        = array_key_exists( 'colModel', $options ) ? $options[ 'colModel' ] : [];
		$height      = array_key_exists( 'height', $options ) ? $options[ 'height' ] : 220;
		$shrinkToFit = array_key_exists( 'shrinkToFit', $options ) ? $options[ 'shrinkToFit' ] : 0;
		/* serialize extraParams */
		$extraParamsJson = json_encode( ( array_key_exists( 'extraParams', $options ) ? $options[ 'extraParams' ] : [] ) );
		$extraParams     = '';
		if ( array_key_exists( 'extraParams', $options ) && is_array( $options[ 'extraParams' ] ) && count( $options[ 'extraParams' ] ) ) {
			foreach ( $options[ 'extraParams' ] as $k => $v ) {
				$extraParams .= "&$k=$v";
			}
		}
		/* Func that is executed after grid has finished loading */
		$funcAfterLoadGrid = array_key_exists( 'funcAfterLoadGrid', $options ) ? $options[ 'funcAfterLoadGrid' ] : 'function(grid, response) {}';
		/* Event handler of onSelectRow event */
		$onSelectRow = array_key_exists( 'onSelectRow', $options ) ? $options[ 'onSelectRow' ] : "";
		$model    = new $model;
		$gridCols = self::generateGridCols( $model, $cols );
		$jsTop = implode( " ", self::$jsAtTop );
		$europeanNumberFieldsJS = self::arrayToObjJS( self::$europeanNumberFields );
		$script = <<< JS
jQuery(function ($) {
    $.fn.inputFilterGridEditor = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  };
    
    var funcFilterFloat = function(value) { return /^-?\d*[.,]?\d{0,2}$/.test(value); };
    
    /* Append UI Element */
    $("body").append(
        '<div class="modal fade" id="confirmDeleteRowModal" tabindex="-1" role="dialog">' +
            '<div class="modal-dialog modal-sm" role="document">' +
                '<div class="modal-content">' +
                    '<div class="modal-body">Hapus data terpilih?</div>' +
                    '<div class="modal-footer">' +
                        '<button type="button" class="btn btn-primary" id="confirmDeleteRowModal_btnDelete">Hapus</button>' +
                        '<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>'
    );
    
    $jsTop
    
    /* variables and functions */
    var grid = $("$element"),
        confirmDeleteModal = $('#confirmDeleteRowModal'),
        confirmDeleteModal_btnDelete = $("#confirmDeleteRowModal_btnDelete"),
        
        europeanNumberFields=$europeanNumberFieldsJS,
        
        //getSelectedRowId = function() { return grid.getGridParam('selrow'); },
        addRow = function() {
            grid.jqGrid('addRow', {
                initdata : {},
                position :"first",
                useDefValues : false,
                useFormatter : false,
                addRowParams : {extraparam:{}}
            });
            $('#jEditButton_jqg1').click();
        },
        deleteRow = function(rowId) {
            /* hide confirm dialog */
            confirmDeleteModal.modal('hide');
            
            /* delete row request */
            $.ajax({
                $url
                method:"POST",
                data: { oper: 'del', id: rowId },
                success:function(response) {
                    console.log(response);
                    if(response.success) {
                        /* remove listener */
                        confirmDeleteModal_btnDelete.unbind();
                        
                        /* remove row from grid */
                        reloadGrid();
                    } else {
                        window.confirmDeleteRow(rowId);
                    }
                },
                error:function(response){
                    console.log(response);
                    alert("error");
                }
          });
        },
        actionsFormatter = function(cellVal,options,rowObject) {
            return '' +
             '<div style="margin-left:8px;">' +
              '<div title="Edit data terpilih" style="float: left; cursor: pointer;" class="ui-pg-div ui-inline-edit" id="jEditButton_jqg1" onclick="$.fn.fmatter.rowactions.call(this,\'edit\');" onmouseover="$(this).addClass(\'active\');" onmouseout="$(this).removeClass(\'active\');">' +
               '<span class="glyphicon glyphicon-edit"></span>' +
              '</div>' +
              '<div title="Hapus baris terpilih" style="float: left; cursor: pointer;" class="ui-pg-div ui-inline-del" id="jDeleteButton_jqg1" onclick="window.confirmDeleteRow(\''+options.rowId+'\');" onmouseover="$(this).addClass(\'active\');" onmouseout="$(this).removeClass(\'active\');">' +
               '<span class="glyphicon glyphicon-trash"></span>' +
              '</div>' +
              '<div title="Simpan" style="float: left; display: none; cursor: pointer;" class="ui-pg-div ui-inline-save" id="jSaveButton_jqg1" onclick="$.fn.fmatter.rowactions.call(this,\'save\');" onmouseover="$(this).addClass(\'active\');" onmouseout="$(this).removeClass(\'active\');">' +
               '<span class="glyphicon glyphicon-floppy-disk"></span>' +
              '</div>' +
              '<div title="Batal" style="float: left; display: none; cursor: pointer;" class="ui-pg-div ui-inline-cancel" id="jCancelButton_jqg1" onclick="$.fn.fmatter.rowactions.call(this,\'cancel\');" onmouseover="$(this).addClass(\'active\');" onmouseout="$(this).removeClass(\'active\');">' +
               '<span class="fa fa-ban"></span>' +
              '</div></div>';
        },
        
        funcEditTypeDate = function(el){
            $(el).datepicker({
                format: "dd-mm-yyyy",
                language: "id",
                daysOfWeekHighlighted: "0",
                autoclose: true,
                todayHighlight: true
            }); 
        },
        renderDate_ddmmyyyy = function(cellValue, options, rowObject){
            if(!cellValue) return '';
            var dt = new Date(cellValue),
                d = dt.getDate(),
                m = dt.getMonth() + 1,
                y = dt.getFullYear();
            return (d<10?'0':'') + d + "-" + (m<10?'0':'') + m + "-" + y;
        },
        
        reloadGrid = function(){
            $.ajax({
                $url
                method:"POST",
                data: $extraParamsJson,
                success: function(response) {
                    //console.log(response);
                    
                    grid.jqGrid('clearGridData')
                        .jqGrid('setGridParam', { datatype: 'local', data: response.rows })
                        .trigger('reloadGrid')
                        .jqGrid('setGridParam', { datatype: 'json' });
                    
                    funcAfterLoadGrid(grid, response);
                }
            })
        },
        funcAfterLoadGrid =$funcAfterLoadGrid,
        
        ajaxGridOpt = {
            beforeSend : function(jqXHR, ajaxOptions) {
                ajaxOptions.data += '$extraParams';
            },
            success : function(response, status, error) {
                
            },
            error : function(response, status, error) {
                
            }
        },
        ajaxRowOpt = {
            beforeSend : function(jqXHR, ajaxOptions) {
                if(europeanNumberFields.length > 0) { // console.log(ajaxOptions.data);
                    let arrData = ajaxOptions.data.split('&');
                    for(i=0; i<europeanNumberFields.length; i++) {
                        for(ii=0; ii<arrData.length; ii++) {
                            let itemArr = arrData[ii].split('=');
                            if(itemArr[0] === europeanNumberFields[i]) { // console.log(itemArr[1]);
                                arrData[ii] = itemArr[0]+'='+window.util.url.encode(window.util.number.getValueOfEuropeanNumber(window.util.url.decode(itemArr[1])));
                            }
                        }
                    }
                    ajaxOptions.data = arrData.join('&');
                    // console.log(ajaxOptions.data);
                }
                ajaxOptions.data += '$extraParams';
            },
            success : function(response, status, error) {
                reloadGrid();
            },
            error : function(response, status, error) {
                $.notify(
                    {
                        message: error + '<br> ' + response.responseJSON.message
                    },
                    {
                        type: 'danger',
                        mouse_over: 'pause',
                        newest_on_top: true,
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    }
                );
            }
        };
    
    /* Global function */
    window.confirmDeleteRow = function(rowId) {
        $('#confirmDeleteRowModal').modal({ backdrop: 'static', keyboard: true, show: true });
        confirmDeleteModal_btnDelete.click(function() { deleteRow(rowId); });
    };
    
    // $.jgrid.formatter.number.decimalSeparator = ',';
    // $.jgrid.formatter.number.thousandsSeparator = '.';
    
    grid.jqGrid({
        $url
        $editurl
        $data
        mtype: "POST",
        datatype: "$datatype",
        jsonReader: {
            root: 'rows',
            records: 'rowsCount',
            id: 'rowId',
            repeatitems: false,
            page:  function(obj) { return 1; },
            total: function(obj) { return 1; }
        },
        // ajaxGridOptions: ajaxGridOpt,
        ajaxRowOptions : ajaxRowOpt,
        
        colModel: [
            $actionCol
            { label: "rowId", name: "rowId", index: "rowId", hidden: true},
            $gridCols
        ],
        
        $onSelectRow
        
        responsive: false,
        styleUI: 'Bootstrap',
        width: 780,
        height: $height,
        rowNum: 20,
        // rownumbers: true,
        autowidth: true,
        shrinkToFit : $shrinkToFit,
        
        pager: '$elementPager',
        pgbuttons: false,
        pgtext: null,
        viewrecords: false
        
    }) // end of jqGrid
    .jqGrid("setLabel", "rn", "No")
    $gridNavButton
	;
    
    // $('#detailGridPager').find('.ui-pg-div').css('color', 'black');
    
    // confirmDeleteModal_btnDelete.click(function() {
    //     var rowId = getSelectedRowId();
    //     deleteRow(rowId);
    // });
    
    var gridContainer = $gridContainer;
    
    /* Grid resize */
    function resizeGrid() {
        $('#detailGrid').setGridWidth(gridContainer.width());
    }
    
    resizeGrid();
    gridContainer.resize(resizeGrid);
    
    reloadGrid();
    
}); // end of JQuery

JS;
		return self::parseJS( $script );
	}
}

//onSelectRow: function(ids) {
//    if(ids == null) {
//        ids=0;
//        if(jQuery("#list10_d").jqGrid('getGridParam','records') >0 )
//        {
//            jQuery("#list10_d").jqGrid('setGridParam',{url:"subgrid.php?q=1&id="+ids,page:1});
//                        jQuery("#list10_d").jqGrid('setCaption',"Invoice Detail: "+ids)
//                        .trigger('reloadGrid');
//                    }
//    } else {
//        jQuery("#list10_d").jqGrid('setGridParam',{url:"subgrid.php?q=1&id="+ids,page:1});
//                    jQuery("#list10_d").jqGrid('setCaption',"Invoice Detail: "+ids)
//                    .trigger('reloadGrid');
//                }
//}