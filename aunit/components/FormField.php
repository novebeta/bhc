<?php
namespace aunit\components;
use aunit\models\Tdarea;
use aunit\models\Tddealer;
use aunit\models\Tdleasing;
use aunit\models\Tdlokasi;
use aunit\models\Tdmotortype;
use aunit\models\Tdmotorwarna;
use aunit\models\Tdsales;
use aunit\models\Tdsupplier;
use aunit\models\Tdteam;
use aunit\models\Tmotor;
use aunit\models\Traccount;
use common\components\Custom;
use kartik\datecontrol\DateControl;
use kartik\number\NumberControl;
use kartik\widgets\Select2;
use kartik\widgets\TimePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
class FormField {
	/* --- LABEL --- */
	private static $select2_js_formatDropDown = <<< JS

function(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-10" style="text-align: left">' + result.id + '</div>' +
           '<div class="col-xs-14">' + result.text + '</div>' +
           '</div>';
}

JS;
	private static $select2_js_matcherDropDown = <<< JS

function(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}

JS;
	/* --- COMBO --- */
	static function label( $text, $style = '' ) {
		if ( ! $style ) {
			$style = "margin: 0; padding: 6px 0;";
		}
		return '<label class="control-label" style="' . $style . '">' . $text . '</label>';
	}
	static function combo( $tipe, $params = [] ) {
		$extraOptions = isset( $params[ 'extraOptions' ] ) ? $params[ 'extraOptions' ] : [];
		$simpleCombo  = isset( $params[ 'extraOptions' ][ 'simpleCombo' ] ) ? $params[ 'extraOptions' ][ 'simpleCombo' ] : false;
		$options      = isset( $params[ 'options' ] ) ? $params[ 'options' ] : [];
		$config       = isset( $params[ 'config' ] ) ? $params[ 'config' ] : [];
		/* non model */
		$name = isset( $params[ 'name' ] ) ? $params[ 'name' ] : $tipe;
		/* by model */
		$form       = isset( $params[ 'form' ] ) ? $params[ 'form' ] : null;
		$model      = isset( $params[ 'model' ] ) ? $params[ 'model' ] : null;
		$label      = isset( $params[ 'label' ] ) ? $params[ 'label' ] : null;
		$labelStyle = isset( $params[ 'labelStyle' ] ) ? $params[ 'labelStyle' ] : null; // replace null with default style
		/* default options */
		if ( array_key_exists( 'class', $options ) ) {
			if ( ! strpos( $options[ 'class' ], 'form-control' ) ) {
				$options[ 'class' ] = 'form-control ' . $options[ 'class' ];
			}
		} else {
			$options[ 'class' ] = 'form-control';
		}
		$config = array_merge( [
			'options'       => $options,
			'theme'         => Select2::THEME_DEFAULT,
			'pluginOptions' => [
				'dropdownAutoWidth' => true,
			],
		], $config );
		if ( ! $simpleCombo ) {
			$config[ 'pluginOptions' ][ 'templateResult' ]    = new JsExpression( self::$select2_js_formatDropDown );
			$config[ 'pluginOptions' ][ 'templateSelection' ] = new JsExpression( self::$select2_js_formatDropDown );
			$config[ 'pluginOptions' ][ 'matcher' ]           = new JsExpression( self::$select2_js_matcherDropDown );
			$config[ 'pluginOptions' ][ 'escapeMarkup' ]      = new JsExpression( "function(m) { return m; }" );
		}
		$allOption = isset( $params[ 'extraOptions' ][ 'allOption' ] ) ? $params[ 'extraOptions' ][ 'allOption' ] : false;
		switch ( $tipe ) {
			case 'LokasiKode' :
				$altLabel     = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'LokasiKode' ];
				$altOrder     = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'LokasiKode' => SORT_ASC, 'LokasiNomor' => SORT_ASC ];
				$altCondition = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "LokasiStatus IN ('A',:status) AND LokasiJenis <> 'Kas'";
				$altParams    = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [ ":status" => Menu::getUserLokasi()[ 'LokasiStatus' ] ];
				if ( ! isset( $config[ 'data' ] ) ) {
					$config[ 'data' ] = ArrayHelper::map( Tdlokasi::find()->select2( 'LokasiKode', $altLabel, $altOrder, [ "condition" => $altCondition, "params" => $altParams ], $allOption ), "value", 'label' );
				}
				break;
			case 'SupKode' :
				$altLabel     = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'SupKode' ];
				$altOrder     = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'SupKode' => SORT_ASC ];
				$altCondition = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
				$altParams    = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
				if ( ! isset( $config[ 'data' ] ) ) {
					$config[ 'data' ] = ArrayHelper::map( Tdsupplier::find()->select2( 'SupKode', $altLabel, $altOrder, [ "condition" => $altCondition, "params" => $altParams ], $allOption ), "value", 'label' );
				}
				break;
			case 'DealerKode' :
				$altLabel     = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'DealerKode' ];
				$altOrder     = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'DealerKode' => SORT_ASC ];
				$altCondition = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
				$altParams    = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
				if ( ! isset( $config[ 'data' ] ) ) {
					$config[ 'data' ] = ArrayHelper::map( Tddealer::find()->select2( 'DealerKode', $altLabel, $altOrder, [ "condition" => $altCondition, "params" => $altParams ], $allOption ), "value", 'label' );
				}
				break;
			case 'LeaseKode' :
				if ( ! isset( $config[ 'data' ] ) ) {
					$altLabel         = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'LeaseKode' ];
					$altOrder         = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'LeaseKode' => SORT_ASC ];
					$altCondition     = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
					$altParams        = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
					$config[ 'data' ] = ArrayHelper::map( Tdleasing::find()->select2( 'LeaseKode', $altLabel, $altOrder, [ "condition" => $altCondition, "params" => $altParams ], $allOption ), "value", 'label' );
				}
				break;
			case 'MotorType' :
				//$config[ 'data' ] = ArrayHelper::map( Tdmotortype::find()->select2( 'MotorType', [ 'MotorType' ], [ 'MotorType' => SORT_ASC ], [ "condition" => '', "params" => [] ],$allOption ), "value", 'label' );
				if ( ! isset( $config[ 'data' ] ) ) {
					$altLabel         = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'MotorType' ];
					$altOrder         = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'MotorType' => SORT_ASC ];
					$altCondition     = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
					$altParams        = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
					$config[ 'data' ] = ArrayHelper::map( Tdmotortype::find()->select2( 'MotorType', $altLabel, $altOrder, [ "condition" => $altCondition, "params" => $altParams ], $allOption ), "value", 'label' );
				}
				$onChange = "";
				if ( isset( $extraOptions[ 'MotorWarnaId' ] ) ) {
					$onChange .= "
                    $('#" . $extraOptions[ 'MotorWarnaId' ] . "').utilSelect2().loadRemote('" . Custom::url( 'tdmotorwarna/find' ) . "', { mode: 'combo', params: {':MotorType':this.value} , condition: 'MotorType = :MotorType','allOption': false}, true);
                    ";
				}
				if ( isset( $extraOptions[ 'MotorNoMesinId' ] ) ) {
					$onChange .= "
                    $('#" . $extraOptions[ 'MotorNoMesinId' ] . "').utilSelect2().loadRemote('" . Custom::url( 'tmotor/find' ) . "', { mode: 'combo', field: 'MotorNoMesin', MotorType: this.value }, true);
                    ";
				}
				if ( isset( $extraOptions[ 'MotorNoRangkaId' ] ) ) {
					$onChange .= "
                    $('#" . $extraOptions[ 'MotorNoRangkaId' ] . "').utilSelect2().loadRemote('" . Custom::url( 'tmotor/find' ) . "', { mode: 'combo', field: 'MotorNoRangka', MotorType: this.value }, true);
                    ";
				}
				if ( isset( $extraOptions[ 'MotorHrgJualId' ] ) ) {
					$onChange .= "
                    $('#" . $extraOptions[ 'MotorHrgJualId' ] . "').utilNumberControl().loadRemote('" . Custom::url( 'tdmotortype/find' ) . "', { mode: 'number', field: 'MotorHrgJual', id: this.value });
                    ";
				}
				if ( $onChange != '' ) {
					$onChange                             = "function(e) { 
					" . $onChange . "
					}";
					$config[ 'pluginEvents' ][ 'change' ] = new \yii\web\JsExpression( $onChange );
				}
				break;
			case 'MotorWarna' :
				//$config[ 'data' ] = ArrayHelper::map( Tdmotorwarna::find()->select2( 'MotorWarna', [ 'MotorWarna' ], [ 'MotorWarna' => SORT_ASC ],[ "condition" => '', "params" => [] ], $allOption ), "value", 'label' );
				if ( ! isset( $config[ 'data' ] ) ) {
					$config[ 'data' ] = ArrayHelper::map( Tdmotorwarna::find()->select2( 'MotorWarna', [ 'MotorWarna' ], [ 'MotorWarna' => SORT_ASC ], [ "condition" => '', "params" => [] ], $allOption ), "value", 'label' );
				}
				break;
			case 'MotorNoMesin' :
				//$config[ 'data' ] = ArrayHelper::map( Tmotor::find()->select2( 'MotorNoMesin', [ 'MotorNoMesin' ], [ 'MotorNoMesin' => SORT_ASC ],[ "condition" => '', "params" => [] ], $allOption ), "value", 'label' );
				if ( ! isset( $config[ 'data' ] ) ) {
					$config[ 'data' ] = ArrayHelper::map( Tmotor::find()->select2( 'MotorNoMesin', [ 'MotorNoMesin' ], [ 'MotorNoMesin' => SORT_ASC ], [ "condition" => '', "params" => [] ], $allOption ), "value", 'label' );
				}
				break;
			case 'MotorNoRangka' :
				//$config[ 'data' ] = ArrayHelper::map( Tmotor::find()->select2( 'MotorNoRangka', [ 'MotorNoRangka' ], [ 'MotorNoRangka' => SORT_ASC ],[ "condition" => '', "params" => [] ], $allOption ), "value", 'label' );
				if ( ! isset( $config[ 'data' ] ) ) {
					$config[ 'data' ] = ArrayHelper::map( Tmotor::find()->select2( 'MotorNoRangka', [ 'MotorNoRangka' ], [ 'MotorNoRangka' => SORT_ASC ], [ "condition" => '', "params" => [] ], $allOption ), "value", 'label' );
				}
				break;
			case 'Kabupaten' :
				$urlTdArea = Url::toRoute( 'tdarea/find' );
				if ( ! isset( $config[ 'data' ] ) ) {
					$config[ 'data' ] = ArrayHelper::map( Tdarea::find()->select2( 'Kabupaten', [ 'Kabupaten' ], 'MIN(`AreaStatus`), `Kabupaten`', [ "condition" => '', "params" => [] ], $allOption ), "value", 'label' );
				}
				if ( isset( $extraOptions[ 'KecamatanId' ] ) ) {
					$config[ 'pluginEvents' ][ 'change' ] = new \yii\web\JsExpression( "function(e) {
                    $('#" . $extraOptions[ 'KecamatanId' ] . "').utilSelect2().loadRemote('" . $urlTdArea . "', {  mode: 'combo', field: 'Kecamatan', Kabupaten: this.value }, true,
                    function(){ $('#" . $extraOptions[ 'KecamatanId' ] . "').trigger( 'change' ) });
                    }" );
				}
				break;
			case 'Kecamatan' :
				$urlTdArea = Url::toRoute( 'tdarea/find' );
				if ( ! isset( $config[ 'data' ] ) ) {
					$config[ 'data' ] = ArrayHelper::map( Tdarea::find()->select2( 'Kecamatan', [ 'Kecamatan' ], [ 'Kecamatan' => SORT_ASC ], [ "condition" => '', "params" => [] ], $allOption ), "value", 'label' );
				}
				if ( isset( $extraOptions[ 'KelurahanId' ] ) ) {
					$config[ 'pluginEvents' ][ 'change' ] = new \yii\web\JsExpression( "function(e) {
                    $('#" . $extraOptions[ 'KelurahanId' ] . "').utilSelect2().loadRemote('" . $urlTdArea . "', {  mode: 'combo', field: 'Kelurahan', Kecamatan: this.value }, true,
                    function(){ $('#" . $extraOptions[ 'KelurahanId' ] . "').trigger( 'change' ) });
                    }" );
				}
				break;
			case 'Kelurahan' :
				$urlTdArea = Url::toRoute( 'tdarea/find' );
				if ( ! isset( $config[ 'data' ] ) ) {
					$config[ 'data' ] = ArrayHelper::map( Tdarea::find()->select2( 'Kelurahan', [ 'Kelurahan' ], [ 'Kelurahan' => SORT_ASC ], [ "condition" => '', "params" => [] ], $allOption ), "value", 'label' );
				}
				if ( isset( $extraOptions[ 'KodePosId' ] ) ) {
					$config[ 'pluginEvents' ][ 'change' ] = new \yii\web\JsExpression( "function(e) {
                    $('#" . $extraOptions[ 'KodePosId' ] . "').utilSelect2().loadRemote('" . $urlTdArea . "', {  mode: 'combo', field: 'KodePos', Kelurahan: this.value }, true);
                    }" );
				}
				break;
			case 'KodePos' :
				$altLabel     = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'KodePos' ];
				$altOrder     = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'KodePos' => SORT_ASC ];
				$altCondition = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
				$altParams    = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
				if ( ! isset( $config[ 'data' ] ) ) {
					$config[ 'data' ] = ArrayHelper::map( Tdarea::find()->select2( 'KodePos', $altLabel, $altOrder, [ 'condition' => $altCondition, 'params' => $altParams ], $allOption ), "value", 'label' );
				}
				break;
			case 'TeamKode' :
				$url          = Url::toRoute( 'tdsales/find' );
				$altLabel     = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'TeamKode' ];
				$altOrder     = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'TeamStatus' => SORT_ASC, 'TeamKode' => SORT_ASC ];
				$altCondition = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "TeamStatus IN ('A','1')";
				$altParams    = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
				if ( ! isset( $config[ 'data' ] ) ) {
					$config[ 'data' ] = ArrayHelper::map( Tdteam::find()->select2( 'TeamKode', $altLabel, $altOrder, [ 'condition' => $altCondition, 'params' => $altParams ], $allOption ), "value", 'label' );
				}
				if ( isset( $extraOptions[ 'SalesKodeId' ] ) ) {
					$config[ 'pluginEvents' ][ 'change' ] = new \yii\web\JsExpression( "function(e) {
                    $('#" . $extraOptions[ 'SalesKodeId' ] . "').utilSelect2().loadRemote('" . $url . "', {  mode: 'combo', value: 'SalesKode', label: ['SalesNama'], condition : 'TeamKode LIKE :TeamKode', params:{':TeamKode':this.value}, allOption:true }, true);
                    }" );
				}
				break;
			case 'SalesKode' :
				$altLabel     = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'SalesKode' ];
				$altOrder     = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'SalesStatus' => SORT_ASC, 'SalesKode' => SORT_ASC ];
				$altCondition = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
				// $altCondition = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "SalesStatus = 'A'";
				$altParams    = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
				if ( ! isset( $config[ 'data' ] ) ) {
					$config[ 'data' ] = ArrayHelper::map( Tdsales::find()->select2( 'SalesKode', $altLabel, $altOrder, [ 'condition' => $altCondition, 'params' => $altParams ], $allOption ), "value", 'label' );
				}
				break;
			case 'NoAccount' :
				$altLabel     = isset( $params[ 'extraOptions' ][ 'altLabel' ] ) ? $params[ 'extraOptions' ][ 'altLabel' ] : [ 'NoAccount' ];
				$altOrder     = isset( $params[ 'extraOptions' ][ 'altOrder' ] ) ? $params[ 'extraOptions' ][ 'altOrder' ] : [ 'NoAccount' => SORT_ASC ];
				$altCondition = isset( $params[ 'extraOptions' ][ 'altCondition' ] ) ? $params[ 'extraOptions' ][ 'altCondition' ] : "";
				$altParams    = isset( $params[ 'extraOptions' ][ 'altParams' ] ) ? $params[ 'extraOptions' ][ 'altParams' ] : [];
				if ( ! isset( $config[ 'data' ] ) ) {
					$config[ 'data' ] = ArrayHelper::map( Traccount::find()->NoAccount( 'NoAccount', $altLabel, $altOrder, [ "condition" => $altCondition, "params" => $altParams ], $allOption ), "value", 'label' );
				}
				break;
			case 'combodksk' :
				$config[ 'data' ] = [
					'CusNama' => 'Nama Konsumen',
					'DKNo' => 'No Data Konsumen',
					'SalesNama' => 'Nama Sales',
					'DKJenis' => 'Jenis',
					'LeaseKode' => 'Leasing',
					'MotorType' => 'Type Motor',
					'MotorWarna' => 'Warna Motor',
					'MotorNoMesin' => 'No Mesin',
					'MotorNoRangka' => 'No Rangka',
					'DKMemo' => 'Keterangan',
					'INNo' => 'No Inden',
					'NamaHadiah' => 'Nama Hadiah',
					'ProgramNama' => 'Nama Program',
					'UserID' => 'User',
					'SKNo' => 'No Surat Jln Konsumen',
					'SPKNo' => 'No SPK',
				];
				break;
            case 'CusStatusRumah' :
                $config[ 'data' ] = [
                    'Rumah Sendiri' => 'Sendiri',
                    'Rumah Ortu'    => 'Ortu',
                    'Rumah Sewa'    => 'Sewa',
                ];
                break;
			case 'CusSex' :
				$config[ 'data' ] = [
					'Perempuan' => 'Perempuan',
					'Laki-Laki' => 'Laki-Laki',
				];
				break;
            case 'StatusMP' :
                $config[ 'data' ] = [
                    'Open' => 'Open',
                    'Close' => 'Close',
                ];
                break;
			case 'CusAgama' :
				$config[ 'data' ] = [
					'Islam'     => 'Islam',
					'Kristen'   => 'Kristen',
					'Katholik'  => 'Katholik',
					'Hindu'     => 'Hindu',
					'Budha'     => 'Budha',
					'Lain-Lain' => 'Lain-Lain',
				];
				break;
			case 'CusPekerjaan' :
				$config[ 'data' ] = [
					'Wiraswasta' => 'Wiraswasta',
					'Karyawan Swasta' => 'Karyawan Swasta',
					'Belum/Tidak Bekerja' => 'Belum/Tidak Bekerja',
					'Biarawan' => 'Biarawan',
					'Biarawati' => 'Biarawati',
					'Bidan' => 'Bidan',
					'Bupati' => 'Bupati',
					'Buruh Bangunan' => 'Buruh Bangunan',
					'Buruh Harian Lepas' => 'Buruh Harian Lepas',
					'Buruh Nelayan/Perikanan' => 'Buruh Nelayan/Perikanan',
					'Buruh Peternakan' => 'Buruh Peternakan',
					'Buruh Tani/Perkebunan' => 'Buruh Tani/Perkebunan',
					'Dokter' => 'Dokter',
					'Dosen' => 'Dosen',
					'Duta Besar' => 'Duta Besar',
					'Gubernur' => 'Gubernur',
					'Guru' => 'Guru',
					'Ibu Rumah Tangga' => 'Ibu Rumah Tangga',
					'Imam Mesjid' => 'Imam Mesjid',
					'Industri' => 'Industri',
					'Juru Masak' => 'Juru Masak',
					'Karyawan BUMD' => 'Karyawan BUMD',
					'Karyawan BUMN' => 'Karyawan BUMN',
					'Karyawan Honorer' => 'Karyawan Honorer',
					'Kepala Desa' => 'Kepala Desa',
					'Kepolisian RI' => 'Kepolisian RI',
					'Konstruksi' => 'Konstruksi',
					'Konsultan' => 'Konsultan',
					'Lainnya' => 'Lainnya',
					'Mahasiswa/Pelajar' => 'Mahasiswa/Pelajar',
					'Mekanik' => 'Mekanik',
					'Mengurus Rumah Tangga' => 'Mengurus Rumah Tangga',
					'Nelayan/Perikanan' => 'Nelayan/Perikanan',
					'Notaris' => 'Notaris',
					'Ojek' => 'Ojek',
					'Paraji' => 'Paraji',
					'Paranormal' => 'Paranormal',
					'Pastor' => 'Pastor',
					'Pedagang' => 'Pedagang',
					'Pegawai Negeri Sipil' => 'Pegawai Negeri Sipil',
					'Pekebun' => 'Pekebun',
					'Pelajar/Mahasiswa' => 'Pelajar/Mahasiswa',
					'Pelaut' => 'Pelaut',
					'Pembantu Rumah Tangga' => 'Pembantu Rumah Tangga',
					'Penata Busana' => 'Penata Busana',
					'Penata Rambut' => 'Penata Rambut',
					'Penata Rias' => 'Penata Rias',
					'Pendeta' => 'Pendeta',
					'Peneliti' => 'Peneliti',
					'Pengacara' => 'Pengacara',
					'Pengrajin' => 'Pengrajin',
					'Pensiunan' => 'Pensiunan',
					'Penterjemah' => 'Penterjemah',
					'Penyiar Radio' => 'Penyiar Radio',
					'Penyiar Televisi' => 'Penyiar Televisi',
					'Perancang Busana' => 'Perancang Busana',
					'Perangkat Desa' => 'Perangkat Desa',
					'Perawat' => 'Perawat',
					'Perdagangan' => 'Perdagangan',
					'Petani' => 'Petani',
					'Peternak' => 'Peternak',
					'Petugas Keamanan' => 'Petugas Keamanan',
					'Pialang' => 'Pialang',
					'Pilot' => 'Pilot',
					'Presiden' => 'Presiden',
					'Promotor Acara' => 'Promotor Acara',
					'Psikiater' => 'Psikiater',
					'Psikolog' => 'Psikolog',
					'Seniman' => 'Seniman',
					'Sopir' => 'Sopir',
					'Tabib' => 'Tabib',
					'Teknisi' => 'Teknisi',
					'Tentara Nasional Indonesia' => 'Tentara Nasional Indonesia',
					'Transportasi' => 'Transportasi',
					'Tukang Batu' => 'Tukang Batu',
					'Tukang Cukur' => 'Tukang Cukur',
					'Tukang Gigi' => 'Tukang Gigi',
					'Tukang Jahit' => 'Tukang Jahit',
					'Tukang Kayu' => 'Tukang Kayu',
					'Tukang Las/Pandai Besi' => 'Tukang Las/Pandai Besi',
					'Tukang Listrik' => 'Tukang Listrik',
					'Tukang Sol Sepatu' => 'Tukang Sol Sepatu',
					'Ustadz/Mubaligh' => 'Ustadz/Mubaligh',
					'Wakil Bupati' => 'Wakil Bupati',
					'Wakil Gubernur' => 'Wakil Gubernur',
					'Wakil Presiden' => 'Wakil Presiden',
					'Wakil Walikota' => 'Wakil Walikota',
					'Walikota' => 'Walikota',
					'Wartawan' => 'Wartawan',
					'Akuntan' => 'Akuntan',
					'Anggota BPK' => 'Anggota BPK',
					'Anggota DPD' => 'Anggota DPD',
					'Anggota DPRD Kabupaten/Kota' => 'Anggota DPRD Kabupaten/Kota',
					'Anggota DPRD Provinsi' => 'Anggota DPRD Provinsi',
					'Anggota DPR-RI' => 'Anggota DPR-RI',
					'Anggota Kabinet/Kementerian' => 'Anggota Kabinet/Kementerian',
					'Anggota Mahkamah Konstitusi' => 'Anggota Mahkamah Konstitusi',
					'Apoteker' => 'Apoteker',
					'Arsitek' => 'Arsitek',
				];
				break;
			case 'CusPendidikan' :
				$config[ 'data' ] = [
					'Tidak Tamat SD'  => 'Tidak Tamat SD',
					'SD'              => 'SD',
					'SLTP/SMP'        => 'SLTP/SMP',
					'SLTA/SMU'        => 'SLTA/SMU',
					'Akademi/Diploma' => 'Akademi/Diploma',
					'Sarjana'         => 'Sarjana',
					'Pasca Sarjana'   => 'Pasca Sarjana',
				];
				break;
			case 'CusPengeluaran' :
				$config[ 'data' ] = [
					'<Rp 700.000,-'                     => '<Rp 700.000,-',
					'Rp 700.001,- s/d Rp 1.000.000,-'   => 'Rp 700.001,- s/d Rp 1.000.000,-',
					'Rp 1.000.001,- s/d Rp 1.500.000,-' => 'Rp 1.000.001,- s/d Rp 1.500.000,-',
					'Rp 1.500.001,- s/d Rp 2.000.000,-' => 'Rp 1.500.001,- s/d Rp 2.000.000,-',
					'Rp 2.000.001,- s/d Rp 3.000.000,-' => 'Rp 2.000.001,- s/d Rp 3.000.000,-',
					'Rp 3.000.001,- s/d Rp 4.000.000,-' => 'Rp 3.000.001,- s/d Rp 4.000.000,-',
					'>Rp 4.000.000,-'                   => '>Rp 4.000.000,-'
				];
				break;
			case 'CusStatusHP' :
				$config[ 'data' ] = [
					'Prabayar'   => 'Prabayar',
					'Pascabayar' => 'Pascabayar',
				];
				break;
			case 'CusKodeKons' :
				$config[ 'data' ] = [
					'Individual Customer (Regular)'     => 'Individual Customer (Regular)',
                    'Individual Customer (Kolektif)'    => 'Individual Customer (Kolektif)',
                    'Individual Customer (Joint Promo)' => 'Individual Customer (Joint Promo)',
                    'Group Customer'                    => 'Group Customer',
                    'Kolektif Customer (Leasing)'       => 'Kolektif Customer (Leasing)',
					'Kolektif Customer (Bank)'          => 'Kolektif Customer (Bank)',
					'Kolektif Customer (Koperasi)'      => 'Kolektif Customer (Koperasi)',
				];
				break;
			case 'DKMerk' :
				$config[ 'data' ] = [
					'Honda'                 => 'Honda',
					'Yamaha'                => 'Yamaha',
					'Suzuki'                => 'Suzuki',
					'Kawasaki'              => 'Kawasaki',
					'Motor Lain'            => 'Motor Lain',
					'Belum pernah memiliki' => 'Belum pernah memiliki',
				];
				break;
			case 'DKJenis' :
				$config[ 'data' ] = [
					'Sport'                 => 'Sport',
					'Cub(Bebek)'            => 'Cub(Bebek)',
					'AT(Automatic)'         => 'AT(Automatic)',
					'Belum pernah memiliki' => 'Belum pernah memiliki',
				];
				break;
			case 'DKMotorUntuk' :
				$config[ 'data' ] = [
					'Berdagang'              => 'Berdagang',
					'Pemakaian jarak dekat'  => 'Pemakaian jarak dekat',
					'Ke Sekolah / ke Kampus' => 'Ke Sekolah / ke Kampus',
					'Rekreasi / Olah raga'   => 'Rekreasi / Olah raga',
					'Kebutuhan Keluarga'     => 'Kebutuhan Keluarga',
					'Bekerja'                => 'Bekerja',
				];
				break;
			case 'DKMotorOleh' :
				$config[ 'data' ] = [
					'Saya sendiri'             => 'Saya sendiri',
					'Anak'                     => 'Anak',
					'Pasangan (suami / istri)' => 'Pasangan (suami / istri)',
				];
				break;
			case 'TujuanUpdate' :
				$config[ 'data' ] = [
					'STNKTglBayar'      => 'Tgl Ajukan Faktur',
					'FakturAHMTgl'      => 'Tgl Jadi Faktur',
					'FakturAHMTglAmbil' => 'Tgl Ambil Faktur',
					'STNKTglMasuk'      => 'Tgl Ajukan STNK',
					'STNKTglJadi'       => 'Tgl Jadi STNK',
					'STNKTglAmbil'      => 'Tgl Ambil STNK',
					'NoticeTglJadi'     => 'Tgl Jadi Notice',
					'NoticeTglAmbil'    => 'Tgl Ambil Notice',
					'PlatTgl'           => 'Tgl Jadi Plat',
					'PlatTglAmbil'      => 'Tgl Ambil Plat',
					'BPKBTglJadi'       => 'Tgl Jadi BPKB',
					'BPKBTglAmbil'      => 'Tgl Ambil BPKB',
					'SRUTTglJadi'       => 'Tgl Jadi SRUT',
					'SRUTTglAmbil'      => 'Tgl Ambil SRUT',
				];
				break;
			default:
				break;
		}
		if(!empty($config[ 'value' ])){
			$config[ 'value' ] = strtolower($config[ 'value' ]);
			$key = array_find_key($config[ 'value' ],$config[ 'data' ]);
			if($key !== false){
				$config[ 'value' ] = $key;
			}
		}
		if ( $form ) {
			$combo = $form->field( $model, $name,
				[ 'template' => ( $label ? '{label}{input}' : '{input}' ) ] )
			              ->widget( Select2::class, $config );
			if ( $label ) {
				$labelParams = [];
				if ( $labelStyle ) {
					$labelParams[ 'style' ] = $labelStyle;
				}
				$combo->label( $label, $labelParams );
			} else {
				$combo->label( false );
			}
		} else {
			$config = array_merge( $config, [ 'name' => $name, ] );
			$combo  = Select2::widget( $config );
		}
		return $combo;
	}
	static function integerInput( $params = [] ) {
		$default = [
			'config' => [
				'maskedInputOptions' => [
					'integerDigits' => 5,
					'digits'        => 0,
					'allowMinus'    => true
				]
			]
		];
		return self::numberInput( array_merge_recursive( $default, $params ) );
	}
	static function numberInput( $params = [] ) {
		$config = isset( $params[ 'config' ] ) ? $params[ 'config' ] : [];
		$name   = isset( $params[ 'name' ] ) ? $params[ 'name' ] : null;
		$form   = isset( $params[ 'form' ] ) ? $params[ 'form' ] : null;
		/* default configs */
		$maskedInputOptions             = [ 'groupSeparator' => '.', 'radixPoint' => ',', 'digits' => 2, 'digitsOptional' => false ];
		$config[ 'maskedInputOptions' ] = isset( $config[ 'maskedInputOptions' ] ) ? array_merge( $maskedInputOptions, $config[ 'maskedInputOptions' ] ) :
			$maskedInputOptions;
		if ( array_key_exists( 'displayOptions', $config ) ) {
			if ( array_key_exists( 'class', $config[ 'displayOptions' ] ) && ! strpos( $config[ 'displayOptions' ][ 'class' ], 'form-control' ) ) {
				$config[ 'displayOptions' ][ 'class' ] = 'form-control ' . $config[ 'displayOptions' ][ 'class' ];
			}
		} else {
			$config[ 'displayOptions' ] = [ 'class' => 'form-control' ];
		}
		$config[ 'maskedInputOptions' ][ 'removeMaskOnSubmit' ] = true;
		$config[ 'maskedInputOptions' ][ 'autoGroup' ]          = true;
		$config[ 'maskedInputOptions' ][ 'autoUnmask' ]         = true;
		$config[ 'maskedInputOptions' ][ 'allowMinus' ]         = true;
		if ( $form ) {
			/* by model */
			$model      = isset( $params[ 'model' ] ) ? $params[ 'model' ] : null;
			$label      = isset( $params[ 'label' ] ) ? $params[ 'label' ] : null;
			$labelStyle = isset( $params[ 'labelStyle' ] ) ? $params[ 'labelStyle' ] : null; // replace null with default style
			$field      = $form->field( $model, $name,
				[ 'template' => ( $label ? '{label}{input}' : '{input}' ) ] )
			                   ->widget( NumberControl::class, $config );
			if ( $label ) {
				$labelParams = [];
				if ( $labelStyle ) {
					$labelParams[ 'style' ] = $labelStyle;
				}
				$field->label( $label, $labelParams );
			} else {
				$field->label( false );
			}
		} else {
			/* non model */
			if ( ! isset( $config[ 'id' ] ) ) {
				$config[ 'id' ] = $name;
			}
			$config = array_merge( $config, [ 'name' => $name, ] );
			$field  = NumberControl::widget( $config );
		}
		return $field;
	}
	/* --- NUMBER INPUT --- */
	static function decimalInput( $params = [] ) {
		$default = [
			'config' => [
				'maskedInputOptions' => [
					'integerDigits' => 14,
					'digits'        => 2,
					'allowMinus'    => true,
				]
			]
		];
		return self::numberInput( array_merge_recursive( $default, $params ) );
	}
	/* --- DATE INPUT --- */
	static function timeInput( $params = [] ) {
		$config = isset( $params[ 'config' ] ) ? $params[ 'config' ] : [];
		$name   = isset( $params[ 'name' ] ) ? $params[ 'name' ] : null;
		$form   = isset( $params[ 'form' ] ) ? $params[ 'form' ] : null;
		if ( isset( $params[ 'config' ][ 'value' ] ) ) {
			$config[ 'value' ] = date( 'H:i:s', strtotime( $params[ 'config' ][ 'value' ] ) );
		}
		$config[ 'pluginOptions' ] = isset( $config[ 'pluginOptions' ] ) ? array_merge( [ 'showSeconds' => true, 'showMeridian' => false, 'template' => false ], $config[ 'pluginOptions' ] ) :
			[ 'showSeconds' => true, 'showMeridian' => false, 'template' => false ];
		$config[ 'addon' ]         = '';
		if ( $form ) {
			/* by model */
			$model      = isset( $params[ 'model' ] ) ? $params[ 'model' ] : null;
			$label      = isset( $params[ 'label' ] ) ? $params[ 'label' ] : null;
			$labelStyle = isset( $params[ 'labelStyle' ] ) ? $params[ 'labelStyle' ] : null; // replace null with default style
			$field      = $form->field( $model, $name,
				[ 'template' => ( $label ? '{label}{input}' : '{input}' ) ] )
			                   ->widget( TimePicker::class, $config );
			if ( $label ) {
				$labelParams = [];
				if ( $labelStyle ) {
					$labelParams[ 'style' ] = $labelStyle;
				}
				$field->label( $label, $labelParams );
			} else {
				$field->label( false );
			}
		} else {
			/* non model */
			$config = array_merge( $config, [ 'name' => $name, ] );
			$field  = TimePicker::widget( $config );
		}
		return $field;
	}
	static function dateInput( $params = [] ) {
		$config                    = isset( $params[ 'config' ] ) ? $params[ 'config' ] : [];
		$name                      = isset( $params[ 'name' ] ) ? $params[ 'name' ] : null;
		$form                      = isset( $params[ 'form' ] ) ? $params[ 'form' ] : null;
		$config[ 'pluginOptions' ] = isset( $config[ 'pluginOptions' ] ) ? array_merge( [ 'todayHighlight' => true, 'todayBtn' => true, 'autoclose' => true, ], $config[ 'pluginOptions' ] ) :
			[ 'todayHighlight' => true, 'todayBtn' => true, 'autoclose' => true ];
		/* default configs */
		if(isset($config['readonly']) && $config['readonly']){
			$config['disabled'] = true;
		}
		if ( ! array_key_exists( 'type', $config ) ) {
			$config[ 'type' ] = DateControl::FORMAT_DATE;
		}
		if ( $form ) {
			/* by model */
			$model      = isset( $params[ 'model' ] ) ? $params[ 'model' ] : null;
			$label      = isset( $params[ 'label' ] ) ? $params[ 'label' ] : null;
			$labelStyle = isset( $params[ 'labelStyle' ] ) ? $params[ 'labelStyle' ] : null; // replace null with default style
			$field      = $form->field( $model, $name,
				[ 'template' => ( $label ? '{label}{input}' : '{input}' ) ] )
			                   ->widget( DateControl::class, $config );
			if ( $label ) {
				$labelParams = [];
				if ( $labelStyle ) {
					$labelParams[ 'style' ] = $labelStyle;
				}
				$field->label( $label, $labelParams );
			} else {
				$field->label( false );
			}
		} else {
			/* non model */
			$config = array_merge( $config, [ 'name' => $name, ] );
			$field  = DateControl::widget( $config );
		}
		return $field;
	}
}