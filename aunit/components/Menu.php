<?php
namespace aunit\components;
use common\components\Custom;
use common\models\User;
use Yii;
use yii\base\Component;
use yii\db\Query;
use yii\helpers\ArrayHelper;
define( "ATTRIBUTE", [
	'System'                               => [ 'class' => 'fa fa-cogs', 'url' => '#' ],
	'Login Dealer'                         => [ 'class' => '', 'url' => Custom::urllgn( 'site/dealer&action=display' ) ],
	'Scan'                                 => [ 'class' => '', 'url' => Custom::url( 'site/scan&action=update' ) ],
	'Broadcast Message'                    => [ 'class' => '', 'url' => Custom::url( 'site/send-message&action=display' ) ],
	'Broadcast Kwitansi'                   => [ 'class' => '', 'url' => Custom::url( 'ttkm/kas-masuk-wa&action=display' ) ],
	'Hak Akses'                            => [ 'class' => '', 'url' => Custom::url( 'tuakses/index&action=display' ) ],
	'User Password'                        => [ 'class' => '', 'url' => Custom::url( 'tuser/index&action=display' ) ],
	'User Profile'                         => [ 'class' => '', 'url' => Custom::url( 'tuser/profile&action=display' ) ],
	'Dealer Profile'                       => [ 'class' => '', 'url' => Custom::url( 'tzcompany/update&action=display' ) ],
	'Backup - Restore'                     => [ 'class' => '', 'url' => Custom::url( 'site/backup-restore&action=display' ) ],
	'Re-Login'                             => [ 'class' => '', 'url' => Custom::urllgn( 'site/logout&action=display' ) ],
	'Keluar'                               => [ 'class' => '', 'url' => Custom::urllgn( 'site/blank&action=display' ) ],
	'Data'                                 => [ 'class' => 'fa fa-database', 'url' => '#' ],
	'Type  Warna Motor'                    => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tdmotorwarna/index&action=display' ) ],
	'Motor'                                => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tmotor/index&action=display' ) ],
	'Warehouse / Pos'                      => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tdlokasi/index&action=display' ) ],
	'Supplier'                             => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tdsupplier/index&action=display' ) ],
	'Dealer'                               => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tddealer/index&action=display' ) ],
	'Team'                                 => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tdteam/index&action=display' ) ],
	'Sales'                                => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tdsales/index&action=display' ) ],
	'Konsumen'                             => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tdcustomer/index&action=display' ) ],
	'Leasing'                              => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tdleasing/index&action=display' ) ],
	'Program'                              => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tdprogram/index&action=display' ) ],
	'Tarif BBN'                            => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tdbbn/index&action=display' ) ],
	'Area'                                 => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tdarea/index&action=display' ) ],
	'Pembelian'                            => [ 'class' => 'fa fa-shopping-cart', 'url' => '#' ],
	'Penerimaan Barang'                    => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttss/index&action=display' ) ],
	'Faktur Beli'                          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttfb/index&action=display' ) ],
	'Retur Beli'                           => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttsd/retur-beli&action=display' ) ],
	'Invoice Retur Beli'                   => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttsd/invoice-retur-beli&action=display' ) ],
	'Penjualan'                            => [ 'class' => 'fa fa-chart-line', 'url' => '#' ],
	'Inden'                                => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttin/index&action=display' ) ],
	'Data Konsumen Kredit'                 => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/data-konsumen-kredit&action=display' ) ],
	'Data Konsumen Tunai'                  => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/data-konsumen-tunai&action=display' ) ],
	'Data Konsumen Status'                 => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/data-konsumen-status&action=display' ) ],
	'Surat Jalan Konsumen'                 => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttsk/index&action=display' ) ],
	'Retur Konsumen'                       => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttrk/index&action=display' ) ],
	'STNK - BPKB'                          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/stnk-bpkb&action=display' ) ],
	'Serah Terima Kolektif'                => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttsk/serah-terima-kolektif&action=display' ) ],
	'Data Portal'                          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/data-portal&action=display' ) ],
	'Pengajuan Berkas'                     => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttabhd/index&action=display' ) ],
	'Pengajuan Bayar BBN via Bank'         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbkhd/pengajuan-bayar-bbn-bank&action=display' ) ],
	'Pengajuan Bayar BBN via Kas'          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkk/pengajuan-bayar-bbn-kas&action=display&JenisBBN=Pengajuan' ) ],
	'Mutasi'                               => [ 'class' => 'fa fa-sync', 'url' => '#' ],
	'Mutasi Eksternal ke Dealer'           => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttsd/mutasi-eksternal-dealer&action=display' ) ],
	'Cek Fisik Stock Opname'               => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttcfhd/index&action=display' ) ],
	'Invoice Eksternal'                    => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttsd/invoice-eksternal-dealer&action=display' ) ],
	'Penerimaan dari Dealer'               => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttpd/penerimaan-dealer&action=display' ) ],
	'Invoice Penerimaan'                   => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttpd/invoice-penerimaan&action=display' ) ],
	'Mutasi Internal ke Pos'               => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttsmhd/mutasi-internal-pos&action=display' ) ],
	'Penerimaan dari Pos'                  => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttpbhd/index&action=display' ) ],
	'Repair IN'                            => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttsmhd/mutasi-repair-in&action=display' ) ],
	'Repair OUT'                           => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttsmhd/mutasi-repair-out&action=display' ) ],
	'Keuangan'                             => [ 'class' => 'fa fa-file-invoice-dollar', 'url' => '#' ],
	'Kas Masuk Inden'                      => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkm/kas-masuk-inden&action=display' ) ],
	'Kas Masuk Pelunasan Leasing'          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkm/kas-masuk-pelunasan-leasing&action=display' ) ],
	'Kas Masuk Uang Muka Kredit'           => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkm/kas-masuk-uang-muka-kredit&action=display' ) ],
	'Kas Masuk Piutang Kons UM'            => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkm/kas-masuk-bayar-piutang-kon-um&action=display' ) ],
	'Kas Masuk Uang Muka Tunai'            => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkm/kas-masuk-uang-muka-tunai&action=display' ) ],
	'Kas Masuk Sisa Piutang Kons'          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkm/kas-masuk-bayar-sisa-piutang&action=display' ) ],
	'Kas Masuk BBN Progresif'              => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkm/kas-masuk-bbn-progresif&action=display' ) ],
	'Kas Masuk BBN Acc'                    => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkm/kas-masuk-bbn-acc&action=display' ) ],
	'Kas Masuk Dari Bank'                  => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkm/kas-masuk-dari-bank&action=display' ) ],
	'Kas Masuk Umum'                       => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkm/kas-masuk-umum&action=display' ) ],
	'Kas Masuk Dari Bengkel H2'            => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkm/kas-masuk-dari-bengkel&action=display' ) ],
	'Kas Masuk Angsuran Karyawan'          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkm/kas-masuk-angsuran-karyawan&action=display' ) ],
	'Kas Masuk Pos Dari Dealer'            => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkm/kas-masuk-pos-dari-dealer&action=display' ) ],
	'Kas Masuk Dealer Dari Pos'            => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkm/kas-masuk-dealer-dari-pos&action=display' ) ],
	'Kas Keluar Subsidi Dealer 2'          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkk/kas-keluar-subsidi-dealer-2&action=display' ) ],
	'Kas Keluar Retur Harga'               => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkk/kas-keluar-retur-harga&action=display' ) ],
	'Kas Keluar Insentif Sales'            => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkk/kas-keluar-insentif-sales&action=display' ) ],
	'Kas Keluar Potongan Khusus'           => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkk/kas-keluar-potongan-khusus&action=display' ) ],
	'Kas Keluar Ke Bank Unit - H1'         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkk/kas-keluar-bank-unit&action=display' ) ],
	'Kas Keluar Umum'                      => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkk/kas-keluar-umum&action=display' ) ],
	'Kas Keluar Ke Bank Bengkel - H2'      => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkk/kas-keluar-bank-bengkel&action=display' ) ],
	'Kas Keluar Pinjaman Karyawan'         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkk/kas-keluar-pinjaman-karyawan&action=display' ) ],
	'Kas Keluar Dealer Ke Pos'             => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkk/kas-keluar-dealer-pos&action=display' ) ],
	'Kas Keluar Pos Ke Dealer'             => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkk/kas-keluar-pos-dealer&action=display' ) ],
	'Kas Keluar Bayar Hutang Unit'         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkk/kas-keluar-bayar-hutang-unit&action=display' ) ],
	'Kas Keluar Bayar Hutang BBN'          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkk/kas-keluar-bayar-hutang-bbn&action=display' ) ],
	'Kas Keluar Bayar BBN Progresif'       => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkk/kas-keluar-bayar-bbn-progresif&action=display' ) ],
	'Kas Keluar Bayar BBN Acc'             => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkk/kas-keluar-bayar-bbn-acc&action=display' ) ],
	'Kas Acc'             		   		   => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkk/kas-keluar-acc&action=display' ) ],
	'Kas Transfer'                         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkk/kas-transfer&action=display' ) ],
	'Bank Masuk Inden'                     => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbmhd/bank-masuk-inden&action=display' ) ],
	'Bank Masuk Leasing'                   => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbmhd/bank-masuk-leasing&action=display' ) ],
	'Bank Masuk Scheme'                    => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbmhd/bank-masuk-scheme&action=display' ) ],
	'Bank Masuk Uang Muka Kredit'          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbmhd/bank-masuk-uang-muka-kredit&action=display' ) ],
	'Bank Masuk Uang Muka Tunai'           => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbmhd/bank-masuk-uang-muka-tunai&action=display' ) ],
	'Bank Masuk Piutang Kons UM'           => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbmhd/bank-masuk-piutang-kons-um&action=display' ) ],
	'Bank Masuk Sisa Piutang Kons'         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbmhd/bank-masuk-sisa-piutang-kons&action=display' ) ],
	'Bank Masuk BBN Progresif'             => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbmhd/bank-masuk-bbn-progresif&action=display' ) ],
	'Bank Masuk Dari Kas'                  => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbmhd/bank-masuk-dari-kas&action=display' ) ],
	'Bank Masuk Umum'                      => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbmhd/bank-masuk-umum&action=display' ) ],
	'Bank Masuk Penjualan Multi'           => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbmhd/bank-masuk-penjualan-multi&action=display' ) ],
	'Bank Masuk Penjualan Multi - PL'      => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbmhd/bank-masuk-penjualan-multi-pl&action=display' ) ],
	'Bank Keluar Subsidi Dealer 2'         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbkhd/bank-keluar-subsidi-dealer-2&action=display' ) ],
	'Bank Keluar Retur Harga'              => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbkhd/bank-keluar-retur-harga&action=display' ) ],
	'Bank Keluar Insentif Sales'           => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbkhd/bank-keluar-insentif-sales&action=display' ) ],
	'Bank Keluar Potongan Khusus'          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbkhd/bank-keluar-potongan-khusus&action=display' ) ],
	'Bank Keluar Bayar Hutang Unit'        => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbkhd/bank-keluar-bayar-hutang-unit&action=display' ) ],
	'Bank Keluar Bayar Hutang BBN'         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbkhd/bank-keluar-bayar-hutang-bbn&action=display' ) ],
	'Bank Keluar Bayar BBN Progresif'      => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbkhd/bank-keluar-bayar-bbn-progresif&action=display' ) ],
	'Bank Keluar Ke Kas'                   => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbkhd/bank-keluar-kas&action=display' ) ],
	'Bank Keluar Umum'                     => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbkhd/bank-keluar-umum&action=display' ) ],
	'Bank Transfer'                        => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbkhd/bank-transfer&action=display' ) ],
	'DK -> Kwitansi Kirim Tagih'           => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/kwitansi-kirim-tagih&action=display' ) ],
	'DK -> Kwitansi UM Konsumen'           => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/kwitansi-um-konsumen&action=display' ) ],
	'DK -> Kwitansi Pelunasan Leasing'     => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/kwitansi-pelunasan-leasing&action=display' ) ],
	'BK -> Kwitansi Bank Bayar BBN'        => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttbkhd/kwitansi-bank-bayar-bbn&action=display' ) ],
	'KK -> Kwitansi Kas Bayar BBN'         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkk/kwitansi-kas-bayar-bbn&action=display' ) ],
	'DK -> Kwitansi BBN'                   => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/kwitansi-bbn&action=display' ) ],
	'DK -> Kwitansi DP PO Leasing'         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/kwitansi-dp-po-leasing&action=display' ) ],
	'DK -> Kwitansi DP Konsumen'           => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/kwitansi-dp-konsumen&action=display' ) ],
	'DK -> Kwitansi Penjualan Leasing'     => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/kwitansi-penjualan-leasing&action=display' ) ],
	'DK -> Kwitansi Penjualan Tunai'       => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/kwitansi-penjualan-tunai&action=display' ) ],
	'DK -> Kwitansi Jasa Perantara'        => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttdk/kwitansi-jasa-perantara&action=display' ) ],
	'KG -> Kwitansi Kirim Tagih'           => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttkg/index&action=display' ) ],
	//	'Kas Saldo'                            => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tsaldoka/index&action=display' ) ],
	//	'Bank Saldo'                           => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tsaldobank/index&action=display' ) ],
	'Push INV1'                            => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'site/push-inv1&action=display' ) ],
	'Akuntansi'                            => [ 'class' => 'fa fa-calculator', 'url' => '#' ],
	'Jurnal Adjustment'                    => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttgeneralledgerhd/adjustment&action=display' ) ],
	'Jurnal Transaksi'                     => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttgeneralledgerhd/transaksi&action=display' ) ],
	'Jurnal Pemindah Bukuan'               => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttgeneralledgerhd/pindahbuku&action=display' ) ],
	'Saldo Awal Motor'                     => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tsaldomotor/index&action=display' ) ],
	'Penyesuaian Nilai Persediaan'         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttamhd/index&action=display' ) ],
	'Data Perkiraan'                       => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'traccount/data-perkiraan&action=display' ) ],
	'Edit Perkiraan'                       => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'traccount/index&action=display' ) ],
	'Neraca Awal'                          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'traccount/neraca-awal&action=display' ) ],
	'Tutup Buku'                           => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tsaldoaccount/index&action=update' ) ],
	'Validasi Jurnal'                      => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttgeneralledgerhd/validasi-jurnal&action=display' ) ],
	'Memorial Penyusutan'                  => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'trmp/index&action=display' ) ],
	'Marketing'                            => [ 'class' => 'fa fa-poll', 'url' => '#' ],
	'Team dan Sales Pass'                  => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttmsalestarget/indexteamsales&action=display' ) ],
	'Target Sales'                         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttmsalestarget/index&action=display' ) ],
	'Market Share'                         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttmmarketshare/index&action=display' ) ],
	'DO'                                   => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttmdo/index&action=display' ) ],
	'PO'                                   => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttmpo/index&action=display' ) ],
	'PC'                                   => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttmpc/index&action=display' ) ],
	'Dashboard'                            => [ 'class' => 'glyphicon glyphicon-dashboard', 'url' => '#' ],
	'Dashboard Akuntansi'                  => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/dashboard-akuntansi&action=display' ) ],
	'Dashboard Akunting'                   => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/dashboard-akunting&action=display' ) ],
	'Dashboard Neraca'                     => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/dashboard-neraca&action=display' ) ],
	'Dashboard Rasio Konsolidasi'          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/dashboard-rasio-konsolidasi&action=display' ) ],
	'Dashboard CDB'                        => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/dashboard-cdb&action=display' ) ],
	'Dashboard Marketing Konsolidasi'      => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/dashboard-markonsol&action=display' ) ],
	'Dashboard Marketing Monitoring'       => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/dashboard-marketing-monitoring&action=display' ) ],
	'Dashboard Akuntansi Konsolidasi'      => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/dashboard-akukonsol&action=display' ) ],
	'Dashboard Marketing'                  => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/dashboard-marketing&action=display' ) ],
    'Laporan Dashboard'                    => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/laporan-dashboard&action=display' ) ],
	'Aksesoris'                            => [ 'class' => 'fa fa-gift', 'url' => '#' ],
	'[SV] Invoice Surat Jalan'             => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttsdhd/invoice-servis&action=display' ) ],
	'[SI] Invoice Penjualan'               => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttsihd/invoice-penjualan&action=display' ) ],
	'[PS] Penerimaan Barang'               => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttpshd/index&action=display' ) ],
	'[PI] Invoice Pembelian'               => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttpihd/index&action=display' ) ],
	'[TI] Transfer'                        => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tttihd/index&action=display' ) ],
	'[TA] Penyesuaian'                     => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'tttahd/index&action=display' ) ],
	'[SR] Retur Penjualan'                 => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttsrhd/index&action=display' ) ],
	'[PR] Retur Pembelian'                 => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttprhd/index&action=display' ) ],
	'Laporan'                              => [ 'class' => 'fa fa-book-reader', 'url' => '#' ],
	'Jurnal Memorial'                      => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttgeneralledgerhd/memorial&action=display' ) ],
	'Daftar Surat Jalan Supplier'          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-surat-jalan-supplier&action=display' ) ],
	'Daftar Faktur Beli'                   => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-faktur-beli&action=display' ) ],
	'Daftar Inden'                         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-inden&action=display' ) ],
	'Daftar Data Konsumen'                 => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-data-konsumen&action=display' ) ],
	'Daftar Surat Jalan Konsumen'          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-surat-jalan-konsumen&action=display' ) ],
	'Daftar Retur Konsumen'                => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-retur-konsumen&action=display' ) ],
	'Cetak Masal Bukti DK - SK'            => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/cetak-masal-bukti-dksk&action=display' ) ],
	'Daftar Surat Jalan Mutasi ke Dealer'  => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-surat-jalan-mutasi-dealer&action=display' ) ],
	'Daftar Surat Jalan Mutasi ke Pos'     => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-surat-jalan-mutasi-pos&action=display' ) ],
	'Daftar Penerimaan Barang dari Dealer' => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-penerimaan-barang-dealer&action=display' ) ],
	'Daftar Penerimaan Barang dari Pos'    => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-penerimaan-barang-pos&action=display' ) ],
	'Daftar Kas Masuk'                     => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-kas-masuk&action=display' ) ],
	'Daftar Kas Keluar'                    => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-kas-keluar&action=display' ) ],
	'Daftar Bank Masuk'                    => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-bank-masuk&action=display' ) ],
	'Daftar Bank Keluar'                   => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-bank-keluar&action=display' ) ],
	'Daftar Saldo Kas - Bank'              => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-saldo-kas-bank&action=display' ) ],
	'Jurnal Umum'                          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/jurnal-umum&action=display' ) ],
	'Neraca Percobaan'                     => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/neraca-percobaan&action=display' ) ],
	'Neraca dan Rugi Laba'                 => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/neraca-rugi-laba&action=display' ) ],
	'Laporan Harian'                       => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/laporan-harian&action=display' ) ],
	'Laporan Piutang'                      => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/laporan-piutang&action=display' ) ],
	'Laporan Hutang'                       => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/laporan-hutang&action=display' ) ],
	'Daftar Stock Motor Acct'              => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-stock-motor-acct&action=display' ) ],
	'Konsolidasi Akuntansi'                => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/konsolidasi-akuntansi&action=display' ) ],
	'Margin Sales Force'                    => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/margin-sales-force&action=display' ) ],
	'Daftar Motor'                         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-motor&action=display' ) ],
	'Daftar STNK dan BPKB'                 => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-stnk-bpkb&action=display' ) ],
	'Daftar Riwayat Motor'                 => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-riwayat-motor&action=display' ) ],
	'Daftar Stock Motor Fisik'             => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-stock-motor-fisik&action=display' ) ],
	'Rekap Harian'                         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/rekap-harian&action=display' ) ],
	'Rekap Bulanan'                        => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/rekap-bulanan&action=display' ) ],
	'Rekap Pivot Tabel'                    => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/rekap-pivot-tabel&action=display' ) ],
	'Dashboard 1'                          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/dashboard-1&action=display' ) ],
	'Dashboard 2'                          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/dashboard-2&action=display' ) ],
	'Dashboard 3'                          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/dashboard-3&action=display' ) ],
    'Daftar [SV] Invoice Surat Jalan'      => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-invoice-servis&action=display' ) ],
    'Daftar [SI] Invoice Penjualan'        => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-invoice-penjualan&action=display' ) ],
    'Daftar [PS] Penerimaan Barang'        => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-penerimaan-barang&action=display' ) ],
    'Daftar [PI] Invoice Pembelian'        => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-invoice-pembelian&action=display' ) ],
    'Daftar [TI] Transfer'                 => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-transfer&action=display' ) ],
    'Daftar [TA] Penyesuaian'              => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-penyesuaian&action=display' ) ],
    'Daftar [SR] Retur Penjualan'          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-retur-penjualan&action=display' ) ],
    'Daftar [PR] Retur Pembelian'          => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/daftar-retur-pembelian&action=display' ) ],
    'Laporan Astra'                        => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/laporan-astra&action=display' ) ],
	'Laporan Query'                        => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/laporan-query&action=display' ) ],
	'Laporan Data'                         => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/laporan-data&action=display' ) ],
	'Laporan User Log'                     => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'report/laporan-user-log&action=display' ) ],
	//	'Validasi'                             => [ 'class' => 'fa fa-unlink', 'url' => '#' ],
	'[PO] Order Pembelian'                 => [ 'class' => 'fa fa-check-circle', 'url' => Custom::url( 'ttpohd/index&action=display' ) ],
] );
class Menu extends Component {
	const ADD = 'MenuTambah';
	const UPDATE = 'MenuEdit';
	const DELETE = 'MenuHapus';
	const PRINT = 'MenuCetak';
	const VIEW = 'MenuView';
	const DISPLAY = 'MenuDisplay';
	//define("ATTRIBUTE","");
	static function akses( $menuText, $akses ) {
		$akses_ = Yii::$app->session->get( '_akses' );
		return ArrayHelper::getValue( $akses_, [ $menuText, $akses ], 0 );
	}
	static function getMenu() {
		$id = \Yii::$app->user->id;
		/** @var User $user */
		$user      = User::findOne( $id );
		$kodeAkses = $user->KodeAkses;
		try {
			$arr = Yii::$app->db->createCommand( "select  
				REPLACE(MenuInduk,'&','') AS MenuInduk, 
				REPLACE(MenuText,'&','') AS MenuText, 
				MenuTambah, MenuEdit,MenuHapus,MenuCetak,MenuView
				FROM tuakses WHERE KodeAkses = :kodeAkses AND MenuStatus = 1				
				ORDER BY MenuInduk,MenuNo;", [ ':kodeAkses' => $kodeAkses ] )
			                    ->queryAll();
			if ( $arr == null ) {
				return '';
			}
			array_unshift($arr,[
			    'MenuInduk'=> 'System',
			    'MenuText'=> 'Scan',
			    'MenuTambah'=> '1',
			    'MenuEdit'=> '1',
			    'MenuHapus'=> '1',
			    'MenuCetak'=> '1',
			    'MenuView'=> '1',
            ]);
			$result = ArrayHelper::map( $arr, 'MenuTambah', 'MenuView', 'MenuText' );
			foreach ( $arr as $value ) {
				$result[ $value[ 'MenuText' ] ] = $value;
			}
			Yii::$app->session->set( '_akses', $result );
			$menu                = [];
			$menu[ 'System' ]    = self::createMenu( 'System', $arr );
			$menu[ 'Data' ]      = self::createMenu( 'Data', $arr );
			$menu[ 'Pembelian' ] = self::createMenu( 'Pembelian', $arr );
			$menu[ 'Penjualan' ] = self::createMenu( 'Penjualan', $arr );
			$menu[ 'Mutasi' ]    = self::createMenu( 'Mutasi', $arr );
			$menu[ 'Keuangan' ]  = self::createMenu( 'Keuangan', $arr );
			$menu[ 'Akuntansi' ] = self::createMenu( 'Akuntansi', $arr );
			$menu[ 'Marketing' ] = self::createMenu( 'Marketing', $arr );
			$menu[ 'Dashboard' ] = self::createMenu( 'Dashboard', $arr );
			$menu[ 'Aksesoris' ] = self::createMenu( 'Aksesoris', $arr );
			$menu[ 'Laporan' ]   = self::createMenu( 'Laporan', $arr );
//			$menu[ 'Validasi' ]  = self::createMenu( 'Validasi', $arr );
			$menuBuilder = '';
			foreach ( $menu as $k => $item ) {
				if ( isset( $result[ $k ] ) && $item != '' ) {
//					if ( $item != '' ) {
					$menuBuilder .= '<li class="treeview">
                                <a href="' . self::val( $k, 'url' ) . '">
                                    <i class="' . self::val( $k, 'class' ) . '"></i> <span>' . $k . '</span>
                                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i></span>
                                </a>
                                <ul class="treeview-menu">' . $item . '</ul>
                           </li>';
//					}
				}
			}
			return $menuBuilder;
		} catch ( Exception $e ) {
		}
	}
	static function createMenu( $parent, &$arr ) {
		$menu = '';
		foreach ( $arr as $k => $m ) {
			if ( in_array( $m[ 'MenuText' ], \Yii::$app->params[ 'menuHide' ] ) ) {
				continue;
			}
			if ( $m[ 'MenuInduk' ] == $parent ) {
				$child = self::createMenu( $m[ 'MenuText' ], $arr );
				if ( $child == '' ) {
					if ( $m[ 'MenuText' ] == '-' ) {
						$menu .= '<li class="divider"></li>';
					} else {
						$menu .= '<li><a href="' . self::val( $m[ 'MenuText' ], 'url' ) . '"><i class="fa fa-check-circle"></i> ' . $m[ 'MenuText' ] . '</a></li>';
					}
				} else {
					$menu .= '<li class="treeview">
                                <a href="' . self::val( $k, 'url' ) . '">
                                    <i class="' . self::val( $k, 'class' ) . '"></i> <span>' . $m[ 'MenuText' ] . '</span>
                                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i></span>
                                </a>
                                <ul class="treeview-menu">' . $child . '</ul>
                           </li>';
				}
				unset( $arr[ '$k' ] );
			}
		}
		return $menu;
	}
	static function val( $menu, $tipe ) {
		return ArrayHelper::getValue( ATTRIBUTE, "$menu.$tipe", ( $tipe == 'url' ? '#' : '' ) );
	}
	static function getUserLokasi() {
		$q = new Query();
		return $q->select( "UserID, KodeAkses, tuser.LokasiKode, tdlokasi.LokasiNomor, tdlokasi.LokasiNama, tdlokasi.LokasiStatus" )
		         ->from( "TUser" )
		         ->leftJoin( "tdlokasi", "tdlokasi.LokasiKode = TUser.LokasiKode" )
		         ->where( [ 'UserID' => Yii::$app->user->id ] )
		         ->one();
	}
	static function showOnlyHakAkses( $Akses = [], $returnTrue = null, $returnFalse = null ) {
		if ( in_array_insensitive( self::getUserLokasi()[ 'KodeAkses' ], $Akses ) ) {
			if ( $returnTrue !== null ) {
				return $returnTrue;
			} else {
				return true;
			}
		} else {
			if ( $returnFalse !== null ) {
				return $returnFalse;
			} else {
				return false;
			}
		}
	}
}