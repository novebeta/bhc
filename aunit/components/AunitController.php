<?php
namespace aunit\components;
use Codeception\PHPUnit\ResultPrinter\HTML;
use common\components\Custom;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
class AunitController extends Controller {
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => [ '@' ],
					],
				],
			],
		];
	}
	public function beforeAction( $action ) {
		try {
			if ( ! parent::beforeAction( $action ) ) {
				return false;
			}
			if(Yii::$app->session->get( '_nav' ) == null){
				return $this->redirect( Custom::urllgn( 'site/dealer' ) );
			}
			if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] === 'create' ) {
				TUi::$actionMode = Menu::ADD;
			}
			if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] === 'update' ) {
				TUi::$actionMode = Menu::UPDATE;
			}
			if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] === 'view' ) {
				TUi::$actionMode = Menu::VIEW;
			}
			if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] === 'display' ) {
				TUi::$actionMode = Menu::DISPLAY;
			}
			if ( ! Yii::$app->user->isGuest ) {
				if ( Yii::$app->session[ 'userSessionTimeout' ] < time() ) {
					return $this->redirect( Custom::urllgn( 'site/logout' ) );
				} else {
					if ( $action->id != 'waktu' ) {
						Yii::$app->session->set( 'userSessionTimeout', time() + Yii::$app->params[ 'sessionTimeoutSeconds' ] );
					}
					return true;
				}
			} else {
				return $this->redirect( Custom::urllgn( 'site/logout' ) );
			}
		} catch ( BadRequestHttpException $e ) {
			return false;
		}
	}

	public function render($view, $params = []){
		if(YII_DEBUG){
			Yii::debug(VarDumper::dumpAsString($params));
		}
		return parent::render($view,$params);
	}

	/**
	 * @param $modelName
	 * @param $id
	 *
	 * @return ActiveRecord
	 * @throws NotFoundHttpException
	 */
	protected function findModelBase64( $modelName, $id ) {
		try {
			$className  = '\aunit\models\\' . $modelName;
			$modelObj   = Yii::createObject( [
				'class' => $className,
			] );
			$primaryKey = $modelObj::getTableSchema()->primaryKey;
			$fields     = $primaryKey;
			$condition  = [];
			$val        = explode( "||", base64_decode( $id ) );
			for ( $i = 0; $i < count( $fields ) && $i < count( $val ); $i ++ ) {
				$condition[ $fields[ $i ] ] = $val[ $i ];
			}
			return $modelObj::findOne( $condition );
//			throw new NotFoundHttpException( 'Data tidak ditemukan.' );
		} catch ( InvalidConfigException $e ) {
			return null;
		}
	}
	/**
	 * @param yii\db\ActiveRecord $model
	 *
	 * @return string
	 */
	protected function generateIdBase64FromModel( $model ) {
		$modelName   = get_class( $model );
		$primaryKeys = $modelName::getTableSchema()->primaryKey;
		return base64_encode( implode( '||', $model->getAttributes( $primaryKeys ) ) );
	}
	protected function decodeIdBase64( $modelName, $id ) {
		$className  = '\aunit\models\\' . $modelName;
		$modelObj   = Yii::createObject( [
			'class' => $className,
		] );
		$primaryKey = $modelObj::getTableSchema()->primaryKey;
		$fields     = $primaryKey;
		$condition  = [];
		$val        = explode( "||", base64_decode( $id ) );
		for ( $i = 0; $i < count( $fields ) && $i < count( $val ); $i ++ ) {
			$condition[ $fields[ $i ] ] = $val[ $i ];
		}
		return $condition;
	}
	/**
	 * @param yii\db\ActiveRecord $model
	 *
	 * @return string
	 */
	protected function modelErrorSummary( $model ) {
		return \yii\helpers\Html::errorSummary( $model );
	}
	/**
	 * @param bool $success
	 * @param string $message
	 * @param array $data
	 *
	 * @return array
	 */
	protected function responseJson( $success, $message, $data = [] ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$res = [
			'success' => $success,
			'message' => $message,
		];
		return count( $data ) ? array_merge( $res, $data ) : $res;
	}
	/**
	 * @param string $message
	 * @param array $data
	 *
	 * @return array
	 */
	protected function responseSuccess( $message, $data = [] ) {
		return $this->responseJson( true, $message, $data );
	}
	/**
	 * @param string $message
	 *
	 * @return array
	 */
	protected function responseFailed( $message ) {
		return $this->responseJson( false, $message );
	}
	/**
	 * @param array $data
	 *
	 * @return array
	 */
	protected function responseDataJson( $data = [], $count = false ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return [
			'success'   => true,
			'rows'      => $data,
			'rowsCount' => $count ? $count : count( $data )
		];
	}
	protected function KodeAkses() {
		return Menu::getUserLokasi()[ 'KodeAkses' ];
	}
	protected function UserID() {
		return Menu::getUserLokasi()[ 'UserID' ];
	}
	protected function LokasiKode() {
		return Menu::getUserLokasi()[ 'LokasiKode' ];
	}
	const MODE_SELECT = 'select';
	const MODE_SELECT_MULTISELECT = 'select,multi-select';
	const MODE_SELECT_SHOW_DETAIL = 'select,show-detail';
	const MODE_SELECT_SHOW_DETAIL_MULTISELECT = 'select,show-detail,multi-select';
	const MODE_SELECT_DETAIL = 'select-detail';
	const MODE_SELECT_DETAIL_MULTISELECT = 'select-detail,multi-select';
	/**
	 * @return bool
	 */
	protected function isPopupForm() {
		return ( isset( $_GET[ 'mode' ] ) && $_GET[ 'mode' ] === 'popup-form' );
	}
	protected function setLayoutForPopupForm() {
		if ( isset( $_GET[ 'mode' ] ) && $_GET[ 'mode' ] === 'popup-form' ) {
			$this->layout = $_GET[ 'mode' ];
		}
	}
	/**
	 * @param array|string $data
	 *
	 * @return string
	 */
	protected function responseToPopupForm( $data ) {
		$this->layout = 'popup-form-return';
		return $this->renderContent( is_array( $data ) ? json_encode( $data ) : ( is_string( $data ) ? "'" . $data . "'" : $data ) );
	}
	
	/**
	 * @param array $params
	 * key :
	 * modelName
	 * urlIndex - default to 'index'
	 * urlUpdate - default to 'update'
	 *
	 * @return Response
	 */
	protected function deleteWithSP($params){
		$urlIndex = 'index';
		$urlUpdate = 'update';
		extract($params, EXTR_OVERWRITE);
        $request = Yii::$app->request;
		$_POST['Action'] = 'Delete';
		$className  = '\aunit\models\\' . $modelName;
		$primaryKeys = $className::getTableSchema()->primaryKey;
		$id = $_REQUEST['id'];
		$model = $this->findModelBase64($modelName, $id);
		$_POST = array_merge($_POST,$model->getAttributes( $primaryKeys ));
		$result = $className::find()->callSP($_POST);
        if ($request->isAjax){
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result[$primaryKeys[0]] == '--') {
                return $this->redirect([$urlIndex]);
            } else {
                $model = $className::findOne($result[$primaryKeys[0]]);
				if(empty($model)){
					$model = $className::find()->one();
				}
				if(empty($model)){
					return $this->redirect([$urlIndex]);
				}
                return $this->redirect([$urlUpdate, 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
            }
        }
    }
}