<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\TdAction;
use aunit\models\Tdsales;
use aunit\models\Tdteam;
use aunit\models\Tdvariabel;
use aunit\models\Ttmsalestarget;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtmsalestargetController implements the CRUD actions for Ttmsalestarget model.
 */
class TtmsalestargetController extends AunitController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actions() {
		$colGrid  = null;
		$joinWith = [
			'sales' => [ 'type' => 'INNER JOIN' ],
		];
		$join     = [];
		$where    = [];
//		if ( isset( $_POST['query'] ) ) {
//			$r = Json::decode( $_POST['query'], true )['r'];
//			switch ( $r ) {
//				case 'ttsd/mutasi-eksternal-dealer':
//					$colGrid  = Ttsd::colGridMutasiEksternalDealer();
//					$joinWith = [
//						'lokasi' => [
//							'type' => 'INNER JOIN'
//						],
//						'dealer' => [
//							'type' => 'INNER JOIN'
//						],
//					];
//					break;
//				case 'ttsd/invoice-eksternal':
//					$colGrid  = Ttsd::colGridInvoiceEksternal();
//					$joinWith = [
//						'lokasi' => [
//							'type' => 'INNER JOIN'
//						],
//						'dealer' => [
//							'type' => 'INNER JOIN'
//						],
//					];
//					break;
//			}
//		}
		return [
			'td' => [
				'class'    => TdAction::class,
				'model'    => Ttmsalestarget::class,
				'colGrid'  => $colGrid,
				'joinWith' => $joinWith
			],
		];
	}
	/**
	 * Lists all Ttmsalestarget models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index' );
	}
	public function actionList() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$postTeamKode                = explode( ',', $requestData[ 'TeamKode' ] );
		$TeamKode                    = [];
		if ( is_array( $postTeamKode ) ) {
			foreach ( $postTeamKode as $item ) {
				$TeamKode[] = base64_decode( $item );
			}
		}
		$query = new Query();
		$query->select( new Expression( "SalesKode as id, concat(SalesKode,' ~ ',REPLACE(SalesNama,'--','')) AS SalesNama, SalesPass" ) )
		      ->from( 'tdsales' )
		      ->andWhere( "SalesStatus = 'A'" )
		      ->andWhere( [ 'IN', 'TeamKode', $TeamKode ] )
		      ->groupBy( 'SalesKode' )
		      ->orderBy( "SalesPass DESC, SalesStatus, SalesKode" );
		$rows = $query->all();
		/* encode row id */
		for ( $i = 0; $i < count( $rows ); $i ++ ) {
			$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
		}
		return [
			'rows'      => $rows,
			'rowsCount' => sizeof( $rows )
		];
	}
	public function actionIndexteamsales() {
		if ( Yii::$app->request->isPost ) {
			Tdteam::updateAll( [ 'TeamPass' => 0 ] );
			Tdsales::updateAll( [ 'SalesPass' => 0 ] );
			Tdteam::updateAll( [ 'TeamIndividu' => $_POST[ 'TeamIndividu' ], 'TeamKolektif' => $_POST[ 'TeamKolektif' ] ] );
			$idsTeamArr  = explode( ',', $_POST[ 'Team' ] );
			$idsSalesArr = explode( ',', $_POST[ 'Sales' ] );
			$idTeam      = $idSales = [];
			foreach ( $idsTeamArr as $team ) {
				$idTeam[] = base64_decode( $team );
			}
			foreach ( $idsSalesArr as $team ) {
				$idSales[] = base64_decode( $team );
			}
			Tdteam::updateAll( [ 'TeamPass' => 1 ], [ 'IN', 'TeamKode', $idTeam ] );
			Tdsales::updateAll( [ 'SalesPass' => 1 ], [ 'IN', 'SalesKode', $idSales ] );
            Tdvariabel::updateAll(['Nilai' => $_POST['Konsumen']], "Nama = 'PlafonPiutangDP'",);
		}
		return $this->render( 'indexteamsales' );
	}
	/**
	 * Displays a single Ttmsalestarget model.
	 *
	 * @param string $SalesTgl
	 * @param string $SalesKode
	 * @param string $TeamKode
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $SalesTgl, $SalesKode, $TeamKode ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $SalesTgl, $SalesKode, $TeamKode ),
		] );
	}
	/**
	 * Creates a new Ttmsalestarget model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Ttmsalestarget();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'SalesTgl' => $model->SalesTgl, 'SalesKode' => $model->SalesKode, 'TeamKode' => $model->TeamKode ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Ttmsalestarget model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $SalesTgl
	 * @param string $SalesKode
	 * @param string $TeamKode
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $SalesTgl, $SalesKode, $TeamKode ) {
		$model = $this->findModel( $SalesTgl, $SalesKode, $TeamKode );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'SalesTgl' => $model->SalesTgl, 'SalesKode' => $model->SalesKode, 'TeamKode' => $model->TeamKode ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Ttmsalestarget model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $SalesTgl
	 * @param string $SalesKode
	 * @param string $TeamKode
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $SalesTgl, $SalesKode, $TeamKode ) {
		$this->findModel( $SalesTgl, $SalesKode, $TeamKode )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Ttmsalestarget model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $SalesTgl
	 * @param string $SalesKode
	 * @param string $TeamKode
	 *
	 * @return Ttmsalestarget the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $SalesTgl, $SalesKode, $TeamKode ) {
		if ( ( $model = Ttmsalestarget::findOne( [ 'SalesTgl' => $SalesTgl, 'SalesKode' => $SalesKode, 'TeamKode' => $TeamKode ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
