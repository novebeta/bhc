<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\models\Tdbaranglokasi;
use aunit\models\Ttsdhd;
use aunit\models\Ttsditbarang;
use aunit\models\Ttsdittime;
use aunit\models\Vtkbm;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtsdhdController implements the CRUD actions for Ttsdhd model.
 */
class TtsdhdController extends AunitController {
	public function actions() {
		$sqlraw     = null;
		$queryRawPK = null;
		$colGrid    = null;
		$where      = [];
		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			switch ( $r ) {
				case 'ttsdhd/daftar-servis':
					$colGrid = Ttsdhd::colGridDaftarServis();
                    $where      = [
                        [
                            'op'        => 'AND',
                            'condition' => "(SDNo NOT LIKE '%=%')",
                            'params'    => []
                        ]
                    ];
                    if(\aunit\components\Menu::showOnlyHakAkses(['warehouse','gudang'])){
						$whereRaw = '';
						if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
							$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
						}
						$sqlraw     = "";
						$queryRawPK = [ 'SDNo' ];
					}
					break;
				case 'ttsdhd/invoice-servis':
					$colGrid = Ttsdhd::colGridInvoiceServis();
                    $where      = [
                        [
                            'op'        => 'AND',
                            'condition' => "(SDNo NOT LIKE '%=%')",
                            'params'    => []
                        ]
                    ];
					if(\aunit\components\Menu::showOnlyHakAkses(['warehouse','gudang'])){
						$whereRaw = '';
						if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
							$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
						}
						$sqlraw     = "SELECT
  twsdhd.SDNo, SDTgl, SDJamMasuk, SDNoUrut, SDStatus, SDKmSekarang, SDKmBerikut, SDKeluhan, KarKode, twsdhd.LokasiKode, SDJamDikerjakan, SDJamSelesai, SDTotalWaktu, SDTotalJasa, SDTotalQty, SDTotalPart, SDTotalGratis, SDTotalNonGratis, SDDiscFinal, SDTotalPajak, SDTotalBiaya, SVNo, SVTgl, twsdhd.UserID, KodeTrans, 
  twsdhd.MotorNoPolisi, tdcustomer.CusNama, MotorType AS MotorNama, MotorWarna, PKBNo, IFNULL(twsditbarang.Cetak, '--') AS PLCetak
FROM
  twsdhd
    INNER JOIN tmotor   ON twsdhd.MotorNoPolisi = tmotor.PlatNo
    INNER JOIN ttdk   ON ttdk.DKNo = tmotor.DKNo
    INNER JOIN tdcustomer   ON tdcustomer.CusKode = tdcustomer.CusKode
  LEFT OUTER JOIN
    (SELECT
      SDNo, Cetak, SDPickingPrint
    FROM
      twsditbarang
    GROUP BY SDNo) twsditbarang
    ON twsdhd.SDNo = twsditbarang.SDNo";
						$queryRawPK = [ 'SDNo' ];
					}
					break;
				case 'ttsdhd/select':
					$colGrid    = Ttsdhd::colGridSD();
                    $whereRaw = '';
                    if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
                        $whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
                    }
					$sqlraw     = "";
					$queryRawPK = [ 'SDNo' ];
					break;
			}
		}
		return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Ttsdhd::class,
				'colGrid'    => $colGrid,
				'queryRaw'   => $sqlraw,
				'queryRawPK' => $queryRawPK,
                'where'      => $where,
//				'joinWith'   => [
//					'customer' => [
//						'type' => 'INNER JOIN'
//					],
//				]
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionSelect() {
		$urlDetail    = Url::toRoute( [ 'ttsditbarang/index', 'jenis' => 'FillItemSDSI' ] );
		$this->layout = 'select-mode';
		return $this->render( 'select', [
			'arr'       => [
				'cmbTxt'    => [
					'SDNo'          => 'No SD',
					'MotorNoPolisi' => 'No Polisi',
					'KarKode'       => 'Karyawan',
					'Cetak'         => 'Cetak',
					'KodeTrans'     => 'Kode Trans',
					'SDKeterangan'  => 'Keterangan',
					'SINo'          => 'No SI',
					'PONo'          => 'No PO',
//					'PosKode'       => 'Pos',
					'UserID'        => 'UserID',
				],
				'cmbNum'    => [
				],
				'cmbTgl'    => [
					'SDTgl' => 'Tanggal SD',
				],
				'sortname'  => "SDTgl",
				'sortorder' => 'desc'
			],
			'options'   => [
				'colGrid' => Ttsdhd::colGridSD(),
				'mode'    => self::MODE_SELECT_DETAIL_MULTISELECT
			],
			'title'     => 'Daftar Service',
			'urlDetail' => $urlDetail
		] );
	}
	public function actionOrder() {
		$urlDetail    = Url::toRoute( [ 'ttsditbarang/index', 'jenis' => 'FillItemSDSI' ] );
		$this->layout = 'select-mode';
		return $this->render( 'select', [
			'arr'       => [
				'cmbTxt'    => [
					'SDNo'          => 'No SD',
					'MotorNoPolisi' => 'No Polisi',
					'KarKode'       => 'Karyawan',
					'Cetak'         => 'Cetak',
					'KodeTrans'     => 'Kode Trans',
					'SDKeterangan'  => 'Keterangan',
					'SINo'          => 'No SI',
					'PONo'          => 'No PO',
					'PosKode'       => 'Pos',
					'UserID'        => 'UserID',
				],
				'cmbNum'    => [
				],
				'cmbTgl'    => [
					'SDTgl' => 'Tanggal SD',
				],
				'sortname'  => "SDTgl",
				'sortorder' => 'desc'
			],
			'options'   => [
				'colGrid' => Ttsdhd::colGridSD(),
				'mode'    => self::MODE_SELECT_MULTISELECT
			],
			'title'     => 'Daftar Service',
			'urlDetail' => $urlDetail
		] );
	}
	public function actionLoop() {
		$_POST[ 'Action' ] = 'Loop';
		$result            = Ttsdhd::find()->callSP( $_POST );
		if ( $result[ 'status' ] == 1 ) {
			Yii::$app->session->addFlash( 'warning', $result[ 'keterangan' ] );
		}
	}
	public function actionDaftarServis() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttsdhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'DaftarServis', [ 'Jenis' => 'SD' ] );
	}
	public function actionDaftarServisCreate() {
		return $this->create( 'SD' );
	}
	public function actionDaftarServisUpdate( $id, $action ) {
		return $this->update( $id, $action, 'SD' );
	}
	public function actionDaftarServisDelete( $id ) {
		return $this->delete( $id, 'SD' );
	}
	public function actionDaftarServisCancel() {
		return $this->cancel( 'SD' );
	}
    public function actionDaftarServisJurnal() {
        return $this->jurnal( 'SD' );
    }
	public function actionDaftarServisPrint( $id ) {
		return $this->print( $id, 'SD' );
	}
	public function actionInvoiceServis() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttsdhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'InvoiceServis', [ 'Jenis' => 'SV' ] );
	}
	public function actionInvoiceServisCreate() {
		return $this->create( 'SV' );
	}
	public function actionInvoiceServisUpdate( $id, $action ) {
		return $this->update( $id, $action, 'SV' );
	}
	public function actionInvoiceServisDelete( $id ) {
		return $this->delete( $id, 'SV' );
	}
	public function actionInvoiceServisCancel() {
		return $this->cancel( 'SV' );
	}
	public function actionInvoiceServisPrint( $id ) {
		return $this->print( $id, 'SV' );
	}
    public function actionInvoiceServisJurnal() {
        return $this->jurnal( 'SV' );
    }
	private function create( $Jenis ) {
		$model                    = new Ttsdhd();
		$model->SDNo              = General::createTemporaryNo( 'SD', 'ttsdhd', 'SDNo' );
		$model->SDTgl             = date( 'Y-m-d H:i:s' );
		$model->SDJamMasuk        = date( 'Y-m-d H:i:s' );
		$model->SDJamDikerjakan   = date( 'Y-m-d H:i:s' );
		$model->SDJamSelesai      = date( 'Y-m-d H:i:s' );
		$model->SDStatus          = "Menunggu";
		$model->MotorNoPolisi     = "UMUM";
		$model->KarKode           = "";
		$model->LokasiKode        = "";
		$model->SDPembawaMotor    = "Umum";
		$model->SDTotalWaktu      = 0;
		$model->SDTotalJasa       = 0;
		$model->SDTotalQty        = 0;
		$model->SDTotalPart       = 0;
		$model->SDTotalGratis     = 0;
		$model->SDTotalNonGratis  = 0;
		$model->SDDiscFinal       = 0;
		$model->SDTotalPajak      = 0;
		$model->SDTotalBiaya      = 0;
		$model->SDKasKeluar       = 0;
		$model->SDUangMuka        = 0;
		$model->SDDurasi          = 0;
		$model->SVNo              = "--";
		$model->SENo              = "--";
		$model->SVTgl             = date( 'Y-m-d H:i:s' );
		$model->UserID            = $this->UserID();
		$model->SDPembawaMotor    = "--";
		$model->SDHubunganPembawa = "Pemilik";
		$model->NoGLSD            = "--";
		$model->NoGLSV            = "--";
		$model->PKBNo             = "--";
		$model->PosKode           = $this->LokasiKode();
		$model->Cetak             = "Belum";
		$model->SDTOP             = 0;
		$model->SDTerbayar        = 0;
		$model->SDKmSekarang      = 0;
		$model->SDKmBerikut       = 0;
		$model->KlaimKPB          = "Belum";
		$model->KlaimC2           = "Belum";
		$model->ASS               = "";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttsdhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTServis                  = Ttsdhd::find()->ttsdhdFillByNo()
		                                    ->where( [ 'twsdhd.SDNo' => $model->SDNo, 'twsdhd.SVNo' => $model->SVNo ] )
		                                    ->asArray( true )->one();
		$id                         = $this->generateIdBase64FromModel( $model );
		$dsTServis[ 'SDNoView' ]    = $result[ 'SDNo' ];
		$dsTServis[ 'StatusBayar' ] = 'BELUM';
		return $this->render( 'create', [
			'model'     => $model,
			'dsTServis' => $dsTServis,
			'id'        => $id,
			'Jenis'     => $Jenis
		] );
	}
	private function update( $id, $action, $Jenis ) {
		$index = Ttsdhd::getRoute( $Jenis, [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		/** @var Ttsdhd $model */
		/** @var Ttsdhd $dsTServis */
		$model     = $this->findModelBase64( 'Ttsdhd', $id );
		$dsTServis = Ttsdhd::find()->ttsdhdFillByNo()
		                   ->where( [ 'twsdhd.SDNo' => $model->SDNo, 'twsdhd.SVNo' => $model->SVNo ] )
		                   ->asArray( true )->one();

		$Terbayar = floatval( $dsTServis[ 'SDTerbayar' ] );
		if ( Yii::$app->request->isPost ) {
			$post = array_merge( $dsTServis, \Yii::$app->request->post() );
			if ( $Jenis != 'SD' ) { /* SD */
				$post[ 'SVNoBaru' ] = str_replace( 'SD', 'SV', $post[ 'SDNo' ] );
			}
			$post[ 'SDTgl' ]      = $post[ 'SDTgl' ] . ' ' . $post[ 'SDJam' ];
			$post[ 'SVTgl' ]      = $post[ 'SVTgl' ] . ' ' . $post[ 'SVJam' ];
			$post[ 'LokasiKode' ] = 'PosKode';
			$post[ 'Action' ]     = 'Update';
			$result               = Ttsdhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model = Ttsdhd::findOne( $result[ 'SDNo' ] );
			$id    = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 ) {
				$post[ 'SDNoView' ] = $result[ 'SDNo' ];
				return $this->redirect( Ttsdhd::getRoute( $Jenis, [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'update', [
					'model'     => $model,
					'dsTServis' => $post,
					'id'        => $id,
					'Jenis'     => $Jenis
				] );
			}
		}
		if ( ( floatval( $dsTServis[ 'SDTotalBiaya' ] ) - $Terbayar ) > 0 ) {
			$dsTServis[ 'StatusBayar' ] = 'BELUM';
		} else if ( ( floatval( $dsTServis[ 'SDTotalBiaya' ] ) - $Terbayar ) <= 0 ) {
			$dsTServis[ 'StatusBayar' ] = 'LUNAS';
		}
		$dsTServis[ 'SisaBayar' ] = floatval( $dsTServis[ 'SDTotalBiaya' ] ) - $Terbayar;
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTServis[ 'Action' ] = 'Load';
			$result                = Ttsdhd::find()->callSP( $dsTServis );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'SDNo' ] = $dsTServis[ 'SDNo' ];
		}
		$dsTServis[ 'SDNoView' ] = $_GET[ 'SDNoView' ] ?? $result[ 'SDNo' ];
		return $this->render( 'update', [
			'model'     => $model,
			'dsTServis' => $dsTServis,
			'id'        => $id,
			'Jenis'     => $Jenis
		] );
	}
	private function delete( $id, $Jenis ) {
		/** @var Ttsdhd $model */
		$model             = $model = $this->findModelBase64( 'Ttsdhd', $_POST[ 'id' ] );
		$_POST[ 'SDNo' ]   = $model->SDNo;
		$_POST[ 'SVNo' ]   = $model->SVNo;
		$_POST[ 'Action' ] = 'Delete';
		$result            = Ttsdhd::find()->callSP( $_POST );
		if ( $result[ 'status' ] == 0 ) {
			return $this->responseSuccess( $result[ 'keterangan' ] );
		} else {
			return $this->responseFailed( $result[ 'keterangan' ] );
		}
	}
	public function actionDelete() {
		$model             = $model = $this->findModelBase64( 'Ttsdhd', $_POST[ 'id' ] );
		$_POST[ 'SDNo' ]   = $model->SDNo;
		$_POST[ 'SVNo' ]   = $model->SVNo;
		$_POST[ 'Action' ] = 'Delete';
		$result            = Ttsdhd::find()->callSP( $_POST );
		if ( $result[ 'status' ] == 0 ) {
			return $this->responseSuccess( $result[ 'keterangan' ] );
		} else {
			return $this->responseFailed( $result[ 'keterangan' ] );
		}
	}
	private function cancel( $Jenis ) {
		/** @var  $model Ttsdhd */
		$post                = \Yii::$app->request->post();
		$post[ 'KodeTrans' ] = $Jenis;
		$post[ 'Action' ]    = 'Cancel';
		$result              = Ttsdhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'SDNo' ] == '--' ) {
			return $this->redirect( [ 'index' ] );
		} else {
			$model = Ttsdhd::findOne( $result[ 'SDNo' ] );
			return $this->redirect( Ttsdhd::getRoute( $Jenis, [ 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] )[ 'update' ] );
		}
	}
	private function print( $id, $jenis ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST[ 'KodeTrans' ] = $jenis;
		$_POST                = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]        = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ]    = Menu::getUserLokasi()[ 'UserID' ];
		$client               = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response             = $client->post( Yii::$app->params[ 'host_report' ] . '/aunit/fsdhd', [
			'body' => Json::encode( $_POST )
		] );
		$html                 = $response->getBody();
		return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	/* Picking List */
	public function actionPickingList( $id ) {
		$this->layout = 'print';
		return $this->render( 'pickingList', [
			'id'            => $id,
			'urlHeaderGrid' => Url::toRoute( [ 'ttsdhd/fill-header-p-l' ] ),
			'urlDetailGrid' => Url::toRoute( [ 'ttsdhd/fill-item-p-l' ] )
		] );
	}
	public function actionFillHeaderPL() {
		$rows = General::cCmd( "SELECT  twSDhd.SDNo, SDTgl, SDPickingNo, SDPickingDate, ttSDitbarang.Cetak 
		FROM ttSDitbarang INNER JOIN ttSDhd ON twSDhd.SDNo = ttSDitbarang.SDNo 
		WHERE twSDhd.SDNo = :SDNo GROUP BY SDPickingNo, SDPickingDate;", [
			':SDNo' => base64_decode( \Yii::$app->request->post( 'id' ) )
		] )->queryAll();
		for ( $i = 0; $i < count( $rows ); $i ++ ) {
			$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'SDPickingNo' ] );
		}
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return [
			'rows'      => $rows,
			'rowsCount' => count( $rows )
		];
	}
	public function actionFillItemPL() {
		$rows                        = General::cCmd( "SELECT ttsditbarang.SDNo, ttsditbarang.SDAuto, ttsditbarang.BrgKode, ttsditbarang.SDQty, ttsditbarang.SDHrgBeli, ttsditbarang.SDHrgJual, ttsditbarang.SDJualDisc, ttsditbarang.SDPajak, tdbarang.BrgNama, tdbarang.BrgSatuan, 
	      IF((ttsditbarang.SDHrgJual * ttsditbarang.SDQty) > 0, ttsditbarang.SDJualDisc / (ttsditbarang.SDHrgJual * ttsditbarang.SDQty) * 100, 0) AS Disc, ttsditbarang.SDQty * ttsditbarang.SDHrgJual - ttsditbarang.SDJualDisc AS Jumlah, 
	      ttsditbarang.LokasiKode, ttsditbarang.SDPickingNo, ttsditbarang.SDPickingDate, tdbarang.BrgGroup, tdbarang.BrgRak1, SaldoQty
	      FROM ttsditbarang 
	      INNER JOIN tdbarang ON ttsditbarang.BrgKode = tdbarang.BrgKode
	      INNER JOIN tdbaranglokasi ON tdbaranglokasi.BrgKode = ttsditbarang.BrgKode AND tdbaranglokasi.lokasiKode = ttsditbarang.lokasiKode
	      WHERE (ttsditbarang.SDPickingNo = :SDPickingNo)
	      ORDER BY ttsditbarang.SDNo, ttsditbarang.SDAuto", [
			':SDPickingNo' => base64_decode( \Yii::$app->request->post( 'id' ) )
		] )->queryAll();
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return [
			'rows'      => $rows,
			'rowsCount' => count( $rows )
		];
	}
	public function actionPrintPickingList() {
		unset( $_POST[ '_csrf-app' ] );
		$_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client            = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response          = $client->post( Yii::$app->params[ 'host_report' ] . '/aunit/plsd', [
			'body' => Json::encode( $_POST )
		] );
		$html              = $response->getBody();
		return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	/* End of Picking List */
	public function actionCheckStock( $BrgKode, $LokasiKode ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return Tdbaranglokasi::find()
		                     ->where( 'BrgKode = :BrgKode AND LokasiKode = :LokasiKode', [ 'BrgKode' => $BrgKode, 'LokasiKode' => $LokasiKode ] )
		                     ->asArray()->one();
	}
	/* Panggil Antrian */
	public function actionPanggilAntrian() {
		return $this->render( 'PanggilAntrian' );
	}
	public function actionPanggilAntrianLoadData() {
		$antrian = ( new \yii\db\Query() )
			->select( 'SDNo, SDNoUrut, ttsdhd.MotorNoPolisi, SDPembawaMotor, SDStatus, SDTgl, CusKelurahan, CusKecamatan, MotorNama' )
			->from( 'ttsdhd' )
			->where( "SVNo ='--'" )
			->andWhere( "DATE(SDTgl) = CURRENT_DATE" )
			->andWhere( "PosKode = :MyPOSLokasiKode", [ ':MyPOSLokasiKode' => $this->LokasiKode() ] )
			->all();
		$result  = [
			'SDNo'           => [],
			'SDNoUrut'       => [],
			'MotorNoPolisi'  => [],
			'SDPembawaMotor' => [],
			'CusKelurahan'   => [],
			'CusKecamatan'   => [],
			'MotorNama'      => []
		];
		foreach ( $antrian as $a ) {
			$a[ 'text' ]                  = $a[ 'SDNo' ];
			$a[ 'id' ]                    = $a[ 'SDNo' ];
			$result[ 'SDNo' ][]           = $a;
			$result[ 'SDNoUrut' ][]       = [ 'id' => $a[ 'id' ], 'text' => $a[ 'SDNoUrut' ] ];
			$result[ 'MotorNoPolisi' ][]  = [ 'id' => $a[ 'id' ], 'text' => $a[ 'MotorNoPolisi' ] ];
			$result[ 'SDPembawaMotor' ][] = [ 'id' => $a[ 'id' ], 'text' => $a[ 'SDPembawaMotor' ] ];
			$result[ 'CusKelurahan' ][]   = [ 'id' => $a[ 'id' ], 'text' => $a[ 'CusKelurahan' ] ];
			$result[ 'CusKecamatan' ][]   = [ 'id' => $a[ 'id' ], 'text' => $a[ 'CusKecamatan' ] ];
			$result[ 'MotorNama' ][]      = [ 'id' => $a[ 'id' ], 'text' => $a[ 'MotorNama' ] ];
		}
		return $this->responseJson( true, '', $result );
	}
	public function actionPanggilAntrianMulaiPanggil() {
		$post = \Yii::$app->request->post();
		$urlName = urlencode($post['urlNama']);
		$urlKelurahan = urlencode($post['urlKelurahan']);
		$urlKecamatan = urlencode($post['urlKecamatan']);
		$urlTujuan = urlencode($post['urlTujuan']);
		$urlGoogle = "https://translate.googleapis.com/translate_tts?ie=UTF-8&q=Panggilan+atas+nama+$urlName+$urlKelurahan+$urlKecamatan+silahkan+menuju+ke+$urlTujuan&tl=id&client=gtx";
		Yii::$app->db->createCommand( "UPDATE ttqshd SET QSPanggil = '" . $post[ 'SDNo' ] . "', QSTujuan = '" . $post[ 'Tujuan' ] . "',  QSPanggilSudah = 'Belum', QSPanggilApa = '" . $post[ 'PanggilApa' ] . "', QsPanggilNama = '" . $urlGoogle . "'; " )->execute();
		return $this->responseSuccess( 'Mulai Panggil' );
	}
	public function actionPanggilAntrianPanggilSudah() {
		Yii::$app->db->createCommand( "UPDATE ttqshd SET QSPanggilSudah = 'Sudah';" )->execute();
		return $this->responseSuccess( 'Panggil Sudah' );
	}
	public function actionPanggilAntrianDatang() {
		$post = \Yii::$app->request->post();
		if ( $post[ 'Tujuan' ] == 'Kasir-Bengkel' ) {
			$field = 'QSKasir';
		} else if ( $post[ 'Tujuan' ] == 'Mekanik S A' ) {
			$field = 'QSMekanik';
		} else {
			$this->responseFailed( 'Pilih Tujuan.' );
		}
		Yii::$app->db->createCommand( "UPDATE ttqshd SET " . $field . " = '" . $post[ 'SDNo' ] . "';" )->execute();
		$kasir   = ( new \yii\db\Query() )
			->select( 'SDNo, SDNoUrut, MotorNoPolisi, SDPembawaMotor, SDStatus, SDTgl' )
			->from( 'ttsdhd' )
			->innerJoin( 'ttqshd', 'ttqshd.QSKasir = ttsdhd.SDNo' )
			->one();
		$mekanik = ( new \yii\db\Query() )
			->select( 'SDNo, SDNoUrut, MotorNoPolisi, SDPembawaMotor, SDStatus, SDTgl' )
			->from( 'ttsdhd' )
			->innerJoin( 'ttqshd', 'ttqshd.QSMekanik = ttsdhd.SDNo' )
			->one();
		return $this->responseSuccess( 'Datang', [
			'kasir'   => $kasir,
			'mekanik' => $mekanik
		] );
	}
	public function actionPanggilAntrianSelesaiKasir() {
		Yii::$app->db->createCommand( "UPDATE ttqshd SET QSKasir = '--';" )->execute();
		return $this->responseSuccess( 'Kasir Selesai' );
	}
	public function actionPanggilAntrianSelesaiMekanik() {
		Yii::$app->db->createCommand( "UPDATE ttqshd SET QSMekanik = '--'" )->execute();
		return $this->responseSuccess( 'Mekanik Selesai' );
	}
	/* End of Panggil Antrian */
	public function actionStartStopMekanik() {
		return $this->render( 'StartStopMekanik' );
	}
	public function actionStartStopMekanikList() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$data                        = [];
		if ( isset( $_POST[ 'SDNo' ] ) ) {
			$data = General::cCmd( "SELECT SDNo, ttsdittime.KarKode, KarNama, Nomor, 
		       DATE_FORMAT(START, '%d/%m/%y %H:%i:%s') AS START, 
		      IF(Finish='2100-12-31 23:59:59','--',DATE_FORMAT(Finish, '%d/%m/%y %H:%i:%s')) AS FINISH, 
		      IF(STATUS='','Diproses',STATUS) AS STATUS FROM ttsdittime
		      INNER JOIN tdkaryawan ON tdkaryawan.KarKode = ttsdittime.KarKode
		      WHERE SDNo LIKE :SDNo AND ttsdittime.KarKode = :KarKode
		      ORDER BY Nomor", [ ':SDNo' => $_POST[ 'SDNo' ],':KarKode' => $_POST['KarKode'] ] )->queryAll();
		}
		return [
			'rows'      => $data,
			'rowsCount' => sizeof( $data )
		];
	}
	public function actionCekKarKodeDiproses() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$data                        = '--';
		if ( isset( $_POST[ 'KarKode' ] ) ) {
			$data = General::cCmd( "SELECT IFNULL(ttsdittime.SDNo,'--')  FROM ttsdittime 
		         INNER JOIN ttsdhd ON ttsdhd.SDNo = ttsdittime.SDNo
		         WHERE ttsdittime.KarKode = :KarKode AND (STATUS = '' OR STATUS = 'Diproses') AND 
               		SVNo = '--' AND SDStatus <> 'Selesai'", [ ':KarKode' => $_POST[ 'KarKode' ] ] )->queryScalar();
			if ( $data === false ) {
				$data = '';
			}
		}
		return [
			'rows'      => $data,
			'rowsCount' => 1
		];
	}
	public function actionStartStopMekanikListUpdate() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$data                        = [];
		switch ( $_POST[ 'oper' ] ) {
			case 'start':
				$Nomor = General::cCmd( "SELECT IFNULL(MAX(NOMOR),0) AS NOMOR FROM ttsdittime  WHERE SDNo = :SDNo",
					[ ':SDNo' => $_POST[ 'SDNo' ] ] )->queryScalar();
				if ( $Nomor === false ) {
					$Nomor = 0;
				}
				$Nomor ++;
				$ttsdittime          = new Ttsdittime;
				$ttsdittime->SDNo    = $_POST[ 'SDNo' ];
				$ttsdittime->KarKode = $_POST[ 'KarKode' ];
				$ttsdittime->Nomor   = $Nomor;
				$ttsdittime->Start   = new Expression( 'NOW()' );
				$ttsdittime->Finish  = '2100-12-31 23:59:59';
				$ttsdittime->Status  = '';
				$ttsdittime->save();
				break;
			case 'stop':
				Ttsdittime::updateAll( [ 'Status' => 'Istirahat', 'Finish' => new Expression( 'NOW()' ) ], [ 'SDNo' => $_POST[ 'SDNo' ], 'Status' => '' ] );
				break;
			case 'finish':
				Ttsdittime::updateAll( [ 'Status' => 'Selesai', 'Finish' => new Expression( 'NOW()' ) ], [ 'SDNo' => $_POST[ 'SDNo' ], 'Status' => '' ] );
				break;
		}
		return [
			'rows'      => $data,
			'rowsCount' => sizeof( $data )
		];
	}
	public function actionStartStopMekanikDurasi() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$SDNo                        = $_POST[ 'SDNo' ];
		if ( $SDNo == '' || $_POST[ 'SDNo' ] == '--' ) {
			$data = 0;
		} else {
			$data = General::cCmd( "SELECT IFNULL(SUM(TIMESTAMPDIFF(MINUTE, START, Finish)),0) 
			FROM ttsdittime WHERE SDNo = :SDNo AND Status <> '' ", [ ':SDNo' => $_POST[ 'SDNo' ] ] )->queryScalar();
		}
		return [
			'rows' => $data,
		];
	}
	/* List Antrian */
	public function actionListAntrian() {
		$dataCompany = \aunit\models\Tzcompany::find()->one();
		return $this->render( 'ListAntrian', [
			'dataCompany' => $dataCompany
		] );
	}
	public function actionListAntrianLoadHeader() {
		$empty   = [ "QSPanggil" => "--", "QSTujuan" => "--", "SDPembawaMotor" => "--", "MotorNoPolisi" => "--", "SDNoUrut" => "--" ];
		$Pgl     = ( new \yii\db\Query() )
			->select( 'QSPanggil, QSTujuan, SDPembawaMotor, MotorNoPolisi, SDNoUrut' )
			->from( 'ttqshd' )
			->innerJoin( 'ttsdhd', 'ttsdhd.SDNo = QSPanggil' )
			->one();
		$Kasir   = ( new \yii\db\Query() )
			->select( 'QSPanggil, QSTujuan, SDPembawaMotor, MotorNoPolisi, SDNoUrut' )
			->from( 'ttqshd' )
			->innerJoin( 'ttsdhd', 'ttsdhd.SDNo = QSKasir' )
			->one();
		$Mekanik = ( new \yii\db\Query() )
			->select( 'QSPanggil, QSTujuan, SDPembawaMotor, MotorNoPolisi, SDNoUrut' )
			->from( 'ttqshd' )
			->innerJoin( 'ttsdhd', 'ttsdhd.SDNo = QSMekanik' )
			->one();
		return $this->responseJson( true, '', [
			'Pgl'     => $Pgl ? $Pgl : $empty,
			'Kasir'   => $Kasir ? $Kasir : $empty,
			'Mekanik' => $Mekanik ? $Mekanik : $empty,
		] );
	}
	public function actionListAntrianLoadAntrian() {
		return $this->responseDataJson( Ttsdhd::find()->getListAntrian( $this->LokasiKode() ) );
	}
	/* End of List Antrian */
	/**
	 * Finds the Ttsdhd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $SDNo
	 * @param string $SVNo
	 *
	 * @return Ttsdhd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $SDNo, $SVNo ) {
		if ( ( $model = Ttsdhd::findOne( [ 'SDNo' => $SDNo, 'SVNo' => $SVNo ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	public function actionHistoryOtomatis() {
		$this->layout = 'popup-form';
		return $this->render( '_formHistory', [ 'data' => $_GET ] );
	}
	public function actionHistoryOtomatisItem( $MotorNoPolisi ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$dt                          = General::cCmd( "SELECT * FROM (SELECT 
              ttsdhd.SDNo, SVNo, SVTgl, BrgNama AS JasaNama, SDQty * SDHrgJual AS SDHrgJual
            FROM
              ttsdhd 
              INNER JOIN ttsditbarang 
                ON ttsditbarang.SDNo = ttsdhd.SDNo 
              INNER JOIN tdbarang 
                ON tdbarang.BrgKode = ttsditbarang.BrgKode 
            WHERE MotorNoPolisi = :MotorNoPolisi 
            GROUP BY SVNo, BrgNama 
            UNION
            SELECT 
              ttsdhd.SDNo, SVNo, SVTgl, JasaNama, SDHrgJual
            FROM
              ttsdhd 
              INNER JOIN ttsditjasa 
                ON ttsditjasa.SDNo = ttsdhd.SDNo 
              INNER JOIN tdjasa 
                ON tdjasa.JasaKode = ttsditjasa.JasaKode 
            WHERE MotorNoPolisi = :MotorNoPolisi
            GROUP BY SVNo, JasaNama ) A ORDER BY SDNo", [ ':MotorNoPolisi' => $MotorNoPolisi ] )->queryAll();
		return [
			'rows' => $dt
		];
	}
	public function actionMekanikOtomatis( $SDNo ) {
		$this->layout = 'popup-form';
		return $this->render( '_formMekanik', [ 'SDNo' => $SDNo ] );
	}
	public function actionMekanikOtomatisItem( $SDNo ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$dt                          = General::cCmd( "SELECT        SDNo, KarKode, Nomor, DATE_FORMAT(`Start`,'%d/%m/%Y %T') AS `Start`,
		DATE_FORMAT(`Finish`,'%d/%m/%Y %T') AS  `Finish`, `Status`
		FROM            ttsdittime
		WHERE        (SDNo = :SDNo)
		ORDER BY Nomor", [ ':SDNo' => $SDNo ] )->queryAll();
		return [
			'rows' => $dt
		];
	}
	public function actionStatusOtomatis() {
		$this->layout = 'popup-form';
		return $this->render( '_formStatus', [ 'data' => $_GET ] );
	}
	public function actionStatusOtomatisItem( $SDNo, $SVNo ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$dt                          = General::cCmd( "SELECT KBMNo, KBMTgl, KBMLink, KBMBayar, Kode, Person, Memo, NoGL FROM vtKBM WHERE (KBMLink = :SVNo ) 
		UNION 
		SELECT KBKNo AS KBMNo, KBKTgl AS KBMTgl, KBKLink AS KBMLink, KBKBayar AS KBMBayar, Kode, Person, Memo, NoGL FROM vtKBK WHERE KBKLink = :SDNo
		ORDER BY KBMTgl;", [ ':SVNo' => $SVNo, ':SDNo' => $SDNo ] )->queryAll();
		return [
			'rows' => $dt
		];
	}
	public function actionSelectInvoiceServis() {
		$this->layout = 'select-mode';
		return $this->render( 'InvoiceServisSelect', [
			'mode'    => self::MODE_SELECT_MULTISELECT,
			'colGrid' => Ttsdhd::colGridInvoiceServisSelect()
		] );
	}
	public function actionKonfirm() {
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Konfirm';
		$result           = Ttsdhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( [ 'ttsdhd/invoice-servis-update', 'action' => 'update', 'id' => base64_encode( $result[ 'SDNo' ] ) ] );
	}
    private function jurnal($Jenis) {
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Jurnal';
		$result = Ttsdhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( Ttsdhd::getRoute( $Jenis, [ 'id' => base64_encode( $result[ 'SDNo' ] ), 'action' => 'display' ] )[ 'update' ] );
	}

	public function actionGetJasaBarang() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$jasa                        = [];
		$barang                      = [];
		switch ( $requestData[ 'SDTC' ] ) {
			case 'TC14' :
				$jasa = General::cCmd(
					"SELECT JasaKode, JasaNama, JasaGroup, JasaHrgBeli, JasaHrgJual, JasaWaktuKerja, JasaWaktuSatuan, JasaWaktuKerjaMenit 
						FROM tdjasa where JasaStatus = 'A' AND LEFT(JasaGroup,3) = 'ASS' Order By JasaStatus, JasaKode" )->queryAll();
				$barang = General::cCmd(
					"SELECT tdbarang.BrgKode, tdbarang.BrgNama, tdbarang.BrgBarCode, tdbarang.BrgGroup, tdbarang.BrgSatuan, tdbarang.BrgHrgBeli, tdbarang.BrgHrgJual, tdbarang.BrgRak1, tdbarang.BrgMinStock, tdbarang.BrgMaxStock, tdbarang.BrgStatus
						FROM tdbarang
						INNER JOIN tdbaranglokasi ON tdbaranglokasi.BrgKode = tdbarang.BrgKode
						WHERE LokasiKode NOT IN ('Retur', 'Klaim', 'Kosong', 'Retur', 'Robbing', 'Rusak', 'Penerimaan1', 'Penerimaan2')
						AND  tdbarang.BrgGroup = 'Oli'
						GROUP BY tdbarang.BrgKode
						HAVING SUM(SaldoQty) > 0
						ORDER BY BrgStatus, BrgKode" )->queryAll();
				break;
			case 'TC11' :
				$jasa   = General::cCmd(
					"SELECT JasaKode, JasaNama, JasaGroup, JasaHrgBeli, JasaHrgJual, JasaWaktuKerja, JasaWaktuSatuan, JasaWaktuKerjaMenit 
				FROM tdjasa where JasaStatus = 'A' AND LEFT(JasaGroup,3) <> 'ASS' AND JasaGroup <> 'CLAIMC2' 
				Order By JasaStatus, JasaKode" )->queryAll();
				$barang = General::cCmd(
					"SELECT tdbarang.BrgKode, tdbarang.BrgNama, tdbarang.BrgBarCode, tdbarang.BrgGroup, tdbarang.BrgSatuan, tdbarang.BrgHrgBeli, tdbarang.BrgHrgJual, tdbarang.BrgRak1, tdbarang.BrgMinStock, tdbarang.BrgMaxStock, tdbarang.BrgStatus
						FROM tdbarang
						INNER JOIN tdbaranglokasi ON tdbaranglokasi.BrgKode = tdbarang.BrgKode
						WHERE LokasiKode NOT IN ('Retur', 'Klaim', 'Kosong', 'Retur', 'Robbing', 'Rusak', 'Penerimaan1', 'Penerimaan2')
						GROUP BY tdbarang.BrgKode
						HAVING SUM(SaldoQty) > 0
						ORDER BY BrgStatus, BrgKode" )->queryAll();
				break;
			case 'TC13' :
				$jasa   = General::cCmd(
					"SELECT JasaKode, JasaNama, JasaGroup, JasaHrgBeli, JasaHrgJual, JasaWaktuKerja, JasaWaktuSatuan, JasaWaktuKerjaMenit 
						FROM tdjasa where JasaStatus = 'A' AND JasaGroup = 'CLAIMC2' Order By JasaStatus, JasaKode" )->queryAll();
				$barang = General::cCmd(
					"SELECT tdbarang.BrgKode, tdbarang.BrgNama, tdbarang.BrgBarCode, tdbarang.BrgGroup, tdbarang.BrgSatuan, tdbarang.BrgHrgBeli, tdbarang.BrgHrgJual, tdbarang.BrgRak1, tdbarang.BrgMinStock, tdbarang.BrgMaxStock, tdbarang.BrgStatus
						FROM tdbarang
						INNER JOIN tdbaranglokasi ON tdbaranglokasi.BrgKode = tdbarang.BrgKode
						WHERE LokasiKode NOT IN ('Retur', 'Klaim', 'Kosong', 'Retur', 'Robbing', 'Rusak', 'Penerimaan1', 'Penerimaan2')
						GROUP BY tdbarang.BrgKode
						HAVING SUM(SaldoQty) > 0
						ORDER BY BrgStatus, BrgKode" )->queryAll();
				break;
		}
		return [
			'jasa'   => $jasa,
			'barang' => $barang
		];
	}
}
