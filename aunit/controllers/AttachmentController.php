<?php

namespace aunit\controllers;

use aunit\components\AunitController;
use aunit\models\Attachment;
use Yii;
use OSS\Core\OssException;

class AttachmentController extends AunitController
{
    const MIME = [
        'pdf' => 'application/pdf',
        'xls' => 'application/vnd.ms-excel',
        'doc' => 'application/msword',
        'rtf' => 'application/rtf',
    ];

    public function actionIndex(){
        $id = Yii::$app->request->get('id');
        try {
            $oss = new Attachment();
            $oss->loadParams($id);
            $listFiles = $oss->getListAttachment();
            return $this->render('index', [ 
                'id' => $id, 'listFiles' => $listFiles, 'titleId' => $oss->titleId, 'redirectUrl' => $oss->redirectUrl
            ]);
        } catch (OssException $e) {
            return $this->redirect( 'aunit' );
        }
    }

    public function actionUpload(){
        if(Yii::$app->request->isPost){
            $id = Yii::$app->request->post('id');
            $img = Yii::$app->request->post('imgBase64');
            try {
                $oss = new Attachment();
                $oss->loadParams($id);
                $oss->upload($img);
            } catch (OssException $e) {
                return $this->responseFailed( \print_r($e->getMessage(), true) );
            }
            return $this->responseSuccess( 'berhasil' );
        }
    }
    public function actionDelete(){
        $result = '';
        if(Yii::$app->request->isPost){
            $id = Yii::$app->request->post('id');
            $basename = Yii::$app->request->post('basename');
            try {
                $oss = new Attachment();
                $oss->loadParams($id);
                $resultDelete = $oss->delete($basename);
                if($resultDelete['result'] !== true){
                    return $this->responseFailed($resultDelete['msg']);
                }
            } catch (OssException $e) {
                return $this->responseFailed( \print_r($e->getMessage(), true) );
            }
            return $this->responseSuccess( 'berhasil');
        }
    }

}