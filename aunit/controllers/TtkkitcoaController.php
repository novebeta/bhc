<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\models\Ttkkitcoa;
use Yii;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtkkitcoaController implements the CRUD actions for Ttkkitcoa model.
 */
class TtkkitcoaController extends AunitController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttkkitcoa models.
	 * @return mixed
	 */
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttkkitcoa::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
			/** @var Ttkkitcoa $model */
			$model = null;
			if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
				$model = $this->findModelBase64( 'Ttkkitcoa', $requestData[ 'id' ] );
			}
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'Action' ]  = 'Insert';
						$requestData[ 'KKAutoN' ] = 0;
					} else {
						$requestData[ 'Action' ] = 'Update';
						if ( $model != null ) {
							$requestData[ 'KKNoOLD' ]     = $model->KKNo;
							$requestData[ 'KKAutoNOLD' ]  = $model->KKAutoN;
							$requestData[ 'NoAccountOLD' ] = $model->NoAccount;
						}
					}
					$result = Ttkkitcoa::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'del'  :
					$requestData             = array_merge( $requestData, $model->attributes );
					$requestData[ 'Action' ] = 'Delete';
					if ( $model != null ) {
						$requestData[ 'KKNoOLD' ]     = $model->KKNo;
						$requestData[ 'KKAutoNOLD' ]  = $model->KKAutoN;
						$requestData[ 'NoAccountOLD' ] = $model->NoAccount;
					}
					$result = Ttkkitcoa::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
			}
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'Ttkkitcoa.' . $primaryKeys[ $i ];
			}
			$query     = ( new \yii\db\Query() )
				->select( new Expression(
					'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
								ttkkitcoa.KKNo, ttkkitcoa.KKAutoN, ttkkitcoa.NoAccount, ttkkitcoa.KKBayar, ttkkitcoa.KKKeterangan, traccount.NamaAccount'
				) )
				->from( 'Ttkkitcoa' )
				->join( 'INNER JOIN', 'traccount', 'ttkkitcoa.NoAccount = traccount.NoAccount' )
				->where( [ 'ttkkitcoa.KKNo' => $requestData[ 'KKNo' ] ] )
				->orderBy( 'ttkkitcoa.KKAutoN' );
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	/**
	 * Displays a single Ttkkitcoa model.
	 *
	 * @param string $KKNo
	 * @param integer $KKAutoN
	 * @param string $NoAccount
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $KKNo, $KKAutoN, $NoAccount ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $KKNo, $KKAutoN, $NoAccount ),
		] );
	}
	/**
	 * Creates a new Ttkkitcoa model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Ttkkitcoa();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'KKNo' => $model->KKNo, 'KKAutoN' => $model->KKAutoN, 'NoAccount' => $model->NoAccount ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Ttkkitcoa model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $KKNo
	 * @param integer $KKAutoN
	 * @param string $NoAccount
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $KKNo, $KKAutoN, $NoAccount ) {
		$model = $this->findModel( $KKNo, $KKAutoN, $NoAccount );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'KKNo' => $model->KKNo, 'KKAutoN' => $model->KKAutoN, 'NoAccount' => $model->NoAccount ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Ttkkitcoa model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $KKNo
	 * @param integer $KKAutoN
	 * @param string $NoAccount
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $KKNo, $KKAutoN, $NoAccount ) {
		$this->findModel( $KKNo, $KKAutoN, $NoAccount )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Ttkkitcoa model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $KKNo
	 * @param integer $KKAutoN
	 * @param string $NoAccount
	 *
	 * @return Ttkkitcoa the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $KKNo, $KKAutoN, $NoAccount ) {
		if ( ( $model = Ttkkitcoa::findOne( [ 'KKNo' => $KKNo, 'KKAutoN' => $KKAutoN, 'NoAccount' => $NoAccount ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
