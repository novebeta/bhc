<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\models\Ttbmhd;
use aunit\models\Ttbmitpm;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtbmitpmController implements the CRUD actions for Ttbmitpm model.
 */
class TtbmitpmController extends AunitController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttbmitpm models.
	 * @return mixed
	 */
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttbmitpm::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			/** @var Ttbmitpm $model */
			$model = null;
			if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
				$model = $this->findModelBase64( 'ttbmitpm', $requestData[ 'id' ] );
			}
			$oper = $requestData[ 'oper' ];
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'Action' ] = 'Insert';
					} else {
						$requestData[ 'Action' ] = 'Update';
						if ( $model != null ) {
							$requestData[ 'BMNoOLD' ]    = $model->BMNo;
							$requestData[ 'DKNoOLD' ]    = $model->DKNo;
							$requestData[ 'BMJenisOLD' ] = $model->BMJenis;
						}
					}
					$result = Ttbmitpm::find()->callSP( $requestData );
					$base64 = urldecode( base64_decode( $requestData[ 'header' ] ) );
					parse_str( $base64, $header );
					$header[ 'Action' ] = 'LoopAfter';
					$resultHeader       = Ttbmhd::find()->callSP( $header );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'batch' :
					\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
					$respon                      = $this->responseFailed( 'Add Item' );
					$base64                      = urldecode( base64_decode( $requestData[ 'header' ] ) );
					parse_str( $base64, $header );
//					$header                      = $requestData[ 'header' ];
					$header[ 'Action' ]          = 'Loop';
					$result                      = Ttbmhd::find()->callSP( $header );
					if ( $result[ 'status' ] != 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					foreach ( $requestData[ 'data' ] as $item ) {
						$items[ 'Action' ]  = 'Insert';
						$items[ 'BMNo' ]    = $header[ 'BMNo' ];
						$items[ 'DKNo' ]    = $item[ 'data' ][ 'DKNo' ];
						$items[ 'BMJenis' ] = $item[ 'data' ][ 'Jenis' ];
						$items[ 'BMBayar' ] = $item[ 'data' ][ 'BMBayar' ];
						$result             = Ttbmitpm::find()->callSP( $items );
						if ( $result[ 'status' ] != 0 && $result[ 'status' ] !== null ) {
							$response = $this->responseFailed( $result[ 'keterangan' ] );
						}
					}
					$header[ 'Action' ] = 'LoopAfter';
					$resultHeader       = Ttbmhd::find()->callSP( $header );
					if ( $resultHeader[ 'status' ] != 0 && $resultHeader[ 'status' ] !== null ) {
						$response = $this->responseFailed( $resultHeader[ 'keterangan' ] );
					}
					return $response;
					break;
				case 'del'  :
					$requestData[ 'Action' ] = 'Delete';
					$result                  = Ttbmitpm::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
			}
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'Ttbmitpm.' . $primaryKeys[ $i ];
			}
			/** @var Ttbmhd $model */
			// $model = Ttbmhd::find()->where( [ 'BMNo' => $requestData[ 'BMNo' ] ] )->one();
			$requestData[ 'Action' ] = 'Load';
			$result = Ttbmitpm::find()->callSP( $requestData );
			// $query = new Query();
			// $query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
            //             ttbmitpm.BMNo, ttbmitpm.DKNo, ttdk.DKTgl, ttbmitpm.BMJenis, tdcustomer.CusNama, tmotor.MotorType, tmotor.MotorWarna, ttdk.LeaseKode, ttdk.DKJenis,
            //              (CASE ttbmitpm.BMJenis 
            //                 WHEN "Leasing" THEN (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) 
            //                 WHEN "Piutang Sisa" THEN (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) 
            //                 WHEN "Tunai" THEN (ttdk.DKDPTerima + ttdk.JADKDPTerima)
            //                 WHEN "Kredit" THEN (ttdk.DKDPTerima + ttdk.JADKDPTerima)
            //                 WHEN "Piutang UM" THEN (ttdk.DKDPLLeasing+JADKDPLLeasing)
            //                 WHEN "Inden" THEN ttdk.DKDPInden
            //                 WHEN "Retur Harga" THEN (ttdk.ReturHarga+ttdk.JAReturHarga)
            //                 WHEN "SubsidiDealer2" THEN (ttdk.PotonganHarga+ttdk.JAPotonganHarga)
            //                 WHEN "Insentif Sales" THEN (ttdk.InsentifSales+ttdk.JAInsentifSales)
            //                 WHEN "Potongan Khusus" THEN (ttdk.PotonganKhusus+ttdk.JAPotonganKhusus)
            //              END) AS Awal, 
            //              BMPaid.Paid - ttbmitpm.BMBayar AS Terbayar, ttbmitpm.BMBayar,  
            //              (CASE ttbmitpm.BMJenis 
            //                 WHEN "Leasing" THEN (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) 
            //                 WHEN "Piutang Sisa" THEN (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) 
            //                 WHEN "Tunai" THEN (ttdk.DKDPTerima + ttdk.JADKDPTerima)
            //                 WHEN "Kredit" THEN (ttdk.DKDPTerima + ttdk.JADKDPTerima) 
            //                 WHEN "Piutang UM" THEN (ttdk.DKDPLLeasing+JADKDPLLeasing)
            //                 WHEN "Inden" THEN ttdk.DKDPInden
            //                 WHEN "Retur Harga" THEN (ttdk.ReturHarga+ttdk.JAReturHarga)
            //                 WHEN "SubsidiDealer2" THEN (ttdk.PotonganHarga+ttdk.JAPotonganHarga)
            //                 WHEN "Insentif Sales" THEN (ttdk.InsentifSales+ttdk.JAInsentifSales)
            //                 WHEN "Potongan Khusus" THEN (ttdk.PotonganKhusus+ttdk.JAPotonganKhusus)
            //              END) - BMPaid.Paid AS Sisa ' ) )
			//       ->from( 'ttbmitpm' )
			//       ->innerJoin( "ttdk", 'ttbmitpm.DKNo = ttdk.DKNo' )
			//       ->innerJoin( "tmotor", 'ttbmitpm.DKNo = tmotor.DKNo' )
			//       ->innerJoin( "tdcustomer", 'ttdk.CusKode = tdcustomer.CusKode' )
			//       ->innerJoin( "ttbmhd", 'ttbmitpm.BMNo = ttbmhd.BMNo' )
			//       ->innerJoin( "(SELECT KMLINK AS DKNo, KMJenis AS Jenis, IFNULL(SUM(KMNominal), 0) AS Paid FROM vtkm GROUP BY KMLINK, KMJenis UNION SELECT KKLINK AS DKNo, KKJenis AS Jenis,
            //              IFNULL(SUM(KKNominal), 0) AS Paid FROM vtkk GROUP BY KKLINK, KKJenis) BMPaid 
            //                   ON ttbmitpm.DKNo = BMPaid.DKNo AND ttbmitpm.BMJenis = BMPaid.Jenis  WHERE ttbmitpm.BMNo = '$model->BMNo'" );
			// $rowsCount = $query->count();
			// $rows      = $query->all();
			/* encode row id */
			// for ( $i = 0; $i < count( $rows ); $i ++ ) {
			// 	$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			// }
			// $response = [
			// 	'rows'      => $rows,
			// 	'rowsCount' => $rowsCount
			// ];
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				$response = $this->responseSuccess( $result[ 'keterangan' ],$result );
			} else {
				$response = $this->responseFailed( $result[ 'keterangan' ] );
			}
		}
		return $response;
	}
	/**
	 * Displays a single Ttbmitpm model.
	 *
	 * @param string $BMNo
	 * @param string $DKNo
	 * @param string $BMJenis
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $BMNo, $DKNo, $BMJenis ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $BMNo, $DKNo, $BMJenis ),
		] );
	}
	/**
	 * Creates a new Ttbmitpm model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Ttbmitpm();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'BMNo' => $model->BMNo, 'DKNo' => $model->DKNo, 'BMJenis' => $model->BMJenis ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Ttbmitpm model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $BMNo
	 * @param string $DKNo
	 * @param string $BMJenis
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $BMNo, $DKNo, $BMJenis ) {
		$model = $this->findModel( $BMNo, $DKNo, $BMJenis );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'BMNo' => $model->BMNo, 'DKNo' => $model->DKNo, 'BMJenis' => $model->BMJenis ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Ttbmitpm model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $BMNo
	 * @param string $DKNo
	 * @param string $BMJenis
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $BMNo, $DKNo, $BMJenis ) {
		$this->findModel( $BMNo, $DKNo, $BMJenis )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Ttbmitpm model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $BMNo
	 * @param string $DKNo
	 * @param string $BMJenis
	 *
	 * @return Ttbmitpm the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $BMNo, $DKNo, $BMJenis ) {
		if ( ( $model = Ttbmitpm::findOne( [ 'BMNo' => $BMNo, 'DKNo' => $DKNo, 'BMJenis' => $BMJenis ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
