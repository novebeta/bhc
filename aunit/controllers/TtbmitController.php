<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\models\Ttbmhd;
use aunit\models\Ttbmit;
use common\components\General;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtbmitController implements the CRUD actions for Ttbmit model.
 */
class TtbmitController extends AunitController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttbmit::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
			/** @var Ttbmit $model */
			$model = null;
			if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
				$model = $this->findModelBase64( 'ttbmit', $requestData[ 'id' ] );
			}
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					$requestData[ 'BMBayar' ] = $requestData[ 'BMBayar' ] ?? $requestData[ 'Bayar' ];
					if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'Action' ] = 'Insert';
						$requestData[ 'DKNo' ]   = '--';
					} else {
						$requestData[ 'Action' ] = 'Update';
						if ( $model != null ) {
							$requestData[ 'BMNoOLD' ] = $model->BMNo;
							$requestData[ 'DKNoOLD' ] = $model->DKNo;
						}
					}
					$result = Ttbmit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'batch' :
					\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
					$respon                      = $this->responseFailed( 'Add Item' );
					$base64                      = urldecode( base64_decode( $requestData[ 'header' ] ) );
					parse_str( $base64, $header );
//					$header                      = $requestData[ 'header' ];
					$header[ 'Action' ] = 'Loop';
					$header[ 'BMJam' ]  = $header[ 'BMTgl' ] . ' ' . $header[ 'BMJam' ];
					$result             = Ttbmhd::find()->callSP( $header );
					$BMNominal          = 0;
					foreach ( $requestData[ 'data' ] as $item ) {
						$items[ 'Action' ] = 'Insert';
						$items[ 'BMNo' ]   = $header[ 'BMNo' ];
						$items[ 'DKNo' ]   = $item[ 'id' ];
						if ( isset( $item[ 'data' ][ 'KKJenis' ] ) && $item[ 'data' ][ 'KKJenis' ] == 'KK Ke Bank' ) {
							$BMBayar = floatval( $item[ 'data' ][ 'KKNominal' ] );
						} else {
							$BMBayar = floatval( $item[ 'data' ][ 'BMBayar' ] );
						}
						$items[ 'BMBayar' ] = $BMBayar;
						$BMNominal          += $BMBayar;
						$result             = Ttbmit::find()->callSP( $items );
					}
					$header[ 'Action' ]    = 'LoopAfter';
					$header[ 'BMNominal' ] = $BMNominal;
					$result                = Ttbmhd::find()->callSP( $header );
					return $this->responseSuccess( $requestData );
					break;
				case 'del'  :
					$requestData             = array_merge( $requestData, $model->attributes );
					$requestData[ 'Action' ] = 'Delete';
					if ( $model != null ) {
						$requestData[ 'BMNoOLD' ] = $model->BMNo;
						$requestData[ 'DKNoOLD' ] = $model->DKNo;
					}
					$result = Ttbmit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
			}
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'Ttbmit.' . $primaryKeys[ $i ];
			}
			/** @var Ttbmhd $model */
			$model = Ttbmhd::find()->where( [ 'BMNo' => $requestData[ 'BMNo' ] ] )->one();
			$query = new Query();
			switch ( $model->BMJenis ) {
				case 'Leasing' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
                            ttbmit.*, BMPaid.Paid - ttbmit.BMBayar AS Terbayar,ttbmit.BMBayar AS Bayar,ttbmit.DKNetto - BMPaid.Paid AS Sisa,
                            ttbmit.DKHarga - ttbmit.ProgramSubsidi AS DKTunai,ttbmit.DKHarga - ttbmit.ProgramSubsidi - BMPaid.Paid AS SisaTunai' ) )
					      ->from( new Expression( "(
                            SELECT ttbmit.BMNo,ttbmit.DKNo, ttdk.DKTgl,ttdk.CusKode,                    
                            IF (TRIM(tdcustomer.CusNama) <> TRIM(tdcustomer.CusPembeliNama) AND tdcustomer.CusPembeliNama <> '--'AND tdcustomer.CusPembeliNama <> '',
                            CONCAT_WS(' qq ',tdcustomer.CusNama,tdcustomer.CusPembeliNama),tdcustomer.CusNama) AS CusNama,
                            CONCAT_WS('',tdcustomer.CusAlamat,'  RT/RW : ',tdcustomer.CusRT,'/',tdcustomer.CusRW) AS CusAlamat,
                            tmotor.MotorType,tmotor.MotorNoMesin,ttdk.LeaseKode,ttbmit.BMBayar,
                            (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) AS DKNetto,
                            (ttdk.DKHarga + ttdk.JADKHarga) AS DKHarga,
                            (ttdk.ProgramSubsidi + ttdk.JAPrgSubsSupplier + ttdk.JAPrgSubsDealer + ttdk.JAPrgSubsFincoy) AS ProgramSubsidi
                            FROM ttbmit
                            INNER JOIN ttdk ON ttbmit.DKNo = ttdk.DKNo
                            INNER JOIN tmotor ON ttbmit.DKNo = tmotor.DKNo
                            INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
                            INNER JOIN ttbmhd ON ttbmit.BMNo = ttbmhd.BMNo  WHERE (ttbmit.BMNo = :BMNo )
                            ORDER BY ttbmit.DKNo ) ttbmit" ) )
					      ->addParams( [ ':BMNo' => $model->BMNo ] )
					      ->innerJoin( "( SELECT KMLINK AS DKNo, IFNULL(SUM(KMNominal), 0) AS Paid
                            FROM vtkm WHERE (KMJenis = 'Leasing') GROUP BY KMLINK) BMPaid ON ttbmit.DKNo = BMPaid.DKNo" );
					break;
				case 'Piutang Sisa' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,ttbmit.*, BMPaid.Paid - ttbmit.BMBayar AS Terbayar, ttbmit.BMBayar AS Bayar, ttbmit.DKNetto - BMPaid.Paid AS Sisa, 
                        ttbmit.DKHarga - ttbmit.ProgramSubsidi AS DKTunai, ttbmit.DKHarga - ttbmit.ProgramSubsidi - BMPaid.Paid AS SisaTunai ' ) )
					      ->from( new Expression( "(               
                               SELECT ttbmit.BMNo, ttbmit.DKNo, ttdk.DKTgl, ttdk.CusKode, tdcustomer.CusNama, CONCAT_WS('', tdcustomer.CusAlamat, '  RT/RW : ', tdcustomer.CusRT, '/',tdcustomer.CusRW) AS CusAlamat, 
                               tmotor.MotorType, tmotor.MotorNoMesin, ttdk.LeaseKode, ttbmit.BMBayar, 
                               (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) AS DKNetto, 
                               (ttdk.DKHarga+ttdk.JADKHarga) AS DKHarga, 
                               (ttdk.ProgramSubsidi + ttdk.JAPrgSubsSupplier + ttdk.JAPrgSubsDealer + ttdk.JAPrgSubsFincoy) AS ProgramSubsidi
                               FROM  ttbmit 
                               INNER JOIN ttdk ON ttbmit.DKNo = ttdk.DKNo 
                               INNER JOIN tmotor ON ttbmit.DKNo = tmotor.DKNo 
                               INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode WHERE (ttbmit.BMNo = :BMNo ) ORDER BY ttbmit.DKNo) ttbmit " ) )
					      ->addParams( [ ':BMNo' => $model->BMNo ] )
					      ->innerJoin( "(SELECT KMLINK AS DKNo, IFNULL(SUM(KMNominal), 0) AS Paid FROM vtkm WHERE (KMJenis = 'Piutang Sisa') GROUP BY KMLINK) BMPaid ON ttbmit.DKNo = BMPaid.DKNo" );
					break;
				case 'Tunai' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
                    ttbmit.*, BMPaid.Paid - ttbmit.BMBayar AS Terbayar, ttbmit.BMBayar AS Bayar, ttbmit.DKDPTerima - BMPaid.Paid AS Sisa, ttbmit.DKHarga - ttbmit.ProgramSubsidi AS DKTunai, 
                    ttbmit.DKHarga - ttbmit.ProgramSubsidi - BMPaid.Paid AS SisaTunai ' ) )
					      ->from( new Expression( "(
                           SELECT ttbmit.BMNo, ttbmit.DKNo, ttdk.DKTgl, ttdk.CusKode, tdcustomer.CusNama, CONCAT_WS('', tdcustomer.CusAlamat, '  RT/RW : ', tdcustomer.CusRT, '/', 
                           tdcustomer.CusRW) AS CusAlamat, tmotor.MotorType, tmotor.MotorNoMesin, ttdk.LeaseKode, ttbmit.BMBayar, 
                           (ttdk.DKDPTerima + ttdk.JADKDPTerima) AS DKNetto, 
                           (ttdk.DKDPTerima + ttdk.JADKDPTerima) AS DKDPTerima,
                           (ttdk.DKHarga+ttdk.JADKHarga) AS DKHarga, 
                           (ttdk.ProgramSubsidi + ttdk.JAPrgSubsSupplier + ttdk.JAPrgSubsDealer + ttdk.JAPrgSubsFincoy) AS ProgramSubsidi
                           FROM  ttbmit 
                           INNER JOIN ttdk ON ttbmit.DKNo = ttdk.DKNo 
                           INNER JOIN tmotor ON ttbmit.DKNo = tmotor.DKNo 
                           INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode  WHERE (ttbmit.BMNo = :BMNo ) ORDER BY ttbmit.DKNo ) ttbmit" ) )
					      ->addParams( [ ':BMNo' => $model->BMNo ] )
					      ->innerJoin( "(SELECT KMLINK AS DKNo, IFNULL(SUM(KMNominal), 0) AS Paid FROM vtkm WHERE (KMJenis = 'Tunai') GROUP BY KMLINK) BMPaid ON ttbmit.DKNo = BMPaid.DKNo" );
					break;
				case 'Kredit' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
                    ttbmit.*, BMPaid.Paid - ttbmit.BMBayar AS Terbayar, ttbmit.BMBayar AS Bayar, ttbmit.DKDPTerima - BMPaid.Paid AS Sisa, ttbmit.DKHarga - ttbmit.ProgramSubsidi AS DKTunai, 
                    ttbmit.DKHarga - ttbmit.ProgramSubsidi - BMPaid.Paid AS SisaTunai ' ) )
					      ->from( new Expression( "(               
                           SELECT ttbmit.BMNo, ttbmit.DKNo, ttdk.DKTgl, ttdk.CusKode, tdcustomer.CusNama, CONCAT_WS('', tdcustomer.CusAlamat, '  RT/RW : ', tdcustomer.CusRT, '/', 
                           tdcustomer.CusRW) AS CusAlamat, tmotor.MotorType, tmotor.MotorNoMesin, ttdk.LeaseKode, ttbmit.BMBayar, 
                           (ttdk.DKDPTerima + ttdk.JADKDPTerima) AS DKNetto, 
                           (ttdk.DKDPTerima + ttdk.JADKDPTerima) AS DKDPTerima,
                           (ttdk.DKHarga+ttdk.JADKHarga) AS DKHarga, 
                           (ttdk.ProgramSubsidi + ttdk.JAPrgSubsSupplier + ttdk.JAPrgSubsDealer + ttdk.JAPrgSubsFincoy) AS ProgramSubsidi 
                           FROM  ttbmit 
                           INNER JOIN ttdk ON ttbmit.DKNo = ttdk.DKNo 
                           INNER JOIN tmotor ON ttbmit.DKNo = tmotor.DKNo 
                           INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode  WHERE (ttbmit.BMNo = :BMNo ) ORDER BY ttbmit.DKNo ) ttbmit" ) )
					      ->addParams( [ ':BMNo' => $model->BMNo ] )
					      ->innerJoin( "(SELECT KMLINK AS DKNo, IFNULL(SUM(KMNominal), 0) AS Paid FROM vtkm WHERE (KMJenis = 'Kredit') GROUP BY KMLINK) BMPaid ON ttbmit.DKNo = BMPaid.DKNo" );
					break;
				case 'Piutang UM' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
                    ttbmit.*, BMPaid.Paid - BMBayar AS Terbayar, ttbmit.BMBayar AS Bayar, DKDPLLeasing - BMPaid.Paid AS Sisa, DKHarga - ProgramSubsidi AS DKTunai, 
                    DKHarga - ProgramSubsidi - BMPaid.Paid AS SisaTunai' ) )
					      ->from( new Expression( "(
                           SELECT ttbmit.BMNo, ttbmit.DKNo, ttdk.DKTgl, ttdk.CusKode, tdcustomer.CusNama, 
                           CONCAT_WS('', tdcustomer.CusAlamat, '  RT/RW : ', tdcustomer.CusRT, '/',tdcustomer.CusRW) AS CusAlamat, 
                           tmotor.MotorType, tmotor.MotorNoMesin, ttdk.LeaseKode, ttbmit.BMBayar,
                           (ttdk.DKDPLLeasing+JADKDPLLeasing) AS DKNetto, 
                           (ttdk.DKDPLLeasing+JADKDPLLeasing) AS DKDPLLeasing, 
                           (ttdk.DKHarga+ttdk.JADKHarga) AS DKHarga,
                           (ttdk.ProgramSubsidi + ttdk.JAPrgSubsSupplier + ttdk.JAPrgSubsDealer + ttdk.JAPrgSubsFincoy) AS ProgramSubsidi
                           FROM  ttbmit 
                           INNER JOIN ttdk ON ttbmit.DKNo = ttdk.DKNo 
                           INNER JOIN tmotor ON ttbmit.DKNo = tmotor.DKNo 
                           INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode  WHERE (ttbmit.BMNo = :BMNo ) ORDER BY ttbmit.DKNo ) ttbmit" ) )
					      ->addParams( [ ':BMNo' => $model->BMNo ] )
					      ->innerJoin( "(SELECT KMLINK AS DKNo, IFNULL(SUM(KMNominal), 0) AS Paid FROM vtkm WHERE (KMJenis = 'Piutang UM') GROUP BY KMLINK) BMPaid ON ttbmit.DKNo = BMPaid.DKNo" );
					break;
				case 'Inden' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,ttbmit.*, IFNULL(BMPaid.Paid,0) Terbayar, ttbmit.INDP-IFNULL(BMPaid.Paid,0) AS Sisa, ttbmit.BMBayar AS Bayar' ) )
					      ->from( new Expression( "(
                           SELECT ttbmit.BMNo, ttbmit.DKNo, ttin.INTgl AS DKTgl, ttin.CusKode, tdcustomer.CusNama,
                           ttin.MotorType, '--'  AS MotorNoMesin, ttin.INDP AS DKNetto, ttin.INDP, ttbmit.BMBayar
                           FROM  ttbmit 
                           INNER JOIN ttin ON ttbmit.DKNo = ttin.INNo 
                           INNER JOIN tdcustomer ON ttin.CusKode = tdcustomer.CusKode  WHERE (ttbmit.BMNo = :BMNo ) ORDER BY ttbmit.DKNo ) ttbmit" ) )
					      ->addParams( [ ':BMNo' => $model->BMNo ] )
					      ->innerJoin( "(SELECT KMLINK AS DKNo, IFNULL(SUM(KMNominal), 0) AS Paid FROM vtkm WHERE (KMJenis = 'Inden') GROUP BY KMLINK) BMPaid ON ttbmit.DKNo = BMPaid.DKNo" );
					break;
				case 'BBN Plus' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
                    BMNo,ttbmit.DKNo, ttbmit.DKTgl, ttbmit.CusKode, ttbmit.BBNPlus AS DKNetto, ttbmit.CusNama, ttbmit.cusalamat, ttbmit.MotorType, ttbmit.MotorNoMEsin, 
                    BBNPlus AS DKNetto, BMPaid.Paid - ttbmit.BMBayar AS Terbayar, ttbmit.BMBayar AS Bayar, ttbmit.BBNPlus - BMPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(               
                           SELECT ttbmit.BMNo, ttbmit.DKNo, ttdk.DKTgl, ttdk.CusKode, 
                           IF(TRIM(tdcustomer.CusNama)<>TRIM(tdcustomer.CusPembeliNama) AND tdcustomer.CusPembeliNama <>'--' AND tdcustomer.CusPembeliNama <>'',CONCAT_WS(' qq ',tdcustomer.CusNama,tdcustomer.CusPembeliNama),tdcustomer.CusNama) AS CusNama, 
                           CONCAT_WS('', tdcustomer.CusAlamat, '  RT/RW : ', tdcustomer.CusRT, '/',tdcustomer.CusRW) AS CusAlamat, 
                           tmotor.MotorType, tmotor.MotorNoMesin, ttdk.LeaseKode, ttbmit.BMBayar, ttdk.BBNPlus
                           FROM  ttbmit 
                           INNER JOIN ttdk ON ttbmit.DKNo = ttdk.DKNo 
                           INNER JOIN tmotor ON ttbmit.DKNo = tmotor.DKNo 
                           INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                           INNER JOIN ttbmhd ON ttbmit.BMNo = ttbmhd.BMNo  WHERE (ttbmit.BMNo = :BMNo ) ORDER BY ttbmit.DKNo) ttbmit " ) )
					      ->addParams( [ ':BMNo' => $model->BMNo ] )
					      ->innerJoin( "(SELECT KMLINK AS DKNo, IFNULL(SUM(KMNominal), 0) AS Paid FROM vtkm WHERE (KMJenis = 'BBN Plus') GROUP BY KMLINK) BMPaid ON ttbmit.DKNo = BMPaid.DKNo" );
					break;
				case 'Scheme' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
                    BMNo,ttbmit.DKNo, ttbmit.DKTgl, ttbmit.CusKode, ttbmit.Scheme AS DKNetto, ttbmit.CusNama, ttbmit.cusalamat, ttbmit.MotorType, ttbmit.MotorNoMEsin, 
                    BMPaid.Paid - ttbmit.BMBayar AS Terbayar, ttbmit.BMBayar AS Bayar, ttbmit.Scheme - BMPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(               
                           SELECT ttbmit.BMNo, ttbmit.DKNo, ttdk.DKTgl, ttdk.CusKode, 
                           IF(TRIM(tdcustomer.CusNama)<>TRIM(tdcustomer.CusPembeliNama) AND tdcustomer.CusPembeliNama <>'--' AND tdcustomer.CusPembeliNama <>'',CONCAT_WS(' qq ',tdcustomer.CusNama,tdcustomer.CusPembeliNama),tdcustomer.CusNama) AS CusNama, 
                           CONCAT_WS('', tdcustomer.CusAlamat, '  RT/RW : ', tdcustomer.CusRT, '/',tdcustomer.CusRW) AS CusAlamat, 
                           tmotor.MotorType, tmotor.MotorNoMesin, ttdk.LeaseKode, ttbmit.BMBayar, (ttdk.PrgSubsFincoy + ttdk.DKScheme) AS Scheme
                           FROM  ttbmit 
                           INNER JOIN ttdk ON ttbmit.DKNo = ttdk.DKNo 
                           INNER JOIN tmotor ON ttbmit.DKNo = tmotor.DKNo 
                           INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                           INNER JOIN ttbmhd ON ttbmit.BMNo = ttbmhd.BMNo   WHERE (ttbmit.BMNo = :BMNo ) ORDER BY ttbmit.DKNo) ttbmit" ) )
					      ->addParams( [ ':BMNo' => $model->BMNo ] )
					      ->innerJoin( "(SELECT KMLINK AS DKNo, IFNULL(SUM(KMNominal), 0) AS Paid FROM vtkm WHERE (KMJenis = 'Scheme') GROUP BY KMLINK) BMPaid ON ttbmit.DKNo = BMPaid.DKNo" );
					break;
				case 'BM Dari Kas' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
                    ttbmit.BMNo, ttbmit.DKNo, ttbmit.BMBayar AS Bayar, ttkk.KKTgl, ttkk.KKNominal, ttkk.KKPerson, ttkk.KKMemo, ttkk.KKJenis, ttkk.KKLink' ) )
					      ->from( 'ttbmhd' )
//                        ->addParams( [ ':BMNo' => $model->BMNo ] )
                          ->innerJoin( "ttbmit", 'ttbmhd.BMNo = ttbmit.BMNo' )
					      ->innerJoin( "ttkk", 'ttbmit.DKNo = ttkk.KKNo' )
					      ->where( "(ttbmit.BMNo = :BMNo) AND (ttbmhd.BMJenis LIKE :BMJenis)", [
						      ':BMNo'    => $model->BMNo,
						      ':BMJenis' => 'BM Dari Kas'
					      ] )
					      ->orderBy( 'ttbmit.DKNo' );
					break;
				case 'Penjualan Multi' :
					$comm      = General::cCmd( "SELECT CONCAT(" . implode( ',"||",', [ 'tmotor.MotorAutoN', 'tmotor.MotorNoMesin' ] ) . ") AS id,
						   ttbmitpm.BMNo, ttbmitpm.DKNo, ttdk.DKTgl, ttbmitpm.BMJenis, tdcustomer.CusNama, tmotor.MotorType, tmotor.MotorWarna, ttdk.LeaseKode, ttdk.DKJenis,
				         (CASE ttbmitpm.BMJenis 
				            WHEN 'Leasing' THEN (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) 
				            WHEN 'Piutang Sisa' THEN (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) 
				            WHEN 'Tunai' THEN (ttdk.DKDPTerima + ttdk.JADKDPTerima)
				            WHEN 'Kredit' THEN (ttdk.DKDPTerima + ttdk.JADKDPTerima)
				            WHEN 'Piutang UM' THEN (ttdk.DKDPLLeasing+JADKDPLLeasing)
				            WHEN 'Inden' THEN ttdk.DKDPInden
				            WHEN 'Retur Harga' THEN (ttdk.ReturHarga+ttdk.JAReturHarga)
				            WHEN 'SubsidiDealer2' THEN (ttdk.PotonganHarga+ttdk.JAPotonganHarga)
				            WHEN 'Insentif Sales' THEN (ttdk.InsentifSales+ttdk.JAInsentifSales)
				            WHEN 'Potongan Khusus' THEN (ttdk.PotonganKhusus+ttdk.JAPotonganKhusus)
				         END) AS Awal, 
				         BMPaid.Paid - ttbmitpm.BMBayar AS Terbayar, ttbmitpm.BMBayar,  
				         (CASE ttbmitpm.BMJenis 
				            WHEN 'Leasing' THEN (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) 
				            WHEN 'Piutang Sisa' THEN (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) 
				            WHEN 'Tunai' THEN (ttdk.DKDPTerima + ttdk.JADKDPTerima)
				            WHEN 'Kredit' THEN (ttdk.DKDPTerima + ttdk.JADKDPTerima) 
				            WHEN 'Piutang UM' THEN (ttdk.DKDPLLeasing+JADKDPLLeasing)
				            WHEN 'Inden' THEN ttdk.DKDPInden
				            WHEN 'Retur Harga' THEN (ttdk.ReturHarga+ttdk.JAReturHarga)
				            WHEN 'SubsidiDealer2' THEN (ttdk.PotonganHarga+ttdk.JAPotonganHarga)
				            WHEN 'Insentif Sales' THEN (ttdk.InsentifSales+ttdk.JAInsentifSales)
				            WHEN 'Potongan Khusus' THEN (ttdk.PotonganKhusus+ttdk.JAPotonganKhusus)
				         END) - BMPaid.Paid AS Sisa 
				         FROM ttbmitpm 
				         INNER JOIN ttdk ON ttbmitpm.DKNo = ttdk.DKNo 
				         INNER JOIN tmotor ON ttbmitpm.DKNo = tmotor.DKNo 
				         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
				         INNER JOIN ttbmhd ON ttbmitpm.BMNo = ttbmhd.BMNo 
				         INNER JOIN  (SELECT KMLINK AS DKNo, KMJenis AS Jenis, IFNULL(SUM(KMNominal), 0) AS Paid FROM vtkm GROUP BY KMLINK, KMJenis
				                      UNION
				                      SELECT KKLINK AS DKNo, KKJenis AS Jenis, IFNULL(SUM(KKNominal), 0) AS Paid FROM vtkk GROUP BY KKLINK, KKJenis
				                     )BMPaid 
				         ON ttbmitpm.DKNo = BMPaid.DKNo AND ttbmitpm.BMJenis = BMPaid.Jenis 
				         WHERE (ttbmitpm.BMNo = :BMNo ) ORDER BY ttbmitpm.DKNo", [
						':BMNo' => $requestData[ 'BMNo' ]
					] );
					$rowsArray = $comm->queryAll();
					for ( $i = 0; $i < count( $rowsArray ); $i ++ ) {
						$rowsArray[ $i ][ 'id' ] = base64_encode( $rowsArray[ $i ][ 'id' ] );
					}
					$response = [
						'rows'      => $rowsArray,
						'rowsCount' => sizeof( $rowsArray )
					];
					return $response;
					break;
			}
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	/**
	 * Displays a single Ttbmit model.
	 *
	 * @param string $BMNo
	 * @param string $DKNo
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $BMNo, $DKNo ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $BMNo, $DKNo ),
		] );
	}
	/**
	 * Creates a new Ttbmit model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Ttbmit();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'BMNo' => $model->BMNo, 'DKNo' => $model->DKNo ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Ttbmit model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $BMNo
	 * @param string $DKNo
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $BMNo, $DKNo ) {
		$model = $this->findModel( $BMNo, $DKNo );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'BMNo' => $model->BMNo, 'DKNo' => $model->DKNo ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Ttbmit model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $BMNo
	 * @param string $DKNo
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $BMNo, $DKNo ) {
		$this->findModel( $BMNo, $DKNo )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Ttbmit model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $BMNo
	 * @param string $DKNo
	 *
	 * @return Ttbmit the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $BMNo, $DKNo ) {
		if ( ( $model = Ttbmit::findOne( [ 'BMNo' => $BMNo, 'DKNo' => $DKNo ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
