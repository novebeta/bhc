<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Ttrk;
use common\components\General;
use GuzzleHttp\Client;
use Throwable;
use Yii;
use yii\base\UserException;
use yii\db\Exception;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
/**
 * TtrkController implements the CRUD actions for Ttrk model.
 */
class TtrkController extends AunitController {
	public function actions() {
		$colGrid  = null;
		$joinWith = [
			'motor'              => [ 'type' => 'INNER JOIN' ],
			'suratJalanKonsumen' => [ 'type' => 'INNER JOIN' ],
			'lokasi'             => [ 'type' => 'INNER JOIN' ],
			'dataKonsumen'       => [ 'type' => 'INNER JOIN' ],
			'konsumen'           => [ 'type' => 'INNER JOIN' ],
			'motorTypes'         => [ 'type' => 'INNER JOIN' ]
		];
		$join     = [];
        $where      = [
            [
                'op'        => 'AND',
                'condition' => "(ttrk.RKNo NOT LIKE '%=%')",
                'params'    => []
            ]
        ];
		return [
			'td' => [
				'class'    => TdAction::class,
				'model'    => Ttrk::class,
				'colGrid'  => $colGrid,
				'joinWith' => $joinWith,
                'where'     => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttrk models.
	 * @return mixed
	 */
	public function actionIndex() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttrk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'index' );
	}
	/**
	 * Finds the Ttrk model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttrk the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttrk::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttrk model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		TUi::$actionMode     = Menu::ADD;
		$model               = new Ttrk();
		$model->RKNo         = General::createTemporaryNo( "RK", 'ttrk', 'RKNo' );
		$model->RKTgl        = date( 'Y-m-d' );
		$model->RKJam        = date( 'Y-m-d H:i:s' );
		$model->RKMemo       = '--';
		$model->DKNo         = '--';
		$model->SKNo         = '--';
		$model->NoGL         = '--';
		$model->MotorNoMesin = '--';
		$model->LokasiKode   = $this->LokasiKode();
		$model->UserID       = $this->UserID();
		$model->save();
		$post                 = $model->attributes;
		$post[ 'Action' ]     = 'Insert';
		$result               = Ttrk::find()->callSP( $post );
		$dsJual               = $model::find()->dsTJualFillByNo()->where( [ 'ttrk.RKNo' => $model->RKNo ] )->asArray()->one();
		$dsJual[ 'RKNoView' ] = $result[ 'RKNo' ];
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$id = $this->generateIdBase64FromModel( $model );
		return $this->render( 'create', [
			'model'  => $model,
			'dsJual' => $dsJual,
			'id'     => $id,
		] );
	}
	/**
	 * Updates an existing Ttrk model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @param $action
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws Exception
	 */
	public function actionUpdate( $id, $action ) {
		$index = \yii\helpers\Url::toRoute( [ 'ttrk/index' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttrk $model */
		$model      = $this->findModelBase64( 'Ttrk', $id );
		$MySKNoAsal = $model->SKNo;
		$MyDKNoAsal = $model->DKNo;
		$dsJual     = $model::find()->dsTJualFillByNo()->where( [ 'ttrk.RKNo' => $model->RKNo ] )->asArray()->one();
		$id         = $this->generateIdBase64FromModel( $model );
		if ( Yii::$app->request->isPost ) {
			$post        = array_merge( $dsJual, Yii::$app->request->post() );
			$transaction = Yii::$app->db->beginTransaction();
			try {
				$post[ 'LokasiKodeRK' ] = $post[ 'LokasiKode' ];
				$post[ 'RKJam' ]        = $post[ 'RKTgl' ] . ' ' . $post[ 'RKJam' ];
				$post[ 'Action' ]       = 'Update';
				$result                 = Ttrk::find()->callSP( $post );
				Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
				$transaction->commit();
				$model = Ttrk::findOne( $result[ 'RKNo' ] );
				$id    = $this->generateIdBase64FromModel( $model );
				if ( $result[ 'status' ] == 0 ) {
					$post[ 'RKNoView' ] = $result[ 'RKNo' ];
					return $this->redirect( [ 'ttrk/update', 'action' => 'display', 'id' => $id ] );
				} else {
					return $this->render( 'update', [
						'model'  => $model,
						'dsJual' => $post,
						'id'     => $id,
					] );
				}
			} catch ( UserException $e ) {
				$transaction->rollBack();
				Yii::$app->session->setFlash( 'error', $e->getMessage() );
				return $this->render( 'update', [
					'model'  => $model,
					'dsJual' => $dsJual,
					'id'     => $id,
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsJual[ 'Action' ] = 'Load';
			$result             = Ttrk::find()->callSP( $dsJual );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'RKNo' ] = $dsJual[ 'RKNo' ];
		}
		$dsJual[ 'RKNoView' ] = $_GET[ 'RKNoView' ] ?? $result[ 'RKNo' ];
		return $this->render( 'update', [
			'model'  => $model,
			'dsJual' => $dsJual,
			'id'     => $id,
		] );
	}
	public function actionLoop() {
		$_POST[ 'Action' ] = 'Loop';
		$result            = Ttrk::find()->callSP( $_POST );
//		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        if($result[ 'status' ] == 1){
            Yii::$app->session->addFlash( 'warning', $result[ 'keterangan' ] );
        }
	}
	/**
	 * Deletes an existing Ttrk model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws Throwable
	 * @throws StaleObjectException
	 */
//	public function actionDelete() {
//		/** @var Ttrk $model */
//		$model             = $model = $this->findModelBase64( 'Ttrk', $_POST[ 'id' ] );
//		$_POST[ 'RKNo' ]   = $model->RKNo;
//		$_POST[ 'Action' ] = 'Delete';
//		$result            = Ttrk::find()->callSP( $_POST );
//		if ( $result[ 'status' ] == 0 ) {
//			return $this->responseSuccess( $result[ 'keterangan' ] );
//		} else {
//			return $this->responseFailed( $result[ 'keterangan' ] );
//		}
//	}

    public function actionDelete()
    {
        /** @var Ttrk $model */
        $request = Yii::$app->request;
        if ($request->isAjax){
            $model = $this->findModelBase64('Ttrk', $_POST['id']);
            $_POST['RKNo'] = $model->RKNo;
            $_POST['Action'] = 'Delete';
            $result = Ttrk::find()->callSP($_POST);
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            $id = $request->get('id');
            $model = $this->findModelBase64('Ttrk', $id);
            $_POST['RKNo'] = $model->RKNo;
            $_POST['Action'] = 'Delete';
            $result = Ttrk::find()->callSP($_POST);
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['RKNo'] == '--') {
                return $this->redirect(['index']);
            } else {
                $model = Ttrk::findOne($result['RKNo']);
                return $this->redirect(['ttrk/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
            }
        }
    }

	public function actionCancel( $id ) {
		/** @var Ttrk $model */
		$_POST[ 'Action' ] = 'Cancel';
		$_POST[ 'RKJam' ]  = $_POST[ 'RKTgl' ] . ' ' . $_POST[ 'RKJam' ];
		$result            = Ttrk::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'RKNo' ] == '--' ) {
			return $this->redirect( [ 'index' ] );
		} else {
			$model = Ttrk::findOne( $result[ 'RKNo' ] );
			return $this->redirect( [ 'ttrk/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST         = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client        = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response      = $client->post( Yii::$app->params[ 'host_report' ] . '/aunit/frk', [
			'body' => Json::encode( $_POST )
		] );
		$html          = $response->getBody();
		return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	public function actionJurnal() {
		$post             = Yii::$app->request->post();
		$post[ 'Action' ] = 'Jurnal';
		$post[ 'RKJam' ]  = $post[ 'RKTgl' ] . ' ' . $post[ 'RKJam' ];
		$result           = Ttrk::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( [ 'ttrk/update', 'action' => 'display', 'id' => base64_encode($result['RKNo']) ] );
	}
}
