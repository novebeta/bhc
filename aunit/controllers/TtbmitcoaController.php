<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\models\Ttbmitcoa;
use Yii;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtbmitcoaController implements the CRUD actions for Ttbmitcoa model.
 */
class TtbmitcoaController extends AunitController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttbmitcoa models.
	 * @return mixed
	 */
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttbmitcoa::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
			/** @var Ttbmitcoa $model */
			$model = null;
			if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
				$model = $this->findModelBase64( 'Ttbmitcoa', $requestData[ 'id' ] );
			}
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'Action' ]  = 'Insert';
						$requestData[ 'BMAutoN' ] = 0;
					} else {
						$requestData[ 'Action' ] = 'Update';
						if ( $model != null ) {
							$requestData[ 'BMNoOLD' ]     = $model->BMNo;
							$requestData[ 'BMAutoNOLD' ]  = $model->BMAutoN;
							$requestData[ 'NoAccountOLD' ] = $model->NoAccount;
						}
					}
					$result = Ttbmitcoa::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'del'  :
					$requestData             = array_merge( $requestData, $model->attributes );
					$requestData[ 'Action' ] = 'Delete';
					if ( $model != null ) {
						$requestData[ 'BMNoOLD' ]     = $model->BMNo;
						$requestData[ 'BMAutoNOLD' ]  = $model->BMAutoN;
						$requestData[ 'NoAccountOLD' ] = $model->NoAccount;
					}
					$result = Ttbmitcoa::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
			}
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'Ttbmitcoa.' . $primaryKeys[ $i ];
			}
			$query     = ( new \yii\db\Query() )
				->select( new Expression(
					'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
								ttbmitcoa.BMNo,
                                ttbmitcoa.BMAutoN,
                                ttbmitcoa.NoAccount,
                                ttbmitcoa.BMBayar,
                                ttbmitcoa.BMKeterangan,
                                traccount.NamaAccount'
				) )
				->from( 'Ttbmitcoa' )
				->join( 'INNER JOIN', 'traccount', 'ttbmitcoa.NoAccount = traccount.NoAccount' )
				->where( [ 'ttbmitcoa.BMNo' => $requestData[ 'BMNo' ] ] )
				->orderBy( 'ttbmitcoa.BMAutoN' );
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	/**
	 * Displays a single Ttbmitcoa model.
	 *
	 * @param string $BMNo
	 * @param integer $BMAutoN
	 * @param string $NoAccount
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $BMNo, $BMAutoN, $NoAccount ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $BMNo, $BMAutoN, $NoAccount ),
		] );
	}
	/**
	 * Creates a new Ttbmitcoa model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Ttbmitcoa();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'BMNo' => $model->BMNo, 'BMAutoN' => $model->BMAutoN, 'NoAccount' => $model->NoAccount ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Ttbmitcoa model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $BMNo
	 * @param integer $BMAutoN
	 * @param string $NoAccount
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $BMNo, $BMAutoN, $NoAccount ) {
		$model = $this->findModel( $BMNo, $BMAutoN, $NoAccount );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'BMNo' => $model->BMNo, 'BMAutoN' => $model->BMAutoN, 'NoAccount' => $model->NoAccount ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Ttbmitcoa model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $BMNo
	 * @param integer $BMAutoN
	 * @param string $NoAccount
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $BMNo, $BMAutoN, $NoAccount ) {
		$this->findModel( $BMNo, $BMAutoN, $NoAccount )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Ttbmitcoa model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $BMNo
	 * @param integer $BMAutoN
	 * @param string $NoAccount
	 *
	 * @return Ttbmitcoa the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $BMNo, $BMAutoN, $NoAccount ) {
		if ( ( $model = Ttbmitcoa::findOne( [ 'BMNo' => $BMNo, 'BMAutoN' => $BMAutoN, 'NoAccount' => $NoAccount ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
