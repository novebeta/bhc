<?php

namespace aunit\controllers;

use aunit\components\AunitController;
use aunit\models\Ttsditjasa;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtsditjasaController implements the CRUD actions for Ttsditjasa model.
 */
class TtsditjasaController extends AunitController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ttsditjasa models.
     * @return mixed
     */
    public function actionIndex()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $requestData                 = \Yii::$app->request->post();
        $primaryKeys                 = Ttsditjasa::getTableSchema()->primaryKey;
        if ( isset( $requestData[ 'oper' ] ) ) {
            /* operation : create, edit or delete */
            $oper = $requestData[ 'oper' ];
            /** @var Ttsditjasa $model */
            $model = null;
            if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
                $model = $this->findModelBase64( 'ttsditjasa', $requestData[ 'id' ] );
            }
            switch ( $oper ) {
                case 'add'  :
                case 'edit' :
                if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
                    $requestData[ 'Action' ] = 'Insert';
                    $requestData[ 'SDAuto' ] = 0;
                } else {
                    $requestData[ 'Action' ] = 'Update';
                    if ( $model != null ) {
                        $requestData[ 'SDNoOLD' ]   = $model->SDNo;
                        $requestData[ 'SDAutoOLD' ] = $model->SDAuto;
                        $requestData[ 'JasaKodeOLD' ] = $model->JasaKode;

                    }
                }
                $result = Ttsditjasa::find()->callSP( $requestData );
                if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                    $response = $this->responseSuccess( $result[ 'keterangan' ] );
                } else {
                    $response = $this->responseFailed( $result[ 'keterangan' ] );
                }
                break;
                case 'batch' :
                    break;
                case 'del'  :
                    $requestData             = array_merge( $requestData, $model->attributes );
                    $requestData[ 'Action' ] = 'Delete';
                    if ( $model != null ) {
                        $requestData[ 'SDNoOLD' ]   = $model->SDNo;
                        $requestData[ 'SDAutoOLD' ] = $model->SDAuto;
                        $requestData[ 'JasaKodeOLD' ] = $model->JasaKode;
                    }
                    $result = Ttsditjasa::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
                    break;
            }
        } else {
            for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
                $primaryKeys[ $i ] = 'Ttsditjasa.' . $primaryKeys[ $i ];
            }
            $query     = ( new \yii\db\Query() )
                ->select( new Expression(
                    'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
                            ttsditjasa.SDNo,
                            ttsditjasa.SDAuto,
                            ttsditjasa.JasaKode,
                            ttsditjasa.SDWaktuKerja,
                            ttsditjasa.SDWaktuSatuan,
                            ttsditjasa.SDWaktuKerjaMenit,
                            ttsditjasa.SDHrgJual,
                            ttsditjasa.SDJualDisc,
                            ttsditjasa.SDPajak,
                            tdjasa.JasaNama,
                            IFNULL( ttsditjasa.SDJualDisc / ttsditjasa.SDHrgJual * 100, 0 ) AS Disc,
                            ttsditjasa.SDHrgJual - ttsditjasa.SDJualDisc AS Jumlah,
                            ttsditjasa.SDHargaBeli,
                            tdjasa.JasaGroup'
                ) )
                ->from( 'ttsditjasa' )
                ->join( 'INNER JOIN', 'tdjasa', 'ttsditjasa.JasaKode = tdjasa.JasaKode' )
                ->where( [ 'ttsditjasa.SDNo' => $requestData[ 'SDNo' ] ] )
                ->orderBy('	ttsditjasa.SDNo,ttsditjasa.SDAuto');
            $rowsCount = $query->count();
            $rows      = $query->all();
            /* encode row id */
            for ( $i = 0; $i < count( $rows ); $i ++ ) {
                $rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
            }
            $response = [
                'rows'      => $rows,
                'rowsCount' => $rowsCount
            ];
        }
        return $response;
    }

    /**
     * Finds the Ttsditjasa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $SDNo
     * @param integer $SDAuto
     * @return Ttsditjasa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($SDNo, $SDAuto)
    {
        if (($model = Ttsditjasa::findOne(['SDNo' => $SDNo, 'SDAuto' => $SDAuto])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
