<?php

namespace aunit\controllers;

use aunit\components\AunitController;
use aunit\models\ContactForm;
use aunit\models\PasswordResetRequestForm;
use aunit\models\ResendVerificationEmailForm;
use aunit\models\ResetPasswordForm;
use aunit\models\SignupForm;
use aunit\models\Tuser;
use aunit\models\VerifyEmailForm;
use common\components\Custom;
use common\components\General;
use DateTime;
use GuzzleHttp\Client;
use SplFileObject;
use Yii;
use yii\base\InvalidArgumentException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;

/**
 * Site controller
 */
class SiteController extends AunitController
{
    const keyNav = [
        'tdarea/update' => ['Provinsi'],
        'tdbbn/update' => ['Kabupaten'],
        'tdprogram/update' => ['ProgramNama'],
        'tdleasing/update' => ['LeaseKode'],
        'tdcustomer/update' => ['CusKode'],
        'tdsales/update' => ['SalesKode'],
        'tdteam/update' => ['TeamKode'],
        'tddealer/update' => ['DealerKode'],
        'tdsupplier/update' => ['SupKode'],
        'tdlokasi/update' => ['LokasiKode'],
        'tdmotortype/update' => ['MotorType'],
        'ttss/update' => ['SSNo'],
        'ttfb/update' => ['FBNo'],
        'ttsd/retur-beli-update' => ['SDNo'],
        'ttsd/invoice-retur-beli-update' => ['SDNo'],
        'ttin/update' => ['INNo'],
        'ttdk/data-konsumen-kredit-update' => ['DKNo'],
        'ttdk/data-konsumen-tunai-update' => ['DKNo'],
        'ttdk/data-konsumen-status' => ['DKNo'],
        'ttdk/data-konsumen-status-update' => ['DKNo'],
        'ttsk/update' => ['SKNo'],
        'ttrk/update' => ['RKNo'],
        'ttdk/stnk-bpkb-update' => ['DKNo'],
        'ttabhd/update' => ['ABNo'],
        'ttbkhd/pengajuan-bayar-bbn-bank-update' => ['BKNo'],
        'ttkk/pengajuan-bayar-bbn-kas-update' => ['KKNo'],
        'ttsd/mutasi-eksternal-dealer-update' => ['SDNo'],
        'ttsd/invoice-eksternal-dealer-update' => ['SDNo'],
        'ttpd/update?MutasiEksternal' => ['PDNo'],
        'ttpd/update?InvoicePenerimaan' => ['PDNo'],
        'ttsmhd/update?MutasiPOS' => ['SMNo'],
        'ttpbhd/update' => ['PBNo'],
        'ttsmhd/update?RepairIN' => ['SMNo'],
        'ttsmhd/update?RepairOUT' => ['SMNo'],
        'ttkm/kas-masuk-inden-update' => ['KMNo'],
        'ttkm/kas-masuk-pelunasan-leasing-update' => ['KMNo'],
        'ttkm/kas-masuk-uang-muka-kredit-update' => ['KMNo'],
        'ttkm/kas-masuk-bayar-piutang-kon-um-update' => ['KMNo'],
        'ttkm/kas-masuk-uang-muka-tunai-update' => ['KMNo'],
        'ttkm/kas-masuk-bayar-sisa-piutang-update' => ['KMNo'],
        'ttkm/kas-masuk-bbn-acc-update' => ['KMNo'],
        'ttkm/kas-masuk-bbn-progresif-update' => ['KMNo'],
        'ttkm/kas-masuk-dari-bank-update' => ['KMNo'],
        'ttkm/kas-masuk-umum-update' => ['KMNo'],
        'ttkm/kas-masuk-dari-bengkel-update' => ['KMNo'],
        'ttkm/kas-masuk-angsuran-karyawan-update' => ['KMNo'],
        'ttkm/kas-masuk-pos-dari-dealer-update' => ['KMNo'],
        'ttkm/kas-masuk-dealer-dari-pos-update' => ['KMNo'],
        'ttkk/kas-keluar-subsidi-dealer-2-update' => ['KKNo'],
        'ttkk/kas-keluar-retur-harga-update' => ['KKNo'],
        'ttkk/kas-keluar-insentif-sales-update' => ['KKNo'],
        'ttkk/kas-keluar-potongan-khusus-update' => ['KKNo'],
        'ttkk/kas-keluar-bayar-hutang-unit-update' => ['KKNo'],
        'ttkk/kas-keluar-bayar-hutang-bbn-update' => ['KKNo'],
        'ttkk/kas-keluar-bayar-bbn-progresif-update' => ['KKNo'],
        'ttkk/kas-keluar-bayar-bbn-acc-update' => ['KKNo'],
        'ttkk/kas-keluar-bank-unit-update' => ['KKNo'],
        'ttkk/kas-keluar-umum-update' => ['KKNo'],
        'ttkk/kas-keluar-bank-bengkel-update' => ['KKNo'],
        'ttkk/kas-keluar-pinjaman-karyawan-update' => ['KKNo'],
        'ttkk/kas-keluar-dealer-pos-update' => ['KKNo'],
        'ttkk/kas-keluar-pos-dealer-update' => ['KKNo'],
        'ttkk/kas-transfer-update' => ['KKLInk'],
        'ttbmhd/bank-masuk-inden-update' => ['BMNo'],
        'ttbmhd/bank-masuk-leasing-update' => ['BMNo'],
        'ttbmhd/bank-masuk-scheme-update' => ['BMNo'],
        'ttbmhd/bank-masuk-uang-muka-kredit-update' => ['BMNo'],
        'ttbmhd/bank-masuk-uang-muka-tunai-update' => ['BMNo'],
        'ttbmhd/bank-masuk-piutang-kons-um-update' => ['BMNo'],
        'ttbmhd/bank-masuk-bbn-progresif-kons-update' => ['BMNo'],
        'ttbmhd/bank-masuk-bbn-progresif-update' => ['BMNo'],
        'ttbmhd/bank-masuk-dari-kas-update' => ['BMNo'],
        'ttbmhd/bank-masuk-umum-update' => ['BMNo'],
        'ttbmhd/bank-masuk-penjualan-multi-update' => ['BMNo'],
        'ttbkhd/bank-keluar-subsidi-dealer-2-update' => ['BKNo'],
        'ttbkhd/bank-keluar-retur-harga-update' => ['BKNo'],
        'ttbkhd/bank-keluar-insentif-sales-update' => ['BKNo'],
        'ttbkhd/bank-keluar-potongan-khusus-update' => ['BKNo'],
        'ttbkhd/bank-keluar-bayar-hutang-unit-update' => ['BKNo'],
        'ttbkhd/bank-keluar-bayar-hutang-bbn-update' => ['BKNo'],
        'ttbkhd/bank-keluar-bayar-bbn-progresif-update' => ['BKNo'],
        'ttbkhd/bank-keluar-kas-update' => ['BKNo'],
        'ttbkhd/bank-keluar-umum-update' => ['BKNo'],
        'ttbkhd/bank-transfer-update' => ['FBNo'],
        'ttgeneralledgerhd/memorial-update'=> ['NoGL'],
        'ttgeneralledgerhd/transaksi-update'=> ['NoGL'],
        'ttgeneralledgerhd/pindahbuku-update'=> ['NoGL'],
        'ttgeneralledgerhd/adjustment-update'=> ['NoGL'],
        'trmp/update'=> ['MPNo'],
        'traccount/update'=> ['NoAccount'],
        'ttamhd/update'=> ['AMNo'],
    ];
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['logout', 'signup'],
//                'rules' => [
//                    [
//                        'actions' => ['signup'],
//                        'allow' => true,
//                        'roles' => ['?'],
//                    ],
//                    [
//                        'actions' => ['logout'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionDekrip()
    {
        /** @var Tuser[] $user */
        $user = Tuser::find()->all();
        $list = [];
        foreach ($user as $u) {
            $list[$u->UserID] = Custom::Decript($u->UserPass);
        }
        $this->layout = '/plain';
        var_dump($list);
        return $list;
    }
//    public function actionIndex(){
//	    $pdf = file_get_contents('http://192.168.56.101:8080/file/tdbbn/pdf');
//	    header('Content-Description: File Transfer');
//	    header('Content-Type: application/octet-stream');
//	    header('Content-Disposition: attachment; filename=tdbbn.pdf');
//	    ob_start();
//	    echo $pdf;
//	    return ob_get_clean();
//    }
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
//	    $client = new Client();
//	    $response = $client->get('http://192.168.56.101:8080/file/tdbbn/pdf')
//	                       ->send();
        if(Yii::$app->session->get( '_nav' ) == null || Yii::$app->session->get( '_company' ) == null){
            return $this->redirect( Custom::urllgn( 'site/dealer' ) );
        }
        return $this->render('index');
    }

    public function actionBackupRestore()
    {
        return $this->render('backup-restore');
    }

    public function actionSendMessage()
    {
        return $this->render('send-message');
    }

    public function actionPreview()
    {
        $data = json_decode($_POST['data'],true);
        $msg = strtr($_POST['pesan'], $data) . "\r\n\r\n" . $_SESSION['_company']['nama'];
        return $this->responseSuccess("No. WA : " . $data['CusTelepon'] . "<br>" . $msg);
    }

    public function actionSend()
    {
        $outlet64 = $_COOKIE['_outlet'];
        $outlet         = base64_decode($outlet64);
        $json = General::send($outlet,$_SESSION['_company']['nama'],$_POST['pesan'], $_POST['data'], $_FILES['file-0'] ?? null);
        if ($json['status']) {
            return $this->responseSuccess($json['message']);
        } else {
            return $this->responseFailed($json['message']);
        }
    }

    public function actionReportRealtime()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return General::reportRealtime($_POST['from'],$_POST['to']);
    }

    public function actionTampil()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = General::cCmd($_POST['query'])->queryAll();
        $header = sizeof($result) > 0 ? array_keys($result[0]) : [];
        $colModel = [];
        foreach ($header as $item) {
            $colModel[] = ['name' => $item];
        }
        return [
            'header' => $header,
            'colModel' => $colModel,
            'data' => $result
        ];
    }

    public function actionScan(){
        return $this->render('scan');
    }

    public function actionWaktu()
    {
        session_write_close();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->formatter->locale = 'id-ID';
        $time1 = new DateTime('@' . (Yii::$app->session->get('_loginTime')));
        $time2 = new DateTime();
        $interval = $time1->diff($time2);
        return [
            'tgl' => Yii::$app->formatter->asDatetime('now', 'eeee, dd MMMM yyyy - HH:mm:ss'),
            'duration' => $interval->format('%H:%I:%S')
        ];
    }

    public function actionTestReport()
    {
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);
        $response = $client->get(Yii::$app->params['H1'][$_POST['db']]['host_report'] . '/login/camellabs/camellabs');
        return $response->getBody()->getContents();
    }

    public function actionTestError()
    {
        $rArr = General::cCmd("SELECT MenuText AS Id,
			                  KodeAkses,
			                  MenuInduk,
			                  MenuText,
       						  TO_BASE64(concat(\"<span style=\"\"margin-top: -5px;\"\" MenuNo=\",MenuNo,\" MenuInduk=\"\"\",MenuInduk,\"\"\" id=\"\"\",MenuText,\"\"\">\" ,MenuText)) as html,
			                  MenuNo,
			                  MenuStatus,
			                  MenuTambah,
			                  MenuEdit,
			                  MenuHapus,
			                  MenuCetak,
			                  MenuView FROM `tuakses` WHERE KodeAkses = :KodeAkses 
							  ORDER BY MenuInduk,MenuNo", [':KodeAkses' => 'Super'])->queryAll();
        return json_encode($rArr);
//		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//		throw new \yii\web\HttpException( 500, 'The requested Item could not be found.' );
    }

    public function actionBackup()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $infoDB = [
            'host' => Custom::getDsnAttribute('host', Yii::$app->getDb()->dsn),
            'dbname' => Custom::getDsnAttribute('dbname', Yii::$app->getDb()->dsn),
            'username' => Yii::$app->getDb()->username,
            'password' => Yii::$app->getDb()->password,
        ];
        $nama = Yii::getAlias('@webroot') . "/backup/" . $_SESSION['_company']['nama'] . " " . date('Ymd Hi') . ".dmp";
        $nama_url = Yii::getAlias('@web') . "/backup/" . $_SESSION['_company']['nama'] . " " . date('Ymd Hi') . ".dmp";
        $logFile = Yii::getAlias('@runtime') . "/" . $infoDB['dbname'] . ".log";
        if (file_exists($logFile)) {
            unlink($logFile);
        }
        session_write_close();
//		sleep( 5 );
        $output = shell_exec("mysqldump -h " . $infoDB['host'] . " -u" . $infoDB['username'] .
            " -p" . $infoDB['password'] . " --routines --databases " .
            $infoDB['dbname'] . " --verbose 2> " . $logFile . " > '" . $nama . "'");
        return [
            'status' => true,
            'url' => htmlentities($nama_url)
        ];
    }

    public function actionMonitorBackup()
    {
        $filename = Yii::getAlias('@runtime') . "/" . Custom::getDsnAttribute('dbname', Yii::$app->getDb()->dsn) . ".log";
        $filenametmp = Yii::getAlias('@runtime') . "/" . Custom::getDsnAttribute('dbname', Yii::$app->getDb()->dsn) . "_tmp.log";
        shell_exec('\cp "' . $filename . '" ' . ' "' . $filenametmp . '"');
//		$output   = shell_exec( 'cat ' . $filename );
//		$output   = file_get_contents($filename);
        if (!file_exists($filenametmp)) {
            return "==EOL==";
        }
        $file = new SplFileObject($filenametmp, "r");
        if (!$file->isFile()) {
            return "==EOL==";
        }
        $file->flock(LOCK_SH);
        $file->seek($_GET['line']);
        if ($file->valid()) {
            return $file->current();
        } else {
            return "==EOL==";
        }
    }

    public function actionMonitorRestore()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return \Yii::$app->db->createCommand("SHOW PROCESSLIST")->queryAll();
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
//    public function actionLogout()
//    {
//        Yii::$app->user->logout();
//
//        return $this->goHome();
//    }
    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }
            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }
        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     *
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');
            return $this->goHome();
        }
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     *
     * @return yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }
        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }
        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }

    public function actionCall()
    {
        header('Content-Type:application/json');
        # atur zona waktu sender server ke Jakarta (WIB / GMT+7)
        date_default_timezone_set("Asia/Jakarta");
        # ambil waktu server pengirim (sender)
        $curr_unix_time = time(); # ambil waktu saat ini dalam bentuk Unix Timestamp UTC (tidak terpengaruh zona waktu)
        // $curr_unix_time = 1567645682; # contoh hasil fungsi time() untuk Thursday, 05-Sep-19 01:08:02 UTC
        // sleep(11); # contoh command sleep untuk delay 11 detik di server pengirim
        # atur API Endpoint & Credentials (bisa disimpan di database, harap dipastikan Api_Key dan Secret_key tersimpan dengan aman)
        $url = 'https://anpersys.com/dgi-api/v1/pkb/read';
//		$url        = 'https://devproxy.astra.co.id/api_gateway/astra-api-developer/dmsahassapi-qa/dgi-api/v1/spk/read';
//		$api_key    = 'dgi-key-live:52ADFCEE-18BE-470E-9772-4E76EB0CDF00';
//		$secret_key = 'dgi-secret-live:15C06B55-B31C-4A1C-BC23-085C23504F28';
        $api_key = 'dgi-key-live:F2AF73B44FF1';
        $secret_key = 'dgi-secret-live:E24013B0CD';
        # atur request header, kirimkan API Key, Unix Timestamp dan Token hasil hash (secret key tidak dikirimkan dalam bentuk plain text agar lebih secure)
        $headers = [
            'Content-Type:application/json',
            'Accept:application/json',
            'DGI-API-Key:' . $api_key,
            'X-Request-Time:' . $curr_unix_time,
            'DGI-API-Token:' . hash('sha256', $api_key . $secret_key . $curr_unix_time) # hash token agar tidak bisa terbaca (one way), token ini digunakan untuk keperluan validasi
        ];
        # cetak contoh token, hanya sebagai contoh untuk melihat hasil token yang digenerate
        // echo hash('sha256', $api_key.$secret_key.$curr_unix_time)."<br/><br/>";
        # set request body dalam bentuk JSON String (Post Data)
        # ini hanya sebagai contoh post data saja
//		$post_raw_json = json_encode(array(
        //'startTime' => '2021-01-01 00:00:00', # atur filter start time (contoh filter lebih dari 7 hari)
        //'endTime' => '2021-01-08 00:00:01', # atur filter end time (contoh filter lebih dari 7 hari)
//			'fromTime' => '2021-01-01 00:00:00', # atur filter start time
//			'toTime' => '2021-02-25 23:59:59', # atur filter end time
        //'dealerId' => '04700', # contoh filter dealerId
        //'spkNo' => 'SPK0001', # contoh filter spkNo
        //"idProspect "=> "H2Z/12345/19/03/PSP/0001/00001",
        //"idSalesPeople "=> "122536",
//			'requestServerTime' => date('Y-m-d H:i:s', $curr_unix_time),
//			'requestServertimezone' => date('T'),
//			'requestServerTimestamp' => $curr_unix_time,
//		));
        $post_raw_json = Json::encode([
            'fromTime' => '2021-01-01 12:31:00',
            'toTime' => '2021-01-08 15:50:00',
            'dealerId' => '02552',
            'requestServerTime' => date('Y-m-d H:i:s', $curr_unix_time),
            'requestServertimezone' => date('T'),
            'requestServerTimestamp' => $curr_unix_time,
        ]);
//		$post_raw_json = Json::encode( [
//			'fromTime'               => '2019-01-15 12:31:00',
//			'toTime'                 => '2019-01-21 15:50:00',
//			'dealerId'               => '08138',
//			"idProspect "=> "H2Z/12345/19/03/PSP/0001/00001",
//			"idSalesPeople "=> "122536",
//		] );
//		 echo $post_raw_json;
        // exit();
        # Inisiasi CURL request
        $ch = curl_init();
        # atur CURL Options
        curl_setopt_array($ch, [
            CURLOPT_URL => $url, # URL endpoint
            CURLOPT_HTTPHEADER => $headers, # HTTP Headers
            CURLOPT_RETURNTRANSFER => 1, # return hasil curl_exec ke variabel, tidak langsung dicetak
            CURLOPT_FOLLOWLOCATION => 1, # atur flag followlocation untuk mengikuti bila ada url redirect di server penerima tetap difollow
            CURLOPT_CONNECTTIMEOUT => 60, # set connection timeout ke 60 detik, untuk mencegah request gantung saat server mati
            CURLOPT_TIMEOUT => 60, # set timeout ke 120 detik, untuk mencegah request gantung saat server hang
            CURLOPT_POST => 1, # set method request menjadi POST
            CURLOPT_POSTFIELDS => $post_raw_json, # attached post data dalam bentuk JSON String,
            // CURLOPT_VERBOSE => 1, # mode debug
            // CURLOPT_HEADER => 1, # cetak header
        ]);
        # eksekusi CURL request dan tampung hasil responsenya ke variabel $resp
        $resp = curl_exec($ch);
        # validasi curl request tidak error
        if (curl_errno($ch) == false) {
            # jika curl berhasil
            $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if ($http_code == 200) {
                # http code === 200 berarti request sukses (harap pastikan server penerima mengirimkan http_code 200 jika berhasil)
                return $resp;
            } else {
                # selain itu request gagal (contoh: error 404 page not found)
                // echo 'Error HTTP Code : '.$http_code."\n";
                echo $resp;
            }
        } else {
            # jika curl error (contoh: request timeout)
            # Daftar kode error : https://curl.haxx.se/libcurl/c/libcurl-errors.html
            echo "Error while sending request, reason:" . curl_error($ch);
        }
        # tutup CURL
        curl_close($ch);
    }

    public function actionPushInv1()
    {
        return $this->render('push-inv1');
    }

    public function actionDetails() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $comm      = General::cCmd("SELECT * FROM 
                        (SELECT CONCAT('TJ/',DealerKodeDAstra,'/',RIGHT(YEAR(SKTgl),2),'/',LPAD(MONTH(SKTgl),2,0),'/',RIGHT(ttsk.SKNo,5)) AS idInvoice, zidSPK AS idSPK, zidCustomer AS idCustomer, kmbmbayar.Amount AS Amount, 
                        'Cash' AS tipePembayaran, 'Close' AS STATUS, KMJenis AS Note, SKJam AS createTime, ttdk.DKNetto, ttdk.DKNo
                        FROM ttdk 
                        INNER JOIN tdcustomer ON tdcustomer.CusKode = ttdk.CusKode
                        INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
                        INNER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo
                        INNER JOIN tddealer ON tddealer.DealerStatus = 1
                        INNER JOIN (SELECT KMlink, SUM(KMNominal) AS Amount, KMJenis FROM vtkm WHERE KMJenis IN ('Piutang Sisa') GROUP BY KMLink) kmbmbayar
                        ON kmbmbayar.KMLINK = ttdk.dkno
                        UNION
                        SELECT CONCAT('TJ/',DealerKodeDAstra,'/',RIGHT(YEAR(SKTgl),2),'/',LPAD(MONTH(SKTgl),2,0),'/',RIGHT(ttsk.SKNo,5)) AS idInvoice,zidSPK AS idSPK, zidCustomer AS idCustomer, kmbmbayar.Amount AS Amount, 
                        'Cash' AS tipePembayaran, 'Close' AS STATUS, KMJenis AS Note, SKJam AS createTime, ttdk.DKDPLLeasing, ttdk.DKNo
                        FROM ttdk 
                        INNER JOIN tdcustomer ON tdcustomer.CusKode = ttdk.CusKode
                        INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
                        INNER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo
                        INNER JOIN tddealer ON tddealer.DealerStatus = 1
                        INNER JOIN (SELECT KMlink, SUM(KMNominal) AS Amount, KMJenis FROM vtkm WHERE KMJenis IN ('Piutang UM') GROUP BY KMLink) kmbmbayar
                        ON kmbmbayar.KMLINK = ttdk.dkno
                        UNION
                        SELECT CONCAT('TJ/',DealerKodeDAstra,'/',RIGHT(YEAR(SKTgl),2),'/',LPAD(MONTH(SKTgl),2,0),'/',RIGHT(ttsk.SKNo,5)) AS idInvoice,zidSPK AS idSPK, zidCustomer AS idCustomer, kmbmbayar.Amount AS Amount, 
                        'Cash' AS tipePembayaran, 'Close' AS STATUS, KMJenis AS Note, SKJam AS createTime, ttdk.DKNetto, ttdk.DKNo
                        FROM ttdk 
                        INNER JOIN tdcustomer ON tdcustomer.CusKode = ttdk.CusKode
                        INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
                        INNER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo
                        INNER JOIN tddealer ON tddealer.DealerStatus = 1
                        INNER JOIN (SELECT KMlink, SUM(KMNominal) AS Amount, KMJenis FROM vtkm WHERE KMJenis IN ('Leasing') GROUP BY KMLink) kmbmbayar
                        ON kmbmbayar.KMLINK = ttdk.dkno
                        UNION
                        SELECT CONCAT('TJ/',DealerKodeDAstra,'/',RIGHT(YEAR(SKTgl),2),'/',LPAD(MONTH(SKTgl),2,0),'/',RIGHT(ttsk.SKNo,5)) AS idInvoice,zidSPK AS idSPK, zidCustomer AS idCustomer, kmbmbayar.Amount AS Amount, 
                        'Cash' AS tipePembayaran, 'Close' AS STATUS, KMJenis AS Note, SKJam AS createTime, ttdk.DKDPTerima, ttdk.DKNo
                        FROM ttdk 
                        INNER JOIN tdcustomer ON tdcustomer.CusKode = ttdk.CusKode
                        INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
                        INNER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo
                        INNER JOIN tddealer ON tddealer.DealerStatus = 1
                        INNER JOIN (SELECT KMlink, SUM(KMNominal) AS Amount, KMJenis FROM vtkm WHERE KMJenis IN ('Kredit') GROUP BY KMLink) kmbmbayar
                        ON kmbmbayar.KMLINK = ttdk.dkno
                        UNION
                        SELECT CONCAT('TJ/',DealerKodeDAstra,'/',RIGHT(YEAR(SKTgl),2),'/',LPAD(MONTH(SKTgl),2,0),'/',RIGHT(ttsk.SKNo,5)) AS idInvoice,zidSPK AS idSPK, zidCustomer AS idCustomer, kmbmbayar.Amount AS Amount, 
                        'Cash' AS tipePembayaran, 'Close' AS STATUS, KMJenis AS Note, SKJam AS createTime, ttdk.DKDPTerima, ttdk.DKNo
                        FROM ttdk 
                        INNER JOIN tdcustomer ON tdcustomer.CusKode = ttdk.CusKode
                        INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
                        INNER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo
                        INNER JOIN tddealer ON tddealer.DealerStatus = 1
                        INNER JOIN (SELECT KMlink, SUM(KMNominal) AS Amount, KMJenis FROM vtkm WHERE KMJenis IN ('Tunai') GROUP BY KMLink) kmbmbayar
                        ON kmbmbayar.KMLINK = ttdk.dkno) INV1
                        WHERE (DKNetto - Amount <> 0) AND DATE(createTime) BETWEEN '2020-01-01' AND '2020-12-31';
                        ");
        $rowsArray = $comm->queryAll();
        for ( $i = 0; $i < count( $rowsArray ); $i ++ ) {
            $rowsArray[ $i ][ 'idSPK' ] = $rowsArray[ $i ][ 'idSPK' ] ;
        }
        $response = [
            'rows'      => $rowsArray,
            'rowsCount' => sizeof( $rowsArray )
        ];
        return $response;

    }

    public function actionNavigation(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $post             = \Yii::$app->request->post();
        $_nav = Yii::$app->session['_navSearch'];
        if($post['oper'] == 'update'){
            $_nav['linkBack'] = $post['linkBack'];
            $_nav['linkNext'] = $post['linkNext'];
            $_nav['current'] = $post['current'];
        }else{
            $sqlraw           = "";
            $params           = [':nilai'=> '%'.$post['inSearch'].'%'];
            switch($post['url']){
                case 'tdarea/update':
                    $sqlraw = "SELECT tdarea.Provinsi
                        FROM tdarea
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (tdarea.Provinsi NOT LIKE '%=%')";
                    break;
                case 'tdbbn/update':
                    $sqlraw = "SELECT tdbbn.Kabupaten
                        FROM tdbbn
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (tdbbn.Kabupaten NOT LIKE '%=%')";
                    break;
                case 'tdprogram/update':
                    $sqlraw = "SELECT tdprogram.ProgramNama
                        FROM tdprogram
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (tdprogram.ProgramNama NOT LIKE '%=%')";
                    break;
                case 'tdleasing/update':
                    $sqlraw = "SELECT tdleasing.LeaseKode
                        FROM tdleasing
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (tdleasing.LeaseKode NOT LIKE '%=%')";
                    break;
                case 'tdcustomer/update':
                    $sqlraw = "SELECT tdcustomer.CusKode
                        FROM tdcustomer
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (tdcustomer.CusKode NOT LIKE '%=%')";
                    break;
                case 'tdsales/update':
                    $sqlraw = "SELECT tdsales.SalesKode
                        FROM tdsales
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (tdsales.SalesKode NOT LIKE '%=%')";
                    break;
                case 'tdteam/update':
                    $sqlraw = "SELECT tdteam.TeamKode
                        FROM tdteam
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (tdteam.TeamKode NOT LIKE '%=%')";
                    break;
                case 'tddealer/update':
                    $sqlraw = "SELECT tddealer.DealerKode
                        FROM tddealer
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (tddealer.DealerKode NOT LIKE '%=%')";
                    break;
                case 'tdsupplier/update':
                    $sqlraw = "SELECT tdsupplier.SupKode
                        FROM tdsupplier
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (tdsupplier.SupKode NOT LIKE '%=%')";
                    break;
                case 'tdlokasi/update':
                    $sqlraw = "SELECT tdlokasi.LokasiKode
                        FROM tdlokasi
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (tdlokasi.LokasiKode NOT LIKE '%=%')";
                    break;
                case 'tdmotortype/update':
                    $sqlraw = "SELECT tdmotortype.MotorType
                        FROM tdmotortype
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (tdmotortype.MotorType NOT LIKE '%=%')";
                    break;
                case 'ttss/update':
                    $sqlraw = "SELECT ttss.SSNo
                        FROM  ttss 
                        LEFT OUTER JOIN ttfb ON ttss.FBNo = ttfb.FBNo 
                        LEFT OUTER JOIN tdsupplier ON ttss.SupKode = tdsupplier.SupKode 
                        LEFT OUTER JOIN tdlokasi ON ttss.LokasiKode = tdlokasi.LokasiKode 
                        LEFT OUTER JOIN 
                            (SELECT COUNT(MotorType) AS SSJum, SSNo FROM tmotor tmotor_1 GROUP BY SSNo) tmotor 
                        ON ttss.SSNo = tmotor.SSNo 
                        WHERE ({$post['cmbSearch']} LIKE :nilai) AND (ttss.SSNo NOT LIKE '%=%')
                        ORDER BY ttss.SSNo";
                    break;
                case 'ttfb/update':
                    $sqlraw = "SELECT ttfb.FBNo
                    FROM `ttfb` LEFT OUTER JOIN `ttss` ON `ttfb`.`SSNo` = `ttss`.`SSNo` 
                    LEFT OUTER JOIN `tdsupplier` ON `ttfb`.`SupKode` = `tdsupplier`.`SupKode` 
                    LEFT OUTER JOIN (SELECT COUNT(MotorType) AS FBJum, SUM(FBHarga) AS FBSubTotal, FBNo FROM tmotor tmotor_1 GROUP BY FBNo) tmotor ON ttfb.FBNo = tmotor.FBNo 
                    WHERE (ttfb.FBNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai) 
                    ORDER BY `FBTgl` DESC";
                    break;
                case 'ttsd/retur-beli-update':
                    $sqlraw = "SELECT ttsd.SDNo
                    FROM `ttsd` LEFT JOIN `tdlokasi` ON `ttsd`.`LokasiKode` = `tdlokasi`.`LokasiKode` 
                    LEFT JOIN `tdsupplier` ON `ttsd`.`DealerKode` = `tdsupplier`.`SupKode` 
                    WHERE (LEFT(SDNo,2) = 'SR' AND ttsd.SDNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai) 
                    ORDER BY `SDTgl` DESC, `SupNama`";
                    break;
                case 'ttsd/invoice-retur-beli-update':
                    $sqlraw = "SELECT ttsd.SDNo
                    FROM `ttsd` LEFT JOIN `tdlokasi` ON `ttsd`.`LokasiKode` = `tdlokasi`.`LokasiKode`
                    LEFT JOIN `tdsupplier` ON `ttsd`.`DealerKode` = `tdsupplier`.`SupKode`
                    WHERE (LEFT(SDNo,2) = 'SR' AND INSTR(`SDNo`, '=') = 0) AND (SDTotal >= 0) AND ({$post['cmbSearch']} LIKE :nilai) 
                    ORDER BY `SDTgl` DESC";
                    break;
                case 'ttin/update':
                    $sqlraw = "SELECT ttin.INNo
                    FROM	`ttin`INNER JOIN `tdleasing` ON	`ttin`.`LeaseKode` = `tdleasing`.`LeaseKode`
                    INNER JOIN `tdsales` ON	`ttin`.`SalesKode` = `tdsales`.`SalesKode`
                    INNER JOIN `tdcustomer` ON	`ttin`.`CusKode` = `tdcustomer`.`CusKode`
                    LEFT OUTER JOIN `ttdk` ON	`ttin`.`INNo` = `ttdk`.`INNo`
                    LEFT OUTER JOIN `tdmotortype` ON	`ttin`.`MotorType` = `tdmotortype`.`MotorType`
                    WHERE ({$post['cmbSearch']} LIKE :nilai) 
                    ORDER BY	`INTgl` DESC";
                    break;
                case 'ttdk/data-konsumen-kredit-update':
                    $sqlraw = "SELECT DKNo
                    FROM (SELECT ttdk.DKNo, tmotor.SKNo,ttdk.DKHarga,ttdk.DKTgl, IFNULL(tdcustomer.CusNama,'--') AS CusNama, 
			         IFNULL(tmotor.MotorType,'--') AS MotorType, 
			         IFNULL(tmotor.MotorWarna,'--') AS MotorWarna, 
			         IFNULL(tmotor.MotorTahun,'--') AS MotorTahun, 
			         IFNULL(tmotor.MotorNoMesin,'--') AS MotorNoMesin, 
			         IFNULL(tmotor.MotorNoRangka,'--') AS MotorNoRangka, 
			         IFNULL(tdmotortype.MotorNama,'--') AS MotorNama 
			         FROM ttdk 
			         INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			         INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType 
			         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
			         WHERE DKJenis LIKE 'Kredit%' ) AS AAA
                    WHERE ({$post['cmbSearch']} LIKE :nilai) 
                    ORDER BY `DKTgl` DESC";
                    break;
                case 'ttdk/data-konsumen-tunai-update':
                    $sqlraw = "SELECT DKNo
                    FROM  (SELECT ttdk.DKNo, tmotor.SKNo,ttdk.DKTgl, IFNULL(tdcustomer.CusNama,'--') AS CusNama, 
			         IFNULL(tmotor.MotorType,'--') AS MotorType, 
			         IFNULL(tmotor.MotorWarna,'--') AS MotorWarna, 
			         IFNULL(tmotor.MotorTahun,'--') AS MotorTahun, 
			         IFNULL(tmotor.MotorNoMesin,'--') AS MotorNoMesin, 
			         IFNULL(tmotor.MotorNoRangka,'--') AS MotorNoRangka, 
			         IFNULL(tdmotortype.MotorNama,'--') AS MotorNama 
			         FROM ttdk 
			         INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			         INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType 
			         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
			         WHERE DKJenis LIKE 'Tunai%'  )  AS AAA
                    WHERE ({$post['cmbSearch']} LIKE :nilai) 
                    ORDER BY `DKTgl` DESC";
                    break;
                case 'ttdk/data-konsumen-status-update': //TODO : belum ada gui
                    $sqlraw = "SELECT DKNo
                    FROM   (SELECT ttdk.DKNo, ttdk.DKTgl, ttdk.SalesKode, ttdk.CusKode, ttdk.LeaseKode, ttdk.DKJenis, ttdk.DKMemo, ttdk.DKLunas, ttdk.UserID, ttdk.DKHPP, 
                         ttdk.ProgramNama, ttdk.Jaket, ttdk.NamaHadiah, ttdk.INNo, ttdk.DKTenor, ttdk.DKAngsuran,   
                         (ttdk.PrgSubsSupplier + ttdk.JAPrgSubsSupplier) AS PrgSubsSupplier, (ttdk.PrgSubsDealer + ttdk.JAPrgSubsDealer) AS PrgSubsDealer, (ttdk.PrgSubsFincoy + ttdk.JAPrgSubsFincoy) AS PrgSubsFincoy, 
                         (ttdk.ProgramSubsidi + ttdk.JAPrgSubsSupplier + ttdk.JAPrgSubsDealer + ttdk.JAPrgSubsFincoy) AS ProgramSubsidi,
                         (ttdk.DKDPLLeasing+JADKDPLLeasing) AS DKDPLLeasing, (ttdk.DKDPTerima + ttdk.JADKDPTerima) AS DKDPTerima, (ttdk.DKDPTotal + ttdk.JADKDPTerima + JADKDPLLeasing) AS DKDPTotal, 
                         (ttdk.DKHarga+ttdk.JADKHarga) AS DKHarga, (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) AS DKNetto, 
                         (ttdk.BBN+ttdk.JABBN) AS BBN, 
                         (ttdk.ReturHarga+ttdk.JAReturHarga) AS ReturHarga, (ttdk.PotonganHarga+ttdk.JAPotonganHarga) AS PotonganHarga, (ttdk.InsentifSales+ttdk.JAInsentifSales) AS InsentifSales, (ttdk.PotonganKhusus+ttdk.JAPotonganKhusus) AS PotonganKhusus,
                         ttdk.JAPrgSubsSupplier, ttdk.JAPrgSubsDealer, ttdk.JAPrgSubsFincoy, 
                         ttdk.JADKHarga, ttdk.JADKDPTerima, ttdk.JADKDPLLeasing,
                         ttdk.JABBN, 
                         ttdk.JAReturHarga, ttdk.JAPotonganHarga, ttdk.JAInsentifSales, ttdk.JAPotonganKhusus,
                         ttdk.PotonganAHM, ttdk.DKDPInden, ttdk.BBNPlus, ttdk.DKSurveyor, 
                         ttdk.TeamKode, ttdk.DKTglTagih, ttdk.DKPONo, ttdk.DKPOTgl, ttdk.ROCount, ttdk.DKScheme,ttdk.DKScheme2,  ttdk.DKSCP, ttdk.DKPengajuanBBN,
                         ttdk.DKMerkSblmnya, ttdk.DKJenisSblmnya, ttdk.DKMotorUntuk, ttdk.DKMotorOleh, ttdk.DKMediator, ttdk.DKTOP, ttdk.DKPOLeasing, ttdk.SPKNo, 
                         tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, 
                         tmotor.SKNo, tmotor.RKNo, tmotor.FakturAHMNo,
                         MotorNama, MotorKategori, SalesNama, LeaseNama, 
                         CusKTP, CusNama, CusAlamat, CusRT, CusRW, CusProvinsi, CusKabupaten, CusKecamatan, CusKelurahan, CusKodePos, CusTelepon, CusSex, 
                         CusTempatLhr, CusTglLhr, CusAgama, CusPekerjaan, CusPendidikan, CusPengeluaran, CusEmail, CusKodeKons, 
                         tdcustomer.CusStatusHP, tdcustomer.CusStatusRumah, tdcustomer.CusPembeliNama, tdcustomer.CusPembeliKTP, tdcustomer.CusPembeliAlamat, tdcustomer.CusKK
                        FROM  ttdk 
                        INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
                        INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                        INNER JOIN tdleasing ON ttdk.LeaseKode = tdleasing.LeaseKode 
                        INNER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode 
                        INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode)   AS AAA
                    WHERE ({$post['cmbSearch']} LIKE :nilai) 
                    ORDER BY `DKTgl` DESC";
                    break;
                case 'ttsk/update': 
                    $sqlraw = "SELECT SKNo
                    FROM   (SELECT ttsk.SKJam, ttsk.SKCetak, ttsk.SKPengirim, ttsk.SKNo, ttsk.SKTgl, ttsk.LokasiKode, ttsk.SKMemo, ttsk.UserID, ttdk.CusKode, tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, tdcustomer.CusTelepon, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, ttdk.DKNo, ttdk.DKHarga, tmotor.RKNo, ttsk.NoGL, tdmotortype.MotorNama
				         FROM ttsk 
				         INNER JOIN tmotor ON ttsk.SKNo = tmotor.SKNo
				         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
						 INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType
				         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
				         UNION ALL
				         SELECT ttsk.SKJam, ttsk.SKCetak, ttsk.SKPengirim, ttsk.SKNo, ttsk.SKTgl, ttsk.LokasiKode, ttsk.SKMemo, ttsk.UserID, ttdk.CusKode, tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, tdcustomer.CusTelepon, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, ttdk.DKNo, ttdk.DKHarga, tmotor.RKNo, ttsk.NoGL, tdmotortype.MotorNama
				         FROM ttsk 
				         INNER JOIN ttrk ON ttsk.SKNo = ttrk.SKNo 
				         INNER JOIN tmotor ON ttrk.RKNo = tmotor.RKNo 
						 INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType
				         INNER JOIN ttdk ON ttrk.DKNo = ttdk.DKNo 
				         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode) AS AAA
                    WHERE ({$post['cmbSearch']} LIKE :nilai) 
                    ORDER BY `SKNo` DESC";
                    break;
                case 'ttrk/update': 
                    $sqlraw = "SELECT ttrk.RKNo
                    FROM	`ttrk`INNER JOIN `tmotor` ON	`ttrk`.`RKNo` = `tmotor`.`RKNo`
                    INNER JOIN `ttsk` ON	`ttrk`.`SKNo` = `ttsk`.`SKNo`
                    INNER JOIN `tdlokasi` ON	`ttrk`.`LokasiKode` = `tdlokasi`.`LokasiKode`
                    INNER JOIN `ttdk` ON	`ttrk`.`DKNo` = `ttdk`.`DKNo`
                    INNER JOIN `tdcustomer` ON	`ttdk`.`CusKode` = `tdcustomer`.`CusKode`
                    INNER JOIN `tdmotortype` ON	`tmotor`.`MotorType` = `tdmotortype`.`MotorType`
                    WHERE	((ttrk.RKNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai) 
                    ORDER BY `RKTgl` DESC";
                    break;
                case 'ttdk/stnk-bpkb-update': 
                    $sqlraw = "SELECT ttdk.DKNo
                    FROM    `ttdk`
                    LEFT OUTER JOIN `tmotor` ON    `ttdk`.`DKNo` = `tmotor`.`DKNo`
                    LEFT OUTER JOIN `tdmotortype` ON    `tmotor`.`MotorType` = `tdmotortype`.`MotorType`
                    INNER JOIN `tdcustomer` ON    `ttdk`.`CusKode` = `tdcustomer`.`CusKode`
                    LEFT JOIN `tdsales` ON    `ttdk`.`SalesKode` = `tdsales`.`SalesKode`
                    WHERE ({$post['cmbSearch']} LIKE :nilai) 
                    ORDER BY `DKTgl` DESC";
                    break;
                case 'ttabhd/update': 
                    $sqlraw = "SELECT ttabhd.ABNo
                    FROM    `ttabhd`
                    WHERE  (ttabhd.ABNo NOT LIKE '%=%')  AND ({$post['cmbSearch']} LIKE :nilai) 
                    ORDER BY `ABTgl` DESC";
                    break;
                case 'ttbkhd/pengajuan-bayar-bbn-bank-update': 
                    $sqlraw = "SELECT ttbkhd.BKNo
                    FROM    `ttbkhd`
                    INNER JOIN `tdsupplier` ON  `ttbkhd`.`SupKode` = `tdsupplier`.`SupKode`
                    INNER JOIN `traccount` ON   `ttbkhd`.`NoAccount` = `traccount`.`NoAccount`
                    WHERE   ((ttbkhd.BKJenis LIKE 'Bayar BBN'       AND ttbkhd.BKNo NOT LIKE '%=%'))
                        AND (ttbkhd.BKNo LIKE 'BP%'     AND ttbkhd.BKNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BKTgl` DESC";
                    break;
                case 'ttkk/pengajuan-bayar-bbn-kas-update': 
                    $sqlraw = "SELECT ttkk.KKNo
                    FROM    `ttkk`
                    WHERE   ((ttkk.KKJenis LIKE 'Bayar BBN' AND ttkk.KKNo NOT LIKE '%=%')) AND ((LEFT(ttkk.KKNo, 2) = 'KP'
                    AND ttkk.KKNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KKTgl` DESC";
                    break;
                case 'ttsd/mutasi-eksternal-dealer-update': 
                    $sqlraw = "SELECT ttsd.SDNo
                    FROM    `ttsd`
                    LEFT JOIN `tdlokasi` ON    `ttsd`.`LokasiKode` = `tdlokasi`.`LokasiKode`
                    LEFT JOIN `tddealer` ON    `ttsd`.`DealerKode` = `tddealer`.`DealerKode`
                    WHERE    LEFT(SDNo, 2) = 'ME' AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `SDTgl` DESC";
                    break;
                case 'ttsd/invoice-eksternal-dealer-update': 
                    $sqlraw = "SELECT ttsd.SDNo
                    FROM    `ttsd`
                    INNER JOIN `tdlokasi` ON    `ttsd`.`LokasiKode` = `tdlokasi`.`LokasiKode`
                    INNER JOIN `tddealer` ON    `ttsd`.`DealerKode` = `tddealer`.`DealerKode`
                    WHERE    LEFT(SDNo,    2) = 'ME' AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `SDTgl` DESC";
                    break;
                case 'ttpd/update?MutasiEksternal': 
                    $sqlraw = "SELECT ttpd.PDNo
                    FROM    `ttpd`
                    INNER JOIN `tddealer` ON    `ttpd`.`DealerKode` = `tddealer`.`DealerKode`
                    INNER JOIN `tdlokasi` ON    `ttpd`.`LokasiKode` = `tdlokasi`.`LokasiKode`
                    WHERE    (PDNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `PDTgl` DESC";
                    break;
                case 'ttpd/update?InvoicePenerimaan': 
                    $sqlraw = "SELECT ttpd.PDNo
                    FROM    `ttpd`
                    INNER JOIN `tddealer` ON    `ttpd`.`DealerKode` = `tddealer`.`DealerKode`
                    INNER JOIN `tdlokasi` ON    `ttpd`.`LokasiKode` = `tdlokasi`.`LokasiKode`
                    WHERE    (PDNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `PDTgl` DESC";
                    break;
                case 'ttsmhd/update?MutasiPOS': 
                    $sqlraw = "SELECT ttsmhd.SMNo
                    FROM    `ttsmhd`
                    LEFT OUTER JOIN `tdlokasi` `lasal` ON    `ttsmhd`.`LokasiAsal` = `lasal`.`LokasiKode`
                    LEFT OUTER JOIN `tdlokasi` `ltujuan` ON    `ttsmhd`.`LokasiTujuan` = `ltujuan`.`LokasiKode`
                    WHERE    (LEFT(ttsmhd.SMNo,    2) = 'MI'        AND ttsmhd.SMNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `SMTgl` DESC";
                    break;
                case 'ttpbhd/update': 
                    $sqlraw = "SELECT ttpbhd.PBNo
                    FROM    `ttpbhd`
                    LEFT OUTER JOIN `ttsmhd` ON    `ttpbhd`.`SMNo` = `ttsmhd`.`SMNo`
                    LEFT OUTER JOIN `tdlokasi` `lasal` ON    `ttpbhd`.`LokasiAsal` = `lasal`.`LokasiKode`
                    LEFT OUTER JOIN `tdlokasi` `ltujuan` ON    `ttpbhd`.`LokasiTujuan` = `ltujuan`.`LokasiKode`
                    WHERE    (LEFT(PBNo,    2) = 'PP') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `PBTgl` DESC";
                    break;
                case 'ttsmhd/update?RepairIN': 
                    $sqlraw = "SELECT ttsmhd.SMNo
                    FROM    `ttsmhd`
                    LEFT OUTER JOIN `tdlokasi` `lasal` ON    `ttsmhd`.`LokasiAsal` = `lasal`.`LokasiKode`
                    LEFT OUTER JOIN `tdlokasi` `ltujuan` ON    `ttsmhd`.`LokasiTujuan` = `ltujuan`.`LokasiKode`
                    WHERE    (LEFT(ttsmhd.SMNo,    2) = 'RI' AND ttsmhd.SMNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `SMTgl` DESC";
                    break;
                case 'ttsmhd/update?RepairOUT': 
                    $sqlraw = "SELECT ttsmhd.SMNo
                    FROM    `ttsmhd`
                    LEFT OUTER JOIN `tdlokasi` `lasal` ON    `ttsmhd`.`LokasiAsal` = `lasal`.`LokasiKode`
                    LEFT OUTER JOIN `tdlokasi` `ltujuan` ON    `ttsmhd`.`LokasiTujuan` = `ltujuan`.`LokasiKode`
                    WHERE    LEFT(ttsmhd.SMNo,    2) = 'RO'    AND ttsmhd.SMNo NOT LIKE '%=%' AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `SMTgl` DESC";
                    break;
                case 'ttkm/kas-masuk-inden-update': 
                    $sqlraw = "SELECT ttkm.KMNo
                    FROM `ttkm` 
                    WHERE (ttkm.KMJenis LIKE 'Inden' AND ttkm.KMNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkm/kas-masuk-pelunasan-leasing-update': 
                    $sqlraw = "SELECT ttkm.KMNo
                    FROM `ttkm` 
                    WHERE (ttkm.KMJenis LIKE 'Leasing' AND ttkm.KMNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkm/kas-masuk-uang-muka-kredit-update': 
                    $sqlraw = "SELECT ttkm.KMNo
                    FROM `ttkm` 
                    WHERE (ttkm.KMJenis LIKE 'Kredit' AND ttkm.KMNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkm/kas-masuk-bayar-piutang-kon-um-update': 
                    $sqlraw = "SELECT ttkm.KMNo
                    FROM `ttkm` 
                    WHERE (ttkm.KMJenis LIKE 'Piutang UM' AND ttkm.KMNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkm/kas-masuk-uang-muka-tunai-update': 
                    $sqlraw = "SELECT ttkm.KMNo
                    FROM `ttkm` 
                    WHERE ((ttkm.KMJenis LIKE 'Tunai' AND ttkm.KMNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkm/kas-masuk-bayar-sisa-piutang-update': 
                    $sqlraw = "SELECT ttkm.KMNo
                    FROM `ttkm` 
                    WHERE ((ttkm.KMJenis LIKE 'Piutang Sisa' AND ttkm.KMNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkm/kas-masuk-bbn-acc-update': 
                    $sqlraw = "SELECT ttkm.KMNo
                    FROM `ttkm` 
                    WHERE (ttkm.KMJenis LIKE 'BBN Acc' AND ttkm.KMNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkm/kas-masuk-bbn-progresif-update':
                    $sqlraw = "SELECT ttkm.KMNo
                    FROM `ttkm` 
                    WHERE (ttkm.KMJenis LIKE 'BBN Plus' AND ttkm.KMNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkm/kas-masuk-dari-bank-update': 
                    $sqlraw = "SELECT ttkm.KMNo
                    FROM `ttkm` 
                    WHERE  ((ttkm.KMJenis LIKE 'KM Dari Bank' AND ttkm.KMNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkm/kas-masuk-umum-update': 
                    $sqlraw = "SELECT ttkm.KMNo
                    FROM `ttkm` 
                    WHERE   ((ttkm.KMJenis LIKE 'Umum' AND ttkm.KMNo NOT LIKE '%=%'))  AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkm/kas-masuk-dari-bengkel-update': 
                    $sqlraw = "SELECT ttkm.KMNo
                    FROM `ttkm` 
                    WHERE ((ttkm.KMJenis LIKE 'KM Dari Bengkel' AND ttkm.KMNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkm/kas-masuk-angsuran-karyawan-update': 
                    $sqlraw = "SELECT ttkm.KMNo
                    FROM `ttkm` 
                    WHERE ((ttkm.KMJenis LIKE 'Angsuran Karyawan' AND ttkm.KMNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkm/kas-masuk-pos-dari-dealer-update': 
                    $sqlraw = "SELECT ttkm.KMNo
                    FROM `ttkm` 
                    WHERE ((ttkm.KMJenis LIKE 'KM Pos Dari Dealer' AND ttkm.KMNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkm/kas-masuk-dealer-dari-pos-update': 
                    $sqlraw = "SELECT ttkm.KMNo
                    FROM `ttkm` 
                    WHERE ((ttkm.KMJenis LIKE 'KM Dealer Dari Pos' AND ttkm.KMNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KMTgl` DESC";
                    break;
                case 'ttkk/kas-keluar-subsidi-dealer-2-update': 
                    $sqlraw = "SELECT ttkk.KKNo
                    FROM `ttkk` 
                    WHERE (ttkk.KKJenis LIKE 'SubsidiDealer2' AND ttkk.KKNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KKTgl` DESC";
                    break;
                case 'ttkk/kas-keluar-retur-harga-update': 
                    $sqlraw = "SELECT ttkk.KKNo
                    FROM `ttkk` 
                    WHERE ((ttkk.KKJenis LIKE 'Retur Harga' AND ttkk.KKNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KKTgl` DESC";
                    break;
                case 'ttkk/kas-keluar-insentif-sales-update': 
                    $sqlraw = "SELECT ttkk.KKNo
                    FROM `ttkk` 
                    WHERE ((ttkk.KKJenis LIKE 'Insentif Sales' AND ttkk.KKNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KKTgl` DESC";
                    break;
                case 'ttkk/kas-keluar-potongan-khusus-update': 
                    $sqlraw = "SELECT ttkk.KKNo
                    FROM `ttkk` 
                    WHERE ((ttkk.KKJenis LIKE 'Potongan Khusus' AND ttkk.KKNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KKTgl` DESC";
                    break;
                case 'ttkk/kas-keluar-bayar-hutang-unit-update': 
                    $sqlraw = "SELECT ttkk.KKNo
                    FROM `ttkk` 
                    WHERE (ttkk.KKJenis LIKE 'Bayar Unit' AND ttkk.KKNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KKTgl` DESC";
                    break;
                case 'ttkk/kas-keluar-bayar-hutang-bbn-update': 
                    $sqlraw = "SELECT ttkk.KKNo
                    FROM `ttkk` 
                    WHERE (ttkk.KKJenis LIKE 'Bayar BBN' AND ttkk.KKNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KKTgl` DESC";
                    break;
                case 'ttkk/kas-keluar-bayar-bbn-progresif-update': 
                    $sqlraw = "SELECT ttkk.KKNo
                    FROM `ttkk` 
                    WHERE ((ttkk.KKJenis LIKE 'BBN Plus' AND ttkk.KKNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KKTgl` DESC";
                    break;
                case 'ttkk/kas-keluar-bayar-bbn-acc-update': 
                    $sqlraw = "SELECT ttkk.KKNo
                    FROM `ttkk` 
                    WHERE ((ttkk.KKJenis LIKE 'BBN Acc' AND ttkk.KKNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KKTgl` DESC";
                    break;
                case 'ttkk/kas-keluar-bank-unit-update': 
                    $sqlraw = "SELECT ttkk.KKNo
                    FROM `ttkk` 
                    WHERE ((ttkk.KKJenis LIKE 'KK Ke Bank' AND ttkk.KKNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KKTgl` DESC";
                    break;
                case 'ttkk/kas-keluar-umum-update': 
                    $sqlraw = "SELECT ttkk.KKNo
                    FROM `ttkk` 
                    WHERE ((ttkk.KKJenis LIKE 'Umum' AND ttkk.KKNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KKTgl` DESC";
                    break;
                case 'ttkk/kas-keluar-bank-bengkel-update': 
                    $sqlraw = "SELECT ttkk.KKNo
                    FROM `ttkk` 
                    WHERE ((ttkk.KKJenis LIKE 'KK Ke Bengkel' AND ttkk.KKNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KKTgl` DESC";
                    break;
                case 'ttkk/kas-keluar-pinjaman-karyawan-update': 
                    $sqlraw = "SELECT ttkk.KKNo
                    FROM `ttkk` 
                    WHERE ((ttkk.KKJenis LIKE 'Pinjaman Karyawan' AND ttkk.KKNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KKTgl` DESC";
                    break;
                case 'ttkk/kas-keluar-dealer-pos-update': 
                    $sqlraw = "SELECT ttkk.KKNo
                    FROM `ttkk` 
                    WHERE (ttkk.KKJenis LIKE 'KK Dealer Ke Pos' AND ttkk.KKNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KKTgl` DESC";
                    break;
                case 'ttkk/kas-keluar-pos-dealer-update': 
                    $sqlraw = "SELECT ttkk.KKNo
                    FROM `ttkk` 
                    WHERE ((ttkk.KKJenis LIKE 'KK Pos Ke Dealer' AND ttkk.KKNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KKTgl` DESC";
                    break;
                case 'ttkk/kas-transfer-update': 
                    $sqlraw = "SELECT ttkk.KKLInk
                    FROM ttkk INNER JOIN ttkm ON ttkk.KKLInk = ttkm.KMLink
                    WHERE (ttkk.KKJenis = 'Kas Transfer' AND ttkm.KMJenis = 'Kas Transfer') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `KKTgl` DESC";
                    break;
                case 'ttbmhd/bank-masuk-inden-update': 
                    $sqlraw = "SELECT ttbmhd.BMNo
                    FROM `ttbmhd` INNER JOIN `tdleasing` ON `ttbmhd`.`LeaseKode` = `tdleasing`.`LeaseKode` 
                    INNER JOIN `traccount` ON `ttbmhd`.`NoAccount` = `traccount`.`NoAccount` 
                    WHERE ((ttbmhd.BMJenis LIKE 'Inden' AND ttbmhd.BMNo NOT LIKE '%=%'))  AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BMTgl` DESC";
                    break;
                case 'ttbmhd/bank-masuk-leasing-update': 
                    $sqlraw = "SELECT ttbmhd.BMNo
                    FROM `ttbmhd` INNER JOIN `tdleasing` ON `ttbmhd`.`LeaseKode` = `tdleasing`.`LeaseKode` 
                    INNER JOIN `traccount` ON `ttbmhd`.`NoAccount` = `traccount`.`NoAccount` 
                    WHERE (ttbmhd.BMJenis LIKE 'Leasing' AND ttbmhd.BMNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BMTgl` DESC";
                    break;
                case 'ttbmhd/bank-masuk-scheme-update': 
                    $sqlraw = "SELECT ttbmhd.BMNo
                    FROM `ttbmhd` INNER JOIN `tdleasing` ON `ttbmhd`.`LeaseKode` = `tdleasing`.`LeaseKode` 
                    INNER JOIN `traccount` ON `ttbmhd`.`NoAccount` = `traccount`.`NoAccount` 
                    WHERE (ttbmhd.BMJenis LIKE 'Scheme' AND ttbmhd.BMNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BMTgl` DESC";
                    break;
                case 'ttbmhd/bank-masuk-uang-muka-kredit-update': 
                    $sqlraw = "SELECT ttbmhd.BMNo
                    FROM `ttbmhd` INNER JOIN `tdleasing` ON `ttbmhd`.`LeaseKode` = `tdleasing`.`LeaseKode` 
                    INNER JOIN `traccount` ON `ttbmhd`.`NoAccount` = `traccount`.`NoAccount` 
                    WHERE ((ttbmhd.BMJenis LIKE 'Kredit' AND ttbmhd.BMNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BMTgl` DESC";
                    break;
                case 'ttbmhd/bank-masuk-piutang-kons-um-update': 
                    $sqlraw = "SELECT ttbmhd.BMNo
                    FROM `ttbmhd` INNER JOIN `tdleasing` ON `ttbmhd`.`LeaseKode` = `tdleasing`.`LeaseKode` 
                    INNER JOIN `traccount` ON `ttbmhd`.`NoAccount` = `traccount`.`NoAccount` 
                    WHERE (ttbmhd.BMJenis LIKE 'Piutang UM' AND ttbmhd.BMNo NOT LIKE '%=%')  AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BMTgl` DESC";
                    break;
                case 'ttbmhd/bank-masuk-uang-muka-tunai-update': 
                    $sqlraw = "SELECT ttbmhd.BMNo
                    FROM `ttbmhd` INNER JOIN `tdleasing` ON `ttbmhd`.`LeaseKode` = `tdleasing`.`LeaseKode` 
                    INNER JOIN `traccount` ON `ttbmhd`.`NoAccount` = `traccount`.`NoAccount` 
                    WHERE (ttbmhd.BMJenis LIKE 'Tunai' AND ttbmhd.BMNo NOT LIKE '%=%')  AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BMTgl` DESC";
                    break;
                case 'ttbmhd/bank-masuk-sisa-piutang-kons-update': 
                    $sqlraw = "SELECT ttbmhd.BMNo
                    FROM `ttbmhd` INNER JOIN `tdleasing` ON `ttbmhd`.`LeaseKode` = `tdleasing`.`LeaseKode` 
                    INNER JOIN `traccount` ON `ttbmhd`.`NoAccount` = `traccount`.`NoAccount` 
                    WHERE (ttbmhd.BMJenis LIKE 'Piutang Sisa' AND ttbmhd.BMNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BMTgl` DESC";
                    break;
                case 'ttbmhd/bank-masuk-bbn-progresif-kons-update': 
                    $sqlraw = "SELECT ttbmhd.BMNo
                    FROM `ttbmhd` INNER JOIN `tdleasing` ON `ttbmhd`.`LeaseKode` = `tdleasing`.`LeaseKode` 
                    INNER JOIN `traccount` ON `ttbmhd`.`NoAccount` = `traccount`.`NoAccount` 
                    WHERE (ttbmhd.BMJenis LIKE 'BBN Plus' AND ttbmhd.BMNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BMTgl` DESC";
                    break;
                case 'ttbmhd/bank-masuk-bbn-progresif-update':
                    $sqlraw = "SELECT ttbmhd.BMNo
                    FROM `ttbmhd` INNER JOIN `tdleasing` ON `ttbmhd`.`LeaseKode` = `tdleasing`.`LeaseKode` 
                    INNER JOIN `traccount` ON `ttbmhd`.`NoAccount` = `traccount`.`NoAccount` 
                    WHERE (ttbmhd.BMJenis LIKE 'BBN Plus' AND ttbmhd.BMNo NOT LIKE '%=%') AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BMTgl` DESC";
                    break;
                case 'ttbmhd/bank-masuk-dari-kas-update': 
                    $sqlraw = "SELECT ttbmhd.BMNo
                    FROM `ttbmhd` INNER JOIN `tdleasing` ON `ttbmhd`.`LeaseKode` = `tdleasing`.`LeaseKode` 
                    INNER JOIN `traccount` ON `ttbmhd`.`NoAccount` = `traccount`.`NoAccount` 
                    WHERE ((ttbmhd.BMJenis LIKE 'BM Dari Kas' AND ttbmhd.BMNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BMTgl` DESC";
                    break;
                case 'ttbmhd/bank-masuk-umum-update': 
                    $sqlraw = "SELECT ttbmhd.BMNo
                    FROM `ttbmhd` INNER JOIN `tdleasing` ON `ttbmhd`.`LeaseKode` = `tdleasing`.`LeaseKode` 
                    INNER JOIN `traccount` ON `ttbmhd`.`NoAccount` = `traccount`.`NoAccount` 
                    WHERE ((ttbmhd.BMJenis LIKE 'Umum' AND ttbmhd.BMNo NOT LIKE '%=%'))  AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BMTgl` DESC";
                    break;
                case 'ttbmhd/bank-masuk-penjualan-multi-update': 
                    $sqlraw = "SELECT ttbmhd.BMNo
                    FROM `ttbmhd` INNER JOIN `traccount` ON `ttbmhd`.`NoAccount` = `traccount`.`NoAccount` 
                    WHERE ((ttbmhd.BMJenis LIKE 'Penjualan Multi' AND ttbmhd.BMNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BMTgl` DESC";
                    break;
                case 'ttbkhd/bank-keluar-subsidi-dealer-2-update': 
                    $sqlraw = "SELECT ttbkhd.BKNo
                    FROM `ttbkhd` INNER JOIN `tdsupplier` ON `ttbkhd`.`SupKode` = `tdsupplier`.`SupKode` 
                    INNER JOIN `traccount` ON `ttbkhd`.`NoAccount` = `traccount`.`NoAccount` 
                    WHERE ((ttbkhd.BKJenis LIKE 'SubsidiDealer2' AND ttbkhd.BKNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BKTgl` DESC";
                    break;
                case 'ttbkhd/bank-keluar-retur-harga-update': 
                    $sqlraw = "SELECT ttbkhd.BKNo
                    FROM `ttbkhd` INNER JOIN `tdsupplier` ON `ttbkhd`.`SupKode` = `tdsupplier`.`SupKode` 
                    INNER JOIN `traccount` ON `ttbkhd`.`NoAccount` = `traccount`.`NoAccount` 
                    WHERE  ((ttbkhd.BKJenis LIKE 'Retur Harga' AND ttbkhd.BKNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BKTgl` DESC";
                    break;
                case 'ttbkhd/bank-keluar-insentif-sales-update': 
                    $sqlraw = "SELECT ttbkhd.BKNo
                    FROM `ttbkhd` INNER JOIN `tdsupplier` ON `ttbkhd`.`SupKode` = `tdsupplier`.`SupKode` 
                    INNER JOIN `traccount` ON `ttbkhd`.`NoAccount` = `traccount`.`NoAccount` 
                    WHERE ((ttbkhd.BKJenis LIKE 'Insentif Sales' AND ttbkhd.BKNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BKTgl` DESC";
                    break;
                case 'ttbkhd/bank-keluar-potongan-khusus-update': 
                    $sqlraw = "SELECT ttbkhd.BKNo
                    FROM `ttbkhd` INNER JOIN `tdsupplier` ON `ttbkhd`.`SupKode` = `tdsupplier`.`SupKode` 
                    INNER JOIN `traccount` ON `ttbkhd`.`NoAccount` = `traccount`.`NoAccount` 
                    WHERE ((ttbkhd.BKJenis LIKE 'Potongan Khusus' AND ttbkhd.BKNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BKTgl` DESC";
                    break;
                case 'ttbkhd/bank-keluar-bayar-hutang-unit-update': 
                    $sqlraw = "SELECT ttbkhd.BKNo
                    FROM `ttbkhd` INNER JOIN `tdsupplier` ON `ttbkhd`.`SupKode` = `tdsupplier`.`SupKode` 
                    INNER JOIN `traccount` ON `ttbkhd`.`NoAccount` = `traccount`.`NoAccount` 
                    WHERE ((ttbkhd.BKJenis LIKE 'Bayar Unit' AND ttbkhd.BKNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BKTgl` DESC";
                    break;
                case 'ttbkhd/bank-keluar-bayar-hutang-bbn-update': 
                    $sqlraw = "SELECT ttbkhd.BKNo
                    FROM `ttbkhd` INNER JOIN `tdsupplier` ON `ttbkhd`.`SupKode` = `tdsupplier`.`SupKode` 
                    INNER JOIN `traccount` ON `ttbkhd`.`NoAccount` = `traccount`.`NoAccount` 
                    WHERE ((ttbkhd.BKJenis LIKE 'Bayar BBN' AND ttbkhd.BKNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BKTgl` DESC";
                    break;
                case 'ttbkhd/bank-keluar-bayar-bbn-progresif-update': 
                    $sqlraw = "SELECT ttbkhd.BKNo
                    FROM `ttbkhd` INNER JOIN `tdsupplier` ON `ttbkhd`.`SupKode` = `tdsupplier`.`SupKode` 
                    INNER JOIN `traccount` ON `ttbkhd`.`NoAccount` = `traccount`.`NoAccount` 
                    WHERE ((ttbkhd.BKJenis LIKE 'BBN Plus' AND ttbkhd.BKNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BKTgl` DESC";
                    break;
                case 'ttbkhd/bank-keluar-kas-update': 
                    $sqlraw = "SELECT ttbkhd.BKNo
                    FROM `ttbkhd` INNER JOIN `tdsupplier` ON `ttbkhd`.`SupKode` = `tdsupplier`.`SupKode` 
                    INNER JOIN `traccount` ON `ttbkhd`.`NoAccount` = `traccount`.`NoAccount` 
                    WHERE ((ttbkhd.BKJenis LIKE 'BK Ke Kas' AND ttbkhd.BKNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BKTgl` DESC";
                    break;
                case 'ttbkhd/bank-keluar-umum-update': 
                    $sqlraw = "SELECT ttbkhd.BKNo
                    FROM `ttbkhd` INNER JOIN `tdsupplier` ON `ttbkhd`.`SupKode` = `tdsupplier`.`SupKode` 
                    INNER JOIN `traccount` ON `ttbkhd`.`NoAccount` = `traccount`.`NoAccount` 
                    WHERE ((ttbkhd.BKJenis LIKE 'Umum' AND ttbkhd.BKNo NOT LIKE '%=%')) AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BKTgl` DESC";
                    break;
                case 'ttbkhd/bank-transfer-update': 
                    $sqlraw = "SELECT ttbkit.FBNo
                    FROM ttbkhd 
                     INNER JOIN ttbkit ON ttbkhd.BKNo = ttbkit.BKNo  
                     INNER JOIN ttbmit ON ttbkit.FBNo = ttbmit.DKNo  
                     INNER JOIN ttbmhd ON ttbmhd.BMNo = ttbmit.BMNo  
                     INNER JOIN traccount traccountbm ON ttbmhd.NoAccount = traccountbm.NoAccount  
                     INNER JOIN traccount traccountbk ON ttbkhd.NoAccount = traccountbk.NoAccount  
                     WHERE ttbkhd.BKJenis = 'Bank Transfer' AND ttbmhd.BMJenis = 'Bank Transfer' AND ttbkhd.BKNo NOT LIKE '%=%' AND ttbmhd.BMNo NOT LIKE '%=%' AND ({$post['cmbSearch']} LIKE :nilai)
                    ORDER BY `BKTgl` DESC";
                    break;
                case stristr ($_nav['url'],'ttgeneralledgerhd/'):
                    $sqlraw = "SELECT ttgeneralledgerhd.NoGL FROM `ttgeneralledgerhd` WHERE  ({$post['cmbSearch']} LIKE :nilai)";
                    break;
                case 'trmp/update':
                    $sqlraw = "SELECT trmp.MPNo FROM `trmp` WHERE  ({$post['cmbSearch']} LIKE :nilai)";
                    break;
                case 'traccount/update':
                    $sqlraw = "SELECT traccount.NoAccount FROM `traccount` WHERE  ({$post['cmbSearch']} LIKE :nilai)";
                    break;
                case 'ttamhd/update':
                    $sqlraw = "SELECT ttamhd.AMNo FROM `ttamhd` WHERE  ({$post['cmbSearch']} LIKE :nilai)";
                    break;
            }
            $result = \Yii::$app->db->createCommand( $sqlraw, $params)->queryAll();
            $_nav = [
                'url'=> $post['url'],
                'cmbSearch'=> $post['cmbSearch'],
                'inSearch'=> $post['inSearch'],
                'data'=> $result,
                'linkBack'=> -1,
                'linkNext'=> 1,
                'current'=> 0,
                'total'=> count($result),
            ];
        }
        Yii::$app->session['_navSearch'] = $_nav;
        $this->generateCurrentID();
        return Yii::$app->session['_navSearch'];
    }

    function generateCurrentID(){
        $_nav = Yii::$app->session['_navSearch'];
        $currentUrl = '';
        if(!empty($_nav['data'][$_nav['current']])){
            $currentData = $_nav['data'][$_nav['current']];
            $arrKey = [];
            foreach(self::keyNav[$_nav['url']] as $key){
                $arrKey[] = $currentData[$key];
            }
            $currentUrl = base64_encode(implode( '||', $arrKey));
        }
        $_nav['currentUrl'] = $currentUrl;
        Yii::$app->session['_navSearch'] = $_nav;
    }
}
