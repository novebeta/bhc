<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Tdcustomer;
use aunit\models\Ttabhd;
use aunit\models\Ttabit;
use common\components\Custom;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
/**
 * TtabhdController implements the CRUD actions for Ttabhd model.
 */
class TtabhdController extends AunitController {
	public function actions() {
        $where      = [
            [
                'op'        => 'AND',
                'condition' => "(ttabhd.ABNo NOT LIKE '%=%')",
                'params'    => []
            ]
        ];
		return [
			'td' => [
				'class' => TdAction::class,
				'model' => Ttabhd::class,
                'where'     => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttabhd models.
	 * @return mixed
	 */
	public function actionIndex() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttabhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}

		return $this->render( 'index');
	}
	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
	/**
	 * Finds the Ttabhd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttabhd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttabhd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttabhd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
        TUi::$actionMode   = Menu::ADD;
        $model              = new Ttabhd();
        $model->ABNo        = General::createTemporaryNo( "AB", 'ttabhd', 'ABNo' );
        $model->ABTgl       = date( 'Y-m-d' );
        $model->ABMemo      = '--';
        $model->UserID      = $this->UserID();
        $model->save();
        $post               = $model->attributes;
        $post[ 'Action' ]   = 'Insert';
		$result                 = Ttabhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsJual = Ttabhd::find()->dsTJualFillByNo()->where( [ 'ttabhd.ABNo' => $model->ABNo ] )->asArray( true )->one();
        $id                  = $this->generateIdBase64FromModel( $model );
		$dsJual[ 'ABNoView' ] = $result[ 'ABNo' ];
        return $this->render( 'create', [
            'model'  => $model,
            'dsJual' => $dsJual,
            'id'     => $id
        ] );
	}
	/**
	 * Updates an existing Ttabhd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id, $action ) {
        $index = \yii\helpers\Url::toRoute( [ 'ttabhd/index' ] );
        if ( ! isset( $_GET[ 'action' ] ) ) {
            return $this->redirect( $index );
        }
        $action = $_GET[ 'action' ];
        switch ( $action ) {
            case 'create':
                TUi::$actionMode = Menu::ADD;
                break;
            case 'update':
                TUi::$actionMode = Menu::UPDATE;
                break;
            case 'view':
                TUi::$actionMode = Menu::VIEW;
                break;
            case 'display':
                TUi::$actionMode = Menu::DISPLAY;
                break;
        }
        /** @var Ttabhd $model */
        $model  = $this->findModelBase64( 'Ttabhd', $id );
        $dsJual = Ttabhd::find()->dsTJualFillByNo()->where( [ 'ttabhd.ABNo' => $model->ABNo ] )->asArray( true )->one();
        if ( Yii::$app->request->isPost ) {
            $post               = \Yii::$app->request->post();
            $post[ 'ABNoBaru' ] = $post[ 'ABNo' ];
            $post[ 'Action' ]   = 'Update';
            $result             = Ttabhd::find()->callSP( $post );
	        Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
	        $model              = Ttabhd::findOne( $result[ 'ABNo' ] );
            $id                 = $this->generateIdBase64FromModel( $model );
	        if ( $result[ 'status' ] == 0 ) {
	            $dsBeli[ 'ABNoView' ] = $result[ 'ABNo' ];
                return $this->redirect( [ 'ttabhd/update',  'action' => 'display','id' => $id ] );
            } else {
                return $this->render( 'update', [
                    'model'  => $model,
                    'dsJual' => $post,
                    'id'     => $id,
                ] );
            }
        }
		$dsJual[ 'Action' ] = 'Load';
		$result             = Ttabhd::find()->callSP( $dsJual );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsJual[ 'ABNoView' ] = $_GET['ABNoView'] ?? $result[ 'ABNo' ];
		return $this->render( 'update', [
            'model'  => $model,
            'dsJual' => $dsJual,
            'id'     => $id
        ] );
	}

	public function actionCancel($id, $action)
	{
		/** @var Ttabhd $model */
		$model = $this->findModelBase64( 'Ttabhd', $id );
		$_POST[ 'ABNo' ]   = $model->ABNo;
		$_POST[ 'Action' ] = 'Cancel';
		$result            = Ttabhd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        if($result[ 'ABNo' ] == '--'){
	        return $this->redirect(['index']);
        }else{
        	$model = Ttabhd::findOne($result[ 'ABNo' ]);
	        return $this->redirect( [ 'ttabhd/update',  'action' => 'display','id' => $this->generateIdBase64FromModel( $model ) ] );
        }
	}
	/**
	 * Deletes an existing Ttabhd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */

    public function actionDelete()
    {
        return $this->deleteWithSP(['modelName'=>'Ttabhd']);
        /** @var Ttabhd $model */
        // $request = Yii::$app->request;
        // if ($request->isAjax){
        //     $model = $this->findModelBase64('Ttab', $_POST['id']);
        //     $_POST['ABNo'] = $model->ABNo;
        //     $_POST['Action'] = 'Delete';
        //     $result = Ttabhd::find()->callSP($_POST);
        //     if ($result['status'] == 0) {
        //         return $this->responseSuccess($result['keterangan']);
        //     } else {
        //         return $this->responseFailed($result['keterangan']);
        //     }
        // }else{
        //     $id = $request->get('id');
        //     $model = $this->findModelBase64('Ttabhd', $id);
        //     $_POST['ABNo'] = $model->ABNo;
        //     $_POST['Action'] = 'Delete';
        //     $result = Ttabhd::find()->callSP($_POST);
        //     Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        //     if ($result['ABNo'] == '--') {
        //         return $this->redirect(['index']);
        //     } else {
        //         $model = Ttabhd::findOne($result['ABNo']);
        //         return $this->redirect(['ttabhd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
        //     }
        // }
    }

    public function actionPrint( $id ) {
        unset( $_POST[ '_csrf-app' ] );
        $_POST         = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
        $_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
        $client        = new Client( [
            'headers' => [ 'Content-Type' => 'application/octet-stream' ]
        ] );
        $response      = $client->post( Yii::$app->params[ 'host_report' ] . '/aunit/fabhd', [
            'body' => Json::encode( $_POST )
        ] );
	    $html          = $response->getBody();
	    return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
    }
}
