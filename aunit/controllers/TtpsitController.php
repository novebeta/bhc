<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\models\Ttpohd;
use aunit\models\Ttpshd;
use aunit\models\Ttpsit;
use Yii;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtpsitController implements the CRUD actions for Ttpsit model.
 */
class TtpsitController extends AunitController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttpsit models.
	 * @return mixed
	 */
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttpsit::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
            /** @var Ttpsit $model */
            $model = null;
            if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
                $model = $this->findModelBase64( 'ttpsit', $requestData[ 'id' ] );
            }
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
                        if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
                            $requestData[ 'Action' ] = 'Insert';
                            $requestData[ 'PSAuto' ] = 0;
                        } else {
                            $requestData[ 'Action' ] = 'Update';
                            if ( $model != null ) {
                                $requestData[ 'PSNoOLD' ]   = $model->PSNo;
                                $requestData[ 'PSAutoOLD' ] = $model->PSAuto;
                                $requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
                            }
                        }
                        $result = Ttpsit::find()->callSP( $requestData );
                        if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                            $response = $this->responseSuccess( $result[ 'keterangan' ] );
                        } else {
                            $response = $this->responseFailed( $result[ 'keterangan' ] );
                        }
					break;
                case 'batch' :
	                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	                $respon                      = [];
	                $base64                      = urldecode( base64_decode( $requestData[ 'header' ] ) );
	                parse_str( $base64, $header );
	                /** @var Ttpohd $PO */
	                $PO = Ttpohd::findOne($header[ 'PONo' ]);
	                if($PO != null){
		                $header[ 'POTgl' ] = $PO->POTgl;
	                }
	                $header[ 'Action' ] = 'Loop';
	                $header[ 'Cetak' ] = $requestData['XCetak'];
	                $result             = Ttpshd::find()->callSP( $header );
	                Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
	                foreach ( $requestData[ 'data' ] as $item ) {
		                $item[ 'Action' ]     = 'Insert';
		                $result                    = Ttpsit::find()->callSP( $item );
		                Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
	                }
	                $header[ 'Action' ] = 'LoopAfter';
	                $result             = Ttpshd::find()->callSP( $header );
	                Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
	                return $respon;
                    break;
				case 'del'  :
                    $requestData             = array_merge( $requestData, $model->attributes );
                    $requestData[ 'Action' ] = 'Delete';
                    if ( $model != null ) {
                        $requestData[ 'PSNoLD' ]   = $model->PSNo;
                        $requestData[ 'PSAutoOLD' ] = $model->PSAuto;
                        $requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
                    }
                    $result = Ttpsit::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
					break;
				case 'cekpops' :
					\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
					$rowsCount = Ttpohd::find()->where("PSNo = :PSNo AND PONo <> :PONo",[
						':PSNo' => $requestData['PSNo'],
						':PONo' => $requestData['PONo']
					])->count();
					return [
						'rowsCount' => $rowsCount
					];
					break;
			}
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'ttpsit.' . $primaryKeys[ $i ];
			}
            $jenis = $_GET['jenis'] ?? ' ';
            switch ( $jenis ) {
                case 'FillItemPSPI':
                    $query = (new \yii\db\Query())
                        ->select(new Expression(
                            'CONCAT(' . implode(',"||",', $primaryKeys) . ') AS id,
								ttpsit.PSNo, ttpsit.PSAuto, ttpsit.BrgKode, (ttpsit.PSQty - ttpsit.PIQtyS) AS PSQty, ttpsit.PSHrgBeli, ttpsit.PSHrgJual, ttpsit.PSDiscount, ttpsit.PSPajak, tdbarang.BrgNama, tdbarang.BrgSatuan, 0 AS Disc, 
                                ttpsit.PSQty * ttpsit.PSHrgJual - ttpsit.PSDiscount AS Jumlah, ttpsit.LokasiKode, tdbarang.BrgGroup'))
                        ->from('ttpsit')
                        ->join('INNER JOIN', 'tdbarang', 'ttpsit.BrgKode = tdbarang.BrgKode')
                        ->where("ttpsit.PSNo = :PSNo AND  ttpsit.PSQty > ttpsit.PIQtyS ",[':PSNo' => $requestData['PSNo']])
                        ->orderBy('ttpsit.PSNo DESC');
                    break;
                case 'FillItemPSTI':
                    $query = (new \yii\db\Query())
                        ->select(new Expression(
                            'CONCAT(' . implode(',"||",', $primaryKeys) . ') AS id,
								ttpsit.PSNo, ttpsit.PSAuto, ttpsit.BrgKode, ttpsit.PSQty, ttpsit.PSHrgBeli, ttpsit.PSHrgJual, ttpsit.PSDiscount, ttpsit.PSPajak, tdbarang.BrgNama, tdbarang.BrgSatuan, 0 AS Disc, 
                                ttpsit.PSQty * ttpsit.PSHrgJual - ttpsit.PSDiscount AS Jumlah, ttpsit.LokasiKode, tdbarang.BrgGroup'))
                        ->from('ttpsit')
                        ->join('INNER JOIN', 'tdbarang', 'ttpsit.BrgKode = tdbarang.BrgKode')
                        ->where("ttpsit.PSNo = :PSNo",[':PSNo' => $requestData['PSNo']])
                        ->orderBy('ttpsit.PSNo DESC');
                    break;
                default:
                    $query = (new \yii\db\Query())
                        ->select(new Expression(
                            'CONCAT(' . implode(',"||",', $primaryKeys) . ') AS id,
								ttpsit.PSNo, ttpsit.PSAuto, ttpsit.BrgKode, ttpsit.PSQty, ttpsit.PSHrgBeli, ttpsit.PSHrgJual, 
								ttpsit.PSDiscount, ttpsit.PSPajak, tdbarang.BrgNama, tdbarang.BrgSatuan, 
								IFNULL(ttpsit.PSDiscount / (ttpsit.PSQty * ttpsit.PSHrgJual) * 100, 0) AS Disc, 
								ttpsit.PSQty * ttpsit.PSHrgJual - ttpsit.PSDiscount AS Jumlah, ttpsit.LokasiKode, 
                                tdbarang.BrgGroup, ttpsit.PONo, ttpsit.PINo'))
                        ->from('ttpsit')
                        ->join('INNER JOIN', 'tdbarang', 'ttpsit.BrgKode = tdbarang.BrgKode')
                        ->where(['ttpsit.PSNo' => $requestData['PSNo']])
                        ->orderBy('ttpsit.PSNo, ttpsit.PSAuto');
            }

			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	/**
	 * Creates a new Ttpsit model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Ttpsit();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'PSNo' => $model->PSNo, 'PSAuto' => $model->PSAuto ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Ttpsit model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $PSNo
	 * @param integer $PSAuto
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $PSNo, $PSAuto ) {
		$model = $this->findModel( $PSNo, $PSAuto );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'PSNo' => $model->PSNo, 'PSAuto' => $model->PSAuto ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Finds the Ttpsit model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $PSNo
	 * @param integer $PSAuto
	 *
	 * @return Ttpsit the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $PSNo, $PSAuto ) {
		if ( ( $model = Ttpsit::findOne( [ 'PSNo' => $PSNo, 'PSAuto' => $PSAuto ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Deletes an existing Ttpsit model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $PSNo
	 * @param integer $PSAuto
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $PSNo, $PSAuto ) {
		$this->findModel( $PSNo, $PSAuto )->delete();
		return $this->redirect( [ 'index' ] );
	}
}
