<?php

namespace aunit\controllers;

use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Tdarea;
use Yii;
use aunit\models\Tdmotortype;

use yii\db\Query;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\Custom;

/**
 * TdmotortypeController implements the CRUD actions for Tdmotortype model.
 */
class TdmotortypeController extends AunitController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
	public function actions()
	{
		return [
			'td' => [
				'class' => TdAction::className(),
				'model' => Tdmotortype::className(),
			],
		];
	}

	/**
	 *
	 */
	public function actionFind() {
		$params = \Yii::$app->request->post();
		$rows = [];

		if($params['mode'] === 'number') {
			$rows = Tdmotortype::findOne( $params[ 'id' ] )->toArray();
		}

		return $this->responseDataJson($rows);
	}
    /**
     * Lists all Tdmotortype models.
     * @return mixed
     */
    public function actionIndex()
    {

        return $this->render('index');
    }

    /**
     * Displays a single Tdmotortype model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tdmotortype model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        TUi::$actionMode   = Menu::ADD;
        $model = new Tdmotortype();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect( [ 'tdmotortype/update','id'=> $this->generateIdBase64FromModel($model) ,'action' => 'display' ] );
        }

        return $this->render('create', [
            'model' => $model,
            'id' => 'new',
        ]);
    }

    /**
     * Updates an existing Tdmotortype model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $index = \yii\helpers\Url::toRoute( [ 'tdmotorwarna/index','action' => 'display','#'=>'type' ] );
        if ( ! isset( $_GET[ 'action' ] ) ) {
            return $this->redirect( $index );
        }
        $action = $_GET[ 'action' ];
        switch ( $action ) {
            case 'create':
                TUi::$actionMode = Menu::ADD;
                break;
            case 'update':
                TUi::$actionMode = Menu::UPDATE;
                break;
            case 'view':
                TUi::$actionMode = Menu::VIEW;
                break;
            case 'display':
                TUi::$actionMode = Menu::DISPLAY;
                break;
        }
        $model = $this->findModelBase64('Tdmotortype',$id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash( 'success', 'Berhasil memperbarui data Motor Type' );
            return $this->redirect( [ 'tdmotortype/update','id'=> $this->generateIdBase64FromModel($model),'action' => 'display' ,'#'=>'type'] );
        }

        return $this->render('update', [
            'model' => $model,
            'id'    => $this->generateIdBase64FromModel($model),
        ]);
    }

    /**
     * Deletes an existing Tdmotortype model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
        /** @var Tdmotortype $model */
        $request = Yii::$app->request;
        $model             = $this->findModelBase64( 'Tdmotortype', $id );
        if ( $model->delete() ) {
            if ($request->isAjax) {
                return $this->responseSuccess( 'Berhasil menghapus data');
            }
            $model = Tdmotortype::find()->one();
            return $this->redirect( [ 'tdmotortype/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );

        } else {
            return $this->responseFailed( 'Gagal menghapus data' );
        }
    }

	public function actionMotorTypeList($q = null) {
		$query = new Query;
		$query->select('MotorType')
		      ->from('tdmotortype')
		      ->where('MotorType LIKE "%' . $q .'%"')
		      ->orderBy('MotorType');
		$command = $query->createCommand();
		$data = $command->queryAll();
		$out = [];
		foreach ($data as $d) {
			$out[] = ['value' => $d['MotorType']];
		}
		echo Json::encode($out);
	}

    /**
     * Finds the Tdmotortype model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Tdmotortype the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tdmotortype::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionCancel( $id, $action ) {
	    $model = Tdmotortype::findOne( $id );
	    if ( $model == null ) {
		    $model = Tdmotortype::find()->orderBy( [ 'MotorType' => SORT_ASC ] )->one();
		    if ( $model == null ) {
			    return $this->redirect( [ 'tdmotortype/index' ] );
		    }
	    }
	    return $this->redirect( [ 'tdmotortype/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
    }
}
