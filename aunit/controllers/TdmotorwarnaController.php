<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Tdmotorwarna;

use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TdmotorwarnaController implements the CRUD actions for Tdmotorwarna model.
 */
class TdmotorwarnaController extends AunitController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actions() {
		return [
			'td' => [
				'class' => TdAction::class,
				'model' => Tdmotorwarna::class,
			],
		];
	}
	/**
	 * Lists all Tdmotorwarna models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index');
	}
	/**
	 * Displays a single Tdmotorwarna model.
	 *
	 * @param string $MotorType
	 * @param string $MotorWarna
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $MotorType, $MotorWarna ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $MotorType, $MotorWarna ),
		] );
	}
	/**
	 * Creates a new Tdmotorwarna model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
        TUi::$actionMode   = Menu::ADD;
		$model = new Tdmotorwarna();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'tdmotorwarna/update','id'=> $this->generateIdBase64FromModel($model) ,'action' => 'display' ] );
		}
		return $this->render( 'create', [
			'model' => $model,
			'id' => 'new',
		] );
	}
	/**
	 * Updates an existing Tdmotorwarna model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $MotorType
	 * @param string $MotorWarna
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
        $index = \yii\helpers\Url::toRoute( [ 'tdmotorwarna/index' ] );
        if ( ! isset( $_GET[ 'action' ] ) ) {
            return $this->redirect( $index );
        }
        $action = $_GET[ 'action' ];
        switch ( $action ) {
            case 'create':
                TUi::$actionMode = Menu::ADD;
                break;
            case 'update':
                TUi::$actionMode = Menu::UPDATE;
                break;
            case 'view':
                TUi::$actionMode = Menu::VIEW;
                break;
            case 'display':
                TUi::$actionMode = Menu::DISPLAY;
                break;
        }
		$model = $this->findModelBase64( 'Tdmotorwarna', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'tdmotorwarna/update','id'=> $this->generateIdBase64FromModel($model) ,'action' => 'display' ] );
		}
		return $this->render( 'update', [
			'model' => $model,
            'id'    => $this->generateIdBase64FromModel($model),
		] );
	}
	/**
	 * Deletes an existing Tdmotorwarna model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $MotorType
	 * @param string $MotorWarna
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $MotorType, $MotorWarna ) {
		$this->findModel( $MotorType, $MotorWarna )->delete();
		return $this->redirect( [ 'index' ] );
	}
    /**
     *
     */
    public function actionFind() {
        $params = \Yii::$app->request->post();
        $rows = [];

        if($params['mode'] === 'combo')
        	$value = $params[ 'value' ]??'MotorWarna';
        	$label = $params[ 'label' ]??['MotorWarna'];
        	$condition = $params[ 'condition' ]??'';
        	$params = $params[ 'params' ]??'';
        	$allOption = isset($params[ 'allOption' ]) ? $params[ 'allOption' ] == 'true' : false;
            $rows = Tdmotorwarna::find()->select2( $value, $label, [], [
	            'condition'  => $condition,
	            'params' => $params
            ], $allOption );

        return $this->responseDataJson($rows);
    }
	/**
	 * Finds the Tdmotorwarna model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $MotorType
	 * @param string $MotorWarna
	 *
	 * @return Tdmotorwarna the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $MotorType, $MotorWarna ) {
		if ( ( $model = Tdmotorwarna::findOne( [ 'MotorType' => $MotorType, 'MotorWarna' => $MotorWarna ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
    public function actionCancel( $id, $action ) {
	    $model = Tdmotorwarna::findOne( $id );
	    if ( $model == null ) {
		    $model = Tdmotorwarna::find()->orderBy( [ 'MotorType' => SORT_ASC ] )->one();
		    if ( $model == null ) {
			    return $this->redirect( [ 'tdmotorwarna/index' ] );
		    }
	    }
	    return $this->redirect( [ 'tdmotorwarna/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
    }
}
