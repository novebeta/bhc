<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Attachment;
use aunit\models\Tdsupplier;
use aunit\models\Traccount;
use aunit\models\Ttbkhd;
use aunit\models\Ttbkit;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtbkhdController implements the CRUD actions for Ttbkhd model.
 */
class TtbkhdController extends AunitController {
	public function actions() {
		$colGrid    = null;
		$joinWith   = [
			'supplier' => [
				'type' => 'INNER JOIN'
			],
			'account'  => [
				'type' => 'INNER JOIN'
			],
		];
		$join       = [];
		$where      = [];
		$sqlraw     = null;
		$queryRawPK = null;
		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			switch ( $r ) {
				case 'ttbkhd/bank-keluar-subsidi-dealer-2':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'SubsidiDealer2' AND ttbkhd.BKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-keluar-retur-harga':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'Retur Harga' AND ttbkhd.BKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-keluar-insentif-sales':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'Insentif Sales' AND ttbkhd.BKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-keluar-potongan-khusus':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'Potongan Khusus' AND ttbkhd.BKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-keluar-bayar-hutang-unit':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'Bayar Unit' AND ttbkhd.BKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-keluar-bayar-hutang-bbn':
					$colGrid    = Ttbkhd::colGridBayarHutangBbn();
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'Bayar BBN' AND ttbkhd.BKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-keluar-bayar-bbn-progresif':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'BBN Plus' AND ttbkhd.BKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-keluar-kas':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'BK Ke Kas' AND ttbkhd.BKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-keluar-umum':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'Umum' AND ttbkhd.BKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/bank-transfer':
					$colGrid    = Ttbkhd::colGridBankTransfer();
					$sqlraw     = "SELECT ttbkit.FBNo, ttbkhd.BKTgl,ttbkhd.BKJam, ttbkit.BKNo, ttbmit.BMNo, ttbmhd.BMTgl, ttbkhd.BKNominal, ttbkhd.BKPerson, 
			         ttbkhd.NoAccount AS BKNoAccount, ttbmhd.NoAccount AS BMNoAccount, ttbmhd.BMPerson, traccountbk.NamaAccount AS BMNamaAccount, 
       					traccountbm.NamaAccount AS BKNamaAccount, ttbkhd.UserID, ttbkhd.NoGL AS BKNoGL, ttbmhd.NoGL  AS BMNoGL, ttbkhd.BKCekNo, ttbkhd.BKJenis,
       					ttbkhd.BKAC, ttbkhd.BKCekTempo, ttbkhd.BKMemo
			         FROM ttbkhd 
			         INNER JOIN ttbkit ON ttbkhd.BKNo = ttbkit.BKNo  
			         INNER JOIN ttbmit ON ttbkit.FBNo = ttbmit.DKNo  
			         INNER JOIN ttbmhd ON ttbmhd.BMNo = ttbmit.BMNo  
			         INNER JOIN traccount traccountbm ON ttbmhd.NoAccount = traccountbm.NoAccount  
			         INNER JOIN traccount traccountbk ON ttbkhd.NoAccount = traccountbk.NoAccount  
			         WHERE ttbkhd.BKJenis = 'Bank Transfer' AND ttbmhd.BMJenis = 'Bank Transfer' AND ttbkhd.BKNo NOT LIKE '%=%' AND ttbmhd.BMNo NOT LIKE '%=%'";
					$queryRawPK = [ 'FBNo' ];
					break;
				case 'ttbkhd/pengajuan-bayar-bbn-bank':
					$colGrid = Ttbkhd::colGridPengajuanBayarBBNBank();
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'Bayar BBN' AND ttbkhd.BKNo NOT LIKE '%=%')",
							'params'    => []
						],
						[
							'op'        => 'AND',
							'condition' => "ttbkhd.BKNo LIKE 'BP%' AND ttbkhd.BKNo NOT LIKE '%=%'",
							'params'    => []
						],
					];
					break;
				case 'ttbkhd/kwitansi-bank-bayar-bbn':
//					$colGrid  = Ttbkhd::colGridKwitansiBankBayarBbn();
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbkhd.BKJenis LIKE 'Bayar BBN' AND ttbkhd.BKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbkhd/select-km-dari-bank':
					$colGrid  = Ttbkhd::colGrid();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$KMNo       = $urlParams[ 'KMNo' ];
					$sqlraw     = "SELECT ttbkhd.BKNo, ttbkhd.BKTgl, ttbkhd.BKNominal AS Sisa,  ttbkhd.SupKode,  
			         ttbkhd.BKCekTempo, ttbkhd.NoAccount, ttbkhd.BKMemo, ttbkhd.UserID, 
			         ttbkhd.BKCekNo, ttbkhd.BKPerson,  ttbkhd.BKAC, ttbkhd.BKJenis, 
			         IFNULL(tdsupplier.SupNama, '--') AS SupNama, IFNULL(traccount.NamaAccount, '--') AS NamaAccount 
			         FROM ttbkhd 
			         INNER JOIN tdsupplier ON ttbkhd.SupKode = tdsupplier.SupKode 
			         INNER JOIN traccount ON ttbkhd.NoAccount = traccount.NoAccount 
			         LEFT OUTER JOIN ttkm ON ttbkhd.BKNo = ttkm.KMLink 
			         WHERE (ttbkhd.BKJenis LIKE 'BK ke Kas%') AND (ttkm.KMNo is NULL OR ttkm.KMNo = '{$KMNo}') 
					  {$whereRaw}";
					$queryRawPK = [ 'BKNo' ];
					break;
			}
		}
		return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Ttbkhd::class,
				'queryRaw'   => $sqlraw,
				'queryRawPK' => $queryRawPK,
				'colGrid'    => $colGrid,
				'joinWith'   => $joinWith,
				'join'       => $join,
				'where'      => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttbkhd models.
	 * @return mixed
	 */
	public function actionSelectKmDariBank() {
		$this->layout = 'select-mode';
		return $this->render( 'select', [
			'title'   => 'Bank Keluar',
			'arr'     => [
				'cmbTxt'    => [
					'BKNo'      => 'No Bank Keluar',
					'BKPerson'  => 'Bayar Ke',
					'BKJenis'   => 'Jenis',
					'SupKode'   => 'Kode Sup',
					'NoAccount' => 'Nomor Account',
					'BKCekNo'   => 'Nomor Cek',
					'BKAC'      => 'Nomor A/C',
					'BKMemo'    => 'Keterangan',
					'UserID'    => 'User ID',
					'NoGL'      => 'No Gl', ],
				'cmbTgl'    => [
					'BKTgl' => 'Tgl Bank Keluar',
				],
				'cmbNum'    => [
					'BKNominal' => 'Nominal',
				],
				'sortname'  => "BKTgl",
				'sortorder' => 'desc'
			],
			'options' => [
				'colGrid' => Ttbkhd::colGrid(),
				'mode'    => self::MODE_SELECT,
			]
		] );
	}
	/**
	 * Lists all Ttbkhd models.
	 * @return mixed
	 */
	public function actionBankKeluarSubsidiDealer2() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankKeluarSubsidiDealer2', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbkhd/td' ] )
		] );
	}
	public function actionBankKeluarSubsidiDealer2Create() {
		return $this->create( 'SubsidiDealer2', 'bank-keluar-subsidi-dealer2', 'BANK KELUAR - Subsidi Dealer2' );
	}
	public function actionBankKeluarSubsidiDealer2Update( $id, $action ) {
		return $this->update( 'SubsidiDealer2', $id, 'bank-keluar-subsidi-dealer2', 'BANK KELUAR - Subsidi Dealer2' );
	}
	/**
	 * Lists all Ttbkhd models.
	 * @return mixed
	 */
	public function actionBankKeluarReturHarga() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankKeluarReturHarga', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbkhd/td' ] )
		] );
	}
	public function actionBankKeluarReturHargaCreate() {
		return $this->create( 'Retur Harga', 'bank-keluar-retur-harga', 'BANK KELUAR - Retur Harga' );
	}
	public function actionBankKeluarReturHargaUpdate( $id, $action ) {
		return $this->update( 'Retur Harga', $id, 'bank-keluar-retur-harga', 'BANK KELUAR - Retur Harga' );
	}
	/**
	 * Lists all Ttbkhd models.
	 * @return mixed
	 */
	public function actionBankKeluarInsentifSales() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankKeluarInsentifSales', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbkhd/td' ] )
		] );
	}
	public function actionBankKeluarInsentifSalesCreate() {
		return $this->create( 'Insentif Sales', 'bank-keluar-insentif-sales', 'BANK KELUAR - Insentif Sales' );
	}
	public function actionBankKeluarInsentifSalesUpdate( $id, $action ) {
		return $this->update( 'Insentif Sales', $id, 'bank-keluar-insentif-sales', 'BANK KELUAR - Insentif Sales' );
	}
	/**
	 * Lists all Ttbkhd models.
	 * @return mixed
	 */
	public function actionBankKeluarPotonganKhusus() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankKeluarPotonganKhusus', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbkhd/td' ] )
		] );
	}
	public function actionBankKeluarPotonganKhususCreate() {
		return $this->create( 'Potongan Khusus', 'bank-keluar-potongan-khusus', 'BANK KELUAR - Potongan Khusus' );
	}
	public function actionBankKeluarPotonganKhususUpdate( $id, $action ) {
		return $this->update( 'Potongan Khusus', $id, 'bank-keluar-potongan-khusus', 'BANK KELUAR - Potongan Khusus' );
	}
	/**
	 * Lists all Ttbkhd models.
	 * @return mixed
	 */
	public function actionBankKeluarBayarHutangUnit() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankKeluarBayarHutangUnit', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbkhd/td' ] )
		] );
	}
	public function actionBankKeluarBayarHutangUnitCreate() {
		TUi::$actionMode   = Menu::ADD;
		$model             = new Ttbkhd();
		$model->BKNo       = General::createTemporaryNo( 'BK', 'Ttbkhd', 'BKNo' );
		$model->BKTgl      = date( 'Y-m-d' );
		$model->BKJam      = date( 'Y-m-d H:i:s' );
		$model->BKCekTempo = date( 'Y-m-d' );
		$model->BKNominal  = 0;
		$model->BKMemo     = "--";
		$model->BKAC       = "--";
		$model->BKCekNo    = "--";
		$model->SupKode    = Tdsupplier::findOne( [ 'SupStatus' => 1 ] )->SupKode ?? '--';
		$model->BKJenis    = "Bayar Unit";
		$model->BKPerson   = "--";
		$model->UserID     = $this->UserID();
		$model->NoGL       = "--";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttbkhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttbkhd::find()->ttbkFillByNo()->where( [ 'ttbkhd.BKNo'    => $model->BKNo,
		                                                                  'ttbkhd.BKJenis' => $model->BKJenis ] )->asArray( true )->one();
		$dsTUang[ 'BKTotal' ]  = 0;
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'BKNoView' ] = $result[ 'BKNo' ];
		return $this->render( 'BankKeluarBayarHutangUnitCreate', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id,
		] );
	}
	public function actionBankKeluarBayarHutangUnitUpdate( $id, $action ) {
		$index = Ttbkhd::getRoute( 'Bayar Unit', [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttbkhd $model */
		$model   = $this->findModelBase64( 'Ttbkhd', $id );
		$dsTUang = Ttbkhd::find()->ttbkFillByNo()->where( [ 'ttbkhd.BKNo'    => $model->BKNo,
		                                                    'ttbkhd.BKJenis' => $model->BKJenis ] )->asArray( true )->one();
		$id      = $this->generateIdBase64FromModel( $model );
		if ( Yii::$app->request->isPost ) {
			$post             = \Yii::$app->request->post();
			$post[ 'BKJam' ]  = $post[ 'BKTgl' ] . ' ' . $post[ 'BKJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttbkhd::findOne( $result[ 'BKNo' ] );
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'BKNoView' ] = $result[ 'BKNo' ];
			if ( $result[ 'status' ] == 0 ) {
				return $this->redirect( Ttbkhd::getRoute( 'Bayar Unit', [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'BankKeluarBayarHutangUnitUpdate', [
					'model'   => $model,
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
		$dsTUang[ 'BKTotal' ] = 0;
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttbkhd::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'BKNo' ] = $dsTUang[ 'BKNo' ];
		}
		$dsTUang[ 'BKNoView' ] = $_GET[ 'BKNoView' ] ?? $result[ 'BKNo' ];
		return $this->render( 'BankKeluarBayarHutangUnitUpdate', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id,
		] );
//        return $this->update($id, $action, 'BANK KELUAR - Bayar Hutang Unit');
	}
	/**
	 * Lists all Ttbkhd models.
	 * @return mixed
	 */
	public function actionBankKeluarBayarHutangBbn() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankKeluarBayarHutangBbn', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbkhd/td' ] )
		] );
	}
	public function actionBankKeluarBayarHutangBbnCreate() {
		return $this->create( 'Bayar BBN', 'bank-keluar-bayar-hutang-bbn', 'BANK KELUAR - Bayar Hutang BBN' );
	}
	public function actionBankKeluarBayarHutangBbnUpdate( $id, $action ) {
		return $this->update( 'Bayar BBN', $id, 'bank-keluar-bayar-hutang-bbn', 'BANK KELUAR - Bayar Hutang Bbn' );
	}
	/**
	 * Lists all Ttbkhd models.
	 * @return mixed
	 */
	public function actionBankKeluarBayarBbnProgresif() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankKeluarBayarBbnProgresif', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbkhd/td' ] )
		] );
	}
	public function actionBankKeluarBayarBbnProgresifCreate() {
		return $this->create( 'BBN Plus', 'bank-keluar-bayar-bbn-progresif', 'BANK KELUAR - Bayar BBN Progresif' );
	}
	public function actionBankKeluarBayarBbnProgresifUpdate( $id, $action ) {
		return $this->update( 'BBN Plus', $id, 'bank-keluar-bayar-bbn-progresif', 'BANK KELUAR - Bayar BBN Progresif' );
	}
	/**
	 * Lists all Ttbkhd models.
	 * @return mixed
	 */
	public function actionBankKeluarKas() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankKeluarKas', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbkhd/td' ] )
		] );
	}
	public function actionBankKeluarKasCreate() {
		TUi::$actionMode   = Menu::ADD;
		$model             = new Ttbkhd();
		$model->BKNo       = General::createTemporaryNo( 'BK', 'Ttbkhd', 'BKNo' );
		$model->BKTgl      = date( 'Y-m-d' );
		$model->BKJam      = date( 'Y-m-d H:i:s' );
		$model->BKCekTempo = date( 'Y-m-d' );
		$model->BKNominal  = 0;
		$model->BKMemo     = "--";
		$model->BKAC       = "--";
		$model->BKCekNo    = "--";
		$model->BKJenis    = "BK Ke Kas";
		$model->SupKode    = ( Tdsupplier::find()->where( [ 'SupStatus' => 1 ] )->one() )->SupKode;
		$model->BKPerson   = "--";
		$model->UserID     = $this->UserID();
		$model->NoGL       = "--";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttbkhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttbkhd::find()->ttbkFillByNo()->where( [ 'ttbkhd.BKNo'    => $model->BKNo,
		                                                                  'ttbkhd.BKJenis' => $model->BKJenis ] )->asArray( true )->one();
		$dsTUang[ 'BKTotal' ]  = 0;
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'BKNoView' ] = $result[ 'BKNo' ];
		return $this->render( 'bank-keluar-kas-create', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id,
		] );
	}
	public function actionBankKeluarKasUpdate( $id ) {
		$index = Ttbkhd::getRoute( 'BK Ke Kas', [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttbkhd $model */
		$model   = $this->findModelBase64( 'Ttbkhd', $id );
		$dsTUang = Ttbkhd::find()->ttbkFillByNo()->where( [ 'ttbkhd.BKNo'    => $model->BKNo,
		                                                    'ttbkhd.BKJenis' => $model->BKJenis ] )->asArray( true )->one();
		$id      = $this->generateIdBase64FromModel( $model );
		if ( Yii::$app->request->isPost ) {
			$post             = \Yii::$app->request->post();
			$post[ 'BKJam' ]  = $post[ 'BKTgl' ] . ' ' . $post[ 'BKJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttbkhd::findOne( $result[ 'BKNo' ] );
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'BKNoView' ] = $result[ 'BKNo' ];
			if ( $result[ 'status' ] == 0 ) {
				return $this->redirect( Ttbkhd::getRoute( 'BK Ke Kas', [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'bank-keluar-kas-update', [
					'model'   => $model,
					'dsTUang' => $dsTUang,
					'id'      => $id,
				] );
			}
		}
		$dsTUang[ 'BKTotal' ] = 0;
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttbkhd::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'BKNo' ] = $dsTUang[ 'BKNo' ];
		}
		$dsTUang[ 'BKNoView' ] = $_GET[ 'BKNoView' ] ?? $result[ 'BKNo' ];
		return $this->render( 'bank-keluar-kas-update', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id,
		] );
	}
	/**
	 * Lists all Ttbkhd models.
	 * @return mixed
	 */
	public function actionBankKeluarUmum() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankKeluarUmum', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbkhd/td' ] )
		] );
	}
	public function actionBankKeluarUmumCreate() {
		TUi::$actionMode   = Menu::ADD;
		$model             = new Ttbkhd();
		$model->BKNo       = General::createTemporaryNo( 'BK', 'Ttbkhd', 'BKNo' );
		$model->BKTgl      = date( 'Y-m-d' );
		$model->BKCekTempo = date( 'Y-m-d' );
		$model->BKJam      = date( 'Y-m-d H:i:s' );
		$model->BKNominal  = 0;
		$model->BKMemo     = "--";
		$model->BKAC       = "--";
		$model->BKCekNo    = "--";
		$model->SupKode    = ( Tdsupplier::find()->where( [ 'SupStatus' => 1 ] )->one() )->SupKode;
		$model->BKJenis    = "Umum";
		$model->BKPerson   = "--";
		$model->UserID     = $this->UserID();
		$model->NoGL       = "--";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttbkhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttbkhd::find()->ttbkFillByNo()->where( [ 'ttbkhd.BKNo'    => $model->BKNo,
		                                                                  'ttbkhd.BKJenis' => $model->BKJenis ] )->asArray( true )->one();
		$dsTUang[ 'BKTotal' ]  = 0;
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'BKNoView' ] = $result[ 'BKNo' ];
		return $this->render( 'bank-keluar-umum-create', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id,
		] );
	}
	public function actionBankKeluarUmumUpdate( $id ) {
		$index = Ttbkhd::getRoute( 'Umum', [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttbkhd $model */
		$model   = $this->findModelBase64( 'Ttbkhd', $id );
		$dsTUang = Ttbkhd::find()->ttbkFillByNo()->where( [ 'ttbkhd.BKNo'    => $model->BKNo,
		                                                    'ttbkhd.BKJenis' => $model->BKJenis ] )->asArray( true )->one();
		$id      = $this->generateIdBase64FromModel( $model );
		if ( Yii::$app->request->isPost ) {
			$post             = \Yii::$app->request->post();
			$post[ 'BKJam' ]  = $post[ 'BKTgl' ] . ' ' . $post[ 'BKJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttbkhd::findOne( $result[ 'BKNo' ] );
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'BKNoView' ] = $result[ 'BKNo' ];
			if ( $result[ 'status' ] == 0 ) {
				return $this->redirect( Ttbkhd::getRoute( 'Umum', [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'bank-keluar-umum-update', [
					'model'   => $model,
					'dsTUang' => $dsTUang,
					'id'      => $id,
				] );
			}
		}
		$dsTUang[ 'SubTotal' ] = 0;
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttbkhd::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'BKNo' ] = $dsTUang[ 'BKNo' ];
		}
		$dsTUang[ 'BKNoView' ] = $_GET[ 'BKNoView' ] ?? $result[ 'BKNo' ];
		return $this->render( 'bank-keluar-umum-update', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id,
		] );
	}
	/**
	 * Lists all Ttbkhd models.
	 * @return mixed
	 */
	public function actionBankTransfer() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbkhd::find()->callSPTransfer( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankTransfer', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbkhd/td' ] )
		] );
	}
	public function actionBankTransferCreate() {
		TUi::$actionMode          = Menu::ADD;
		$BankKode                 = Traccount::find()->where( "LEFT(NoAccount,5) = '11020' AND JenisAccount = 'Detail'" )
		                                     ->orderBy( 'NoAccount' )->one();
		$dsTUang[ 'BTTgl' ]       = date( 'Y-m-d H:i:s' );
		$dsTUang[ 'BKTgl' ]       = date( 'Y-m-d H:i:s' );
		$dsTUang[ 'BMTgl' ]       = date( 'Y-m-d H:i:s' );
		$dsTUang[ 'BKCekTempo' ]  = date( 'Y-m-d H:i:s' );
		$dsTUang[ 'BMCekTempo' ]  = date( 'Y-m-d H:i:s' );
		$dsTUang[ 'BKJenis' ]     = 'Bank Transfer';
		$dsTUang[ 'BMJenis' ]     = 'Bank Transfer';
		$dsTUang[ 'KodeTrans' ]   = 'BT';
		$dsTUang[ 'BKNoGL' ]      = '--';
		$dsTUang[ 'BMNoGL' ]      = '--';
		$dsTUang[ 'BKPerson' ]    = '--';
		$dsTUang[ 'BMPerson' ]    = '--';
		$dsTUang[ 'BKMemo' ]      = '--';
		$dsTUang[ 'BKAC' ]        = '--';
		$dsTUang[ 'BMAC' ]        = '--';
		$dsTUang[ 'BKCekNo' ]     = '--';
		$dsTUang[ 'BMCekNo' ]     = '--';
		$dsTUang[ 'BKNominal' ]   = 0;
		$dsTUang[ 'BKNoAccount' ] = ( $BankKode != null ) ? $BankKode->NoAccount : '';
		$dsTUang[ 'BMNoAccount' ] = ( $BankKode != null ) ? $BankKode->NoAccount : '';
		$dsTUang[ 'FBNo' ]        = '--';
		$dsTUang[ 'DKNo' ]        = '--';
		$dsTUang[ 'BKNo' ]        = '--';
		$dsTUang[ 'BMNo' ]        = '--';
		$dsTUang[ 'Action' ]      = 'Insert';
		$result                   = Ttbkhd::find()->callSPTransfer( $dsTUang );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang[ 'BTNo' ] = $result[ 'BTNo' ];
		return $this->render( 'bank-transfer-create', [
			'dsTUang' => $dsTUang,
			'id'      => base64_encode( $dsTUang[ 'BKNo' ] ),
			'title'   => 'Bank Transfer',
		] );
	}
	public function actionBankTransferUpdate( $id, $action ) {
		$index = Ttbkhd::getRoute( 'Bank Transfer', [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'UpdateAdd':
				TUi::$actionMode = Menu::ADD;
				break;
			case 'UpdateEdit':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}

		$dsTUang           = Ttbkhd::find()->transferBank( base64_decode( $id ) );
		$dsTUang[ 'BTNo' ] = base64_decode( $id );
		if ( Yii::$app->request->isPost ) {
			$post             = array_merge( $dsTUang ?? [], \Yii::$app->request->post() );
			$post[ 'BTJam' ]  = $post[ 'BTTgl' ] . ' ' . $post[ 'BTJam' ];
			$post[ 'Action' ] = $action;
			$result           = TtBKhd::find()->callSPTransfer( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$post[ 'BTNo' ] = $result[ 'BTNo' ];
			$id             = base64_encode( $result[ 'BTNo' ] );
			if ( $result[ 'status' ] == 0 ) {
				return $this->redirect( Ttbkhd::getRoute( 'Bank Transfer', [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'bank-transfer-update', [
					'dsTUang' => $post,
					'id'      => $id,
					'title'   => 'Bank Transfer',
				] );
			}
		}
		$dsTUang[ 'Action' ] = 'Load';
		$result              = Ttbkhd::find()->callSPTransfer( $dsTUang );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->render( 'bank-transfer-update', [
			'dsTUang' => $dsTUang,
			'id'      => $id,
			'title'   => 'Bank Transfer',
		] );
	}
	public function actionBankTransferCancel( $id ) {
		$post             = \Yii::$app->request->post();
		$post[ 'BTJam' ]  = $post[ 'BTTgl' ] . ' ' . $post[ 'BTJam' ];
		$post[ 'Action' ] = 'Cancel';
		$result           = Ttbkhd::find()->callSPTransfer( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'BTNo' ] == '--' ) {
			return $this->redirect( [ 'ttbkhd/bank-transfer' ] );
		} else {
			return $this->redirect( Ttbkhd::getRoute( 'Bank Transfer', [ 'action' => 'display', 'id' => base64_encode( $result[ 'BTNo' ] ) ] )[ 'update' ] );
		}
	}
	public function actionPengajuanBayarBbnBank() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbkhd::find()->callSPPengajuan( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'PengajuanBayarBBNBank', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbkhd/td' ] )
		] );
	}
	public function actionPengajuanBayarBbnBankCreate() {
		TUi::$actionMode   = Menu::ADD;
		$model             = new Ttbkhd();
		$model->BKNo       = General::createTemporaryNo( 'BK', 'Ttbkhd', 'BKNo' );
		$model->BKTgl      = date( 'Y-m-d H:i:s' );
		$model->BKJam      = date( 'Y-m-d H:i:s' );
		$model->BKCekTempo = date( 'Y-m-d' );
		$model->BKNominal  = 0;
		$model->BKMemo     = "--";
		$model->BKAC       = "--";
		$model->BKCekNo    = "--";
		$model->BKJenis    = "Bayar BBN";
		$model->SupKode    = ( Tdsupplier::find()->where( [ 'SupStatus' => 1 ] )->one() )->SupKode;
		$model->BKPerson   = "--";
		$model->UserID     = $this->UserID();
		$model->NoGL       = "--";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttbkhd::find()->callSPPengajuan( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttbkhd::find()->ttbkFillByNo()->where( [ 'ttbkhd.BKNo'    => $model->BKNo,
		                                                                  'ttbkhd.BKJenis' => $model->BKJenis ] )->asArray( true )->one();
		$dsTUang[ 'BKTotal' ]  = 0;
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'BKNoView' ] = $result[ 'BKNo' ];
		return $this->render( 'pengajuan-bayar-bbn-bank-create', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id,
		] );
	}
	public function actionPengajuanBayarBbnBankUpdate( $id ) {
		$index = Ttbkhd::getRoute( 'Bayar BBN', [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttbkhd $model */
		$model   = $this->findModelBase64( 'Ttbkhd', $id );
		$dsTUang = Ttbkhd::find()->ttbkFillByNo()->where( [ 'ttbkhd.BKNo'    => $model->BKNo,
		                                                    'ttbkhd.BKJenis' => $model->BKJenis ] )->asArray( true )->one();
		$id      = $this->generateIdBase64FromModel( $model );
		if ( Yii::$app->request->isPost ) {
			$post             = \Yii::$app->request->post();
			$post[ 'BKJam' ]  = $post[ 'BKTgl' ] . ' ' . $post[ 'BKJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttbkhd::find()->callSPPengajuan( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model = Ttbkhd::findOne( $result[ 'BKNo' ] );
			$id    = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 ) {
				$post[ 'BKNoView' ] = $result[ 'BKNo' ];
//				return $this->redirect( Ttbkhd::getRoute( 'Bayar BBN', [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
				return $this->redirect( [ 'ttbkhd/pengajuan-bayar-bbn-bank-update', 'action' => 'display', 'id' => $id ] );
			} else {
				return $this->render( 'pengajuan-bayar-bbn-bank-update', [
					'model'   => $model,
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
		$dsTUang[ 'BKTotal' ] = 0;
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttbkhd::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'BKNo' ] = $dsTUang[ 'BKNo' ];
		}
		$dsTUang[ 'BKNoView' ] = $_GET[ 'BKNoView' ] ?? $result[ 'BKNo' ];
		return $this->render( 'pengajuan-bayar-bbn-bank-update', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id,
		] );
	}
	public function actionKwitansiBankBayarBbn() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KwitansiBankBayarBbn', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbkhd/td' ] )
		] );
	}
	/**
	 * Deletes an existing Ttbkhd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete() {
		/** @var Ttbkhd $model */
		$request = Yii::$app->request;
		$PK         = base64_decode( $_REQUEST[ 'id' ] );
		$model      = null;
		$isTransfer = false;
		if ( strpos( $PK, 'BT' ) !== false ) {
			$details = Ttbkit::findOne( [ 'FBNo' => $PK ] );
			$PK      = $details->BKNo;
		}
		$model             = Ttbkhd::findOne( $PK );
		$_POST[ 'BKNo' ]   = $model->BKNo;
		$_POST[ 'Action' ] = 'Delete';
		$BKJenis = $model->BKJenis;
		if ( isset( $_GET[ 'JenisBBN' ] ) && $_GET[ 'JenisBBN' ] == 'Pengajuan' ) {
			$result = Ttbkhd::find()->callSPPengajuan( $_POST );
		} else {
			if ( $isTransfer ) {
				$result = Ttbkhd::find()->callSPTransfer( $_POST );
			} else {
				$result = Ttbkhd::find()->callSP( $_POST );
			}
		}
		if ($request->isAjax){
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['BKNo'] == '--') {
                if ( isset( $_GET[ 'JenisBBN' ] ) && $_GET[ 'JenisBBN' ] == 'Pengajuan' ) {
					return $this->redirect( [ 'ttbkhd/pengajuan-bayar-bbn-bank' ] );
				}
				return $this->redirect( Ttbkhd::getRoute( $BKJenis )[ 'index' ] );
            } else {
                $model = Ttbkhd::findOne($result['BKNo']);
				if(empty($model)){
					if ( isset( $_GET[ 'JenisBBN' ] ) && $_GET[ 'JenisBBN' ] == 'Pengajuan' ) {
						$model = Ttbkhd::find()
						->where(['BKJenis' => 'Bayar BBN'])
						->andWhere(['like', 'BKNo','BP%',false])
						->andWhere(['not like', 'BKNo','=',])
						->one();
					}else{
						$model = Ttbkhd::find()->where(['BKJenis' => $BKJenis])->andWhere(['not like', 'BKNo','=',])->one();
					}
				}
				if(empty($model)){
					if ( isset( $_GET[ 'JenisBBN' ] ) && $_GET[ 'JenisBBN' ] == 'Pengajuan' ) {
						return $this->redirect( [ 'ttbkhd/pengajuan-bayar-bbn-bank' ] );
					}
					return $this->redirect( Ttbkhd::getRoute( $BKJenis )[ 'index' ] );
				}
                if ( isset( $_GET[ 'JenisBBN' ] ) && $_GET[ 'JenisBBN' ] == 'Pengajuan' ) {
					return $this->redirect( [ 'ttbkhd/pengajuan-bayar-bbn-bank-update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
				}
				return $this->redirect( Ttbkhd::getRoute( $BKJenis, [ 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] )[ 'update' ] );
            }
        }
	}
	/**
	 * Finds the Ttbkhd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttbkhd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttbkhd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	private function create( $BKJenis, $view, $title ) {
		TUi::$actionMode   = Menu::ADD;
		$model             = new Ttbkhd();
		$model->BKNo       = General::createTemporaryNo( 'BK', 'Ttbkhd', 'BKNo' );
		$model->BKTgl      = date( 'Y-m-d' );
		$model->BKCekTempo = date( 'Y-m-d' );
		$model->BKJam      = date( 'Y-m-d H:i:s' );
		$model->BKNominal  = floatval( $_GET[ 'BKNominal' ] ?? 0 );
		$model->BKMemo     = $_GET[ 'BKMemo' ] ?? "--";
		$model->BKPerson   = $_GET[ 'BKPerson' ] ?? "--";
		$model->SupKode    = $_GET[ 'SupKode' ] ?? "--";
		$model->NoAccount  = $_GET[ 'NoAccount' ] ?? null;
		$model->BKJenis    = $BKJenis;
		$model->UserID     = $this->UserID();
		$model->BKAC       = "--";
		$model->BKCekNo    = "--";
		$model->NoGL       = "--";
		$model->save();
		$model->refresh();
		if ( isset( $_GET[ 'FBNo' ] ) && $_GET[ 'FBNo' ] != '' ) {
			$item          = new Ttbkit();
			$item->BKNo    = $model->BKNo;
			$item->BKBayar = $model->BKNominal;
			$item->FBNo    = $_GET[ 'FBNo' ];
			$item->save();
		}
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttbkhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttbkhd::find()->ttbkFillByNo()->where( [ 'ttbkhd.BKNo'    => $model->BKNo,
		                                                                  'ttbkhd.BKJenis' => $model->BKJenis ] )->asArray( true )->one();
		$dsTUang[ 'BKTotal' ]  = 0;
		$dsTUang[ 'BKNoView' ] = $result[ 'BKNo' ];
		$id                    = $this->generateIdBase64FromModel( $model );
		return $this->render( 'create', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id,
			'view'    => $view,
			'title'   => $title
		] );
	}
	private function update( $BKJenis, $id, $view, $title ) {
		$index = Ttbkhd::getRoute( $BKJenis, [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttbkhd $model */
		$model   = $this->findModelBase64( 'Ttbkhd', $id );
		$dsTUang = Ttbkhd::find()->ttbkFillByNo()->where( [ 'ttbkhd.BKNo'    => $model->BKNo,
		                                                    'ttbkhd.BKJenis' => $model->BKJenis ] )->asArray( true )->one();
		$id      = $this->generateIdBase64FromModel( $model );
		if ( Yii::$app->request->isPost ) {
			$post             = \Yii::$app->request->post();
			$post[ 'UserID' ] = $this->UserID();
			$post[ 'BKJam' ]  = $post[ 'BKTgl' ] . ' ' . $post[ 'BKJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttbkhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttbkhd::findOne( $result[ 'BKNo' ] );
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'BKNoView' ] = $result[ 'BKNo' ];
			if ( $result[ 'status' ] == 0 ) {
				return $this->redirect( Ttbkhd::getRoute( $BKJenis, [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'update', [
					'model'     => $model,
					'dsTUang'   => $post,
					'id'        => $id,
					'viewIndex' => $view,
					'view'      => $view,
					'title'     => $title
				] );
			}
		}
		$dsTUang[ 'BKTotal' ] = 0;
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttbkhd::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'BKNo' ] = $dsTUang[ 'BKNo' ];
		}
		$dsTUang[ 'BKNoView' ] = $_GET[ 'BKNoView' ] ?? $result[ 'BKNo' ];
		return $this->render( 'update', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id,
			'view'    => $view,
			'title'   => $title
		] );
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client            = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response          = $client->post( Yii::$app->params[ 'host_report' ] . '/aunit/fbkhd2', [
			'body' => Json::encode( $_POST )
		] );
		return Yii::$app->response->sendContentAsFile( $response->getBody(), 'fbkhd2.pdf', [
			'inline'   => true,
			'mimeType' => 'application/pdf'
		] );
	}
	public function actionKonfirmPengajuan( $id ) {
		/** @var  $model Ttbkhd */
		$model            = $this->findModelBase64( 'Ttbkhd', $id );
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Konfirm';
		$post[ 'BKJam' ]  = $model->BKJam;
		$result           = Ttbkhd::find()->callSPPengajuan( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( [ 'ttbkhd/pengajuan-bayar-bbn-bank-update', 'action' => 'display', 'id' => base64_encode( $result[ 'BKNo' ] ) ] );
	}
	public function actionCancel( $id, $action ) {
		/** @var  $model Ttbkhd */
		$model = $this->findModelBase64( 'Ttbkhd', $id );
		if ( $model == null ) {
			$BKJenis = $_GET[ 'BKJenis' ];
			return $this->redirect( Ttbkhd::getRoute( $BKJenis )[ 'index' ] );
		} else {
			$BKJenis = $model->BKJenis;
		}
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Cancel';
		$post[ 'BKNo' ]   = $model->BKNo;
		$post[ 'BKJam' ]  = $model->BKJam;
		if ( isset( $_GET[ 'JenisBBN' ] ) && $_GET[ 'JenisBBN' ] == 'Pengajuan' ) {
			$result = Ttbkhd::find()->callSPPengajuan( $post );
		} else {
			$result = Ttbkhd::find()->callSP( $post );
		}
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'BKNo' ] == '--' ) {
			if ( isset( $_GET[ 'JenisBBN' ] ) && $_GET[ 'JenisBBN' ] == 'Pengajuan' ) {
				return $this->redirect( [ 'ttbkhd/pengajuan-bayar-bbn-bank' ] );
			}
			return $this->redirect( Ttbkhd::getRoute( $BKJenis )[ 'index' ] );
		} else {
			$model = Ttbkhd::findOne( $result[ 'BKNo' ] );
			if ( isset( $_GET[ 'JenisBBN' ] ) && $_GET[ 'JenisBBN' ] == 'Pengajuan' ) {
				return $this->redirect( [ 'ttbkhd/pengajuan-bayar-bbn-bank-update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
			}
			return $this->redirect( Ttbkhd::getRoute( $BKJenis, [ 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] )[ 'update' ] );
		}
	}
	public function actionJurnal() {
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Jurnal';
		$post[ 'BKJam' ]  = $post[ 'BKTgl' ] . ' ' . $post[ 'BKJam' ];
		$result           = Ttbkhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( Ttbkhd::getRoute( $post[ 'BKJenis' ], [ 'action' => 'display', 'id' => base64_encode( $result[ 'BKNo' ] ) ] )[ 'update' ] );
	}

	public function actionLampiran($id){
		/** @var  $model Ttbkhd */
		$model = $this->findModelBase64( 'Ttbkhd', $id );
		$attach = new Attachment();
		$attach->table = 'ttbkhd';
		$attach->tableId = $model->BKNo;
		$attach->titleId = $model->BKNo;
		$attach->model = $model;
		$attach->redirectUrl = Ttbkhd::getRoute( $model->BKJenis, [ 'action' => 'display', 'id' => $id])[ 'update' ];
		return $this->redirect($attach->getRedirectUrl());
	}
}
