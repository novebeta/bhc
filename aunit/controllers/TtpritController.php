<?php

namespace aunit\controllers;

use aunit\components\AunitController;
use aunit\models\Ttprit;
use Yii;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtpritController implements the CRUD actions for Ttprit model.
 */
class TtpritController extends AunitController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ttprit models.
     * @return mixed
     */
    public function actionIndex()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $requestData                 = \Yii::$app->request->post();
        $primaryKeys                 = Ttprit::getTableSchema()->primaryKey;
        if ( isset( $requestData[ 'oper' ] ) ) {
            /* operation : create, edit or delete */
            $oper = $requestData[ 'oper' ];
            /** @var Ttprit $model */
            $model = null;
            if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
                $model = $this->findModelBase64( 'ttprit', $requestData[ 'id' ] );
            }
            switch ( $oper ) {
                case 'add'  :
                case 'edit' :
                    if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
                        $requestData[ 'Action' ] = 'Insert';
                        $requestData[ 'PRAuto' ] = 0;
                    } else {
                        $requestData[ 'Action' ] = 'Update';
                        if ( $model != null ) {
                            $requestData[ 'PRNoOLD' ]   = $model->PRNo;
                            $requestData[ 'PRAutoOLD' ] = $model->PRAuto;
                            $requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
                        }
                    }
                    $result = Ttprit::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
                break;
                case 'batch' :
                    break;
                case 'del'  :
                    $requestData             = array_merge( $requestData, $model->attributes );
                    $requestData[ 'Action' ] = 'Delete';
                    if ( $model != null ) {
                        $requestData[ 'PRNoLD' ]   = $model->PRNo;
                        $requestData[ 'PRAutoOLD' ] = $model->PRAuto;
                        $requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
                    }
                    $result = Ttprit::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
                    break;
            }
        } else {
            for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
                $primaryKeys[ $i ] = 'Ttprit.' . $primaryKeys[ $i ];
            }
            $query     = ( new \yii\db\Query() )
                ->select( new Expression(
                    'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
							    	ttprit.PRNo,
                                    ttprit.PRAuto,
                                    ttprit.BrgKode,
                                    ttprit.PRQty,
                                    ttprit.PRHrgBeli,
                                    ttprit.PRHrgJual,
                                    ttprit.PRDiscount,
                                    ttprit.PRPajak,
                                    IFNULL(
                                        ttprit.PRDiscount / (
                                            ttprit.PRQty * ttprit.PRHrgJual
                                        ) * 100,
                                        0
                                    ) AS Disc,
                                    ttprit.PRQty * ttprit.PRHrgJual - ttprit.PRDiscount AS Jumlah,
                                    tdbarang.BrgNama,
                                    tdbarang.BrgSatuan,
                                    ttprit.LokasiKode,
                                    tdbarang.BrgGroup'
                ) )
                ->from( 'ttprit' )
                ->join( 'LEFT JOIN', 'tdbarang', 'ttprit.BrgKode = tdbarang.BrgKode' )
                ->where( [ 'ttprit.PRNo' => $requestData[ 'PRNo' ] ] )
                ->orderBy('	ttprit.PRNo, ttprit.PRAuto');
            $rowsCount = $query->count();
            $rows      = $query->all();
            /* encode row id */
            for ( $i = 0; $i < count( $rows ); $i ++ ) {
                $rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
            }
            $response = [
                'rows'      => $rows,
                'rowsCount' => $rowsCount
            ];
        }
        return $response;
    }

    /**
     * Displays a single Ttprit model.
     * @param string $PRNo
     * @param integer $PRAuto
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($PRNo, $PRAuto)
    {
        return $this->render('view', [
            'model' => $this->findModel($PRNo, $PRAuto),
        ]);
    }

    /**
     * Creates a new Ttprit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ttprit();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'PRNo' => $model->PRNo, 'PRAuto' => $model->PRAuto]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ttprit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $PRNo
     * @param integer $PRAuto
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($PRNo, $PRAuto)
    {
        $model = $this->findModel($PRNo, $PRAuto);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'PRNo' => $model->PRNo, 'PRAuto' => $model->PRAuto]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ttprit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $PRNo
     * @param integer $PRAuto
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($PRNo, $PRAuto)
    {
        $this->findModel($PRNo, $PRAuto)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ttprit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $PRNo
     * @param integer $PRAuto
     * @return Ttprit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($PRNo, $PRAuto)
    {
        if (($model = Ttprit::findOne(['PRNo' => $PRNo, 'PRAuto' => $PRAuto])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
