<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\models\Ttkk;
use aunit\models\Ttkkit;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;
/**
 * TtkkitController implements the CRUD actions for Ttkkit model.
 */
class TtkkitController extends AunitController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttkkit models.
	 * @return mixed
	 */
	public function actionIndex() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$requestData                = Yii::$app->request->post();
		$primaryKeys                = Ttkkit::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
			/** @var Ttkkit $model */
			$model = null;
			if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
				$model = $this->findModelBase64( 'ttkkit', $requestData[ 'id' ] );
			}
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'Action' ] = 'Insert';
						$requestData[ 'FBNo' ]   = '--';
					} else {
						$requestData[ 'Action' ] = 'Update';
						if ( $model != null ) {
							$requestData[ 'KKNoOLD' ] = $model->KKNo;
							$requestData[ 'FBNoOLD' ] = $model->FBNo;
						}
					}
					if ( isset( $_GET[ 'KKJenis' ] ) && $_GET[ 'KKJenis' ] == 'Bayar BBN' ) {
						$result = Ttkkit::find()->callSPPengajuan( $requestData );
					} else {
						$result = Ttkkit::find()->callSP( $requestData );
					}
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'batch' :
					\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
					$respon                      = $this->responseFailed( 'Add Item' );
					$base64                      = urldecode( base64_decode( $requestData[ 'header' ] ) );
					parse_str( $base64, $header );
//					$header                      = $requestData[ 'header' ];
					$header[ 'Action' ]          = 'Loop';
					$header[ 'KKJam' ]          = $header[ 'KKTgl' ].' '.$header[ 'KKJam' ];
					$result                      = Ttkk::find()->callSP( $header );
					$KKNominal                   = 0;
					foreach ( $requestData[ 'data' ] as $item ) {
						$items[ 'Action' ]  = 'Insert';
						$items[ 'KKNo' ]    = $header[ 'KKNo' ];
						$items[ 'FBNo' ]    = $item[ 'id' ];
						$items[ 'KKBayar' ] = $item[ 'data' ][ 'KKBayar' ];
						$KKNominal          += floatval( $item[ 'data' ][ 'KKBayar' ] );
						$result             = Ttkkit::find()->callSP( $items );
					}
					$header[ 'Action' ]    = 'LoopAfter';
					$header[ 'KKNominal' ] = $KKNominal;
					$result                = Ttkk::find()->callSP( $header );
					return $this->responseSuccess( $requestData );
					break;
				case 'batchPengajuan' :
					\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
					$respon                      = $this->responseFailed( 'Add Item' );
					$base64                      = urldecode( base64_decode( $requestData[ 'header' ] ) );
					parse_str( $base64, $header );
//					$header                      = $requestData[ 'header' ];
					$header[ 'Action' ]          = 'Loop';
					$header[ 'KKJam' ]          = $header[ 'KKTgl' ].' '.$header[ 'KKJam' ];
					$result                      = Ttkk::find()->callSPPengajuan( $header );
					foreach ( $requestData[ 'data' ] as $item ) {
						$items[ 'Action' ]  = 'Insert';
						$items[ 'KKNo' ]    = $header[ 'KKNo' ];
						$items[ 'FBNo' ]    = $item[ 'id' ];
						$items[ 'KKBayar' ] = $item[ 'data' ][ 'KKBayar' ];
						$result             = Ttkkit::find()->callSPPengajuan( $items );
					}
					return $this->responseSuccess( $requestData );
					break;
				case 'del'  :
					$requestData             = array_merge( $requestData, $model->attributes );
					$requestData[ 'Action' ] = 'Delete';
					if ( $model != null ) {
						$requestData[ 'KKNoOLD' ] = $model->KKNo;
						$requestData[ 'FBNoOLD' ] = $model->FBNo;
					}
					if ( isset( $_GET[ 'KKJenis' ] ) && $_GET[ 'KKJenis' ] == 'Pengajuan' ) {
						$result = Ttkkit::find()->callSPPengajuan( $requestData );
					} else {
						$result = Ttkkit::find()->callSP( $requestData );
					}
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
			}
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'Ttkkit.' . $primaryKeys[ $i ];
			}
			/** @var Ttkk $model */
			$model = Ttkk::find()->where( [ 'KKNo' => $requestData[ 'KKNo' ] ] )->one();
			$query = new Query();
			switch ( $model->KKJenis ) {
				case 'Bayar BBN' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
                            KKNo, ttkkit.FBNo, KKBayar, DKTgl, LeaseKode, CusNama, MotorType, BBN, MotorTahun, MotorNoMesin , CusKecamatan, CusKabupaten, 
                            KKPaid.Paid - KKBayar AS Terbayar, KKBayar, BBN - KKPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(
                            SELECT ttkkit.KKNo, ttkkit.FBNo, ttkkit.KKBayar, ttdk.DKTgl, ttdk.LeaseKode, tdcustomer.CusNama, tmotor.MotorType, 
                            tmotor.MotorTahun, tmotor.MotorNoMesin , tdcustomer.CusKecamatan, tdcustomer.CusKabupaten,
                            (ttdk.BBN+ttdk.JABBN) AS BBN
                            FROM ttkkit 
                            INNER JOIN ttdk ON ttkkit.FBNo = ttdk.DKNo 
                            INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                            INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo WHERE (ttkkit.KKNo = :KKNo ) ORDER BY ttkkit.FBNo) ttkkit" ) )
					      ->addParams( [ ':KKNo' => $model->KKNo ] )
					      ->innerJoin( "(SELECT KKLINK AS FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'Bayar BBN') GROUP BY KKLINK) KKPaid 
                        ON ttkkit.FBNo = KKPaid.FBNo" );
					break;
				case 'BBN Plus' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
                            KKNo, ttkkit.FBNo, KKBayar, DKTgl, LeaseKode, CusNama, MotorType, BBN, MotorTahun, MotorNoMesin , CusKecamatan, CusKabupaten, 
                            KKPaid.Paid - KKBayar AS Terbayar, KKBayar, BBN - KKPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(               
                               SELECT ttkkit.KKNo, ttkkit.FBNo, ttkkit.KKBayar, ttdk.DKTgl, ttdk.LeaseKode, tdcustomer.CusNama, tmotor.MotorType, 
                                tmotor.MotorTahun, tmotor.MotorNoMesin , tdcustomer.CusKecamatan, tdcustomer.CusKabupaten,
                                (ttdk.BBNPlus) AS BBN
                                FROM ttkkit 
                                INNER JOIN ttdk ON ttkkit.FBNo = ttdk.DKNo 
                                INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                                INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo WHERE (ttkkit.KKNo = :KKNo ) ORDER BY ttkkit.FBNo) ttkkit" ) )
					      ->addParams( [ ':KKNo' => $model->KKNo ] )
					      ->innerJoin( "(SELECT KKLINK AS FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'BBN Plus') GROUP BY KKLINK) KKPaid 
                            ON ttkkit.FBNo = KKPaid.FBNo" );
					break;
				case 'BBN Acc' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
                            KKNo, ttkkit.FBNo, KKBayar, DKTgl, LeaseKode, CusNama, MotorType, BBN, MotorTahun, MotorNoMesin , CusKecamatan, CusKabupaten, 
                            KKPaid.Paid - KKBayar AS Terbayar, KKBayar, BBN - KKPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(
                           SELECT ttkkit.KKNo, ttkkit.FBNo, ttkkit.KKBayar, ttdk.DKTgl, ttdk.LeaseKode, tdcustomer.CusNama, tmotor.MotorType, 
				            tmotor.MotorTahun, tmotor.MotorNoMesin , tdcustomer.CusKecamatan, tdcustomer.CusKabupaten,
                            (ttdk.DKSCP) AS BBN
                            FROM ttkkit 
                            INNER JOIN ttdk ON ttkkit.FBNo = ttdk.DKNo 
                            INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                            INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo WHERE (ttkkit.KKNo = :KKNo ) ORDER BY ttkkit.FBNo) ttkkit" ) )
					      ->addParams( [ ':KKNo' => $model->KKNo ] )
					      ->innerJoin( "(SELECT KKLINK AS FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'BBN Acc') GROUP BY KKLINK) KKPaid ON ttkkit.FBNo = KKPaid.FBNo" );
					break;
				case 'Bayar Unit' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
                            KKNo, ttkkit.FBNo, FBTgl, SSNo, SupKode , FBTotal, SupNama, Paid - KKBayar AS Terbayar, KKBayar, FBTotal - KKPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(               
                           SELECT  ttkkit.KKNo, ttkkit.FBNo, ttkkit.KKBayar, ttfb.FBTgl, ttfb.SSNo, ttfb.SupKode , ttfb.FBTotal,tdsupplier.SupNama
                            FROM ttkkit 
                            INNER JOIN ttfb ON ttkkit.FBNo = ttfb.FBNo 
                            INNER JOIN tdsupplier ON ttfb.SupKode = tdsupplier.SupKode WHERE (ttkkit.KKNo = :KKNo) ORDER BY ttkkit.FBNo) ttkkit" ) )
					      ->addParams( [ ':KKNo' => $model->KKNo ] )
					      ->innerJoin( "(SELECT KKLINK AS FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'Bayar Unit') GROUP BY KKLINK) KKPaid ON ttkkit.FBNo = KKPaid.FBNo " );
					break;
			}
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	/**
	 * Displays a single Ttkkit model.
	 *
	 * @param string $KKNo
	 * @param string $FBNo
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $KKNo, $FBNo ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $KKNo, $FBNo ),
		] );
	}
	/**
	 * Creates a new Ttkkit model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Ttkkit();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'KKNo' => $model->KKNo, 'FBNo' => $model->FBNo ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Ttkkit model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $KKNo
	 * @param string $FBNo
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $KKNo, $FBNo ) {
		$model = $this->findModel( $KKNo, $FBNo );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'KKNo' => $model->KKNo, 'FBNo' => $model->FBNo ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Ttkkit model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $KKNo
	 * @param string $FBNo
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $KKNo, $FBNo ) {
		$this->findModel( $KKNo, $FBNo )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Ttkkit model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $KKNo
	 * @param string $FBNo
	 *
	 * @return Ttkkit the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $KKNo, $FBNo ) {
		if ( ( $model = Ttkkit::findOne( [ 'KKNo' => $KKNo, 'FBNo' => $FBNo ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
