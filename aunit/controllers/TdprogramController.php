<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Tdprogram;
use common\components\General;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TdprogramController implements the CRUD actions for Tdprogram model.
 */
class TdprogramController extends AunitController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actions() {
		return [
			'td' => [
				'class' => TdAction::class,
				'model' => Tdprogram::class,
			],
		];
	}
	/**
	 * Lists all Tdprogram models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index' );
	}
	/**
	 * Displays a single Tdprogram model.
	 *
	 * @param string $ProgramNama
	 * @param string $MotorType
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $ProgramNama, $MotorType ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $ProgramNama, $MotorType ),
		] );
	}
	/**
	 * Creates a new Tdprogram model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		TUi::$actionMode = Menu::ADD;
		$model           = new Tdprogram();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'tdprogram/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
		}
		return $this->render( 'create', [
			'model' => $model,
			'id'    => 'new',
		] );
	}
	/**
	 * Updates an existing Tdprogram model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$index = \yii\helpers\Url::toRoute( [ 'tdprogram/index' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
                TUi::$actionMode = Menu::ADD;
                break;
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		$model = $this->findModelBase64( 'Tdprogram', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'tdprogram/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
		}
		return $this->render( 'update', [
			'model' => $model,
			'id'    => $this->generateIdBase64FromModel( $model ),
		] );
	}
	/**
	 * Deletes an existing Tdprogram model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $ProgramNama
	 * @param string $MotorType
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
        /** @var Tdprogram $model */
        $request = Yii::$app->request;
        $model             = $this->findModelBase64( 'Tdprogram', $id );
        if ( $model->delete() ) {
            if ($request->isAjax) {
                return $this->responseSuccess( 'Berhasil menghapus data Program');
            }
            $model = Tdprogram::find()->one();
            return $this->redirect( [ 'tdprogram/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );

        } else {
            return $this->responseFailed( 'Gagal menghapus data Program' );
        }
	}
	/**
	 * Finds the Tdprogram model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $ProgramNama
	 * @param string $MotorType
	 *
	 * @return Tdprogram the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $ProgramNama, $MotorType ) {
		if ( ( $model = Tdprogram::findOne( [ 'ProgramNama' => $ProgramNama, 'MotorType' => $MotorType ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	public function actionCancel( $id, $action ) {
		$model = Tdprogram::findOne( $id );
		if ( $model == null ) {
			$model = Tdprogram::find()->orderBy( [ 'ProgramNama' => SORT_ASC ] )->one();
			if ( $model == null ) {
				return $this->redirect( [ 'tdprogram/index' ] );
			}
		}
		return $this->redirect( [ 'tdprogram/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
	}
	public function actionGetCombo() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$data                        = General::cCmd( "SELECT ProgramNama,MotorType,
		COALESCE(PrgSubsSupplier,0) AS PrgSubsSupplier,
		COALESCE(PrgSubsDealer,0) AS PrgSubsDealer,
		COALESCE(PrgSubsFincoy,0) AS PrgSubsFincoy,
		COALESCE(PrgSubsTotal,0) AS PrgSubsTotal,
		PrgTglAwal,PrgTglAkhir
		FROM tdprogram WHERE PrgTglAwal <= :PrgTglAwal 
		AND PrgTglAkhir >= :PrgTglAkhir 
		AND MotorType =  :MotorType
		AND MotorType <> '--' 
		ORDER BY ProgramNama", [
			':PrgTglAwal'  => $_POST[ 'DKTgl' ],
			':PrgTglAkhir' => $_POST[ 'DKTgl' ],
			':MotorType'   => $_POST[ 'MotorType' ],
		] )->queryAll();
		$rows[]                      = [
			'ProgramNama'     => '--',
			'MotorType'       => '--',
			'PrgSubsSupplier' => 0,
			'PrgSubsDealer'   => 0,
			'PrgSubsFincoy'   => 0,
			'PrgSubsTotal'    => 0,
			'PrgTglAwal'      => '1900-01-01',
			'PrgTglAkhir'     => '2078-12-30',
		];
		$rows                        = array_merge( $rows, $data );
		$rows[]                      = [
			'ProgramNama'     => 'Multi Program',
			'MotorType'       => 'Multi Program',
			'PrgSubsSupplier' => 0,
			'PrgSubsDealer'   => 0,
			'PrgSubsFincoy'   => 0,
			'PrgSubsTotal'    => 0,
			'PrgTglAwal'      => '1900-01-01',
			'PrgTglAkhir'     => '2078-12-30',
		];
		return [
			'rows'      => $rows,
			'rowsCount' => sizeof( $rows )
		];
	}
}
