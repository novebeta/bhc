<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Attachment;
use aunit\models\Tdleasing;
use aunit\models\Tdsales;
use aunit\models\Tdsupplier;
use aunit\models\Tmotor;
use aunit\models\Ttbkhd;
use aunit\models\Ttbmhd;
use aunit\models\Ttdk;
use aunit\models\Ttkk;
use aunit\models\Ttkm;
use common\components\Api;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\db\Exception;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtdkController implements the CRUD actions for Ttdk model.
 */
class TtdkController extends AunitController {
	public function actions() {
		$colGrid    = null;
		$joinWith   = [
			'motor'      => [
				'type' => 'LEFT OUTER JOIN'
			],
			'motorTypes' => [
				'type' => 'LEFT OUTER JOIN'
			],
			'customer'   => [
				'type' => 'INNER JOIN'
			]
		];
		$join       = [];
		$where      = [];
		$sqlraw     = null;
		$queryRawPK = null;
		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			switch ( $r ) {
				case 'ttdk/data-konsumen-kredit':
					$colGrid = Ttdk::colGridDataKonsumenKredit();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$sqlraw     = "SELECT ttdk.DKDoc,ttdk.DKNo, tmotor.SKNo,ttdk.DKHarga,ttdk.DKTgl, IFNULL(tdcustomer.CusNama,'--') AS CusNama, 
			         IFNULL(tmotor.MotorType,'--') AS MotorType, 
			         IFNULL(tmotor.MotorWarna,'--') AS MotorWarna, 
			         IFNULL(tmotor.MotorTahun,'--') AS MotorTahun, 
			         IFNULL(tmotor.MotorNoMesin,'--') AS MotorNoMesin, 
			         IFNULL(tmotor.MotorNoRangka,'--') AS MotorNoRangka, 
			         IFNULL(tdmotortype.MotorNama,'--') AS MotorNama 
			         FROM ttdk 
			         INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			         INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType 
			         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
			         WHERE DKJenis LIKE 'Kredit%' {$whereRaw}";
					$queryRawPK = [ 'DKNo' ];
					break;
				case 'ttdk/data-konsumen-tunai':
					$colGrid = Ttdk::colGridDataKonsumenTunai();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$sqlraw     = "SELECT ttdk.DKDoc,ttdk.DKNo, tmotor.SKNo,ttdk.DKTgl, IFNULL(tdcustomer.CusNama,'--') AS CusNama, 
			         IFNULL(tmotor.MotorType,'--') AS MotorType, 
			         IFNULL(tmotor.MotorWarna,'--') AS MotorWarna, 
			         IFNULL(tmotor.MotorTahun,'--') AS MotorTahun, 
			         IFNULL(tmotor.MotorNoMesin,'--') AS MotorNoMesin, 
			         IFNULL(tmotor.MotorNoRangka,'--') AS MotorNoRangka, 
			         IFNULL(tdmotortype.MotorNama,'--') AS MotorNama 
			         FROM ttdk 
			         INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			         INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType 
			         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
			         WHERE DKJenis LIKE 'Tunai%'  {$whereRaw}";
					$queryRawPK = [ 'DKNo' ];
					break;
				case 'ttdk/select':
					$colGrid    = Ttdk::colGridDataKonsumenTunai();
					$sqlraw     = "SELECT ttsk.SKNo, ttsk.SKTgl, ttsk.LokasiKode, ttsk.SKMemo, ttsk.UserID, ttdk.CusKode, tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, tdcustomer.CusTelepon, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, ttdk.DKNo, ttdk.DKTgl, ttdk.DKHarga, tmotor.RKNo, ttsk.NoGL 
			         FROM ttsk 
			         INNER JOIN tmotor ON ttsk.SKNo = tmotor.SKNo 
			         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
			         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			         UNION ALL 
			         SELECT ttsk.SKNo, ttsk.SKTgl, ttsk.LokasiKode, ttsk.SKMemo, ttsk.UserID, ttdk.CusKode, tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, tdcustomer.CusTelepon, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, ttdk.DKNo, ttdk.DKTgl, ttdk.DKHarga, tmotor.RKNo, ttsk.NoGL  
			         FROM ttsk 
			         INNER JOIN ttrk ON ttsk.SKNo = ttrk.SKNo 
			         INNER JOIN tmotor ON ttrk.RKNo = tmotor.RKNo 
			         INNER JOIN ttdk ON ttrk.DKNo = ttdk.DKNo 
			         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			         WHERE (tmotor.SSNo <> '--' OR tmotor.FBNo <> '--' OR tmotor.PDNo <>'--')";
					$queryRawPK = [ 'DKNo' ];
					break;
				case 'ttdk/select-surat-jalan-konsumen':
					$colGrid    = Ttdk::colGridDataKonsumenTunai();
					$DKNo       = $urlParams[ 'DKNo' ] ?? '--';
					$sqlraw     = "SELECT 
					  ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, 
       					tmotor.MotorNoMesin, tmotor.MotorNoRangka, tdmotortype.MotorNama 
					FROM ttdk INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
					  INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType 
					  INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
					  LEFT OUTER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo 
					WHERE (ttsk.SKNo IS NULL OR ttdk.DKNo = '$DKNo')";
					$queryRawPK = [ 'DKNo' ];
					break;
				case 'ttdk/select-order-bbn':
					$colGrid    = Ttdk::colGridSelectOrderBBN();
					$sqlraw     = "SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, 
					tmotor.MotorWarna, tmotor.MotorTahun, ttdk.DKNetto, ttdk.BBN, tmotor.MotorNoMesin, tmotor.MotorNoRangka,
					ttdk.LeaseKode,  tdcustomer.CusKelurahan,  tdcustomer.CusKabupaten
					FROM ttdk INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
					INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
         			WHERE ttdk.DKPengajuanBBN = 'Belum' AND LEFT(tmotor.FakturAHMNo,2) <> 'RK'";
					$queryRawPK = [ 'DKNo' ];
					break;
				case 'ttdk/select-kas-masuk-umum':
					$colGrid    = Ttdk::colGridDataKonsumenTunai();
					$sqlraw     = "SELECT  ttdk.*, tdcustomer.CusNama, tmotor.MotorType, tmotor.MotorWarna, 
       				tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tdmotortype.MotorNama 
					FROM ttdk 
					  INNER JOIN tmotor 
					    ON ttdk.DKNo = tmotor.DKNo 
					  INNER JOIN tdcustomer 
					    ON ttdk.CusKode = tdcustomer.CusKode 
					  INNER JOIN tdmotortype 
					    ON tdmotortype.MotorType = tmotor.MotorType";
					$queryRawPK = [ 'DKNo' ];
					break;
				case 'ttdk/data-konsumen-status':
					$colGrid = Ttdk::colGridDataKonsumenStatus();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "WHERE {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$sqlraw     = "SELECT ttdk.DKNo, ttdk.DKTgl, ttdk.SalesKode, ttdk.CusKode, ttdk.LeaseKode, ttdk.DKJenis, ttdk.DKMemo, ttdk.DKLunas, ttdk.UserID, ttdk.DKHPP, 
                         ttdk.ProgramNama, ttdk.Jaket, ttdk.NamaHadiah, ttdk.INNo, ttdk.DKTenor, ttdk.DKAngsuran,   
                         (ttdk.PrgSubsSupplier + ttdk.JAPrgSubsSupplier) AS PrgSubsSupplier, (ttdk.PrgSubsDealer + ttdk.JAPrgSubsDealer) AS PrgSubsDealer, (ttdk.PrgSubsFincoy + ttdk.JAPrgSubsFincoy) AS PrgSubsFincoy, 
                         (ttdk.ProgramSubsidi + ttdk.JAPrgSubsSupplier + ttdk.JAPrgSubsDealer + ttdk.JAPrgSubsFincoy) AS ProgramSubsidi,
                         (ttdk.DKDPLLeasing+JADKDPLLeasing) AS DKDPLLeasing, (ttdk.DKDPTerima + ttdk.JADKDPTerima) AS DKDPTerima, (ttdk.DKDPTotal + ttdk.JADKDPTerima + JADKDPLLeasing) AS DKDPTotal, 
                         (ttdk.DKHarga+ttdk.JADKHarga) AS DKHarga, (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) AS DKNetto, 
                         (ttdk.BBN+ttdk.JABBN) AS BBN, 
                         (ttdk.ReturHarga+ttdk.JAReturHarga) AS ReturHarga, (ttdk.PotonganHarga+ttdk.JAPotonganHarga) AS PotonganHarga, (ttdk.InsentifSales+ttdk.JAInsentifSales) AS InsentifSales, (ttdk.PotonganKhusus+ttdk.JAPotonganKhusus) AS PotonganKhusus,
                         ttdk.JAPrgSubsSupplier, ttdk.JAPrgSubsDealer, ttdk.JAPrgSubsFincoy, 
                         ttdk.JADKHarga, ttdk.JADKDPTerima, ttdk.JADKDPLLeasing,
                         ttdk.JABBN, 
                         ttdk.JAReturHarga, ttdk.JAPotonganHarga, ttdk.JAInsentifSales, ttdk.JAPotonganKhusus,
                         ttdk.PotonganAHM, ttdk.DKDPInden, ttdk.BBNPlus, ttdk.DKSurveyor, 
                         ttdk.TeamKode, ttdk.DKTglTagih, ttdk.DKPONo, ttdk.DKPOTgl, ttdk.ROCount, ttdk.DKScheme,ttdk.DKScheme2,  ttdk.DKSCP, ttdk.DKPengajuanBBN,
                         ttdk.DKMerkSblmnya, ttdk.DKJenisSblmnya, ttdk.DKMotorUntuk, ttdk.DKMotorOleh, ttdk.DKMediator, ttdk.DKTOP, ttdk.DKPOLeasing, ttdk.SPKNo, 
                         tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, 
                         tmotor.SKNo, tmotor.RKNo, tmotor.FakturAHMNo,
                         MotorNama, MotorKategori, SalesNama, LeaseNama, 
                         CusKTP, CusNama, CusAlamat, CusRT, CusRW, CusProvinsi, CusKabupaten, CusKecamatan, CusKelurahan, CusKodePos, CusTelepon, CusSex, 
                         CusTempatLhr, CusTglLhr, CusAgama, CusPekerjaan, CusPendidikan, CusPengeluaran, CusEmail, CusKodeKons, 
                         tdcustomer.CusStatusHP, tdcustomer.CusStatusRumah, tdcustomer.CusPembeliNama, tdcustomer.CusPembeliKTP, tdcustomer.CusPembeliAlamat, tdcustomer.CusKK
                        FROM  ttdk 
                        INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
                        INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                        INNER JOIN tdleasing ON ttdk.LeaseKode = tdleasing.LeaseKode 
                        INNER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode 
                        INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
			         {$whereRaw} ";
					$queryRawPK = [ 'DKNo' ];
					break;
				case 'ttdk/stnk-bpkb':
					$joinWith = array_merge( $joinWith, [
						'sales' => [
							'type' => 'LEFT JOIN'
						]
					] );
					$colGrid  = Ttdk::colGridStnkBpkb();
					break;
				case 'ttdk/data-portal':
					$colGrid = Ttdk::colGridDataPortal();
					break;
				case 'ttdk/select-pengajuan':
					$colGrid    = Ttdk::colGridPengajuanBerkas();
					$sqlraw     = "SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, 
		            tmotor.MotorWarna, tmotor.MotorTahun, ttdk.DKNetto, ttdk.BBN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, 
		            ttdk.LeaseKode,  tdcustomer.CusKelurahan,  tdcustomer.CusKabupaten , DKPengajuanBBN
		            FROM ttdk 
		            INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
		            INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
		            WHERE ttdk.DKPengajuanBBN = 'Belum' AND LEFT(tmotor.FakturAHMNo,2) <> 'RK'";
					$queryRawPK = [ 'DKNo' ];
					break;
				case 'ttdk/select-pengajuan-bayar-bbn-bank':
					$colGrid  = Ttdk::colGridPengajuanBerkas();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$sqlraw     = "SELECT
						ttdk.DKNo,	ttdk.DKTgl,	tdcustomer.CusNama,	tmotor.MotorType,	tmotor.MotorWarna,	tmotor.MotorTahun,	ttdk.DKNetto,
						IFNULL( ttabit.ABNo, '--' ) AS ABNo,	( ttdk.BBN + ttdk.JABBN ) AS BBN,	tmotor.MotorNoMesin,
						tmotor.MotorNoRangka,	ttdk.LeaseKode,	IFNULL( BKPaid.Paid, 0.00 ) Terbayar,
						( ttdk.BBN + ttdk.JABBN ) - IFNULL( BKPaid.Paid, 0.00 ) AS Sisa,	0.00 AS BKBayar 
					FROM
						ttdk
						INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
						INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
						LEFT OUTER JOIN ttabit ON ttabit.DKNo = ttdk.DKNo
						LEFT OUTER JOIN ( 
							SELECT KKLINK AS FBNo, SUM( KKNominal ) AS Paid 
							FROM vtkk WHERE ( KKJenis = 'Bayar BBN' ) GROUP BY KKLINK ) BKPaid ON ttdk.DKNo = BKPaid.FBNo 
					WHERE
						( ( ttdk.BBN + ttdk.JABBN ) - IFNULL( BKPaid.Paid, 0 ) > 0 ) 
						AND ttdk.DKPengajuanBBN = 'Sudah' 
						AND tmotor.FakturAHMNo NOT LIKE 'RK%' {$whereRaw}";
					$queryRawPK = [ 'DKNo' ];
					break;
				case 'ttdk/select-pengajuan-bayar-bbn-kas':
					$colGrid  = Ttdk::colGridPengajuanBerkas();
					$whereRaw = "WHERE ((ttdk.BBN + ttdk.JABBN) - IFNULL(KKPaid.Paid, 0) > 0)
							AND ttdk.DKPengajuanBBN = 'Sudah'
							AND tmotor.FakturAHMNo NOT LIKE 'RK%'";
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw .= "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$sqlraw     = "SELECT
						ttdk.DKNo,	ttdk.DKTgl,	IFNULL( ttabit.ABNo, '--' ) AS ABNo,	tdcustomer.CusNama,
						tmotor.MotorType,	tmotor.MotorWarna,	tmotor.MotorTahun,	ttdk.DKNetto,	( ttdk.BBN + ttdk.JABBN ) AS BBN,
						tmotor.MotorNoMesin,	tmotor.MotorNoRangka,	IFNULL( KKPaid.Paid, 0 ) Terbayar,
						( ttdk.BBN + ttdk.JABBN ) - IFNULL( KKPaid.Paid, 0 ) AS Sisa,	0.00 AS KKBayar 
					FROM
						ttdk
						INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
						INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
						LEFT OUTER JOIN ttabit ON ttabit.DKNo = ttdk.DKNo
						LEFT OUTER JOIN ( SELECT KKLINK AS FBNo, SUM( KKNominal ) AS Paid 
							FROM vtkk WHERE ( KKJenis = 'Bayar BBN' ) 
							GROUP BY KKLINK ) KKPaid ON ttdk.DKNo = KKPaid.FBNo
						{$whereRaw}";
					$queryRawPK = [ 'DKNo' ];
					break;
				case 'ttdk/select-bayar-bbn-bank':
					$colGrid  = Ttdk::colGridPengajuanBayarBBNBank();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$sqlraw     = "SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, 
		            tmotor.MotorWarna, tmotor.MotorTahun, ttdk.DKNetto,(ttdk.BBN+ttdk.JABBN) AS BBN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, 
		            ttdk.LeaseKode, 
		            IFNULL(BKPaid.Paid,0.00) Terbayar, (ttdk.BBN+ttdk.JABBN)-IFNULL(BKPaid.Paid,0.00) AS BKBayar, 0.00 As Sisa  
		            FROM ttdk 
		            INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
		            INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
		            LEFT OUTER JOIN 
		            (SELECT KKLINK As FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'Bayar BBN') GROUP BY KKLINK) BKPaid 
		            ON ttdk.DKNo = BKPaid.FBNo 
		            WHERE ((ttdk.BBN+ttdk.JABBN) - IFNULL(BKPaid.Paid, 0) > 0) 
		             AND ttdk.DKPengajuanBBN = 'Sudah' 
		             AND tmotor.FakturAHMNo NOT LIKE 'RK%'  
					  {$whereRaw} 
					ORDER BY ttdk.DKNo";
					$queryRawPK = [ 'DKNo' ];
					break;
				case 'ttdk/select-bayar-bbn-plus':
					$colGrid  = Ttdk::colGridKKBayarBBNPlus();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$sqlraw     = " SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, 
			         tmotor.MotorWarna, tmotor.MotorTahun, ttdk.DKNetto, (ttdk.BBNPlus) AS BBN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, 
			         IFNULL(KKPaid.Paid,0) Terbayar,(ttdk.BBNPlus)-IFNULL(KKPaid.Paid,0) AS Sisa, 0.00 As BKBayar 
			         FROM ttdk 
			         INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			         LEFT OUTER JOIN 
			         (SELECT KKLINK As FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'BBN Plus') GROUP BY KKLINK) KKPaid 
			         ON ttdk.DKNo = KKPaid.FBNo 
			         WHERE ((ttdk.BBNPlus) - IFNULL(KKPaid.Paid, 0) > 0) 
			          AND tmotor.FakturAHMNo NOT LIKE 'RK%'  
					  {$whereRaw} 
					ORDER BY ttdk.DKNo";
					$queryRawPK = [ 'DKNo' ];
					break;
				case 'ttdk/select-kk-bayar-bbn-bank':
					$colGrid  = Ttdk::colGridKKBayarBBNBank();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$sqlraw     = "SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, 
			         tmotor.MotorWarna, tmotor.MotorTahun, ttdk.DKNetto, (ttdk.BBN+ttdk.JABBN) AS BBN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, 
			         IFNULL(KKPaid.Paid,0) Terbayar,(ttdk.BBN+ttdk.JABBN)-IFNULL(KKPaid.Paid,0) AS KKBayar, 0.00 As Sisa  
			         FROM ttdk 
			         INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			         LEFT OUTER JOIN 
			         (SELECT KKLINK As FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'Bayar BBN') GROUP BY KKLINK) KKPaid 
			         ON ttdk.DKNo = KKPaid.FBNo 
			         WHERE ((ttdk.BBN+ttdk.JABBN) - IFNULL(KKPaid.Paid, 0) > 0) 
			          AND ttdk.DKPengajuanBBN = 'Sudah' 
			          AND tmotor.FakturAHMNo NOT LIKE 'RK%'  
					  {$whereRaw} 
					ORDER BY ttdk.DKNo";
					$queryRawPK = [ 'DKNo' ];
					break;
				case 'ttdk/select-kk-bayar-bbn-plus':
					$colGrid  = Ttdk::colGridKKBayarBBNPlus();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$sqlraw     = "  SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, 
			         tmotor.MotorWarna, tmotor.MotorTahun, ttdk.DKNetto, (ttdk.BBNPlus) AS BBN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, 
			         IFNULL(KKPaid.Paid,0) Terbayar,(ttdk.BBNPlus)-IFNULL(KKPaid.Paid,0) AS KKBayar, 0.00 As Sisa  
			         FROM ttdk 
			         INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			         LEFT OUTER JOIN 
			         (SELECT KKLINK As FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'BBN Plus') GROUP BY KKLINK) KKPaid 
			         ON ttdk.DKNo = KKPaid.FBNo 
			         WHERE ((ttdk.BBNPlus) - IFNULL(KKPaid.Paid, 0) > 0) 
			          AND tmotor.FakturAHMNo NOT LIKE 'RK%' 
					  {$whereRaw} 
					ORDER BY ttdk.DKNo";
					$queryRawPK = [ 'DKNo' ];
					break;
				case 'ttdk/select-kk-bayar-bbn-acc':
					$colGrid  = Ttdk::colGridKKBayarBBNAcc();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$sqlraw     = "  SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, 
			         tmotor.MotorWarna, tmotor.MotorTahun, ttdk.DKNetto, (ttdk.DKSCP) AS BBN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, 
			         IFNULL(KKPaid.Paid,0) Terbayar,(ttdk.DKSCP)-IFNULL(KKPaid.Paid,0) AS KKBayar, 0.00 AS Sisa  
			         FROM ttdk 
			         INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			         LEFT OUTER JOIN 
			         (SELECT KKLINK AS FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'BBN Acc') GROUP BY KKLINK) KKPaid 
			         ON ttdk.DKNo = KKPaid.FBNo 
			         WHERE ((ttdk.DKSCP) - IFNULL(KKPaid.Paid, 0) > 0) 
			          AND tmotor.FakturAHMNo NOT LIKE 'RK%' 
					  {$whereRaw} 
					ORDER BY ttdk.DKNo";
					$queryRawPK = [ 'DKNo' ];
					break;
				case 'ttdk/select-km-order-dk':
					$colGrid  = Ttdk::colGridKKOrderDK();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$KMNo          = $urlParams[ 'KMNo' ];
					$KMLink        = $urlParams[ 'KMLink' ];
					$jenis         = $urlParams[ 'jenis' ];
					$jenispotongan = $urlParams[ 'jenispotongan' ] ?? '--';
					switch ( $jenis ) {
						case 'Umum':
							$sqlraw = "SELECT ttdk.* , tdcustomer.CusNama, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, 
			               tmotor.MotorNoMesin, tmotor.MotorNoRangka, tdmotortype.MotorNama 
			               FROM ttdk 
			               INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			               INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			               INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType";
							break;
						case 'Kredit':
							if ( $jenispotongan == 'KreditTanpaPotongan' ) {
								$sqlraw = "SELECT ttdk.DKNo, ttdk.DKTgl, (ttdk.DKDPTerima + ttdk.JADKDPTerima) As DKDPTerima, tdcustomer.CusNama, 
								tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun,tmotor.MotorNoMesin, tmotor.MotorNoRangka, tdmotortype.MotorNama, 
								ttdk.DKDPTerima AS Awal,IFNULL(Kredit,0) AS Terbayar, ttdk.DKDPTerima  + ttdk.JADKDPTerima - IFNULL(Kredit,0) As Sisa  
				                 FROM ttdk 
				                  INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
				                  INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
				                  INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType 
				                  LEFT OUTER JOIN 
				                    (SELECT vtkm.KMLink, vtkm.KMJenis, IFNULL(SUM(vtkm.KMNominal),0) AS Kredit FROM vtkm 
				                     WHERE vtkm.KMJenis = 'Kredit' AND vtkm.KMNo <> '{$KMNo}' GROUP BY vtkm.KMLink, vtkm.KMJenis ) ttkM 
				                  ON ttdk.DKNo = ttkm.KMLink 
				                 WHERE (ttdk.DKNo = '{$KMLink}' OR (ttdk.DKDPTerima + ttdk.JADKDPTerima - IFNULL(ttkM.Kredit, 0)) > 0) 
				                 AND tmotor.FakturAHMNo NOT LIKE 'RK%' AND DKJenis = 'Kredit' ";
							} elseif ( $jenispotongan == 'KreditPotonganLangsung' ) {
								$sqlraw = "SELECT ttdk.DKNo, ttdk.DKTgl, ttdk.DKDPTerima + ttdk.JADKDPTerima - ttdk.ReturHarga - ttdk.PotonganHarga - ttdk.InsentifSales  AS DKDPTerima , 
       							tdcustomer.CusNama, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun,tmotor.MotorNoMesin, tmotor.MotorNoRangka, 
       							tdmotortype.MotorNama, IFNULL(Kredit,0) AS KreditTerbayar, ttdk.DKDPTerima + ttdk.JADKDPTerima - ttdk.ReturHarga - ttdk.PotonganHarga - ttdk.InsentifSales - IFNULL(Kredit,0) As Sisa  
				                 FROM ttdk 
				                  INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
				                  INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
				                  INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType 
				                  LEFT OUTER JOIN 
				                    (SELECT vtkm.KMLink, vtkm.KMJenis, IFNULL(SUM(vtkm.KMNominal),0) AS Kredit FROM vtkm 
				                     WHERE vtkm.KMJenis = 'Kredit' AND vtkm.KMNo <> '{$KMNo}' GROUP BY vtkm.KMLink, vtkm.KMJenis ) ttkM 
				                  ON ttdk.DKNo = ttkm.KMLink 
				                 WHERE (ttdk.DKNo = '{$KMLink}' OR (ttdk.DKDPTerima + ttdk.JADKDPTerima - ttdk.ReturHarga - ttdk.PotonganHarga - ttdk.InsentifSales - IFNULL(ttkM.Kredit, 0)) > 0) AND 
                 				DKJenis = 'Kredit'  AND tmotor.FakturAHMNo NOT LIKE 'RK%'";
							}
							break;
						case 'PiutangUM':
							$sqlraw = "SELECT ttdk.* , tdcustomer.CusNama, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, 
			               tmotor.MotorNoMesin, tmotor.MotorNoRangka, tdmotortype.MotorNama, DKDPLLeasing + JADKDPLLeasing As Awal, 
			               IFNULL(ttkM.PiutUMSudahMasuk, 0) AS Terbayar, (DKDPLLeasing + JADKDPLLeasing - IFNULL(ttkM.PiutUMSudahMasuk, 0)) As Sisa 
			               FROM ttdk 
			               INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			               INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			               INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType 
			               LEFT OUTER JOIN 
			                 (SELECT vtkm.KMLink, vtkm.KMJenis, SUM(vtkm.KMNominal) AS PiutUMSudahMasuk FROM vtkm 
			                  WHERE vtkm.KMJenis = 'Piutang UM' AND vtkm.KMNo <> '{$KMNo}' GROUP BY vtkm.KMLink, vtkm.KMJenis ) ttkM 
			               ON ttdk.DKNo = ttkm.KMLink 
			               WHERE (ttdk.DKNo = '{$KMLink}' OR (ttdk.DKDPLLeasing + ttdk.JADKDPLLeasing  -  IFNULL(ttkM.PiutUMSudahMasuk, 0)) > 0 ) AND 
			               DKDPLLeasing + JADKDPLLeasing > 0 AND tmotor.FakturAHMNo NOT LIKE 'RK%'";
							break;
						case 'Tunai':
							if ( $jenispotongan == 'TunaiTanpaPotongan' ) {
								$sqlraw = "SELECT ttdk.DKNo, ttdk.DKTgl, (ttdk.DKDPTerima + ttdk.JADKDPTerima) AS DKDPTerima, tdcustomer.CusNama, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, 
			                  tmotor.MotorNoMesin, tmotor.MotorNoRangka, tdmotortype.MotorNama, ttdk.DKDPTerima AS Awal,IFNULL(Tunai,0) AS Terbayar, ttdk.DKDPTerima + ttdk.JADKDPTerima - IFNULL(Tunai,0) As Sisa  
			                  FROM ttdk 
			                  INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			                  INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			                  INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType 
			                  LEFT OUTER JOIN 
			                    (SELECT vtkm.KMLink, vtkm.KMJenis, SUM(vtkm.KMNominal) AS Tunai FROM vtkm 
			                     WHERE vtkm.KMJenis = 'Tunai' AND vtkm.KMNo <> '{$KMNo}' GROUP BY vtkm.KMLink, vtkm.KMJenis ) ttkM 
			                  ON ttdk.DKNo = ttkm.KMLink 
			                  WHERE (ttdk.DKNo = '{$KMLink}' OR (ttdk.DKDPTerima + ttdk.JADKDPTerima - IFNULL(ttkM.Tunai, 0)) > 0) AND 
			                  DKJenis = 'Tunai'  AND tmotor.FakturAHMNo NOT LIKE 'RK%'";
							} elseif ( $jenispotongan == 'TunaiPotonganLangsung' ) {
								$sqlraw = "SELECT ttdk.DKNo, ttdk.DKTgl, ttdk.DKDPTerima + ttdk.JADKDPTerima - ttdk.ReturHarga - ttdk.PotonganHarga - ttdk.InsentifSales AS DKDPTerima , tdcustomer.CusNama, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, 
			                  tmotor.MotorNoMesin, tmotor.MotorNoRangka, tdmotortype.MotorNama, ttdk.DKDPTerima AS Awal,IFNULL(Tunai,0) AS Terbayar, ttdk.DKDPTerima + ttdk.JADKDPTerima - ttdk.ReturHarga - ttdk.PotonganHarga - ttdk.InsentifSales - IFNULL(Tunai,0) As Sisa  
			                  FROM ttdk 
			                  INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			                  INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			                  INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType 
			                  LEFT OUTER JOIN 
			                    (SELECT vtkm.KMLink, vtkm.KMJenis, SUM(vtkm.KMNominal) AS Tunai FROM vtkm 
			                     WHERE vtkm.KMJenis = 'Tunai' AND vtkm.KMNo <> '{$KMNo}' GROUP BY vtkm.KMLink, vtkm.KMJenis ) ttkM 
			                  ON ttdk.DKNo = ttkm.KMLink 
			                  WHERE (ttdk.DKNo = '{$KMLink}' OR (ttdk.DKDPTerima + ttdk.JADKDPTerima - ttdk.ReturHarga - ttdk.PotonganHarga - ttdk.InsentifSales  - IFNULL(ttkM.Tunai, 0)) > 0) AND 
			                  DKJenis = 'Tunai' AND tmotor.FakturAHMNo NOT LIKE 'RK%'";
							}
							break;
						case 'PiutangSisa':
							$sqlraw = "SELECT ttdk.DKNo, ttdk.DKTgl, ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy AS DKNetto, tdcustomer.CusNama, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, 
			               tmotor.MotorNoMesin, tmotor.MotorNoRangka, tdmotortype.MotorNama, ttdk.DKNetto AS Awal,
			               IFNULL(ttkM.PiutSisaSudahMasuk, 0) AS Terbayar, (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy - IFNULL(ttkM.PiutSisaSudahMasuk, 0)) AS Sisa 
			               FROM ttdk 
			               INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			               INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			               INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType 
			               LEFT OUTER JOIN 
			                 (SELECT vtkm.KMLink, vtkm.KMJenis, SUM(vtkm.KMNominal) AS PiutSisaSudahMasuk FROM vtkm 
			                  WHERE vtkm.KMJenis = 'Piutang Sisa' AND vtkm.KMNo <> '{$KMNo}' GROUP BY vtkm.KMLink, vtkm.KMJenis ) ttkM 
			               ON ttdk.DKNo = ttkm.KMLink 
			               WHERE (IFNULL(ttkM.KMJenis,'--') <> 'Piutang Sisa' OR ttdk.DKNo = '{$KMLink}' OR (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy - IFNULL(ttkM.PiutSisaSudahMasuk, 0)) > 0)  AND 
			               DKJenis = 'Tunai'  AND  (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) > 0 AND tmotor.FakturAHMNo NOT LIKE 'RK%'";
							break;
						case 'Leasing':
							$sqlraw = "SELECT ttdk.* , tdcustomer.CusNama, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, 
			               tmotor.MotorNoMesin, tmotor.MotorNoRangka, tdmotortype.MotorNama, ttdk.DKNetto AS Awal,
			               IFNULL(ttkM.PiutSisaSudahMasuk, 0) AS Terbayar, (ttdk.DKNetto  + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy - IFNULL(ttkM.PiutSisaSudahMasuk, 0)) AS Sisa 
			               FROM ttdk 
			               INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			               INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			               INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType 
			               LEFT OUTER JOIN 
			                 (SELECT vtkm.KMLink, vtkm.KMJenis, SUM(vtkm.KMNominal) AS PiutSisaSudahMasuk FROM vtkm 
			                  WHERE vtkm.KMJenis = 'Leasing' AND vtkm.KMNo <> '{$KMNo}' GROUP BY vtkm.KMLink, vtkm.KMJenis ) ttkM 
			               ON ttdk.DKNo = ttkm.KMLink 
			               WHERE (IFNULL(ttkM.KMJenis,'--') <> 'Leasing' OR ttdk.DKNo = '{$KMLink}' OR (ttdk.DKNetto  + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy - IFNULL(ttkM.PiutSisaSudahMasuk, 0)) > 0)  AND 
			               DKJenis = 'Kredit'  AND  (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) > 0 AND tmotor.FakturAHMNo NOT LIKE 'RK%'";
							break;
						case 'AngsuranKredit':
							$sqlraw = "SELECT ttdk.* , tdcustomer.CusNama, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, 
			               tmotor.MotorNoMesin, tmotor.MotorNoRangka, tdmotortype.MotorNama, ttdk.DKAngsuran AS Awal,
			               IFNULL(ttkM.PiutSisaSudahMasuk, 0) AS Terbayar, (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy - IFNULL(ttkM.PiutSisaSudahMasuk, 0)) AS Sisa 
			               FROM ttdk 
			               INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			               INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			               INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType 
			               LEFT OUTER JOIN 
			                 (SELECT vtkm.KMLink, vtkm.KMJenis, SUM(vtkm.KMNominal) AS PiutSisaSudahMasuk FROM vtkm 
			                  WHERE vtkm.KMJenis = 'Angsuran Kredit' AND vtkm.KMNo <> '{$KMNo}' GROUP BY vtkm.KMLink, vtkm.KMJenis ) ttkM 
			               ON ttdk.DKNo = ttkm.KMLink 
			               WHERE (IFNULL(ttkM.KMJenis,'--') <> 'Angsuran Kredit' OR ttdk.DKNo = '{$KMLink}' OR (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy - IFNULL(ttkM.PiutSisaSudahMasuk, 0)) > 0)  AND 
			               DKJenis = 'Kredit'  AND  (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) > 0 AND LEASEKode = 'AUM' AND tmotor.FakturAHMNo NOT LIKE 'RK%'";
							break;
						case 'BBNPlus':
							$sqlraw = "SELECT ttdk.* , tdcustomer.CusNama, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, 
			               tmotor.MotorNoMesin, tmotor.MotorNoRangka, tdmotortype.MotorNama, ttdk.BBNPlus AS Awal,
			               IFNULL(ttkM.PiutSisaSudahMasuk, 0) AS Terbayar, (ttdk.BBNPlus - IFNULL(ttkM.PiutSisaSudahMasuk, 0)) AS Sisa 
			               FROM ttdk 
			               INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			               INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			               INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType 
			               LEFT OUTER JOIN 
			                 (SELECT vtkm.KMLink, vtkm.KMJenis, SUM(vtkm.KMNominal) AS PiutSisaSudahMasuk FROM vtkm 
			                  WHERE vtkm.KMJenis = 'BBN Plus' AND vtkm.KMNo <> '{$KMNo}' GROUP BY vtkm.KMLink, vtkm.KMJenis ) ttkM 
			               ON ttdk.DKNo = ttkm.KMLink 
			               WHERE (IFNULL(ttkM.KMJenis,'--') <> 'BBN Plus' OR ttdk.DKNo = '{$KMLink}' OR (ttdk.BBNPlus - IFNULL(ttkM.PiutSisaSudahMasuk, 0)) > 0)  AND 
			               BBNPlus > 0 AND tmotor.FakturAHMNo NOT LIKE 'RK%'";
							break;
						case 'BBNAcc':
							$sqlraw = "SELECT ttdk.* , tdcustomer.CusNama, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, 
			               tmotor.MotorNoMesin, tmotor.MotorNoRangka, tdmotortype.MotorNama, ttdk.DKSCP AS Awal,
			               IFNULL(ttkM.PiutSisaSudahMasuk, 0) AS Terbayar, (ttdk.DKSCP - IFNULL(ttkM.PiutSisaSudahMasuk, 0)) AS Sisa 
			               FROM ttdk 
			               INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			               INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			               INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType 
			               LEFT OUTER JOIN 
			                 (SELECT vtkm.KMLink, vtkm.KMJenis, SUM(vtkm.KMNominal) AS PiutSisaSudahMasuk FROM vtkm 
			                  WHERE vtkm.KMJenis = 'BBN Acc' AND vtkm.KMNo <> '{$KMNo}' GROUP BY vtkm.KMLink, vtkm.KMJenis ) ttkM 
			               ON ttdk.DKNo = ttkm.KMLink 
			               WHERE (IFNULL(ttkM.KMJenis,'--') <> 'BBN Acc' OR ttdk.DKNo = '{$KMLink}' OR (ttdk.BBNPlus - IFNULL(ttkM.PiutSisaSudahMasuk, 0)) > 0)  AND 
			               DKSCP > 0 AND tmotor.FakturAHMNo NOT LIKE 'RK%'";
							break;
					}
					$sqlraw     .= " {$whereRaw} ORDER BY ttdk.DKNo";
					$queryRawPK = [ 'DKNo' ];
					break;
				case 'ttdk/select-kk-order-dk':
					$colGrid  = Ttdk::colGridKKOrderDK();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$KKNo   = $urlParams[ 'KKNo' ];
					$KKLink = $urlParams[ 'KKLink' ];
					$jenis  = $urlParams[ 'jenis' ];
					switch ( $jenis ) {
						case 'Umum':
							$sqlraw = "SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, 
			               tmotor.MotorNoMesin, tmotor.MotorNoRangka, tdmotortype.MotorNama, 
			               ttdk.PrgSubsDealer, ttdk.ReturHarga, ttdk.PotonganKhusus, ttdk.PotonganHarga  
			               FROM ttdk 
			               INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			               INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType 
			               INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode ";
							break;
						case 'ReturHarga':
							$sqlraw = "SELECT ttdk.DKNo, ttdk.DKTgl, (ttdk.ReturHarga + ttdk.JAReturHarga) AS ReturHarga, tdcustomer.CusNama, 
       						tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tdmotortype.MotorNama, 
       						IFNULL(Kredit,0) AS Terbayar, (ttdk.ReturHarga + ttdk.JAReturHarga) - IFNULL(Kredit,0) As Sisa  
		                     FROM ttdk 
		                     INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
		                     INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
		                     INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType 
		                     LEFT OUTER JOIN 
		                       (SELECT vtKK.KKLink, vtKK.KKJenis, IFNULL(SUM(vtKK.KKNominal),0) AS Kredit FROM vtKK 
		                        WHERE vtKK.KKJenis = 'Retur Harga' AND vtKK.KKNo <> '{$KKNo}' GROUP BY vtKK.KKLink, vtKK.KKJenis ) ttKK 
		                     ON ttdk.DKNo = ttKK.KKLink 
		                     WHERE (ttdk.DKNo = '{$KKLink}' OR ((ttdk.ReturHarga+ttdk.JAReturHarga) - IFNULL(ttKK.Kredit, 0)) > 0) AND 
		                           tmotor.FakturAHMNo NOT LIKE 'RK%' AND ReturHarga > 0 ";
							break;
						case 'PotonganKhusus':
							$sqlraw = "SELECT ttdk.DKNo, ttdk.DKTgl, (ttdk.ReturHarga + ttdk.JAReturHarga) AS ReturHarga, tdcustomer.CusNama, 
       						tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tdmotortype.MotorNama, 
       						ttdk.PotonganKhusus AS Awal,IFNULL(Kredit,0) AS Terbayar,(ttdk.PotonganKhusus+ttdk.JAPotonganKhusus) - IFNULL(Kredit,0) As Sisa 
		                     FROM ttdk 
		                     INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
		                     INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
		                     INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType 
		                     LEFT OUTER JOIN 
		                       (SELECT vtKK.KKLink, vtKK.KKJenis, IFNULL(SUM(vtKK.KKNominal),0) AS Kredit FROM vtKK 
		                        WHERE vtKK.KKJenis = 'Potongan Khusus' AND vtKK.KKNo <> '{$KKNo}' GROUP BY vtKK.KKLink, vtKK.KKJenis ) ttKK 
		                     ON ttdk.DKNo = ttKK.KKLink 
		                     WHERE (ttdk.DKNo = '{$KKLink}' OR ((ttdk.ReturHarga+ttdk.JAReturHarga) - IFNULL(ttKK.Kredit, 0)) > 0) AND 
		                           tmotor.FakturAHMNo NOT LIKE 'RK%' AND PotonganKhusus > 0";
							break;
						case 'SubsidiDealer2':
							$sqlraw = "SELECT ttdk.DKNo,ttdk.DKTgl,(ttdk.PotonganHarga+ttdk.JAPotonganHarga) AS PotonganHarga, tdcustomer.CusNama, 
       						tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tdmotortype.MotorNama, 
       						ttdk.PotonganHarga AS Awal,IFNULL(Kredit,0) AS Terbayar, (ttdk.PotonganHarga + ttdk.JAPotonganHarga) - IFNULL(Kredit,0) As Sisa  
		                     FROM ttdk 
		                     INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
		                     INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
		                     INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType 
		                     LEFT OUTER JOIN 
		                       (SELECT vtKK.KKLink, vtKK.KKJenis, IFNULL(SUM(vtKK.KKNominal),0) AS Kredit FROM vtKK 
		                        WHERE vtKK.KKJenis = 'SubsidiDealer2' AND vtKK.KKNo <> '{$KKNo}' GROUP BY vtKK.KKLink, vtKK.KKJenis ) ttKK 
		                     ON ttdk.DKNo = ttKK.KKLink 
		                     WHERE (ttdk.DKNo = '{$KKLink}' OR ((ttdk.PotonganHarga+ttdk.JAPotonganHarga) - IFNULL(ttKK.Kredit, 0)) > 0) AND 
		                           tmotor.FakturAHMNo NOT LIKE 'RK%' AND PotonganHarga > 0 ";
							break;
						case 'InsentifSales':
							$sqlraw = "SELECT ttdk.DKNo,ttdk.DKTgl,(ttdk.InsentifSales+ttdk.JAInsentifSales) AS InsentifSales, tdcustomer.CusNama, 
       						tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tdmotortype.MotorNama, 
       						ttdk.InsentifSales AS Awal,IFNULL(Kredit,0) AS Terbayar, (ttdk.InsentifSales+ttdk.JAInsentifSales) - IFNULL(Kredit,0) As Sisa  
		                     FROM ttdk 
		                     INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
		                     INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
		                     INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType 
		                     LEFT OUTER JOIN 
		                       (SELECT vtKK.KKLink, vtKK.KKJenis, IFNULL(SUM(vtKK.KKNominal),0) AS Kredit FROM vtKK 
		                        WHERE vtKK.KKJenis = 'Insentif Sales' AND vtKK.KKNo <> '{$KKNo}' GROUP BY vtKK.KKLink, vtKK.KKJenis ) ttKK 
		                     ON ttdk.DKNo = ttKK.KKLink 
		                     WHERE (ttdk.DKNo = '{$KKLink}' OR ((ttdk.InsentifSales+ttdk.JAInsentifSales) - IFNULL(ttKK.Kredit, 0)) > 0) AND 
		                           tmotor.FakturAHMNo NOT LIKE 'RK%' AND (ttdk.InsentifSales > 0)";
							break;
					}
					$sqlraw     .= " {$whereRaw} ORDER BY ttdk.DKNo";
					$queryRawPK = [ 'DKNo' ];
					break;
				case 'ttdk/select-bm-order-leasing':
					$colGrid  = Ttdk::colGridBMOrderDK();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$orderBy = 'ttdk.DKNo';
//					$BMNo   = $urlParams[ 'BMNo' ];
					$jenis = $urlParams[ 'jenis' ];
					switch ( $jenis ) {
						case 'Scheme':
							$LeaseKode = $urlParams[ 'LeaseKode' ];
							$sqlraw    = "SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, 
				               tmotor.MotorWarna, tmotor.MotorTahun, ttdk.DKNetto, tmotor.MotorNoMesin, tmotor.MotorNoRangka,  
				               IFNULL(BMPaid.Paid,0) Terbayar, (ttdk.PrgSubsFincoy + ttdk.DKScheme)-IFNULL(BMPaid.Paid,0) AS BMBayar, 0 As Sisa, 
				               (ttdk.PrgSubsFincoy + ttdk.DKScheme) AS NilaiAwal 
				               FROM ttdk 
				               INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
				               INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
				               LEFT OUTER JOIN 
				               (SELECT vtkm.KMLink AS DKNo, IFNULL(SUM(vtkm.KMNominal),0) AS Paid FROM vtkm WHERE vtkm.KMJenis = 'Scheme' GROUP BY vtkm.KMLink) BMPaid 
				               ON ttdk.DKNo = BMPaid.DKNo 
				               WHERE  ((ttdk.PrgSubsFincoy + ttdk.DKScheme) - IFNULL(BMPaid.Paid, 0) > 0) 
				               AND (ttdk.LeaseKode = '{$LeaseKode}') AND tmotor.FakturAHMNo NOT LIKE 'RK%' ";
							break;
						case 'Leasing':
							$LeaseKode = $urlParams[ 'LeaseKode' ];
							$sqlraw    = "SELECT ttdk.DKNo, ttdk.DKTgl, 
				               IF(TRIM(tdcustomer.CusNama)<>TRIM(tdcustomer.CusPembeliNama) AND tdcustomer.CusPembeliNama <>'--' AND tdcustomer.CusPembeliNama <>'',CONCAT_WS(' qq ',tdcustomer.CusNama,tdcustomer.CusPembeliNama),tdcustomer.CusNama) AS CusNama, 
				               tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) AS NilaiAwal, tmotor.MotorNoMesin, tmotor.MotorNoRangka, 
				               IFNULL(BMPaid.Paid,0) Terbayar, (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(BMPaid.Paid,0) AS BMBayar, 0 As Sisa, 
				               (ttdk.DKHarga - ttdk.ProgramSubsidi) As DKTunai 
				               FROM ttdk 
				               INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
				               INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
				               LEFT OUTER JOIN 
				               (SELECT vtkm.KMLink AS DKNo, IFNULL(SUM(vtkm.KMNominal),0) AS Paid FROM vtkm WHERE vtkm.KMJenis = 'Leasing' GROUP BY vtkm.KMLink) BMPaid 
				               ON ttdk.DKNo = BMPaid.DKNo 
				               WHERE ((ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(BMPaid.Paid, 0) > 0) 
				               AND (ttdk.LeaseKode = '{$LeaseKode}') AND tmotor.FakturAHMNo NOT LIKE 'RK%' ";
							break;
						case 'PiutangSisa':
							$sqlraw = " SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, 
				               tmotor.MotorWarna, tmotor.MotorTahun, (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) AS NilaiAwal, tmotor.MotorNoMesin, tmotor.MotorNoRangka,  
				               IFNULL(BMPaid.Paid,0) Terbayar, (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy)-IFNULL(BMPaid.Paid,0) AS Sisa, 0 As BMBayar, 
				               (ttdk.DKHarga - ttdk.ProgramSubsidi) As DKTunai 
				               FROM ttdk 
				               INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
				               INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
				               LEFT OUTER JOIN 
				               (SELECT vtkm.KMLink AS DKNo, IFNULL(SUM(vtkm.KMNominal),0) AS Paid FROM vtkm WHERE vtkm.KMJenis = 'Piutang Sisa' GROUP BY vtkm.KMLink) BMPaid 
				               ON ttdk.DKNo = BMPaid.DKNo 
				               WHERE  ((ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(BMPaid.Paid, 0) > 0) 
				               AND (ttdk.LeaseKode = 'TUNAI') AND tmotor.FakturAHMNo NOT LIKE 'RK%' ";
							break;
						case 'Tunai':
							$sqlraw = " SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, 
			                  tmotor.MotorWarna, tmotor.MotorTahun, (ttdk.DKDPTerima + ttdk.JADKDPTerima) AS NilaiAwal, tmotor.MotorNoMesin, tmotor.MotorNoRangka,  
			                  IFNULL(BMPaid.Paid,0) Terbayar, (ttdk.DKDPTerima + ttdk.JADKDPTerima) -IFNULL(BMPaid.Paid,0) AS BMBayar, 0 As Sisa, 
			                  (ttdk.DKHarga - ttdk.ProgramSubsidi) As DKTunai 
			                  FROM ttdk 
			                  INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			                  INNER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo 
			                  INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			                  LEFT OUTER JOIN 
			                  (SELECT vtkm.KMLink AS DKNo, IFNULL(SUM(vtkm.KMNominal),0) AS Paid FROM vtkm WHERE vtkm.KMJenis = 'Tunai' GROUP BY vtkm.KMLink) BMPaid 
			                  ON ttdk.DKNo = BMPaid.DKNo 
			                  WHERE  ((ttdk.DKDPTerima + ttdk.JADKDPTerima) - IFNULL(BMPaid.Paid, 0) > 0) AND ttdk.DKJenis = 'Tunai' AND tmotor.FakturAHMNo NOT LIKE 'RK%' ";
							break;
						case 'Kredit':
							$sqlraw = "SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, 
			                  tmotor.MotorWarna, tmotor.MotorTahun, (ttdk.DKDPTerima + ttdk.JADKDPTerima) AS NilaiAwal, tmotor.MotorNoMesin, tmotor.MotorNoRangka,  
			                  IFNULL(BMPaid.Paid,0) Terbayar, (ttdk.DKDPTerima + ttdk.JADKDPTerima) - IFNULL(BMPaid.Paid,0) AS BMBayar, 0 As Sisa, 
			                  (ttdk.DKHarga - ttdk.ProgramSubsidi) As DKTunai 
			                  FROM ttdk 
			                  INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			                  INNER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo 
			                  INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			                  LEFT OUTER JOIN 
			                  (SELECT vtkm.KMLink AS DKNo, IFNULL(SUM(vtkm.KMNominal),0) AS Paid FROM vtkm WHERE vtkm.KMJenis = 'Kredit' GROUP BY vtkm.KMLink) BMPaid 
			                  ON ttdk.DKNo = BMPaid.DKNo 
			                  WHERE  ((ttdk.DKDPTerima + ttdk.JADKDPTerima) - IFNULL(BMPaid.Paid, 0) > 0) AND ttdk.DKJenis = 'Kredit' AND tmotor.FakturAHMNo NOT LIKE 'RK%' ";
							break;
						case 'PiutangUM':
							$sqlraw = "SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, 
		                    tmotor.MotorWarna, tmotor.MotorTahun, ttdk.DKDPLLeasing+JADKDPLLeasing AS NilaiAwal, tmotor.MotorNoMesin, tmotor.MotorNoRangka,  
		                    IFNULL(BMPaid.Paid,0) Terbayar, ttdk.DKDPLLeasing+JADKDPLLeasing-IFNULL(BMPaid.Paid,0) AS BMBayar, 0 As Sisa, 
		                    (ttdk.DKHarga - ttdk.ProgramSubsidi) As DKTunai 
		                    FROM ttdk 
		                    INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
		                    INNER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo 
		                    INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
		                    LEFT OUTER JOIN 
		                    (SELECT vtkm.KMLink AS DKNo, IFNULL(SUM(vtkm.KMNominal),0) AS Paid FROM vtkm WHERE vtkm.KMJenis = 'Piutang UM' GROUP BY vtkm.KMLink) BMPaid 
		                    ON ttdk.DKNo = BMPaid.DKNo 
		                    WHERE  (ttdk.DKDPLLeasing + JADKDPLLeasing - IFNULL(BMPaid.Paid, 0) > 0) AND tmotor.FakturAHMNo NOT LIKE 'RK%' ";
							break;
						case 'Inden':
							$sqlraw   = " SELECT ttin.INNo AS DKNo, ttin.INTgl AS DKTgl, tdcustomer.CusNama, ttin.MotorType, 
		                    ttin.MotorWarna, '--' As MotorTahun, ttin.INDP AS NilaiAwal, '--' AS MotorNoMesin, '--' As MotorNoRangka, 
		                    IFNULL(BMPaid.Paid,0) Terbayar, ttin.INDP-IFNULL(BMPaid.Paid,0) AS BMBayar, 0 As Sisa  
		                    FROM ttin 
		                    INNER JOIN tdleasing ON ttin.LeaseKode = tdleasing.LeaseKode 
		                    INNER JOIN tdsales ON ttin.SalesKode = tdsales.SalesKode 
		                    INNER JOIN tdcustomer ON ttin.CusKode = tdcustomer.CusKode 
		                    LEFT OUTER JOIN 
		                    (SELECT vtkm.KMLink AS DKNo, IFNULL(SUM(vtkm.KMNominal),0) AS Paid FROM vtkm WHERE vtkm.KMJenis = 'Inden' GROUP BY vtkm.KMLink) BMPaid 
		                    ON ttin.INNo = BMPaid.DKNo 
		                    WHERE (ttin.INDP -IFNULL(BMPaid.Paid,0)) > 0  ";
							$whereRaw = str_replace( 'DKTgl', 'INTgl', $whereRaw );
							$whereRaw = str_replace( 'DKNo', 'INNo', $whereRaw );
							$orderBy  = 'INNo';
							break;
						case 'BBNPlus':
							$sqlraw = " SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, 
			               tmotor.MotorWarna, tmotor.MotorTahun, ttdk.DKNetto, tmotor.MotorNoMesin, tmotor.MotorNoRangka,  
			               IFNULL(BMPaid.Paid,0) Terbayar, ttdk.BBNPlus-IFNULL(BMPaid.Paid,0) AS BMBayar, 0 As Sisa, 
			               ttdk.BBNPlus AS NilaiAwal
			               FROM ttdk 
			               INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			               INNER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo 
			               INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			               LEFT OUTER JOIN 
			               (SELECT vtkm.KMLink AS DKNo, IFNULL(SUM(vtkm.KMNominal),0) AS Paid FROM vtkm WHERE vtkm.KMJenis = 'BBN Plus' GROUP BY vtkm.KMLink) BMPaid 
			               ON ttdk.DKNo = BMPaid.DKNo 
			               WHERE  (ttdk.BBNPlus - IFNULL(BMPaid.Paid, 0) > 0) AND tmotor.FakturAHMNo NOT LIKE 'RK%' ";
							break;
					}
					$sqlraw     .= " {$whereRaw} ORDER BY {$orderBy}";
					$queryRawPK = [ 'DKNo' ];
					break;
				case 'ttdk/select-bm-multi':
					$colGrid  = Ttdk::colGridBMMulti();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$sqlraw     = "SELECT  vdkpm.DKNo, vdkpm.DKTgl, vdkpm.Jenis, vdkpm.KeteranganJenis, tmotor.MotorType, tdcustomer.CusNama, vdkpm.Awal AS NilaiAwal, 
       						IFNULL(vdkpmbayar.Paid, 0) AS Terbayar, 0.00 AS Sisa, (vdkpm.Awal - IFNULL(vdkpmbayar.Paid, 0)  ) AS BMBayar   
				            FROM vdkpm 
				            LEFT OUTER JOIN vdkpmbayar  ON vdkpm.DKNo = vdkpmbayar.DKNo  AND  vdkpm.Jenis = vdkpmbayar.Jenis 
				             INNER JOIN tmotor ON vdkpm.DKNO = tmotor.DKNO 
							  INNER JOIN tdcustomer ON vdkpm.CusKode = tdcustomer.CusKode 
							WHERE (vdkpm.Awal - IFNULL(vdkpmbayar.Paid, 0)) > 0 
							  AND vdkpm.Jenis <> 'Leasing' 
							  AND tmotor.FakturAHMNo NOT LIKE 'RK%' {$whereRaw} 
							ORDER BY vdkpm.DKNo";
					$queryRawPK = [ 'DKNo', 'Jenis' ];
					break;
				case 'ttdk/select-bk-order-penjualan':
					$colGrid  = Ttdk::colGridBKOrderPenjualan();
					$jenis    = $urlParams[ 'jenis' ];
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					switch ( $jenis ) {
						case 'SubsidiDealer2':
							$sqlraw = " SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, 
			               tmotor.MotorWarna, tmotor.MotorTahun, ttdk.DKNetto, (ttdk.PotonganHarga+ttdk.JAPotonganHarga) AS Awal, tmotor.MotorNoMesin, tmotor.MotorNoRangka, 
			               IFNULL(BKPaid.Paid,0) Terbayar, (ttdk.PotonganHarga+ttdk.JAPotonganHarga) -IFNULL(BKPaid.Paid,0) AS BKBayar, 0.00 As Sisa  
			               FROM ttdk 
			               INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			               INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			               LEFT OUTER JOIN 
			               (SELECT KKLINK As FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'SubsidiDealer2') GROUP BY KKLINK) bkPaid  
			               ON ttdk.DKNo = BKPaid.FBNo 
			               WHERE ((ttdk.PotonganHarga+ttdk.JAPotonganHarga) - IFNULL(BKPaid.Paid, 0) > 0) AND tmotor.FakturAHMNo NOT LIKE 'RK%'   
							  {$whereRaw} 
							ORDER BY ttdk.DKNo";
							break;
						case 'ReturHarga':
							$sqlraw = "SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, 
			               tmotor.MotorWarna, tmotor.MotorTahun, ttdk.DKNetto, (ttdk.ReturHarga+ttdk.JAReturHarga) AS Awal, tmotor.MotorNoMesin, tmotor.MotorNoRangka, 
			               IFNULL(BKPaid.Paid,0) Terbayar, (ttdk.ReturHarga+ttdk.JAReturHarga)-IFNULL(BKPaid.Paid,0) AS BKBayar, 0.00 As Sisa  
			               FROM ttdk 
			               INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			               INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			               LEFT OUTER JOIN 
			               (SELECT KKLINK As FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'Retur Harga') GROUP BY KKLINK) bkPaid  
			               ON ttdk.DKNo = BKPaid.FBNo 
			               WHERE ((ttdk.ReturHarga+ttdk.JAReturHarga) - IFNULL(BKPaid.Paid, 0) > 0) AND tmotor.FakturAHMNo NOT LIKE 'RK%'   
							  {$whereRaw} 
							ORDER BY ttdk.DKNo";
							break;
						case 'PotonganKhusus':
							$sqlraw = "SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, 
			               tmotor.MotorWarna, tmotor.MotorTahun, ttdk.DKNetto, (ttdk.PotonganKhusus+ttdk.JAPotonganKhusus) AS Awal, tmotor.MotorNoMesin, tmotor.MotorNoRangka, 
			               IFNULL(BKPaid.Paid,0) Terbayar, (ttdk.PotonganKhusus+ttdk.JAPotonganKhusus)-IFNULL(BKPaid.Paid,0) AS BKBayar, 0.00 As Sisa  
			               FROM ttdk 
			               INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			               INNER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo 
			               INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			               LEFT OUTER JOIN 
			               (SELECT KKLINK As FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'Potongan Khusus') GROUP BY KKLINK) bkPaid  
			               ON ttdk.DKNo = BKPaid.FBNo 
			               WHERE ((ttdk.PotonganKhusus+ttdk.JAPotonganKhusus) - IFNULL(BKPaid.Paid, 0) > 0) AND tmotor.FakturAHMNo NOT LIKE 'RK%'   
							  {$whereRaw} 
							ORDER BY ttdk.DKNo";
							break;
						case 'InsentifSales':
							$sqlraw = "SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, 
			               tmotor.MotorWarna, tmotor.MotorTahun, ttdk.DKNetto, (ttdk.InsentifSales+ttdk.JAInsentifSales) AS Awal, tmotor.MotorNoMesin, tmotor.MotorNoRangka, 
			               IFNULL(BKPaid.Paid,0) Terbayar, (ttdk.InsentifSales+ttdk.JAInsentifSales)-IFNULL(BKPaid.Paid,0) AS BKBayar, 0.00 As Sisa  
			               FROM ttdk 
			               INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
			               INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
			               LEFT OUTER JOIN 
			               (SELECT KKLINK As FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'Insentif Sales') GROUP BY KKLINK) bkPaid  
			               ON ttdk.DKNo = BKPaid.FBNo 
			               WHERE ((ttdk.InsentifSales+ttdk.JAInsentifSales) - IFNULL(BKPaid.Paid, 0) > 0) AND tmotor.FakturAHMNo NOT LIKE 'RK%'  
							  {$whereRaw} 
							ORDER BY ttdk.DKNo";
							break;
					}
					$queryRawPK = [ 'DKNo' ];
					break;
				case 'ttdk/list-transactions':
					$DKNo    = $urlParams[ 'DKNo' ];
					$sqlraw     = "SELECT * FROM vdknoalluang
					WHERE DKno = '$DKNo'
					ORDER BY Notrans, TglTrans, Jenis";
					$queryRawPK = [ 'NoTrans'];
					break;
				case 'ttdk/kwitansi-kirim-tagih':
					$colGrid = Ttdk::colGridKwitansiKirimTagih();
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(DKJenis LIKE '%')",
							'params'    => []
						]
					];
					break;
				case 'ttdk/kwitansi-um-konsumen':
					$colGrid = Ttdk::colGridKwitansiUmKonsumen();
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(DKJenis LIKE '%')",
							'params'    => []
						]
					];
					break;
				case 'ttdk/kwitansi-pelunasan-leasing':
					$colGrid = Ttdk::colGridKwitansiPelunasanLeasing();
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "(DKJenis LIKE '%')",
							'params'    => []
						]
					];
					break;
                case 'ttdk/kwitansi-bbn':
                    $colGrid = Ttdk::colGridKwitansi();
                    $where   = [
                        [
                            'op'        => 'AND',
                            'condition' => "(DKJenis LIKE '%')",
                            'params'    => []
                        ]
                    ];
                    break;
                case 'ttdk/kwitansi-dp-po-leasing':
                    $colGrid = Ttdk::colGridKwitansi();
                    $where   = [
                        [
                            'op'        => 'AND',
                            'condition' => "(DKJenis LIKE '%')",
                            'params'    => []
                        ]
                    ];
                    break;
                case 'ttdk/kwitansi-dp-konsumen':
                    $colGrid = Ttdk::colGridKwitansi();
                    $where   = [
                        [
                            'op'        => 'AND',
                            'condition' => "(DKJenis LIKE '%')",
                            'params'    => []
                        ]
                    ];
                    break;
                case 'ttdk/kwitansi-penjualan-leasing':
                    $colGrid = Ttdk::colGridKwitansi();
                    $where   = [
                        [
                            'op'        => 'AND',
                            'condition' => "(DKJenis LIKE 'Kredit%')",
                            'params'    => []
                        ]
                    ];
                    break;
                case 'ttdk/kwitansi-penjualan-tunai':
                    $colGrid = Ttdk::colGridKwitansi();
                    $where   = [
                        [
                            'op'        => 'AND',
                            'condition' => "(DKJenis LIKE 'Tunai%')",
                            'params'    => []
                        ]
                    ];
                    break;
                case 'ttdk/kwitansi-jasa-perantara':
                    $colGrid = Ttdk::colGridKwitansi();
                    $where   = [
                        [
                            'op'        => 'AND',
                            'condition' => "(DKJenis LIKE '%')",
                            'params'    => []
                        ]
                    ];
                    break;
			}
		}
		return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Ttdk::class,
				'queryRaw'   => $sqlraw,
				'queryRawPK' => $queryRawPK,
				'colGrid'    => $colGrid,
				'joinWith'   => $joinWith,
				'join'       => $join,
				'where'      => $where,
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}


	public function actionListTransactions() {
		$this->layout = 'select-mode';
		return $this->render( 'listtransaction', [
			'colGrid'=> Ttdk::colGridListTransactions(),
			'mode' => self::MODE_SELECT
		] );
	}

	public function actionLoop() {
		$_POST[ 'Action' ] = 'Loop';
		$result            = Ttdk::find()->callSP( $_POST );
//		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'status' ] == 1 ) {
			Yii::$app->session->addFlash( 'warning', $result[ 'keterangan' ] );
		}
	}
	public function actionLoopIn() {
		$_POST[ 'Action' ] = 'LoopIN';
		$result            = Ttdk::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
	}
	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
	public function actionDataKonsumenKredit() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'DKJenis' ] = 'Kredit';
			$post[ 'Action' ]  = 'Display';
			$result            = Ttdk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'DataKonsumenKredit' );
	}
	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
	public function actionDataKonsumenTunai() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'DKJenis' ] = 'Tunai';
			$post[ 'Action' ]  = 'Display';
			$result            = Ttdk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'DataKonsumenTunai' );
	}
	/**
	 * Lists all Ttfb models.
	 * @return mixed
	 */
	public function actionSelect() {
		$this->layout = 'select-mode';
		return $this->render( 'DataKonsumenSelect', [
			'mode' => self::MODE_SELECT
		] );
	}
	/**
	 * Lists all Ttfb models.
	 * @return mixed
	 */
	public function actionSelectSuratJalanKonsumen() {
		$this->layout = 'select-mode';
		return $this->render( 'DataKonsumenSelect', [
			'mode' => self::MODE_SELECT
		] );
	}
	public function actionSelectKasMasukUmum() {
		$this->layout = 'select-mode';
		return $this->render( 'DataKonsumenSelect', [
			'mode' => self::MODE_SELECT
		] );
	}
	public function actionSelectOrderBbn() {
		$this->layout = 'select-mode';
		return $this->render( 'DataKonsumenSelect', [
			'mode' => self::MODE_SELECT
		] );
	}
	public function actionSelectPengajuan() {
		$this->layout = 'select-mode';
		return $this->render( 'DataKonsumenSelect', [
			'mode' => self::MODE_SELECT_MULTISELECT
		] );
	}
	public function actionSelectBayarBbnBank() {
		$this->layout = 'select-mode';
		return $this->render( 'DataKonsumenSelect', [
			'mode'    => self::MODE_SELECT_MULTISELECT,
			'colGrid' => Ttdk::colGridPengajuanBayarBBNBank()
		] );
	}
	public function actionSelectBayarBbnPlus() {
		$this->layout = 'select-mode';
		return $this->render( 'DataKonsumenSelect', [
			'mode'    => self::MODE_SELECT_MULTISELECT,
			'colGrid' => Ttdk::colGridKKBayarBBNPlus()
		] );
	}
	public function actionSelectKkBayarBbnBank() {
		$this->layout = 'select-mode';
		return $this->render( 'DataKonsumenSelect', [
			'mode'    => self::MODE_SELECT_MULTISELECT,
			'colGrid' => Ttdk::colGridKKBayarBBNAcc()
		] );
	}
	public function actionSelectKkBayarBbnPlus() {
		$this->layout = 'select-mode';
		return $this->render( 'DataKonsumenSelect', [
			'mode'    => self::MODE_SELECT_MULTISELECT,
			'colGrid' => Ttdk::colGridKKBayarBBNPlus()
		] );
	}
	public function actionSelectKkBayarBbnAcc() {
		$this->layout = 'select-mode';
		return $this->render( 'DataKonsumenSelect', [
			'mode'    => self::MODE_SELECT_MULTISELECT,
			'colGrid' => Ttdk::colGridKKBayarBBNAcc()
		] );
	}
	public function actionSelectKmOrderDk() {
		$this->layout = 'select-mode';
		return $this->render( 'DataKonsumenSelect', [
			'mode'    => self::MODE_SELECT,
			'colGrid' => Ttdk::colGridKKOrderDK()
		] );
	}
	public function actionSelectKkOrderDk() {
		$this->layout = 'select-mode';
		return $this->render( 'DataKonsumenSelect', [
			'mode'    => self::MODE_SELECT,
			'colGrid' => Ttdk::colGridKKOrderDK()
		] );
	}
	public function actionSelectBmOrderLeasing() {
		$this->layout = 'select-mode';
		return $this->render( 'DataKonsumenSelect', [
			'mode'    => self::MODE_SELECT_MULTISELECT,
			'colGrid' => Ttdk::colGridBMOrderDK()
		] );
	}
	public function actionSelectBmMulti() {
		$this->layout = 'select-mode';
		return $this->render( 'DataKonsumenSelect', [
			'mode'    => self::MODE_SELECT_MULTISELECT,
			'colGrid' => Ttdk::colGridBMMulti()
		] );
	}
	public function actionSelectBkOrderPenjualan() {
		$this->layout = 'select-mode';
		return $this->render( 'DataKonsumenSelect', [
			'mode'    => self::MODE_SELECT_MULTISELECT,
			'colGrid' => Ttdk::colGridBKOrderPenjualan()
		] );
	}
	public function actionSelectPengajuanBayarBbnBank() {
		$this->layout = 'select-mode';
		return $this->render( 'DataKonsumenSelect', [
			'mode'    => self::MODE_SELECT_MULTISELECT,
			'colGrid' => Ttdk::colGridPengajuanBayarBBNBank()
		] );
	}
	public function actionSelectPengajuanBayarBbnKas() {
		$this->layout = 'select-mode';
		return $this->render( 'DataKonsumenSelect', [
			'mode'    => self::MODE_SELECT_MULTISELECT,
			'colGrid' => Ttdk::colGridDataKonBayarBBNKas()
		] );
	}
	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
	public function actionDataKonsumenStatus() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttdk::find()->callSPStatus( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'DataKonsumenStatus' );
	}
	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
	public function actionStnkBpkb() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttdk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'StnkBpkb' );
	}
	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
	public function actionDataPortal() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttdk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'DataPortal' );
	}
	/**
	 * Updates an existing Ttdk model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDataPortalUpdate( $id ) {
        $index = \yii\helpers\Url::toRoute( [ 'ttdk/data-portal' ] );
        if ( ! isset( $_GET[ 'action' ] ) ) {
            return $this->redirect( $index );
        }
        $action = $_GET[ 'action' ];
        switch ( $action ) {
            case 'create':
            case 'update':
                TUi::$actionMode = Menu::UPDATE;
                break;
            case 'view':
                TUi::$actionMode = Menu::VIEW;
                break;
            case 'display':
                TUi::$actionMode = Menu::DISPLAY;
                break;
        }

		$model = $this->findModelBase64( 'Ttdk', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'DataPortal', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
		$dsJual             = $model->find()->dsJual()->where( [ 'ttdk.DKNo' => $model->DKNo ] )->asArray( true )->one();
		$dsJual[ 'Action' ] = 'Load';
		$result             = Ttdk::find()->callSP( $dsJual );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->render( 'DataPortal-update', [
			'model'  => $model,
			'dsJual' => $dsJual,
			'id'     => $this->generateIdBase64FromModel( $model ),
		] );
	}
	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
	public function actionKwitansiKirimTagih() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttdk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KwitansiKirimTagih' );
	}
    public function actionKwitansiBbn() {
        if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
            $post[ 'Action' ] = 'Display';
            $result           = Ttdk::find()->callSP( $post );
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        }
        return $this->render( 'KwitansiBbn' );
    }
    public function actionKwitansiDpPoLeasing() {
        if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
            $post[ 'Action' ] = 'Display';
            $result           = Ttdk::find()->callSP( $post );
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        }
        return $this->render( 'KwitansiDpPoLeasing' );
    }
    public function actionKwitansiDpKonsumen() {
        if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
            $post[ 'Action' ] = 'Display';
            $result           = Ttdk::find()->callSP( $post );
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        }
        return $this->render( 'KwitansiDpKonsumen' );
    }
    public function actionKwitansiPenjualanTunai() {
        if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
            $post[ 'Action' ] = 'Display';
            $result           = Ttdk::find()->callSP( $post );
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        }
        return $this->render( 'KwitansiPenjualanTunai' );
    }
    public function actionKwitansiPenjualanLeasing() {
        if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
            $post[ 'Action' ] = 'Display';
            $result           = Ttdk::find()->callSP( $post );
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        }
        return $this->render( 'KwitansiPenjualanLeasing' );
    }
    public function actionKwitansiJasaPerantara() {
        if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
            $post[ 'Action' ] = 'Display';
            $result           = Ttdk::find()->callSP( $post );
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        }
        return $this->render( 'KwitansiJasaPerantara' );
    }
	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
	public function actionKwitansiUmKonsumen() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttdk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KwitansiUmKonsumen' );
	}
	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
	public function actionKwitansiPelunasanLeasing() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttdk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KwitansiPelunasanLeasing' );
	}
	/**
	 * Creates a new Ttdk model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 * @throws \yii\base\InvalidConfigException
	 */
	public function actionDataKonsumenKreditCreate() {
		TUi::$actionMode          = Menu::ADD;
		$sales                    = Tdsales::find()->orderBy( 'SalesStatus,SalesKode' )->one();
		$model                    = new Ttdk();
		$model->DKNo              = General::createTemporaryNo( 'DK', 'Ttdk', 'DKNo' );
		$model->DKTgl             = Yii::$app->formatter->asDate( 'now', 'yyyy-MM-dd' );
		$model->DKTglTagih        = Yii::$app->formatter->asDate( 'now', 'yyyy-MM-dd' );
		$model->DKPOTgl           = Yii::$app->formatter->asDate( 'now', 'yyyy-MM-dd' );
		$model->DKHPP             = 0;
		$model->DKHarga           = 0;
		$model->DKDPTotal         = 0;
		$model->DKDPLLeasing      = 0;
		$model->DKDPTerima        = 0;
		$model->DKDPInden         = 0;
		$model->DKNetto           = 0;
		$model->ProgramSubsidi    = 0;
		$model->BBN               = 0;
		$model->Jaket             = 0;
		$model->PrgSubsSupplier   = 0;
		$model->PrgSubsDealer     = 0;
		$model->PrgSubsFincoy     = 0;
		$model->PotonganHarga     = 0;
		$model->ReturHarga        = 0;
		$model->PotonganKhusus    = 0;
		$model->PotonganAHM       = 0;
		$model->DKTenor           = 0;
		$model->DKAngsuran        = 0;
		$model->DKTOP             = 7;
		$model->InsentifSales     = 0;
		$model->BBNPlus           = 0;
		$model->ROCount           = 0;
		$model->DKScheme          = 0;
		$model->DKSCP             = 0;
		$model->DKScheme2         = 0;
		$model->JAPrgSubsSupplier = 0;
		$model->JAPrgSubsDealer   = 0;
		$model->JAPrgSubsFincoy   = 0;
		$model->JADKHarga         = 0;
		$model->JADKDPTerima      = 0;
		$model->JADKDPLLeasing    = 0;
		$model->JABBN             = 0;
		$model->JAReturHarga      = 0;
		$model->JAPotonganHarga   = 0;
		$model->JAInsentifSales   = 0;
		$model->JAPotonganKhusus  = 0;
		$model->DKJenis           = 'Kredit';
		$Sales                    = Tdsales::find()->where( [ 'SalesStatus' => 'A' ] )->orderBy( 'SalesStatus,SalesKode' )->one();
		$model->SalesKode         = $Sales->SalesKode ?? '--';
		$model->TeamKode          = $Sales->TeamKode ?? '--';
		$model->CusKode           = '--';
		$model->LeaseKode         = '--';
		$model->INNo              = '--';
		$model->DKMemo            = '--';
		$model->DKLunas           = '--';
		$model->DKPengajuanBBN    = '--';
		$model->SPKNo             = '--';
		$model->ProgramNama       = '--';
		$model->DKMerkSblmnya     = '--';
		$model->DKJenisSblmnya    = '--';
		$model->DKMotorUntuk      = '--';
		$model->DKMotorOleh       = '--';
		$model->DKPOLeasing       = '--';
		$model->NamaHadiah        = '--';
		$model->DPTOP             = 0;
		$model->DKMediator        = '--';
		$model->DKSurveyor        = '--';
		$model->DKPONo            = '--';
		$model->NoBukuServis      = '--';
		$model->UserID            = $this->UserID();
		$model->save();
		$dsTJualDatKonFill             = $model->find()->dsTJualFillByNo()->where( "(ttdk.DKNo = :DKNo) AND (ttdk.DKJenis LIKE :DKJenis)",
			[ ':DKNo' => $model->DKNo, ':DKJenis' => $model->DKJenis, ] )->asArray( true )->one();
		$dsTJualDatKonFill[ 'Action' ] = 'Insert';
		$result                        = Ttdk::find()->callSP( $dsTJualDatKonFill );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$id                                 = $this->generateIdBase64FromModel( $model );
		$dsTJualDatKonFill[ 'CusKodeKons' ] = 'Kolektif Customer (Leasing)';
		$dsTJualDatKonFill[ 'DKNoView' ]    = $result[ 'DKNo' ];
		return $this->render( 'DataKonsumenKredit-create', [
			'model'             => $model,
			'dsTJualDatKonFill' => $dsTJualDatKonFill,
			'id'                => $id,
		] );
	}
	/**
	 * Updates an existing Ttdk model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDataKonsumenKreditUpdate( $id ) {
		$index = \yii\helpers\Url::toRoute( [ 'ttdk/data-konsumen-kredit' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttdk $model */
		$model = $this->findModelBase64( 'Ttdk', $id );
		if ( $model === null ) {
			Yii::$app->session->addFlash( 'warning', 'Fatal Error DKNo : ' . base64_decode( $id ) . ' tidak ditemukan.' );
			return $this->redirect( $index );
		}
		$dsTJualDatKonFill = Ttdk::find()->dsTJualFillByNo()->where( "(ttdk.DKNo = :DKNo) AND (ttdk.DKJenis LIKE :DKJenis)", [
			':DKNo'    => $model->DKNo,
			':DKJenis' => $model->DKJenis,
		] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$result = $this->save();
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				$model = Ttdk::findOne( $_POST[ 'DKNo' ] );
				return $this->redirect( [ 'ttdk/data-konsumen-kredit-update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
			} else {
				$_POST[ 'DKNoView' ] = $_GET[ 'DKNoView' ] ?? $result[ 'DKNo' ];
				return $this->render( 'DataKonsumenKredit-update', [
					'model'             => $model,
					'dsTJualDatKonFill' => array_merge( $dsTJualDatKonFill, $_POST ),
					'id'                => $this->generateIdBase64FromModel( $model ),
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTJualDatKonFill[ 'Action' ] = 'Load';
			$result                        = Ttdk::find()->callSP( $dsTJualDatKonFill );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		$dsTJualDatKonFill[ 'DKNoView' ] = $_GET[ 'DKNoView' ] ?? $result[ 'DKNo' ];
		return $this->render( 'DataKonsumenKredit-update', [
			'model'             => $model,
			'dsTJualDatKonFill' => $dsTJualDatKonFill,
			'id'                => $this->generateIdBase64FromModel( $model ),
		] );
	}
	protected function save() {
//		$post = Yii::$app->request->post();
//		/** @var Ttdk $model */
//		$post[ 'DKNoBaru' ] = $post[ 'DKNo' ];
		$_POST[ 'Action' ] = 'Update';
		$result            = Ttdk::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$_POST[ 'DKNoView' ] = $result[ 'DKNo' ];
		$_POST[ 'DKNo' ]     = $result[ 'DKNo' ];
		return $result;
	}
	public function actionDataKonsumenValidate() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$success                     = true;
		$msg                         = Tdsales::find()->CekPiutangKonsSales( $_POST[ 'DKTgl' ], $_POST[ 'SalesKode' ] );
		if ( $msg != '' ) {
			return [ 'success' => false, 'msg' => $msg ];
		}
		$tmotor = Tmotor::find()->where( [ 'MotorNoMesin' => $_POST[ 'MotorNoMesin' ] ] )->one();
		if ( $tmotor != null ) {
			if ( $tmotor->DKNo != '--' && $tmotor->DKNo != $_POST[ 'DKNo' ] ) {
				return [ 'success' => false, 'msg' => "Motor Telah di pakai oleh No Data Konsumen : " . $tmotor->DKNo ];
			}
		}
		$ttdk = Ttdk::find()->where( [ 'INNo' => $_POST[ 'INNo' ] ] )->andWhere( [ '<>', 'DKNo', $_POST[ 'DKNo' ] ] )->one();
		if ( $ttdk != null ) {
			if ( $ttdk->DKNo != '--' && $ttdk->DKNo != $_POST[ 'DKNo' ] ) {
				return [ 'success' => false, 'msg' => "No Inden telah dipakai oleh No Data Konsumen : " . $ttdk->DKNo ];
			}
		}
		return [ 'success' => $success, 'msg' => '' ];
	}
	/**
	 * Updates an existing Ttdk model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDataKonsumenTunaiUpdate( $id ) {
		$index = \yii\helpers\Url::toRoute( [ 'ttdk/data-konsumen-tunai' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttdk $model */
		$model = $this->findModelBase64( 'Ttdk', $id );
		if ( $model === null ) {
			Yii::$app->session->addFlash( 'warning', 'Fatal Error DKNo : ' . base64_decode( $id ) . ' tidak ditemukan.' );
			return $this->redirect( $index );
		}
		$dsTJualDatKonFill = $model->find()->dsTJualFillByNo()->where( "(ttdk.DKNo = :DKNo) AND (ttdk.DKJenis LIKE :DKJenis)", [
			':DKNo'    => $model->DKNo,
			':DKJenis' => 'Tunai',
		] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$result = $this->save();
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				$model = Ttdk::findOne( $_POST[ 'DKNo' ] );
				return $this->redirect( [ 'ttdk/data-konsumen-tunai-update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
			} else {
				$_POST[ 'DKNoView' ] = $_GET[ 'DKNoView' ] ?? $result[ 'DKNo' ];
				return $this->render( 'DataKonsumenTunai-update', [
					'model'             => $model,
					'dsTJualDatKonFill' => array_merge( $dsTJualDatKonFill, $_POST ),
					'id'                => $this->generateIdBase64FromModel( $model ),
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTJualDatKonFill[ 'Action' ] = 'Load';
			$result                        = Ttdk::find()->callSP( $dsTJualDatKonFill );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		$dsTJualDatKonFill[ 'DKNoView' ] = $_GET[ 'DKNoView' ] ?? $result[ 'DKNo' ];
		return $this->render( 'DataKonsumenTunai-update', [
			'model'             => $model,
			'dsTJualDatKonFill' => $dsTJualDatKonFill,
			'id'                => $this->generateIdBase64FromModel( $model ),
		] );
	}
	/**
	 * Creates a new Ttdk model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 * @throws \yii\base\InvalidConfigException
	 */
	public function actionDataKonsumenTunaiCreate() {
		TUi::$actionMode          = Menu::ADD;
		$model                    = new Ttdk();
		$model->DKNo              = General::createTemporaryNo( 'DK', 'Ttdk', 'DKNo' );
		$model->DKTgl             = Yii::$app->formatter->asDate( 'now', 'yyyy-MM-dd' );
		$model->DKTglTagih        = Yii::$app->formatter->asDate( 'now', 'yyyy-MM-dd' );
		$model->DKPOTgl           = Yii::$app->formatter->asDate( 'now', 'yyyy-MM-dd' );
		$model->DKHPP             = 0;
		$model->DKHarga           = 0;
		$model->DKDPTotal         = 0;
		$model->DKDPLLeasing      = 0;
		$model->DKDPTerima        = 0;
		$model->DKDPInden         = 0;
		$model->DKNetto           = 0;
		$model->ProgramSubsidi    = 0;
		$model->BBN               = 0;
		$model->Jaket             = 0;
		$model->PrgSubsSupplier   = 0;
		$model->PrgSubsDealer     = 0;
		$model->PrgSubsFincoy     = 0;
		$model->PotonganHarga     = 0;
		$model->ReturHarga        = 0;
		$model->PotonganKhusus    = 0;
		$model->PotonganAHM       = 0;
		$model->DKTenor           = 0;
		$model->DKAngsuran        = 0;
		$model->DKTOP             = 1;
		$model->InsentifSales     = 0;
		$model->BBNPlus           = 0;
		$model->ROCount           = 0;
		$model->DKScheme          = 0;
		$model->DKSCP             = 0;
		$model->DKScheme2         = 0;
		$model->JAPrgSubsSupplier = 0;
		$model->JAPrgSubsDealer   = 0;
		$model->JAPrgSubsFincoy   = 0;
		$model->JADKHarga         = 0;
		$model->JADKDPTerima      = 0;
		$model->JADKDPLLeasing    = 0;
		$model->JABBN             = 0;
		$model->JAReturHarga      = 0;
		$model->JAPotonganHarga   = 0;
		$model->JAInsentifSales   = 0;
		$model->JAPotonganKhusus  = 0;
		$model->DKJenis           = 'Tunai';
		$Sales                    = Tdsales::find()->where( [ 'SalesStatus' => 'A' ] )->orderBy( 'SalesStatus,SalesKode' );
		$model->SalesKode         = $Sales->one()->SalesKode ?? '--';
		$model->TeamKode          = $Sales->one()->TeamKode ?? '--';
		$model->CusKode           = '--';
		$model->LeaseKode         = Tdleasing::findOne( 'TUNAI' )->LeaseKode ?? '--';
		$model->INNo              = '--';
		$model->DKMemo            = '--';
		$model->DKLunas           = '--';
		$model->DKPengajuanBBN    = '--';
		$model->SPKNo             = '--';
		$model->ProgramNama       = '--';
		$model->DKMerkSblmnya     = '--';
		$model->DKJenisSblmnya    = '--';
		$model->DKMotorUntuk      = '--';
		$model->DKMotorOleh       = '--';
		$model->DKPOLeasing       = '--';
		$model->NamaHadiah        = '--';
		$model->DKMediator        = '--';
		$model->DKSurveyor        = '--';
		$model->DKPONo            = '--';
		$model->NoBukuServis      = '--';
		$model->UserID            = $this->UserID();
		$model->save();
		$model->refresh();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttdk::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTJualDatKonFill                  = $model->find()->dsTJualFillByNo()->where( "(ttdk.DKNo = :DKNo) AND (ttdk.DKJenis LIKE :DKJenis)", [
			':DKNo'    => $model->DKNo,
			':DKJenis' => 'Tunai',
		] )->asArray( true )->one();
		$dsTJualDatKonFill[ 'CusKodeKons' ] = 'Individual Customer (Regular)';
		$dsTJualDatKonFill[ 'DKNoView' ]    = $result[ 'DKNo' ];
		return $this->render( 'DataKonsumenTunai-create', [
			'model'             => $model,
			'dsTJualDatKonFill' => $dsTJualDatKonFill,
			'id'                => $this->generateIdBase64FromModel( $model ),
		] );
	}
	public function actionDataKonsumenUpdate( $id ) {
		/** @var Ttdk $model */
		$model = $this->findModelBase64( 'Ttdk', $id );
		if ( $model->DKJenis == 'Tunai' ) {
			$this->redirect( [ 'ttdk/data-konsumen-tunai-update', 'id' => $id, 'action' => $_GET[ 'action' ], 'mode' => $_GET[ 'mode' ] ] );
		} else {
			$this->redirect( [ 'ttdk/data-konsumen-kredit-update', 'id' => $id, 'action' => $_GET[ 'action' ], 'mode' => $_GET[ 'mode' ] ] );
		}
	}
	/**
	 * Deletes an existing Ttdk model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
//	public function actionDelete() {
//		/** @var Ttdk $model */
//		$model = $model = $this->findModelBase64( 'Ttdk', $_POST[ 'id' ] );
//		/* delete data detail  */
//		$_POST[ 'DKNo' ]   = $model->DKNo;
//		$_POST[ 'Action' ] = 'Delete';
//		$result            = Ttdk::find()->callSP( $_POST );
//		if ( $result[ 'status' ] == 0 ) {
//			return $this->responseSuccess( $result[ 'keterangan' ] );
//		} else {
//			return $this->responseFailed( $result[ 'keterangan' ] );
//		}
//	}
    public function actionDelete()
    {
        /** @var Ttdk $model */
        $request = Yii::$app->request;
        if ($request->isAjax){
            $model = $this->findModelBase64('Ttdk', $_POST['id']);
            $_POST['DKNo'] = $model->DKNo;
            $_POST['Action'] = 'Delete';
            $result = Ttdk::find()->callSP($_POST);
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            $id = $request->get('id');
            $index = $request->get('index');
            $model = $this->findModelBase64('Ttdk', $id);
            $_POST['DKNo'] = $model->DKNo;
            $_POST['Action'] = 'Delete';
            $result = Ttdk::find()->callSP($_POST);
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ( $result[ 'DKNo' ] == '--' ) {
				return $this->redirect( [ base64_decode( $index ) ] );
			} else {
				$model = Ttdk::findOne( $result[ 'DKNo' ] );
				$url   = base64_decode( $index ) . '-update';
				return $this->redirect( [ $url, 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
			}
        }
    }
	/**
	 * Finds the Ttdk model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttdk the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttdk::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	public function actionDataKonsumenStatusUpdate( $id ) {
		$index = Url::toRoute( [ 'ttdk/data-konsumen-status', 'id' => $id ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttdk $model */
		$model  = $this->findModelBase64( 'Ttdk', $id );
		$dsJual = Ttdk::find()->dsJualStatusKonsumen()
		              ->where( [ 'ttdk.DKNo' => $model->DKNo ] )
		              ->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			try {
				unset( $_POST[ 'DKTgl' ] );
//				$model->DKTOP      = floatval( $_POST[ 'DKTOP' ] );
//				$model->DKPONo     = $_POST[ 'DKPONo' ];
//				$model->DKPOTgl    = $_POST[ 'DKPOTgl' ];
//				$model->DKTglTagih = $_POST[ 'DKTglTagih' ];
//				$model->DKMemo     = $_POST[ 'DKMemo' ];
//				$model->save();
//				$cus                   = Tdcustomer::findOne( $_POST[ 'CusKode' ] );
//				$cus->CusPembeliNama   = $_POST[ 'CusPembeliNama' ];
//				$cus->CusPembeliKTP    = $_POST[ 'CusPembeliKTP' ];
//				$cus->CusPembeliAlamat = $_POST[ 'CusPembeliAlamat' ];
//				$cus->save();
				$post             = array_merge( $dsJual, $_POST );
				$post[ 'Action' ] = 'Update';
				$result           = Ttdk::find()->callSPStatus( $post );
				Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
				if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
					$post[ 'DKNoView' ] = $result[ 'DKNo' ];
					return $this->redirect( [ 'ttdk/data-konsumen-status-update', 'id' => $id, 'action' => 'display' ] );
				} else {
					return $this->render( 'data-konsumen-status-update', [
						'model'             => $model,
						'dsTJualDatKonFill' => $post,
						'id'                => $id,
					] );
				}
			} catch ( Exception $e ) {
				return $this->render( 'data-konsumen-status-update', [
					'model'             => $model,
					'dsTJualDatKonFill' => $_POST,
					'id'                => $id,
				] );
			}
		}
		$txtNPrgSubsSupplier = floatval( $dsJual[ 'PrgSubsSupplier' ] );
		$txtNPrgSubsDealer   = floatval( $dsJual[ 'PrgSubsDealer' ] );
		$txtNPrgSubsFincoy   = floatval( $dsJual[ 'PrgSubsFincoy' ] );
		$txtNDKDPLeasing     = floatval( $dsJual[ 'DKDPLLeasing' ] );
		$txtNDKDPInden       = floatval( $dsJual[ 'DKDPInden' ] );
		$txtNDKDPTerima      = floatval( $dsJual[ 'DKDPTerima' ] );
		$txtNDPPO            = $txtNPrgSubsSupplier + $txtNPrgSubsDealer + $txtNPrgSubsFincoy + $txtNDKDPLeasing + $txtNDKDPInden + $txtNDKDPTerima;
		$txtNUMPKBayar       = "0.00";
		$txtUMPKNoBukti      = "--";
		$txtUMPKTglBukti     = "--";
		$txtNUMIndenBayar    = "0.00";
		$txtUMIndenNoBukti   = "--";
		$txtUMIndenTglBukti  = "--";
		$txtNUMDBayar        = "0.00";
		$txtUMDNoBukti       = "--";
		$txtUMDTglBukti      = "--";
		$txtNDKNettoBayar    = "0.00";
		$txtDKNettoNoBukti   = "--";
		$txtDKNettoTglBukti  = "--";
		$txtNRHBayar         = "0.00";
		$txtRHNoBukti        = "--";
		$txtRHTglBukti       = "--";
		$txtNSD2Bayar        = "0.00";
		$txtSD2NoBukti       = "--";
		$txtSD2TglBukti      = "--";
		$txtNISBayar         = "0.00";
		$txtISNoBukti        = "--";
		$txtISTglBukti       = "--";
		$txtNPKBayar         = "0.00";
		$txtPKNoBukti        = "--";
		$txtPKTglBukti       = "--";
		$txtNPKSisa          = "0.00";
		$txtNBBNBayar        = "0.00";
		$txtBBNNoBukti       = "--";
		$txtBBNTglBukti      = "--";
		$txtNBBNSisa         = "0.00";
		$txtNBBNPlusBayar    = "0.00";
		$txtBBNPlusNoBukti   = "--";
		$txtBBNPlusTglBukti  = "--";
		$txtNBBNPlusSisa     = "0.00";
		$Nominal             = 0;
		$JenisDK             = $dsJual[ 'DKJenis' ];
		$INNo                = $dsJual[ 'INNo' ];
		if ( $INNo == '--' ) {
			$INNo = '';
		}
		###### ANGKA KM
		$foundRows    = Ttdk::find()->SelectKMBMDK( $dsJual[ 'DKNo' ], $INNo );
		$foundRowsCol = array_column( $foundRows, 'KMJenis' );
		$i            = array_search( 'Piutang UM', $foundRowsCol );
		if ( $i !== false ) {
			$txtNUMPKBayar   = floatval( $foundRows[ $i ][ 'KMNominal' ] );
			$txtUMPKNoBukti  = $foundRows[ $i ][ 'KMNo' ];
			$txtUMPKTglBukti = $foundRows[ $i ][ 'KMTgl' ];
		}
		$txtNUMPKSisa = $txtNDKDPLeasing - $txtNUMPKBayar;
		$i            = array_search( 'Inden', $foundRowsCol );
		if ( $i !== false ) {
			$txtNUMIndenBayar   = floatval( $foundRows[ $i ][ 'KMNominal' ] );
			$txtUMIndenNoBukti  = $foundRows[ $i ][ 'KMNo' ];
			$txtUMIndenTglBukti = $foundRows[ $i ][ 'KMTgl' ];
		}
		$txtNUMIndenSisa = $txtNDKDPInden - $txtNUMIndenBayar;
		$txtNBBNPlus     = floatval( $dsJual[ 'BBNPlus' ] );
		if ( $txtNBBNPlus != 0 ) {
			$i = array_search( 'BBN Plus', $foundRowsCol );
			if ( $i != false ) {
				$txtNBBNPlusBayar   = floatval( $foundRows[ $i ][ 'KMNominal' ] );
				$txtBBNPlusNoBukti  = $foundRows[ $i ][ 'KMNo' ];
				$txtBBNPlusTglBukti = $foundRows[ $i ][ 'KMTgl' ];
			}
			$txtNBBNPlusSisa = $txtNBBNPlus - $txtNBBNPlusBayar;
		}
		$i = array_search( 'Kredit', $foundRowsCol );
		if ( $i !== false ) {
			$txtNUMDBayar   = floatval( $foundRows[ $i ][ 'KMNominal' ] );
			$txtUMDNoBukti  = $foundRows[ $i ][ 'KMNo' ];
			$txtUMDTglBukti = $foundRows[ $i ][ 'KMTgl' ];
		}
		$i = array_search( 'Tunai', $foundRowsCol );
		if ( $i !== false ) {
			$txtNUMDBayar   = floatval( $foundRows[ $i ][ 'KMNominal' ] );
			$txtUMDNoBukti  = $foundRows[ $i ][ 'KMNo' ];
			$txtUMDTglBukti = $foundRows[ $i ][ 'KMTgl' ];
		}
		$txtNUMDSisa = $txtNDKDPTerima - $txtNUMDBayar;
		$i           = array_search( 'Leasing', $foundRowsCol );
		if ( $i !== false ) {
			$txtNDKNettoBayar   = floatval( $foundRows[ $i ][ 'KMNominal' ] );
			$txtDKNettoNoBukti  = $foundRows[ $i ][ 'KMNo' ];
			$txtDKNettoTglBukti = $foundRows[ $i ][ 'KMTgl' ];
		}
		$i = array_search( 'Piutang Sisa', $foundRowsCol );
		if ( $i !== false ) {
			$txtNDKNettoBayar   = floatval( $foundRows[ $i ][ 'KMNominal' ] );
			$txtDKNettoNoBukti  = $foundRows[ $i ][ 'KMNo' ];
			$txtDKNettoTglBukti = $foundRows[ $i ][ 'KMTgl' ];
		}
		$txtNDKNetto     = floatval( $dsJual[ 'DKNetto' ] );
		$txtNDKNettoSisa = $txtNDKNetto - $txtNDKNettoBayar;
		###### WARNA KM
		$dsJual[ 'lblPiutangUM' ]      = General::FontColorMaroon;
		$dsJual[ 'lblUMInden' ]        = General::FontColorMaroon;
		$dsJual[ 'lblReturHarga' ]     = General::FontColorMaroon;
		$dsJual[ 'lblSubsidiDealer2' ] = General::FontColorMaroon;
		$dsJual[ 'lblInsentifSales' ]  = General::FontColorMaroon;
		$dsJual[ 'lblPotonganKhusus' ] = General::FontColorMaroon;
		$dsJual[ 'lblBBNPlus' ]        = General::FontColorMaroon;
		$dsJual[ 'lblUMDiterima' ]     = General::FontColorMaroon;
		$dsJual[ 'lblLeasing' ]        = General::FontColorMaroon;
		$dsJual[ 'lblBiayaBBN' ]       = General::FontColorMaroon;
		if ( $txtNDKDPLeasing > 0 ) {
			if ( $txtNDKDPLeasing - $txtNUMPKBayar <= 0 ) {
				$dsJual[ 'lblPiutangUM' ] = General::FontColorLime;
			} elseif ( $txtNUMPKBayar == 0 ) {
				$dsJual[ 'lblPiutangUM' ] = General::FontColorPink;
			} elseif ( $txtNDKDPLeasing - $txtNUMPKBayar > 0 ) {
				$dsJual[ 'lblPiutangUM' ] = General::FontColorYellow;
			}
		}
		if ( $txtNDKDPInden > 0 ) {
			$Nominal     = 0;
			$foundRowsIN = Ttdk::find()->SelectKMBMIN( $INNo );
			if ( ! empty( $foundRowsIN ) ) {
				$Nominal = floatval( $foundRowsIN[ 0 ][ 'KMNominal' ] );
			}
			if ( $txtNDKDPInden - $Nominal <= 0 ) {
				$dsJual[ 'lblUMInden' ] = General::FontColorLime;
			} elseif ( $Nominal == 0 ) {
				$dsJual[ 'lblUMInden' ] = General::FontColorPink;
			} elseif ( $txtNDKDPInden - $Nominal > 0 ) {
				$dsJual[ 'lblUMInden' ] = General::FontColorYellow;
			}
		}
		if ( $txtNDKDPTerima > 0 ) {
			$Nominal = $txtNUMDBayar;
			if ( $txtNDKDPTerima - $Nominal <= 0 ) {
				$dsJual[ 'lblUMDiterima' ] = General::FontColorLime;
			} elseif ( $Nominal == 0 ) {
				$dsJual[ 'lblUMDiterima' ] = General::FontColorPink;
			} elseif ( $txtNDKDPTerima - $Nominal > 0 ) {
				$dsJual[ 'lblUMDiterima' ] = General::FontColorYellow;
			}
		}
		if ( $txtNDKNetto > 0 ) {
			$Nominal = $txtNDKNettoBayar;
			if ( $txtNDKNetto - $Nominal <= 0 ) {
				$dsJual[ 'lblLeasing' ] = General::FontColorLime;
			} elseif ( $Nominal == 0 ) {
				$dsJual[ 'lblLeasing' ] = General::FontColorPink;
			} elseif ( $txtNDKNetto - $Nominal > 0 ) {
				$dsJual[ 'lblLeasing' ] = General::FontColorYellow;
			}
		}
		if ( $txtNBBNPlus > 0 ) {
			$Nominal = $txtNBBNPlusBayar;
			if ( $txtNBBNPlus - $Nominal <= 0 ) {
				$dsJual[ 'lblBBNPlus' ] = General::FontColorLime;
			} elseif ( $Nominal == 0 ) {
				$dsJual[ 'lblBBNPlus' ] = General::FontColorPink;
			} elseif ( $txtNBBNPlus - $Nominal > 0 ) {
				$dsJual[ 'lblBBNPlus' ] = General::FontColorYellow;
			}
		}
		###### ANGKA KK
		$foundRows    = Ttdk::find()->SelectKKBKDK( $dsJual[ 'DKNo' ] );
		$foundRowsCol = array_column( $foundRows, 'KKJenis' );
		$i            = array_search( 'Retur Harga', $foundRowsCol );
		if ( $i !== false ) {
			$txtNRHBayar   = floatval( $foundRows[ $i ][ 'KKNominal' ] );
			$txtRHNoBukti  = $foundRows[ $i ][ 'KKNo' ];
			$txtRHTglBukti = $foundRows[ $i ][ 'KKTgl' ];
		}
		$txtNReturHarga = floatval( $dsJual[ 'ReturHarga' ] );
		$txtNRHSisa     = $txtNReturHarga - $txtNRHBayar;
		$i              = array_search( 'SubsidiDealer2', $foundRowsCol );
		if ( $i !== false ) {
			$txtNSD2Bayar   = floatval( $foundRows[ $i ][ 'KKNominal' ] );
			$txtSD2NoBukti  = $foundRows[ $i ][ 'KKNo' ];
			$txtSD2TglBukti = $foundRows[ $i ][ 'KKTgl' ];
		}
		$txtNPotonganHarga = floatval( $dsJual[ 'PotonganHarga' ] );
		$txtNSD2Sisa       = $txtNPotonganHarga - $txtNSD2Bayar;
		$i                 = array_search( 'Insentif Sales', $foundRowsCol );
		if ( $i !== false ) {
			$txtNISBayar   = floatval( $foundRows[ $i ][ 'KKNominal' ] );
			$txtISNoBukti  = $foundRows[ $i ][ 'KKNo' ];
			$txtISTglBukti = $foundRows[ $i ][ 'KKTgl' ];
		}
		$txtNInsentifSales = floatval( $dsJual[ 'InsentifSales' ] );
		$txtNISSisa        = $txtNInsentifSales - $txtNISBayar;
		$i                 = array_search( 'Bayar BBN', $foundRowsCol );
		if ( $i !== false ) {
			$txtNBBNBayar   = floatval( $foundRows[ $i ][ 'KKNominal' ] );
			$txtBBNNoBukti  = $foundRows[ $i ][ 'KKNo' ];
			$txtBBNTglBukti = $foundRows[ $i ][ 'KKTgl' ];
		}
		$txtNBBN     = floatval( $dsJual[ 'BBN' ] );
		$txtNBBNSisa = $txtNBBN - $txtNBBNBayar;
		$i           = array_search( 'Potongan Khusus', $foundRowsCol );
		if ( $i !== false ) {
			$txtNPKBayar   = floatval( $foundRows[ $i ][ 'KKNominal' ] );
			$txtPKNoBukti  = $foundRows[ $i ][ 'KKNo' ];
			$txtPKTglBukti = $foundRows[ $i ][ 'KKTgl' ];
		}
		$txtNPotonganKhusus = floatval( $dsJual[ 'PotonganKhusus' ] );
		$txtNPKSisa         = $txtNPotonganKhusus + $txtNPKBayar;
		if ( $txtNPotonganKhusus > 0 ) {
			$Nominal = $txtNPKBayar;
			if ( $txtNPotonganKhusus - $Nominal <= 0 ) {
				$dsJual[ 'lblPotonganKhusus' ] = General::FontColorLime;
			} elseif ( $Nominal == 0 ) {
				$dsJual[ 'lblPotonganKhusus' ] = General::FontColorPink;
			} elseif ( $txtNPotonganKhusus - $Nominal > 0 ) {
				$dsJual[ 'lblPotonganKhusus' ] = General::FontColorYellow;
			}
		}
		if ( $txtNPotonganHarga > 0 ) {
			$Nominal = $txtNSD2Bayar;
			if ( $txtNPotonganHarga - $Nominal <= 0 ) {
				$dsJual[ 'lblSubsidiDealer2' ] = General::FontColorLime;
			} elseif ( $Nominal == 0 ) {
				$dsJual[ 'lblSubsidiDealer2' ] = General::FontColorPink;
			} elseif ( $txtNPotonganHarga - $Nominal > 0 ) {
				$dsJual[ 'lblSubsidiDealer2' ] = General::FontColorYellow;
			}
		}
		if ( $txtNReturHarga > 0 ) {
			$Nominal = $txtNRHBayar;
			if ( $txtNReturHarga - $Nominal <= 0 ) {
				$dsJual[ 'lblReturHarga' ] = General::FontColorLime;
			} elseif ( $Nominal == 0 ) {
				$dsJual[ 'lblReturHarga' ] = General::FontColorPink;
			} elseif ( $txtNReturHarga - $Nominal > 0 ) {
				$dsJual[ 'lblReturHarga' ] = General::FontColorYellow;
			}
		}
		if ( $txtNInsentifSales > 0 ) {
			$Nominal = $txtNISBayar;
			if ( $txtNInsentifSales - $Nominal <= 0 ) {
				$dsJual[ 'lblInsentifSales' ] = General::FontColorLime;
			} elseif ( $Nominal == 0 ) {
				$dsJual[ 'lblInsentifSales' ] = General::FontColorPink;
			} elseif ( $txtNInsentifSales - $Nominal > 0 ) {
				$dsJual[ 'lblInsentifSales' ] = General::FontColorYellow;
			}
		}
		if ( $txtNBBN > 0 ) {
			$Nominal = $txtNBBNBayar;
			if ( $txtNBBN - $Nominal <= 0 ) {
				$dsJual[ 'lblBiayaBBN' ] = General::FontColorLime;
			} elseif ( $Nominal == 0 ) {
				$dsJual[ 'lblBiayaBBN' ] = General::FontColorPink;
			} elseif ( $txtNBBN - $Nominal > 0 ) {
				$dsJual[ 'lblBiayaBBN' ] = General::FontColorYellow;
			}
		}
		$dsJual[ 'DPPO' ]            = $txtNDPPO;
		$dsJual[ 'UMPKBayar' ]       = $txtNUMPKBayar;
		$dsJual[ 'UMPKNoBukti' ]     = $txtUMPKNoBukti;
		$dsJual[ 'UMPKTglBukti' ]    = $txtUMPKTglBukti;
		$dsJual[ 'UMPKSisa' ]        = $txtNUMPKSisa;
		$dsJual[ 'UMIndenSisa' ]     = $txtNUMIndenSisa;
		$dsJual[ 'UMIndenBayar' ]    = $txtNUMIndenBayar;
		$dsJual[ 'UMIndenNoBukti' ]  = $txtUMIndenNoBukti;
		$dsJual[ 'UMIndenTglBukti' ] = $txtUMIndenTglBukti;
		$dsJual[ 'BBNPlusBayar' ]    = $txtNBBNPlusBayar;
		$dsJual[ 'BBNPlusNoBukti' ]  = $txtBBNPlusNoBukti;
		$dsJual[ 'BBNPlusTglBukti' ] = $txtBBNPlusTglBukti;
		$dsJual[ 'BBNPlusSisa' ]     = $txtNBBNPlusSisa;
		$dsJual[ 'UMDBayar' ]        = $txtNUMDBayar;
		$dsJual[ 'UMDNoBukti' ]      = $txtUMDNoBukti;
		$dsJual[ 'UMDTglBukti' ]     = $txtUMDTglBukti;
		$dsJual[ 'UMDSisa' ]         = $txtNUMDSisa;
		$dsJual[ 'DKNettoBayar' ]    = $txtNDKNettoBayar;
		$dsJual[ 'DKNettoNoBukti' ]  = $txtDKNettoNoBukti;
		$dsJual[ 'DKNettoTglBukti' ] = $txtDKNettoTglBukti;
		$dsJual[ 'DKNettoSisa' ]     = $txtNDKNettoSisa;
		$dsJual[ 'RHBayar' ]         = $txtNRHBayar;
		$dsJual[ 'RHNoBukti' ]       = $txtRHNoBukti;
		$dsJual[ 'RHTglBukti' ]      = $txtRHTglBukti;
		$dsJual[ 'RHSisa' ]          = $txtNRHSisa;
		$dsJual[ 'SD2Bayar' ]        = $txtNSD2Bayar;
		$dsJual[ 'SD2NoBukti' ]      = $txtSD2NoBukti;
		$dsJual[ 'SD2TglBukti' ]     = $txtSD2TglBukti;
		$dsJual[ 'SD2Sisa' ]         = $txtNSD2Sisa;
		$dsJual[ 'ISBayar' ]         = $txtNISBayar;
		$dsJual[ 'ISNoBukti' ]       = $txtISNoBukti;
		$dsJual[ 'ISTglBukti' ]      = $txtISTglBukti;
		$dsJual[ 'ISSisa' ]          = $txtNISSisa;
		$dsJual[ 'BBNBayar' ]        = $txtNBBNBayar;
		$dsJual[ 'BBNNoBukti' ]      = $txtBBNNoBukti;
		$dsJual[ 'BBNTglBukti' ]     = $txtBBNTglBukti;
		$dsJual[ 'BBNSisa' ]         = $txtNBBNSisa;
		$dsJual[ 'PKBayar' ]         = $txtNPKBayar;
		$dsJual[ 'PKNoBukti' ]       = $txtPKNoBukti;
		$dsJual[ 'PKTglBukti' ]      = $txtPKTglBukti;
		$dsJual[ 'PKSisa' ]          = $txtNPKSisa;
		$dsJual[ 'Action' ]          = 'Load';
		$result                      = Ttdk::find()->callSPStatus( $dsJual );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsJual[ 'DKNoView' ] = $_GET[ 'DKNoView' ] ?? $result[ 'DKNo' ];
		return $this->render( 'data-konsumen-status-update', [
			'model'             => $model,
			'dsTJualDatKonFill' => $dsJual,
			'id'                => $id,
		] );
	}
	public function actionStnkBpkbUpdate( $id ) {
		/** @var Ttdk $model */
		$model  = $this->findModelBase64( 'Ttdk', $id );
		$dsJual = Tmotor::find()->dsTJualFillByNo()->where( "(tmotor.DKNo = :DKNo)", [
			':DKNo' => $model->DKNo,
		] )->asArray( true )->one();
		$id     = $this->generateIdBase64FromModel( $model );
		if ( Yii::$app->request->isPost ) {
			$post = array_merge( $dsJual, $_POST );
			/** @var Tmotor $motor */
			$motor                    = Tmotor::findOne( [ 'DKNo' => $model->DKNo ] );
			$motor->FakturAHMNilai    = $post[ 'FakturAHMNilai' ];
			$motor->FakturAHMNo       = $post[ 'FakturAHMNo' ];
			$motor->STNKTglBayar      = $post[ 'STNKTglBayar' ];
			$motor->FakturAHMTgl      = $post[ 'FakturAHMTgl' ];
			$motor->FakturAHMTglAmbil = $post[ 'FakturAHMTglAmbil' ];
			$motor->STNKNama          = $post[ 'STNKNama' ];
			$motor->STNKTglMasuk      = $post[ 'STNKTglMasuk' ];
			$motor->STNKAlamat        = $post[ 'STNKAlamat' ];
			$motor->STNKNo            = $post[ 'STNKNo' ];
			$motor->STNKTglJadi       = $post[ 'STNKTglJadi' ];
			$motor->STNKTglAmbil      = $post[ 'STNKTglAmbil' ];
			$motor->STNKPenerima      = $post[ 'STNKPenerima' ];
			$motor->NoticeNo          = $post[ 'NoticeNo' ];
			$motor->NoticeTglJadi     = $post[ 'NoticeTglJadi' ];
			$motor->NoticeTglAmbil    = $post[ 'NoticeTglAmbil' ];
			$motor->NoticePenerima    = $post[ 'NoticePenerima' ];
			$motor->PlatNo            = $post[ 'PlatNo' ];
			$motor->PlatTgl           = $post[ 'PlatTgl' ];
			$motor->PlatTglAmbil      = $post[ 'PlatTglAmbil' ];
			$motor->PlatPenerima      = $post[ 'PlatPenerima' ];
			$motor->BPKBNo            = $post[ 'BPKBNo' ];
			$motor->BPKBTglJadi       = $post[ 'BPKBTglJadi' ];
			$motor->BPKBTglAmbil      = $post[ 'BPKBTglAmbil' ];
			$motor->BPKBPenerima      = $post[ 'BPKBPenerima' ];
			$motor->SRUTNo            = $post[ 'SRUTNo' ];
			$motor->SRUTTglJadi       = $post[ 'SRUTTglJadi' ];
			$motor->SRUTTglAmbil      = $post[ 'SRUTTglAmbil' ];
			$motor->SRUTPenerima      = $post[ 'SRUTPenerima' ];
			$motor->MotorMemo         = $post[ 'MotorMemo' ];
			$motor->NoticeNilai       = $post[ 'NoticeNilai' ];
			$motor->NoticeDenda       = $post[ 'NoticeDenda' ];
			$motor->NoticeTglPrint    = $post[ 'NoticeTglPrint' ];
			$motor->save();
			$result[ 'status' ]     = sizeof( $motor->errors ) ? 1 : 0;
			$result[ 'keterangan' ] = $result[ 'status' ] == 0 ? 'Berhasil menyimpan data Data Konsumen [DK] No: ' . $model->DKNo :
				'Gagal, silahkan melengkapi data Data Konsumen [DK] No: ' . $model->DKNo .'<br>'.Html::errorSummary($motor);
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				$post[ 'DKNoView' ] = $model->DKNo;
				return $this->redirect( [ 'ttdk/stnk-bpkb-update', 'id' => $id, 'action' => 'display' ] );
			} else {
				return $this->render( 'stnk-bpkb-update', [
					'model'  => $model,
					'dsJual' => $post,
					'id'     => $id,
				] );
			}
		}
		$dsJual[ 'Action' ] = 'Load';
		$result             = Ttdk::find()->callSP( $dsJual );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsJual[ 'DKNoView' ] = $result[ 'DKNo' ];
		return $this->render( 'stnk-bpkb-update', [
			'model'  => $model,
			'dsJual' => $dsJual,
			'id'     => $id,
		] );
	}
	public function actionKwitansiKirimTagihCreate() {
		$model = new Ttdk();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'ttdk/kwitansi-kirim-tagih', 'id' => $model->DKNo ] );
		}
		return $this->render( 'kwitansi-kirim-tagih-create', [
			'model' => $model,
		] );
	}
	public function actionKwitansiUMKonsumenCreate() {
		return $this->create( 'Kwitansi UM Konsumen', 'kwitansi-um-konsumen', 'DK - Kwitansi UM Konsumen' );
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client            = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response          = $client->post( Yii::$app->params[ 'host_report' ] . '/aunit/fdk', [
			'body' => Json::encode( $_POST )
		] );
		$html              = $response->getBody();
		return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	public function actionCancel( $id, $action, $index ) {
		/** @var Ttdk $model */
		$_POST[ 'DKTgl' ]      = '1900-01-01';
		$_POST[ 'MotorTahun' ] = 1900;
		$_POST[ 'Action' ]     = 'Cancel';
		$result                = Ttdk::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'DKNo' ] == '--' ) {
			return $this->redirect( [ base64_decode( $index ) ] );
		} else {
			$model = Ttdk::findOne( $result[ 'DKNo' ] );
			$url   = base64_decode( $index ) . '-update';
			return $this->redirect( [ $url, 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}
	public function actionProsesLink() {
		$b64    = base64_decode( $_GET[ 'data' ] );
		$data   = Json::decode( $b64 );
		$Jenis  = $data[ 'jenis' ];
		$Target = $data[ 'target' ];
		switch ( $Target ) {
			case 'kas':
				if ( $data[ 'arus' ] === 'masuk' ) {
					$KMLink = ( $Jenis === 'Inden' ) ? $data[ 'INNo' ] : $data[ 'DKNo' ];
					$param  = [ 'action' => 'create', 'KMNominal' => $data[ 'sisa' ], 'KMMemo' => $Jenis, 'KMPerson' => $data[ 'CusNama' ], 'KMLink' => $KMLink ];
					return $this->redirect( Ttkm::getRoute( $Jenis, $param )[ 'create' ] );
				} elseif ( $data[ 'arus' ] === 'keluar' ) {
					$KKPerson = ( $Jenis === 'Retur Harga' ) ? $data[ 'DKMediator' ] : $data[ 'CusNama' ];
					if ( $Jenis === 'Insentif Sales' ) {
						$KKPerson = General::cCmd( "Select tdsales.SalesNama FROM tdsales INNER JOIN ttdk ON (tdsales.SalesKode = ttdk.SalesKode) WHERE DKNo = :DKNo", [
							':DKNo' => $data[ 'DKNo' ]
						] )->queryScalar();
					}
					$param = [ 'action' => 'create', 'KKNominal' => $data[ 'sisa' ], 'KKMemo' => $Jenis, 'KKPerson' => $KKPerson, 'KKLink' => $data[ 'DKNo' ] ];
					return $this->redirect( Ttkk::getRoute( $Jenis, $param )[ 'create' ] );
				}
				break;
			case 'bank':
				if ( $data[ 'arus' ] === 'masuk' ) {
					$DKNo  = ( $Jenis === 'Inden' ) ? $data[ 'INNo' ] : $data[ 'DKNo' ];
					$param = [ 'action' => 'create', 'BMNominal' => $data[ 'sisa' ], 'BMMemo' => $Jenis, 'BMPerson' => $data[ 'CusNama' ],
					           'DKNo'   => $DKNo, 'LeaseKode' => $data[ 'LeaseKode' ], 'NoAccount' => '11020101' ];
					return $this->redirect( Ttbmhd::getRoute( $Jenis, $param )[ 'create' ] );
				} elseif ( $data[ 'arus' ] === 'keluar' ) {
					$SupKode = Tdsupplier::find()->where( [ 'SupStatus' => 1 ] )->one()->SupKode;
					$param   = [ 'action' => 'create', 'BKNominal' => $data[ 'sisa' ], 'BKMemo' => $Jenis, 'BKPerson' => $data[ 'CusNama' ],
					             'FBNo'   => $data[ 'DKNo' ], 'NoAccount' => '11020101', 'SupKode' => $SupKode ];
					return $this->redirect( Ttbkhd::getRoute( $Jenis, $param )[ 'create' ] );
				}
				break;
			case 'show':
				$param  = [ 'action' => 'update', 'id' => $data[ 'link' ] ];
				$substr = substr( base64_decode( $data[ 'link' ] ), 0, 2 );
				$isKas  = ( $substr == 'KK' || $substr == 'KM' );
				switch ( $Jenis ) {
					case 'Piutang UM':
						return $this->redirect( $isKas ? Ttkm::getRoute( 'Piutang UM', $param )[ 'update' ] : Ttbmhd::getRoute( 'Piutang UM', $param )[ 'update' ] );
					case 'Inden':
						return $this->redirect( $isKas ? Ttkm::getRoute( 'Inden', $param )[ 'update' ] : Ttbmhd::getRoute( 'Inden', $param )[ 'update' ] );
					case 'Tunai':
						return $this->redirect( $isKas ? Ttkm::getRoute( 'Tunai', $param )[ 'update' ] : Ttbmhd::getRoute( 'Tunai', $param )[ 'update' ] );
					case 'Kredit':
						return $this->redirect( $isKas ? Ttkm::getRoute( 'Kredit', $param )[ 'update' ] : Ttbmhd::getRoute( 'Kredit', $param )[ 'update' ] );
					case 'Leasing':
						return $this->redirect( $isKas ? Ttkm::getRoute( 'Leasing', $param )[ 'update' ] : Ttbmhd::getRoute( 'Leasing', $param )[ 'update' ] );
					case 'Piutang Sisa':
						return $this->redirect( $isKas ? Ttkm::getRoute( 'Piutang Sisa', $param )[ 'update' ] : Ttbmhd::getRoute( 'Piutang Sisa', $param )[ 'update' ] );
					case 'Retur Harga':
						return $this->redirect( $isKas ? Ttkk::getRoute( 'Retur Harga', $param )[ 'update' ] : Ttbkhd::getRoute( 'Retur Harga', $param )[ 'update' ] );
					case 'SubsidiDealer2':
						return $this->redirect( $isKas ? Ttkk::getRoute( 'SubsidiDealer2', $param )[ 'update' ] : Ttbkhd::getRoute( 'SubsidiDealer2', $param )[ 'update' ] );
					case 'Insentif Sales':
						return $this->redirect( $isKas ? Ttkk::getRoute( 'Insentif Sales', $param )[ 'update' ] : Ttbkhd::getRoute( 'Insentif Sales', $param )[ 'update' ] );
					case 'Potongan Khusus':
						return $this->redirect( $isKas ? Ttkk::getRoute( 'Potongan Khusus', $param )[ 'update' ] : Ttbkhd::getRoute( 'Potongan Khusus', $param )[ 'update' ] );
					case 'BBN Plus':
						return $this->redirect( $isKas ? Ttkk::getRoute( 'BBN Plus', $param )[ 'update' ] : Ttbkhd::getRoute( 'BBN Plus', $param )[ 'update' ] );
				}
				break;
		}
	}

    public function actionImportdk()
    {
        $this->layout = 'select-mode';
        return $this->render('importdk', [
            'colGrid' => Ttdk::colGridImportDkItems(),
            'mode' => self::MODE_SELECT_DETAIL_MULTISELECT,
        ]);
    }
    public function actionImportstnk()
    {
        $this->layout = 'select-mode';
        return $this->render('importstnk', [
            'colGrid' => Ttdk::colGridImportDKStnkItems(),
            'mode' => self::MODE_SELECT_DETAIL_MULTISELECT,
        ]);
    }
    public function actionImportdkstatus()
    {
        $this->layout = 'select-mode';
        return $this->render('importdkstatus', [
            'colGrid' => Ttdk::colGridImportDKStatusItems(),
            'mode' => self::MODE_SELECT,
        ]);
    }

    public function actionImportItems($type,$apiType)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $fromTime = Yii::$app->formatter->asDate('-90 day', 'yyyy-MM-dd');
        $toTime = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
		$addParam = [];
        if (isset($_POST['query'])) {
            $query = Json::decode($_POST['query'], true);
            $fromTime = $query['tgl1'] ?? $fromTime;
            $toTime = $query['tgl2'] ?? $toTime;
			if($query['txt1'] !== ''){
				$addParam = array_merge_recursive($addParam,[$query['cmbTxt1']=> $query['txt1']]);
			}
        }
        $api = new Api();
        $mainDealer = $api->getMainDealer();
        $arrHasil = [];
        switch ($mainDealer) {
            case API::MD_HSO:
                $arrParams = [
                    'fromTime' => $fromTime . ' 00:00:00',
                    'toTime' => $toTime . ' 23:59:59',
                    'dealerId' => $api->getDealerId(),
					'idProspect'=> "",
					'idSalesPeople'=> ""
                ];

                break;
            case API::MD_ANPER:
                $arrParams = [
                    'fromTime' => $fromTime . ' 00:00:00',
                    'toTime' => $toTime . ' 23:59:59',
                    'dealerId' => $api->getDealerId(),
                    'requestServerTime' => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                    'requestServertimezone' => date('T'),
                    'requestServerTimestamp' => $api->getCurrUnixTime(),
                ];
                break;
			default:
				$arrParams = [
					'fromTime'               => $fromTime . ' 00:00:00',
					'toTime'                 => $toTime . ' 23:59:59',
					'dealerId'               => $api->getDealerId(),
					'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
					'requestServertimezone'  => date('T'),
					'requestServerTimestamp' => $api->getCurrUnixTime(),
				];
        }
		$arrParams = array_merge_recursive($arrParams,$addParam);
		$filterResult = [];
		if($query['txt2'] !== ''){
			$filterResult = [
				'key' => $query['cmbTxt2'],
				'value' => $query['txt2'],
			];
		}
        $hasil = $api->call($apiType, Json::encode($arrParams),$filterResult);
        $arrHasil = Json::decode($hasil);		
        if (!isset($arrHasil['data'])) {
            return [
                'rows' => [],
                'rowsCount' => 0
            ];
        }
        $_SESSION['ttdk'][$type] = $arrHasil['data'];
        return [
            'rows' => $arrHasil['data'],
            'rowsCount' => sizeof($arrHasil['data'])
        ];
    }

    public function actionImportDetails($type,$keyCol,$prop = 'unit')
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $key = array_search($_POST[$keyCol], array_column($_SESSION['ttdk'][$type], $keyCol));
        return [
            'rows' => $_SESSION['ttdk'][$type][$key][$prop] ?? [],
            'rowsCount' => sizeof($_SESSION['ttdk'][$type][$key][$prop] ?? [])
        ];
    }

	public function actionStnkBpkbImportLoop(){
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$respon = [];
		$base64 = urldecode(base64_decode($requestData['header']));
		parse_str($base64, $header);
		$requestData['data'][0]['header']['data']['Action'] = 'ImporBefore';
		$requestData['data'][0]['header']['data']['NoTrans'] = $header['DKNo'];
		$result = Api::callDOCH($requestData['data'][0]['header']['data']);
		Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		$model = Ttdk::findOne($result['NoTrans']);
		$id = $this->generateIdBase64FromModel($model);
		$respon['id'] = $id;
		if($result['status'] == 1){
			return $respon;
		}
		foreach ($requestData['data'] as $item) {
			$item['data']['Action'] = 'Impor';
			$item['data']['NoTrans'] = $model->DKNo;
			$result = Api::callDOCHit($item['data']);
			Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		}
		$requestData['data'][0]['header']['data']['Action'] = 'ImporAfter';
		$result = Api::callDOCH($requestData['data'][0]['header']['data']);
		Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		return $respon;
	}
	public function actionStatusImportLoop(){
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$respon = [];
		$base64 = urldecode(base64_decode($requestData['header']));
		parse_str($base64, $header);
		$requestData['data']['data']['Action'] = 'ImporBefore';
		$requestData['data']['data']['NoTrans'] = $header['DKNo'];
		$result = Api::callLSNG($requestData['data']['data']);
		Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		$model = Ttdk::findOne($result['NoTrans']);
		$id = $this->generateIdBase64FromModel($model);
		$respon['id'] = $id;
		if($result['status'] == 1){
			return $respon;
		}
		$requestData['data']['data']['Action'] = 'ImporAfter';
		$requestData['data']['data']['NoTrans'] = $header['DKNo'];
		$result = Api::callLSNG($requestData['data']['data']);
		Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		$model = Ttdk::findOne($result['NoTrans']);
		$id = $this->generateIdBase64FromModel($model);
		$respon['id'] = $id;
		return $respon;
	}
	public function actionImportLoop(){
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$respon = [];
		$base64 = urldecode(base64_decode($requestData['header']));
		parse_str($base64, $header);
		$requestData['data'][0]['header']['data']['Action'] = 'ImporBefore';
		$requestData['data'][0]['header']['data']['NoTrans'] = $header['DKNo'];
		$result = Api::callSPK($requestData['data'][0]['header']['data']);
		Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		$model = Ttdk::findOne($result['NoTrans']);
		$id = $this->generateIdBase64FromModel($model);
		$respon['id'] = $id;
		if($result['status'] == 1){
			return $respon;
		}
		foreach ($requestData['data'] as $item) {
			$item['data']['Action'] = 'Impor';
			$item['data']['NoTrans'] = $model->DKNo;
			$result = Api::callSPKitmotor($item['data']);
			Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		}
		foreach ($requestData['dtGrid2'] ?? [] as $dt) {
			$dt['Action'] = 'Impor';
			$dt['NoTrans'] = $model->DKNo;
			$result = Api::callSPKitkk($dt);
			Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		}
		$requestData['data'][0]['header']['data']['Action'] = 'ImporAfter';
		$result = Api::callSPK($requestData['data'][0]['header']['data']);
		Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		return $respon;
	}

    public function actionPlat()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
        $requestData = \Yii::$app->request->post();
        $plat = $requestData['plat'];
        $nosin = $requestData['nosin'];
        }

            $querysbs = "SELECT  MotorNoMesin, PlatNo FROM  tmotor WHERE PlatNo = '".$plat."' AND MotorNoMesin <> '--' AND PlatNo <> '--' ";
            $rows = Yii::$app->db->createCommand($querysbs)->queryAll();
            if($rows == []){
                $get = 'tidak';
                $mesin = '';
            }
            else{
                foreach ($rows as $key){
                    $mesin = $key['MotorNoMesin'];
                }
                if($nosin === $mesin){
                    $get = 'tidak';
                }else{
                $get = 'ada';
                $mesin = $key['MotorNoMesin'];
                }
            }
            return [
                'plat' => $get,
                'mesin' => $mesin,
            ];


    }

	public function actionLampiran($id){
		/** @var  $model Ttdk */
		$model = $this->findModelBase64( 'Ttdk', $id );
		$attach = new Attachment();
		$attach->table = 'ttdk';
		$attach->tableId = $model->DKNo;
		$attach->titleId = $model->DKNo;
		$attach->model = $model;
		$view = Yii::$app->request->get('view');

		if(!empty($view) && $view == 'stnkbpkb'){
			$attach->table = 'Tmotor';
			$attach->model = Tmotor::findOne(['DKNo'=> $model->DKNo]);
			$attach->redirectUrl = \yii\helpers\Url::toRoute( [ 'ttdk/stnk-bpkb-update','action' => 'display', 'id' => $id ] );
		}else{
			if ( $model->DKJenis == 'Tunai' ) {
				$attach->redirectUrl = \yii\helpers\Url::toRoute( [ 'ttdk/data-konsumen-tunai-update','action' => 'display', 'id' => $id ] );
			} else {
				$attach->redirectUrl = \yii\helpers\Url::toRoute( [ 'ttdk/data-konsumen-kredit-update','action' => 'display', 'id' => $id ] );
			}
		}

		return $this->redirect($attach->getRedirectUrl());
	}

}
