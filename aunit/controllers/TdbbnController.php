<?php

namespace aunit\controllers;

use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Tdbbn;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * TdbbnController implements the CRUD actions for Tdbbn model.
 */
class TdbbnController extends AunitController
{
    public function actions()
    {
        return [
            'td' => [
                'class' => TdAction::class,
                'model' => Tdbbn::class,
                'joinWith' => [
                    'motorTypes' => [
                        'type' => 'INNER JOIN'
                    ]
                ]
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionGetBbnValue()
    {
        $post = \Yii::$app->request->post();
        /** @var Tdbbn $tdbbn */
        $tdbbn = Tdbbn::find()->where(['MotorType' => $post['MotorType_id'], 'Kabupaten' => $post['CusKabupaten']])->one();
        $value = 0;
        if ($tdbbn != null) {
            $tglDK = intval(date('Y', strtotime($post['DKTgl'])));
            $tglMotor = intval($post['MotorTahun']);
            if ($tglMotor < $tglDK) {
                $value = intval(($tdbbn->BBNTahunLalu == 0) ? $tdbbn->BBNBiaya : $tdbbn->BBNTahunLalu);
            } else {
                $value = $tdbbn->BBNBiaya;
            }
        }
        return $this->responseSuccess($value);
    }

    /**
     * Lists all Tdbbn models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displays a single Tdbbn model.
     *
     * @param string $Kabupaten
     * @param string $MotorType
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($Kabupaten, $MotorType)
    {
        return $this->render('view', [
            'model' => $this->findModel($Kabupaten, $MotorType),
        ]);
    }

    /**
     * Creates a new Tdbbn model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        TUi::$actionMode = Menu::ADD;
        $model = new Tdbbn();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $Kabupaten = $model->Kabupaten ;
            $MotorType = $model->MotorType ;
            return $this->redirect(['tdbbn/update', 'id' => $this->generateIdBase64FromModel($model), 'action' => 'display', 'Kabupaten' => $Kabupaten, 'MotorType' => $MotorType]);
        }
        return $this->render('create', [
            'model' => $model,
            'id' => 'new'
        ]);
    }

    /**
     * Updates an existing Tdbbn model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param string $id
     * @param string $Kabupaten
     * @param string $MotorType

     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionUpdate($id)
    {
        $index = \yii\helpers\Url::toRoute(['tdbbn/index']);
        if (!isset($_GET['action'])) {
            return $this->redirect($index);
        }
        $action = $_GET['action'];
        switch ($action) {
            case 'create':
                TUi::$actionMode = Menu::ADD;
                break;
            case 'update':
                TUi::$actionMode = Menu::UPDATE;
                break;
            case 'view':
                TUi::$actionMode = Menu::VIEW;
                break;
            case 'display':
                TUi::$actionMode = Menu::DISPLAY;
                break;
        }
        $model = $this->findModelBase64( 'Tdbbn', $id );
        if ($model === null){
            $Kabupaten = $_GET['Kabupaten'];
            $MotorType = $_GET['MotorType'];
            $model = $this->findModel($Kabupaten, $MotorType);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['tdbbn/update', 'id' => $this->generateIdBase64FromModel($model), 'action' => 'display']);
        }
        return $this->render('update', [
            'model' => $model,
            'id' => $this->generateIdBase64FromModel($model),
        ]);
    }

    /**
     * Deletes an existing Tdbbn model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param string $Kabupaten
     * @param string $MotorType
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        /** @var Tdbbn $model */
        $request = Yii::$app->request;
        $model             = $this->findModelBase64( 'Tdbbn', $id );
        if ( $model->delete() ) {
            if ($request->isAjax) {
                return $this->responseSuccess( 'Berhasil menghapus data BBN');
            }
            $model = Tdbbn::find()->one();
            return $this->redirect( [ 'tdbbn/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );

        } else {
            return $this->responseFailed( 'Gagal menghapus data BBN' );
        }
    }

    /**
     * Finds the Tdbbn model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param string $Kabupaten
     * @param string $MotorType
     *
     * @return Tdbbn the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Kabupaten, $MotorType)
    {
        if (($model = Tdbbn::findOne(['Kabupaten' => $Kabupaten, 'MotorType' => $MotorType])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionCancel($id, $action)
    {
        $model = Tdbbn::findOne($id);
        if ($model == null) {
            $model = Tdbbn::find()->orderBy(['MotorType' => SORT_ASC])->one();
            if ($model == null) {
                return $this->redirect(['tdbbn/index']);
            }
        }
        return $this->redirect(['tdbbn/update', 'id' => $this->generateIdBase64FromModel($model), 'action' => 'display']);
    }
}
