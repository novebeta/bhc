<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Tdbaranglokasi;
use aunit\models\Tdlokasi;
use aunit\models\Tdsupplier;
use aunit\models\Ttprhd;
use aunit\models\Ttprit;
use common\components\Custom;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtprhdController implements the CRUD actions for Ttprhd model.
 */
class TtprhdController extends AunitController {
	public function actions() {
        $where      = [
            [
                'op'        => 'AND',
                'condition' => "(PRNo NOT LIKE '%=%')",
                'params'    => []
            ]
        ];
		return [
			'td' => [
				'class' => TdAction::class,
				'model' => Ttprhd::class,
                'where' => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttprhd models.
	 * @return mixed
	 */
	public function actionIndex() {
		$post[ 'Action' ] = 'Display';
		$result           = Ttprhd::find()->callSP( $post );
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'index', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'twprhd/td' ] )
		] );
	}
	/**
	 * Displays a single Ttprhd model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Finds the Ttprhd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttprhd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttprhd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttprhd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$LokasiKodeCmb       = Tdlokasi::find()->where( [ 'LokasiStatus' => 'A', 'LEFT(LokasiNomor,1)' => '7' ] )->orderBy( 'LokasiStatus, LokasiKode' )->one();
		$SupplierKodeCmb     = Tdsupplier::find()->where( [ 'SupStatus' => 'A', 'LEFT(SupStatus,1)' => '1' ] )->orderBy( 'SupStatus, SupKode' )->one();
		$model               = new Ttprhd();
		$model->PRNo         = General::createTemporaryNo( 'PR', 'Ttprhd', 'PRNo' );
		$model->PRTgl        = date( 'Y-m-d H:i:s' );
		$model->PRKeterangan = "--";
		$model->LokasiKode   = ( $LokasiKodeCmb != null ) ? $LokasiKodeCmb->LokasiKode : '';
		$model->SupKode      = ( $SupplierKodeCmb != null ) ? $SupplierKodeCmb->SupKode : '';
		$model->PRSubTotal   = 0;
		$model->PRDiscFinal  = 0;
		$model->PRTotalPajak = 0;
		$model->PRBiayaKirim = 0;
		$model->PRTotal      = 0;
		$model->PRTerbayar   = 0;
		$model->UserID       = $this->UserID();
		$model->KodeTrans    = "PR";
		$model->PINo         = "--";
		$model->NoGL         = "--";
		$model->PosKode      = $this->LokasiKode();
		$model->Cetak        = "Belum";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttprhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTBeli                  = Ttprhd::find()->ttprhdFillByNo()->where( [ 'ttprhd.PRNo' => $model->PRNo ] )
		                                  ->asArray( true )->one();
		$id                       = $this->generateIdBase64FromModel( $model );
		$dsTBeli[ 'PRTerbayar' ]  = 0;
		$dsTBeli[ 'StatusBayar' ] = 'Belum';
		$dsTBeli[ 'PRNoView' ]    = $result[ 'PRNo' ];
		return $this->render( 'create', [
			'model'   => $model,
			'dsTBeli' => $dsTBeli,
			'id'      => $id
		] );
	}
	/**
	 * Updates an existing Ttprhd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$index = \yii\helpers\Url::toRoute( [ 'ttprhd/index' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		/** @var Ttprhd $model */
		$model   = $this->findModelBase64( 'Ttprhd', $id );
		$dsTBeli = Ttprhd::find()->ttprhdFillByNo()->where( [ 'ttprhd.PRNo' => $model->PRNo ] )
		                 ->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post             = array_merge( $dsTBeli, \Yii::$app->request->post() );
			$post[ 'PRTgl' ]  = $post[ 'PRTgl' ] . ' ' . $post[ 'PRJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttprhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttprhd::findOne( $result[ 'PRNo' ] );
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'PRNoView' ] = $result[ 'PRNo' ];
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				return $this->redirect( [ 'ttprhd/update', 'id' => $id, 'action' => 'display', ] );
			} else {
				return $this->render( 'update', [
					'model'   => $model,
					'dsTBeli' => $post,
					'id'      => $id,
				] );
			}
		}
//		$dsTBeli[ 'PRTerbayar' ]  = 0;
		$dsTBeli[ 'StatusBayar' ] = 'Belum';
		$dsTBeli[ 'Action' ]      = 'Load';
		$result                   = Ttprhd::find()->callSP( $dsTBeli );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTBeli[ 'PRNoView' ] = $_GET[ 'PRNoView' ] ?? $result[ 'PRNo' ];
		return $this->render( 'update', [
			'model'   => $model,
			'dsTBeli' => $dsTBeli,
			'id'      => $id
		] );
	}
	/**
	 * Deletes an existing Ttprhd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete() {
		/** @var Ttprhd $model */
		$model             = $model = $this->findModelBase64( 'Ttprhd', $_POST[ 'id' ] );
		$_POST[ 'PRNo' ]   = $model->PRNo;
		$_POST[ 'Action' ] = 'Delete';
		$result            = Ttprhd::find()->callSP( $_POST );
		if ( $result[ 'status' ] == 0 ) {
			return $this->responseSuccess( $result[ 'keterangan' ] );
		} else {
			return $this->responseFailed( $result[ 'keterangan' ] );
		}
	}
	public function actionCancel( $id, $action ) {
		/** @var Ttprhd $model */
		$model             = $this->findModelBase64( 'Ttprhd', $id );
		$_POST[ 'PRNo' ]   = $model->PRNo;
		$_POST[ 'PRJam' ]  = $_POST[ 'PRTgl' ] . ' ' . $_POST[ 'PRJam' ];
		$_POST[ 'Action' ] = 'Cancel';
		$result            = Ttprhd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'PRNo' ] == '--' ) {
			return $this->redirect( [ 'index' ] );
		} else {
			$model = Ttprhd::findOne( $result[ 'PRNo' ] );
			return $this->redirect( [ 'ttprhd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client            = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response          = $client->post( Yii::$app->params[ 'host_report' ] . '/aunit/fprhd', [
			'body' => Json::encode( $_POST )
		] );
        $html                 = $response->getBody();
        return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	public function actionCheckStock( $BrgKode, $LokasiKode ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return Tdbaranglokasi::find()
		                     ->where( 'BrgKode = :BrgKode AND LokasiKode = :LokasiKode', [ 'BrgKode' => $BrgKode, 'LokasiKode' => $LokasiKode ] )
		                     ->asArray()->one();
	}
	public function actionJurnal() {
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Jurnal';
		$result           = Ttprhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( [ 'ttprhd/update', 'action' => 'display', 'id' => base64_encode( $result[ 'PRNo' ] ) ] );
	}

    public function actionStatusOtomatis() {
        $this->layout   = 'popup-form';
        return $this->render( '_formStatus', [ 'data' => $_GET ] );
    }

    public function actionStatusOtomatisItem( $PRNo ) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $dt = General::cCmd( "SELECT KBMNo, KBMTgl, KBMLink, KBMBayar, Kode, Person, Memo,NoGL FROM vtKBM WHERE KBMLink = :PRNo ORDER BY KBMTgl;", [':PRNo' => $PRNo ] )->queryAll();
        return [
            'rows' => $dt
        ];

    }
}
