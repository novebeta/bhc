<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Tdbaranglokasi;
use aunit\models\Tdlokasi;
use aunit\models\Tttahd;
use aunit\models\Tttait;
use aunit\models\Tttihd;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TttahdController implements the CRUD actions for Tttahd model.
 */
class TttahdController extends AunitController {
	public function actions() {
        $where      = [
            [
                'op'        => 'AND',
                'condition' => "(TANo NOT LIKE '%=%')",
                'params'    => []
            ]
        ];
		return [
			'td' => [
				'class' => TdAction::class,
				'model' => Tttahd::class,
                'where' => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Tttahd models.
	 * @return mixed
	 */
	public function actionIndex() {
        $post[ 'Action' ] = 'Display';
        $result           = Tttahd::find()->callSP( $post );
        if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        }
        return $this->render( 'index', [
            'mode'    => '',
            'url_add' => Url::toRoute( [ 'twtahd/td' ] )
        ] );
	}
	/**
	 * Displays a single Tttahd model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Finds the Tttahd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Tttahd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */

	protected function findModel( $id ) {
		if ( ( $model = Tttahd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Tttahd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$LokasiKodeCmb       = Tdlokasi::find()->where( [ 'LokasiStatus' => 'A', 'LEFT(LokasiNomor,1)' => '7' ] )->orderBy( 'LokasiStatus, LokasiKode' )->one();
        $model               = new Tttahd();
        $model->TANo         = General::createTemporaryNo( 'TA', 'Tttahd', 'TANo' );
		$model->TATgl        = date( 'Y-m-d H:i:s' );
		$model->TAKeterangan = "--";
		$model->TATotal      = 0;
		$model->KodeTrans    = "--";
		$model->Cetak        = "Belum";
		$model->PosKode      = $this->LokasiKode();
		$model->LokasiKode   = ( $LokasiKodeCmb != null ) ? $LokasiKodeCmb->LokasiKode : '';
		$model->save();
        $post             = $model->attributes;
        $post[ 'Action' ] = 'Insert';
        $result           = Tttahd::find()->callSP( $post );
        Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTMutasi = Tttahd::find()->tttahdFillByNo()->where( [ 'tttahd.TANo' => $model->TANo ] )
		                   ->asArray( true )->one();
		$id        = $this->generateIdBase64FromModel( $model );
        $dsTMutasi[ 'TANoView' ] = $result[ 'TANo' ];
		return $this->render( 'create', [
			'model'     => $model,
			'dsTMutasi' => $dsTMutasi,
			'id'        => $id
		] );
	}
	/**
	 * Updates an existing Tttahd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
        $index = \yii\helpers\Url::toRoute( [ 'tttahd/index' ] );
        if ( ! isset( $_GET[ 'action' ] ) ) {
            return $this->redirect( $index );
        }
		/** @var Tttahd $model */
		$model     = $this->findModelBase64( 'Tttahd', $id );
		$dsTMutasi = Tttahd::find()->tttahdFillByNo()->where( [ 'tttahd.TANo' => $model->TANo ] )
		                   ->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post               = array_merge($dsTMutasi, \Yii::$app->request->post());
            $post[ 'TATgl' ]    = $post[ 'TATgl' ] . ' ' . $post[ 'TAJam' ];
			$post[ 'Action' ]   = 'Update';
            $result             = Tttahd::find()->callSP( $post );
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Tttahd::findOne( $result[ 'TANo' ] );
			$id                 = $this->generateIdBase64FromModel( $model );
            $post[ 'TANoView' ] = $result[ 'TANo' ];
            if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                return $this->redirect( [ 'tttahd/update',  'action' => 'display','id' => $id ] );
			} else {
				return $this->render( 'update', [
					'model'     => $model,
					'dsTMutasi' => $post,
					'id'        => $id,
				] );
			}
		}
		$dsTMutasi[ 'Action' ] = 'Load';
        $result                = Tttahd::find()->callSP( $dsTMutasi );
        Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        $dsTMutasi[ 'TANoView' ] = $result[ 'TANo' ];
        $dsTMutasi[ 'TANoView' ] = $_GET[ 'TANoView' ] ?? $result[ 'TANo' ];
		return $this->render( 'update', [
			'model'     => $model,
			'dsTMutasi' => $dsTMutasi,
			'id'        => $id
		] );
	}
	public function actionDelete() {
        /** @var Tttahd $model */
        $model              = $this->findModelBase64( 'Tttahd', $_POST[ 'id' ] );
        $_POST[ 'TANo' ]    = $model->TANo;
        $_POST[ 'Action' ]  = 'Delete';
        $result             = Tttahd::find()->callSP( $_POST );
        if ( $result[ 'status' ] == 0 ) {
            return $this->responseSuccess( $result[ 'keterangan' ] );
        } else {
            return $this->responseFailed( $result[ 'keterangan' ] );
        }
	}

	public function actionCancel( $id, $action ) {
        /** @var Tttahd $model */
        $model = $this->findModelBase64( 'Tttahd', $id );
        $_POST[ 'TANo' ]   = $model->TANo;
        $_POST[ 'TATgl' ] = $_POST[ 'TATgl' ].' '.$_POST[ 'TAJam' ];
        $_POST[ 'Action' ] = 'Cancel';
        $result            = Tttahd::find()->callSP( $_POST );
        Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        if($result[ 'TANo' ] == '--'){
            return $this->redirect(['index']);
        }else{
            $model = Tttahd::findOne($result[ 'TANo' ]);
            return $this->redirect( [ 'tttahd/update',  'action' => 'display','id' => $this->generateIdBase64FromModel( $model )] );
        }
	}

	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST         = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client        = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response      = $client->post( Yii::$app->params[ 'host_report' ] . '/aunit/ftahd', [
			'body' => Json::encode( $_POST )
		] );
//		return $response->getBody();
        $html                 = $response->getBody();
        return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	public function actionFillBarangAddEdit() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$union                       = "";
		if ( $requestData[ 'KodeTrans' ] == 'TC16' ) {
			$union = "UNION
			SELECT tdbarang.BrgKode, tdbarang.BrgNama, tdbarang.BrgBarCode, tdbarang.BrgGroup, tdbarang.BrgSatuan, tdbarang.BrgHrgBeli, tdbarang.BrgHrgJual, tdbarang.BrgRak1, tdbarang.BrgMinStock, tdbarang.BrgMaxStock, tdbarang.BrgStatus FROM tdbarang
			INNER JOIN tdbaranglokasi ON tdbaranglokasi.BrgKode = tdbarang.BrgKode
			WHERE Lokasikode LIKE '%'
			GROUP BY tdbarang.BrgKode HAVING SUM(SaldoQty) > 0 Order By BrgStatus, BrgKode";
		} elseif ( $requestData[ 'KodeTrans' ] == 'TC04' ) {
			$union = "UNION
			SELECT tdbarang.BrgKode, tdbarang.BrgNama, tdbarang.BrgBarCode, tdbarang.BrgGroup, tdbarang.BrgSatuan, tdbarang.BrgHrgBeli, tdbarang.BrgHrgJual, tdbarang.BrgRak1, tdbarang.BrgMinStock, tdbarang.BrgMaxStock, tdbarang.BrgStatus FROM tdbarang
			WHERE BrgStatus = 'A' Order By BrgStatus, BrgKode";
		}
		$barang = \common\components\General::cCmd( "SELECT tdbarang.BrgKode, tdbarang.BrgNama, tdbarang.BrgBarCode, tdbarang.BrgGroup, tdbarang.BrgSatuan,
       TAHrgBeli As BrgHrgBeli,  BrgHrgJual, tdbarang.BrgRak1, tdbarang.BrgMinStock, tdbarang.BrgMaxStock, tdbarang.BrgStatus FROM tdbarang
       INNER JOIN tdbaranglokasi ON tdbaranglokasi.BrgKode = tdbarang.BrgKode
       INNER JOIN ttTAit ON ttTAit.BrgKode = tdbarang.BrgKode
       WHERE ttTAit.TANo = :TANo
       GROUP BY tdbarang.BrgKode
       " . $union, [ ':TANo' => $requestData[ 'TANo' ] ] )->queryAll();
		return json_encode( $barang );
	}
}
