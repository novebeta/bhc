<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Ttin;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
/**
 * TtinController implements the CRUD actions for Ttin model.
 */
class TtinController extends AunitController {
	public function actions() {
		$colGrid    = null;
		$joinWith   = [
			'leasing'  => [
				'type' => 'INNER JOIN'
			],
			'sales'    => [
				'type' => 'INNER JOIN'
			],
			'customer' => [
				'type' => 'INNER JOIN'
			],
		];
		$join       = [];
		$where      = [];
		$sqlraw     = null;
		$queryRawPK = null;
		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			switch ( $r ) {
				case 'ttin/index':
					$colGrid                    = Ttin::colGrid();
					$joinWith[ 'dataKonsumen' ] = [
						'type' => 'LEFT OUTER JOIN'
					];
					$joinWith[ 'motorTypes' ]   = [
						'type' => 'LEFT OUTER JOIN'
					];
					break;
				case 'ttin/kas-masuk-inden':
					$colGrid = Ttin::colGridMutasiInternalPos();
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "LEFT(ttsmhd.SMNo,2) = 'MI'",
							'params'    => []
						]
					];
					$join    = [
						[
							'type'   => 'LEFT OUTER JOIN',
							'table'  => "(SELECT vtkm.KMLink, vtkm.KMJenis, SUM(vtkm.KMNominal) AS Inden FROM vtkm WHERE vtkm.KMJenis = 'Inden' AND vtkm.KMNo <> '\" & Number & \"' GROUP BY vtkm.KMLink, vtkm.KMJenis) ttkM",
							'on'     => 'ttin.INNo = ttkM.KMLINK',
							'params' => []
						]
					];
					break;
				case 'ttin/select-km-inden':
					$colGrid  = Ttin::colGridSelectKMInden();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$sqlraw     = " SELECT ttin.INNo, ttin.INTgl, ttin.SalesKode, ttin.CusKode, ttin.LeaseKode, ttin.INJenis, ttin.DKNo, ttin.INMemo, 
			         ttin.INDP, ttin.UserID, IFNULL(tdcustomer.CusNama, '--') AS CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, 
			         tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, IFNULL(tdcustomer.CusKabupaten, '--') AS CusKabupaten, tdcustomer.CusTelepon, 
			         IFNULL(tdsales.SalesNama, '--') AS SalesNama, IFNULL(tdleasing.LeaseNama,'--') AS LeaseNama, ttin.INHarga, ttin.MotorType, 
			         ttin.MotorWarna, IFNULL(Inden,0) AS IndenSudah,(ttin.INDP-IFNULL(Inden,0)) As Sisa  
			         FROM ttin 
			         INNER JOIN tdleasing ON ttin.LeaseKode = tdleasing.LeaseKode 
			         INNER JOIN tdsales ON ttin.SalesKode = tdsales.SalesKode 
			         INNER JOIN tdcustomer ON ttin.CusKode = tdcustomer.CusKode 
			         LEFT OUTER JOIN 
			           (SELECT vtkm.KMLink, vtkm.KMJenis, SUM(vtkm.KMNominal) AS Inden FROM vtkm 
			            WHERE vtkm.KMJenis = 'Inden' AND vtkm.KMNo <> '{$urlParams['KMNo']}' GROUP BY vtkm.KMLink, vtkm.KMJenis ) ttkM 
			         ON ttin.INNo = ttkM.KMLINK 
			         WHERE ((ttin.INDP - IFNULL(Inden,0)) > 0 OR ttin.INNo = '{$urlParams['KMLink']}' ) {$whereRaw}";
					$queryRawPK = [ 'INNo' ];
					break;
				case 'ttin/select-dk':
					$colGrid  = Ttin::colGrid();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$sqlraw     = "SELECT  ttin.INNo, ttin.INTgl, ttin.SalesKode, ttin.CusKode, ttin.LeaseKode, ttin.INJenis, ttin.INMemo, 
         ttin.INDP, ttin.UserID, IFNULL(tdcustomer.CusNama, '--') AS CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, 
         tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, IFNULL(tdcustomer.CusKabupaten, '--') AS CusKabupaten, tdcustomer.CusTelepon, tdcustomer.CusTelepon2, 
         IFNULL(tdsales.SalesNama, '--') AS SalesNama, IFNULL(tdleasing.LeaseNama,'--') AS LeaseNama, ttin.INHarga, ttin.MotorType, 
         ttin.MotorWarna FROM ttin 
         INNER JOIN tdleasing ON ttin.LeaseKode = tdleasing.LeaseKode 
         INNER JOIN tdsales ON ttin.SalesKode = tdsales.SalesKode 
         INNER JOIN tdcustomer ON ttin.CusKode = tdcustomer.CusKode 
         WHERE (ttin.DKNo LIKE '--%' OR ttIN.DKNo = '{$urlParams['DKNo']}'  ) {$whereRaw}";
					$queryRawPK = [ 'INNo' ];
					break;
			}
		}
		return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Ttin::class,
				'queryRaw'   => $sqlraw,
				'queryRawPK' => $queryRawPK,
				'colGrid'    => $colGrid,
				'joinWith'   => $joinWith,
				'join'       => $join,
				'where'      => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttin models.
	 * @return mixed
	 */
	public function actionIndex() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttin::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'index', [
			'mode' => ''
		] );
	}
	public function actionSelectKmInden() {
		$this->layout = 'select-mode';
		return $this->render( 'select', [
			'title'   => 'Kas Masuk - UM Inden',
			'arr'     => [
				'cmbTxt'    => [
					'tdcustomer.CusNama' => 'Nama Konsumen',
					'INNo'               => 'No Inden',
					'CusKode'            => 'Kode Konsumen',
					'SalesKode'          => 'Kode Sales',
					'LeaseKode'          => 'Leasing',
					'INMemo'             => 'Keterangan',
					'INJenis'            => 'Jenis Inden',
					'UserID'             => 'User ID',
				],
				'cmbTgl'    => [
					'INTgl' => 'Tanggal IN'
				],
				'cmbNum'    => [
					'INDP'    => 'DP Inden',
					'INHarga' => 'Harga OTR',
				],
				'sortname'  => "INTgl",
				'sortorder' => 'desc'
			],
			'options' => [
				'colGrid' => Ttin::colGridSelectKMInden(),
				'mode'    => self::MODE_SELECT,
			]
		] );
	}
	public function actionSelectDk() {
		$this->layout = 'select-mode';
		return $this->render( 'select', [
			'title'   => 'Data Konsumen',
			'arr'     => [
				'cmbTxt'    => [
					'CusNama' => 'Nama Konsumen',
					'INNo'               => 'No Inden',
					'CusKode'            => 'Kode Konsumen',
					'SalesKode'          => 'Kode Sales',
					'LeaseKode'          => 'Leasing',
					'INMemo'             => 'Keterangan',
					'INJenis'            => 'Jenis Inden',
					'UserID'             => 'User ID',
				],
				'cmbTgl'    => [
					'INTgl' => 'Tanggal IN'
				],
				'cmbNum'    => [
					'INDP'    => 'DP Inden',
					'INHarga' => 'Harga OTR',
				],
				'sortname'  => "INTgl",
				'sortorder' => 'desc'
			],
			'options' => [
				'colGrid' => Ttin::colGrid(),
				'mode'    => self::MODE_SELECT,
			]
		] );
	}
	/**
	 * Finds the Ttin model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttin the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttin::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttin model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		TUi::$actionMode   = Menu::ADD;
		$model             = new Ttin();
		$model->INNo       = General::createTemporaryNo( 'IN', 'Ttin', 'INNo' );
		$model->INTgl      = date( 'Y-m-d' );
		$model->INDP       = 0;
		$model->INHarga    = 0;
		$model->SalesKode  = '--';
		$model->CusKode    = '--';
		$model->LeaseKode  = '--';
		$model->DKNo       = '--';
		$model->INJenis    = 'Tunai';
		$model->INMemo     = '--';
		$model->MotorType  = '--';
		$model->MotorWarna = '--';
		$model->UserID     = $this->UserID();
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttin::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsJual                = $model->find()->dsTJual()->where( " (ttin.INNo = :INNo)", [ ':INNo' => $model->INNo, ] )->asArray( true )->one();
		$dsJual[ 'INNoView' ]  = $result[ 'INNo' ];
		$dsJual[ 'CusTglLhr' ] = Yii::$app->formatter->asDate( '-9000 day', 'yyyy-MM-dd' );
		$dsJual[ 'INDP' ]      = 0;
		$dsJual[ 'CusKode' ]   = '--';
		$id                    = $this->generateIdBase64FromModel( $model );
		return $this->render( 'create', [
			'model'  => $model,
			'dsJual' => $dsJual,
			'id'     => $id
		] );
	}
	/**
	 * Updates an existing Ttin model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$index = \yii\helpers\Url::toRoute( [ 'ttin/index' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttin $model */
		$model  = $this->findModelBase64( 'Ttin', $id );
		$dsJual = $model->find()->dsTJual()->where( " (ttin.INNo = :INNo)", [ ':INNo' => $model->INNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post                   = Yii::$app->request->post();
			$post[ 'Action' ]       = 'Update';
			$post[ 'CusNama' ]      = ucwords( strtolower( $post[ 'CusNama' ] ) );
			$post[ 'CusAlamat' ]    = ucwords( strtolower( $post[ 'CusAlamat' ] ) );
			$post[ 'CusTempatLhr' ] = ucwords( strtolower( $post[ 'CusTempatLhr' ] ) );
			$result                 = Ttin::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model = Ttin::findOne( $result[ 'INNo' ] );
			$id    = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				$post[ 'INNoView' ] = $result[ 'INNo' ];
				return $this->redirect( [ 'ttin/update', 'id' => $id, 'action' => 'display' ] );
			} else {
				return $this->render( 'update', [
					'model'  => $model,
					'dsJual' => $post,
					'id'     => $id,
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsJual[ 'Action' ] = 'Load';
			$result             = Ttin::find()->callSP( $dsJual );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'INNo' ] = $dsJual[ 'INNo' ];
		}
		$dsJual[ 'INNoView' ] = $_GET[ 'INNoView' ] ?? $result[ 'INNo' ];
		return $this->render( 'update', [
			'model'  => $model,
			'dsJual' => $dsJual,
			'id'     => $id
		] );
	}
	/**
	 * Deletes an existing Ttin model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */

    public function actionDelete()
    {
		return $this->deleteWithSP(['modelName'=>'Ttin']);
        /** @var Ttin $model */
        // $request = Yii::$app->request;
        // if ($request->isAjax){
        //     $model = $this->findModelBase64('Ttin', $_POST['id']);
        //     $_POST['INNo'] = $model->INNo;
        //     $_POST['Action'] = 'Delete';
        //     $result = Ttin::find()->callSP($_POST);
        //     if ($result['status'] == 0) {
        //         return $this->responseSuccess($result['keterangan']);
        //     } else {
        //         return $this->responseFailed($result['keterangan']);
        //     }
        // }else{
        //     $id = $request->get('id');
        //     $model = $this->findModelBase64('Ttin', $id);
        //     $_POST['INNo'] = $model->INNo;
        //     $_POST['Action'] = 'Delete';
        //     $result = Ttin::find()->callSP($_POST);
        //     Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        //     if ($result['INNo'] == '--') {
        //         return $this->redirect(['index']);
        //     } else {
        //         $model = Ttin::findOne($result['INNo']);
        //         return $this->redirect(['ttin/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
        //     }
        // }
    }
	/**
	 * Lists all Ttfb models.
	 * @return mixed
	 */
	public function actionSelect() {
		$this->layout = 'select-mode';
		return $this->render( 'index', [
			'mode' => self::MODE_SELECT
		] );
	}
	public function actionLoop() {
		$_POST[ 'Action' ] = 'Loop';
		$result            = Ttin::find()->callSP( $_POST );
//		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        if($result[ 'status' ] == 1){
            Yii::$app->session->addFlash( 'warning', $result[ 'keterangan' ] );
        }
	}
	public function actionCancel( $id, $action ) {
		$_POST[ 'Action' ] = 'Cancel';
		$result            = Ttin::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if($result[ 'INNo' ] == '--'){
			return $this->redirect(['index']);
		}else{
			$model = Ttin::findOne($result[ 'INNo' ]);
			return $this->redirect( [ 'ttin/update',  'action' => 'display','id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST         = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client        = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response      = $client->post( Yii::$app->params[ 'host_report' ] . '/aunit/fin', [
			'body' => Json::encode( $_POST )
		] );
		$html          = $response->getBody();
		return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
}
