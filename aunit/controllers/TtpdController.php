<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Tddealer;
use aunit\models\Tmotor;
use aunit\models\Ttpd;
use aunit\models\Ttss;
use common\components\Custom;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\base\UserException;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
/**
 * TtpdController implements the CRUD actions for Ttpd model.
 */
class TtpdController extends AunitController {
	public function actions() {
		$colGrid  = null;
		$joinWith = [
			'dealer' => [
				'type' => 'INNER JOIN'
			],
			'lokasi' => [
				'type' => 'INNER JOIN'
			]
		];
		$join     = [];
		$where    = [
			[
				'op'        => 'AND',
				'condition' => "LEFT(PDNo,2) = 'PD'",
				'params'    => []
			]
		];
		if ( isset( $_POST[ 'query' ] ) ) {
			$r = Json::decode( $_POST[ 'query' ], true )[ 'r' ];
			switch ( $r ) {
				case 'ttpd/penerimaan-dealer':
                    $where      = [
                        [
                            'op'        => 'AND',
                            'condition' => "(PDNo NOT LIKE '%=%')",
                            'params'    => []
                        ]
                    ];
					$colGrid = Ttpd::colGridPenerimaanDealer();
					break;
				case 'ttpd/invoice-penerimaan':
                    $where      = [
                        [
                            'op'        => 'AND',
                            'condition' => "(PDNo NOT LIKE '%=%')",
                            'params'    => []
                        ]
                    ];
					$colGrid = Ttpd::colGridInvoicePenerimaan();
					break;
			}
		}
		return [
			'td' => [
				'class'    => TdAction::class,
				'model'    => Ttpd::class,
				'colGrid'  => $colGrid,
				'joinWith' => $joinWith,
				'where'    => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttpd models.
	 * @return mixed
	 */
	public function actionPenerimaanDealer() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttpd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'PenerimaanDealer' );
	}
	/**
	 * Lists all Ttpd models.
	 * @return mixed
	 */
	public function actionInvoicePenerimaan() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttpd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'InvoicePenerimaan' );
	}
	/**
	 * Displays a single Ttpd model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Finds the Ttpd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttpd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttpd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttpd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		TUi::$actionMode   = Menu::ADD;
		$model             = new Ttpd();
		$model->PDNo       = General::createTemporaryNo( "PD", 'Ttpd', 'PDNo' );
		$model->PDTgl      = date( 'Y-m-d' );
		$model->PDJam      = date( "Y-m-d H:i:s" );
		$model->SDNo       = '--';
		$model->PDMemo     = '--';
		$model->LokasiKode = $this->LokasiKode();
		$model->DealerKode = ( Tddealer::find()->one() )->DealerKode;
		$model->PDTotal    = 0;
		$model->UserID     = $this->UserID();
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttpd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsBeli               = Ttpd::find()->dsTMutasiTtpd()->where( [ 'ttpd.PDNo' => $model->PDNo ] )->asArray( true )->one();
		$id                   = $this->generateIdBase64FromModel( $model );
		$dsBeli[ 'PDNoView' ] = $result[ 'PDNo' ];
		return $this->render( 'create', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'jenis'  => "MutasiEksternal",
			'id'     => $id
		] );
	}
	/**
	 * Updates an existing Ttpd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \yii\db\Exception
	 */
	public function actionUpdate( $id, $jenis ) {
		if ( $jenis == 'MutasiEksternal' ) {
			$route = 'ttpd/penerimaan-dealer';
		} else {
			$route = 'ttpd/invoice-penerimaan';
		}
		$index = \yii\helpers\Url::toRoute( [ $route ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
				TUi::$actionMode = Menu::ADD;
				break;
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttpd $model */
		$model  = $this->findModelBase64( 'Ttpd', $id );
		$dsBeli = Ttpd::find()->dsTMutasiTtpd()->where( [ 'ttpd.PDNo' => $model->PDNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post        = Yii::$app->request->post();
			$post[ 'PDJam' ]        = $post[ 'PDTgl' ] . ' '.$post[ 'PDJam' ]  ;
			$post[ 'LokasiKodePD' ] = $post[ 'LokasiKode' ];
			$post[ 'Action' ]       = 'Update';
			$result                 = Ttpd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttpd::findOne( $result[ 'PDNo' ] );
            if ( $result[ 'PDNo' ] == '--' || $model == null) {
                if ( $model === null ) {
                    Yii::$app->session->addFlash( 'warning', 'PDNo : ' . $result[ 'PDNo' ]. ' tidak ditemukan.' );
                }
                return $this->redirect( $index);
            }
			$post[ 'PDNoView' ] = $result[ 'PDNo' ];
			$id                 = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				return $this->redirect( [ 'ttpd/update', 'id' => $id, 'action' => 'display', 'jenis' => $jenis ] );
			} else {
				return $this->render( 'update', [
					'model'  => $model,
					'dsBeli' => $post,
					'jenis'  => $jenis,
					'id'     => $id,
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsBeli[ 'Action' ] = 'Load';
			$result             = Ttpd::find()->callSP( $dsBeli );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		$dsBeli[ 'PDNoView' ] = $_GET[ 'PDNoView' ] ??  $result[ 'PDNo' ];
		return $this->render( 'update', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'jenis'  => $jenis,
			'id'     => $id
		] );
	}
	/**
	 * Deletes an existing Ttpd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
    public function actionDelete()
    {
        /** @var Ttpd $model */
        $request = Yii::$app->request;
        if ($request->isAjax){
            $model = $this->findModelBase64('Ttpd', $_POST['id']);
            $_POST['PDNo'] = $model->PDNo;
            $_POST['Action'] = 'Delete';
            $result = Ttpd::find()->callSP($_POST);
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            $id = $request->get('id');
            $model = $this->findModelBase64('Ttpd', $id);
            $_POST['PDNo'] = $model->PDNo;
            $_POST['Action'] = 'Delete';
            $result = Ttpd::find()->callSP($_POST);
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['PDNo'] == '--') {
                return $this->redirect(['index']);
            } else {
                $model = Ttpd::findOne($result['PDNo']);
                return $this->redirect(['ttpd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model),'jenis' => $_GET['jenis']]);
            }
        }
    }

	public function actionDetail() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = [ 'tmotor.MotorAutoN', 'tmotor.MotorNoMesin' ];
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
			/** @var Tmotor $model */
			$model = null;
			if ( isset( $requestData[ 'id' ] ) ) {
				$model = $this->findModelBase64( 'Tmotor', $requestData[ 'id' ] );
			}
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'MotorAutoN' ] = 0;
						$requestData[ 'Action' ]     = 'Insert';
					} else {
						$requestData[ 'Action' ] = 'Update';
						if ( $model != null ) {
							$requestData[ 'MotorAutoNOLD' ]   = $model->MotorAutoN;
							$requestData[ 'MotorNoMesinOLD' ] = $model->MotorNoMesin;
						}
					}
					$result = Tmotor::find()->callSP_PD( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'batch' :
					\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
					$respon                      = $this->responseFailed( 'Add Item' );
					$base64                      = urldecode( base64_decode( $requestData[ 'header' ] ) );
					parse_str( $base64, $header );
					$header[ 'PDJam' ] = $header[ 'PDTgl' ]. ' '.$header[ 'PDJam' ];
					$header[ 'Action' ]          = 'Loop';
					$result                      = Ttpd::find()->callSP( $header );
					Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
					$KKNominal                   = 0;
					foreach ( $requestData[ 'data' ] as $item ) {
						$items[ 'Action' ]        = 'Loop';
						$items[ 'PDNo' ]          = $header[ 'PDNo' ];
						$items[ 'MotorType' ]     = $item['data'][ 'MotorType' ];
						$items[ 'MotorTahun' ]    = $item['data'][ 'MotorTahun' ];
						$items[ 'MotorWarna' ]    = $item['data'][ 'MotorWarna' ];
						$items[ 'MotorNoMesin' ]  = $item['data'][ 'MotorNoMesin' ];
						$items[ 'MotorNoRangka' ] = $item['data'][ 'MotorNoRangka' ];
						$items[ 'MotorAutoN' ]    = $item['data'][ 'MotorAutoN' ];
						$items[ 'SSHarga' ]       = $item['data'][ 'SDHarga' ];
						$result                      =  Tmotor::find()->callSP_PD( $items );
						Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
					}
					$header[ 'Action' ]          = 'LoopAfter';
					$result                      = Ttpd::find()->callSP( $header );
					Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
					return $this->responseSuccess( $requestData );
					break;
				case 'del'  :
					$requestData[ 'Action' ] = 'Delete';
					if ( $model != null ) {
						$requestData = array_merge( $requestData, $model->attributes);
						$requestData[ 'MotorAutoNOLD' ]   = $model->MotorAutoN;
						$requestData[ 'MotorNoMesinOLD' ] = $model->MotorNoMesin;
					}
					$result = Tmotor::find()->callSP_PD( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
			}
		} else {
			/* operation : read */
			$query = ( new \yii\db\Query() );
			if ( isset( $_GET[ 'jenis' ] ) ) {
				switch ( $_GET[ 'jenis' ] ) {
					case 'MutasiEksternal':
					case 'InvoicePenerimaan':
						$query = ( new \yii\db\Query() )
							->select( new Expression(
								'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
								tmotor.BPKBNo, tmotor.BPKBTglAmbil, tmotor.BPKBTglJadi, tmotor.DKNo, tmotor.DealerKode, tmotor.FBHarga, tmotor.FBNo, tmotor.FakturAHMNo, 
                         tmotor.FakturAHMTgl, tmotor.FakturAHMTglAmbil, tmotor.MotorAutoN, tmotor.MotorMemo, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorTahun, 
                         tmotor.MotorType, tmotor.MotorWarna, tmotor.PDNo, tmotor.PlatNo, tmotor.PlatTgl, tmotor.PlatTglAmbil, tmotor.SDNo, tmotor.SKNo, tmotor.SSNo, tmotor.STNKAlamat, 
                         tmotor.STNKNama, tmotor.STNKNo, tmotor.STNKTglAmbil, tmotor.STNKTglBayar, tmotor.STNKTglJadi, tmotor.STNKTglMasuk, tdmotortype.MotorNama, 
                         tmotor.FakturAHMNilai, tmotor.RKNo, tmotor.RKHarga, tmotor.SKHarga, tmotor.SDHarga, tmotor.MMHarga, tmotor.SSHarga' ) )
							->from( 'tmotor' )
							->join( 'LEFT OUTER JOIN', 'tdmotortype', 'tmotor.MotorType = tdmotortype.MotorType' )
							->where( [
								'PDNo' => $requestData[ 'PDNo' ]
							] )
							->orderBy( 'tmotor.MotorType, tmotor.MotorTahun, tmotor.MotorWarna' );
						break;
				}
			}
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST         = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client        = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response      = $client->post( Yii::$app->params[ 'host_report' ] . '/aunit/fpd', [
			'body' => Json::encode( $_POST )
		] );
		$html          = $response->getBody();
		return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	public function actionCancel( $id, $action, $index ) {
		/** @var Ttpd $model */
		$model             = $this->findModelBase64( 'Ttpd', $id );
		$_POST[ 'PDJam' ]  = $_POST[ 'PDTgl' ] . ' ' . $_POST[ 'PDJam' ];
		$_POST[ 'Action' ] = 'Cancel';
		$result            = Ttpd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'PDNo' ] == '--' ) {
			return $this->redirect( [ base64_decode( $index ) ] );
		} else {
			$model = Ttpd::findOne( $result[ 'PDNo' ] );
			return $this->redirect( [ 'ttpd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ), 'jenis' => $_GET[ 'jenis' ] ] );
		}
	}
	public function actionJurnal( $jenis ) {
		$_POST[ 'PDJam' ]  = $_POST[ 'PDTgl' ] . ' ' . $_POST[ 'PDJam' ];
		$_POST[ 'Action' ] = 'Jurnal';
		$result            = Ttpd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( [ 'ttpd/update', 'action' => 'display', 'id' => base64_encode( $result[ 'PDNo' ] ), 'jenis' => $jenis ] );
	}
}
