<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\models\Tdbaranglokasi;
use aunit\models\Tddealer;
use aunit\models\Tdkaryawan;
use aunit\models\Tdlokasi;
use aunit\models\Tdprogramhd;
use aunit\models\Ttsihd;
use aunit\models\Vtkbm;
use common\components\General;
use common\models\User;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtsihdController implements the CRUD actions for Ttsihd model.
 */
class TtsihdController extends AunitController {
	public function actions() {
//		$user       = User::findOne( Yii::$app->user->id );
		$join       = [];
		$where      = [];
		$sqlraw     = null;
		$colGrid    = null;
		$queryRawPK = null;
		$joinWith   = [
			'customer' => [
				'type' => 'INNER JOIN'
			]
		];
		if ( \aunit\components\Menu::showOnlyHakAkses(['warehouse','gudang']) ) {
			$joinWith[ 'detil' ] = [
				'type' => 'INNER JOIN'
			];
		}
		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			switch ( $r ) {
				case 'ttsihd/invoice-penjualan':
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$sqlraw     = "SELECT  twsihd.SINo, twsihd.SITgl, twsihd.LokasiKode, twsihd.KarKode, twsihd.SIKeterangan, twsihd.SISubTotal, twsihd.SIDiscFinal, 
					twsihd.SITotalPajak, twsihd.SIBiayaKirim, twsihd.SITotal, twsihd.UserID, twsihd.MotorNoPolisi, 
					tdcustomer.CusNama, MotorType AS MotorNama, MotorWarna, MotorNoMesin, twsihd.SONo, twsihd.SDNo, twsihd.SIJenis, 
					twsihd.PosKode, twsihd.KodeTrans, twsihd.PrgNama, twsihd.DealerKode, twsihd.SIUangMuka, twsihd.SITOP, twsihd.SITerbayar, twsihd.NoGL, twsihd.Cetak,
					IF(SITotal>SITerbayar,'Belum','Lunas') AS STATUS
					FROM  twsihd
    INNER JOIN tmotor   ON twsihd.MotorNoPolisi = tmotor.PlatNo
    INNER JOIN ttdk   ON ttdk.DKNo = tmotor.DKNo
    INNER JOIN tdcustomer   ON tdcustomer.CusKode = tdcustomer.CusKode
			        WHERE SINo NOT LIKE '%=%' AND KodeTrans IN ('TC12','TC17','TC20','TC21','TC22','TC23')";
					if(\aunit\components\Menu::showOnlyHakAkses(['warehouse','gudang'])){
						$sqlraw     = "SELECT  twsihd.SINo, twsihd.SITgl, twsihd.LokasiKode, twsihd.KarKode, twsihd.SIKeterangan, twsihd.SISubTotal, twsihd.SIDiscFinal, 
					twsihd.SITotalPajak, twsihd.SIBiayaKirim, twsihd.SITotal, twsihd.UserID, twsihd.MotorNoPolisi, 
					tdcustomer.CusNama, MotorType AS MotorNama, MotorWarna, MotorNoMesin, twsihd.SONo, twsihd.SDNo, twsihd.SIJenis, 
					twsihd.PosKode, twsihd.KodeTrans, twsihd.PrgNama, twsihd.DealerKode, twsihd.SIUangMuka, twsihd.SITOP, twsihd.SITerbayar, twsihd.NoGL, twsihd.Cetak,
					IF(SITotal>SITerbayar,'Belum','Lunas') AS STATUS
					FROM  twsihd
    INNER JOIN tmotor   ON twsihd.MotorNoPolisi = tmotor.PlatNo
    INNER JOIN ttdk   ON ttdk.DKNo = tmotor.DKNo
    INNER JOIN tdcustomer   ON tdcustomer.CusKode = tdcustomer.CusKode
			        WHERE SINo NOT LIKE '%=%' AND KodeTrans IN ('TC12','TC17','TC20','TC21','TC22','TC23')";
					}
					$queryRawPK = [ 'SINo' ];
					break;
				case 'ttsihd/penjualan-internal':
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$sqlraw     = "SELECT  twsihd.SINo, twsihd.SITgl, twsihd.LokasiKode, twsihd.KarKode, twsihd.SIKeterangan, twsihd.SISubTotal, twsihd.SIDiscFinal, 
					twsihd.SITotalPajak, twsihd.SIBiayaKirim, twsihd.SITotal, twsihd.UserID, twsihd.MotorNoPolisi, 
					tdcustomer.CusNama, MotorType AS MotorNama, MotorWarna, MotorNoMesin, twsihd.SONo, twsihd.SDNo, twsihd.SIJenis, 
					twsihd.PosKode, twsihd.KodeTrans, twsihd.PrgNama, twsihd.DealerKode, twsihd.SIUangMuka, twsihd.SITOP, twsihd.SITerbayar, twsihd.NoGL, twsihd.Cetak,
					IF(SITotal>SITerbayar,'Belum','Lunas') AS STATUS
					FROM  twsihd
    INNER JOIN tmotor   ON twsihd.MotorNoPolisi = tmotor.PlatNo
    INNER JOIN ttdk   ON ttdk.DKNo = tmotor.DKNo
    INNER JOIN tdcustomer   ON tdcustomer.CusKode = tdcustomer.CusKode
			        WHERE SINo NOT LIKE '%=%' AND KodeTrans IN ('TC12','TC17','TC20','TC21','TC22','TC23')";
					if(\aunit\components\Menu::showOnlyHakAkses(['warehouse','gudang'])){
						$sqlraw     = "SELECT  twsihd.SINo, twsihd.SITgl, twsihd.LokasiKode, twsihd.KarKode, twsihd.SIKeterangan, twsihd.SISubTotal, twsihd.SIDiscFinal, 
					twsihd.SITotalPajak, twsihd.SIBiayaKirim, twsihd.SITotal, twsihd.UserID, twsihd.MotorNoPolisi, 
					tdcustomer.CusNama, MotorType AS MotorNama, MotorWarna, MotorNoMesin, twsihd.SONo, twsihd.SDNo, twsihd.SIJenis, 
					twsihd.PosKode, twsihd.KodeTrans, twsihd.PrgNama, twsihd.DealerKode, twsihd.SIUangMuka, twsihd.SITOP, twsihd.SITerbayar, twsihd.NoGL, twsihd.Cetak,
					IF(SITotal>SITerbayar,'Belum','Lunas') AS STATUS
					FROM  twsihd
    INNER JOIN tmotor   ON twsihd.MotorNoPolisi = tmotor.PlatNo
    INNER JOIN ttdk   ON ttdk.DKNo = tmotor.DKNo
    INNER JOIN tdcustomer   ON tdcustomer.CusKode = tdcustomer.CusKode
			        WHERE SINo NOT LIKE '%=%' AND KodeTrans IN ('TC12','TC17','TC20','TC21','TC22','TC23')";
					}
					$queryRawPK = [ 'SINo' ];
					break;
				case 'ttsihd/select':
					$colGrid  = Ttsihd::colGridPick();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$sqlraw     = "SELECT ttSIhd.*, tdcustomer.CusNama
                      FROM ttSIhd 
                      INNER JOIN ttSIit ON ttSIhd.SINo = ttSIit.SINo 
                      INNER JOIN  tdcustomer ON tdcustomer.MotorNoPolisi = ttSIhd.MotorNoPolisi
                      WHERE ttSIhd.SINo NOT LIKE '%=%' AND  SITotal = SITerbayar {$whereRaw}
                      GROUP BY ttSIhd.SINo ORDER BY ttSIhd.SINo";
					$queryRawPK = [ 'SINo' ];
					break;
			}
		}
		return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Ttsihd::class,
				'joinWith'   => $joinWith,
				'where'      => $where,
				'queryRaw'   => $sqlraw,
				'queryRawPK' => $queryRawPK,
				'colGrid'    => $colGrid,
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionSelect() {
		$urlDetail    = Url::toRoute( [ 'ttsiit/index', 'jenis' => 'FillItemSISR' ] );
		$this->layout = 'select-mode';
		return $this->render( 'select',
			[
				'arr'       => [
					'cmbTxt'    => [
						'SINo'          => 'No Jual',
						'MotorNoPolisi' => 'No Polisi',
						'CusNama'       => 'Nama Konsumen',
						'MotorNama'     => 'Nama Motor',
						'LokasiKode'    => 'Lokasi',
						'KarKode'       => 'Kode Karyawan',
						'SIKeterangan'  => 'Keterangan',
						'Cetak'         => 'Cetak',
						'UserID'        => 'User',
					],
					'cmbTgl'    => [
						'SITgl' => 'Tgl SI',
					],
					'cmbNum'    => [
						'SITotal' => 'Total',
					],
					'sortname'  => "SITgl",
					'sortorder' => 'desc'
				],
				'title'     => 'Daftar Sales Invoice / Invoice Penjualan',
				'options'   => [ 'colGrid' => Ttsihd::colGridPick(), 'mode' => self::MODE_SELECT_DETAIL_MULTISELECT ],
				'urlDetail' => $urlDetail
			] );
	}
	/**
	 * Lists all Ttsihd models.
	 * @return mixed
	 */
	public function actionInvoicePenjualan() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'Jenis' ]  = 'TC12';
			$result           = Ttsihd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'InvoicePenjualan', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'twsihd/td' ] )
		] );
	}
	/**
	 * Lists all Ttsihd models.
	 * @return mixed
	 */
	public function actionPenjualanInternal() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$post[ 'Jenis' ]  = 'TC15';
			$result           = Ttsihd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'PenjualanInternal', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttsihd/td' ] )
		] );
	}
	/**
	 * Creates a new Ttsihd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param $jenis
	 *
	 * @return mixed
	 */
	public function actionInvoicePenjualanCreate() {
		return $this->create( 'TC12', 'invoice-penjualan', 'Invoice Penjualan' );
	}
	public function actionPenjualanInternalCreate() {
		return $this->create( 'TC15', 'penjualan-internal', 'Penjualan Internal' );
	}
	public function actionInvoicePenjualanUpdate( $id ) {
		return $this->update( 'TC12', 'invoice-penjualan', 'Invoice Penjualan', $id );
	}
	public function actionPenjualanInternalUpdate( $id ) {
		return $this->update( 'TC15', 'penjualan-internal', 'Penjualan Internal', $id );
	}
	public function create( $jenis, $view, $title ) {
		$LokasiKodeCmb        = Tdlokasi::find()->where( "LokasiStatus = 'A' or LokasiStatus = '1'" )->orderBy( 'LokasiStatus, LokasiKode' )->one();
		$DealerKodeCmb        = Tddealer::find()->where( "DealerStatus = 'A' or DealerStatus = '1'" )->orderBy( 'DealerStatus, DealerKode' )->one();
		$KarKodeCmb           = Tdkaryawan::find()->where( [ 'KarStatus' => 'A' ] )->orderBy( 'KarStatus,KarKode' )->one();
		$PrgNamaCmb           = Tdprogramhd::find()->where( "PrgTglMulai <= :tgl AND PrgTglAkhir >= :tgl", [
			':tgl' => date( 'Y-m-d' )
		] )->orderBy( 'PrgNama, PrgTglMulai DESC' )->one();
		$model                = new Ttsihd();
		$model->SINo          = General::createTemporaryNo( 'SI', 'Ttsihd', 'SINo' );
		$model->SITgl         = date( 'Y-m-d H:i:s' );
		$model->SONo          = "--";
		$model->SDNo          = "--";
		$model->KarKode       = ( $KarKodeCmb != null ) ? $KarKodeCmb->KarKode : '--';
		$model->MotorNoPolisi = "UMUM";
		$model->SIKeterangan  = "";
		$model->LokasiKode    = ( $LokasiKodeCmb != null ) ? $LokasiKodeCmb->LokasiKode : '--';
		$model->SISubTotal    = 0;
		$model->SIDiscFinal   = 0;
		$model->SITotalPajak  = 0;
		$model->SIBiayaKirim  = 0;
		$model->SITotal       = 0;
		$model->SIUangMuka    = 0;
		$model->UserID        = $this->UserID();
		$model->NoGL          = "--";
		$model->KodeTrans     = $jenis;
		$model->PosKode       = $this->LokasiKode();
		$model->Cetak         = "Belum";
		$model->PrgNama       = ( $PrgNamaCmb != null ) ? $PrgNamaCmb->PrgNama : '--';
		$model->SIJenis       = 'Tunai';
		$model->SITOP         = 0;
		$model->DealerKode    = ( $DealerKodeCmb != null ) ? $DealerKodeCmb->DealerKode : '--';
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttsihd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTJual                   = Ttsihd::find()->ttsihdFillByNo()->where( [ 'ttsihd.SINo' => $model->SINo ] )
		                                   ->asArray( true )->one();
		$dsTJual[ 'StatusBayar' ]  = 'BELUM';
		$dsTJual[ 'UangMuka' ]     = 0;
		$dsTJual[ 'SITotalPajak' ] = 0;
		$dsTJual[ 'SIBiayaKirim' ] = 0;
		$dsTJual[ 'SIDiscPersen' ] = 0;
		$dsTJual[ 'SOTgl' ]        = date( 'Y-m-d H:i:s' );
		$id                        = $this->generateIdBase64FromModel( $model );
		$dsTJual[ 'SINoView' ]     = $result[ 'SINo' ];
		return $this->render( 'create', [
			'model'   => $model,
			'dsTJual' => $dsTJual,
			'id'      => $id,
			'jenis'   => $jenis,
			'view'    => $view,
			'title'   => $title
		] );
	}
	/**
	 * Updates an existing Ttsihd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 */
	public function update( $jenis, $view, $title, $id ) {
		$index = Ttsihd::getRoute( $jenis, [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		/** @var Ttsihd $model */
		/** @var Ttsihd $dsTJual */
		$model    = $this->findModelBase64( 'Ttsihd', $id );
		$dsTJual  = Ttsihd::find()->ttsihdFillByNo()->where( [ 'ttsihd.SINo' => $model->SINo ] )
		                  ->asArray( true )->one();
		$Terbayar = floatval($dsTJual['SITerbayar']);
		if ( Yii::$app->request->isPost ) {
			$post                = array_merge( $dsTJual, \Yii::$app->request->post() );
			$post[ 'SITgl' ]     = $post[ 'SITgl' ] . ' ' . $post[ 'SIJam' ];
			$post[ 'Action' ]    = 'Update';
			$result              = Ttsihd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttsihd::findOne( $result[ 'SINo' ] );
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'SINoView' ] = $result[ 'SINo' ];
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				return $this->redirect( Ttsihd::getRoute( $jenis, [ 'jenis' => $jenis, 'view' => $view, 'title' => $title, 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'update', [
					'model'   => $model,
					'dsTJual' => $post,
					'id'      => $id,
					'jenis'   => $jenis,
					'view'    => $view,
					'title'   => $title
				] );
			}
		}
		if ( ( floatval( $dsTJual[ 'SITotal' ] ) - $Terbayar ) > 0 || floatval( $dsTJual[ 'SITotal' ] ) == 0 ) {
			$dsTJual[ 'StatusBayar' ] = 'BELUM';
		} else if ( ( floatval( $dsTJual[ 'SITotal' ] ) - $Terbayar ) <= 0 ) {
			$dsTJual[ 'StatusBayar' ] = 'LUNAS';
		}
		$dsTJual[ 'SisaBayar' ] = floatval( $dsTJual[ 'SITotal' ] ) - $Terbayar;
		$dsTJual[ 'Action' ]    = 'Load';
//		$dsTJual[ 'KodeTrans' ] = $jenis;
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$result = Ttsihd::find()->callSP( $dsTJual );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$dsTJual[ 'SINoView' ] = $result[ 'SINo' ];
		} else {
			$result[ 'SINo' ] = $dsTJual[ 'SINo' ];
		}
		$dsTJual[ 'SINoView' ] = $_GET[ 'SINoView' ] ?? $result[ 'SINo' ];
		return $this->render( 'update', [
			'model'   => $model,
			'dsTJual' => $dsTJual,
			'id'      => $id,
			'jenis'   => $jenis,
			'view'    => $view,
			'title'   => $title
		] );
	}
	/**
	 * Deletes an existing Ttsihd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @param $jenis
	 *
	 * @return mixed
	 */
	public function actionDelete() {
		$model             = $model = $this->findModelBase64( 'Ttsihd', $_POST[ 'id' ] );
		$_POST[ 'SINo' ]   = $model->SINo;
		$_POST[ 'Action' ] = 'Delete';
		$result            = Ttsihd::find()->callSP( $_POST );
		if ( $result[ 'status' ] == 0 ) {
			return $this->responseSuccess( $result[ 'keterangan' ] );
		} else {
			return $this->responseFailed( $result[ 'keterangan' ] );
		}
	}
	public function actionCancel( $id ) {
		/** @var Ttsihd $model */
		$model             = $this->findModelBase64( 'Ttsihd', $id );
		$KodeTrans         = $model->KodeTrans;
		$_POST[ 'Action' ] = 'Cancel';
		$_POST[ 'SITgl' ]  = $_POST[ 'SITgl' ] . ' ' . $_POST[ 'SIJam' ];
		$result            = Ttsihd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'SINo' ] == '--' ) {
			Ttsihd::getRoute( $KodeTrans )[ 'index' ];
		} else {
			$model = Ttsihd::findOne( $result[ 'SINo' ] );
			return $this->redirect( Ttsihd::getRoute( $KodeTrans, [ 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] )[ 'update' ] );
		}
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client            = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response          = $client->post( Yii::$app->params[ 'host_report' ] . 'aunit/fsihd', [
			'body' => Json::encode( $_POST )
		] );
        $html                 = $response->getBody();
        return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	/* Picking List */
	public function actionPickingList( $id ) {
		$this->layout = 'print';
		return $this->render( 'pickingList', [
			'id'            => $id,
			'urlHeaderGrid' => Url::toRoute( [ 'ttsihd/fill-header-p-l' ] ),
			'urlDetailGrid' => Url::toRoute( [ 'ttsihd/fill-item-p-l' ] )
		] );
	}
	public function actionFillHeaderPL() {
		$rows = General::cCmd( "SELECT  ttsihd.SINo, SITgl, SIPickingNo, SIPickingDate, ttsiit.Cetak 
			FROM ttsiit INNER JOIN ttsihd ON ttsihd.SINo = ttsiit.SINo 
			WHERE ttsihd.SINo = :SINo 
			GROUP BY SIPickingNo, SIPickingDate", [
			':SINo' => base64_decode( \Yii::$app->request->post( 'id' ) )
		] )->queryAll();
		for ( $i = 0; $i < count( $rows ); $i ++ ) {
			$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'SIPickingNo' ] );
		}
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return [
			'rows'      => $rows,
			'rowsCount' => count( $rows )
		];
	}
	public function actionFillItemPL() {
		$rows                        = General::cCmd( "SELECT ttsiit.SINo, ttsiit.SIAuto, ttsiit.BrgKode, ttsiit.SIQty, ttsiit.SIHrgBeli, ttsiit.SIHrgJual, ttsiit.SIDiscount, ttsiit.SIPajak, tdbarang.BrgNama, tdbarang.BrgSatuan, 
		      IF((ttsiit.SIHrgJual * ttsiit.SIQty) > 0,ttsiit.SIDiscount / (ttsiit.SIHrgJual * ttsiit.SIQty) * 100, 0) AS Disc, ttsiit.SIQty * ttsiit.SIHrgJual - ttsiit.SIDiscount AS Jumlah, 
		      ttsiit.LokasiKode, ttsiit.SIPickingNo, ttsiit.SIPickingDate, tdbarang.BrgGroup, tdbarang.BrgRak1, SaldoQty
		      FROM ttsiit 
		      INNER JOIN tdbarang ON ttsiit.BrgKode = tdbarang.BrgKode
		      INNER JOIN tdbaranglokasi ON tdbaranglokasi.BrgKode = ttsiit.BrgKode AND tdbaranglokasi.lokasiKode = ttsiit.lokasiKode
		      WHERE (ttsiit.SIPickingNo = :SIPickingNo) ORDER BY ttsiit.SINo, ttsiit.SIAuto", [
			':SIPickingNo' => base64_decode( \Yii::$app->request->post( 'id' ) )
		] )->queryAll();
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return [
			'rows'      => $rows,
			'rowsCount' => count( $rows )
		];
	}
	public function actionPrintPickingList() {
		unset( $_POST[ '_csrf-app' ] );
		$_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client            = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response          = $client->post( Yii::$app->params[ 'host_report' ] . '/aunit/plsi', [
			'body' => Json::encode( $_POST )
		] );
		$html              = $response->getBody();
		return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	/* End of Picking List */
	/**
	 * Finds the Ttsihd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttsihd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttsihd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	public function actionCheckStock( $BrgKode, $LokasiKode ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return Tdbaranglokasi::find()
		                     ->where( 'BrgKode = :BrgKode AND LokasiKode = :LokasiKode', [ 'BrgKode' => $BrgKode, 'LokasiKode' => $LokasiKode ] )
		                     ->asArray()->one();
	}

    public function actionStatusOtomatis() {
        $this->layout   = 'popup-form';
        return $this->render( '_formStatus', [ 'data' => $_GET ] );
    }

    public function actionStatusOtomatisItem( $SINo ) {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $dt = General::cCmd( "SELECT KBMNo, KBMTgl, KBMLink, KBMBayar, Kode, Person, Memo, NoGL FROM vtKBM WHERE KBMLink = :SINo ORDER BY KBMTgl;", [':SINo' => $SINo ] )->queryAll();
        return [
            'rows' => $dt
        ];

    }

}
