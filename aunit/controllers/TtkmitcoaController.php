<?php

namespace aunit\controllers;

use aunit\components\AunitController;
use common\components\Custom;
use Yii;
use aunit\models\Ttkmitcoa;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TtkmitcoaController implements the CRUD actions for Ttkmitcoa model.
 */
class TtkmitcoaController extends AunitController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ttkmitcoa models.
     * @return mixed
     */
    public function actionIndex()
    {
	    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	    $requestData                 = \Yii::$app->request->post();
	    $primaryKeys                 = Ttkmitcoa::getTableSchema()->primaryKey;
	    if ( isset( $requestData[ 'oper' ] ) ) {
		    /* operation : create, edit or delete */
		    $oper = $requestData[ 'oper' ];
		    /** @var Ttkmitcoa $model */
		    $model = null;
		    if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
			    $model = $this->findModelBase64( 'ttkmitcoa', $requestData[ 'id' ] );
		    }
		    switch ( $oper ) {
			    case 'add'  :
			    case 'edit' :
				    if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
					    $requestData[ 'Action' ]  = 'Insert';
					    $requestData[ 'KMAutoN' ] = 0;
				    } else {
					    $requestData[ 'Action' ] = 'Update';
					    if ( $model != null ) {
						    $requestData[ 'KMNoOLD' ]   = $model->KMNo;
						    $requestData[ 'KMAutoNOLD' ] = $model->KMAutoN;
						    $requestData[ 'NoAccountOLD' ] = $model->NoAccount;
					    }
				    }
				    $result = Ttkmitcoa::find()->callSP( $requestData );
				    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
					    $response = $this->responseSuccess( $result[ 'keterangan' ] );
				    } else {
					    $response = $this->responseFailed( $result[ 'keterangan' ] );
				    }
				    break;
			    case 'batch' :
				    break;
			    case 'del'  :
				    $requestData             = array_merge( $requestData, $model->attributes );
				    $requestData[ 'Action' ] = 'Delete';
				    if ( $model != null ) {
					    $requestData[ 'KMNoOLD' ]   = $model->KMNo;
					    $requestData[ 'KMAutoNOLD' ] = $model->KMAutoN;
					    $requestData[ 'NoAccountOLD' ] = $model->NoAccount;
				    }
				    $result = Ttkmitcoa::find()->callSP( $requestData );
				    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
					    $response = $this->responseSuccess( $result[ 'keterangan' ] );
				    } else {
					    $response = $this->responseFailed( $result[ 'keterangan' ] );
				    }
				    break;
		    }
	    } else {
		    for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
			    $primaryKeys[ $i ] = 'Ttkmitcoa.' . $primaryKeys[ $i ];
		    }
		    $query     = ( new \yii\db\Query() )
			    ->select( new Expression(
				    'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
								ttkmitcoa.KMNo, ttkmitcoa.KMAutoN, ttkmitcoa.NoAccount, ttkmitcoa.KMBayar, ttkmitcoa.KMKeterangan, traccount.NamaAccount'
			    ) )
			    ->from( 'Ttkmitcoa' )
			    ->join( 'INNER JOIN', 'traccount', 'ttkmitcoa.NoAccount = traccount.NoAccount' )
			    ->where( [ 'ttkmitcoa.KMNo' => $requestData[ 'KMNo' ] ] )
			    ->orderBy( 'ttkmitcoa.KMAutoN' );
		    $rowsCount = $query->count();
		    $rows      = $query->all();
		    /* encode row id */
		    for ( $i = 0; $i < count( $rows ); $i ++ ) {
			    $rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
		    }
		    $response = [
			    'rows'      => $rows,
			    'rowsCount' => $rowsCount
		    ];
	    }
	    return $response;
    }

    /**
     * Displays a single Ttkmitcoa model.
     * @param string $KMNo
     * @param integer $KMAutoN
     * @param string $NoAccount
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($KMNo, $KMAutoN, $NoAccount)
    {
        return $this->render('view', [
            'model' => $this->findModel($KMNo, $KMAutoN, $NoAccount),
        ]);
    }

    /**
     * Creates a new Ttkmitcoa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ttkmitcoa();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'KMNo' => $model->KMNo, 'KMAutoN' => $model->KMAutoN, 'NoAccount' => $model->NoAccount]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ttkmitcoa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $KMNo
     * @param integer $KMAutoN
     * @param string $NoAccount
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($KMNo, $KMAutoN, $NoAccount)
    {
        $model = $this->findModel($KMNo, $KMAutoN, $NoAccount);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'KMNo' => $model->KMNo, 'KMAutoN' => $model->KMAutoN, 'NoAccount' => $model->NoAccount]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ttkmitcoa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $KMNo
     * @param integer $KMAutoN
     * @param string $NoAccount
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($KMNo, $KMAutoN, $NoAccount)
    {
        $this->findModel($KMNo, $KMAutoN, $NoAccount)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ttkmitcoa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $KMNo
     * @param integer $KMAutoN
     * @param string $NoAccount
     * @return Ttkmitcoa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($KMNo, $KMAutoN, $NoAccount)
    {
        if (($model = Ttkmitcoa::findOne(['KMNo' => $KMNo, 'KMAutoN' => $KMAutoN, 'NoAccount' => $NoAccount])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
