<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Tdsales;

use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TdsaleController implements the CRUD actions for Tdsale model.
 */
class TdsalesController extends AunitController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actions() {
		return [
			'td' => [
				'class' => TdAction::className(),
				'model' => Tdsales::className(),
			],
		];
	}
	/**
	 * Lists all Tdsale models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index');
	}


	/**
	 * Lists all Ttsk models.
	 * @return mixed
	 */
	public function actionSelect() {
		$this->layout = 'select-mode';
		return $this->render( 'index', [
			'mode' => self::MODE_SELECT
		] );
	}
	/**
	 * Displays a single Tdsale model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Finds the Tdsale model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Tdsales the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Tdsales::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Tdsale model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
        TUi::$actionMode   = Menu::ADD;
		$model = new Tdsales();
        $no = Yii::$app->db->createCommand('SELECT SalesKode FROM tdsales WHERE SalesKode like "%SL-%" ORDER BY SalesKode DESC limit 1')->queryAll();
		$salesKode = '0000';
		if(!empty($no)){
			preg_match_all('!\d+!', $no[0]['SalesKode'], $matches);
			if(!empty($matches) && !empty($matches[0])){
				$salesKode = $matches[0][0];
			}
		}
		$pad = \strlen($salesKode);
		$counter = \intval($salesKode);
		$counter++;
		$model->SalesKode = 'SL-'.str_pad($counter, $pad, "0", STR_PAD_LEFT);
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'tdsales/update','id'=> $this->generateIdBase64FromModel($model) ,'action' => 'display' ] );
		}
		return $this->render( 'create', [
			'model' => $model,
            'id'    => 'new'
		] );
	}
	/**
	 * Updates an existing Tdsale model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
        $index = \yii\helpers\Url::toRoute( [ 'tdsales/index' ] );
        if ( ! isset( $_GET[ 'action' ] ) ) {
            return $this->redirect( $index );
        }
        $action = $_GET[ 'action' ];
        switch ( $action ) {
            case 'create':
                TUi::$actionMode = Menu::ADD;
                break;
            case 'update':
                TUi::$actionMode = Menu::UPDATE;
                break;
            case 'view':
                TUi::$actionMode = Menu::VIEW;
                break;
            case 'display':
                TUi::$actionMode = Menu::DISPLAY;
                break;
        }
		$model = $this->findModelBase64( 'Tdsales', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'tdsales/update','id'=> $this->generateIdBase64FromModel($model) ,'action' => 'display' ] );
		}
		return $this->render( 'update', [
			'model' => $model,
			'id'    => $this->generateIdBase64FromModel( $model ),
		] );
	}
	/**
	 *
	 */
	public function actionFind() {
		$params = \Yii::$app->request->post();
		$rows   = [];
		if ( $params[ 'mode' ] === 'combo' ) {
			$value     = isset( $params[ 'value' ] ) ? $params[ 'value' ] : 'SalesKode';
			$label     = isset( $params[ 'label' ] ) ? $params[ 'label' ] : [ 'SalesNama' ];
			$order     = isset( $params[ 'order' ] ) ? $params[ 'order' ] : [];
			$condition = isset( $params[ 'condition' ] ) ? $params[ 'condition' ] : [];
			$params_cmb    = isset( $params[ 'params' ] ) ? $params[ 'params' ] : [];
			$allOption = isset( $params[ 'allOption' ] ) ? $params[ 'allOption' ] : false;
			$rows      = Tdsales::find()->select2( $value, $label, $order, [ 'condition' => $condition, 'params' => $params_cmb ], $allOption );
		}
		return $this->responseDataJson( $rows );
	}
	/**
	 * Deletes an existing Tdsale model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete( $id ) {
        /** @var Tdsales $model */
        $request = Yii::$app->request;
        $model             = $this->findModelBase64( 'Tdsales', $id );
        if ( $model->delete() ) {
            if ($request->isAjax) {
                return $this->responseSuccess( 'Berhasil menghapus data');
            }
            $model = Tdsales::find()->one();
            return $this->redirect( [ 'tdsales/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );

        } else {
            return $this->responseFailed( 'Gagal menghapus data' );
        }
	}

    public function actionCancel( $id, $action ) {
	    $model = Tdsales::findOne( $id );
	    if ( $model == null ) {
		    $model = Tdsales::find()->orderBy( [ 'SalesKode' => SORT_ASC ] )->one();
		    if ( $model == null ) {
			    return $this->redirect( [ 'tdsales/index' ] );
		    }
	    }
	    return $this->redirect( [ 'tdsales/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
    }
}
