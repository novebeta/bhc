<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Tdleasing;
use aunit\models\Ttbmhd;
use aunit\models\Ttbmit;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtbmhdController implements the CRUD actions for Ttbmhd model.
 */
class TtbmhdController extends AunitController {
	public function actions() {
		$colGrid  = null;
		$joinWith = [
			'leasing' => [
				'type' => 'INNER JOIN'
			],
			'account' => [
				'type' => 'INNER JOIN'
			],
		];
		$join     = [];
		$where    = [];
		if ( isset( $_POST[ 'query' ] ) ) {
			$r = Json::decode( $_POST[ 'query' ], true )[ 'r' ];
			switch ( $r ) {
				case 'ttbmhd/bank-masuk-inden':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'Inden' AND ttbmhd.BMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-leasing':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'Leasing' AND ttbmhd.BMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-scheme':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'Scheme' AND ttbmhd.BMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-uang-muka-kredit':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'Kredit' AND ttbmhd.BMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-piutang-kons-um':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'Piutang UM' AND ttbmhd.BMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-uang-muka-tunai':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'Tunai' AND ttbmhd.BMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-sisa-piutang-kons':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'Piutang Sisa' AND ttbmhd.BMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-bbn-progresif':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'BBN Plus' AND ttbmhd.BMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-dari-kas':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'BM Dari Kas' AND ttbmhd.BMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-umum':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'Umum' AND ttbmhd.BMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttbmhd/bank-masuk-penjualan-multi':
					$joinWith = [
						'account' => [
							'type' => 'INNER JOIN'
						],
					];
					$where    = [
						[
							'op'        => 'AND',
							'condition' => "(ttbmhd.BMJenis LIKE 'Penjualan Multi' AND ttbmhd.BMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
			}
		}
		return [
			'td' => [
				'class'    => TdAction::class,
				'model'    => Ttbmhd::class,
				'colGrid'  => $colGrid,
				'joinWith' => $joinWith,
				'join'     => $join,
				'where'    => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttbmhd models.
	 * @return mixed
	 */
	public function actionBankMasukInden() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankMasukInden', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbmhd/td' ] )
		] );
	}
	public function actionBankMasukIndenCreate() {
		return $this->create( 'Inden', 'Inden', 'bank-masuk-inden', 'BANK MASUK - Inden' );
	}
	public function actionBankMasukIndenUpdate( $id ) {
		return $this->update( 'Inden', 'Inden', $id, 'bank-masuk-inden', 'BANK MASUK - Inden' );
	}
	public function actionBankMasukIndenCancel( $id, $action ) {
		return $this->cancel( $id, $action, 'bank-masuk-inden' );
	}
	public function actionBankMasukLeasing() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankMasukLeasing', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbmhd/td' ] )
		] );
	}
	public function actionBankMasukLeasingCreate() {
		return $this->create( 'Piut Lease', 'Leasing', 'bank-masuk-leasing', 'BANK MASUK - Pelunasan Leasing' );
	}
	public function actionBankMasukLeasingUpdate( $id ) {
		return $this->update( 'Piut Lease', 'Leasing', $id, 'bank-masuk-leasing', 'BANK MASUK - Pelunasan Leasing' );
	}
	public function actionBankMasukLeasingCancel( $id, $action ) {
		return $this->cancel( $id, $action, 'bank-masuk-leasing' );
	}
	public function actionBankMasukScheme() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankMasukScheme', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbmhd/td' ] )
		] );
	}
	public function actionBankMasukSchemeCreate() {
		return $this->create( 'Scheme', 'Scheme', 'bank-masuk-scheme', 'BANK MASUK - Pelunasan Scheme' );
	}
	public function actionBankMasukSchemeUpdate( $id ) {
		return $this->update( 'Scheme', 'Scheme', $id, 'bank-masuk-scheme', 'BANK MASUK - Pelunasan Scheme' );
	}
	public function actionBankMasukSchemeCancel( $id, $action ) {
		return $this->cancel( $id, $action, 'bank-masuk-scheme' );
	}
	public function actionBankMasukUangMukaKredit() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankMasukUangMukaKredit', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbmhd/td' ] )
		] );
	}
	public function actionBankMasukUangMukaKreditCreate() {
		return $this->create( 'UM Diterima', 'Kredit', 'bank-masuk-uang-muka-kredit', 'BANK MASUK - Uang Muka Kredit' );
	}
	public function actionBankMasukUangMukaKreditUpdate( $id ) {
		return $this->update( 'UM Diterima', 'Kredit', $id, 'bank-masuk-uang-muka-kredit', 'BANK MASUK - Uang Muka Kredit' );
	}
	public function actionBankMasukUangMukaKreditCancel( $id, $action ) {
		return $this->cancel( $id, $action, 'bank-masuk-uang-muka-kredit' );
	}
	public function actionBankMasukUangMukaTunai() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankMasukUangMukaTunai', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbmhd/td' ] )
		] );
	}
	public function actionBankMasukUangMukaTunaiCreate() {
		return $this->create( 'UM Diterima', 'Tunai', 'bank-masuk-uang-muka-tunai', 'BANK MASUK - Uang Muka Tunai' );
	}
	public function actionBankMasukUangMukaTunaiUpdate( $id ) {
		return $this->update( 'UM Diterima', 'Tunai', $id, 'bank-masuk-uang-muka-tunai', 'BANK MASUK - Uang Muka Tunai' );
	}
	public function actionBankMasukPiutangKonsUm() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankMasukPiutangKonsUm', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbmhd/td' ] )
		] );
	}
	public function actionBankMasukPiutangKonsUmCreate() {
		return $this->create( 'Piutang UM Kons', 'Piutang UM', 'bank-masuk-piutang-kons-um', 'BANK MASUK - Piutang Konsumen UM' );
	}
	public function actionBankMasukPiutangKonsUmUpdate( $id ) {
		return $this->update( 'Piutang UM Kons', 'Piutang UM', $id, 'bank-masuk-piutang-kons-um', 'BANK MASUK - Piutang Konsumen UM' );
	}
	public function actionBankMasukSisaPiutangKons() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankMasukSisaPiutangKons', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbmhd/td' ] )
		] );
	}
	public function actionBankMasukSisaPiutangKonsCreate() {
		return $this->create( 'Piut Tunai', 'Piutang Sisa', 'bank-masuk-sisa-piutang-kons', 'BANK MASUK - Sisa Piutang Kons' );
	}
	public function actionBankMasukSisaPiutangKonsUpdate( $id ) {
		return $this->update( 'Piut Tunai', 'Piutang Sisa', $id, 'bank-masuk-sisa-piutang-kons', 'BANK MASUK - Sisa Piutang Kons' );
	}
	public function actionBankMasukBbnProgresif() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankMasukBbnProgresif', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbmhd/td' ] )
		] );
	}
	public function actionBankMasukBbnProgresifCreate() {
		return $this->create( 'BBN Plus', 'BBN Plus', 'bank-masuk-bbn-progresif', 'BANK MASUK - Bbn Progresif' );
	}
	public function actionBankMasukBbnProgresifUpdate( $id ) {
		return $this->update( 'BBN Plus', 'BBN Plus', $id, 'bank-masuk-bbn-progresif', 'BANK MASUK - Bbn Progresif' );
	}
	public function actionBankMasukDariKas() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankMasukDariKas', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbmhd/td' ] )
		] );
	}
	public function actionBankMasukDariKasCreate() {
		return $this->create( 'BM Dari Kas', 'BM Dari Kas', 'bank-masuk-dari-kas', 'BANK MASUK DARI KAS' );
	}
	public function actionBankMasukDariKasUpdate( $id ) {
		return $this->update( 'BM Dari Kas', 'BM Dari Kas', $id, 'bank-masuk-dari-kas', 'BANK MASUK - DARI KAS' );
	}
	/**
	 * Lists all Ttbmhd models.
	 * @return mixed
	 */
	public function actionBankMasukUmum() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankMasukUmum', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbmhd/td' ] )
		] );
	}
	public function actionBankMasukUmumCreate() {
		TUi::$actionMode   = Menu::ADD;
		$model             = new Ttbmhd();
		$model->BMNo       = General::createTemporaryNo( 'BM', 'Ttbmhd', 'BMNo' );
		$model->BMTgl      = date( 'Y-m-d' );
		$model->BMJam      = date( 'Y-m-d H:i:s' );
		$model->BMCekTempo = date( 'Y-m-d' );
		$model->BMNominal  = 0;
		$model->BMMemo     = "--";
		$model->BMJenis    = "Umum";
		$model->BMAC       = "--";
		$model->BMCekNo    = "--";
		$model->BMPerson   = "--";
		$model->UserID     = $this->UserID();
		$model->NoGL       = "--";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttbmhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttbmhd::find()->ttbmhdFillGetData()->where( [ 'ttbmhd.BMNo'    => $model->BMNo,
		                                                                       'ttbmhd.BMJenis' => $model->BMJenis ] )->asArray( true )->one();
		$dsTUang[ 'BMTotal' ]  = 0;
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'BMNoView' ] = $result[ 'BMNo' ];
		return $this->render( 'bank-masuk-umum-create', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id,
		] );
	}
	public function actionBankMasukUmumUpdate( $id ) {
		$index = Ttbmhd::getRoute( 'Umum', [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttbmhd $model */
		$model   = $this->findModelBase64( 'Ttbmhd', $id );
		$dsTUang = Ttbmhd::find()->ttbmhdFillGetData()->where( [ 'ttbmhd.BMNo'    => $model->BMNo,
		                                                         'ttbmhd.BMJenis' => $model->BMJenis ] )->asArray( true )->one();
		$id      = $this->generateIdBase64FromModel( $model );
		if ( Yii::$app->request->isPost ) {
			$post                 = \Yii::$app->request->post();
			$post[ 'BMNoBaru' ]   = $post[ 'BMNo' ];
			$post[ 'BMTotal' ]    = 0;
			$post[ 'Action' ]     = 'Update';
			$result               = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttbmhd::findOne( $result[ 'BMNo' ] );
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'BMNoView' ] = $result[ 'BMNo' ];
			if ( $result[ 'status' ] == 0 ) {
				return $this->redirect( Ttbmhd::getRoute( 'Umum', [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'bank-masuk-umum-update', [
					'model'   => $model,
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
		$dsTUang[ 'BMTotal' ]  = 0;
		$dsTUang[ 'SubTotal' ] = 0;
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttbmhd::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'BMNo' ] = $dsTUang[ 'BMNo' ];
		}
		$dsTUang[ 'BMNoView' ] = $_GET[ 'BMNoView' ] ?? $result[ 'BMNo' ];
		return $this->render( 'bank-masuk-umum-update', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id,
		] );
	}
	/**
	 * Lists all Ttbmhd models.
	 * @return mixed
	 */
	public function actionBankMasukPenjualanMulti() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'BankMasukPenjualanMulti', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttbmhd/td' ] )
		] );
	}
	public function actionBankMasukPenjualanMultiCreate() {
		TUi::$actionMode  = Menu::ADD;
		$model            = new Ttbmhd();
		$model->BMNo      = General::createTemporaryNo( 'BM', 'Ttbmhd', 'BMNo' );
		$model->BMTgl     = date( 'Y-m-d' );
		$model->BMJam     = date( 'Y-m-d H:i:s' );
		$model->BMNominal = 0;
		$model->BMMemo    = "--";
		$model->BMJenis   = "Penjualan Multi";
		$model->BMPerson  = "--";
		$model->BMAC      = "--";
		$model->BMCekNo   = "--";
		$model->UserID    = $this->UserID();
		$model->NoGL      = "--";
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttbmhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang                     = Ttbmhd::find()->ttbmhdFillGetData()->where( [ 'ttbmhd.BMNo'    => $model->BMNo,
		                                                                             'ttbmhd.BMJenis' => $model->BMJenis ] )->asArray( true )->one();
		$dsTUang[ 'TotalSetoran' ]   = 0;
		$dsTUang[ 'TotalPotongan' ]  = 0;
		$dsTUang[ 'TotalBankMasuk' ] = 0;
		$dsTUang[ 'BMNoView' ]       = str_replace( '=', '-', $dsTUang[ 'BMNo' ] );
		$id                          = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'BMNoView' ]       = $result[ 'BMNo' ];
		return $this->render( 'bank-masuk-penjualan-multi-create', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionBankMasukPenjualanMultiUpdate( $id ) {
		$index = Ttbmhd::getRoute( "Penjualan Multi", [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttbmhd $model */
		$model   = $this->findModelBase64( 'Ttbmhd', $id );
		$dsTUang = Ttbmhd::find()->ttbmhdFillGetData()->where( [ 'ttbmhd.BMNo'    => $model->BMNo,
		                                                         'ttbmhd.BMJenis' => $model->BMJenis ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post               = \Yii::$app->request->post();
//			$post[ 'BMTgl' ]    = date( 'Y-m-d' );
//			$post[ 'BMJam' ]    = date( 'Y-m-d H:i:s' );
			$post[ 'BMNoBaru' ] = $post[ 'BMNo' ];
			$post[ 'Action' ]   = 'Update';
			$result             = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttbmhd::findOne( $result[ 'BMNo' ] );
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'BMNoView' ] = $result[ 'BMNo' ];
			if ( $result[ 'status' ] == 0 ) {
				return $this->redirect( Ttbmhd::getRoute( "Penjualan Multi", [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'bank-masuk-penjualan-multi-update', [
					'model'   => $model,
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
		$dsTUang[ 'TotalSetoran' ]   = 0;
		$dsTUang[ 'TotalPotongan' ]  = 0;
		$dsTUang[ 'TotalBankMasuk' ] = 0;
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttbmhd::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'BMNo' ] = $dsTUang[ 'BMNo' ];
		}
		$dsTUang[ 'BMNoView' ] = $_GET[ 'BMNoView' ] ?? $result[ 'BMNo' ];
		return $this->render( 'bank-masuk-penjualan-multi-update', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	/**
	 * Lists all Ttbmhd models.
	 * @return mixed
	 */
	public function actionBankMasukPenjualanMultiPl() {
		return $this->render( 'BankMasukPenjualanMultiPl' );
	}
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * @param $KKJenis
	 * @param $view
	 * @param $title
	 *
	 * @return string
	 */
	private function create( $label, $BMJenis, $view, $title ) {
		TUi::$actionMode = Menu::ADD;
		$LeaseCond       = [ 'LeaseStatus' => 'A' ];
		if ( $BMJenis == 'Leasing' || $BMJenis == 'Kredit' || $BMJenis == 'Scheme' ) {
			$LeaseCond = [ 'and', $LeaseCond, [ '<>', 'LeaseKode', 'TUNAI' ] ];
		} elseif ( $BMJenis == 'Tunai' || $BMJenis == 'BM Dari Kas' || $BMJenis == 'Piutang Sisa' || $BMJenis == 'BBN Plus' ) {
			$LeaseCond = [ 'and', $LeaseCond, [ 'LeaseKode' => 'TUNAI' ] ];
		}
		$model             = new Ttbmhd();
		$model->BMNo       = General::createTemporaryNo( 'BM', 'Ttbmhd', 'BMNo' );
		$model->BMTgl      = date( 'Y-m-d' );
		$model->BMJam      = date( 'Y-m-d H:i:s' );
		$model->BMCekTempo = date( 'Y-m-d' );
		$model->BMNominal  = floatval( $_GET[ 'BMNominal' ] ?? 0 );
		$model->BMMemo     = $_GET[ 'BMMemo' ] ?? "--";
		$model->NoAccount  = $_GET[ 'NoAccount' ] ?? null;
		$model->BMPerson   = $_GET[ 'BMPerson' ] ?? "--";
		$model->LeaseKode  = $_GET[ 'LeaseKode' ] ?? ( Tdleasing::find()->where( $LeaseCond )->orderBy( 'LeaseKode' )->one() )->LeaseKode;
		$model->BMAC       = "--";
		$model->BMCekNo    = "--";
		$model->BMJenis    = $BMJenis;
		$model->UserID     = $this->UserID();
		$model->NoGL       = "--";
		$model->save();
		$model->refresh();
		if ( isset( $_GET[ 'DKNo' ] ) && $_GET[ 'DKNo' ] != '' ) {
			$item          = new Ttbmit();
			$item->BMNo    = $model->BMNo;
			$item->BMBayar = $model->BMNominal;
			$item->DKNo    = $_GET[ 'DKNo' ];
			$item->save();
		}
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttbmhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttbmhd::find()->ttbmhdFillGetData()->where( [ 'ttbmhd.BMNo'    => $model->BMNo,
		                                                                       'ttbmhd.BMJenis' => $model->BMJenis ] )->asArray( true )->one();
		$dsTUang[ 'BMTotal' ]  = 0;
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'BMNoView' ] = $result[ 'BMNo' ];
		return $this->render( 'create', [
			'model'   => $model,
			'dsTUang' => $dsTUang,
			'id'      => $id,
			'label'   => $label,
			'view'    => $view,
			'title'   => $title
		] );
	}
	/**
	 * @param $label
	 * @param $BMJenis
	 * @param $id
	 * @param $view
	 * @param $title
	 *
	 * @return string|\yii\web\Response
	 * @throws NotFoundHttpException
	 */
	private function update( $label, $BMJenis, $id, $view, $title ) {
		$index = Ttbmhd::getRoute( $BMJenis, [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttbmhd $model */
		$model   = $this->findModelBase64( 'Ttbmhd', $id );
		$dsTUang = Ttbmhd::find()->ttbmhdFillGetData()->where( [ 'ttbmhd.BMNo' => $model->BMNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post             = \Yii::$app->request->post();
			$post[ 'BMJam' ]  = $post[ 'BMTgl' ] . ' ' . $post[ 'BMJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttbmhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$modelnew = Ttbmhd::findOne( $result[ 'BMNo' ] );
			if($modelnew !== null ){
				$model = $modelnew;
				$id    = $this->generateIdBase64FromModel( $model );
			}
			if ( $result[ 'status' ] == 0 ) {
				$post[ 'BMNoView' ] = $result[ 'BMNo' ];
				return $this->redirect( [
					\Yii::$app->controller->id . '/' . \Yii::$app->controller->action->id,
					'id'     => $id,
					'action' => 'display'
				] );
			} else {
				return $this->render( 'update', [
					'model'   => $model,
					'dsTUang' => $post,
					'id'      => $id,
					'title'   => $title,
					'label'   => $label,
					'view'    => $view,
				] );
			}
		}
		$dsTUang[ 'BMTotal' ] = 0;
		$ttkkit               = Ttbmit::find()->where( [ 'BMNo' => $model->BMNo ] )->one();
		$dsTUang[ 'DKNo' ]    = ( $ttkkit == null ) ? '' : $ttkkit->DKNo;
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttbmhd::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'BMNo' ] = $dsTUang[ 'BMNo' ];
		}
		$dsTUang[ 'BMNoView' ] = $_GET[ 'BMNoView' ] ?? $result[ 'BMNo' ];
		return $this->render( 'update', [
			'model'   => $model,
			'view'    => $view,
			'title'   => $title,
			'dsTUang' => $dsTUang,
			'id'      => $id,
			'label'   => $label,
		] );
	}
	public function actionCancel( $id, $action ) {
		/** @var  $model Ttbmhd */
		$model            = $this->findModelBase64( 'Ttbmhd', $id );
		$BMJenis          = $model->BMJenis;
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Cancel';
		$post[ 'BMJam' ]  = $model->BMJam;
		$result           = Ttbmhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'BMNo' ] == '--' ) {
			return $this->redirect( Ttbmhd::getRoute( $BMJenis )[ 'index' ] );
		} else {
			$model = Ttbmhd::findOne( $result[ 'BMNo' ] );
			return $this->redirect( Ttbmhd::getRoute( $BMJenis, [ 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] )[ 'update' ] );
		}
	}
	public function actionJurnal() {
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Jurnal';
		$post[ 'BMJam' ]  = $post[ 'BMTgl' ] . ' ' . $post[ 'BMJam' ];
		$result           = Ttbmhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( Ttbmhd::getRoute( $post[ 'BMJenis' ], [ 'action' => 'display', 'id' => base64_encode( $result[ 'BMNo' ] ) ] )[ 'update' ] );
	}
	/**
	 * Deletes an existing Ttbmhd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
//	public function actionDelete() {
//		/** @var Ttbmhd $model */
//		$model             = $model = $this->findModelBase64( 'Ttbmhd', $_POST[ 'id' ] );
//		$_POST[ 'BMNo' ]   = $model->BMNo;
//		$_POST[ 'Action' ] = 'Delete';
//		$result            = Ttbmhd::find()->callSP( $_POST );
//		if ( $result[ 'status' ] == 0 ) {
//			return $this->responseSuccess( $result[ 'keterangan' ] );
//		} else {
//			return $this->responseFailed( $result[ 'keterangan' ] );
//		}
//	}

    public function actionDelete() {
        /** @var Ttbmhd $model */
        $request = Yii::$app->request;
        $PK         = base64_decode( $_REQUEST[ 'id' ] );
        $model      = null;
        $model             = Ttbmhd::findOne( $PK );
        $_POST[ 'BMNo' ]   = $model->BMNo;
        $_POST[ 'Action' ] = 'Delete';
        $BMJenis = $model->BMJenis;
        $result = Ttbmhd::find()->callSP( $_POST );
        if ($request->isAjax){
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['BMNo'] == '--') {
                    return $this->redirect( [ 'ttbmhd/pengajuan-bayar-bbn-bank' ] );
            } else {
                $model = Ttbmhd::findOne($result['BMNo']);
                if(empty($model)){
                    return $this->redirect( Ttbmhd::getRoute( $BMJenis )[ 'index' ] );
                }
                return $this->redirect( Ttbmhd::getRoute( $BMJenis, [ 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] )[ 'update' ] );
            }
        }
    }


	/**
	 * Finds the Ttbmhd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttbmhd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttbmhd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST         = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client        = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response      = $client->post( Yii::$app->params[ 'host_report' ] . '/aunit/fbmunit', [
			'body' => Json::encode( $_POST )
		] );

		return Yii::$app->response->sendContentAsFile( $response->getBody(), 'fbmunit.pdf', [
			'inline'   => true,
			'mimeType' => 'application/pdf'
		] );
	}
}
