<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Tdlokasi;
use aunit\models\Ttpbhd;
use aunit\models\Ttsmhd;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\base\UserException;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtsmhdController implements the CRUD actions for Ttsmhd model.
 */
class TtsmhdController extends AunitController {
	public function actions() {
		$colGrid    = null;
		$joinWith   = [
			'rLokasiAsal lasal'     => [ 'type' => 'LEFT OUTER JOIN' ],
			'rLokasiTujuan ltujuan' => [ 'type' => 'LEFT OUTER JOIN' ]
		];
		$join       = [];
		$where      = [];
		$sqlraw     = null;
		$queryRawPK = null;
		if ( isset( $_POST[ 'query' ] ) ) {
			$r = Json::decode( $_POST[ 'query' ], true )[ 'r' ];
			switch ( $r ) {
				case 'ttsmhd/mutasi-internal-pos-select':
					$colGrid  = Ttsmhd::colGridMutasiInternalPos();
					$whereSql = '';
					$q        = Json::decode( $_POST[ 'query' ], true );
					if ( $q[ 'cmbTxt1' ] == 'SMNo' && ( $q[ 'txt1' ] !== '' || $q[ 'txt1' ] !== '--' ) ) {
						$whereSql = "OR ttsmhd.SMNo = '{$q['txt1']}'";
					}
					$sqlraw     = "SELECT 
									  ttsmhd.SMNo, ttsmhd.SMTgl, ttsmhd.LokasiAsal, ttsmhd.LokasiTujuan, ttsmhd.SMMemo, 
       								  ttsmhd.SMTotal, ttsmhd.UserID, IFNULL(tdlokasi.LokasiNama, '--') AS LokasiNamaAsal, 
       								  IFNULL(tdlokasi_1.LokasiNama, '--') AS LokasiNamaTujuan 
									FROM
									  ttsmhd 
									  INNER JOIN tdlokasi 
									    ON ttsmhd.LokasiAsal = tdlokasi.LokasiKode 
									  INNER JOIN tdlokasi tdlokasi_1 
									    ON ttsmhd.LokasiTujuan = tdlokasi_1.LokasiKode 
									  LEFT OUTER JOIN ttpbhd 
									    ON ttsmhd.SMNo = ttpbhd.SMNo 
									WHERE ttsmhd.SMNo LIKE '%%' 
									  AND (ttpbhd.PBNo IS NULL $whereSql)  
									ORDER BY ttsmhd.SMNo";
					$queryRawPK = [ 'SMNo' ];
					break;
				case 'ttsmhd/mutasi-internal-pos':
					$colGrid = Ttsmhd::colGridMutasiInternalPos();
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "LEFT(ttsmhd.SMNo,2) = 'MI' AND ttsmhd.SMNo NOT LIKE '%=%'",
							'params'    => []
						]
					];
					break;
				case 'ttsmhd/mutasi-repair-in':
					$colGrid = Ttsmhd::colGridMutasiRepairIn();
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "LEFT(ttsmhd.SMNo,2) = 'RI' AND ttsmhd.SMNo NOT LIKE '%=%'",
							'params'    => []
						]
					];
					break;
				case 'ttsmhd/mutasi-repair-out':
					$colGrid = Ttsmhd::colGridMutasiRepairOut();
					$where   = [
						[
							'op'        => 'AND',
							'condition' => "LEFT(ttsmhd.SMNo,2) = 'RO' AND ttsmhd.SMNo NOT LIKE '%=%'",
							'params'    => []
						]
					];
					break;
			}
		}
		return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Ttsmhd::class,
				'queryRaw'   => $sqlraw,
				'queryRawPK' => $queryRawPK,
				'colGrid'    => $colGrid,
				'joinWith'   => $joinWith,
				'join'       => $join,
				'where'      => $where,
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttsmhd models.
	 * @return mixed
	 */
	public function actionMutasiInternalPos() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttsmhd::find()->callSP( $post, 'MutasiPOS' );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'MutasiInternalPos', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttsmhd/td' ] )
		] );
	}
	/**
	 * Lists all Ttsmhd models.
	 * @return mixed
	 */
	public function actionMutasiInternalPosSelect() {
		$this->layout = 'select-mode';
		return $this->render( 'MutasiInternalPos', [
			'mode' => self::MODE_SELECT_DETAIL_MULTISELECT,
		] );
	}
	/**
	 * Lists all Ttsmhd models.
	 * @return mixed
	 */
	public function actionMutasiRepairIn() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttsmhd::find()->callSP( $post, 'RepairIN' );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'MutasiRepairIn', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttsmhd/td' ] )
		] );
	}
	/**
	 * Lists all Ttsmhd models.
	 * @return mixed
	 */
	public function actionMutasiRepairOut() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttsmhd::find()->callSP( $post, 'RepairOUT' );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'MutasiRepairOut', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttsmhd/td' ] )
		] );
	}
	/**
	 * Creates a new Ttsmhd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param $jenis
	 *
	 * @return mixed
	 */
	public function actionCreate( $jenis ) {
		TUi::$actionMode     = Menu::ADD;
		$model               = new Ttsmhd();
		$model->SMNo         = $this->createNumber( $jenis, true );
		$model->SMTgl        = date( 'Y-m-d' );
		$model->SMJam        = date( "Y-m-d H:i:s" );
		$model->SMMemo       = '--';
		$model->LokasiAsal   = ( Tdlokasi::find()->where( "LokasiStatus <> 'N'" )->orderBy( "LokasiStatus,  LokasiNomor" )->one() )->LokasiKode;
		$model->LokasiTujuan = ( Tdlokasi::find()->where( "LokasiStatus <> 'N' AND LokasiKode <> :LokasiKode", [ ':LokasiKode' => $model->LokasiAsal ] )->orderBy( "LokasiStatus,  LokasiKode" )->one() )->LokasiKode;
		if ( $jenis == 'RepairIN' ) {
			$model->LokasiTujuan = 'Repair';
		}
		if ( $jenis == 'RepairOUT' ) {
			$model->LokasiAsal = 'Repair';
		}
		if ( $jenis == 'MutasiPOS' ) {
			$model->LokasiAsal = ( Tdlokasi::find()->where( "LokasiStatus IN ('1','A') AND LokasiKode <> 'Repair' AND LokasiJenis <> 'Kas'" )->orderBy( "LokasiStatus,  LokasiKode" )->one() )->LokasiKode;
		}
		$model->SMTotal = 0;
		$model->UserID  = $this->UserID();
		$model->save();
		$model->refresh();
		if ( $jenis != 'MutasiPOS' ) {
			$modelPB               = new Ttpbhd();
			$modelPB->PBNo         = $model->SMNo;
			$modelPB->PBTgl        = $model->SMTgl;
			$modelPB->PBJam        = $model->SMJam;
			$modelPB->PBMemo       = $model->SMMemo;
			$modelPB->LokasiAsal   = $model->LokasiAsal;
			$modelPB->LokasiTujuan = $model->LokasiTujuan;
			$modelPB->PBTotal      = $model->SMTotal;
			$modelPB->UserID       = $model->UserID;
			$modelPB->save();
		}
		$dsBeli             = Ttsmhd::find()->GetDataByNo()->where( [ 'ttsmhd.SMNo' => $model->SMNo ] )->asArray( true )->one();
		$dsBeli[ 'Action' ] = 'Insert';
		$result             = Ttsmhd::find()->callSP( $dsBeli, $jenis );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$id                   = $this->generateIdBase64FromModel( $model );
		$dsBeli[ 'SMNoView' ] = $result[ 'SMNo' ];
		return $this->render( 'create', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'jenis'  => $jenis,
			'id'     => $id
		] );
	}
	protected function createNumber( $jenis, $tmp = false ) {
		switch ( $jenis ) {
			case "MutasiPOS":
				return General::createNo( "MI", 'Ttsmhd', 'SMNo', $tmp );
				break;
			case "RepairIN":
				return General::createNo( "RI", 'Ttsmhd', 'SMNo', $tmp );
				break;
			case "RepairOUT":
				return General::createNo( "RO", 'Ttsmhd', 'SMNo', $tmp );
				break;
		}
	}
	/**
	 * Updates an existing Ttsmhd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 * @param $jenis
	 * @param $action
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \yii\db\Exception
	 */
	public function actionUpdate( $id, $jenis, $action ) {
		$index = \yii\helpers\Url::toRoute( [ 'ttsmhd/index' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttsmhd $model */
		$model  = $this->findModelBase64( 'Ttsmhd', $id );
		$dsBeli = Ttsmhd::find()->GetDataByNo()->where( [ 'ttsmhd.SMNo' => $model->SMNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$transaction = Ttsmhd::getDb()->beginTransaction();
			$post        = array_merge( $dsBeli, Yii::$app->request->post() );
			try {
				$post[ 'UserID' ] = $this->UserID();
				$SubTotal         = 0;
				$post[ 'SMTgl' ]  = date( "Y-m-d" );
				$post[ 'SMJam' ]  = date( "Y-m-d H:i:s" );
				$post[ 'Action' ] = 'Update';
				$result           = Ttsmhd::find()->callSP( $post, $jenis );
				$model            = Ttsmhd::findOne( $result[ 'SMNo' ] );
				$id               = $this->generateIdBase64FromModel( $model );
				Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
				$transaction->commit();
				if ( $result[ 'status' ] == 0 ) {
					$post[ 'SMNoView' ] = $result[ 'SMNo' ];
					return $this->redirect( [ 'ttsmhd/update', 'jenis' => $jenis, 'action' => 'display', 'id' => $id ] );
				} else {
					return $this->render( 'update', [
						'model'  => $model,
						'dsBeli' => $post,
						'jenis'  => $jenis,
						'id'     => $id
					] );
				}
			} catch ( UserException $e ) {
				$transaction->rollBack();
				Yii::$app->session->setFlash( 'error', $e->getMessage() );
				return $this->render( 'update', [
					'model'  => $model,
					'dsBeli' => $post,
					'jenis'  => $jenis,
					'id'     => $id
				] );
			}
		}
		$dsBeli[ 'Action' ] = 'Load';
		$result             = Ttsmhd::find()->callSP( $dsBeli, $jenis );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsBeli[ 'SMNoView' ] = $result[ 'SMNo' ];
		return $this->render( 'update', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'jenis'  => $jenis,
			'id'     => $id
		] );
	}
	public function actionFillMotorGridAddEdit( $jenis ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$SMNo                        = $requestData[ 'SMNo' ];
		$LokasiKode                  = $requestData[ 'LokasiKode' ];
		$Jam                         = $requestData[ 'SMTgl' ] . ' ' . date( 'H:i:s' );
		$query2                      = ( new \yii\db\Query() )
			->select( new Expression(
				'tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType,
				 tmotor.MotorWarna, tmotor.MotorTahun, tmotor.FBHarga, tdmotortype.MotorNama'
			) )
			->from( 'tmotor' )
			->join( 'LEFT OUTER JOIN', 'tdmotortype', 'tmotor.MotorType = tdmotortype.MotorType' )
			->join( 'INNER JOIN', 'ttsmit', 'tmotor.MotorNoMesin = ttsmit.MotorNoMesin AND  ttsmit.MotorAutoN = tmotor.MotorAutoN' )
			->where( [ 'ttsmit.SMNo' => $SMNo ] );
		$query1                      = new \yii\db\Query();
		$query1->select( 'tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType, 
						tmotor.MotorWarna, tmotor.MotorTahun, tmotor.FBHarga, tdmotortype.MotorNama' )
		       ->from( 'tmotor' )
		       ->join( 'LEFT OUTER JOIN', 'tdmotortype', 'tmotor.MotorType = tdmotortype.MotorType' )
		       ->join( 'INNER JOIN', 'vmotorLastlokasi', '(tmotor.MotorNoMesin = vmotorLastlokasi.MotorNoMesin AND tmotor.MotorAutoN = vmotorLastlokasi.MotorAutoN)' );
		switch ( $jenis ) {
			case 'MutasiPOS':
			case 'RepairIN':
				$query1
					->where( "(vmotorLastlokasi.LokasiKode = :LokasiKode AND vmotorLastlokasi.LokasiKode <> 'Repair' 
							AND vmotorLastlokasi.Kondisi = 'IN' AND vmotorLastlokasi.Jam <= :Jam )", [
						':LokasiKode' => $LokasiKode,
						':Jam'        => $Jam
					] );
				break;
			case 'RepairOUT':
				$query1
					->where( "(vmotorLastlokasi.LokasiKode = :LokasiKode AND vmotorLastlokasi.LokasiKode = 'Repair' 
							AND vmotorLastlokasi.Kondisi = 'IN' AND vmotorLastlokasi.Jam <= :Jam )", [
						':LokasiKode' => $LokasiKode,
						':Jam'        => $Jam
					] );
				break;
		}
		$query = $query1->union( $query2 );
		$query->orderBy( 'tmotor.MotorType, MotorWarna, MotorTahun' );
		$rowsCount = $query->count();
		$rows      = $query->all();
		/* encode row id */
		$response = [
			'rows'      => $rows,
			'rowsCount' => $rowsCount
		];
		return $response;
	}
	/**
	 * Deletes an existing Ttsmhd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param $jenis
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
    public function actionDelete($jenis)
    {
        /** @var Ttsmhd $model */
        $request = Yii::$app->request;
        if ($request->isAjax){
            $model = $this->findModelBase64('Ttsmhd', $_POST['id']);
            $_POST['SMNo'] = $model->SMNo;
            $_POST['Action'] = 'Delete';
            $result = Ttsmhd::find()->callSP($_POST, $jenis);
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            $id = $request->get('id');
            $model = $this->findModelBase64('Ttsmhd', $id);
            $_POST['SMNo'] = $model->SMNo;
            $_POST['Action'] = 'Delete';
            $result = Ttsmhd::find()->callSP($_POST, $jenis);
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['SMNo'] == '--') {
                return $this->redirect(['index']);
            } else {
                $model = Ttsmhd::findOne($result['SMNo']);
                return $this->redirect(['ttsmhd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model),'jenis' => $jenis]);
            }
        }
    }
	/**
	 * Finds the Ttsmhd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttsmhd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttsmhd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	public function actionCancel( $id, $jenis ) {
		/** @var Ttsmhd $model */
		$model             = $this->findModelBase64( 'Ttsmhd', $id );
		$_POST[ 'Action' ] = 'Cancel';
		$_POST[ 'SMJam' ]  = $model->SMJam;
		$result            = Ttsmhd::find()->callSP( $_POST, $jenis );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'SMNo' ] == '--' ) {
			return $this->redirect( Ttsmhd::getRoute( $jenis )[ 'index' ] );
		} else {
			$model = Ttsmhd::findOne( $result[ 'SMNo' ] );
			return $this->redirect( Ttsmhd::getRoute( $jenis, [ 'action' => 'display', 'jenis' => $jenis, 'id' => $this->generateIdBase64FromModel( $model ) ] )[ 'update' ] );
		}
	}
	public function actionJurnal( $jenis ) {
		$_POST[ 'Action' ] = 'Jurnal';
		$_POST[ 'SMJam' ]  = $_POST[ 'SMTgl' ].' '.$_POST[ 'SMJam' ];
		$result            = Ttsmhd::find()->callSP( $_POST, $jenis );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( Ttsmhd::getRoute( $jenis, [ 'action' => 'display', 'jenis' => $jenis, 'id' => base64_encode($result['SMNo']) ] )[ 'update' ] );
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST         = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client        = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response      = $client->post( Yii::$app->params[ 'host_report' ] . '/aunit/fsmhd', [
			'body' => Json::encode( $_POST )
		] );
		$html          = $response->getBody();
		return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
}
