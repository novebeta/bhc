<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Tdlokasi;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
/**
 * TdlokasiController implements the CRUD actions for Tdlokasi model.
 */
class TdlokasiController extends AunitController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actions() {
		$colGrid    = null;
		$joinWith   = [
			'sales' => [
				'type' => 'LEFT OUTER JOIN'
			]
		];
		$join       = [];
		$where      = [];
		$sqlraw     = null;
		$queryRawPK = null;
		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			switch ( $r ) {
				case 'tdlokasi/select':
					$where    = [
						[
							'op'        => 'AND',
							'condition' => "LokasiNomor <> '01' ",
							'params'    => []
						]
					];
					break;
			}
		}
		return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Tdlokasi::class,
				'queryRaw'   => $sqlraw,
				'queryRawPK' => $queryRawPK,
				'colGrid'    => $colGrid,
				'joinWith'   => $joinWith,
				'join'       => $join,
				'where'      => $where,
			],
		];
	}
	/**
	 * Lists all Tdlokasi models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index' );
	}
	/**
	 * Lists all Ttsk models.
	 * @return mixed
	 */
	public function actionSelect() {
		$this->layout = 'select-mode';
		return $this->render( 'index', [
			'mode' => self::MODE_SELECT
		] );
	}
	/**
	 * Displays a single Tdlokasi model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Creates a new Tdlokasi model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
        TUi::$actionMode   = Menu::ADD;
        $model = new Tdlokasi();
        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'tdlokasi/update','id'=> $this->generateIdBase64FromModel($model) ,'action' => 'display' ] );
        }
        return $this->render( 'create', [
            'model' => $model,
            'id'    => 'new'
        ] );
	}
	/**
	 * Updates an existing Tdlokasi model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$index = \yii\helpers\Url::toRoute( [ 'tdlokasi/index' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
                TUi::$actionMode = Menu::ADD;
                break;
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		$model = $this->findModelBase64( 'Tdlokasi', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'tdlokasi/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
		}else{
            Yii::$app->session->addFlash(  'warning', Html::errorSummary($model) );
        }
		return $this->render( 'update', [
			'model' => $model,
			'id'    => $this->generateIdBase64FromModel( $model ),
		] );
	}
	/**
	 * Deletes an existing Tdlokasi model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
		/** @var Tdlokasi $model */
        $request = Yii::$app->request;
        $model             = $this->findModelBase64( 'Tdlokasi', $id );
        if ( $model->delete() ) {
            if ($request->isAjax) {
                return $this->responseSuccess( 'Berhasil menghapus data Lokasi');
            }
            $model = Tdlokasi::find()->one();
            return $this->redirect( [ 'tdlokasi/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );

        } else {
            return $this->responseFailed( 'Gagal menghapus data Lokasi' );
        }
	}
	/**
	 * Finds the Tdlokasi model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Tdlokasi the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Tdlokasi::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	public function actionCancel( $id, $action ) {
		$model = Tdlokasi::findOne( $id );
		if ( $model == null ) {
			$model = Tdlokasi::find()->orderBy( [ 'LokasiKode' => SORT_ASC ] )->one();
			if ( $model == null ) {
				return $this->redirect( [ 'tdlokasi/index' ] );
			}
		}
		return $this->redirect( [ 'tdlokasi/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
	}
}
