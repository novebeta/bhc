<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\models\Ttgeneralledgerit;
use Yii;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
class TtgeneralledgeritController extends AunitController {
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttgeneralledgerit::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
			/** @var Ttgeneralledgerit $model */
			$model = null;
			if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
				$model = $this->findModelBase64( 'Ttgeneralledgerit', $requestData[ 'id' ] );
			}
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'Action' ]  = 'Insert';
						$requestData[ 'GLAutoN' ] = 0;
					} else {
						$requestData[ 'Action' ] = 'Update';
						if ( $model != null ) {
							$requestData[ 'NoGLOLD' ]      = $model->NoGL;
							$requestData[ 'GLAutoNOLD' ]   = $model->GLAutoN;
							$requestData[ 'TglGLOLD' ]     = $model->TglGL;
							$requestData[ 'NoAccountOLD' ] = $model->NoAccount;
						}
					}
					$result = Ttgeneralledgerit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'batch' :
					break;
				case 'del'  :
					$requestData             = array_merge( $requestData, $model->attributes );
					$requestData[ 'Action' ] = 'Delete';
					if ( $model != null ) {
						$requestData[ 'NoGLOLD' ]      = $model->NoGL;
						$requestData[ 'GLAutoNOLD' ]   = $model->GLAutoN;
						$requestData[ 'TglGLOLD' ]     = $model->TglGL;
						$requestData[ 'NoAccountOLD' ] = $model->NoAccount;
					}
					$result = Ttgeneralledgerit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
			}
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'Ttgeneralledgerit.' . $primaryKeys[ $i ];
			}
			$query     = ( new \yii\db\Query() )
				->select( new Expression(
					'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
								Ttgeneralledgerit.NoGL, Ttgeneralledgerit.GLAutoN, Ttgeneralledgerit.NoAccount, Ttgeneralledgerit.DebetGL, 
								Ttgeneralledgerit.KreditGL,Ttgeneralledgerit.KeteranganGL, traccount.NamaAccount'
				) )
				->from( 'Ttgeneralledgerit' )
				->join( 'INNER JOIN', 'traccount', 'Ttgeneralledgerit.NoAccount = traccount.NoAccount' )
				->where( [ 'Ttgeneralledgerit.NoGL' => $requestData[ 'NoGL' ] ] )
				->orderBy( 'Ttgeneralledgerit.GLAutoN' );
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	public function actionView( $NoGL, $TglGL, $GLAutoN ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $NoGL, $TglGL, $GLAutoN ),
		] );
	}
	public function actionCreate() {
		$model = new Ttgeneralledgerit();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'NoGL' => $model->NoGL, 'TglGL' => $model->TglGL, 'GLAutoN' => $model->GLAutoN ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	public function actionUpdate( $NoGL, $TglGL, $GLAutoN ) {
		$model = $this->findModel( $NoGL, $TglGL, $GLAutoN );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'NoGL' => $model->NoGL, 'TglGL' => $model->TglGL, 'GLAutoN' => $model->GLAutoN ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	public function actionDelete( $NoGL, $TglGL, $GLAutoN ) {
		$this->findModel( $NoGL, $TglGL, $GLAutoN )->delete();
		return $this->redirect( [ 'index' ] );
	}
	protected function findModel( $NoGL, $TglGL, $GLAutoN ) {
		if ( ( $model = Ttgeneralledgerit::findOne( [ 'NoGL' => $NoGL, 'TglGL' => $TglGL, 'GLAutoN' => $GLAutoN ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
