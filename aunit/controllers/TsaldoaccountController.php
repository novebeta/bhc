<?php
namespace aunit\controllers;
use aunit\components\TdAction;
use Yii;
use aunit\models\Tsaldoaccount;
use yii\db\Expression;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TsaldoaccountController implements the CRUD actions for Tsaldoaccount model.
 */
class TsaldoaccountController extends \aunit\components\AunitController {
	public function actions() {
		return [
			'td' => [
				'class' => TdAction::class,
				'model' => Tsaldoaccount::class,
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Tsaldoaccount models.
	 * @return mixed
	 */
	public function actionIndex() {
		$model           = new Tsaldoaccount();
		$model->TglSaldo = date( 'Y-m-d' );
		$id              = $this->generateIdBase64FromModel( $model );
		return $this->render( 'create', [
			'model' => $model,
			'id'    => $id
		] );
	}
	public function actionList() {
		$post                        = Yii::$app->request->post();
		$result                      = Tsaldoaccount::find()->listTutupBuku( $post[ 'TglDari' ], $post[ 'TglSampai' ] );
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return [
			'rows'      => $result,
			'rowsCount' => count( $result )
		];
	}
	/**
	 * @return array
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDetail() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$rows                        = [];
		$rowsCount                   = 0;
		$status                      = 0;
		$msg                         = "Berhasil";
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, */
			switch ( $requestData[ 'oper' ] ) {
				case 'add':
					$requestData[ 'Action' ] = 'Insert';
					$result                  = Tsaldoaccount::find()->callSP( $requestData );
					$status                  = $result[ 'status' ];
					$msg                     = $result[ 'keterangan' ];
					break;
				case 'del':
					$requestData[ 'Action' ] = 'Delete';
					$result                  = Tsaldoaccount::find()->callSP( $requestData );
					$status                  = $result[ 'status' ];
					$msg                     = $result[ 'keterangan' ];
					break;
				case 'read':
					/* operation : read */
					$query = ( new \yii\db\Query() );
					$query->select( new Expression( "TSaldoAccount.Debet,TSaldoAccount.Kredit ,  TSaldoAccount.TglOpen, TSaldoAccount.MyOpening, 
					TSaldoAccount.NoAccount, TSaldoAccount.TglSaldo, TSaldoAccount.SaldoAccount, TRAccount.NamaAccount, TRAccount.OpeningBalance, 
                    TRAccount.OpeningBalance + TSaldoAccount.SaldoAccount AS Saldo, TRAccount.JenisAccount, 
                    TRAccount.StatusAccount, TRAccount.NoParent" ) )
					      ->from( 'tsaldoaccount' )
					      ->where( [
						      'TglSaldo' => $requestData[ 'Tanggal' ]
					      ] )
					      ->innerJoin( 'traccount', 'traccount.NoAccount = tsaldoaccount.NoAccount' )
					      ->orderBy( 'tsaldoaccount.NoAccount' );
					$rowsCount = $query->count();
					$rows      = $query->all();
					break;
			}
		}
		return [
			'status'    => $status,
			'msg'       => $msg,
			'rows'      => $rows,
			'rowsCount' => $rowsCount
		];
	}
	/**
	 * Creates a new Tsaldoaccount model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model           = new Tsaldoaccount();
		$model->TglSaldo = date( 'Y-m-d' );
		$id              = $this->generateIdBase64FromModel( $model );
		return $this->render( 'create', [
			'model' => $model,
			'id'    => $id
		] );
	}
	/**
	 * Deletes an existing Tsaldoaccount model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $NoAccount
	 * @param string $TglSaldo
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $NoAccount, $TglSaldo ) {
		$this->findModel( $NoAccount, $TglSaldo )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Tsaldoaccount model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $NoAccount
	 * @param string $TglSaldo
	 *
	 * @return Tsaldoaccount the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $NoAccount, $TglSaldo ) {
		if ( ( $model = Tsaldoaccount::findOne( [ 'NoAccount' => $NoAccount, 'TglSaldo' => $TglSaldo ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
