<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\TdAction;
use aunit\models\Tmotor;

use common\components\Custom;
use common\components\General;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TmotorController implements the CRUD actions for Tmotor model.
 */
class TmotorController extends AunitController {
	public function actions() {
		$sqlraw = "";
		if ( isset( $_POST['query'] ) ) {
			$query = json_decode( $_POST['query'] );
			switch ( $query->tyStatus ) {
				case '' :
					if(!empty($query->refresh) && $query->refresh == true){
						General::cCmd("TRUNCATE tvmotorlokasi; INSERT INTO tvmotorlokasi SELECT * FROM vmotorlokasi;")->execute();
					}
					$sqlraw = "SELECT
						tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorMemo,
						tmotor.DealerKode, tmotor.SSNo, tmotor.FBNo, tmotor.FBHarga, tmotor.DKNo, tmotor.SKNo, tmotor.PDNo, tmotor.SDNo, tmotor.BPKBNo,
						tmotor.BPKBTglJadi, tmotor.BPKBTglAmbil, tmotor.STNKNo, tmotor.STNKTglMasuk, tmotor.STNKTglBayar, tmotor.STNKTglJadi, tmotor.STNKTglAmbil,
						tmotor.STNKNama, tmotor.STNKAlamat, tmotor.PlatNo, tmotor.PlatTgl, tmotor.PlatTglAmbil, tmotor.FakturAHMNo, tmotor.FakturAHMTgl,
						tmotor.FakturAHMTglAmbil, 'In Transit' AS LokasiKode, '--' AS NoTrans, ttfb.FBTgl AS  Jam, 'Transit' AS Kondisi,
						IFNULL(ttdk.DKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS DKTgl, IFNULL(ttss.SSTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SSTgl, IFNULL(ttfb.FBTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS FBTgl,
						IFNULL(ttpd.PDTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS PDTgl, IFNULL(ttsd.SDTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SDTgl, IFNULL(ttsk.SKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SKTgl,
						IFNULL(tdcustomer.CusNama, '--') AS CusNama, tmotor.RKNo, IFNULL(ttrk.RKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS RKTgl, IFNULL(ttin.INNo, '--') AS INNo, IFNULL(ttin.INTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS INTgl
						FROM tmotor 
						INNER JOIN ttfb ON tmotor.FBNo = ttfb.FBNo 
						LEFT OUTER JOIN ttss ON tmotor.SSNo = ttss.SSNo
						LEFT OUTER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo
						LEFT OUTER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						LEFT OUTER JOIN ttsd ON tmotor.SDNo = ttsd.SDNo
						LEFT OUTER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
						LEFT OUTER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo
						LEFT OUTER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
						LEFT OUTER JOIN ttin ON ttdk.INNo = ttin.INNo
						WHERE (tmotor.MotorType LIKE '" . $query->typeMotor . "%' AND tmotor.MotorWarna LIKE '" . $query->warnaMotor . "%') 
						AND MotorTahun BETWEEN " . $query->thn1 . " AND " . $query->thn2 . " AND Tmotor.SSNo = '--'
						UNION ALl
						SELECT
						tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType, tmotor.MotorWarna, 
						tmotor.MotorTahun, tmotor.MotorMemo, tmotor.DealerKode, tmotor.SSNo, tmotor.FBNo, tmotor.FBHarga,
						tmotor.DKNo, tmotor.SKNo, tmotor.PDNo, tmotor.SDNo, tmotor.BPKBNo, tmotor.BPKBTglJadi, tmotor.BPKBTglAmbil,
						tmotor.STNKNo, tmotor.STNKTglMasuk, tmotor.STNKTglBayar, tmotor.STNKTglJadi, tmotor.STNKTglAmbil, tmotor.STNKNama, 
						tmotor.STNKAlamat, tmotor.PlatNo, tmotor.PlatTgl, tmotor.PlatTglAmbil, tmotor.FakturAHMNo, tmotor.FakturAHMTgl, tmotor.FakturAHMTglAmbil,
						LokasiMotor.LokasiKode, LokasiMotor.NoTrans, LokasiMotor.Jam, LokasiMotor.Kondisi,
						IFNULL(ttdk.DKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS DKTgl, IFNULL(ttss.SSTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SSTgl, IFNULL(ttfb.FBTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS FBTgl, 
						IFNULL(ttpd.PDTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS PDTgl, IFNULL(ttsd.SDTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SDTgl, IFNULL(ttsk.SKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SKTgl, 
						IFNULL(tdcustomer.CusNama, '--') AS CusNama, tmotor.RKNo, IFNULL(ttrk.RKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS RKTgl, IFNULL(ttin.INNo, '--') AS INNo, IFNULL(ttin.INTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS INTgl 
						FROM tmotor 
						INNER JOIN 
						(SELECT MotorAutoN, MotorNoMesin, LokasiKode, NoTrans, Jam, Kondisi
						FROM vmotorlastlokasi 
						WHERE vmotorlastlokasi.Kondisi LIKE '%') LokasiMotor
						ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN
						LEFT OUTER JOIN ttss ON tmotor.SSNo = ttss.SSNo
						LEFT OUTER JOIN ttfb ON tmotor.FBNo = ttfb.FBNo
						LEFT OUTER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo
						LEFT OUTER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						LEFT OUTER JOIN ttsd ON tmotor.SDNo = ttsd.SDNo
						LEFT OUTER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
						LEFT OUTER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo 
						LEFT OUTER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
						LEFT OUTER JOIN ttin ON ttdk.INNo = ttin.INNo 
						WHERE (tmotor.MotorType LIKE '" . $query->typeMotor . "%' AND tmotor.MotorWarna LIKE '" . $query->warnaMotor . "%') 
						AND MotorTahun BETWEEN " . $query->thn1 . " AND " . $query->thn2;
					break;
				case 'TRANSIT' :
					$sqlraw = "SELECT
						tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorMemo,
						tmotor.DealerKode, tmotor.SSNo, tmotor.FBNo, tmotor.FBHarga, tmotor.DKNo, tmotor.SKNo, tmotor.PDNo, tmotor.SDNo, tmotor.BPKBNo,
						tmotor.BPKBTglJadi, tmotor.BPKBTglAmbil, tmotor.STNKNo, tmotor.STNKTglMasuk, tmotor.STNKTglBayar, tmotor.STNKTglJadi, tmotor.STNKTglAmbil,
						tmotor.STNKNama, tmotor.STNKAlamat, tmotor.PlatNo, tmotor.PlatTgl, tmotor.PlatTglAmbil, tmotor.FakturAHMNo, tmotor.FakturAHMTgl,
						tmotor.FakturAHMTglAmbil, 'In Transit' AS LokasiKode, '--' AS NoTrans, ttfb.FBTgl AS  Jam, 'Transit' AS Kondisi, 
						IFNULL(ttdk.DKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS DKTgl, IFNULL(ttss.SSTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SSTgl, IFNULL(ttfb.FBTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS FBTgl, 
						IFNULL(ttpd.PDTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS PDTgl, IFNULL(ttsd.SDTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SDTgl, IFNULL(ttsk.SKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SKTgl,
						IFNULL(tdcustomer.CusNama, '--') AS CusNama, tmotor.RKNo, IFNULL(ttrk.RKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS RKTgl, IFNULL(ttin.INNo, '--') AS INNo, IFNULL(ttin.INTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS INTgl
						FROM tmotor
						INNER JOIN ttfb ON tmotor.FBNo = ttfb.FBNo
						LEFT OUTER JOIN ttss ON tmotor.SSNo = ttss.SSNo
						LEFT OUTER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo 
						LEFT OUTER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						LEFT OUTER JOIN ttsd ON tmotor.SDNo = ttsd.SDNo
						LEFT OUTER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
						LEFT OUTER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo
						LEFT OUTER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
						LEFT OUTER JOIN ttin ON ttdk.INNo = ttin.INNo
						WHERE (tmotor.MotorType LIKE '" . $query->typeMotor . "%' AND tmotor.MotorWarna LIKE '" . $query->warnaMotor . "%') 
						AND MotorTahun BETWEEN " . $query->thn1 . " AND " . $query->thn2 . " AND Tmotor.SSNo = '--'";
					break;
				default :
					$sqlraw = "SELECT 
						tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType, tmotor.MotorWarna, 
						tmotor.MotorTahun, tmotor.MotorMemo, tmotor.DealerKode, tmotor.SSNo, tmotor.FBNo, tmotor.FBHarga,
						tmotor.DKNo, tmotor.SKNo, tmotor.PDNo, tmotor.SDNo, tmotor.BPKBNo, tmotor.BPKBTglJadi, tmotor.BPKBTglAmbil,
						tmotor.STNKNo, tmotor.STNKTglMasuk, tmotor.STNKTglBayar, tmotor.STNKTglJadi, tmotor.STNKTglAmbil, tmotor.STNKNama,
						tmotor.STNKAlamat, tmotor.PlatNo, tmotor.PlatTgl, tmotor.PlatTglAmbil, tmotor.FakturAHMNo, tmotor.FakturAHMTgl, tmotor.FakturAHMTglAmbil,
						LokasiMotor.LokasiKode, LokasiMotor.NoTrans, LokasiMotor.Jam, LokasiMotor.Kondisi,
						IFNULL(ttdk.DKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS DKTgl, IFNULL(ttss.SSTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SSTgl, IFNULL(ttfb.FBTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS FBTgl,
						IFNULL(ttpd.PDTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS PDTgl, IFNULL(ttsd.SDTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SDTgl, IFNULL(ttsk.SKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SKTgl,
						IFNULL(tdcustomer.CusNama, '--') AS CusNama, tmotor.RKNo, IFNULL(ttrk.RKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS RKTgl, IFNULL(ttin.INNo, '--') AS INNo, IFNULL(ttin.INTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS INTgl
						FROM tmotor 
						INNER JOIN
						(SELECT MotorAutoN, MotorNoMesin, LokasiKode, NoTrans, Jam, Kondisi
						FROM vmotorlastlokasi 
						WHERE vmotorlastlokasi.Kondisi LIKE '" . $query->tyStatus . "%') LokasiMotor
						ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN
						LEFT OUTER JOIN ttss ON tmotor.SSNo = ttss.SSNo
						LEFT OUTER JOIN ttfb ON tmotor.FBNo = ttfb.FBNo
						LEFT OUTER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo
						LEFT OUTER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						LEFT OUTER JOIN ttsd ON tmotor.SDNo = ttsd.SDNo
						LEFT OUTER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
						LEFT OUTER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo
						LEFT OUTER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
						LEFT OUTER JOIN ttin ON ttdk.INNo = ttin.INNo
						WHERE (tmotor.MotorType LIKE '" . $query->typeMotor . "%' AND tmotor.MotorWarna LIKE '" . $query->warnaMotor . "%') 
						AND MotorTahun BETWEEN " . $query->thn1 . " AND " . $query->thn2;
					break;
			}
		}
		return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Tmotor::class,
				'queryRaw'   => $sqlraw,
				'queryRawPK' => [ 'MotorAutoN', 'MotorNoMesin' ]
//				'joinWith' => [
//					'fakturBeli' => [
//						'type' => 'INNER JOIN'
//					],
//					'terimaBarangSupplier' => [
//						'type' => 'LEFT OUTER JOIN'
//					],
//					'terimaBarangDealer' => [
//						'type' => 'LEFT OUTER JOIN'
//					],
//					'suratJalanKonsumen' => [
//						'type' => 'LEFT OUTER JOIN'
//					],
//					'mutasiDealerLain' => [
//						'type' => 'LEFT OUTER JOIN'
//					],
//					'dataKonsumen' => [
//						'type' => 'LEFT OUTER JOIN'
//					],
//					'returKonsumen' => [
//						'type' => 'LEFT OUTER JOIN'
//					],
//					'konsumen' => [
//						'type' => 'LEFT OUTER JOIN'
//					],
//					'inden' => [
//						'type' => 'LEFT OUTER JOIN'
//					],
//				]
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Tmotor models.
	 * @return mixed
	 */
	public function actionIndex() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			General::cCmd("TRUNCATE tvmotorlokasi;	
			INSERT IGNORE INTO tvmotorlokasi SELECT * FROM vmotorlokasi;")->execute();
		}
		return $this->render( 'index');
	}
	/**
	 * Displays a single Tmotor model.
	 *
	 * @param integer $MotorAutoN
	 * @param string $MotorNoMesin
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $MotorAutoN, $MotorNoMesin ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $MotorAutoN, $MotorNoMesin ),
		] );
	}


	public function actionFind() {
		$params = \Yii::$app->request->post();
		$rows = [];

		if($params['mode'] === 'combo')
			$rows = Tmotor::find()->combo($params);

		return $this->responseDataJson($rows);
	}

	/**
	 * Updates an existing Tmotor model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 */
	public function actionUpdate( $id ) {
		/** @var Tmotor $model */
		$model = $this->findModelBase64( 'Tmotor', $id );
//	public function actionUpdate( $MotorAutoN, $MotorNoMesin ) {
//		$model = $this->findModel( $MotorAutoN, $MotorNoMesin );
		if ( Yii::$app->request->isPost ) {
			$connection  = Yii::$app->getDb();
			$transaction = $connection->beginTransaction();
			try {
				$MotorNoMesinOld = $model->MotorNoMesin;
				$MotorAutoNOld   = $model->MotorAutoN;
				$kode            = Custom::getKodeAkses();
				if ( $kode == "Admin" || $kode == "Auditor" ) {
					$model->MotorType = $_POST['MotorType'];
				}
				$model->MotorWarna    = $_POST['MotorWarna'];
				$model->MotorNoMesin  = $_POST['Tmotor']['MotorNoMesin'];
				$model->MotorNoRangka = $_POST['Tmotor']['MotorNoRangka'];
				$model->MotorTahun    = $_POST['Tmotor']['MotorTahun'];
				$model->save();
				$connection->createCommand( "Update ttpbit SET
				MotorNoMesin = :MotorNoMesinNew WHERE MotorNoMesin = :MotorNoMesin 
				AND MotorAutoN = :MotorAutoN", [
					':MotorNoMesinNew' => $_POST['Tmotor']['MotorNoMesin'],
					':MotorNoMesin'    => $MotorNoMesinOld,
					':MotorAutoN'      => $MotorAutoNOld,
				] )->execute();
				$connection->createCommand( "Update ttsmit SET
				MotorNoMesin = :MotorNoMesinNew WHERE MotorNoMesin = :MotorNoMesin 
				AND MotorAutoN = :MotorAutoN", [
					':MotorNoMesinNew' => $_POST['Tmotor']['MotorNoMesin'],
					':MotorNoMesin'    => $MotorNoMesinOld,
					':MotorAutoN'      => $MotorAutoNOld,
				] )->execute();
				$connection->createCommand( "DELETE FROM tvmotorlokasi; 
				INSERT INTO tvmotorlokasi SELECT * FROM vmotorlokasi;" )->execute();
				$transaction->commit();
			} catch ( \Exception $e ) {
				$transaction->rollBack();
				throw $e;
			} catch ( \Throwable $e ) {
				$transaction->rollBack();
				throw $e;
			}
			return $this->redirect( [ 'tmotor/index', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
		return $this->render( 'update', [
			'model' => $model,
			'id'    => $this->generateIdBase64FromModel( $model ),
			'ro'    => json_decode( base64_decode( $_GET['ro'] ) )
		] );
	}
	/**
	 * Deletes an existing Tmotor model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $MotorAutoN
	 * @param string $MotorNoMesin
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete( $MotorAutoN, $MotorNoMesin ) {
		$this->findModel( $MotorAutoN, $MotorNoMesin )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Tmotor model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $MotorAutoN
	 * @param string $MotorNoMesin
	 *
	 * @return Tmotor the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $MotorAutoN, $MotorNoMesin ) {
		if ( ( $model = Tmotor::findOne( [
				'MotorAutoN'   => $MotorAutoN,
				'MotorNoMesin' => $MotorNoMesin
			] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
