<?php

namespace aunit\controllers;

use Yii;
use aunit\models\Ttcfit;
use yii\data\ActiveDataProvider;
use aunit\components\AunitController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TtcfitController implements the CRUD actions for Ttcfit model.
 */
class TtcfitController extends AunitController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ttcfit models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Ttcfit::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ttcfit model.
     * @param string $CFNo
     * @param integer $MotorAutoN
     * @param string $MotorNoMesin
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($CFNo, $MotorAutoN, $MotorNoMesin)
    {
        return $this->render('view', [
            'model' => $this->findModel($CFNo, $MotorAutoN, $MotorNoMesin),
        ]);
    }

    /**
     * Creates a new Ttcfit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ttcfit();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'CFNo' => $model->CFNo, 'MotorAutoN' => $model->MotorAutoN, 'MotorNoMesin' => $model->MotorNoMesin]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ttcfit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $CFNo
     * @param integer $MotorAutoN
     * @param string $MotorNoMesin
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($CFNo, $MotorAutoN, $MotorNoMesin)
    {
        $model = $this->findModel($CFNo, $MotorAutoN, $MotorNoMesin);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'CFNo' => $model->CFNo, 'MotorAutoN' => $model->MotorAutoN, 'MotorNoMesin' => $model->MotorNoMesin]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ttcfit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $CFNo
     * @param integer $MotorAutoN
     * @param string $MotorNoMesin
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($CFNo, $MotorAutoN, $MotorNoMesin)
    {
        $this->findModel($CFNo, $MotorAutoN, $MotorNoMesin)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ttcfit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $CFNo
     * @param integer $MotorAutoN
     * @param string $MotorNoMesin
     * @return Ttcfit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($CFNo, $MotorAutoN, $MotorNoMesin)
    {
        if (($model = Ttcfit::findOne(['CFNo' => $CFNo, 'MotorAutoN' => $MotorAutoN, 'MotorNoMesin' => $MotorNoMesin])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
