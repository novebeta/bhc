<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\models\Ttbkhd;
use aunit\models\Ttbkit;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtbkitController implements the CRUD actions for Ttbkit model.
 */
class TtbkitController extends AunitController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttbkit models.
	 * @return mixed
	 */
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttbkit::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
			/** @var Ttbkit $model */
			$model = null;
			if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
				$model = $this->findModelBase64( 'ttbkit', $requestData[ 'id' ] );
			}
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'Action' ] = 'Insert';
						$requestData[ 'FBNo' ]   = '--';
					} else {
						$requestData[ 'Action' ] = 'Update';
						if ( $model != null ) {
							$requestData[ 'BKNoOLD' ] = $model->BKNo;
							$requestData[ 'FBNoOLD' ] = $model->FBNo;
						}
					}
					if ( isset( $_GET[ 'BKJenis' ] ) && $_GET[ 'BKJenis' ] == 'Bayar BBN' ) {
						$result = Ttbkit::find()->callSPPengajuan( $requestData );
					} else {
						$result = Ttbkit::find()->callSP( $requestData );
					}
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'batchPengajuan' :
					\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
					$respon                      = $this->responseFailed( 'Add Item' );
					$base64                      = urldecode( base64_decode( $requestData[ 'header' ] ) );
					parse_str( $base64, $header );
					$header[ 'Action' ]          = 'Loop';
					$header[ 'BKJam' ]           = $header[ 'BKTgl' ] . ' ' . $header[ 'BKJam' ];
					$result                      = Ttbkhd::find()->callSPPengajuan( $header );
					foreach ( $requestData[ 'data' ] as $item ) {
						$items[ 'Action' ]  = 'Insert';
						$items[ 'BKNo' ]    = $header[ 'BKNo' ];
						$items[ 'FBNo' ]    = $item[ 'id' ];
						$items[ 'BKBayar' ] = $item[ 'data' ][ 'BKBayar' ];
						$result             = Ttbkit::find()->callSPPengajuan( $items );
					}
					return $this->responseSuccess( $requestData );
					break;
				case 'batch' :
					\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
					$respon                      = $this->responseFailed( 'Add Item' );
					$base64                      = urldecode( base64_decode( $requestData[ 'header' ] ) );
					parse_str( $base64, $header );
//					$header                      = $requestData[ 'header' ];
					$header[ 'BKJam' ]  = $header[ 'BKTgl' ] . ' ' . $header[ 'BKJam' ];
					$header[ 'Action' ]          = 'Loop';
					$result                      = Ttbkhd::find()->callSP( $header );
					foreach ( $requestData[ 'data' ] as $item ) {
						$items[ 'Action' ]  = 'Insert';
						$items[ 'BKNo' ]    = $header[ 'BKNo' ];
						$items[ 'FBNo' ]    = $item[ 'id' ];
						$items[ 'BKBayar' ] = $item[ 'data' ][ 'BKBayar' ];
						$result             = Ttbkit::find()->callSP( $items );
					}
					$header[ 'Action' ] = 'LoopAfter';
					$result             = Ttbkhd::find()->callSP( $header );
					return $this->responseSuccess( $requestData );
					break;
				case 'del'  :
					$requestData             = array_merge( $requestData, $model->attributes );
					$requestData[ 'Action' ] = 'Delete';
					if ( $model != null ) {
						$requestData[ 'BKNoOLD' ] = $model->BKNo;
						$requestData[ 'FBNoOLD' ] = $model->FBNo;
					}
					if ( isset( $_GET[ 'BKJenis' ] ) && $_GET[ 'BKJenis' ] == 'Pengajuan' ) {
						$result = Ttbkit::find()->callSPPengajuan( $requestData );
					} else {
						$result = Ttbkit::find()->callSP( $requestData );
					}
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
			}
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'ttbkit.' . $primaryKeys[ $i ];
			}
			/** @var Ttbkhd $model */
			$model = Ttbkhd::find()->where( [ 'BKNo' => $requestData[ 'BKNo' ] ] )->one();
			$query = new Query();
			switch ( $model->BKJenis ) {
				case 'Bayar Unit' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,ttbkit.*, bkPaid.Paid - ttbkit.BKBayar AS Terbayar,
			         ttbkit.BKBayar, ttbkit.FBTotal - bkPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(SELECT ttbkit.BKNo, ttbkit.FBNo, ttfb.FBTgl, ttfb.SSNo, ttss.SSTgl, ttfb.FBTotal, ttbkit.BKBayar , ttfb.FBPSS, 
			              ttfb.FBPtgLain, ttfb.FBExtraDisc FROM ttbkit 
				            INNER JOIN ttfb ON ttbkit.FBNo = ttfb.FBNo 
				            INNER JOIN ttbkhd ON ttbkit.BKNo = ttbkhd.BKNo 
				            LEFT OUTER JOIN ttss ON ttfb.SSNo = ttss.SSNo WHERE (ttbkit.BKNo = :BKNo ) ORDER BY ttbkit.FBNo) ttbkit" ) )
					      ->addParams( [ ':BKNo' => $model->BKNo ] )
					      ->innerJoin( "(SELECT KKLINK AS FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'Bayar Unit') GROUP BY KKLINK) bkPaid",
						      "ttbkit.FBNo = bkPaid.FBNo" );
					break;
				case 'Bayar BBN' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,ttbkit.*, bkPaid.Paid - ttbkit.BKBayar AS Terbayar, 
			                ttbkit.BKBayar, ttbkit.BBN - bkPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(SELECT ttbkit.BKNo, ttbkit.FBNo, ttdk.DKTgl, ttdk.CusKode, tdcustomer.CusNama, CONCAT_WS('', tdcustomer.CusAlamat, '  RT/RW : ', tdcustomer.CusRT, '/', tdcustomer.CusRW) AS CusAlamat, 
				            tmotor.MotorType, tmotor.MotorNoMesin, ttdk.LeaseKode, tmotor.MotorTahun, ttbkit.BKBayar,
				            (ttdk.BBN+ttdk.JABBN) AS BBN  FROM ttbkit 
				            INNER JOIN ttdk ON ttbkit.FBNo = ttdk.DKNo 
				            INNER JOIN tmotor ON ttbkit.FBNo = tmotor.DKNo 
				            INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
				            INNER JOIN ttbkhd ON ttbkit.BKNo = ttbkhd.BKNo WHERE (ttbkit.BKNo = :BKNo) ORDER BY ttbkit.FBNo ) ttbkit" ) )
					      ->addParams( [ ':BKNo' => $model->BKNo ] )
					      ->innerJoin( "(SELECT KKLINK AS FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'Bayar BBN') GROUP BY KKLINK) bkPaid",
						      "ttbkit.FBNo = bkPaid.FBNo" );
					break;
				case 'BBN Plus' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,ttbkit.*, bkPaid.Paid - ttbkit.BKBayar AS Terbayar, 
			        ttbkit.BKBayar, ttbkit.BBN - bkPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(SELECT ttbkit.BKNo, ttbkit.FBNo, ttdk.DKTgl, ttdk.CusKode, tdcustomer.CusNama, CONCAT_WS('', tdcustomer.CusAlamat, '  RT/RW : ', tdcustomer.CusRT, '/', tdcustomer.CusRW) AS CusAlamat, 
				            tmotor.MotorType, tmotor.MotorNoMesin, ttdk.LeaseKode, tmotor.MotorTahun, ttbkit.BKBayar,
				            (ttdk.BBNPlus) AS BBN  FROM ttbkit 
				            INNER JOIN ttdk ON ttbkit.FBNo = ttdk.DKNo 
				            INNER JOIN tmotor ON ttbkit.FBNo = tmotor.DKNo 
				            INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
				            INNER JOIN ttbkhd ON ttbkit.BKNo = ttbkhd.BKNo WHERE (ttbkit.BKNo = :BKNo ) ORDER BY ttbkit.FBNo ) ttbkit" ) )
					      ->addParams( [ ':BKNo' => $model->BKNo ] )
					      ->innerJoin( "(SELECT KKLINK AS FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'BBN Plus') GROUP BY KKLINK) bkPaid",
						      "ttbkit.FBNo = bkPaid.FBNo" );
					break;
				case 'SubsidiDealer2' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,ttbkit.*, bkPaid.Paid - ttbkit.BKBayar AS Terbayar, 
			        ttbkit.BKBayar, ttbkit.BBN - bkPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(SELECT ttbkit.BKNo, ttbkit.FBNo, ttdk.DKTgl, ttdk.CusKode, tdcustomer.CusNama, CONCAT_WS('', tdcustomer.CusAlamat, '  RT/RW : ', tdcustomer.CusRT, '/', tdcustomer.CusRW) AS CusAlamat, 
				             tmotor.MotorType, tmotor.MotorNoMesin, ttdk.LeaseKode, ttbkit.BKBayar,
				             (ttdk.PotonganHarga + ttdk.JAPotonganHarga) AS BBN FROM ttbkit 
				             INNER JOIN ttdk ON ttbkit.FBNo = ttdk.DKNo 
				             INNER JOIN tmotor ON ttbkit.FBNo = tmotor.DKNo 
				             INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
				             WHERE (ttbkit.BKNo = :BKNo ) ORDER BY ttbkit.FBNo ) ttbkit" ) )
					      ->addParams( [ ':BKNo' => $model->BKNo ] )
					      ->innerJoin( "(SELECT KKLINK AS FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'SubsidiDealer2') GROUP BY KKLINK) bkPaid",
						      "ttbkit.FBNo = bkPaid.FBNo" );
					break;
				case 'Retur Harga' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,ttbkit.*, bkPaid.Paid - ttbkit.BKBayar AS Terbayar, 
			        ttbkit.BKBayar, ttbkit.BBN - bkPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(SELECT ttbkit.BKNo, ttbkit.FBNo, ttdk.DKTgl, ttdk.CusKode, tdcustomer.CusNama, CONCAT_WS('', tdcustomer.CusAlamat, '  RT/RW : ', tdcustomer.CusRT, '/', tdcustomer.CusRW   ) AS CusAlamat, 
				            tmotor.MotorType, tmotor.MotorNoMesin, ttdk.LeaseKode, ttbkit.BKBayar, 
				            (ttdk.ReturHarga + ttdk.JAReturHarga) AS BBN FROM ttbkit 
				            INNER JOIN ttdk ON ttbkit.FBNo = ttdk.DKNo 
				            INNER JOIN tmotor ON ttbkit.FBNo = tmotor.DKNo 
				            INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode WHERE (ttbkit.BKNo = :BKNo) ORDER BY ttbkit.FBNo ) ttbkit" ) )
					      ->addParams( [ ':BKNo' => $model->BKNo ] )
					      ->innerJoin( "(SELECT KKLINK AS FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'Retur Harga') GROUP BY KKLINK) bkPaid",
						      "ttbkit.FBNo = bkPaid.FBNo" );
					break;
				case 'Potongan Khusus' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,ttbkit.*, bkPaid.Paid - ttbkit.BKBayar AS Terbayar, 
			        ttbkit.BKBayar, ttbkit.BBN - bkPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(SELECT ttbkit.BKNo, ttbkit.FBNo, ttdk.DKTgl, ttdk.CusKode, tdcustomer.CusNama, CONCAT_WS('', tdcustomer.CusAlamat, '  RT/RW : ', tdcustomer.CusRT, '/', tdcustomer.CusRW   ) AS CusAlamat, 
				            tmotor.MotorType, tmotor.MotorNoMesin, ttdk.LeaseKode, ttbkit.BKBayar, 
				            (ttdk.PotonganKhusus+ttdk.JAPotonganKhusus) AS BBN FROM ttbkit 
				            INNER JOIN ttdk ON ttbkit.FBNo = ttdk.DKNo 
				            INNER JOIN tmotor ON ttbkit.FBNo = tmotor.DKNo 
				            INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode  WHERE (ttbkit.BKNo = 'MyNumber' ) ORDER BY ttbkit.FBNo ) ttbkit" ) )
					      ->addParams( [ ':BKNo' => $model->BKNo ] )
					      ->innerJoin( "(SELECT KKLINK AS FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'Potongan Khusus') GROUP BY KKLINK) bkPaid",
						      "ttbkit.FBNo = bkPaid.FBNo" );
					break;
				case 'Insentif Sales' :
					$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,ttbkit.*, bkPaid.Paid - ttbkit.BKBayar AS Terbayar, 
			        ttbkit.BKBayar, ttbkit.BBN - bkPaid.Paid AS Sisa' ) )
					      ->from( new Expression( "(SELECT ttbkit.BKNo, ttbkit.FBNo, ttdk.DKTgl, ttdk.CusKode, tdcustomer.CusNama, CONCAT_WS('', tdcustomer.CusAlamat, '  RT/RW : ', tdcustomer.CusRT, '/', tdcustomer.CusRW   ) AS CusAlamat, 
				            tmotor.MotorType, tmotor.MotorNoMesin, ttdk.LeaseKode, ttbkit.BKBayar, 
				            (ttdk.InsentifSales+ttdk.JAInsentifSales) AS BBN FROM ttbkit 
				            INNER JOIN ttdk ON ttbkit.FBNo = ttdk.DKNo 
				            INNER JOIN tmotor ON ttbkit.FBNo = tmotor.DKNo 
				            INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode WHERE (ttbkit.BKNo = :BKNo ) ORDER BY ttbkit.FBNo) ttbkit" ) )
					      ->addParams( [ ':BKNo' => $model->BKNo ] )
					      ->innerJoin( "(SELECT KKLINK AS FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'Insentif Sales') GROUP BY KKLINK) bkPaid",
						      "ttbkit.FBNo = bkPaid.FBNo" );
					break;
			}
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	/**
	 * Displays a single Ttbkit model.
	 *
	 * @param string $BKNo
	 * @param string $FBNo
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $BKNo, $FBNo ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $BKNo, $FBNo ),
		] );
	}
	/**
	 * Creates a new Ttbkit model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Ttbkit();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'BKNo' => $model->BKNo, 'FBNo' => $model->FBNo ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Ttbkit model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $BKNo
	 * @param string $FBNo
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $BKNo, $FBNo ) {
		$model = $this->findModel( $BKNo, $FBNo );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'BKNo' => $model->BKNo, 'FBNo' => $model->FBNo ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Ttbkit model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $BKNo
	 * @param string $FBNo
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $BKNo, $FBNo ) {
		$this->findModel( $BKNo, $FBNo )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Ttbkit model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $BKNo
	 * @param string $FBNo
	 *
	 * @return Ttbkit the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $BKNo, $FBNo ) {
		if ( ( $model = Ttbkit::findOne( [ 'BKNo' => $BKNo, 'FBNo' => $FBNo ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
