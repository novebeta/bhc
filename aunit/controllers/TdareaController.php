<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Tdarea;
use Yii;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
class TdareaController extends AunitController {
	public function actions() {
		return [
			'td' => [
				'class' => TdAction::className(),
				'model' => Tdarea::className(),
			],
		];
	}
	public function actionIndex() {
		return $this->render( 'index' );
	}
	/**
	 * Creates a new Tdarea model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		TUi::$actionMode = Menu::ADD;
		$model           = new Tdarea();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'tdarea/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
		}else{
            Yii::$app->session->addFlash(  'warning', Html::errorSummary($model) );
        }
		return $this->render( 'create', [
			'model' => $model,
			'id'    => 'new'
		] );
	}
	/**
	 * Updates an existing Tdarea model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$index = \yii\helpers\Url::toRoute( [ 'tdarea/index' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		$model = $this->findModelBase64( 'Tdarea', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'tdarea/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
		}else{
            Yii::$app->session->addFlash(  'warning', Html::errorSummary($model) );
        }
		return $this->render( 'update', [
			'model' => $model,
			'id'    => $this->generateIdBase64FromModel( $model ),
		] );
	}
	public function actionCancel( $id, $action ) {
		$model = Tdarea::findOne( $id );
		if ( $model == null ) {
			$model = Tdarea::find()->orderBy( [ 'AreaStatus' => SORT_ASC ] )->one();
			if ( $model == null ) {
				return $this->redirect( [ 'tdarea/index' ] );
			}
		}
		return $this->redirect( [ 'tdarea/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
	}
	/**
	 *
	 */
	public function actionFind() {
		$params = \Yii::$app->request->post();
		$rows   = [];
		if ( $params[ 'mode' ] === 'combo' ) {
			$rows = Tdarea::find()->combo( $params );
		}
		return $this->responseDataJson( $rows );
	}
	protected function findModel( $Provinsi, $Kabupaten, $Kecamatan, $Kelurahan, $KodePos ) {
		if ( ( $model = Tdarea::findOne( [
				'Provinsi'  => $Provinsi,
				'Kabupaten' => $Kabupaten,
				'Kecamatan' => $Kecamatan,
				'Kelurahan' => $Kelurahan,
				'KodePos'   => $KodePos
			] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}

    public function actionDelete( $id ) {
        /** @var Tdarea $model */
        $request = Yii::$app->request;
        $model             = $this->findModelBase64( 'Tdarea', $id );
        if ( $model->delete() ) {
            if ($request->isAjax) {
                return $this->responseSuccess( 'Berhasil menghapus data Area');
            }
            $model = Tdarea::find()->one();
            return $this->redirect( [ 'tdarea/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );

        } else {
            return $this->responseFailed( 'Gagal menghapus data Area' );
        }
    }
}
