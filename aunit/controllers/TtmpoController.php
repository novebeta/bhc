<?php

namespace aunit\controllers;

use aunit\components\AunitController;
use aunit\components\TdAction;
use Yii;
use aunit\models\Ttmpo;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TtmpoController implements the CRUD actions for Ttmpo model.
 */
class TtmpoController extends AunitController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	public function actions() {
		$colGrid  = null;
		$joinWith = [
			'motorTypes'              => [ 'type' => 'INNER JOIN' ],
		];
		$join     = [];
		$where    = [];
//		if ( isset( $_POST['query'] ) ) {
//			$r = Json::decode( $_POST['query'], true )['r'];
//			switch ( $r ) {
//				case 'ttsd/mutasi-eksternal-dealer':
//					$colGrid  = Ttsd::colGridMutasiEksternalDealer();
//					$joinWith = [
//						'lokasi' => [
//							'type' => 'INNER JOIN'
//						],
//						'dealer' => [
//							'type' => 'INNER JOIN'
//						],
//					];
//					break;
//				case 'ttsd/invoice-eksternal':
//					$colGrid  = Ttsd::colGridInvoiceEksternal();
//					$joinWith = [
//						'lokasi' => [
//							'type' => 'INNER JOIN'
//						],
//						'dealer' => [
//							'type' => 'INNER JOIN'
//						],
//					];
//					break;
//			}
//		}
		return [
			'td' => [
				'class'    => TdAction::className(),
				'model'    => Ttmpo::className(),
				'colGrid'  => $colGrid,
				'joinWith' => $joinWith
			],
		];
	}

	/**
	 * Lists all Ttmmarketshare models.
	 * @return mixed
	 */
	public function actionIndex()
	{

//	    if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
//		    $post[ 'Action' ] = 'Display';
//		    $result           = Ttmteamtarget::find()->callSP( $post );
//		    Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
//	    }
		return $this->render( 'index' );
	}

    /**
     * Displays a single Ttmpo model.
     * @param string $PONo
     * @param string $POTgl
     * @param string $MotorType
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($PONo, $POTgl, $MotorType)
    {
        return $this->render('view', [
            'model' => $this->findModel($PONo, $POTgl, $MotorType),
        ]);
    }

    /**
     * Creates a new Ttmpo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ttmpo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'PONo' => $model->PONo, 'POTgl' => $model->POTgl, 'MotorType' => $model->MotorType]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ttmpo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $PONo
     * @param string $POTgl
     * @param string $MotorType
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($PONo, $POTgl, $MotorType)
    {
        $model = $this->findModel($PONo, $POTgl, $MotorType);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'PONo' => $model->PONo, 'POTgl' => $model->POTgl, 'MotorType' => $model->MotorType]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ttmpo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $PONo
     * @param string $POTgl
     * @param string $MotorType
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($PONo, $POTgl, $MotorType)
    {
        $this->findModel($PONo, $POTgl, $MotorType)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ttmpo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $PONo
     * @param string $POTgl
     * @param string $MotorType
     * @return Ttmpo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($PONo, $POTgl, $MotorType)
    {
        if (($model = Ttmpo::findOne(['PONo' => $PONo, 'POTgl' => $POTgl, 'MotorType' => $MotorType])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
