<?php

namespace aunit\controllers;

use Yii;
use aunit\models\Ttkg;
use yii\data\ActiveDataProvider;
use aunit\components\AunitController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use common\components\General;
use GuzzleHttp\Client;
/**
 * TtkgController implements the CRUD actions for Ttkg model.
 */
class TtkgController extends AunitController
{
    public function actions() {
        $colGrid    = null;
		$joinWith   = [];
		$join       = [];
		$where      = [];
		$colGrid    = null;
		$sqlraw     = null;
		$queryRawPK = null;
		if ( isset( $_POST[ 'query' ] ) ) {
            $json      = Json::decode( $_POST[ 'query' ], true );
            $r         = $json[ 'r' ];
            $urlParams = $json[ 'urlParams' ];
            switch ( $r ) {
                case 'ttkg/index':
                    $colGrid    = Ttkg::colGrid();
                    $sqlraw     = "SELECT ttkg.KGNo,ttkg.KGTgl,	tdcustomer.CusNama,	ttkg.DKNo,ttdk.DKTgl,ttkg.KGJenis,ttkg.KGNominal,
                    ttkg.KGPengirim,ttkg.KGPenerima,ttkg.KGMemo,ttkg.KGStatus,ttkg.UserID,
                    CONCAT(tdcustomer.CusAlamat, 'RT/RW : ', tdcustomer.CusRT, '/', tdcustomer.CusRW) AS CusAlamat,
                    CONCAT(tdcustomer.CusKelurahan, ', ', tdcustomer.CusKecamatan, ', ', tdcustomer.CusKabupaten, ', ', tdcustomer.CusProvinsi) AS CusArea,
                    CONCAT(tdmotortype.MotorNama, ' - ', tmotor.MotorWarna, ' - ', tdmotortype.MotorCC, 'CC', ' - ', tmotor.MotorTahun) AS TypeMotor,
                    CONCAT(tmotor.MotorType, ' - ', tmotor.MotorNoMesin, ' - ', tmotor.MotorNoRangka) AS NoSinNoKa
                    FROM ttkg
                    LEFT OUTER JOIN ttdk ON ttdk.DKNo = ttkg.DKNo
                    LEFT OUTER JOIN tdcustomer	ON ttdk.CusKode = tdcustomer.CusKode
                    LEFT OUTER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
                    LEFT OUTER JOIN tdmotortype	ON tdmotortype.MotorType = tmotor.MotorType";
                    $queryRawPK = [ 'KGNo' ];
                    break;
                case 'ttkg/select':
                    $colGrid    = Ttkg::colGridSelect();
                    $sqlraw     = "SELECT ttdk.DKNo, DKTgl, CusNama, MotorNama,'UMPK' AS Jenis, DKDPLLeasing AS Nominal, IFNULL(KGNo,'--') AS KGNo, KGJenis FROM  ttdk
                    INNER JOIN tdcustomer ON tdcustomer.CusKode = ttdk.CusKode
                    INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
                    INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType
                    LEFT OUTER JOIN ttkg ON ttkg.DKNo = ttdk.DKNo AND ttkg.KGJenis = 'UMPK'
                    WHERE DKDPLLeasing > 0 AND (KGNo IS NULL OR KGNo = '{$urlParams['KGNo']}')
                    UNION
                    SELECT ttdk.DKNo, DKTgl, CusNama, MotorNama,'UMIN' AS Jenis, DKDPInden  AS Nominal, IFNULL(KGNo,'--') AS KGNo, KGJenis FROM  ttdk
                    INNER JOIN tdcustomer ON tdcustomer.CusKode = ttdk.CusKode
                    INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
                    INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType
                    LEFT OUTER JOIN ttkg ON ttkg.DKNo = ttdk.DKNo AND ttkg.KGJenis = 'UMIN'
                    WHERE DKDPInden > 0  AND (KGNo IS NULL OR KGNo = '{$urlParams['KGNo']}')
                    UNION
                    SELECT ttdk.DKNo, DKTgl, CusNama, MotorNama,'UMDP' AS Jenis, DKDPTerima  AS Nominal, IFNULL(KGNo,'--') AS KGNo, KGJenis FROM  ttdk
                    INNER JOIN tdcustomer ON tdcustomer.CusKode = ttdk.CusKode
                    INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
                    INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType
                    LEFT OUTER JOIN ttkg ON ttkg.DKNo = ttdk.DKNo AND ttkg.KGJenis = 'UMDP'
                    WHERE DKDPTerima > 0  AND (KGNo IS NULL OR KGNo = '{$urlParams['KGNo']}')
                    UNION
                    SELECT ttdk.DKNo, DKTgl, CusNama, MotorNama, 'SISA' AS Jenis, DKNetto AS Nominal, IFNULL(KGNo,'--') AS KGNo, KGJenis FROM  ttdk
                    INNER JOIN tdcustomer ON tdcustomer.CusKode = ttdk.CusKode
                    INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
                    INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType
                    LEFT OUTER JOIN ttkg ON ttkg.DKNo = ttdk.DKNo AND ttkg.KGJenis = 'SISA'
                    WHERE DKNetto > 0 AND DKJenis = 'Tunai'  AND (KGNo IS NULL OR KGNo = '{$urlParams['KGNo']}')";
                    $queryRawPK = [ 'DKNo' ];
                    break;
            }
        }
        return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Ttkg::class,
				'queryRaw'   => $sqlraw,
				'queryRawPK' => $queryRawPK,
				'colGrid'    => $colGrid,
				'joinWith'   => $joinWith,
				'join'       => $join,
				'where'      => $where,
			],
		];
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ttkg models.
     * @return mixed
     */
    public function actionIndex()
    {
        if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkg::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'index' );
    }

    /**
     * Creates a new Ttkg model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        TUi::$actionMode   = Menu::ADD;
        $model = new Ttkg();
        $model->KGNo = General::createTemporaryNo('KG', 'ttkg', 'KGNo');
        $model->KGTgl = date('Y-m-d H:i:s');
        $model->UserID = $this->UserID();
        $model->save();
        $post = $model->attributes;
        $post['Action'] = 'Insert';
        $result = Ttkg::find()->callSP($post);
        Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        $id                    = $this->generateIdBase64FromModel( $model );
        $data = Ttkg::find()->dsTUang()->where(['ttkg.KGNo' => $model->KGNo])->asArray(true)->one();
        $data['KGNoView'] = $result['KGNo'];
		return $this->render( 'create', [
			'model'  => $model,
			'data' => $data,
			'id'     => $id
		] );
    }

    /**
     * Updates an existing Ttkg model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $index = \yii\helpers\Url::toRoute(['ttkg/index']);
        if (!isset($_GET['action'])) {
            return $this->redirect($index);
        }
        $action = $_GET['action'];
        switch ($action) {
            case 'create':
                TUi::$actionMode = Menu::ADD;
                break;
            case 'update':
                TUi::$actionMode = Menu::UPDATE;
                break;
            case 'view':
                TUi::$actionMode = Menu::VIEW;
                break;
            case 'display':
                TUi::$actionMode = Menu::DISPLAY;
                break;
        }
        /** @var Ttkg $model */
        $model = $this->findModelBase64('Ttkg', $id);
        $data = Ttkg::find()->dsTUang()->where(['ttkg.KGNo' => $model->KGNo])->asArray(true)->one();
        if (Yii::$app->request->isPost) {
            $post = array_merge($data, \Yii::$app->request->post());
            $post['Action'] = 'Update';
            $result = Ttkg::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
            $model = Ttkg::findOne($result['KGNo']);
            $id = $this->generateIdBase64FromModel($model);
            if ($result['status'] == 0 && $result['status'] !== null) {
                $post['KGNoView'] = $result['KGNo'];
                return $this->redirect(['ttkg/update', 'action' => 'display', 'id' => $id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'data' => $post,
                    'id' => $id,
                ]);
            }
        }
        if ( ! isset( $_GET[ 'oper' ] ) ) {
			$data['Action'] = 'Load';
            $result = Ttkg::find()->callSP($data);
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'KGNo' ] = $data['KGNo'];
		}
		$data[ 'KGNoView' ] = $_GET[ 'KGNoView' ] ?? $result[ 'KGNo' ];
        return $this->render('update', [
            'model' => $model,
            'data' => $data,
            'id' => $id
        ]);
    }

    public function actionCancel($id, $action)
    {
        /** @var Ttkg $model */
        $_POST['Action'] = 'Cancel';
        $result = Ttkg::find()->callSP($_POST);
        Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        if ($result['KGNo'] == '--') {
            return $this->redirect(['index']);
        } else {
            $model = Ttkg::findOne($result['KGNo']);
            return $this->redirect(['ttkg/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
        }
    }

    public function actionSelect() {
		$this->layout = 'select-mode';
		return $this->render( 'select', [
			'title'   => 'Daftar Program',

            
			'arr'     => [
				'cmbTxt'    => [
					'DKNo'               => 'No DK',
					'CusNama'            => 'Nama Konsumen',
					'MotorNama'            => 'Motor',
					'Jenis'          => 'Jenis',
				],
				'cmbTgl'    => [
					'DATE(DKTgl)' => 'Tgl DK'
				],
				'cmbNum'    => [
					'Nominal'    => 'Nominal',
				],
				'sortname'  => "DKTgl",
				'sortorder' => 'desc'
			],
			'options' => [
				'colGrid' => Ttkg::colGridSelect(),
				'mode'    => self::MODE_SELECT,
			]
		] );
	}

    public function actionLoop() {
		$_POST[ 'Action' ] = 'Loop';
		$result            = Ttkg::find()->callSP( $_POST );
        if($result[ 'status' ] == 1){
            Yii::$app->session->addFlash( 'warning', $result[ 'keterangan' ] );
        }
	}

    public function actionLoopDk() {
		$_POST[ 'Action' ] = 'LoopAfter';
		$result            = Ttkg::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
	}

    public function actionPrint($id)
    {
        unset($_POST['_csrf-app']);
        $_POST = array_merge($_POST, Yii::$app->session->get('_company'));
        $_POST['db'] = base64_decode($_COOKIE['_outlet']);
        $_POST['UserID'] = Menu::getUserLokasi()['UserID'];
        $client = new Client([
            'headers' => ['Content-Type' => 'application/octet-stream']
        ]);
        $response = $client->post(Yii::$app->params['H1'][$_POST['db']]['host_report'] . '/aunit/kwkg', [
            'body' => Json::encode($_POST)
        ]);
        return Yii::$app->response->sendContentAsFile( $response->getBody(), $_POST['KGNo'].'.pdf', [
            'inline'   => true,
            'mimeType' => 'application/pdf'
        ]);
    }

    /**
     * Deletes an existing Ttkg model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        return $this->deleteWithSP(['modelName'=>'Ttkg']);
    }

    /**
     * Finds the Ttkg model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Ttkg the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ttkg::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
