<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\models\Tdbaranglokasi;
use aunit\models\Tdlokasi;
use aunit\models\Tttihd;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TttihdController implements the CRUD actions for Tttihd model.
 */
class TttihdController extends AunitController {
	public function actions() {
        $where      = [
            [
                'op'        => 'AND',
                'condition' => "(TINo NOT LIKE '%=%')",
                'params'    => []
            ]
        ];
		return [
			'td' => [
				'class' => TdAction::class,
				'model' => Tttihd::class,
                'where' => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Tttihd models.
	 * @return mixed
	 */
	public function actionIndex() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Tttihd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'index', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'twtihd/td' ] )
		] );
	}
	/**
	 * Creates a new Tttihd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$LokasiKodeCmb       = Tdlokasi::find()->where( [ 'LokasiStatus' => 'A', 'LEFT(LokasiNomor,1)' => '7' ] )->orderBy( 'LokasiStatus, LokasiKode' )->one();
		$model               = new Tttihd();
		$model->TINo         = General::createTemporaryNo( 'TI', 'Tttihd', 'TINo' );
		$model->TITgl        = date( 'Y-m-d H:i:s' );
		$model->TIKeterangan = "--";
		$model->TITotal      = 0;
		$model->PSNo         = "--";
		$model->KodeTrans    = "TI";
		$model->Cetak        = "Belum";
		$model->PosKode      = $this->LokasiKode();
		$model->LokasiAsal   = ( $LokasiKodeCmb != null ) ? $LokasiKodeCmb->LokasiKode : '';
		$model->LokasiTujuan = ( $LokasiKodeCmb != null ) ? $LokasiKodeCmb->LokasiKode : '';
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Tttihd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTMutasi               = Tttihd::find()->tttihdFillByNo()->where( [ 'tttihd.TINo' => $model->TINo ] )->asArray( true )->one();
		$dsTMutasi[ 'TIJam' ]    = $dsTMutasi[ 'TITgl' ];
		$id                      = $this->generateIdBase64FromModel( $model );
		$dsTMutasi[ 'TINoView' ] = $result[ 'TINo' ];
		return $this->render( 'create', [
			'model'     => $model,
			'dsTMutasi' => $dsTMutasi,
			'id'        => $id
		] );
	}
	/**
	 * Updates an existing Tttihd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$index = \yii\helpers\Url::toRoute( [ 'tttihd/index' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		/** @var Tttihd $model */
		$model     = $this->findModelBase64( 'Tttihd', $id );
		$dsTMutasi = Tttihd::find()->tttihdFillByNo()->where( [ 'tttihd.TINo' => $model->TINo ] )
		                   ->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post            = \Yii::$app->request->post();
            $post[ 'TITgl' ]    = $post[ 'TITgl' ] . ' ' . $post[ 'TIJam' ];
			$post[ 'Action' ]   = 'Update';
			$result             = Tttihd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Tttihd::findOne( $result[ 'TINo' ] );
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'TINoView' ] = $result[ 'TINo' ];
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				return $this->redirect( [ 'tttihd/update', 'action' => 'display', 'id' => $id ] );
			} else {
				return $this->render( 'update', [
					'model'     => $model,
					'dsTMutasi' => $post,
					'id'        => $id,
				] );
			}
		}
		$dsTMutasi[ 'TIJam' ]  = $dsTMutasi[ 'TITgl' ];
		$dsTMutasi[ 'Action' ] = 'Load';
		$result                = Tttihd::find()->callSP( $dsTMutasi );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTMutasi[ 'TINoView' ] = $result[ 'TINo' ];
		$dsTMutasi[ 'TINoView' ] = $_GET[ 'TINoView' ] ?? $result[ 'TINo' ];
		return $this->render( 'update', [
			'model'     => $model,
			'dsTMutasi' => $dsTMutasi,
			'id'        => $id
		] );
	}
	/**
	 * Deletes an existing Tttihd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete() {
		/** @var Tttihd $model */
		$model             = $model = $this->findModelBase64( 'Tttihd', $_POST[ 'id' ] );
		$_POST[ 'TINo' ]   = $model->TINo;
		$_POST[ 'Action' ] = 'Delete';
		$result            = Tttihd::find()->callSP( $_POST );
		if ( $result[ 'status' ] == 0 ) {
			return $this->responseSuccess( $result[ 'keterangan' ] );
		} else {
			return $this->responseFailed( $result[ 'keterangan' ] );
		}
	}
	public function actionCancel( $id, $action ) {
		/** @var Tttihd $model */
		$model             = $this->findModelBase64( 'Tttihd', $id );
		$_POST[ 'TINo' ]   = $model->TINo;
		$_POST[ 'TIJam' ]  = $_POST[ 'TITgl' ] . ' ' . $_POST[ 'TIJam' ];
		$_POST[ 'Action' ] = 'Cancel';
		$result            = Tttihd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'TINo' ] == '--' ) {
			return $this->redirect( [ 'index' ] );
		} else {
			$model = Tttihd::findOne( $result[ 'TINo' ] );
			return $this->redirect( [ 'tttihd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST         = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client        = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response      = $client->post( Yii::$app->params[ 'host_report' ] . '/aunit/ftihd', [
			'body' => Json::encode( $_POST )
		] );
        $html                 = $response->getBody();
        return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	/**
	 * Finds the Tttihd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Tttihd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Tttihd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
