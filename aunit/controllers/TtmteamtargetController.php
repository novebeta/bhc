<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\TdAction;
use aunit\models\Ttmteamtarget;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtmteamtargetController implements the CRUD actions for Ttmteamtarget model.
 */
class TtmteamtargetController extends AunitController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actions() {
		$colGrid  = null;
		$joinWith = [
			'sales' => [ 'type' => 'INNER JOIN' ],
		];
		$join     = [];
		$where    = [];
//		if ( isset( $_POST['query'] ) ) {
//			$r = Json::decode( $_POST['query'], true )['r'];
//			switch ( $r ) {
//				case 'ttsd/mutasi-eksternal-dealer':
//					$colGrid  = Ttsd::colGridMutasiEksternalDealer();
//					$joinWith = [
//						'lokasi' => [
//							'type' => 'INNER JOIN'
//						],
//						'dealer' => [
//							'type' => 'INNER JOIN'
//						],
//					];
//					break;
//				case 'ttsd/invoice-eksternal':
//					$colGrid  = Ttsd::colGridInvoiceEksternal();
//					$joinWith = [
//						'lokasi' => [
//							'type' => 'INNER JOIN'
//						],
//						'dealer' => [
//							'type' => 'INNER JOIN'
//						],
//					];
//					break;
//			}
//		}
		return [
			'td' => [
				'class'    => TdAction::class,
				'model'    => Ttmteamtarget::class,
				'colGrid'  => $colGrid,
				'joinWith' => $joinWith
			],
		];
	}
	/**
	 * Lists all Ttmteamtarget models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index' );
	}
	public function actionList() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$query                       = new Query();
		$query->select( new Expression( "TeamKode as id, concat(TeamKode,' ~ ',REPLACE(TeamNama,'--','')) AS TeamNama, TeamPass" ) )
		      ->from( 'tdteam' )
		      ->where( "TeamStatus = 'A'" )
		      ->groupBy( 'TeamKode' )
		      ->orderBy( "TeamPass DESC, TeamStatus, TeamKode" );
		$rows = $query->all();
		/* encode row id */
		for ( $i = 0; $i < count( $rows ); $i ++ ) {
			$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
		}
		return [
			'rows'      => $rows,
			'rowsCount' => sizeof( $rows )
		];
	}
	/**
	 * Displays a single Ttmteamtarget model.
	 *
	 * @param string $TeamKode
	 * @param string $TargetTgl
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $TeamKode, $TargetTgl ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $TeamKode, $TargetTgl ),
		] );
	}
	/**
	 * Creates a new Ttmteamtarget model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Ttmteamtarget();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'TeamKode' => $model->TeamKode, 'TargetTgl' => $model->TargetTgl ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Ttmteamtarget model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $TeamKode
	 * @param string $TargetTgl
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $TeamKode, $TargetTgl ) {
		$model = $this->findModel( $TeamKode, $TargetTgl );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'TeamKode' => $model->TeamKode, 'TargetTgl' => $model->TargetTgl ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Ttmteamtarget model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $TeamKode
	 * @param string $TargetTgl
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $TeamKode, $TargetTgl ) {
		$this->findModel( $TeamKode, $TargetTgl )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Ttmteamtarget model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $TeamKode
	 * @param string $TargetTgl
	 *
	 * @return Ttmteamtarget the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $TeamKode, $TargetTgl ) {
		if ( ( $model = Ttmteamtarget::findOne( [ 'TeamKode' => $TeamKode, 'TargetTgl' => $TargetTgl ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
