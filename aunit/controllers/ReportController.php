<?php

namespace aunit\controllers;

use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\models\Tddealer;
use aunit\models\Ttbkhd;
use aunit\models\Ttbmhd;
use aunit\models\Ttdk;
use aunit\models\Ttfb;
use aunit\models\Ttgeneralledgerhd;
use aunit\models\Ttin;
use aunit\models\Ttkk;
use aunit\models\Ttkm;
use aunit\models\Ttpbhd;
use aunit\models\Ttpd;
use aunit\models\Ttrk;
use aunit\models\Ttsd;
use aunit\models\Ttsk;
use aunit\models\Ttsmhd;
use aunit\models\Ttss;
use aunit\models\Vmotorlokasi;
use aunit\models\VmotorlokasiQuery;
use GuzzleHttp\Client;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\RangeNotSatisfiableHttpException;

class ReportController extends AunitController
{
    const MIME = [
        'pdf' => 'application/pdf',
        'xls' => 'application/vnd.ms-excel',
        'doc' => 'application/msword',
        'rtf' => 'application/rtf',
    ];

    public function actionDaftarSuratJalanSupplier()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttss::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rBeliSuratJalanSupplier', '@aunit/views/report/Pembelian/DaftarSuratJalanSupplier');
    }

    protected function prosesLaporan($target, $view)
    {
        session_write_close();
        if (isset($_POST['tipe'])) {
            unset($_POST['_csrf-app']);
            $_POST = array_merge($_POST, Yii::$app->session->get('_company'));
            $_POST['db'] = base64_decode($_COOKIE['_outlet']);
            $_POST['UserID'] = Menu::getUserLokasi()['UserID'];
            $client = new Client([
                'headers' => ['Content-Type' => 'application/octet-stream']
            ]);
            $response = $client->post(Yii::$app->params['H1'][$_POST['db']]['host_report'] . '/' . $target, [
                'body' => Json::encode($_POST)
            ]);
            try {
                if ($_POST['filetype'] == 'xlsr') {
                    $_POST['filetype'] = 'xls';
                }
                return Yii::$app->response->sendContentAsFile(
                    $response->getBody(),
                    $_POST['tipe'] . '.' . $_POST['filetype'],
                    [
                        'inline' => true,
                        'mimeType' => $this::MIME[$_POST['filetype']]
                    ]
                );
            } catch (RangeNotSatisfiableHttpException $e) {
            }
        }
        unset($_POST);
        return $this->render($view);
    }

    public function actionDaftarFakturBeli()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttfb::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rBeliFakturBeli', '@aunit/views/report/Pembelian/DaftarFakturBeli');
    }

    public function actionDaftarInden()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttin::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rJualInden', '@aunit/views/report/Penjualan/DaftarInden');
    }

    public function actionDaftarDataKonsumen()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttdk::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rJualDataKonsumen', '@aunit/views/report/Penjualan/DaftarDataKonsumen');
    }

    public function actionDaftarSuratJalanKonsumen()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttsk::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rJualSuratJalanKonsumen', '@aunit/views/report/Penjualan/DaftarSuratJalanKonsumen');
    }

    public function actionDaftarReturKonsumen()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttrk::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rJualReturKonsumen', '@aunit/views/report/Penjualan/DaftarReturKonsumen');
    }

    public function actionCetakMasalBuktiDksk()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttsk::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rJualCetakMasalBuktiSKDK', '@aunit/views/report/Penjualan/CetakMasalBuktiDKSK');
    }

    public function actionDaftarSuratJalanMutasiDealer()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttsd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rMutasiSDealer', '@aunit/views/report/Mutasi/DaftarSuratJalanMutasiDealer');
    }

    public function actionDaftarSuratJalanMutasiPos()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttsmhd::find()->callSP($post, 'MutasiPOS');
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rMutasiSPOS', '@aunit/views/report/Mutasi/DaftarSuratJalanMutasiPos');
    }

    public function actionDaftarPenerimaanBarangDealer()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttpd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rMutasiPDealer', '@aunit/views/report/Mutasi/DaftarPenerimaanBarangDealer');
    }

    public function actionDaftarPenerimaanBarangPos()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttpbhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rMutasiPPos', '@aunit/views/report/Mutasi/DaftarPenerimaanBarangPos');
    }

    public function actionDaftarKasMasuk()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttkm::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rKeuanganKasMasuk', '@aunit/views/report/Keuangan/DaftarKasMasuk');
    }

    public function actionDaftarKasKeluar()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttkk::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rKeuanganKasKeluar', '@aunit/views/report/Keuangan/DaftarKasKeluar');
    }

    public function actionDaftarBankMasuk()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttbmhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rKeuanganBankMasuk', '@aunit/views/report/Keuangan/DaftarBankMasuk');
    }

    public function actionDaftarBankKeluar()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttbkhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rKeuanganBankKeluar', '@aunit/views/report/Keuangan/DaftarBankKeluar');
    }

    public function actionDaftarSaldoKasBank()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttbkhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
            $result = Ttbmhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
            $result = Ttkk::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
            $result = Ttkm::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rKeuanganSaldoKasBank', '@aunit/views/report/Keuangan/DaftarSaldoKasBank');
    }

    public function actionJurnalUmum()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttgeneralledgerhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rUGLJurnalUmum', '@aunit/views/report/Akuntansi/JurnalUmum.php');
    }

    public function actionNeracaPercobaan()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttgeneralledgerhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rUGLNeracaPercobaan', '@aunit/views/report/Akuntansi/NeracaPercobaan.php');
    }

    public function actionNeracaRugiLaba()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttgeneralledgerhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rUGLNeracaRugiLaba', '@aunit/views/report/Akuntansi/NeracaRugiLaba.php');
    }

    public function actionLaporanHarian()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttgeneralledgerhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rUGLHarian', '@aunit/views/report/Auditor/LaporanHarian');
    }

    public function actionLaporanPiutang()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $result = Ttgeneralledgerhd::find()->callSP_Piutang();
//			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        }
        return $this->prosesLaporan('aunit/rUGLPiutang', '@aunit/views/report/Auditor/LaporanPiutang');
    }

    public function actionLaporanHutang()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $result = Ttgeneralledgerhd::find()->callSP_Piutang();
//			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        }
        return $this->prosesLaporan('aunit/rUGLHutang', '@aunit/views/report/Auditor/LaporanHutang');
    }

    public function actionDashboardAkuntansi()
    {
        return $this->prosesLaporan('aunit/rGLDashboard', '@aunit/views/report/Auditor/DashboardAkuntansi');
    }

    public function actionDaftarStockMotorAcct()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttgeneralledgerhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rGLStockMotor', '@aunit/views/report/Motor/DaftarStockMotorAcct');
    }

    public function actionDaftarMotor()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $result = Vmotorlokasi::find()->callSP();
//			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        }
        return $this->prosesLaporan('aunit/rMotorDaftarMotor', '@aunit/views/report/Motor/DaftarMotor');
    }

    public function actionDaftarStnkBpkb()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $result = Vmotorlokasi::find()->callSP();
//			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        }
        return $this->prosesLaporan('aunit/rMotorSTNKBPKB', '@aunit/views/report/Motor/DaftarSTNKBPKB');
    }

    public function actionDaftarRiwayatMotor()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $result = Vmotorlokasi::find()->callSP();
//			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        }
        return $this->prosesLaporan('aunit/rMotorRiwayatMotor', '@aunit/views/report/Motor/DaftarRiwayatMotor');
    }

    public function actionDaftarStockMotorFisik()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $result = Vmotorlokasi::find()->callSP();
//			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        }
        return $this->prosesLaporan('aunit/rMotorStockMotorFisik', '@aunit/views/report/Motor/DaftarStockMotorFisik');
    }

    public function actionRekapHarian()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttgeneralledgerhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rMarketRekapHarian', '@aunit/views/report/Market/RekapHarian');
    }

    public function actionRekapBulanan()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttgeneralledgerhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rMarketRekapBulanan', '@aunit/views/report/Market/RekapBulanan');
    }

    public function actionRekapPivotTabel()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttgeneralledgerhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rMarketPivotTabel', '@aunit/views/report/Market/RekapPivotTabel');
    }

    public function actionDashboard1()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttgeneralledgerhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rMarketDashboard1', '@aunit/views/report/Market/Dashboard1');
    }

    public function actionDashboard2()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttgeneralledgerhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rMarketDashboard2', '@aunit/views/report/Market/Dashboard2');
    }

    public function actionDashboard3()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttgeneralledgerhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rMarketDashboard3', '@aunit/views/report/Market/Dashboard3');
    }

    public function actionLaporanAstra()
    {
        return $this->prosesLaporan('aunit/rAstra', '@aunit/views/report/Astra/LaporanAstra');
    }

    public function actionLaporanQuery()
    {
        return $this->render('@aunit/views/report/Astra/LaporanQuery');
    }

    public function actionLaporanData()
    {
        return $this->prosesLaporan('aunit/rdata', '@aunit/views/report/Data/LaporanData');
    }

    public function actionLaporanUserLog()
    {
        return $this->prosesLaporan('aunit/ruserlog', '@aunit/views/report/Data/LaporanUserLog');
    }

    public function actionAstraQuery()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $post = Yii::$app->request->post();
        $query = \Yii::$app->db->createCommand("CALL rAstra(?,?,?,?,?,?,?)")
            ->bindParam(1, $post['tgl_type'])
            ->bindParam(2, $post['tgl1'])
            ->bindParam(3, $post['tgl2'])
            ->bindParam(4, $post['astra'])
            ->bindParam(5, $post['jenis_jual'])
            ->bindParam(6, $post['sales'])
            ->bindParam(7, $post['pos'])
            ->queryAll();
        $columns = [];
        if (sizeof($query) > 0) {
            $columns_arr = array_keys($query[0]);
            foreach ($columns_arr as $item) {
                $columns[] = ['text' => $item, 'datafield' => $item];
            }
        }
        return [
            'columns' => $columns,
            'rows' => $query
        ];
    }

    public function actionQuery()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $post = Yii::$app->request->post();
        $query = \Yii::$app->db->createCommand(base64_decode($post['q']))->queryAll();
        $columns = [];
        if (sizeof($query) > 0) {
            $columns_arr = array_keys($query[0]);
            foreach ($columns_arr as $item) {
                $columns[] = ['text' => $item, 'datafield' => $item];
            }
        }
        return [
            'columns' => $columns,
            'rows' => $query
        ];
    }

    public function actionExportExcel()
    {

    }

    public function actionDaftarInvoiceServis()
    {
        return $this->prosesLaporan('aunit/rServisInvoice', '@aunit/views/report/Aksesoris/DaftarInvoiceServis');
    }

    public function actionDaftarInvoicePenjualan()
    {
        return $this->prosesLaporan('aunit/rPenjualanInvoice', '@aunit/views/report/Aksesoris/DaftarInvoicePenjualan');
    }

    public function actionDaftarPenerimaanBarang()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttgeneralledgerhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rPenerimaanBarang', '@aunit/views/report/Aksesoris/DaftarPenerimaanBarang');
    }

    public function actionDaftarInvoicePembelian()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttgeneralledgerhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rPembelianInvoice', '@aunit/views/report/Aksesoris/DaftarInvoicePembelian');
    }

    public function actionDaftarTransfer()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttgeneralledgerhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rTransfer', '@aunit/views/report/Aksesoris/DaftarTransfer');
    }

    public function actionDaftarPenyesuaian()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttgeneralledgerhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rPenyesuaian', '@aunit/views/report/Aksesoris/DaftarPenyesuaian');
    }

    public function actionDaftarReturPenjualan()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttgeneralledgerhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rPenjualanRetur', '@aunit/views/report/Aksesoris/DaftarReturPenjualan');
    }

    public function actionDaftarReturPembelian()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $post['UserID'] = 'System';
            $result = Ttgeneralledgerhd::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->prosesLaporan('aunit/rPembelianRetur', '@aunit/views/report/Aksesoris/DaftarReturPembelian');
    }


    public function actionDashboardAkunting()
    {
        return $this->prosesLaporan('', '@aunit/views/report/Auditor/DashboardAkunting');
    }

    public function actionDashboardNeraca()
    {
        return $this->prosesLaporan('', '@aunit/views/report/Auditor/DashboardNeraca');
    }

    public function actionDetails()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $requestData = \Yii::$app->request->post();
        $query = new Query();
        $primaryKeys = ['ttsk.SKNo', 'ttdk.DKNo'];
        $query->select(new Expression('CONCAT(' . implode(',"||",', $primaryKeys) . ') AS id,
                              tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorMemo, 
                              tmotor.DealerKode, tmotor.SSNo, tmotor.FBNo, tmotor.FBHarga, tmotor.SKNo, tmotor.SKNo, tmotor.PDNo, tmotor.SDNo, tmotor.BPKBNo, tmotor.BPKBTglJadi, 
                              tmotor.BPKBTglAmbil, tmotor.STNKNo, tmotor.STNKTglMasuk, tmotor.STNKTglBayar, tmotor.STNKTglJadi, tmotor.STNKTglAmbil, tmotor.STNKNama, 
                              tmotor.STNKAlamat, tmotor.PlatNo, tmotor.PlatTgl, tmotor.PlatTglAmbil, tmotor.FakturAHMNo, tmotor.FakturAHMTgl, tmotor.FakturAHMTglAmbil, 
                              tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, 
                              tdcustomer.CusKabupaten, tdcustomer.CusTelepon, tmotor.FakturAHMNilai, tmotor.RKNo, tmotor.RKHarga, tmotor.SKHarga, tmotor.SDHarga, 
                              tmotor.MMHarga, ttdk.*,ttsk.*,tdleasing.LeaseNama, tdsales.SalesNama, 0 AS Cek'))
            ->from('tmotor')
            ->innerJoin("ttdk", "tmotor.DKNo = ttdk.DKNo")
            ->innerJoin("ttsk", "tmotor.SKNo = ttsk.SKNo")
            ->innerJoin("tdcustomer", "ttdk.CusKode = tdcustomer.CusKode")
            ->innerJoin("tdleasing", "ttdk.LeaseKode = tdleasing.LeaseKode")
            ->innerJoin("tdsales", "ttdk.SalesKode = tdsales.SalesKode")
            ->andWhere(['between', $requestData['cmbDKSKTgl'], $requestData['dtpDKSKTgl1'], $requestData['dtpDKSKTgl2']])
//            ->andWhere( [ 'between', 'ttdk.DKTgl', '2021-01-30', '2021-02-01' ] )
            ->orderBy('ttdk.DKNo');
        $rowsCount = $query->count();
        $rows = $query->all();
        /* encode row id */
        for ($i = 0; $i < count($rows); $i++) {
            $rows[$i]['id'] = base64_encode($rows[$i]['id']);
        }
        $response = [
            'rows' => $rows,
            'rowsCount' => $rowsCount
        ];
        return $response;
    }

    public function actionPrintDksk($id)
    {
        unset($_POST['_csrf-app']);
        $_POST = array_merge($_POST, Yii::$app->session->get('_company'));
        $_POST['db'] = base64_decode($_COOKIE['_outlet']);
        $_POST['UserID'] = Menu::getUserLokasi()['UserID'];
        $client = new Client([
            'headers' => ['Content-Type' => 'application/octet-stream']
        ]);
        $response = $client->post(Yii::$app->params['H1'][$_POST['db']]['host_report'] . '/aunit/rJualCetakMassal', [
            'body' => Json::encode($_POST)
        ]);
        return Yii::$app->response->sendContentAsFile($response->getBody(), 'CetakMasalBuktiSKDK.pdf', [
                'inline' => true,
                'mimeType' => 'application/pdf'
            ]
        );
    }


    public function actionDashboardMarketing()
    {
        return $this->prosesLaporan('', '@aunit/views/report/Auditor/DashboardMarketing');

    }

    public function actionMarquery()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            if ($requestData == []) {
                $tgl1 = date('Y-m-01');
                $tgl2 = date('Y-m-d');
                $eom = json_encode($requestData['eom']);
            } else {
                $gettgl1 = json_encode($requestData['tgl1']);
                $tgl1 = str_replace('"', '', $gettgl1);
                $gettgl2 = json_encode($requestData['tgl2']);
                $tgl2 = str_replace('"', '', $gettgl2);
                $eom = json_encode($requestData['eom']);
            }

            //SALES BY SEGMEN
            $querysbs = "CALL rdMarketing('SalesBySegment', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ");";
            $rows = Yii::$app->db->createCommand($querysbs)->queryAll();
            foreach ($rows as $key => $val) {
                $LabelSalesBySegment[] = $val['label'];
            }
            if (!isset($LabelSalesBySegment)) {
                $label = [];
            } else {
                $label = $LabelSalesBySegment;
            }
            foreach ($rows as $key => $val) {
                $MTD[] = $val['MTD'];
            }
            if (!isset($MTD)) {
                $mtd = [];
            } else {
                $mtd = $MTD;
            }
            foreach ($rows as $key => $val) {
                $MTD1[] = $val['MTD-1'];
            }
            if (!isset($MTD1)) {
                $mtd1 = [];
            } else {
                $mtd1 = $MTD1;
            }

            //DAILY DISTRIBUTION TREND
            $queryddt = "CALL rdMarketing('DailyDistTrend', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ");";
            $rowsddt = Yii::$app->db->createCommand($queryddt)->queryAll();;
            foreach ($rowsddt as $key => $val) {
                $LabelDDT[] = $val['label'];
            }
            if (!isset($LabelDDT)) {
                $labelddt = [];
            } else {
                $labelddt = $LabelDDT;
            }
            foreach ($rowsddt as $key => $val) {
                $actualDdt[] = $val['Actual'];
            }
            if (!isset($actualDdt)) {
                $actualddt = [];
            } else {
                $actualddt = $actualDdt;
            }
            foreach ($rowsddt as $key => $val) {
                $targetDdt[] = $val['Target'];
            }
            if (!isset($targetDdt)) {
                $targetddt = [];
            } else {
                $targetddt = $targetDdt;
            }

            //DAILY RETAIL SALES TREND
            $querydrst = "CALL rdMarketing('DailyRetailSalesTrend', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ");";
            $rowdrst = Yii::$app->db->createCommand($querydrst)->queryAll();;
            foreach ($rowdrst as $key => $val) {
                $LabelDRST[] = $val['label'];
            }
            if (!isset($LabelDRST)) {
                $labeldrst = [];
            } else {
                $labeldrst = $LabelDRST;
            }
            foreach ($rowdrst as $key => $val) {
                $actualDrst[] = $val['Actual'];
            }
            if (!isset($actualDrst)) {
                $actualdrst = [];
            } else {
                $actualdrst = $actualDrst;
            }
            foreach ($rowdrst as $key => $val) {
                $targetDrst[] = $val['Target'];
            }
            if (!isset($targetDrst)) {
                $targetdrst = [];
            } else {
                $targetdrst = $targetDrst;
            }

            //SALES STRUCTURE SS1
            $queryss = "CALL rdMarketing('SalesStructure', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ");";
            $rowss = Yii::$app->db->createCommand($queryss)->queryAll();;
            foreach ($rowss as $key => $val) {
                $LabelSS[] = $val['label'];
            }
            if (!isset($LabelSS)) {
                $labelss = [];
            } else {
                $labelss = $LabelSS;
            }
            foreach ($rowss as $key => $val) {
                $cash[] = $val['cash'];
            }
            if (!isset($cash)) {
                $cashss = [];
            } else {
                $cashss = $cash;
            }
            foreach ($rowss as $key => $val) {
                $credit[] = $val['credit'];
            }
            if (!isset($credit)) {
                $creditss = [];
            } else {
                $creditss = $credit;
            }

            $astring1 = $cashss[0].','.$creditss[0];
            $astring2 = explode(',',$astring1);
            $astring3 = $cashss[1].','.$creditss[1];
            $astring4 = explode(',',$astring3);

            //SALES STRUCTURE SS2
            $queryss2 = "CALL rdMarketing('SalesStructureSum', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ");";
            $rowss2 = Yii::$app->db->createCommand($queryss2)->queryAll();;
            foreach ($rowss2 as $key => $val) {
                $LabelSS2[] = $val['label'];
            }
            if (!isset($LabelSS2)) {
                $labelss2 = [];
            } else {
                $labelss2 = $LabelSS2;
            }
            foreach ($rowss2 as $key => $val) {
                $cash2[] = $val['MTDNow'];
            }
            if (!isset($cash2)) {
                $cashss2 = [];
            } else {
                $cashss2 = $cash2;
            }
            foreach ($rowss2 as $key => $val) {
                $credit2[] = $val['MTDBefore'];
            }
            if (!isset($credit2)) {
                $creditss2 = [];
            } else {
                $creditss2 = $credit2;
            }
            $string1 = $cashss2[0].','.$cashss2[1];
            $string2 = explode(',',$string1);
            $string3 = $creditss2[0].','.$creditss2[1];
            $string4 = explode(',',$string3);

            //POST RANK
            $query = "CALL rdMarketing('PosRank', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ");";
            $posrank = Yii::$app->db->createCommand($query)->queryAll();

            //PEFORMANCE REPORT
            $querypeformance = "CALL rdMarketing('SalesVSTarget', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ");";
            $peformance = Yii::$app->db->createCommand($querypeformance)->queryAll();

            //AGING PIUTANG REPORT
            $queryagingpiutang = "CALL rdMarketing('AgingPiutang', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ");";
            $agingpiutang = Yii::$app->db->createCommand($queryagingpiutang)->queryAll();


            //Rank
            $queryrank = "CALL rdMarketing('Rank', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ");";
            $rank = Yii::$app->db->createCommand($queryrank)->queryAll();

            $queryrankgroup = "CALL rdMarketing('RankGroup', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ");";
            $rankgroup = Yii::$app->db->createCommand($queryrankgroup)->queryAll();
            foreach ($rankgroup as $key => $val){
                $kolom1 = $val['Kolom1'];
                $kolom2 = $val['Kolom2'];
            }

            $queryworkday = "CALL rdMarketing('WorkDay', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ");";
            $workday = Yii::$app->db->createCommand($queryworkday)->queryAll();
            foreach ($workday as $key => $val){
                $kolom = $val['Kolom'];
            }

            //POST  SALES, DISCOUNT AND PROFIT
            $querysdp = "CALL rdMarketing('SaDiscPro', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ");";
            $sdp = Yii::$app->db->createCommand($querysdp)->queryAll();

            //Piutang Leasing
            $querypl = "CALL rdMarketing('PiutangLeasing', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ");";
            $pl = Yii::$app->db->createCommand($querypl)->queryAll();

            return [
                'rows' => $rows, //SALES BY SEGMEN
                'rowsddt' => $rowsddt, //DAILY DISTRIBUTION TREND
                'rowdrst' => $rowdrst, //DAILY RETAIL SALES TREND
                'rowss' => $rowss, //SALES STRUCTURE
                'rowss2' => $rowss2, //SALES STRUCTURE SUM
                'label' => $label,
                'mtd' => $mtd,
                'mtd1' => $mtd1,
                'labelddt' => $labelddt,
                'actualddt' => $actualddt,
                'targetddt' => $targetddt,
                'labeldrst' => $labeldrst,
                'actualdrst' => $actualdrst,
                'targetdrst' => $targetdrst,
                'labelss' => $labelss,
                'cashss' => $cashss,
//                'cashss' => $astring2,
                'creditss' => $creditss,
//                'creditss' => $astring4,
                'posrank' => $posrank,
                'peformance' => $peformance,
                'rank' => $rank,
                'labelss2' => $labelss2,
//                'cashss2' => $cashss2,
                'cashss2' => $string2,
//                'creditss2' => $creditss2,
                'creditss2' => $string4,
                'agingpiutang' => $agingpiutang,
                'kolom1' => $kolom1,
                'kolom2' => $kolom2,
                'kolom' => $kolom,
                'sdp' => $sdp,
                'pl' => $pl
            ];

        }
    }

    public function actionMarkonsol1()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
//            $count = $requestData;
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];


            //DAILY DISTRIBUTION TREND
            $queryddt = "CALL rdMarketingConsol('DailyDistTrend', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";
            $rowsddt = Yii::$app->db->createCommand($queryddt)->queryAll();
            foreach ($rowsddt as $key => $val) {
                $LabelDDT[] = $val['label'];
            }
            if (!isset($LabelDDT)) {
                $labelddt = [];
            } else {
                $labelddt = $LabelDDT;
            }
            foreach ($rowsddt as $key => $val) {
                $actualDdt[] = $val['Actual'];
            }
            if (!isset($actualDdt)) {
                $actualddt = [];
            } else {
                $actualddt = $actualDdt;
            }
            foreach ($rowsddt as $key => $val) {
                $targetDdt[] = $val['Target'];
            }
            if (!isset($targetDdt)) {
                $targetddt = [];
            } else {
                $targetddt = $targetDdt;
            }

            return [
                'labelddt' => $labelddt,
                'actualddt' => $actualddt,
                'targetddt' => $targetddt,
            ];

        }
    }

    public function actionMarkonsol2()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
//            $count = $requestData;
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];


            //DAILY RETAIL SALES TREND
            $querydrst = "CALL rdMarketingConsol('DailyRetailSalesTrend', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";
            $rowdrst = Yii::$app->db->createCommand($querydrst)->queryAll();
            foreach ($rowdrst as $key => $val) {
                $LabelDRST[] = $val['label'];
            }
            if (!isset($LabelDRST)) {
                $labeldrst = [];
            } else {
                $labeldrst = $LabelDRST;
            }
            foreach ($rowdrst as $key => $val) {
                $actualDrst[] = $val['Actual'];
            }
            if (!isset($actualDrst)) {
                $actualdrst = [];
            } else {
                $actualdrst = $actualDrst;
            }
            foreach ($rowdrst as $key => $val) {
                $targetDrst[] = $val['Target'];
            }
            if (!isset($targetDrst)) {
                $targetdrst = [];
            } else {
                $targetdrst = $targetDrst;
            }

            return [
                'labeldrst' => $labeldrst,
                'actualdrst' => $actualdrst,
                'targetdrst' => $targetdrst,
            ];

        }
    }

    public function actionMarkonsol3()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
//            $count = $requestData;
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];


            //SALES BY SEGMEN
            $querysbs = "CALL rdMarketingConsol('SalesBySegment', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";
            $rows = Yii::$app->db->createCommand($querysbs)->queryAll();
            foreach ($rows as $key => $val) {
                $LabelSalesBySegment[] = $val['label'];
            }
            if (!isset($LabelSalesBySegment)) {
                $label = [];
            } else {
                $label = $LabelSalesBySegment;
            }
            foreach ($rows as $key => $val) {
                $MTD[] = $val['MTD'];
            }
            if (!isset($MTD)) {
                $mtd = [];
            } else {
                $mtd = $MTD;
            }
            foreach ($rows as $key => $val) {
                $MTD1[] = $val['MTD-1'];
            }
            if (!isset($MTD1)) {
                $mtd1 = [];
            } else {
                $mtd1 = $MTD1;
            }

            return [
                'rows' => $rows,
                'label' => $label,
                'mtd' => $mtd,
                'mtd1' => $mtd1,
            ];

        }
    }

    public function actionMarkonsol4()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
//            $count = $requestData;
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];


            //SALES STRUCTURE SS1
            $queryss = "CALL rdMarketingConsol('SalesStructure', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";
            $rowss = Yii::$app->db->createCommand($queryss)->queryAll();
            foreach ($rowss as $key => $val) {
                $LabelSS[] = $val['label'];
            }
            if (!isset($LabelSS)) {
                $labelss = [];
            } else {
                $labelss = $LabelSS;
            }
            foreach ($rowss as $key => $val) {
                $cash[] = $val['cash'];
            }
            if (!isset($cash)) {
                $cashss = [];
            } else {
                $cashss = $cash;
            }
            foreach ($rowss as $key => $val) {
                $credit[] = $val['credit'];
            }
            if (!isset($credit)) {
                $creditss = [];
            } else {
                $creditss = $credit;
            }

            $astring1 = $cashss[0].','.$creditss[1];
            $astring2 = explode(',',$astring1);
            $astring3 = $cashss[1].','.$creditss[0];
            $astring4 = explode(',',$astring3);



            //SALES STRUCTURE SS2
            $queryss2 = "CALL rdMarketingConsol('SalesStructureSum', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";
            $rowss2 = Yii::$app->db->createCommand($queryss2)->queryAll();
            foreach ($rowss2 as $key => $val) {
                $LabelSS2[] = $val['label'];
            }
            if (!isset($LabelSS2)) {
                $labelss2 = [];
            } else {
                $labelss2 = $LabelSS2;
            }
            foreach ($rowss2 as $key => $val) {
                $cash2[] = $val['MTDNow'];
            }
            if (!isset($cash2)) {
                $cashss2 = [];
            } else {
                $cashss2 = $cash2;
            }
            foreach ($rowss2 as $key => $val) {
                $credit2[] = $val['MTDBefore'];
            }
            if (!isset($credit2)) {
                $creditss2 = [];
            } else {
                $creditss2 = $credit2;
            }

            $string1 = $cashss2[0].','.$cashss2[1];
            $string2 = explode(',',$string1);
            $string3 = $creditss2[0].','.$creditss2[1];
            $string4 = explode(',',$string3);

            return [
                'rowss' => $rowss, //SALES STRUCTURE
                'rowss2' => $rowss2,
                'labelss' => $labelss,
                'cashss' => $cashss,
                'creditss' => $creditss,
                'labelss2' => $labelss2,
                'cashss2' => $string2,
                'creditss2' => $string4,
            ];

        }
    }

    public function actionMarkonsol5()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];
            //POST RANK
            $query = "CALL rdMarketingConsol('PosRank', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";
            $posrank = Yii::$app->db->createCommand($query)->queryAll();
            return [
                'posrank' => $posrank,
            ];

        }
    }

    public function actionMarkonsol6()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];

            //PEFORMANCE REPORT
            $querypeformance = "CALL rdMarketingConsol('SalesVSTarget', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";
            $peformance = Yii::$app->db->createCommand($querypeformance)->queryAll();
            return [
                'peformance' => $peformance
            ];

        }
    }

    public function actionMarkonsol7()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];

            $queryrankgroup = "CALL rdMarketingConsol('RankGroup', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";
            $rankgroup = Yii::$app->db->createCommand($queryrankgroup)->queryAll();
            foreach ($rankgroup as $key => $val){
                $kolom1 = $val['Kolom1'];
                $kolom2 = $val['Kolom2'];
            }

            $queryworkday = "CALL rdMarketingConsol('WorkDay', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";
            $workday = Yii::$app->db->createCommand($queryworkday)->queryAll();
            foreach ($workday as $key => $val){
                $kolom = $val['Kolom'];
            }
            //Rank
            $queryrank = "CALL rdMarketingConsol('Rank', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'" . $user ."'," . $level . ",'','','');";
            $rank = Yii::$app->db->createCommand($queryrank)->queryAll();
            return [
                'rank' => $rank,
                'kolom1' => $kolom1,
                'kolom2' => $kolom2,
                'kolom' => $kolom
            ];

        }
    }

    public function actionAkquery()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            if ($requestData == []) {
                $tgl1 = date('Y-m-01');
                $tgl2 = date('Y-m-d');
                $eom = json_encode($requestData['eom']);
            } else {
                $gettgl1 = json_encode($requestData['tgl1']);
                $tgl1 = str_replace('"', '', $gettgl1);
                $gettgl2 = json_encode($requestData['tgl2']);
                $tgl2 = str_replace('"', '', $gettgl2);
                $eom = json_encode($requestData['eom']);
            }

            $query = "CALL rdAkutansi('LabaRugi', '" . $tgl1 . "','" . $tgl2 . "','" . $eom . "');";
            $rowlr = Yii::$app->db->createCommand($query)->queryAll();;
            foreach ($rowlr as $key => $val) {
                $LabelLR[] = $val['label'];
            }
            if (!isset($LabelLR)) {
                $labellr = [];
            } else {
                $labellr = $LabelLR;
            }
            foreach ($rowlr as $key => $val) {
                $DataLR[] = $val['data'];
            }
            if (!isset($DataLR)) {
                $datalr = [];
            } else {
                $datalr = $DataLR;
            }

            $query = "CALL rdAkutansi('LabaPenjualan', '" . $tgl1 . "','" . "','" . $eom . "');";
            $rowlp = Yii::$app->db->createCommand($query)->queryAll();;
            foreach ($rowlp as $key => $val) {
                $LabelLP[] = $val['label'];
            }
            if (!isset($LabelLP)) {
                $labellp = [];
            } else {
                $labellp = $LabelLP;
            }
            foreach ($rowlp as $key => $val) {
                $DataLPA[] = $val['anggaran'];
            }
            if (!isset($DataLPA)) {
                $datalpa = [];
            } else {
                $datalpa = $DataLPA;
            }
            foreach ($rowlp as $key => $val) {
                $DataLPR[] = $val['realisasi'];
            }
            if (!isset($DataLPR)) {
                $datalpr = [];
            } else {
                $datalpr = $DataLPR;
            }

            $query = "CALL rdAkutansi('ProyeksiKas', '" . $tgl1 . "','" . "','" . $eom . "');";
            $rowpk = Yii::$app->db->createCommand($query)->queryAll();;
            foreach ($rowpk as $key => $val) {
                $LabelPK[] = $val['label'];
            }
            if (!isset($LabelPK)) {
                $labelpk = [];
            } else {
                $labelpk = $LabelPK;
            }
            foreach ($rowpk as $key => $val) {
                $DataPKP[] = $val['penjualan'];
            }
            if (!isset($DataPKP)) {
                $datapkp = [];
            } else {
                $datapkp = $DataPKP;
            }
            foreach ($rowpk as $key => $val) {
                $DataPKF[] = $val['fixcost'];
            }
            if (!isset($DataPKF)) {
                $datapkf = [];
            } else {
                $datapkf = $DataPKF;
            }
            foreach ($rowpk as $key => $val) {
                $DataPKV[] = $val['varcost'];
            }
            if (!isset($DataPKV)) {
                $datapkv = [];
            } else {
                $datapkv = $DataPKV;
            }

            $querygh = "CALL rdAkutansi('Harta', '" . $tgl1 . "','" . $tgl2 . "','" . $eom . "');";
            $rowgh = Yii::$app->db->createCommand($querygh)->queryAll();
            foreach ($rowgh as $key => $val) {
                $LabelGH[] = $val['label'];
            }
            if (!isset($LabelGH)) {
                $labelgh = [];
            } else {
                $labelgh = $LabelGH;
            }
            foreach ($rowgh as $key => $val) {
                $DataGH[] = $val['data'];
            }
            if (!isset($DataGH)) {
                $datagh = [];
            } else {
                $datagh = $DataGH;
            }

            $querykb = "CALL rdAkutansi('KasBank', '" . $tgl1 . "','" . $tgl2 . "','" . $eom . "');";
            $rowkb = Yii::$app->db->createCommand($querykb)->queryAll();
            foreach ($rowkb as $key => $val) {
                $LabelKB[] = $val['label'];
            }
            if (!isset($LabelKB)) {
                $labelkb = [];
            } else {
                $labelkb = $LabelKB;
            }
            foreach ($rowkb as $key => $val) {
                $DataKB[] = $val['data'];
            }
            if (!isset($DataKB)) {
                $datakb = [];
            } else {
                $datakb = $DataKB;
            }


            return [
                'labellr' => $labellr,
                'datalr' => $datalr,
                'labellp' => $labellp,
                'datalpa' => $datalpa,
                'datalpr' => $datalpr,
                'labelpk' => $labelpk,
                'datapkp' => $datapkp,
                'datapkf' => $datapkf,
                'datapkv' => $datapkv,
                'labelgh' => $labelgh,
                'datagh' => $datagh,
                'labelkb' => $labelkb,
                'datakb' => $datakb,

            ];

        }
    }

    public function actionLaporanDashboard()
    {
        return $this->prosesLaporan('', '@aunit/views/report/Auditor/LaporanDashboard');

    }

    public function actionDashboardShow()
    {
        $param = $_GET['param'];
        $query = "CALL rdMarketing($param);";
        $row1['data'] = Yii::$app->db->createCommand($query)->queryAll();
        $row2['param'] = $param = $_GET['param'];
        $row = array_merge($row1, $row2);
        return $this->render('@aunit/views/report/Auditor/DashboardShow', ['data' => $row]);

    }

    public function actionDashboardShowPenjualan()
    {
        $param = $_GET['param'];
        $param2 = str_replace('PenjualanKecamatan', 'PenjualanKecamatanShare', $param);
        $param3 = str_replace('PenjualanKecamatan', 'PenjualanKecamatanUnit', $param);
        $query = "CALL rdMarketing($param);";
        $query2 = "CALL rdMarketing($param2);";
        $query3 = "CALL rdMarketing($param3);";
        $row1['satu'] = Yii::$app->db->createCommand($query)->queryAll();
        $row2['dua'] = Yii::$app->db->createCommand($query)->queryAll();
        $row3['tiga'] = Yii::$app->db->createCommand($query)->queryAll();
        $kode['kode'] = ['0'];
        $row = array_merge($row1,$row2,$row3,$kode);
        return $this->render('@aunit/views/report/Auditor/DashboardShowPenjualan', ['data' => $row]);

    }

    public function actionDashboardShowSegmen()
    {
        $param = $_GET['param'];
        $param2 = str_replace('PenjualanSegmenBulanan', 'PenjualanSegmenBulananGraph', $param);
        $query = "CALL rdMarketing($param);";
        $query2 = "CALL rdMarketing($param2);";
        $row1['tabel'] = Yii::$app->db->createCommand($query)->queryAll();
        $row2['grafik'] = Yii::$app->db->createCommand($query2)->queryAll();
        $row = array_merge($row1, $row2);
        return $this->render('@aunit/views/report/Auditor/DashboardShowSegmen', ['data' => $row]);

    }

    public function actionDashboardShowRank()
    {
        $param = $_GET['param'];
        $query = "CALL rdMarketing($param);";
        $row = Yii::$app->db->createCommand($query)->queryAll();
        return $this->render('@aunit/views/report/Auditor/DashboardShowRank', ['data' => $row]);

    }

    public function actionDashboardShowPos()
    {
        $param = $_GET['param'];
        $param2 = str_replace('PenjualanSales', 'PenjualanHarian', $param);
        $query = "CALL rdMarketing($param);";
        $query2 = "CALL rdMarketing($param2);";

        $rowdt = Yii::$app->db->createCommand($query)->queryAll();
        $rowdt2 = Yii::$app->db->createCommand($query2)->queryAll();
        foreach ($rowdt as $key){
            $data[$key['Tahun']] = $key;
        }
        foreach ($data as $key => $val){
            if ($key == '2020'){
                foreach ($val as $value){
                $dt1[] =$value;
                }
            }
        }
//        $dt2020 = array_shift($dt1);

        $row1['2020'] = [];
        $row2['tabel'] = $rowdt;
        $row3['tabel2'] = $rowdt2;
        $row = array_merge($row1, $row2, $row3);

        return $this->render('@aunit/views/report/Auditor/DashboardShowPos', ['data' => $row]);

    }

    public function actionDashboardShowCashCredit()
    {
//        $param = $_GET['param'];
//        $param2 = str_replace('PenjualanKecamatanUnit', 'PenjualanKecamatanShare', $param);
//        $query = "CALL rdMarketing($param);";
//        $query2 = "CALL rdMarketing($param2);";
//        $row1['Unit'] = Yii::$app->db->createCommand($query)->queryAll();
//        $row2['Share'] = Yii::$app->db->createCommand($query2)->queryAll();
//        $row = array_merge($row1, $row2);
//
        $param = $_GET['param'];
        $query = "CALL rdMarketing($param);";
        $row = Yii::$app->db->createCommand($query)->queryAll();
        return $this->render('@aunit/views/report/Auditor/DashboardShowCashCredit', ['data' => $row]);

    }

    public function actionDashboardShowAging()
    {
        $param = $_GET['param'];
//        $param2 = str_replace('ProfitTahun', 'ProfitTarget', $param);
        $param2 = str_replace('PeoplePenjualan', 'AveragePenjualan', $param);
        $query = "CALL rdMarketing($param);";
        $query2 = "CALL rdMarketing($param2);";
        $row1['People'] = Yii::$app->db->createCommand($query)->queryAll();
        $row2['Average'] = Yii::$app->db->createCommand($query2)->queryAll();
        $row = array_merge($row1, $row2);
//        return $this->render('@aunit/views/report/Auditor/DashboardShowAging', ['data' => $row]);
        return $this->render('@aunit/views/report/Auditor/DashboardShowPeople', ['data' => $row]);

    }

    public function actionDashboardShowTargetSales()
    {
        $param = $_GET['param'];
        $query = "CALL rdMarketing($param);";
        $row = Yii::$app->db->createCommand($query)->queryAll();
        return $this->render('@aunit/views/report/Auditor/DashboardShowTargetSales', ['data' => $row]);

    }
    
    public function actionDashboardQuery()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            if ($requestData == []) {
                $tgl1 = date('Y-m-01');
                $tgl2 = date('Y-m-d');
                $gettype = json_encode($requestData['tipe']);
                $type = str_replace('"', '', $gettype);
                $geteom = json_encode($requestData['eom']);
                $eom = str_replace('"', '', $geteom);
            } else {
                $gettgl1 = json_encode($requestData['tgl1']);
                $tgl1 = str_replace('"', '', $gettgl1);
                $gettgl2 = json_encode($requestData['tgl2']);
                $tgl2 = str_replace('"', '', $gettgl2);
                $gettype = json_encode($requestData['tipe']);
                $type = str_replace('"', '', $gettype);
                $geteom = json_encode($requestData['eom']);
                $eom = str_replace('"', '', $geteom);
            }
        }

        $row = "CALL rdDashBoard('" . $type . "', '" . $tgl1 . "','" . $tgl2 . "','" . $eom . "');";
        $query = Yii::$app->db->createCommand($row)->queryAll();

        $columns = [];
        if (sizeof($query) > 0) {
            $columns_arr = array_keys($query[0]);
            foreach ($columns_arr as $item) {
                $columns[] = ['text' => $item, 'datafield' => $item];
            }
        }
        return [
            'columns' => $columns,
            'rows' => $query
        ];

    }

    public function actionDashboardCdb()
    {
        return $this->prosesLaporan('', '@aunit/views/report/Auditor/DashboardCdb');

    }

    public function actionCdbJenis()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $count = $requestData;
            if ($requestData == []) {
                $tgl1 = date('Y-m-01');
                $tgl2 = date('Y-m-d');
                $eom = json_encode($requestData['eom']);
                $korwil = json_encode($requestData['korwil']);
               $user = $_SESSION['_userProfile']['UserID'];
            }
            else {
                {
                    $gettgl1 = json_encode($requestData['tgl1']);
                    $tgl1 = str_replace('"', '', $gettgl1);
                    $gettgl2 = json_encode($requestData['tgl2']);
                    $tgl2 = str_replace('"', '', $gettgl2);
                    $eom = json_encode($requestData['eom']);
                    $korwil = json_encode($requestData['korwil']);
                    $user = $_SESSION['_userProfile']['UserID'];
                }
            }
            //JENIS PEKERJAAN
            $querypekerjaan = "CALL rdCDCConsol('JenisPekerjaan', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'".$user."',".$korwil.",'','','');";
            $pekerjaan = Yii::$app->db->createCommand($querypekerjaan)->queryAll();
            foreach ($pekerjaan as $key => $value){
                $lpekerjaan[] = $value['Label'];
                $ipekerjaan[] = $value['Persen'];
            }
            $labelpekerjaan = $lpekerjaan;
            $isipekerjaan = $ipekerjaan;


            return [
                'labelpekerjaan' => $labelpekerjaan, //label PEKERJAAN
                'isipekerjaan' => $isipekerjaan, //isi PEKERJAAN
            ];

        }
    }

    public function actionCdbSales()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $count = $requestData;
            if ($requestData == []) {
                $tgl1 = date('Y-m-01');
                $tgl2 = date('Y-m-d');
                $eom = json_encode($requestData['eom']);
                $korwil = json_encode($requestData['korwil']);
                $user = $_SESSION['_userProfile']['UserID'];
            }
            else {
                {
                    $gettgl1 = json_encode($requestData['tgl1']);
                    $tgl1 = str_replace('"', '', $gettgl1);
                    $gettgl2 = json_encode($requestData['tgl2']);
                    $tgl2 = str_replace('"', '', $gettgl2);
                    $eom = json_encode($requestData['eom']);
                    $korwil = json_encode($requestData['korwil']);
                    $user = $_SESSION['_userProfile']['UserID'];
                }
            }

            //SALES BY TYPE
            $querysales = "CALL rdCDCConsol('SalesByType', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'".$user."',".$korwil.",'','','');";
            $sales = Yii::$app->db->createCommand($querysales)->queryAll();
            foreach ($sales as $key => $value){
                $labelsales[] = $value['Label'];
                $isisales[] = $value['Jumlah'];
            }


            return [
                'labelsales' => $labelsales, //label sales
                'isisales' => $isisales, //isi sales
            ];
        }
    }

    public function actionCdbTenor()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $count = $requestData;
            if ($requestData == []) {
                $tgl1 = date('Y-m-01');
                $tgl2 = date('Y-m-d');
                $eom = json_encode($requestData['eom']);
                $korwil = json_encode($requestData['korwil']);
                $user = $_SESSION['_userProfile']['UserID'];
            }
            else {
                {
                    $gettgl1 = json_encode($requestData['tgl1']);
                    $tgl1 = str_replace('"', '', $gettgl1);
                    $gettgl2 = json_encode($requestData['tgl2']);
                    $tgl2 = str_replace('"', '', $gettgl2);
                    $eom = json_encode($requestData['eom']);
                    $korwil = json_encode($requestData['korwil']);
                    $user = $_SESSION['_userProfile']['UserID'];
                }
            }

            //tenor
            $querytenor = "CALL rdCDCConsol('Tenor', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'".$user."',".$korwil.",'','','');";
            $tenor = Yii::$app->db->createCommand($querytenor)->queryAll();
            foreach ($tenor as $key => $value){
                $labeltenor[] = $value['Label'];
                $isitenor[] = $value['Persen'];
            }

            return [
                'labeltenor' => $labeltenor, //label tenor
                'isitenor' => $isitenor, //isi tenor
            ];

        }
    }

    public function actionCdbUsia()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $count = $requestData;
            if ($requestData == []) {
                $tgl1 = date('Y-m-01');
                $tgl2 = date('Y-m-d');
                $eom = json_encode($requestData['eom']);
                $korwil = json_encode($requestData['korwil']);
                $user = $_SESSION['_userProfile']['UserID'];
            }
            else {
                {
                    $gettgl1 = json_encode($requestData['tgl1']);
                    $tgl1 = str_replace('"', '', $gettgl1);
                    $gettgl2 = json_encode($requestData['tgl2']);
                    $tgl2 = str_replace('"', '', $gettgl2);
                    $eom = json_encode($requestData['eom']);
                    $korwil = json_encode($requestData['korwil']);
                    $user = $_SESSION['_userProfile']['UserID'];
                }
            }

             //usia
            $queryusia = "CALL rdCDCConsol('RangeUsia', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'".$user."',".$korwil.",'','','');";
            $usia = Yii::$app->db->createCommand($queryusia)->queryAll();
            foreach ($usia as $key => $value){
                $labelusia[] = $value['Label'];
                $isiusia[] = $value['Persen'];
            }
            return [
                'labelusia' => $labelusia, //label tenor
                'isiusia' => $isiusia, //isi tenor
            ];

        }
    }

    public function actionCdbPendidikan()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $count = $requestData;
            if ($requestData == []) {
                $tgl1 = date('Y-m-01');
                $tgl2 = date('Y-m-d');
                $eom = json_encode($requestData['eom']);
                $korwil = json_encode($requestData['korwil']);
                $user = $_SESSION['_userProfile']['UserID'];
            }
            else {
                {
                    $gettgl1 = json_encode($requestData['tgl1']);
                    $tgl1 = str_replace('"', '', $gettgl1);
                    $gettgl2 = json_encode($requestData['tgl2']);
                    $tgl2 = str_replace('"', '', $gettgl2);
                    $eom = json_encode($requestData['eom']);
                    $korwil = json_encode($requestData['korwil']);
                    $user = $_SESSION['_userProfile']['UserID'];
                }
            }

            //pendidikan
            $querypendidikan = "CALL rdCDCConsol('Pendidikan', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'".$user."',".$korwil.",'','','');";
            $pendidikan = Yii::$app->db->createCommand($querypendidikan)->queryAll();
            foreach ($pendidikan as $key => $value){
                $labelpendidikan[] = $value['Label'];
                $isipendidikan[] = $value['Persen'];
            }


            return [
                'labelpendidikan' => $labelpendidikan, //label tenor
                'isipendidikan' => $isipendidikan, //isi tenor

            ];

        }
    }

    public function actionCdbPayment()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $count = $requestData;
            if ($requestData == []) {
                $tgl1 = date('Y-m-01');
                $tgl2 = date('Y-m-d');
                $eom = json_encode($requestData['eom']);
                $korwil = json_encode($requestData['korwil']);
                $user = $_SESSION['_userProfile']['UserID'];
            }
            else {
                {
                    $gettgl1 = json_encode($requestData['tgl1']);
                    $tgl1 = str_replace('"', '', $gettgl1);
                    $gettgl2 = json_encode($requestData['tgl2']);
                    $tgl2 = str_replace('"', '', $gettgl2);
                    $eom = json_encode($requestData['eom']);
                    $korwil = json_encode($requestData['korwil']);
                    $user = $_SESSION['_userProfile']['UserID'];
                }
            }

            //payment
            $querypayment = "CALL rdCDCConsol('DownPayment', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'".$user."',".$korwil.",'','','');";
            $payment = Yii::$app->db->createCommand($querypayment)->queryAll();
            foreach ($payment as $key => $value){
                $labelpayment[] = $value['Label'];
                $isipayment[] = $value['Persen'];
            }

            return [
                'labelpayment' => $labelpayment, //label tenor
                'isipayment' => $isipayment, //isi tenor
            ];

        }
    }

    public function actionCdbSes()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $count = $requestData;
            if ($requestData == []) {
                $tgl1 = date('Y-m-01');
                $tgl2 = date('Y-m-d');
                $eom = json_encode($requestData['eom']);
                $korwil = json_encode($requestData['korwil']);
                $user = $_SESSION['_userProfile']['UserID'];
            }
            else {
                {
                    $gettgl1 = json_encode($requestData['tgl1']);
                    $tgl1 = str_replace('"', '', $gettgl1);
                    $gettgl2 = json_encode($requestData['tgl2']);
                    $tgl2 = str_replace('"', '', $gettgl2);
                    $eom = json_encode($requestData['eom']);
                    $korwil = json_encode($requestData['korwil']);
                    $user = $_SESSION['_userProfile']['UserID'];
                }
            }

            //pengeluaran
            $querypengeluaran = "CALL rdCDCConsol('Pengeluaran', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'".$user."',".$korwil.",'','','');";
            $pengeluaran = Yii::$app->db->createCommand($querypengeluaran)->queryAll();
            foreach ($pengeluaran as $key => $value){
                $labelpengeluaran[] = $value['Label'];
                $isipengeluaran[] = $value['Persen'];
            }

            return [
                'labelpengeluaran' => $labelpengeluaran, //label tenor
                'isipengeluaran' => $isipengeluaran, //isi tenor
            ];

        }
    }

    public function actionCdbKredit()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $count = $requestData;
            if ($requestData == []) {
                $tgl1 = date('Y-m-01');
                $tgl2 = date('Y-m-d');
                $eom = json_encode($requestData['eom']);
                $korwil = json_encode($requestData['korwil']);
                $user = $_SESSION['_userProfile']['UserID'];
            }
            else {
                {
                    $gettgl1 = json_encode($requestData['tgl1']);
                    $tgl1 = str_replace('"', '', $gettgl1);
                    $gettgl2 = json_encode($requestData['tgl2']);
                    $tgl2 = str_replace('"', '', $gettgl2);
                    $eom = json_encode($requestData['eom']);
                    $korwil = json_encode($requestData['korwil']);
                    $user = $_SESSION['_userProfile']['UserID'];
                }
            }

            //kredit
            $querykredit = "CALL rdCDCConsol('CashKredit', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'".$user."',".$korwil.",'','','');";
            $kredit = Yii::$app->db->createCommand($querykredit)->queryAll();
            foreach ($kredit as $key => $value){
                $labelkredit[] = $value['Label'];
                $isikredit[] = $value['Persen'];
            }


            return [
                'labelkredit' => $labelkredit, //label tenor
                'isikredit' => $isikredit, //isi tenor
            ];

        }
    }

    public function actionCdb()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $count = $requestData;
            if ($requestData == []) {
                $tgl1 = date('Y-m-01');
                $tgl2 = date('Y-m-d');
                $eom = json_encode($requestData['eom']);
                $korwil = json_encode($requestData['korwil']);
                $user = $_SESSION['_userProfile']['UserID'];
            }
            else {
                {
                    $gettgl1 = json_encode($requestData['tgl1']);
                    $tgl1 = str_replace('"', '', $gettgl1);
                    $gettgl2 = json_encode($requestData['tgl2']);
                    $tgl2 = str_replace('"', '', $gettgl2);
                    $eom = json_encode($requestData['eom']);
                    $korwil = json_encode($requestData['korwil']);
                    $user = $_SESSION['_userProfile']['UserID'];
                }
            }

            //sex
            $querysex = "CALL rdCDCConsol('Sex', '" . $tgl1 . "','" . $tgl2 . "'," . $eom . ",'".$user."',".$korwil.",'','','');";
            $sex = Yii::$app->db->createCommand($querysex)->queryAll();
            foreach ($sex as $key => $value){
                if ($value['Label'] == 'Laki-Laki'){
                    $male = $value['Persen'];
                }
                if ($value['Label'] == 'Perempuan'){
                    $famale = $value['Persen'];
                }
                if (!isset($famale)) {
                    $famale = '0';
                }
            }

            return [
                'male' => $male,
                'famale' => $famale,
            ];

        }
    }

    public function actionDashboardMarkonsol()
    {
        $query = "SELECT  Korwil FROM hconsole.tddealer WHERE Korwil <> 'Foen' AND MD <> 'Anper'  GROUP BY Korwil;";
        $row = Yii::$app->db->createCommand($query)->queryAll();

        $querymd = "SELECT  MD FROM hconsole.tddealer WHERE Korwil <> 'Foen' AND Korwil <> 'Sisca'  GROUP BY MD;";
        $rowmd = Yii::$app->db->createCommand($querymd)->queryAll();

        $querydealer = "SELECT  Dealer FROM hconsole.tddealer WHERE Korwil <> 'Foen' AND MD <> 'Anper'  ;";
        $rowdealer = Yii::$app->db->createCommand($querydealer)->queryAll();

        return $this->render('@aunit/views/report/Auditor/DashboardMarkonsol', ['data' => $row, 'datamd' => $rowmd, 'datadealer' => $rowdealer]);
//        return $this->prosesLaporan('', '@aunit/views/report/Auditor/DashboardMarkonsol');

    }

    public function actionDashboardAkukonsol()
    {
        return $this->prosesLaporan('', '@aunit/views/report/Auditor/DashboardAkukonsol');

    }
    public function actionDashboardMarketingMonitoring()
    {
        return $this->prosesLaporan('', '@aunit/views/report/Auditor/DashboardMarketingMonitoring');

    }

    public function actionDashboardShowKonsol()
    {
        $param = $_GET['param'];
        $query = "CALL rdMarketingConsol($param);";
        $row1['data'] = Yii::$app->db->createCommand($query)->queryAll();
        $row2['param'] = $param = $_GET['param'];
        $row = array_merge($row1, $row2);
        return $this->render('@aunit/views/report/Auditor/DashboardShow', ['data' => $row]);

    }

    public function actionDashboardShowPenjualanKonsol()
    {
//        $param = $_GET['param'];
//        $query = "CALL rdMarketingConsol($param);";
//        $row = Yii::$app->db->createCommand($query)->queryAll();
//        return $this->render('@aunit/views/report/Auditor/DashboardShowPenjualan', ['data' => $row]);
        $param = $_GET['param'];
        $param2 = str_replace('PenjualanKecamatan', 'PenjualanKecamatanShare', $param);
        $param3 = str_replace('PenjualanKecamatan', 'PenjualanKecamatanUnit', $param);
        $query = "CALL rdMarketingConsol($param);";
        $query2 = "CALL rdMarketingConsol($param2);";
        $query3 = "CALL rdMarketingConsol($param3);";
        $row1['satu'] = Yii::$app->db->createCommand($query)->queryAll();
        $row2['dua'] = Yii::$app->db->createCommand($query2)->queryAll();
        $row3['tiga'] = Yii::$app->db->createCommand($query3)->queryAll();
        $kode['kode'] = ['1'];
            $row = array_merge($row1,$row2,$row3,$kode);
        return $this->render('@aunit/views/report/Auditor/DashboardShowPenjualan', ['data' => $row]);

    }

    public function actionDashboardShowSegmenKonsol()
    {
        $param = $_GET['param'];
        $param2 = str_replace('PenjualanSegmenBulanan', 'PenjualanSegmenBulananGraph', $param);
        $query = "CALL rdMarketingConsol($param);";
        $query2 = "CALL rdMarketingConsol($param2);";
        $row1['tabel'] = Yii::$app->db->createCommand($query)->queryAll();
        $row2['grafik'] = Yii::$app->db->createCommand($query2)->queryAll();
        $row = array_merge($row1, $row2);
        return $this->render('@aunit/views/report/Auditor/DashboardShowSegmen', ['data' => $row]);

    }

    public function actionDashboardShowRankKonsol()
    {
        $param = $_GET['param'];
        $query = "CALL rdMarketingConsol($param);";
        $row = Yii::$app->db->createCommand($query)->queryAll();
        return $this->render('@aunit/views/report/Auditor/DashboardShowRank', ['data' => $row]);

    }

    public function actionDashboardShowPosKonsol()
    {
//        $param = $_GET['param'];
//        $query = "CALL rdMarketingConsol($param);";
//        $row = Yii::$app->db->createCommand($query)->queryAll();
//        return $this->render('@aunit/views/report/Auditor/DashboardShowPos', ['data' => $row]);

        $param = $_GET['param'];
        $param2 = str_replace('PenjualanSales', 'PenjualanHarian', $param);
        $query = "CALL rdMarketingConsol($param);";
        $query2 = "CALL rdMarketingConsol($param2);";

        $rowdt = Yii::$app->db->createCommand($query)->queryAll();
        $rowdt2 = Yii::$app->db->createCommand($query2)->queryAll();
        foreach ($rowdt as $key){
            $data[$key['Tahun']] = $key;
        }
        foreach ($data as $key => $val){
            if ($key == '2020'){
                foreach ($val as $value){
                    $dt1[] =$value;
                }
            }
        }
//        $dt2020 = array_shift($dt1);

        $row1['2020'] = [];
        $row2['tabel'] = $rowdt;
        $row3['tabel2'] = $rowdt2;
        $row = array_merge($row1, $row2, $row3);

        return $this->render('@aunit/views/report/Auditor/DashboardShowPos', ['data' => $row]);

    }

    public function actionDashboardShowCashCreditKonsol()
    {
//        $param = $_GET['param'];
//        $param2 = str_replace('PenjualanKecamatanUnit', 'PenjualanKecamatanShare', $param);
//        $query = "CALL rdMarketingConsol($param);";
//        $query2 = "CALL rdMarketingConsol($param2);";
//        $row1['Unit'] = Yii::$app->db->createCommand($query)->queryAll();
//        $row2['Share'] = Yii::$app->db->createCommand($query2)->queryAll();
//        $row = array_merge($row1, $row2);

        $param = $_GET['param'];
        $query = "CALL rdMarketingConsol($param);";
        $row = Yii::$app->db->createCommand($query)->queryAll();


        return $this->render('@aunit/views/report/Auditor/DashboardShowCashCredit', ['data' => $row]);

    }

    public function actionDashboardShowAgingKonsol()
    {
        $param = $_GET['param'];
//        $param2 = str_replace('ProfitTahun', 'ProfitTarget', $param);
        $param2 = str_replace('PeoplePenjualan', 'AveragePenjualan', $param);
        $query = "CALL rdMarketingConsol($param);";
        $query2 = "CALL rdMarketingConsol($param2);";
        $row1['People'] = Yii::$app->db->createCommand($query)->queryAll();
        $row2['Average'] = Yii::$app->db->createCommand($query2)->queryAll();
        $row = array_merge($row1, $row2);
//        return $this->render('@aunit/views/report/Auditor/DashboardShowAging', ['data' => $row]);
        return $this->render('@aunit/views/report/Auditor/DashboardShowPeople', ['data' => $row]);

    }

    public function actionDashboardShowTargetSalesKonsol()
    {
        $param = $_GET['param'];
        $query = "CALL rdMarketingConsol($param);";
        $row = Yii::$app->db->createCommand($query)->queryAll();
        return $this->render('@aunit/views/report/Auditor/DashboardShowTargetSales', ['data' => $row]);

    }

    public function actionQuerycombo()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $query = "SELECT  Korwil FROM hconsole.tddealer WHERE Korwil <> 'Foen' AND MD <> 'Anper'  GROUP BY Korwil;";
        $row = Yii::$app->db->createCommand($query)->queryAll();

        $querymd = "SELECT  MD FROM hconsole.tddealer WHERE Korwil <> 'Foen' AND Korwil <> 'Sisca'  GROUP BY MD;";
        $rowmd = Yii::$app->db->createCommand($querymd)->queryAll();

        $querydealer = "SELECT  Dealer FROM hconsole.tddealer WHERE Korwil <> 'Foen' AND MD <> 'Anper'  ;";
        $rowdealer = Yii::$app->db->createCommand($querydealer)->queryAll();

        return [
            'row' => $row,
            'rowmd' => $rowmd,
            'rowdealer' => $rowdealer,
        ];

    }

    public function actionQuerycombokwl()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $requestData = \Yii::$app->request->post();
        $level = json_encode($requestData['level']);
        $user = Menu::getUserLokasi()['UserID'];

        if($level == '"Korwil"') {
            $query = "SELECT  Korwil as id, Korwil as text  FROM hconsole.tddealer WHERE  Korwil <> 'Foen' OR Korwil =  '$user' GROUP BY Korwil;";
            $row = Yii::$app->db->createCommand($query)->queryAll();
//            $querymd = "SELECT  MD as id, MD as text FROM hconsole.tddealer WHERE Korwil =  '$user' GROUP BY MD;";
//            $querymd1 = "SELECT  MD as id, MD as text FROM hconsole.tddealer WHERE Korwil <>  'Foen' AND Korwil IN ('$korwil') GROUP BY MD;";
//            $rowmd = Yii::$app->db->createCommand($querymd)->queryAll();
//            $querydealer = "SELECT  Dealer as id, Dealer as text FROM hconsole.tddealer WHERE Korwil = '$user' ;";
//            $rowdealer = Yii::$app->db->createCommand($querydealer)->queryAll();
        }else if ($level == '"Anper"'){
            $query = "SELECT  Korwil as id, Korwil as text FROM hconsole.tddealer WHERE Korwil =  'Sisca'  GROUP BY Korwil;";
            $row = Yii::$app->db->createCommand($query)->queryAll();
//            $querymd = "SELECT  MD as id, MD as text FROM hconsole.tddealer WHERE Korwil =  'Sisca'  GROUP BY MD;";
//            $rowmd = Yii::$app->db->createCommand($querymd)->queryAll();
//            $querydealer = "SELECT  Dealer as id, Dealer as text  FROM hconsole.tddealer WHERE Korwil = 'Sisca' ;";
//            $rowdealer = Yii::$app->db->createCommand($querydealer)->queryAll();
        }else if ($level == '"BHC"'){
            $query = "SELECT  Korwil as id, Korwil as text FROM hconsole.tddealer WHERE Korwil <> 'Foen'   GROUP BY Korwil;";
            $row = Yii::$app->db->createCommand($query)->queryAll();
//            $querymd = "SELECT  MD as id, MD as text FROM hconsole.tddealer WHERE Korwil <> 'Foen'   GROUP BY MD;";
//            $rowmd = Yii::$app->db->createCommand($querymd)->queryAll();
//            $querydealer = "SELECT  Dealer as id, Dealer as text FROM hconsole.tddealer  WHERE Korwil <> 'Foen';";
//            $rowdealer = Yii::$app->db->createCommand($querydealer)->queryAll();
        }else{
            $query = "SELECT  Korwil as id, Korwil as text FROM hconsole.tddealer WHERE Korwil <> 'Foen' AND MD <> 'Anper'  GROUP BY Korwil;";
            $row = Yii::$app->db->createCommand($query)->queryAll();
//            $querymd = "SELECT  MD as id, MD as text FROM hconsole.tddealer WHERE Korwil <> 'Foen' AND Korwil <> 'Sisca'  GROUP BY MD;";
//            $rowmd = Yii::$app->db->createCommand($querymd)->queryAll();
//            $querydealer = "SELECT  Dealer as id, Dealer as text FROM hconsole.tddealer WHERE Korwil <> 'Foen' AND MD <> 'Anper'  ;";
//            $rowdealer = Yii::$app->db->createCommand($querydealer)->queryAll();
        }

        return [
            'data' => $row,
//            'md' => $rowmd,
//            'dealer' => $rowdealer,
        ];

    }

    public function actionQuerycombomd()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $requestData = \Yii::$app->request->post();
        if ($requestData != []){
            $getkorwil = json_encode($requestData['korwil']);
            $korwilawal = str_replace('["','', $getkorwil);
            $korwil = str_replace('"]','', $korwilawal);
        }
        else{
            $korwil = '';
        }

        $user = Menu::getUserLokasi()['UserID'];

        $query1 = "SELECT  MD as id, MD as text FROM hconsole.tddealer WHERE Korwil <>  'Foen' AND Korwil IN ('$korwil') GROUP BY MD;";
        $query = str_replace('"',"'", $query1);
        $row = Yii::$app->db->createCommand($query)->queryAll();

        return [
            'datamd' => $row,
        ];

    }

    public function actionQuerycombodealer()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $requestData = \Yii::$app->request->post();
        if ($requestData != []){
            $getmd = json_encode($requestData['md']);
            $mdawal = str_replace('["','', $getmd);
            $md = str_replace('"]','', $mdawal);
        }
        else{
            $md = '';
        }
        $user = Menu::getUserLokasi()['UserID'];

        $query1 = "SELECT  Dealer as id, Dealer as text FROM hconsole.tddealer WHERE Korwil <>  'Foen' AND Korwil IN ('$md') GROUP BY MD;";
        $query = str_replace('"',"'", $query1);
        $row = Yii::$app->db->createCommand($query)->queryAll();

        return [
            'datadealer' => $row,
        ];

    }

    public function actionGetdatakwl()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $requestData = \Yii::$app->request->post();
        $id_lvl = $_POST['id_lvl'];
        $response   = Tddealer::find()->getkorwil($id_lvl);
//        $data = json_encode($response);
        return [
            'data' => $response
        ];
    }

    public function actionDashboardShowPlKonsol()
    {
        $param = $_GET['param'];
        $query = "CALL rdMarketingConsol($param);";
        $row = Yii::$app->db->createCommand($query)->queryAll();
        return $this->render('@aunit/views/report/Auditor/DashboardShowAgingPiutang', ['data' => $row]);

    }
    public function actionDashboardShowPl()
    {
        $param = $_GET['param'];
        $query = "CALL rdMarketing($param);";
        $row = Yii::$app->db->createCommand($query)->queryAll();
        return $this->render('@aunit/views/report/Auditor/DashboardShowAgingPiutang', ['data' => $row]);

    }

    public function actionShowJenisPekerjaan()
    {
        $param = $_GET['param'];
        $query = "CALL rdCDCConsol($param);";
        $pekerjaan = Yii::$app->db->createCommand($query)->queryAll();
        foreach ($pekerjaan as $key => $value){
            $lpekerjaan[] = $value['Label'];
            $ipekerjaan[] = $value['Persen'];
        }
        $labelpekerjaan['label'] = $lpekerjaan;
        $isipekerjaan['isi'] = $ipekerjaan;
        $row = array_merge($labelpekerjaan, $isipekerjaan);
        return $this->render('@aunit/views/report/Auditor/ShowJenisPekerjaan', ['data' => $row]);
    }


    public function actionShowSalesByType()
    {
        $param = $_GET['param'];
        $query = "CALL rdCDCConsol($param);";
        $pekerjaan = Yii::$app->db->createCommand($query)->queryAll();
        foreach ($pekerjaan as $key => $value){
            $lpekerjaan[] = $value['Label'];
            $ipekerjaan[] = $value['Jumlah'];
        }
        $labelpekerjaan['label'] = $lpekerjaan;
        $isipekerjaan['isi'] = $ipekerjaan;
        $row = array_merge($labelpekerjaan, $isipekerjaan);
        return $this->render('@aunit/views/report/Auditor/ShowSalesByType', ['data' => $row]);

    }

    public function actionAkukonsolprofit()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $bulan = str_replace('"', '',date('"F Y"', strtotime($tgl1)));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];
            $callquery = "CALL rdAkuntansiConsol('DashboardConsol','" . $tgl1 . "','" . $tgl2 . "','".$eom."','".$user."',".$level.",'','','');";
            $get_query = Yii::$app->db->createCommand($callquery)->queryAll();
            foreach ($get_query as $query) {
                $profitops[] = $query['ProfitOperasional'];
                $kota[] = $query['Kota'];
                $profitnpbt[] = $query['TotalProfitNPBT'];
                $profitperunit[] = $query['ProfitPerUnit'];
                $BiayaPPHPPN[] = $query['BiayaPPHPPN'];
                $ProfitOpEstimasi[] = $query['ProfitOpEstimasi'];
                $TotalInsOwner[] = $query['TotalInsOwner'];
                $TotalProfitNPBT[] = $query['TotalProfitNPBT'];
                $ProfitPerUnit[] = $query['ProfitPerUnit'];
                $GajiReal[] = $query['GajiReal'];
                $GajiBudget[] = $query['GajiBudget'];
                $InsentifReal[] = $query['InsentifReal'];
                $InsentifBudget[] = $query['InsentifBudget'];
                $PemasaranReal[] = $query['PemasaranReal'];
                $PemasaranBudget[] = $query['PemasaranBudget'];
            }
            $totalops = number_format(array_sum($profitops));
            $totalnpbt =  number_format(array_sum($profitnpbt));
            $totalperunit =  number_format(array_sum($profitperunit) / count($profitperunit));
            return [
                'profitops' => $profitops,
                'kota' => $kota,
                'profitnpbt' => $profitnpbt,
                'profitperunit' => $profitperunit,
                'bulan' => $bulan,
                'totalops' => $totalops,
                'totalnpbt' => $totalnpbt,
                'totalperunit' => $totalperunit,
                'BiayaPPHPPN' => $BiayaPPHPPN,
                'ProfitOpEstimasi' => $ProfitOpEstimasi,
                'TotalInsOwner' => $TotalInsOwner,
                'TotalProfitNPBT' => $TotalProfitNPBT,
                'ProfitPerUnit' => $ProfitPerUnit,
                'GajiReal' => $GajiReal,
                'GajiBudget' => $GajiBudget,
                'InsentifReal' => $InsentifReal,
                'InsentifBudget' => $InsentifBudget,
                'PemasaranReal' => $PemasaranReal,
                'PemasaranBudget' => $PemasaranBudget,
            ];
        }
    }

    public function actionQueryDashboardNeraca()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];
            $callquery = "CALL rdAkuntansiConsol('DashAcct','" . $tgl1 . "','" . $tgl2 . "','".$eom."','".$user."',".$level.",'','','');";
            $callquery2 = "CALL rdAkuntansiConsol('Titipan','" . $tgl1 . "','" . $tgl2 . "','".$eom."','".$user."',".$level.",'','','');";
            $get_query = Yii::$app->db->createCommand($callquery)->queryAll();
            $get_query2 = Yii::$app->db->createCommand($callquery2)->queryAll();
            foreach ($get_query as $query) {
                if( $query['Keterangan'] == 'Current Asset'){
                    $CurrentAsset[] = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Current Liabilities'){
                    $CurrentLiabilities[] = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Working Capital'){
                    $WorkingCapital[] = $query['NilaiJuta'];
                }else if($query['Judul'] == 'Laba Rugi'){
                    $LabaRugi[] = $query['NilaiJuta'];
                    $LabaRugiLabel[] = $query['Keterangan'];
                }else if($query['Keterangan'] == 'Aktiva'){
                    $TotalAssets = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Cash & Bank'){
                    $CashBank = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Piutang'){
                    $Piutang= $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Persediaan'){
                    $Persediaan = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Depositor, Adv & Prepay'){
                    $Depositor = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Aktiva Tetap & Lain2'){
                    $AktivaTetap = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Pasiva'){
                    $TotalLiabilities = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Hutang'){
                    $Hutang = $query['NilaiJuta'];
                } else if($query['Keterangan'] == 'Titipan'){
                    $Titipan = $query['NilaiJuta'];
                } else if($query['Keterangan'] == 'Modal'){
                    $Modal = $query['NilaiJuta'];
                } else if($query['Keterangan'] == 'Total Rugi Laba'){
                    $TotalRugiLaba = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Total Asset'){
                    $TotalAsset[] = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Total Liabilites'){
                    $TotalLiabilites[] = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Total Equity'){
                    $TotalEquity[] = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Gaji & Tunj'){
                    $Gaji[] = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Insentif'){
                    $Insentif[] = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'By Pemasaran'){
                    $Pemasaran[] = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'By Penyusutan Asuransi & Sewa'){
                    $Asuransi[] = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'By Penyusutan Asset'){
                    $Asset[] = $query['NilaiJuta'];
                }else if($query['Keterangan'] == 'Opex lainnya'){
                    $Opex[] = $query['NilaiJuta'];
                }

                else if($query['Keterangan'] == 'Piutang Total'){
                    $TotalPiutang = $query['NilaiJuta'];
                }
                else if($query['Keterangan'] == 'Piutang Overdue'){
                    $PiutangOverDue = $query['NilaiJuta'];
                }
                else if($query['Keterangan'] == 'Piutang Current'){
                    $PiutangCurrent = $query['NilaiJuta'];
                }
            }

            foreach ($get_query2 as $query) {
                    $Titipan_Lain_Lain[] = $query['Titipan_Lain_Lain'];
                    $Pendapatan_Dari_Titipan[] = $query['Pendapatan_Dari_Titipan'];
                    $Bulan_Titipan[] = $query['Bulan'];
            }

            return [
                'TotalPiutang' => str_replace(',', '.', number_format($TotalPiutang)),
                'PiutangOverDue' => str_replace(',', '.', number_format($PiutangOverDue)),
                'PiutangCurrent' => str_replace(',', '.', number_format($PiutangCurrent)),
                'TotalLiabilities' => str_replace(',', '.', number_format($TotalLiabilities)),
                'Hutang' => str_replace(',', '.', number_format($Hutang)),
                'Titipan_Lain_Lain' => $Titipan_Lain_Lain,
                'Pendapatan_Dari_Titipan' => $Pendapatan_Dari_Titipan,
                'Bulan_Titipan' => $Bulan_Titipan,
                'Hutang1' => $Hutang,
                'Titipan' => str_replace(',', '.', number_format($Titipan)),
                'Titipan1' => $Titipan,
                'Modal' => str_replace(',', '.', number_format($Modal)),
                'Modal1' => $Modal,
                'TotalRugiLaba' => str_replace(',', '.', number_format($TotalRugiLaba)),
                'TotalRugiLaba1' => $TotalRugiLaba,
                'CurrentAsset' => $CurrentAsset,
                'CurrentLiabilities' => $CurrentLiabilities,
                'WorkingCapital' => $WorkingCapital,
                'LabaRugiLabel' => $LabaRugiLabel,
                'LabaRugi' => $LabaRugi,
                'TotalAssets' => str_replace(',', '.', number_format($TotalAssets)),
                'CashBank1' => $CashBank,
                'CashBank' => str_replace(',', '.', number_format($CashBank)),
                'Piutang' => str_replace(',', '.', number_format($Piutang)),
                'Piutang1' => $Piutang,
                'Persediaan' => str_replace(',', '.', number_format($Persediaan)),
                'Persediaan1' => $Persediaan,
                'Depositor' =>str_replace(',', '.', number_format( $Depositor)),
                'Depositor1' =>$Depositor,
                'AktivaTetap' => str_replace(',', '.', number_format($AktivaTetap)),
                'AktivaTetap1' => $AktivaTetap,
                'TotalAsset' => $TotalAsset,
                'TotalLiabilites' => $TotalLiabilites,
                'TotalEquity' => $TotalEquity,
                'Insentif' => $Insentif,
                'Gaji' => $Gaji,
                'Pemasaran' => $Pemasaran,
                'Asuransi' => $Asuransi,
                'Asset' => $Asset,
                'Opex' => $Opex,
            ];
        }
    }

    public function actionKonsolidasiAkuntansi()
    {
        return $this->prosesLaporan('aunit/rdAkuntansiConsol', '@aunit/views/report/Akuntansi/KonsolidasiAkuntansi');
    }

    public function actionSpleasing()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $user = $_SESSION['_userProfile']['UserID'];
            $level = json_encode($requestData['params'][':level']);
            $eom = json_encode($requestData['params'][':eom']);
            $tgl1 = str_replace('/', '-',$requestData['params'][':tgl1']);
            $tgl2 = str_replace('/', '-',$requestData['params'][':tgl2']);
            $callquery = "CALL rdAkuntansiConsol('ListLeasing',".$tgl2.",".$tgl1.",".$eom.",'".$user."',".$level.",'','','');";
            $get_query = Yii::$app->db->createCommand($callquery)->queryAll();
            foreach ($get_query as $query) {
                $data['label'] = $query['LeaseKode'];
                $data['value'] = $query['LeaseKode'];
                $LeaseKode[] = $data;
            }
            return $this->responseDataJson($LeaseKode);
        }
    }

    public function actionQueryDashboardMarketing()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];
            $callquery = "CALL rdMarketingConsol('Dash Monitor','" . $tgl1 . "','" . $tgl2 . "','".$eom."','".$user."',".$level.",'','','');";
//            $callquery = "CALL rdMarketingConsol('Dash Monitor','2023-12-01','2023-12-31','1','Foen','Nasional','','','');";
            $get_query = Yii::$app->db->createCommand($callquery)->queryAll();


            foreach ($get_query as $query) {
                if( $query['Keterangan'] == 'Total Sales MN'){
                    $TotalSalesMN[] = str_replace(',', '.', number_format($query['Nilai']));
                }else if($query['Keterangan'] == 'Total Sales MB'){
                    $TotalSalesMB[] = str_replace(',', '.', number_format($query['Nilai']));
                }else if($query['Keterangan'] == 'Credit Share MN'){
                    $CreditShareMN[] = str_replace('.', ',', $query['Nilai']);
                }else if($query['Keterangan'] == 'Credit Share MB'){
                    $CreditShareMB[] = str_replace('.', ',', $query['Nilai']);
                }else if($query['Keterangan'] == 'NPBT MN'){
                    $NPBTMN[] = str_replace(',', '.', number_format($query['Nilai']));
                }else if($query['Keterangan'] == 'NPBT MB'){
                    $NPBTMB[] = str_replace(',', '.', number_format($query['Nilai']));
                }else if($query['Keterangan'] == 'Sales Growth'){
                    $SalesGrowth[] = str_replace('.', ',', $query['Nilai']);
                }else if($query['Keterangan'] == 'Tunai Growth'){
                    $TunaiGrowth[] = str_replace('.', ',', $query['Nilai']);
                }else if($query['Keterangan'] == 'Credit Growth'){
                    $CreditGrowth[] = str_replace('.', ',', $query['Nilai']);
                }else if($query['Keterangan'] == 'NPBT Growth'){
                    $NPBTGrowth[] = str_replace('.', ',', $query['Nilai']);
                }else if($query['Keterangan'] == 'Distribusi MN'){
                    $DistribusiMN[] = str_replace(',', '.', number_format($query['Nilai']));
                }else if($query['Keterangan'] == 'Distribusi MB'){
                    $DistribusiMB[] = str_replace(',', '.', number_format($query['Nilai']));
                }else if($query['Keterangan'] == 'Distribusi Growth'){
                    $DistribusiGrowth[] = str_replace('.', ',', $query['Nilai']);
                }
            }
            return [
                'TotalSalesMN'	=>	$TotalSalesMN,
                'TotalSalesMB'	=>	$TotalSalesMB,
                'CreditShareMN'	=>	$CreditShareMN,
                'CreditShareMB'	=>	$CreditShareMB,
                'NPBTMN'	=>	$NPBTMN,
                'NPBTMB'	=>	$NPBTMB,
                'SalesGrowth'	=>	$SalesGrowth,
                'TunaiGrowth'	=>	$TunaiGrowth,
                'CreditGrowth'	=>	$CreditGrowth,
                'NPBTGrowth'	=>	$NPBTGrowth,
                'DistribusiMN'	=>	$DistribusiMN,
                'DistribusiMB'	=>	$DistribusiMB,
                'DistribusiGrowth'	=>	$DistribusiGrowth
            ];
        }
    }

    public function actionQueryDashboardMarketingLease()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];
            $callquery = "CALL rdMarketingConsol('Dash Lease','" . $tgl1 . "','" . $tgl2 . "','".$eom."','".$user."',".$level.",'','','');";
//            $callquery = "CALL rdMarketingConsol('Dash Lease','2023-12-01','2023-12-31','1','Foen','Nasional','','','');";
            $dashlease = Yii::$app->db->createCommand($callquery)->queryAll();


            $ketunit = ['Unit'];
            $ketms = ['MS'];
            $ketmscp = ['MSCP'];
            foreach ($dashlease as $lease){
                $leaseket[] =  $lease['Keterangan'];
                $leaseunit[] =  str_replace(',', '.', number_format($lease['Unit']));
//                $leasems[] =  str_replace('.', ',',$lease['MS']);
                $leasems[] =  str_replace('.', ',',$lease['MS'].'%');
                $leasemscp[] =  str_replace('.', ',',$lease['MSCP'].'%');
            }




            return [
                'leaseunit'	=>	$leaseunit,
                'leasems'	=>	$leasems,
                'leasemscp'	=>	$leasemscp,
                'dashlease'	=>	$dashlease,
            ];
        }
    }

    public function actionQueryDashboardMarketingSales()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];
            $callquery = "CALL rdMarketingConsol('Dash DO Week','" . $tgl1 . "','" . $tgl2 . "','".$eom."','".$user."',".$level.",'','','');";
//            $callquery = "CALL rdMarketingConsol('Dash DO Week','2023-12-01','2023-12-31','1','Foen','Nasional','','','');";
            $get_query = Yii::$app->db->createCommand($callquery)->queryAll();
            foreach ($get_query as $query) {
                $Periode[] = $query['Periode'];
                $SalesMN[] = $query['SalesMN'];
                $SalesMB[] = $query['SalesMB'];
                $DOMN[] = $query['DOMN'];
            }
            return [
                'Periode'	=>	$Periode,
                'SalesMN'	=>	$SalesMN,
                'SalesMB'	=>	$SalesMB,
                'DOMN'	=>	$DOMN,
            ];
        }
    }

    public function actionQueryDashboardMarketingSalesDo()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];
            $callquery = "CALL rdMarketingConsol('Dash DO Day','" . $tgl1 . "','" . $tgl2 . "','".$eom."','".$user."',".$level.",'','','');";
//            $callquery = "CALL rdMarketingConsol('Dash DO Day','2023-12-01','2023-12-31','1','Foen','Nasional','','','');";
            $get_query = Yii::$app->db->createCommand($callquery)->queryAll();
            foreach ($get_query as $query) {
                $Periode[] = $query['Periode'];
                $SalesMN[] = $query['SalesMN'];
                $SalesMB[] = $query['SalesMB'];
                $DOMN[] = $query['DOMN'];
            }
            return [
                'Periode'	=>	$Periode,
                'SalesMN'	=>	$SalesMN,
                'SalesMB'	=>	$SalesMB,
                'DOMN'	=>	$DOMN,
            ];
        }
    }

    public function actionKomponenLrMonthly()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];
            $callquery = "CALL rdAkuntansiConsol('KomponenLRMonthly','" . $tgl1 . "','" . $tgl2 . "','".$eom."','".$user."',".$level.",'','','');";
//            $callquery = "CALL rdAkuntansiConsol('KomponenLRMonthly','2023-12-01','2023-12-31','1','Foen','Nasional','','','');";
            $get_query = Yii::$app->db->createCommand($callquery)->queryAll();
            foreach ($get_query as $query) {
                $Bulan[] = $query['Bulan'];
                $Potongan[] = $query['Potongan'];
                $LabaKotor[] = $query['LabaKotor'];
                $Opex[] = $query['Opex'];
                $PendapatanNonOP[] = $query['PendapatanNonOP'];
                $MO[] = $query['MO'];
                $ProfitNPBT[] = $query['ProfitNPBT'];
                $Pembagi[] = $query['Pembagi'];
            }
            return [
                'Bulan'	=>	$Bulan,
                'Potongan'	=>	$Potongan,
                'LabaKotor'	=>	$LabaKotor,
                'PendapatanNonOP'	=>	$PendapatanNonOP,
                'MO'	=>	$MO,
                'Opex'	=>	$Opex,
                'ProfitNPBT'	=>	$ProfitNPBT,
                'Pembagi'	=>	$Pembagi,
            ];
        }
    }

    public function actionKomponenLr()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];
            $callquery = "CALL rdAkuntansiConsol('KomponenLR','" . $tgl1 . "','" . $tgl2 . "','".$eom."','".$user."',".$level.",'','','');";
//            $callquery = "CALL rdAkuntansiConsol('KomponenLR','2023-12-01','2023-12-31','1','Foen','Nasional','','','');";
            $get_query = Yii::$app->db->createCommand($callquery)->queryAll();
            foreach ($get_query as $query) {
                $Kota[] = $query['Kota'];
                $Potongan[] = $query['Potongan'];
                $LabaKotor[] = $query['LabaKotor'];
                $Opex[] = $query['Opex'];
                $PendapatanNonOP[] = $query['PendapatanNonOP'];
                $MO[] = $query['MO'];
                $ProfitNPBT[] = $query['ProfitNPBT'];
                $Pembagi[] = $query['Pembagi'];
            }
            return [
                'Kota'	=>	$Kota,
                'Potongan'	=>	$Potongan,
                'LabaKotor'	=>	$LabaKotor,
                'PendapatanNonOP'	=>	$PendapatanNonOP,
                'MO'	=>	$MO,
                'Opex'	=>	$Opex,
                'ProfitNPBT'	=>	$ProfitNPBT,
                'Pembagi'	=>	$Pembagi,
            ];
        }
    }

    public function actionQueryDashboardAkuntansi()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];
            $callquery = "CALL rdAkuntansiConsol('Dash Lease','" . $tgl1 . "','" . $tgl2 . "','".$eom."','".$user."',".$level.",'','','');";
            $get_query = Yii::$app->db->createCommand($callquery)->queryAll();
            return [

            ];
        }
    }

    public function actionDashboardRasioKonsolidasi()
    {
        return $this->prosesLaporan('', '@aunit/views/report/Auditor/DashboardRasioKonsolidasi');
    }

    public function actionQueryDashboardRasioKonsolidasi()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $tgl1 = str_replace('"', '', json_encode($requestData['tgl1']));
            $tgl2 = str_replace('"', '', json_encode($requestData['tgl2']));
            $eom = json_encode($requestData['eom']);
            $level = json_encode($requestData['level']);
            $user = $_SESSION['_userProfile']['UserID'];
            $Cr1Kota = [];
            $Cr1Kasbank = [];
            $Cr1CashRasio = [];
            $Cr1CurrentLiabilities = [];
            $Cr2Kota = [];
            $Cr2Kasbank = [];
            $Cr2CashRasio = [];
            $Cr2CurrentLiabilities = [];
            $QrKota = [];
            $QrKasbank = [];
            $QrCashRasio = [];
            $QrCurrentLiabilities = [];
            $WcKota = [];
            $WcKasbank = [];
            $WcCashRasio = [];
            $ArKota = [];
            $ArCashRasio = [];
            $ArDayKota = [];
            $ArDayCashRasio = [];
            $InventoryKota = [];
            $InventoryCashRasio = [];
            $InventoryDayKota = [];
            $InventoryDayCashRasio = [];
            $GrossKota = [];
            $GrossCashRasio = [];
            $OprtKota = [];
            $OprtCashRasio = [];
            $NetKota = [];
            $NetCashRasio = [];
            $NPBTKota = [];
            $NPBTCashRasio  = [];
            $callquery = "CALL rdAkuntansiConsol('RasioDealer','" . $tgl1 . "','" . $tgl2 . "','".$eom."','".$user."',".$level.",'','','');";
//            $callquery = "CALL rdAkuntansiConsol('RasioDealer','2023-12-01','2023-12-31','1','Billy','Korwil','','','');";
            $get_query = Yii::$app->db->createCommand($callquery)->queryAll();
            foreach ($get_query as $query) {
                if( $query['Keterangan'] == 'Cash Ratio 1'){
                    $Cr1Kota[] = $query['Kota'];
                    $Cr1Kasbank[] = $query['KasBank'];
                    $Cr1CashRasio[] = $query['Nilai'];
                    $Cr1CurrentLiabilities[] = $query['Hutang'];
                }else if($query['Keterangan'] == 'Cash Ratio 2'){
                    $Cr2Kota[] = $query['Kota'];
                    $Cr2Kasbank[] = $query['KasBank'];
                    $Cr2CashRasio[] = $query['Nilai'];
                    $Cr2CurrentLiabilities[] = $query['Hutang'];
                }else if($query['Keterangan'] == 'Quick Ratio'){
                    $QrKota[] = $query['Kota'];
                    $QrKasbank[] = $query['KasBank'];
                    $QrCashRasio[] = $query['Nilai'];
                    $QrCurrentLiabilities[] = $query['Hutang'];
                } else if($query['Keterangan'] == 'WC Ratio'){
                    $WcKota[] = $query['Kota'];
                    $WcKasbank[] = $query['KasBank'];
                    $WcCashRasio[] = $query['Nilai'];
                }else if($query['Keterangan'] == 'AR Turnover'){
                    $ArKota[] = $query['Kota'];
                    $ArCashRasio[] = $query['Nilai'];
                }else if($query['Keterangan'] == 'AR Days'){
                    $ArDayKota[] = $query['Kota'];
                    $ArDayCashRasio[] = $query['Nilai'];
                }else if($query['Keterangan'] == 'Inventory Turnover'){
                    $InventoryKota[] = $query['Kota'];
                    $InventoryCashRasio[] = $query['Nilai'];
                }else if($query['Keterangan'] == 'Inventory Days'){
                    $InventoryDayKota[] = $query['Kota'];
                    $InventoryDayCashRasio[] = $query['Nilai'];
                }else if($query['Keterangan'] == 'Gross Profit Ratio'){
                    $GrossKota[] = $query['Kota'];
                    $GrossCashRasio[] = $query['Nilai'];
                }else if($query['Keterangan'] == 'Oprt Profit Ratio'){
                    $OprtKota[] = $query['Kota'];
                    $OprtCashRasio[] = $query['Nilai'];
                }else if($query['Keterangan'] == 'Net Profit Ratio'){
                    $NetKota[] = $query['Kota'];
                    $NetCashRasio[] = $query['Nilai'];
                }else if($query['Keterangan'] == 'NPBT Ratio'){
                    $NPBTKota[] = $query['Kota'];
                    $NPBTCashRasio[] = $query['Nilai'];
                }
            }

            return [
                'Cr1Kota' => $Cr1Kota,
                'Cr1Kasbank' => $Cr1Kasbank,
                'Cr1CashRasio' => $Cr1CashRasio,
                'Cr1CurrentLiabilities' => $Cr1CurrentLiabilities,
                'Cr2Kota' => $Cr2Kota,
                'Cr2Kasbank' => $Cr2Kasbank,
                'Cr2CashRasio' => $Cr2CashRasio,
                'Cr2CurrentLiabilities' => $Cr2CurrentLiabilities,
                'QrKota' => $QrKota,
                'QrKasbank' => $QrKasbank,
                'QrCashRasio' => $QrCashRasio,
                'QrCurrentLiabilities' => $QrCurrentLiabilities,
                'WcKota' => $WcKota,
                'WcKasbank' => $WcKasbank,
                'WcCashRasio' => $WcCashRasio,
                'ArKota' => $ArKota,
                'ArCashRasio' => $ArCashRasio,
                'ArDayKota' => $ArDayKota,
                'ArDayCashRasio' => $ArDayCashRasio,
                'InventoryKota' => $InventoryKota,
                'InventoryCashRasio' => $InventoryCashRasio,
                'InventoryDayKota' => $InventoryDayKota,
                'InventoryDayCashRasio' => $InventoryDayCashRasio,
                'GrossKota' => $GrossKota,
                'GrossCashRasio' => $GrossCashRasio,
                'OprtKota' => $OprtKota,
                'OprtCashRasio' => $OprtCashRasio,
                'NetKota' => $NetKota,
                'NetCashRasio' => $NetCashRasio,
                'NPBTKota' => $NPBTKota,
                'NPBTCashRasio' => $NPBTCashRasio
            ];
        }
    }

    public function actionMarginSalesForce()
    {
        return $this->prosesLaporan('aunit/rdAkuntansiConsol', '@aunit/views/report/Akuntansi/MarginSalesForce');
    }


    public function actionSpteam()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $user = $_SESSION['_userProfile']['UserID'];
            $level = json_encode($requestData['params'][':level']);
            $eom = json_encode($requestData['params'][':eom']);
            $tgl1 = str_replace('/', '-',$requestData['params'][':tgl1']);
            $tgl2 = str_replace('/', '-',$requestData['params'][':tgl2']);
            $callquery = "CALL rdAkuntansiConsol('ListTeam',".$tgl2.",".$tgl1.",".$eom.",'".$user."',".$level.",'','','');";
            $get_query = Yii::$app->db->createCommand($callquery)->queryAll();
            foreach ($get_query as $query) {
                $data['label'] = $query['TeamKode'];
                $data['value'] = $query['TeamKode'];
                $TeamKode[] = $data;
            }
            return $this->responseDataJson($TeamKode);
        }
    }

    public function actionSpsales()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $requestData = \Yii::$app->request->post();
            $user = $_SESSION['_userProfile']['UserID'];
            $level = json_encode($requestData['params'][':level']);
            $eom = json_encode($requestData['params'][':eom']);
            $tgl1 = str_replace('/', '-',$requestData['params'][':tgl1']);
            $tgl2 = str_replace('/', '-',$requestData['params'][':tgl2']);
            $callquery = "CALL rdAkuntansiConsol('ListSales',".$tgl2.",".$tgl1.",".$eom.",'".$user."',".$level.",'','','');";
            $get_query = Yii::$app->db->createCommand($callquery)->queryAll();
            foreach ($get_query as $query) {
                $data['label'] = $query['SalesKode'];
                $data['value'] = $query['SalesKode'];
                $SalesKode[] = $data;
            }
            return $this->responseDataJson($SalesKode);
        }
    }

    public function actionAkuntansiDetailPotongan()
    {
        $param = $_GET['param'];
        $query = "CALL rdAkuntansiConsol($param);";
        $row['data'] = Yii::$app->db->createCommand($query)->queryAll();
        return $this->render('@aunit/views/report/Auditor/AkuntansiDetailPotongan', ['data' => $row]);
    }

    public function actionAkuntansiDetailMo()
    {
        $param = $_GET['param'];
        $query = "CALL rdAkuntansiConsol($param);";
        $row = Yii::$app->db->createCommand($query)->queryAll();
        return $this->render('@aunit/views/report/Auditor/AkuntansiDetailMo', ['data' => $row]);
    }

    public function actionAkuntansiDetailPendapatan()
    {
        $param = $_GET['param'];
        $query = "CALL rdAkuntansiConsol($param);";
        $row['data'] = Yii::$app->db->createCommand($query)->queryAll();
        return $this->render('@aunit/views/report/Auditor/AkuntansiDetailPendapatan', ['data' => $row]);
    }

}








