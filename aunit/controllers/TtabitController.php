<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\models\Ttabhd;
use aunit\models\Ttabit;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtabitController implements the CRUD actions for Ttabit model.
 */
class TtabitController extends AunitController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttabit::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper  = $requestData[ 'oper' ];
			/** @var Ttabit $model */
			$model = null;
			if ( isset( $requestData[ 'id' ] ) ) {
				$model = $this->findModelBase64( 'ttabit', $requestData[ 'id' ] );
			}
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'Action' ] = 'Insert';
					} else {
						$requestData[ 'Action' ] = 'Update';
					}
					$result = Ttabit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'del'  :
					$requestData[ 'Action' ] = 'Delete';
					if ( $model != null ) {
						$requestData[ 'ABNo' ] = $model->ABNo;
						$requestData[ 'DKNo' ] = $model->DKNo;
					}
					$result = Ttabit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'batch' :
					\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
					$respon                      = $this->responseFailed( 'Add Item' );
					$header                      = $requestData[ 'header' ];
					$header[ 'Action' ]          = 'Loop';
					$result                      = Ttabhd::find()->callSP( $header );
					foreach ( $requestData[ 'data' ] as $item ) {
						$items[ 'Action' ] = 'Insert';
						$items[ 'ABNo' ]   = $header[ 'ABNo' ];
						$items[ 'DKNo' ]   = $item['id'];
						$result            = Ttabit::find()->callSP( $items );
					}
					return $this->responseSuccess( $requestData );
					break;
			}
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'ttabit.' . $primaryKeys[ $i ];
			}
			$query = new Query();
			$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
			ttabit.ABNo, ttabit.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, tmotor.MotorNoMesin, tdmotortype.MotorNama, 
			tdcustomer.CusKelurahan, ttdk.BBN, tdcustomer.CusKabupaten, tmotor.MotorTahun,tmotor.SKNo' ) )
			      ->from( "ttabit" )
			      ->innerJoin( "ttdk", "ttabit.DKNo = ttdk.DKNo" )
			      ->innerJoin( "tdcustomer", "ttdk.CusKode = tdcustomer.CusKode" )
			      ->innerJoin( "tmotor", "ttdk.DKNo = tmotor.DKNo" )
			      ->innerJoin( "tdmotortype", "tmotor.MotorType = tdmotortype.MotorType" )
			      ->where( "(ttabit.ABNo = :ABNo)", [ ':ABNo' => $requestData[ 'ABNo' ] ] );
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	protected function findModel( $ABNo, $DKNo ) {
		if ( ( $model = Ttabit::findOne( [ 'ABNo' => $ABNo, 'DKNo' => $DKNo ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
