<?php
namespace aunit\controllers;
use aunit\components\TdAction;
use aunit\models\Ttamhd;
use aunit\models\Ttamit;
use common\components\General;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtamhdController implements the CRUD actions for Ttamhd model.
 */
class TtamhdController extends \aunit\components\AunitController {
	public function actions() {
		return [
			'td' => [
				'class' => TdAction::class,
				'model' => Ttamhd::class,
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttamhd models.
	 * @return mixed
	 */
	public function actionIndex() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttamhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'index' );
	}
	/**
	 * Displays a single Ttamhd model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * @return array
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDetail() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttamit::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper  = $requestData[ 'oper' ];
			$id  = $requestData[ 'id' ];
			$model =  null;
			if(strpos($id, 'new_row') !== false){
				// $oper = 'add';
				$requestData[ 'Action' ] = 'Insert';
			}elseif($oper != 'del'){
				$model = $this->findModelBase64( 'Ttamit', $requestData[ 'id' ] );
				$requestData[ 'AMNoOLD' ] = $model->AMNo;
				$requestData[ 'MotorAutoNOLD' ] = $model->MotorAutoN;
				$requestData[ 'MotorNoMesinOLD' ] = $model->MotorNoMesin;
				$requestData[ 'Action' ] = 'Update';
			}else{
				$requestData[ 'Action' ] = 'Delete';
			}
			$result           = Ttamit::find()->callSP( $requestData );
			if($result[ 'status' ] == 0 ){
				$response = $this->responseSuccess( $result[ 'keterangan' ] );
			}else{
				$response = $this->responseFailed($result[ 'keterangan' ]  );
			}
		} else {
			/* operation : read */
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'ttamit.' . $primaryKeys[ $i ];
			}

			$rows                        = General::cCmd( "SELECT CONCAT(". implode( ",'||',", $primaryKeys ) .") AS id,
			ttamit.AMNo, ttamit.MotorNoMesin, 
			ttamit.MotorAutoN, tmotor.MotorType, tmotor.MotorTahun, tmotor.MotorWarna, tmotor.MotorNoRangka, 
			tdmotortype.MotorNama, tmotor.MMHarga
			FROM ttamit INNER JOIN
				tmotor ON ttamit.MotorNoMesin = tmotor.MotorNoMesin AND ttamit.MotorAutoN = tmotor.MotorAutoN INNER JOIN
				tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
			WHERE (ttamit.AMNo = :AMNo)", [
				':AMNo' => $requestData[ 'AMNo' ]
			] )->queryAll();

			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}

			$response =  [
				'rows'      => $rows,
				'rowsCount' => count( $rows )
			];
		}
		return $response;
	}
	/**
	 * Creates a new Ttamhd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model             = new Ttamhd();
		$model->AMNo       = Ttamhd::generateReference( true );
		$model->MotorTahun = date( 'Y' );
		$model->save();
		$post[ 'Action' ] = 'Insert';
		$result           = Ttamhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$id = $this->generateIdBase64FromModel( $model );
		return $this->render( 'create', [
			'model'    => $model,
			'AMNoView' => $result[ 'AMNo' ],
			'id'       => $id
		] );
	}
	/**
	 * Updates an existing Ttamhd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 * @param string $action
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id, $action ) {
		$model  = $this->findModelBase64( 'Ttamhd', $id );
		$dsJual = $model->attributes;
		if ( Yii::$app->request->isPost ) {
			$post             = Yii::$app->request->post()[ 'Ttamhd' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttamhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model = Ttamhd::findOne( $result[ 'AMNo' ] );
			$id    = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				$post[ 'AMNoView' ] = $result[ 'AMNo' ];
				return $this->redirect( [ 'ttamhd/update', 'id' => $id,  'action' => 'display' ] );
			} else {
				return $this->render( 'update', [
					'model'    => $model,
					'AMNoView' => $_POST[ 'AMNoView' ],
					'id'       => $id,
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsJual[ 'Action' ] = 'Load';
			$result             = Ttamhd::find()->callSP( $dsJual );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'AMNo' ] = $dsJual[ 'AMNo' ];
		}
		$dsJual[ 'AMNoView' ] = $_GET[ 'AMNoView' ] ?? $result[ 'AMNo' ];
		return $this->render( 'update', [
			'model'    => $model,
			'AMNoView' => $dsJual[ 'AMNoView' ],
			'id'       => $id
		] );
	}
	/**
	 * Deletes an existing Ttamhd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 * @param string $action
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionCancel( $id, $action ) {
		/** @var Ttamhd $model */
		$model = $this->findModelBase64( 'Ttamhd', $id );
		$_POST[ 'AMNo' ]   = $model->AMNo;
		$_POST[ 'Action' ] = 'Cancel';
		$result            = Ttamhd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if($result[ 'AMNo' ] == '--'){
			return $this->redirect(['index']);
		}else{
			$model = Ttamhd::findOne($result[ 'AMNo' ]);
			return $this->redirect( [ 'ttamhd/update',  'action' => 'display','id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}
	/**
	 * Deletes an existing Ttamhd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */

    public function actionDelete()
    {
        /** @var Ttamhd $model */
        $request = Yii::$app->request;
        if ($request->isAjax){
            $model = $this->findModelBase64('Ttamhd', $_POST['id']);
            $_POST['AMNo'] = $model->AMNo;
            $_POST['Action'] = 'Delete';
            $result = Ttamhd::find()->callSP($_POST);
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            $id = $request->get('id');
            $model = $this->findModelBase64('Ttamhd', $id);
            $_POST['AMNo'] = $model->AMNo;
            $_POST['Action'] = 'Delete';
            $result = Ttamhd::find()->callSP($_POST);
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['AMNo'] == '--') {
                return $this->redirect(['index']);
            } else {
                $model = Ttamhd::findOne($result['AMNo']);
                return $this->redirect(['ttamhd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
            }
        }
    }


	public function actionFillCombo() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$comm                        = General::cCmd( "SELECT CONCAT(" . implode( ',"||",', [ 'tmotor.MotorAutoN', 'tmotor.MotorNoMesin' ] ) . ") AS id,
						  tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.FBHarga, tdmotortype.MotorNama, MMHarga 
					      FROM tmotor 
					      INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
					       WHERE tmotor.MotorTahun BETWEEN :tgl AND :tgl2
						ORDER BY tmotor.MotorType, MotorWarna, MotorTahun", [
			':tgl'  => $requestData[ 'tgl' ] -1 ,
			':tgl2' => $requestData[ 'tgl2' ],
		] );
		$rowsArray                   = $comm->queryAll();
		for ( $i = 0; $i < count( $rowsArray ); $i ++ ) {
			$rowsArray[ $i ][ 'id' ] = base64_encode( $rowsArray[ $i ][ 'id' ] );
		}
		$response = [
			'rows'      => $rowsArray,
			'rowsCount' => sizeof( $rowsArray )
		];
		return $response;
	}
	/**
	 * Finds the Ttamhd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttamhd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttamhd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
