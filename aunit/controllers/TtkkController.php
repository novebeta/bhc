<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Attachment;
use aunit\models\Tdlokasi;
use aunit\models\Ttdk;
use aunit\models\Ttkk;
use aunit\models\Ttkm;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\db\Exception;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtkkController implements the CRUD actions for Ttkk model.
 */
class TtkkController extends AunitController {
	public function actions() {
		$colGrid    = null;
		$joinWith   = [];
		$join       = [];
		$where      = [];
		$sqlraw     = null;
		$queryRawPK = null;
		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			switch ( $r ) {
				case 'ttkk/pengajuan-bayar-bbn-kas':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'Bayar BBN' AND ttkk.KKNo NOT LIKE '%=%')",
							'params'    => []
						],
						[
							'op'        => 'AND',
							'condition' => "(LEFT(ttkk.KKNo,2) = 'KP' AND ttkk.KKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-subsidi-dealer-2':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'SubsidiDealer2' AND ttkk.KKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-retur-harga':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'Retur Harga' AND ttkk.KKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-insentif-sales':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'Insentif Sales' AND ttkk.KKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-potongan-khusus':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'Potongan Khusus' AND ttkk.KKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-bank-unit':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'KK Ke Bank' AND ttkk.KKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-umum':
					$colGrid    = Ttkk::colGridKasKeluarUmum();
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'Umum' AND ttkk.KKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-bank-bengkel':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'KK Ke Bengkel' AND ttkk.KKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-pinjaman-karyawan':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'Pinjaman Karyawan' AND ttkk.KKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-dealer-pos':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'KK Dealer Ke Pos' AND ttkk.KKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-pos-dealer':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'KK Pos Ke Dealer' AND ttkk.KKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-bayar-hutang-unit':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'Bayar Unit' AND ttkk.KKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-bayar-hutang-bbn':
					$colGrid    = Ttkk::colGridBayarHutangBbn();
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'Bayar BBN' AND ttkk.KKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-bayar-bbn-progresif':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'BBN Plus' AND ttkk.KKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-keluar-bayar-bbn-acc':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'BBN Acc' AND ttkk.KKNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkk/kas-transfer':
					$colGrid    = Ttkk::colGridTransferKas();
					$sqlraw     = "SELECT ttkk.KKNo, ttkk.KKTgl, ttkk.KKMemo, ttkk.KKNominal, ttkk.KKJenis, ttkk.KKPerson, 
					ttkk.KKLink, ttkk.UserID, ttkk.LokasiKode, ttkk.KKJam, ttkk.NoGL, ttkm.KMNo, ttkm.KMTgl, ttkm.KMMemo, ttkm.KMNominal, 
					ttkm.KMJenis, ttkm.KMPerson, ttkm.KMLink, 
					ttkm.KMJam FROM ttkk INNER JOIN ttkm ON ttkk.KKLInk = ttkm.KMLink
         			WHERE (ttkk.KKJenis = 'Kas Transfer' AND ttkm.KMJenis = 'Kas Transfer')";
					$queryRawPK = [ 'KKLInk' ];
					break;
				case 'ttkk/select-km-dari-bengkel':
					$colGrid  = Ttkk::colGrid();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND Date({$json['cmbTgl1']}) >= '{$json['tgl1']}' AND DATE({$json['cmbTgl1']}) <='{$json['tgl2']}' ";
					}
					$db         = 'b' . base64_decode( $_COOKIE[ '_outlet' ] ) . '.';
					$sql        = "Select ttkkhd.KKNo, ttkkhd.KKTgl, ttkkhd.KKMemo, ttkkhd.KKNominal, 
       				ttkkhd.KKNominal AS Sisa, ttkkhd.KKJenis, ttkkhd.KKPerson, ttkkhd.UserID, ttkkhd.KodeTrans  
			         FROM HVYS.ttkkhd 
			         INNER JOIN  HVYS.ttkkit ON ttkkit.KKNo =  ttkkhd.KKNo 
			         WHERE (ttkkhd.KodeTrans = 'H1')  AND (ttkkit.KKLink = 'KK Ke H1' OR  ttkkit.KKLink = '--') {$whereRaw} ";
					$sqlraw     = str_replace( 'HVYS.', $db, $sql );
					$queryRawPK = [ 'KKNo' ];
					break;
				case 'ttkk/select-bm-dari-kas':
					$colGrid  = Ttkk::colGrid();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND Date({$json['cmbTgl1']}) >= '{$json['tgl1']}' AND Date({$json['cmbTgl1']}) <= '{$json['tgl2']}' ";
					}
					$sqlraw     = "SELECT ttkk.KKNo, ttkk.KKTgl, ttkk.KKMemo, ttkk.KKNominal, ttkk.KKJenis, 
			         ttkk.KKPerson, ttkk.KKLink, ttkk.UserID, ttbmit.DKNo 
			         FROM ttkk 
			         LEFT OUTER JOIN ttbmit ON ttkk.KKNo = ttbmit.DKNo 
			         WHERE (ttkk.KKJenis LIKE 'KK Ke Bank') AND (ttbmit.DKNo is NULL)  {$whereRaw} ";
					$queryRawPK = [ 'KKNo' ];
					break;
				case 'ttkk/select-km-order-kk':
					$colGrid  = Ttkk::colGridSelectKMOrderKK();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND Date({$json['cmbTgl1']}) >= '{$json['tgl1']}' AND Date({$json['cmbTgl1']}) <= '{$json['tgl2']}' ";
					}
					$jenis          = $urlParams[ 'jenis' ];
					$KMNo           = $urlParams[ 'KMNo' ];
					$POSLokasiNomor = Menu::getUserLokasi()[ 'LokasiKode' ];
					switch ( $jenis ) {
						case 'KMPosDariDealer':
							$sqlraw = "Select ttkk.KKNo, ttkk.KKTgl, ttkk.KKMemo, ttkk.KKNominal, ttkk.KKJenis, 
				               ttkk.KKPerson, ttkk.KKLink, ttkk.UserID, KMSudah.KMLINK, KMSudah.KMNo, ttkk.KKNominal AS Sisa FROM ttkk 
				               LEFT OUTER JOIN (Select * from ttkm where kmjenis = 'KM Pos Dari Dealer') KMSudah 
				                   ON ttkk.KKNo = KMSudah.KMLINK 
				               WHERE (ttkk.KKJenis = 'KK Dealer Ke Pos')  AND (KKLink = '{$POSLokasiNomor}' ) AND 
				                     (KMSudah.KMNo is NULL OR KMSudah.KMNo  = '{$KMNo}')";
							break;
						case 'KMDealerDariPos':
							$sqlraw = "Select ttkk.KKNo, ttkk.KKTgl, ttkk.KKMemo, ttkk.KKNominal, ttkk.KKJenis, 
				               ttkk.KKPerson, ttkk.KKLink, ttkk.UserID , ttkk.KKNominal AS Sisa FROM ttkk 
				               LEFT OUTER JOIN (Select * from ttkm where kmjenis = 'KM Dealer Dari Pos') KMSudah 
				                   ON ttkk.KKNo = KMSudah.KMLINK 
				               WHERE (ttkk.KKJenis = 'KK Pos Ke Dealer') AND (KKLink = '{$POSLokasiNomor}' )  AND 
				                     (KMSudah.KMNo is NULL OR KMSudah.KMNo  = '{$KMNo}')";
							break;
					}
					$sqlraw     .= $whereRaw;
					$queryRawPK = [ 'KKNo' ];
					break;
				case 'ttkk/kwitansi-kas-bayar-bbn': // Keuangan kwitansi cetak awal
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkk.KKJenis LIKE 'Bayar BBN')",
							'params'    => []
						]
					];
					break;
			}
		}
		return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Ttkk::class,
				'queryRaw'   => $sqlraw,
				'queryRawPK' => $queryRawPK,
				'colGrid'    => $colGrid,
				'joinWith'   => $joinWith,
				'join'       => $join,
				'where'      => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionSelectKmDariBengkel() {
		$this->layout = 'select-mode';
		return $this->render( 'select', [
			'title'   => 'Bank Keluar Bayar Hutang Unit',
			'arr'     => [
				'cmbTxt'    => [
					'KKPerson' => 'Penerima',
					'KKNo'     => 'No Kas Keluar',
					'KKJenis'  => 'Jenis',
					'KKLink'   => 'No Transaksi',
					'KKMemo'   => 'Keterangan',
					'UserID'   => 'User',
					'NoGL'     => 'No GL',
				],
				'cmbTgl'    => [
					'KKTgl' => 'Tanggal Kas Keluar',
				],
				'cmbNum'    => [
					'KKNominal' => 'Nominal',
				],
				'sortname'  => "KKTgl",
				'sortorder' => 'desc'
			],
			'options' => [
				'colGrid' => Ttkk::colGridSelectKMDariBengkel(),
				'mode'    => self::MODE_SELECT,
			]
		] );
	}
	public function actionSelectKmOrderKk() {
		$this->layout = 'select-mode';
		return $this->render( 'select', [
			'title'   => 'Bank Keluar Bayar Hutang Unit',
			'arr'     => [
				'cmbTxt'    => [
					'KKPerson' => 'Penerima',
					'KKNo'     => 'No Kas Keluar',
					'KKJenis'  => 'Jenis',
					'KKLink'   => 'No Transaksi',
					'KKMemo'   => 'Keterangan',
					'UserID'   => 'User',
					'NoGL'     => 'No GL',
				],
				'cmbTgl'    => [
					'KKTgl' => 'Tanggal Kas Keluar',
				],
				'cmbNum'    => [
					'KKNominal' => 'Nominal',
				],
				'sortname'  => "KKTgl",
				'sortorder' => 'desc'
			],
			'options' => [
				'colGrid' => Ttkk::colGridSelectKMOrderKK(),
				'mode'    => self::MODE_SELECT,
			]
		] );
	}
	public function actionSelectBmDariKas() {
		$this->layout = 'select-mode';
		return $this->render( 'select', [
			'title'   => 'Bank Keluar Bayar Hutang Unit',
			'arr'     => [
				'cmbTxt'    => [
					'KKPerson' => 'Penerima',
					'KKNo'     => 'No Kas Keluar',
					'KKJenis'  => 'Jenis',
					'KKLink'   => 'No Transaksi',
					'KKMemo'   => 'Keterangan',
					'UserID'   => 'User',
					'NoGL'     => 'No GL',
				],
				'cmbTgl'    => [
					'KKTgl' => 'Tanggal Kas Keluar',
				],
				'cmbNum'    => [
					'KKNominal' => 'Nominal',
				],
				'sortname'  => "KKTgl",
				'sortorder' => 'desc'
			],
			'options' => [
				'colGrid' => Ttkk::colGridSelectKMOrderKK(),
				'mode'    => self::MODE_SELECT_MULTISELECT,
			]
		] );
	}
	/**
	 * Lists all Ttdk models.
	 * @return mixed
	 */
	public function actionPengajuanBayarBbnKas() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkk::find()->callSPPengajuan( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'PengajuanBayarBBNKas', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkk/td' ] )
		] );
	}
	public function actionPengajuanBayarBbnKasCreate() {
		return $this->bayar_hutang_create( 'Bayar BBN', 'pengajuan-bayar-bbn-kas', 'Pengajuan Bayar BBN via Kas', '_kas-keluar-form-bayar-bbn' );
	}
	/**
	 * @param $KKJenis
	 * @param $view
	 * @param $title
	 *
	 * @return string
	 */
	private function bayar_hutang_create( $KKJenis, $view, $title, $templateForm = '_kas-keluar-form' ) {
		TUi::$actionMode   = Menu::ADD;
		$model             = new Ttkk();
		$model->KKNo       = Ttkk::generateKKNo( true );
		$model->KKTgl      = date( 'Y-m-d' );
		$model->KKJam      = date( 'Y-m-d H:i:s' );
		$model->KKNominal  = floatval( $_GET[ 'KKNominal' ] ?? 0 );
		$model->KKLink     = $_GET[ 'KKLink' ] ?? "--";
		$model->KKMemo     = $_GET[ 'KKMemo' ] ?? "--";
		$model->KKPerson   = $_GET[ 'KKPerson' ] ?? "--";
		$model->KKJenis    = $KKJenis;
		$model->UserID     = $this->UserID();
		$model->NoGL       = "--";
		$model->LokasiKode = Menu::getUserLokasi()[ 'LokasiKode' ];
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		if ( $templateForm == "_kas-keluar-form-bayar-bbn" ) {
			$result = Ttkk::find()->callSPPengajuan( $post );
		} else {
			$result = Ttkk::find()->callSP( $post );
		}
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttkk::find()->ttkkFillByNo()->where( [ 'ttkk.KKNo' => $model->KKNo, 'ttkk.KKJenis' => $model->KKJenis, 'ttkk.LokasiKode' => $model->LokasiKode ] )
		                             ->asArray( true )->one();
		$dsTUang[ 'Total' ]    = 0;
		$id                    = $this->generateIdBase64FromModel( $model );
		$dsTUang[ 'KKNoView' ] = $result[ 'KKNo' ];
		return $this->render( 'kas-keluar-create', [
			'model'        => $model,
			'dsTUang'      => $dsTUang,
			'id'           => $id,
			'view'         => $view,
			'title'        => $title,
			'templateForm' => $templateForm,
		] );
	}
	public function actionPengajuanBayarBbnKasUpdate( $id ) {
		return $this->bayar_hutang_update( 'Bayar BBN', $id, 'pengajuan-bayar-bbn-kas', 'Pengajuan Bayar BBN via Kas', '_kas-keluar-form-bayar-bbn' );
	}
	private function bayar_hutang_update( $KKJenis, $id, $view, $title, $templateForm = '_kas-keluar-form' ) {
		$index = Ttkk::getRoute( $KKJenis, [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttkk $model */
		$model              = $this->findModelBase64( 'Ttkk', $id );
		$dsTUang            = Ttkk::find()->ttkkFillByNo()->where( [ 'ttkk.KKNo' => $model->KKNo, 'ttkk.KKJenis' => $model->KKJenis, 'ttkk.LokasiKode' => $model->LokasiKode ] )
		                          ->asArray( true )->one();
		$dsTUang[ 'Total' ] = 0;
		if ( Yii::$app->request->isPost ) {
			$post             = array_merge( $dsTUang, \Yii::$app->request->post() );
			$post[ 'UserID' ] = $this->UserID();
			$post[ 'KKJam' ]  = $post[ 'KKTgl' ] . ' ' . $post[ 'KKJam' ];
			$post[ 'Action' ] = 'Update';
			if ( $templateForm == "_kas-keluar-form-bayar-bbn" ) {
				$result = Ttkk::find()->callSPPengajuan( $post );
			} else {
				$result = Ttkk::find()->callSP( $post );
			}
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model = Ttkk::findOne( $result[ 'KKNo' ] );
			$id    = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 ) {
				$post[ 'KKNoView' ] = $result[ 'KKNo' ];
				if ( $templateForm == "_kas-keluar-form-bayar-bbn" ) {
					return $this->redirect( [ 'ttkk/pengajuan-bayar-bbn-kas-update', 'action' => 'display', 'id' => $id ] );
				}
				$this->redirect( Ttkk::getRoute( $KKJenis, [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'kas-keluar-update', [
					'model'        => $model,
					'dsTUang'      => $post,
					'id'           => $id,
					'view'         => $view,
					'title'        => $title,
					'templateForm' => $templateForm,
				] );
			}
		}
		$dsTUang[ 'Action' ] = 'Load';
		if ( $templateForm == "_kas-keluar-form-bayar-bbn" ) {
			$result = Ttkk::find()->callSPPengajuan( $dsTUang );
		} else {
			$result = Ttkk::find()->callSP( $dsTUang );
		}
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang[ 'KKNoView' ] = $_GET[ 'KKNoView' ] ?? $result[ 'KKNo' ];
		return $this->render( 'kas-keluar-update', [
			'model'        => $model,
			'dsTUang'      => $dsTUang,
			'id'           => $id,
			'view'         => $view,
			'title'        => $title,
			'templateForm' => $templateForm,
		] );
	}
	public function actionKasKeluarSubsidiDealer2() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasKeluarSubsidiDealer2', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkk/td' ] )
		] );
	}
	public function actionKasKeluarSubsidiDealer2Create() {
		return $this->create( 'SubsidiDealer2', 'kas-keluar-subsidi-dealer-2', 'KAS KELUAR - Subsidi Dealer 2' );
	}
	/**
	 * @param $KKJenis
	 * @param $view
	 * @param $title
	 *
	 * @return string
	 */
	private function create( $KKJenis, $view, $title ) {
		TUi::$actionMode  = Menu::ADD;
		$model            = new Ttkk();
		$model->KKNo      = Ttkk::generateKKNo( true );
		$model->KKTgl     = date( 'Y-m-d' );
		$model->KKJam     = date( 'Y-m-d H:i:s' );
		$model->KKNominal = floatval( $_GET[ 'KKNominal' ] ?? 0 );
		$model->KKLink    = $_GET[ 'KKLink' ] ?? "--";
		if ( $KKJenis === 'KK Pos Ke Dealer' ) {
			$model->KKLink = Tdlokasi::find()->where( [ 'LokasiNomor' => '01' ] )->one()->LokasiKode;
		}
		$model->KKMemo     = $_GET[ 'KKMemo' ] ?? "--";
		$model->KKPerson   = $_GET[ 'KKPerson' ] ?? "--";
		$model->KKJenis    = $KKJenis;
		$model->UserID     = $this->UserID();
		$model->NoGL       = "--";
		$model->LokasiKode = Menu::getUserLokasi()[ 'LokasiKode' ];
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttkk::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang = Ttkk::find()->ttkkFillByNo()->where( [ 'ttkk.KKNo' => $model->KKNo, 'ttkk.KKJenis' => $model->KKJenis, 'ttkk.LokasiKode' => $model->LokasiKode ] )
		               ->asArray( true )->one();
		switch ( $KKJenis ) {
			case 'KK Ke Bank' :
				$JoinData = General::cCmd( "SELECT BMNo, BMTgl, BMMemo, BMNominal, BMJenis, ttbmhd.LeaseKode,
				BMCekNo, BMCekTempo, BMPerson, BMAC, ttbmhd.NoAccount, UserID, tdleasing.LeaseNama, traccount.NamaAccount
				FROM ttbmhd
				INNER JOIN tdleasing ON ttbmhd.LeaseKode = tdleasing.LeaseKode
				INNER JOIN traccount ON ttbmhd.NoAccount = traccount.NoAccount
				WHERE BMNo = :BMNo", [ ':BMNo' => $dsTUang[ 'KKLink' ] ] )->queryOne();
				if ( $JoinData != null ) {
					$JoinData[ 'KKLinkTgl' ] = $JoinData[ 'BMTgl' ];
					$JoinData[ 'MotorType' ] = $JoinData[ 'BMAC' ] . ' - ' . $JoinData[ 'BMCekNo' ] . ' - ' . General::asDate( $JoinData[ 'BMCekTempo' ], 'dd/MM/yyyy' );
					$JoinData[ 'CusNama' ]   = $JoinData[ 'BMPerson' ];
					$JoinData[ 'CusAlamat' ] = $JoinData[ 'NamaAccount' ] . ' - ' . $JoinData[ 'LeaseNama' ];
				} else {
					$JoinData[ 'KKLinkTgl' ] = '1900-01-01';
					$JoinData[ 'MotorType' ] = '--';
					$JoinData[ 'CusNama' ]   = '--';
					$JoinData[ 'CusAlamat' ] = '--';
					$JoinData[ 'LeaseKode' ] = '';
				}
				break;
			case 'KK Ke Bengkel' :
				$ttkm                    = General::cCmd( "SELECT KMNo, KMTgl, KMMemo, KMNominal, KMJenis, KMPerson, KMLink, UserID, LokasiKode, KMJam, NoGL FROM ttkm
 				WHERE KMNo = :KMNo", [ ':KMNo' => $dsTUang[ 'KKLink' ] ] )->queryOne();
				$db                      = 'b' . base64_decode( $_COOKIE[ '_outlet' ] ) . '.';
				$sql                     = "SELECT ttbmhd.BMNo, BMTgl, BMMemo, BMNominal, BMJenis,
				BMCekNo, BMCekTempo, BMPerson, BMAC, ttbmhd.BankKode, UserID, traccount.NamaAccount
				FROM HVYS.ttbmhd
				INNER JOIN HVYS.ttbmit ON ttbmhd.BMNo = ttbmit.BMNo
				INNER JOIN HVYS.traccount ON ttbmhd.BankKode = traccount.NoAccount
				WHERE BMLink = :BMLink";
				$JoinData                = General::cCmd( str_replace( 'HVYS.', $db, $sql ), [ ':BMLink' => $model->KKNo ] )->queryOne();
                if ( $JoinData != null ) {
                    $joinDataFalse           = ( $JoinData === false );
                    $JoinData[ 'MotorType' ] = ( $joinDataFalse ) ? '' : $JoinData[ 'BMAC' ] . ' - ' . $JoinData[ 'BMCekNo' ] . ' - ' . General::asDate( $JoinData[ 'BMCekTempo' ], 'dd/MM/yyyy' );
                    $JoinData[ 'CusAlamat' ] = ( $joinDataFalse ) ? '' : $JoinData[ 'BMNo' ] . ' - ' . $JoinData[ 'BMPerson' ] . ' - ' . $JoinData[ 'NamaAccount' ];
                    $JoinData[ 'LeaseKode' ] = '';
                    $JoinData[ 'CusNama' ]   = $ttkm[ 'KMPerson' ];
                    $JoinData[ 'KKLinkTgl' ] = $ttkm[ 'KMTgl' ];
                } else {
                    $JoinData[ 'KKLinkTgl' ] = '1900-01-01';
                    $JoinData[ 'MotorType' ] = '--';
                    $JoinData[ 'CusNama' ]   = '--';
                    $JoinData[ 'CusAlamat' ] = '--';
                    $JoinData[ 'LeaseKode' ] = '';
                }
				break;
			case 'Pinjaman Karyawan' :
				$JoinData = General::cCmd( "SELECT SalesKode, SalesNama, SalesAlamat, SalesTelepon, SalesStatus, SalesKeterangan
				FROM tdsales WHERE SalesKode = :SalesKode", [ ':SalesKode' => $dsTUang[ 'KKLink' ] ] )->queryOne();
				if ( $JoinData != null ) {
					$JoinData[ 'MotorType' ] = $JoinData[ 'SalesKeterangan' ];
					$JoinData[ 'LeaseKode' ] = '';
//				$JoinData[ 'CusAlamat' ] = $JoinData[ 'SalesAlamat' ] . ' - '.$JoinData[ 'SalesTelepon' ];
					$JoinData[ 'CusAlamat' ] = '--';
					$JoinData[ 'SalesNama' ] = '--';
					$JoinData[ 'CusNama' ]   = $JoinData[ 'SalesNama' ];
					$JoinData[ 'KKLinkTgl' ] = '1900-01-01';
				} else {
					$JoinData[ 'KKLinkTgl' ] = '1900-01-01';
					$JoinData[ 'MotorType' ] = '--';
					$JoinData[ 'CusNama' ]   = '--';
					$JoinData[ 'CusAlamat' ] = '--';
					$JoinData[ 'LeaseKode' ] = '';
					$JoinData[ 'SalesNama' ] = '--';
				}
				break;
			case 'KK Dealer Ke Pos' :
				$JoinData                = General::cCmd( "SELECT LokasiKode, LokasiNama, LokasiPetugas, tdsales.SalesNama,
				LokasiAlamat, LokasiStatus, LokasiTelepon, LokasiNomor
				FROM tdlokasi
				INNER JOIN tdsales ON tdlokasi.LokasiPetugas = tdsales.SalesKode
				WHERE LokasiKode = :LokasiKode", [ ':LokasiKode' => $dsTUang[ 'KKLink' ] ] )->queryOne();
				$joinDataFalse           = ( $JoinData === false );
				$JoinData[ 'CusNama' ]   = ( $joinDataFalse ) ? '' : $JoinData[ 'LokasiNama' ];
				$JoinData[ 'CusAlamat' ] = ( $joinDataFalse ) ? '' : $JoinData[ 'LokasiAlamat' ] . ' - ' . $JoinData[ 'LokasiTelepon' ];
				$JoinData[ 'MotorType' ] = ( $joinDataFalse ) ? '' : $JoinData[ 'LokasiPetugas' ] . ' - ' . $JoinData[ 'SalesNama' ];
				$JoinData[ 'LeaseKode' ] = '';
				$JoinData[ 'KKLinkTgl' ] = '1900-01-01';
				break;
			case 'KK Pos Ke Dealer' :
				$JoinData                = General::cCmd( "SELECT LokasiKode, LokasiNama, LokasiPetugas, tdsales.SalesNama,
				LokasiAlamat, LokasiStatus, LokasiTelepon, LokasiNomor
       			FROM tdlokasi 
				INNER JOIN tdsales ON tdlokasi.LokasiPetugas = tdsales.SalesKode
				WHERE LokasiKode = :LokasiKode", [ ':LokasiKode' => $dsTUang[ 'KKLink' ] ] )->queryOne();
				$joinDataFalse           = ( $JoinData === false );
				$JoinData[ 'CusNama' ]   = ( $joinDataFalse ) ? '' : $JoinData[ 'LokasiNama' ];
				$JoinData[ 'CusAlamat' ] = ( $joinDataFalse ) ? '' : $JoinData[ 'LokasiAlamat' ] . ' - ' . $JoinData[ 'LokasiTelepon' ];
				$JoinData[ 'MotorType' ] = ( $joinDataFalse ) ? '' : $JoinData[ 'LokasiPetugas' ] . ' - ' . $JoinData[ 'SalesNama' ];
				$JoinData[ 'LeaseKode' ] = '';
				$JoinData[ 'KKLinkTgl' ] = '1900-01-01';
				break;
			default:
				$JoinData = Ttdk::find()->TtdkGetJoinData()->where( [ 'ttdk.DKNo' => $model->KKLink ] )->asArray( true )->one();
				if ( $JoinData != null ) {
//				$dsTUang[ 'CusNama' ] = $JoinData['CusNama'];
					$JoinData[ 'CusAlamat' ] = $JoinData[ 'CusAlamat' ] . ' - RT/RW :' . $JoinData[ 'CusRT' ] . '/' . $JoinData[ 'CusRW' ] . ' - ' . $JoinData[ 'CusKelurahan' ] .
					                           ' - ' . $JoinData[ 'CusKecamatan' ] . ' - ' . $JoinData[ 'CusKabupaten' ] . ' - ' . $JoinData[ 'Provinsi' ];
					$JoinData[ 'MotorType' ] = $JoinData[ 'MotorType' ] . ' - ' . $JoinData[ 'MotorWarna' ] . ' - ' . $JoinData[ 'MotorTahun' ] . ' - ' . $JoinData[ 'MotorNoMesin' ] . ' - ' . $JoinData[ 'MotorNoRangka' ];
					$JoinData[ 'KKLinkTgl' ] = $JoinData[ 'DKTgl' ];
				} else {
					$JoinData[ 'KKLinkTgl' ] = '1900-01-01';
					$JoinData[ 'MotorType' ] = '--';
					$JoinData[ 'CusNama' ]   = '--';
					$JoinData[ 'CusAlamat' ] = '--';
					$JoinData[ 'LeaseKode' ] = '';
					$JoinData[ 'SalesNama' ] = '--';
				}
		}
		$dsTUang[ 'KKNoView' ] = $result[ 'KKNo' ];
		$dsTUang[ 'SubTotal' ] = 0;
		$id                    = $this->generateIdBase64FromModel( $model );
		return $this->render( 'create', [
			'model'    => $model,
			'dsTUang'  => $dsTUang,
			'JoinData' => $JoinData,
			'id'       => $id,
			'view'     => $view,
			'title'    => $title
		] );
	}
	public function actionKasKeluarSubsidiDealer2Update( $id ) {
		return $this->update( $id, 'kas-keluar-subsidi-dealer-2', 'KAS KELUAR - Subsidi Dealer 2', 'SubsidiDealer2' );
	}
	/**
	 * @param $id
	 * @param $view
	 * @param $title
	 *
	 * @return string|\yii\web\Response
	 * @throws NotFoundHttpException
	 */
	private function update( $id, $view, $title, $KKJenis ) {
		$index = Ttkk::getRoute( $KKJenis, [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttkk $model */
		$model   = $this->findModelBase64( 'Ttkk', $id );
		$dsTUang = Ttkk::find()->ttkkFillByNo()->where( [ 'ttkk.KKNo' => $model->KKNo, 'ttkk.KKJenis' => $model->KKJenis, 'ttkk.LokasiKode' => $model->LokasiKode ] )
		               ->asArray( true )->one();
		switch ( $KKJenis ) {
			case 'KK Ke Bank' :
				$JoinData = General::cCmd( "SELECT BMNo, BMTgl, BMMemo, BMNominal, BMJenis, ttbmhd.LeaseKode,
				BMCekNo, BMCekTempo, BMPerson, BMAC, ttbmhd.NoAccount, UserID, tdleasing.LeaseNama, traccount.NamaAccount
				FROM ttbmhd
				INNER JOIN tdleasing ON ttbmhd.LeaseKode = tdleasing.LeaseKode
				INNER JOIN traccount ON ttbmhd.NoAccount = traccount.NoAccount
				WHERE BMNo = :BMNo", [ ':BMNo' => $dsTUang[ 'KKLink' ] ] )->queryOne();
				if ( $JoinData != null ) {
					$JoinData[ 'KKLinkTgl' ] = $JoinData[ 'BMTgl' ];
					$JoinData[ 'MotorType' ] = $JoinData[ 'BMAC' ] . ' - ' . $JoinData[ 'BMCekNo' ] . ' - ' . General::asDate( $JoinData[ 'BMCekTempo' ], 'dd/MM/yyyy' );
					$JoinData[ 'CusNama' ]   = $JoinData[ 'BMPerson' ];
					$JoinData[ 'CusAlamat' ] = $JoinData[ 'NamaAccount' ] . ' - ' . $JoinData[ 'LeaseNama' ];
				} else {
					$JoinData[ 'KKLinkTgl' ] = '1900-01-01';
					$JoinData[ 'MotorType' ] = '--';
					$JoinData[ 'CusNama' ]   = '--';
					$JoinData[ 'CusAlamat' ] = '--';
					$JoinData[ 'LeaseKode' ] = '';
				}
				break;
			case 'KK Ke Bengkel' :
				$ttkm                    = General::cCmd( "SELECT KMNo, KMTgl, KMMemo, KMNominal, KMJenis, KMPerson, KMLink, UserID, LokasiKode, KMJam, NoGL FROM ttkm
 				WHERE KMNo = :KMNo", [ ':KMNo' => $dsTUang[ 'KKLink' ] ] )->queryOne();
				$db                      = 'b' . base64_decode( $_COOKIE[ '_outlet' ] ) . '.';
				$sql                     = "SELECT ttbmhd.BMNo, BMTgl, BMMemo, BMNominal, BMJenis,
				BMCekNo, BMCekTempo, BMPerson, BMAC, ttbmhd.BankKode, UserID, traccount.NamaAccount
				FROM HVYS.ttbmhd
				INNER JOIN HVYS.ttbmit ON ttbmhd.BMNo = ttbmit.BMNo
				INNER JOIN HVYS.traccount ON ttbmhd.BankKode = traccount.NoAccount
				WHERE BMLink = :BMLink";
				$JoinData                = General::cCmd( str_replace( 'HVYS.', $db, $sql ), [ ':BMLink' => $model->KKNo ] )->queryOne();
				$joinDataFalse           = ( $JoinData === false );
				$JoinData[ 'MotorType' ] = ( $joinDataFalse ) ? '' : $JoinData[ 'BMAC' ] . ' - ' . $JoinData[ 'BMCekNo' ] . ' - ' . General::asDate( $JoinData[ 'BMCekTempo' ], 'dd/MM/yyyy' );
				$JoinData[ 'CusAlamat' ] = ( $joinDataFalse ) ? '' : $JoinData[ 'BMNo' ] . ' - ' . $JoinData[ 'BMPerson' ] . ' - ' . $JoinData[ 'NamaAccount' ];
				$JoinData[ 'LeaseKode' ] = '';
				$JoinData[ 'CusNama' ]   = $ttkm[ 'KMPerson' ];
				$JoinData[ 'KKLinkTgl' ] = $ttkm[ 'KMTgl' ];
				break;
			case 'Pinjaman Karyawan' :
				$JoinData = General::cCmd( "SELECT SalesKode, SalesNama, SalesAlamat, SalesTelepon, SalesStatus, SalesKeterangan
				FROM tdsales WHERE SalesKode = :SalesKode", [ ':SalesKode' => $dsTUang[ 'KKLink' ] ] )->queryOne();
				if ( $JoinData != null ) {
					$JoinData[ 'MotorType' ] = $JoinData[ 'SalesKeterangan' ];
					$JoinData[ 'LeaseKode' ] = '';
					$JoinData[ 'CusAlamat' ] = $JoinData[ 'SalesAlamat' ] . ' - ' . $JoinData[ 'SalesTelepon' ];
					$JoinData[ 'CusNama' ]   = $JoinData[ 'SalesNama' ];
//					$JoinData[ 'SalesNama' ] = '--';
					$JoinData[ 'KKLinkTgl' ] = '1900-01-01';
				} else {
					$JoinData[ 'KKLinkTgl' ] = '1900-01-01';
					$JoinData[ 'MotorType' ] = '--';
					$JoinData[ 'CusNama' ]   = '--';
					$JoinData[ 'CusAlamat' ] = '--';
					$JoinData[ 'LeaseKode' ] = '';
					$JoinData[ 'SalesNama' ] = '--';
				}
				break;
			case 'KK Dealer Ke Pos' :
				$JoinData                = General::cCmd( "SELECT LokasiKode, LokasiNama, LokasiPetugas, tdsales.SalesNama,
				LokasiAlamat, LokasiStatus, LokasiTelepon, LokasiNomor
				FROM tdlokasi
				INNER JOIN tdsales ON tdlokasi.LokasiPetugas = tdsales.SalesKode
				WHERE LokasiKode = :LokasiKode", [ ':LokasiKode' => $dsTUang[ 'KKLink' ] ] )->queryOne();
				$joinDataFalse           = ( $JoinData === false );
				$JoinData[ 'CusNama' ]   = ( $joinDataFalse ) ? '' : $JoinData[ 'LokasiNama' ];
				$JoinData[ 'CusAlamat' ] = ( $joinDataFalse ) ? '' : $JoinData[ 'LokasiAlamat' ] . ' - ' . $JoinData[ 'LokasiTelepon' ];
				$JoinData[ 'MotorType' ] = ( $joinDataFalse ) ? '' : $JoinData[ 'LokasiPetugas' ] . ' - ' . $JoinData[ 'SalesNama' ];
				$JoinData[ 'LeaseKode' ] = '';
				$JoinData[ 'KKLinkTgl' ] = '1900-01-01';
				break;
			case 'KK Pos Ke Dealer' :
				$JoinData                = General::cCmd( "SELECT LokasiKode, LokasiNama, LokasiPetugas, tdsales.SalesNama,
				LokasiAlamat, LokasiStatus, LokasiTelepon, LokasiNomor
       			FROM tdlokasi 
				INNER JOIN tdsales ON tdlokasi.LokasiPetugas = tdsales.SalesKode
				WHERE LokasiKode = :LokasiKode", [ ':LokasiKode' => $dsTUang[ 'KKLink' ] ] )->queryOne();
				$joinDataFalse           = ( $JoinData === false );
				$JoinData[ 'CusNama' ]   = ( $joinDataFalse ) ? '' : $JoinData[ 'LokasiNama' ];
				$JoinData[ 'CusAlamat' ] = ( $joinDataFalse ) ? '' : $JoinData[ 'LokasiAlamat' ] . ' - ' . $JoinData[ 'LokasiTelepon' ];
				$JoinData[ 'MotorType' ] = ( $joinDataFalse ) ? '' : $JoinData[ 'LokasiPetugas' ] . ' - ' . $JoinData[ 'SalesNama' ];
				$JoinData[ 'LeaseKode' ] = '';
				$JoinData[ 'KKLinkTgl' ] = '1900-01-01';
				break;
			default:
				$JoinData = Ttdk::find()->TtdkGetJoinData()->where( [ 'ttdk.DKNo' => $model->KKLink ] )->asArray( true )->one();
				if ( $JoinData != null ) {
//				$dsTUang[ 'CusNama' ] = $JoinData['CusNama'];
					$JoinData[ 'CusAlamat' ] = $JoinData[ 'CusAlamat' ] . ' - RT/RW :' . $JoinData[ 'CusRT' ] . '/' . $JoinData[ 'CusRW' ] . ' - ' . $JoinData[ 'CusKelurahan' ] .
					                           ' - ' . $JoinData[ 'CusKecamatan' ] . ' - ' . $JoinData[ 'CusKabupaten' ] . ' - ' . $JoinData[ 'Provinsi' ];
//					$JoinData[ 'MotorType' ] = $JoinData[ 'MotorType' ] . ' - ' . $JoinData[ 'MotorWarna' ] . ' - ' . $JoinData[ 'MotorTahun' ] . ' - ' . $JoinData[ 'MotorNoMesin' ] . ' - ' . $JoinData[ 'MotorNoRangka' ];
					$JoinData[ 'MotorType' ] = $JoinData[ 'MotorNama' ] . ' - ' . $JoinData[ 'MotorWarna' ] . ' - ' . $JoinData[ 'MotorTahun' ] . ' - ' . $JoinData[ 'MotorNoMesin' ] . ' - ' . $JoinData[ 'MotorNoRangka' ];
					$JoinData[ 'KKLinkTgl' ] = $JoinData[ 'DKTgl' ];
				} else {
					$JoinData[ 'KKLinkTgl' ] = '1900-01-01';
					$JoinData[ 'MotorType' ] = '--';
					$JoinData[ 'CusNama' ]   = '--';
					$JoinData[ 'CusAlamat' ] = '--';
					$JoinData[ 'LeaseKode' ] = '';
					$JoinData[ 'SalesNama' ] = '--';
				}
		}
		if ( Yii::$app->request->isPost ) {
			$post             = \Yii::$app->request->post();
			$post[ 'UserID' ] = $this->UserID();
//			$post[ 'KKTgl' ]    = date( 'Y-m-d' );
			$post[ 'KKJam' ]    = $post[ 'KKTgl' ] . ' ' . $post[ 'KKJam' ];
			$post[ 'KKNoBaru' ] = $post[ 'KKNo' ];
			$post[ 'Action' ]   = 'Update';
			$result             = Ttkk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model = Ttkk::findOne( $result[ 'KKNo' ] );
			$id    = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 ) {
				$post[ 'KKNoView' ] = $result[ 'KKNo' ];
				return $this->redirect( Ttkk::getRoute( $KKJenis, [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'update', [
					'model'    => $model,
					'dsTUang'  => $post,
					'JoinData' => $JoinData,
					'title'    => $title,
					'view'     => $view,
					'id'       => $id,
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttkk::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'KKNo' ] = $dsTUang[ 'KKNo' ];
		}
		$dsTUang[ 'KKNoView' ] = $_GET[ 'KKNoView' ] ?? $result[ 'KKNo' ];
		return $this->render( 'update', [
			'model'    => $model,
			'dsTUang'  => $dsTUang,
			'JoinData' => $JoinData,
			'id'       => $id,
			'view'     => $view,
			'title'    => $title
		] );
	}
	public function actionKasKeluarReturHarga() {
		$post[ 'Action' ] = 'Display';
		$result           = Ttkk::find()->callSP( $post );
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasKeluarReturHarga', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkk/td' ] )
		] );
	}
	public function actionKasKeluarReturHargaCreate() {
		return $this->create( 'Retur Harga', 'kas-keluar-retur-harga', 'KAS KELUAR - Retur Harga' );
	}
	public function actionKasKeluarReturHargaUpdate( $id ) {
		return $this->update( $id, 'kas-keluar-retur-harga', 'KAS KELUAR - Retur Harga', 'Retur Harga' );
	}
	public function actionKasKeluarInsentifSales() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasKeluarInsentifSales', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkk/td' ] )
		] );
	}
	public function actionKasKeluarInsentifSalesCreate() {
		return $this->create( 'Insentif Sales', 'kas-keluar-insentif-sales', 'KAS KELUAR - Insentif Sales' );
	}
	public function actionKasKeluarInsentifSalesUpdate( $id ) {
		return $this->update( $id, 'kas-keluar-insentif-sales', 'KAS KELUAR - Insentif Sales', 'Insentif Sales' );
	}
	public function actionKasKeluarPotonganKhusus() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasKeluarPotonganKhusus', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkk/td' ] )
		] );
	}
	public function actionKasKeluarPotonganKhususCreate() {
		return $this->create( 'Potongan Khusus', 'kas-keluar-potongan-khusus', 'KAS KELUAR - Potongan Khusus' );
	}
	public function actionKasKeluarPotonganKhususUpdate( $id ) {
		return $this->update( $id, 'kas-keluar-potongan-khusus', 'KAS KELUAR - Potongan Khusus', 'Potongan Khusus' );
	}
	public function actionKasKeluarBankUnit() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasKeluarBankUnit', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkk/td' ] )
		] );
	}
	public function actionKasKeluarBankUnitCreate() {
		return $this->create( 'KK Ke Bank', 'kas-keluar-bank-unit', 'KAS KELUAR - Ke Bank Unit - H1' );
	}
	public function actionKasKeluarBankUnitUpdate( $id ) {
		return $this->update( $id, 'kas-keluar-potongan-khusus', 'KAS KELUAR - Ke Bank Unit - H1', 'KK Ke Bank' );
	}
	public function actionKasKeluarUmum() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasKeluarUmum', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkk/td' ] )
		] );
	}
	public function actionKasKeluarUmumCreate() {
		TUi::$actionMode   = Menu::ADD;
		$model             = new Ttkk();
		$model->KKNo       = Ttkk::generateKKNo( true );
		$model->KKTgl      = date( 'Y-m-d' );
		$model->KKJam      = date( 'Y-m-d H:i:s' );
		$model->KKNominal  = 0;
		$model->KKLink     = "--";
		$model->KKMemo     = "--";
		$model->KKJenis    = "Umum";
		$model->KKPerson   = "--";
		$model->UserID     = $this->UserID();
		$model->NoGL       = "--";
		$model->LokasiKode = Menu::getUserLokasi()[ 'LokasiKode' ];
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttkk::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang                 = Ttkk::find()->ttkkFillByNo()->where( [ 'ttkk.KKNo' => $model->KKNo, 'ttkk.KKJenis' => $model->KKJenis, 'ttkk.LokasiKode' => $model->LokasiKode ] )
		                               ->asArray( true )->one();
		$JoinData                = Ttdk::find()->GetJoinData()->where( [ 'ttdk.DKNo' => $model->KKLink ] )->asArray( true )->one();
		$JoinData[ 'CusNama' ]   = '--';
		$JoinData[ 'CusAlamat' ] = '--';
		$JoinData[ 'MotorType' ] = '--';
		$JoinData[ 'LeaseKode' ] = '--';
		$dsTUang[ 'KKNoView' ]   = $result[ 'KKNo' ];
		$dsTUang[ 'SubTotal' ]   = 0;
		$id                      = $this->generateIdBase64FromModel( $model );
		return $this->render( 'kas-keluar-umum-create', [
			'model'    => $model,
			'dsTUang'  => $dsTUang,
			'JoinData' => $JoinData,
			'id'       => $id
		] );
	}
	/**
	 * @param $id
	 * @param $action
	 *
	 * @return string|\yii\web\Response
	 * @throws NotFoundHttpException
	 */
	public function actionKasKeluarUmumUpdate( $id ) {
		$index = Ttkk::getRoute( 'Umum', [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		if(!empty($_GET[ 'source' ]) && $_GET[ 'source' ] == 'KasKeluarAcc'){
			$val        = explode( "||", base64_decode( $id ) );
			$outlet64 	= $_COOKIE['_outlet'];
        	$outlet     = base64_decode($outlet64);
			if($val[0] == $outlet){
				$id 		= base64_encode($val[1]);
			}else{
				Yii::$app->session->addFlash( 'warning', 'Silahkan relogin sesuai dengan cabang untuk membuka data ini.' );
				return $this->redirect([ 'ttkk/kas-keluar-acc','action' => 'update']);
			}
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttkk $model */
		$model                   = $this->findModelBase64( 'Ttkk', $id );
		$dsTUang                 = Ttkk::find()->ttkkFillByNo()->where( [ 'ttkk.KKNo' => $model->KKNo, 'ttkk.KKJenis' => $model->KKJenis, 'ttkk.LokasiKode' => $model->LokasiKode ] )
		                               ->asArray( true )->one();
		$JoinData                = Ttdk::find()->GetJoinData()->where( [ 'ttdk.DKNo' => $model->KKLink ] )->asArray( true )->one();
		$JoinData[ 'CusNama' ]   = $JoinData[ 'CusNama' ] ?? '--';
		$JoinData[ 'CusAlamat' ] = $JoinData[ 'CusAlamat' ] ?? '--';
		$JoinData[ 'MotorType' ] = $JoinData[ 'MotorType' ] ?? '--';
		$JoinData[ 'LeaseKode' ] = $JoinData[ 'LeaseKode' ] ?? '--';
		if ( Yii::$app->request->isPost ) {
			$post             = array_merge( $dsTUang, \Yii::$app->request->post() );
			$post[ 'UserID' ] = $this->UserID();
			$post[ 'KKJam' ]  = $post[ 'KKTgl' ] . ' ' . $post[ 'KKJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttkk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttkk::findOne( $result[ 'KKNo' ] );
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'KKNoView' ] = $result[ 'KKNo' ];
			if ( $result[ 'status' ] == 0 ) {
				return $this->redirect( Ttkk::getRoute( 'Umum', [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'kas-keluar-umum-update', [
					'model'    => $model,
					'dsTUang'  => $post,
					'JoinData' => $JoinData,
					'id'       => $id,
				] );
			}
		}
		$dsTUang[ 'SubTotal' ] = 0;
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttkk::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'KKNo' ] = $dsTUang[ 'KKNo' ];
		}
		$dsTUang[ 'KKNoView' ] = $_GET[ 'KKNoView' ] ?? $result[ 'KKNo' ];
		return $this->render( 'kas-keluar-umum-update', [
			'model'    => $model,
			'dsTUang'  => $dsTUang,
			'JoinData' => $JoinData,
			'id'       => $id
		] );
	}
	public function actionLoopKasKeluarUmum() {
		$_POST[ 'Action' ] = 'Loop';
		$_POST[ 'KKJam' ]  = $_POST[ 'KKTgl' ] . ' ' . $_POST[ 'KKJam' ];
		$result            = Ttkk::find()->callSP( $_POST );
//		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'status' ] == 1 ) {
			Yii::$app->session->addFlash( 'warning', $result[ 'keterangan' ] );
		}
		$_POST[ 'Action' ] = 'LoopAfter';
		$result            = Ttkk::find()->callSP( $_POST );
		if ( $result[ 'status' ] == 1 ) {
			Yii::$app->session->addFlash( 'warning', $result[ 'keterangan' ] );
		}
	}
	public function actionLoopKasKeluar() {
		if ( $_POST[ 'KKJenis' ] === 'Insentif Sales' ) {
			$_POST[ 'KKPerson' ] = General::cCmd( "Select tdsales.SalesNama FROM tdsales INNER JOIN ttdk ON (tdsales.SalesKode = ttdk.SalesKode) WHERE DKNo = :DKNo", [
				':DKNo' => $_POST[ 'KKLink' ]
			] )->queryScalar();
		}
		if ( $_POST[ 'KKJenis' ] === 'Retur Harga' ) {
			$_POST[ 'KKPerson' ] = General::cCmd( "Select DKMediator FROM ttdk WHERE DKNo = :DKNo", [
				':DKNo' => $_POST[ 'KKLink' ]
			] )->queryScalar();
		}
		$_POST[ 'Action' ] = 'Loop';
		$_POST[ 'KKJam' ]  = $_POST[ 'KKTgl' ] . ' ' . $_POST[ 'KKJam' ];
		$result            = Ttkk::find()->callSP( $_POST );
		if ( $result[ 'status' ] == 1 ) {
			Yii::$app->session->addFlash( 'warning', $result[ 'keterangan' ] );
		}
		$_POST[ 'Action' ] = 'LoopAfter';
		$result            = Ttkk::find()->callSP( $_POST );
		if ( $result[ 'status' ] == 1 ) {
			Yii::$app->session->addFlash( 'warning', $result[ 'keterangan' ] );
		}
	}
	public function actionLoopKasMasukDariBengkel() {
		$sql = "UPDATE HVYS.ttkkit SET KKLink =:KKLink WHERE KKNo = :KKNo";
		$db  = 'b' . base64_decode( $_COOKIE[ '_outlet' ] ) . '.';
		$sql = str_replace( 'HVYS.', $db, $sql );
		General::cCmd( $sql, [ ':KKLink' => $_POST[ 'KMNo' ], ':KKNo' => $_POST[ 'KMLink' ], ] )->execute();
		$_POST[ 'Action' ] = 'Loop';
		$_POST[ 'KKJam' ]  = $_POST[ 'KKTgl' ] . ' ' . $_POST[ 'KKJam' ];
		$result            = Ttkm::find()->callSP( $_POST );
		if ( $result[ 'status' ] == 1 ) {
			Yii::$app->session->addFlash( 'warning', $result[ 'keterangan' ] );
		}
		$_POST[ 'Action' ] = 'LoopAfter';
		$result            = Ttkm::find()->callSP( $_POST );
		if ( $result[ 'status' ] == 1 ) {
			Yii::$app->session->addFlash( 'warning', $result[ 'keterangan' ] );
		}
	}
	/**
	 * Lists all Ttkk models.
	 * @return mixed
	 */
	public function actionKasKeluarBankBengkel() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasKeluarBankBengkel', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkk/td' ] )
		] );
	}
	public function actionKasKeluarBankBengkelCreate() {
		return $this->create( 'KK Ke Bengkel', 'kas-keluar-bank-bengkel', 'KAS KELUAR - Ke Bank Bengkel - H2' );
	}
	public function actionKasKeluarBankBengkelUpdate( $id ) {
		return $this->update( $id, 'kas-keluar-bank-bengkel', 'KAS KELUAR - Ke Bank Bengkel - H2', 'KK Ke Bengkel' );
	}
	public function actionKasKeluarPinjamanKaryawan() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasKeluarPinjamanKaryawan', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkk/td' ] )
		] );
	}
	public function actionKasKeluarPinjamanKaryawanCreate() {
		return $this->create( 'Pinjaman Karyawan', 'kas-keluar-pinjaman-karyawan', 'KAS KELUAR - Pinjaman Karyawan' );
	}
	public function actionKasKeluarPinjamanKaryawanUpdate( $id ) {
		return $this->update( $id, 'kas-keluar-pinjaman-karyawan', 'KAS KELUAR - Pinjaman Karyawan', 'Pinjaman Karyawan' );
	}
	public function actionKasKeluarDealerPos() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasKeluarDealerPos', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkk/td' ] )
		] );
	}
	public function actionKasKeluarDealerPosCreate() {
		return $this->create( 'KK Dealer Ke Pos', 'kas-keluar-dealer-pos', 'KAS KELUAR - Dealer Ke Pos' );
	}
	public function actionKasKeluarDealerPosUpdate( $id ) {
		return $this->update( $id, 'kas-keluar-dealer-pos', 'KAS KELUAR - Dealer Ke Pos', 'KK Dealer Ke Pos' );
	}
	public function actionKasKeluarBayarHutangUnit() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasKeluarBayarHutangUnit', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkk/td' ] )
		] );
	}
	public function actionKasKeluarBayarHutangUnitCreate() {
		return $this->bayar_hutang_create( 'Bayar Unit', 'kas-keluar-bayar-hutang-unit', 'KAS KELUAR - Bayar Hutang Unit' );
	}
	public function actionKasKeluarBayarHutangUnitUpdate( $id ) {
		return $this->bayar_hutang_update( 'Bayar Unit', $id, 'kas-keluar-bayar-hutang-unit', 'KAS KELUAR - Bayar Hutang Unit' );
	}
	public function actionKasKeluarBayarHutangBbn() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasKeluarBayarHutangBbn', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkk/td' ] )
		] );
	}
	public function actionKasKeluarBayarHutangBbnCreate() {
		return $this->bayar_hutang_create( 'Bayar BBN', 'kas-keluar-bayar-hutang-bbn', 'KAS KELUAR - Bayar Hutang BBN' );
	}
	public function actionKasKeluarBayarHutangBbnUpdate( $id ) {
		return $this->bayar_hutang_update( 'Bayar BBN', $id, 'kas-keluar-bayar-hutang-bbn', 'KAS KELUAR - Bayar Hutang BBN' );
	}
	public function actionKasKeluarBayarBbnProgresif() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasKeluarBayarBbnProgresif', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkk/td' ] )
		] );
	}
	public function actionKasKeluarBayarBbnProgresifCreate() {
		return $this->bayar_hutang_create( 'BBN Plus', 'kas-keluar-bayar-bbn-progresif', 'KAS KELUAR - Bayar BBN Progresif' );
	}
	public function actionKasKeluarBayarBbnProgresifUpdate( $id ) {
		return $this->bayar_hutang_update( 'BBN Plus', $id, 'kas-keluar-bayar-bbn-progresif', 'KAS KELUAR - BBN Progresif' );
	}
	public function actionKasKeluarBayarBbnAcc() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasKeluarBayarBbnAcc', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkk/td' ] )
		] );
	}
	public function actionKasKeluarAcc() {
		// if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
		// 	$post[ 'Action' ] = 'Display';
		// 	$result           = Ttkk::find()->callSP( $post );
		//	Yii::$app->session->addFlash( 'success' ,'Terima kasih, 100% sudah final, siap di gunakan....' );
		// }
		$KodeAkses = Yii::$app->session->get('_userProfile')['KodeAkses'];
		$UserID = Yii::$app->session->get('_userProfile')['UserID'];
		$LokasiKode = Yii::$app->session->get('_userProfile')['LokasiKode'];
		$command = General::cCmd( "CALL aiu.fkkwa ('Display',@Status,@Keterangan,'{$UserID}','{$KodeAkses}','{$LokasiKode}','','','','','','','','2023-01-01','2023-12-31','', '','');" );
		$rows = $command->queryAll();
		$exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Keterangan NULL';
		Yii::$app->session->addFlash( ( $status == 0 ) ? 'success' : 'warning', $keterangan );
		return $this->render( 'KasKeluarAcc', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkk/td' ] )
		] );
	}
	public function actionKasKeluarAccIndex() {
		$json      = Json::decode( $_POST[ 'query' ], true );
		$KodeAkses = Yii::$app->session->get('_userProfile')['KodeAkses'];
		$UserID = Yii::$app->session->get('_userProfile')['UserID'];
		$LokasiKode = Yii::$app->session->get('_userProfile')['LokasiKode'];
		if($json['check'] == 'off'){
			$json['cmbTgl1'] = '';
			$json['cmbNum1'] = '';
		}
		$command = General::cCmd( "CALL aiu.fkkwa ('Display',@Status,@Keterangan,'{$UserID}','{$KodeAkses}','{$LokasiKode}','','','{$json['cmbTxt1']}','{$json['txt1']}','{$json['cmbTxt2']}','{$json['txt2']}','{$json['cmbTgl1']}','{$json['tgl1']}','{$json['tgl2']}','{$json['cmbNum1']}', '{$json['cmbNum2']}','{$json['num1']}');" );
		$rows = $command->queryAll();
		foreach ($rows as $key => $value) {
			$rows[$key]['rowId'] =$value['Dealer'].'||'.$value['KKNo'];
		}
		$finalResult = [
			'rows'=>$rows
		];
		return json_encode($finalResult);
	}
	public function actionKasKeluarAccCreate() {
		try {
			$post = Yii::$app->request->post();
			$KodeAkses = Yii::$app->session->get('_userProfile')['KodeAkses'];
			$UserID = Yii::$app->session->get('_userProfile')['UserID'];
			General::cCmd( "CALL aiu.fkkwa ('{$post['Action']}',@Status,@Keterangan,'{$UserID}','{$KodeAkses}','','{$post['KKNo']}','{$post['Dealer']}','','','','','','2022-01-01','2022-12-31','', '','0');" )->execute();
			$exe = General::cCmd( "SELECT @Status,@Keterangan;" )->queryOne();
		} catch ( Exception $e ) {
			return [
				'status'     => 1,
				'keterangan' => $e->getMessage()
			];
		}
		$status     = $exe[ '@Status' ] ?? 1;
		$keterangan = $exe[ '@Keterangan' ] ?? 'Action : [Acc] Keterangan NULL';
		return json_encode([
			'status'     => $status,
			'keterangan' => str_replace( " ", "&nbsp;", $keterangan )
		]);
	}
	public function actionKasKeluarBayarBbnAccCreate() {
		return $this->bayar_hutang_create( 'BBN Acc', 'kas-keluar-bayar-bbn-acc', 'KAS KELUAR - Bayar BBN ACC' );
	}
	public function actionKasKeluarBayarBbnAccUpdate( $id ) {
		return $this->bayar_hutang_update( 'BBN Acc', $id, 'kas-keluar-bayar-bbn-acc', 'KAS KELUAR - Bayar BBN Acc' );
	}
	public function actionKasKeluarPosDealer() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasKeluarPosDealer', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkk/td' ] )
		] );
	}
	public function actionKasKeluarPosDealerCreate() {
		return $this->create( 'KK Pos Ke Dealer', 'kas-keluar-pos-dealer', 'KAS KELUAR - Pos Ke Dealer' );
	}
	public function actionKasKeluarPosDealerUpdate( $id ) {
		return $this->update( $id, 'kas-keluar-pos-dealer', 'KAS KELUAR - Pos Ke Dealer', 'KK Pos Ke Dealer' );
	}
	public function actionKasTransfer() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkk::find()->callSPTransfer( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasTransfer', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkk/td' ] )
		] );
	}
	public function actionKasTransferCreate() {
		TUi::$actionMode           = Menu::ADD;
		$dsTUang[ 'CusNama' ]      = '--';
		$dsTUang[ 'KKNo' ]         = '--';
		$dsTUang[ 'KMNo' ]         = '--';
		$dsTUang[ 'KTNo' ]         = '--';
		$dsTUang[ 'KKTgl' ]        = date( 'Y-m-d H:i:s' );
		$dsTUang[ 'KKJam' ]        = date( 'Y-m-d H:i:s' );
		$dsTUang[ 'KTJam' ]        = date( 'Y-m-d H:i:s' );
		$dsTUang[ 'NoGLKK' ]       = '--';
		$dsTUang[ 'NoGLKM' ]       = '--';
		$dsTUang[ 'KKPerson' ]     = '--';
		$dsTUang[ 'KMPerson' ]     = '--';
		$dsTUang[ 'LokasiKodeKK' ] = ( Tdlokasi::find()->where( "LokasiStatus <> 'N'" )->orderBy( "LokasiStatus,  LokasiNomor" )->one() )->LokasiKode;
		$dsTUang[ 'LokasiKodeKM' ] = ( Tdlokasi::find()->where( "LokasiStatus <> 'N' AND LokasiKode <> :LokasiKode", [ ':LokasiKode' => $dsTUang[ 'LokasiKodeKK' ] ] )->orderBy( "LokasiStatus,  LokasiKode" )->one() )->LokasiKode;
		$dsTUang[ 'KKMemo' ]       = '--';
		$dsTUang[ 'KKJenis' ]      = 'Kas Transfer';
		$dsTUang[ 'KKNominal' ]    = 0;
		$dsTUang[ 'Action' ]       = 'Insert';
		$result                    = Ttkk::find()->callSPTransfer( $dsTUang );
		$dsTUang[ 'KTNo' ]         = $result[ 'KTNo' ];
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->render( 'kas-keluar-transfer-antar-kas-create', [
			'dsTUang' => $dsTUang,
			'id'      => base64_encode( $dsTUang[ 'KTNo' ] ),
		] );
	}
	/**
	 * @param $id
	 * @param $action
	 *
	 * @return string|\yii\web\Response
	 * @throws NotFoundHttpException
	 */
	public function actionKasTransferUpdate( $id ) {
		$index = Ttkk::getRoute( 'Kas Transfer', [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'UpdateAdd':
				TUi::$actionMode = Menu::ADD;
				break;
			case 'UpdateEdit':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		$dsTUang = Ttkk::find()->ttkkTransfer()->andWhere( [ 'KKLink' => base64_decode( $id ) ] )->asArray( true )->one();
		$dsTUang = $dsTUang ?? [];
		if ( Yii::$app->request->isPost ) {
			$post             = array_merge( $dsTUang, \Yii::$app->request->post() );
			$post[ 'KKJam' ]  = $post[ 'KKTgl' ] . ' ' . $post[ 'KKJam' ];
			$post[ 'Action' ] = $action;
			$result           = Ttkk::find()->callSPTransfer( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			if ( $result[ 'status' ] == 0 ) {
				return $this->redirect( Ttkk::getRoute( 'Kas Transfer', [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'kas-keluar-transfer-antar-kas-update', [
					'dsTUang' => $post,
					'id'      => $id,
				] );
			}
		}
		$dsTUang[ 'KTNo' ]   = $dsTUang[ 'KKLink' ];
		$dsTUang[ 'Action' ] = 'Load';
		$result              = Ttkk::find()->callSPTransfer( $dsTUang );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->render( 'kas-keluar-transfer-antar-kas-update', [
			'dsTUang' => $dsTUang,
			'id'      => $id
		] );
	}
	public function actionKasTransferCancel( $id ) {
		$post             = \Yii::$app->request->post();
		$post[ 'KKJam' ]  = $post[ 'KKTgl' ] . ' ' . $post[ 'KKJam' ];
		$post[ 'Action' ] = 'Cancel';
		$result           = Ttkk::find()->callSPTransfer( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'KTNo' ] == '--' ) {
			return $this->redirect( [ 'ttkk/kas-transfer' ] );
		} else {
			$model = Ttkk::findOne( [ 'KKLink' => $result[ 'KTNo' ] ] );
			return $this->redirect( [ 'ttkk/kas-transfer-update', 'action' => 'display', 'id' => base64_encode( $model->KKLink ) ] );
		}
	}
	public function actionKasTransferJurnal() {
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Jurnal';
		$post[ 'KKJam' ]  = $post[ 'KKTgl' ] . ' ' . $post[ 'KKJam' ];
		if ( isset( $_GET[ 'JenisBBN' ] ) && $_GET[ 'JenisBBN' ] == 'Pengajuan' ) {
			$result = Ttkk::find()->callSPPengajuan( $post );
		} else {
			$result = Ttkk::find()->callSP( $post );
		}
		$post[ 'KKLink' ] = $post[ 'KTNo' ];
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( Ttkk::getRoute( $post[ 'KKJenis' ], [ 'action' => 'display', 'id' => base64_encode( $post[ 'KKLink' ] ) ] )[ 'update' ] );
	}
	/**
	 * Lists all Ttkk models.
	 * @return mixed
	 */
	public function actionKwitansiKasBayarBbn() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KwitansiKasBayarBbn', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkk/td' ] )
		] );
	}
	/**
	 * Displays a single Ttkk model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Finds the Ttkk model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttkk the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttkk::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Deletes an existing Ttkk model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete() {
		/** @var Ttkk $model */
		$request = Yii::$app->request;
		$PK         = base64_decode( $_REQUEST[ 'id' ] );
		$model      = null;
		$isTransfer = false;
		if ( strpos( $PK, 'KT' ) !== false ) {
			$model      = Ttkk::findOne( [ 'KKLink' => $PK ] );
			$isTransfer = true;
		} else {
			$model = Ttkk::findOne( $PK );
		}
		$_POST[ 'KKNo' ]   = $model->KKNo;
		$_POST[ 'Action' ] = 'Delete';
		$KKJenis = $model->KKJenis;
		if ( isset( $_GET[ 'JenisBBN' ] ) && $_GET[ 'JenisBBN' ] == 'Pengajuan' ) {
			$result = Ttkk::find()->callSPPengajuan( $_POST );
		} else {
			if ( $isTransfer ) {
				$result = Ttkk::find()->callSPTransfer( $_POST );
			} else {
				$result = Ttkk::find()->callSP( $_POST );
			}
		}
		if ($request->isAjax){
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			if ( $result[ 'KKNo' ] == '--' ) {
				if ( isset( $_GET[ 'JenisBBN' ] ) && $_GET[ 'JenisBBN' ] == 'Pengajuan' ) {
					return $this->redirect( [ 'ttkk/pengajuan-bayar-bbn-kas','JenisBBN'=>'Pengajuan' ] );
				}
				return $this->redirect( Ttkk::getRoute( $KKJenis )[ 'index' ] );
			} else {
				$model = Ttkk::findOne( $result[ 'KKNo' ] );
				if(empty($model)){
					if ( isset( $_GET[ 'JenisBBN' ] ) && $_GET[ 'JenisBBN' ] == 'Pengajuan' ) {
						$model = Ttkk::find()
						->where(['KKJenis' => 'Bayar BBN'])
						->andWhere(['like', 'KKNo','KP%',false])
						->andWhere(['not like', 'KKNo','=',])
						->one();
					}else{
						$model = Ttkk::find()->where(['KKJenis' => $KKJenis])->andWhere(['not like', 'KKNo','=',])->one();
					}
				}
				if(empty($model)){
					if ( isset( $_GET[ 'JenisBBN' ] ) && $_GET[ 'JenisBBN' ] == 'Pengajuan' ) {
						return $this->redirect( [ 'ttkk/pengajuan-bayar-bbn-kas','JenisBBN'=>'Pengajuan' ] );
					}
					return $this->redirect( Ttkk::getRoute( $KKJenis )[ 'index' ] );
				}
				if ( isset( $_GET[ 'JenisBBN' ] ) && $_GET[ 'JenisBBN' ] == 'Pengajuan' ) {
					return $this->redirect( [ 'ttkk/pengajuan-bayar-bbn-kas-update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ),'JenisBBN'=>'Pengajuan' ] );
				}
				return $this->redirect( Ttkk::getRoute( $KKJenis, [ 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] )[ 'update' ] );
			}
		}
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client            = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response          = $client->post( Yii::$app->params[ 'host_report' ] . '/aunit/fkk', [
			'body' => Json::encode( $_POST )
		] );
		return Yii::$app->response->sendContentAsFile( $response->getBody(), 'fkk.pdf', [
			'inline'   => true,
			'mimeType' => 'application/pdf'
		] );
	}
	public function actionKonfirmPengajuan( $id ) {
		/** @var  $model Ttkk */
		$model            = $this->findModelBase64( 'Ttkk', $id );
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Konfirm';
		$post[ 'KKJam' ]  = $model->KKJam;
		$result           = Ttkk::find()->callSPPengajuan( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( [ 'ttkk/pengajuan-bayar-bbn-kas-update', 'action' => 'display', 'id' => base64_encode( $result[ 'KKNo' ] ) ] );
	}
	public function actionCancel( $id, $action ) {
		/** @var  $model Ttkk */
		$model            = $this->findModelBase64( 'Ttkk', $id );
		$KKJenis          = $model->KKJenis;
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Cancel';
		$post[ 'KKJam' ]  = $model->KKJam;
		if ( isset( $_GET[ 'JenisBBN' ] ) && $_GET[ 'JenisBBN' ] == 'Pengajuan' ) {
			$result = Ttkk::find()->callSPPengajuan( $post );
		} else {
			$result = Ttkk::find()->callSP( $post );
		}
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'KKNo' ] == '--' ) {
			if ( isset( $_GET[ 'JenisBBN' ] ) && $_GET[ 'JenisBBN' ] == 'Pengajuan' ) {
				return $this->redirect( [ 'ttkk/pengajuan-bayar-bbn-kas','JenisBBN'=>'Pengajuan' ] );
			}
			return $this->redirect( Ttkk::getRoute( $KKJenis )[ 'index' ] );
		} else {
			$model = Ttkk::findOne( $result[ 'KKNo' ] );
			if ( isset( $_GET[ 'JenisBBN' ] ) && $_GET[ 'JenisBBN' ] == 'Pengajuan' ) {
				return $this->redirect( [ 'ttkk/pengajuan-bayar-bbn-kas-update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ),'JenisBBN'=>'Pengajuan' ] );
			}
			return $this->redirect( Ttkk::getRoute( $KKJenis, [ 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] )[ 'update' ] );
		}
	}
	public function actionJurnal() {
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Jurnal';
		$post[ 'KKJam' ]  = $post[ 'KKTgl' ] . ' ' . $post[ 'KKJam' ];
		if ( isset( $_GET[ 'JenisBBN' ] ) && $_GET[ 'JenisBBN' ] == 'Pengajuan' ) {
			$result = Ttkk::find()->callSPPengajuan( $post );
		} else {
			$result = Ttkk::find()->callSP( $post );
		}
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( isset( $_GET[ 'JenisBBN' ] ) && $_GET[ 'JenisBBN' ] == 'Pengajuan' ) {
			return $this->redirect( [ 'ttkk/pengajuan-bayar-bbn-kas-update', 'action' => 'display', 'id' => base64_encode( $result[ 'KKNo' ] ) ] );
		}
		return $this->redirect( Ttkk::getRoute( $post[ 'KKJenis' ], [ 'action' => 'display', 'id' => base64_encode( $result[ 'KKNo' ] ) ] )[ 'update' ] );
	}

	public function actionLampiran($id){
		/** @var  $model Ttkk */
		$model = $this->findModelBase64( 'Ttkk', $id );
		$attach = new Attachment();
		$attach->table = 'ttkk';
		$attach->tableId = $model->KKNo;
		$attach->titleId = $model->KKNo;
		$attach->model = $model;
		$attach->redirectUrl = Ttkk::getRoute( $model->KKJenis, [ 'action' => 'display', 'id' => $id ])[ 'update' ];
		return $this->redirect($attach->getRedirectUrl());
	}
}
