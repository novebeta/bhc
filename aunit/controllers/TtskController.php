<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Attachment;
use aunit\models\Tmotor;
use aunit\models\Ttsk;
use aunit\models\Ttsmhd;
use common\components\Api;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
/**
 * TtskController implements the CRUD actions for Ttsk model.
 */
class TtskController extends AunitController {
	public function actions() {
		$sql   = "";
		$where = [];
		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			switch ( $r ) {
				case 'ttsk/index':
					$sql = "SELECT ttsk.SKDoc,ttsk.SKJam, ttsk.SKCetak, ttsk.SKPengirim, ttsk.SKNo, ttsk.SKTgl, ttsk.LokasiKode, ttsk.SKMemo, ttsk.UserID, ttdk.CusKode, tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, tdcustomer.CusTelepon, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, ttdk.DKNo, ttdk.DKHarga, tmotor.RKNo, ttsk.NoGL, tdmotortype.MotorNama
				         FROM ttsk 
				         INNER JOIN tmotor ON ttsk.SKNo = tmotor.SKNo
				         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
						 INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType
				         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
				         UNION ALL
				         SELECT ttsk.SKDoc,ttsk.SKJam, ttsk.SKCetak, ttsk.SKPengirim, ttsk.SKNo, ttsk.SKTgl, ttsk.LokasiKode, ttsk.SKMemo, ttsk.UserID, ttdk.CusKode, tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, tdcustomer.CusTelepon, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, ttdk.DKNo, ttdk.DKHarga, tmotor.RKNo, ttsk.NoGL, tdmotortype.MotorNama
				         FROM ttsk 
				         INNER JOIN ttrk ON ttsk.SKNo = ttrk.SKNo 
				         INNER JOIN tmotor ON ttrk.RKNo = tmotor.RKNo 
						 INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType
				         INNER JOIN ttdk ON ttrk.DKNo = ttdk.DKNo 
				         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
				         WHERE (tmotor.SSNo <> '--' OR tmotor.FBNo <> '--' OR tmotor.PDNo <>'--') 
		         ";
					break;
				case 'ttsk/select':
					$SKNo = $urlParams[ 'SKNo' ] ?? '--';
					$sql  = "SELECT ttsk.*, tdcustomer.CusNama, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun,
					tmotor.MotorNoMesin, tmotor.MotorNoRangka, tdmotortype.MotorNama
					FROM ttsk 
					INNER JOIN tmotor ON ttsk.SKNo = tmotor.SKNo
					INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
					INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType
					INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
					LEFT OUTER JOIN ttrk ON ttsk.SKNo = ttrk.SKNo
					WHERE (ttrk.RKNo is NUll OR ttsk.SKNo = '$SKNo')";
					break;
				case 'ttsk/serah-terima-kolektif':
					$sql = "SELECT ttsk.SKNo, ttsk.SKTgl, ttdk.DKNo, ttdk.DKTgl, ttdk.LeaseKode, tdcustomer.CusNama, tdsales.SalesNama, 
			         tdmotortype.MotorNama, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorAutoN, tmotor.MotorNoMesin, FakturAHMNilai,
			         tmotor.FakturAHMNo, tmotor.STNKTglBayar, tmotor.FakturAHMTgl, tmotor.FakturAHMTglAmbil, 
			         tmotor.STNKNo, tmotor.STNKTglMasuk, tmotor.STNKTglJadi, tmotor.STNKTglAmbil, tmotor.STNKNama, tmotor.STNKAlamat, tmotor.STNKPenerima, 
			         tmotor.NoticeNo, tmotor.NoticeTglJadi, tmotor.NoticeTglAmbil, tmotor.NoticePenerima, tmotor.PlatNo, tmotor.PlatTgl, tmotor.PlatTglAmbil, tmotor.PlatPenerima, 
			         tmotor.BPKBNo, tmotor.BPKBTglJadi, tmotor.BPKBTglAmbil, tmotor.BPKBPenerima,
			         tmotor.SRUTNo, tmotor.SRUTTglJadi, tmotor.SRUTTglAmbil, tmotor.SRUTPenerima
			         FROM  ttsk 
			         INNER JOIN tmotor ON ttsk.SKNo = tmotor.SKNo 
			         INNER JOIN ttdk ON ttdk.DKNo = tmotor.DKNo 
			         INNER JOIN tdsales ON tdsales.SalesKode = ttdk.SalesKode 
			         INNER JOIN tdlokasi ON ttsk.LokasiKode = tdlokasi.LokasiKode 
			         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
			         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode";
					break;
			}
		}
		return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Ttsk::class,
				'queryRaw'   => $sql,
				'queryRawPK' => [ 'SKNo' ]
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttsk models.
	 * @return mixed
	 */
	public function actionIndex() {
		if ($_SERVER['HTTP_ACCEPT'] == 'text/event-stream'){
			if(connection_aborted()) exit();
			session_write_close();
			if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
				@ob_end_clean();
				ini_set('output_buffering', 0);
				header('Content-Type: text/event-stream');
				header('Cache-Control: no-cache');
				$post[ 'Action' ] = 'Display';
				echo "data:".json_encode(Ttsk::find()->callSP( $post ))."\n\n";
				flush();
				sleep(1);
			}
		}else{
			return $this->render( 'index', [
				'mode' => ''
			] );
		}
	}
	/**
	 * Lists all Ttsk models.
	 * @return mixed
	 */
	public function actionSelect() {
		$this->layout = 'select-mode';
		return $this->render( 'index', [
			'mode' => self::MODE_SELECT
		] );
	}

    public function actionImport()
    {
        $this->layout = 'select-mode';
        return $this->render('import', [
            'colGrid' => Ttsk::colGridImportItems(),
            'mode' => self::MODE_SELECT_DETAIL_MULTISELECT,
        ]);
    }

    public function actionImportItems()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $fromTime = Yii::$app->formatter->asDate('-90 day', 'yyyy-MM-dd');
        $toTime = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
		$addParam = [
			'DeliveryDocumentId' => '',
			'idSPK' => '',
			'idCustomer' => '',
		];
        if (isset($_POST['query'])) {
            $query = Json::decode($_POST['query'], true);
            $fromTime = $query['tgl1'] ?? $fromTime;
            $toTime = $query['tgl2'] ?? $toTime;
			if($query['txt1'] !== ''){
				$addParam = array_merge_recursive($addParam,[$query['cmbTxt1']=> $query['txt1']]);
			}
        }
        $api = new Api();
        $mainDealer = $api->getMainDealer();
        $arrHasil = $arrParams = [];
        switch ($mainDealer) {
            case API::MD_HSO:
                $arrParams = [
                    'fromTime' => $fromTime . ' 00:00:00',
                    'toTime' => $toTime . ' 23:59:59',
                    'dealerId' => $api->getDealerId(),
                ];
                break;
            case API::MD_ANPER:
                $arrParams = [
                    'fromTime' => $fromTime . ' 00:00:00',
                    'toTime' => $toTime . ' 23:59:59',
                    'dealerId' => $api->getDealerId(),
                    'requestServerTime' => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                    'requestServertimezone' => date('T'),
                    'requestServerTimestamp' => $api->getCurrUnixTime(),
                ];
                break;
			default:
				$arrParams = [
					'fromTime'               => $fromTime . ' 00:00:00',
					'toTime'                 => $toTime . ' 23:59:59',
					'dealerId'               => $api->getDealerId(),
					'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
					'requestServertimezone'  => date('T'),
					'requestServerTimestamp' => $api->getCurrUnixTime(),
				];
        }
		$arrParams = array_merge_recursive($arrParams,$addParam);
		$filterResult = [];
		if($query['txt2'] !== ''){
			$filterResult = [
				'key' => $query['cmbTxt2'],
				'value' => $query['txt2'],
			];
		}
        $hasil = $api->call(Api::TYPE_BAST, Json::encode($arrParams),$filterResult);
        $arrHasil = Json::decode($hasil);
        if (!isset($arrHasil['data'])) {
            return [
                'rows' => [],
                'rowsCount' => 0
            ];
        }
        $_SESSION['ttsk']['import'] = $arrHasil['data'];
        return [
            'rows' => $arrHasil['data'],
            'rowsCount' => sizeof($arrHasil['data'])
        ];
    }

    public function actionImportDetails()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $key = array_search($_POST['deliveryDocumentId'], array_column($_SESSION['ttsk']['import'], 'deliveryDocumentId'));
        return [
            'rows' => $_SESSION['ttsk']['import'][$key]['detail'],
            'rowsCount' => sizeof($_SESSION['ttsk']['import'][$key]['detail'])
        ];
    }

	/**
	 * Lists all Ttsk models.
	 * @return mixed
	 */
	public function actionMutasiOtomatis() {
		$this->layout       = 'popup-form';
		$SKNo               = $_GET[ 'SKNo' ];
		$data[ 'ada' ]      = false;
		$data[ 'MONo' ]     = str_replace( 'SK', 'MO', $SKNo );
		$data[ 'MOTgl' ]    = date( 'Y-m-d' );
		$data[ 'MOJam' ]    = date( "Y-m-d H:i:s" );
		$SK                 = Ttsk::findOne( $SKNo );
		$data[ 'MOAsal' ]   = $SK->LokasiKode;
		$data[ 'MOTujuan' ] = $SK->LokasiKode;
		$MO                 = ttsmhd::findOne( $data[ 'MONo' ] );
		if ( $MO != null ) {
			$data[ 'MOTujuan' ] = $MO->LokasiAsal;
			$data[ 'MOTujuan' ] = $MO->LokasiTujuan;
			$data[ 'ada' ]      = true;
		}
		return $this->renderAjax( '_formMutasi', [ 'data' => $data ] );
	}
	public function actionLoopDk() {
		$_POST[ 'Action' ] = 'LoopDK';
		$_POST[ 'SKJam' ]  = $_POST[ 'SKTgl' ] . ' ' . $_POST[ 'SKJam' ];
		$result            = Ttsk::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
	}
	public function actionCreateMo( $SKId, $SKNoView ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$post                        = \Yii::$app->request->post();
		$post[ 'Action' ]            = 'Update';
		return Ttsk::find()->callSP_MO( $post );
	}
	public function actionCekMo( $SKNo, $MONo ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$SK                          = Ttsk::findOne( $SKNo );
		$data[ 'MOAsal' ]            = $SK->LokasiKode;
		$data[ 'MOTujuan' ]          = $SK->LokasiKode;
		$data[ 'MOTgl' ]             = $SK->SKTgl;
		$data[ 'MOJam' ]             = $SK->SKJam;
		$data[ 'ada' ]               = false;
		$MO                          = ttsmhd::findOne( $MONo );
		if ( $MO != null ) {
			$data[ 'MOAsal' ]   = $MO->LokasiAsal;
			$data[ 'MOTujuan' ] = $MO->LokasiTujuan;
			$data[ 'MOTgl' ]    = $MO->SMTgl;
			$data[ 'MOJam' ]    = $MO->SMJam;
			$data[ 'ada' ]      = true;
		}
		return $data;
	}
	public function actionDeleteMo( $SKId, $SKNoView ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$post                        = \Yii::$app->request->post();
		$post[ 'Action' ]            = 'Delete';
		return Ttsk::find()->callSP_MO( $post );
	}
	/**
	 * Lists all Ttsk models.
	 * @return mixed
	 */
	public function actionSerahTerimaKolektif() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttsk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'serah-terima-kolektif' );
	}
	public function actionSerahTerimaKolektifUpdate() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		foreach ( $requestData[ 'ids' ] as $record ) {
			/** @var Tmotor $model */
//			$model = Tmotor::findOne( [ 'MotorAutoN' => $record[ 'MotorAutoN' ], 'MotorNoMesin' => $record[ 'MotorNoMesin' ] ] );
			$model = Tmotor::findOne( [ 'MotorNoMesin' => $record[ 'MotorNoMesin' ] ] );
			$model->setAttribute( $requestData[ 'tujuan' ], $requestData[ 'tgl' ] );
			switch ( $requestData[ 'tujuan' ] ) {
				case 'STNKTglAmbil':
					$model->setAttribute( 'STNKPenerima', $requestData[ 'penerima' ] );
					break;
				case 'NoticeTglAmbil':
					$model->setAttribute( 'NoticePenerima', $requestData[ 'penerima' ] );
					break;
				case 'PlatTglAmbil':
					$model->setAttribute( 'PlatPenerima', $requestData[ 'penerima' ] );
					break;
				case 'BPKBTglAmbil':
					$model->setAttribute( 'BPKBPenerima', $requestData[ 'penerima' ] );
					break;
				case 'FakturAHMTglAmbil':
				case 'FakturAHMTgl':
				case 'STNKTglJadi':
				case 'NoticeTglJadi':
				case 'PlatTgl':
				case 'BPKBTglJadi':
				case 'STNKTglBayar':
				case 'STNKTglMasuk':
				case 'SRUTTglJadi':
				case 'SRUTTglAmbil':
					$model->setAttribute( 'SRUTPenerima', $requestData[ 'penerima' ] );
					break;
			}
			$model->save();
		}
		return [
			'status'     => 0,
			'keterangan' => 'Berhasil menyimpan data'
		];
	}
	public function actionSerahTerimaKolektifUpdateLine() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		switch ( $requestData[ 'TyTglDoc' ] ) {
			case 'STNKTglAmbil':
				Tmotor::updateAll( [ 'STNKPenerima' => $requestData[ 'gcsDocPenerima' ], 'STNKNo' => $requestData[ 'gcsDocNo' ] ],
					[ 'MotorNoMesin' => $requestData[ 'MotorNoMesin' ], 'MotorAutoN' => $requestData[ 'MotorAutoN' ] ] );
				break;
			case 'NoticeTglAmbil':
				Tmotor::updateAll( [ 'NoticePenerima' => $requestData[ 'gcsDocPenerima' ], 'NoticeNo' => $requestData[ 'gcsDocNo' ] ],
					[ 'MotorNoMesin' => $requestData[ 'MotorNoMesin' ], 'MotorAutoN' => $requestData[ 'MotorAutoN' ] ] );
				break;
			case 'PlatTglAmbil':
				Tmotor::updateAll( [ 'PlatPenerima' => $requestData[ 'gcsDocPenerima' ], 'PlatNo' => $requestData[ 'gcsDocNo' ] ],
					[ 'MotorNoMesin' => $requestData[ 'MotorNoMesin' ], 'MotorAutoN' => $requestData[ 'MotorAutoN' ] ] );
				break;
			case 'BPKBTglAmbil':
				Tmotor::updateAll( [ 'BPKBPenerima' => $requestData[ 'gcsDocPenerima' ], 'BPKBNo' => $requestData[ 'gcsDocNo' ] ],
					[ 'MotorNoMesin' => $requestData[ 'MotorNoMesin' ], 'MotorAutoN' => $requestData[ 'MotorAutoN' ] ] );
				break;
			case 'FakturAHMTglAmbil':
				Tmotor::updateAll( [ 'FakturAHMNo' => $requestData[ 'gcsDocNo' ] ],
					[ 'MotorNoMesin' => $requestData[ 'MotorNoMesin' ], 'MotorAutoN' => $requestData[ 'MotorAutoN' ] ] );
				break;
			case 'FakturAHMTgl':
                Tmotor::updateAll( [ 'FakturAHMNo' => $requestData[ 'gcsDocNo' ] ],
                    [ 'MotorNoMesin' => $requestData[ 'MotorNoMesin' ], 'MotorAutoN' => $requestData[ 'MotorAutoN' ] ] );
                break;
			case 'STNKTglJadi':
				Tmotor::updateAll( [ 'STNKNo' => $requestData[ 'gcsDocNo' ] ],
					[ 'MotorNoMesin' => $requestData[ 'MotorNoMesin' ], 'MotorAutoN' => $requestData[ 'MotorAutoN' ] ] );
				break;
			case 'NoticeTglJadi':
				Tmotor::updateAll( [ 'NoticeNo' => $requestData[ 'gcsDocNo' ], 'NoticeNilai' => $requestData[ 'gcsDocSales' ] ],
					[ 'MotorNoMesin' => $requestData[ 'MotorNoMesin' ], 'MotorAutoN' => $requestData[ 'MotorAutoN' ] ] );
				break;
			case 'PlatTgl':
				Tmotor::updateAll( [ 'PlatNo' => $requestData[ 'gcsDocNo' ] ],
					[ 'MotorNoMesin' => $requestData[ 'MotorNoMesin' ], 'MotorAutoN' => $requestData[ 'MotorAutoN' ] ] );
				break;
			case 'BPKBTglJadi':
				Tmotor::updateAll( [ 'BPKBNo' => $requestData[ 'gcsDocNo' ] ],
					[ 'MotorNoMesin' => $requestData[ 'MotorNoMesin' ], 'MotorAutoN' => $requestData[ 'MotorAutoN' ] ] );
				break;
			case 'STNKTglBayar':
                Tmotor::updateAll( [ 'FakturAHMNo' => $requestData[ 'gcsDocNo' ], 'NoticeNilai' => $requestData[ 'gcsDocSales' ] ],
                    [ 'MotorNoMesin' => $requestData[ 'MotorNoMesin' ], 'MotorAutoN' => $requestData[ 'MotorAutoN' ] ] );
                break;
			case 'STNKTglMasuk':
				break;
			case 'SRUTTglJadi':
				Tmotor::updateAll( [ 'SRUTNo' => $requestData[ 'gcsDocNo' ] ],
					[ 'MotorNoMesin' => $requestData[ 'MotorNoMesin' ], 'MotorAutoN' => $requestData[ 'MotorAutoN' ] ] );
				break;
			case 'SRUTTglAmbil':
				Tmotor::updateAll( [ 'SRUTPenerima' => $requestData[ 'gcsDocPenerima' ], 'SRUTNo' => $requestData[ 'gcsDocNo' ] ],
					[ 'MotorNoMesin' => $requestData[ 'MotorNoMesin' ], 'MotorAutoN' => $requestData[ 'MotorAutoN' ] ] );
				break;
		}
		return [
			'status'     => 0,
			'keterangan' => 'Berhasil menyimpan data'
		];
	}
	/**
	 * Finds the Ttsk model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttsk the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttsk::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttsk model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		TUi::$actionMode   = Menu::ADD;
		$model             = new Ttsk();
		$model->SKNo       = General::createTemporaryNo( 'SK', 'ttsk', 'SKNo' );
		$model->SKTgl      = date( 'Y-m-d' );
		$model->SKJam      = date( "Y-m-d H:i:s" );
		$model->SKCetak    = "Belum";
		$model->LokasiKode = $this->LokasiKode();
		$model->DKNo       = "--";
		$model->SKPengirim = "--";
		$model->NoGL       = "--";
		$model->SKMemo     = "--";
		$model->SKPengirim = "--";
		$model->UserID     = $this->UserID();
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttsk::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsJual                   = $model::find()->dsTJualFillByNo()->where( [ 'ttsk.SKNo' => $model->SKNo ] )->asArray()->one();
		$dsJual[ 'SKNoView' ]     = $result[ 'SKNo' ];
		$dsJual[ 'NoBukuServis' ] = "--";
		$id                       = $this->generateIdBase64FromModel( $model );
		return $this->render( 'create', [
			'model'  => $model,
			'dsJual' => $dsJual,
			'id'     => $id,
		] );
	}
	/**
	 * Updates an existing Ttsk model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$index = \yii\helpers\Url::toRoute( [ 'ttsk/index' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttsk $model */
		$model  = $this->findModelBase64( 'Ttsk', $id );
		$dsJual = $model::find()->dsTJualFillByNo()->where( [ 'ttsk.SKNo' => $model->SKNo ] )->asArray()->one();
		if ( Yii::$app->request->isPost ) {
			$post                   = array_merge( $dsJual, \Yii::$app->request->post() );
			$post[ 'LokasiKodeSK' ] = $post[ 'LokasiKode' ];
			$post[ 'SKJam' ]        = $post[ 'SKTgl' ] . ' ' . $post[ 'SKJam' ];
			$post[ 'Action' ]       = 'Update';
			$result                 = Ttsk::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            $model                  = Ttsk::findOne( $result[ 'SKNo' ] );
            if ( $result[ 'SKNo' ] == '--' || $model == null) {
                if ( $model === null ) {
                    Yii::$app->session->addFlash( 'warning', 'SKNo : ' . $result[ 'SKNo' ]. ' tidak ditemukan.' );
                }
                return $this->redirect( $index);
            }
            $id    = $this->generateIdBase64FromModel( $model );
            $post[ 'SKNoView' ] = $result[ 'SKNo' ];
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				return $this->redirect( [ 'ttsk/update', 'id' => $id, 'action' => 'display' ] );
			} else {
				return $this->render( 'update', [
					'model'  => $model,
					'dsJual' => $post,
					'id'     => $id,
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsJual[ 'Action' ] = 'Load';
			$result             = Ttsk::find()->callSP( $dsJual );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'SKNo' ] = $dsJual[ 'SKNo' ];
		}
		$dsJual[ 'SKNoView' ] = $_GET[ 'SKNoView' ] ?? $result[ 'SKNo' ];
		return $this->render( 'update', [
			'model'  => $model,
			'dsJual' => $dsJual,
			'id'     => $id,
		] );
	}

	public function actionImportLoop(){
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$respon = [];
		$base64 = urldecode(base64_decode($requestData['header']));
		parse_str($base64, $header);
		// $header[ 'FBNo' ]  = $header[ 'FBNoLama' ];
		// $header[ 'FBJam' ] = $header[ 'FBTgl' ] . ' ' . $header[ 'FBJam' ];              
		$requestData['data'][0]['header']['data']['Action'] = 'ImporBefore';
		$requestData['data'][0]['header']['data']['NoTrans'] = $header['SKNo'];
		$result = Api::callBAST($requestData['data'][0]['header']['data']);
		// $isSameID = $result['SSNo'] == $header['SSNo'];
		Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		$model = Ttsk::findOne($result['NoTrans']);
		$id = $this->generateIdBase64FromModel($model);
		$respon['id'] = $id;
		foreach ($requestData['data'] as $item) {
			$item['data']['Action'] = 'Impor';
			$item['data']['NoTrans'] = $model->SKNo;
			$result = Api::callBASTit($item['data']);
			Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		}
		$requestData['data'][0]['header']['data']['Action'] = 'ImporAfter';
		$result = Api::callBAST($requestData['data'][0]['header']['data']);
		Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		return $respon;
	}

	public function actionDetails() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$query                       = new Query();
		$primaryKeys                 = [ 'ttsk.SKNo', 'ttdk.DKNo' ];
		$query->select( new Expression( 'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
                            ttsk.SKNo, ttsk.SKTgl, ttdk.DKNo, ttdk.DKTgl, ttdk.LeaseKode, tdcustomer.CusNama, tdsales.SalesNama, 
					         tdmotortype.MotorNama, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorAutoN, tmotor.MotorNoMesin, FakturAHMNilai,
					         tmotor.FakturAHMNo, tmotor.STNKTglBayar, tmotor.FakturAHMTgl, tmotor.FakturAHMTglAmbil, 
					         tmotor.STNKNo, tmotor.STNKTglMasuk, tmotor.STNKTglJadi, tmotor.STNKTglAmbil, tmotor.STNKNama, tmotor.STNKAlamat, tmotor.STNKPenerima, 
					         tmotor.NoticeNo,tmotor.NoticeNilai, tmotor.NoticeTglJadi, tmotor.NoticeTglAmbil, tmotor.NoticePenerima, tmotor.PlatNo, tmotor.PlatTgl, tmotor.PlatTglAmbil, tmotor.PlatPenerima, 
					         tmotor.BPKBNo, tmotor.BPKBTglJadi, tmotor.BPKBTglAmbil, tmotor.BPKBPenerima,
					         tmotor.SRUTNo, tmotor.SRUTTglJadi, tmotor.SRUTTglAmbil, tmotor.SRUTPenerima' ) )
		      ->from( 'ttsk' )
		      ->innerJoin( "tmotor", "ttsk.SKNo = tmotor.SKNo" )
		      ->innerJoin( "ttdk", "ttdk.DKNo = tmotor.DKNo" )
		      ->innerJoin( "tdsales", "tdsales.SalesKode = ttdk.SalesKode" )
		      ->innerJoin( "tdlokasi", "ttsk.LokasiKode = tdlokasi.LokasiKode" )
		      ->innerJoin( "tdmotortype", "tmotor.MotorType = tdmotortype.MotorType" )
		      ->innerJoin( "tdcustomer", "ttdk.CusKode = tdcustomer.CusKode" )
              ->where( "FakturAHMNo  NOT LIKE 'RK%'" )
		      ->andWhere( [ 'between', $requestData[ 'cboDKSKTgl' ], $requestData[ 'dtpDKSKTgl1' ], $requestData[ 'dtpDKSKTgl2' ] ] )
		      ->andWhere( [ 'between', $requestData[ 'cboDKSKTgl' ], $requestData[ 'dtpDKSKTgl1' ], $requestData[ 'dtpDKSKTgl2' ] ] )
		      ->andWhere( [ 'between', $requestData[ 'TyTglDoc' ], $requestData[ 'dtpDoc1' ], $requestData[ 'dtpDoc2' ] ] )
		      ->andWhere( [ 'like', 'ttdk.SalesKode', '%' . $requestData[ 'SalesKode' ], false ] )
		      ->andWhere( [ 'like', 'ttdk.LeaseKode', '%' . $requestData[ 'LeaseKode' ], false ] )
		      ->orderBy( $requestData[ 'Sort' ] . ' ' . $requestData[ 'TypeSort' ] );
		$rowsCount = $query->count();
		$rows      = $query->all();
		/* encode row id */
		for ( $i = 0; $i < count( $rows ); $i ++ ) {
			$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
		}
		$response = [
			'rows'      => $rows,
			'rowsCount' => $rowsCount
		];
		return $response;
	}
	/**
	 * Deletes an existing Ttsk model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionCancel( $id, $action ) {
		/** @var Ttsk $model */
		$_POST[ 'SKJam' ]  = $_POST[ 'SKTgl' ] . ' ' . $_POST[ 'SKJam' ];
		$_POST[ 'Action' ] = 'Cancel';
		$result            = Ttsk::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        $model             = Ttsk::findOne( $result[ 'SKNo' ] );
        if ( $result[ 'SKNo' ] == '--' || $model == null) {
            if ( $model === null ) {
                Yii::$app->session->addFlash( 'warning', 'SKNo : ' . base64_decode( $id ) . ' tidak ditemukan.' );
            }
            $index = \yii\helpers\Url::toRoute( [ 'ttsk/index' ] );
            return $this->redirect( $index );
		} else {
			return $this->redirect( [ 'ttsk/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}
	public function actionJurnal() {
		$_POST[ 'SKJam' ]  = $_POST[ 'SKTgl' ] . ' ' . $_POST[ 'SKJam' ];
		$_POST[ 'Action' ] = 'Jurnal';
		$result            = Ttsk::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( [ 'ttsk/update', 'action' => 'display', 'id' => base64_encode( $result[ 'SKNo' ] ) ] );
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client            = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$endpoint = '/aunit/fsk';
		if($_POST['StatPrint'] == '3'){
			$endpoint = '/aunit/fskkw';
			$response          = $client->post( Yii::$app->params[ 'host_report' ] . $endpoint, [
				'body' => Json::encode( $_POST )
			] );
			return Yii::$app->response->sendContentAsFile( $response->getBody(), str_replace('SK','FK',$_POST['SKNo']).'.pdf', [
					'inline'   => true,
					'mimeType' => 'application/pdf'
				]
			);
		}
		$response          = $client->post( Yii::$app->params[ 'host_report' ] . $endpoint, [
			'body' => Json::encode( $_POST )
		] );
		$html              = $response->getBody();
		return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	public function actionPrintKolektif( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client            = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response          = $client->post( Yii::$app->params[ 'host_report' ] . '/aunit/fskkolektif', [
			'body' => Json::encode( $_POST )
		] );
		return Yii::$app->response->sendContentAsFile( $response->getBody(), 'fskkolektif.pdf', [
				'inline'   => true,
				'mimeType' => 'application/pdf'
			]
		);
	}
	public function actionCekPrintKwitansi() {
		$result            = Ttsk::find()->callSP_PrintKwitansi( $_POST );
		if ( $result[ 'status' ] == 0 ) {
			return $this->responseSuccess( $result[ 'keterangan' ] );
		} else {
			return $this->responseFailed( $result[ 'keterangan' ] );
		}
	}

    public function actionDelete()
    {
		return $this->deleteWithSP(['modelName'=>'Ttsk']);
        /** @var Ttsk $model */
        // $request = Yii::$app->request;
        // if ($request->isAjax){
        //     $model = $this->findModelBase64('Ttsk', $_POST['id']);
        //     $_POST['SKNo'] = $model->SKNo;
        //     $_POST['Action'] = 'Delete';
        //     $result = Ttsk::find()->callSP($_POST);
        //     if ($result['status'] == 0) {
        //         return $this->responseSuccess($result['keterangan']);
        //     } else {
        //         return $this->responseFailed($result['keterangan']);
        //     }
        // }else{
        //     $id = $request->get('id');
        //     $model = $this->findModelBase64('Ttsk', $id);
        //     $_POST['SKNo'] = $model->SKNo;
        //     $_POST['Action'] = 'Delete';
        //     $result = Ttsk::find()->callSP($_POST);
        //     Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
        //     if ($result['SKNo'] == '--') {
        //         return $this->redirect(['index']);
        //     } else {
        //         $model = Ttsk::findOne($result['SKNo']);
        //         return $this->redirect(['ttsk/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
        //     }
        // }
    }

	public function actionLampiran($id){
		/** @var  $model Ttsk */
		$model = $this->findModelBase64( 'Ttsk', $id );
		$attach = new Attachment();
		$attach->table = 'ttsk';
		$attach->tableId = $model->SKNo;
		$attach->titleId = $model->SKNo;
		$attach->model = $model;
		$attach->redirectUrl = \yii\helpers\Url::toRoute( [ 'ttsk/update','action' => 'display', 'id' => $id ] );
		return $this->redirect($attach->getRedirectUrl());
	}
}

