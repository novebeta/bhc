<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\models\Ttpihd;
use aunit\models\Ttpiit;
use aunit\models\Ttpshd;
use Yii;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
/**
 * TtpiitController implements the CRUD actions for Ttpiit model.
 */
class TtpiitController extends AunitController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttpiit models.
	 * @return mixed
	 */
	public function actionIndex() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttpiit::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
			/** @var TtpIit $model */
			$model = null;
			if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
				$model = $this->findModelBase64( 'ttpiit', $requestData[ 'id' ] );
			}
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'Action' ]  = 'Insert';
						$requestData[ 'PIAuto' ]  = 0;
						$requestData[ 'PIPajak' ] = 0;
					} else {
						$requestData[ 'Action' ] = 'Update';
						if ( $model != null ) {
							$requestData[ 'PINoOLD' ]    = $model->PINo;
							$requestData[ 'PIAutoOLD' ]  = $model->PIAuto;
							$requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
						}
					}
					$result = Ttpiit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'batch' :
					\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
					$respon                      = [];
					$base64                      = urldecode( base64_decode( $requestData[ 'header' ] ) );
					parse_str( $base64, $header );
					/** @var Ttpshd $PS */
//					$PS = Ttpshd::findOne($header[ 'PSNo' ]);
//					if($PS != null){
//						$header[ 'PSTgl' ] = $PS->PSTgl;
//					}
					$header[ 'Action' ] = 'Loop';
	                $result             = Ttpihd::find()->callSP( $header );
					Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
					foreach ( $requestData[ 'data' ] as $item ) {
						$item[ 'Action' ]     = 'Insert';
		                $result                    = Ttpiit::find()->callSP( $item );
						Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
					}
					$header[ 'Action' ] = 'LoopAfter';
					$result             = Ttpihd::find()->callSP( $header );
					Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
					return $respon;
					break;
				case 'del'  :
					$requestData             = array_merge( $requestData, $model->attributes );
					$requestData[ 'Action' ] = 'Delete';
					if ( $model != null ) {
						$requestData[ 'PINoLD' ]     = $model->PINo;
						$requestData[ 'PIAutoOLD' ]  = $model->PIAuto;
						$requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
					}
					$result = Ttpiit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
			}
		} else {
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'Ttpiit.' . $primaryKeys[ $i ];
			}
			$query     = ( new \yii\db\Query() )
				->select( new Expression(
					'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
							    	ttpiit.PINo,
                                    ttpiit.PIAuto,
                                    ttpiit.BrgKode,
                                    ttpiit.PIQty,
                                    ttpiit.PIHrgBeli,
                                    ttpiit.PIHrgJual,
                                    ttpiit.PIDiscount,
                                    ttpiit.PIPajak,
                                    tdbarang.BrgNama,
                                    tdbarang.BrgSatuan,
                                    IFNULL(
                                        ttpiit.PIDiscount / (
                                            ttpiit.PIHrgJual * ttpiit.PIQty
                                        ) * 100,
                                        0
                                    ) AS Disc,
                                    ttpiit.PIQty * ttpiit.PIHrgJual - ttpiit.PIDiscount AS Jumlah,
                                    ttpiit.LokasiKode,
                                    tdbarang.BrgGroup,
                                    ttpiit.PSNo,
                                    IFNULL(ttpsit.PSQty, 0) AS PSQty'
				) )
				->from( 'ttpiit' )
				->join( 'INNER JOIN', 'tdbarang', 'ttpiit.BrgKode = tdbarang.BrgKode' )
				->join( 'LEFT OUTER JOIN', 'ttpsit', 'ttpiit.BrgKode = ttpsit.BrgKode AND ttpiit.PSNo = ttpsit.PSNo' )
				->where( [ 'ttpiit.PINo' => $requestData[ 'PINo' ] ] )
				->orderBy( '	ttpiit.PINo,ttpiit.PIAuto' );
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	/**
	 * Displays a single Ttpiit model.
	 *
	 * @param string $PINo
	 * @param integer $PIAuto
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $PINo, $PIAuto ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $PINo, $PIAuto ),
		] );
	}
	/**
	 * Creates a new Ttpiit model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Ttpiit();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'PINo' => $model->PINo, 'PIAuto' => $model->PIAuto ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Ttpiit model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $PINo
	 * @param integer $PIAuto
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $PINo, $PIAuto ) {
		$model = $this->findModel( $PINo, $PIAuto );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'PINo' => $model->PINo, 'PIAuto' => $model->PIAuto ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Ttpiit model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $PINo
	 * @param integer $PIAuto
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $PINo, $PIAuto ) {
		$this->findModel( $PINo, $PIAuto )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Ttpiit model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $PINo
	 * @param integer $PIAuto
	 *
	 * @return Ttpiit the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $PINo, $PIAuto ) {
		if ( ( $model = Ttpiit::findOne( [ 'PINo' => $PINo, 'PIAuto' => $PIAuto ] ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
