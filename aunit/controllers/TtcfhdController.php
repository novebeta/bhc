<?php

namespace aunit\controllers;

use Yii;
use aunit\models\Ttcfhd;
use yii\data\ActiveDataProvider;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Attachment;
use aunit\models\Tdlokasi;
use aunit\models\Ttcfit;
use common\components\General;
use GuzzleHttp\Client;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * TtcfhdController implements the CRUD actions for Ttcfhd model.
 */
class TtcfhdController extends AunitController
{
    public function actions() {
		$sqlraw     = null;
		$queryRawPK = null;
		$colGrid    = null;
		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			$colGrid    = Ttcfhd::colGrid();
			$sqlraw     = "SELECT CFNo, CFTgl, CFMemo, LokasiKode, ttcfhd.SalesKode, SalesNama, StockBuku, StockFisik, 
							NilaiBuku, NilaiFisik, UserID FROM ttcfhd INNER JOIN tdsales ON tdsales.SalesKode = ttcfhd.SalesKode";
			$queryRawPK = [ 'CFNo' ];
		}
		return [
			'td' => [
				'class' => TdAction::class,
				'model' => Ttcfhd::class,
				'queryRaw'   => $sqlraw,
				'queryRawPK' => $queryRawPK,
				'colGrid'    => $colGrid,
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttcfhd models.
	 * @return mixed
	 */
	// private function getLokasi(){
	// 	return ArrayHelper::map( Tdlokasi::find()->select( [ 'LokasiKode' ] )->orderBy( 'LokasiStatus, LokasiKode' )->asArray()->all(), 'LokasiKode', 'LokasiKode' );
	// }

	// private function getSales(){
	// 	$querySales = Yii::$app->db->createCommand( "SELECT SalesKode, SalesNama FROM tdSales WHERE SalesStatus = 'A' ORDER BY SalesNama, SalesKode" )->queryAll();
	// 	return array_merge( [ '' => 'Semua' ], ArrayHelper::map( $querySales, 'SalesKode', 'SalesNama' ) );
	// }

	public function actionIndex() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttcfhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'index' );
	}
	/**
	 * Displays a single Ttcfhd model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * @return array
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDetail() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Ttcfit::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper  = $requestData[ 'oper' ];
			$id  = $requestData[ 'id' ];
			$model =  null;
			if(strpos($id, 'new_row') !== false){
				// $oper = 'add';
				$requestData[ 'Action' ] = 'Insert';
			}elseif($oper != 'del'){
				$model = $this->findModelBase64( 'Ttcfit', $requestData[ 'id' ] );
				$requestData[ 'CFNoOLD' ] = $model->CFNo;
				$requestData[ 'MotorAutoNOLD' ] = $model->MotorAutoN;
				$requestData[ 'MotorNoMesinOLD' ] = $model->MotorNoMesin;
				$requestData[ 'Action' ] = 'Update';
			}else{
				$model = $this->findModelBase64( 'Ttcfit', $requestData[ 'id' ] );
				$requestData[ 'MotorAutoNO' ] = $model->MotorAutoN;
				$requestData[ 'MotorNoMesin' ] = $model->MotorNoMesin;
				$requestData[ 'Action' ] = 'Delete';
			}
			$result           = Ttcfit::find()->callSP( $requestData );
			if($result[ 'status' ] == 0 ){
				$response = $this->responseSuccess( $result[ 'keterangan' ] );
				if(!empty($requestData['imgBase64'])){
					$attach = new Attachment();
					$attach->table = 'ttcfit';
					$attach->tableId = $result[ 'CFNo' ];
					$attach->titleId = $result[ 'CFNo' ];
					$attach->model = $model;
					$filename = $requestData[ 'MotorNoMesin' ].'_'.$requestData[ 'SPKNo' ].'.PNG';
					$attach->upload( $requestData[ 'imgBase64' ], $filename );
				}
			}else{
				$response = $this->responseFailed($result[ 'keterangan' ]  );
			}
		} else {
			/* operation : read */
			for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
				$primaryKeys[ $i ] = 'ttcfit.' . $primaryKeys[ $i ];
			}
			$rows = General::cCmd( "SELECT CONCAT(". implode( ",'||',", $primaryKeys ) .") AS id,
			CFNo, ttcfit.MotorAutoN, ttcfit.MotorNoMesin, ttcfit.FBHarga, tmotor.MotorType, tmotor.MotorWarna, MotorNama, MotorKategori
			FROM ttcfit INNER JOIN tmotor ON tmotor.MotorAutoN = ttcfit.MotorAutoN AND tmotor.MotorNoMesin = ttcfit.MotorNoMesin
			INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
			WHERE CFNo = :CFNo", [
				':CFNo' => $requestData[ 'CFNo' ]
			] )->queryAll();

			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}

			$response =  [
				'rows'      => $rows,
				'rowsCount' => count( $rows )
			];
		}
		return $response;
	}
	/**
	 * Creates a new Ttcfhd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
        TUi::$actionMode   	= Menu::ADD;
		$model             	= new Ttcfhd();
		$model->CFNo       	= General::createTemporaryNo( "CF", 'ttcfhd', 'CFNo' );
		$model->CFTgl 		= date( 'Y-m-d H:i:s' );
		$model->CFMemo 		= '--';
		$model->UserID 		= Menu::getUserLokasi()[ 'UserID' ];
		$model->SalesKode 	= '--';
		$model->LokasiKode 	= '--';
		$model->save(); $model->refresh();
		$dsJual = $model->toArray();
		$post[ 'Action' ] = 'Insert';
		$result           = Ttcfhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$id = $this->generateIdBase64FromModel( $model );
		$dsJual['CFNoView'] = $result[ 'CFNo' ];
		// $this->view->params['LokasiKodeCmb'] = $this->getLokasi();
		// $this->view->params['SalesKodeCmb'] = $this->getSales();
		return $this->render( 'create', [
			'model'    => $model,
			'dsJual'   => $dsJual,
			'id'       => $id
		] );
	}
	/**
	 * Updates an existing Ttcfhd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 * @param string $action
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id, $action ) {
		$index = \yii\helpers\Url::toRoute(['Ttcfhd/index']);
        if (!isset($_GET['action'])) {
            return $this->redirect($index);
        }
        $action = $_GET['action'];
        switch ($action) {
            case 'create':
                TUi::$actionMode = Menu::ADD;
                break;
            case 'update':
                TUi::$actionMode = Menu::UPDATE;
                break;
            case 'view':
                TUi::$actionMode = Menu::VIEW;
                break;
            case 'display':
                TUi::$actionMode = Menu::DISPLAY;
                break;
        }
        /** @var Ttcfhd $model */
        $model = $this->findModelBase64('Ttcfhd', $id);
		if(!$model){
			return $this->redirect(['index']);
		}
		$dsJual = $model->attributes;
		if ( Yii::$app->request->isPost ) {
			$post 			  = array_merge($dsJual, \Yii::$app->request->post());
			$post[ 'Action' ] = 'Update';
			$post[ 'LokasiKodeKu' ] = $post[ 'LokasiKode' ];
			$post[ 'CFTgl' ] = $post[ 'CFTgl' ].' '.$post[ 'JamCF' ];
			$result           = Ttcfhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model = Ttcfhd::findOne( $result[ 'CFNo' ] );
			$id    = $this->generateIdBase64FromModel( $model );
			$dsJual[ 'CFNoView' ] = $post[ 'CFNoView' ];
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
				$post[ 'CFNoView' ] = $result[ 'CFNo' ];
				return $this->redirect( [ 'ttcfhd/update', 'id' => $id,  'action' => 'display' ] );
			} else {
				return $this->render( 'update', [
					'model'    => $model,
					'CFNoView' => $_POST[ 'CFNoView' ],
					'dsJual'   => $dsJual,
					'id'       => $id,
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsJual[ 'Action' ] = 'Load';
			$result             = Ttcfhd::find()->callSP( $dsJual );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'CFNo' ] = $dsJual[ 'CFNo' ];
		}
		$dsJual[ 'CFNoView' ] = $_GET[ 'CFNoView' ] ?? $result[ 'CFNo' ];
		return $this->render( 'update', [
			'model'    => $model,
			'CFNoView' => $dsJual[ 'CFNoView' ],
			'dsJual'   => $dsJual,
			'id'       => $id
		] );
	}
	/**
	 * Deletes an existing Ttcfhd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 * @param string $action
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionCancel( $id, $action ) {
		/** @var Ttcfhd $model */
		$model = $this->findModelBase64( 'Ttcfhd', $id );
		$_POST[ 'CFNo' ]   = $model->CFNo;
		$_POST[ 'Action' ] = 'Cancel';
		$result            = Ttcfhd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if($result[ 'CFNo' ] == '--'){
			return $this->redirect(['index']);
		}else{
			$model = Ttcfhd::findOne($result[ 'CFNo' ]);
			return $this->redirect( [ 'ttcfhd/update',  'action' => 'display','id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}
	/**
	 * Deletes an existing Ttcfhd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */

    public function actionDelete()
    {
        /** @var Ttcfhd $model */
        $request = Yii::$app->request;
        if ($request->isAjax){
            $model = $this->findModelBase64('Ttcfhd', $_POST['id']);
            $_POST['CFNo'] = $model->CFNo;
            $_POST['Action'] = 'Delete';
            $result = Ttcfhd::find()->callSP($_POST);
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            $id = $request->get('id');
            $model = $this->findModelBase64('Ttcfhd', $id);
            $_POST['CFNo'] = $model->CFNo;
            $_POST['Action'] = 'Delete';
            $result = Ttcfhd::find()->callSP($_POST);
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['CFNo'] == '--') {
                return $this->redirect(['index']);
            } else {
                $model = Ttcfhd::findOne($result['CFNo']);
                return $this->redirect(['ttcfhd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
            }
        }
    }

    /**
     * Finds the Ttcfhd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Ttcfhd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ttcfhd::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

	public function actionPrint() {
		unset( $_POST[ '_csrf-app' ] );
		$_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client            = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response          = $client->post( Yii::$app->params[ 'host_report' ] . '/aunit/fcfhd', [
			'body' => Json::encode( $_POST )
		] );
		$html              = $response->getBody();
		return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
}
