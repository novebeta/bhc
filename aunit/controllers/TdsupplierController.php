<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Tdsupplier;

use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use common\components\Custom;
/**
 * TdsupplierController implements the CRUD actions for Tdsupplier model.
 */
class TdsupplierController extends AunitController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actions() {
		return [
			'td' => [
				'class' => TdAction::className(),
				'model' => Tdsupplier::className(),
			],
		];
	}
	/**
	 * Lists all Tdsupplier models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index' );
	}
	/**
	 * Displays a single Tdsupplier model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Creates a new Tdsupplier model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
        TUi::$actionMode   = Menu::ADD;
		$model = new Tdsupplier();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'tdsupplier/update','id'=> $this->generateIdBase64FromModel($model) ,'action' => 'display' ] );
		}
		return $this->render( 'create', [
			'model' => $model,
            'id'    => 'new'
		] );
	}
	/**
	 * Updates an existing Tdsupplier model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
        $index = \yii\helpers\Url::toRoute( [ 'tdsupplier/index' ] );
        if ( ! isset( $_GET[ 'action' ] ) ) {
            return $this->redirect( $index );
        }
        $action = $_GET[ 'action' ];
        switch ( $action ) {
            case 'create':
                TUi::$actionMode = Menu::ADD;
                break;
            case 'update':
                TUi::$actionMode = Menu::UPDATE;
                break;
            case 'view':
                TUi::$actionMode = Menu::VIEW;
                break;
            case 'display':
                TUi::$actionMode = Menu::DISPLAY;
                break;
        }
		$model = $this->findModelBase64( 'Tdsupplier', $id );
		if ( $model->load( Yii::$app->request->post() ) ) {
			if ( ! $model->save() ) {
				Yii::$app->getSession()->setFlash( 'error', Html::errorSummary( $model ) );
				return $this->render( 'update', [
					'model' => $model,
				] );
			}
            return $this->redirect( [ 'tdsupplier/update','id'=> $this->generateIdBase64FromModel($model) ,'action' => 'display' ] );
		}
		return $this->render( 'update', [
			'model' => $model,
            'id'    => $this->generateIdBase64FromModel($model),
		] );
	}
	/**
	 * Deletes an existing Tdsupplier model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete($id) {
        /** @var Tdsupplier $model */
        $request = Yii::$app->request;
        $model             = $this->findModelBase64( 'Tdsupplier', $id );
        if ( $model->delete() ) {
            if ($request->isAjax) {
                return $this->responseSuccess( 'Berhasil menghapus data Supplier');
            }
            $model = Tdsupplier::find()->one();
            return $this->redirect(['tdsupplier/update', 'id' => $this->generateIdBase64FromModel($model), 'action' => 'display']);

        } else {
            return $this->responseFailed( 'Gagal menghapus data Supplier' );
        }
	}
	/**
	 * Finds the Tdsupplier model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Tdsupplier the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Tdsupplier::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}

    public function actionCancel( $id, $action ) {
	    $model = Tdsupplier::findOne( $id );
	    if ( $model == null ) {
		    $model = Tdsupplier::find()->orderBy( [ 'SupKode' => SORT_ASC ] )->one();
		    if ( $model == null ) {
			    return $this->redirect( [ 'tdsupplier/index' ] );
		    }
	    }
	    return $this->redirect( [ 'tdsupplier/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
    }
}
