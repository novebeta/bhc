<?php

namespace aunit\controllers;

use aunit\components\TdAction;
use Yii;
use aunit\models\Tsaldoka;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TsaldokaController implements the CRUD actions for Tsaldoka model.
 */
class TsaldokaController extends Controller
{
	public function actions() {
		return [
			'td' => [
				'class' => TdAction::className(),
				'model' => Tsaldoka::className(),
			],
		];
	}
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tsaldoka models.
     * @return mixed
     */
    public function actionIndex()
    {

        return $this->render('index');
    }

    /**
     * Displays a single Tsaldoka model.
     * @param string $SaldoKasTgl
     * @param string $LokasiKode
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($SaldoKasTgl, $LokasiKode)
    {
        return $this->render('view', [
            'model' => $this->findModel($SaldoKasTgl, $LokasiKode),
        ]);
    }

    /**
     * Creates a new Tsaldoka model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tsaldoka();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'SaldoKasTgl' => $model->SaldoKasTgl, 'LokasiKode' => $model->LokasiKode]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tsaldoka model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $SaldoKasTgl
     * @param string $LokasiKode
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($SaldoKasTgl, $LokasiKode)
    {
        $model = $this->findModel($SaldoKasTgl, $LokasiKode);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'SaldoKasTgl' => $model->SaldoKasTgl, 'LokasiKode' => $model->LokasiKode]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tsaldoka model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $SaldoKasTgl
     * @param string $LokasiKode
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($SaldoKasTgl, $LokasiKode)
    {
        $this->findModel($SaldoKasTgl, $LokasiKode)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tsaldoka model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $SaldoKasTgl
     * @param string $LokasiKode
     * @return Tsaldoka the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($SaldoKasTgl, $LokasiKode)
    {
        if (($model = Tsaldoka::findOne(['SaldoKasTgl' => $SaldoKasTgl, 'LokasiKode' => $LokasiKode])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
