<?php

namespace aunit\controllers;

use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Tdlokasi;
use aunit\models\Tdsupplier;
use aunit\models\Tmotor;
use aunit\models\Ttss;
use common\components\Api;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class TtssController extends AunitController
{
    public function actions()
    {
        $colGrid = null;
        $joinWith = [];
        $join = [];
        $where = [
            [
                'op' => 'AND',
                'condition' => "(ttss.SSNo NOT LIKE '%=%')",
                'params' => []
            ]
        ];
        $sqlraw = null;
        $queryRawPK = null;
        if (isset($_POST['query'])) {
            $r = Json::decode($_POST['query'], true)['r'];
            switch ($r) {
                case 'ttss/select':
                    $where = [
                        [
                            'op' => 'AND',
                            'condition' => "(SSNo NOT LIKE '%=%')",
                            'params' => []
                        ]
                    ];
                   $whereSql = '--';
                   $q = Json::decode($_POST['query'], true);
                   if ($q['cmbTxt1'] == 'ttss.SSNo' && ($q['txt1'] !== '' || $q['txt1'] !== '--')) {
                       $whereSql = $q['txt1'];
                   }
                    $sqlraw = "SELECT ttss.SSNo, ttss.SSTgl, ttss.FBNo, ttss.SupKode, ttss.LokasiKode, ttss.SSMemo, ttss.UserID, ttss.DONo, 
					IFNULL(tdsupplier.SupNama, '--') AS SupNama, IFNULL(tdlokasi.LokasiNama, '--') AS LokasiNama, IFNULL(ttfb.FBTgl, 
					DATE('1900-01-01')) AS FBTgl, IFNULL(tmotor.SSJum, 0) AS SSJum 
					FROM  ttss 
					  LEFT OUTER JOIN ttfb ON ttss.FBNo = ttfb.FBNo 
					  LEFT OUTER JOIN tdsupplier ON ttss.SupKode = tdsupplier.SupKode 
					  LEFT OUTER JOIN tdlokasi ON ttss.LokasiKode = tdlokasi.LokasiKode 
					  LEFT OUTER JOIN 
					    (SELECT COUNT(MotorType) AS SSJum, SSNo FROM tmotor tmotor_1 GROUP BY SSNo) tmotor 
					  ON ttss.SSNo = tmotor.SSNo 
					  WHERE (ttfb.FBNo IS NULL OR ttss.SSNo = '{$whereSql}') AND (ttss.SSNo NOT LIKE '%=%')
					ORDER BY ttss.SSNo";
                    $queryRawPK = ['SSNo'];
                    break;
                default :
                    $joinWith = [
                        'fBNos' => [
                            'type' => 'LEFT OUTER JOIN'
                        ],
                        'supplier' => [
                            'type' => 'LEFT OUTER JOIN'
                        ],
                        'lokasi' => [
                            'type' => 'LEFT OUTER JOIN'
                        ],
                    ];
                    $join = [
                        [
                            'type' => 'LEFT OUTER JOIN',
                            'table' => '(SELECT COUNT(tmotor.MotorType) AS SSJum, tmotor.SSNo FROM tmotor GROUP BY SSNo) tmotor',
                            'on' => 'ttss.SSNo = tmotor.SSNo',
                            'params' => []
                        ]
                    ];
                    break;
            }
        }
        return [
            'td' => [
                'class' => TdAction::class,
                'model' => Ttss::class,
                'queryRaw' => $sqlraw,
                'queryRawPK' => $queryRawPK,
                'joinWith' => $joinWith,
                'join' => $join,
                'where' => $where
            ],
        ];
    }

    public function actionIndex()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'display') {
            $post['Action'] = 'Display';
            $result = Ttss::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        }
        return $this->render('index', [
            'mode' => '',
            'url_add' => Url::toRoute(['ttss/td'])
        ]);
    }

    /**
     * Lists all Ttfb models.
     * @return mixed
     */
    public function actionSelect()
    {
        $this->layout = 'select-mode';
        return $this->render('select', [
            'mode' => self::MODE_SELECT_DETAIL_MULTISELECT,
        ]);
    }

    public function actionImport()
    {
        $this->layout = 'select-mode';
        return $this->render('import', [
            'colGrid' => Ttss::colGridImportItems(),
            'mode' => self::MODE_SELECT_DETAIL_MULTISELECT,
        ]);
    }

    public function actionImportItems()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $fromTime = Yii::$app->formatter->asDate('-90 day', 'yyyy-MM-dd');
        $toTime = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        $addParam = [];
        if (isset($_POST['query'])) {
            $query = Json::decode($_POST['query'], true);
            $fromTime = $query['tgl1'] ?? $fromTime;
            $toTime = $query['tgl2'] ?? $toTime;
            if($query['txt1'] !== ''){
				$addParam = array_merge_recursive($addParam,[$query['cmbTxt1']=> $query['txt1']]);
			}
        }
        $api = new Api();
        $mainDealer = $api->getMainDealer();
        $arrHasil = [];
        switch ($mainDealer) {
            case API::MD_ANPER:
                $arrParams = [
                    'fromTime' => $fromTime . ' 00:00:00',
                    'toTime' => $toTime . ' 23:59:59',
                    'dealerId' => $api->getDealerId(),
                    'requestServerTime' => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                    'requestServertimezone' => date('T'),
                    'requestServerTimestamp' => $api->getCurrUnixTime(),
                ];
                break;
            default:
                $arrParams = [
                    'fromTime'               => $fromTime . ' 00:00:00',
                    'toTime'                 => $toTime . ' 23:59:59',
                    'dealerId'               => $api->getDealerId(),
                    'poId'                   => '',
                    'noShippingList'         => '',
                ];
        }
        $arrParams = array_merge_recursive($arrParams,$addParam);
        $filterResult = [];
		if($query['txt2'] !== ''){
			$filterResult = [
				'key' => $query['cmbTxt2'],
				'value' => $query['txt2'],
			];
		}
        $hasil = $api->call(Api::TYPE_UINB, Json::encode($arrParams),$filterResult);
        $arrHasil = Json::decode($hasil);
        if (!isset($arrHasil['data'])) {
            return [
                'rows' => [],
                'rowsCount' => 0
            ];
        }
        $_SESSION['ttss']['import'] = $arrHasil['data'];
        return [
            'rows' => $arrHasil['data'],
            'rowsCount' => sizeof($arrHasil['data'])
        ];
    }

    public function actionImportDetails()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        // $key = array_search($_POST['noShippingList'], array_column($_SESSION['ttss']['import'], 'noShippingList'));
        $arrayChecker = [
            'noShippingList'=>$_POST['noShippingList'],
            'noInvoice'=>$_POST['noInvoice']
        ];
        if(!empty($_POST['createdTime'])){
            $arrayChecker['createdTime'] = $_POST['createdTime'];
        }
        $key = API::array_search_m($arrayChecker, $_SESSION['ttss']['import']);
        return [
            'rows' => $_SESSION['ttss']['import'][$key]['unit'],
            'rowsCount' => sizeof($_SESSION['ttss']['import'][$key]['unit'])
        ];
    }

    public function actionJurnal()
    {
        $post = \Yii::$app->request->post();
        $post['Action'] = 'Jurnal';
        $post['SSJam'] = $post['SSTgl'] . ' ' . $post['SSJam'];
        $post['SSNo'] = $post['SSNoLama'];
        $result = Ttss::find()->callSP($post);
        Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        return $this->redirect(['ttss/update', 'action' => 'display', 'id' => base64_encode($result['SSNo'])]);
    }

    /**
     * Creates a new Ttss model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        TUi::$actionMode = Menu::ADD;
        $LokasiKodeCmb = Tdlokasi::find()->where("LokasiStatus IN ('A',:status) AND LokasiJenis <> 'Kas'", [
            ':status' => Menu::getUserLokasi()['LokasiStatus']
        ])->orderBy('LokasiStatus, LokasiKode')->one();
        $SuppKodeCmb = Tdsupplier::find()->where("SupStatus = 'A' or  SupStatus = '1'")->orderBy('SupStatus, SupKode')->one();
        $model = new Ttss();
        $model->SSNo = General::createTemporaryNo('SS', 'ttss', 'SSNo');
        $model->SSNoTerima = General::createTemporaryNo('PS', 'ttss', 'SSNoTerima');
        $model->SSTgl = date('Y-m-d');
        $model->SSJam = date('Y-m-d H:i:s');
        $model->FBNo = '--';
        $model->SSMemo = '--';
        $model->DONo = '--';
        $model->SupKode = ($SuppKodeCmb != null) ? $SuppKodeCmb->SupKode : '';
        $model->LokasiKode = ($LokasiKodeCmb != null) ? $LokasiKodeCmb->LokasiKode : '';
        $model->UserID = $this->UserID();
        $model->save();
        $post = $model->attributes;
        $post['Action'] = 'Insert';
        $post['LokasiKodeSS'] = $model->LokasiKode;
        $result = Ttss::find()->callSP($post);
        Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        $id = $this->generateIdBase64FromModel($model);
        $dsBeli = Ttss::find()->dsTBeliFillByNo()->where(['ttss.SSNo' => $model->SSNo])->asArray(true)->one();
        $dsBeli['SSNoView'] = $result['SSNo'];
        return $this->render('create', [
            'model' => $model,
            'dsBeli' => $dsBeli,
            'id' => $id
        ]);
    }

    public function actionUpdate($id)
    {
        $index = \yii\helpers\Url::toRoute(['ttss/index']);
        if (!isset($_GET['action'])) {
            return $this->redirect($index);
        }
        $action = $_GET['action'];
        switch ($action) {
            case 'create':
                TUi::$actionMode = Menu::ADD;
                break;
            case 'update':
                TUi::$actionMode = Menu::UPDATE;
                break;
            case 'view':
                TUi::$actionMode = Menu::VIEW;
                break;
            case 'display':
                TUi::$actionMode = Menu::DISPLAY;
                break;
        }
        /** @var Ttss $model */
        $model = $this->findModelBase64('Ttss', $id);
        $dsBeli = Ttss::find()->dsTBeliFillByNo()->where(['ttss.SSNo' => $model->SSNo])->asArray(true)->one();
        if (Yii::$app->request->isPost) {
            $post = array_merge($dsBeli, \Yii::$app->request->post());
            $post['SSJam'] = $post['SSTgl'] . ' ' . $post['SSJam'];
            $post['Action'] = 'Update';
            $post['LokasiKodeSS'] = $post['LokasiKode'];
            $result = Ttss::find()->callSP($post);
            Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
            $model = Ttss::findOne($result['SSNo']);
            $id = $this->generateIdBase64FromModel($model);
            if ($result['status'] == 0 && $result['status'] !== null && $post['SaveMode'] == 'ALL') {
                $post['SSNoView'] = $result['SSNo'];
                return $this->redirect(['ttss/update', 'action' => 'display', 'id' => $id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'dsBeli' => $post,
                    'id' => $id,
                ]);
            }
        }
        if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsBeli['Action'] = 'Load';
            $result = Ttss::find()->callSP($dsBeli);
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'SSNo' ] = $dsBeli['SSNo'];
		}
		$dsBeli[ 'SSNoView' ] = $_GET[ 'SSNoView' ] ?? $result[ 'SSNo' ];
        // $dsBeli['Action'] = 'Load';
        // $result = Ttss::find()->callSP($dsBeli);
        // Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        // $dsBeli['SSNoView'] = $_GET['SSNoView'] ?? $dsBeli['SSNo'];
        return $this->render('update', [
            'model' => $model,
            'dsBeli' => $dsBeli,
            'id' => $id
        ]);
    }

    public function actionDelete()
    {
        /** @var Ttss $model */
        $request = Yii::$app->request;
        if ($request->isAjax){
            $model = $this->findModelBase64('Ttss', $_POST['id']);
            $_POST['SSNo'] = $model->SSNo;
            $_POST['Action'] = 'Delete';
            $result = Ttss::find()->callSP($_POST);
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            $id = $request->get('id');
            $model = $this->findModelBase64('Ttss', $id);
            $_POST['SSNo'] = $model->SSNo;
            $_POST['Action'] = 'Delete';
            $result = Ttss::find()->callSP($_POST);
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['SSNo'] == '--') {
                return $this->redirect(['index']);
            } else {
                $model = Ttss::findOne($result['SSNo']);
                return $this->redirect(['ttss/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
            }
        }
    }

    public function actionDetail()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $requestData = \Yii::$app->request->post();
        $primaryKeys = Tmotor::getTableSchema()->primaryKey;
        if (isset($requestData['oper'])) {
            /* operation : create, edit or delete */
            $oper = $requestData['oper'];
            /** @var Tmotor $model */
            $model = null;
            if (isset($requestData['id'])) {
                $model = $this->findModelBase64('tmotor', $requestData['id']);
            }
            switch ($oper) {
                case 'add'  :
                case 'edit' :
                    if (strpos($requestData['id'], 'new_row') !== false) {
                        $requestData['Action'] = 'Insert';
                        $requestData['MotorAutoN'] = 0;
                    } else {
                        $requestData['Action'] = 'Update';
                        if ($model != null) {
                            $requestData['MotorAutoNOLD'] = $model->MotorAutoN;
                            $requestData['MotorNoMesinOLD'] = $model->MotorNoMesin;
                        }
                    }
                    $result = Tmotor::find()->callSP_SS($requestData);
                    if ($result['status'] == 0 && $result['status'] !== null) {
                        $response = $this->responseSuccess($result['keterangan']);
                    } else {
                        $response = $this->responseFailed($result['keterangan']);
                    }
                    break;
                case 'batch' :
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    $respon = [];
                    $base64 = urldecode(base64_decode($requestData['header']));
                    parse_str($base64, $header);
//					if ( strpos( $header[ 'SSNoLama' ], '=' ) !== false && str_replace( '=', '-', $header[ 'SSNoLama' ] ) == $header[ 'SSNo' ] ) {
//						$header[ 'SSNoBaru' ] = General::createNo( 'SS', 'ttss', 'SSNo' );
//					} else {
                    $header['SSNoBaru'] = $header['SSNoView'];
//					}
//					$header[ 'SSNo' ] = $header[ 'SSNoLama' ];
//					if ( strpos( $header[ 'SSNoTerima' ], '=' ) !== false ) {
//						$header[ 'SSNoTerima' ] = General::createNo( 'PS', 'ttss', 'SSNoTerima' );
//					}
                    $header['SSJam'] = $header['SSTgl'] . ' ' . $header['SSJam'];
                    $header['Action'] = 'LoopBefore';
                    $result = Ttss::find()->callSP($header);
                    // $isSameID = $result['SSNo'] == $header['SSNo'];
                    // if ($isSameID) {
                    //     $respon['notif'][] = $result;
                    // } else {
                    // }
                    Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
                    $model = Ttss::findOne($result['SSNo']);
                    $id = $this->generateIdBase64FromModel($model);
                    $respon['id'] = $id;
//					$respon[ 'header' ] = Ttss::find()->dsTBeliFillByNo()->where( [ 'ttss.SSNo' => $model->SSNo ] )->asArray( true )->one();
                    foreach ($requestData['data'] as $item) {
                        $item['Action'] = 'Loop';
                        $item['SSNo'] = $model->SSNo;
                        $item['MotorAutoNOLD'] = $item['MotorAutoN'];
                        $item['MotorNoMesinOLD'] = $item['MotorNoMesin'];
                        $result = Tmotor::find()->callSP_SS($item);
						Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
                        // if ($isSameID) {
                        //     $respon['notif'][] = $result;
                        // } else {
                        // }
                        // Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
//						$response = $this->responseSuccess( $result[ 'keterangan' ] );
//						if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
//						} else {
//							$response = $this->responseFailed( $result[ 'keterangan' ] );
//						}
                    }
                    $header['Action'] = 'LoopAfter';
                    $result = Ttss::find()->callSP($header);
                    return $respon;
                    break;
                case 'import' :
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    $respon = [];
                    $base64 = urldecode(base64_decode($requestData['header']));
                    parse_str($base64, $header);
                    $header['SSNoBaru'] = $header['SSNoView'];
                    $header['SSJam'] = $header['SSTgl'] . ' ' . $header['SSJam'];                    
                    $requestData['data'][0]['header']['data']['Action'] = 'ImporBefore';
                    $requestData['data'][0]['header']['data']['NoTrans'] = $header['SSNo'];
                    $result = Api::callUINB($requestData['data'][0]['header']['data']);
                    // $isSameID = $result['SSNo'] == $header['SSNo'];
                    Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
                    $model = Ttss::findOne($result['NoTrans']);
                    $id = $this->generateIdBase64FromModel($model);
                    $respon['id'] = $id;
                    if($result['status'] == 1){
                        return $respon;
                    }
                    foreach ($requestData['data'] as $item) {
                        $item['data']['Action'] = 'Impor';
                        $item['data']['NoTrans'] = $model->SSNo;
                        $result = Api::callUINBit($item['data']);
                        Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
                    }
                    $requestData['data'][0]['header']['data']['Action'] = 'ImporAfter';
                    $result = Api::callUINB($requestData['data'][0]['header']['data']);
                    Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
		            return $respon;
                    break;
                case 'del'  :
                    $requestData = array_merge($requestData, $model->attributes);
                    $requestData['Action'] = 'Delete';
                    if ($model != null) {
                        $requestData['MotorAutoNOLD'] = $model->MotorAutoN;
                        $requestData['MotorNoMesinOLD'] = $model->MotorNoMesin;
                    }
                    $result = Tmotor::find()->callSP_SS($requestData);
                    if ($result['status'] == 0 && $result['status'] !== null) {
                        $response = $this->responseSuccess($result['keterangan']);
                    } else {
                        $response = $this->responseFailed($result['keterangan']);
                    }
                    break;
            }
        } else {
            /* operation : read */
            $query = (new \yii\db\Query())
                ->select([
                    'CONCAT(' . implode( ',"||",', [ 'tmotor.MotorAutoN', 'tmotor.MotorNoMesin' ] ) . ') AS id,
                    tmotor.*, tdmotortype.KodeAHM AS KodeAHM'
                ])
                ->from(Tmotor::tableName())
                ->join( 'LEFT OUTER JOIN', 'tdmotortype', 'tmotor.MotorType = tdmotortype.MotorType' )
                ->where([
                    'SSNo' => $requestData['SSNo']
                ])
                ->orderBy('MotorType');
            $rowsCount = $query->count();
            $rows = $query->all();
            /* encode row id */
            for ($i = 0; $i < count($rows); $i++) {
                $rows[$i]['id'] = base64_encode($rows[$i]['id']);
            }
            $response = [
                'rows' => $rows,
                'rowsCount' => $rowsCount
            ];
        }
        return $response;
    }

    public function actionCancel($id, $action)
    {
        /** @var Ttss $model */
        $_POST['Action'] = 'Cancel';
        $_POST['SSJam'] = $_POST['SSTgl'] . ' ' . $_POST['SSJam'];
        $result = Ttss::find()->callSP($_POST);
        Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
        if ($result['SSNo'] == '--') {
            return $this->redirect(['index']);
        } else {
            $model = Ttss::findOne($result['SSNo']);
            return $this->redirect(['ttss/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
        }
    }

    public function actionPrint($id)
    {
        unset($_POST['_csrf-app']);
        $_POST = array_merge($_POST, Yii::$app->session->get('_company'));
        $_POST['db'] = base64_decode($_COOKIE['_outlet']);
        $_POST['UserID'] = Menu::getUserLokasi()['UserID'];
        $client = new Client([
            'headers' => ['Content-Type' => 'application/octet-stream']
        ]);
        $response = $client->post(Yii::$app->params['H1'][$_POST['db']]['host_report'] . '/aunit/fss', [
            'body' => Json::encode($_POST)
        ]);
        $html = $response->getBody();
        return str_replace("<BODY", '<BODY onload="window.print()"', $html);
    }

    /**
     * Finds the Ttss model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param string $id
     *
     * @return Ttss the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ttss::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function CekDataGrid($detil)
    {
        $pesanNoMesin = [];
        $pesanNoRangka = [];
        $pesanHarga = [];
        foreach ($detil as $row) {
            if ($row['SSHarga'] == 0) {
                $pesanHarga[] = $row['MotorType'];
            }
            if (strlen($row['MotorNoMesin']) < 12) {
                $pesanNoMesin[] = $row['MotorNoMesin'];
            }
            if (strlen($row['MotorNoRangka']) < 17) {
                $pesanNoRangka[] = $row['MotorNoRangka'];
            }
            if (strlen($row['FBNo']) < 18) {
                $pesanFBNo[] = $row['FBNo'];
            }
        }
        $msg = '';
        if (sizeof($pesanNoMesin) > 0) {
            $msg = "Berikut No Mesin yang kurang dari 12 karakter : " . implode(', ', $pesanNoMesin);
        }
        if (sizeof($pesanNoRangka) > 0) {
            $msg = "Berikut No Rangka yang kurang dari 17 karakter : " . implode(', ', $pesanNoRangka);
        }
        if (sizeof($pesanFBNo) > 0) {
            $msg = "Berikut No Faktur yang kurang dari 17 karakter : " . implode(', ', $pesanFBNo);
        }
        if (sizeof($pesanHarga) > 0) {
            $msg = "Berikut Type Motor yang belum memiliki HPP : " . implode(', ', $pesanHarga) . "\n Silahkan memberi nilai HPP melalui Menu Data - Type & Warna Motor";
        }
        return $msg;
    }

    public function actionCount()
    {
        $requestData = \Yii::$app->request->post();
        $num = Ttss::find()->where(['SSNo'=>$requestData['data']])->count();
        return $this->responseJson(true,$num);
    }

}
