<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Traccount;
use aunit\models\Ttgeneralledgerhd;
use common\components\General;
use Yii;
use yii\bootstrap\Html;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TraccountController implements the CRUD actions for Traccount model.
 */
class TraccountController extends AunitController {
	public function actions() {
		return [
			'td' => [
				'class'    => TdAction::class,
				'model'    => Traccount::class,
				'joinWith' => [
					'parent' => [
						'type' => 'INNER JOIN'
					]
				]
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Traccount models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index' );
	}
	public function actionNeracaAwal() {
		if(Yii::$app->request->isPost){
			$requestData = Yii::$app->request->post();
			$requestData[ 'Action' ] = 'Insert';
			$result                  = Traccount::find()->callSP( $requestData );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		$TotalDebet      = $TotalKredit = $TotalPendapatan = $TotalBiaya = $TotalLabaRugi = 0;
		$TotalDebet      = General::cCmd( "SELECT IFNULL(SUM(DebetGL),0)-IFNULL(SUM(KreditGL),0) FROM ttgeneralledgerit WHERE NOGL = '00NA00000' AND LEFT(NoAccount,1) = '1'" )
		                          ->queryScalar();
		$TotalKredit     = General::cCmd( "SELECT IFNULL(SUM(KreditGL),0)-IFNULL(SUM(DebetGL),0) FROM ttgeneralledgerit WHERE NOGL = '00NA00000' AND LEFT(NoAccount,1) = '2'" )
		                          ->queryScalar();
		$TotalPendapatan = General::cCmd( "SELECT IFNULL(SUM(KreditGL),0)-IFNULL(SUM(DebetGL),0) FROM ttgeneralledgerit WHERE NOGL = '00NA00000' AND LEFT(NoAccount,1) IN ('3','4','9')" )
		                          ->queryScalar();
		$TotalBiaya      = General::cCmd( "SELECT IFNULL(SUM(DebetGL),0)-IFNULL(SUM(KreditGL),0) FROM ttgeneralledgerit WHERE NOGL = '00NA00000' AND LEFT(NoAccount,1) IN ('5','6','8')" )
		                          ->queryScalar();
		$TotalLabaRugi   = $TotalPendapatan - $TotalBiaya;
		$TotalKredit     = $TotalKredit + $TotalLabaRugi;
		$tglAwal         = Ttgeneralledgerhd::find()->TglSaldoAwal();
		return $this->render( 'NeracaAwal', [
			'TotalDebet'    => $TotalDebet,
			'TotalKredit'   => $TotalKredit,
			'TotalLabaRugi' => $TotalLabaRugi,
			'tglAwal'       => $tglAwal,
			'aktiva'        => Json::encode( Traccount::find()->getAccount( '10000000' ) ),
			'pasiva'        => Json::encode( Traccount::find()->getAccount( '20000000' ) ),
			'sales'         => Json::encode( Traccount::find()->getAccount( '30000000' ) ),
			'pend_ope'      => Json::encode( Traccount::find()->getAccount( '40000000' ) ),
			'hpp'           => Json::encode( Traccount::find()->getAccount( '50000000' ) ),
			'biaya_ope'     => Json::encode( Traccount::find()->getAccount( '60000000' ) ),
			'biaya_non'     => Json::encode( Traccount::find()->getAccount( '80000000' ) ),
			'pend_non_ope'  => Json::encode( Traccount::find()->getAccount( '90000000' ) ),
		] );
	}
	public function actionLoadDataAccount() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$data = General::cCmd("SELECT  KeteranganGL, DebetGL, KreditGL FROM ttgeneralledgerit 
			WHERE NoGL = '00NA00000' AND NoAccount = :NoAccount",[':NoAccount' => $_POST['NoAccount']])->queryOne();
		return ($data === false) ? ['KeteranganGL'=>'--','DebetGL'=>"0.00",'KreditGL'=>"0.00"] : $data;
	}
	public function actionNeracaAwalUpdate() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData             = Yii::$app->request->post();
		$requestData[ 'Action' ] = 'Update';
		$result                  = Traccount::find()->callSP( $requestData );
		return [ 'status' => $result['status'], 'msg' => $result['keterangan'] ];
	}
	public function actionNeracaAwalDelete() {
		$requestData             = Yii::$app->request->post();
		$requestData[ 'Action' ] = 'Delete';
		$result                  = Traccount::find()->callSP( $requestData );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( Url::toRoute( [ 'traccount/neraca-awal' ] ) );
	}



	public function actionDataPerkiraan() {
		return $this->render( 'DataPerkiraan', [
			'aktiva'       => Json::encode( Traccount::find()->getAccount( '10000000' ) ),
			'pasiva'       => Json::encode( Traccount::find()->getAccount( '20000000' ) ),
			'sales'        => Json::encode( Traccount::find()->getAccount( '30000000' ) ),
			'pend_ope'     => Json::encode( Traccount::find()->getAccount( '40000000' ) ),
			'hpp'          => Json::encode( Traccount::find()->getAccount( '50000000' ) ),
			'biaya_ope'    => Json::encode( Traccount::find()->getAccount( '60000000' ) ),
			'biaya_non'    => Json::encode( Traccount::find()->getAccount( '80000000' ) ),
			'pend_non_ope' => Json::encode( Traccount::find()->getAccount( '90000000' ) ),
		] );
	}
	/**
	 * Creates a new Traccount model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionDataPerkiraanCreate() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$model                       = new Traccount();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return [
				'status' => true,
				'msg'    => 'Data perkiraan berhasil disimpan.',
				'record' => Traccount::find()->getAccount( $_POST[ 'RootID' ] ),
			];
		}
		return [ 'status' => false, 'msg' => 'Data perkiraan gagal disimpan .' . Html::errorSummary( $model ) ];
	}
	/**
	 * Updates an existing Traccount model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDataPerkiraanUpdate() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$id                          = $_POST[ 'NoAccountOld' ];
		$model                       = $this->findModel( $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return [
				'status' => true,
				'msg'    => 'Data perkiraan berhasil disimpan.',
				'record' => Traccount::find()->getAccount( $_POST[ 'RootID' ] ),
			];
		}
		return [ 'status' => false, 'msg' => 'Data perkiraan gagal disimpan .' . Html::errorSummary( $model ) ];
	}
	/**
	 * Deletes an existing Traccount model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDataPerkiraanDelete() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//		$this->findModel( $_POST['NoAccount'] )->delete();
		if ( $this->findModel( $_POST[ 'NoAccount' ] )->delete() ) {
			return [
				'status' => true,
				'msg'    => 'Data perkiraan berhasil dihapus.',
				'record' => Traccount::find()->getAccount( $_POST[ 'RootID' ] )
			];
		}
		return [ 'status' => false, 'msg' => 'Data perkiraan gagal dihapus .' ];
	}
	public function actionGetAccount( $NoAccount ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//		return Traccount::find()
//		              ->with(['parent'])
//			->asArray()
//		              ->all();
//		return  ArrayHelper::map(Traccount::find()->all(), 'NoAccount', function ($model) {
//			return ArrayHelper::toArray($model->parent);
//		});
		$induk   = Traccount::find()
		                    ->select(
			                    [
				                    'NoAccount',
				                    'CONCAT(NoAccount," ",NamaAccount) AS label',
				                    'IF(JenisAccount ="Detail","true","false") AS isLeaf',
				                    new Expression( '"0" as level' ),
				                    new Expression( '"true" as loaded' ),
				                    new Expression( '"true" as expanded' )
			                    ] )
		                    ->where( [ 'NoAccount' => $NoAccount ] )->asArray()->one();
		$child   = Traccount::find()->getChild( $NoAccount );
		$child[] = $induk;
		return [
			'rows' => $child
		];
	}
	/**
	 * Creates a new Traccount model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		TUi::$actionMode = Menu::ADD;
		$model = new Traccount();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'traccount/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
		}
		return $this->render( 'create', [
			'model' => $model,
			'id'=> 'new'
		] );
	}
	/**
	 * Updates an existing Traccount model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$index = \yii\helpers\Url::toRoute( [ 'traccount/index' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		$model = $this->findModelBase64( 'Traccount', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'traccount/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
		}
		return $this->render( 'update', [
			'model' => $model,
			'id'=>$id
		] );
	}
	/**
	 * Deletes an existing Traccount model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete() {
		$request = Yii::$app->request;
		$msg = "Berhasil menghapus data Perkiraan No: ";
        if ($request->isAjax){
            $model = $this->findModelBase64('Traccount', $_POST['id']);
			$NoAccount = $model->NoAccount;
			$model->delete();
			return $this->responseSuccess($msg.$NoAccount);
            // if ($result['status'] == 0) {
            //     return $this->responseSuccess($result['keterangan']);
            // } else {
            //     return $this->responseFailed($result['keterangan']);
            // }
        }else{
            $id = $request->get('id');
            $model = $this->findModelBase64('Traccount', $id);
			$NoAccount = $model->NoAccount;
			$model->delete();
            Yii::$app->session->addFlash( 'success', $msg.$NoAccount);
			$model = Traccount::find()->one();
			if(!empty($model)){
				return $this->redirect( ['traccount/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
			}else{
				return $this->redirect(['traccount']);
			}
            // if ( $result[ 'DKNo' ] == '--' ) {
			// 	return $this->redirect( [ base64_decode( $index ) ] );
			// } else {
			// 	$model = Ttdk::findOne( $result[ 'DKNo' ] );
			// 	$url   = base64_decode( $index ) . '-update';
			// 	return $this->redirect( [ $url, 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
			// }
        }
	}
	/**
	 * Finds the Traccount model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Traccount the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Traccount::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
