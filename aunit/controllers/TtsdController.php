<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Tddealer;
use aunit\models\Tmotor;
use aunit\models\Ttsd;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\base\UserException;
use yii\db\Exception;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
/**
 * TtsdController implements the CRUD actions for Ttsd model.
 */
class TtsdController extends AunitController {
	const JENIS = [
		'1' => [ 'jenis' => 'SuratReturBeli', 'index' => 'ttsd/retur-beli', 'KodeTrans' => 'SR' ],
		'2' => [ 'jenis' => 'InvoiceReturBeli', 'index' => 'ttsd/invoice-retur-beli', 'KodeTrans' => 'IR' ],
		'3' => [ 'jenis' => 'MutasiEksternal', 'index' => 'ttsd/mutasi-eksternal-dealer', 'KodeTrans' => 'ME' ],
		'4' => [ 'jenis' => 'InvoiceEksternal', 'index' => 'ttsd/invoice-eksternal-dealer', 'KodeTrans' => 'IE' ],
	];
	public function actions() {
		$colGrid    = null;
		$joinWith   = [
			'supplier' => [
				'type' => 'LEFT JOIN'
			],
			'lokasi'   => [
				'type' => 'LEFT JOIN'
			]
		];
		$join       = [];
        $where      = [];
		$queryRaw   = null;
		$queryRawPK = null;
		if ( isset( $_POST[ 'query' ] ) ) {
			$r = Json::decode( $_POST[ 'query' ], true )[ 'r' ];
			switch ( $r ) {
				case 'ttsd/mutasi-eksternal-dealer-select':
					$urlParams  = Json::decode( $_POST[ 'query' ], true )[ 'urlParams' ];
					$colGrid    = Ttsd::colGridMutasiEksternalDealerSelect();
					$DealerKode = $urlParams[ 'DealerKode' ];
					$queryRawPK = [ "SDNo" ];
					$LokasiKode = Tddealer::findOne(['DealerStatus'=>'1'])->DealerKode;
					$queryRaw   = "SELECT 
       ttsd.*, IFNULL(tmotor.SDJum, 0) AS SDJum, IFNULL($LokasiKode.ttpd.SDNo,'--') AS PDSDNo
                    FROM  $DealerKode.ttsd
                    LEFT OUTER JOIN 
                        (SELECT 
                                COUNT(MotorType) AS SDJum, SDNo 
                        FROM $DealerKode.tmotor 
                        GROUP BY SDNo) tmotor 
                            ON ttsD.SDNo = tmotor.SDNo
                    LEFT OUTER JOIN $LokasiKode.ttpd 
                        ON $DealerKode.ttsd.SDNo = $LokasiKode.ttpd.SDNo 
						AND $LokasiKode.ttpd.DealerKode = '$DealerKode'
                    WHERE $LokasiKode.ttpd.SDNo IS NULL
                        AND ttsd.SDNo LIKE '%%'
                       AND ($DealerKode.ttsd.DealerKode = '$LokasiKode')
                    ORDER BY ttsd.SDNo";
					break;
				case 'ttsd/mutasi-eksternal-dealer':
					$colGrid  = Ttsd::colGridMutasiEksternalDealer();
					$joinWith = [
						'lokasi' => [
							'type' => 'LEFT JOIN'
						],
						'dealer' => [
							'type' => 'LEFT JOIN'
						],
					];
					$where    = [
						[
							'op'        => 'AND',
							'condition' => "LEFT(SDNo,2) = 'ME'",
							'params'    => []
						]
					];
					break;
				case 'ttsd/invoice-eksternal-dealer':
					$colGrid  = Ttsd::colGridInvoiceEksternal();
					$joinWith = [
						'lokasi' => [
							'type' => 'INNER JOIN'
						],
						'dealer' => [
							'type' => 'INNER JOIN'
						],
					];
					$where    = [
						[
							'op'        => 'AND',
							'condition' => "LEFT(SDNo,2) = 'ME'",
							'params'    => []
						]
					];
					break;
				case 'ttsd/retur-beli':
					$colGrid   = Ttsd::colGridReturBeli();
					$joinWith  = [
						'lokasi'   => [
							'type' => 'LEFT JOIN'
						],
						'supplier' => [
							'type' => 'LEFT JOIN'
						],
					];
					$condition = "LEFT(SDNo,2) = 'SR' AND ttsd.SDNo NOT LIKE '%=%'";
					if ( $r == 'ttsd/invoice-retur-beli' ) {
						$condition .= " AND INSTR(`SDNo`, '=') = 0 ";
					}
					$where = [
						[
							'op'        => 'AND',
							'condition' => $condition,
							'params'    => []
						]
					];
					break;
				case 'ttsd/invoice-retur-beli':
					$colGrid   = Ttsd::colGridInvoiceReturBeli();
					$joinWith  = [
						'lokasi'   => [
							'type' => 'LEFT JOIN'
						],
						'supplier' => [
							'type' => 'LEFT JOIN'
						],
					];
					$condition = "LEFT(SDNo,2) = 'SR'";
					if ( $r == 'ttsd/invoice-retur-beli' ) {
						$condition .= " AND INSTR(`SDNo`, '=') = 0";
					}
					$where = [
						[
							'op'        => 'AND',
							'condition' => $condition,
							'params'    => []
						]
					];
					break;
			}
		}
		return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Ttsd::class,
				'queryRaw'   => $queryRaw,
				'queryRawPK' => $queryRawPK,
				'colGrid'    => $colGrid,
				'joinWith'   => $joinWith,
				'where'      => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttsd models.
	 * @return mixed
	 */
	public function actionReturBeli() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ]    = 'Display';
			$post[ 'KodeTrans' ] = 'SR';
			$post[ 'StatPrint' ] = '1';
			$result              = Ttsd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'retur-beli' );
	}
	/**
	 * Lists all Ttsd models.
	 * @return mixed
	 */
	public function actionReturBeliCreate() {
		TUi::$actionMode   = Menu::ADD;
		$model             = new Ttsd();
		$model->SDNo       = General::createTemporaryNo( "SR", 'ttsd', 'SDNo' );
		$model->SDTgl      = date( 'Y-m-d' );
		$model->SDMemo     = '--';
		$model->SDTotal    = 0;
		$model->LokasiKode = $this->LokasiKode();
		$model->UserID     = $this->UserID();
		$model->save();
		$post                = $model->attributes;
		$post[ 'Action' ]    = 'Insert';
		$post[ 'KodeTrans' ] = 'SR';
		$post[ 'SDNoBaru' ]  = $post[ 'SDNo' ];
		$result              = Ttsd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$id                   = $this->generateIdBase64FromModel( $model );
		$dsBeli               = Ttsd::find()->dsTMutasiReturBeli()->where( [ 'ttsd.SDNo' => $model->SDNo ] )->asArray( true )->one();
		$dsBeli[ 'SDNoView' ] = $result[ 'SDNo' ];
		return $this->render( 'create', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'jenis'  => "SuratReturBeli",
			'id'     => $id
		] );
	}
	/**
	 * Lists all Ttsd models.
	 *
	 * @param $id
	 * @param $action
	 *
	 * @return mixed
	 * @throws Exception
	 * @throws NotFoundHttpException
	 */
	public function actionReturBeliUpdate( $id, $action ) {
		$index = \yii\helpers\Url::toRoute( [ 'ttsd/retur-beli' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttsd $model */
		$model  = $this->findModelBase64( 'Ttsd', $id );
		$dsBeli = Ttsd::find()->dsTMutasiReturBeli()->where( [ 'ttsd.SDNo' => $model->SDNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$_POST                = array_merge( $dsBeli, $_POST );
			$_POST[ 'KodeTrans' ] = 'SR';
			$transaction          = Ttsd::getDb()->beginTransaction();
			try {
				$this->prosesUpdateReturBeli( $model, $action );
				$model = Ttsd::findOne($_POST[ 'SDNo' ]);
				$transaction->commit();
				return $this->redirect( [ 'ttsd/retur-beli-update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model) ] );
			} catch ( UserException $e ) {
				$transaction->rollBack();
				Yii::$app->session->addFlash( 'error', $e->getMessage() );
				return $this->render( 'update', [
					'model'  => $model,
					'dsBeli' => $_POST,
					'jenis'  => "SuratReturBeli",
					'id'     => $id
				] );
			}
		}
		$dsBeli[ 'Action' ]    = 'Load';
		$dsBeli[ 'KodeTrans' ] = 'SR';
		$result                = Ttsd::find()->callSP( $dsBeli );
		$dsBeli[ 'SDNoView' ]  = $result[ 'SDNo' ];
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->render( 'update', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'jenis'  => "SuratReturBeli",
			'id'     => $id
		] );
	}
	/**
	 * @param Ttsd $model
	 * @param $action
	 *
	 * @throws Exception
	 */
	protected function prosesUpdateReturBeli( $model, $action ) {
//		$_POST[ 'SDNoBaru' ]     = $_POST[ 'SDNo' ];
		$_POST[ 'SDJam' ]        = $_POST[ 'SDTgl' ] . ' ' . $_POST[ 'SDJam' ];
		$_POST[ 'Action' ]       = 'Update';
		$_POST[ 'LokasiKodeSD' ] = $_POST[ 'LokasiKode' ];
		$result                  = Ttsd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$_POST[ 'SDNo' ] = $result['SDNo'];
		$_POST[ 'SDNoView' ] = str_replace( 'SR', $_POST[ 'KodeTrans' ], $result[ 'SDNo' ] );
	}
	/**
	 * Lists all Ttsd models.
	 *
	 * @param $id
	 * @param $action
	 *
	 * @return mixed
	 * @throws Exception
	 * @throws NotFoundHttpException
	 */
	public function actionInvoiceReturBeliUpdate( $id, $action ) {
		$index = \yii\helpers\Url::toRoute( [ 'ttsd/invoice-retur-beli' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttsd $model */
		$model  = $this->findModelBase64( 'Ttsd', $id );
		$dsBeli = Ttsd::find()->dsTMutasiReturBeli()->where( [ 'ttsd.SDNo' => $model->SDNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$transaction = Ttsd::getDb()->beginTransaction();
			try {
				$_POST                = array_merge( $dsBeli, $_POST );
				$_POST[ 'KodeTrans' ] = 'IR';
				$this->prosesUpdateReturBeli( $model, $action );
				$model = Ttsd::findOne($_POST[ 'SDNo' ]);
				$transaction->commit();
				return $this->redirect( [ 'ttsd/invoice-retur-beli-update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
			} catch ( UserException $e ) {
				$transaction->rollBack();
				Yii::$app->session->addFlash( 'error', $e->getMessage() );
				return $this->render( 'update', [
					'model'  => $model,
					'dsBeli' => $dsBeli,
					'jenis'  => "InvoiceReturBeli",
					'id'     => $id
				] );
			}
		}
		$dsBeli[ 'Action' ]    = 'Load';
		$dsBeli[ 'KodeTrans' ] = 'IR';
		$result                = Ttsd::find()->callSP( $dsBeli );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsBeli[ 'SDNoView' ] = str_replace( 'SR', 'IR', $result[ 'SDNo' ] );
		return $this->render( 'update', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'jenis'  => "InvoiceReturBeli",
			'id'     => $id
		] );
	}
	/**
	 * Lists all Ttsd models.
	 * @return mixed
	 */
	public function actionInvoiceReturBeli() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ]    = 'Display';
			$post[ 'StatPrint' ] = '2';
			$post[ 'KodeTrans' ] = 'IR';
			$result              = Ttsd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'invoice-retur-beli' );
	}
	/**
	 * Lists all Ttsd models.
	 * @return mixed
	 */
	public function actionMutasiEksternalDealer() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'KodeTrans' ] = 'ME';
			$post[ 'Action' ]    = 'Display';
			$post[ 'StatPrint' ] = '3';
			$result              = Ttsd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'MutasiEksternalDealer', [
			'mode'  => '',
			'jenis' => 'MutasiEksternal'
		] );
	}
	/**
	 * Lists all Ttsd models.
	 * @return mixed
	 */
	public function actionMutasiEksternalDealerSelect() {
		$this->layout = 'select-mode';
		return $this->render( 'MutasiEksternalDealer', [
			'mode'  => self::MODE_SELECT_DETAIL_MULTISELECT,
			'jenis' => 'MutasiEksternal'
		] );
	}
	/**
	 * Lists all Ttsd models.
	 * @return mixed
	 */
	public function actionInvoiceEksternalDealer() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'KodeTrans' ] = 'IE';
			$post[ 'Action' ]    = 'Display';
			$post[ 'StatPrint' ] = '4';
			$result              = Ttsd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'InvoiceEksternal' );
	}
	/**
	 * Lists all Ttsd models.
	 * @return mixed
	 */
	public function actionPenerimaanDealer() {
		$post[ 'Action' ] = 'Display';
		$result           = Ttsd::find()->callSP( $post );
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'invoice-retur-beli' );
	}
	/**
	 * Lists all Ttsd models.
	 * @return mixed
	 */
	public function actionInvoicePenerimaan() {
		$post[ 'Action' ] = 'Display';
		$result           = Ttsd::find()->callSP( $post );
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'invoice-retur-beli' );
	}
	public function actionMutasiEksternalDealerCreate() {
		TUi::$actionMode   = Menu::ADD;
		$model             = new Ttsd();
		$model->SDNo       = General::createTemporaryNo( "ME", 'ttsd', 'SDNo' );
		$model->SDTgl      = date( 'Y-m-d' );
		$model->SDJam      = date( 'Y-m-d H:i:s' );
		$model->SDMemo     = '--';
		$model->SDTotal    = 0;
		$model->LokasiKode = $this->LokasiKode();
		$model->UserID     = $this->UserID();
		$model->save();
		$post                = $model->attributes;
		$post[ 'Action' ]    = 'Insert';
		$post[ 'KodeTrans' ] = 'ME';
		$result              = Ttsd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsBeli               = Ttsd::find()->dsTMutasiReturBeli()->where( [ 'ttsd.SDNo' => $model->SDNo ] )->asArray( true )->one();
		$id                   = $this->generateIdBase64FromModel( $model );
		$dsBeli[ 'SDNoView' ] = $result[ 'SDNo' ];
		return $this->render( 'mutasi-eksternal-dealer-create', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'jenis'  => "MutasiEksternal",
			'id'     => $id
		] );
	}
	public function actionMutasiEksternalDealerUpdate( $id, $action ) {
		$index = \yii\helpers\Url::toRoute( [ 'ttsd/mutasi-eksternal-dealer' ] );
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttsd $model */
		$model  = $this->findModelBase64( 'Ttsd', $id );
		$dsBeli = Ttsd::find()->dsTMutasiReturBeli()->where( [ 'ttsd.SDNo' => $model->SDNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post                   = array_merge( $dsBeli, $_POST );
			$post[ 'KodeTrans' ]    = 'ME';
			$post[ 'SDJam' ]        = $post[ 'SDTgl' ] . ' ' . $post[ 'SDJam' ];
			$post[ 'LokasiKodeSD' ] = $post[ 'LokasiKode' ];
			$post[ 'SupKode' ]      = $post[ 'DealerKode' ];
			$post[ 'Action' ]       = 'Update';
			$result                 = Ttsd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttsd::findOne( $result[ 'SDNo' ] );
			$id                 = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 ) {
				$post[ 'SDNoView' ] = $result[ 'SDNo' ];
				return $this->redirect( [ 'ttsd/mutasi-eksternal-dealer-update', 'action' => 'display', 'id' => $id ] );
			} else {
				return $this->render( 'mutasi-eksternal-dealer-update', [
					'model'  => $model,
					'dsBeli' => $post,
					'jenis'  => "MutasiEksternal",
					'id'     => $id,
				] );
			}
		}
		$dsBeli[ 'Action' ]    = 'Load';
		$dsBeli[ 'KodeTrans' ] = 'ME';
		$result                = Ttsd::find()->callSP( $dsBeli );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsBeli[ 'SDNoView' ] = $result[ 'SDNo' ];
		return $this->render( 'mutasi-eksternal-dealer-update', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'jenis'  => "MutasiEksternal",
			'id'     => $id
		] );
	}
	public function actionInvoiceEksternalDealerUpdate( $id, $action ) {
		/** @var Ttsd $model */
		$SDNo = str_replace( 'IE', 'ME', base64_decode( $id ) );
		$model  = Ttsd::findOne($SDNo );
		$dsBeli = Ttsd::find()->dsTMutasiReturBeli()->where( [ 'ttsd.SDNo' => $model->SDNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post                   = array_merge( $dsBeli, $_POST );
			$post[ 'KodeTrans' ]    = 'IE';
			$post[ 'SDJam' ]        = $post[ 'SDTgl' ] . ' ' . $post[ 'SDJam' ];
			$post[ 'LokasiKodeSD' ] = $post[ 'LokasiKode' ];
			$post[ 'SupKode' ]      = $post[ 'DealerKode' ];
			$post[ 'Action' ]       = 'Update';
			$result                 = Ttsd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttsd::findOne( $result[ 'SDNo' ] );
			$id                 = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 ) {
				$post[ 'SDNoView' ] = str_replace( 'ME', 'IE', $result[ 'SDNo' ] );
				return $this->redirect( [ 'ttsd/invoice-eksternal-dealer-update', 'action' => 'display', 'id' => $id ] );
			} else {
				return $this->render( 'mutasi-eksternal-dealer-update', [
					'model'  => $model,
					'dsBeli' => $post,
					'jenis'  => "InvoiceEksternal",
					'id'     => $id,
				] );
			}
		}
		$dsBeli[ 'KodeTrans' ] = 'IE';
		$dsBeli[ 'Action' ]    = 'Load';
		$result                = Ttsd::find()->callSP( $dsBeli );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsBeli[ 'SDNoView' ] = str_replace( 'ME', 'IE', $result[ 'SDNo' ] );
		return $this->render( 'mutasi-eksternal-dealer-update', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'jenis'  => "InvoiceEksternal",
			'id'     => $id
		] );
	}
	public function actionInvoiceReturBeliCreate() {
		TUi::$actionMode     = Menu::ADD;
		$model               = new Ttsd();
		$post[ 'Action' ]    = 'Insert';
		$post[ 'KodeTrans' ] = 'IR';
		$result              = Ttsd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsBeli               = Ttsd::find()->dsTMutasiReturBeli()->where( [ 'ttsd.SDNo' => $model->SDNo ] )->asArray( true )->one();
		$id                   = $this->generateIdBase64FromModel( $model );
		$dsBeli[ 'SDNoView' ] = str_replace( 'SR', 'IR', $result[ 'SDNo' ] );
		return $this->render( 'invoice-retur-beli-create', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'jenis'  => "InvoiceReturBeli",
			'id'     => $id
		] );
	}
	/**
	 * @return array
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDetail() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Tmotor::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
			/** @var Tmotor $model */
			$model = null;
			if ( isset( $requestData[ 'id' ] ) ) {
				$model = $this->findModelBase64( 'tmotor', $requestData[ 'id' ] );
			}
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'Action' ]          = 'Insert';
						$requestData[ 'MotorAutoNOLD' ]   = $model->MotorAutoN;
						$requestData[ 'MotorNoMesinOLD' ] = $model->MotorNoMesin;
					} else {
						$requestData[ 'Action' ] = 'Update';
						if ( $model != null ) {
							$requestData[ 'MotorAutoNOLD' ]   = $model->MotorAutoN;
							$requestData[ 'MotorNoMesinOLD' ] = $model->MotorNoMesin;
						}
					}
					$result = Tmotor::find()->callSP_SD( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'del'  :
					$requestData             = array_merge( $requestData, $model->attributes );
					$requestData[ 'Action' ] = 'Delete';
					if ( $model != null ) {
						$requestData[ 'MotorAutoNOLD' ]   = $model->MotorAutoN;
						$requestData[ 'MotorNoMesinOLD' ] = $model->MotorNoMesin;
					}
					$result = Tmotor::find()->callSP_SD( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
			}
		} else {
			$jenis = $_GET['jenis'] ?? '';
			$DealerKode = '';
			if($jenis === 'MutasiEksternal'){
				$DealerKode = $_GET['DealerKode'] ?? '';
				if($DealerKode !=='') $DealerKode .= '.';
			}
			$query = ( new \yii\db\Query() )
				->select( new Expression(
					'CONCAT(' . implode( ',"||",', [ 'tmotor.MotorAutoN', 'tmotor.MotorNoMesin' ] ) . ') AS id,
								tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, 
								tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.SDHarga, tdmotortype.MotorNama'
				) )
				->from( $DealerKode.'tmotor' )
				->join( 'LEFT OUTER JOIN', $DealerKode.'tdmotortype', 'tmotor.MotorType = tdmotortype.MotorType' )
				->join( 'INNER JOIN', $DealerKode.'ttsd', 'tmotor.SDNo = ttsd.SDNo' );
			if ( isset( $_GET[ 'jenis' ] ) ) {
				switch ( $_GET[ 'jenis' ] ) {
					case 'SuratReturBeli':
					case 'InvoiceReturBeli':
					case 'InvoiceEksternal':
					case 'MutasiEksternal':
						$query->where( [
							'ttsd.SDNo' => $requestData[ 'SDNo' ],
						] );
						break;
					case 'FillMotorGridAddEdit':
						$comm      = General::cCmd( "SELECT CONCAT(" . implode( ',"||",', [ 'tmotor.MotorAutoN', 'tmotor.MotorNoMesin' ] ) . ") AS id,
						    tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType, tmotor.MotorWarna, 
						    tmotor.MotorTahun, tmotor.FBHarga, tmotor.SDHarga, tmotor.SSHarga, tmotor.SKHarga, tdmotortype.MotorNama,
                            vmotorLastlokasi.LokasiKode
						FROM
						  tmotor 
						  INNER JOIN vmotorLastlokasi 
						    ON (
						      tmotor.MotorNoMesin = vmotorLastlokasi.MotorNoMesin 
						      AND tmotor.MotorAutoN = vmotorLastlokasi.MotorAutoN
						    ) 
						  LEFT OUTER JOIN tdmotortype 
						    ON tmotor.MotorType = tdmotortype.MotorType 
						  LEFT OUTER JOIN ttdk 
						    ON tmotor.DKNo = ttdk.DKNo 
						  LEFT OUTER JOIN ttsd 
						    ON tmotor.SDNo = ttsd.SDNo 
						WHERE (
						    ttdk.DKNo IS NULL 
						    AND ttsd.SDNo IS NULL
						  ) 
						  AND (
						    vmotorLastlokasi.LokasiKode = :LokasiKode 
						    AND vmotorLastlokasi.Kondisi = 'IN' 
						    AND vmotorLastlokasi.LokasiKode <> 'Repair' 
						    AND vmotorLastlokasi.Jam <= :Jam
						  )	
						UNION
						SELECT CONCAT(" . implode( ',"||",', [ 'tmotor.MotorAutoN', 'tmotor.MotorNoMesin' ] ) . ") AS id,
							tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType, tmotor.MotorWarna, 
							tmotor.MotorTahun, tmotor.FBHarga, tmotor.SDHarga, tmotor.SSHarga, tmotor.SKHarga, tdmotortype.MotorNama, 
							ttsd.LokasiKode AS LokasiKode 
						FROM  tmotor 
						  LEFT OUTER JOIN tdmotortype   ON tmotor.MotorType = tdmotortype.MotorType 
						  INNER JOIN ttsd  ON tmotor.SDNo = ttsd.SDNo 
						WHERE ttsd.SDNo = :SDNo 
						ORDER BY MotorType, MotorWarna, MotorTahun ", [
							':LokasiKode' => $requestData[ 'LokasiKode' ],
							':Jam'        => $requestData[ 'SDTgl' ] . ' ' . $requestData[ 'SDJam' ],
							':SDNo'=> $requestData[ 'SDNo' ]
						] );
						$rowsArray = $comm->queryAll();
						for ( $i = 0; $i < count( $rowsArray ); $i ++ ) {
							$rowsArray[ $i ][ 'id' ] = base64_encode( $rowsArray[ $i ][ 'id' ] );
						}
						$response = [
							'rows'      => $rowsArray,
							'rowsCount' => sizeof( $rowsArray )
						];
						return $response;
						break;
				}
			}
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	/**
	 * Deletes an existing Ttsd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param $KodeTrans
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */

    public function actionDelete()
    {
        /** @var Ttsd $model */
        $request = Yii::$app->request;
		$id = $_REQUEST['id'];
		$model = $this->findModelBase64('Ttsd', $id);
		$kodeTrans = substr($model->SDNo,0,2);
		$indexStat = array_search($kodeTrans, array_column($this::JENIS,'KodeTrans'));
		$StatPrint = $indexStat + 1;
		$_POST['SDNo'] = $model->SDNo;
		$_POST['Action'] = 'Delete';
		$result = Ttsd::find()->callSP($_POST);
        if ($request->isAjax){
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['SDNo'] == '--') {
                return $this->redirect([$this::JENIS[ $StatPrint ][ 'index' ]]);
            } else {
                $model = Ttsd::findOne($result['SDNo']);
                return $this->redirect([$this::JENIS[ $StatPrint ][ 'index' ].'-update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
            }
        }
    }
	public function actionCancel( $id, $action, $StatPrint ) {
		/** @var Ttsd $model */
		$_POST[ 'SDJam' ]     = $_POST[ 'SDTgl' ] . ' ' . $_POST[ 'SDJam' ];
		$_POST[ 'Action' ]    = 'Cancel';
		$_POST[ 'KodeTrans' ] = $this::JENIS[ $StatPrint ][ 'KodeTrans' ];
		$result               = Ttsd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'SDNo' ] == '--' ) {
			return $this->redirect( [ $this::JENIS[ $StatPrint ][ 'index' ]] );
		} else {
			$model = Ttsd::findOne( $result[ 'SDNo' ] );
			return $this->redirect( [ $this::JENIS[ $StatPrint ][ 'index' ].'-update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}
	public function actionJurnal($StatPrint ) {
		$_POST[ 'SDJam' ]     = $_POST[ 'SDTgl' ] . ' ' . $_POST[ 'SDJam' ];
		$_POST[ 'Action' ]    = 'Jurnal';
		$_POST[ 'KodeTrans' ] = $this::JENIS[ $StatPrint ][ 'KodeTrans' ];
		$result               = Ttsd::find()->callSPJurnal( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( [ $this::JENIS[ $StatPrint ][ 'index' ].'-update', 'action' => 'display', 'id' => base64_encode($result['SDNo']) ] );
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST         = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client        = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response      = $client->post( Yii::$app->params[ 'host_report' ] . 'aunit/fsd', [
			'body' => Json::encode( $_POST )
		] );
		$html          = $response->getBody();
		return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
//	public function actionJurnal() {
//		$post             = \Yii::$app->request->post();
//		$post[ 'Action' ] = 'Jurnal';
//		$post[ 'SDJam' ]  = $post[ 'SDTgl' ] . ' ' . $post[ 'SDJam' ];
//		$result           = Ttsd::find()->callSP( $post );
//		if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
//			return $this->responseSuccess( $result[ 'keterangan' ] );
//		} else {
//			return $this->responseFailed( $result[ 'keterangan' ] );
//		}
//	}
	/**
	 * Finds the Ttsd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttsd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttsd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
