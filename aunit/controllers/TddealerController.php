<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Tddealer;

use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use common\components\Custom;
/**
 * TddealerController implements the CRUD actions for Tddealer model.
 */
class TddealerController extends AunitController {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actions() {
		return [
			'td' => [
				'class' => TdAction::className(),
				'model' => Tddealer::className(),
			],
		];
	}
	/**
	 * Lists all Tddealer models.
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render( 'index');
	}
	/**
	 * Displays a single Tddealer model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Creates a new Tddealer model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
        TUi::$actionMode   = Menu::ADD;
		$model = new Tddealer();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'tddealer/update','id'=> $this->generateIdBase64FromModel($model) ,'action' => 'display' ] );
		}
		return $this->render( 'create', [
			'model' => $model,
            'id'    => 'new'
		] );
	}
	/**
	 * Updates an existing Tddealer model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
        $index = \yii\helpers\Url::toRoute( [ 'tddealer/index' ] );
        if ( ! isset( $_GET[ 'action' ] ) ) {
            return $this->redirect( $index );
        }
        $action = $_GET[ 'action' ];
        switch ( $action ) {
            case 'create':
                TUi::$actionMode = Menu::ADD;
                break;
            case 'update':
                TUi::$actionMode = Menu::UPDATE;
                break;
            case 'view':
                TUi::$actionMode = Menu::VIEW;
                break;
            case 'display':
                TUi::$actionMode = Menu::DISPLAY;
                break;
        }
		$model = $this->findModelBase64( 'Tddealer', $id );
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'tddealer/update','id'=> $this->generateIdBase64FromModel($model) ,'action' => 'display' ] );
		}
		return $this->render( 'update', [
			'model' => $model,
            'id'    => $this->generateIdBase64FromModel($model),
		] );
	}
	/**
	 * Deletes an existing Tddealer model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete( $id ) {
        /** @var Tddealer $model */
        $request = Yii::$app->request;
        $model             = $this->findModelBase64( 'Tddealer', $id );
        if ( $model->delete() ) {
            if ($request->isAjax) {
                return $this->responseSuccess( 'Berhasil menghapus data Dealer');
            }
            $model = Tddealer::find()->one();
            return $this->redirect( [ 'tddealer/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );

        } else {
            return $this->responseFailed( 'Gagal menghapus data Dealer' );
        }
	}
	/**
	 * Finds the Tddealer model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Tddealer the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Tddealer::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}

    public function actionCancel( $id, $action ) {
	    $model = Tddealer::findOne( $id );
	    if ( $model == null ) {
		    $model = Tddealer::find()->orderBy( [ 'DealerKode' => SORT_ASC ] )->one();
		    if ( $model == null ) {
			    return $this->redirect( [ 'tddealer/index' ] );
		    }
	    }
	    return $this->redirect( [ 'tddealer/update', 'id' => $this->generateIdBase64FromModel( $model ), 'action' => 'display' ] );
    }
}
