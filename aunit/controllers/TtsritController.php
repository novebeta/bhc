<?php

namespace aunit\controllers;

use aunit\components\AunitController;
use aunit\models\Ttsrit;
use aunit\models\Ttsihd;
use aunit\models\Ttsrhd;
use Yii;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
/**
 * TtsritController implements the CRUD actions for Ttsrit model.
 */
class TtsritController extends AunitController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ttsrit models.
     * @return mixed
     */
    public function actionIndex()
    {
	    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	    $requestData                 = \Yii::$app->request->post();
	    $primaryKeys                 = Ttsrit::getTableSchema()->primaryKey;
	    if ( isset( $requestData[ 'oper' ] ) ) {
		    /* operation : create, edit or delete */
		    $oper = $requestData[ 'oper' ];
            /** @var Ttsrit $model */
            $model = null;
            if ( isset( $requestData[ 'id' ] ) && ( strpos( $requestData[ 'id' ], 'new_row' ) === false ) ) {
                $model = $this->findModelBase64( 'ttsrit', $requestData[ 'id' ] );
            }
		    switch ( $oper ) {
			    case 'add'  :
			    case 'edit' :
                if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
                    $requestData[ 'Action' ] = 'Insert';
                    $requestData[ 'SRAuto' ] = 0;
				    } else {
					    $requestData[ 'Action' ] = 'Update';
                        if ( $model != null ) {
                            $requestData[ 'SRNoOLD' ]   = $model->SRNo;
                            $requestData[ 'SRAutoOLD' ] = $model->SRAuto;
                            $requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
                        }
				    }
				    $result = Ttsrit::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
				    break;
                case 'batch' :
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    $respon                      = [];
                    $base64                      = urldecode( base64_decode( $requestData[ 'header' ] ) );
                    parse_str( $base64, $header );
                    /** @var Ttsihd $SI */
                    $SI = Ttsihd::findOne($header[ 'SINo' ]);
                    if($SI != null){
                        $header[ 'SITgl' ] = $SI->SITgl;
                    }
                    $header[ 'Action' ] = 'Loop';
                    $result             = Ttsrhd::find()->callSP( $header );
                    Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
                    foreach ( $requestData[ 'data' ] as $item ) {
                        $item[ 'Action' ]     = 'Insert';
                        $result                    = Ttsrit::find()->callSP( $item );
                        Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
                    }
	                $header[ 'Action' ] = 'LoopAfter';
	                $result             = Ttsrhd::find()->callSP( $header );
	                Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
	                return $respon;
                    break;
			    case 'del'  :
                    $requestData             = array_merge( $requestData, $model->attributes );
                    $requestData[ 'Action' ] = 'Delete';
                    if ( $model != null ) {
                        $requestData[ 'SRNoLD' ]   = $model->SRNo;
                        $requestData[ 'SRAutoOLD' ] = $model->SRAuto;
                        $requestData[ 'BrgKodeOLD' ] = $model->BrgKode;
                    }
				    $result = Ttsrit::find()->callSP( $requestData );
                    if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
                        $response = $this->responseSuccess( $result[ 'keterangan' ] );
                    } else {
                        $response = $this->responseFailed( $result[ 'keterangan' ] );
                    }
				    break;
		    }
	    } else {
		    for ( $i = 0; $i < count( $primaryKeys ); $i ++ ) {
			    $primaryKeys[ $i ] = 'ttsrit.' . $primaryKeys[ $i ];
		    }
		    $query     = ( new \yii\db\Query() )
			    ->select( new Expression(
				    'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
								ttsrit.SRNo, ttsrit.SRAuto, ttsrit.BrgKode, ttsrit.SRQty, ttsrit.SRHrgBeli, ttsrit.SRHrgJual, 
								ttsrit.SRDiscount, ttsrit.SRPajak, tdbarang.BrgNama, tdbarang.BrgSatuan, 
								IFNULL(ttsrit.SRDiscount / (ttsrit.SRHrgJual * ttsrit.SRQty) * 100,0) AS Disc, 
								ttsrit.SRQty * ttsrit.SRHrgJual - ttsrit.SRDiscount AS Jumlah, ttsrit.LokasiKode, tdbarang.BrgGroup') )
			    ->from( 'ttsrit' )
			    ->join( 'INNER JOIN', 'tdbarang', 'ttsrit.BrgKode = tdbarang.BrgKode' )
			    ->where( [ 'ttsrit.SRNo' => $requestData[ 'SRNo' ] ] )
			    ->orderBy( 'ttsrit.SRNo, ttsrit.SRAuto' );
		    $rowsCount = $query->count();
		    $rows      = $query->all();
		    /* encode row id */
		    for ( $i = 0; $i < count( $rows ); $i ++ ) {
			    $rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
		    }
		    $response = [
			    'rows'      => $rows,
			    'rowsCount' => $rowsCount
		    ];
	    }
	    return $response;
    }

    /**
     * Creates a new Ttsrit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ttsrit();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'SRNo' => $model->SRNo, 'SRAuto' => $model->SRAuto]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ttsrit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $SRNo
     * @param integer $SRAuto
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($SRNo, $SRAuto)
    {
        $model = $this->findModel($SRNo, $SRAuto);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'SRNo' => $model->SRNo, 'SRAuto' => $model->SRAuto]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ttsrit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $SRNo
     * @param integer $SRAuto
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($SRNo, $SRAuto)
    {
        $this->findModel($SRNo, $SRAuto)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ttsrit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $SRNo
     * @param integer $SRAuto
     * @return Ttsrit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($SRNo, $SRAuto)
    {
        if (($model = Ttsrit::findOne(['SRNo' => $SRNo, 'SRAuto' => $SRAuto])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
