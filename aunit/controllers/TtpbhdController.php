<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\models\Tdlokasi;
use aunit\models\Tmotor;
use aunit\models\Ttpbhd;
use aunit\models\Ttpbit;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtpbhdController implements the CRUD actions for Ttpbhd model.
 */
class TtpbhdController extends AunitController {
	public function actions() {
		$colGrid  = null;
		$joinWith = [
			'suratJalan'            => [ 'type' => 'LEFT OUTER JOIN' ],
			'rLokasiAsal lasal'     => [ 'type' => 'LEFT OUTER JOIN' ],
			'rLokasiTujuan ltujuan' => [ 'type' => 'LEFT OUTER JOIN' ]
		];
		$join     = [];
		$where    = [];
//		if ( isset( $_POST['query'] ) ) {
//			$r = Json::decode( $_POST['query'], true )['r'];
//			switch ( $r ) {
//				case 'ttsmhd/mutasi-internal-pos':
//					$colGrid = Ttsmhd::colGridMutasiInternalPos();
//					$where = [
//						[
//							'op'        => 'AND',
//							'condition' => "LEFT(ttsmhd.SMNo,2) = 'MI'",
//							'params'    => []
//						]
//					];
//					break;
//				case 'ttsmhd/mutasi-repair-in':
//					$colGrid = Ttsmhd::colGridMutasiRepairIn();
//					$where = [
//						[
//							'op'        => 'AND',
//							'condition' => "LEFT(ttsmhd.SMNo,2) = 'RI'",
//							'params'    => []
//						]
//					];
//					break;
//				case 'ttsmhd/mutasi-repair-out':
//					$colGrid = Ttsmhd::colGridMutasiRepairOut();
		$where = [
			[
				'op'        => 'AND',
				'condition' => "LEFT(PBNo,2) = 'PP'",
				'params'    => []
			]
		];
//					break;
//			}
//		}
		return [
			'td' => [
				'class'    => TdAction::class,
				'model'    => Ttpbhd::class,
				//				'colGrid'  => $colGrid,
				'joinWith' => $joinWith,
				'join'     => $join,
				'where'    => $where,
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Ttpbhd models.
	 * @return mixed
	 */
	public function actionIndex() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttpbhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'index', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttpbhd/td' ] )
		] );
	}
	/**
	 * Displays a single Ttpbhd model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Finds the Ttpbhd model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttpbhd the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttpbhd::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	/**
	 * Creates a new Ttpbhd model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model               = new Ttpbhd();
		$model->PBNo         = General::createTemporaryNo( "PB", 'ttpbhd', 'PBNo' );
		$model->PBTgl        = date( 'Y-m-d' );
		$model->PBJam        = date( 'Y-m-d H:i:s' );
		$model->PBMemo       = '--';
		$model->SMNo         = '--';
		$model->LokasiAsal   = ( Tdlokasi::find()->where( "LokasiStatus <> 'N'" )->orderBy( "LokasiStatus,  LokasiKode" )->one() )->LokasiKode;
		$model->LokasiTujuan = ( Tdlokasi::find()->where( "LokasiStatus <> 'N' AND LokasiKode <> :LokasiKode", [ ':LokasiKode' => $model->LokasiAsal ] )->orderBy( "LokasiStatus,  LokasiKode" )->one() )->LokasiKode;
		$model->PBTotal      = 0;
		$model->UserID       = $this->UserID();
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttpbhd::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTMutasi               = Ttpbhd::find()->ttpbhdFillByNo()->where( [ 'ttpbhd.PBNo' => $model->PBNo ] )
		                                 ->asArray( true )->one();
		$dsTMutasi[ 'PBNoView' ] = $result[ 'PBNo' ];
		$id                      = $this->generateIdBase64FromModel( $model );
		return $this->render( 'create', [
			'model'     => $model,
			'dsTMutasi' => $dsTMutasi,
			'id'        => $id
		] );
	}
	/**
	 * Updates an existing Ttpbhd model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		/** @var Ttpbhd $model */
		$model     = $this->findModelBase64( 'Ttpbhd', $id );
		$dsTMutasi = Ttpbhd::find()->ttpbhdFillByNo()->where( [ 'ttpbhd.PBNo' => $model->PBNo ] )
		                   ->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post             = array_merge( $dsTMutasi, \Yii::$app->request->post() );
			$post[ 'PBTgl' ]  = date( "Y-m-d" );
			$post[ 'PBJam' ]  = date( "Y-m-d H:i:s" );
			$post[ 'UserID' ] = $this->UserID();
			$post[ 'Action' ] = 'Update';
			$result           = Ttpbhd::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model = Ttpbhd::findOne( $result[ 'PBNo' ] );
			$id    = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 ) {
				$post[ 'PBNoView' ] = $result[ 'PBNo' ];
				return $this->redirect( [ 'ttpbhd/update', 'action' => 'display', 'id' => $id ] );
			} else {
				return $this->render( 'update', [
					'model'     => $model,
					'dsTMutasi' => $post,
					'id'        => $id,
				] );
			}
		}
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTMutasi[ 'Action' ] = 'Load';
			$result                = Ttpbhd::find()->callSP( $dsTMutasi );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		$dsTMutasi[ 'PBNoView' ] = $_GET[ 'PBNoView' ] ?? $result[ 'PBNo' ];
		return $this->render( 'update', [
			'model'     => $model,
			'dsTMutasi' => $dsTMutasi,
			'id'        => $id
		] );
	}
	/**
	 * Deletes an existing Ttpbhd model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
//	public function actionDelete() {
////		$this->findModel( $id )->delete();
////		return $this->redirect( [ 'index' ] );
//		/** @var Ttpbhd $model */
//		$model             = $model = $this->findModelBase64( 'Ttpbhd', $_POST[ 'id' ] );
//		$_POST[ 'PBNo' ]   = $model->PBNo;
//		$_POST[ 'Action' ] = 'Delete';
//		$result            = Ttpbhd::find()->callSP( $_POST );
//		if ( $result[ 'status' ] == 0 ) {
//			return $this->responseSuccess( $result[ 'keterangan' ] );
//		} else {
//			return $this->responseFailed( $result[ 'keterangan' ] );
//		}
//	}

    public function actionDelete()
    {
        /** @var Ttpbhd $model */
        $request = Yii::$app->request;
        if ($request->isAjax){
            $model = $this->findModelBase64('Ttpbhd', $_POST['id']);
            $_POST['PBNo'] = $model->PBNo;
            $_POST['Action'] = 'Delete';
            $result = Ttpbhd::find()->callSP($_POST);
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            $id = $request->get('id');
            $model = $this->findModelBase64('Ttpbhd', $id);
            $_POST['PBNo'] = $model->PBNo;
            $_POST['Action'] = 'Delete';
            $result = Ttpbhd::find()->callSP($_POST);
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['PBNo'] == '--') {
                return $this->redirect(['index']);
            } else {
                $model = Ttpbhd::findOne($result['PBNo']);
                return $this->redirect(['ttpbhd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
            }
        }
    }

	public function actionDetail() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Tmotor::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper  = $requestData[ 'oper' ];
			$model = null;
			if ( isset( $requestData[ 'id' ] ) ) {
				$model = $this->findModelBase64( 'tmotor', $requestData[ 'id' ] );
			}
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'Action' ] = 'Insert';
						//						$requestData[ 'MotorAutoN' ] = 0;
						$requestData[ 'MotorAutoNOLD' ]   = $model->MotorAutoN;
						$requestData[ 'MotorNoMesinOLD' ] = $model->MotorNoMesin;
					} else {
						$requestData[ 'Action' ] = 'Update';
						if ( $model != null ) {
							$requestData[ 'MotorAutoNOLD' ]   = $model->MotorAutoN;
							$requestData[ 'MotorNoMesinOLD' ] = $model->MotorNoMesin;
						}
					}
					$result = Ttpbit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'batch' :
					\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
					$respon                      = [];
					if ( sizeof( $requestData[ 'data' ] ) == 0 ) {
						return $respon;
					}
//					$base64                      = urldecode( base64_decode( $requestData[ 'header' ] ) );
//					parse_str( $base64, $header );
					$header                        = $requestData[ 'header' ];
					$requestData[ 'SMNo' ]         = $requestData[ 'data' ][ 0 ][ 'header' ][ 'id' ];
					$requestData[ 'PBNo' ]         = $header[ 'PBNo' ];
					$requestData[ 'LokasiAsal' ]   = $requestData[ 'data' ][ 0 ][ 'header' ][ 'data' ][ 'LokasiAsal' ];
					$requestData[ 'LokasiTujuan' ] = $requestData[ 'data' ][ 0 ][ 'header' ][ 'data' ][ 'LokasiTujuan' ];
					$requestData[ 'Action' ]       = 'Loop';
					$resultpb                      = Ttpbhd::find()->callSP( $requestData );
//					$respon[ 'notif' ][] = $resultpb;
					foreach ( $requestData[ 'data' ] as $item ) {
						$item[ 'data' ][ 'Action' ]          = 'Insert';
						$item[ 'data' ][ 'PBNo' ]            = $header[ 'PBNo' ];
						$item[ 'data' ][ 'MotorAutoNOLD' ]   = $item[ 'data' ][ 'MotorAutoN' ];
						$item[ 'data' ][ 'MotorNoMesinOLD' ] = $item[ 'data' ][ 'MotorNoMesin' ];
						$result                              = Ttpbit::find()->callSP( $item[ 'data' ] );
//						$respon[ 'notif' ][] = $result;
//					    } else {
//						$response = $this->responseSuccess( $result[ 'keterangan' ] );
//						if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
//						} else {
//							$response = $this->responseFailed( $result[ 'keterangan' ] );
//						}
					}
					return $this->responseSuccess( '' );
					break;
				case 'del'  :
					$requestData             = array_merge( $requestData, $model->attributes );
					$requestData[ 'Action' ] = 'Delete';
					if ( $model != null ) {
						$requestData[ 'MotorAutoNOLD' ]   = $model->MotorAutoN;
						$requestData[ 'MotorNoMesinOLD' ] = $model->MotorNoMesin;
					}
					$result = Ttpbit::find()->callSP( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
			}
		} else {
			$query     = ( new \yii\db\Query() )
				->select( new Expression(
					'CONCAT(' . implode( ',"||",', [ 'tmotor.MotorAutoN', 'tmotor.MotorNoMesin' ] ) . ') AS id,
								 ttpbit.PBNo, ttpbit.MotorNoMesin, ttpbit.MotorAutoN, tmotor.MotorType, tmotor.MotorTahun, 
								 tmotor.MotorWarna, tmotor.MotorNoRangka, tmotor.FBHarga,tdmotortype.MotorNama'
				) )
				->from( 'ttpbit' )
				->join( 'INNER JOIN', 'tmotor', 'ttpbit.MotorNoMesin = tmotor.MotorNoMesin AND ttpbit.MotorAutoN = tmotor.MotorAutoN' )
				->join( 'LEFT OUTER JOIN', 'tdmotortype', 'tmotor.MotorType = tdmotortype.MotorType' )
				->where( [ 'ttpbit.PBNo' => $requestData[ 'PBNo' ] ] );
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST         = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client        = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response      = $client->post( Yii::$app->params[ 'host_report' ] . '/aunit/fpbhd', [
			'body' => Json::encode( $_POST )
		] );
		return $response->getBody();
	}
	public function actionCancel( $id ) {
		/** @var Ttpbhd $model */
		$_POST[ 'Action' ] = 'Cancel';
		$result            = Ttpbhd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'PBNo' ] == '--' ) {
			return $this->redirect( [ 'index' ] );
		} else {
			$model = Ttpbhd::findOne( $result[ 'PBNo' ] );
			return $this->redirect( [ 'ttpbhd/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}
	public function actionJurnal() {
		$_POST[ 'Action' ] = 'Jurnal';
		$result            = Ttpbhd::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( [ 'ttpbhd/update', 'action' => 'display', 'id' => base64_encode( $result[ 'PBNo' ] ) ] );
	}
}
