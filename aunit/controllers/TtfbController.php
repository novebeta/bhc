<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Tdsupplier;
use aunit\models\Tmotor;
use aunit\models\Ttfb;
use common\components\Api;
use common\components\General;
use GuzzleHttp\Client;
use Yii;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
class TtfbController extends AunitController {
	public function actions() {
		$queryRaw   = null;
		$queryRawPK = null;
		$colGrid    = Ttfb::colGrid();
        $where      = [
            [
                'op'        => 'AND',
                'condition' => "(ttfb.FBNo NOT LIKE '%=%')",
                'params'    => []
            ]
        ];
		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			if ( isset( $urlParams[ 'jenis' ] ) ) {
				switch ( $urlParams[ 'jenis' ] ) {
					case 'SS':
						$whereSql = '';
                        $where      = [
                            [
                                'op'        => 'AND',
                                'condition' => "(FBNo NOT LIKE '%=%')",
                                'params'    => []
                            ]
                        ];
						$q        = Json::decode( $_POST[ 'query' ], true );
						if ( $q[ 'cmbTxt1' ] == 'ttfb.FBNo' && ( $q[ 'txt1' ] !== '' || $q[ 'txt1' ] !== '--' ) ) {
							$whereSql = "OR ttfb.FBNo = '{$q['txt1']}'";
						}
						$queryRawPK = [ "FBNo" ];
						$queryRaw   = "SELECT ttfb.FBNo, ttfb.FBTgl, ttfb.SSNo, ttfb.SupKode, ttfb.FBTermin, 
	                            ttfb.FBTglTempo, ttfb.FBPSS, ttfb.FBPtgLain, ttfb.FBTotal, ttfb.FBMemo, ttfb.FBLunas, 
	                            ttfb.UserID, IFNULL(tdsupplier.SupNama, '--') AS SupNama, IFNULL(ttss.SSTgl, 
	                            DATE('1900-01-01')) AS SSTgl, IFNULL(tmotor.FBJum, 0) AS FBJum, 
	                            IFNULL(tmotor.FBSubTotal, 0) AS FBSubTotal 
							FROM  ttfb 
							  LEFT OUTER JOIN ttss ON ttfb.SSNo = ttss.SSNo 
							  LEFT OUTER JOIN tdsupplier  ON ttfb.SupKode = tdsupplier.SupKode 
							  LEFT OUTER JOIN   
							  (SELECT COUNT(MotorType) AS FBJum, SUM(FBHarga) AS FBSubTotal, FBNo FROM tmotor tmotor_1 
							  WHERE SSNo = '--' GROUP BY FBNo) tmotor  ON ttfb.FBNo = tmotor.FBNo 
							WHERE ttfb.FBNo LIKE '%%' 
							  AND (IFNULL(tmotor.FBJum, 0) > 0 $whereSql) 
							ORDER BY ttFB.FBNo";
						break;
				}
			}
			switch ( $r ) {
				case 'ttfb/select-bank-keluar-bayar-hutang-unit':
					$colGrid  = Ttfb::colGridBankKeluarBayarHutangUnit();
                    $where      = [
                        [
                            'op'        => 'AND',
                            'condition' => "(FBNo NOT LIKE '%=%')",
                            'params'    => []
                        ]
                    ];
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$queryRaw   = "SELECT ttfb.FBNo, ttfb.FBTgl, ttfb.SSNo, ttfb.SupKode, ttfb.FBTermin, ttfb.FBTglTempo, ttfb.FBPSS, 
			         ttfb.FBPtgLain, ttfb.FBTotal, ttfb.FBMemo, ttfb.FBLunas, ttfb.UserID, 
			         IFNULL(tdsupplier.SupNama, '--') AS SupNama, IFNULL(ttss.SSTgl, DATE('1900-01-01')) AS SSTgl, 
			         0.00 As Sisa, IFNULL(BKPaid.Paid,0.00) Terbayar, ttfb.FBTotal - IFNULL(BKPaid.Paid,0.00) AS BKBayar 
			         FROM ttfb 
			         INNER JOIN tdsupplier ON ttfb.SupKode = tdsupplier.SupKode 
			         LEFT OUTER JOIN ttss ON ttfb.SSNo = ttss.SSNo 
			         LEFT OUTER JOIN 
			         (SELECT KKLINK As FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'Bayar Unit') 
			         GROUP BY KKLINK) BKPaid 
			         ON ttfb.FBNo = BKPaid.FBNo 
			         WHERE (ttfb.FBTotal - IFNULL(BKPaid.Paid, 0) > 0) 
			         {$whereRaw}";
					$queryRawPK = [ 'FBNo' ];
					break;
				case 'ttfb/select-kas-keluar-bayar-hutang-unit':
					$colGrid  = Ttfb::colGridKasKeluarBayarHutangUnit();
                    $where      = [
                        [
                            'op'        => 'AND',
                            'condition' => "(FBNo NOT LIKE '%=%')",
                            'params'    => []
                        ]
                    ];
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$queryRaw   = " SELECT ttfb.FBNo, ttfb.FBTgl, ttfb.SSNo, ttfb.SupKode, ttfb.FBTermin, ttfb.FBTglTempo, ttfb.FBPSS, 
			         ttfb.FBPtgLain, ttfb.FBTotal, ttfb.FBMemo, ttfb.FBLunas, ttfb.UserID, 
			         IFNULL(tdsupplier.SupNama, '--') AS SupNama, IFNULL(ttss.SSTgl, DATE('1900-01-01')) AS SSTgl, 
			         0.00 As Sisa, IFNULL(KKPaid.Paid,0) Terbayar, ttfb.FBTotal - IFNULL(KKPaid.Paid,0.00) AS KKBayar 
			         FROM ttfb 
			         INNER JOIN tdsupplier ON ttfb.SupKode = tdsupplier.SupKode 
			         LEFT OUTER JOIN ttss ON ttfb.SSNo = ttss.SSNo 
			         LEFT OUTER JOIN 
			         (SELECT KKLINK As FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'Bayar Unit') GROUP BY KKLINK) KKPaid 
			         ON ttfb.FBNo = KKPaid.FBNo 
			         WHERE (ttfb.FBTotal - IFNULL(KKPaid.Paid, 0) > 0)  
			         {$whereRaw}";
					$queryRawPK = [ 'FBNo' ];
					break;
			}
		}
		return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Ttfb::class,
				'queryRaw'   => $queryRaw,
				'queryRawPK' => $queryRawPK,
				'colGrid'    => $colGrid,
                'where'      => $where,
				'joinWith'   => [
					'suratSupp' => [
						'type' => 'LEFT OUTER JOIN'
					],
					'supplier'  => [
						'type' => 'LEFT OUTER JOIN'
					],
				],
				'join'       => [
					[
						'type'   => 'LEFT OUTER JOIN',
						'table'  => '(SELECT COUNT(MotorType) AS FBJum, SUM(FBHarga) AS FBSubTotal, FBNo FROM tmotor tmotor_1 GROUP BY FBNo) tmotor',
						'on'     => 'ttfb.FBNo = tmotor.FBNo',
						'params' => []
					]
				]
			],
		];
	}
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionIndex() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttfb::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'index', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttfb/create', 'action' => 'create' ] )
		] );
	}
	public function actionSelect() {
		$this->layout = 'select-mode';
		return $this->render( 'index', [
			'mode'    => self::MODE_SELECT_DETAIL_MULTISELECT,
			'url_add' => Url::toRoute( [ 'ttfb/td', 'jenis' => $_GET[ 'jenis' ] ] )
		] );
	}
	public function actionSelectBankKeluarBayarHutangUnit() {
		$this->layout = 'select-mode';
		return $this->render( 'select', [
			'title'   => 'Bank Keluar Bayar Hutang Unit',
			'arr'     => [
				'cmbTxt'    => [
					'FBNo'    => 'No Faktur Beli',
					'SSNo'    => 'No Surat Jalan Supplier',
					'SupKode' => 'Supplier',
					'SupNama' => 'Nama Supplier',
					'FBMemo'  => 'Keterangan',
					'FBLunas' => 'Lunas',
					'UserID'  => 'User ID',
				],
				'cmbTgl'    => [
					'FBTgl'      => 'Tgl FB',
					'FBTglTempo' => 'Tgl Tempo',
					'SSTgl'      => 'Tgl Surat Jalan',
				],
				'cmbNum'    => [
					'FBTotal'    => 'Total',
					'FBTermin'   => 'Termin',
					'fbJum'      => 'Jumlah Motor',
					'FBPSS'      => 'Potongan Supplier',
					'FBPtgLain'  => 'Potangan Lain Lain',
					'FBSubTotal' => 'Sub Total',
				],
				'sortname'  => "FBTgl",
				'sortorder' => 'desc'
			],
			'options' => [
				'colGrid'=> Ttfb::colGridBankKeluarBayarHutangUnit(),
				'mode' => self::MODE_SELECT_MULTISELECT,
			]
		] );
	}
	public function actionSelectKasKeluarBayarHutangUnit() {
		$this->layout = 'select-mode';
		return $this->render( 'select', [
			'title'   => 'Bank Keluar Bayar Hutang Unit',
			'arr'     => [
				'cmbTxt'    => [
					'FBNo'    => 'No Faktur Beli',
					'SSNo'    => 'No Surat Jalan Supplier',
					'SupKode' => 'Supplier',
					'SupNama' => 'Nama Supplier',
					'FBMemo'  => 'Keterangan',
					'FBLunas' => 'Lunas',
					'UserID'  => 'User ID',
				],
				'cmbTgl'    => [
					'FBTgl'      => 'Tgl FB',
					'FBTglTempo' => 'Tgl Tempo',
					'SSTgl'      => 'Tgl Surat Jalan',
				],
				'cmbNum'    => [
					'FBTotal'    => 'Total',
					'FBTermin'   => 'Termin',
					'fbJum'      => 'Jumlah Motor',
					'FBPSS'      => 'Potongan Supplier',
					'FBPtgLain'  => 'Potangan Lain Lain',
					'FBSubTotal' => 'Sub Total',
				],
				'sortname'  => "FBTgl",
				'sortorder' => 'desc'
			],
			'options' => [
				'colGrid'=> Ttfb::colGridKasKeluarBayarHutangUnit(),
				'mode' => self::MODE_SELECT_MULTISELECT,
			]
		] );
	}


    public function actionImport()
    {
        $this->layout = 'select-mode';
        return $this->render('import', [
            'colGrid' => Ttfb::colGridImportItems(),
            'mode' => self::MODE_SELECT_DETAIL_MULTISELECT,
        ]);
    }

    public function actionImportItems()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $fromTime = Yii::$app->formatter->asDate('-90 day', 'yyyy-MM-dd');
        $toTime = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
		$addParam = [];
        if (isset($_POST['query'])) {
            $query = Json::decode($_POST['query'], true);
            $fromTime = $query['tgl1'] ?? $fromTime;
            $toTime = $query['tgl2'] ?? $toTime;
			if($query['txt1'] !== ''){
				$addParam = array_merge_recursive($addParam,[$query['cmbTxt1']=> $query['txt1']]);
			}
        }
        $api = new Api();
        $mainDealer = $api->getMainDealer();
        $arrHasil = [];
        switch ($mainDealer) {
            case API::MD_HSO:
                $arrParams = [
                    'fromTime' => $fromTime . ' 00:00:00',
                    'toTime' => $toTime . ' 23:59:59',
                    'dealerId' => $api->getDealerId(),
                ];

                break;
            case API::MD_ANPER:
                $arrParams = [
                    'fromTime' => $fromTime . ' 00:00:00',
                    'toTime' => $toTime . ' 23:59:59',
                    'dealerId' => $api->getDealerId(),
                    'requestServerTime' => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                    'requestServertimezone' => date('T'),
                    'requestServerTimestamp' => $api->getCurrUnixTime(),
                ];
                break;
			default:
				$arrParams = [
					'fromTime'               => $fromTime . ' 00:00:00',
					'toTime'                 => $toTime . ' 23:59:59',
					'dealerId'               => $api->getDealerId(),
					'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
					'requestServertimezone'  => date('T'),
					'requestServerTimestamp' => $api->getCurrUnixTime(),
				];
        }
		$arrParams = array_merge_recursive($arrParams,$addParam);
		$filterResult = [];
		if($query['txt2'] !== ''){
			$filterResult = [
				'key' => $query['cmbTxt2'],
				'value' => $query['txt2'],
			];
		}
        $hasil = $api->call(Api::TYPE_UINB, Json::encode($arrParams),$filterResult);
        $arrHasil = Json::decode($hasil);
        if (!isset($arrHasil['data'])) {
            return [
                'rows' => [],
                'rowsCount' => 0
            ];
        }
        $_SESSION['ttfb']['import'] = $arrHasil['data'];
        return [
            'rows' => $arrHasil['data'],
            'rowsCount' => sizeof($arrHasil['data'])
        ];
    }

    public function actionImportDetails()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $key = array_search($_POST['noShippingList'], array_column($_SESSION['ttfb']['import'], 'noShippingList'));
        return [
            'rows' => $_SESSION['ttfb']['import'][$key]['unit'],
            'rowsCount' => sizeof($_SESSION['ttfb']['import'][$key]['unit'])
        ];
    }

	public function actionJurnal() {
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Jurnal';
		$post[ 'FBJam' ]  = $post[ 'FBTgl' ] . ' ' . $post[ 'FBJam' ];
		$result           = Ttfb::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( [ 'ttfb/update',  'action' => 'display','id' =>  base64_encode($result['FBNo'])] );
	}
	public function actionCreate() {
        TUi::$actionMode   = Menu::ADD;
		$model              = new Ttfb();
		$model->FBNo        = General::createTemporaryNo( "FB", 'ttfb', 'FBNo' );
		$model->FBTgl       = date( 'Y-m-d' );
		$model->FBJam       = date( 'Y-m-d H:i:s' );
		$model->DOTgl       = date( 'Y-m-d' );
		$model->FBTglTempo  = date( 'Y-m-d' );
		$model->SSNo        = '--';
		$model->FBMemo      = '--';
		$model->SupKode      = Tdsupplier::findOne(['SupStatus'=>1])->SupKode ?? '--';
		$model->FBTermin    = 0;
		$model->FBPSS       = 0;
		$model->FBPtgLain   = 0;
		$model->FBTotal     = 0;
		$model->FBExtraDisc = 0;
		$model->FBPtgMD     = 0;
		$model->FBPPNm      = 0;
		$model->FBLunas     = "Belum";
		$model->UserID      = $this->UserID();
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttfb::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsBeli                 = Ttfb::find()->dsTBeliFillByNo()->where( [ 'ttfb.FBNo' => $model->FBNo ] )->asArray( true )->one();
		$dsBeli[ 'FBNoView' ]   = $result[ 'FBNo' ];
		$dsBeli[ 'FBJum' ]      = 0;
		$dsBeli[ 'FBSubTotal' ] = 0;
		$dsBeli[ 'FBPtgTotal' ] = 0;
		$dsBeli[ 'FPTgl' ]      = "1900-01-01";
		$dsBeli[ 'FPNo' ]       = "--";
		$id                     = $this->generateIdBase64FromModel( $model );
		return $this->render( 'create', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'id'     => $id
		] );
	}
	public function actionUpdate( $id ) {
        $index = \yii\helpers\Url::toRoute( [ 'ttfb/index' ] );
        if ( ! isset( $_GET[ 'action' ] ) ) {
            return $this->redirect( $index );
        }
        $action = $_GET[ 'action' ];
        switch ( $action ) {
            case 'create':
                TUi::$actionMode = Menu::ADD;
                break;
            case 'update':
                TUi::$actionMode = Menu::UPDATE;
                break;
            case 'view':
                TUi::$actionMode = Menu::VIEW;
                break;
            case 'display':
                TUi::$actionMode = Menu::DISPLAY;
                break;
        }
		/** @var Ttfb $model */
		$model  = $this->findModelBase64( 'Ttfb', $id );
		$dsBeli = Ttfb::find()->dsTBeliFillByNo()->where( [ 'ttfb.FBNo' => $model->FBNo ] )->asArray( true )->one();
		if ( Yii::$app->request->isPost ) {
			$post = array_merge( $dsBeli, \Yii::$app->request->post() );
			$post[ 'FBJam' ]  = $post[ 'FBTgl' ] . ' ' . $post[ 'FBJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttfb::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model = Ttfb::findOne( $result[ 'FBNo' ] );
			$id    = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null && $post[ 'SaveMode' ] == 'ALL' ) {
                return $this->redirect( [ 'ttfb/update',  'action' => 'display','id' => $id ] );
			} else {
				return $this->render( 'update', [
					'model'  => $model,
					'dsBeli' => $post,
					'id'     => $id,
				] );
			}
		}
		$dsBeli[ 'Action' ] = 'Load';
		$result             = Ttfb::find()->callSP( $dsBeli );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsBeli[ 'FBNoView' ] = $_GET[ 'FBNoView' ] ?? $dsBeli[ 'FBNo' ];
		return $this->render( 'update', [
			'model'  => $model,
			'dsBeli' => $dsBeli,
			'id'     => $id
		] );
	}
//	public function actionDelete() {
//		/** @var Ttfb $model */
//		$model             = $model = $this->findModelBase64( 'Ttfb', $_POST[ 'id' ] );
//		$_POST[ 'FBNo' ]   = $model->FBNo;
//		$_POST[ 'Action' ] = 'Delete';
//		$result            = Ttfb::find()->callSP( $_POST );
//		if ( $result[ 'status' ] == 0 ) {
//			return $this->responseSuccess( $result[ 'keterangan' ] );
//		} else {
//			return $this->responseFailed( $result[ 'keterangan' ] );
//		}
//	}
    public function actionDelete()
    {
        /** @var Ttsd $model */
        $request = Yii::$app->request;
        if ($request->isAjax){
            $model = $this->findModelBase64('Ttfb', $_POST['id']);
            $_POST['FBNo'] = $model->FBNo;
            $_POST['Action'] = 'Delete';
            $result = Ttfb::find()->callSP($_POST);
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            $id = $request->get('id');
            $model = $this->findModelBase64('Ttfb', $id);
            $_POST['FBNo'] = $model->FBNo;
            $_POST['Action'] = 'Delete';
            $result = Ttfb::find()->callSP($_POST);
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ($result['FBNo'] == '--') {
                return $this->redirect(['index']);
            } else {
                $model = Ttfb::findOne($result['FBNo']);
                return $this->redirect(['ttfb/update', 'action' => 'display', 'id' => $this->generateIdBase64FromModel($model)]);
            }
        }
    }

	public function actionDetail() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$requestData                 = \Yii::$app->request->post();
		$primaryKeys                 = Tmotor::getTableSchema()->primaryKey;
		if ( isset( $requestData[ 'oper' ] ) ) {
			/* operation : create, edit or delete */
			$oper = $requestData[ 'oper' ];
			/** @var Tmotor $model */
			$model = null;
			if ( isset( $requestData[ 'id' ] ) ) {
				$model = $this->findModelBase64( 'tmotor', $requestData[ 'id' ] );
			}
			switch ( $oper ) {
				case 'add'  :
				case 'edit' :
					if ( strpos( $requestData[ 'id' ], 'new_row' ) !== false ) {
						$requestData[ 'Action' ]     = 'Insert';
						$requestData[ 'MotorAutoN' ] = 0;
					} else {
						$requestData[ 'Action' ] = 'Update';
						if ( $model != null ) {
							$requestData[ 'MotorAutoNOLD' ]   = $model->MotorAutoN;
							$requestData[ 'MotorNoMesinOLD' ] = $model->MotorNoMesin;
						}
					}
					$result = Tmotor::find()->callSP_FB( $requestData );
					if ( $result[ 'status' ] == 0 ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
				case 'batch' :
					\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
					$isSameID                    = true;
					$respon                      = [];
					$base64                      = urldecode( base64_decode( $requestData[ 'header' ] ) );
					parse_str( $base64, $header );
					$header[ 'FBNo' ]  = $header[ 'FBNoLama' ];
					$header[ 'FBJam' ] = $header[ 'FBTgl' ] . ' ' . $header[ 'FBJam' ];
					if ( false ) {
						if ( strpos( $header[ 'FBNoLama' ], '=' ) !== false && str_replace( '=', '-', $header[ 'FBNoLama' ] ) == $header[ 'FBNo' ] ) {
							$header[ 'FBNoBaru' ] = General::createNo( 'FB', 'ttfb', 'FBNo' );
						} else {
							$header[ 'FBNoBaru' ] = $header[ 'FBNo' ];
						}
						$header[ 'Action' ] = 'Update';
						$result             = Ttfb::find()->callSP( $header );
						$isSameID           = $result[ 'FBNo' ] == $header[ 'FBNo' ];
						if ( $isSameID ) {
							$respon[ 'notif' ][] = $result;
						} else {
							Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
						}
					}
					foreach ( $requestData[ 'data' ] as $item ) {
						$item[ 'Action' ]          = 'Loop';
						$item[ 'FBNo' ]            = $header[ 'FBNo' ];
						$item[ 'MotorAutoNOLD' ]   = $item[ 'MotorAutoN' ];
						$item[ 'MotorNoMesinOLD' ] = $item[ 'MotorNoMesin' ];
						$result                    = Tmotor::find()->callSP_FB( $item );
						if ( $isSameID ) {
							$respon[ 'notif' ][] = $result;
						} else {
							Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
						}
					}
					return $respon;
					break;
				case 'import' :
						\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
						$respon = [];
						$base64 = urldecode(base64_decode($requestData['header']));
						parse_str($base64, $header);
						$header[ 'FBNo' ]  = $header[ 'FBNoLama' ];
						$header[ 'FBJam' ] = $header[ 'FBTgl' ] . ' ' . $header[ 'FBJam' ];              
						$requestData['data'][0]['header']['data']['Action'] = 'ImporBefore';
						$requestData['data'][0]['header']['data']['NoTrans'] = $header['FBNo'];
						$result = Api::callUINB($requestData['data'][0]['header']['data']);
						// $isSameID = $result['SSNo'] == $header['SSNo'];
						Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
						$model = Ttfb::findOne($result['NoTrans']);
						$id = $this->generateIdBase64FromModel($model);
						$respon['id'] = $id;
						if($result['status'] == 1){
							return $respon;
						}
						foreach ($requestData['data'] as $item) {
							$item['data']['Action'] = 'Impor';
							$item['data']['NoTrans'] = $model->FBNo;
							$result = Api::callUINBit($item['data']);
							Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
						}
						$requestData['data'][0]['header']['data']['Action'] = 'ImporAfter';
						$result = Api::callUINB($requestData['data'][0]['header']['data']);
						Yii::$app->session->addFlash(($result['status'] == 0) ? 'success' : 'warning', $result['keterangan']);
						return $respon;
						break;
				case 'del'  :
					$requestData             = array_merge( $requestData, $model->attributes );
					$requestData[ 'Action' ] = 'Delete';
					if ( $model != null ) {
						$requestData[ 'MotorAutoNOLD' ]   = $model->MotorAutoN;
						$requestData[ 'MotorNoMesinOLD' ] = $model->MotorNoMesin;
					}
					$result = Tmotor::find()->callSP_FB( $requestData );
					if ( $result[ 'status' ] == 0 && $result[ 'status' ] !== null ) {
						$response = $this->responseSuccess( $result[ 'keterangan' ] );
					} else {
						$response = $this->responseFailed( $result[ 'keterangan' ] );
					}
					break;
			}
		} else {
			/* operation : read */
			$query = ( new \yii\db\Query() )
				->select( [
					'CONCAT(' . implode( ",'||',", $primaryKeys ) . ') AS id',
					'tmotor.*'
				] )
				->from( Tmotor::tableName() )
				->where( [
					'FBNo' => $requestData[ 'FBNo' ]
				] )
				->orderBy( 'MotorType' );
			if ( isset( $_GET[ 'jenis' ] ) ) {
				switch ( $_GET[ 'jenis' ] ) {
					case 'SS':
						$query = ( new \yii\db\Query() )
							->select( new Expression(
								'CONCAT(' . implode( ',"||",', $primaryKeys ) . ') AS id,
								BPKBNo, BPKBTglJadi, DKNo, FBHarga, FBNo, FakturAHMNo, FakturAHMTgl,
								MotorNoMesin, MotorNoRangka, MotorTahun, MotorType, MotorWarna, PDNo, PlatNo,
								PlatTgl, SDNo, SKNo, SSNo, STNKAlamat, STNKNama, STNKNo, STNKTglBayar, STNKTglJadi, STNKTglMasuk, MotorAutoN'
							) )
							->from( 'tmotor' )
							->where( [
								'FBNo' => $requestData[ 'FBNo' ],
								'SSNo' => '--'
							] )
							->orderBy( 'SSNo DESC, MotorType, MotorTahun, MotorWarna' );
						break;
				}
			}
			$rowsCount = $query->count();
			$rows      = $query->all();
			/* encode row id */
			for ( $i = 0; $i < count( $rows ); $i ++ ) {
				$rows[ $i ][ 'id' ] = base64_encode( $rows[ $i ][ 'id' ] );
			}
			$response = [
				'rows'      => $rows,
				'rowsCount' => $rowsCount
			];
		}
		return $response;
	}
	public function actionCancel( $id, $action ) {
		/** @var Ttfb $model */
		$_POST[ 'FBJam' ]  = $_POST[ 'FBTgl' ] . ' ' . $_POST[ 'FBJam' ];
		$_POST[ 'Action' ] = 'Cancel';
		$result            = Ttfb::find()->callSP( $_POST );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if($result[ 'FBNo' ] == '--'){
			return $this->redirect(['index']);
		}else{
			$model = Ttfb::findOne($result[ 'FBNo' ]);
			return $this->redirect( [ 'ttfb/update',  'action' => 'display','id' => $this->generateIdBase64FromModel( $model ) ] );
		}
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$_POST         = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client        = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response      = $client->post( Yii::$app->params[ 'host_report' ] . '/aunit/ffb', [
			'body' => Json::encode( $_POST )
		] );
		$html          = $response->getBody();
		return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	protected function findModel( $id ) {
		if ( ( $model = Ttfb::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	protected function CekDataGrid( $detil ) {
		$pesanNoMesin  = [];
		$pesanNoRangka = [];
		$pesanHarga    = [];
		foreach ( $detil as $row ) {
			if ( $row[ 'SSHarga' ] == 0 ) {
				$pesanHarga[] = $row[ 'MotorType' ];
			}
			/*if ( strlen( $row[ 'MotorNoMesin' ] ) < 12 ) {
				$pesanNoMesin[] = $row[ 'MotorNoMesin' ];
			}
			if ( strlen( $row[ 'MotorNoRangka' ] ) < 17 ) {
				$pesanNoRangka[] = $row[ 'MotorNoRangka' ];
			}*/
		}
		$msg = '';
		/*
		if ( sizeof( $pesanNoMesin ) > 0 ) {
			$msg = "Berikut No Mesin yang kurang dari 12 karakter : " . implode( ', ', $pesanNoMesin );
		}
		if ( sizeof( $pesanNoRangka ) > 0 ) {
			$msg = "Berikut No Rangka yang kurang dari 17 karakter : " . implode( ', ', $pesanNoRangka );
		}*/
		if ( sizeof( $pesanHarga ) > 0 ) {
			$msg = "Berikut Type Motor yang belum memiliki HPP : " . implode( ', ', $pesanHarga ) . "\n Silahkan memberi nilai HPP melalui Menu Data - Type & Warna Motor";
		}
		return $msg;
	}
}
