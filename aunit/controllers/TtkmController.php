<?php
namespace aunit\controllers;
use aunit\components\AunitController;
use aunit\components\Menu;
use aunit\components\TdAction;
use aunit\components\TUi;
use aunit\models\Ttdk;
use aunit\models\Ttkm;
use common\components\Custom;
use common\components\General;
use GuzzleHttp\Client;
use PHPUnit\Util\Log\JSON as LogJSON;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
/**
 * TtkmController implements the CRUD actions for Ttkm model.
 */
class TtkmController extends AunitController {
	public function actions() {
		$colGrid    = null;
		$joinWith   = [];
		$join       = [];
		$where      = [];
		$sqlraw     = null;
		$queryRawPK = null;
		if ( isset( $_POST[ 'query' ] ) ) {
			$json      = Json::decode( $_POST[ 'query' ], true );
			$r         = $json[ 'r' ];
			$urlParams = $json[ 'urlParams' ];
			switch ( $r ) {
				case 'ttkm/kas-masuk-inden':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'Inden' AND ttkm.KMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-pelunasan-leasing':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'Leasing' AND ttkm.KMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-uang-muka-kredit':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'Kredit' AND ttkm.KMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-bayar-piutang-kon-um':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'Piutang UM' AND ttkm.KMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-uang-muka-tunai':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'Tunai' AND ttkm.KMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-bayar-sisa-piutang':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'Piutang Sisa' AND ttkm.KMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-bbn-progresif':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'BBN Plus' AND ttkm.KMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-bbn-acc':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'BBN Acc' AND ttkm.KMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-dari-bank':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'KM Dari Bank' AND ttkm.KMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-umum':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'Umum' AND ttkm.KMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-dari-bengkel':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'KM Dari Bengkel' AND ttkm.KMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-angsuran-karyawan':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'Angsuran Karyawan' AND ttkm.KMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-pos-dari-dealer':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'KM Pos Dari Dealer' AND ttkm.KMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/kas-masuk-dealer-dari-pos':
					$where = [
						[
							'op'        => 'AND',
							'condition' => "(ttkm.KMJenis LIKE 'KM Dealer Dari Pos' AND ttkm.KMNo NOT LIKE '%=%')",
							'params'    => []
						]
					];
					break;
				case 'ttkm/select-kk-ke-bengkel':
					$colGrid  = Ttkm::colGridKKkeBengkel();
					$whereRaw = '';
					if ( $json[ 'cmbTgl1' ] !== '' && $json[ 'tgl1' ] !== '' && $json[ 'tgl2' ] !== '' && $json[ 'check' ] == 'on' ) {
						$whereRaw = "AND {$json['cmbTgl1']} >= DATE('{$json['tgl1']}') AND {$json['cmbTgl1']} <= DATE('{$json['tgl2']}') ";
					}
					$KKLink     = $urlParams[ 'KKLink' ];
					$sqlraw     = "SELECT ttkm.KMNo, ttkm.KMTgl, ttkm.KMMemo, ttkm.KMNominal,ttkm.KMNominal AS Sisa, ttkm.KMJenis, ttkm.KMPerson, ttkm.KMLink, 
       					ttkm.UserID, ttkm.LokasiKode, ttkm.KMJam, ttkm.NoGL, ttkk.KKLink  
			         FROM ttkm 
			         LEFT OUTER JOIN ttkk ON ttkk.KKLink = ttkm.KMNo 
			         WHERE (ttkm.KMJenis LIKE '%KM Dari Bengkel') AND (ttkk.KKLink is NULL) OR ttkk.KKLink = '{$KKLink}' {$whereRaw}";
					$queryRawPK = [ 'KMNo' ];
					break;
			}
		}
		return [
			'td' => [
				'class'      => TdAction::class,
				'model'      => Ttkm::class,
				'queryRaw'   => $sqlraw,
				'queryRawPK' => $queryRawPK,
				'colGrid'    => $colGrid,
				'joinWith'   => $joinWith,
				'join'       => $join,
				'where'      => $where
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actionIndex() {
		return $this->render( 'index' );
	}

	public function actionKasMasukWa() {
		// if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
		// 	$post[ 'Action' ] = 'Display';
		// 	$result           = Ttkm::find()->callSP( $post );
		// 	Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		// }
		return $this->render( 'KasMasukWa', [
			'mode'    => '',
			// 'url_add' => Url::toRoute( [ 'ttkm/td' ] )
		] );
	}
	public function actionKasMasukWaIndex() {
		$json      = Json::decode( $_POST[ 'query' ], true );
		$KodeAkses = Yii::$app->session->get('_userProfile')['KodeAkses'];
		$UserID = Yii::$app->session->get('_userProfile')['UserID'];
		$LokasiKode = Yii::$app->session->get('_userProfile')['LokasiKode'];
		if($json['check'] == 'off'){
			$json['cmbTgl1'] = '';
			$json['cmbNum1'] = '';
		}
		$command = General::cCmd( "CALL fkmwa ('FillGrid',@Status,@Keterangan,'{$UserID}','{$KodeAkses}','{$LokasiKode}','','','{$json['cmbTxt1']}','{$json['txt1']}','{$json['cmbTxt2']}','{$json['txt2']}','{$json['cmbTgl1']}','{$json['tgl1']}','{$json['tgl2']}','{$json['cmbNum1']}', '{$json['cmbNum2']}','{$json['num1']}');" );
		$rows = $command->queryAll();
		// foreach ($rows as $key => $value) {
		// 	$rows[$key]['rowId'] =$value['Dealer'].'||'.$value['KKNo'];
		// }
		$finalResult = [
			'rows'=>$rows
		];
		return JSON::encode($finalResult);
	}
	public function actionSelectKkKeBengkel() {
		$this->layout = 'select-mode';
		return $this->render( 'select', [
			'title'   => 'Kas Keluar dari Bengkel',
			'arr'     => [
				'cmbTxt'    => [
					'KMPerson' => 'Penyetor',
					'KMNo'     => 'No Kas Masuk',
					'KMJenis'  => 'Jenis',
					'KMLink'   => 'No Transaksi',
					'KMMemo'   => 'Keterangan',
					'UserID'   => 'User',
					'NoGL'     => 'No GL',
				],
				'cmbTgl'    => [
					'KMTgl' => 'Tanggal Kas Masuk',
				],
				'cmbNum'    => [
					'KMNominal' => 'Nominal',
				],
				'sortname'  => "KMTgl",
				'sortorder' => 'desc'
			],
			'options' => [
				'colGrid' => Ttkm::colGridKKkeBengkel(),
				'mode'    => self::MODE_SELECT,
			]
		] );
	}
	public function actionKasMasukInden() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkm::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasMasukInden', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkm/td' ] )
		] );
	}
	public function actionKasMasukIndenCreate() {
		return $this->create( 'Inden', 'kas-masuk-inden', 'KAS MASUK - UM Inden' );
	}
	public function actionKasMasukIndenUpdate( $id ) {
		return $this->update( $id, 'kas-masuk-inden', 'KAS MASUK - UM Inden', 'Inden', );
	}
	public function actionKasMasukPelunasanLeasing() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkm::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasMasukPelunasanLeasing', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkm/td' ] )
		] );
	}
	public function actionKasMasukPelunasanLeasingCreate() {
		return $this->create( 'Leasing', 'kas-masuk-pelunasan-leasing', 'KAS MASUK - Pelunasan Leasing' );
	}
	public function actionKasMasukPelunasanLeasingUpdate( $id ) {
		return $this->update( $id, 'kas-masuk-pelunasan-leasing', 'KAS MASUK - Pelunasan Leasing', 'Leasing' );
	}
	public function actionKasMasukUangMukaKredit() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkm::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasMasukUangMukaKredit', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkm/td' ] )
		] );
	}
	public function actionKasMasukUangMukaKreditCreate() {
		return $this->create( 'Kredit', 'kas-masuk-uang-muka-kredit', 'KAS MASUK - Uang Muka Kredit' );
	}
	public function actionKasMasukUangMukaKreditUpdate( $id ) {
		return $this->update( $id, 'kas-masuk-uang-muka-kredit', 'KAS MASUK - Uang Muka Kredit', 'Kredit' );
	}
	public function actionKasMasukBayarPiutangKonUm() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkm::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasMasukBayarPiutangKonUm', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkm/td' ] )
		] );
	}
	public function actionKasMasukBayarPiutangKonUmCreate() {
		return $this->create( 'Piutang UM', 'kas-masuk-bayar-piutang-kon-um', 'KAS MASUK - Piutang Konsumen UM' );
	}
	public function actionKasMasukBayarPiutangKonUmUpdate( $id ) {
		return $this->update( $id, 'kas-masuk-bayar-piutang-kon-um', 'KAS MASUK - Piutang Konsumen UM', 'Piutang UM' );
	}
	public function actionKasMasukUangMukaTunai() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkm::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasMasukUangMukaTunai', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkm/td' ] )
		] );
	}
	public function actionKasMasukUangMukaTunaiCreate() {
		return $this->create( 'Tunai', 'kas-masuk-uang-muka-tunai', 'KAS MASUK - Uang Muka Tunai' );
	}
	public function actionKasMasukUangMukaTunaiUpdate( $id ) {
		return $this->update( $id, 'kas-masuk-uang-muka-tunai', 'KAS MASUK - Uang Muka Tunai', 'Tunai' );
	}
	public function actionKasMasukBayarSisaPiutang() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkm::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasMasukBayarSisaPiutang', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkm/td' ] )
		] );
	}
	public function actionKasMasukBayarSisaPiutangCreate() {
		return $this->create( 'Piutang Sisa', 'kas-masuk-bayar-sisa-piutang', 'KAS MASUK - Bayar Sisa Piutang' );
	}
	public function actionKasMasukBayarSisaPiutangUpdate( $id ) {
		return $this->update( $id, 'kas-masuk-bayar-sisa-piutang', 'KAS MASUK - Bayar Sisa Piutang', 'Piutang Sisa' );
	}
	public function actionKasMasukBbnProgresif() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkm::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasMasukBbnProgresif', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkm/td' ] )
		] );
	}
	public function actionKasMasukBbnProgresifCreate() {
		return $this->create( 'BBN Plus', 'kas-masuk-bbn-progresif', 'KAS MASUK - BBN Progresif' );
	}
	public function actionKasMasukBbnProgresifUpdate( $id ) {
		return $this->update( $id, 'kas-masuk-bbn-progresif', 'KAS MASUK - BBN Progresif', 'BBN Plus' );
	}
	public function actionKasMasukBbnAcc() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkm::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasMasukBbnAcc', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkm/td' ] )
		] );
	}
	public function actionKasMasukBbnAccCreate() {
		return $this->create( 'BBN Acc', 'kas-masuk-bbn-acc', 'KAS MASUK - BBN Acc' );
	}
	public function actionKasMasukBbnAccUpdate( $id ) {
		return $this->update( $id, 'kas-masuk-bbn-acc', 'KAS MASUK - BBN Acc', 'BBN Acc' );
	}
	public function actionKasMasukDariBank() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkm::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasMasukDariBank', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkm/td' ] )
		] );
	}
	public function actionKasMasukDariBankCreate() {
		return $this->create( 'KM Dari Bank', 'kas-masuk-dari-bank', 'KAS MASUK - Dari Bank' );
	}
	public function actionKasMasukDariBankUpdate( $id ) {
		return $this->update( $id, 'kas-masuk-dari-bank', 'KAS MASUK - Dari Bank', 'KM Dari Bank' );
	}
	public function actionKasMasukDariBengkel() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkm::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasMasukDariBengkel', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkm/td' ] )
		] );
	}
	public function actionKasMasukDariBengkelCreate() {
		return $this->create( 'KM Dari Bengkel', 'kas-masuk-dari-bengkel', 'KAS MASUK - Dari Bengkel' );
	}
	public function actionKasMasukDariBengkelUpdate( $id ) {
		return $this->update( $id, 'kas-masuk-dari-bengkel', 'KAS MASUK - Dari Bengkel', 'KM Dari Bengkel' );
	}
	public function actionKasMasukAngsuranKaryawan() {
		return $this->render( 'KasMasukAngsuranKaryawan' );
	}
	public function actionKasMasukAngsuranKaryawanCreate() {
		return $this->create( 'Angsuran Karyawan', 'kas-masuk-angsuran-karyawan', 'KAS MASUK - Angsuran Karyawan' );
	}
	public function actionKasMasukAngsuranKaryawanUpdate( $id ) {
		return $this->update( $id, 'kas-masuk-angsuran-karyawan', 'KAS MASUK - Angsuran Karyawan', 'Angsuran Karyawan' );
	}
	public function actionKasMasukPosDariDealer() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkm::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasMasukPosDariDealer', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkm/td' ] )
		] );
	}
	public function actionKasMasukPosDariDealerCreate() {
		return $this->create( 'KM Pos Dari Dealer', 'kas-masuk-pos-dari-dealer', 'KAS MASUK - Pos Dari Dealer' );
	}
	public function actionKasMasukPosDariDealerUpdate( $id ) {
		return $this->update( $id, 'kas-masuk-pos-dari-dealer', 'KAS MASUK - Pos Dari Dealer', 'KM Pos Dari Dealer' );
	}
	public function actionKasMasukDealerDariPos() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkm::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasMasukDealerDariPos', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkm/td' ] )
		] );
	}
	public function actionKasMasukDealerDariPosCreate() {
		return $this->create( 'KM Dealer Dari Pos', 'kas-masuk-dealer-dari-pos', 'KAS MASUK - Dealer Dari Pos' );
	}
	public function actionKasMasukDealerDariPosUpdate( $id ) {
		return $this->update( $id, 'kas-masuk-dealer-dari-pos', 'KAS MASUK - Dealer Dari Pos', 'KM Dealer Dari Pos' );
	}
	public function actionKasMasukUmum() {
		if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
			$post[ 'Action' ] = 'Display';
			$result           = Ttkm::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		return $this->render( 'KasMasukUmum', [
			'mode'    => '',
			'url_add' => Url::toRoute( [ 'ttkm/td' ] )
		] );
	}
	public function actionKasMasukUmumCreate() {
		TUi::$actionMode   = Menu::ADD;
		$model             = new Ttkm();
		$model->KMNo       = General::createTemporaryNo( 'KM', 'Ttkm', 'KMNo' );
		$model->KMTgl      = date( 'Y-m-d' );
		$model->KMJam      = date( 'Y-m-d H:i:s' );
		$model->KMNominal  = 0;
		$model->KMLink     = "--";
		$model->KMMemo     = "--";
		$model->KMJenis    = 'Umum';
		$model->KMPerson   = "--";
		$model->UserID     = $this->UserID();
		$model->NoGL       = "--";
		$model->LokasiKode = Menu::getUserLokasi()[ 'LokasiKode' ];
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttkm::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang               = Ttkm::find()->ttkmFillByNo()->where( [ 'ttkm.KMNo' => $model->KMNo, 'ttkm.KMJenis' => $model->KMJenis, 'ttkm.LokasiKode' => $model->LokasiKode ] )
		                             ->asArray( true )->one();
		$JoinData              = Ttdk::find()->GetJoinData()->where( [ 'ttdk.DKNo' => $model->KMLink ] )->asArray( true )->one();
        if ( $JoinData == null ) {
            $JoinData[ 'CusAlamat' ] = '--';
            $JoinData[ 'MotorType' ] = '--';
            $JoinData[ 'LeaseKode' ] = '--';
            $JoinData[ 'CusNama' ]   = '--';
            $JoinData[ 'KMLinkTgl' ] = $model->KMTgl;
        }
        $dsTUang[ 'KMNoView' ] = $result[ 'KMNo' ];
		$dsTUang[ 'SubTotal' ] = 0;
		$id                    = $this->generateIdBase64FromModel( $model );
		return $this->render( 'kas-masuk-umum-create', [
			'model'    => $model,
			'dsTUang'  => $dsTUang,
			'JoinData' => $JoinData,
			'id'       => $id
		] );
	}
	public function actionLoopKasMasuk() {
		$_POST[ 'KMJam' ]  = $_POST[ 'KMTgl' ] . ' ' . $_POST[ 'KMJam' ];
		$_POST[ 'Action' ] = 'Loop';
		$result            = Ttkm::find()->callSP( $_POST );
        if($result[ 'status' ] == 1){
            Yii::$app->session->addFlash( 'warning', $result[ 'keterangan' ] );
        }
		$_POST[ 'Action' ] = 'LoopAfter';
		$result            = Ttkm::find()->callSP( $_POST );
        if($result[ 'status' ] == 1){
            Yii::$app->session->addFlash( 'warning', $result[ 'keterangan' ] );
        }
	}
	public function actionKasMasukUmumUpdate( $id, $action ) {
		$index = Ttkm::getRoute( 'Umum', [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttkm $model */
		$model    = $this->findModelBase64( 'Ttkm', $id );
		$dsTUang  = Ttkm::find()->ttkmFillByNo()->where( [ 'ttkm.KMNo' => $model->KMNo, 'ttkm.KMJenis' => $model->KMJenis, 'ttkm.LokasiKode' => $model->LokasiKode ] )
		                ->asArray( true )->one();
		$JoinData = Ttdk::find()->GetJoinData()->where( [ 'ttdk.DKNo' => $model->KMLink ] )->asArray( true )->one();
		if ( $JoinData == null ) {
			$JoinData[ 'CusAlamat' ] = '--';
			$JoinData[ 'MotorType' ] = '--';
			$JoinData[ 'LeaseKode' ] = '--';
			$JoinData[ 'CusNama' ]   = '--';
			$JoinData[ 'KMLinkTgl' ] = $model->KMTgl;
		} else {
			$JoinData[ 'KMLinkTgl' ] = $JoinData[ 'DKTgl' ];
		}
		if ( Yii::$app->request->isPost ) {
			$post             = array_merge( $dsTUang, \Yii::$app->request->post() );
			$post[ 'KMJam' ]  = $post[ 'KMTgl' ] . ' ' . $post[ 'KMJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttkm::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model = Ttkm::findOne( $result[ 'KMNo' ] );
			$id    = $this->generateIdBase64FromModel( $model );
			if ( $result[ 'status' ] == 0 ) {
				$post[ 'KMNoView' ] = $result[ 'KMNo' ];
				return $this->redirect( Ttkm::getRoute( 'Umum', [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'kas-masuk-umum-update', [
					'model'    => $model,
					'dsTUang'  => $post,
					'JoinData' => $JoinData,
					'id'       => $id,
				] );
			}
		}
		$dsTUang[ 'SubTotal' ] = 0;
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttkm::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		}
		$dsTUang[ 'KMNoView' ] = $_GET[ 'KMNoView' ] ?? $result[ 'KMNo' ];
		return $this->render( 'kas-masuk-umum-update', [
			'model'    => $model,
			'dsTUang'  => $dsTUang,
			'JoinData' => $JoinData,
			'id'       => $id
		] );
	}
	/**
	 * Displays a single Ttkm model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * @param $KMJenis
	 * @param $view
	 * @param $title
	 *
	 * @return string
	 */
	private function create( $KMJenis, $view, $title ) {
		TUi::$actionMode   = Menu::ADD;
		$model             = new Ttkm();
		$model->KMNo       = General::createTemporaryNo( 'KM', 'Ttkm', 'KMNo' );
		$model->KMTgl      = date( 'Y-m-d' );
		$model->KMJam      = date( 'Y-m-d H:i:s' );
		$model->KMNominal  = floatval( $_GET[ 'KMNominal' ] ?? 0 );
		$model->KMLink     = $_GET[ 'KMLink' ] ?? "--";
		$model->KMMemo     = $_GET[ 'KMMemo' ] ?? "--";
		$model->KMPerson   = $_GET[ 'KMPerson' ] ?? "--";
		$model->KMJenis    = $KMJenis;
		$model->UserID     = $this->UserID();
		$model->NoGL       = "--";
		$model->LokasiKode = Menu::getUserLokasi()[ 'LokasiKode' ];
		$model->save();
		$post             = $model->attributes;
		$post[ 'Action' ] = 'Insert';
		$result           = Ttkm::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		$dsTUang = Ttkm::find()->ttkmFillByNo()->where( [ 'ttkm.KMNo' => $model->KMNo, 'ttkm.KMJenis' => $model->KMJenis, 'ttkm.LokasiKode' => $model->LokasiKode ] )
		               ->asArray( true )->one();
		switch ( $KMJenis ) {
			case 'Inden' :
				$JoinData = General::cCmd( "SELECT  ttin.INNo,ttin.DKNo, ttin.INTgl, ttin.MotorType, ttin.MotorWarna, ttin.LeaseKode,
                                            tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT,
                                            tdcustomer.CusRW, tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, tdarea.Provinsi
                                            FROM ttin
                                            INNER JOIN tdcustomer ON ttin.CusKode = tdcustomer.CusKode
                                            INNER JOIN tdarea ON tdcustomer.CusKabupaten = tdarea.Kabupaten
                                            WHERE ttin.INNo = :INNo", [ ':INNo' => $dsTUang[ 'KMLink' ] ] )->queryOne();
				if ( $JoinData != null ) {
					$dsTUang[ 'CusNama' ]    = $JoinData[ 'CusNama' ];
					$JoinData[ 'CusAlamat' ] = $JoinData[ 'CusAlamat' ] . ' - RT/RW :' . $JoinData[ 'CusRT' ] . '/' . $JoinData[ 'CusRW' ] . ' - ' . $JoinData[ 'CusKelurahan' ] .
					                           ' - ' . $JoinData[ 'CusKecamatan' ] . ' - ' . $JoinData[ 'CusKabupaten' ] . ' - ' . $JoinData[ 'Provinsi' ];
					$JoinData[ 'MotorType' ] = $JoinData[ 'MotorType' ] . ' - ' . $JoinData[ 'MotorWarna' ];
					$JoinData[ 'KMLinkTgl' ] = $JoinData[ 'INTgl' ];
				} else {
					$dsTUang[ 'CusNama' ]    = '--';
					$JoinData[ 'DKNo' ]      = '--';
					$JoinData[ 'LeaseKode' ] = '--';
					$JoinData[ 'MotorType' ] = '--';
					$JoinData[ 'CusAlamat' ] = '--';
					$JoinData[ 'KMLinkTgl' ] = '1900-01-01';
				}
				if ( $JoinData[ 'DKNo' ] != '--' ) {
					$motorData               = General::cCmd( "SELECT ttdk.DKNo, ttdk.DKTgl, tmotor.MotorType, tmotor.MotorWarna,
                    tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka
                    FROM ttdk
                    INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
                    WHERE ttdk.DKNo = :DKNo", [ ':DKNo' => $JoinData[ 'DKNo' ] ] )->queryOne();
					$JoinData[ 'MotorType' ] .= ' - ' . $motorData[ 'MotorTahun' ] . ' - ' . $motorData[ 'MotorNoMesin' ] . ' - ' . $motorData[ 'MotorNoRangka' ];
				}
				break;
			case 'KM Dari Bengkel' :
				$sql      = "SELECT * FROM HVYS.ttkkhd 
    			INNER JOIN HVYS.ttkkit ON ttkkit.KKNo = ttkkhd.kkno 
				WHERE ttkkit.KKLink  = :KKLink";
				$db       = 'b' . base64_decode( $_COOKIE[ '_outlet' ] ) . '.';
				$JoinData = General::cCmd( str_replace( 'HVYS.', $db, $sql ), [ ':KKLink' => $dsTUang[ 'KMNo' ] ] )->queryOne();
				if ( $JoinData != null ) {
					$JoinData[ 'MotorType' ] = $JoinData[ 'KKJenis' ] . ' - ' . $JoinData[ 'KodeTrans' ];
					$JoinData[ 'LeaseKode' ] = $JoinData[ 'PosKode' ];
					$dsTUang[ 'CusNama' ]    = $JoinData[ 'KKPerson' ];
					$JoinData[ 'KMLinkTgl' ] = $JoinData[ 'KKTgl' ];
					$JoinData[ 'CusAlamat' ] = $JoinData[ 'KKMemo' ];
				} else {
					$JoinData[ 'MotorType' ] = '--';
					$JoinData[ 'LeaseKode' ] = '';
					$dsTUang[ 'CusNama' ]    = '--';
					$JoinData[ 'KMLinkTgl' ] = '--';
					$JoinData[ 'CusAlamat' ] = '--';
				}
				break;
			case 'Angsuran Karyawan' :
				$JoinData                = General::cCmd( "SELECT SalesKode, SalesNama, SalesAlamat, SalesTelepon, SalesStatus, SalesKeterangan
	            FROM tdsales WHERE SalesKode = :SalesKode", [ ':SalesKode' => $dsTUang[ 'KMLink' ] ] )->queryOne();
				$JoinData[ 'MotorType' ] = $JoinData[ 'SalesKeterangan' ] ?? '';
				$JoinData[ 'LeaseKode' ] = '';
				$dsTUang[ 'CusNama' ]    = $JoinData[ 'SalesNama' ] ?? '--';
				$JoinData[ 'KMLinkTgl' ] = '1900-01-01';
				$JoinData[ 'CusAlamat' ] = ( $JoinData[ 'SalesAlamat' ] ?? '' ) . ' - ' . ( $JoinData[ 'SalesTelepon' ] ?? '' );
				break;
			case 'KM Dari Bank' :
				$JoinData = General::cCmd( "SELECT BKNo, BKTgl, BKMemo, BKNominal, BKJenis,
				BKCekNo, BKCekTempo, BKPerson, BKAC, ttBKhd.NoAccount, UserID, traccount.NamaAccount
				FROM ttbkhd
				INNER JOIN traccount ON ttBKhd.NoAccount = traccount.NoAccount
				WHERE BKNo = :BKNo", [ ':BKNo' => $dsTUang[ 'KMLink' ] ] )->queryOne();
				if ( $JoinData != null ) {
					$JoinData[ 'CusAlamat' ] = $JoinData[ 'NoAccount' ] . ' - ' . $JoinData[ 'NamaAccount' ] . " - \n" . $JoinData[ 'BKMemo' ];
					$dsTUang[ 'CusNama' ]    = $JoinData[ 'BKPerson' ];
					$JoinData[ 'MotorType' ] = $JoinData[ 'BKAC' ] . ' - ' . $JoinData[ 'BKCekNo' ] . ' - ' . General::asDate( $JoinData[ 'BKCekTempo' ], 'dd/MM/yyyy' );
					$JoinData[ 'KMLinkTgl' ] = ( isset( $JoinData[ 'BKTgl' ] ) ) ? $JoinData[ 'BKTgl' ] : date( 'Y-m-d' );
					$JoinData[ 'LeaseKode' ] = '--';
				} else {
					$dsTUang[ 'CusNama' ]    = '--';
					$JoinData[ 'CusAlamat' ] = '--';
					$JoinData[ 'MotorType' ] = '--';
					$JoinData[ 'KMLinkTgl' ] = date( 'Y-m-d' );
					$JoinData[ 'LeaseKode' ] = '--';
				}
				break;
			default:
				$JoinData = Ttdk::find()->GetJoinData()->where( [ 'ttdk.DKNo' => $model->KMLink ] )->asArray( true )->one();
				if ( $JoinData != null ) {
					$dsTUang[ 'CusNama' ]    = $JoinData[ 'CusNama' ];
					$JoinData[ 'KMLinkTgl' ] = $JoinData[ 'DKTgl' ];
				} else {
					$JoinData[ 'KMLinkTgl' ] = date( 'Y-m-d' );
					$dsTUang[ 'CusNama' ]    = '--';
					$JoinData[ 'CusAlamat' ] = '--';
					$JoinData[ 'MotorType' ] = '--';
					$JoinData[ 'LeaseKode' ] = '--';
				}
				break;
		}
		$dsTUang[ 'KMNoView' ] = $result[ 'KMNo' ];
		$id                    = $this->generateIdBase64FromModel( $model );
		return $this->render( 'create', [
			'model'    => $model,
			'dsTUang'  => $dsTUang,
			'JoinData' => $JoinData,
			'id'       => $id,
			'view'     => $view,
			'title'    => $title
		] );
	}
	/**
	 * @param $id
	 * @param $view
	 * @param $title
	 *
	 * @return string|\yii\web\Response
	 * @throws NotFoundHttpException
	 */
	private function update( $id, $view, $title, $KMJenis ) {
		$index = Ttkm::getRoute( $KMJenis, [ 'id' => $id ] )[ 'index' ];
		if ( ! isset( $_GET[ 'action' ] ) ) {
			return $this->redirect( $index );
		}
		$action = $_GET[ 'action' ];
		switch ( $action ) {
			case 'create':
			case 'update':
				TUi::$actionMode = Menu::UPDATE;
				break;
			case 'view':
				TUi::$actionMode = Menu::VIEW;
				break;
			case 'display':
				TUi::$actionMode = Menu::DISPLAY;
				break;
		}
		/** @var Ttkm $model */
		$model   = $this->findModelBase64( 'Ttkm', $id );
		$dsTUang = Ttkm::find()->ttkmFillByNo()->where( [ 'ttkm.KMNo' => $model->KMNo, 'ttkm.KMJenis' => $model->KMJenis, 'ttkm.LokasiKode' => $model->LokasiKode ] )
		               ->asArray( true )->one();
		switch ( $KMJenis ) {
			case 'Inden' :
				$JoinData = General::cCmd( "SELECT  ttin.INNo,ttin.DKNo, ttin.INTgl, ttin.MotorType, ttin.MotorWarna, ttin.LeaseKode,
                                            tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT,
                                            tdcustomer.CusRW, tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, tdarea.Provinsi
                                            FROM ttin
                                            INNER JOIN tdcustomer ON ttin.CusKode = tdcustomer.CusKode
                                            INNER JOIN tdarea ON tdcustomer.CusKabupaten = tdarea.Kabupaten
                                            WHERE ttin.INNo = :INNo", [ ':INNo' => $dsTUang[ 'KMLink' ] ] )->queryOne();
				if ( $JoinData != null ) {
					$dsTUang[ 'CusNama' ]    = $JoinData[ 'CusNama' ];
					$JoinData[ 'CusAlamat' ] = $JoinData[ 'CusAlamat' ] . ' - RT/RW :' . $JoinData[ 'CusRT' ] . '/' . $JoinData[ 'CusRW' ] . ' - ' . $JoinData[ 'CusKelurahan' ] .
					                           ' - ' . $JoinData[ 'CusKecamatan' ] . ' - ' . $JoinData[ 'CusKabupaten' ] . ' - ' . $JoinData[ 'Provinsi' ];
					$JoinData[ 'MotorType' ] = $JoinData[ 'MotorType' ] . ' - ' . $JoinData[ 'MotorWarna' ];
					$JoinData[ 'KMLinkTgl' ] = $JoinData[ 'INTgl' ];
				} else {
					$dsTUang[ 'CusNama' ]    = '--';
					$JoinData[ 'DKNo' ]      = '--';
					$JoinData[ 'LeaseKode' ] = '--';
					$JoinData[ 'MotorType' ] = '--';
					$JoinData[ 'CusAlamat' ] = '--';
					$JoinData[ 'KMLinkTgl' ] = '1900-01-01';
				}
				if ( $JoinData[ 'DKNo' ] != '--' ) {
					$motorData               = General::cCmd( "SELECT ttdk.DKNo, ttdk.DKTgl, tmotor.MotorType, tmotor.MotorWarna,
                    tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka
                    FROM ttdk
                    INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
                    WHERE ttdk.DKNo = :DKNo", [ ':DKNo' => $JoinData[ 'DKNo' ] ] )->queryOne();
					$JoinData[ 'MotorType' ] .= ' - ' . $motorData[ 'MotorTahun' ] . ' - ' . $motorData[ 'MotorNoMesin' ] . ' - ' . $motorData[ 'MotorNoRangka' ];
				}
				break;
			case 'KM Dari Bengkel' :
				$sql      = "SELECT * FROM HVYS.ttkkhd 
    			INNER JOIN HVYS.ttkkit ON ttkkit.KKNo = ttkkhd.kkno 
				WHERE ttkkit.KKLink  = :KKLink";
				$db       = 'b' . base64_decode( $_COOKIE[ '_outlet' ] ) . '.';
				$JoinData = General::cCmd( str_replace( 'HVYS.', $db, $sql ), [ ':KKLink' => $dsTUang[ 'KMNo' ] ] )->queryOne();
				if ( $JoinData != null ) {
					$JoinData[ 'MotorType' ] = $JoinData[ 'KKJenis' ] . ' - ' . $JoinData[ 'KodeTrans' ];
					$JoinData[ 'LeaseKode' ] = $JoinData[ 'PosKode' ];
					$dsTUang[ 'CusNama' ]    = $JoinData[ 'KKPerson' ];
					$JoinData[ 'KMLinkTgl' ] = $JoinData[ 'KKTgl' ];
					$JoinData[ 'CusAlamat' ] = $JoinData[ 'KKMemo' ];
				} else {
					$JoinData[ 'MotorType' ] = '--';
					$JoinData[ 'LeaseKode' ] = '';
					$dsTUang[ 'CusNama' ]    = '--';
					$JoinData[ 'KMLinkTgl' ] = '--';
					$JoinData[ 'CusAlamat' ] = '--';
				}
				break;
			case 'Angsuran Karyawan' :
				$JoinData                = General::cCmd( "SELECT SalesKode, SalesNama, SalesAlamat, SalesTelepon, SalesStatus, SalesKeterangan
	            FROM tdsales WHERE SalesKode = :SalesKode", [ ':SalesKode' => $dsTUang[ 'KMLink' ] ] )->queryOne();
				$JoinData[ 'MotorType' ] = $JoinData[ 'SalesKeterangan' ] ?? '';
				$JoinData[ 'LeaseKode' ] = '';
				$dsTUang[ 'CusNama' ]    = $JoinData[ 'SalesNama' ] ?? '--';
				$JoinData[ 'KMLinkTgl' ] = '1900-01-01';
				$JoinData[ 'CusAlamat' ] = ( $JoinData[ 'SalesAlamat' ] ?? '' ) . ' - ' . ( $JoinData[ 'SalesTelepon' ] ?? '' );
				break;
			case 'KM Dari Bank' :
				$JoinData = General::cCmd( "SELECT BKNo, BKTgl, BKMemo, BKNominal, BKJenis,
				BKCekNo, BKCekTempo, BKPerson, BKAC, ttBKhd.NoAccount, UserID, traccount.NamaAccount
				FROM ttbkhd
				INNER JOIN traccount ON ttBKhd.NoAccount = traccount.NoAccount
				WHERE BKNo = :BKNo", [ ':BKNo' => $dsTUang[ 'KMLink' ] ] )->queryOne();
				if ( $JoinData != null ) {
					$JoinData[ 'CusAlamat' ] = $JoinData[ 'NoAccount' ] . ' - ' . $JoinData[ 'NamaAccount' ] . " - \n" . $JoinData[ 'BKMemo' ];
					$dsTUang[ 'CusNama' ]    = $JoinData[ 'BKPerson' ];
					$JoinData[ 'MotorType' ] = $JoinData[ 'BKAC' ] . ' - ' . $JoinData[ 'BKCekNo' ] . ' - ' . General::asDate( $JoinData[ 'BKCekTempo' ], 'dd/MM/yyyy' );
					$JoinData[ 'KMLinkTgl' ] = ( isset( $JoinData[ 'BKTgl' ] ) ) ? $JoinData[ 'BKTgl' ] : date( 'Y-m-d' );
					$JoinData[ 'LeaseKode' ] = '--';
				} else {
					$dsTUang[ 'CusNama' ]    = '--';
					$JoinData[ 'CusAlamat' ] = '--';
					$JoinData[ 'MotorType' ] = '--';
					$JoinData[ 'KMLinkTgl' ] = date( 'Y-m-d' );
					$JoinData[ 'LeaseKode' ] = '--';
				}
				break;
			default:
				$JoinData = Ttdk::find()->GetJoinData()->where( [ 'ttdk.DKNo' => $model->KMLink ] )->asArray( true )->one();
				if ( $JoinData != null ) {
					$dsTUang[ 'CusNama' ]    = $JoinData[ 'CusNama' ];
					$JoinData[ 'KMLinkTgl' ] = $JoinData[ 'DKTgl' ];
				} else {
					$JoinData[ 'KMLinkTgl' ] = date( 'Y-m-d' );
					$dsTUang[ 'CusNama' ]    = '--';
					$JoinData[ 'CusAlamat' ] = '--';
					$JoinData[ 'MotorType' ] = '--';
					$JoinData[ 'LeaseKode' ] = '--';
				}
				break;
		}
		if ( Yii::$app->request->isPost ) {
			$post             = array_merge( $dsTUang ?? [], \Yii::$app->request->post() );
			$post[ 'KMJam' ]  = $post[ 'KMTgl' ] . ' ' . $post[ 'KMJam' ];
			$post[ 'Action' ] = 'Update';
			$result           = Ttkm::find()->callSP( $post );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
			$model              = Ttkm::findOne( $result[ 'KMNo' ] );
			$id                 = $this->generateIdBase64FromModel( $model );
			$post[ 'KMNoView' ] = $result[ 'KMNo' ];
			if ( $result[ 'status' ] == 0 ) {
				return $this->redirect( Ttkm::getRoute( $KMJenis, [ 'id' => $id, 'action' => 'display' ] )[ 'update' ] );
			} else {
				return $this->render( 'update', [
					'model'    => $model,
					'dsTUang'  => $post,
					'JoinData' => $JoinData,
					'id'       => $id,
					'view'     => $view,
					'title'    => $title
				] );
			}
		}
		$dsTUang[ 'SubTotal' ] = 0;
		if ( ! isset( $_GET[ 'oper' ] ) ) {
			$dsTUang[ 'Action' ] = 'Load';
			$result              = Ttkm::find()->callSP( $dsTUang );
			Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		} else {
			$result[ 'KMNo' ] = $dsTUang[ 'KMNo' ];
		}
		$dsTUang[ 'KMNoView' ] = $_GET[ 'KMNoView' ] ?? $result[ 'KMNo' ];
		return $this->render( 'update', [
			'model'    => $model,
			'dsTUang'  => $dsTUang,
			'JoinData' => $JoinData,
			'id'       => $id,
			'view'     => $view,
			'title'    => $title
		] );
	}
	/**
	 * Deletes an existing Ttkm model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
    public function actionDelete() {
        /** @var Ttkm $model */
        $request = Yii::$app->request;
        $PK         = base64_decode( $_REQUEST[ 'id' ] );
        $model      = null;
        $model = Ttkm::findOne( $PK );
        $_POST[ 'KMNo' ]   = $model->KMNo;
        $_POST[ 'Action' ] = 'Delete';
        $KMJenis = $model->KMJenis;
        $result = Ttkm::find()->callSP( $_POST );
        if ($request->isAjax){
            if ($result['status'] == 0) {
                return $this->responseSuccess($result['keterangan']);
            } else {
                return $this->responseFailed($result['keterangan']);
            }
        }else{
            Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
            if ( $result[ 'KMNo' ] == '--' ) {
                return $this->redirect( Ttkm::getRoute( $KMJenis )[ 'index' ] );
            } else {
                $model = Ttkm::findOne( $result[ 'KMNo' ] );
                if(empty($model)){
                    return $this->redirect( Ttkm::getRoute( $KMJenis )[ 'index' ] );
                }
                return $this->redirect( Ttkm::getRoute( $KMJenis, [ 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] )[ 'update' ] );
            }
        }
    }
	/**
	 * Finds the Ttkm model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Ttkm the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Ttkm::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
	public function actionPrint( $id ) {
		unset( $_POST[ '_csrf-app' ] );
		$filename = $_POST['KMNo'].'.pdf';
		$_POST             = array_merge( $_POST, Yii::$app->session->get( '_company' ) );
		$_POST[ 'db' ]     = base64_decode( $_COOKIE[ '_outlet' ] );
		$_POST[ 'UserID' ] = Menu::getUserLokasi()[ 'UserID' ];
		$client            = new Client( [
			'headers' => [ 'Content-Type' => 'application/octet-stream' ]
		] );
		$response          = $client->post( Yii::$app->params[ 'host_report' ] . '/aunit/fkm', [
			'body' => Json::encode( $_POST )
		] );
		$contentDisposition = $response->getHeader('Content-Disposition');
		if(!empty($contentDisposition)){
			$result = Custom::getFilenameContentDisposition($contentDisposition[0]);
			if($result !== false){
				$filename = $result;
			}
		}
		return Yii::$app->response->sendContentAsFile( $response->getBody(), $filename, [
				'inline'   => true,
				'mimeType' => 'application/pdf'
			]
		);
//		return str_replace( "<BODY", '<BODY onload="window.print()"', $html );
	}
	public function actionCancel( $id, $action ) {
		/** @var  $model Ttkm */
		$model            = $this->findModelBase64( 'Ttkm', $id );
		$KMJenis          = $model->KMJenis;
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Cancel';
		$post[ 'KMJam' ]  = $model->KMJam;
		$result           = Ttkm::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		if ( $result[ 'KMNo' ] == '--' ) {
			return $this->redirect( Ttkm::getRoute( $KMJenis )[ 'index' ] );
		} else {
			$model = Ttkm::findOne( $result[ 'KMNo' ] );
			return $this->redirect( Ttkm::getRoute( $KMJenis, [ 'action' => 'display', 'id' => $this->generateIdBase64FromModel( $model ) ] )[ 'update' ] );
		}
	}
	public function actionJurnal() {
		$post             = \Yii::$app->request->post();
		$post[ 'Action' ] = 'Jurnal';
		$post[ 'KMJam' ]  = $post[ 'KMTgl' ] . ' ' . $post[ 'KMJam' ];
		$result           = Ttkm::find()->callSP( $post );
		Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
		return $this->redirect( Ttkm::getRoute( $post[ 'KMJenis' ], [ 'action' => 'display', 'id' => base64_encode( $result[ 'KMNo' ] ) ] )[ 'update' ] );
	}
}
