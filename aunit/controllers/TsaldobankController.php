<?php

namespace aunit\controllers;

use aunit\components\TdAction;
use Yii;
use aunit\models\Tsaldobank;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TsaldobankController implements the CRUD actions for Tsaldobank model.
 */
class TsaldobankController extends Controller
{
	public function actions() {
		return [
			'td' => [
				'class' => TdAction::className(),
				'model' => Tsaldobank::className(),
				'joinWith' => [
					'account'  => [
						'type' => 'INNER JOIN'
					],
				],
			],
		];
	}
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tsaldobank models.
     * @return mixed
     */
    public function actionIndex()
    {

        return $this->render('index');
    }

    /**
     * Displays a single Tsaldobank model.
     * @param string $SaldoBankTgl
     * @param string $NoAccount
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($SaldoBankTgl, $NoAccount)
    {
        return $this->render('view', [
            'model' => $this->findModel($SaldoBankTgl, $NoAccount),
        ]);
    }

    /**
     * Creates a new Tsaldobank model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tsaldobank();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'SaldoBankTgl' => $model->SaldoBankTgl, 'NoAccount' => $model->NoAccount]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tsaldobank model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $SaldoBankTgl
     * @param string $NoAccount
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($SaldoBankTgl, $NoAccount)
    {
        $model = $this->findModel($SaldoBankTgl, $NoAccount);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'SaldoBankTgl' => $model->SaldoBankTgl, 'NoAccount' => $model->NoAccount]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tsaldobank model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $SaldoBankTgl
     * @param string $NoAccount
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($SaldoBankTgl, $NoAccount)
    {
        $this->findModel($SaldoBankTgl, $NoAccount)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tsaldobank model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $SaldoBankTgl
     * @param string $NoAccount
     * @return Tsaldobank the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($SaldoBankTgl, $NoAccount)
    {
        if (($model = Tsaldobank::findOne(['SaldoBankTgl' => $SaldoBankTgl, 'NoAccount' => $NoAccount])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
