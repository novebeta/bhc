<?php

namespace aunit\controllers;

use aunit\components\TdAction;
use Yii;
use aunit\models\Ttmmarketshare;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TtmmarketshareController implements the CRUD actions for Ttmmarketshare model.
 */
class TtmmarketshareController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
	public function actions() {
		$colGrid  = null;
		$joinWith = [
//			'sales'              => [ 'type' => 'INNER JOIN' ],
		];
		$join     = [];
		$where    = [];
//		if ( isset( $_POST['query'] ) ) {
//			$r = Json::decode( $_POST['query'], true )['r'];
//			switch ( $r ) {
//				case 'ttsd/mutasi-eksternal-dealer':
//					$colGrid  = Ttsd::colGridMutasiEksternalDealer();
//					$joinWith = [
//						'lokasi' => [
//							'type' => 'INNER JOIN'
//						],
//						'dealer' => [
//							'type' => 'INNER JOIN'
//						],
//					];
//					break;
//				case 'ttsd/invoice-eksternal':
//					$colGrid  = Ttsd::colGridInvoiceEksternal();
//					$joinWith = [
//						'lokasi' => [
//							'type' => 'INNER JOIN'
//						],
//						'dealer' => [
//							'type' => 'INNER JOIN'
//						],
//					];
//					break;
//			}
//		}
		return [
			'td' => [
				'class'    => TdAction::className(),
				'model'    => Ttmmarketshare::className(),
				'colGrid'  => $colGrid,
				'joinWith' => $joinWith
			],
		];
	}

    /**
     * Lists all Ttmmarketshare models.
     * @return mixed
     */
    public function actionIndex()
    {

//	    if ( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'display' ) {
//		    $post[ 'Action' ] = 'Display';
//		    $result           = Ttmteamtarget::find()->callSP( $post );
//		    Yii::$app->session->addFlash( ( $result[ 'status' ] == 0 ) ? 'success' : 'warning', $result[ 'keterangan' ] );
//	    }
	    return $this->render( 'index' );
    }

    /**
     * Displays a single Ttmmarketshare model.
     * @param string $Tanggal
     * @param string $Provinsi
     * @param string $Kabupaten
     * @param string $Kecamatan
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($Tanggal, $Provinsi, $Kabupaten, $Kecamatan)
    {
        return $this->render('view', [
            'model' => $this->findModel($Tanggal, $Provinsi, $Kabupaten, $Kecamatan),
        ]);
    }

    /**
     * Creates a new Ttmmarketshare model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ttmmarketshare();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Tanggal' => $model->Tanggal, 'Provinsi' => $model->Provinsi, 'Kabupaten' => $model->Kabupaten, 'Kecamatan' => $model->Kecamatan]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ttmmarketshare model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $Tanggal
     * @param string $Provinsi
     * @param string $Kabupaten
     * @param string $Kecamatan
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($Tanggal, $Provinsi, $Kabupaten, $Kecamatan)
    {
        $model = $this->findModel($Tanggal, $Provinsi, $Kabupaten, $Kecamatan);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Tanggal' => $model->Tanggal, 'Provinsi' => $model->Provinsi, 'Kabupaten' => $model->Kabupaten, 'Kecamatan' => $model->Kecamatan]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ttmmarketshare model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $Tanggal
     * @param string $Provinsi
     * @param string $Kabupaten
     * @param string $Kecamatan
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($Tanggal, $Provinsi, $Kabupaten, $Kecamatan)
    {
        $this->findModel($Tanggal, $Provinsi, $Kabupaten, $Kecamatan)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ttmmarketshare model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $Tanggal
     * @param string $Provinsi
     * @param string $Kabupaten
     * @param string $Kecamatan
     * @return Ttmmarketshare the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Tanggal, $Provinsi, $Kabupaten, $Kecamatan)
    {
        if (($model = Ttmmarketshare::findOne(['Tanggal' => $Tanggal, 'Provinsi' => $Provinsi, 'Kabupaten' => $Kabupaten, 'Kecamatan' => $Kecamatan])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
