<?php
namespace aunit\assets;
use yii\web\AssetBundle;
/**
 * Main aunit application asset bundle.
 */
class BootboxAsset extends AssetBundle {
	public $basePath = '@webroot/';
	public $baseUrl = '@web/';
	public $css = [
	];
	public $js = [
		'js/bootbox.min.js',
	];
	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
	];
}
