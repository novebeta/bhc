<?php

namespace aunit\assets;

use yii\web\AssetBundle;

/**
 * Main aunit application asset bundle.
 */
class JqwidgetsAsset extends AssetBundle
{
    public $basePath = '@webroot/jqwidgets/';
    public $baseUrl = '@web/jqwidgets/';
    public $css = [
        'https://cdnjs.cloudflare.com/ajax/libs/jqwidgets/11.1.3/jqwidgets/styles/jqx.base.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/jqwidgets/11.1.3/jqwidgets/styles/jqx.bootstrap.min.css',
    ];
    public $js = [
    	'https://cdnjs.cloudflare.com/ajax/libs/jqwidgets/11.1.3/jqwidgets/jqxcore.min.js',
    	'https://cdnjs.cloudflare.com/ajax/libs/jqwidgets/11.1.3/jqwidgets/jqxdata.min.js',
    	'https://cdnjs.cloudflare.com/ajax/libs/jqwidgets/11.1.3/jqwidgets/jqxbuttons.min.js',
    	'https://cdnjs.cloudflare.com/ajax/libs/jqwidgets/11.1.3/jqwidgets/jqxscrollbar.min.js',
    	'https://cdnjs.cloudflare.com/ajax/libs/jqwidgets/11.1.3/jqwidgets/jqxpanel.min.js',
    	'https://cdnjs.cloudflare.com/ajax/libs/jqwidgets/11.1.3/jqwidgets/jqxtree.min.js',
    	'https://cdnjs.cloudflare.com/ajax/libs/jqwidgets/11.1.3/jqwidgets/jqxcheckbox.min.js',
    	'https://cdnjs.cloudflare.com/ajax/libs/jqwidgets/11.1.3/jqwidgets/jqxcombobox.min.js',
    	'https://cdnjs.cloudflare.com/ajax/libs/jqwidgets/11.1.3/jqwidgets/jqxlistbox.min.js',
    	'https://cdnjs.cloudflare.com/ajax/libs/jqwidgets/11.1.3/jqwidgets/jqxnumberinput.min.js',
    	'https://cdnjs.cloudflare.com/ajax/libs/jqwidgets/11.1.3/jqwidgets/jqxradiobutton.min.js',
    	'https://cdnjs.cloudflare.com/ajax/libs/jqwidgets/11.1.3/jqwidgets/jqxmenu.min.js',
    	'https://cdnjs.cloudflare.com/ajax/libs/jqwidgets/11.1.3/jqwidgets/jqxgrid.min.js',
    	'https://cdnjs.cloudflare.com/ajax/libs/jqwidgets/11.1.3/jqwidgets/jqxgrid.selection.min.js',
    	'https://cdnjs.cloudflare.com/ajax/libs/jqwidgets/11.1.3/jqwidgets/jqxgrid.columnsresize.min.js',
    	'https://cdnjs.cloudflare.com/ajax/libs/jqwidgets/11.1.3/jqwidgets/jqxdata.export.min.js',
    	'https://cdnjs.cloudflare.com/ajax/libs/jqwidgets/11.1.3/jqwidgets/jqxgrid.export.min.js',
    	'https://cdnjs.cloudflare.com/ajax/libs/jqwidgets/11.1.3/jqwidgets/jqxgrid.sort.min.js',
    	'https://cdnjs.cloudflare.com/ajax/libs/jqwidgets/11.1.3/jqwidgets/jqxexport.min.js',
    	'js/jszip.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
