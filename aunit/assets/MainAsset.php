<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace aunit\assets;
use yii\web\AssetBundle;
/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MainAsset extends AssetBundle {
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'css/site.css',
//		'https://use.fontawesome.com/releases/v5.3.1/css/all.css',
		'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css',
	];
	public $js = [
		'jquery-slimscroll/jquery.slimscroll.min.js',
		'js/bootstrap-notify.min.js',
		'js/window.util.js',
		'js/jQuery.utils.js',
		'https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js',
		'https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.20/lodash.min.js',
		//		'js/lodash.min.js',
		//		'jquery/jquery.blockUI.min.js',
	];
	public $depends = [
		'dmstr\web\AdminLteAsset',
		'aunit\assets\BootboxAsset',
	];
}
