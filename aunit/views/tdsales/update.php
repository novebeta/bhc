<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tdsales */
$this->title                   = str_replace('Menu','',\aunit\components\TUi::$actionMode).'Sales: ' . $model->SalesKode;
$this->params['breadcrumbs'][] = [ 'label' => 'Sales', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->SalesKode, 'url' => [ 'view', 'id' => $model->SalesKode ] ];
$this->params['breadcrumbs'][] = 'Edit';
$params = '&id='.$id;
?>
<div class="tdsale-update">
	<?= $this->render( '_form', [
		'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdsales/update', 'id' => $id ] ),
            'cancel' => Url::toRoute( [ 'tdsales/cancel', 'id' => $id ,'action' => 'create'] )
        ]
//        'url' => [
//            'save'    => Custom::url(\Yii::$app->controller->id.'/update'.$params ),
//            'update'    => Custom::url(\Yii::$app->controller->id.'/update' ),
//            'cancel'    => Custom::url(\Yii::$app->controller->id.'/' ),
//        ]
	] ) ?>
</div>
