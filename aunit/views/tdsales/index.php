<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
use aunit\assets\AppAsset;
AppAsset::register($this);
$this->title = 'Sales';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'SalesNama' => 'Nama Sales',
        'SalesKode' => 'Kode Sales',
        'TeamKode' => 'Team',
        'SalesAlamat' => 'Alamat',
        'SalesKota' => 'Kota',
		'SalesTelepon' => 'Telepon',
        'SalesKeterangan' => 'Keterangan',
        'SalesStatus' => 'Status',
//		'SalesKodeAstra' => 'Kode Astra',
//		'SalesHSOid' => 'HSO ID',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
	'sortname'  => "SalesStatus asc,SalesKode asc,SalesNama",
	'sortorder' => 'asc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Tdsales::className(),
    'Sales',[
		'mode' => isset( $mode ) ? $mode : '',
	] ), \yii\web\View::POS_READY );