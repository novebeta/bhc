<?php
use aunit\components\FormField;
use common\components\Custom;
use aunit\models\Tdteam;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model aunit\models\Tdsales */
/* @var $form yii\widgets\ActiveForm */
//$url['cancel'] = Url::toRoute('tdsales/index');
?>
<div class="tdsale-form">
    <?php
    $form = ActiveForm::begin(['id' => 'frm_tdsales_id']);
    \aunit\components\TUi::form(
        [
            'class' => "row-no-gutters",
            'items' => [
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelSalesKode"],
                        ['class' => "col-sm-4",'items' => ":SalesKode"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelKodeAstra"],
                        ['class' => "col-sm-4",'style' => "padding-left: 1px",'items' => ":KodeAstra"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelSalesHSOid"],
                        ['class' => "col-sm-8",'style' => "padding-left: 1px",'items' => ":SalesHSOid"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelSalesNama"],
                        ['class' => "col-sm-11",'items' => ":SalesNama"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelTeamKode"],
                        ['class' => "col-sm-8",'style' => "padding-left: 1px", 'items' => ":TeamKode"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelSalesAlamat"],
                        ['class' => "col-sm-11",'items' => ":SalesAlamat"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2", 'items' => ":LabelSalesTelepon"],
                        ['class' => "col-sm-8",'style' => "padding-left: 1px",'items' => ":SalesTelepon"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelSalesKeterangan"],
                        ['class' => "col-sm-11",'items' => ":SalesKeterangan"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelSalesStatus"],
                        ['class' => "col-sm-8",'style' => "padding-left: 1px",'items' => ":SalesStatus"],
                    ],
                ],
            ]
        ],
        ['class' => "row-no-gutters",
            'items'  => [
                [
                    'class' => "col-md-12 pull-left",
                    'items' => $this->render( '../_nav', [
                        'url'=> $_GET['r'],
                        'options'=> [
                            'tdsales.SalesNama' => 'Nama Sales',
                            'tdsales.SalesKode' => 'Kode Sales',
                            'tdsales.TeamKode' => 'Team',
                            'tdsales.SalesAlamat' => 'Alamat',
                            'tdsales.SalesKota' => 'Kota',
                            'tdsales.SalesTelepon' => 'Telepon',
                            'tdsales.SalesKeterangan' => 'Keterangan',
                            'tdsales.SalesStatus' => 'Status',
                        ],
                    ])
                ],
                ['class' => "col-md-12 pull-right",
                    'items'  => [
                        ['class' => "pull-right", 'items' => ":btnAction"],
                    ],
                ],
            ],
        ],
        [
            /* label */
            ":LabelSalesKode"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode Sales</label>',
            ":LabelKodeAstra"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode Astra</label>',
            ":LabelSalesHSOid"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">HSO ID</label>',
            ":LabelSalesNama"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Sales</label>',
            ":LabelTeamKode"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode Team</label>',
            ":LabelSalesAlamat"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
            ":LabelSalesTelepon"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon</label>',
            ":LabelSalesKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
            ":LabelSalesStatus"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',

            /* component */
            ":SalesKode"        => $form->field($model, 'SalesKode', ['template' => '{input}'])->textInput(['maxlength' => true]),
            ":KodeAstra"        => $form->field($model, 'SalesKodeAstra', ['template' => '{input}'])->textInput(['maxlength' => true]),
            ":SalesHSOid"       => $form->field($model, 'SalesHSOid', ['template' => '{input}'])->textInput(['maxlength' => true]),
            ":TeamKode"         => FormField::combo( 'TeamKode', ['name' =>'Tdsales[TeamKode]','config'       => [ 'value' =>  $model->TeamKode ], 'extraOptions' => ['altLabel'     => [ 'TeamNama' ],]] ),
            ":SalesNama"        => $form->field($model, 'SalesNama', ['template' => '{input}'])->textInput(['maxlength' => true]),
            ":SalesAlamat"      => $form->field($model, 'SalesAlamat', ['template' => '{input}'])->textInput(['maxlength' => true]),
            ":SalesTelepon"     => $form->field($model, 'SalesTelepon', ['template' => '{input}'])->textInput(['maxlength' => true]),
            ":SalesKeterangan"  => $form->field($model, 'SalesKeterangan', ['template' => '{input}'])->textInput(['maxlength' => true]),
            ":SalesStatus"      => $form->field($model, 'SalesStatus', ['template' => '{input}'])->dropDownList(['A' => 'AKTIF', 'N' => 'NON AKTIF'], ['maxlength' => true]),

            /* button */
            ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
            ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
            ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
            ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
            ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
        ],
        [
            '_akses' => 'Sales',
            'url_main' => 'tdsales',
            'url_id' => $_GET['id'] ?? '',
        ]
    );
    ActiveForm::end();

    $urlAdd   = Url::toRoute( [ 'tdsales/create','id' => 'new','action'=>'create']);
    $urlIndex   = Url::toRoute( [ 'tdsales/index' ] );
    $this->registerJs( <<< JS
     
    $('#btnAdd').click(function (event) {	      
        window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });  

     $('#btnCancel').click(function (event) {	  
         window.location.href = '{$url['cancel']}';
    });

     $('#btnSave').click(function (event) {	      
//       $('#frm_tdsales_id').attr('action','{$url['update']}');
       $('#frm_tdsales_id').submit();
    });    
     
     
     
JS);

    ?>
</div>
