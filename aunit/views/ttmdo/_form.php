<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttmdo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ttmdo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'DONo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DOTgl')->textInput() ?>

    <?= $form->field($model, 'MotorType')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DOQty')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
