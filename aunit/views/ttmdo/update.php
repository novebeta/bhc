<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttmdo */

$this->title = 'Update Ttmdo: ' . $model->DONo;
$this->params['breadcrumbs'][] = ['label' => 'Ttmdos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->DONo, 'url' => ['view', 'DONo' => $model->DONo, 'DOTgl' => $model->DOTgl, 'MotorType' => $model->MotorType]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ttmdo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
