<?php
use aunit\assets\AppAsset;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
AppAsset::register( $this );
$this->title                     = 'DO';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'DONo'                  => 'No Do',
		'ttmdo.MotorType'       => 'Type',
		'tdmotortype.MotorNama' => 'Nama Motor',
	],
	'cmbTgl'    => [
		'DOTgl' => 'Tanggal',
	],
	'cmbNum'    => [
		'DOQty' => 'Qty'
	],
	'sortname'  => "DOTgl",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttmdo::className(),
	'DO', [
//		'mode' => isset( $mode ) ? $mode : '',
//		'url_delete'    => Url::toRoute( [ 'ttsk/delete'] ),
	] ), \yii\web\View::POS_READY );