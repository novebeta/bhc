<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use aunit\assets\AppAsset;
AppAsset::register($this);
$this->title = 'Warehouse / Pos';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'LokasiKode' => 'Kode',
		'LokasiNama' => 'Nama',
        'LokasiAlamat' => 'Alamat',
        'LokasiStatus' => 'Status',
        'LokasiPetugas' => 'Petugas',
        'LokasiNomor' => 'Nomor',
        'LokasiTelepon' => 'Telepon',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
	'sortname'  => "LokasiStatus asc,LokasiKode asc,LokasiNama",
	'sortorder' => 'asc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Tdlokasi::className(),
    'Warehouse / Pos',[
		'mode' => isset( $mode ) ? $mode : '',
	]  ), \yii\web\View::POS_READY );