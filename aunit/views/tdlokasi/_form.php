<?php
use common\components\Custom;
use kartik\number\NumberControl;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use aunit\models\Tdsales;
use yii\web\JsExpression;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model aunit\models\Tdlokasi */
/* @var $form yii\widgets\ActiveForm */
//$url['cancel'] = Url::toRoute('tdlokasi/index');
$format = <<< SCRIPT
function formattdlokasi(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-6" style="text-align: left">' + result.id + '</div>' +
           '<div class="col-xs-18">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
?>


<div class="tdlokasi-form">
    <?php
    $form = ActiveForm::begin(['id' => 'frm_tdlokasi_id']);
    \aunit\components\TUi::form(
        [
            'class' => "row-no-gutters",
            'items' => [
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelLokasiKode"],
                        ['class' => "col-sm-5",'items' => ":LokasiKode"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelLokasiNomor"],
                        ['class' => "col-sm-1",'items' => ":LokasiNomor"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-1",'items' => ":LabelLokasiJenis"],
                        ['class' => "col-sm-2",'items' => ":LokasiJenis"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-1",'items' => ":LabelNoAccount"],
                        ['class' => "col-sm-2",'items' => ":NoAccount"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-1",'items' => ":LabelLokasiStatus"],
                        ['class' => "col-sm-2",'items' => ":LokasiStatus"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelLokasiNama"],
                        ['class' => "col-sm-9",'items' => ":LokasiNama"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelLokasiPetugas"],
                        ['class' => "col-sm-9",'items' => ":LokasiPetugas"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelLokasiAlamat"],
                        ['class' => "col-sm-9",'items' => ":LokasiAlamat"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelLokasiTelepon"],
                        ['class' => "col-sm-9",'items' => ":LokasiTelepon"],
                    ],
                ],

            ]
        ],
        ['class' => "row-no-gutters",
            'items'  => [
                [
                    'class' => "col-md-12 pull-left",
                    'items' => $this->render( '../_nav', [
                        'url'=> $_GET['r'],
                        'options'=> [
                            'tdlokasi.LokasiKode' => 'Kode',
                            'tdlokasi.LokasiNama' => 'Nama',
                            'tdlokasi.LokasiAlamat' => 'Alamat',
                            'tdlokasi.LokasiStatus' => 'Status',
                            'tdlokasi.LokasiPetugas' => 'Petugas',
                            'tdlokasi.LokasiNomor' => 'Nomor',
                            'tdlokasi.LokasiTelepon' => 'Telepon',
                        ],
                    ])
                ],
                ['class' => "col-md-12 pull-right",
                    'items'  => [
                        ['class' => "pull-right", 'items' => ":btnAction"],
                    ],
                ],
            ],
        ],
        [
            /* label */
            ":LabelLokasiKode"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode Warehouse</label>',
            ":LabelLokasiNomor"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Urut</label>',
            ":LabelNoAccount"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">COA</label>',
            ":LabelLokasiStatus"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',
            ":LabelLokasiJenis"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
            ":LabelLokasiNama"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Warehouse</label>',
            ":LabelLokasiPetugas"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Petugas</label>',
            ":LabelLokasiAlamat"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
            ":LabelLokasiTelepon"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon</label>',

            /* component */
            ":LokasiKode"           => $form->field($model, 'LokasiKode', ['template' => '{input}'])->textInput(['maxlength' => true,]),
            ":LokasiNomor"          => $form->field($model, 'LokasiNomor', ['template' => '{input}'])
                                        ->textInput([
                                            'maxlength' => true,
                                            'type' => 'text',
                                            'style' => 'text-align:right']),
            ":LokasiJenis"          => $form->field($model, 'LokasiJenis', ['template' => '{input}'])
                                        ->dropDownList([
                                            'Lokasi' => 'Lokasi',
                                            'Kas' => 'Kas',
                                        ], ['maxlength' => true]),
            ":NoAccount"            => $form->field($model, 'NoAccount', ['template' => '{input}'])->textInput(['maxlength' => true,]),
            ":LokasiStatus"         => $form->field($model, 'LokasiStatus', ['template' => '{input}'])
                                        ->dropDownList([
                                            'A' => 'AKTIF',
                                            'N' => 'NON AKTIF',
                                            '1' => 'DEALER',
                                        ], ['maxlength' => true]),
            ":LokasiNama"           => $form->field($model, 'LokasiNama', ['template' => '{input}'])->textInput(['maxlength' => true,]),
            ":LokasiPetugas"        => $form->field($model, 'LokasiPetugas', ['template' => '{input}'])
                                        ->widget(Select2::classname(), [
	                                        'value' => $model->LokasiPetugas,
	                                        'theme' => Select2::THEME_DEFAULT,
	                                        'data' => Tdsales::find()->petugas(),
	                                        'pluginOptions' => [
                                                'dropdownAutoWidth' => true,
                                                'templateResult' => new JsExpression('formattdlokasi'),
                                                'templateSelection' => new JsExpression('formattdlokasi'),
                                                'matcher' => new JsExpression('matchCustom'),
                                                'escapeMarkup' => $escape]]),
            ":LokasiAlamat"         => $form->field($model, 'LokasiAlamat', ['template' => '{input}'])->textInput(['maxlength' => true,]),
            ":LokasiTelepon"        => $form->field($model, 'LokasiTelepon', ['template' => '{input}'])->textInput(['maxlength' => true,]),

            /* button */
            ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
            ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
            ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
            ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
            ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),

        ],
        [
            '_akses' => 'Warehouse / Pos',
            'url_main' => 'tdlokasi',
            'url_id' => $_GET['id'] ?? '',
        ]

    );
    ActiveForm::end();

    $urlAdd   = Url::toRoute( [ 'tdlokasi/create','id' => 'new','action'=>'create']);
    $urlIndex   = Url::toRoute( [ 'tdlokasi/index' ] );
    $this->registerJs( <<< JS
     
    $('#btnAdd').click(function (event) {	      
        window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });  

     $('#btnCancel').click(function (event) {	  
         window.location.href = '{$url['cancel']}';
    });

     $('#btnSave').click(function (event) {	      
       // $('#frm_tdlokasi_id').attr('action','{$url['update']}');
       $('#frm_tdlokasi_id').submit();
    });
     
     
     
JS);

    ?>
</div>