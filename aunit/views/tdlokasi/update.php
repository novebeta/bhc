<?php
use common\components\Custom;
use yii\helpers\Url;/* @var $this yii\web\View */
/* @var $model aunit\models\Tdlokasi */
$this->title                   = str_replace('Menu','',\aunit\components\TUi::$actionMode).'Warehouse / Pos: ' . $model->LokasiKode;
$this->params['breadcrumbs'][] = [ 'label' => 'Warehouse / Pos', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->LokasiKode, 'url' => [ 'view', 'id' => $model->LokasiKode ] ];
$this->params['breadcrumbs'][] = 'Edit';
$params = '&id='.$id;
?>
<div class="tdlokasi-update">
	<?= $this->render( '_form', [
		'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdlokasi/update', 'id' => $id ] ),
            'cancel' => Url::toRoute( [ 'tdlokasi/cancel', 'id' => $id ,'action' => 'create'] )
        ]
//        'url' => [
//            'save'    => Custom::url(\Yii::$app->controller->id.'/update'.$params ),
//            'update'    => Custom::url(\Yii::$app->controller->id.'/update' ),
//            'cancel'    => Custom::url(\Yii::$app->controller->id.'/' ),
//        ]
	] ) ?>
</div>
