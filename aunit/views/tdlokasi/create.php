<?php
use yii\helpers\Html;
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tdlokasi */
$this->title                   = 'Tambah Warehouse / Pos';
$this->params['breadcrumbs'][] = [ 'label' => 'Warehouse / Pos', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdlokasi-create">
	<?= $this->render( '_form', [
		'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdlokasi/update', 'id' => $id ] ),
            'cancel' => Url::toRoute( [ 'tdlokasi/cancel', 'id' => $id ,'action' => 'create'] )
        ]
//        'url' => [
//            'save'    => Custom::url(\Yii::$app->controller->id.'/create' ),
//            'update'    => Custom::url(\Yii::$app->controller->id.'/update' ),
//            'cancel'    => Custom::url(\Yii::$app->controller->id.'/' ),
//        ]
	] ) ?>
</div>
