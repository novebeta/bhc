<?php
use aunit\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '[TI] Transfer';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'TINo'         => 'No TI',
		'KodeTrans'    => 'Kode Trans',
		'PosKode'      => 'POS',
		'LokasiTujuan' => 'Lokasi Tujuan',
		'LokasiAsal'   => 'Lokasi Asal',
		'PSNo'         => 'No PS',
		'TIKeterangan' => 'Keterangan',
		'NoGL'         => 'No GL',
		'Cetak'        => 'Cetak',
		'UserID'       => 'User ID',
	],
	'cmbTgl'    => [
		'TITgl'        => 'Tanggal TI',
	],
	'cmbNum'    => [
		'TITotal'      => 'Total',
	],
	'sortname'  => "TITgl",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Tttihd::className(), '[TI] Transfer',
    [
        'url_delete' => Url::toRoute( [ 'tttihd/delete' ] ),
    ]
), \yii\web\View::POS_READY );
