<?php
use aunit\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Tttihd */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="tttihd-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_tttihd_id', 'action' => $url[ 'update' ] ] );
		\aunit\components\TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelTINo" ],
						      [ 'class' => "col-sm-5", 'items' => ":TINo" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelTITgl" ],
						      [ 'class' => "col-sm-5", 'items' => ":TITgl" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-1", 'items' => ":LabelTIJam" ],
						      [ 'class' => "col-sm-5", 'items' => ":TIJam", 'style' => 'padding-left:5px;' ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-5",
					      'items' => [
						      [ 'class' => "col-sm-5", 'items' => ":LabelPINo" ],
						      [ 'class' => "col-sm-12", 'items' => ":PSNo" ],
						      [ 'class' => "col-sm-2", 'items' => ":BtnSearch" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-7",
					      'items' => [
						      [ 'class' => "col-sm-15" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelTC" ],
						      [ 'class' => "col-sm-6", 'items' => ":TC" ],
					      ],
					    ],
				    ],
				  ],
				  [
					  'class' => "row col-md-24",
					  'style' => "margin-bottom: 6px;",
					  'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-18",
					      'items' => [
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-4", 'items' => ":KodeBarang" ],
							        [ 'class' => "col-sm-10", 'items' => ":BarangNama", 'style' => 'padding-left:3px;' ],
							        [ 'class' => "col-sm-4", 'items' => ":Gudang", 'style' => 'padding-left:3px;' ],
							        [ 'class' => "col-sm-4", 'items' => ":Satuan", 'style' => 'padding-left:3px;' ],
							        [ 'class' => "col-sm-2", 'items' => ":BtnInfo" ],
						        ],
						      ],
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "form-group col-md-23",
							          'items' => [
								          [ 'class' => "col-sm-3", 'items' => ":LabelKeterangan" ],
								          [ 'class' => "col-sm-21", 'items' => ":Keterangan" ],
							          ],
							        ],
						        ],
						      ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-6",
					      'items' => [
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-6", 'items' => ":LabelSubTotal" ],
							        [ 'class' => "col-sm-16", 'items' => ":SubTotal" ],
						        ],
						      ],
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-6", 'items' => ":LabelCetak" ],
							        [ 'class' => "col-sm-16", 'items' => ":Cetak" ],
						        ],
						      ],
					      ],
					    ],
				    ],
				  ],
			  ],
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "col-md-24",
				    'items' => [
					    [ 'class' => "pull-right", 'items' => ":btnPrint :btnAction" ]
				    ]
				  ]
			  ]
			],
			[
				":LabelTINo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor TI</label>',
				":LabelTITgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tanggal</label>',
				":LabelTIJam"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jam</label>',
				":LabelPINo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No PS</label>',
				":LabelTC"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">TC</label>',
				":LabelSubTotal"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total</label>',
				":LabelKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelCetak"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Cetak</label>',

				":TINo"       => Html::textInput( 'TINoView', $dsTMutasi[ 'TINoView' ], [ 'class' => 'form-control', 'readonly' => 'readonly', 'tabindex' => '1' ] ) .
				                 Html::textInput( 'TINo', $dsTMutasi[ 'TINo' ], [ 'class' => 'hidden' ] ),
				":PSNo"       => Html::textInput( 'PSNo', $dsTMutasi[ 'PSNo' ], [ 'class' => 'form-control', 'maxlength' => '10', 'tabindex' => '3' ] ),
				":Keterangan" => Html::textInput( 'TIKeterangan', $dsTMutasi[ 'TIKeterangan' ], [ 'class' => 'form-control', 'maxlength' => '150', 'tabindex' => '5' ] ),
				":JT"         => Html::textInput( 'NoGL', $dsTMutasi[ 'NoGL' ], [ 'class' => 'form-control', 'readOnly' => true ] ),
				":Cetak"      => Html::textInput( 'Cetak', $dsTMutasi[ 'Cetak' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":TITgl"      => FormField::dateInput( [ 'name' => 'TITgl', 'config' => [ 'value' => General::asDate( $dsTMutasi[ 'TITgl' ] ) ] ] ),
				":TIJam"      => FormField::timeInput( [ 'name' => 'TIJam', 'config' => [ 'value' => $dsTMutasi[ 'TIJam' ] ] ] ),
				":SubTotal"   => FormField::numberInput( [ 'name' => 'TITotal', 'config' => [ 'id' => 'TITotal', 'value' => $dsTMutasi[ 'TITotal' ], 'readonly' => 'readonly' ] ] ),
				":TC"         => FormField::combo( 'TCTI', [ 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":KodeBarang" => Html::textInput( 'KodeBarang', '', [ 'id' => 'KodeBarang', 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":BarangNama" => Html::textInput( 'BarangNama', '', [ 'id' => 'BarangNama', 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":Gudang"     => Html::textInput( 'LokasiKodeTxt', '', [ 'id' => 'LokasiKodeTxt', 'class' => 'form-control', 'readonly' => 'readonly' ] ),

				":Satuan"     => Html::textInput( 'lblStock', 0, [ 'type' => 'number', 'class' => 'form-control', 'style' => 'color: blue; font-weight: bold;', 'readonly' => 'readonly' ] ),
				":BtnInfo"   => '<button type="button" class="btn btn-default"><i class="fa fa-info-circle fa-lg"></i></button>',
				":BtnSearch" => '<button id="BtnSearch" type="button" class="btn btn-default" tabindex="4"><i class="glyphicon glyphicon-search"></i></button>',
			], [
				'url_main' => 'tttihd',
				'url_id'   => $id,
			]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlCheckStock             = Url::toRoute( [ 'site/check-stock' ] );
$urlDetail                 = $url[ 'detail' ];
$urlPrint                  = $url[ 'print' ];
$urlDaftarPenerimaanSelect = Url::toRoute( [ 'ttpshd/select' ] );
$urlPenerimaanData         = Url::toRoute( [ 'ttpshd/td', 'oper' => 'data' ] );
$readOnly = ( $_GET[ 'action' ] == 'view' ? 'true' : 'false' );
$barang = \common\components\General::cCmd(
	"SELECT tdbarang.BrgKode, tdbarang.BrgNama, tdbarang.BrgBarCode, tdbarang.BrgGroup, tdbarang.BrgSatuan, tdbarang.BrgHrgBeli, tdbarang.BrgHrgJual, tdbarang.BrgRak1, tdbarang.BrgMinStock, tdbarang.BrgMaxStock, tdbarang.BrgStatus, SUM(SaldoQty) AS SaldoQty, tdbaranglokasi.LokasiKode 
            FROM tdbarang 
            INNER JOIN tdbaranglokasi ON tdbaranglokasi.BrgKode = tdbarang.BrgKode 
            GROUP BY tdbarang.BrgKode HAVING SUM(SaldoQty) > 0 
            UNION 
            SELECT tdbarang.BrgKode, tdbarang.BrgNama, tdbarang.BrgBarCode, tdbarang.BrgGroup, tdbarang.BrgSatuan, tdbarang.BrgHrgBeli,tdbarang.BrgHrgJual, tdbarang.BrgRak1, tdbarang.BrgMinStock, tdbarang.BrgMaxStock, tdbarang.BrgStatus ,SUM(SaldoQty) AS SaldoQty, tdbaranglokasi.LokasiKode
            FROM tdbarang 
            INNER JOIN tdbaranglokasi ON tdbaranglokasi.BrgKode = tdbarang.BrgKode 
            INNER JOIN tttiit ON tttiit.BrgKode = tdbarang.BrgKode
            WHERE tttiit.TINo = :TINo GROUP BY tdbarang.BrgKode 
            ORDER BY BrgStatus, BrgKode", [ ':TINo' => $dsTMutasi[ 'TINo' ] ] )->queryAll();
$DtLokasi = \common\components\General::cCmd( "SELECT * FROM tdLokasi Order By LokasiStatus, LokasiNomor" )->queryAll();
$this->registerJsVar( '__Barang', json_encode( $barang ) );
$this->registerJsVar( '__dtLokasi', json_encode( $DtLokasi ) );
$this->registerJsVar( '__model', $dsTMutasi );
$this->registerJs( <<< JS
    var __BrgKode = JSON.parse(__Barang);
     __BrgKode = $.map(__BrgKode, function (obj) {
                      obj.id = obj.BrgKode;
                      obj.text = obj.BrgKode;
                      return obj;
                    });
     var __BrgNama = JSON.parse(__Barang);
     __BrgNama = $.map(__BrgNama, function (obj) {
                      obj.id = obj.BrgNama;
                      obj.text = obj.BrgNama;
                      return obj;
                    });
     var __LokasiKode = JSON.parse(__dtLokasi);
     __LokasiKode = $.map(__LokasiKode, function (obj) {
                      obj.id = obj.LokasiKode;
                      obj.text = obj.LokasiKode;
                      return obj;
                    });
      function HitungTotal(){
        let TITotal = $('#detailGrid').jqGrid('getCol','Jumlah',false,'sum');
        $('#TITotal').utilNumberControl().val(TITotal);
      }         
      window.HitungTotal = HitungTotal;
      function HitungBawah(){
      }         
      window.HitungBawah = HitungBawah;
      function checkStok(BrgKode,BarangNama,LokasiAsal){
           $.ajax({
                url: '$urlCheckStock',
                type: "POST",
                data: { 
                    BrgKode: BrgKode,
                    LokasiKode: LokasiAsal
                },
                success: function (res) {
                    $('[name=LokasiKode]').val(res.LokasiKode);
                    $('#KodeBarang').val(BrgKode);
                    $('[name=BarangNama]').val(BarangNama);
                    $('[name=lblStock]').val(res.SaldoQtyReal);
                },
                error: function (jXHR, textStatus, errorThrown) {
                    
                },
                async: false
            });
      }
      window.checkStok = checkStok;
     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 260,
        extraParams: {
            TINo: $('input[name="TINo"]').val()
        },
        rownumbers: true,  
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,     
        colModel: [
            { template: 'actions'  },
            {
                name: 'BrgKode',
                label: 'Kode',
                editable: true,
                width: 110,
                editor: {
                    type: 'select2',
                    data:__BrgKode,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'BrgRak1'+'"]').val(data.BrgRak1);
                            $('[id="'+rowid+'_'+'TIHrgBeli'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgBeli));
                            window.hitungLine();
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').val(data.BrgNama); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').trigger('change');
						},
						'change': function(){      
                            let rowid = $(this).attr('rowid');
                            checkStok($(this).val(),$('[id="'+rowid+'_'+'BrgNama'+'"]').val(),$('[id="'+rowid+'_'+'LokasiAsal'+'"]').val());
						}
                    }                 
                }
            },            
             {
                name: 'BrgNama',
                label: 'Nama Barang',
                width: 150,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __BrgNama,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'BrgRak1'+'"]').val(data.BrgRak1);
                            $('[id="'+rowid+'_'+'TIHrgBeli'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgBeli));
                            window.hitungLine();
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').val(data.BrgKode); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').trigger('change');
						}
                    }              
                }
            },
            {
                name: 'BrgRak1',
                label: 'BIN',
                width: 100,
                editable: true,
            },
            {
                name: 'LokasiAsal',
                label: 'Asal',
                width: 100,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __LokasiKode,   
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    console.log(data);
						    let rowid = $(this).attr('rowid');
						    checkStok($('[id="'+rowid+'_'+'BrgKode'+'"]').val(),$('[id="'+rowid+'_'+'BrgNama'+'"]').val(),$(this).val());
						}
                    }                       
                }
            },
            {
                name: 'TIQty',
                label: 'Qty',
                width: 70,
                editable: true,
                template: 'number'
            },        
            {
                name: 'BrgSatuan',
                label: 'Satuan',
                width: 100,
                editable: true,
            },
            {
                name: 'LokasiTujuan',
                label: 'Tujuan',
                width: 100,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __LokasiKode            
                }
            },
            {
                name: 'TIHrgBeli',
                label: 'Harga Part',
                width: 100,
                editable: true,
                template: 'money'
            },
            {
                name: 'Jumlah',
                label: 'Jumlah',
                width: 100,
                editable: true,
                template: 'money'
            },  
            {
                name: 'TIAuto',
                label: 'TIAuto',
                editable: true,
                hidden: true
            },                  
        ], 
        listeners: {
            afterLoad: function (data) {
             window.HitungTotal();
            }    
        }, 
        onSelectRow : function(rowid,status,e){ 
            if (window.rowId !== -1) return;
            if (window.rowId_selrow === rowid) return;
            if(rowid.includes('new_row') === true){
                $('[name=KodeBarang]').val('--');
                $('[name=LokasiKode]').val('Kosong');
                $('[name=BarangNama]').val('--');
                $('[name=lblStock]').val(0);
                return ;
            };
            let r = $('#detailGrid').utilJqGrid().getData(rowid).data;
            checkStok(r.BrgKode,r.BrgNama,r.LokasiAsal);
            window.rowId_selrow = rowid;            
        }    
     }).init().fit($('.box-body')).load({url: '$urlDetail'});    
       window.rowId_selrow = -1;
     function hitungLine(){
                let TIHrgBeli = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'TIHrgBeli'+'"]').val()));
                let TIQty = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'TIQty'+'"]').val()));
                $('[id="'+window.cmp.rowid+'_'+'Jumlah'+'"]').val(TIHrgBeli * TIQty);
                window.HitungTotal();
             }
             window.hitungLine = hitungLine;
     
     $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
             window.cmp.rowid = rowid;    
              
             if(rowid.includes('new_row')){
              var BrgKode = $('[id="'+rowid+'_'+'BrgKode'+'"]');
              var dtBrgKode = BrgKode.select2('data');
              if (dtBrgKode.length > 0){
                  var dt = dtBrgKode[0];
                  BrgKode.val(dt.BrgKode).trigger('change').trigger({
                    type: 'select2:select',
                    params: {
                        data: dt
                    }
                });
              }
            $('[id="'+rowid+'_'+'TIQty'+'"]').val(1);
          }
             
              $('[id="'+window.cmp.rowid+'_'+'TIQty'+'"]').on('keyup', function (event){
                  hitungLine();
              });
              $('[id="'+window.cmp.rowid+'_'+'TIHrgBeli'+'"]').on('keyup', function (event){
                  hitungLine();
              });
             hitungLine();
         });
    	
	$('#btnSave').click(function (event) {	 
	    let Nominal = parseFloat($('#TITotal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Total tidak boleh 0', size: 'small'});
               return;
           }  
        $('input[name="SaveMode"]').val('ALL');
        $('#form_tttihd_id').attr('action','{$url['update']}');
        $('#form_tttihd_id').submit();
    });
        
JS
);
$urlPS = Url::toRoute( [ 'ttpshd/ti-find-ps' ] );
$this->registerJsVar( '__urlPS', $urlPS );
$this->registerJs( <<< JS
    
    window.getUrlPS = function(){
        let PSNo = $('input[name="PSNo"]').val();
        return __urlPS + ((PSNo === '' || PSNo === '--') ? '':'&check=off&cmbTxt1=PSNo&txt1='+PSNo);
    } 
    
    $('#BtnSearch').click(function()
     {
        window.util.app.dialogListData.show({
            title: 'Daftar Purchase Sheet / Penerimaan Barang',
            url: getUrlPS()
        },
        function (data) {
             console.log(data);
              if (!Array.isArray(data)){
                 return;
             }
             var itemData = [];
             data.forEach(function(itm,ind) {
                let cmp = $('input[name="PSNo"]');
                cmp.val(itm.header.data.PSNo);
                itm.data.PSNo = itm.header.data.PSNo; 
                itm.data.TINo = __model.TINo; 
                itm.data.TIHrgBeli = itm.data.PSHrgBeli; 
                itm.data.TIQty = itm.data.PSQty; 
                itm.data.TIDiscount = itm.data.PSDiscount;  
                itm.data.LokasiAsal = itm.data.LokasiKode;  
                itm.data.LokasiTujuan = 'Gudang1';  
                itemData.push(itm.data);
            });
            $.ajax({
             type: "POST",
             url: '$urlDetail',
             data: {
                 oper: 'batch',
                 data: itemData,
                 header: btoa($('#form_tttihd_id').serialize())
             },
             success: function(data) {
               window.location.href = "{$url['update']}&oper=skip-load&TINoView={$dsTMutasi['TINoView']}"; 
             },
           });
        })
    });

$('[name=TITgl]').utilDateControl().cmp().attr('tabindex', '2');

JS
);


