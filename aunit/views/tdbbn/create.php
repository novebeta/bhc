<?php
use common\components\Custom;

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tdbbn */

$this->title = 'Tambah Tarif BBN';
$this->params['breadcrumbs'][] = ['label' => 'Tarif BBN', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdbbn-create">

    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdbbn/update', 'id' => $id ] ),
            'cancel' => Url::toRoute( [ 'tdbbn/cancel', 'id' => $id ,'action' => 'create'] )
        ]
    ]) ?>

</div>
