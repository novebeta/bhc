<?php
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Tarif BBN';
$this->params['breadcrumbs'][] = $this->title;
//$dataProvider->pagination      = [ 'pageSize' => 15 ];
$arr = [
	'cmbTxt'    => [
		'Kabupaten' => 'Kabupaten',
		'Tdbbn.MotorType' => 'Type Motor',
	],
	'cmbTgl'    => [
	],
	'cmbNum'    => [
		'BBNBiaya' => 'Tarif BBN',
	],
	'sortname'  => "Kabupaten asc,MotorType asc,BBNBiaya",
	'sortorder' => 'asc'
];
$this->registerJsVar( 'setcmb', $arr );
use aunit\assets\AppAsset;
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Tdbbn::className(),
	'Tarif BBN' ), \yii\web\View::POS_READY );