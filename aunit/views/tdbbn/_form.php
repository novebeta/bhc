<?php
use aunit\components\Menu;
use common\components\Custom;
use aunit\models\Tdmotortype;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use aunit\models\Tdarea;
use yii\web\JsExpression;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model aunit\models\Tdbbn */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function formattdbbn(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-8" style="text-align: left">' + result.id + '</div>' +
           '<div class="col-xs-16">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
//$url['cancel'] = Url::toRoute('tdbbn/index');
?>

<div class="tdbbn-form">
    <?php
    $form = ActiveForm::begin(['id' => 'frm_tdbbn_id']);
    \aunit\components\TUi::form(
        ['class' => "row-no-gutters",
            'items' => [
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2", 'items' => ":LabelKabupaten"],
                        ['class' => "col-sm-12", 'items' => ":Kabupaten"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2", 'items' => ":LabelBBNTahunLalu"],
                        ['class' => "col-sm-7", 'items' => ":BBNTahunLalu"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2", 'items' => ":LabelMotorType"],
                        ['class' => "col-sm-12", 'items' => ":MotorType"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2", 'items' => ":LabelBBNBiaya"],
                        ['class' => "col-sm-7", 'items' => ":BBNBiaya"],
                    ],
                ],
            ]
        ],
        ['class' => "row-no-gutters",
            'items'  => [
                [
                    'class' => "col-md-12 pull-left",
                    'items' => $this->render( '../_nav', [
                        'url'=> $_GET['r'],
                        'options'=> [
                            'tdbbn.Kabupaten' => 'Kabupaten',
                            'tdbbn.MotorType' => 'Type Motor',
                        ],
                    ])
                ],
                ['class' => "col-md-12 pull-right",
                    'items'  => [
                        ['class' => "pull-right", 'items' => ":btnAction"],
                    ],
                ],
            ],
        ],
        [
            ":LabelKabupaten" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kabupaten</label>',
            ":LabelA" => '<label class="control-label" style="margin: 0; padding: 6px 0;">A</label>',
            ":LabelMotorType" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Type Motor</label>',
            ":LabelBBNTahunLalu" => '<label class="control-label" style="margin: 0; padding: 6px 0;">BBN Tahun Lalu</label>',
            ":LabelBBNBiaya" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tarif BBN</label>',

            ":Kabupaten" => $form->field($model, 'Kabupaten', ['template' => '{input}'])
                ->widget(Select2::class, [
                    'value' => $model->Kabupaten,
                    'theme' => Select2::THEME_DEFAULT,
                    'pluginOptions' => ['highlight' => true],
                    'data' => Tdarea::find()->kabupaten()
                ]),
            ":BBNTahunLalu" => $form->field($model, 'BBNTahunLalu', ['template' => '{input}'])
                ->widget(NumberControl::class, [
                    'maskedInputOptions' => ['allowMinus' => false],
                    'displayOptions' => ['class' => 'form-control', 'placeholder' => '0',],
                    'disabled' => !\aunit\components\Menu::showOnlyHakAkses(['Admin', 'Auditor']),]),
            ":BBNBiaya" => $form->field($model, 'BBNBiaya', ['template' => '{input}'])
                ->widget(NumberControl::class, [
                    'maskedInputOptions' => ['allowMinus' => false],
                    'displayOptions' => ['class' => 'form-control', 'placeholder' => '0',],]),
            ":MotorType" => $form->field($model, 'MotorType', ['template' => '{input}'])
                ->widget(Select2::class, [
                    'value' => $model->MotorType,
                    'theme' => Select2::THEME_DEFAULT,
                    'data' => Tdmotortype::find()->comboselect(),
                    'pluginOptions' => [
                        'dropdownAutoWidth' => true,
                        'templateResult' => new JsExpression('formattdbbn'),
                        'templateSelection' => new JsExpression('formattdbbn'),
                        'matcher' => new JsExpression('matchCustom'),
                        'escapeMarkup' => $escape,],
                ]),

            ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
            ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
            ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
            ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
            ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
            ":btnDelete" => Html::button('<i class="glyphicon glyphicon-trash"><br><br>Hapus</i>', ['class' => 'btn bg-dark', 'id' => 'btnDelete', 'style' => 'width:60px;height:60px;background-color:grey;color:white']),
        ],
        [
            '_akses' => 'Tarif BBN',
            'url_main' => 'tdbbn',
            'url_id' => $_GET['id'] ?? '',
        ]
    );
    ActiveForm::end();

    $urlAdd = Url::toRoute(['tdbbn/create', 'id' => 'new','action'=>'create']);
    $urlIndex = Url::toRoute(['tdbbn/index']);
    $akses = $_SESSION['_userProfile']['KodeAkses'];
    $this->registerJs(<<< JS

    $('#btnAdd').click(function (event) {	      
        window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });  

     $('#btnCancel').click(function (event) {	  
         window.location.href = '{$url['cancel']}';
    });

     $('#btnSave').click(function (event) {
         let tahunini = window.util.number.getValueOfEuropeanNumber($('#tdbbn-bbnbiaya-disp').val());
         let tahunlalu = window.util.number.getValueOfEuropeanNumber($('#tdbbn-bbntahunlalu-disp').val());
         var thl = tahunlalu+300000;
         
         var akses = "<?php$akses;?>";
         var a = akses.replace('<?php', '');
         var s = a.replace(';?>', '');
        if(s !== "Admin" && s !== "Auditor"){
             if (tahunini < tahunlalu ){
               $('#tdbbn-bbnbiaya-disp').val(tahunlalu+',00').trigger('change');
               bootbox.alert({message:' Nilai BBN sekarang, tidak boleh lebih kecil dari BBN tahun lalu', size: 'small'});
               return
            }
        }
         
           
           if (tahunini > thl){
               bootbox.alert({message:'Edit max. 300.000 dibanding thn lalu', size: 'small'});
               $('#tdbbn-bbnbiaya-disp').val(thl+',00').trigger('change');
               return
            }
       
       // $('input[name="SaveMode"]').val('ALL');
       // $('#frm_tdbbn_id').attr('action','{$url['update']}');
       $('#frm_tdbbn_id').submit();
       
    });
     
     
     
JS
    );

    ?>
</div>
