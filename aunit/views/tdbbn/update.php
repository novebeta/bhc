<?php
use common\components\Custom;

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tdbbn */

$this->title = str_replace('Menu','',\aunit\components\TUi::$actionMode).'Tarif BBN: ' . $model->Kabupaten;
$this->params['breadcrumbs'][] = ['label' => 'Tarif BBN', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Kabupaten, 'url' => ['view', 'Kabupaten' => $model->Kabupaten, 'MotorType' => $model->MotorType]];
$this->params['breadcrumbs'][] = 'Edit';
$params = '&id='.$id;
?>
<div class="tdbbn-update">


    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdbbn/update', 'id' => $id,'action' => 'update' ] ),
            'cancel' => Url::toRoute( [ 'tdbbn/cancel', 'id' => $id ,'action' => 'create'] )
        ]
    ]) ?>

</div>
