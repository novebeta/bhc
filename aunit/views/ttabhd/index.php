<?php
use aunit\assets\AppAsset;
use yii\helpers\Url;
AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Pengajuan Berkas';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'ABNo'   => 'No Pengajuan Berkas',
		'ABMemo' => 'Keterangan',
		'UserID' => 'UserID',
	],
	'cmbTgl'    => [
		'ABTgl' => 'Tgl Pengajuan'
	],
	'cmbNum'    => [
	],
	'sortname'  => "ABTgl",
	'sortorder' => 'desc',
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttabhd::class,
	'Pengajuan Berkas', [
        'url_delete'    => Url::toRoute( [ 'ttabhd/delete'] ),
//		'colGrid' => Ttdk::colGridPengajuanBerkas()
	] ), \yii\web\View::POS_READY );
