<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttabhd */

$cancel = Url::toRoute( [ 'ttabhd/cancel','id'=>$id, 'action' => 'create' ] );
$this->title                     = 'Tambah Pengajuan Berkas';
?>
<div class="ttabhd-create">
	<?= $this->render( '_form', [
	    'id'=>$id,
        'model' => $model,
        'dsJual' => $dsJual,
        'url'   => [
            'update' => Url::toRoute( [ 'ttabhd/update','id'=>$id, 'action' => 'create' ] ),
			'print'  => Url::toRoute( [ 'ttabhd/print', 'action' => 'print' ] ),
			'cancel' => $cancel,
            'detail' => Url::toRoute( 'ttabit/index' )
        ]
	] ) ?>
</div>
