<?php
use aunit\components\FormField;
use common\components\Custom;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttabhd */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function formatttss(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-10" style="text-align: left">' + result.id + '</div>' +
           '<div class="col-xs-14">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttabhd-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_ttabhd_id', 'action' => $url[ 'update' ] ] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-1", 'items' => ":LabelNoAB" ],
						  [ 'class' => "col-sm-3", 'items' => ":NoAB" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelTglAB" ],
						  [ 'class' => "col-sm-3", 'items' => ":TglAB" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelMemoAB" ],
						  [ 'class' => "col-sm-12", 'items' => ":MemoAB" ],
					  ],
					],
					[
						'class' => "row col-md-24",
						'style' => "margin-bottom: 6px;",
						'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
					],
				]
			],
            [ 'class' => "row-no-gutters",
                'items' => [
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'ABNo'   => 'No Pengajuan Berkas',
                                'ABMemo' => 'Keterangan',
                                'UserID' => 'UserID',
                            ],
                        ])
                    ],
			        ['class' => "pull-right", 'items' => ":btnPrint :btnAction"],
                ]
            ],
			[
				/* label */
				":LabelNoAB"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">No AB</label>',
				":LabelTglAB"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl AB</label>',
				":LabelMemoAB" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				/* component */
				":NoAB"        => Html::textInput( 'ABNoView', $dsJual[ 'ABNoView' ], [ 'class' => 'form-control', 'maxlength' => 18, 'tabindex' => '1' ] ) .
				                  Html::textInput( 'ABNo', $dsJual[ 'ABNo' ], [ 'class' => 'hidden' ] ),
				":MemoAB"      => Html::textInput( 'ABMemo', $dsJual[ 'ABMemo' ], [ 'class' => 'form-control', 'maxlength' => 100, 'tabindex' => '3' ] ),
				":TglAB"       => FormField::dateInput( [ 'name' => 'ABTgl', 'config' => [ 'value' => $dsJual[ 'ABTgl' ] ] ] ),
				/* button */

                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
//                ":btnImport"      => Html::button('<i class="fa fa-arrow-circle-down"><br><br>Import</i>   ', ['class' => 'btn bg-navy ', 'id' => 'btnImport', 'style' => 'width:60px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
//                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),


//                ":btnSave"     => Html::button( '<i class="glyphicon glyphicon-floppy-disk"></i> Simpan', [ 'class' => 'btn btn-info btn-primary ', 'id' => 'btnSave' . Custom::viewClass() ] ),
//				":btnCancel"   => Html::Button( '<i class="fa fa-ban"></i> Batal', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:80px' ] ),
//				":btnAdd"      => Html::button( '<i class="fa fa-plus-circle"></i> Tambah', [ 'class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:80px' ] ),
//				":btnEdit"     => Html::button( '<i class="fa fa-edit"></i> Edit', [ 'class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:80px' ] ),
//				":btnDaftar"   => Html::Button( '<i class="fa fa-list"></i> Daftar ', [ 'class' => 'btn btn-primary ', 'id' => 'btnDaftar', 'style' => 'width:80px' ] ),
//				":btnPrint"    => Html::button( '<i class="glyphicon glyphicon-print"></i>  Print ', [ 'class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:80px' ] ),
			],
            [
                '_akses' => 'Pengajuan Berkas',
                'url_main' => 'ttabhd',
                'url_id' => $_GET['id'] ?? '',
                'url_delete'    => Url::toRoute(['ttabhd/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlAdd    = Url::toRoute( [ 'ttabhd/create', 'action' => 'create' ] );
$urlIndex  = Url::toRoute( [ 'ttabhd/index', 'id' => $id ] );
$urlPrint  = $url[ 'print' ];
$urlDetail = Url::toRoute( [ 'ttabit/index' ] );
$urlDK     = Url::toRoute( [ 'ttdk/select-pengajuan' ] );
$urlLoop   = Url::toRoute( [ 'ttabit/index' ] );
$readOnly  = ( ($_GET[ 'action' ] == 'view' || $_GET[ 'action' ] == 'display') ? 'true' : 'false' );
$this->registerJs( <<< JS
     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 350,
        loadonce:true,
        rowNum: 1000,
        extraParams: {
            ABNo: '$model->ABNo'
        },
        postData: {
            ABNo: '$model->ABNo'
        },
        navButtonTambah: false,
        navButtonEdit:false,
        rownumbers: true,   
        readOnly: $readOnly,       
        colModel: [
            { template: 'actions' },
            {
                name: 'DKNo',
                label: 'No Dat Kon',
                editable: true,
                width: 80,
            },
            {
                name: 'DKTgl',
                label: 'Tanggal DK',
                width: 80,
                editable: true,
            },
            {
                name: 'CusNama',
                label: 'Nama Konsumen',
                width: 200,
                editable: true,
            },
            {
                name: 'CusKabupaten',
                label: 'Kabupaten',
                width: 125,
                editable: true,
            },
            {
                name: 'MotorType',
                label: 'Type',
                width: 195,
                editable: true,
            },
            {
                name: 'MotorNoMesin',
                label: 'Nomor Mesin',
                width: 100,
                editable: true,
            },
            {
                name: 'MotorTahun',
                label: 'Tahun',
                width: 50,
                editable: true,
            },
            {
                name: 'BBN',
                label: 'BBN',
                width: 100,
                editable: true,
                template: 'money',
            },           
        ],     
        
     }).init().fit($('.box-body')).load({url: '$urlDetail'});
    $('#detailGrid').jqGrid('navButtonAdd',"#detailGrid-pager",{ 
        caption: ' Tambah',
        buttonicon: 'glyphicon glyphicon-plus',
        onClickButton:  function() {
            window.util.app.dialogListData.closeTriggerFromIframe = function(){ 
                var iframe = window.util.app.dialogListData.iframe();
                 var selRowId = iframe.contentWindow.util.app.dialogListData.gridFn.getSelectedRow(true);
                console.log(selRowId);                
                $.ajax({
                     type: "POST",
                     url: '$urlLoop',
                     data: {
                         oper: 'batch',
                         data: selRowId,
                         header: {
                             'ABNo': $('[name="ABNo"]').val()
                         }
                     },
                     success: function(data) {
                        iframe.contentWindow.util.app.dialogListData.gridFn.mainGrid.jqGrid().trigger('reloadGrid');
                     },
                   });
                return false;
            }
            // window.util.app.dialogListData.close = function (data) {
            //     this.dialog.modal('hide');
            //     console.log(data); 
            // },
            window.util.app.dialogListData.show({
                title: 'Daftar Data Konsumen untuk Pengajuan Berkas Nomor : {$dsJual["ABNoView"]}',
                url: '$urlDK'
            },function(){ $('#detailGrid').utilJqGrid().load(); });
        }, 
        position: "last", 
        title:"", 
        cursor: "pointer"
    });
    
    $('#btnSave').click(function (event) {	      
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttabhd_id').attr('action','{$url['update']}');
        $('#form_ttabhd_id').submit();
    }); 
    
    $('#btnPrint').click(function (event) {	      
        $('#form_ttabhd_id').attr('action','$urlPrint');
        $('#form_ttabhd_id').attr('target','_blank');
        $('#form_ttabhd_id').submit();
	  }); 
    
    
     $('#btnCancel').click(function (event) {	      
       $('#form_ttabhd_id').attr('action','{$url['cancel']}');
       $('#form_ttabhd_id').submit();
    });
     
     $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });   
     
    $('[name=ABTgl]').utilDateControl().cmp().attr('tabindex', '2');
    
    
    
JS
);