<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttabhd */
$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel' . $params );
$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode).' Pengajuan Berkas: ' .$dsJual[ 'ABNoView' ];
?>
<div class="ttabhd-update">
	<?= $this->render( '_form', [
        'id'=>$id,
		'model' => $model,
        'dsJual' => $dsJual,
		'url'   => [
			'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
			'cancel' => $cancel,
            'detail'    => Custom::url(\Yii::$app->controller->id.'/detail' )
		]
	] ) ?>
</div>
