<?php
use kartik\alert\AlertBlock;
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this \yii\web\View */
/* @var $content string */
aunit\assets\MainAsset::register( $this );
//	dmstr\web\AdminLteAsset::register( $this );
//	$directoryAsset = Yii::$app->assetManager->getPublishedUrl( '@vendor/almasaeed2010/adminlte/dist' );
$directoryAsset = Url::base( true );
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--        <meta http-equiv="refresh" content="--><?php //echo Yii::$app->params['sessionTimeoutSeconds']; ?><!--;"/>-->
	<?= Html::csrfMetaTags() ?>
    <title><?= Html::encode( $this->title ) ?></title>
	<?php $this->head() ?>
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <script src="js/lockr.js" type="text/javascript"></script>
    <script>
        var AdminLTEOptions = {
            sidebarExpandOnHover: true,
            enableBoxRefresh: true,
            enableBSToppltip: true
        };
    </script>
    <style>
        a .sidebar-toggle {
            height: 50px;
        }

        hr {
            margin: 5px;
        }


        .container-fluid {
            padding-right: 0;
            padding-left: 0;
            padding-bottom: 2px;
        }

        .divider {
            height: 1px;
            width: 97%;
            display: block; /* for use on default inline elements like span */
            margin: 9px 0;
            overflow: hidden;
            /*background-color: #8aa4af;*/
            /*box-shadow: 0 0 10px 1px #8aa4af;*/
            background-image: -webkit-linear-gradient(left, #707070, #e3e3e3, #707070);
            background-image: -moz-linear-gradient(left, #707070, #e3e3e3, #707070);
            background-image: -ms-linear-gradient(left, #707070, #e3e3e3, #707070);
            background-image: -o-linear-gradient(left, #707070, #e3e3e3, #707070);
        }

        /*.divider::after{*/
        /*    display: block;*/
        /*    height: 10px;*/
        /*    content: '';*/
        /*}*/

        .panel {
            margin-bottom: 0px;
        }

        .panel-body {
            padding: 10px;
        }

        .content {
            min-height: 150px;
            padding: 5px 10px 5px 10px;
        }

        .content-header {
            padding-top: 5px;
        }

        .content-header > .breadcrumb {
            top: 5px;
        }

        .table-striped > tbody > tr:nth-of-type(odd) {
            background-color: #dff2fa;
        }

        .ui-jqgrid .ui-jqgrid-labels th.ui-th-column {
            background-color: #ffd26b;
            background-image: none
        }

        .ui-jqgrid .ui-jqgrid-btable tbody tr.jqgrow td.jqgrid-rownum {
            background-color: transparent;
            background-image: none;
        }

        .table > thead > tr > td.success, .table > tbody > tr > td.success, .table > tfoot > tr > td.success, .table > thead > tr > th.success, .table > tbody > tr > th.success, .table > tfoot > tr > th.success, .table > thead > tr.success > td, .table > tbody > tr.success > td, .table > tfoot > tr.success > td, .table > thead > tr.success > th, .table > tbody > tr.success > th, .table > tfoot > tr.success > th{
            background-color: #0eaaf5;
            font-weight: bolder;
        }

        /*.ui-jqgrid .ui-jqgrid-bdiv {*/
        /*    overflow-x:hidden !important;*/
        /*    overflow-y:auto !important;*/
        /*}*/

        .ui-jqgrid .loading, .loading_pivot {
            border: none;
            background-color: transparent;
            top: 35%;
        }

        .ui-pg-div {
            color: #fe0000;
        }

        .select2-container--default .select2-results__option[aria-selected=true],
        .select2-container--default .select2-results__option[aria-selected=true]:hover {
            background-color: #ddd;
            color: #444;
        }

        .select2-container--default .select2-results__option[aria-selected=true] {
            background-color: #ddd;
        }

        .select2-container--default .select2-results__option--highlighted[aria-selected] {
            background-color: #F0544C;
            color: white;
        }
        .select2-container--default.select2-container--focus, .select2-selection.select2-container--focus,
        .select2-container--default:focus, .select2-selection:focus, .select2-container--default:active,
        .select2-selection:active {
            outline: 1px solid darkgrey;
        }

        .material-switch > input[type="checkbox"] {
            display: none;
        }

        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 40px;
        }

        label {
            margin-top: 5px;
            margin-bottom: 0;
        }

        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position: absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
        }

        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }

        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }

        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }

        .disabledbutton {
            pointer-events: none;
            opacity: 0.6;
        }

        a {
            color: #fe0000;
        }

        a:hover,
        a:active,
        a:focus {
            outline: none;
            text-decoration: none;
            color: #ff4c4c;
        }

        .select2-selection {
            height: 31px;
        }

        /** jquery ajax animation   */
        .ajaxloading {
            display: none;
            position: fixed;
            z-index: 2000;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba(255, 255, 255, .8) url("img/Spin-1s-200px.gif") 50% 50% no-repeat;
        }

        .center {
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 50%;
        }

        /* When the body has the loading class, we turn
		   the scrollbar off with overflow:hidden */
        body.loading .modal {
            overflow: hidden;
        }

        /* Anytime the body has the loading class, our
		   modal element will be visible */
        body.loading .modal {
            display: block;
        }

        [data-notify="container"] {
            width: auto;
            font-family: 'Courier New', Courier, monospace;
            font-weight: bold;
        }

        .bootbox > .modal-dialog > .modal-content > .modal-header {
            background-color: #f4f4f4;
            border-top-left-radius: 6px;
            border-top-right-radius: 6px;
        }

        .bootbox > .modal-dialog > .modal-content > .modal-header > .modal-title {
            font-weight: bold;
            font-size: larger;
        }
    </style>
</head>
<body class="hold-transition skin-red fixed sidebar-mini">
<? echo AlertBlock::widget( [
	'useSessionFlash' => true,
	'type'            => AlertBlock::TYPE_GROWL,
	'closeButton'     => null
] ); ?>
<?php $this->beginBody() ?>
<div class="wrapper">
	<?= $this->render( 'header.php' ) ?>
	<?= $this->render( 'left.php' ) ?>
	<?= $this->render( 'content.php', [ 'content' => $content ] ) ?>
</div>
<?php $this->endBody() ?>
<div class="modal ajaxloading"></div>
</body>
</html>
<?php $this->endPage() ?>
<script>
    window.FontAwesomeConfig = {searchPseudoElements: true};
    $(function () {
        var url = $(location).attr('href').replace("#", "");
        $('ul.sidebar-menu a').filter(function () {
            return decodeURIComponent(this.href) == decodeURIComponent(url);
        }).parent().addClass('active');
        $('ul.treeview-menu a').filter(function () {
            return decodeURIComponent(this.href) == decodeURIComponent(url);
        }).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');
        $('#sidebar-form').on('submit', function (e) {
            e.preventDefault();
        });
        $('.sidebar-menu li.active').data('lte.pushmenu.active', true);
        $('#search-input').on('keyup', function () {
            var term = $('#search-input').val().trim();
            if (term.length === 0) {
                $('.sidebar-menu li').each(function () {
                    $(this).show(0);
                    $(this).removeClass('active');
                    if ($(this).data('lte.pushmenu.active')) {
                        $(this).addClass('active');
                    }
                });
                return;
            }
            $('.sidebar-menu li').each(function () {
                if ($(this).text().toLowerCase().indexOf(term.toLowerCase()) === -1) {
                    $(this).hide(0);
                    $(this).removeClass('pushmenu-search-found', false);
                    if ($(this).is('.treeview')) {
                        $(this).removeClass('active');
                    }
                } else {
                    $(this).show(0);
                    $(this).addClass('pushmenu-search-found');
                    if ($(this).is('.treeview')) {
                        $(this).addClass('active');
                    }
                    var parent = $(this).parents('li').first();
                    if (parent.is('.treeview')) {
                        parent.show(0);
                    }
                }
                if ($(this).is('.header')) {
                    $(this).show();
                }
            });
            $('.sidebar-menu li.pushmenu-search-found.treeview').each(function () {
                $(this).find('.pushmenu-search-found').show(0);
            });
        });
        var _loginTime = "<?=date( "Y-m-d H:i:s", Yii::$app->session->get( '_loginTime' ) );?>";
        var dt = new Date(_loginTime);
        var _now = "<?=date( "Y-m-d H:i:s" );?>";
        var dtNow = new Date(_now);
        setInterval(function () {
            dtNow.setSeconds(dtNow.getSeconds() + 1);
            let date1_ms = dt.getTime();
            let date2_ms = dtNow.getTime();
            let difference_ms = date2_ms - date1_ms;
            difference_ms = difference_ms / 1000;
            let seconds = Math.floor(difference_ms % 60);
            difference_ms = difference_ms / 60;
            let minutes = Math.floor(difference_ms % 60);
            difference_ms = difference_ms / 60;
            let hours = Math.floor(difference_ms % 24);
            $("#duration").html(("00" + hours).slice(-2) + ':' + ("00" + minutes).slice(-2) + ':' + ("00" + seconds).slice(-2));
            let options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute: '2-digit', second: '2-digit'};
            $("#timer").html(dtNow.toLocaleTimeString('id-ID', options));
        }, 1000);
        $(document).ready(function () {
            const queryString = window.location.search;
            const urlParams = new URLSearchParams(queryString);
            const target = urlParams.get('oper');
            if (target === 'flush') {
                Lockr.flush();
            }
        });
    });
</script>