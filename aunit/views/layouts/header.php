<?php
use common\components\Custom;
use yii\helpers\Html;
/* @var $this \yii\web\View */
/* @var $content string */
$logo_mini = "HSYS";
$logo_lg = Yii::$app->name;
if(Custom::isFinance()){
	$logo_mini = 'HFIN';
	$logo_lg = 'HFIN - Finance';
}
?>
<header class="main-header">
	<?= Html::a( '<span class="logo-mini">'.$logo_mini.'</span><span class="logo-lg">' . $logo_lg. '</span>', Yii::$app->homeUrl, [ 'class' => 'logo' ] ) ?>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu hidden-xs" style="float: left;">
            <h3 style="margin-top: 12px;color: #ffffff"><?=Yii::$app->session->get( '_company' )['nama'];?></h3>
        </div>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="../../dist/img/customers-icon-35906.png" class="user-image" alt="User Image">
                        <span><?=Yii::$app->session->get( '_profile' );?></span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>
