<?php

use yii\helpers\Html;
use yii\helpers\Url;

aunit\assets\MainAsset::register( $this );
$directoryAsset = Url::base( true );

$this->beginPage()
?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <?php $this->head() ?>
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <script src="js/lockr.js" type="text/javascript"></script>
        <script>

            var AdminLTEOptions = {
                sidebarExpandOnHover: true,
                enableBoxRefresh: true,
                enableBSToppltip: true
            };
        </script>
        <style>
            a .sidebar-toggle {
                height: 50px;
            }

            hr {
                margin: 5px;
            }

            .container-fluid {
                padding-right: 0;
                padding-left: 0;
                padding-bottom: 2px;
            }

            .divider {
                height: 1px;
                width: 97%;
                display: block; /* for use on default inline elements like span */
                margin: 9px 0;
                overflow: hidden;
                /*background-color: #8aa4af;*/
                /*box-shadow: 0 0 10px 1px #8aa4af;*/
                background-image: -webkit-linear-gradient(left, #707070, #e3e3e3, #707070);
                background-image: -moz-linear-gradient(left, #707070, #e3e3e3, #707070);
                background-image: -ms-linear-gradient(left, #707070, #e3e3e3, #707070);
                background-image: -o-linear-gradient(left, #707070, #e3e3e3, #707070);
            }

            /*.divider::after{*/
            /*    display: block;*/
            /*    height: 10px;*/
            /*    content: '';*/
            /*}*/

            .panel {
                margin-bottom: 0px;
            }

            .panel-body {
                padding: 10px;
            }

            .content {
                min-height: 150px;
                padding: 5px 10px 5px 10px;
            }

            .content-header {
                padding-top: 5px;
            }

            .content-header > .breadcrumb {
                top: 5px;
            }

            .table-striped > tbody > tr:nth-of-type(odd) {
                background-color: #dff2fa;
            }

            .ui-jqgrid .ui-jqgrid-labels th.ui-th-column {
                background-color: #ffd26b;
                background-image: none
            }

            .ui-jqgrid .ui-jqgrid-btable tbody tr.jqgrow td.jqgrid-rownum {
                background-color: transparent;
                background-image: none;
            }

            .table > thead > tr > td.success, .table > tbody > tr > td.success, .table > tfoot > tr > td.success, .table > thead > tr > th.success, .table > tbody > tr > th.success, .table > tfoot > tr > th.success, .table > thead > tr.success > td, .table > tbody > tr.success > td, .table > tfoot > tr.success > td, .table > thead > tr.success > th, .table > tbody > tr.success > th, .table > tfoot > tr.success > th{
                background-color: #0eaaf5;
                font-weight: bolder;
            }

            /*.ui-jqgrid .ui-jqgrid-bdiv {*/
            /*    overflow-x:hidden !important;*/
            /*    overflow-y:auto !important;*/
            /*}*/

            .ui-jqgrid .loading, .loading_pivot {
                border: none;
                background-color: transparent;
                top: 35%;
            }

            .ui-pg-div {
                color: #fe0000;
            }

            .select2-container--default .select2-results__option[aria-selected=true],
            .select2-container--default .select2-results__option[aria-selected=true]:hover {
                background-color: #ddd;
                color: #444;
            }

            .select2-container--default .select2-results__option[aria-selected=true] {
                background-color: #ddd;
            }

            .select2-container--default .select2-results__option--highlighted[aria-selected] {
                background-color: #F0544C;
                color: white;
            }

            .material-switch > input[type="checkbox"] {
                display: none;
            }

            .material-switch > label {
                cursor: pointer;
                height: 0px;
                position: relative;
                width: 40px;
            }

            label {
                margin-top: 5px;
                margin-bottom: 0;
            }

            .material-switch > label::before {
                background: rgb(0, 0, 0);
                box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
                border-radius: 8px;
                content: '';
                height: 16px;
                margin-top: -8px;
                position: absolute;
                opacity: 0.3;
                transition: all 0.4s ease-in-out;
                width: 40px;
            }

            .material-switch > label::after {
                background: rgb(255, 255, 255);
                border-radius: 16px;
                box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
                content: '';
                height: 24px;
                left: -4px;
                margin-top: -8px;
                position: absolute;
                top: -4px;
                transition: all 0.3s ease-in-out;
                width: 24px;
            }

            .material-switch > input[type="checkbox"]:checked + label::before {
                background: inherit;
                opacity: 0.5;
            }

            .material-switch > input[type="checkbox"]:checked + label::after {
                background: inherit;
                left: 20px;
            }

            .disabledbutton {
                pointer-events: none;
                opacity: 0.6;
            }

            a {
                color: #fe0000;
            }

            a:hover,
            a:active,
            a:focus {
                outline: none;
                text-decoration: none;
                color: #ff4c4c;
            }

            .select2-selection {
                height: 31px;
            }
            /** jquery ajax animation   */
            .ajaxloading {
                display:    none;
                position:   fixed;
                z-index:    2000;
                top:        0;
                left:       0;
                height:     100%;
                width:      100%;
                background: rgba( 255, 255, 255, .8 )
                url("img/Spin-1s-200px.gif")
                50% 50%
                no-repeat;
            }

            .center {
                display: block;
                margin-left: auto;
                margin-right: auto;
                width: 50%;
            }
            /* When the body has the loading class, we turn
			   the scrollbar off with overflow:hidden */
            body.loading .modal {
                overflow: hidden;
            }

            /* Anytime the body has the loading class, our
			   modal element will be visible */
            body.loading .modal {
                display: block;
            }

            [data-notify="container"] {
                width: auto;
                font-family: 'Courier New', Courier, monospace;
                font-weight: bold;
            }

            .bootbox > .modal-dialog > .modal-content > .modal-header {
                background-color: #f4f4f4;
                border-top-left-radius: 6px;
                border-top-right-radius: 6px;
            }
            .bootbox > .modal-dialog > .modal-content > .modal-header > .modal-title {
                font-weight: bold;
                font-size: larger;
            }
        </style>
    </head>
    <body class="hold-transition skin-red fixed sidebar-mini">
    <?php $this->beginBody() ?>
    <?= $content ?>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>
