<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbmhd */
$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel' . $params );
$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode)." - $title : " . $dsTUang['BMNoView'];
//$this->params[ 'breadcrumbs' ][] = [ 'label' => $title, 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = 'Edit';
?>
<div class="ttbmhd-update">
	<?= $this->render( '_form', [
		'model'   => $model,
		'dsTUang' => $dsTUang,
		'label'   => $label,
		'id'      => $id,
		'url'     => [
			'update' => Custom::url( \Yii::$app->controller->id . '/' . $view . '-update' . $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
			'cancel' => $cancel,
			'detail' => Url::toRoute( [ 'ttbmit/index', 'action' => 'create' ] ),
		]
	] ) ?>
</div>
