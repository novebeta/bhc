<?php
use common\components\General;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use aunit\components\FormField;
use kartik\datecontrol\DateControl;
use yii\web\JsExpression;
use yii\web\View;
use common\components\Custom;
use yii\helpers\Url;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use aunit\models\Tdlokasi;
\aunit\assets\AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbmhd */
/* @var $form yii\widgets\ActiveForm */

$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
<style>
    .minus{
        color: red;
    }
</style>
<div class="ttbmhd-form">
<?php
        $form = ActiveForm::begin(['id' => 'form_ttbmhd_id','action' => $url[ 'update' ]]);
        \aunit\components\TUi::form(
            [
                'class' => "row-no-gutters",
                'items' => [
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "col-sm-1",'items' => ":LabelBMNo"],
                            ['class' => "col-sm-4",'items' => ":BMNo"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-1",'items' => ":LabelBMTgl"],
                            ['class' => "col-sm-3",'items' => ":BMTgl"],
                            ['class' => "col-sm-3",'items' => ":BMJam"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-2",'items' => ":LabelBMMemo"],
                            ['class' => "col-sm-8",'items' => ":BMMemo"],
                        ],
                    ],
                    [
                        'class' => "row col-md-24",
                        'style' => "margin-bottom: 6px;",
                        'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "col-sm-1",'items' => ":LabelSetoran"],
                            ['class' => "col-sm-3",'items' => ":Setoran",'style' => 'padding-left:5px;'],
                            ['class' => "col-sm-1",'style' => 'text-align:center','items' => ":Minus"],
                            ['class' => "col-sm-2",'items' => ":LabelPotongan"],
                            ['class' => "col-sm-3",'items' => ":Potongan",'style' => 'padding-right:5px;'],
                            ['class' => "col-sm-1",'items' => ":SD",'style' => 'text-align:center;'],
                            ['class' => "col-sm-3",'items' => ":LabelBMTotal"],
                            ['class' => "col-sm-3",'items' => ":BMTotal"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-3",'items' => ":LabelNominalBM"],
                            ['class' => "col-sm-3",'items' => ":NominalBM"]
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "col-sm-1",'items' => ":LabelNoAccount"],
                            ['class' => "col-sm-9",'items' => ":NoAccount",'style' => 'padding-left:5px;'],
                            ['class' => "col-sm-1",],
                            ['class' => "col-sm-3",'items' => ":LabelBMPerson"],
                            ['class' => "col-sm-10",'items' => ":BMPerson"]
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "col-sm-1",'items' => ":LabelBMAC"],
                            ['class' => "col-sm-3",'items' => ":BMAC",'style' => 'padding-left:5px;'],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-1",'items' => ":LabelBMCekNo"],
                            ['class' => "col-sm-4",'items' => ":BMCekNo"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-3",'items' => ":LabelBMCekTempo"],
                            ['class' => "col-sm-3",'items' => ":BMCekTempo"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-1",'items' => ":LabelJenisBM"],
                            ['class' => "col-sm-5",'items' => ":JenisBM"],
                        ],
                    ],
                ]
            ],
	        [ 'class' => "row-no-gutters",
	          'items' => [
                  ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                      'items'  => [
                          ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":NoGL"],
                          ['class' => "col-sm-4 pull-right", 'items' => ":LabelNoGL"],
                      ],
                  ],
                  [
                      'class' => "col-md-12 pull-left",
                      'items' => $this->render( '../_nav', [
                          'url'=> $_GET['r'],
                          'options'=> [
                              'BMNo' => 'No Bank Masuk',
                              'BMPerson' => 'Terima Dari',
                              'BMJenis' => 'Jenis',
                              'LeaseKode' => 'Kode Lease',
                              'NoAccount' => 'No Account',
                              'BMCekNo' => 'Nomor Cek',
                              'BMAC' => 'Nomor AC',
                              'BMMemo' => 'Keterangan',
                              'UserID' => 'User ID',
                              'NoGL' => 'No GL',
                          ],
                      ])
                  ],
                  ['class' => "col-md-12 pull-right",
		            'items' => [
			            [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
		            ]
		          ]
	          ]
	        ],
            [
                /* label */
                ":LabelBMNo"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No BM</label>',
                ":LabelBMTgl"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl BM</label>',
                ":LabelBMMemo"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
                ":LabelNominalBM"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal Bank Masuk</label>',
                ":LabelSetoran"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Setoran</label>',
                ":LabelPotongan"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Potongan</label>',
                ":LabelBMTotal"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total Bank Masuk</label>',
                ":LabelNoAccount"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bank</label>',
                ":LabelBMPerson"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Terima Dari</label>',
                ":LabelBMAC"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No A/C</label>',
                ":LabelBMCekNo"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Cek</label>',
                ":LabelBMCekTempo"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Cair</label>',
                ":LabelJenisBM"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
                ":Minus"            => '<label class="control-label" style="margin: 0; padding: 6px 0;">-</label>',
                ":SD"               => '<label class="control-label" style="margin: 0; padding: 6px 0;">=</label>',
                ":LabelNoGL"        => \common\components\General::labelGL( $dsTUang[ 'NoGL' ] ),


                /* component */
                ":NoGL"         => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control', 'readonly'=> true ] ),
                ":BMNo"         => Html::textInput( 'BMNoView', $dsTUang[ 'BMNoView' ], [ 'class' => 'form-control', 'readonly' => true, 'tabindex' => '1' ] ) .
                                   Html::textInput( 'BMNo', $dsTUang[ 'BMNo' ], [ 'class' => 'hidden' ] ),
                ":BMTgl"        => FormField::dateInput( [ 'name' => 'BMTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BMTgl' ] ) ] ] ),
                ":BMCekTempo"   => FormField::dateInput( [ 'name' => 'BMCekTempo', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BMCekTempo' ] ) ] ] ),
                ":BMJam"        => FormField::timeInput( [ 'name' => 'BMJam', 'config'=>['value'=>$dsTUang[ 'BMJam' ]]] ),
                ":BMMemo"       => Html::textInput( 'BMMemo', $dsTUang[ 'BMMemo' ], [ 'class' => 'form-control', 'tabindex' => '3' ] ),
                ":BMPerson"     => Html::textInput( 'BMPerson', $dsTUang[ 'BMPerson' ], [ 'class' => 'form-control', 'tabindex' => '9' ] ),
                ":BMAC"         => Html::textInput( 'BMAC', $dsTUang[ 'BMAC' ], [ 'class' => 'form-control' , 'tabindex' => '10'] ),
                ":BMCekNo"      => Html::textInput( 'BMCekNo', $dsTUang[ 'BMCekNo' ], [ 'class' => 'form-control', 'tabindex' => '11' ] ),
                ":JenisBM"      => Html::textInput( 'BMJenis', $dsTUang[ 'BMJenis' ], [ 'class' => 'form-control', 'tabindex' => '13' ] ),
                ":NoAccount"       => FormField::combo( 'NoAccount', [ 'config' => [ 'id' => 'NoAccount','value' => $dsTUang[ 'NoAccount' ] ,'data'=> \aunit\models\Traccount::find()->bank()], 'options' => ['tabindex' => '8'], ] ),
                ":Setoran"      => FormField::numberInput( [ 'id'=>'TotalSetoran','name' => 'TotalSetoran', 'config' => [ 'value' => $dsTUang[ 'TotalSetoran' ] ] ] ),
                ":Potongan"     => FormField::numberInput( [ 'id'=>'TotalPotongan','name' => 'TotalPotongan', 'config' => [ 'value' => $dsTUang[ 'TotalPotongan' ] ] ] ),
                ":BMTotal"      => FormField::numberInput( [ 'id'=>'TotalBankMasuk', 'name' => 'TotalBankMasuk', 'config' => [ 'value' => $dsTUang[ 'TotalBankMasuk' ] ] ] ),
                ":NominalBM"    => FormField::numberInput( [ 'name' => 'BMNominal', 'config' => [ 'value' => $dsTUang[ 'BMNominal' ] ] ] ),
                /* button */
                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),

            ],
            [
                'url_main'         => 'ttbmhd',
                'url_delete'        => Url::toRoute(['ttbmhd/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
        );
        ActiveForm::end();
        ?>
    </div>
    <?php
$urlDetail = $url['detail'];
$urlPrint = $url['print'];
$url[ 'jurnal' ] = \yii\helpers\Url::toRoute(['ttbmhd/jurnal']);
$readOnly = ( $_GET['action'] == 'view' ? 'true' : 'false' );
$urlLoop = Url::toRoute(['ttbmitpm/index']);
$urlDK = Url::toRoute( [ 'ttdk/select-bm-multi', 'jenis' => 'PenjualanMulti' ,'tgl1'    => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd')] );
$urlAdd   = \aunit\models\Ttbmhd::getRoute( $dsTUang[ 'BMJenis' ] ,['action'=>'create'])[ 'create' ];
$urlIndex = \aunit\models\Ttbmhd::getRoute( $dsTUang[ 'BMJenis' ], [ 'id' => $id ] )[ 'index' ];
$this->registerJsVar('__update', $_GET['action']);
$this->registerJs(<<< JS
    window.updateBankNomnal = false;
     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 230,
        extraParams: {
            BMNo: $('input[name="BMNo"]').val(),
            header: btoa($('#form_ttbmhd_id').serialize())
        },
        postData: {
            BMNo: $('input[name="BMNo"]').val(),
            header: btoa($('#form_ttbmhd_id').serialize())
        },
        rownumbers: true,      
        loadonce:true,
        rowNum: 1000,  
        readOnly: $readOnly,    
        navButtonTambah: false, 
        gridview: true,
        rowattr: function (rd) {
            if (rd.BMJenis ==='Retur Harga' || rd.BMJenis ==='SubsidiDealer2' || rd.BMJenis ==='Insentif Sales') {
                return {"class": "minus"};
            }
        },
        colModel: [
            { template: 'actions' },
            // {
            //     name: 'BMNo',
            //     label: 'BMNo',
            //     editable: true,
            //     width: 80,
            // },
            {
                name: 'DKNo',
                label: 'No DK',
                width: 100,
                editable: true,
            },
            // {
            //     name: 'BMBayar',
            //     label: 'Kredit',
            //     width: 80,
            //     editable: true,
            //     template: 'money'
            // },
            
            {
                name: 'DKTgl',
                label: 'DKTgl',
                width: 100,
                editable: true,
            },
            {
                name: 'BMJenis',
                label: 'Jenis',
                width: 100,
                editable: true,
            },
            {
                name: 'CusNama',
                label: 'Konsumen',
                width: 150,
                editable: true,
            },
            {
                name: 'MotorType',
                label: 'MotorType',
                width: 100,
                editable: true,
            },
            // {
            //     name: 'MotorWarna',
            //     label: 'MotorWarna',
            //     width: 80,
            //     editable: true,
            // },
            {
                name: 'Awal',
                label: 'Awal',
                width: 95,
                editable: true,
                template: 'money',
                editor: {
                    readOnly:true
                }
            },
            {
                name: 'Terbayar',
                label: 'Terbayar',
                width: 95,
                editable: true,
                template: 'money',
                editor: {
                    readOnly:true
                }
            },
            {
                name: 'BMBayar',
                label: 'Bayar',
                width: 95,
                editable: true,
                template: 'money',
                editrules:{
                    custom:true, 
                    custom_func: function(value) {
                        var BMBayar = parseFloat(window.util.number.getValueOfEuropeanNumber(value));
                        var Awal = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Awal'+'"]').val()));
                        var Terbayar = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Terbayar'+'"]').val()));
                        if (BMBayar === 0) 
                            return [false,"Silahkan mengisi bayar."];  
                        else {    
                            var sisa = Awal - Terbayar;
                            if (BMBayar > sisa){
                                $('[id="'+window.cmp.rowid+'_'+'BMBayar'+'"]').val(sisa);
                               return [false,"Nilai Bayar maksimal : " + window.util.number.format(sisa)];   
                            }else{
                                return [true,""];  
                            }
                        }
                    }
                }
            },
            {
                name: 'Sisa',
                label: 'Sisa',
                width: 95,
                editable: true,
                template: 'money',
                editor: {
                    readOnly:true
                }
            },
        ],              
        listeners: {
            afterLoad: function(grid, response) {
                console.log(window.updateBankNomnal);
                var total = 0, setor = 0, potong = 0, count = 0;            
                for(var i = 0; i < response.rows.length; i++) {
                    if (response.rows[i].BMJenis ==='Retur Harga' || response.rows[i].BMJenis ==='SubsidiDealer2' || 
                        response.rows[i].BMJenis ==='Insentif Sales'){                        
                        potong += parseFloat(response.rows[i].BMBayar);
                    }else{
                        setor += parseFloat(response.rows[i].BMBayar);
                    }
                    count++;
                }
                total = setor - potong;
                // var BMTotal = $('#detailGrid').jqGrid('getCol','BMBayar',false,'sum');
                $('#TotalSetoran').utilNumberControl().val(setor);
                $('#TotalPotongan').utilNumberControl().val(potong);
                $('#TotalBankMasuk').utilNumberControl().val(total);
                if(window.updateBankNomnal){
                    $('#BMNominal').utilNumberControl().val(total);
                    window.updateBankNomnal = false;
                }
            },
        } 
        
     }).init().fit($('.box-body')).load({url: '$urlDetail'});

    $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
        window.cmp.rowid = rowid;    
    });

    $('#detailGrid').jqGrid('navButtonAdd',"#detailGrid-pager",{ 
        caption: ' Tambah',
        buttonicon: 'glyphicon glyphicon-plus',
        onClickButton:  function() {
            window.util.app.dialogListData.closeTriggerFromIframe = function(){ 
                var iframe = window.util.app.dialogListData.iframe();
                var selRowId = iframe.contentWindow.util.app.dialogListData.gridFn.getSelectedRow(true);
                // console.log(selRowId);                
                $.ajax({
                        type: "POST",
                        url: '$urlLoop',
                        data: {
                            oper: 'batch',
                            data: selRowId,
                            header: btoa($('#form_ttbmhd_id').serialize())
                        },
                        success: function(data) {
                            if(data.success === false){
                                $.notify({message:data.message},{type: 'warning',z_index: 1100});
                            }
                            iframe.contentWindow.util.app.dialogListData.gridFn.mainGrid.jqGrid().trigger('reloadGrid');
                        },
                    });
                return false;
            }
            window.util.app.dialogListData.show({
                title: 'Daftar Data Konsumen untuk  Nomor : {$dsTUang["BMNoView"]}',
                url: '$urlDK'
            },function(){
                window.updateBankNomnal = true;
                $('#detailGrid').utilJqGrid().load();
            });
            
        }, 
        position: "last", 
        title:"", 
        cursor: "pointer"
    });
        
        
      $('#TotalSetoran-disp').blur(function() {
        $('#BMNominal').utilNumberControl().val($('#TotalSetoran').utilNumberControl().val());
      });
        
      $('#btnPrint').click(function (event) {	      
        $('#form_ttbmhd_id').attr('action','$urlPrint');
        $('#form_ttbmhd_id').attr('target','_blank');
        $('#form_ttbmhd_id').utilForm().submit();
	  }); 
      
      
    $('#btnJurnal').click(function (event) {	
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#form_ttbmhd_id').attr('action','{$url[ 'jurnal' ]}');
                    $('#form_ttbmhd_id').utilForm().submit();
                }
            }
        });
    }); 
        
     $('#btnCancel').click(function (event) {	      
       $('#form_ttbmhd_id').attr('action','{$url['cancel']}');
       $('#form_ttbmhd_id').utilForm().submit();
    });
     
      $('#BMTotal-disp').blur(function() {
        $('#BMNominal').utilNumberControl().val($('#BMTotal').utilNumberControl().val());
      });
     
      $('#btnSave').click(function (event) {
           let Nominal = parseFloat($('#BMNominal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Silahkan mengisi nominal', size: 'small'});
               return;
           }
        $('#form_ttbmhd_id').attr('action','{$url['update']}');
        $('#form_ttbmhd_id').utilForm().submit();
    }); 
      
       $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });    
    
     $('[name=BMTgl]').utilDateControl().cmp().attr('tabindex', '2');
      $('[name=BMCekTempo]').utilDateControl().cmp().attr('tabindex', '12');
      $('[name=TotalPotongan').utilNumberControl().cmp().attr('tabindex', '4');
      $('[name=TotalBankMasuk').utilNumberControl().cmp().attr('tabindex', '5');
      $('[name=TotalSetoran').utilNumberControl().cmp().attr('tabindex', '6');
      $('[name=BMNominal').utilNumberControl().cmp().attr('tabindex', '7');
      //enable kode saat edit request 07/06/2024
	/*
      $( document ).ready(function() {
        if(__update == 'update'){
            $('#NoAccount').attr('disabled',true);
        }
    });*/
      
     
JS
);

