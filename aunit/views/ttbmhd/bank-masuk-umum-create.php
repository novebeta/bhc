<?php
use common\components\Custom;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbmhd */

$params                          = '&id=' . $id . '&action=create';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = 'Tambah - Bank Masuk Umum';
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Bank Masuk Umum', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="ttbmhd-create">
    <?= $this->render('bank-masuk-umum-form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'id' => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/bank-masuk-umum-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail'       => Url::toRoute( [ 'ttbmitcoa/index','action' => 'create' ] ),
        ]
//        'url' => [
//            'update' => Url::toRoute([\Yii::$app->controller->id . '/update', 'id' => $id, 'action' => 'update']),
//            'print' => Url::toRoute([\Yii::$app->controller->id . '/print', 'id' => $id, 'action' => 'update']),
//            'cancel' => Url::toRoute([\Yii::$app->controller->id . '/cancel', 'id' => $id, 'action' => 'update']),
//            'detail'       => Url::toRoute( [ 'ttbmitcoa/index','action' => 'create' ] ),
//        ]
    ]) ?>

</div>
