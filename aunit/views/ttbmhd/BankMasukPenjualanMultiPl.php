<?php
use aunit\assets\AppAsset;
use common\components\Custom;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bank Masuk Penjualan Multi - PL';
$this->params['breadcrumbs'][] = $this->title;
$arr = [
    'cmbTxt'    => [
        'BMNo'       => 'No Bank Masuk',
        'BMPerson'   => 'Terima Dari',
        'BMJenis'    => 'Jenis',
        'LeaseKode'  => 'Kode Lease',
        'NoAccount'  => 'No Account',
        'BMCekNo'    => 'Nomor Cek',
        'BMAC'       => 'Nomor AC',
        'BMMemo'     => 'Keterangan',
        'UserID'     => 'User ID',
        'NoGL'       => 'No GL',	],
    'cmbTgl'    => [
        'BMTgl'      => 'Tgl Bank Masuk',
    ],
    'cmbNum'    => [
        'BMNominal'  => 'Nominal',
    ],
    'sortname'  => "BMTgl",
    'sortorder' => 'desc'
];

AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttbmhd::className(),
	'Bank Masuk Penjualan Multi - PL' ), \yii\web\View::POS_READY );
