<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbmhd */
$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel' . $params );
$this->title                     = str_replace( 'Menu', '', \aunit\components\TUi::$actionMode ) . '- Bank Masuk Umum : ' . $dsTUang['BMNoView'];
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Bank Masuk Umum', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = 'Edit';
?>
<div class="ttbmhd-update">
	<?= $this->render( 'bank-masuk-umum-form', [
		'model'   => $model,
		'dsTUang' => $dsTUang,
		'id'      => $id,
		'url'     => [
			'update' => Custom::url( \Yii::$app->controller->id . '/bank-masuk-umum-update' . $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
			'cancel' => $cancel,
			'detail' => Url::toRoute( [ 'ttbmitcoa/index', 'action' => 'create' ] ),
		]
	] ) ?>
</div>
