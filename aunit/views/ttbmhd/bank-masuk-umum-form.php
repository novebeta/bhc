<?php
use aunit\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use aunit\components\Menu;
use yii\helpers\Url;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbkhd */
/* @var $form yii\widgets\ActiveForm */

$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttbmhd-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_ttbmhd_id', 'action' => $url[ 'update' ] ] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelBMNo" ],
						  [ 'class' => "col-sm-5", 'items' => ":BMNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelBMTgl" ],
						  [ 'class' => "col-sm-2", 'items' => ":BMTgl" ],
						  [ 'class' => "col-sm-2", 'items' => ":BMJam" ],
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelBMMemo" ],
						  [ 'class' => "col-sm-11", 'items' => ":BMMemo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelNominalBM" ],
						  [ 'class' => "col-sm-3", 'items' => ":NominalBM" ]
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelNoAccount" ],
						  [ 'class' => "col-sm-11", 'items' => ":NoAccount" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelBMPerson" ],
						  [ 'class' => "col-sm-8", 'items' => ":BMPerson" ]
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelBMAC" ],
						  [ 'class' => "col-sm-5", 'items' => ":BMAC" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelBMCekNo" ],
						  [ 'class' => "col-sm-4", 'style' => 'padding-left:8px', 'items' => ":BMCekNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelBMCekTempo" ],
						  [ 'class' => "col-sm-3", 'items' => ":BMCekTempo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelJenisBM" ],
						  [ 'class' => "col-sm-3", 'items' => ":JenisBM" ],
					  ],
					],
					[
						'class' => "row col-md-24",
						'style' => "margin-bottom: 6px;",
						'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
					],
				]
			],
			[
				'class' => "row-no-gutters",
				'items' => [
                    ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                        'items'  => [
                            ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":NoGL"],
                            ['class' => "col-sm-4 pull-right", 'items' => ":LabelNoGL"],
                        ],
                    ],
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'BMNo' => 'No Bank Masuk',
                                'BMPerson' => 'Terima Dari',
                                'BMJenis' => 'Jenis',
                                'LeaseKode' => 'Kode Lease',
                                'NoAccount' => 'No Account',
                                'BMCekNo' => 'Nomor Cek',
                                'BMAC' => 'Nomor AC',
                                'BMMemo' => 'Keterangan',
                                'UserID' => 'User ID',
                                'NoGL' => 'No GL',
                            ],
                        ])
                    ],
                    ['class' => "col-md-12 pull-right",
					  'items' => [
						  [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
					  ]
					]
				]
			],
			[
				/* label */
				":LabelBMNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No BM</label>',
				":LabelBMTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl BM</label>',
				":LabelBMMemo"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelNominalBM"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
				":LabelNoAccount"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bank</label>',
				":LabelBMPerson"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Terima Dari</label>',
				":LabelBMAC"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No A/C</label>',
				":LabelBMCekNo"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Cek</label>',
				":LabelBMCekTempo" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Cair</label>',
				":LabelJenisBM"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				":LabelNoGL"       => \common\components\General::labelGL( $dsTUang[ 'NoGL' ] ),
				/* component */
				":BMNo"            => Html::textInput( 'BMNoView', $dsTUang[ 'BMNoView' ], [ 'class' => 'form-control', 'readonly' => true, 'tabindex' => '1' ] ) .
				                      Html::textInput( 'BMNo', $dsTUang[ 'BMNo' ], [ 'class' => 'hidden' ] ),
				":BMTgl"           => FormField::dateInput( [ 'name' => 'BMTgl', 'config' => [ 'id' => 'BMTgl','value' => General::asDate( $dsTUang[ 'BMTgl' ] ) ] ] ),
				":BMCekTempo"      => FormField::dateInput( [ 'name' => 'BMCekTempo', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BMCekTempo' ] ) ] ] ),
				":BMJam"           => FormField::timeInput( [ 'name' => 'BMJam' , 'config' => [ 'value' =>  $dsTUang[ 'BMJam' ] ] ] ),
				":BMMemo"          => Html::textInput( 'BMMemo', $dsTUang[ 'BMMemo' ], [ 'class' => 'form-control', 'tabindex' => '3' ] ),
				":NominalBM"       => FormField::numberInput( [ 'name' => 'BMNominal', 'config' => [ 'value' => $dsTUang[ 'BMNominal' ] ] ] ),
				":BMPerson"        => Html::textInput( 'BMPerson', $dsTUang[ 'BMPerson' ], [ 'class' => 'form-control', 'tabindex' => '5'  ] ),
				":BMAC"            => Html::textInput( 'BMAC', $dsTUang[ 'BMAC' ], [ 'class' => 'form-control', 'tabindex' => '6'  ] ),
				":BMCekNo"         => Html::textInput( 'BMCekNo', $dsTUang[ 'BMCekNo' ], [ 'class' => 'form-control', 'tabindex' => '7'  ] ),
				":JenisBM"         => Html::textInput( 'BMJenis', $dsTUang[ 'BMJenis' ], [ 'class' => 'form-control', 'tabindex' => '9'  ] ),
				":NoAccount"       => FormField::combo( 'NoAccount', [ 'config' => [ 'id' => 'NoAccount','value' => $dsTUang[ 'NoAccount' ] ,'data'=> \aunit\models\Traccount::find()->bank()], 'options' => ['tabindex' => '4'], ] ),
				":BMTotal"         => FormField::numberInput( [ 'name' => 'BMTotal', 'config' => [ 'value' => $dsTUang[ 'BMTotal' ] ] ] ),
				":Lease"           => FormField::combo( 'LeaseKode', [ 'config' => [ 'value' => 'LeaseKode' ], 'extraOptions' => [ 'altLabel' => [ 'LeaseNama' ] ] ] ),
				":NoGL"            => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				/* button */
                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),
			],
            [
                'url_main'         => 'ttbmhd',
                'url_delete'        => Url::toRoute(['ttbmhd/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlDetail = $url[ 'detail' ];
$urlPrint  = $url[ 'print' ];
$readOnly  = ( ($_GET[ 'action' ] == 'view' || $_GET[ 'action' ] == 'display') ? 'true' : 'false' );
$DtAccount = \common\components\General::cCmd( "SELECT NoAccount, NamaAccount, OpeningBalance, JenisAccount, StatusAccount, NoParent FROM traccount 
WHERE JenisAccount = 'Detail' ORDER BY NoAccount" )->queryAll();
$url[ 'jurnal' ] = \yii\helpers\Url::toRoute(['ttbmhd/jurnal']);
$urlAdd   = \aunit\models\Ttbmhd::getRoute( $dsTUang[ 'BMJenis' ],['action'=>'create'] )[ 'create' ];
$urlIndex = \aunit\models\Ttbmhd::getRoute( $dsTUang[ 'BMJenis' ], [ 'id' => $id ] )[ 'index' ];
$this->registerJsVar( 'akses', Menu::getUserLokasi()[ 'KodeAkses' ] );
$this->registerJsVar( '__dtAccount', json_encode( $DtAccount ) );
$this->registerJsVar('__update', $_GET['action']);
$this->registerJs( <<< JS
    var __NoAccount = JSON.parse(__dtAccount);
     __NoAccount = $.map(__NoAccount, function (obj) {
                      obj.id = obj.NoAccount;
                      obj.text = obj.NoAccount;
                      return obj;
                    });
     var __NamaAccount = JSON.parse(__dtAccount);
     __NamaAccount = $.map(__NamaAccount, function (obj) {
                      obj.id = obj.NamaAccount;
                      obj.text = obj.NamaAccount;
                      return obj;
                    });
     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 210,
        extraParams: {
            BMNo: $('input[name="BMNo"]').val()
        },
        rownumbers: true,        
        loadonce:true,
        rowNum: 1000,  
        readOnly: $readOnly,  
        colModel: [
            { template: 'actions' },
            {
                name: 'NoAccount',
                label: 'Kode',
                editable: true,
                width: 140,
                editor: {
                    type: 'select2',
                    data: __NoAccount,  
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    // console.log(data);
						    let rowid = $(this).attr('rowid');
                            $('[id="'+rowid+'_'+'NamaAccount'+'"]').val(data.NamaAccount); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'NamaAccount'+'"]').trigger('change');
						}
                    }                  
                }
            },
            {
                name: 'NamaAccount',
                label: 'Nama',
                width: 250,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __NamaAccount, 
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    // console.log(data);
						    let rowid = $(this).attr('rowid');
                            $('[id="'+rowid+'_'+'NoAccount'+'"]').val(data.NoAccount); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'NoAccount'+'"]').trigger('change');
						}
                    }                              
                }
            },
            {
                name: 'BMKeterangan',
                label: 'Keterangan',
                width: 320,
                editable: true,
            },
            {
                name: 'BMBayar',
                label: 'Kredit',
                width: 200,
                editable: true,
                template: 'money'
            },            
            {
                name: 'BMAutoN',
                label: 'BMAutoN',
                width: 210,
                editable: true,
                hidden: true
            },
        ],    
        listeners: {
            afterLoad: function(){
                let TotalBMBayar = $('#detailGrid').jqGrid('getCol','BMBayar',false,'sum');
                $("#detailGrid-pager_right").html('<label class="control-label" style="margin-right:10px;">Sub Total COA </label>' +
                 '<label style="width:190px;margin-right:25px;">'+TotalBMBayar.toLocaleString("id-ID", {minimumFractionDigits:2})+'</label>');
            }
        }
     }).init().fit($('.box-body')).load({url: '$urlDetail'}); 
     
      $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowId, orgClickEvent) {
        // console.log(orgClickEvent);
        if (orgClickEvent.extraparam.oper === 'add'){
            var BMKeterangan = $('[id="'+rowId+'_'+'BMKeterangan'+'"]');
            BMKeterangan.val($('[name="BMMemo"]').val());
        }        
    });     
     
     $('#btnPrint').click(function (event) {	      
        $('#form_ttbmhd_id').utilForm().submit('$urlPrint', 'POST','_blank');
	  });  
     
     
    $('#btnJurnal').click(function (event) {	
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#form_ttbmhd_id').utilForm().submit('{$url[ 'jurnal' ]}', 'POST');
                }
            }
        });
    }); 
     
     $('#btnCancel').click(function (event) {	      
       $('#form_ttbmhd_id').utilForm().submit('{$url['cancel']}', 'POST');
    });
     
      $('#BMTotal-disp').blur(function() {
        $('#BMNominal').utilNumberControl().val($('#BMTotal').utilNumberControl().val());
      });
     
     $('#btnSave').click(function (event) {	
        let TotalKMBayar = $('#detailGrid').jqGrid('getCol','BMBayar',false,'sum');
        let SubTotal = $('#BMNominal').utilNumberControl().val();
        let BMTgl = new Date($('#BMTgl').val());
        let tglNow = new Date();
        let diffTime = Math.abs(tglNow - BMTgl);
        let diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
        // console.log(TotalKMBayar + ' ' + SubTotal);
        console.log(BMTgl);
        console.log(typeof BMTgl);
        console.log(tglNow);
        console.log(diffDays);
        if (TotalKMBayar !== SubTotal){
            bootbox.alert({ message: "Jumlah nominal dengan subtotal coa berbeda.", size: 'small'});
            return;
        }
        let param = ['Admin', 'Auditor'];
        if(!param.includes(akses)){
         if (diffDays > 3){
            bootbox.alert({ message: "Tgl Transaksi (" + BMTgl + ") lebih kecil dari Tgl Sistem (" + tglNow + ") Silakan Hhubungi Admin untuk melakukan proses Hapus atau Simpan" , size: 'small'});
            return;
         }
         $('#form_ttbmhd_id').utilForm().submit('{$url['update']}', 'POST');
        }
        $('#form_ttbmhd_id').utilForm().submit('{$url['update']}', 'POST');
    });
     
      $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });    
    
    $('[name=BMTgl]').utilDateControl().cmp().attr('tabindex', '2');
    $('[name=BMNominal').utilNumberControl().cmp().attr('tabindex', '4');
    $('[name=BMCekTempo').utilNumberControl().cmp().attr('tabindex', '8');
    
    //enable kode saat edit request 07/06/2024
	/*
    $( document ).ready(function() {
        if(__update == 'update'){
            $('#NoAccount').attr('disabled',true);
        }
    });
    
	 */
    
    
JS
);

