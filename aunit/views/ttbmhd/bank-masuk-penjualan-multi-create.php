<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbmhd */
$params                          = '&id=' . $id . '&action=create';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel' . $params );
$this->title                     = 'Tambah - Bank Masuk Penjualan Multi';
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Bank Masuk Penjualan Multi', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="ttbmhd-create">
	<?= $this->render( 'bank-masuk-penjualan-multi-form', [
		'model'   => $model,
		'dsTUang' => $dsTUang,
		'id'      => $id,
		'url'     => [
			'update' => Custom::url( \Yii::$app->controller->id . '/bank-masuk-penjualan-multi-update' . $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
			'cancel' => $cancel,
			'detail' => Url::toRoute( [ 'ttbmitpm/index', 'action' => 'create' ] ),
		]
	] ) ?>
</div>
