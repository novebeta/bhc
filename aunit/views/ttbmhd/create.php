<?php
use common\components\Custom;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbmhd */

$params                          = '&id=' . $id . '&action=create';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = "Tambah - $title";
//$this->params[ 'breadcrumbs' ][] = [ 'label' => $title, 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = $this->title;

?>
<div class="ttbmhd-create">
    <?= $this->render(isset($templateForm) ? $templateForm : '_form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'id'     => $id,
        'label'     => $label,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/' .$view.'-update'. $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail'       => Url::toRoute( [ 'ttbmit/index','action' => 'create' ] ),
        ]
    ]) ?>

</div>
