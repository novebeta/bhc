<?php
use aunit\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbmhd */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttbmhd-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_ttbmhd_id', 'action' => $url[ 'update' ], 'options' => ['autocomplete'=>'off', 'method' => 'post', 'fieldConfig' => [ 'template' => "{input}" ] ] ] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-1", 'items' => ":LabelBMNo" ],
						  [ 'class' => "col-sm-4", 'items' => ":BMNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelBMTgl" ],
						  [ 'class' => "col-sm-3", 'items' => ":BMTgl" ],
						  [ 'class' => "col-sm-2", 'items' => ":BMJam" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ( $dsTUang[ 'BMJenis' ] == 'Scheme' || $dsTUang[ 'BMJenis' ] == 'Leasing' ) ? ":LabelLease" : "" ],
						  [ 'class' => "col-sm-9", 'items' => ( $dsTUang[ 'BMJenis' ] == 'Scheme' || $dsTUang[ 'BMJenis' ] == 'Leasing' ) ? ":Lease" : "", 'style' => 'padding-left:5px', ],
					  ],
					],
					[ 'class' => "col-md-24", 'style' => 'padding-top:5px',
					  'items' => [
						  [ 'class' => "col-md-15", 'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>' ],
					  ],
					],
					[ 'class' => "form-group col-md-24" ],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelBMMemo" ],
						  [ 'class' => "col-sm-10", 'items' => ":BMMemo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelBMTotal" ],
						  [ 'class' => "col-sm-3", 'items' => ":BMTotal" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelNominalBM" ],
						  [ 'class' => "col-sm-3", 'items' => ":NominalBM" ]
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelNoAccount" ],
						  [ 'class' => "col-sm-10", 'items' => ":NoAccount" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelBMPerson" ],
						  [ 'class' => "col-sm-9", 'items' => ":BMPerson" ]
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelBMAC" ],
						  [ 'class' => "col-sm-4", 'items' => ":BMAC" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelBMCekNo" ],
						  [ 'class' => "col-sm-4", 'items' => ":BMCekNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelBMCekTempo" ],
						  [ 'class' => "col-sm-3", 'items' => ":BMCekTempo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelJenisBM" ],
						  [ 'class' => "col-sm-4", 'items' => ":JenisBM" ]
					  ],
					],
				]
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
                  ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                      'items'  => [
                          ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":NoGL"],
                          ['class' => "col-sm-4 pull-right", 'items' => ":LabelNoGL"],
                      ],
                  ],
                  [
                      'class' => "col-md-12 pull-left",
                      'items' => $this->render( '../_nav', [
                          'url'=> $_GET['r'],
                          'options'=> [
                              'BMNo' => 'No Bank Masuk',
                              'BMPerson' => 'Terima Dari',
                              'BMJenis' => 'Jenis',
                              'LeaseKode' => 'Kode Lease',
                              'NoAccount' => 'No Account',
                              'BMCekNo' => 'Nomor Cek',
                              'BMAC' => 'Nomor AC',
                              'BMMemo' => 'Keterangan',
                              'UserID' => 'User ID',
                              'NoGL' => 'No GL',
                          ],
                      ])
                  ],
                  ['class' => "col-md-12 pull-right",
				    'items' => [
					    [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
				    ]
				  ]
			  ]
			],
			[
				/* label */
				":LabelNoGL"       => \common\components\General::labelGL( $dsTUang[ 'NoGL' ] ),
				":LabelBMNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No BM</label>',
				":LabelBMTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl BM</label>',
				":LabelLease"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Leasing</label>',
				":LabelBMMemo"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelBMTotal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total</label>',
				":LabelNominalBM"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
				":LabelNoAccount"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bank</label>',
				":LabelBMPerson"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Terima Dari</label>',
				":LabelBMAC"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No A/C</label>',
				":LabelBMCekNo"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Cek</label>',
				":LabelBMCekTempo" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Cair</label>',
				":LabelJenisBM"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				/* component */
				":BMNo"            => Html::textInput( 'BMNoView', $dsTUang[ 'BMNoView' ], [ 'class' => 'form-control', 'readonly' => true, 'tabindex' => '1' ] ) .
				                      Html::textInput( 'BMNo', $dsTUang[ 'BMNo' ], [ 'class' => 'hidden' ] ).
				                      Html::textInput( 'StatPrint', 1, [ 'class' => 'hidden' ] ),
				":BMTgl"           => FormField::dateInput( [ 'name' => 'BMTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BMTgl' ], ) ], 'readonly' => true ] ),
				":BMCekTempo"      => FormField::dateInput( [ 'name' => 'BMCekTempo', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BMCekTempo' ] ) ] ] ),
				":NoGL"            => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":BMJam"           => FormField::timeInput( [ 'name' => 'BMJam', 'config' => ['value' => $dsTUang['BMJam']] ] ),
				":BMMemo"          => Html::textInput( 'BMMemo', $dsTUang[ 'BMMemo' ], [ 'class' => 'form-control', 'tabindex' => '3' ] ),
				":NominalBM"       => FormField::numberInput( [ 'name' => 'BMNominal', 'config' => [ 'value' => $dsTUang[ 'BMNominal' ] ] ] ),
				":BMPerson"        => Html::textInput( 'BMPerson', $dsTUang[ 'BMPerson' ], [ 'class' => 'form-control', 'tabindex' => '7' ] ),
				":BMAC"            => Html::textInput( 'BMAC', $dsTUang[ 'BMAC' ], [ 'class' => 'form-control', 'tabindex' => '8' ] ),
				":BMCekNo"         => Html::textInput( 'BMCekNo', $dsTUang[ 'BMCekNo' ], [ 'class' => 'form-control', 'tabindex' => '9' ] ),
				":JenisBM"         => Html::textInput( 'BMJenis', $dsTUang[ 'BMJenis' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NoAccount"       => FormField::combo( 'NoAccount', [ 'config' => [ 'id' => 'NoAccount','value' => $dsTUang[ 'NoAccount' ], 'data' => \aunit\models\Traccount::find()->bank() ], 'options' => ['tabindex' => '6'] ] ),
				":BMTotal"         => FormField::numberInput( [ 'name' => 'BMTotal', 'config' => [ 'value' => $dsTUang[ 'BMTotal' ] ] ] ),
				":Lease"           => FormField::combo( 'LeaseKode', [ 'config' => [ 'id' => 'LeaseKode', 'value' => $dsTUang['LeaseKode'] ], 'extraOptions' => [ 'altLabel' => [ 'LeaseNama' ] ] ] ),
				/* button */
                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),
            ],
            [
                'url_main'         => 'ttbmhd',
                'url_delete'        => Url::toRoute(['ttbmhd/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlDetail = $url[ 'detail' ];
$urlPrint  = $url[ 'print' ];
$readOnly  = ( ($_GET[ 'action' ] == 'view' || $_GET[ 'action' ] == 'display') ? 'true' : 'false' );
$urlLoop   = Url::toRoute( [ 'ttbmit/index' ] );
$urlAdd    = \aunit\models\Ttbmhd::getRoute( $dsTUang[ 'BMJenis' ], [ 'action' => 'create' ] )[ 'create' ];
$urlIndex  = \aunit\models\Ttbmhd::getRoute( $dsTUang[ 'BMJenis' ], [ 'id' => $id ] )[ 'index' ];
switch ( $dsTUang[ 'BMJenis' ] ) {
	case 'Inden' :
		$urlDK  = Url::toRoute( [ 'ttdk/select-bm-order-leasing', 'jenis' => 'Inden', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ) ] );
		$BMLink = 'DKNo';
		break;
	case 'Scheme' :
		$urlDK  = Url::toRoute( [ 'ttdk/select-bm-order-leasing', 'jenis' => 'Scheme', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ) ] );
		$BMLink = 'DKNo';
		break;
	case 'Leasing' :
		$urlDK  = Url::toRoute( [ 'ttdk/select-bm-order-leasing', 'jenis' => 'Leasing', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ) ] );
		$BMLink = 'DKNo';
		break;
	case 'Piutang Sisa' :
		$urlDK  = Url::toRoute( [ 'ttdk/select-bm-order-leasing', 'jenis' => 'PiutangSisa', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ) ] );
		$BMLink = 'DKNo';
		break;
	case 'Tunai' :
		$urlDK  = Url::toRoute( [ 'ttdk/select-bm-order-leasing', 'jenis' => 'Tunai', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ) ] );
		$BMLink = 'DKNo';
		break;
	case 'Kredit' :
		$urlDK  = Url::toRoute( [ 'ttdk/select-bm-order-leasing', 'jenis' => 'Kredit', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ) ] );
		$BMLink = 'DKNo';
		break;
	case 'Piutang UM' :
		$urlDK  = Url::toRoute( [ 'ttdk/select-bm-order-leasing', 'jenis' => 'PiutangUM', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ) ] );
		$BMLink = 'DKNo';
		break;
	case 'BBN Plus' :
		$urlDK  = Url::toRoute( [ 'ttdk/select-bm-order-leasing', 'jenis' => 'BBNPlus', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ) ] );
		$BMLink = 'DKNo';
		break;
	case 'BM Dari Kas' :
		$urlDK  = Url::toRoute( [ 'ttkk/select-bm-dari-kas', 'jenis' => 'BMDariKas', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ) ] );
		$BMLink = 'KKNo';
		break;
}
$this->registerJsVar( 'urlDK', $urlDK );
$this->registerJsVar( 'jenis', $dsTUang[ 'BMJenis' ] );
$url[ 'jurnal' ] = \yii\helpers\Url::toRoute(['ttbmhd/jurnal']);
$this->registerJsVar('__update', $_GET['action']);
$this->registerJs( <<< JS


    var columnModel = [
            { template: 'actions'},
            {
                name: 'DKNo',
                label: 'No Dat Kon',
                editable: true,
                width: 80,
                editoptions:{
                    readOnly: true    
                },
            },
            {
                name: 'DKTgl',
                label: 'Tanggal DK',
                width: 80,
                editable: true,
                editoptions:{
                    readOnly: true    
                },
            },
            {
                name: 'CusNama',
                label: 'Nama Konsumen',
                width: 185,
                editable: true,
                editoptions:{
                    readOnly: true    
                },
            },
            {
                name: 'MotorType',
                label: 'Type',
                width: 150,
                editable: true,
                editoptions:{
                    readOnly: true    
                },
            },
            {
                name: 'DKNetto',
                label: '$label',
                width: 150,
                editable: true,
                template: 'money',
                editoptions:{
                    readOnly: true    
                },
                
            },
            {
                name: 'Terbayar',
                label: 'Terbayar',
                width: 95,
                editable: true,
                template: 'money',
                editoptions:{
                    readOnly: true    
                },
            },
            {
                name: 'Bayar',
                label: 'Bayar',
                width: 95,
                editable: true,
                template: 'money',
                editrules:{
                    custom:true, 
                    custom_func: function(value) {
                        var Bayar = parseFloat(window.util.number.getValueOfEuropeanNumber(value));
                        var DKNetto = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'DKNetto'+'"]').val()));
                        var Terbayar = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Terbayar'+'"]').val()));
                        if (Bayar === 0) 
                            return [false,"Silahkan mengisi bayar."];  
                        else {              
                            var sisa = DKNetto - Terbayar;
                            if (Bayar > sisa){
                                $('[id="'+window.cmp.rowid+'_'+'Bayar'+'"]').val(sisa);
                               return [false,"Nilai Bayar maksimal : " + window.util.number.format(sisa)];   
                            }else{
                                return [true,""];  
                            }
                        }
                    }
                }
            },                       
            {
                name: 'Sisa',
                label: 'Sisa',
                width: 95,
                editable: true,
                template: 'money',
                editoptions:{
                    readOnly: true    
                },
            },
        ];
    if (jenis === 'BM Dari Kas'){
        columnModel = [
            { template: 'actions'},
            {
                name: 'DKNo',
                label: 'No KK',
                editable: true,
                width: 80,
            },
            {
                name: 'KKTgl',
                label: 'Tanggal KK',
                width: 80,
                editable: true,
            },
            {
                name: 'KKPerson',
                label: 'Nama',
                width: 185,
                editable: true,
            },
            {
                name: 'KKMemo',
                label: 'Keterangan',
                width: 150,
                editable: true,
            },
            {
                name: 'KKNominal',
                label: 'Nominal',
                width: 150,
                editable: true,
                template: 'money'
                
            },
        ];
    }

     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 220,
        extraParams: {
            BMNo: '$model->BMNo'
        },
        postData: {
            BMNo: '$model->BMNo'
        },
        rownumbers: true,        
        loadonce:true,
        rowNum: 1000,  
        readOnly: $readOnly, 
        navButtonTambah: false, 
        colModel: columnModel,     
        listeners: {
            afterLoad: function(grid, response) {
                let colSum = 'Bayar';
                if(jenis === 'BM Dari Kas'){
                    colSum = 'KKNominal';
                }
                var BMTotal = $('#detailGrid').jqGrid('getCol',colSum,false,'sum');
                $('#BMTotal-disp').utilNumberControl().val(BMTotal);
            },
        }  
        
     }).init().fit($('.box-body')).load({url: '$urlDetail'}); 

    $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
        window.cmp.rowid = rowid;    
    });
    
    $('#detailGrid').jqGrid('navButtonAdd',"#detailGrid-pager",{ 
        caption: ' Tambah',
        buttonicon: 'glyphicon glyphicon-plus',
        onClickButton:  function() {
            window.util.app.dialogListData.closeTriggerFromIframe = function(){ 
                var iframe = window.util.app.dialogListData.iframe();
                var selRowId = iframe.contentWindow.util.app.dialogListData.gridFn.getSelectedRow(false);
            //    console.log(selRowId);
               if(selRowId === false) return false;
               var len = selRowId.length;
               while (len--) {
                if(parseFloat(selRowId[len].data.BMBayar) == 0){
                    bootbox.alert({message:'Silahkan mengisi kolom bayar '+selRowId[len].data.DKNo, size: 'small'});
                    return false;
                }
               }
               selRowId = iframe.contentWindow.util.app.dialogListData.gridFn.getSelectedRow(true);
                $.ajax({
                    type: "POST",
                    url: '$urlLoop',
                    data: {
                        oper: 'batch',
                        data: selRowId,
                        header: btoa($('#form_ttbmhd_id').serialize())
                    },
                    success: function(data) {
                        iframe.contentWindow.util.app.dialogListData.gridFn.mainGrid.jqGrid().trigger('reloadGrid');
                    },
                });
                return false;
            }
            if (jenis === 'Scheme' || jenis === 'Leasing'){
                urlDK = urlDK + '&LeaseKode='+$('[name="LeaseKode"]').val();
            }
            window.util.app.dialogListData.show({
                title: 'Daftar Data Konsumen untuk  Nomor : {$dsTUang["BMNoView"]}',
                url: urlDK
            },function(data){
                console.log(data);
                 window.location.href = "{$url['update']}&oper=skip-load&BMNoView={$dsTUang['BMNoView']}"; 
            });
        }, 
        position: "last", 
        title:"", 
        cursor: "pointer"
    });
    function submitPrint(StatPrint){
        $('[name=StatPrint]').val(StatPrint);
        $('#form_ttbmhd_id').utilForm().submit();
    }
    $('#btnPrint').click(async function (event) {	      
        $('#form_ttbmhd_id').attr('action','$urlPrint');
        $('#form_ttbmhd_id').attr('target','_blank');
        if($.inArray(jenis,['Inden','Piutang UM','Piutang Sisa','Kredit','Tunai']) != -1){
			for (let i = 1; i < 3; i++) {
				submitPrint(i);
				await sleep(500);
			}
		}else{
			submitPrint(1);
		}
	  }); 
        
    $('#btnJurnal').click(function (event) {	
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#form_ttbmhd_id').attr('action','{$url[ 'jurnal' ]}');
                    $('#form_ttbmhd_id').utilForm().submit();
                }
            }
        });
    }); 
        
     $('#btnCancel').click(function (event) {	      
       $('#form_ttbmhd_id').attr('action','{$url['cancel']}');
       $('#form_ttbmhd_id').utilForm().submit();
    });
     
      $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });    
    
      $('#btnSave').click(function (event) {
           let Nominal = parseFloat($('#BMNominal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Silahkan mengisi nominal', size: 'small'});
               return;
           }
        $('#form_ttbmhd_id').attr('action','{$url['update']}');
        $('#form_ttbmhd_id').utilForm().submit('{$url['update']}','POST','_self');
    }); 
      
      $('#BMTotal-disp').blur(function() {
        $('#BMNominal').utilNumberControl().val($('#BMTotal').utilNumberControl().val());
      });
      
      $('[name=BMTgl]').utilDateControl().cmp().attr('tabindex', '2');
      $('[name=BMCekTempo]').utilDateControl().cmp().attr('tabindex', '10');
      $('[name=BMTotal').utilNumberControl().cmp().attr('tabindex', '4');
      $('[name=BMNominal').utilNumberControl().cmp().attr('tabindex', '5');
      //enable kode saat edit request 07/06/2024
	/*
      $( document ).ready(function() {
        if(__update == 'update'){
            $('#NoAccount').attr('disabled',true);
        }
    });*/
      
             
JS
);


