<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel aunit\models\TdmotortypeSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use aunit\assets\AppAsset;

AppAsset::register($this);
$this->title = 'Motor';
$this->params['breadcrumbs'][] = $this->title;
$arr = [
    'cmbTxt' => [
        'MotorType' => 'Type Motor',
        'MotorWarna' => 'WarnaMotor',
        'MotorNama' => 'Nama Motor',
        'MotorKategori' => 'Kategori Motor',
        'MotorNoMesin' => 'No Mesin',
        'MotorNoRangka' => 'No Kendaraan',
    ],
    'cmbTgl' => [
    ],
    'cmbNum' => [
    ],
    'sortname'  => "TypeStatus asc,MotorType",
    'sortorder' => 'asc'
];
$this->registerJsVar('setcmb', $arr);
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render('../_search'); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs(\aunit\components\TdUi::mainGridJs(\aunit\models\Tdmotortype::className()), \yii\web\View::POS_READY);
