<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tdmotortype */
$this->title                   = str_replace('Menu','',\aunit\components\TUi::$actionMode).' Type Motor: ' . $model->MotorType;
$this->params['breadcrumbs'][] = [ 'label' => 'Type Motor', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->MotorType, 'url' => [ 'view', 'id' => $model->MotorType ] ];
$this->params['breadcrumbs'][] = 'Edit';
$params                          = '&id=' . $id . '&action=save';
?>
<div class="tdmotortype-update">
	<?= $this->render( '_form', [
		'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdmotortype/update', 'id' => $id,'action' => 'update' ] ),
            'cancel' => Url::toRoute( [ 'tdmotortype/cancel', 'id' => $id ,'action' => 'create'] )
        ]
//        'url' => [
//            'update'    => Custom::url(\Yii::$app->controller->id.'/create' ),
//            'save'    => Custom::url(\Yii::$app->controller->id.'/update'.$params ),
//            'cancel'    => Custom::url(\Yii::$app->controller->id.'/' ),
//        ]
	] ) ?>
</div>
