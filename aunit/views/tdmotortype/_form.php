<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\components\Custom;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Tdmotortype */
/* @var $form yii\widgets\ActiveForm */

//$url['cancel'] = Url::toRoute('tdmotorwarna/index');
?>

<div class="tdmotortype-form">
    <?php

    $form = ActiveForm::begin(['id' => 'frm_tdmotortype_id', 'action' => $url['update'], 'fieldConfig' => ['template' => "{input}"]]);
    \aunit\components\TUi::form(
        [
            'class' => "row-no-gutters",
            'items' => [
                ['class' => "form-group col-md-24", 'style' => "",
                    'items' => [
                        ['class' => "col-sm-2", 'items' => ":LabelMotorType"],
                        ['class' => "col-sm-9", 'items' => ":MotorType"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2", 'items' => ":LabelMotorKategori"],
                        ['class' => "col-sm-3",'items' => ":MotorKategori"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelMotorHrgJual"],
                        ['class' => "col-sm-4",'items' => ":MotorHrgJual"],
                    ],
                ],
                ['class' => "form-group col-md-24",'style' => "",
                    'items' => [
                        ['class' => "col-sm-2",'items' =>":LabelMotorNama"],
                        ['class' => "col-sm-9",'items' => ":MotorNama"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelMotorCC"],
                        ['class' => "col-sm-3",'items' => ":MotorCC"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelMotorHrgBeli"],
                        ['class' => "col-sm-4",'items' => ":MotorHrgBeli"],
                    ],
                ],
                ['class' => "form-group col-md-24",'style' => "",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelMotorNoMesin"],
                        ['class' => "col-sm-3",'items' => ":MotorNoMesin"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelMotorNoRangka"],
                        ['class' => "col-sm-3",'items' => ":MotorNoRangka"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2", 'items' => ":LabelKodeAHM"],
                        ['class' => "col-sm-3",'items' => ":KodeAHM"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelTypeStatus"],
                        ['class' => "col-sm-4",'items' => ":TypeStatus"],
                    ],
                ],
            ]
        ],
        ['class' => "row-no-gutters",
            'items'  => [
                [
                    'class' => "col-md-12 pull-left",
                    'items' => $this->render( '../_nav', [
                        'url'=> $_GET['r'],
                        'options'=> [
                            'tdmotortype.MotorType' => 'Type Motor',
                            'tdmotortype.MotorWarna' => 'WarnaMotor',
                            'tdmotortype.MotorNama' => 'Nama Motor',
                            'tdmotortype.MotorKategori' => 'Kategori Motor',
                            'tdmotortype.MotorNoMesin' => 'No Mesin',
                            'tdmotortype.MotorNoRangka' => 'No Kendaraan',
                        ],
                    ])
                ],
                ['class' => "col-md-12 pull-right",
                    'items'  => [
                        ['class' => "pull-right", 'items' => ":btnAction"],
                    ],
                ],
            ],
        ],

        [
            /* label */
            ":LabelKodeAHM"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode AHM</label>',
            ":LabelMotorType"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Type</label>',
            ":LabelMotorKategori"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kategori</label>',
            ":LabelMotorNama"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Motor</label>',
            ":LabelMotorCC"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Motor CC</label>',
            ":LabelMotorNoMesin"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Mesin</label>',
            ":LabelMotorNoRangka"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Rangka</label>',
            ":LabelMotorHrgJual"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Harga Jual</label>',
            ":LabelMotorHrgBeli"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Harga Beli</label>',
            ":LabelTypeStatus"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',

            /* component */
            ":KodeAHM"          => $form->field($model, 'KodeAHM', ['template' => '{input}'])->textInput(['maxlength' => '50','autofocus' => 'autofocus']),
            ":MotorType"        => $form->field($model, 'MotorType', ['template' => '{input}'])->textInput(['maxlength' => true, 'style'=>"text-transform:uppercase"]),
            ":MotorKategori"    => $form->field($model, 'MotorKategori', ['template' => '{input}'])
                                    ->dropDownList([
                                        'Cub Low' => 'Cub Low',
                                        'Cub Mid' => 'Cub Mid',
                                        'Cub High' => 'Cub High',
                                        'AT Low' => 'AT Low',
                                        'AT Mid' => 'AT Mid',
                                        'AT High' => 'AT High',
                                        'Sport Low' => 'Sport Low',
                                        'Sport Mid' => 'Sport Mid',
                                        'Sport High' => 'Sport High',
                                    ],['maxlength' => true]),
            ":MotorNama"        => $form->field($model, 'MotorNama', ['template' => '{input}'])->textInput(['maxlength' => true]),
            ":MotorCC"          => $form->field($model, 'MotorCC', ['template' => '{input}'])->textInput(['maxlength' => true,'placeholder'=>'0', 'style' => 'text-align:right']),
            ":MotorNoMesin"     => $form->field($model, 'MotorNoMesin', ['template' => '{input}'])->textInput(['maxlength' => true]),
            ":MotorNoRangka"    => $form->field($model, 'MotorNoRangka', ['template' => '{input}'])->textInput(['maxlength' => true]),
            ":MotorHrgJual"     => $form->field($model, 'MotorHrgJual', ['template' => '{input}'])
                                    ->widget(\kartik\number\NumberControl::className(), [
                                        'value' => 0,
                                        'maskedInputOptions' => ['groupSeparator' => '.', 'radixPoint' => ','],
                                        'displayOptions' => ['class' => 'form-control', 'placeholder' => '0',]]),
            ":MotorHrgBeli"     => $form->field($model, 'MotorHrgBeli', ['template' => '{input}'])
                                    ->widget(\kartik\number\NumberControl::className(), [
                                        'value' => 0,
                                        'maskedInputOptions' => ['groupSeparator' => '.', 'radixPoint' => ',',],
                                        'displayOptions' => ['class' => 'form-control', 'placeholder' => '0',]]),
            ":TypeStatus"       => $form->field($model, 'TypeStatus', ['template' => '{input}'])
                                    ->dropDownList(
                                        ['A' => 'AKTIF', 'N' => 'NON AKTIF'],
                                        ['maxlength' => true,]),

            /* button */
            ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
            ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
            ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
            ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
            ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),


        ],
        [
            '_akses' => 'Type Warna Motor',
            'url_main' => 'tdmotortype',
            'url_id' => $_GET['id'] ?? '',
        ]
    );
    ActiveForm::end();

    $urlAdd   = Url::toRoute( [ 'tdmotortype/create','id'=>'new','action'=>'create' ] );
    $urlIndex   = Url::toRoute( [ 'tdmotorwarna/index' ] );
    $this->registerJs( <<< JS
     
    $('#btnAdd').click(function (event) {	      
        window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });  

     $('#btnCancel').click(function (event) {	  
         window.location.href = '{$url['cancel']}';
    });

     $('#btnSave').click(function (event) {	      
       // $('#frm_tdmotortype_id').attr('action','{$url['update']}');
       $('#frm_tdmotortype_id').submit();
    });
     
     
     
JS);

    ?>
</div>