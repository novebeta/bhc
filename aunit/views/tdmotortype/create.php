<?php

use yii\helpers\Html;
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tdmotortype */

$this->title = 'Tambah Type Motor';
$this->params['breadcrumbs'][] = ['label' => 'Type Motor', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdmotortype-create">
    <?= $this->render('_form', [
        'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdmotortype/create', 'id' => $id,'action' => 'create'] ),
            'cancel' => Url::toRoute( [ 'tdmotortype/cancel', 'id' => $id ,'action' => 'create'] )
        ]

//        'url' => [
//            'update'    => Custom::url(\Yii::$app->controller->id.'/create' ),
//            'save'    => Custom::url(\Yii::$app->controller->id.'/create' ),
//            'cancel'    => Custom::url(\Yii::$app->controller->id.'/' ),
//        ]
    ]) ?>

</div>
