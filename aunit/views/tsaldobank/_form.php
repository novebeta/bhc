<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model aunit\models\Tsaldobank */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tsaldobank-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'SaldoBankTgl')->textInput() ?>

    <?= $form->field($model, 'NoAccount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SaldoBankAwal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SaldoBankMasuk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SaldoBankKeluar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SaldoBankSaldo')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
