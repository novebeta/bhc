<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Tsaldobank */

$this->title = 'Create Tsaldobank';
$this->params['breadcrumbs'][] = ['label' => 'Tsaldobanks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tsaldobank-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
