<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Tsaldobank */

$this->title = str_replace('Menu','',\aunit\components\TUi::$actionMode).'Tsaldobank: ' . $model->SaldoBankTgl;
$this->params['breadcrumbs'][] = ['label' => 'Tsaldobanks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->SaldoBankTgl, 'url' => ['view', 'SaldoBankTgl' => $model->SaldoBankTgl, 'NoAccount' => $model->NoAccount]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="tsaldobank-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
