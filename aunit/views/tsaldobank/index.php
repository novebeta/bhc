<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel aunit\models\TsaldobankSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bank Saldo';
$this->params['breadcrumbs'][] = $this->title;
$arr = [
	'cmbTxt'    => [
//		'SSNo'       => 'No SS',
//		'DONo' => 'No DO',
	],
	'cmbTgl'    => [
//		'SSTgl' => 'Tgl SS'
	],
	'cmbNum'    => [
	],
	'sortname'  => "SaldoBankTgl",
	'sortorder' => 'desc',
];
use aunit\assets\AppAsset;
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Tsaldobank::className(),
	'Bank Saldo' ), \yii\web\View::POS_READY );
