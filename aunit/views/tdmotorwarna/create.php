<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tdmotorwarna */
$this->title                   = 'Tambah Warna Motor';
$this->params['breadcrumbs'][] = [ 'label' => 'Warna Motor', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdmotorwarna-create">
	<?= $this->render( '_form', [
		'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdmotorwarna/update', 'id' => $id ] ),
            'cancel' => Url::toRoute( [ 'tdmotorwarna/cancel', 'id' => $id ,'action' => 'create'] )
        ]
//        'url' => [
//            'update'    => Custom::url(\Yii::$app->controller->id.'/create' ),
//            'save'    => Custom::url(\Yii::$app->controller->id.'/create' ),
//            'cancel'    => Custom::url(\Yii::$app->controller->id.'/' ),
//        ]
	] ) ?>
</div>
