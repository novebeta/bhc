<?php
use aunit\models\Tdmotortype;
use aunit\assets\AppAsset;
use aunit\components\Menu;
use aunit\models\Tdmotorwarna;
use common\components\Custom;
use kartik\datecontrol\DateControl;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
AppAsset::register( $this );
$this->title                   = 'Type Warna Motor';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
        'MotorType' => 'Type Motor',
        'MotorWarna' => 'WarnaMotor',
        'MotorNama' => 'Nama Motor',
        'MotorKategori' => 'Kategori Motor',
        'MotorNoMesin' => 'No Mesin',
        'MotorNoRangka' => 'No Kendaraan',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
//	'sortname'  => "TglUpdate",
//	'sortorder' => 'DESC'
];
$this->registerJsVar( 'setcmb', $arr );
$arr                           = [
	'cmbTxt' => [
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
//	'sortname'  => "MotorWarna",
//	'sortorder' => 'ASC'
];
$this->registerJsVar( 'setcmb_warna', $arr );
?>
    <div class="panel panel-default" >
        <div class="panel-body no-padding">
            <div class="nav-tabs-custom">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#type" data-toggle="tab">Type</a></li>
                    <li><a href="#warna" data-toggle="tab">Warna</a></li>
                </ul>
                <div class="tab-content">
                    <!-- Morris chart - Sales -->
                    <div class="chart tab-pane active" id="type">
                        <div id="findModal" class="modal modal-danger fade" tabindex="-1" role="dialog">
                            <div class="modal-dialog modal-sm" role="document">
                                <div class="modal-content">
                                    <div class="form-inline" id="top-menu">
                                        <div class="input-group">
                                            <input id="txt-find-id" type="search" class="form-control" style="width: 235px"
                                                   aria-describedby="basic-addon1">
                                            <span class="input-group-btn">
                                                <button id="find-up" class="btn btn-default" type="button">
                                                    <span class="fa fa-angle-up" aria-hidden="true"></span>
                                                </button>
                                                <button id="find-down" class="btn btn-default" type="button">
                                                    <span class="fa fa-angle-down" aria-hidden="true"></span>
                                                </button>
                                                <button class="btn btn-default" type="button" data-dismiss="modal">
                                                    <span class="fa fa-close" aria-hidden="true"></span>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div>
                        <form id="filter-bottom-form">
                            <div class="form-inline" style="margin-bottom: 3px" id="top-menu">
                                <select id="filter-text-cmb1" class="form-control cmbTxt chg">
                                </select>
                                <input type="text" id="filter-text-val1" class="form-control blur" aria-label="Search">
                                <input type="text" readonly class="form-control" value="Dan" style="width: 50px">
                                <select id="filter-text-cmb2" class="form-control cmbTxt chg">
                                </select>
                                <input type="text" id="filter-text-val2" class="form-control blur" aria-label="Search">
                                <div class="material-switch form-control">
                                    <input id="checked-filter" type="checkbox" class="chg" checked/>
                                    <label for="checked-filter" class="label-success"></label>
                                    <span id="span-checked-filter" style="margin-left: 5px">Filter #2 Aktif</span>
                                </div>
                                <button id="lbl-find-id" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                            <div class="form-inline inline-filter2" style="margin-bottom: 3px">
                                <select id="filter-tgl-cmb1" class="form-control filter2 chg">
                                    <option value="">Tanggal</option>
                                </select>
								<? try {
									echo DateControl::widget( [
										'name'          => 'tgl1',
										'type'          => DateControl::FORMAT_DATE,
										'value'         => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ),
										'options'       => [
											'id'    => 'filter-tgl-val1',
											'class' => 'filter2 dt',
										],
										'pluginOptions' => [
											'autoclose' => true,
										],
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                                -
								<? try {
									echo DateControl::widget( [
										'name'          => 'tgl2',
										'type'          => DateControl::FORMAT_DATE,
										'value'         => Yii::$app->formatter->asDate( 'now', 'yyyy-MM-dd' ),
										'options'       => [
											'id'    => 'filter-tgl-val2',
											'class' => 'filter2 dt',
										],
										'pluginOptions' => [
											'autoclose' => true,
										],
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                                <select id="filter-num-cmb1" class="form-control filter2 chg">
                                    <option value="" selected>Jumlah</option>
                                </select>
                                <select id="filter-num-cmb2" class="form-control filter2 chg" style="width: 60px;">
                                    <option value=">">&gt;</option>
                                    <option value="=">&gt;=</option>
                                    <option value="<">&lt;</option>
                                    <option value="<=">&lt;=</option>
                                    <option value="=">=</option>
                                    <option value="<>">&lt;&gt;</option>
                                </select>
                                <input id="filter-num-val" type="number" class="form-control blur" placeholder="Nominal" value="0">
                                <button id="btn-find-id" class="btn btn-default" type="button">
                                    <span class="fa fa-binoculars"></span>
                                </button>
                            </div>
                        </form>
                        <table id="jqGrid_type"></table>
                        <div id="jqGridPager_type"></div>
                    </div>
                    <div class="chart tab-pane" id="warna">
                        <div id="findModal_warna" class="modal modal-danger fade" tabindex="-1" role="dialog">
                            <div class="modal-dialog modal-sm" role="document">
                                <div class="modal-content">
                                    <div class="form-inline" id="top-menu_warna">
                                        <div class="input-group">
                                            <input id="txt-find-id_warna" type="search" class="form-control" style="width: 235px"
                                                   aria-describedby="basic-addon1">
                                            <span class="input-group-btn">
                                                <button id="find-up_warna" class="btn btn-default" type="button">
                                                    <span class="fa fa-angle-up" aria-hidden="true"></span>
                                                </button>
                                                <button id="find-down_warna" class="btn btn-default" type="button">
                                                    <span class="fa fa-angle-down" aria-hidden="true"></span>
                                                </button>
                                                <button class="btn btn-default" type="button" data-dismiss="modal">
                                                    <span class="fa fa-close" aria-hidden="true"></span>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div>
                        <form id="filter-bottom-form_warna">
                            <div class="form-inline" style="margin-bottom: 3px" id="top-menu_warna">
                                <select id="filter-text-cmb1_warna" class="form-control cmbTxt chg_warna">
                                </select>
                                <input type="text" id="filter-text-val1_warna" class="form-control blur_warna" aria-label="Search">
                                <input type="text" readonly class="form-control" value="Dan" style="width: 50px">
                                <select id="filter-text-cmb2_warna" class="form-control cmbTxt chg_warna">
                                </select>
                                <input type="text" id="filter-text-val2_warna" class="form-control blur_warna" aria-label="Search">
                                <div class="material-switch form-control">
                                    <input id="checked-filter_warna" type="checkbox" class="chg_warna" checked/>
                                    <label for="checked-filter" class="label-success"></label>
                                    <span id="span-checked-filter_warna" style="margin-left: 5px">Filter #2 Aktif</span>
                                </div>
                                <button id="lbl-find-id_warna" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                            <div class="form-inline inline-filter2" style="margin-bottom: 3px">
                                <select id="filter-tgl-cmb1_warna" class="form-control filter2 chg_warna">
                                    <option value="">Tanggal</option>
                                </select>
			                    <? try {
				                    echo DateControl::widget( [
					                    'name'          => 'tgl1',
					                    'type'          => DateControl::FORMAT_DATE,
					                    'value'         => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ),
					                    'options'       => [
						                    'id'    => 'filter-tgl-val1_warna',
						                    'class' => 'filter2 dt',
					                    ],
					                    'pluginOptions' => [
						                    'autoclose' => true,
					                    ],
				                    ] );
			                    } catch ( \yii\base\InvalidConfigException $e ) {
			                    } ?>
                                -
			                    <? try {
				                    echo DateControl::widget( [
					                    'name'          => 'tgl2',
					                    'type'          => DateControl::FORMAT_DATE,
					                    'value'         => Yii::$app->formatter->asDate( 'now', 'yyyy-MM-dd' ),
					                    'options'       => [
						                    'id'    => 'filter-tgl-val2_warna',
						                    'class' => 'filter2 dt',
					                    ],
					                    'pluginOptions' => [
						                    'autoclose' => true,
					                    ],
				                    ] );
			                    } catch ( \yii\base\InvalidConfigException $e ) {
			                    } ?>
                                <select id="filter-num-cmb1_warna" class="form-control filter2 chg_warna">
                                    <option value="" selected>Jumlah</option>
                                </select>
                                <select id="filter-num-cmb2_warna" class="form-control filter2 chg_warna" style="width: 60px;">
                                    <option value=">">&gt;</option>
                                    <option value="=">&gt;=</option>
                                    <option value="<">&lt;</option>
                                    <option value="<=">&lt;=</option>
                                    <option value="=">=</option>
                                    <option value="<>">&lt;&gt;</option>
                                </select>
                                <input id="filter-num-val_warna" type="number" class="form-control blur" placeholder="Nominal" value="0">
                                <button id="btn-find-id_warna" class="btn btn-default" type="button">
                                    <span class="fa fa-binoculars"></span>
                                </button>
                            </div>
                        </form>
                        <table id="jqGrid_warna"></table>
                        <div id="jqGridPager_warna"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
$menuText       = $url_update = $url_delete = $url_view = $url_print = '';
$url            = Custom::url( 'tdmotortype/td' );
$url_add        = Custom::url( 'tdmotortype/create&action=create' );
$url_update     = Custom::url( 'tdmotortype/update&action=update' );
$url_view       = Custom::url( 'tdmotortype/update&action=view' );
$url_print      = Custom::url( 'tdmotortype/print' );
$url_delete     = $url;
$menuText       = 'Type  Warna Motor';
$button_add_type     = Menu::akses( $menuText, Menu::ADD ) ? "grid.navButtonAdd('#jqGridPager_type',{
		   caption:' Tambah', 
		   buttonicon:'glyphicon glyphicon-plus', 
		   onClickButton: function(){ 
		      window.location = '$url_add';
		   }, 
	        position:'last'
			})" : '';
$button_update  = Menu::akses( $menuText, Menu::UPDATE ) ?
	"<a href=\"$url_update&id='+id+'&action=update\" title=\"Edit\" aria-label=\"Update\" data-pjax=\"0\"><span class=\"glyphicon glyphicon-edit\"></span></a>" : "";
$button_delete  = Menu::akses( $menuText, Menu::DELETE ) ?
	"<a href=\"$url_delete\" rowId=\"'+id+'\" title=\"Hapus\" aria-label=\"Delete\" data-pjax=\"0\" data-confirm=\"Are you sure you want to delete this item?\" data-method=\"post\"><span class=\"glyphicon glyphicon-trash\"></span></a>" : "";
$button_view    = Menu::akses( $menuText, Menu::VIEW ) ?
	"<a href=\"$url_view&id='+id+'&action=view\" title=\"View\" aria-label=\"View\" data-pjax=\"0\"><span class=\"fa fa-eye\"></span></a>" : "";
$cols           = [];
$gridColsProses = '';
if ( $button_view != '' ||$button_update != '' || $button_delete != '' ) {
	$gridColsProses = " {
                 label: 'Proses', 
                 name: 'actions', 
                 sortable: false,
                 width: 75, 
                 align: 'center',
                 frozen:true,
                 formatter: function(cellvalue, options, rowObject) {
                     let id = btoa(options.rowId);
                   return '$button_view $button_update $button_delete';
                 }
             },";
}
foreach ( Tdmotortype::colGrid() as $filed => $label ) {
	$is_array = is_array( $label );
	$col      = [];
	if ( $is_array ) {
		$col = $label;
	} else {
		$col['name']  = $filed;
		$col['label'] = $label;
	}
	$cols[] = json_encode( $col );
}
$gridCols = implode( ', ', $cols );
$gridCols = preg_replace( '/[\'"](func\w+)[\'"]/', '$1', $gridCols );
$cols_type = $gridColsProses . $gridCols;
//$this->registerJsVar( 'cols', '[' . $gridColsProses . $gridCols . ']' );

$url            = Custom::url( 'tdmotorwarna/td' );
$url_add        = Custom::url( 'tdmotorwarna/create&action=create' );
$url_update     = Custom::url( 'tdmotorwarna/update&action=update' );
$url_view       = Custom::url( 'tdmotorwarna/update&action=view' );
$url_print      = Custom::url( 'tdmotorwarna/print' );
$url_delete     = $url;
//$menuText       = 'Type  Warna Motor';
$button_add_warna     = Menu::akses( $menuText, Menu::ADD ) ? "grid.navButtonAdd('#jqGridPager_warna',{
		   caption:' Tambah', 
		   buttonicon:'glyphicon glyphicon-plus', 
		   onClickButton: function(){ 
		      window.location = '$url_add';
		   }, 
	        position:'last'
			})" : '';
$button_update  = Menu::akses( $menuText, Menu::UPDATE ) ?
	"<a href=\"$url_update&id='+id+'&action=update\" title=\"Edit\" aria-label=\"Update\" data-pjax=\"0\"><span class=\"glyphicon glyphicon-edit\"></span></a>" : "";
$button_delete  = Menu::akses( $menuText, Menu::DELETE ) ?
	"<a href=\"$url_delete\" rowId=\"'+id+'\" title=\"Hapus\" aria-label=\"Delete\" data-pjax=\"0\" data-confirm=\"Are you sure you want to delete this item?\" data-method=\"post\"><span class=\"glyphicon glyphicon-trash\"></span></a>" : "";
$button_view    = Menu::akses( $menuText, Menu::VIEW ) ?
	"<a href=\"$url_view&id='+id+'&action=view\" title=\"View\" aria-label=\"View\" data-pjax=\"0\"><span class=\"fa fa-eye\"></span></a>" : "";
$cols           = [];
$gridColsProses = '';
if ( $button_view != '' || $button_update != '' || $button_delete != '' ) {
	$gridColsProses = " {
                 label: 'Proses', 
                 name: 'actions', 
                 sortable: false,
                 width: 75, 
                 align: 'center',
                 frozen:true,
                 formatter: function(cellvalue, options, rowObject) {
                     let id = btoa(options.rowId);
                   return '$button_view $button_update $button_delete';
                 }
             },";
}
foreach ( Tdmotorwarna::colGrid() as $filed => $label ) {
	$is_array = is_array( $label );
	$col      = [];
	if ( $is_array ) {
		$col = $label;
	} else {
		$col['name']  = $filed;
		$col['label'] = $label;
	}
	$cols[] = json_encode( $col );
}
$gridCols = implode( ', ', $cols );
$gridCols = preg_replace( '/[\'"](func\w+)[\'"]/', '$1', $gridCols );
//$this->registerJsVar( 'cols_warna', '[' . $gridColsProses . $gridCols . ']' );
$cols_warna = $gridColsProses . $gridCols;
$this->registerJs( $this->render( 'index.js',[
        'cols' => $cols_type,
        'btnAddType' => $button_add_type,
        'cols_warna' => $cols_warna,
        'btnAddWarna' => $button_add_warna,
]) );