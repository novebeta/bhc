jQuery(function ($) {
    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    }
    var ajaxRowOpt = {
        success: function (response, status, error) {
            $("#jqGrid_type").trigger("reloadGrid");
            $.notify(
                {
                    message: response.message
                },
                {
                    type: response.success ? 'success' : 'warning',
                    mouse_over: 'pause',
                    newest_on_top: true,
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    }
                }
            );
        },
        error: function (response, status, error) {
            $.notify(
                {
                    message: error + '<br> ' + response.responseJSON.message
                },
                {
                    type: 'danger',
                    mouse_over: 'pause',
                    newest_on_top: true,
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    }
                }
            );
        }
    };
    $.jgrid.defaults.styleUI = 'Bootstrap';
    $.jgrid.defaults.pgtext = "{0} of {1}";
    var url_string = $(location).attr('href');
    var url = new URL(url_string);
    var c = decodeURIComponent(url.searchParams.get("r"));
    var pageKey = c;
    var grid = $("#jqGrid_type");
    grid.jqGrid({
        url: 'index?r=tdmotortype/td',
        mtype: "POST",
        datatype: "json",
        jsonReader: {
            root: 'rows',
            page: 'currentPage',
            total: 'totalPages',
            records: 'rowsCount',
            id: 'rowId',
            repeatitems: false
        },
        rownumbers: true,
        colModel: [
            <?=$cols?>
        ],
        multiSort: true,
        sortname: setcmb.sortname,
        sortorder: setcmb.sortorder,
        search: true,
        postData: {
            query: function () {
                const search = {
                    cmbTxt1: '',
                    txt1: '',
                    cmbTxt2: '',
                    txt2: '',
                    check: 'on',
                    cmbTgl1: $('#filter-tgl-cmb1').val(),
                    tgl1: $('#filter-tgl-val1').val(),
                    tgl2: $('#filter-tgl-val2').val(),
                    cmbNum1: '',
                    cmbNum2: '',
                    num1: '',
                    r: c
                };
                return JSON.stringify(search);
            }
        },
        altclass: 'row-zebra',
        altRows: true,
        pager: "#jqGridPager_type",
        viewrecords: true,
        rowNum: 15,
        rowList: [15, 30, 45, 60, 75],
        width: 780,
        height: 320,
        autowidth: false,
        shrinkToFit: false,
        styleUI: 'Bootstrap',
        // toppager:true,
        ajaxRowOptions: ajaxRowOpt,
        page: (document.cookie.match(new RegExp("(?:.*;)?\s*" + pageKey + "\s*=\s*([^;]+)(?:.*)?$")) || [, 1])[1],
        gridComplete: function () {
            $('a[data-confirm]').on('click', function (ev) {
                var href = $(this).attr('href');
                var rowId = $(this).attr('rowId');
                var dataConfirmModal = $('#dataConfirmModal');
                if (!dataConfirmModal.length) {
                    $('body').append('<div id="dataConfirmModal" class="modal bootstrap-dialog type-warning fade size-normal in" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true">' +
                        '<div class="modal-dialog"><div class="modal-content">' +
                        '<div class="modal-header"><div class="bootstrap-dialog-header"><div class="bootstrap-dialog-close-button" style="display: none;"><button class="close" data-dismiss="modal" aria-label="close">×</button></div><div class="bootstrap-dialog-title" id="w3_title">Confirmation</div></div></div>' +
                        '<form id="delete-form" method="post"><div class="modal-body"><div class="bootstrap-dialog-body"><div class="bootstrap-dialog-message">Are you sure you want to delete this item?</div></div></div>' +
                        '<div class="modal-footer"><div class="bootstrap-dialog-footer"><div class="bootstrap-dialog-footer-buttons"><button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><span class="fa fa-ban"></span>  Cancel</button><button class="btn btn-warning" type="submit" ><span class="glyphicon glyphicon-ok"></span> Ok</button></div></div></div></form></div></div></div>');
                }
                dataConfirmModal.find('.modal-body').text($(this).attr('data-confirm'));
                // $('#dataConfirmOK').attr('href', href);
                dataConfirmModal.modal({show: true});
                $('#delete-form').on('submit', function (e) {
                    e.preventDefault();
                    // $.notifyClose();
                    $.ajax({
                        url: href, //this is the submit URL
                        type: 'POST', //or POST
                        data: {
                            'oper': 'del',
                            'id': rowId
                        },
                        success: function (data) {
                            $.notify({
                                    message: data.message
                                },
                                {
                                    type: data.success ? 'success' : 'warning',
                                    allow_duplicates: false,
                                    mouse_over: 'pause',
                                    newest_on_top: true,
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    animate: {
                                        enter: 'animated fadeInDown',
                                        exit: 'animated fadeOutUp'
                                    }
                                });
                            $('#jqGrid_type').trigger('reloadGrid');
                        }
                    });
                    $('#dataConfirmModal').modal('hide');
                });
                return false;
            });
        }
    }).navGrid('#jqGridPager_type', {
        edit: false, add: false, del: false, search: false, refresh: false
    }).bindKeys();
    grid.jqGrid("setLabel", "rn", "No", {'text-align': 'center'});
    grid.jqGrid("setLabel", "actions", "Proses", {'text-align': 'center'});
    // grid warna
    var grid = $("#jqGrid_warna");
    grid.jqGrid({
        url: 'index?r=tdmotorwarna/td',
        mtype: "POST",
        datatype: "json",
        jsonReader: {
            root: 'rows',
            page: 'currentPage',
            total: 'totalPages',
            records: 'rowsCount',
            id: 'rowId',
            repeatitems: false
        },
        rownumbers: true,
        colModel: [
            <?=$cols_warna?>
        ],
        multiSort: true,
        sortname: setcmb_warna.sortname,
        sortorder: setcmb_warna.sortorder,
        search: true,
        postData: {
            query: function () {
                const search = {
                    cmbTxt1: '',
                    txt1: '',
                    cmbTxt2: '',
                    txt2: '',
                    check: 'on',
                    cmbTgl1: $('#filter-tgl-cmb1_warna').val(),
                    tgl1: $('#filter-tgl-val1_warna').val(),
                    tgl2: $('#filter-tgl-val2_warna').val(),
                    cmbNum1: '',
                    cmbNum2: '',
                    num1: '',
                    r: c
                };
                return JSON.stringify(search);
            }
        },
        altclass: 'row-zebra',
        altRows: true,
        pager: "#jqGridPager_warna",
        viewrecords: true,
        rowNum: 15,
        rowList: [15, 30, 45, 60, 75],
        width: 780,
        height: 320,
        autowidth: false,
        shrinkToFit: false,
        styleUI: 'Bootstrap',
        // toppager:true,
        ajaxRowOptions: ajaxRowOpt,
        page: (document.cookie.match(new RegExp("(?:.*;)?\s*" + pageKey + "\s*=\s*([^;]+)(?:.*)?$")) || [, 1])[1],
        gridComplete: function () {
            $('a[data-confirm]').on('click', function (ev) {
                var href = $(this).attr('href');
                var rowId = $(this).attr('rowId');
                var dataConfirmModal = $('#dataConfirmModal');
                if (!dataConfirmModal.length) {
                    $('body').append('<div id="dataConfirmModal" class="modal bootstrap-dialog type-warning fade size-normal in" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true">' +
                        '<div class="modal-dialog"><div class="modal-content">' +
                        '<div class="modal-header"><div class="bootstrap-dialog-header"><div class="bootstrap-dialog-close-button" style="display: none;"><button class="close" data-dismiss="modal" aria-label="close">×</button></div><div class="bootstrap-dialog-title" id="w3_title">Confirmation</div></div></div>' +
                        '<form id="delete-form" method="post"><div class="modal-body"><div class="bootstrap-dialog-body"><div class="bootstrap-dialog-message">Are you sure you want to delete this item?</div></div></div>' +
                        '<div class="modal-footer"><div class="bootstrap-dialog-footer"><div class="bootstrap-dialog-footer-buttons"><button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><span class="fa fa-ban"></span>  Cancel</button><button class="btn btn-warning" type="submit" ><span class="glyphicon glyphicon-ok"></span> Ok</button></div></div></div></form></div></div></div>');
                }
                dataConfirmModal.find('.modal-body').text($(this).attr('data-confirm'));
                // $('#dataConfirmOK').attr('href', href);
                dataConfirmModal.modal({show: true});
                $('#delete-form').on('submit', function (e) {
                    e.preventDefault();
                    // $.notifyClose();
                    $.ajax({
                        url: href, //this is the submit URL
                        type: 'POST', //or POST
                        data: {
                            'oper': 'del',
                            'id': rowId
                        },
                        success: function (data) {
                            $.notify({
                                    message: data.message
                                },
                                {
                                    type: data.success ? 'success' : 'warning',
                                    allow_duplicates: false,
                                    mouse_over: 'pause',
                                    newest_on_top: true,
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    animate: {
                                        enter: 'animated fadeInDown',
                                        exit: 'animated fadeOutUp'
                                    }
                                });
                            $('#jqGrid_type').trigger('reloadGrid');
                        }
                    });
                    $('#dataConfirmModal').modal('hide');
                });
                return false;
            });
        }
    }).navGrid('#jqGridPager_warna', {
        edit: false, add: false, del: false, search: false, refresh: false
    }).bindKeys();
    grid.jqGrid("setLabel", "rn", "No", {'text-align': 'center'});
    grid.jqGrid("setLabel", "actions", "Proses", {'text-align': 'center'});

    <?=$btnAddType;?>

    <?=$btnAddWarna;?>

    function resizeGrid() {
        $('#jqGrid_type')
            .setGridWidth($('.panel-body').width(), false);
        $('#jqGrid_warna')
            .setGridWidth($('.panel-body').width(), false);
    }

    resizeGrid();
    $('.panel-body').resize(resizeGrid);
    var tgl1 = $('#filter-tgl-val1');
    var tgl2 = $('#filter-tgl-val2');
    tgl1.change(function () {
        $('#filter-bottom-form').submit();
    })
    tgl2.change(function () {
        $('#filter-bottom-form').submit();
    })
    $("#checked-filter").change(function () {
        if (this.checked) {
            $("#span-checked-filter").text("Filter #2 Aktif");
            $(".inline-filter2").show();
        } else {
            $("#span-checked-filter").text("Filter #2 Non Aktif");
            $(".inline-filter2").hide();
        }
        // $(".filter2").prop('disabled', !this.checked);
    });
    var option = $('.cmbTxt');
    option.find('option').remove().end();
    for (let val in setcmb['cmbTxt']) {
        option.append('<option value="' + val + '">' + setcmb['cmbTxt'][val] + '</option>');
    }
    $('#filter-text-cmb2').prop("selectedIndex", 1);
    if (Object.keys(setcmb['cmbTgl']).length > 0) {
        var option = $('#filter-tgl-cmb1');
        option.find('option').remove().end();
        for (let val in setcmb['cmbTgl']) {
            option.append('<option value="' + val + '">' + setcmb['cmbTgl'][val] + '</option>');
        }
    }
    if (Object.keys(setcmb['cmbNum']).length > 0) {
        var option = $('#filter-num-cmb1');
        option.find('option').remove().end();
        for (let val in setcmb['cmbNum']) {
            option.append('<option value="' + val + '">' + setcmb['cmbNum'][val] + '</option>');
        }
    }

    function submitFilter() {
        // console.log($('#checked-filter').is(":checked"));
        var url_string = $(location).attr('href');
        var url = new URL(url_string);
        var c = decodeURIComponent(url.searchParams.get("r"));
        const search = {
            cmbTxt1: $('#filter-text-cmb1').val(),
            txt1: $('#filter-text-val1').val(),
            cmbTxt2: $('#filter-text-cmb2').val(),
            txt2: $('#filter-text-val2').val(),
            check: $('#checked-filter').is(":checked") ? 'on' : 'off',
            cmbTgl1: $('#filter-tgl-cmb1').val(),
            tgl1: $('#filter-tgl-val1').val(),
            tgl2: $('#filter-tgl-val2').val(),
            cmbNum1: $('#filter-num-cmb1').val(),
            cmbNum2: $('#filter-num-cmb2').val(),
            num1: $('#filter-num-val').val(),
            r: c
        };
        $('#jqGrid_type')
            .jqGrid('setGridParam', {
                sortname: setcmb.sortname,
                sortorder: setcmb.sortorder,
                postData: {
                    query: JSON.stringify(search)
                }
            }).trigger("reloadGrid");
        $('#gbox_grid .s-ico').css('display', 'none');
        $('#gbox_grid #jqgh_grid_id .s-ico').css('display', '');
        $('#gbox_grid #jqgh_grid_id .s-ico .ui-icon-triangle-1-s').removeClass('ui-state-disabled');
    }

    function findString(back) {
        var str = $('#txt-find-id').val();
        if (parseInt(navigator.appVersion) < 4) return;
        var strFound;
        if (window.find) {
            // CODE FOR BROWSERS THAT SUPPORT window.find
            strFound = self.find(str, 0, back);
            // if (!strFound) {
            //  strFound=self.find(str,0,1);
            //  while (self.find(str,0,1)) continue;
            // }
        } else if (navigator.appName.indexOf("Microsoft") != -1) {
            // EXPLORER-SPECIFIC CODE
            if (TRange != null) {
                TRange.collapse(false);
                strFound = TRange.findText(str);
                if (strFound) TRange.select();
            }
            if (TRange == null || strFound == 0) {
                TRange = self.document.body.createTextRange();
                strFound = TRange.findText(str);
                if (strFound) TRange.select();
            }
        } else if (navigator.appName == "Opera") {
            alert("Opera browsers not supported, sorry...")
            return;
        }
        if (!strFound) alert("String '" + str + "' not found!")
        return;
    }

    $('#filter-bottom-form').on('submit', function (e) {
        e.preventDefault();
        submitFilter();
    });
    $('#btn-find-id').on('click', function (e) {
        e.preventDefault();
        $('#findModal').modal();
    });
    $('#find-up').on('click', function (e) {
        e.preventDefault();
        findString(1);
    });
    $('#find-down').on('click', function (e) {
        e.preventDefault();
        findString(0);
    });
    $('.blur').on('blur', function (e) {
        submitFilter();
    });
    $('.chg').on('change', function (e) {
        setTimeout(submitFilter(), 500);
    });
    $('.blur').keydown(function (e) {
        if (e.keyCode == 13) {
            submitFilter();
        }
    });
    var tgl1 = $('#filter-tgl-val1_warna');
    var tgl2 = $('#filter-tgl-val2_warna');
    tgl1.change(function () {
        $('#filter-bottom-form_warna').submit();
    });
    tgl2.change(function () {
        $('#filter-bottom-form_warna').submit();
    });
    $("#checked-filter_warna").change(function () {
        if (this.checked) {
            $("#span-checked-filter_warna").text("Filter #2 Aktif");
            $(".inline-filter2").show();
        } else {
            $("#span-checked-filter_warna").text("Filter #2 Non Aktif");
            $(".inline-filter2").hide();
        }
        // $(".filter2").prop('disabled', !this.checked);
    });
    var option = $('.cmbTxt_warna');
    option.find('option').remove().end();
    for (let val in setcmb_warna['cmbTxt']) {
        option.append('<option value="' + val + '">' + setcmb_warna['cmbTxt'][val] + '</option>');
    }
    $('#filter-text-cmb2_warna').prop("selectedIndex", 1);
    if (Object.keys(setcmb_warna['cmbTgl']).length > 0) {
        var option = $('#filter-tgl-cmb1_warna');
        option.find('option').remove().end();
        for (let val in setcmb_warna['cmbTgl']) {
            option.append('<option value="' + val + '">' + setcmb_warna['cmbTgl'][val] + '</option>');
        }
    }
    if (Object.keys(setcmb_warna['cmbNum']).length > 0) {
        var option = $('#filter-num-cmb1');
        option.find('option').remove().end();
        for (let val in setcmb_warna['cmbNum']) {
            option.append('<option value="' + val + '">' + setcmb_warna['cmbNum'][val] + '</option>');
        }
    }

    function submitFilterWarna() {
        // console.log($('#checked-filter').is(":checked"));
        var url_string = $(location).attr('href');
        var url = new URL(url_string);
        var c = decodeURIComponent(url.searchParams.get("r"));
        const search = {
            cmbTxt1: $('#filter-text-cmb1_warna').val(),
            txt1: $('#filter-text-val1_warna').val(),
            cmbTxt2: $('#filter-text-cmb2_warna').val(),
            txt2: $('#filter-text-val2_warna').val(),
            check: $('#checked-filter_warna').is(":checked") ? 'on' : 'off',
            cmbTgl1: $('#filter-tgl-cmb1_warna').val(),
            tgl1: $('#filter-tgl-val1_warna').val(),
            tgl2: $('#filter-tgl-val2_warna').val(),
            cmbNum1: $('#filter-num-cmb1_warna').val(),
            cmbNum2: $('#filter-num-cmb2_warna').val(),
            num1: $('#filter-num-val_warna').val(),
            r: c
        };
        $('#jqGrid_warna')
            .jqGrid('setGridParam', {
                sortname: setcmb_warna.sortname,
                sortorder: setcmb_warna.sortorder,
                postData: {
                    query: JSON.stringify(search)
                }
            }).trigger("reloadGrid");
        $('#gbox_grid .s-ico').css('display', 'none');
        $('#gbox_grid #jqgh_grid_id .s-ico').css('display', '');
        $('#gbox_grid #jqgh_grid_id .s-ico .ui-icon-triangle-1-s').removeClass('ui-state-disabled');
    }

    function findStringWarna(back) {
        var str = $('#txt-find-id_warna').val();
        if (parseInt(navigator.appVersion) < 4) return;
        var strFound;
        if (window.find) {
            // CODE FOR BROWSERS THAT SUPPORT window.find
            strFound = self.find(str, 0, back);
            // if (!strFound) {
            //  strFound=self.find(str,0,1);
            //  while (self.find(str,0,1)) continue;
            // }
        } else if (navigator.appName.indexOf("Microsoft") != -1) {
            // EXPLORER-SPECIFIC CODE
            if (TRange != null) {
                TRange.collapse(false);
                strFound = TRange.findText(str);
                if (strFound) TRange.select();
            }
            if (TRange == null || strFound == 0) {
                TRange = self.document.body.createTextRange();
                strFound = TRange.findText(str);
                if (strFound) TRange.select();
            }
        } else if (navigator.appName == "Opera") {
            alert("Opera browsers not supported, sorry...")
            return;
        }
        if (!strFound) alert("String '" + str + "' not found!")
        return;
    }

    $('#filter-bottom-form_warna').on('submit', function (e) {
        e.preventDefault();
        submitFilterWarna();
    });
    $('#btn-find-id_warna').on('click', function (e) {
        e.preventDefault();
        $('#findModal_warna').modal();
    });
    $('#find-up_warna').on('click', function (e) {
        e.preventDefault();
        findStringWarna(1);
    });
    $('#find-down_warna').on('click', function (e) {
        e.preventDefault();
        findStringWarna(0);
    });
    $('.blur_warna').on('blur', function (e) {
        submitFilterWarna();
    });
    $('.chg_warna').on('change', function (e) {
        setTimeout(submitFilterWarna(), 500);
    });
    $('.blur_warna').keydown(function (e) {
        if (e.keyCode == 13) {
            submitFilterWarna();
        }
    });
});