<?php
use aunit\assets\JqwidgetsAsset;
use kartik\checkbox\CheckboxX;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tuakses */
/* @var $dataProvider yii\data\ActiveDataProvider */
JqwidgetsAsset::register( $this );
$this->title                   = 'Hak Akses';
$this->params['breadcrumbs'][] = $this->title;
//$this->registerJsVar( 'rTuakses', $data, \yii\web\View::POS_READY );
$this->registerJsVar( 'rArr', $rArr, \yii\web\View::POS_READY );
?>
    <div class="panel panel-default" style="height: 525px">
    <div class="box-body col-sm-12">
        <div class="form-group row">
            <label class="control-label col-sm-6">Hak Akses</label>
            <div class="col-sm-18">
				<?= Html::textInput( 'KodeAkses', $model->KodeAkses, [
					'id'    => 'KodeAkses',
					'class' => 'form-control'
				] ) ?>
            </div>
        </div>
        <div id='jqxWidget'>
            <div id='jqxTree' style='visibility: hidden;'>
            </div>
        </div>
    </div>
    <div class="box-body col-sm-12">
        <div class="form-group row">
            <div>
                <input type="hidden" id="recordId">
				<?= Html::textInput( 'MenuText', '', [
					'id'       => 'MenuText',
					'class'    => 'form-control',
					'readonly' => true
				] ) ?>
            </div>
        </div>
        <div class="form-group row">
			<? echo CheckboxX::widget( [
				'name'          => 'MenuTambah',
				'value'         => 0,
				'options'       => [ 'id' => 'MenuTambah' ],
				'pluginOptions' => [ 'threeState' => false ]
			] ); ?>
            <label class="cbx-label" for="MenuTambah">Tambah</label>
        </div>
        <div class="form-group row">
			<? echo CheckboxX::widget( [
				'name'          => 'MenuEdit',
				'value'         => 0,
				'options'       => [ 'id' => 'MenuEdit' ],
				'pluginOptions' => [ 'threeState' => false ]
			] ); ?>
            <label class="cbx-label" for="MenuEdit">Edit</label>
        </div>
        <div class="form-group row">
			<? echo CheckboxX::widget( [
				'name'          => 'MenuHapus',
				'value'         => 0,
				'options'       => [ 'id' => 'MenuHapus' ],
				'pluginOptions' => [ 'threeState' => false ]
			] ); ?>
            <label class="cbx-label" for="MenuHapus">Hapus</label>
        </div>
        <div class="form-group row">
			<? echo CheckboxX::widget( [
				'name'          => 'MenuCetak',
				'value'         => 0,
				'options'       => [ 'id' => 'MenuCetak' ],
				'pluginOptions' => [ 'threeState' => false ]
			] ); ?>
            <label class="cbx-label" for="MenuCetak">Cetak</label>
        </div>
        <div class="form-group row">
			<? echo CheckboxX::widget( [
				'name'          => 'MenuView',
				'value'         => 0,
				'options'       => [ 'id' => 'MenuView' ],
				'pluginOptions' => [ 'threeState' => false ]
			] ); ?>
            <label class="cbx-label" for="MenuView">Jurnal / View</label>
        </div>
        <div class="box-footer">
            <div class="pull-right">
				<?php ActiveForm::begin( [
					'id' => 'tuakses_form',
				] ); ?>
                <input type="hidden" id="records" name="records">
                <input type="hidden" id="KodeAksesId" name="KodeAkses">
				<?= Html::submitButton('<i class="glyphicon glyphicon-floppy-disk"></i> Simpan', ['class' => 'btn btn-info btn-primary '. Custom::viewClass(),  'id' => 'btnSave'] )?>
                <a href="<?= Url::to( [ 'tuakses/index' ] ) ?>"class="btn btn-danger btn-primary" role="button"><i class="fa fa-ban"></i>&nbsp&nbsp Batal &nbsp</a>'
				<?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
<?php
$this->registerJs( $this->render( 'tuakses.js' ) );
