<?php
/* @var $this yii\web\View */
/* @var $model aunit\models\Tuser */
$this->title                   = str_replace('Menu','',\aunit\components\TUi::$actionMode).'Hak Akses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tuakses-update">
	<?= $this->render( '_form', [
		'model' => $model,
		'rArr' => $rArr,
	] ) ?>
</div>
