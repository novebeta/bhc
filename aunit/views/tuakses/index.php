<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel aunit\models\TuaksesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hak Akses';
$this->params['breadcrumbs'][] = $this->title;
$arr = [
	'cmbTxt'    => [
		'KodeAkses' => 'KodeAkses',
	],
	'cmbTgl'    => [],
	'cmbNum'    => [],
	'sortname'  => "KodeAkses",
	'sortorder' => 'asc'
];
use aunit\assets\AppAsset;
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Tuakses::className(),
	'Hak Akses' ), \yii\web\View::POS_READY );
