<?php
/* @var $this yii\web\View */
/* @var $model aunit\models\Tuser */
$this->title                   = 'Tambah Hak Akses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tuakses-create">
	<?= $this->render( '_form', [
		'model' => $model,
		'rArr' => $rArr,
	] ) ?>
</div>
