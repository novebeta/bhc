<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttkk */
$params = '&id=' . $id . '&action=update&KKJenis=' . $model->KKJenis;
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel' . $params );
$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode)." - $title : " . $dsTUang[ 'KKNoView' ];
//$this->params[ 'breadcrumbs' ][] = [ 'label' => $title, 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = str_replace('Menu','',\aunit\components\TUi::$actionMode);
?>
<div class="ttkk-update">
	<?= $this->render( isset( $templateForm ) ? $templateForm : '_form', [
		'model'    => $model,
		'dsTUang'  => $dsTUang,
		'JoinData' => $JoinData,
		'id'       => $id,
		'url'      => [
			'update' => Custom::url( \Yii::$app->controller->id . "/$view-update" . $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
			'cancel' => $cancel,
			'detail' => Url::toRoute( [ 'ttkkitcoa/index', 'action' => 'create' ] ),
		]
	] ) ?>
</div>
