<?php
use aunit\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttkk */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttkk-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_ttkk_id', 'action' => $url[ 'update' ] ] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-1", 'items' => ":LabelKKNo" ],
						  [ 'class' => "col-sm-4", 'items' => ":KKNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelKKTgl" ],
						  [ 'class' => "col-sm-3", 'items' => ":KKTgl" ],
						  [ 'class' => "col-sm-3", 'items' => ":KKJam" ],
						  [ 'class' => "col-sm-3" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelLokasiKode" ],
						  [ 'class' => "col-sm-7", 'items' => ":LokasiKode" ],
					  ],
					],
					[ 'class' => "form-group col-md-24", 'style' => 'padding-top:5px;',
					  'items' => [
						  [ 'class' => "col-md-15", 'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>' ],
					  ],
					],
					[ 'class' => "col-sm-1" ],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelBayar" ],
						  [ 'class' => "col-sm-9", 'items' => ":Bayar" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelTotal" ],
						  [ 'class' => "col-sm-4", 'items' => ":Total" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelNominal" ],
						  [ 'class' => "col-sm-4", 'items' => ":Nominal" ],
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelKKMemo" ],
						  [ 'class' => "col-sm-9", 'items' => ":KKMemo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":Label2LokasiKode" ],
						  [ 'class' => "col-sm-4", 'items' => ":2LokasiKode" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelJenis" ],
						  [ 'class' => "col-sm-4", 'items' => ":Jenis" ],
					  ],
					],
				]
			],
			[
				'class' => "row-no-gutters",
				'items' => [
                    ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                        'items'  => [
                            ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":NoGL"],
                            ['class' => "col-sm-4 pull-right", 'items' => ":LabelNoGL"],
                        ],
                    ],
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'KKPerson' => 'Penerima',
                                'KKNo'     => 'No Kas Keluar',
                                'KKJenis'  => 'Jenis',
                                'KKLink'   => 'No Transaksi',
                                'KKMemo'   => 'Keterangan',
                                'UserID'   => 'User',
                                'NoGL'     => 'No GL',
                            ],
                        ])
                    ],
                    ['class' => "col-md-12 pull-right",
					  'items' => [
						  [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
					  ]
					]
				]
			],
			[
				/* label */
				":LabelKKNo"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No KK</label>',
				":LabelLokasiKode"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">POS</label>',
				":Label2LokasiKode" => '<label class="control-label" style="margin: 0; padding: 6px 0;">POS</label>',
				":LabelKKTgl"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl KK</label>',
				":LabelBayar"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bayar Ke</label>',
				":LabelTotal"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total</label>',
				":LabelNominal"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
				":LabelKKMemo"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelJenis"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				":LabelNoGL"        => \common\components\General::labelGL( $dsTUang[ 'NoGL' ] ),
				":KKNo"             => Html::textInput( 'KKNoView', $dsTUang[ 'KKNoView' ], [ 'class' => 'form-control', 'readonly' => true, 'tabindex' => '1' ] ) .
				                       Html::textInput( 'KKNo', $dsTUang[ 'KKNo' ], [ 'class' => 'hidden' ] ),
				":KKTgl"            => FormField::dateInput( [ 'name' => 'KKTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'KKTgl' ] ) ] ] ),
				":KKJam"            => FormField::timeInput( [ 'name' => 'KKJam', 'config' => [ 'value' => $dsTUang[ 'KKJam' ] ] ] ),
				":Nominal"          => FormField::decimalInput( [ 'name' => 'KKNominal', 'config' => [ 'value' => $dsTUang[ 'KKNominal' ] ] ] ),
				":KKMemo"           => Html::textInput( 'KKMemo', $dsTUang[ 'KKMemo' ], [ 'class' => 'form-control', 'tabindex' => '7' ] ),
				":Jenis"            => Html::textInput( 'KKJenis', $dsTUang[ 'KKJenis' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NoGL"             => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":LokasiKode"       => FormField::combo( 'LokasiKode', [ 'config' => [ 'value' => $dsTUang[ 'LokasiKode' ] ],'options' => ['tabindex'=>'3'], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":2LokasiKode"      => Html::textInput( 'lblLokasiKode', $dsTUang[ 'LokasiKode' ], [ 'class' => 'form-control', 'readOnly' => true ] ),
				":Bayar"            => Html::textInput( 'KKPerson', $dsTUang[ 'KKPerson' ], [ 'class' => 'form-control', 'tabindex' => '4' ] ),
				":Total"            => FormField::decimalInput( [ 'name' => 'Total', 'config' => [ 'value' => $dsTUang[ 'Total' ] ] ] ),
//				":btnSave"          => Html::button( '<i class="glyphicon glyphicon-floppy-disk"></i> Simpan', [ 'class' => 'btn btn-info btn-primary ', 'id' => 'btnSave' . Custom::viewClass() ] ),
//				":btnCancel"        => Html::Button( '<i class="fa fa-ban"></i> Batal', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:80px' ] ),
//				":btnAdd"           => Html::button( '<i class="fa fa-plus-circle"></i> Konfirm ', [ 'class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:80px' ] ),
//				":btnEdit"          => Html::button( '<i class="fa fa-edit"></i> Edit', [ 'class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:80px' ] ),
//				":btnDaftar"        => Html::Button( '<i class="fa fa-list"></i> Daftar ', [ 'class' => 'btn btn-primary ', 'id' => 'btnDaftar', 'style' => 'width:80px' ] ),
//				":btnPrint"         => Html::button( '<i class="glyphicon glyphicon-print"></i>  Print ', [ 'class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:80px' ] ),
//				":btnJurnal"        => Html::button( '<i class="fa fa-balance-scale"></i>  Jurnal ', [ 'class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:80px' ] ),

                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),
			],
            [
                '_akses' => 'Pengajuan Bayar BBN via Kas',
                'url_main' => 'ttkk',
                'url_extra' => ['JenisBBN'=>'Pengajuan'],
                'url_id' => $_GET['id'] ?? '',
                'url_delete'        => Url::toRoute(['ttkk/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlDetail        = $url[ 'detail' ];
$urlPrint         = $url[ 'print' ];
$url[ 'konfirm' ] = Url::toRoute( [ 'ttkk/konfirm-pengajuan', 'id' => $id ] );
$url[ 'jurnal' ] = \yii\helpers\Url::toRoute(['ttkk/jurnal']);
$hideFB           = ( $model->KKJenis == 'Bayar Unit' ? 'false' : 'true' );
$hideDK           = ( $model->KKJenis == 'Bayar Unit' ? 'true' : 'false' );
$readOnly         = ( ( $_GET[ 'action' ] == 'view' || $_GET[ 'action' ] == 'display' ) ? 'true' : 'false' );
$urlDK            = Url::toRoute( [ 'ttdk/select-pengajuan-bayar-bbn-kas', 'id' => $id, 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ) ] );
$urlLoop          = Url::toRoute( [ 'ttkkit/index' ] );
$urlAdd           = Url::toRoute( [ 'ttkk/pengajuan-bayar-bbn-kas-create', 'action' => 'create' ] );
$urlIndex         = Url::toRoute( [ 'ttkk/pengajuan-bayar-bbn-kas', 'id' => $id ] );
if ( isset( $_GET[ 'mode' ] ) ) {
	// kwitansi cetak
	$urlIndex = Url::toRoute( [ 'ttkk/' . $_GET[ 'mode' ] , 'id' => $id] );
}
$this->registerJs( <<< JS
     $('#detailGrid').utilJqGrid({  
        editurl: '$urlDetail',
        height: 295,
        extraParams: {
            KKNo: $('input[name="KKNo"]').val()
        },
        postData: {
            KKNo: $('input[name="KKNo"]').val()
        },
        rownumbers: true,  
        loadonce:true,
        rowNum: 1000,  
        readOnly: $readOnly,         
        navButtonTambah: false,
        colModel: [
            { template: 'actions', hidden: $readOnly  },
            {
                name: 'FBNo',
                label: 'No Dat Kon',
                editable: true,
                hidden: $hideDK ,
                width: 100,
            },
            {
                name: 'DKTgl',
                label: 'Tanggal DK',
                hidden: $hideDK ,
                width: 80,
                editable: true,
            },
            {
                name: 'CusNama',
                label: 'Nama Konsumen',
                hidden: $hideDK ,
                width: 200,
                editable: true,
            },
            {
                name: 'MotorNoMesin',
                label: 'No Mesin',
                hidden: $hideDK ,
                width: 120,
                editable: true,
            },
            {
                name: 'BBN',
                label: 'BBN',
                hidden: $hideDK ,
                width: 100,
                template: 'money',
                editable: true,
                editor: {
                    readOnly:true
                }  
            },
            {
                name: 'Terbayar',
                label: 'Terbayar',
                width: 100,
                template: 'money',
                editable: true,
                editor: {
                    readOnly:true
                }  
            },
            {
                name: 'KKBayar',
                label: 'Bayar',
                width: 100,
                template: 'money',
                editable: true,
                editrules:{
                    custom:true, 
                    custom_func: function(value) {
                        var KKBayar = parseFloat(window.util.number.getValueOfEuropeanNumber(value));
                        var BBN = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'BBN'+'"]').val()));
                        var Terbayar = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Terbayar'+'"]').val()));
                        if (KKBayar === 0) 
                            return [false,"Silahkan mengisi bayar."];  
                        else {                            
                            if (KKBayar > (BBN + Terbayar)){
                                $('[id="'+window.cmp.rowid+'_'+'KKBayar'+'"]').val(BBN - Terbayar);
                               return [false,"Nilai Bayar maksimal : " + window.util.number.format(BBN - Terbayar)];   
                            }else{
                                return [true,""];  
                            }
                        }
                    }
                } 
            },
            {
                name: 'Sisa',
                label: 'Sisa',
                width: 100,
                template: 'money',
                editable: true,
                editor: {
                    readOnly:true
                } 
            },
        ],     
        listeners: {
            afterLoad: function(grid, response) {
                let Total = $('#detailGrid').jqGrid('getCol','KKBayar',false,'sum');
                $('#Total-disp').utilNumberControl().val(Total);
            },
        } 
     }).init().fit($('.box-body')).load({url: '$urlDetail'});

    $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
        window.cmp.rowid = rowid;    
    });

 $('#detailGrid').jqGrid('navButtonAdd',"#detailGrid-pager",{ 
        caption: ' Tambah',
        buttonicon: 'glyphicon glyphicon-plus',
        onClickButton:  function() {
            window.util.app.dialogListData.closeTriggerFromIframe = function(){ 
                var iframe = window.util.app.dialogListData.iframe();
                var selRowId = iframe.contentWindow.util.app.dialogListData.gridFn.getSelectedRow(true);
               // console.log(selRowId);                
                $.ajax({
                     type: "POST",
                     url: '$urlLoop',
                     data: {
                         oper: 'batchPengajuan',
                         data: selRowId,
                         header: btoa($('#form_ttkk_id').serialize())
                     },
                     success: function(data) {
                        iframe.contentWindow.util.app.dialogListData.gridFn.mainGrid.jqGrid().trigger('reloadGrid');
                     },
                   });
                return false;
            }
            window.util.app.dialogListData.show({
                title: 'Daftar Data Konsumen untuk Pengajuan Bayar BBN Nomor : {$dsTUang["KKNoView"]}',
                url: '$urlDK'
            },function(){ 
                $('#detailGrid').utilJqGrid().load(); 
            });
        }, 
        position: "last", 
        title:"", 
        cursor: "pointer"
    });

    $('#btnPrint').click(function (event) {	      
        var url = new URL($(location).attr('href'));
        var c = decodeURIComponent(url.searchParams.get("mode"));
        if(c === 'kwitansi-kas-bayar-bbn'){
                bootbox.dialog({
                    title: 'Input',
                    message: '<div class="form-group col-md-24">' +
                     '<div class="col-md-8 col-sm-8 col-lg-8 col-xs-8"><label class="control-label" style="margin: 0; padding: 6px 0;">Pembayaran</label></div>' +
                     '<div class="col-md-16 col-sm-16 col-lg-16 col-xs-16" style="margin-bottom: 2px;">' +
                      '<select id="Pembayaran" class="form-control">' +
                          '<option value="Full">Full</option>' +
                          '<option value="Sebagian">Sebagian</option>' +
                          '<option value="Pelunasan">Pelunasan</option>' +
                      '</select></div>' +
                     '<div class="col-md-8 col-sm-8 col-lg-8 col-xs-8"><label class="control-label" style="margin: 0; padding: 6px 0;">Penerima</label></div>' +
                     '<div class="col-md-16 col-sm-16 col-lg-16 col-xs-16" style="margin-bottom: 2px;"><input id="Penerima" class="form-control" /></div>' +
                      '</div>',
                    size: 'medium',
                    onEscape: true,
                    backdrop: true,
                    buttons: {
                        ok: {
                            label: '<i class="glyphicon glyphicon-ok"></i>&nbsp&nbsp OK &nbsp&nbsp',
                            className: 'btn-success',
                            callback: function(){ 
                               $("#form_ttkk_id").append("<input name='StatPrint' type='hidden' value=25 >");
                               $("#form_ttkk_id").append("<input name='Pembayaran' type='hidden' value='"+$("#Pembayaran").val()+"'>");
                               $("#form_ttkk_id").append("<input name='Penerima' type='hidden' value='"+$("#Penerima").val()+"'>");
                               $('#form_ttkk_id').attr('action','$urlPrint');
                                $('#form_ttkk_id').attr('target','_blank');
                                $('#form_ttkk_id').submit();
                               location.reload(); 
                            }
                        },
                        cancel: {
                            label: '<i class="fa fa-ban"></i>&nbsp&nbsp Cancel &nbsp',
                            className: 'btn-danger',
                            callback: this.callback
                        }
                    }
                });
        }else{
                $('#form_ttkk_id').attr('action','$urlPrint');
                $('#form_ttkk_id').attr('target','_blank');
                $('#form_ttkk_id').submit();
        }
	  }); 
    
    
    $('#btnJurnal').click(function (event) {	
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#form_ttkk_id').attr('action','{$url[ 'jurnal' ]}');
                    $('#form_ttkk_id').submit();                     
                }
            }
        });
    }); 
    
     $('#btnCancel').click(function (event) {	      
       $('#form_ttkk_id').attr('action','{$url['cancel']}');
       $('#form_ttkk_id').submit();
    });
     
     
     $('#KKTotal-disp').blur(function() {
        $('#KKNominal').utilNumberControl().val($('#KKTotal').utilNumberControl().val());
      }); 
     
       $('#btnSave').click(function (event) {         
        $('#form_ttkk_id').attr('action','{$url['update']}');
        $('#form_ttkk_id').submit();
    }); 
       
        $('#btnAdd').click(function (event) {	      
        $('#form_ttkk_id').attr('action','{$url['konfirm']}');
       $('#form_ttkk_id').submit();
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });   
    
    $('[name=KKTgl]').utilDateControl().cmp().attr('tabindex', '2');
    $('[name=Total]').utilNumberControl().cmp().attr('tabindex', '5');
    $('[name=KKNominal]').utilNumberControl().cmp().attr('tabindex', '6');
     
JS
);


