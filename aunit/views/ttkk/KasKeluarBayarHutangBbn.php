<?php
use aunit\assets\AppAsset;
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'KAS KELUAR - Bayar Hutang BBN';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
    'cmbTxt'    => [
        'KKPerson'   => 'Penerima',
        'KKNo'       => 'No Kas Keluar',
        'KKJenis'    => 'Jenis',
        'KKLink'     => 'No Transaksi',
        'KKMemo'     => 'Keterangan',
        'UserID'     => 'User',
        'NoGL'       => 'No GL',

    ],
    'cmbTgl'    => [
        'KKTgl'      => 'Tanggal Kas Keluar',
    ],
    'cmbNum'    => [
        'KKNominal'  => 'Nominal',
    ],
    'sortname'  => "KKTgl",
    'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttkk::class,
	'Kas Keluar Bayar Hutang BBN' , [
        'colGrid'    => \aunit\models\Ttkk::colGridBayarHutangBbn(),
        'url_update' => Url::toRoute( [ 'ttkk/kas-keluar-bayar-hutang-bbn-update','action' => 'update' ] ),
        'url_add'    => Url::toRoute( [ 'ttkk/kas-keluar-bayar-hutang-bbn-create','action' => 'create' ] ),
        'url_delete' => Url::toRoute( [ 'ttkk/delete' ] ),
    ] ), \yii\web\View::POS_READY );
