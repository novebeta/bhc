<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttkk */

$params                          = '&id=' . $id . '&action=update&KKJenis='.$model->KKJenis;
$cancel                          = Custom::url( \Yii::$app->controller->id . "/cancel".$params );
$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode)." Kas Keluar Umum : "  . $dsTUang['KKNoView'];
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Kas Keluar Umum', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = str_replace('Menu','',\aunit\components\TUi::$actionMode);

//$cancel                          = Url::toRoute(['ttkkhd/kas-keluar-umum']);
//$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode).'Kas Keluar Umum : ' . $model->KKNo;
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Kas Keluar Umum', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = 'Edit';
?>
<div class="ttkk-update">
	<?= $this->render( '_kas-keluar-umum-form', [
		'model'    => $model,
		'dsTUang'  => $dsTUang,
		'JoinData' => $JoinData,
		'id'       => $id,
		'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/kas-keluar-umum-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttkkitcoa/index', 'action' => 'create' ] ),
        ]
//		'url'      => [
//			'detail' => Url::toRoute( [ 'ttkkitcoa/index', 'action' => 'create' ] ),
//		]
	] ) ?>
</div>
