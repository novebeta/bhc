<?php
use aunit\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttkk */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
$btnAttachment = '';
/** @var $model ttkk */
if($model->KKJenis == 'Bayar BBN'){
    $btnAttachment = ':btnAttachment';
}
?>
    <div class="ttkk-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_ttkk_id', 'action' => $url[ 'update' ] ] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-1", 'items' => ":LabelKKNo" ],
						  [ 'class' => "col-sm-4", 'items' => ":KKNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelKKTgl" ],
						  [ 'class' => "col-sm-3", 'items' => ":KKTgl" ],
						  [ 'class' => "col-sm-3", 'items' => ":KKJam" ],
						  [ 'class' => "col-sm-3" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelLokasiKode" ],
						  [ 'class' => "col-sm-7", 'items' => ":LokasiKode" ],
					  ],
					],
					[ 'class' => " col-md-24", 'style' => 'padding-top:5px',
					  'items' => [
						  [ 'class' => "col-md-15", 'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>' ],
					  ],
					],
					[ 'class' => "col-sm-1" ],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelBayar" ],
						  [ 'class' => "col-sm-9", 'items' => ":Bayar" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelTotal" ],
						  [ 'class' => "col-sm-4", 'items' => ":Total" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelNominal" ],
						  [ 'class' => "col-sm-4", 'items' => ":Nominal" ],
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelKKMemo" ],
						  [ 'class' => "col-sm-9", 'items' => ":KKMemo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":Label2LokasiKode" ],
						  [ 'class' => "col-sm-4", 'items' => ":2LokasiKode" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelJenis" ],
						  [ 'class' => "col-sm-4", 'items' => ":Jenis" ],
					  ],
					],
				]
			],
			[
				'class' => "row-no-gutters",
				'items' => [
                    ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                        'items'  => [
                            ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":NoGL"],
                            ['class' => "col-sm-4 pull-right", 'items' => ":LabelNoGL"],
                        ],
                    ],
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'KKPerson'   => 'Penerima',
                                'KKNo'       => 'No Kas Keluar',
                                'KKJenis'    => 'Jenis',
                                'KKLink'     => 'No Transaksi',
                                'KKMemo'     => 'Keterangan',
                                'UserID'     => 'User',
                                'NoGL'       => 'No GL',
                            ],
                        ])
                    ],
                    ['class' => "col-md-12 pull-right",
					  'items' => [
						  [ 'class' => "pull-right", 'items' => $btnAttachment . ":btnJurnal :btnPrint :btnAction" ]
					  ]
					]
				]
			],
			[
				/* label */
				":LabelKKNo"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No KK</label>',
				":LabelLokasiKode"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">POS</label>',
				":Label2LokasiKode" => '<label class="control-label" style="margin: 0; padding: 6px 0;">POS</label>',
				":LabelKKTgl"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl KK</label>',
				":LabelBayar"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bayar Ke</label>',
				":LabelTotal"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total</label>',
				":LabelNominal"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
				":LabelKKMemo"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelJenis"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				":LabelNoGL"        => \common\components\General::labelGL( $dsTUang[ 'NoGL' ] ),
				":KKNo"             => Html::textInput( 'KKNoView', $dsTUang[ 'KKNoView' ], [ 'class' => 'form-control', 'readonly' => true, 'tabindex' => '1' ] ) .
                                       Html::textInput( 'StatPrint', 1, [ 'class' => 'hidden' ] ).
				                       Html::textInput( 'KKNo', $dsTUang[ 'KKNo' ], [ 'class' => 'hidden' ] ),
				":KKTgl"            => FormField::dateInput( [ 'name' => 'KKTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'KKTgl' ] ) ] ] ),
				":KKJam"            => FormField::timeInput( [ 'name' => 'KKJam', 'config' => [ 'value' => $dsTUang[ 'KKJam' ] ] ] ),
				":Nominal"          => FormField::decimalInput( [ 'name' => 'KKNominal', 'config' => [ 'value' => $dsTUang[ 'KKNominal' ] ] ] ),
				":KKMemo"           => Html::textInput( 'KKMemo', $dsTUang[ 'KKMemo' ], [ 'class' => 'form-control', 'tabindex' => '7'  ] ),
				":Jenis"            => Html::textInput( 'KKJenis', $dsTUang[ 'KKJenis' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NoGL"             => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":LokasiKode"       => FormField::combo( 'LokasiKode', [ 'config' => [ 'value' => $dsTUang[ 'LokasiKode' ] ],'options' => ['tabindex' => '3'], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":2LokasiKode"      => Html::textInput( 'lblLokasiKode', $dsTUang[ 'LokasiKode' ], [ 'class' => 'form-control', 'readOnly' => true ] ),
				":Bayar"            => Html::textInput( 'KKPerson', $dsTUang[ 'KKPerson' ], [ 'class' => 'form-control', 'tabindex' => '4'  ] ),
				":Total"            => FormField::decimalInput( [ 'name' => 'Total', 'config' => [ 'value' => $dsTUang[ 'Total' ] ] ] ),

                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
                ":btnAttachment"       => Html::button('<i class="glyphicon glyphicon-file"><br><br>Lampiran</i>', ['class' => 'btn bg-black '.(($_GET[ 'action' ] ?? '') == 'create' ? 'hidden' : ''), 'id' => 'btnAttachment', 'style' => 'width:80px;height:60px']),
                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),

			],
            [
                'url_main'         => 'ttkk',
                'url_delete'        => Url::toRoute(['ttkk/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlDetail = $url[ 'detail' ];
$urlPrint  = $url[ 'print' ];
$hideFB    = ( $model->KKJenis == 'Bayar Unit' ? 'false' : 'true' );
$hideDK    = ( $model->KKJenis == 'Bayar Unit' ? 'true' : 'false' );
$readOnly  = ( ($_GET[ 'action' ] == 'view' || $_GET[ 'action' ] == 'display') ? 'true' : 'false' );
switch ( $dsTUang[ 'KKJenis' ] ) {
	case 'Bayar Unit':
		$urlDK = Url::toRoute( [ 'ttfb/select-kas-keluar-bayar-hutang-unit' ,'tgl1'    => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd')] );
		break;
	case 'Bayar BBN':
		$urlDK = Url::toRoute( [ 'ttdk/select-kk-bayar-bbn-bank' ,'tgl1'    => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd')] );
		break;
	case 'BBN Plus':
		$urlDK = Url::toRoute( [ 'ttdk/select-kk-bayar-bbn-plus','tgl1'    => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd') ] );
		break;
	case 'BBN Acc':
		$urlDK = Url::toRoute( [ 'ttdk/select-kk-bayar-bbn-acc' ,'tgl1'    => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd')] );
		break;
}
$urlLoop = Url::toRoute( [ 'ttkkit/index' ] );
$url[ 'jurnal' ] = \yii\helpers\Url::toRoute(['ttkk/jurnal']);
$urlAdd   = \aunit\models\Ttkk::getRoute( $dsTUang[ 'KKJenis' ],['action'=>'create'] )[ 'create' ];
$urlIndex = \aunit\models\Ttkk::getRoute( $dsTUang[ 'KKJenis' ], [ 'id' => $id ] )[ 'index' ];
$urlLampiran = Url::toRoute( [ 'ttkk/lampiran', 'id' => $id ] );
$this->registerJs( <<< JS
     $('#detailGrid').utilJqGrid({  
        editurl: '$urlDetail',
        height: 295,
        extraParams: {
            KKNo: $('input[name="KKNo"]').val()
        },
        postData: {
            KKNo: $('input[name="KKNo"]').val()
        },
        rownumbers: true,  
        loadonce:true,
        rowNum: 1000,  
        readOnly: $readOnly,    
        navButtonTambah: false,        
        colModel: [
            { template: 'actions', hidden: $readOnly  },
            {
                name: 'FBNo',
                label: 'No Dat Kon',
                editable: true,
                hidden: $hideDK ,
                width: 100,
                editor: { type:'text',readOnly: true }
            },
            {
                name: 'FBNo',
                label: 'No FB',
                width: 130,
                hidden: $hideFB ,
                editable: true,
                editor: { type:'text',readOnly: true }
            },
            {
                name: 'DKTgl',
                label: 'Tanggal DK',
                hidden: $hideDK ,
                width: 80,
                editable: true,
                editor: { type:'text',readOnly: true }
            },
            {
                name: 'FBTgl',
                label: 'Tanggal Faktur',
                hidden: $hideFB ,
                width: 110,
                editable: true,
                editor: { type:'text',readOnly: true }
            },
            {
                name: 'SSNo',
                label: 'No Srt Jln',
                hidden: $hideFB ,
                width: 160,
                editable: true,
                editor: { type:'text',readOnly: true }
            },
            {
                name: 'SupKode',
                label: 'SupKode',
                hidden: $hideFB ,
                width: 120,
                editable: true,
                editor: { type:'text',readOnly: true }
            },
            {
                name: 'FBKode',
                label: 'Total',
                hidden: $hideFB ,
                width: 90,
                editable: true,
                editor: { type:'text',readOnly: true }
            },
            {
                name: 'CusNama',
                label: 'Nama Konsumen',
                hidden: $hideDK ,
                width: 215,
                editable: true,
                editor: { type:'text',readOnly: true }
            },
            {
                name: 'MotorNoMesin',
                label: 'No Mesin',
                hidden: $hideDK ,
                width: 120,
                editable: true,
                editor: { type:'text',readOnly: true }
            },
            {
                name: 'BBN',
                label: 'BBN',
                hidden: $hideDK ,
                width: 100,
                editable: true,
                template: 'money',
                editor: { readOnly: true }
            },
            {
                name: 'Terbayar',
                label: 'Terbayar',
                width: 100,
                editable: true,
                template: 'money',
                editor: { readOnly: true }
            },
            {
                name: 'KKBayar',
                label: 'Bayar',
                width: 100,
                editable: true,
                template: 'money',
                editrules:{
                    custom:true, 
                    custom_func: function(value) {
                        var Bayar = parseFloat(window.util.number.getValueOfEuropeanNumber(value));
                        var BBN = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'BBN'+'"]').val()));
                        var Terbayar = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Terbayar'+'"]').val()));
                        if (Bayar === 0) 
                            return [false,"Silahkan mengisi bayar."];  
                        else {                            
                            if (Bayar > (BBN + Terbayar)){
                                $('[id="'+window.cmp.rowid+'_'+'KKBayar'+'"]').val(BBN - Terbayar);
                               return [false,"Nilai Bayar maksimal : " + window.util.number.format(BBN - Terbayar)];   
                            }else{
                                return [true,""];  
                            }
                        }
                    }
                }
            },
            {
                name: 'Sisa',
                label: 'Sisa',
                width: 100,
                editable: true,
                template: 'money',
                editor: { readOnly: true }
            },
        ],     
        listeners: {
            afterLoad: function(grid, response) {
                 let Total = $('#detailGrid').jqGrid('getCol','KKBayar',false,'sum');
                $('#Total-disp').utilNumberControl().val(Total);
            },
        } 
     }).init().fit($('.box-body')).load({url: '$urlDetail'});

    $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
        window.cmp.rowid = rowid;    
    });

    $('#detailGrid').jqGrid('navButtonAdd',"#detailGrid-pager",{ 
        caption: ' Tambah',
        buttonicon: 'glyphicon glyphicon-plus',
        onClickButton:  function() {
            window.util.app.dialogListData.closeTriggerFromIframe = function(){ 
                var iframe = window.util.app.dialogListData.iframe();
                var selRowId = iframe.contentWindow.util.app.dialogListData.gridFn.getSelectedRow(true);
               $('[name=KKLink]').val('Many');
                $.ajax({
                     type: "POST",
                     url: '$urlLoop',
                     data: {
                         oper: 'batch',
                         data: selRowId,
                         header: btoa($('#form_ttkk_id').serialize())
                     },
                     success: function(data) {
                        iframe.contentWindow.util.app.dialogListData.gridFn.mainGrid.jqGrid().trigger('reloadGrid');
                     },
                   });
                return false;
            }
            // window.util.app.dialogListData.close = function (data) {
            //     this.dialog.modal('hide');
            //     console.log(data); 
            // },
            window.util.app.dialogListData.show({
                title: 'Daftar Data untuk Kas Keluar : {$dsTUang["KKNoView"]}',
                url: '$urlDK'
            },function(){
                 window.location.href = "{$url['update']}&oper=skip-load&KKNoView={$dsTUang['KKNoView']}";  
            });
        }, 
        position: "last", 
        title:"", 
        cursor: "pointer"
    });
    
    function submitPrint(StatPrint){
        $('[name=StatPrint]').val(StatPrint);
        $('#form_ttkk_id').attr('action','$urlPrint');
        $('#form_ttkk_id').attr('target','_blank');
        $('#form_ttkk_id').submit();
    }

    $('#btnPrint').click(function (event) {	      
        submitPrint(1);
        // setTimeout(function() {submitPrint(2);},5000);
	  }); 
    
    
    $('#btnJurnal').click(function (event) {	
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#form_ttkk_id').attr('action','{$url[ 'jurnal' ]}');
                    $('#form_ttkk_id').submit();                     
                }
            }
        });
    }); 
    
     $('#btnCancel').click(function (event) {	      
       $('#form_ttkk_id').attr('action','{$url['cancel']}');
       $('#form_ttkk_id').submit();
    });
     
     
     $('#KKTotal-disp').blur(function() {
        $('#KKNominal').utilNumberControl().val($('#KKTotal').utilNumberControl().val());
      }); 
     
       $('#btnSave').click(function (event) {
           let Nominal = parseFloat($('#KKNominal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Silahkan mengisi nominal', size: 'small'});
               return;
           }
        $('#form_ttkk_id').attr('action','{$url['update']}');
        $('#form_ttkk_id').submit();
    }); 
       
        $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });    
    
    $('[name=KKTgl]').utilDateControl().cmp().attr('tabindex', '2');
    $('[name=Total').utilNumberControl().cmp().attr('tabindex', '5');
    $('[name=KKNominal]').utilNumberControl().cmp().attr('tabindex', '6');
    
    $('#btnAttachment').click(function (event) {
        window.location.href = '{$urlLampiran }';
    });
JS
);


