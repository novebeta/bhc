<?php

use common\components\Custom;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttkk */
$params                          = '&id=' . $id . '&action=create&KKJenis='.$model->KKJenis;
if ( isset( $_GET[ 'JenisBBN' ] ) && $_GET[ 'JenisBBN' ] == 'Pengajuan' ) {
	$params .= '&JenisBBN=Pengajuan';
}
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = "Tambah - $title";
//$this->params[ 'breadcrumbs' ][] = [ 'label' => $title, 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="ttkk-create">
    <?= $this->render(isset($templateForm) ? $templateForm : '_kas-keluar-form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'id'     => $id,
        'url' => [
            'update'    => Custom::url(\Yii::$app->controller->id."/$view-update".$params ),
            'print'     => Custom::url(\Yii::$app->controller->id.'/print'.$params ),
            'cancel'    => $cancel,
            'detail'    => Url::toRoute( [ 'ttkkit/index', 'action' => 'create' ] ),
        ]
    ]) ?>

</div>
