<?php
use common\components\Custom;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttkk */

$params = '&id='.$id.'&action=update&KKJenis='.urlencode($model->KKJenis);
if ( isset( $_GET[ 'JenisBBN' ] ) && $_GET[ 'JenisBBN' ] == 'Pengajuan' ) {
	$params .= '&JenisBBN=Pengajuan';
}
$cancel = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title = str_replace('Menu','',\aunit\components\TUi::$actionMode)." - $title: " . $dsTUang['KKNoView'];
//$this->params['breadcrumbs'][] = ['label' => $title, 'url' => $cancel];
//$this->params['breadcrumbs'][] = str_replace('Menu','',\aunit\components\TUi::$actionMode);
?>
<div class="ttkk-update">
	<?= $this->render( isset( $templateForm ) ? $templateForm : '_kas-keluar-form', [
		'model'   => $model,
		'dsTUang' => $dsTUang,
		'id'      => $id,
		'url' => [
            'update'    => Custom::url(\Yii::$app->controller->id."/$view-update".$params ),
            'print'     => Custom::url(\Yii::$app->controller->id.'/print'.$params ),
            'cancel'    => $cancel,
            'detail' => Url::toRoute( [ 'ttkkit/index', 'action' => 'create' ] ),
        ]
	] ) ?>
</div>
