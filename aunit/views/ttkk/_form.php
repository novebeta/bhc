<?php
use abengkel\models\Tdlokasi;
use aunit\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use common\models\User;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttkk */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
$btnAttachment = '';
/** @var $model ttkk */
if(in_array($model->KKJenis, array('KK Ke Bank','KK Ke Bengkel'))){
    $btnAttachment = ':btnAttachment';
}
?>
    <div class="ttkk-form">
		<?php
		$LokasiKodeCmb = ArrayHelper::map( Tdlokasi::find()->select( [ 'LokasiKode' ] )->where( [ 'LokasiStatus' => 'A' ] )->orderBy( 'LokasiStatus, LokasiKode' )->asArray()->all(), 'LokasiKode', 'LokasiKode' );
		$form          = ActiveForm::begin( [ 'id' => 'form_ttkk_id', 'action' => $url[ 'update' ] ] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelKKNo" ],
						  [ 'class' => "col-sm-5", 'items' => ":NoKK" ],
						  [ 'class' => "col-sm-2" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelKKTgl" ],
						  [ 'class' => "col-sm-3", 'items' => ":KKTgl" ],
						  [ 'class' => "col-sm-3", 'items' => ":KKJam" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-3", 'items' => ":LabelNominalKK" ],
						  [ 'class' => "col-sm-4", 'items' => ":KKNominal" ],
					  ],
					],
					[ 'class' => "form-group col-md-16",
					  'items' => [
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-3", 'items' => ":LabelCusNama" ],
							    [ 'class' => "col-sm-9", 'items' => ":CusNama" ],
							    [ 'class' => "col-sm-6", 'style' => 'padding-left:3px', 'items' => ":KKLink" ],
							    [ 'class' => "col-sm-6", 'items' => ":CariTanggal" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-3" ],
							    [ 'class' => "col-sm-21", 'items' => ":CusAlamat" ],
							    [ 'class' => "col-sm-3" ],
							    [ 'class' => "col-sm-21", 'style' => 'padding-top:3px', 'items' => ":txtMotor" ],
							    [ 'class' => "col-sm-3", 'items' => ":LabelKKMemo" ],
							    [ 'class' => "col-sm-21", 'style' => 'padding-top:3px', 'items' => ":KKMemo" ],
						    ],
						  ],
					  ],
					],
					[ 'class' => "col-sm-1" ],
					[ 'class' => "form-group col-md-7",
					  'items' => [
						  [ 'class' => "col-sm-24", 'items' => ":LabelKKPerson" ],
						  [ 'class' => "col-sm-24", 'items' => ":KKPerson" ],
						  [ 'class' => "col-sm-6", 'style' => 'padding-top:3px', 'items' => ":LabelLokasiKode" ],
						  [ 'class' => "col-sm-18", 'style' => 'padding-top:3px', 'items' => ":LokasiKode" ],
						  [ 'class' => "col-sm-6", 'style' => 'padding-top:3px', 'items' => ":LabelLease" ],
						  [ 'class' => "col-sm-18", 'style' => 'padding-top:3px', 'items' => ":Lease" ],
						  [ 'class' => "col-sm-6", 'style' => 'padding-top:3px', 'items' => ":LabelKKJenis" ],
						  [ 'class' => "col-sm-18", 'style' => 'padding-top:3px', 'items' => ":KKJenis" ],
					  ],
					],
				]
			],
			[
				'class' => "row-no-gutters",
				'items' => [
                    ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                        'items'  => [
                            ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":NoGL"],
                            ['class' => "col-sm-4 pull-right", 'items' => ":LabelNoGL"],
                        ],
                    ],
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'KKPerson'   => 'Penerima',
                                'KKNo'       => 'No Kas Keluar',
                                'KKJenis'    => 'Jenis',
                                'KKLink'     => 'No Transaksi',
                                'KKMemo'     => 'Keterangan',
                                'UserID'     => 'User',
                                'NoGL'       => 'No GL',
                            ],
                        ])
                    ],
                    ['class' => "col-md-12 pull-right",
					  'items' => [
						  [ 'class' => "pull-right", 'items' => $btnAttachment ." :btnJurnal :btnPrint :btnAction" ]
					  ]
					]
				]
			],
			[
				/* label */
				":LabelKKNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No KK</label>',
				":LabelKKTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl KK</label>',
				":LabelNominalKK"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
				":LabelCusNama"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Konsumen</label>',
				":LabelKKMemo"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelKKPerson"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bayar Ke / Diterima Oleh</label>',
				":LabelLokasiKode" => '<label class="control-label" style="margin: 0; padding: 6px 0;">POS</label>',
				":LabelLease"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Lease</label>',
				":LabelKKJenis"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				":LabelNoGL"       => \common\components\General::labelGL( $dsTUang[ 'NoGL' ] ),
				":NoKK"        => Html::textInput( 'KKNoView', $dsTUang[ 'KKNoView' ], [ 'class' => 'form-control', 'readonly' => true, 'tabindex' => '1' ] ) .
				                  Html::textInput( 'KKNo', $dsTUang[ 'KKNo' ], [ 'class' => 'hidden' ] ),
				":KKTgl"       => FormField::dateInput( [ 'name' => 'KKTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'KKTgl' ]) ] ] ),
				":KKJam"       => FormField::timeInput( [ 'name' => 'KKJam', 'config' => [ 'value' => $dsTUang[ 'KKJam' ] ] ] ),
				":KKNominal"   => FormField::decimalInput( [ 'name' => 'KKNominal', 'config' => [ 'value' => $dsTUang[ 'KKNominal' ] ] ] ),
				":KKPerson"    => Html::textInput( 'KKPerson', $dsTUang[ 'KKPerson' ], [ 'class' => 'form-control', 'tabindex' => '4' ] ),
				":KKLink"      => Html::textInput( 'KKLink', $dsTUang[ 'KKLink' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":KKMemo"      => Html::textInput( 'KKMemo', $dsTUang[ 'KKMemo' ], [ 'class' => 'form-control', 'tabindex' => '6' ] ),
				":KKJenis"     => Html::textInput( 'KKJenis', $dsTUang[ 'KKJenis' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NoGL"        => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":LokasiKode"  => ($dsTUang[ 'KKJenis' ]== 'KK Ke Bank') ? Html::textInput( 'LokasiKode', $dsTUang[ 'LokasiKode' ], [ 'class' => 'form-control', 'readOnly'=>true ] ) :
                    FormField::combo( 'LokasiKode', [ 'config' => [ 'value' => $dsTUang[ 'LokasiKode' ],'options' => ['tabindex' => '5'],'pluginEvents' => [
					                                                                  "change" => "function() {
                                                $('#LokasiKodeTxt').val(this.value);
                                        }",
				                                                                  ] ], 'data' => $LokasiKodeCmb, 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":CusNama"     => Html::textInput( 'CusNama', $JoinData[ 'CusNama' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":CariTanggal" => '<div class="input-group">' . FormField::dateInput( [ 'name' => 'dp_1', 'config' => [ 'value' => General::asDate( $JoinData[ 'KKLinkTgl' ] ), 'readonly' => true ] ] ) . '
                                    <span class="input-group-btn"><button id="BtnSearch" class="btn btn-default" type="button"><i class="glyphicon glyphicon-search" readonly></i></button></span></div>',
				":CusAlamat" => Html::textarea( 'CusAlamat', $JoinData[ 'CusAlamat' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":txtMotor"  => Html::textarea( 'MotorType', $JoinData[ 'MotorType' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Lease"     => Html::textInput( 'LeaseKode', $JoinData[ 'LeaseKode' ], [ 'class' => 'form-control', 'readonly' => true ] ),

                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
                ":btnAttachment"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Lampiran</i>   ', ['class' => 'btn bg-black '.(($_GET[ 'action' ] ?? '') == 'create' ? 'hidden' : ''), 'id' => 'btnAttachment', 'style' => 'width:80px;height:60px']),
                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),

			],
            [
                'url_main'         => 'ttkk',
                'url_delete'        => Url::toRoute(['ttkk/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php

$urlPrint  = $url[ 'print' ];
$url[ 'jurnal' ] = \yii\helpers\Url::toRoute(['ttkk/jurnal']);
$urlLoopDK = Url::toRoute( [ 'ttkk/loop-kas-keluar' ] );
$urlAdd   = \aunit\models\Ttkk::getRoute( $dsTUang[ 'KKJenis' ] ,['action'=>'create'])[ 'create' ];
$urlIndex = \aunit\models\Ttkk::getRoute( $dsTUang[ 'KKJenis' ], [ 'id' => $id ] )[ 'index' ];
$urlLampiran = Url::toRoute( [ 'ttkk/lampiran', 'id' => $id ] );
$msg = 'Silahkah Pilih Data Referensi terlebih dahulu';
$this->registerJsVar('__KKJenis', $dsTUang[ 'KKJenis' ]);
switch ( $dsTUang[ 'KKJenis' ] ) {
	case 'SubsidiDealer2' :
		$urlDatKonSelect = Url::toRoute( [ 'ttdk/select-kk-order-dk', 'KKNo' => $dsTUang[ 'KKNo' ], 'KKLink' => $dsTUang[ 'KKLink' ], 'jenis' => 'SubsidiDealer2','tgl1'    => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd') ] );
		$KKLink          = 'DKNo';
		$msg = 'Silahkah Pilih Data Referensi terlebih dahulu';
		break;
	case 'Retur Harga' :
		$urlDatKonSelect = Url::toRoute( [ 'ttdk/select-kk-order-dk', 'KKNo' => $dsTUang[ 'KKNo' ], 'KKLink' => $dsTUang[ 'KKLink' ], 'jenis' => 'ReturHarga','tgl1'    => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd') ] );
		$KKLink          = 'DKNo';
		$msg = 'Silahkah Pilih Data Referensi terlebih dahulu';
		break;
	case 'Insentif Sales' :
		$urlDatKonSelect = Url::toRoute( [ 'ttdk/select-kk-order-dk', 'KKNo' => $dsTUang[ 'KKNo' ], 'KKLink' => $dsTUang[ 'KKLink' ], 'jenis' => 'InsentifSales','tgl1'    => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd') ] );
		$KKLink          = 'DKNo';
		$msg = 'Silahkah Pilih Data Referensi terlebih dahulu';
		break;
	case 'Potongan Khusus' :
		$urlDatKonSelect = Url::toRoute( [ 'ttdk/select-kk-order-dk', 'KKNo' => $dsTUang[ 'KKNo' ], 'KKLink' => $dsTUang[ 'KKLink' ], 'jenis' => 'PotonganKhusus','tgl1'    => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd') ] );
		$KKLink          = 'DKNo';
		$msg = 'Silahkah Pilih Data Referensi terlebih dahulu';
		break;
	case 'KK Ke Bengkel' :
		$urlDatKonSelect = Url::toRoute( [ 'ttkm/select-kk-ke-bengkel', 'KKNo' => $dsTUang[ 'KKNo' ], 'KKLink' => $dsTUang[ 'KKLink' ], 'jenis' => 'KKKeBengkel','tgl1'    => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd') ] );
		$KKLink          = 'KMNo';
		$msg = 'Silahkah Pilih Data Referensi terlebih dahulu';
		break;
	case 'Pinjaman Karyawan':
		$urlDatKonSelect = Url::toRoute( [ 'tdsales/select', 'KKNo' => $dsTUang[ 'KKNo' ], 'KKLink' => $dsTUang[ 'KKLink' ], 'jenis' => 'PinjamanKaryawan' ] );
		$KKLink          = 'SalesKode';
		$msg = 'Silahkah Pilih Data Referensi terlebih dahulu';
		break;
	case 'KK Ke Bank' :
	case 'KK Pos Ke Dealer':
		$urlDatKonSelect = 'javascript:void(0)';
		$KKLink          = 'KMNo';
	$msg             = ( $dsTUang[ 'KKJenis' ] == 'KM Pos Dari Dealer' ) ? "Silahkah Pilih Data Kas Keluar Dealer Terlebih Dahulu" : "Silahkah Pilih Data Kas Keluar Pos Terlebih Dahulu";
		break;
	case 'KK Dealer Ke Pos':
		$urlDatKonSelect = Url::toRoute( [ 'tdlokasi/select', 'KKNo' => $dsTUang[ 'KKNo' ], 'KKLink' => $dsTUang[ 'KKLink' ], 'jenis' => str_replace( ' ', '', $dsTUang[ 'KKJenis' ] ) ] );
		$KKLink          = 'LokasiKode';
		$msg = 'Silahkah Pilih Data Referensi terlebih dahulu';
		break;
}

$user = User::findOne(Yii::$app->user->id);
$this->registerJsVar('KodeAkses', $user->KodeAkses);
$this->registerJs( <<< JS

    $('#BtnSearch').click(function () {
        // if (!__isCreate) return;
        window.util.app.dialogListData.show({
                title: 'Daftar Data Kas Keluar',
                url: '$urlDatKonSelect'
            },
            function (data) {
                if (data.data === undefined) return;
                console.log(data);
                $('[name=KKNominal]').utilNumberControl().val(data.data.Sisa);
                $('[name=KKLink]').val(data.data.{$KKLink});
                if(__KKJenis === 'Pinjaman Karyawan'){
                    $('[name=CusNama]').val(data.data.SalesNama);
                    $('[name=KKPerson]').val(data.data.SalesNama);
                    $('[name=CusAlamat]').val(data.data.SalesAlamat);
                }                
                if (data.data.CusNama !== undefined){
                    $('[name=KKPerson]').val(data.data.CusNama);
                }
                $.ajax({
                    type: 'POST',
                    url: '$urlLoopDK',
                    data: $('#form_ttkk_id').serialize()
                }).then(function (cusData) { // console.log(data);
                    window.location.href = "{$url['update']}&oper=skip-load&KKNoView={$dsTUang['KKNoView']}"; 
                });
            })
    });

    $('#btnPrint').click(function (event) {	      
        $('#form_ttkk_id').attr('action','$urlPrint');
        $('#form_ttkk_id').attr('target','_blank');
        $('#form_ttkk_id').submit();
	  }); 
    
    $('#btnJurnal').click(function (event) {	
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#form_ttkk_id').attr('action','{$url[ 'jurnal' ]}');
                    $('#form_ttkk_id').submit();                     
                }
            }
        });
    }); 

     $('#btnCancel').click(function (event) {	      
       $('#form_ttkk_id').attr('action','{$url['cancel']}');
       $('#form_ttkk_id').submit();
    });
     
     $('#KKTotal-disp').blur(function() {
        $('#KKNominal').utilNumberControl().val($('#KKTotal').utilNumberControl().val());
      }); 
     
       $('#btnSave').click(function (event) {
             let KKLink = $('[name="KKLink"]').val();
             let KKJenis = $('[name="KKJenis"]').val();             
             let KKTgl = new Date($('[name=KKTgl]').val()).toDateString();
            let tglNow = new Date().toDateString();
           if (KKJenis !== 'KK Ke Bank' && (KKLink === '' || KKLink === '--') ){
               bootbox.alert({message:'{$msg}', size: 'small'});
               return;
           }
           if (KodeAkses != 'Auditor') {
                   if(KKTgl < tglNow ){
                    // bootbox.alert({ message: "Tgl Transaksi ("+KKTgl+") lebih kecil dari Tgl Sistem ("+tglNow+") Silakan hubungi Admin untuk melakukan proses Hapus atau Simpan.", size: 'small'});
                    bootbox.alert({ message: "Tgl Transaksi lebih kecil dari Tgl Sistem, Silakan hubungi Admin untuk melakukan proses Hapus atau Simpan.", size: 'small'});
                   return;
               }
           }           
           let Nominal = parseFloat($('#KKNominal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Silahkan mengisi nominal', size: 'small'});
               return;
           }
        $('#form_ttkk_id').attr('action','{$url['update']}');
        $('#form_ttkk_id').submit();
    }); 
       
        $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
      
    $('#btnAttachment').click(function (event) {	      
        window.location.href = '{$urlLampiran }';
    }); 
    
    $('[name=KKTgl]').utilDateControl().cmp().attr('tabindex', '2');
    $('[name=KKNominal]').utilNumberControl().cmp().attr('tabindex', '3');
    
     
JS
);

