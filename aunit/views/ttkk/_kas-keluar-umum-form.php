<?php
use abengkel\models\Tdlokasi;
use aunit\assets\AppAsset;
use aunit\components\FormField;
use aunit\components\TUi;
use aunit\models\Ttkk;
use common\components\Custom;
use common\components\General;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use aunit\components\Menu;
AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttkm */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttkk-form">
		<?php
		$LokasiKodeCmb = ArrayHelper::map( Tdlokasi::find()->select( [ 'LokasiKode' ] )->orderBy( 'LokasiStatus, LokasiKode' )->asArray()->all(), 'LokasiKode', 'LokasiKode' );
		$form          = ActiveForm::begin( [ 'id' => 'form_ttkk_id', 'action' => $url[ 'update' ] ] );
		TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelKKNo" ],
						  [ 'class' => "col-sm-5", 'items' => ":NoKK" ],
						  [ 'class' => "col-sm-2" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelKKTgl" ],
						  [ 'class' => "col-sm-3", 'items' => ":KKTgl" ],
						  [ 'class' => "col-sm-3", 'items' => ":KKJam" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-3", 'items' => ":LabelNominalKK" ],
						  [ 'class' => "col-sm-4", 'items' => ":KKNominal" ],
					  ],
					],
					[ 'class' => "form-group col-md-16",
					  'items' => [
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-3", 'items' => ":LabelKKKonsumen" ],
							    [ 'class' => "col-sm-9", 'items' => ":KKKonsumen" ],
							    [ 'class' => "col-sm-6", 'style' => 'padding-left:3px', 'items' => ":KKLink" ],
							    [ 'class' => "col-sm-6", 'items' => ":CariTanggal" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-3" ],
							    [ 'class' => "col-sm-21", 'items' => ":CusAlamat" ],
							    [ 'class' => "col-sm-3" ],
							    [ 'class' => "col-sm-21", 'style' => 'padding-top:3px', 'items' => ":txtMotor" ],
							    [ 'class' => "col-sm-3", 'items' => ":LabelKKMemo" ],
							    [ 'class' => "col-sm-21", 'style' => 'padding-top:3px', 'items' => ":KKMemo" ],
						    ],
						  ],
					  ],
					],
					[ 'class' => "col-sm-1" ],
					[ 'class' => "form-group col-md-7",
					  'items' => [
						  [ 'class' => "col-sm-24", 'items' => ":LabelTerimaDari" ],
						  [ 'class' => "col-sm-24", 'items' => ":NamaTerimaDari" ],
						  [ 'class' => "col-sm-6", 'style' => 'padding-top:3px', 'items' => ":LabelLokasiKode" ],
						  [ 'class' => "col-sm-18", 'style' => 'padding-top:3px', 'items' => ":LokasiKode" ],
						  [ 'class' => "col-sm-6", 'style' => 'padding-top:3px', 'items' => ":LabelLease" ],
						  [ 'class' => "col-sm-18", 'style' => 'padding-top:3px', 'items' => ":Lease" ],
						  [ 'class' => "col-sm-6", 'style' => 'padding-top:3px', 'items' => ":LabelKKJenis" ],
						  [ 'class' => "col-sm-18", 'style' => 'padding-top:3px', 'items' => ":KKJenis" ],
					  ],
					],
					[
						'class' => "row col-md-24",
						'style' => "margin-bottom: 6px;",
						'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
					],
				]
			],
			[
				'class' => "row-no-gutters",
				'items' => [
                    ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                        'items'  => [
                            ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":NoGL"],
                            ['class' => "col-sm-4 pull-right", 'items' => ":LabelNoGL"],
                        ],
                    ],
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'KKPerson'   => 'Penerima',
                                'KKNo'       => 'No Kas Keluar',
                                'KKJenis'    => 'Jenis',
                                'KKLink'     => 'No Transaksi',
                                'KKMemo'     => 'Keterangan',
                                'UserID'     => 'User',
                                'NoGL'       => 'No GL',
                            ],
                        ])
                    ],
                    ['class' => "col-md-12 pull-right",
					  'items' => [
						  [ 'class' => "pull-right", 'items' => ":btnAttachment :btnJurnal :btnPrint :btnAction" ]
					  ]
					]
				]
			],
			[
				/* label */
				":LabelKKNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No KK</label>',
				":LabelKKTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl KK</label>',
				":LabelNominalKK"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
				":LabelKKKonsumen" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Konsumen</label>',
				":LabelKKMemo"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelTerimaDari" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Terima Dari / Diserahkan Oleh</label>',
				":LabelLokasiKode" => '<label class="control-label" style="margin: 0; padding: 6px 0;">POS</label>',
				":LabelLease"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Lease</label>',
				":LabelKKJenis"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				":LabelNoGL"       => General::labelGL( $dsTUang[ 'NoGL' ] ),

				":NoKK"            => Html::textInput( 'KKNoView', $dsTUang[ 'KKNoView' ], [ 'class' => 'form-control', 'readonly' => true, 'tabindex' => '1' ] ) .
				                      Html::textInput( 'KKNo', $dsTUang[ 'KKNo' ], [ 'class' => 'hidden' ] ),
				":KKTgl"           => FormField::dateInput( [ 'name' => 'KKTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'KKTgl' ] ) ] ] ),
				":KKJam"           => FormField::timeInput( [ 'name' => 'KKJam', 'config' => [ 'value' => $dsTUang[ 'KKJam' ] ] ] ),
				":KKNominal"       => FormField::decimalInput( [ 'name' => 'KKNominal', 'config' => [ 'value' => $dsTUang[ 'KKNominal' ] ] ] ),
				":KKKonsumen"      => Html::textInput( 'CusNama', $JoinData[ 'CusNama' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":KKLink"          => Html::textInput( 'KKLink', $dsTUang[ 'KKLink' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":KKMemo"          => Html::textInput( 'KKMemo', $dsTUang[ 'KKMemo' ], [ 'class' => 'form-control', 'tabindex' => '5' ] ),
				":KKJenis"         => Html::textInput( 'KKJenis', $dsTUang[ 'KKJenis' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NoGL"            => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":LokasiKode"      => FormField::combo( 'LokasiKode', [ 'config' => [ 'value'            => $dsTUang[ 'LokasiKode' ], 'data' => $LokasiKodeCmb,
				                                                                      'pluginEvents'     => [
					                                                                      "change" => "function() {
                                                            $('#LokasiKodeTxt').val(this.value);
                                                            }",
				                                                                      ], 'pluginOptions' => [ 'disabled' => true ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":NamaTerimaDari"  => Html::textarea( 'KKPerson', $dsTUang[ 'KKPerson' ], [ 'class' => 'form-control', 'tabindex' => '4' ] ),
				":CariTanggal"     => '<div class="input-group">' . FormField::dateInput( [ 'name' => 'dp_1', 'config' => [ 'value' => General::asDate( 'now' ), 'readonly' => true ] ] ) . '
                                    <span class="input-group-btn"><button id="BtnSearchDatkon" class="btn btn-default" type="button"><i class="glyphicon glyphicon-search" readonly></i></button></span></div>',
				":CusAlamat"       => Html::textarea( 'CusAlamat', $JoinData[ 'CusAlamat' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":txtMotor"        => Html::textarea( 'MotorType', $JoinData[ 'MotorType' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Lease"           => Html::textInput( 'LeaseKode', $JoinData[ 'LeaseKode' ], [ 'class' => 'form-control', 'readonly' => true ] ),

                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
                ":btnAttachment"       => Html::button('<i class="glyphicon glyphicon-file"><br><br>Lampiran</i>', ['class' => 'btn bg-black '.(($_GET[ 'action' ] ?? '') == 'create' ? 'hidden' : ''), 'id' => 'btnAttachment', 'style' => 'width:80px;height:60px']),
                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),
			],
            [
                'url_main'         => 'ttkk',
                'url_delete'        => Url::toRoute(['ttkk/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php

$urlDetail       = $url[ 'detail' ];
$urlPrint        = $url[ 'print' ];
$url[ 'jurnal' ] = Url::toRoute( [ 'ttkk/jurnal' ] );
$readOnly        = ( ( $_GET[ 'action' ] == 'view' || $_GET[ 'action' ] == 'display' ) ? 'true' : 'false' );
//$DtAccount       = General::cCmd( "SELECT NoAccount, NamaAccount, OpeningBalance, JenisAccount, StatusAccount, NoParent FROM traccount
//WHERE JenisAccount = 'Detail' ORDER BY NoAccount" )->queryAll();
$akses =  Menu::getUserLokasi()[ 'KodeAkses' ] ;
if($akses === "Admin" || $akses === "Auditor"){
    $DtAccount       = General::cCmd(
            "SELECT NoAccount, NamaAccount, OpeningBalance, JenisAccount, StatusAccount, NoParent FROM traccount 
                WHERE  StatusAccount = 'A' AND JenisAccount = 'Detail' AND LEFT(NoAccount,4) NOT IN ('1102','1103') 
                Order By StatusAccount, NoAccount" )->queryAll();
}else{
    $DtAccount       = General::cCmd(
            "SELECT NoAccount, NamaAccount, StatusAccount FROM traccount 
                WHERE StatusAccount = 'A' AND JenisAccount = 'Detail' AND NoAccount <> '11010100' 
                AND LEFT(NoAccount,4) NOT IN ('1102','1103','7000','8010','9010') Order By StatusAccount, NoAccount" )->queryAll();
}

$this->registerJsVar( '__dtAccount', json_encode( $DtAccount ) );
$urlDatKonSelect = Url::toRoute( [ 'ttdk/select-kas-masuk-umum', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ) ] );
$urlLoopDK       = Url::toRoute( [ 'ttkk/loop-kas-keluar-umum' ] );
$urlAdd   = Ttkk::getRoute( $dsTUang[ 'KKJenis' ], [ 'action' => 'create' ] )[ 'create' ];
$urlIndex = Ttkk::getRoute( $dsTUang[ 'KKJenis' ], [ 'id' => $id ] )[ 'index' ];
$urlLampiran = Url::toRoute( [ 'ttkk/lampiran', 'id' => $id ] );
$this->registerJsVar( 'akses', Menu::getUserLokasi()[ 'KodeAkses' ] );
$this->registerJs( <<< JS
     var __NoAccount = JSON.parse(__dtAccount);
     __NoAccount = $.map(__NoAccount, function (obj) {
                      obj.id = obj.NoAccount;
                      obj.text = obj.NoAccount;
                      return obj;
                    });
     var __NamaAccount = JSON.parse(__dtAccount);
     __NamaAccount = $.map(__NamaAccount, function (obj) {
                      obj.id = obj.NamaAccount;
                      obj.text = obj.NamaAccount;
                      return obj;
                    });
     $('#detailGrid').utilJqGrid({  
        editurl: '$urlDetail',
        height: 160,
        extraParams: {
            KKNo: $('input[name="KKNo"]').val()
        },
        rownumbers: true, 
        loadonce:true,
        rowNum: 1000,  
        readOnly: $readOnly,        
        colModel: [
            { template: 'actions'},
            {
                name: 'NoAccount',
                label: 'Kode',
                editable: true,
                width: 140,
                editor: {
                    type: 'select2',
                    data: __NoAccount,  
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    // console.log(data);
						    let rowid = $(this).attr('rowid');
                            $('[id="'+rowid+'_'+'NamaAccount'+'"]').val(data.NamaAccount); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'NamaAccount'+'"]').trigger('change');
						}
                    }                  
                }
            },
            {
                name: 'NamaAccount',
                label: 'Nama',
                width: 250,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __NamaAccount, 
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    // console.log(data);
						    let rowid = $(this).attr('rowid');
                            $('[id="'+rowid+'_'+'NoAccount'+'"]').val(data.NoAccount); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'NoAccount'+'"]').trigger('change');
						}
                    }                              
                }
            },
            {
                name: 'KKKeterangan',
                label: 'Keterangan',
                width: 285,
                editable: true,
                editoptions:{
                    maxlength:"150"
                },               
            },
            {
                name: 'KKBayar',
                label: 'Debit',
                width: 190,
                editable: true,
                template: 'money'
            },            
            {
                name: 'KKAutoN',
                label: 'KKAutoN',
                width: 210,
                editable: true,
                hidden: true
            },
        ],     
        listeners: {
            afterLoad: function(){
                let TotalKKBayar = $('#detailGrid').jqGrid('getCol','KKBayar',false,'sum');
                $("#detailGrid-pager_right").html('<label class="control-label" style="margin-right:10px;">Sub Total COA </label>' +
                 '<label style="width:190px;margin-right:15px;">'+TotalKKBayar.toLocaleString("id-ID", {minimumFractionDigits:2})+'</label>');
            }
        }
     }).init().fit($('.box-body')).load({url: '$urlDetail'});
     
     $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowId, orgClickEvent) {
        // console.log(orgClickEvent);
        if (orgClickEvent.extraparam.oper === 'add'){
            var KKKeterangan = $('[id="'+rowId+'_'+'KKKeterangan'+'"]');
            KKKeterangan.val($('[name="KKMemo"]').val());
        }
        
    });
     
    $('#BtnSearchDatkon').click(function () {
        // if (!__isCreate) return;
        window.util.app.dialogListData.show({
                title: 'Daftar Data Konsumen',
                url: '$urlDatKonSelect'
            },
            function (data) {
                // console.log(data);
                if (data.data == undefined){
                    return;
                }
                $('[name=KKLink]').val(data.data.DKNo);
                $.ajax({
                    type: 'POST',
                    url: '$urlLoopDK',
                    data: $('#form_ttkk_id').serialize()
                }).then(function (cusData) { // console.log(data);
                    window.location.href = "{$url['update']}&oper=skip-load&KKNoView={$dsTUang['KKNoView']}"; 
                });
            })
    });
     
    $('#btnPrint').click(function (event) {	      
        $('#form_ttkk_id').attr('action','$urlPrint');
        $('#form_ttkk_id').attr('target','_blank');
        $('#form_ttkk_id').submit();
	  }); 
    
    
    $('#btnJurnal').click(function (event) {	
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#form_ttkk_id').attr('action','{$url['jurnal']}');
                    $('#form_ttkk_id').submit();                     
                }
            }
        });
    }); 
    
     $('#btnCancel').click(function (event) {	      
       $('#form_ttkk_id').attr('action','{$url['cancel']}');
       $('#form_ttkk_id').submit();
    });
     
     $('#KKTotal-disp').blur(function() {
        $('#KKNominal').utilNumberControl().val($('#KKTotal').utilNumberControl().val());
      }); 
     
     $('#btnSave').click(function (event) {	
        let TotalKMBayar = $('#detailGrid').jqGrid('getCol','KKBayar',false,'sum');
        let SubTotal = $('#KKNominal').utilNumberControl().val();
        let BMTgl = new Date($('#BMTgl').val());
        let tglNow = new Date();
        let diffTime = Math.abs(tglNow - BMTgl);
        let diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
        // console.log(TotalKMBayar + ' ' + SubTotal);
        /* move to sp
        if (TotalKMBayar !== SubTotal){
            bootbox.alert({ message: "Jumlah nominal dengan subtotal coa berbeda.", size: 'small'});
            return;
        }*/
        let param = ['Admin', 'Auditor'];
        if(!param.includes(akses)){
         if (diffDays > 3){
            bootbox.alert({ message: "Tgl Transaksi (" + BMTgl + ") lebih kecil dari Tgl Sistem (" + tglNow + ") Silakan Hhubungi Admin untuk melakukan proses Hapus atau Simpan" , size: 'small'});
            return;
         }
         $('#form_ttkk_id').attr('action','{$url['update']}');
         $('#form_ttkk_id').submit();
        }
        $('#form_ttkk_id').attr('action','{$url['update']}');
        $('#form_ttkk_id').submit();
    });
     
      $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
    });

    $('#btnAttachment').click(function (event) {	      
        window.location.href = '{$urlLampiran }';
    }); 
    
    $('[name=KKTgl]').utilDateControl().cmp().attr('tabindex', '2');
    $('[name=KKNominal').utilNumberControl().cmp().attr('tabindex', '3');
    
    
JS
);

