<?php
use common\components\Custom;
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttkk */
$params                          = '&id=' . $id . '&action=UpdateAdd';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/kas-transfer-cancel'.$params );
$this->title                     = 'Tambah - Kas Transfer';
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Kas Transfer', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="ttkkhd-create">
    <?= $this->render('_kasTransferform', [
        'dsTUang' => $dsTUang,
        'id'=>$id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/kas-transfer-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
        ]
    ]) ?>
</div>
