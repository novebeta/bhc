<?php
use aunit\assets\AppAsset;
use common\components\Custom;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'KAS KELUAR - Umum';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
    'cmbTxt'    => [
        'KKPerson'   => 'Penerima',
        'KKNo'       => 'No Kas Keluar',
        'KKJenis'    => 'Jenis',
        'KKLink'     => 'No Transaksi',
        'KKMemo'     => 'Keterangan',
        'UserID'     => 'User',
        'NoGL'       => 'No GL',

    ],
    'cmbTgl'    => [
        'KKTgl'      => 'Tanggal Kas Keluar',
    ],
    'cmbNum'    => [
        'KKNominal'  => 'Nominal',
    ],
    'sortname'  => "KKTgl",
    'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttkk::className(),
	'Kas Keluar Umum' , [
        'colGrid'    => \aunit\models\Ttkk::colGridKasKeluarUmum(),
        'url_update' => Url::toRoute( [ 'ttkk/kas-keluar-umum-update','action' => 'update' ] ),
        'url_add'    => Url::toRoute( [ 'ttkk/kas-keluar-umum-create','action' => 'create' ] ),
        'url_delete' => Url::toRoute( [ 'ttkk/delete' ] ),
//        'url_add'    => Custom::url( 'ttkk/kas-keluar-umum-create' ),
//        'url_update' => Custom::url( 'ttkk/kas-keluar-umum-update' ),
//        'url_delete' => Custom::url( 'ttkk/td' ),
    ] ), \yii\web\View::POS_READY );
