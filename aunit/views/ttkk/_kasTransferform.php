<?php
use aunit\assets\AppAsset;
use aunit\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttkk */
$sqlWarehouse = Yii::$app->db
	->createCommand( "SELECT LokasiKode as value, LokasiNama as label FROM tdlokasi WHERE LokasiStatus <> 'N' ORDER BY LokasiStatus,LokasiKode" )
	->queryAll();
$cmbWarehouse = ArrayHelper::map( $sqlWarehouse, 'value', 'label' );
$format       = <<< SCRIPT
SCRIPT;
$escape       = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
/* @var $form yii\widgets\ActiveForm */
AppAsset::register( $this );
?>
    <div class="ttkk-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_ttkk_id', 'action' => $url[ 'update' ] ] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelKTNo" ],
						  [ 'class' => "col-sm-4", 'items' => ":KTNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelKTTgl" ],
						  [ 'class' => "col-sm-3", 'items' => ":KTTgl" ],
						  [ 'class' => "col-sm-3", 'items' => ":KTJam" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelKKNo" ],
						  [ 'class' => "col-sm-3", 'items' => ":KKNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelKMNo" ],
						  [ 'class' => "col-sm-3", 'items' => ":KMNo" ],
					  ],
					],
					[ 'class' => "col-sm-1" ],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelKKJenis" ],
						  [ 'class' => "col-sm-5", 'items' => ":KKJenis" ],
						  [ 'class' => "col-sm-8" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelJTKK" ],
						  [ 'class' => "col-sm-3", 'items' => ":JTKK" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelJTKM" ],
						  [ 'class' => "col-sm-3", 'items' => ":JTKM" ],
					  ],
					],
					[ 'class' => "col-sm-1" ],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelPosAsal" ],
						  [ 'class' => "col-sm-5", 'items' => ":LokasiAsal" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-4", 'items' => ":LabelKKTerimaDari" ],
						  [ 'class' => "col-sm-12", 'items' => ":KKTerimaDari" ],
					  ],
					],
					[ 'class' => "col-sm-1" ],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelPosTujuan" ],
						  [ 'class' => "col-sm-5", 'items' => ":LokasiTujuan" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-4", 'items' => ":LabelKMDiterimaOleh" ],
						  [ 'class' => "col-sm-12", 'items' => ":KMDiterimaOleh" ],
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelKTMemo" ],
						  [ 'class' => "col-sm-15", 'items' => ":KTMemo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelNominal" ],
						  [ 'class' => "col-sm-4", 'items' => ":Nominal" ],
					  ],
					],
				]
			],
            [ 'class' => "row-no-gutters",
                'items' => [
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'KKPerson'   => 'Penerima',
                                'KKNo'       => 'No Kas Keluar',
                                'KKJenis'    => 'Jenis',
                                'KKLink'     => 'No Transaksi',
                                'KKMemo'     => 'Keterangan',
                                'UserID'     => 'User',
                                'NoGL'       => 'No GL',
                            ],
                        ])
                    ],
			        ['class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction"],
                ]
            ],
			[
				/* label */
				":LabelKTNo"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">No KT</label>',
				":LabelKTTgl"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl KT</label>',
				":LabelKKNo"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">No KK</label>',
				":LabelKKJenis"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				":LabelPosAsal"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">POS Asal</label>',
				":LabelPosTujuan"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">POS Tujuan</label>',
				":LabelKTMemo"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelJTKK"           => \common\components\General::labelGL( $dsTUang[ 'NoGLKK' ], 'JT KK' ),
				":LabelJTKM"           => \common\components\General::labelGL( $dsTUang[ 'NoGLKM' ], 'JT KM' ),
				":LabelKKTerimaDari"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Terima Dari / Diserahkan Oleh</label>',
				":LabelKMDiterimaOleh" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bayar Ke / Diterima Oleh</label>',
				":LabelNominal"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
				":LabelKMNo"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">No KM</label>',
				":KTNo"                => Html::textInput( 'KTNo', $dsTUang[ 'KTNo' ], [ 'class' => 'form-control' , 'readonly' => true, 'tabindex' => '1'] ),
				":KKNo"                => Html::textInput( 'KKNo', $dsTUang[ 'KKNo' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":KMNo"                => Html::textInput( 'KMNo', $dsTUang[ 'KMNo' ], [ 'class' => 'form-control' , 'readonly' => true] ) .
				                          Html::textInput( 'KodeTrans', 'KT', [ 'class' => 'hidden' ] ) .
				                          Html::textInput( 'KMJam', 'KMJam', [ 'class' => 'hidden' ] ) .
				                          Html::textInput( 'KMTgl', 'KMTgl', [ 'class' => 'hidden' ] ) ,
				":KTTgl"               => FormField::dateInput( [ 'name' => 'KKTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'KKTgl' ] ) ] ] ),
				":KTJam"               => FormField::timeInput( [ 'name' => 'KKJam', 'config' => [ 'value' => $dsTUang[ 'KKJam' ] ] ] ),
				":Trans"               => Html::textInput( 'Trans', 'KT - Kas Transfer', [ 'class' => 'form-control', 'readonly' => true ] ),
				":KKJenis"             => Html::textInput( 'KKJenis', $dsTUang[ 'KKJenis' ], [ 'class' => 'form-control' , 'readonly' => true] ),
				":JTKK"                => Html::textInput( 'NoGLKK', $dsTUang[ 'NoGLKK' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":JTKM"                => Html::textInput( 'NoGLKM', $dsTUang[ 'NoGLKM' ], [ 'class' => 'form-control' , 'readonly' => true] ),
				":KMDiterimaOleh"      => Html::textInput( 'KKPerson', $dsTUang[ 'KKPerson' ], [ 'class' => 'form-control', 'tabindex' => '6' ] ),
				":LokasiAsal"          => FormField::combo( 'LokasiKode', [ 'name' => 'LokasiKodeKK', 'config' => [ 'value' => $dsTUang[ 'LokasiKodeKK' ] ],'options' => ['tabindex' => '3'], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":LokasiTujuan"        => FormField::combo( 'LokasiKode', [ 'name' => 'LokasiKodeKM', 'config' => [ 'value' => $dsTUang[ 'LokasiKodeKM' ] ],'options' => ['tabindex' => '5'], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":KTMemo"              => Html::textInput( 'KKMemo', $dsTUang[ 'KKMemo' ], [ 'class' => 'form-control', 'tabindex' => '7' ] ),
				":KKTerimaDari"        => Html::textInput( 'KMPerson', $dsTUang[ 'KMPerson' ], [ 'class' => 'form-control', 'tabindex' => '4' ] ),
				":Nominal"             => FormField::numberInput( [ 'name' => 'KKNominal', 'config' => [ 'value' => $dsTUang[ 'KKNominal' ] ] ] ),

                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),
			],
            [
                'url_main'         => 'ttkk',
                'url_delete'        => Url::toRoute(['ttkk/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
    <?php
$urlPrint = $url[ 'print' ];
$url[ 'jurnal' ] = \yii\helpers\Url::toRoute(['ttkk/kas-transfer-jurnal']);
$urlAdd   = \aunit\models\Ttkk::getRoute( 'Kas Transfer',['action'=>'create'])[ 'create' ];
$urlIndex = \aunit\models\Ttkk::getRoute( 'Kas Transfer', [ 'id' => $id ] )[ 'index' ];
$this->registerJs( <<< JS
    $('#btnPrint').click(function (event) {	      
        $('#form_ttkk_id').attr('action','$urlPrint');
        $('#form_ttkk_id').attr('target','_blank');
        $('#form_ttkk_id').submit();
	  }); 

    $('#btnJurnal').click(function (event) {	
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#form_ttkk_id').attr('action','{$url[ 'jurnal' ]}');
                    $('#form_ttkk_id').submit();                     
                }
            }
        });
    }); 

     $('#btnCancel').click(function (event) {	      
       $('#form_ttkk_id').attr('action','{$url['cancel']}');
       $('#form_ttkk_id').submit();
    });
     
     
     $('#KKTotal-disp').blur(function() {
        $('#KKNominal').utilNumberControl().val($('#KKTotal').utilNumberControl().val());
      }); 
     
       $('#btnSave').click(function (event) {
           let Nominal = parseFloat($('#KKNominal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Silahkan mengisi nominal', size: 'small'});
               return;
           }
        $('#form_ttkk_id').attr('action','{$url['update']}');
        $('#form_ttkk_id').submit();
    }); 
       
        $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });    
    
    $('[name=KKTgl]').utilDateControl().cmp().attr('tabindex', '2');
    $('[name=KKNominal').utilNumberControl().cmp().attr('tabindex', '8');
    
     
JS
);