<?php

use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttkk */
$params                          = '&id=' . $id . '&action=create&KKJenis='.$model->KKJenis;
$cancel = Custom::url( \Yii::$app->controller->id . "/cancel".$params );
$this->title = "Tambah KAS KELUAR - Umum";
//$this->params['breadcrumbs'][] = ['label' => 'Umum', 'url' => $cancel];
//$this->params['breadcrumbs'][] = $this->title;

//$cancel = Url::toRoute(['ttkkhd/kas-keluar-umum']);
//$this->title = "Tambah KAS KELUAR - Umum";
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Umum', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = $this->title;

?>
<div class="ttkk-create">
    <?= $this->render('_kas-keluar-umum-form', [
        'model'    => $model,
        'dsTUang'  => $dsTUang,
        'JoinData' => $JoinData,
        'id'       => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/kas-keluar-umum-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttkkitcoa/index', 'action' => 'create' ] ),
        ]
    ]) ?>
</div>
