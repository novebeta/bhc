<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttkk */
$params = '&id=' . $id . '&action=create&KKJenis=' . $model->KKJenis;
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel' . $params );
$this->title                     = "Tambah - $title";
//$this->params[ 'breadcrumbs' ][] = [ 'label' => $title, 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="ttkk-create">
	<?= $this->render( isset( $templateForm ) ? $templateForm : '_form', [
		'model'    => $model,
		'dsTUang'  => $dsTUang,
		'JoinData' => $JoinData,
		'id'       => $id,
		'url'      => [
			'update' => Custom::url( \Yii::$app->controller->id . "/$view-update" . $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . "/$view" ),
			'cancel' => $cancel,
		]
	] ) ?>
</div>
