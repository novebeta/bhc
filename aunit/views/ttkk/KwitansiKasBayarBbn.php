<?php
use aunit\assets\AppAsset;
use aunit\components\TdUi;
use aunit\models\Ttkk;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = 'KK -> Kwitansi Kas Bayar BBN';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'KKNo'       => 'No KK',
		'KKLink'     => 'Link',
		'KKPerson'   => 'Diterima Oleh',
		'KKJenis'    => 'Jenis',
		'KKMemo'     => 'Keterangan',
		'NoGL'       => 'No Gl',
		'LokasiKode' => 'Lokasi',
		'UserID'     => 'UserID',
	],
	'cmbTgl'    => [
		'KKTgl' => 'Tanggal KK',
	],
	'cmbNum'    => [
		'KKNominal' => 'Nominal',
	],
	'sortname'  => "KKTgl",
	'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( TdUi::mainGridJs( Ttkk::class,
	'KK -> Kwitansi Kas Bayar BBN', [
		'btnAdd_Disabled'    => true,
		'btnDelete_Disabled' => true,
		'btnUpdate_Disabled' => true,
		'url_view'           => \yii\helpers\Url::toRoute( [ 'ttkk/kas-keluar-bayar-hutang-bbn-update', 'mode' => 'kwitansi-kas-bayar-bbn', 'action' => 'view' ] ),
	] ), \yii\web\View::POS_READY );
