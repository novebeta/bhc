<?php
use aunit\assets\AppAsset;
use common\components\Custom;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'KAS KELUAR - Subsidi Dealer 2';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
            'KKPerson'   => 'Penerima',
	        'KKNo'       => 'No Kas Keluar',
            'KKJenis'    => 'Jenis',
            'KKLink'     => 'No Transaksi',
            'KKMemo'     => 'Keterangan',
            'UserID'     => 'User',
            'NoGL'       => 'No GL',

	],
	'cmbTgl'    => [
            'KKTgl'      => 'Tanggal Kas Keluar',
	],
	'cmbNum'    => [
            'KKNominal'  => 'Nominal',
	],
	'sortname'  => "KKTgl",
	'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttkk::className(),
	'Kas Keluar Subsidi Dealer 2' , [
        'url_add'    => \yii\helpers\Url::toRoute( [ 'ttkk/kas-keluar-subsidi-dealer-2-create', 'action' => 'create' ] ),
        'url_update' => \yii\helpers\Url::toRoute( [ 'ttkk/kas-keluar-subsidi-dealer-2-update' , 'action' => 'update' ] ),
        'url_delete' => \yii\helpers\Url::toRoute( [ 'ttkk/delete'] ),
    ] ), \yii\web\View::POS_READY );
