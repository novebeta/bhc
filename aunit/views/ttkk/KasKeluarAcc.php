<?php
use aunit\assets\AppAsset;
use aunit\models\Ttkk;
use common\components\Custom;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$urlCreateAcc     = Url::toRoute( [ 'ttkk/kas-keluar-acc-create' ] );
$this->title = 'Kas Acc';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
    'cmbTxt'    => [
        'KKNo'              => 'No Kas Keluar',
        'KKMemo'            => 'Keterangan',
        'KKPerson'          => 'Penerima',
        'KKLink'            => 'No Transaksi',
        'UserID'            => 'User',
        'Dealer'            => 'Dealer',
        'LokasiKode'        => 'LokasiKode',

    ],
    'cmbTgl'    => [
        'KKTgl'      => 'Tanggal Kas Keluar',
    ],
    'cmbNum'    => [
        'KKNominal'  => 'Nominal',
    ],
    'sortname'  => "KKTgl",
    'sortorder' => 'desc'
];

AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( <<< JS
    var priorDate = new Date(new Date().setDate((new Date()).getDate() - 30));
    $('#filter-tgl-val1-disp').kvDatepicker('update', priorDate).trigger('change');
    window.gridParam_jqGrid = {
        // multiselect:true,
        // cellEdit : true,
        onSelectRow: function(selRowId){
            var rowData = $(this).jqGrid("getRowData", selRowId);
            var grid = $(this);
            $.ajax({
                type: "POST",
                url: '$urlCreateAcc',
                data: rowData,
                success: function(returnedData) {
                    let response = JSON.parse(returnedData);
                    $.notify({message: response.keterangan},{
                        type: (response.status == 0)? 'success' : 'warning',
                    });
                    grid.trigger("reloadGrid");
                },
            });
        },
    }
    window.grid_before_init_jqGrid = function(grid,params){
        params.colModel.splice(1, 0, {width:40,hidden:false,label:"Tolak",name:'Tolak',formatter : function ( cellvalue, options, rowObject ){
            return '<input type="checkbox" onchange="actionKasKeluar(\'Tolak\',\''+rowObject.rowId+'\')" '+(cellvalue == '1' ? 'checked' : '')+'>';
        }});
        params.colModel.splice(1, 0, {width:40,hidden:false,label:"Acc",name:'ACC',formatter : function ( cellvalue, options, rowObject ){
            return '<input type="checkbox" onchange="actionKasKeluar(\'Acc\',\''+rowObject.rowId+'\')" '+(cellvalue == '1' ? 'checked' : '')+'>';
        }});
    }

    window.actionKasKeluar = function(action, cellValue) {
        var actionText = 'menolak';
        if(action == 'Acc'){
            actionText = 'menyetujui';
        }
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah Anda "+actionText+" pengajuan Kas Keluar ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> YA',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> TIDAK',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if(result) {
                    var rowData = $('#jqGrid').jqGrid("getRowData", cellValue);
                    rowData.Action = action;
                    $.ajax({
                        type: "POST",
                        url: '$urlCreateAcc',
                        data: rowData,
                        success: function(returnedData) {
                            let response = JSON.parse(returnedData);
                            $.notify({message: response.keterangan},{
                                type: (response.status == 0)? 'success' : 'warning',
                            });
                            $('#jqGrid').trigger("reloadGrid");
                        },
                    });
                }else{
                    $('#jqGrid').trigger("reloadGrid");
                }
            }
        });
    }
JS
);
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttkk::class,
	'Kas Acc' , [
        'colGrid'    => Ttkk::colGridKasAcc(),
        'url'        => \yii\helpers\Url::toRoute( [ 'ttkk/kas-keluar-acc-index' ]),
        'url_update' => Url::toRoute( [ 'ttkk/kas-keluar-umum-update','action' => 'update','source'=>'KasKeluarAcc' ] ),
        'btnView_Disabled'=> true,
        'btnDelete_Disabled'=> true,
    ] ), \yii\web\View::POS_READY );
