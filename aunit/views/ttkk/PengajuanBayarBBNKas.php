<?php
use aunit\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = 'Pengajuan Bayar BBN Kas';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'KKPerson' => 'Penerima',
		'KKNo'     => 'No Kas Keluar',
		'KKJenis'  => 'Jenis',
		'KKLink'   => 'No Transaksi',
		'KKMemo'   => 'Keterangan',
		'UserID'   => 'User',
		'NoGL'     => 'No GL',
	],
	'cmbTgl'    => [
		'KKTgl' => 'Tanggal Kas Keluar',
	],
	'cmbNum'    => [
		'KKNominal' => 'Nominal',
	],
	'sortname'  => "KKTgl",
	'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttkk::class,
	'Pengajuan Bayar BBN via Kas', [
		'url_update' => Url::toRoute( [ 'ttkk/pengajuan-bayar-bbn-kas-update','JenisBBN'=>'Pengajuan', 'action' => 'update' ] ),
		'url_add'    => Url::toRoute( [ 'ttkk/pengajuan-bayar-bbn-kas-create','JenisBBN'=>'Pengajuan', 'action' => 'create' ] ),
		'url_delete' => Url::toRoute( [ 'ttkk/delete', 'JenisBBN' => 'Pengajuan' ] ),
	] ), \yii\web\View::POS_READY );
