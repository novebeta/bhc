<?php
use common\components\Custom;
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttkk */

$params                          = '&id=' . $id . '&action=UpdateEdit';
$cancel                          = Custom::url( \Yii::$app->controller->id . "/kas-transfer-cancel".$params );
$this->title                     = "Edit - Kas Transfer : "  . $dsTUang['KTNo'];
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Kas Transfer', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = 'Edit';

//$params                          = '&id=' . $id . '&action=update';
//$cancel                          = Custom::url( \Yii::$app->controller->id . '/kas-transfer' );
//$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode).'- Kas Transfer : ' . $model->KKNo;
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Kas Transfer', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = 'Edit';
?>
<div class="ttkkhd-update">
    <?= $this->render('_kasTransferform', [
        'dsTUang' => $dsTUang,
        'id'=>$id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/kas-transfer-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ '' ] ),
        ]
//        'url'    => [
//            'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
//            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
//            'cancel' => $cancel,
//        ]
    ]) ?>

</div>
