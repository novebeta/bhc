<?php
/* @var $this yii\web\View */

/* @var $dataProvider yii\data\ActiveDataProvider */

use aunit\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
$this->title = 'Import Invoice';
$this->params['breadcrumbs'][] = $this->title;
$arr = [
    'cmbTxt' => [
        'dealerId' => 'dealerId',
        'noShippingList' => 'noShippingList',
        'poId' => 'poId',
    ],
    'cmbTxt2' => [
        'noShippingList' => 'noShippingList',
        'mainDealerId' => 'mainDealerId',
        'dealerId' => 'dealerId',
        'noInvoice' => 'noInvoice',
        'statusShippingList' => 'statusShippingList'
    ],
    'cmbTgl' => [],
    'cmbNum' => [],
    'sortname' => "poId",
    'sortorder' => 'desc'
];
$this->registerJsVar('setcmb', $arr);
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render('../_search'); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs(<<< JS
    window.gridParam_jqGrid = {
        readOnly: true,
        rownumbers: true,
        rowNum: 10000,
        pgbuttons: false,
        pginput: false,
        viewrecords: false,
        rowList: [],
        pgtext: ""
    }
    $('#filter-tgl-val1').change(function() {
        let _tgl2 = new Date($(this).val());      
        _tgl2.setDate(_tgl2.getDate() + 7);
        $('[name=tgl2]').val(_tgl2.toISOString().slice(0, 10));
	    $('#filter-tgl-val2-disp').datepicker('update', _tgl2);
	    $('#filter-tgl-val2-disp').trigger('change');
    });
JS
);

$this->registerJs(\aunit\components\TdUi::mainGridJs(\aunit\models\Ttfb::class,
    'Import Penerimaan Barang', [
        'colGrid' => $colGrid,
        'url' => Url::toRoute(['ttfb/import-items']),
        'mode' => isset($mode) ? $mode : '',
    ]), \yii\web\View::POS_READY);
$urlDetail = Url::toRoute(['ttfb/import-details']);
$this->registerJs(<<< JS
jQuery(function ($) {
    if($('#jqGridDetail').length) {
        $('#jqGridDetail').utilJqGrid({
            url: '$urlDetail',
            height: 240,
            readOnly: true,
            rownumbers: true,
            rowNum: 1000,
            colModel: [
               	{
	                name: 'noGoodsReceipt',
	                label: 'noGoodsReceipt',
	                width: 100,
	            },
                {
                    name: 'kodeTipeUnit',
                    label: 'Type Unit',
                    width: 100
                },
                {
                    name: 'Type',
                    label: 'Type',
                    width: 150
                },
	            {
	                name: 'kodeWarna',
	                label: 'Kode Warna',
	                width: 100,
	            },
	            {
	                name: 'Warna',
	                label: 'Warna',
	                width: 100,
	            },
	            {
	                name: 'noMesin',
	                label: 'No Mesin',
	                width: 160,
	            },
	            {
	                name: 'noRangka',
	                label: 'No Rangka',
	                width: 160,
	            },
	            {
	                name: 'kuantitasTerkirim',
	                label: 'kuantitas Terkirim',
	                width: 150,
	            },
	            {
	                name: 'kuantitasDiterima',
	                label: 'kuantitas Diterima',
	                width: 150,
	            },
	            {
	                name: 'statusRFS',
	                label: 'status RFS',
	                width: 100,
	            },
	            {
	                name: 'poId',
	                label: 'po Id',
	                width: 100,
	            },
	            {
	                name: 'kelengkapanUnit',
	                label: 'kelengkapan Unit',
	                width: 150,
	            },
	            {
	                name: 'docNRFSId',
	                label: 'docNRFSId',
	                width: 100,
	            },
	            {
	                name: 'createdTime',
	                label: 'createdTime',
	                width: 150,
	            },
            ],
	        multiselect: gridFn.detail.multiSelect(),
	        onSelectRow: gridFn.detail.onSelectRow,
	        onSelectAll: gridFn.detail.onSelectAll,
	        ondblClickRow: gridFn.detail.ondblClickRow,
	        loadComplete: gridFn.detail.loadComplete,
	        listeners: {
                afterLoad: function(grid, response) {
                    $("#cb_jqGridDetail").trigger('click');
                }
            }
        }).init().setHeight(gridFn.detail.height);
    }
});
JS, \yii\web\View::POS_READY);