<?php
use aunit\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = $title;
$this->params[ 'breadcrumbs' ][] = $this->title;
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php


switch ($_GET['r']){
	case 'ttfb/select-bank-keluar-bayar-hutang-unit':
	case 'ttfb/select-kas-keluar-bayar-hutang-unit':
		$this->registerJs(<<<JS

        window.gridParam_jqGrid = {        
            cellEdit : true,                
            cellsubmit:'clientArray',  
        };

JS);
		break;
}

$this->registerJs( \aunit\components\TdUi::mainGridJs(
        \aunit\models\Ttfb::className(),
        $title, $options ),	\yii\web\View::POS_READY );

switch ($_GET['r']) {
	case 'ttfb/select-kas-keluar-bayar-hutang-unit':
		$this->registerJs( <<<JS
        var grid = $('#jqGrid');
        grid.bind("jqGridAfterSaveCell", function (e, rowid, orgClickEvent) {
            var rowData = grid.jqGrid('getRowData', rowid);
            var NilaiAwal = parseFloat(rowData.FBTotal);
            var Terbayar = parseFloat(rowData.Terbayar);
            var Bayar = parseFloat(rowData.KKBayar);
            var Sisa = NilaiAwal - Terbayar - Bayar;
            if (Sisa < 0) return;
            grid.jqGrid("setCell", rowid, "Sisa", Sisa);
            grid.jqGrid('setSelection', rowid, false);
        });   
JS
		);
		break;
	case 'ttfb/select-bank-keluar-bayar-hutang-unit':
		$this->registerJs( <<<JS
        var grid = $('#jqGrid');
        grid.bind("jqGridAfterSaveCell", function (e, rowid, orgClickEvent) {
            var rowData = grid.jqGrid('getRowData', rowid);
            var NilaiAwal = parseFloat(rowData.FBTotal);
            var Terbayar = parseFloat(rowData.Terbayar);
            var Bayar = parseFloat(rowData.BKBayar);
            var Sisa = NilaiAwal - Terbayar - Bayar;
            if (Sisa < 0) return;
            grid.jqGrid("setCell", rowid, "Sisa", Sisa);
            grid.jqGrid('setSelection', rowid, false);
        });   
JS
		);
		break;
}