<?php
use aunit\components\FormField;
use aunit\components\Menu;
use common\components\Custom;
use common\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttfb */
/* @var $form yii\widgets\ActiveForm */
\aunit\assets\AppAsset::register( $this );
?>
    <div class="ttfb-form">
		<?php
		$format = <<< SCRIPT
function formattin(result) { //console.log(result);
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-6">' + result.id + '</div>' +
           '<div class="col-xs-18">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
		$escape = new JsExpression( "function(m) { return m; }" );
		$this->registerJs( $format, \yii\web\View::POS_HEAD );
		$form = ActiveForm::begin( ['id' => 'form_ttfb_id', 'action' => $url[ 'update' ]] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelFBNo" ],
						  [ 'class' => "col-sm-5", 'items' => ":FBNo" ],
						  [ 'class' => "col-sm-2", 'items' => ":FBTgl_" ],
						  [ 'class' => "col-sm-2", 'items' => ":FBJam" ],
						  [ 'class' => "col-sm-2" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelSupKode" ],
						  [ 'class' => "col-sm-9", 'items' => ":SupKode" ],
					  ],
					],
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelSSNo" ],
						  [ 'class' => "col-sm-4", 'items' => ":SSNo" ],
						  [ 'class' => "col-sm-1", 'items' => ":BtnSearchFB" ],
						  [ 'class' => "col-sm-2", 'items' => ":SSTgl" ],
						  [ 'class' => "col-sm-4" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelFBTermin" ],
						  [ 'class' => "col-sm-2", 'items' => ":FBTermin" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelFBTglTempo" ],
						  [ 'class' => "col-sm-4", 'items' => ":FBTglTempo" ],
					  ],
					],
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelDONo" ],
						  [ 'class' => "col-sm-5", 'items' => ":DONo" ],
						  [ 'class' => "col-sm-2", 'items' => ":DOTgl" ],
						  [ 'class' => "col-sm-4" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelFPNo" ],
						  [ 'class' => "col-sm-7", 'items' => ":FPNo" ],
						  [ 'class' => "col-sm-2", 'items' => ":FPTgl" ],
					  ],
					],
					[
						'class' => "row col-md-24",
						'style' => "margin-bottom: 6px;",
						'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
					],
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-3", 'items' => ":LabelJumlahUnit" ],
						  [ 'class' => "col-sm-4", 'items' => ":JumlahUnit" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-3", 'items' => ":LabelFBPtgMD" ],
						  [ 'class' => "col-sm-4", 'items' => ":FBPtgMD" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-3", 'items' => ":LabelSubTotal" ],
						  [ 'class' => "col-sm-5", 'items' => ":SubTotal" ],
					  ],
					],
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-3", 'items' => ":LabelFBLunas" ],
						  [ 'class' => "col-sm-4", 'items' => ":FBLunas" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-3", 'items' => ":LabelFBPtgLain" ],
						  [ 'class' => "col-sm-4", 'items' => ":FBPtgLain" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-3", 'items' => ":LabelPotongan" ],
						  [ 'class' => "col-sm-5", 'items' => ":Potongan" ],
					  ],
					],
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-3", 'items' => ":LabelFBPosting" ],
						  [ 'class' => "col-sm-4", 'items' => ":FBPosting" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-3", 'items' => ":LabelFBExtraDisc" ],
						  [ 'class' => "col-sm-4", 'items' => ":FBExtraDisc" ],
						  [ 'class' => "col-sm-1" ],
                          [ 'class' => "col-sm-3", 'items' => ":LabelPPN" ],
                          [ 'class' => "col-sm-5", 'items' => ":PPN" ],
					  ],
					],
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-3", 'items' => ":LabelFBMemo" ],
						  [ 'class' => "col-sm-21", 'items' => ":FBMemo" ],
					  ],
					],
				]
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
                  ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                      'items'  => [
                          ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":NoGL"],
                          ['class' => "col-sm-4 pull-right", 'items' => ":LabelNoGL"],
                      ],
                  ],
                  [
                      'class' => "col-md-12 pull-left",
                      'items' => $this->render( '../_nav', [
                          'url'=> $_GET['r'],
                          'options'=> [
                              'ttfb.FBNo'      => 'No Faktur Beli',
                              'ttfb.SSNo'      => 'No Surat Jalan Supplier',
                              'ttfb.SupKode'   => 'Supplier',
                              'SupNama'        => 'Nama Supplier',
                              'ttfb.FBMemo'    => 'Keterangan',
                              'ttfb.NoGL'      => 'No GL',
                              'ttfb.FBLunas'   => 'Lunas',
                              'ttfb.UserID'    => 'User ID',
                              'ttfb.FPNo'      => 'No FP [Faktur Pajak]',
                              'ttfb.FBPosting' => 'Posting',
                          ],
                      ])
                  ],
                  ['class' => "col-md-12 pull-right",
				    'items' => [
					    [ 'class' => "pull-right", 'items' => " :btnJurnal :btnPrint :btnAction" ]
				    ]
				  ]
			  ]
			],
			[
				/* label */
				":LabelFBNo"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor</label>',
				":LabelSupKode"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Supplier</label>',
				":LabelSSNo"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Surat Jalan</label>',
				":LabelFBTermin"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Termin</label>',
				":LabelFBTglTempo"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jatuh Tempo</label>',
				":LabelDONo"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">DO</label>',
				":LabelFPNo"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Faktur Pajak</label>',
				":LabelJumlahUnit"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jumlah Unit</label>',
				":LabelFBPtgLain"   => '<label class="control-label" style="margin: 0; padding: 6px 0;" id="lblFBPtgLain">Pendapatan Unit</label>',
				":LabelSubTotal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sub Total</label>',
				":LabelFBLunas"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Lunas</label>',
				":LabelFBPtgMD"     => '<label class="control-label" style="margin: 0; padding: 6px 0;" id="lblFBPSS">Potongan Program</label>',
				":LabelPPN"         => '<label class="control-label" style="margin: 0; padding: 6px 0;" id="lblppn">PPN</label>'.
                                        '<label class="control-label" style="margin: 0; padding: 6px 0;">Total &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</label>',
				":LabelPotongan"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Potongan</label>',


				":LabelFBPosting"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Posting</label>',
				":LabelFBExtraDisc" => '<label class="control-label" style="margin: 0; padding: 6px 0;" id="lblFBExtraDisc">Extra Discount</label>'.
				                       '<label class="control-label" style="margin: 0; padding: 6px 0;" id="lblFBPtgMD">Titipan Potongan MD</label>',
				":LabelFBMemo"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelNoGL"        => \common\components\General::labelGL($dsBeli[ 'NoGL' ]),
				/* component */
				":FBNo"             => Html::textInput( 'FBNoView', $dsBeli[ 'FBNoView' ], [ 'class' => 'form-control', 'maxlength' => 30, 'tabindex'=>'1' ] ) .
				                       Html::textInput( 'FBNo', $dsBeli[ 'FBNo' ], [ 'class' => 'hidden' ] ) .
				                       Html::textInput( 'FBNoLama', $dsBeli[ 'FBNo' ], [ 'class' => 'hidden' ] ).
				                       Html::textInput( 'SaveMode', 'ALL', [ 'class' => 'hidden' ] ),
				":SSNo"             => Html::textInput( 'SSNo', $dsBeli[ 'SSNo' ], [ 'class' => 'form-control','readonly' => true, 'maxlength' => 30 ] ),
				":DONo"             => Html::textInput( 'DONoView', str_replace('=','-',$dsBeli[ 'FBNo' ]), [ 'class' => 'form-control', 'readonly' => true, 'maxlength' => 30 ] ) .
				                       Html::textInput( 'DONo', $dsBeli[ 'FBNo' ], [ 'class' => 'hidden' ] ),
				":FPNo"             => Html::textInput( 'FPNo', $dsBeli[ 'FPNo' ], [ 'class' => 'form-control', 'maxlength' => 30 ] ),
				":FBMemo"           => Html::textInput( 'FBMemo', $dsBeli[ 'FBMemo' ], [ 'class' => 'form-control', 'maxlength' => 100 ] ),
				":NoGL"             => Html::textInput( 'NoGL', $dsBeli[ 'NoGL' ], [ 'class' => 'form-control', 'readonly' => true, 'maxlength' => 10 ] ),
				":FBLunas"          => Html::textInput( 'FBLunas', $dsBeli[ 'FBLunas' ], [ 'class' => 'form-control', 'readonly' => true, 'maxlength' => 5 ] ),
				":FBPosting"        => Html::textInput( 'FBPosting', $dsBeli[ 'FBPosting' ], [ 'class' => 'form-control', 'readonly' => true, 'maxlength' => 15 ] ),
				":FBTgl_"           => FormField::dateInput( [ 'name' => 'FBTgl', 'config' => [ 'id'=>'FBTgl','value' => $dsBeli[ 'FBTgl' ], 'disabled' => !Menu::showOnlyHakAkses(['Admin','Auditor']) ]  ] ),
				":FBJam"            => FormField::timeInput( [ 'name' => 'FBJam', 'config' => [ 'value' => $dsBeli[ 'FBJam' ] ] ] ),
				":SSTgl"            => FormField::dateInput( [ 'name' => 'SSTgl', 'config' => [ 'id'=>'SSTgl','value' => $dsBeli[ 'SSTgl' ] ] ] ),
				":DOTgl"            => FormField::dateInput( [ 'name' => 'DOTgl', 'config' => [ 'id'=>'DOTgl', 'value' => $dsBeli[ 'DOTgl' ] ] ] ),
				":FBTglTempo"       => FormField::dateInput( [ 'name' => 'FBTglTempo', 'config' => [ 'value' => $dsBeli[ 'FBTglTempo' ] ] ] ),
				":FPTgl"            => FormField::dateInput( [ 'name' => 'FPTgl', 'config' => [ 'value' => $dsBeli[ 'FPTgl' ] ] ] ),
				":FBTermin"         => FormField::integerInput( [ 'name' => 'FBTermin', 'config' => [ 'value' => $dsBeli[ 'FBTermin' ] ] ] ),
				":JumlahUnit"       => FormField::integerInput( [ 'name' => 'FBJum', 'config' => ['id'=>'FBJum', 'value' => $dsBeli[ 'FBJum' ], 'readonly' => true ] ] ),
				":FBPtgLain"        => FormField::decimalInput( [ 'name' => 'FBPtgLain', 'config' => ['id'=>'FBPtgLain', 'value' => $dsBeli[ 'FBPtgLain' ] ,'displayOptions'=>['class'=>'hitung']] ] ),
				":FBPtgMD"          => FormField::decimalInput( [ 'name' => 'FBPSS', 'config' => [ 'id'=>'FBPSS','value' => $dsBeli[ 'FBPSS' ],'displayOptions'=>['class'=>'hitung'] ] ] ),
				":FBExtraDisc"      => FormField::decimalInput( [ 'name' => 'FBExtraDisc', 'config' => [ 'id'=>'FBExtraDisc','value' => $dsBeli[ 'FBExtraDisc' ],'displayOptions'=>['class'=>'hitung'] ] ] ).
				                       FormField::decimalInput( [ 'name' => 'FBPtgMD', 'config' => [ 'id'=>'FBPtgMD','value' => $dsBeli[ 'FBPtgMD' ],'displayOptions'=>['class'=>' hitung', 'style'=>'margin-top:2px'], ] ] ),
				":SubTotal"         => FormField::decimalInput( [ 'name' => 'FBSubTotal', 'config' => ['id'=>'FBSubTotal', 'value' => $dsBeli[ 'FBSubTotal' ], 'readonly' => true ] ] ),
				":Potongan"         => FormField::decimalInput( [ 'name' => 'FBPtgTotal', 'config' => ['id'=>'FBPtgTotal', 'value' => $dsBeli[ 'FBPtgTotal' ], 'readonly' => true ] ] ),

				":PPN"              => FormField::decimalInput( [ 'name' => 'FBPPNm', 'config' => ['id'=>'FBPPN','value' => $dsBeli[ 'FBPPNm' ], 'readonly' => true ] ] ).
                                       FormField::decimalInput( [ 'name' => 'FBTotal', 'config' => [ 'id'=>'FBTotal','value' => $dsBeli[ 'FBTotal' ],'displayOptions'=>['class'=>' hitung', 'style'=>'margin-top:2px'], 'readonly' => true ] ] ),
				":SupKode"          => FormField::combo( 'SupKode', [ 'config' => [ 'value' => $dsBeli[ 'SupKode' ] ],'options' => ['tabindex'=>'3'], 'extraOptions' => [ 'altLabel' => [ 'SupNama' ] ] ] ),
				/* button */
                ":BtnSearchFB"     => Html::button( '<i class="glyphicon glyphicon-search"></i>', [ 'class' => 'btn btn-default ' . Custom::viewClass(), 'id' => 'btnSS' ] ),
                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
                ":btnImport"      => Html::button('<i class="fa fa-arrow-circle-down"><br><br>Import</i>   ', ['class' => 'btn bg-navy ', 'id' => 'btnImport', 'style' => 'width:60px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),
			],[
                '_akses' => 'Faktur Beli',
                'url_main' => 'ttfb',
                'url_id' => $_GET['id'] ?? '',
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlAdd   = Url::toRoute( [ 'ttfb/create','action' => 'create'  ] );
$urlDetail     = $url[ 'detail' ];
$urlPrint      = $url[ 'print' ];
$urlJurnal      = Url::toRoute(['ttfb/jurnal']);
$urlIndex      = Url::toRoute(['ttfb/index']);
$urlMotorWarna = \common\components\Custom::url( 'tdmotorwarna/find' );
$dataMotorType = json_encode( \aunit\models\Tdmotortype::find()->comboSelect2() );
$readOnly = ( ($_GET['action'] ?? '') == 'view' ? 'true' : 'false' );
$urlUpdate = $url[ 'update' ];
$urlFB     = Url::toRoute( [ 'ttss/select', 'jenis' => 'FB' ] );
//$urlImport        = Url::toRoute( [ 'ttfb/import','tgl1'    => Yii::$app->formatter->asDate( '-5 day', 'yyyy-MM-dd') ] );
$urlImport        = Url::toRoute( [ 'ttfb/import','tgl1'    => Yii::$app->formatter->asDate( 'now', 'yyyy-MM-dd') ] );
$this->registerJsVar( '__urlFB', $urlFB );
$this->registerJsVar('__model',$dsBeli);
$this->registerJsVar('_company',Yii::$app->session->get( '_company'));
$this->registerJs( <<< JS

    window.getUrlFB = function(){
        let SSNo = $('input[name="SSNo"]').val();
        return __urlFB + ((SSNo === '' || SSNo === '--') ? '':'&check=off&cmbTxt1=SSNo&txt1='+SSNo);
    } 

    function Hitung(){
        let subTotal = $('#detailGrid').jqGrid('getCol','FBHarga',false,'sum');
        $('#FBSubTotal').utilNumberControl().val(subTotal);
        $('#FBPtgTotal').utilNumberControl().val( $('#FBPSS').utilNumberControl().val() + $('#FBPtgLain').utilNumberControl().val() +
            $('#FBExtraDisc').utilNumberControl().val() + $('#FBPtgMD').utilNumberControl().val());
        
        if (_company.nama.trim() === "CV. ANUGERAH PERDANA MOROWALI" || _company.nama.trim() === "CV. ANUGERAH PERDANA 1" || _company.nama.trim() === "CV. ANUGERAH PERDANA 2" || _company.nama.trim() === "CV. ANUGERAH PERDANA 3" || _company.nama.trim() === "CV. ANUGERAH PERDANA 4" || _company.nama.trim() === "CV. ANUGERAH PERDANA 5" || _company.nama.trim() === "CV. ANUGERAH PERDANA 6" || _company.nama.trim() === "CV. ANUGERAH PERDANA 7" || _company.nama.trim() === "CV. ANUGERAH PERDANA 8" || _company.nama.trim() === "CV. ANUGERAH PERDANA PARIGI"){
        $('#FBPPN').utilNumberControl().val(Math.round(($('#FBSubTotal').utilNumberControl().val()) * (11/100)));
        
        $('#FBTotal').utilNumberControl().val($('#FBSubTotal').utilNumberControl().val()  - ($('#FBPSS').utilNumberControl().val() +
            $('#FBPtgLain').utilNumberControl().val() +$('#FBExtraDisc').utilNumberControl().val() + $('#FBPtgMD').utilNumberControl().val()));    
        }else{
            $('#FBPPN').utilNumberControl().val(Math.round(
                ($('#FBSubTotal').utilNumberControl().val() - ($('#FBPSS').utilNumberControl().val() +
            $('#FBPtgLain').utilNumberControl().val() +$('#FBExtraDisc').utilNumberControl().val() + $('#FBPtgMD').utilNumberControl().val())) * (11/100)
            ));
        
        $('#FBTotal').utilNumberControl().val($('#FBSubTotal').utilNumberControl().val() + $('#FBPPN').utilNumberControl().val() - ($('#FBPSS').utilNumberControl().val() +
            $('#FBPtgLain').utilNumberControl().val() +$('#FBExtraDisc').utilNumberControl().val() + $('#FBPtgMD').utilNumberControl().val()));
            
        }
        
        
        
    }
    $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail', 
        height: 162,
        extraParams: {
            FBNo: '$model->FBNo'
        },
        onSaveExtraParams: {
            header: btoa($('#form_ttfb_id').serialize())
        },
        loadonce:true,
        rownumbers: true,
        rowNum: 1000,
        readOnly: $readOnly,  
        colModel: [
            { template: 'actions' },
            
            {
                name: 'MotorType',
                label: 'Type',
                editable: true,
                width: 165,
                editor: {
                    type: 'select2',
                    data: $dataMotorType,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    $('[id="'+$(this).attr('rowid')+'_'+'MotorWarna'+'"]')
                            .utilSelect2()
                            .loadRemote('$urlMotorWarna', { mode: 'combo', value: 'MotorWarna', label: ['MotorWarna'], 
                            condition: 'MotorType = :MotorType', params:{ ':MotorType': this.value},allOption:false }, true);   
						    $('[id="'+$(this).attr('rowid')+'_'+'MotorTahun'+'"]').val(new Date().getFullYear());
						    $('[id="'+$(this).attr('rowid')+'_'+'MotorNoMesin'+'"]').val(data.MotorNoMesin);
						    $('[id="'+$(this).attr('rowid')+'_'+'KodeAHM'+'"]').val(data.KodeAHM);
                            $('[id="'+$(this).attr('rowid')+'_'+'MotorNoRangka'+'"]').val(data.MotorNoRangka);
                            $('[id="'+$(this).attr('rowid')+'_'+'FBHarga'+'"]').utilNumberControl().val(data.MotorHrgBeli);
                            $('[id="'+$(this).attr('rowid')+'_'+'SSNo'+'"]').val('--');
						}
                    },
                }
            },
            {
                name: 'MotorTahun',
                label: 'Tahun',
                width: 60,
                editable: true, 
                template: 'tahun',
                editoptions:{
                    maxlength: "8"
                },
            },
            {
                name: 'MotorWarna',
                label: 'Warna',
                editable: true,
                width: 80,
                editor: {
                    type: 'select2',
                    editorConfig:{
                        tags: true                    
                    }
                }
            },
            {
                name: 'MotorNoMesin',
                label: 'NoMesin',
                width: 160,
                editable: true, 
                editoptions:{
                    maxlength:"12",
                    style: "text-transform:uppercase"
                },
                editrules:{
                    custom:true, 
                    custom_func: function(value) {
                        // if (value.length < 12)
                        //    return [false,"No Mesin kurang dari 12 karakter"];
                        // else 
                           return [true,""];                        
                    }
                }
            },
            {
                name: 'MotorAutoN',
                label: 'NoMesin',
                width: 160,
                hidden: true ,
                editable: true
            },
            {
                name: 'MotorNoRangka',
                label: 'NoRangka',
                width: 160,
                editable: true,
                editoptions:{
                    maxlength:"17",
                    style: "text-transform:uppercase"
                },                
                editrules:{
                    custom:true, 
                    custom_func: function(value) {
                        // if (value.length < 17)
                        //    return [false,"No Rangka kurang dari 17 karakter"];
                        // else 
                           return [true,""];                        
                    }
                }
            },
            {
                name: 'SSNo',
                label: 'No Surat Jalan',
                width: 105,
                editable: true, 
                editoptions:{
                    readOnly:true,
                    maxlength:"30"
                },               
            },
            {
                name: 'FBHarga',
                label: 'Hrg Beli',
                width: 90,
                template: 'money',
                editable: true
            },
            {
                name: 'KodeAHM',
                label: 'Kode AHM',
                width: 70,
                editable: true
            },
        ],
        listeners: {
            afterLoad: function(grid, response) {
                Hitung();
                let count = $('#detailGrid').getGridParam("reccount");
                $('#FBJum').utilNumberControl().val(count);
            }
        }
    }).init().fit($('.box-body')).load({url: '$urlDetail'});

    $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowId, orgClickEvent) {
        
        if (orgClickEvent.extraparam.oper === 'add'){
            var cmbType = $('[id="'+rowId+'_'+'MotorType'+'"]');
            var data = cmbType.select2('data')[0];
            cmbType.val(data.MotorType).trigger('change').trigger({
                type: 'select2:select',
                params: {
                    data: data
                }
            });
        }else{
            window.rowData = $('#detailGrid').utilJqGrid().getData(rowId);  
            
            var cmbMotorType = $('[id="'+rowId+'_'+'MotorType'+'"]');
            var cmbWarna = $('[id="'+rowId+'_'+'MotorWarna'+'"]');
            cmbWarna.utilSelect2()
                .loadRemote('$urlMotorWarna', { mode: 'combo', value: 'MotorWarna', label: ['MotorWarna'], 
                condition: 'MotorType = :MotorType', params:{ ':MotorType': window.rowData.data.MotorType},allOption:false }, true,function() {
                        cmbWarna.val(window.rowData.data.MotorWarna);
                        cmbWarna.trigger('change');
                });
            if (rowData.data.SSNo !== '' && rowData.data.SSNo !== '--'){
                cmbWarna.prop('disabled',true);
                cmbMotorType.prop('disabled',true);
            }
            
        }     
        
    });
    
    function hitungTerm(){
         var tgl1 = $('[name=FBTgl]');
         var tgl2 = $('[name=FBTglTempo]').utilDateControl().cmp();
         var term = parseInt($('[name=FBTermin]').val());         
         let d = new Date(tgl1.val());
         let d2 = d.getTime() + (term * 24*60*60*1000);
          tgl2.kvDatepicker('update',new Date(d2));
     }
     window.hitungTerm = hitungTerm;

    $('[name=FBTermin]').utilNumberControl().cmp().inputmask({
	    onKeyDown: function(event, buffer, caretPos, opts){
	        hitungTerm();
	    }
	  });
	  
	  $('[name=FBTgl]').change(function (){
	      hitungTerm();
	  });
	  
	  
	  $('[name=FBTglTempo]').change(function (){
	      var tgl1 = $('[name=FBTgl]');
         var tgl2 = $('[name=FBTglTempo]');
         let dt1 = new Date(tgl1.val());
         let dt2 = new Date(tgl2.val());
         let diff = dt2.getTime() - dt1.getTime();
         let diff_day = diff / (1000 * 3600 * 24);
         $('[name=FBTermin]').utilNumberControl().val(diff_day);
	  });
    
    $('#btnPrint').click(function (event) {	      
        $('#form_ttfb_id').attr('action','$urlPrint');
        $('#form_ttfb_id').attr('target','_blank');
        $('#form_ttfb_id').submit();
    });
    
    hitungTerm();
    
    $('#btnCancel').click(function (event) {	      
       $('#form_ttfb_id').attr('action','{$url['cancel']}');
       $('#form_ttfb_id').submit();
    });
    
    $('#btnJurnal').click(function (event) {	
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#form_ttfb_id').attr('action','{$urlJurnal}');
                    $('#form_ttfb_id').submit();                     
                }
            }
        });
    }); 
    
    $('#btnSave').click(function (event) {	
        Hitung();
        $('input[name="SaveMode"]').val('ALL');      
        $('#form_ttfb_id').attr('action','{$url[ 'update' ]}');
        $('#form_ttfb_id').submit();
    }); 
    
    $('.hitung').blur(function () {
        Hitung();
    });
    
    $('#btnSS').click(function() {
        window.util.app.dialogListData.show({
            title: 'Daftar Penerimaan Barang',
            url: getUrlFB()
        },
        function (data) {
            // console.log(data);
            if (!Array.isArray(data)){
                 return;
             }
             var itemData = [];
            // console.log(data);
             data.forEach(function(itm,ind) {
                let SupKode = $('input[name="SupKode"]');
                SupKode.val(itm.header.data.SupKode);
                SupKode.trigger('change');
                let cmp = $('input[name="SSNo"]');
                cmp.val(itm.header.data.SSNo);
                let SSTgl = $('#SSTgl-disp');
                let dtSSTgl = window.util.date.fromStringDate(itm.header.data.SSTgl)
                SSTgl.kvDatepicker('update', dtSSTgl);
                itm.data.SSNo = __model.FBNo; 
                itm.data.FBHarga = itm.data.SSHarga; 
                itemData.push(itm.data);
            });
             
             $.ajax({
             type: "POST",
             url: '$urlDetail',
             data: {
                 oper: 'batch',
                 data: itemData,
                 header: btoa($('#form_ttfb_id').serialize())
             },
             success: function(data) {
                  data.notif.forEach(function(itm,ind) {
                           $.notify({message: itm.keterangan},{type: (itm.status) ? 'success' : 'warning'});
                   });
                   $('#detailGrid').utilJqGrid().load({ url: '$urlDetail'});
             },
           });
             
        })
    });
    
    if (_company.nama.trim() === "CV. ANUGERAH PERDANA 1" || _company.nama.trim() === "CV. ANUGERAH PERDANA 2" || _company.nama.trim() === "CV. ANUGERAH PERDANA 3" || _company.nama.trim() === "CV. ANUGERAH PERDANA 4" || _company.nama.trim() === "CV. ANUGERAH PERDANA 5" || _company.nama.trim() === "CV. ANUGERAH PERDANA 6" || _company.nama.trim() === "CV. ANUGERAH PERDANA 7" || _company.nama.trim() === "CV. ANUGERAH PERDANA 8" || _company.nama.trim() === "CV. ANUGERAH PERDANA PARIGI"){        
        $('#lblFBPSS').text("Piut Prepayment FIF");
        $('#lblFBPtgLain').text("Piut Prepayment WOM");
        $('#lblFBExtraDisc').text("Piut Prepayment MMF");
    }else if(_company.nama.trim() === "CV. ANUGERAH PERDANA MOROWALI" || _company.nama.trim() === "CV. CENDANA MEGAH SANTOSA LUWUK"){
        $('#lblFBExtraDisc').text("Potongan Ekspedisi");
        $('#FBPtgMD-disp').hide();
        $('#lblFBPtgMD').hide();
    }
    else{
        $('#FBPtgMD-disp').hide();
        $('#lblFBPtgMD').hide();
    }
    
    //PPN Hide
    if (_company.nama.trim()  === "CV. ANUGERAH PERDANA MOROWALI" || _company.nama.trim() === "CV. ANUGERAH PERDANA 1" || _company.nama.trim() === "CV. ANUGERAH PERDANA 2" || _company.nama.trim() === "CV. ANUGERAH PERDANA 3" || _company.nama.trim() === "CV. ANUGERAH PERDANA 4" || _company.nama.trim() === "CV. ANUGERAH PERDANA 5" || _company.nama.trim() === "CV. ANUGERAH PERDANA 6" || _company.nama.trim() === "CV. ANUGERAH PERDANA 7" || _company.nama.trim() === "CV. ANUGERAH PERDANA 8" || _company.nama.trim() === "CV. ANUGERAH PERDANA PARIGI"){
        $('#FBPPN-disp').hide();        
        $('#lblppn').hide();        
    }
    
     $('#btnAdd').click(function (event) {	      
        $('#form_ttfb_id').attr('action','{$urlAdd}');
        $('#form_ttfb_id').submit();
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });  
    
    $('[name=FBTgl]').utilDateControl().cmp().attr('tabindex', '2');
    
    
    $('#btnImport').click(function (event) {	      
        window.util.app.dialogListData.show({
            title: 'UnitInbound from Purchase Order',
            url: '{$urlImport}'
        },
        function (data) {
			if(!Array.isArray(data)) return;
            $.ajax({
             type: "POST",
             url: '$urlDetail',
             data: {
                 oper: 'import',
                 data: data,
                 header: btoa($('#form_ttfb_id').serialize())
             },
             success: function(data) {
                let FBNoView = $('[name=FBNoView]').val();
                window.location.href = "{$url['update']}&oper=skip-load&FBNoView="+FBNoView;
             },
           });
        });
    });   
    
     $('#DOTgl').on('change', function(e){
        console.log($('#DOTgl-disp').val());
        console.log($('#FBTgl-disp').val());
        const dotgl = new Date($('#DOTgl-disp').val()); 
        const fbtgl = new Date($('#FBTgl-disp').val());
        if (dotgl > fbtgl){
            bootbox.alert({ message: "Tanggal DO tidak boleh lebih dari tanggal FB", size: 'small'});
            return;
        }
	  });  
   
    
    
JS
);