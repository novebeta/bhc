<?php
use aunit\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = 'Faktur Beli';
$this->params[ 'breadcrumbs' ][] = $this->title;
$jenis                           = isset( $_GET[ 'jenis' ] ) ? $_GET[ 'jenis' ] : '';
$arr                             = [
	'cmbTxt'    => [
		'ttfb.FBNo'      => 'No Faktur Beli',
		'ttfb.SSNo'      => 'No Surat Jalan Supplier',
		'ttfb.SupKode'   => 'Supplier',
		'SupNama'        => 'Nama Supplier',
		'ttfb.FBMemo'    => 'Keterangan',
		'ttfb.NoGL'      => 'No GL',
		'ttfb.FBLunas'   => 'Lunas',
		'ttfb.UserID'    => 'User ID',
		'ttfb.FPNo'      => 'No FP [Faktur Pajak]',
		'ttfb.FBPosting' => 'Posting',
	],
	'cmbTgl'    => [
		'ttfb.FBTgl'      => 'Tanggal FB',
		'ttfb.FPTgl'      => 'Tanggal FP',
		'ttfb.DOTgl'      => 'Tanggal DO',
		'ttfb.FBTglTempo' => 'Tanggal Tempo',
	],
	'cmbNum'    => [
		'ttfb.FBTotal'     => 'Total',
		'ttfb.FBTermin'    => 'Termin',
		'ttfb.FBPSS'       => 'PSS - Potongan Program',
		'ttfb.FBPtgLain'   => 'Potangan Lain Lain',
		'ttfb.FBExtraDisc' => 'Extra Disc',
		'ttfb.FBPtgMD'     => 'Potongan Main Dealer',
		'ttfb.FPHargaUnit' => 'FP HargaUnit',
		'ttfb.FPDiscount'  => 'FP Discount',
		'ttfb.FPDPP'       => 'FP FPP',
	],
	'sortname'  => "FBTgl",
	'sortorder' => 'desc'
];
if ( $jenis == 'SS' ) {
	$arr = [
		'cmbTxt'    => [
			'FBNo'      => 'No Faktur Beli',
			'SSNo'      => 'No Surat Jalan Supplier',
			'SupKode'   => 'Supplier',
			'SupNama'   => 'Nama Supplier',
			'FBMemo'    => 'Keterangan',
			'NoGL'      => 'No GL',
			'FBLunas'   => 'Lunas',
			'UserID'    => 'User ID',
			'FPNo'      => 'No FP [Faktur Pajak]',
			'FBPosting' => 'Posting',
		],
		'cmbTgl'    => [
			'FBTgl'      => 'Tanggal FB',
			'FPTgl'      => 'Tanggal FP',
			'DOTgl'      => 'Tanggal DO',
			'FBTglTempo' => 'Tanggal Tempo',
		],
		'cmbNum'    => [
			'FBTotal'     => 'Total',
			'FBTermin'    => 'Termin',
			'FBPSS'       => 'PSS - Potongan Program',
			'FBPtgLain'   => 'Potangan Lain Lain',
			'FBExtraDisc' => 'Extra Disc',
			'FBPtgMD'     => 'Potongan Main Dealer',
			'FPHargaUnit' => 'FP HargaUnit',
			'FPDiscount'  => 'FP Discount',
			'FPDPP'       => 'FP FPP',
		],
		'sortname'  => "FBTgl",
		'sortorder' => 'desc'
	];
}
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttfb::className(),
	'Faktur Beli', [ 'mode' => isset( $mode ) ? $mode : '', 'url_add' => $url_add, 'url_delete' => Url::toRoute( [ 'ttfb/delete' ] ), ] ),
	\yii\web\View::POS_READY );
$urlDetail = Url::toRoute( [ \Yii::$app->controller->id . '/detail', 'jenis' => $jenis ] );
$this->registerJs( <<< JS
jQuery(function ($) {
    if($('#jqGridDetail').length) {
        $('#jqGridDetail').utilJqGrid({
            url: '$urlDetail',
            height: 240,
            readOnly: true,
            rownumbers: true,
            rowNum: 1000,
            colModel: [
                {
                    name: 'MotorType',
                    label: 'Type',
                    width: 200
                },
                {
                    name: 'MotorTahun',
                    label: 'Tahun',
                    width: 100
                },
                {
                    name: 'MotorWarna',
                    label: 'Warna',
                    width: 150
                },
                {
                    name: 'MotorNoMesin',
                    label: 'NoMesin',
                    width: 200,
                    editable: true
                },
                {
                    name: 'MotorAutoN',
                    label: 'MotorAutoN',
                    width: 200,
                    hidden: true,
                    editable: true
                },
                {
                    name: 'MotorNoRangka',
                    label: 'NoRangka',
                    width: 200
                },
                {
                    name: 'FBNo',
                    label: 'No Faktur',
                    width: 200
                },
                // {
                //     name: 'SSHarga',
                //     label: 'HrgBeli',
                //     template: 'money',
                //     width: 200
                // },
                {
                    name: 'FBHarga',
                    label: 'HrgBeli',
                    template: 'money',
                    width: 200
                }
            ],
	        multiselect: gridFn.detail.multiSelect(),
	        onSelectRow: gridFn.detail.onSelectRow,
	        onSelectAll: gridFn.detail.onSelectAll,
	        ondblClickRow: gridFn.detail.ondblClickRow,
	        loadComplete: gridFn.detail.loadComplete,
	        listeners: {
                afterLoad: function(grid, response) {
                    $("#cb_jqGridDetail").trigger('click');
                }
            }
        }).init().setHeight(gridFn.detail.height);
    }
});
JS, \yii\web\View::POS_READY );