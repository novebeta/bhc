<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttfb */
$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode).'- Faktur Beli: ' . $model->FBNo;
?>
<div class="ttfb-update">
	<?= $this->render( '_form', [
		'model'  => $model,
		'dsBeli' => $dsBeli,
		'url'    => [
			'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
			'cancel' => $cancel,
			'detail' => Custom::url( \Yii::$app->controller->id . '/detail' ),
			'tdmotorwarna' => Custom::url( 'tdmotorwarna/find' ),
		]
	] ) ?>
</div>
