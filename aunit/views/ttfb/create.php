<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttfb */
$params                          = '&id=' . $id . '&action=create';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = 'Tambah - Faktur Beli';
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Faktur Beli', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="ttfb-create">
	<?= $this->render( '_form', [
		'model'  => $model,
		'dsBeli' => $dsBeli,
		'url'    => [
			'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
			'cancel' => $cancel,
			'detail' => Custom::url( \Yii::$app->controller->id . '/detail' ),
			'tdmotorwarna' => Custom::url( 'tdmotorwarna/find' ),
		]
	] ) ?>
</div>
