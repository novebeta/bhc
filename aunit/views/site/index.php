<?php

/* @var $this yii\web\View */

$this->title = 'Dashboard';
?>
<!-- Info boxes -->

<!-- /.row -->
<?php
$this->registerCssFile('@web/adminlte/css/ionicons.min.css');
$this->registerCssFile('@web/adminlte/css/jquery-jvectormap.css');
$this->registerJsFile('@web/adminlte/js/fastclick.js',[
	'depends' => [
		\yii\web\JqueryAsset::class
	]
]);
$this->registerJsFile('@web/adminlte/jvectormap/jquery-jvectormap-1.2.2.min.js',[
	'depends' => [
		\yii\web\JqueryAsset::class
	]
]);
$this->registerJsFile('@web/adminlte/jvectormap/jquery-jvectormap-world-mill-en.js',[
	'depends' => [
		\yii\web\JqueryAsset::class
	]
]);
$this->registerJsFile('@web/adminlte/js/jquery.sparkline.min.js',[
	'depends' => [
		\yii\web\JqueryAsset::class
	]
]);
$this->registerJsFile('@web/adminlte/js/Chart.js',[
	'depends' => [
		\yii\web\JqueryAsset::class
	]
]);
$this->registerJsFile('@web/adminlte/js/pages/dashboard2.js',[
	'depends' => [
		\yii\web\JqueryAsset::class
	]
]);
$this->registerJsFile('@web/adminlte/js/demo.js',[
	'depends' => [
		\yii\web\JqueryAsset::class
	]
]);
?>