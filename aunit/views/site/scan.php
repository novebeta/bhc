<?php

use aunit\assets\AppAsset;
use aunit\components\FormField;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttgeneralledgerhd */
/* @var $form yii\widgets\ActiveForm */
AppAsset::register($this);
$this->registerJsFile("https://unpkg.com/@zxing/library@0.18.6/umd/index.min.js", ['position' => \yii\web\View::POS_HEAD]);
$this->title = 'Scan';
$this->params['breadcrumbs'][] = $this->title;

?>
    <div>
    <button type="button" id="startButton">Start</button>
    <button type="button" id="resetButton">Reset</button>
    </div>

    <div>
        <video id="video" width="300" height="200" style="border: 1px solid gray"></video>
    </div>

    <div id="sourceSelectPanel" style="display:none">
        <label for="sourceSelect">Change video source:</label>
        <select id="sourceSelect" style="max-width:400px">
        </select>
    </div>

    <div style="display: table">
        <label for="decoding-style"> Decoding Style:</label>
        <select id="decoding-style" size="1">
            <option value="once">Decode once</option>
            <option value="continuously">Decode continuously</option>
        </select>
    </div>

    <label>Result:</label>
    <pre><code id="result"></code></pre>

    <script type="text/javascript">
      window.addEventListener('load', function () {
      let selectedDeviceId;
      const codeReader = new ZXing.BrowserMultiFormatReader()
      console.log('ZXing code reader initialized')
      codeReader.listVideoInputDevices()
        .then((videoInputDevices) => {
          const sourceSelect = document.getElementById('sourceSelect')
          selectedDeviceId = videoInputDevices[0].deviceId
          if (videoInputDevices.length >= 1) {
            videoInputDevices.forEach((element) => {
              const sourceOption = document.createElement('option')
              sourceOption.text = element.label
              sourceOption.value = element.deviceId
              sourceSelect.appendChild(sourceOption)
            })

            sourceSelect.onchange = () => {
              selectedDeviceId = sourceSelect.value;
            };

            const sourceSelectPanel = document.getElementById('sourceSelectPanel')
            sourceSelectPanel.style.display = 'block'
          }

          document.getElementById('startButton').addEventListener('click', () => {
            codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
              if (result) {
                console.log(result)
                document.getElementById('result').textContent = result.text
              }
              if (err && !(err instanceof ZXing.NotFoundException)) {
                console.error(err)
                document.getElementById('result').textContent = err
              }
            })
            console.log(`Started continous decode from camera with id `+selectedDeviceId)
          })

          document.getElementById('resetButton').addEventListener('click', () => {
            codeReader.reset()
            document.getElementById('result').textContent = '';
            console.log('Reset.')
          })

        })
        .catch((err) => {
          console.error(err)
        })
    });
    </script>