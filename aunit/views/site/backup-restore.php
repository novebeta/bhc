<?php
use aunit\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttgeneralledgerhd */
/* @var $form yii\widgets\ActiveForm */
AppAsset::register( $this );
$this->title                   = 'Backup - Restore';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="ttgeneralledgerhd-form">
        <div class="col-sm-16">
			<?php
			$form = ActiveForm::begin( [ 'action' => "#", 'options' => ['autocomplete'=>'off', 'method' => 'post', 'style' => "height: 400px" ] ] );
			\aunit\components\TUi::form(
				[
					'class' => "row-no-gutters",
					'items' => [
						[
//						'class' => "form-inline",
							'items' => [
								[
									'items' => ":txtProgress"
								],
							]
						],
					]
				],
				[
//            'class' => "pull-right",
					'items' => ":btnBackup :btnRestore"
				],
				[
					":txtProgress" => Html::textarea( 'TxtProgress', '',
						[
							'id'    => 'TxtProgress',
							'class' => 'form-control',
							'style' => "width: '100%;height: 400px'",
							'rows'  => 15
						] ),
					":btnBackup"   => Html::button( 'Backup', [ 'class' => 'btn btn-info btn-primary', 'id' => 'btnBackup' ] ),
					":btnRestore"  => Html::button( 'Restore', [ 'class' => 'btn btn-default pull-right', 'id' => 'btnRestore' ] )
				]
			);
			ActiveForm::end();
			?>
        </div>
    </div>
<?php
$this->registerJsVar( 'urlBackup', Url::toRoute( [ 'site/backup' ] ) );
$this->registerJsVar( 'urlMonitor', Url::toRoute( [ 'site/monitor-backup' ] ) );
$this->registerJs( $this->render( 'backup-restore.js' ) );