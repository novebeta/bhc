<?php
use yii\helpers\Html;
use aunit\assets\AppAsset;
use yii\widgets\ActiveForm;
use aunit\components\FormField;
use kartik\datecontrol\DateControl;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttgeneralledgerhd */
/* @var $form yii\widgets\ActiveForm */
AppAsset::register( $this );
$this->title                     = 'Broadcast Message';
$this->params[ 'breadcrumbs' ][] = $this->title;

?>

<style>
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
#img-upload{
    width: 100%;
}
</style>
		<?php
		$form = ActiveForm::begin( [ 'id' => 'send_message_form', 'options' => [ 'autocomplete' => 'off', 'method' => 'post', 'style' => "height: 400px" ] ] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					['class' => "col-md-18", 'style' => "margin-bottom: 3px;", 'items' => '<table id="detailGrid"></table>'],
                    ['class' => "col-md-5", 'style' => "margin-bottom: 3px;margin-left: 3px;", 'items' => '<label class="control-label" style="margin: 0; padding: 6px 0;">Image / Video</label>
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-default btn-file">
                                Browse...<input type="file" id="imgInp">
                            </span>
                        </span>
                        <input type="text" class="form-control" readonly>
                        <input type="hidden" id="imageEncode">
                    </div>
                    <video width="100%" controls id="myvideo" style="height:100%">
                    Your browser does not support HTML5 video.
                    </video>
                    <img id="img-upload"/>'],
                    [
						'class' => "form-group col-md-24",
						'items' => [
							[ 'class' => "col-sm-2", 'items' => ":LabelPhone" ],
							[ 'class' => "col-sm-22", 'items' => ":Phone" ],
						],
					],
					[
						'class' => "form-group col-md-24",
						'items' => [
							[ 'class' => "col-sm-2", 'items' => ":LabelPesan" ],
							[ 'class' => "col-sm-22", 'items' => ":Pesan" ],
						],
					],
				]
			],
			[
				'class' => "col-md-24",
				'items' => [
				        [ 'class' => "col-sm-6", 'items' => ":btnLoad :btnQuery" ],
				        [ 'class' => "col-sm-18 pull-right", 'items' => [
                            'class' => "pull-right col-md-24",
                            'items' => [
                                [ 'class' => "col-sm-4", 'items' => ":from" ],
                                [ 'class' => "col-sm-4", 'items' => ":to" ],
                                [ 'class' => "col-sm-16 ", 'items' => ":btnReport :btnPreview :btnSend" ],
                            ],
                        ] ],
                ]
			],
			[
				":LabelPhone" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Query</label>',
				":LabelPesan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pesan</label><input type="file" id="file-input" class="hidden"/>',
				":Phone"      => Html::textarea( 'query', 'SELECT   tdcustomer.CusTelepon2, tdcustomer.CusTelepon,  tdcustomer.CusNama,   ttdk.DKTgl,  tdmotortype.MotorNama,  tmotor.MotorWarna
                    FROM tdcustomer
                      INNER JOIN ttdk  ON tdcustomer.CusKode = ttdk.CusKode
                      INNER JOIN tmotor  ON ttdk.DKNo = tmotor.DKNo
                      INNER JOIN tdmotortype   ON tmotor.MotorType = tdmotortype.MotorType
                    WHERE DKTgl BETWEEN DATE_ADD(NOW(), INTERVAL -100 DAY) AND NOW() LIMIT 5;', [ 'class' => 'form-control' ] ),
				":Pesan"      => Html::textarea( 'msg', 'Saya CusNama, Motorku MotorNama berwarna MotorWarna', [ 'class' => 'form-control' ] ),
                ":from" => FormField::dateInput( [ 'name' => 'from', 'config' => [ 'id' => 'from','value' => date('Y-m-d') ] ] ),
                ":to" => FormField::dateInput( [ 'name' => 'to', 'config' => [ 'id' => 'to', 'value' => date('Y-m-d') ] ] ),
                ":btnLoad"    => Html::button( '<i class="glyphicon glyphicon-folder-open"></i>&nbsp &nbsp Load', [ 'class' => 'btn btn-primary', 'id' => 'btnLoad', 'style' => "width: 80px;height: 35px" ] ),
				":btnReport"   => Html::button( '<i class="glyphicon glyphicon-th"></i> &nbsp; Report', [ 'class' => 'btn btn-default', 'id' => 'btnReport', 'style' => "width: 80px;height: 35px" ] ),
				":btnQuery"   => Html::button( '<i class="glyphicon glyphicon-th"></i> &nbsp; Tampil', [ 'class' => 'btn btn-danger', 'id' => 'btnQuery', 'style' => "width: 80px;height: 35px" ] ),
				":btnPreview" => Html::button( '<i class="glyphicon glyphicon-eye-open"></i>  &nbsp; View', [ 'class' => 'btn btn-success', 'id' => 'btnPreview', 'style' => "width: 80px;height: 35px" ] ),
				":btnSend"    => Html::button( '<i class="glyphicon glyphicon-envelope"></i> &nbsp; Kirim', [ 'class' => 'btn bg-purple', 'id' => 'btnSend', 'style' => "width: 80px;height: 35px" ] ),
			]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlTampil  = \yii\helpers\Url::toRoute( [ 'site/tampil' ] );
$urlSend    = \yii\helpers\Url::toRoute( [ 'site/send' ] );
$urlPreview = \yii\helpers\Url::toRoute( [ 'site/preview' ] );
$urlReport = \yii\helpers\Url::toRoute( [ 'site/report-realtime' ] );
$this->registerJs( <<< JS
var colModels = [];
var grid = $("#detailGrid"); 
grid.jqGrid({
    datatype: 'local',
    rowNum: 1100,  
    loadonce:true,         
    data: [],
    rownumbers: true,
    colModel: colModels,
    multiSort: true,
    search: false,
    altclass: 'row-zebra',
    altRows: true,
    multiselect:true,  
    pgbuttons: false,
    pginput: false,
    pgtext: "",
    height: 260,
    autowidth:true, 
    shrinkToFit:true,
    styleUI : 'Bootstrap',
}).navGrid('#detailGridPagerHp',{
    edit:false,add:false,del:false,search:false,refresh:false
}).bindKeys();   

$('#gbox_detailGrid').css('width','100%');


$('#btnQuery').click(function (event) {	
     $.ajax({
         type: "POST",
         url: '$urlTampil',
         data: {
             query: $('[name=query]').val()
         },
         success: function(response) {             
             $.jgrid.gridUnload("#detailGrid");
             var grid = $("#detailGrid"); 
             grid.jqGrid({
                datatype: 'local',
                rowNum: 1100,  
                loadonce:true,         
                data: response.data,
                rownumbers: true,
                colModel: response.colModel,
                multiSort: true,
                search: false,
                altclass: 'row-zebra',
                altRows: true,
                multiselect:true,  
                pgbuttons: false,
                pginput: false,
                pgtext: "",
                height: 260,
                autowidth:true, 
                shrinkToFit:true,
                styleUI : 'Bootstrap',
                }).navGrid('#detailGridPagerHp',{
                edit:false,add:false,del:false,search:false,refresh:false
                }).bindKeys();   
                
                $('#gbox_detailGrid').css('width','100%');           
           
         },
     });
}); 

// SELECT   '087738074320' CusTelepon2, '087738074320' CusTelepon,  'Aan' CusNama
// Saya CusNama
function sendMsg(url, rowData){
    var data = new FormData();
    jQuery.each(jQuery('#imgInp')[0].files, function(i, file) {
        data.append('file-'+i, file);
    });
    data.append('pesan', $('[name=msg]').val());
    data.append('data', JSON.stringify(rowData));
   $.ajax({
         type: "POST",
         url: url,
         data: data,
         cache: false,
         contentType: false,
         processData: false,
         type: 'POST',
         success: function(response) {
             $.notify({message: response.message},{
                type: (response.success) ? 'success' : 'warning',
             });
             var rowData = window.records.shift();
            if (rowData !== undefined) sendMsg(url, rowData);
         },
    });
}

$('#btnSend').click(function (event) {	
    window.records = [];
    var myGrid = $('#detailGrid'),
    selRowId = myGrid.jqGrid ('getGridParam', 'selarrrow');
    selRowId.forEach(function(itm,ind) {
        window.records.push($('#detailGrid').jqGrid ('getRowData', itm));
    });
    var rowData = window.records.shift();
    if (rowData !== undefined) sendMsg('$urlSend',rowData);
}); 
$('#btnReport').click(function (event) {
    $.ajax({
         type: "POST",
         url: '$urlReport',
         data: {
             from: $('[name=from]').val(),
             to: $('[name=to]').val(),
         },
         success: function(response) {             
             $.jgrid.gridUnload("#detailGrid");
             var grid = $("#detailGrid"); 
             grid.jqGrid({
                datatype: 'local',
                rowNum: 1100,  
                loadonce:true,         
                data: response.data,
                rownumbers: true,
                colModel: response.colModel,
                multiSort: true,
                search: true,
                altclass: 'row-zebra',
                altRows: true,
                multiselect:false,  
                pgbuttons: false,
                pginput: false,
                pgtext: "",
                height: 260,
                autowidth:true, 
                shrinkToFit:true,
                styleUI : 'Bootstrap',
                }).navGrid('#detailGridPagerHp',{
                edit:false,add:false,del:false,search:false,refresh:false
                }).bindKeys();   
                
                $('#gbox_detailGrid').css('width','100%');           
           
         },
     });
}); 

$('#btnPreview').click(function (event) {	
    window.records = [];
    var myGrid = $('#detailGrid'),
    selRowId = myGrid.jqGrid ('getGridParam', 'selarrrow');
    selRowId.forEach(function(itm,ind) {
        window.records.push($('#detailGrid').jqGrid ('getRowData', itm));
    });
    var rowData = window.records.shift();
    if (rowData !== undefined) sendMsg('$urlPreview',rowData);
}); 

$("[name=query]").focus(function(){
  window.aktif_input = $(this);
}); 
$("[name=msg]").focus(function(){
  window.aktif_input = $(this);
}); 

function readSingleFile(e) {
               console.log(e);
  var file = e.target.files[0];
  if (!file) {
    return;
  }
  var reader = new FileReader();
  reader.onload = function(e) {
    var contents = e.target.result;
    window.aktif_input.val(contents);
  };
  reader.readAsText(file);
}
document.getElementById('file-input').addEventListener('change', readSingleFile, false);
$('#btnLoad').click(function() {
    if (window.aktif_input === undefined){
        bootbox.alert({message:'Silahkan taruh kursor di tempat yang diinginkan.',size:'small'});
        return;
    }
    $('#file-input').trigger('click');
});

$(document).on('change', '.btn-file :file', function() {
var input = $(this),
    label = input.val().replace('C:\\\\fakepath\\\\','');
    if(isImage(label)){
        input.trigger('fileselect', [label]);
    }else{
        alert("File yang akan dikirim harus memiliki ekstensi \\r\\njpg/jpeg/gif/png/doc/docx/pdf/odt/csv/ppt/pptx/xls/xlsx/txt/mp4/mpeg !");
    }
});

$('.btn-file :file').on('fileselect', function(event, label) {
		    
    var input = $(this).parents('.input-group').find(':text'),
        log = label;
    
    if( input.length ) {
        input.val(log);
    } else {
        if( log ) alert(log);
    }

});
function readURL(input) {
    var allowedExtensions = /(\.jpg|\.jpeg|\.gif|\.png|\.doc|\.docx|\.pdf|\.odt|\.csv|\.ppt|\.pptx|\.xls|\.xlsx|\.txt|\.mp4|\.mpeg)$/i;
    if (input.files && input.files[0] && allowedExtensions.exec(input.value)) {
        var fileTypes = ['mp4', 'mpeg'];
        var extension = input.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
            isVideo = fileTypes.indexOf(extension) > -1;
        var reader = new FileReader();
        
        reader.onload = function (e) {
            if(!isVideo){
                $('#myvideo').hide();
                $("#img-upload").show();
                $('#img-upload').attr('src', e.target.result);
            }else{
                $('#myvideo').show();
                $("#img-upload").hide();
                var video_here = document.getElementById('myvideo');
                video_here.setAttribute('src', e.target.result);
            }
        }

        // reader.onloadend = () => {
        //     const base64String = reader.result
        //             .replace("data:", "")
        //             .replace(/^.+,/, "");
        //             $("#imageEncode").val(base64String);
        // };
        
        reader.readAsDataURL(input.files[0]);
    }else{
        alert("File yang akan dikirim harus memiliki ekstensi \\r\\njpg/jpeg/gif/png/doc/docx/pdf/odt/csv/ppt/pptx/xls/xlsx/txt/mp4/mpeg !");
        $("#imgInp").reset();
        $("#imageEncode").reset();

    }
}

$("#imgInp").change(function(){
    readURL(this);
}); 

function getExtension(filename) {
  var parts = filename.split('.');
  return parts[parts.length - 1];
}

function isImage(filename) {
  var ext = getExtension(filename);
  switch (ext.toLowerCase()) {
    case 'jpg':
    case 'gif':
    case 'jpeg':
    case 'png':
    case 'doc':
    case 'docx':
    case 'pdf':
    case 'odt':
    case 'csv':
    case 'ppt':
    case 'pptx':
    case 'xls':
    case 'xlsx':
    case 'txt':
    case 'mp4':
    case 'mpeg':
      return true;
  }
  return false;
}

$('#myvideo').hide();
$("#img-upload").hide();

JS
);