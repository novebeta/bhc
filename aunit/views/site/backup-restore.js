jQuery(function ($) {
    var refreshtime = 10;
    // var running = true;
    var line = 0;
    var url_dmp = null;

    function tc() {
        asyncAjax("GET", urlMonitor, Math.random(), display, {});
    }

    function display(xhr, cdat) {
        if (xhr.readyState == 4 && xhr.status == 200) {
            if (xhr.responseText != "==EOL==") {
                let cmp = document.getElementById("TxtProgress");
                cmp.innerHTML = cmp.innerHTML + xhr.responseText;
                var textarea = $('#TxtProgress');
                textarea.scrollTop(textarea[0].scrollHeight);
                line++;
            } else {
                console.log(xhr.responseText);
            }
            if (url_dmp == null || (xhr.responseText !== "==EOL==")) {
                tc();
            } else {
                window.open(url_dmp, 'blank_');
                $("#btnBackup").attr("disabled", false);
                $("#btnRestore").attr("disabled", false);
            }
        }
    }

    function asyncAjax(method, url, qs, callback, callbackData) {
        var xmlhttp = new XMLHttpRequest();
        //xmlhttp.cdat=callbackData;
        if (method == "GET") {
            url += "&line=" + line + "&v=" + qs;
        }
        var cb = callback;
        callback = function () {
            var xhr = xmlhttp;
            //xhr.cdat=callbackData;
            var cdat2 = callbackData;
            cb(xhr, cdat2);
            return;
        };
        xmlhttp.open(method, url, true);
        xmlhttp.onreadystatechange = callback;
        if (method == "POST") {
            xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xmlhttp.send(qs);
        } else {
            xmlhttp.send(null);
        }
    }

    $("#btnBackup").click(function () {
        $("#btnBackup").attr("disabled", true);
        $("#btnRestore").attr("disabled", true);
        tc();
        $.ajax({
            url: urlBackup,
            method: 'GET',
            async: true,
            cache: false,
            success: function (data) {
                url_dmp = data.url;
            }
        });
    });
});