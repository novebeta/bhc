<?php
use yii\helpers\Html;
use aunit\assets\AppAsset;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttgeneralledgerhd */
/* @var $form yii\widgets\ActiveForm */
AppAsset::register( $this );
$this->title                     = 'PUSH INV1';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr = [
    'cmbTxt' => [
        'noShippingList' =>	'No Shipping List',
        'idSalesPeople' =>	'Id Sales People',
        'idSpk' =>	'Id Spk',
    ],
    'cmbTgl' => [],
    'cmbNum' => [],
    'sortname' => "noShippingList",
    'sortorder' => 'desc'
];
$this->registerJsVar('setcmb', $arr);
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render('../_search'); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>

		<?php
		$form = ActiveForm::begin( [ 'id' => 'push-inv1-form', 'options' => [ 'method' => 'post', 'style' => "height: 400px" ] ] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "col-md-24", 'style' => "margin-bottom: 3px;", 'items' => '<table id="detailGrid"></table>' ],
                    [ 'class' => "form-group col-md-23", 'items' => [[ 'class' => "pull-right", 'items' => ":btnPush" ]]],
				],
			],
            [],
			[
                ":btnPush"      => Html::button( '<i class="fas fa-upload"></i> PUSH INV1', [ 'class' => 'btn btn-success', 'id' => 'btnPush' ] ),
            ]
		);
		ActiveForm::end();
		?>

<?php
$urlDetail                = Url::toRoute( [ 'site/details' ] );
$this->registerJs( <<< JS
window.reload = true;
$('#detailGrid').utilJqGrid({
        url: '$urlDetail',
        height: 300,        
	    scrollrows : true,           
        loadonce:true,
        rowNum: 100000,
        navButtonTambah: false,
        rownumbers: true,    
        datatype: "json",
        serializeRowData: function (postdata) {
            let header = $('#push-inv1-form').utilForm().getData();
            postdata = Object.assign(postdata, header);
            console.log(postdata);
            return postdata;
        },
        postData: $('#push-inv1-form').serialize(),  
        colModel: [
            {
                name: 'idInvoice',
                label: 'idInvoice',
                width: 150,
            },
            {
                name: 'idSPK',
                label: 'idSPK',
                width: 70,
            },         
            {
                name: 'idCustomer',
                label: 'idCustomer',
                width: 130,
            },                  
            {
                name: 'Amount',
                label: 'Amount',
                template: 'money',
                width: 100,
            },
            {
                name: 'tipePembayaran',
                label: 'tipePembayaran',
                width: 120,
            },
            {
                name: 'STATUS',
                label: 'STATUS',
                width: 80,
            },
            {
                name: 'Note',
                label: 'Note',
                width: 100,
            },
            {
                name: 'DKNetto',
                label: 'DKNetto',
                template: 'money',
                width: 120,
            },
            {
                name: 'DKNo',
                label: 'DKNo',
                width: 120,
            },
        ],
    }).init().fit($('.box-body'));
JS
);