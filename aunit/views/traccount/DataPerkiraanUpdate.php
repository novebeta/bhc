<?php
/* @var $this yii\web\View */
/* @var $model aunit\models\Traccount */
$this->title                   = str_replace('Menu','',\aunit\components\TUi::$actionMode).'Data Perkiraan: ' . $model->NoAccount;
$this->params['breadcrumbs'][] = [ 'label' => 'Data Perkiraan', 'url' => [ 'data-perkiraan' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->NoAccount, 'url' => [ 'data-perkiraan-update', 'id' => base64_encode($model->NoAccount) ] ];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="traccount-update">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
