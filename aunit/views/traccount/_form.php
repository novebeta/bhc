<?php
use common\components\Custom;
use aunit\models\Traccount;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $model aunit\models\Traccount */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function formattraccount(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-8" style="text-align: left">' + result.id + '</div>' +
           '<div class="col-xs-16">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
$url[ 'cancel' ] = Url::toRoute( 'traccount/index' );
?>
<div class="traccount-form">
	<?php
	$form = ActiveForm::begin( [ 'id' => 'frm_traccount_id' ] );
	\aunit\components\TUi::form(
		[
			'class' => "row-no-gutters",
			'items' => [
				[ 'class' => "form-group col-md-24",
				  'items' => [
					  [ 'class' => "col-sm-3", 'items' => ":LabelNomorPerkiraan" ],
					  [ 'class' => "col-sm-8", 'items' => ":NomorPerkiraan" ],
					  [ 'class' => "col-sm-1" ],
					  [ 'class' => "col-sm-3", 'items' => ":LabelNamaPerkiraan" ],
					  [ 'class' => "col-sm-8", 'items' => ":NamaPerkiraan" ],
				  ],
				],
				[ 'class' => "form-group col-md-24",
				  'items' => [
					  [ 'class' => "col-sm-3", 'items' => ":LabelJenisPerkiraan" ],
					  [ 'class' => "col-sm-8", 'items' => ":JenisPerkiraan" ],
					  [ 'class' => "col-sm-1" ],
					  [ 'class' => "col-sm-3", 'items' => ":LabelStatus" ],
					  [ 'class' => "col-sm-4", 'items' => ":Status" ],
				  ],
				],
				[ 'class' => "form-group col-md-24",
				  'items' => [
					  [ 'class' => "col-sm-3", 'items' => ":LabelNomorInduk" ],
					  [ 'class' => "col-sm-8", 'items' => ":NomorInduk" ],
				  ],
				],
			]
		],
        ['class' => "row-no-gutters",
            'items'  => [
                [
                    'class' => "col-md-12 pull-left",
                    'items' => $this->render( '../_nav', [
                        'url'=> $_GET['r'],
                        'options'=> [
                            'traccount.NamaAccount' => 'NamaAccount',
                            'traccount.NoAccount'   => 'NoAccount',
                            'traccount.NoParent'      => 'NoParent',
                        ],
                    ])
                ],
                ['class' => "col-md-12 pull-right",
                    'items'  => [
                        ['class' => "pull-right", 'items' => ":btnAction"],
                    ],
                ],
            ],
        ],
		[
			":LabelNomorPerkiraan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor Perkiraan</label>',
			":LabelNamaPerkiraan"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Perkiraan</label>',
			":LabelJenisPerkiraan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis Perkiraan</label>',
			":LabelStatus"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',
			":LabelNomorInduk"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor Induk</label>',
			":NomorPerkiraan"      => $form->field( $model, 'NoAccount', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true, 'tabindex' => '1' ] ),
			":NamaPerkiraan"       => $form->field( $model, 'NamaAccount', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true, 'tabindex' => '2' ] ),
			":JenisPerkiraan"      => $form->field( $model, 'JenisAccount', [ 'template' => '{input}' ] )->dropDownList( [ 'Header' => 'Header', 'Detail' => 'Detail' ], [ 'maxlength' => true, 'tabindex' => '3' ] ),
			":Status"              => $form->field( $model, 'StatusAccount', [ 'template' => '{input}' ] )->dropDownList( [ 'A' => 'AKTIF', 'N' => 'NON AKTIF' ], [ 'maxlength' => true, 'tabindex' => '4' ] ),
			":NomorInduk"          => $form->field( $model, 'NoParent', [ 'template' => '{input}' ] )
			                               ->widget( Select2::classname(), [
				                               'value'         => $model->NoParent,
				                               'theme'         => Select2::THEME_DEFAULT,
				                               'data'          => Traccount::find()->select2(),
				                               'options'       => [ 'tabindex' => '5' ],
				                               'pluginOptions' => [
					                               'dropdownAutoWidth' => true,
					                               'templateResult'    => new JsExpression( 'formattraccount' ),
					                               'templateSelection' => new JsExpression( 'formattraccount' ),
					                               'matcher'           => new JsExpression( 'matchCustom' ),
					                               'escapeMarkup'      => $escape,
				                               ],
			                               ] ),
			":btnSave"             => Html::button( '<i class="glyphicon glyphicon-floppy-disk"><br><br> Simpan</i>', [ 'class' => 'btn btn-info btn-primary ', 'id' => 'btnSave' , 'style' => 'width:60px;height:60px'] ),
			":btnCancel"           => Html::Button( '<i class="fa fa-ban"><br><br>Batal</i>', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px' ] ),
			":btnAdd"              => Html::button( '<i class="fa fa-plus-circle"><br><br> Tambah</i>', [ 'class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:60px;height:60px' ] ),
			":btnEdit"             => Html::button( '<i class="fa fa-edit"><<br><br> Edit</i>', [ 'class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px' ] ),
			":btnDaftar"           => Html::Button( '<i class="fa fa-list"><br><br> Daftar</i> ', [ 'class' => 'btn btn-primary ', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px' ] ),
		],
        [
			 '_akses' => 'Edit Perkiraan',
			'url_main' => 'traccount',
			'url_id' => $_GET['id'] ?? '',
		]
	);
	ActiveForm::end();
	$urlAdd   = Url::toRoute( [ 'traccount/create', 'id' => 'new', 'action'=>'create' ] );
	$urlEdit  = Url::toRoute( [ 'traccount/update', 'id' => $id, 'action'=>'update' ] );
	$urlCancel  = Url::toRoute( [ 'traccount/update', 'id' => $id, 'action'=>'display' ] );
	$urlIndex = Url::toRoute( [ 'traccount/index' ] );
	$this->registerJs( <<< JS
    $('#btnAdd').click(function (event) {	      
        window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$urlEdit}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });  

     $('#btnCancel').click(function (event) {	      
       window.location.href = '{$urlCancel}';
    });
      
     $('#btnSave').click(function (event) {
        $('#frm_traccount_id').submit();
      }); 
     
JS
	);
	?>
</div>