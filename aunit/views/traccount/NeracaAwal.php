<?php
use aunit\assets\JqwidgetsAsset;
use kartik\datecontrol\DateControl;
use kartik\number\NumberControl;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
JqwidgetsAsset::register( $this );
$this->title                     = 'Neraca Awal';
$this->params[ 'breadcrumbs' ][] = $this->title;
$this->registerJsVar( 'dt_aktiva', $aktiva, \yii\web\View::POS_READY );
$this->registerJsVar( 'dt_pasiva', $pasiva, \yii\web\View::POS_READY );
$this->registerJsVar( 'dt_sales', $sales, \yii\web\View::POS_READY );
$this->registerJsVar( 'dt_pend_ope', $pend_ope, \yii\web\View::POS_READY );
$this->registerJsVar( 'dt_hpp', $hpp, \yii\web\View::POS_READY );
$this->registerJsVar( 'dt_biaya_ope', $biaya_ope, \yii\web\View::POS_READY );
$this->registerJsVar( 'dt_biaya_non', $biaya_non, \yii\web\View::POS_READY );
$this->registerJsVar( 'dt_pend_non_ope', $pend_non_ope, \yii\web\View::POS_READY );
$sqlBank = Yii::$app->db
	->createCommand( "
	SELECT NoAccount AS value, NamaAccount AS label, StatusAccount, NoAccount AS NoAccount FROM traccount 
	WHERE StatusAccount = 'A' AND JenisAccount = 'Header' 
	ORDER BY StatusAccount, NoAccount" )
	->queryAll();
$cmbBank = ArrayHelper::map( $sqlBank, 'value', 'label' );
$format  = <<< SCRIPT
function formatBank(result) {
    return '<div class="row">' +
           '<div class="col-xs-3">' + result.id + '</div>' +
           '<div class="col-xs-21">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape  = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
$dispOptions = [ 'class' => 'form-control kv-monospace' ];
$saveCont = [ 'class' => 'kv-saved-cont' ];
$form          = ActiveForm::begin( [ 'id' => 'form_neraca_awal' ] );
?>
    <div class="panel panel-default" style="height: 525px">
        <div class="box-body no-padding col-sm-14">
            <div class="nav-tabs-custom" style="box-shadow: none;">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs ">
                    <li class="active"><a href="#aktiva" data-toggle="tab">Aktiva</a></li>
                    <li><a href="#pasiva" data-toggle="tab">Pasiva</a></li>
                    <li><a href="#pendapatan-sales" data-toggle="tab">Penjualan</a></li>
                    <li><a href="#pendapatan-operasional" data-toggle="tab">Pendapatan Operasional</a></li>
                    <li><a href="#hpp" data-toggle="tab">HPP</a></li>
                    <li><a href="#biaya-operasional" data-toggle="tab">Biaya Operasional</a></li>
                </ul>
                <div class="tab-content">
                    <div class="chart tab-pane active" id="aktiva">
                        <div id='jqxWidget_10000000'>
                        </div>
                    </div>
                    <div class="chart tab-pane" id="pasiva">
                        <div id='jqxWidget_20000000'>
                        </div>
                    </div>
                    <div class="chart tab-pane" id="pendapatan-sales">
                        <div id='jqxWidget_30000000'>
                        </div>
                    </div>
                    <div class="chart tab-pane" id="pendapatan-operasional">
                        <div id='jqxWidget_40000000'>
                        </div>
                    </div>
                    <div class="chart tab-pane" id="hpp">
                        <div id='jqxWidget_50000000'>
                        </div>
                    </div>
                    <div class="chart tab-pane" id="biaya-operasional">
                        <div id='jqxWidget_60000000'>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-body no-padding col-sm-10">
            <div class="box-body">
                <div class="form-group row">
                    <label class="control-label col-sm-6">Tanggal Awal</label>
                    <div class="col-sm-9">
						<? try {
							echo DateControl::widget( [
								'name'          => 'TglGL',
								'id'            => 'TglGL',
								'type'          => DateControl::FORMAT_DATE,
								'value'         => $tglAwal,
								'pluginOptions' => [
									'autoclose' => true,
								],
							] );
						} catch ( \yii\base\InvalidConfigException $e ) {
						} ?>
                    </div>
                    <label class="control-label col-sm-9">&nbsp;</label>
                </div>
                <div class="form-group row">
                    <label class="control-label col-sm-6">No Jurnal</label>
                    <div class="col-sm-9">
						<?= Html::textInput( 'NoGL', ( $tglAwal == null ? '' : '00NA00000' ), [ 'id' => 'NoGL', 'class' => 'form-control', 'tabindex' => '2' ] ) ?>
                    </div>
                    <label class="control-label col-sm-3">No Item</label>
                    <div class="col-sm-6">
						<?= NumberControl::widget( [
							'name'           => 'tot_item',
							'displayOptions' => $dispOptions,
						] ); ?>
                    </div>
                </div>
                <hr/>
                <div class="form-group row">
                    <label class="control-label col-sm-6">Nomor Perkiraan</label>
                    <div class="col-sm-9">
						<?= Html::textInput( 'NoAccount', '', [ 'id' => 'NoAccount', 'class' => 'form-control', 'tabindex' => '4' ] ) ?>
                    </div>
                    <label class="control-label col-sm-3">Jenis</label>
                    <div class="col-sm-6" style="margin-bottom: 5px">
						<?= Html::dropDownList( 'JenisAccount', null, [
							'Header' => 'Header',
							'Detail' => 'Detail',
						],
							[
								'id'    => 'JenisAccount',
								'class' => 'form-control',
                                'tabindex' => '5'
							] ) ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-sm-6">Nama Perkiraan</label>
                    <div class="col-sm-18">
						<?= Html::textInput( 'NamaAccount', '', [ 'id' => 'NamaAccount', 'class' => 'form-control', 'tabindex' => '6'] ) ?>
                    </div>
                </div>
                <hr/>
                <div class="form-group row">
                    <label class="control-label col-sm-6">Debet</label>
                    <div class="col-sm-18">
						<?= NumberControl::widget( [
							'name'           => 'debet',
							'displayOptions' => $dispOptions,
						] ); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-sm-6">Kredit</label>
                    <div class="col-sm-18">
						<?= NumberControl::widget( [
							'name'           => 'kredit',
							'displayOptions' => $dispOptions,
						] ); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-sm-6">Keterangan</label>
                    <div class="col-sm-18">
						<?= Html::textInput( 'Note', '', [ 'id' => 'note_id', 'class' => 'form-control', 'tabindex' => '9' ] ) ?>
                    </div>
                </div>
                <hr/>
                <div class="form-group row">
                    <label class="control-label col-sm-12 text-center">Total Debet</label>
                    <label class="control-label col-sm-12 text-center">Total Kredit</label>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
						<?= NumberControl::widget( [
							'name'           => 'tot_debet',
							'value'          => $TotalDebet,
							'displayOptions' => $dispOptions,
						] ); ?>
                    </div>
                    <div class="col-sm-12" data-toggle="tooltip" data-placement="bottom" title="Laba Rugi Tahun Berjalan = <?= Yii::$app->formatter->asDecimal($TotalLabaRugi)?>">
						<?= NumberControl::widget( [
							'name'           => 'tot_kredit',
							'value'          => $TotalKredit,
							'displayOptions' => $dispOptions,
						] ); ?>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="pull-right">
					<?= Html::button( '<i class="fa fa-plus-circle"><br><br> Tambah </i>', [ 'class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:60px;height:60px' ]) ?>
					<?= Html::button( '<i class="fa fa-edit"><br><br> Edit </i>', [ 'class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px' ] ) ?>
                    <?= Html::button( '<i class="glyphicon glyphicon-floppy-disk"><br><br> Simpan </i>', [ 'class' => 'btn btn-info btn-primary ', 'id' => 'btnSave','style' => 'width:60px;height:60px' ] ) ?>
                    <?= Html::Button( '<i class="fa fa-ban"><br><br> Batal </i>', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px' ] ) ?>
					<?= Html::button( '<i class="fa fa-ban"><br><br> Hapus </i>', [ 'class' => 'btn btn-danger', 'id' => 'btnDelete', 'style' => 'width:60px;height:60px' ] ) ?>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <div id="dataConfirmModal" class="modal bootstrap-dialog type-warning fade size-normal in" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="bootstrap-dialog-header">
                        <div class="bootstrap-dialog-close-button" style="display: none;">
                            <button class="close" data-dismiss="modal" aria-label="close">×</button>
                        </div>
                        <div class="bootstrap-dialog-title" id="w3_title">Confirmation</div>
                    </div>
                </div>
                <form action="index.php?r=traccount/data-perkiraan-delete" id="delete-form" method="post">
                    <div class="modal-body"></div>
                    <input type="hidden" name="NoAccount"/>
                    <div class="modal-footer">
                        <div class="bootstrap-dialog-footer">
                            <div class="bootstrap-dialog-footer-buttons">
                                <button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><span class="fa fa-ban"></span> Cancel</button>
                                <button class="btn btn-warning" type="submit"><span class="glyphicon glyphicon-ok"></span> Ok</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
$this->registerJs( <<< JS

    function tree(event) {
        var args = event.args;
        // console.log(event);
        var item = $(this).jqxTree('getItem', args.element);
        var d = JSON.parse(item.value);
        $('#NoAccount-old').val(d.NoAccount);
        $('#NoAccount-root').val(args.owner.itemMapping[0].item.id);
        $('#NoAccount').val(d.NoAccount);
        $('#NamaAccount').val(d.NamaAccount);
        $('#JenisAccount').val(d.JenisAccount);
        $('#StatusAccount').val(d.StatusAccount);
        $("#NoParent").val(d.NoParent).trigger('change.select2');
        if (event.type === 'select'){
            $.ajax({
                type: "post",
                url: 'index.php?r=traccount/load-data-account',
                data: {
                    NoAccount: d.NoAccount
                },
                success: function (data) {
                    console.log(data);
                    $('#note_id').val(data.KeteranganGL);
                    $('[name=debet]').utilNumberControl().val(data.DebetGL);
                    $('[name=kredit]').utilNumberControl().val(data.KreditGL);
                }
            });
        }
    }

    function createTree(tree_id, NoAccount, data) {
        var source =
            {
                datatype: "json",
                datafields: [
                    {name: 'id'},
                    {name: 'NoAccount'},
                    {name: 'NamaAccount'},
                    {name: 'OpeningBalance'},
                    {name: 'JenisAccount'},
                    {name: 'StatusAccount'},
                    {name: 'NoParent'},
                    {name: 'LevelAccount'},
                    {name: 'label'},
                    {name: 'value'},
                    {name: 'icon'}
                ],
                id: 'id',
            };
        source.localdata = data;
        const dataAdapter = new $.jqx.dataAdapter(source);
        dataAdapter.dataBind();
        let record = dataAdapter.getRecordsHierarchy('id', 'NoParent', 'items');
        let treeId = $('#' + tree_id);
        treeId.jqxTree({source: record, width: '99%', height: '450px'}); //290px
        treeId.jqxTree('expandItem', $("#" + NoAccount)[0]);
        treeId.on('select', tree);
        treeId.on('itemClick', tree);
    }

    createTree('jqxWidget_10000000', '10000000', dt_aktiva);
    createTree('jqxWidget_20000000', '20000000', dt_pasiva);
    createTree('jqxWidget_30000000', '30000000', dt_sales);
    createTree('jqxWidget_40000000', '40000000', dt_pend_ope);
    createTree('jqxWidget_50000000', '50000000', dt_hpp);
    createTree('jqxWidget_60000000', '60000000', dt_biaya_ope);

    
     btnCreate = $('#btnAdd'),
     btnEdit = $('#btnEdit'),
        btnDelete = $('#btnDelete'),
        btnSave = $('#btnSave'),
        btnCancel = $('#btnCancel'),
        modeDisplay = function(){
            btnCreate.show();
            btnEdit.show();
              btnDelete.show()
                btnSave.hide();
                btnCancel.hide();
        },
        modeSaving = function(){
            btnCreate.hide();
            btnEdit.hide();
              btnDelete.hide()
                btnSave.show();
                btnCancel.show();
        }
    modeDisplay();

     btnCancel.click(function () {
         modeDisplay();
     });
     
   btnCreate.click(function () {
       if($('#NoGL') === '00NA00000'){
            bootbox.alert({message:'Data Neraca Awal Sudah Tersedia, Anda Tidak Dapat Membuat Neraca Awal',size:'small'});
            return ;
        }else{
            bootbox.alert({message:'Silahkan Memasukkan Tanggal Neraca Awal, kemudian Klik Tombol Simpan',size:'small'});
        }
        $('#NoAccount').val("");
        $('#NamaAccount').val("");
        $('#JenisAccount').val("Detail");
        $('#StatusAccount').val("A");
        window.modeSimpan = 'add';
       modeSaving();
    });
   
    btnEdit.click(function () {
        if ($('#JenisAccount').val() === 'Header') return ;
        if ($('#NoAccount').val() === '') return ;
        window.modeSimpan = 'edit';
         modeSaving();
    });
    
    btnSave.click(function () {
        modeDisplay();
        return;
        if (window.modeSimpan === 'add'){
            $('#form_neraca_awal').attr('action','index.php?r=traccount/neraca-awal');
            $('#form_neraca_awal').submit();
        }else{
            $.ajax({
                type: "post",
                url: 'index.php?r=traccount/neraca-awal-update',
                data: $('#form_neraca_awal').serialize(),
                success: function (data) {
                    // var obj = JSON.parse(data);
                    bootbox.alert({size: "small", message: data.msg});
                    if (data.status) {
                        let rootid = $('#NoAccount-root').val();
                        createTree('jqxWidget_' + rootid, rootid, data.record);
                    }
                }
            });
        }
    });
    
    btnDelete.click(function () {
        bootbox.confirm({
            size: "small",
            message: "Are you sure?",
            callback: function (result) {
                if (result) {
                     $('#form_neraca_awal').attr('action','index.php?r=traccount/neraca-awal-delete');
                     $('#form_neraca_awal').submit();
                }
            }
        })
    });
    
    $('[name=TglGL]').utilDateControl().cmp().attr('tabindex', '1');
    $('[name=tot_item').utilNumberControl().cmp().attr('tabindex', '3');
    $('[name=debet').utilNumberControl().cmp().attr('tabindex', '7');
    $('[name=kredit').utilNumberControl().cmp().attr('tabindex', '8');

JS
);