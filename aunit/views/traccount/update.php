<?php
/* @var $this yii\web\View */
/* @var $model aunit\models\Traccount */
$this->title                   = str_replace('Menu','',\aunit\components\TUi::$actionMode).' Data Perkiraan: ' . $model->NoAccount;
$this->params['breadcrumbs'][] = [ 'label' => 'Data Perkiraan', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->NoAccount, 'url' => [ 'view', 'id' => $model->NoAccount ] ];
$this->params['breadcrumbs'][] = str_replace('Menu','',\aunit\components\TUi::$actionMode);
?>
<div class="traccount-update">
	<?= $this->render( '_form', [
		'model' => $model,
		'id'=>$id
	] ) ?>
</div>
