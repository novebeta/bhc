<?php
/* @var $this yii\web\View */
/* @var $model aunit\models\Traccount */
$this->title                   = 'Tambah Data Perkiraan';
$this->params['breadcrumbs'][] = [ 'label' => 'Data Perkiraan', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="traccount-create">
	<?= $this->render( '_form', [
		'model' => $model,
		'id'=>$id
	] ) ?>
</div>
