<?php
use aunit\assets\AppAsset;
use aunit\components\FormField;
use aunit\components\TUi;
use aunit\models\Tdteam;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
AppAsset::register( $this );
$this->title                     = 'Team dan Sales Pass';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [],
	'cmbTgl'    => [],
	'cmbNum'    => [],
	'sortname'  => "SalesTgl",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
$Team = Tdteam::findOne(['TeamStatus'=>'A']);
$queryNilai = Yii::$app->db->createCommand( "SELECT Nilai as value , Nilai as label FROM tdvariabel WHERE Nama = 'PlafonPiutangDP';" )->queryAll();
foreach ($queryNilai as $key){
    $Nilai = $key['value'];}
?>
    <div class="tdteamsales-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'frm_tdteamsales_id' ] );
		TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "team form-group col-md-12",
				    'items' => [
					    'class' => "row col-md-12",
					    'style' => "margin-top: 10px;padding-left:5px;",
					    'items' => '<table id="teamGrid"></table><div id="teamGridPager"></div>'
				    ],
				  ],
				  [ 'class' => "sales form-group col-md-12",
				    'items' => [
					    'class' => "row col-md-12",
					    'style' => "margin-top: 10px;padding-left:5px;",
					    'items' => '<table id="salesGrid"></table><div id="salesGridPager"></div>'
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
                        [ 'class' => "form-group col-md-12",
                            'items' => [
                                [ 'class' => "col-sm-24", 'items' => ":LabelBatas" ],
                                [ 'class' => "col-sm-4", 'items' => ":LabelIndividu" ],
                                [ 'class' => "col-sm-10", 'items' => ":Individu" ],
                                [ 'class' => "col-sm-2", 'items' => ":Hari" ],
                            ],
                        ],
                        [ 'class' => "form-group col-md-12",
                            'items' => [
                                [ 'class' => "col-sm-24", 'items' => ":LabelPlafon" ],
                                [ 'class' => "col-sm-10", 'items' => ":Konsumen" ],
                            ],
                        ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelKolektif" ],
					    [ 'class' => "col-sm-5", 'items' => ":Kolektif" ],
					    [ 'class' => "col-sm-1", 'items' => ":Hari" ],
					    [ 'class' => "pull-right", 'items' => ":btnUpdate" ]
				    ],
				  ],
			  ]
			],
			[],
			[
				":LabelTeamKode" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Team</label>',
				":LabelTeamNama" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sales</label>',
				":LabelBatas"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Batas Waktu Pelunasan Piutang</label>',
				":LabelPlafon"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Plafon Piutang DP Konsumen</label>',
				":LabelIndividu" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Individu</label>',
				":LabelKolektif" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kolektif</label>',
				":Hari"          => '<label class="control-label" style="margin: 0; padding: 6px 0;"> &nbsp;&nbsp;&nbsp;Hari</label>',
				":Individu"      => FormField::integerInput( [ 'name' => 'TeamIndividu', 'config' => [ 'value' => $Team->TeamIndividu ] ] ).
                                    Html::hiddenInput('Team').Html::hiddenInput('Sales'),
				":Konsumen"      => FormField::integerInput( [ 'name' => 'Konsumen', 'config' => [ 'value' => $Nilai  ] ] ),
				":Kolektif"      => FormField::integerInput( [ 'name' => 'TeamKolektif', 'config' => [ 'value' => $Team->TeamKolektif  ] ] ),
				":btnUpdate"     => Html::button( '<i class="glyphicon glyphicon-refresh"></i> Update', [ 'class' => 'btn btn-info btn-warning ', 'id'=>'btnSave' ] ),
//				":btnKeluar"     => Html::button( '<i class="glyphicon glyphicon-ban-circle"></i> Keluar', [ 'class' => 'btn btn-info btn-danger ' ] ),
			]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$url[ 'teamidx' ]  = Url::toRoute( [ 'ttmteamtarget/list' ] );
$url[ 'salesidx' ] = Url::toRoute( [ 'ttmsalestarget/list' ] );
$this->registerJs( <<< JS
    window.firstload = true;
    window.TeamKode = [];
     $('#teamGrid').utilJqGrid({
        height: 320,
        navButtonTambah: false,
        extraParams: {},
        rownumbers: false,       
        multiselect: true,
        shrinkToFit : true,
        autowidth: true,
        loadonce:true,
        rowNum: 1000,
        colModel: [
            {
                name: 'TeamNama',
                label: 'Team',
                editable: true,
                editor : {
                    type: 'text',
                    readOnly: true
                }
            },    
        ], 
        listeners: {
            afterLoad: function (a,b) {
                if (Array.isArray(b.rows)){
                    let i=0;
                    while(i < b.rows.length) {
                      if (b.rows[i].TeamPass === '1'){
                          $('#teamGrid').setSelection(b.rows[i].id);
                      }
                      i++;
                    }
                }
                window.firstload = false;
                $('#salesGrid').utilJqGrid().load({url: '{$url['salesidx']}',param:{ TeamKode: ($('#teamGrid').utilJqGrid().selected.getArrayId()).join(',') } });
            }    
        },
        onSelectRow: function(id, rowid){
            if (window.firstload) return;
            $('#salesGrid').utilJqGrid().load({url: '{$url['salesidx']}',param:{ TeamKode: ($('#teamGrid').utilJqGrid().selected.getArrayId()).join(',') } });
        }, 
        onSelectAll: function(id,status){
            $('#salesGrid').utilJqGrid().load({url: '{$url['salesidx']}',param:{ TeamKode: ($('#teamGrid').utilJqGrid().selected.getArrayId()).join(',') } });
        }      
     }).init().fit($('.team')).load({url: '{$url['teamidx']}'});
     
     $('#salesGrid').utilJqGrid({
        height: 320,
        navButtonTambah: false,
        extraParams: {
            TeamKode: $('#teamGrid').utilJqGrid().selected.getArrayId()
        },
        rownumbers: false,       
        multiselect: true,
        shrinkToFit : true,
        autowidth: true,
        loadonce:true,
        rowNum: 1000,
        colModel: [
            {
                name: 'SalesNama',
                label: 'Sales',
                editable: true,
                editor : {
                    type: 'text',
                    readOnly: true
                }
            },    
        ],
        listeners: {
            afterLoad: function (a,b) {
                if (Array.isArray(b.rows)){
                    let i=0;
                    while(i < b.rows.length) {
                      if (b.rows[i].SalesPass === '1'){
                          $('#salesGrid').setSelection(b.rows[i].id);
                      }
                      i++;
                    }
                }
            }    
        },      
     }).init().fit($('.sales')).load({url: '{$url['salesidx']}'});
     
      $('#btnSave').click(function (event) {
          $('[name=Team]').val($('#teamGrid').utilJqGrid().selected.getArrayId().join(','));
          $('[name=Sales]').val($('#salesGrid').utilJqGrid().selected.getArrayId().join(','));
        $('#frm_tdteamsales_id').submit();
    });
      
      $('[name=TeamIndividu').utilNumberControl().cmp().attr('tabindex', '1');
      $('[name=TeamKolektif').utilNumberControl().cmp().attr('tabindex', '2');
     
JS
);
