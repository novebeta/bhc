<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttmsalestarget */

$this->title = 'Update Ttmsalestarget: ' . $model->SalesTgl;
$this->params['breadcrumbs'][] = ['label' => 'Ttmsalestargets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->SalesTgl, 'url' => ['view', 'SalesTgl' => $model->SalesTgl, 'SalesKode' => $model->SalesKode, 'TeamKode' => $model->TeamKode]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ttmsalestarget-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
