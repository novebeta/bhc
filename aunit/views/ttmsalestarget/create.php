<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttmsalestarget */

$this->title = 'Create Ttmsalestarget';
$this->params['breadcrumbs'][] = ['label' => 'Ttmsalestargets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttmsalestarget-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
