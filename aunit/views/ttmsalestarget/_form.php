<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttmsalestarget */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ttmsalestarget-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'SalesTgl')->textInput() ?>

    <?= $form->field($model, 'SalesKode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TeamKode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SalesQty')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
