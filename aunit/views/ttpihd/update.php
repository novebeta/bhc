<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttpihd */

$params                          = '&id=' . $id . '&action=update';
$cancel = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = 'Edit - Invoice Pembelian: ' . $dsTBeli['PINoView'];
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Invoice Pembelian', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = 'Edit';

?>
<div class="ttpihd-update">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTBeli' => $dsTBeli,
        'id'     => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail'       => Url::toRoute( [ 'ttpiit/index','action' => 'update' ] ),
        ]
    ]) ?>

</div>
