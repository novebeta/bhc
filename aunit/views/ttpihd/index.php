<?php
use aunit\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = '[PI] Invoice Pembelian';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'PINo'         => 'No PI',
		'PIJenis'      => 'Jenis',
		'PSNo'         => 'No PS',
		'KodeTrans'    => 'Kode Trans/TC',
		'PosKode'      => 'POS',
		'SupKode'      => 'Supplier',
		'LokasiKode'   => 'Lokasi',
		'PINoRef'      => 'No Ref',
		'PIKeterangan' => 'Keterangan',
		'NoGL'         => 'No GL',
		'Cetak'        => 'Cetak',
		'UserID'       => 'UserID',
	],
	'cmbTgl'    => [
		'PITgl'      => 'Tgl PI',
		'PITglTempo' => 'Tgl Tempo',
	],
	'cmbNum'    => [
		'PITerm'      => 'Term',
		'PISubTotal'  => 'Sub Total',
		'PIDiscFinal' => 'Disc Final',
		'PITotal'     => 'Total',
		'PITerbayar'  => 'Terbayar',
	],
	'sortname'  => "PITgl",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttpihd::className(), '[PI] Invoice Pembelian', [
		'url_delete' => Url::toRoute( [ 'ttpihd/delete' ] ),
	]
), \yii\web\View::POS_READY );
