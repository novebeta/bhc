<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttpihd */

$params                          = '&id=' . $id . '&action=create';
$cancel = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = 'Tambah - Invoice Pembelian';
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Invoice Pembelian', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = $this->title;

?>
<div class="ttpihd-create">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTBeli' => $dsTBeli,
        'id'     => $id,
        'url'    => [
			'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
			'cancel' => $cancel,
            'detail'       => Url::toRoute( [ 'ttpiit/index','action' => 'update' ] ),
		]
    ]) ?>

</div>
