<?php
use aunit\components\FormField;
use aunit\models\Tdlokasi;
use common\components\Custom;
use kartik\datecontrol\DateControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Trmp */
/* @var $form yii\widgets\ActiveForm */
$format     = <<< SCRIPT
function formattrmp(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-10" style="text-align: left">' + result.id + '</div>' +
           '<div class="col-xs-14">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape     = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="trmp-form">
        <?php
        $form = ActiveForm::begin( [ 'id' => 'form_trmp_id', 'action' => $url[ 'update' ] ] );
        \aunit\components\TUi::form(
            [
                'class' => "row-no-gutters",
                'items' => [
                    [ 'class' => "form-group col-md-24",
                        'items' => [
                            [ 'class' => "col-sm-2", 'items' => ":LabelMPNo" ],
                            [ 'class' => "col-sm-3", 'items' => ":MPNo" ],
                            [ 'class' => "col-sm-1" ],
                            [ 'class' => "col-sm-1", 'items' => ":LabelMPTgl" ],
                            [ 'class' => "col-sm-3", 'items' => ":MPTgl" ],
                            [ 'class' => "col-sm-7" ],
                            [ 'class' => "col-sm-2", 'items' => ":LabelMPSaldoAwal" ],
                            [ 'class' => "col-sm-5", 'items' => ":MPSaldoAwal" ],
                        ],
                    ],
                    [ 'class' => "form-group col-md-24",
                        'items' => [
                            [ 'class' => "col-sm-2", 'items' => ":LabelMPMemo" ],
                            [ 'class' => "col-sm-14", 'items' => ":MPMemo" ],
                            [ 'class' => "col-sm-1" ],
                            [ 'class' => "col-sm-2", 'items' => ":LabelMPUmur" ],
                            [ 'class' => "col-sm-2", 'items' => ":MPUmur" ],
                        ],
                    ],
                    [ 'class' => "col-md-24",
                        'items' => [
                            [ 'class' => "form-group col-md-4", 'items' => ":LabelMPAwal" ],
                            [ 'class' => "form-group col-md-4", 'items' => ":MPAwal" ],
                            [ 'class' => "form-group col-md-1" ],
                            [ 'class' => "form-group col-md-2", 'items' => ":LabelMPCOADebet" ],
                            [ 'class' => "form-group col-md-13", 'items' => ":MPCOADebet" ],
                        ]
                    ],
                    [ 'class' => "col-md-24",
                        'items' => [
                            [ 'class' => "form-group col-md-4", 'items' => ":LabelMPNext" ],
                            [ 'class' => "form-group col-md-4", 'items' => ":MPNext" ],
                            [ 'class' => "form-group col-md-1" ],
                            [ 'class' => "form-group col-md-2", 'items' => ":LabelMPCOAKredit" ],
                            [ 'class' => "form-group col-md-13", 'items' => ":MPCOAKredit" ],
                        ]
                    ],
                    [ 'class' => "col-md-24",
                        'items' => [
                            [ 'class' => "form-group col-md-4", 'items' => ":LabelMulaiMPTgl" ],
                            [ 'class' => "form-group col-md-4", 'items' => ":MulaiMPTgl" ],
                            [ 'class' => "form-group col-md-1" ],
                            [ 'class' => "form-group col-md-2", 'items' => ":LabelMPStatus" ],
                            [ 'class' => "form-group col-md-2", 'items' => ":MPStatus" ],
                            [ 'class' => "form-group col-md-1" ],
                            [ 'class' => "form-group col-md-2", 'items' => ":LabelMPSaldoNow" ],
                            [ 'class' => "form-group col-md-8", 'items' => ":MPSaldoNow" ],
                        ]
                    ],
                    [ 'class' => "row col-md-24", 'style' => "margin-bottom: 3px;", 'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>' ],

                ],
            ],
            [ 'class' => "row-no-gutters",
                'items' => [
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'trmp.MPNo'         => 'Mp No',
                                'MPTgl'        => 'Mp Tgl',
                                'MPMemo'       => 'Mp Memo',
                                'MPSaldoAwal'  => 'Saldo Awal',
                                'MPUmur'       => 'Umur',
                                'MPAwal'       => 'MP Awal',
                                'MPNext'       => 'MP Selanjutnya',
                                'MPCOADebet'   => 'MP COA Debet',
                                'MPCOAKredit'  => 'MP COA Kredit',
                                'MPTglMulai'   => 'MP Tgl Mulai',
                                'MPSaldoNow'   => 'MP Saldo Sekarang',
                                'MPStatus'     => 'MP Status',
                                'UserID'       => 'USer ID',
                            ],
                        ])
                    ],
                    ['class' => "pull-right", 'items' => ":btnPrint :btnAction"],
                ],
            ],
            [
                ":LabelMPNo"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">No MP</label>',
                ":LabelMPTgl"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl MP</label>',
                ":LabelMPSaldoAwal"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nilai Saldo Awal</label>',
                ":LabelMPMemo" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
                ":LabelMPUmur"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Umur Penyusutan</label>',
                ":LabelMPAwal"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nilai Penyusutan Awal</label>',
                ":LabelMPCOADebet"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">COA Debet</label>',
                ":LabelMPCOAKredit"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">COA Kredit</label>',
                ":LabelMPNext"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nilai Penyusutan Selanjutnya</label>',
                ":LabelMulaiMPTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tanggal Mulai Penyusutan</label>',
                ":LabelMPStatus"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',
                ":LabelMPSaldoNow"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Saldo Sekarang</label>',

                ":MPNo"             => Html::textInput( 'MPNoView', $dsMp[ 'MPNoView' ], [ 'class' => 'form-control', 'tabindex' => '1' ] ) .
                                        Html::textInput( 'MPNo', $dsMp[ 'MPNo' ], [ 'class' => 'hidden' ] ),
                ":MPTgl"            => FormField::dateInput( [ 'name' => 'MPTgl', 'config' => [ 'value' => $dsMp[ 'MPTgl' ] ] ] ),
                ":MPSaldoAwal"      => FormField::decimalInput( [ 'name' => 'MPSaldoAwal', 'config' => [ 'value' => $dsMp[ 'MPSaldoAwal' ] ] ] ),
                ":MPUmur"           => FormField::decimalInput( [ 'name' => 'MPUmur', 'config' => [ 'value' => $dsMp[ 'MPUmur' ] ] ] ),
                ":MPAwal"           => FormField::decimalInput( [ 'name' => 'MPAwal', 'config' => [ 'value' => $dsMp[ 'MPAwal' ] ] ] ),
                ":MPNext"           => FormField::decimalInput( [ 'name' => 'MPNext', 'config' => [ 'value' => $dsMp[ 'MPNext' ] ] ] ),
                ":MPSaldoNow"           => FormField::decimalInput( [ 'name' => 'MPSaldoNow', 'config' => [ 'value' => $dsMp[ 'MPSaldoNow' ] ] ] ),
                ":MPMemo"            => Html::textInput( 'MPMemo', $dsMp[ 'MPMemo' ], [ 'class' => 'form-control' ] ),
                ":MPStatus"            => FormField::combo( 'StatusMP', [ 'name' => 'MPStatus', 'config' => [ 'value' => $dsMp[ 'MPStatus' ] ],  'extraOptions' => [ 'simpleCombo' => true ] ] ),
                ":MulaiMPTgl"             => FormField::dateInput( [ 'name' => 'MPTglMulai', 'config' => [ 'value' => $dsMp[ 'MPTglMulai' ] ] ] ),

                ":MPCOADebet"        => FormField::combo( 'NoAccount', [ 'name' => 'MPCOADebet','config' => [ 'value' => $dsMp[ 'MPCOADebet' ] ], 'extraOptions' => [ 'altLabel' => [ 'NamaAccount' ],] ] ),
                ":MPCOAKredit"        => FormField::combo( 'NoAccount', [  'name' => 'MPCOAKredit','config' => [ 'value' => $dsMp[ 'MPCOAKredit' ] ], 'extraOptions' => [ 'altLabel' => [ 'NamaAccount' ],] ] ),


                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
            ],
            [
                '_akses' => 'Memorial Penyusutan',
                'url_main' => 'trmp',
                'url_id' => $_GET['id'] ?? '',
                'url_delete'        => Url::toRoute(['trmp/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
        );
        ActiveForm::end();
        ?>
    </div>
<?php
$urlAdd    = Url::toRoute( [ 'trmp/create', 'action' => 'create' ] );
$urlIndex  = Url::toRoute( [ 'trmp/index' ] );
$urlPrint  = $url[ 'print' ];
$urlDetail = $url[ 'detail' ];
$readOnly      = ( ($_GET[ 'action' ] == 'view' || $_GET[ 'action' ] == 'display') ? 'true' : 'false' );
//$DtAccount                  = \common\components\General::cCmd( "SELECT NoAccount, NamaAccount, OpeningBalance, JenisAccount, StatusAccount, NoParent FROM traccount WHERE JenisAccount = 'Detail' ORDER BY NoAccount" )->queryAll();
$DtAccount                  = \common\components\General::cCmd( "SELECT  ttgeneralledgerhd.NoGL, ttgeneralledgerhd.TglGL , ttgeneralledgerit.NoAccount,NamaAccount, DebetGL, KreditGL
			FROM ttgeneralledgerhd 
			INNER JOIN ttgeneralledgerit ON ttgeneralledgerhd.NoGL = ttgeneralledgerit.NoGL
			INNER JOIN traccount ON ttgeneralledgerit.NoAccount = traccount.NoAccount
			INNER JOIN trmp ON trmp.MPNo = ttgeneralledgerhd.GLLink" )->queryAll();

$this->registerJsVar( '__dtAccount', json_encode( $DtAccount )  );
$this->registerJs( <<< JS
     var __NoAccount = JSON.parse(__dtAccount);
     __NoAccount = $.map(__NoAccount, function (obj) {
                      obj.id = obj.NoAccount;
                      obj.text = obj.NoAccount;
                      return obj;
                    });
     var __NamaAccount = JSON.parse(__dtAccount);
     __NamaAccount = $.map(__NamaAccount, function (obj) {
                      obj.id = obj.NamaAccount;
                      obj.text = obj.NamaAccount;
                      return obj;
                    });
     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 190,
        extraParams: {
            MPNo: $('input[name="MPNo"]').val()
        },
        rownumbers: true,  
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,     
        colModel: [
            { template: 'actions'  },
            {
                name: 'NoGL',
                label: 'No JT',
                width: 150,
                editable: true,
            },
            {
                name: 'TglGL',
                label: 'Tgl JT',
                width: 150,
                editable: true,
            },
            {
                name: 'NoAccount',
                label: 'No Account',
                editable: true,
                width: 173,
                editor: {
                    type: 'select2',
                    data: __NoAccount,  
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowid = $(this).attr('rowid');
                            $('[id="'+rowid+'_'+'NamaAccount'+'"]').val(data.NamaAccount); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'NamaAccount'+'"]').trigger('change');
						}
                    }                  
                }
            },
            {
                name: 'NamaAccount',
                label: 'Nama Account',
                width: 250,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __NamaAccount, 
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowid = $(this).attr('rowid');
                            $('[id="'+rowid+'_'+'NoAccount'+'"]').val(data.NoAccount); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'NoAccount'+'"]').trigger('change');
						}
                    }                              
                }
            },
            
            {
                name: 'DebetGL',
                label: 'Debit',
                width: 180,
                editable: true,
                template: 'money'
            },
            {
                name: 'KreditGL',
                label: 'Kredit',
                width: 180,
                editable: true,
                template: 'money'
            },
        ], 
        listeners: {
            afterLoad: function(){
                
            }
        },
     }).init().fit($('.box-body')).load({url: '$urlDetail'});
	
	$('#btnSave').click(function (event) {
        $('input[name="SaveMode"]').val('ALL');
        $('#form_trmp_id').attr('action','{$url['update']}');
        $('#form_trmp_id').submit();
    }); 
	
	$('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  });
	
	$('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
	
	$('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  });
	
	$('#detailGrid').bind("jqGridInlineEditRow", function (e, rowId, orgClickEvent) {
    });
	
	$('#btnCancel').click(function (event) {	      
       $('#form_trmp_id').attr('action','{$url['cancel']}');
       $('#form_trmp_id').submit();
    });
    
    function Hitung(){
        let MPSaldoAwal = $('#MPSaldoAwal').utilNumberControl().val();
        let MPUmur = $('#MPUmur').utilNumberControl().val();
        let Angsuran = MPSaldoAwal / MPUmur;        
        let MPAwal = MPSaldoAwal - (Angsuran * (MPUmur - 1));
        let MPNext = Angsuran;
        console.log(MPSaldoAwal);
        console.log(Angsuran);
        console.log(MPAwal);
        console.log(MPNext);
        $('#MPAwal').utilNumberControl().val(MPAwal);
        $('#MPNext').utilNumberControl().val(MPNext);
    }
    
    $('#MPUmur').change(function (){
	      Hitung();
	  });
    
    
	
	

JS
);