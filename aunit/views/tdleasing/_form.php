<?php
use aunit\models\Tdmotortype;
use kartik\datecontrol\DateControl;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tdleasing */
/* @var $form yii\widgets\ActiveForm */
//$url['cancel'] = Url::toRoute('tdleasing/index');
?>

<div class="tdleasing-form">
    <?php
    $form = ActiveForm::begin(['id' => 'frm_leasing_id']);
    \aunit\components\TUi::form(
        ['class' => "row-no-gutters",
            'items' => [
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelLeaseKode"],
                        ['class' => "col-sm-5",'items' => ":LeaseKode"],
                        ['class' => "col-sm-7"],
                        ['class' => "col-sm-2",'items' => ":LabelLeaseMO"],
                        ['class' => "col-sm-3",'items' => ":LeaseMO"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelLeaseNama"],
                        ['class' => "col-sm-11",'items' => ":LeaseNama"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelLeaseContact"],
                        ['class' => "col-sm-8",'items' => ":LeaseContact"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelLeaseAlamat"],
                        ['class' => "col-sm-11",'items' => ":LeaseAlamat"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelLeaseKota"],
                        ['class' => "col-sm-8",'items' => ":LeaseKota"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelLeaseTelepon"],
                        ['class' => "col-sm-8", 'items' => ":LeaseTelepon"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-1",'items' => ":LabelLeaseFax"],
                        ['class' => "col-sm-5",'items' => ":LeaseFax"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-1",'items' => ":LabelLeaseEmail"],
                        ['class' => "col-sm-5",'items' => ":LeaseEmail"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelLeaseKeterangan"],
                        ['class' => "col-sm-11",'items' => ":LeaseKeterangan"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelLeaseStatus"],
                        ['class' => "col-sm-8",'items' => ":LeaseStatus"],
                    ],
                ],
            ]
        ],
        ['class' => "row-no-gutters",
            'items'  => [
                [
                    'class' => "col-md-12 pull-left",
                    'items' => $this->render( '../_nav', [
                        'url'=> $_GET['r'],
                        'options'=> [
                            'tdleasing.LeaseNama' => 'Nama Leasing',
                            'tdleasing.LeaseKode' => 'Kode Leasing',
                            'tdleasing.LeaseContact' => 'Kontak Person',
                            'tdleasing.LeaseEmail' => 'Email',
                            'tdleasing.LeaseKeterangan' => 'Keterangan',
                            'tdleasing.LeaseAlamat' => 'Alamat',
                            'tdleasing.LeaseKota' => 'Kota',
                            'tdleasing.LeaseTelepon' => 'Telepon',
                            'tdleasing.LeaseFax' => 'Fax',
                        ],
                    ])
                ],
                ['class' => "col-md-12 pull-right",
                    'items'  => [
                        ['class' => "pull-right", 'items' => ":btnAction"],
                    ],
                ],
            ],
        ],
        [
            ":LabelLeaseKode"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode Leasing</label>',
            ":LabelLeaseNama"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Leasing</label>',
            ":LabelLeaseContact"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kontak</label>',
            ":LabelLeaseAlamat"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
            ":LabelLeaseKota"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kota</label>',
            ":LabelLeaseTelepon"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon</label>',
            ":LabelLeaseFax"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Fax</label>',
            ":LabelLeaseEmail"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Email</label>',
            ":LabelLeaseKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
            ":LabelLeaseStatus"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',
            ":LabelLeaseMO"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">MO</label>',

            ":LeaseMO"        => $form->field($model, 'LeaseMO', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
            ":LeaseKode"        => $form->field($model, 'LeaseKode', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
            ":LeaseNama"        => $form->field($model, 'LeaseNama', ['template' => '{input}'])->textInput(['maxlength' => true]),
            ":LeaseContact"     => $form->field($model, 'LeaseContact', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
            ":LeaseAlamat"      => $form->field($model, 'LeaseAlamat', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
            ":LeaseKota"        => $form->field($model, 'LeaseKota', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
            ":LeaseTelepon"     => $form->field($model, 'LeaseTelepon', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
            ":LeaseFax"         => $form->field($model, 'LeaseFax', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
            ":LeaseEmail"       => $form->field($model, 'LeaseEmail', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
            ":LeaseKeterangan"  => $form->field($model, 'LeaseKeterangan', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
            ":LeaseStatus"      => $form->field($model, 'LeaseStatus', ['template' => '{input}'])
                                    ->dropDownList([
                                        'A' => 'AKTIF',
                                        'N' => 'NON AKTIF',
                                    ], ['maxlength' => true]),

            ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
            ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
            ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
            ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
            ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),

        ],
        [
            '_akses' => 'Leasing',
            'url_main' => 'tdleasing',
            'url_id' => $_GET['id'] ?? '',
        ]
    );
    ActiveForm::end();


    $urlAdd   = Url::toRoute( [ 'tdleasing/create','id' => 'new','action'=>'create']);
    $urlIndex   = Url::toRoute( [ 'tdleasing/index' ] );
    $this->registerJs( <<< JS
     
    $('#btnAdd').click(function (event) {	      
        window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });  

     $('#btnCancel').click(function (event) {	  
         window.location.href = '{$url['cancel']}';
    });

     $('#btnSave').click(function (event) {	      
//       $('#frm_leasing_id').attr('action','{$url['update']}');
       $('#frm_leasing_id').submit();
    });    
     
     
     
     
JS);

    ?>
</div>


