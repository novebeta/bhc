<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tdleasing */
$this->title                   = str_replace('Menu','',\aunit\components\TUi::$actionMode).'Leasing: ' . $model->LeaseKode;
$this->params['breadcrumbs'][] = [ 'label' => 'Leasing', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->LeaseKode, 'url' => [ 'view', 'id' => $model->LeaseKode ] ];
$this->params['breadcrumbs'][] = 'Edit';
$params = '&id='.$id;
?>
<div class="tdleasing-update">
	<?= $this->render( '_form', [
		'model' => $model,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdleasing/update', 'id' => $id ] ),
            'cancel' => Url::toRoute( [ 'tdleasing/cancel', 'id' => $id ,'action' => 'create'] )
        ]
//        'url' => [
//            'save'    => Custom::url(\Yii::$app->controller->id.'/update'.$params ),
//            'update'    => Custom::url(\Yii::$app->controller->id.'/update' ),
//            'cancel'    => Custom::url(\Yii::$app->controller->id.'/' ),
//        ]
	] ) ?>
</div>
