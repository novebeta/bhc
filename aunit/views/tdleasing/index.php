<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel aunit\models\TdleasingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
use aunit\assets\AppAsset;
AppAsset::register($this);
$this->title = 'Leasing';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
        'LeaseNama' => 'Nama Leasing',
        'LeaseKode' => 'Kode Leasing',
		'LeaseContact' => 'Kontak Person',
        'LeaseEmail' => 'Email',
        'LeaseKeterangan' => 'Keterangan',
        'LeaseAlamat' => 'Alamat',
		'LeaseKota' => 'Kota',
		'LeaseTelepon' => 'Telepon',
		'LeaseFax' => 'Fax',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
	'sortname'  => "LeaseStatus asc,LeaseKode asc,LeaseNama",
	'sortorder' => 'asc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Tdleasing::className() ,'Leasing'), \yii\web\View::POS_READY );