<?php
use yii\helpers\Url;
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttin */

$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode).'- Inden: ' . $dsJual[ 'INNoView' ];
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Inden', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = 'Edit';
?>
<div class="ttin-update">
	<?= $this->render( '_form', [
		'id'=>$id,
		'dsJual' => $dsJual,
        'url' => [
            'update'    => Custom::url(\Yii::$app->controller->id.'/update'.$params ),
            'print'     => Custom::url(\Yii::$app->controller->id.'/print'.$params ),
            'cancel'    => $cancel,
            'detail'    => Custom::url(\Yii::$app->controller->id.'/detail' )
        ]
	] ) ?>
</div>
