<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Inden';
$this->params['breadcrumbs'][] = $this->title;
$arr = [
	'cmbTxt'    => [
        'ttin.INNo'                          => 'No Inden',
        'tdcustomer.CusNama'            => 'Nama Konsumen',
        'ttin.DKNo'                          => 'No DK',
        'ttin.SalesKode'                     => 'Kode Sales',
        'tdsales.SalesNama'             => 'Nama Sales',
        'tmotor.MotorType'              => 'Type Motor',
        'tmotor.MotorWarna'             => 'Warna Motor',
		'LeaseKode'                     => 'Leasing',
		'INJenis'                       => 'Jenis Inden',
		'INMemo'                        => 'Keterangan',
		'UserID'                        => 'User ID',
		'CusKode'                       => 'Kode Konsumen',
		'tdcustomer.CusAlamat'          => 'Alamat Konsumen',
		'tdcustomer.CusRT'              => 'RT Konsumen',
		'tdcustomer.CusRW'              => 'RW Konsumen',
		'tdcustomer.CusKelurahan'       => 'Kelurahan',
		'tdcustomer.CusKecamatan'       => 'Kecamatan',
		'tdcustomer.CusKabupaten'       => 'Kabupaten',
		'tdcustomer.CusTelepon'         => 'Telepon1',
		'tdcustomer.CusTelepon2'        => 'Telepon2',
		'tdleasing.LeaseNama'           => 'Nama Leasing',
	],
	'cmbTgl'    => [
		'INTgl' => 'Tanggal IN'
	],
	'cmbNum'    => [
		'INDP'    => 'DP Inden', 
		'INHarga' => 'Harga OTR', 
	],
	'sortname'  => "INTgl",
	'sortorder' => 'desc'
];
use aunit\assets\AppAsset;
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttin::className(),
	'Inden' ,[ 'mode' => isset($mode) ? $mode : '']), \yii\web\View::POS_READY );
