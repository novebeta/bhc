<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttin */
$cancel = Url::toRoute( [ 'ttin/cancel','id'=>$id, 'action' => 'create' ] );
$this->title                     = 'Tambah Inden';
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Inden', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="ttin-create">
	<?= $this->render( '_form', [
		'id'=>$id,
		'dsJual' => $dsJual,
		'url'    => [
			'update' => Url::toRoute( [ 'ttin/update','id'=>$id, 'action' => 'create' ] ),
			'print'  => Url::toRoute( [ 'ttin/print', 'action' => 'print' ] ),
			'cancel' => $cancel
		]
	] ) ?>
</div>
