<?php
use aunit\components\FormField;
use common\components\Custom;
use kartik\datecontrol\DateControl;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttin */
/* @var $form yii\widgets\ActiveForm */
?>
    <div class="ttin-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'frm_ttin_id', 'action' => $url[ 'update' ],  'fieldConfig' => [ 'template' => "{input}" ] ] );
		\aunit\components\TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [
					  'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelINNo" ],
						  [ 'class' => "col-sm-6", 'items' => ":INNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelSalesKode" ],
						  [ 'class' => "col-sm-7", 'items' => ":SalesKode" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelDKNo" ],
						  [ 'class' => "col-sm-4", 'items' => ":DKNo" ],
					  ],
				  ],
				  [
					  'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelINTgl" ],
						  [ 'class' => "col-sm-6", 'items' => ":INTgl" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelLeaseKode" ],
						  [ 'class' => "col-sm-13", 'items' => ":LeaseKode" ],
					  ],
				  ],
				  [ 'class' => "row col-md-24", 'items' => "<hr>" ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusNama" ],
					    [ 'class' => "col-sm-6", 'items' => ":CusNama" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusKTP" ],
					    [ 'class' => "col-sm-4", 'items' => ":CusKTP" ],
					    [ 'class' => "col-sm-1", 'items' => ":BtnSearchFB" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusKabupaten" ],
					    [ 'class' => "col-sm-5", 'items' => ":CusKabupaten" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusAlamat" ],
					    [ 'class' => "col-sm-6", 'items' => ":CusAlamat" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusRT" ],
					    [ 'class' => "col-sm-2", 'items' => ":CusRT" ],
					    [ 'class' => "col-sm-1", 'style' => "text-align:center;", 'items' => ":Slash" ],
					    [ 'class' => "col-sm-2", 'items' => ":CusRW" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusKecamatan" ],
					    [ 'class' => "col-sm-5", 'items' => ":CusKecamatan" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusTelepon" ],
					    [ 'class' => "col-sm-2", 'style' => "padding-right: 1px;", 'items' => ":CusTelepon1" ],
					    [ 'class' => "col-sm-2", 'style' => "padding-left: 1px;", 'items' => ":CusTelepon2" ],
					    [ 'class' => "col-sm-2", 'style' => "padding-left: 1px;", 'items' => ":JenisTelepon" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusKodePos" ],
					    [ 'class' => "col-sm-5", 'items' => ":CusKodePos" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusKelurahan" ],
					    [ 'class' => "col-sm-5", 'items' => ":CusKelurahan" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusEmail" ],
					    [ 'class' => "col-sm-6", 'items' => ":CusEmail" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelStatusRumah" ],
					    [ 'class' => "col-sm-5", 'items' => ":CusStatusRumah" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelKodeKons" ],
					    [ 'class' => "col-sm-5", 'items' => ":KodeKons" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusSex" ],
					    [ 'class' => "col-sm-3", 'items' => ":CusSex" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-3", 'items' => ":LabelCusTempatLhr" ],
					    [ 'class' => "col-sm-4", 'items' => ":CusTempatLhr" ],
					    [ 'class' => "col-sm-1", 'style' => "text-align:center;", 'items' => ":Minus" ],
					    [ 'class' => "col-sm-2", 'items' => ":CusTglLhr" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusAgama" ],
					    [ 'class' => "col-sm-5", 'items' => ":CusAgama" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusPekerjaan" ],
					    [ 'class' => "col-sm-6", 'items' => ":CusPekerjaan" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusPendidikan" ],
					    [ 'class' => "col-sm-5", 'items' => ":CusPendidikan" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusPengeluaran" ],
					    [ 'class' => "col-sm-5", 'items' => ":CusPengeluaran" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusKK" ],
					    [ 'class' => "col-sm-6", 'items' => ":CusKK" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelCusNPWP" ],
					    [ 'class' => "col-sm-5", 'items' => ":CusNPWP" ],
					    //					    [ 'class' => "col-sm-1" ],
					    //					    [ 'class' => "col-sm-2", 'items' => ":LabelCusPengeluaran" ],
					    //					    [ 'class' => "col-sm-5", 'items' => ":CusPengeluaran" ],
				    ],
				  ],
				  [ 'class' => "row col-md-24", 'items' => "<hr>" ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelMotorType" ],
					    [ 'class' => "col-sm-6", 'items' => ":TypeMotor" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelMotorWarna" ],
					    [ 'class' => "col-sm-5", 'items' => ":WarnaMotor" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelMotorHrgJual" ],
					    [ 'class' => "col-sm-5", 'items' => ":MotorHrgJual" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelNote" ],
					    [ 'class' => "col-sm-14", 'items' => ":Note" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelUangMuka" ],
					    [ 'class' => "col-sm-5", 'items' => ":UangMuka" ],
				    ],
				  ],
			  ],
			],
            [ 'class' => "row-no-gutters",
                'items' => [
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'ttin.INNo'                          => 'No Inden',
                                'tdcustomer.CusNama'            => 'Nama Konsumen',
                                'DKNo'                          => 'No DK',
                                'SalesKode'                     => 'Kode Sales',
                                'tdsales.SalesNama'             => 'Nama Sales',
                                'tmotor.MotorType'              => 'Type Motor',
                                'tmotor.MotorWarna'             => 'Warna Motor',
                                'LeaseKode'                     => 'Leasing',
                                'INJenis'                       => 'Jenis Inden',
                                'INMemo'                        => 'Keterangan',
                                'UserID'                        => 'User ID',
                                'CusKode'                       => 'Kode Konsumen',
                                'tdcustomer.CusAlamat'          => 'Alamat Konsumen',
                                'tdcustomer.CusRT'              => 'RT Konsumen',
                                'tdcustomer.CusRW'              => 'RW Konsumen',
                                'tdcustomer.CusKelurahan'       => 'Kelurahan',
                                'tdcustomer.CusKecamatan'       => 'Kecamatan',
                                'tdcustomer.CusKabupaten'       => 'Kabupaten',
                                'tdcustomer.CusTelepon'         => 'Telepon1',
                                'tdcustomer.CusTelepon2'        => 'Telepon2',
                                'tdleasing.LeaseNama'           => 'Nama Leasing',
                            ],
                        ])
                    ],
                    ['class' => "pull-right", 'items' => ":btnPrint :btnAction"],
                ]
            ],
			[
				":LabelINNo"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor</label>',
				":LabelINTgl"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tanggal</label>',
				":LabelSalesKode"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sales</label>',
				":LabelLeaseKode"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Leasing</label>',
				":LabelDKNo"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">No DK</label>',
				":LabelCusNama"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Konsumen</label>',
				":LabelCusAlamat"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
				":LabelCusTelepon"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon 1- 2</label>',
				":LabelCusEmail"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Email</label>',
				":LabelCusSex"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis Kelamin</label>',
				":LabelCusPekerjaan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pekerjaan</label>',
				":LabelCusKTP"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">KTP</label>',
				":LabelCusRT"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">RT / RW</label>',
				":LabelCusKodePos"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode Pos</label>',
				":LabelCusTempatLhr"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tempat - Tgl Lahir</label>',
				":LabelCusPendidikan"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pendidikan</label>',
				":LabelCusKabupaten"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kabupaten</label>',
				":LabelCusKecamatan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kecamatan</label>',
				":LabelCusKelurahan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kelurahan</label>',
				":LabelCusAgama"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Agama</label>',
				":LabelCusPengeluaran" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pengeluaran</label>',
				":LabelMotorType"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Type Motor</label>',
				":LabelNote"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelMotorWarna"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Warna</label>',
				":LabelUangMuka"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Uang Muka</label>',
				":Slash"               => '<label class="control-label" style="margin: 0; padding: 6px 0;"> / </label>',
				":Minus"               => '<label class="control-label" style="margin: 0; padding: 6px 0;"> - </label>',
				":LabelStatusRumah"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status Rumah</label>',
				":LabelKodeKons"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode Kons</label>',
				":LabelMotorHrgJual"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Harga OTR</label>',
				":LabelCusKK"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Kartu Klrg</label>',
				":LabelCusNPWP"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">NPWP Kons</label>',
				":INNo"                => Html::textInput( 'INNoView', $dsJual[ 'INNoView' ], [ 'class' => 'form-control', 'readOnly' => true, 'tabindex' => '1' ] ) .
				                          Html::hiddenInput( 'INNo', $dsJual[ 'INNo' ] ),
				":INTgl"               => FormField::dateInput( [ 'name' => 'INTgl', 'config' => [ 'value' => $dsJual[ 'INTgl' ] ] ] ),
				":SalesKode"           => FormField::combo( 'SalesKode', [ 'config'       => [ 'value' => $dsJual[ 'SalesKode' ] ], 'options' => ['tabindex'=>'2'],'extraOptions' => ['altLabel' => [ 'SalesNama' ],]] ),
				":LeaseKode"           => FormField::combo( 'LeaseKode', [ 'config'       => [ 'value' => $dsJual[ 'LeaseKode' ] ], 'options' => ['tabindex'=>'5'], 'extraOptions' => ['altLabel'     => [ 'LeaseNama' ], 'altOrder'     => [ 'LeaseStatus' => SORT_ASC, 'LeaseKode' => SORT_ASC ], 'altCondition' => "LeaseStatus = 'A' "]] ),
				":DKNo"                => Html::textInput( 'DKNo', $dsJual[ 'DKNo' ], [ 'class' => 'form-control' , 'tabindex' => '3', 'readonly' => true] ),
				":Note"                => Html::textInput( 'INMemo', $dsJual[ 'INMemo' ], [ 'class' => 'form-control' ] ),
				":UangMuka"            => FormField::decimalInput( [ 'name' => 'INDP', 'config' => [ 'value' => $dsJual[ 'INDP' ] ] ] ),
				":MotorHrgJual"        => FormField::decimalInput( [ 'name' => 'INHarga', 'config' => [ 'id' => 'INHarga_id', 'value' => $dsJual[ 'INHarga' ] ] ] ),
				":CusNama"             => Html::textInput( 'CusNama', $dsJual[ 'CusNama' ], [ 'class' => 'form-control' ] ) .
				                          Html::textInput( 'CusKode', $dsJual[ 'CusKode' ], [ 'class' => 'hidden' ] ),
				":CusAlamat"           => Html::textInput( 'CusAlamat', $dsJual[ 'CusAlamat' ], [ 'class' => 'form-control' ] ),
				":CusKTP"              => MaskedInput::widget( [ 'name' => 'CusKTP', 'value' => $dsJual[ 'CusKTP' ], 'mask' => '999999-999999-9999', 'class' => 'form-control' ] ),
				":CusTelepon1"         => Html::textInput( 'CusTelepon', $dsJual[ 'CusTelepon' ], [ 'class' => 'form-control' ] ),
				":CusTelepon2"         => Html::textInput( 'CusTelepon2', $dsJual[ 'CusTelepon2' ], [ 'class' => 'form-control' ] ),
				":CusRT"               => Html::textInput( 'CusRT', $dsJual[ 'CusRT' ], [ 'class' => 'form-control' ] ),
				":CusRW"               => Html::textInput( 'CusRW', $dsJual[ 'CusRW' ], [ 'class' => 'form-control' ] ),
				":CusKK"               => Html::textInput( 'CusKK', $dsJual[ 'CusKK' ], [ 'class' => 'form-control' ] ),
				":CusNPWP"             => Html::textInput( 'CusNPWP', $dsJual[ 'CusNPWP' ], [ 'class' => 'form-control' ] ),
				":CusTempatLhr"        => Html::textInput( 'CusTempatLhr', $dsJual[ 'CusTempatLhr' ], [ 'class' => 'form-control' ] ),
				":CusKabupaten"        => FormField::combo( 'Kabupaten', [ 'name' => 'CusKabupaten', 'config' => [ 'value' => $dsJual[ 'CusKabupaten' ] ], 'extraOptions' => [ 'KecamatanId' => 'Kecamatan_id', 'KecamatanValue' => $dsJual[ 'CusKecamatan' ], 'simpleCombo' => true ] ] ),
				":CusKecamatan"        => FormField::combo( 'Kecamatan', [ 'name' => 'CusKecamatan', 'config' => [ 'value' => $dsJual[ 'CusKecamatan' ] ], 'options' => [ 'id' => 'Kecamatan_id' ], 'extraOptions' => [ 'KelurahanId' => 'Kelurahan_id', 'KelurahanValue' => $dsJual[ 'CusKelurahan' ], 'simpleCombo' => true ] ] ),
				":CusKelurahan"        => FormField::combo( 'Kelurahan', [ 'name' => 'CusKelurahan', 'config' => [ 'value' => $dsJual[ 'CusKelurahan' ] ], 'options' => [ 'id' => 'Kelurahan_id' ], 'extraOptions' => [ 'KodePosId' => 'KodePos_id', 'KodePosValue' => $dsJual[ 'CusKodePos' ], 'simpleCombo' => true ] ] ),
				":CusKodePos"          => FormField::combo( 'KodePos', [ 'name' => 'CusKodePos', 'config' => [ 'value' => $dsJual[ 'CusKodePos' ] ], 'options' => [ 'id' => 'KodePos_id' ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":CusStatusRumah"      => FormField::combo( 'CusStatusRumah', [ 'name' => 'CusStatusRumah', 'config' => [ 'value' => $dsJual[ 'CusStatusRumah' ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":CusSex"              => FormField::combo( 'CusSex', [ 'name' => 'CusSex', 'config' => [ 'value' => $dsJual[ 'CusSex' ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":CusAgama"            => FormField::combo( 'CusAgama', [ 'name' => 'CusAgama', 'config' => [ 'value' => $dsJual[ 'CusAgama' ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":CusPekerjaan"        => FormField::combo( 'CusPekerjaan', [ 'name' => 'CusPekerjaan', 'config' => [ 'value' => $dsJual[ 'CusPekerjaan' ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":CusPendidikan"       => FormField::combo( 'CusPendidikan', [ 'name' => 'CusPendidikan', 'config' => [ 'value' => $dsJual[ 'CusPendidikan' ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":CusPengeluaran"      => FormField::combo( 'CusPengeluaran', [ 'name' => 'CusPengeluaran', 'config' => [ 'value' => $dsJual[ 'CusPengeluaran' ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":CusTglLhr"           => FormField::dateInput( [ 'name' => 'CusTglLhr', 'config' => [ 'value' => $dsJual[ 'CusTglLhr' ] ] ] ),
				":JenisTelepon"        => FormField::combo( 'CusStatusHP', [ 'name' => 'CusStatusHP', 'config' => [ 'value' => $dsJual[ 'CusStatusHP' ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":CusEmail"            => MaskedInput::widget( [ 'name' => 'CusEmail', 'value' => $dsJual[ 'CusEmail' ], 'clientOptions' => [ 'alias' => "email" ], 'class' => 'form-control' ] ),
				":KodeKons"            => FormField::combo( 'CusKodeKons', [ 'name' => 'CusKodeKons', 'config' => [ 'value' => $dsJual[ 'CusKodeKons' ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":TypeMotor"           => FormField::combo( 'MotorType', [ 'name' => 'MotorType', 'config' => [ 'value' => $dsJual[ 'MotorType' ] ], 'options' => [ 'id' => 'MotorType_id' ], 'extraOptions' => [ 'altCondition' => "TypeStatus = 'A'", 'MotorHrgJualId' => 'INHarga_id', 'MotorWarnaValue' => $dsJual[ 'MotorWarna' ], 'simpleCombo' => true ] ] ),
				":WarnaMotor"          => FormField::combo( 'MotorWarna', [ 'name' => 'MotorWarna', 'config' => [ 'value' => $dsJual[ 'MotorWarna' ] ], 'options' => [ 'id' => 'MotorWarna_id' ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":BtnSearchFB"         => '<button type="button" class="btn btn-default" id="BtnSearchFB"><i class="glyphicon glyphicon-search"></i></button>',


                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),

            ],
            [
                '_akses' => 'Inden',
                'url_main' => 'ttin',
                'url_id' => $_GET['id'] ?? '',
                'url_delete'    => Url::toRoute(['ttin/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlFB    = Url::toRoute( [
	'tdcustomer/select',
	'cmbTgl1' => 'CusTglBeli',
	'tgl1'    => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' )
] );
$urlLoop  = Url::toRoute( [ 'ttin/loop' ] );
$urlPrint = $url[ 'print' ];
$urlWarna = Custom::url( 'tdmotorwarna/find' );
$urlAdd   = Url::toRoute( [ 'ttin/create','action' => 'create' ] );
$urlIndex   = Url::toRoute( [ 'ttin/index','id'=> $id ] );
$this->registerJs( <<< JS
    $('#BtnSearchFB').click(function() {
        window.util.app.dialogListData.show({
        title: 'Daftar Konsumen',
        url: '$urlFB'
    },
    function (data) {
        $.ajax({
            type: 'POST',
            url: '$urlLoop',
            data: {
                'INNo': '{$dsJual['INNo']}',
                'CusKode': data.data.CusKode
            }
        }).then(function (result) {
           window.location.href = "{$url['update']}&oper=skip-load&INNoView={$dsJual['INNoView']}";      
        });
        })
    });    

  
     $('#MotorType_id').on('change', function (e) {
         $('#MotorWarna_id').utilSelect2().loadRemote('{$urlWarna}', 
         { mode: 'combo', params: {':MotorType':this.value} , 
         condition: 'MotorType = :MotorType','allOption': false}, true,function() {
             if ($('#MotorType_id').val() === '{$dsJual['MotorType']}'){
                $('#MotorWarna_id').val('{$dsJual['MotorWarna']}');
                $('#MotorWarna_id').trigger('change');
            }
         });
    });
     
     $('#MotorType_id').trigger('change');

    $('#btnPrint').click(function (event) {	      
        $('#frm_ttin_id').attr('action','$urlPrint');
        $('#frm_ttin_id').attr('target','_blank');
        $('#frm_ttin_id').submit();
	  }); 
    
    $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });         
    
     $('#btnCancel').click(function (event) {	      
       $('#frm_ttin_id').attr('action','{$url['cancel']}');
       $('#frm_ttin_id').submit();
    });
     
     $('#btnSave').click(function (event) {
        let INDP = $('input[name="INDP"]').utilNumberControl().val();
//         if (INDP === 0){
//             bootbox.alert({message : "Silahkan mengisi uang muka inden.", size: 'small'}); 
//             return;
//         }
        let CusNama = $('input[name="CusNama"]').val();
        let motorType = $('#MotorType_id').val();
        if(CusNama === "" || CusNama ==="--"){
           bootbox.alert({message :"Anda belum memasukkan data konsumen dengan lengkap.", size: 'small'});        
           return;
        }else if(motorType === ""){
           bootbox.alert({message :"Motor Tidak Tersedia", size: 'small'});
           return;
        }
       $('#frm_ttin_id').attr('action','{$url['update']}');
       $('#frm_ttin_id').submit();
    });   
   
     $('[name=INTgl]').utilDateControl().cmp().attr('tabindex', '4');
     
JS
);