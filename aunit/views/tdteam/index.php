<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use aunit\models\Tdteam;
use aunit\assets\AppAsset;
use aunit\components\Menu;
use common\components\Custom;
use kartik\datecontrol\DateControl;
/* @var $this yii\web\View */
/* @var $searchModel aunit\models\TdteamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
AppAsset::register($this);
$this->title = 'Team';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
		'TeamKode'       => 'Kode',
		'TeamNama'       => 'Nama',
		'TeamAlamat'     => 'Alamat',
        'TeamStatus'     => 'Status',
        'TeamPetugas'     => 'Petugas',
		'TeamTelepon'    => 'Telepon',
		'TeamKeterangan' => 'Keterangan',

	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
	'sortname'  => "TeamStatus asc,TeamKode asc,TeamNama",
	'sortorder' => 'asc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Tdteam::className() ,'Team'), \yii\web\View::POS_READY );