<?php
use common\components\Custom;
use kartik\number\NumberControl;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model aunit\models\Tdteam */
/* @var $form yii\widgets\ActiveForm */
//$url['cancel'] = Url::toRoute('tdteam/index');

?>

<div class="tdteam-form">
    <?php
    $form = ActiveForm::begin(['id'=>'frm_tdteam_id']);
    \aunit\components\TUi::form(
        ['class' => "row-no-gutters",
            'items' => [
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelTeamKode"],
                        ['class' => "col-sm-5",'items' => ":TeamKode"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelTeamNo"],
                        ['class' => "col-sm-2",'items' => ":TeamNo"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelTeamNama"],
                        ['class' => "col-sm-12",'items' => ":TeamNama"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelTeamStatus"],
                        ['class' => "col-sm-7",'items' => ":TeamStatus"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelTeamAlamat"],
                        ['class' => "col-sm-12",'items' => ":TeamAlamat"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelTeamTelepon"],
                        ['class' => "col-sm-7",'items' => ":TeamTelepon"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelTeamKeterangan"],
                        ['class' => "col-sm-22",'items' => ":TeamKeterangan"],
                    ],
                ],
            ]
        ],
        ['class' => "row-no-gutters",
            'items'  => [
                [
                    'class' => "col-md-12 pull-left",
                    'items' => $this->render( '../_nav', [
                        'url'=> $_GET['r'],
                        'options'=> [
                            'tdteam.TeamKode'       => 'Kode',
                            'tdteam.TeamNama'       => 'Nama',
                            'tdteam.TeamAlamat'     => 'Alamat',
                            'tdteam.TeamStatus'     => 'Status',
                            'tdteam.TeamPetugas'     => 'Petugas',
                            'tdteam.TeamTelepon'    => 'Telepon',
                            'tdteam.TeamKeterangan' => 'Keterangan',
                        ],
                    ])
                ],
                ['class' => "col-md-12 pull-right",
                    'items'  => [
                        ['class' => "pull-right", 'items' => ":btnAction"],
                    ],
                ],
            ],
        ],
        [
            ":LabelTeamKode"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode Team</label>',
            ":LabelTeamNo"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Urut</label>',
            ":LabelTeamNama"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Team</label>',
            ":LabelTeamStatus"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',
            ":LabelTeamAlamat"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
            ":LabelTeamTelepon"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon</label>',
            ":LabelTeamKeterangan"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',

            ":TeamKode"         => $id == 'new' ? $form->field($model, 'TeamKode', ['template' => '{input}'])->textInput(['maxlength' => true ]) : $form->field($model, 'TeamKode', ['template' => '{input}'])->textInput(['maxlength' => true,'readonly' => true ]),
            ":TeamNo"           => $form->field($model, 'TeamNo', ['template' => '{input}'])->textInput(['maxlength' => true, 'style' => 'text-align:right']),
            ":TeamNama"         => $form->field($model, 'TeamNama', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
            ":TeamStatus"       => $form->field($model, 'TeamStatus', ['template' => '{input}'])
                                    ->dropDownList([
                                        'A' => 'AKTIF',
                                        'N' => 'NON AKTIF',
                                    ], ['maxlength' => true]),
            ":TeamAlamat"       => $form->field($model, 'TeamAlamat', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
            ":TeamTelepon"      => $form->field($model, 'TeamTelepon', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
            ":TeamKeterangan"   => $form->field($model, 'TeamKeterangan', ['template' => '{input}'])->textInput(['maxlength' => true, ]),

            ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
            ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
            ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
            ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
            ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
        ],
        [
            '_akses' => 'Team',
            'url_main' => 'tdteam',
            'url_id' => $_GET['id'] ?? '',
        ]
    );
    ActiveForm::end();

    $urlAdd   = Url::toRoute( [ 'tdteam/create','id' => 'new','action'=>'create']);
    $urlIndex   = Url::toRoute( [ 'tdteam/index' ] );
    $this->registerJs( <<< JS
     
    $('#btnAdd').click(function (event) {	      
        window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });  

     $('#btnCancel').click(function (event) {	  
         window.location.href = '{$url['cancel']}';
    });

     $('#btnSave').click(function (event) {	      
       // $('#frm_tdteam_id').attr('action','{$url['update']}');
       $('#frm_tdteam_id').submit();
    });
     
     
JS);

    ?>
</div>

