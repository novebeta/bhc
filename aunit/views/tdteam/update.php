<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tdteam */
$this->title                   = str_replace('Menu','',\aunit\components\TUi::$actionMode).'Team: ' . $model->TeamKode;
$this->params['breadcrumbs'][] = [ 'label' => 'Team', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->TeamKode, 'url' => [ 'view', 'id' => $model->TeamKode ] ];
$this->params['breadcrumbs'][] = 'Edit';
$params = '&id='.$id;
?>
<div class="tdteam-update">
	<?= $this->render( '_form', [
		'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdteam/update', 'id' => $id ] ),
            'cancel' => Url::toRoute( [ 'tdteam/cancel', 'id' => $id ,'action' => 'create'] )
        ]
//        'url' => [
//            'save'    => Custom::url(\Yii::$app->controller->id.'/update'.$params ),
//            'update'    => Custom::url(\Yii::$app->controller->id.'/update' ),
//            'cancel'    => Custom::url(\Yii::$app->controller->id.'/' ),
//        ]
	] ) ?>
</div>
