<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttmpo */

$this->title = 'Create Ttmpo';
$this->params['breadcrumbs'][] = ['label' => 'Ttmpos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttmpo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
