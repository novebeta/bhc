<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttmpo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ttmpo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'PONo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'POTgl')->textInput() ?>

    <?= $form->field($model, 'MotorType')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'POQty')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
