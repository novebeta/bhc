<?php
use aunit\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
AppAsset::register( $this );
$this->title                   = 'PO';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'PONo'                  => 'No PO',
		'ttmdo.MotorType'       => 'Type',
		'tdmotortype.MotorNama' => 'Nama Motor',
	],
	'cmbTgl'    => [
		'POTgl' => 'Tanggal',
	],
	'cmbNum'    => [
		'POQty' => 'Qty'
	],
	'sortname'  => "POTgl",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttmpo::className(),
	'PO',[
//		'mode' => isset( $mode ) ? $mode : '',
//		'url_delete'    => Url::toRoute( [ 'ttsk/delete'] ),
	] ), \yii\web\View::POS_READY );