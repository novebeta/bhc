<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttmpo */

$this->title = 'Update Ttmpo: ' . $model->PONo;
$this->params['breadcrumbs'][] = ['label' => 'Ttmpos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->PONo, 'url' => ['view', 'PONo' => $model->PONo, 'POTgl' => $model->POTgl, 'MotorType' => $model->MotorType]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ttmpo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
