<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttpbhd */
$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode).' - Penerimaan dari Pos: ' . $dsTMutasi['PBNoView'];
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Penerimaan dari Pos', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = 'Edit';
?>
<div class="ttpbhd-update">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTMutasi' => $dsTMutasi,
        'id'     => $id,
        'url'    => [
			'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
			'cancel' => $cancel,
			'detail' => Custom::url( \Yii::$app->controller->id . '/detail' ),
		]
    ]) ?>

</div>
