<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\components\Custom;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttpbhd */
$params                          = '&id=' . $id . '&action=create';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = 'Tambah - Penerimaan dari Pos';
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Penerimaan dari Pos', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = $this->title;

?>
<div class="ttpbhd-create">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTMutasi' => $dsTMutasi,
        'id'     => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => Custom::url( \Yii::$app->controller->id . '/detail' ),
        ]
    ]) ?>

</div>
