<?php
use aunit\components\FormField;
use aunit\models\Tdlokasi;
use common\components\Custom;
use kartik\datecontrol\DateControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttpbhd */
/* @var $form yii\widgets\ActiveForm */
$format     = <<< SCRIPT
function formatttpbhd(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-10" style="text-align: left">' + result.id + '</div>' +
           '<div class="col-xs-14">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape     = new JsExpression( "function(m) { return m; }" );
$DataLokasi = ArrayHelper::map( Tdlokasi::find()->select2( 'LokasiKode', [ 'LokasiNama' ], "LokasiStatus,  LokasiKode",
	[ "condition" => "LokasiStatus <> 'N'", 'params' => [] ], false ), "value", 'label' );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttpd-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_ttpbhd_id', 'action' => $url[ 'update' ] ] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelPBNo" ],
						  [ 'class' => "col-sm-5", 'items' => ":PBNo" ],
						  [ 'class' => "col-sm-2", 'items' => ":PBTgl" ],
						  [ 'class' => "col-sm-3", 'items' => ":PBJam" ],
						  [ 'class' => "col-sm-2" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelLokasiAsal" ],
						  [ 'class' => "col-sm-8", 'items' => ":LokasiAsal" ],
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelSMNo" ],
						  [ 'class' => "col-sm-4", 'items' => ":SMNo" ],
						  [ 'class' => "col-sm-1", 'items' => ":BtnSearchFB" ],
						  [ 'class' => "col-sm-2", 'items' => ":SMTgl" ],
						  [ 'class' => "col-sm-5" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelLokasiTujuan" ],
						  [ 'class' => "col-sm-8", 'items' => ":LokasiTujuan" ],
					  ],
					],
					[ 'class' => "row col-md-24", 'style' => "margin-bottom: 3px;", 'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>' ],
					[ 'class' => "col-md-24",
					  'items' => [
						  [ 'class' => "form-group col-md-2", 'items' => ":LabelPBMemo" ],
						  [ 'class' => "form-group col-md-11", 'items' => ":PBMemo" ],
						  [ 'class' => "form-group col-md-1" ],
						  [ 'class' => "form-group col-md-2", 'items' => ":LabelJmlUnit" ],
						  [ 'class' => "form-group col-md-2", 'items' => ":JmlUnit" ],
						  [ 'class' => "form-group col-md-1" ],
					  ]
					],
				],
			],
            [ 'class' => "row-no-gutters",
                'items' => [
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'PBNo'         => 'No PP / PB',
                                'SMNo'         => 'No MI / SM',
                                'LokasiAsal'   => 'Lokasi Asal',
                                'LokasiTujuan' => 'Lokasi Tujuan',
                                'PBMemo'       => 'Keterangan',
                                'UserID'       => 'User ID',
                            ],
                        ])
                    ],
			        ['class' => "pull-right", 'items' => ":btnPrint :btnAction"],
                ],
            ],
			[
				":LabelPBNo"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Terima</label>',
				":LabelLokasiAsal"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Asal</label>',
				":LabelSMNo"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Surat Jalan</label>',
				":LabelLokasiTujuan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tujuan</label>',
				":LabelPBMemo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelJmlUnit"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jumlah Unit</label>',
				":LabelPBTotal"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total</label>',
				":PBNo"              => Html::textInput( 'PBNoView', $dsTMutasi[ 'PBNoView' ], [ 'class' => 'form-control', 'tabindex' => '1' ] ) .
				                        Html::textInput( 'PBNo', $dsTMutasi[ 'PBNo' ], [ 'class' => 'hidden' ] ),
				":SMNo"              => Html::textInput( 'SMNo', $dsTMutasi[ 'SMNo' ], [ 'class' => 'form-control', 'readOnly' => true ] ),
				":PBTgl"             => FormField::dateInput( [ 'name' => 'PBTgl', 'config' => [ 'value' => $dsTMutasi[ 'PBTgl' ] ] ] ),
				":SMTgl"             => FormField::dateInput( [ 'name' => 'SMTgl', 'config' => [ 'value' => $dsTMutasi[ 'SMTgl' ] ] ] ),
				":PBJam"             => FormField::timeInput( [ 'name' => 'PBJam', 'config' => [ 'value' => $dsTMutasi[ 'PBJam' ] ] ] ),
				":JmlUnit"           => FormField::integerInput( [ 'name' => 'JmlUnit', 'config' => [ 'value' => 0 ] ] ),
				":PBMemo"            => Html::textInput( 'PBMemo', $dsTMutasi[ 'PBMemo' ], [ 'class' => 'form-control', 'tabindex' => '7' ] ),
				":PBTotal"           => FormField::decimalInput( [ 'name' => 'PBTotal', 'config' => [ 'value' => $dsTMutasi[ 'PBTotal' ] ] ] ),
				":LokasiAsal"        => FormField::combo( 'LokasiKode', [ 'name' => 'LokasiAsal', 'config' => [ 'id' => 'LokasiAsal', 'value' => $dsTMutasi[ 'LokasiAsal' ], 'options' => [ 'tabindex' => '3' ], 'data' => $DataLokasi ] ] ),
				":LokasiTujuan"      => FormField::combo( 'LokasiKode', [ 'name' => 'LokasiTujuan', 'config' => [ 'id' => 'LokasiTujuan', 'value' => $dsTMutasi[ 'LokasiTujuan' ], 'options' => [ 'tabindex' => '6' ], 'data' => $DataLokasi ] ] ),
				":BtnSearchFB" => '<button type="button" class="btn btn-default" id="BtnSearchFB" tabindex="4"><i class="glyphicon glyphicon-search"></i></button>',


                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
//                ":btnImport"      => Html::button('<i class="fa fa-arrow-circle-down"><br><br>Import</i>   ', ['class' => 'btn bg-navy ', 'id' => 'btnImport', 'style' => 'width:60px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
//                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),


//                ":btnSave"     => Html::button( '<i class="glyphicon glyphicon-floppy-disk"></i> Simpan', [ 'class' => 'btn btn-info btn-primary ', 'id' => 'btnSave' . Custom::viewClass() ] ),
//				":btnCancel"   => Html::Button( '<i class="fa fa-ban"></i> Batal', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:80px' ] ),
//				":btnAdd"      => Html::button( '<i class="fa fa-plus-circle"></i> Tambah', [ 'class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:80px' ] ),
//				":btnEdit"     => Html::button( '<i class="fa fa-edit"></i> Edit', [ 'class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:80px' ] ),
//				":btnDaftar"   => Html::Button( '<i class="fa fa-list"></i> Daftar ', [ 'class' => 'btn btn-primary ', 'id' => 'btnDaftar', 'style' => 'width:80px' ] ),
//				":btnPrint"    => Html::button( '<i class="glyphicon glyphicon-print"></i>  Print ', [ 'class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:80px' ] ),
			],
            [
                '_akses' => 'Penerimaan dari Pos',
                'url_main' => 'ttpbhd',
                'url_id' => $_GET['id'] ?? '',
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlDetail       = $url[ 'detail' ];
$urlPrint        = $url[ 'print' ];
$url[ 'jurnal' ] = \yii\helpers\Url::toRoute( [ 'ttpbhd/jurnal' ] );
$urlAdd          = Url::toRoute( [ 'ttpbhd/create', 'action' => 'create' ] );
$urlIndex        = Url::toRoute( [ 'ttpbhd/index' ] );
$readOnly        = ( ( $_GET[ 'action' ] ?? '' ) == 'view' ? 'true' : 'false' );
$hideHarga       = 'true';
$this->registerJsVar( '__model', $dsTMutasi );
$this->registerJsVar( '__urlSM', Url::toRoute( [ 'ttsmhd/mutasi-internal-pos-select' ] ) );
$this->registerJs( <<< JS

     window.getUrlSM = function(){
        let SMNo = $('input[name="SMNo"]').val();
        return __urlSM + ((SMNo === '' || SMNo === '--') ? '':'&check=off&cmbTxt1=SMNo&txt1='+SMNo);
     } 

     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 300,
        extraParams: {
            PBNo: $('input[name="PBNo"]').val()
        },
        postData: {
            PBNo: $('input[name="PBNo"]').val()
        },
        rownumbers: true,     
        loadonce:true,
        rowNum: 1000,      
        readOnly: $readOnly,  
        navButtonTambah: false,         
        colModel: [
            { template: 'actions', hidden: $readOnly  },
            {
                name: 'MotorType',
                label: 'Type',
                editable: true,
                width: 190
            },
             {
                name: 'MotorTahun',
                label: 'Tahun',
                width: 60,
                editable: true
            },
            {
                name: 'MotorNama',
                label: 'MotorNama',
                width: 190,
                editable: true
            },
           
            {
                name: 'MotorWarna',
                label: 'Warna',
                editable: true,
                width: 190,
            },
            {
                name: 'MotorNoMesin',
                label: 'NoMesin',
                width: 160,
                editable: true,
            },
            {
                name: 'MotorAutoN',
                label: 'NoMesin',
                width: 160,
                hidden: true ,
                editable: true
            },            
            {
                name: 'MotorNoRangka',
                label: 'NoRangka',
                width: 155,
                editable: true,
            },
        ],
        listeners: {
            afterLoad: function(grid, response) {
                 let jQty = $('#detailGrid').jqGrid('getGridParam', 'records');
                $('input[name="JmlUnit"]').utilNumberControl().val(jQty);      
                let __memo = __model.PBMemo.replace(/~( )*Jumlah( )*Unit( )*:( )*\d*/g,'');
                $('input[name="PBMemo"]').val(__memo + ' ~ Jumlah Unit :  '+ jQty);   
                if (jQty > 0){
                    $("#LokasiAsal").attr('disabled', true);
                    $("#form_ttsmhd_id").append("<input id='LokasiAsalHidden' name='LokasiAsal' type='hidden' value='"+$("#LokasiAsal").val()+"'>");
                }else{
                    $("#LokasiAsal").attr('disabled', false);
                    $("#LokasiAsalHidden").remove();
                }
            },   
        }
    }).init().fit($('.box-body')).load({url: '$urlDetail'});
    $('#btnSave').click(function (event) {
        if($('#LokasiAsal').val() === $('#LokasiTujuan').val()){
            bootbox.alert({'size':'small','message':"Lokasi Asal dan Tujuan tidak boleh sama"});
            return;
        }
        $('#form_ttpbhd_id').submit();
    }); 
    $('#btnPrint').click(function (event) {	      
        $('#form_ttpbhd_id').attr('action','$urlPrint');
        $('#form_ttpbhd_id').attr('target','_blank');
        $('#form_ttpbhd_id').submit();
	  }); 
    
    $('#btnJurnal').click(function (event) {	
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#form_ttpbhd_id').attr('action','{$url['jurnal']}');
                    $('#form_ttpbhd_id').submit();                     
                }
            }
        });
    });   
    
    $('#BtnSearchFB').click(function() {
        window.util.app.dialogListData.show({
            title: 'Daftar Surat Jalan Mutasi',
            url: getUrlSM()
        },
        function (data) {
             console.log(data);
             if (!Array.isArray(data)){
                 return;
             }             
            $.ajax({
             type: "POST",
             url: '$urlDetail',
             data: {
                 oper: 'batch',
                 data: data,
                 header: {
                     PBNo: __model.PBNo,
                 }
             },
             success: function(data) {
               window.location.href = "{$url['update']}&oper=skip-load&PBNoView={$dsTMutasi['PBNoView']}"; 
             },
           });
        })
    });
   
    
     $('#btnCancel').click(function (event) {	      
       $('#form_ttpbhd_id').attr('action','{$url['cancel']}');
       $('#form_ttpbhd_id').submit();
    });
     
     
    
     $('#btnAdd').click(function (event) {	      
        $('#form_ttpbhd_id').attr('action','{$urlAdd}');
        $('#form_ttpbhd_id').submit();
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });  
    
    $('[name=PBTgl]').utilDateControl().cmp().attr('tabindex', '2');
    $('[name=SMTgl]').utilDateControl().cmp().attr('tabindex', '5');
    $('[name=JmlUnit]').utilNumberControl().cmp().attr('tabindex', '8');
    
    
JS
);
