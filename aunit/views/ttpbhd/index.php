<?php
use aunit\assets\AppAsset;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Penerimaan dari Pos';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'PBNo'         => 'No PP / PB',
		'SMNo'         => 'No MI / SM',
		'LokasiAsal'   => 'Lokasi Asal',
		'LokasiTujuan' => 'Lokasi Tujuan',
		'PBMemo'       => 'Keterangan',
		'UserID'       => 'User ID',
	],
	'cmbTgl'    => [
        'ttPBhd.PBTgl'        => 'Tgl PP / PB',
	],
	'cmbNum'    => [
	    'ttPBhd.PBTotal'      => 'Total',
	],
	'WHERE ttPBhd.PBNo LIKE  "PP%"',
	'sortname'  => "PBTgl",
	'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttpbhd::className(),
	'Penerimaan dari Pos',['url_delete'    => Url::toRoute( [ 'ttpbhd/delete'] ),]), \yii\web\View::POS_READY );