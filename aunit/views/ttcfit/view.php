<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttcfit */

$this->title = $model->CFNo;
$this->params['breadcrumbs'][] = ['label' => 'Ttcfits', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ttcfit-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'CFNo' => $model->CFNo, 'MotorAutoN' => $model->MotorAutoN, 'MotorNoMesin' => $model->MotorNoMesin], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'CFNo' => $model->CFNo, 'MotorAutoN' => $model->MotorAutoN, 'MotorNoMesin' => $model->MotorNoMesin], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'CFNo',
            'MotorAutoN',
            'MotorNoMesin',
            'FBHarga',
        ],
    ]) ?>

</div>
