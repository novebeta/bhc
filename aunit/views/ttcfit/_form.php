<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttcfit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ttcfit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'CFNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MotorAutoN')->textInput() ?>

    <?= $form->field($model, 'MotorNoMesin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FBHarga')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
