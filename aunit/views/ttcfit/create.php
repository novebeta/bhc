<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttcfit */

$this->title = 'Create Ttcfit';
$this->params['breadcrumbs'][] = ['label' => 'Ttcfits', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttcfit-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
