<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttcfit */

$this->title = 'Update Ttcfit: ' . $model->CFNo;
$this->params['breadcrumbs'][] = ['label' => 'Ttcfits', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->CFNo, 'url' => ['view', 'CFNo' => $model->CFNo, 'MotorAutoN' => $model->MotorAutoN, 'MotorNoMesin' => $model->MotorNoMesin]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ttcfit-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
