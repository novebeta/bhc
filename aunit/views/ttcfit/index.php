<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ttcfits';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttcfit-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ttcfit', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'CFNo',
            'MotorAutoN',
            'MotorNoMesin',
            'FBHarga',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
