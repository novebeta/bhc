<?php
use aunit\components\FormField;
use aunit\models\Tdmotortype;
use common\components\Custom;
use kartik\date\DatePicker;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tdprogram */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function formattdprogram(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-10" style="text-align: left">' + result.id + '</div>' +
           '<div class="col-xs-14">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
//$url[ 'cancel' ] = Url::toRoute( 'tdprogram/index' );
?>
<div class="tdprogram-form">
	<?php
	$form = ActiveForm::begin( [ 'id' => 'frm_tdprogram_id' ] );
	\aunit\components\TUi::form(
		[
			'class' => "row-no-gutters",
			'items' => [
				[ 'class' => "form-group col-md-18",
				  'items' => [
					  [ 'class' => "col-sm-4", 'items' => ":LabelProgramNama" ],
					  [ 'class' => "col-sm-10", 'items' => ":ProgramNama" ],
					  [ 'class' => "col-sm-1" ],
					  [ 'class' => "col-sm-3", 'items' => ":LabelPrgSubsSupplier" ],
					  [ 'class' => "col-sm-5", 'items' => ":PrgSubsSupplier" ],
				  ],
				],
				[ 'class' => "form-group col-md-18",
				  'items' => [
					  [ 'class' => "col-sm-4", 'items' => ":LabelTanggalBerlaku" ],
					  [ 'class' => "col-sm-5", 'style' => "padding-right:1px;", 'items' => ":PrgTglAwal" ],
					  [ 'class' => "col-sm-5", 'style' => "padding-left:1px;", 'items' => ":PrgTglAkhir" ],
					  [ 'class' => "col-sm-1" ],
					  [ 'class' => "col-sm-3", 'items' => ":LabelPrgSubsDealer" ],
					  [ 'class' => "col-sm-5", 'items' => ":PrgSubsDealer" ],
					  [ 'class' => "col-sm-4", 'items' => ":LabelMotorType" ],
					  [ 'class' => "col-sm-10", 'items' => ":MotorType" ],
					  [ 'class' => "col-sm-1" ],
					  [ 'class' => "col-sm-3", 'items' => ":LabelPrgSubsFincoy" ],
					  [ 'class' => "col-sm-5", 'items' => ":PrgSubsFincoy" ],
				  ],
				],
				[ 'class' => "form-group col-md-2" ],
				[ 'class' => "form-group col-md-6",
				  'items' => [
					  [ 'class' => "col-sm-24", 'items' => ":LabelPrgSubsTotal" ],
					  [ 'class' => "col-sm-24", 'items' => ":PrgSubsTotal" ],
				  ],
				],
			]
		],
        ['class' => "row-no-gutters",
            'items'  => [
                [
                    'class' => "col-md-12 pull-left",
                    'items' => $this->render( '../_nav', [
                        'url'=> $_GET['r'],
                        'options'=> [
                            'tdprogram.ProgramNama' => 'Nama Program',
                            'tdprogram.MotorType' => 'Type Motor',
                        ],
                    ])
                ],
                ['class' => "col-md-12 pull-right",
                    'items'  => [
                        ['class' => "pull-right", 'items' => ":btnAction"],
                    ],
                ],
            ],
        ],
		[
			/* label */
			":LabelProgramNama"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Program</label>',
			":LabelMotorType"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Type Motor</label>',
			":LabelTanggalBerlaku"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tanggal Berlaku</label>',
			":LabelPrgSubsSupplier" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Subsidi Supplier</label>',
			":LabelPrgSubsDealer"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Subsidi Dealer</label>',
			":LabelPrgSubsFincoy"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Subsidi Fincoy</label>',
			":LabelPrgSubsTotal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Subsidi Total</label>',
			":LabelSpasi"           => '<label class="control-label" style="margin: 0; padding: 6px 0;"> &nbsp</label>',
			/* component */
			":PrgTglAwal"           => FormField::dateInput( [ 'name' => 'Tdprogram[PrgTglAwal]', 'config' => [ 'value' => $model->PrgTglAwal, 'readonly' => false ] ] ),
			":PrgTglAkhir"          => FormField::dateInput( [ 'name' => 'Tdprogram[PrgTglAkhir]', 'config' => [ 'value' => $model->PrgTglAkhir, 'readonly' => false ] ] ),
			":ProgramNama"          => $form->field( $model, 'ProgramNama', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true, ] ),
			":MotorType"            => $form->field( $model, 'MotorType', [ 'template' => '{input}' ] )
			                                ->widget( Select2::class, [
				                                'value'         => $model->MotorType,
				                                'theme'         => Select2::THEME_DEFAULT,
				                                'data'          => Tdmotortype::find()->comboselect(),
				                                'pluginOptions' => [
					                                'dropdownAutoWidth' => true,
					                                'templateResult'    => new JsExpression( 'formattdprogram' ),
					                                'templateSelection' => new JsExpression( 'formattdprogram' ),
					                                'matcher'           => new JsExpression( 'matchCustom' ),
					                                'escapeMarkup'      => $escape, ] ] ),
			":PrgSubsSupplier"      => $form->field( $model, 'PrgSubsSupplier', [ 'template' => '{input}' ] )
			                                ->widget( NumberControl::class, [
				                                'maskedInputOptions' => [
					                                'allowMinus' => false,
					                                'value'      => 0 ],
				                                'displayOptions'     => [ 'class' => 'hitung form-control', 'placeholder' => '0', ] ] ),
			":PrgSubsDealer"        => $form->field( $model, 'PrgSubsDealer', [ 'template' => '{input}' ] )
			                                ->widget( NumberControl::class, [
				                                'maskedInputOptions' => [
					                                'allowMinus' => false,
					                                'value'      => 0
				                                ],
				                                'displayOptions'     => [ 'class' => 'hitung form-control', 'placeholder' => '0', ]
			                                ] ),
			":PrgSubsFincoy"        => $form->field( $model, 'PrgSubsFincoy', [ 'template' => '{input}' ] )
			                                ->widget( NumberControl::class, [
				                                'maskedInputOptions' => [
					                                'allowMinus'  => false,
					                                'placeholder' => 'fjkjd',
					                                'value'       => 0
				                                ],
				                                'displayOptions'     => [ 'class' => 'hitung form-control', 'placeholder' => '0', ]
			                                ] ),
			":PrgSubsTotal"         => $form->field( $model, 'PrgSubsTotal', [ 'template' => '{input}' ] )
			                                ->widget( NumberControl::class, [
				                                'id'                 => 'PrgSubsTotal',
				                                'maskedInputOptions' => [
					                                'allowMinus' => false,
				                                ],
				                                'displayOptions'     => [ 'class' => 'form-control', 'placeholder' => '0', ]
			                                ] ),
			/* button */
            ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
            ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
            ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
            ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
            ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),

		],
        [
            '_akses' => 'Program',
            'url_main' => 'tdprogram',
            'url_id' => $_GET['id'] ?? '',
        ]
	);
	ActiveForm::end();
	$urlAdd   = Url::toRoute( [ 'tdprogram/create', 'id' => 'new','action'=>'create']);
	$urlIndex = Url::toRoute( [ 'tdprogram/index' ] );
	$this->registerJs( <<< JS
     
    $('#btnAdd').click(function (event) {	      
        window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });  

     $('#btnCancel').click(function (event) {	  
         window.location.href = '{$url['cancel']}';
    });

     $('#btnSave').click(function (event) {	      
//       $('#frm_tdprogram_id').attr('action','{$url['update']}');
       $('#frm_tdprogram_id').submit();
    });     
     
     $(".hitung").change(function() {
       let PrgSubsSupplier = Number($('[name="Tdprogram[PrgSubsSupplier]"]').val());
       let PrgSubsDealer = Number($('[name="Tdprogram[PrgSubsDealer]"]').val());
       let PrgSubsFincoy = Number($('[name="Tdprogram[PrgSubsFincoy]"]').val());
       let PrgSubsTotal = PrgSubsSupplier + PrgSubsDealer +  PrgSubsFincoy;       
         console.log(PrgSubsTotal);
       $('[name="Tdprogram[PrgSubsTotal]').utilNumberControl().val(PrgSubsTotal);
     });  
     
     
     
JS
	);
	?>
</div>
