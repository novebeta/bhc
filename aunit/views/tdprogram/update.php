<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tdprogram */
$this->title                   = str_replace('Menu','',\aunit\components\TUi::$actionMode).' Program: ' . $model->ProgramNama;
$this->params['breadcrumbs'][] = [ 'label' => 'Program', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->ProgramNama,
                                   'url'   => [
	                                   'view',
	                                   'ProgramNama' => $model->ProgramNama,
	                                   'MotorType'   => $model->MotorType
                                   ]
];
$this->params['breadcrumbs'][] = str_replace('Menu','',\aunit\components\TUi::$actionMode);
$params = '&id='.$id;
?>
<div class="tdprogram-update">
	<?= $this->render( '_form', [
		'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdprogram/update', 'id' => $id ,'action' => 'update'] ),
            'cancel' => Url::toRoute( [ 'tdprogram/cancel', 'id' => $id ,'action' => 'create'] )
        ]
//        'url' => [
//            'save'    => Custom::url(\Yii::$app->controller->id.'/update'.$params ),
//            'cancel'    => Custom::url(\Yii::$app->controller->id.'/' ),
//        ]
	] ) ?>
</div>
