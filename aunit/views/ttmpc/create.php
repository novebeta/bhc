<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttmpc */

$this->title = 'Create Ttmpc';
$this->params['breadcrumbs'][] = ['label' => 'Ttmpcs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttmpc-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
