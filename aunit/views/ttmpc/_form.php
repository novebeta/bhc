<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttmpc */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ttmpc-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'PCNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PCTgl')->textInput() ?>

    <?= $form->field($model, 'MotorType')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PCQty')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
