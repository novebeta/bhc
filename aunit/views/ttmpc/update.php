<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttmpc */

$this->title = 'Update Ttmpc: ' . $model->PCNo;
$this->params['breadcrumbs'][] = ['label' => 'Ttmpcs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->PCNo, 'url' => ['view', 'PCNo' => $model->PCNo, 'PCTgl' => $model->PCTgl, 'MotorType' => $model->MotorType]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ttmpc-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
