<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttsrhd */

$params                          = '&id=' . $id . '&action=update';
$cancel = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = 'Edit - Retur Penjualan: ' . $dsTJual['SRNoView'];
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Retur Penjualan', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = 'Edit';
?>
<div class="ttsrhd-update">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTJual' => $dsTJual,
        'id'     => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail'       => Url::toRoute( [ 'ttsrit/index','action' => 'update' ] ),
        ]
    ]) ?>

</div>
