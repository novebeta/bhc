<?php
use aunit\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '[SR] Retur Penjualan';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'SRNo'          => 'SR No',
		'SINo'          => 'SI No',
		'KodeTrans'     => 'Kode Trans',
		'LokasiKode'    => 'Lokasi Kode',
		'PosKode'       => 'Pos Kode',
		'KarKode'       => 'Kar Kode',
		'MotorNoPolisi' => 'No Polisi',
		'SRKeterangan'  => 'SR Keterangan',
		'NoGL'          => 'No Gl',
		'Cetak'         => 'Cetak',
		'UserID'        => 'User ID',
	],
	'cmbTgl'    => [
		'SRTgl'         => 'SR Tgl',
	],
	'cmbNum'    => [
		'SRTotal'       => 'SR Total',
		'SRTerbayar'    => 'SR Terbayar',
		'SRSubTotal'    => 'SR Sub Total',
		'SRDiscFinal'   => 'SR Disc Final',
	],
	'sortname'  => "SRTgl",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \abengkel\models\Ttsrhd::className(), '[SR] Retur Penjualan',
    [
        'url_delete' => Url::toRoute( [ 'ttsrhd/delete' ] ),
    ]
 ),\yii\web\View::POS_READY );
