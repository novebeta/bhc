<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\checkbox\CheckboxX;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;

$this->title                   = 'Laporan Neraca Percobaan';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;

$sqlBank                           = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, 0 AS StatusAccount,  '' AS NoAccount
	UNION
	SELECT NoAccount AS value, NamaAccount AS label, StatusAccount, NoAccount AS NoAccount FROM traccount 
	WHERE StatusAccount = 'A' AND JenisAccount = 'Detail' 
	ORDER BY StatusAccount, NoAccount" )
	->queryAll();
$cmbBank                           = ArrayHelper::map( $sqlBank, 'value', 'label' );

$format = <<< SCRIPT
function formatBank(result) {
    return '<div class="row">' +
           '<div class="col-md-3">' + result.id + '</div>' +
           '<div class="col-md-21">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );

?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Data Perkiraan' => 'Data Perkiraan',
									'Neraca Percobaan 1' => 'Neraca Percobaan 1',
									'Neraca Percobaan 2' => 'Neraca Percobaan 2',
									'Neraca Saldo Detail' => 'Neraca Saldo Detail',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-footer">
				<div class="container-fluid">
					<div class="col-md-2">
						<div class="form-group">
							<label class="control-label col-sm-24">Tanggal</label>
						</div>
					</div>
					<div class="col-md-18">
						<div class="form-group">
							<div class="col-md-6">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'id'            => 'tgl1_id',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format' => 'dd/mm/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
							<div class="col-md-6">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'id'            => 'tgl2_id',
										'type'          => DatePicker::TYPE_INPUT,
                                        'value'         => date('t/m/Y',strtotime("today")),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
							<label class="control-label col-sm-2">Bulan</label>
							<div class="col-md-6">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'TglKu',
										'attribute2'=>'to_date',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','MMMM  yyyy' ),
										'pluginOptions' => [
											'startView'=>'year',
											'minViewMode'=>'months',
											'format' => 'MM  yyyy'
                                        ],
										'pluginEvents'  => [
											"change" => new JsExpression("function(e) {
											 let d = new Date(Date.parse('01 ' + $(this).val()));											 
											 var tgl1 = $('#tgl1_id');
											 var tgl2 = $('#tgl2_id');
											  tgl1.kvDatepicker('update', d);
											  tgl2.kvDatepicker('update',new Date(d.getFullYear(), d.getMonth()+1, 0));
											}")
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<div class="col-sm-24">
										<? try {
											echo CheckboxX::widget([
												'name'=>'ringkas',
												'options'=>['id'=>'ringkas'],
												'pluginOptions'=>['threeState'=>false, 'size'=>'lg']
											]);
											echo '<label class="cbx-label" for="ringkas">Ringkas</label>';
										} catch ( \yii\base\InvalidConfigException $e ) {
										} ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<div class="col-md-2">
						<div class="form-group">
							<label class="control-label col-sm-24">Perkiraan 1</label>
						</div>
					</div>
					<div class="col-md-18">
						<div class="form-group">
							<div class="col-sm-24">
								<? echo Select2::widget( [
									'name'          => 'perkiraan',
									'data'          => $cmbBank,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatBank' ),
										'templateSelection' => new JsExpression( 'formatBank' ),
										'matcher'           => new JsExpression( 'matchCustom' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
							</div>
						</div>
					</div>
				</div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php

