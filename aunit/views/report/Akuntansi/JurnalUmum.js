jQuery(function ($) {
    $("#tipe-id").change(function () {
        var tipe = this.value;
        if (tipe === "Ledger Detail" || tipe === "Ledger Harian" || tipe === "Jurnal Tidak Balance") {
            $(".jurnalHide").hide();
        } else {
            $(".jurnalHide").show();
        }
    });
});