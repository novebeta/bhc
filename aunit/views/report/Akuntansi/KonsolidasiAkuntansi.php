<?php
use yii\helpers\Url;
use kartik\checkbox\CheckboxX;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;
$this->title                     = 'Laporan Konsolidasi Akuntansi';
$this->params[ 'breadcrumbs' ][] = $this->title;
$formatter                       = \Yii::$app->formatter;
$datalevel = [
    'Dealer' => 'Dealer',
    'Korwil' => 'Korwil',
    'Nasional' => 'Nasional',
    'Anper' => 'Anper',
    'BHC' => 'BHC'
];

$sqlLeasing = Yii::$app->db
    ->createCommand("
	SELECT '%' AS value, 'Semua' AS label, '' AS LeaseStatus, '' AS LeaseKode
	UNION
	SELECT LeaseKode AS value, LeaseKode As label , LeaseStatus , LeaseKode FROM tdLeasing 
	WHERE LeaseStatus = 'A' 
	ORDER BY LeaseStatus, LeaseKode")
    ->queryAll();
$cmbLeasing = ArrayHelper::map($sqlLeasing, 'value', 'label');
$getLeasing = Url::toRoute(['report/spleasing']);
$format = <<< SCRIPT
function formatDual(result) {
   return '<div class="row">' +
           '<div class="col-md-8" style="padding-left:10px; ">' + result.id + '</div>' +
           '<div class="col-md-16">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);

?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
            <div class="box-body">
                <div class="container-fluid">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Laporan</label>
                            <div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Konsolidasi Pencairan Leasing'             => 'Konsolidasi Pencairan Leasing',
									'Pencairan Leasing Monthly'             => 'Pencairan Leasing Monthly',
									'Detail Pencairan Leasing'          => 'Detail Pencairan Leasing',
									'Profit & NPBT'          => 'Profit & NPBT',
									'Scheme Leasing PV'          => 'Scheme Leasing PV',
									'Scheme Leasing Monthly'          => 'Scheme Leasing Monthly',
									'Scheme Leasing List'          => 'Scheme Leasing List'
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Format</label>
                            <div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="container-fluid">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Tanggal</label>
                        </div>
                    </div>
                    <div class="col-md-22">
                        <div class="form-group">
                            <div class="col-md-4">
                                <? try {
                                    echo DatePicker::widget( [
                                        'name'          => 'tgl1',
                                        'id'            => 'tgl1_id',
                                        'type'          => DatePicker::TYPE_INPUT,
                                        'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                            'todayHighlight' => true
                                        ]
                                    ] );
                                } catch ( \yii\base\InvalidConfigException $e ) {
                                } ?>
                            </div>
                            <div class="col-md-4">
                                <? try {
                                    echo DatePicker::widget( [
                                        'name'          => 'tgl2',
                                        'id'            => 'tgl2_id',
                                        'type'          => DatePicker::TYPE_INPUT,
                                        'value'         => date('t/m/Y',strtotime("today")),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format'    => 'dd/mm/yyyy'
                                        ]
                                    ] );
                                } catch ( \yii\base\InvalidConfigException $e ) {
                                } ?>
                            </div>
                            <label class="control-label col-sm-1">Bulan</label>
                            <div class="col-md-4">
                                <? try {
                                    echo DatePicker::widget( [
                                        'name'          => 'TglKu',
                                        'id'            => 'TglKu_id',
                                        'attribute2'    =>'to_date',
                                        'type'          => DatePicker::TYPE_INPUT,
                                        'value'         => date('t/m/Y',strtotime("today")),
                                        'pluginOptions' => [
                                            'startView'=>'year',
                                            'minViewMode'=>'months',
                                            'format' => 'MM  yyyy'
                                        ],
                                        'pluginEvents'  => [
                                            "change" => new JsExpression("function(e) {
											 let d = new Date(Date.parse('01 ' + $(this).val()));											 
											 var tgl1 = $('#tgl1_id');
											 var tgl2 = $('#tgl2_id');
											  tgl1.kvDatepicker('update', d);
											  tgl2.kvDatepicker('update',new Date(d.getFullYear(), d.getMonth()+1, 0));
											}")
                                        ]
                                    ] );
                                } catch ( \yii\base\InvalidConfigException $e ) {
                                } ?>
                            </div>
                            <div class="col-sm-2">
                                <? try {
                                    echo CheckboxX::widget([
                                        'name'=>'eom',
                                        'options'=>['id'=>'eom'],
                                        'value'=>true,
                                        'pluginOptions'=>['threeState'=>false, 'size'=>'lg'],
                                        'pluginEvents'  => [
                                            "change" => new JsExpression("function(e) {
											 var today = new Date();
											 var tgl2 = $('#tgl2_id');
											 var TglKu = $('#TglKu_id');
											  let tglku = TglKu.kvDatepicker('getDate');
											 if($(this).val() == 1){
											    var lastDayOfMonth = new Date(tglku.getFullYear(), tglku.getMonth()+1, 0);
											    tgl2.kvDatepicker('update', lastDayOfMonth)
											 }else{
											    tgl2.kvDatepicker('update', new Date(today.getFullYear(), today.getMonth(), today.getDate()));
											 }
											}")
                                        ]
                                    ]);
                                    echo '<label class="cbx-label" for="eom">EOM</label>';
                                } catch ( \yii\base\InvalidConfigException $e ) {
                                } ?>
                            </div>
                            <label class="control-label col-sm-1">Level</label>
                            <div class="col-sm-3">
                                <? echo Select2::widget( [
                                    'name'          => 'level',
                                    'id'          => 'level',
                                    'data'          => $datalevel,
                                    'options'       => [ 'class' => 'form-control' ],
                                    'theme'         => Select2::THEME_DEFAULT,
                                    'pluginOptions' => [
                                             'allowClear' => true
                                    ],
                                    'pluginEvents' => [
                                        "change" => new JsExpression( "function(e) {	
											$('#leasing').utilSelect2().loadRemote('" . \yii\helpers\Url::toRoute(['report/spleasing']) . "', {  mode: 'combo', value: 'LeaseKode', label: ['LeaseKode'], condition : 'level = :level', params:{':level':this.value,':eom':$('#eom').val(),':tgl1':$('#tgl1').val(),':tgl2':$('#tgl2').val()}, allOption:true }, true);
											}" )
                                    ]
                                ] ); ?>

                            </div>
                                <label class="control-label col-sm-1">Leasing</label>
                                <div class="col-md-3">
                                    <? echo Select2::widget( [
                                        'name'          => 'leasing',
                                        'data'          => $cmbLeasing,
                                        'options'       => [ 'id'          => 'leasing','class' => 'form-control' ],
                                        'theme'         => Select2::THEME_DEFAULT,
                                    ] ); ?>
                                </div>

                        </div>
                    </div>
                </div>
				<div class="container-fluid">

				</div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
