$("#tipe-id").change(function () {
    var tipe = this.value;
    var option = $('#jenis-id');
    option.find('option').remove().end();
    for (let val in setcmb[tipe].sort) {
        option.append('<option value="' + val + '">' + setcmb[tipe].sort[val] + '</option>');
    }
    option.prop("selectedIndex", 0);


});
$("#tipe-id").trigger('change');

jQuery(function ($) {
    $('#TglKu_id').kvDatepicker('show');
    $('#TglKu_id').kvDatepicker('hide');
    $("#tipe-id").change(function () {
        var tipe = this.value;
        if (tipe === "Neraca T" || tipe === "Laba-Rugi Tri" || tipe === "Pendapatan Tri" || tipe === "Laba-Rugi Semester" || tipe === "Pendapatan Semester" || tipe === "Laba-Rugi Mini") {
            $(".jurnalHide").hide();
        } else {
            $(".jurnalHide").show();
        }
    });
});