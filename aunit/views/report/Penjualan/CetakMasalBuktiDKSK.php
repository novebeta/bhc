<?php
use aunit\components\FormField;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\components\General;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
\aunit\assets\AppAsset::register($this);
$escape = new JsExpression("function(m) { return m; }");
$this->title = 'Cetak Masal Bukti DK & SK';
$this->params['breadcrumbs'][] = $this->title;
$formatter = \Yii::$app->formatter;
$urlDetail = Url::toRoute(['report/details']);
?>
    <div class="dksk-form">
        <?php
        $form = ActiveForm::begin(['id' => 'form_dksk_id']);
        \aunit\components\TUi::form(
            [
                'class' => "row-no-gutters",
                'items' => [
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "form-group col-md-6",
                                'items' => [
                                    ['class' => "col-sm-5", 'items' => ":labellaporan"],
                                    ['class' => "col-sm-19", 'items' => ":laporan"]

                                ],
                            ],
                        ],
                    ],
                    ['class' => "form-group col-md-6",
                        'items' => [
                            ['class' => "col-sm-5", 'items' => ":labeltgl"],
                            ['class' => "col-sm-9", 'items' => ":AwalTgl"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-9", 'items' => ":AkhirTgl"],

                        ],
                    ],
                    ['class' => "form-group col-md-18",
                        'items' => [
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-6", 'items' => ":combo"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-9", 'items' => ":filter"],
                        ],
                    ],

                    [
                        'class' => "row col-md-24",
                        'style' => "margin-bottom: 3px;",
                        'items' => '<br><table id="detailGrid"></table><div id="detailGridPager"></div>'
                    ],
                ]
            ],
            [
                'class' => "pull-right",
                'items' => ":btnPrint"
            ],
            [
                /* label */
                ":labellaporan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Laporan</label>',
                ":labeltgl" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tanggal</label>',
                ":AwalTgl" => FormField::dateInput(['name' => 'dtpDKSKTgl1', 'config' => ['id' => 'dtpDKSKTgl1', 'value' => Yii::$app->formatter->asDate('-3 month', 'yyyy-MM-dd')]]),
                ":AkhirTgl" => FormField::dateInput(['name' => 'dtpDKSKTgl2', 'config' => ['id' => 'dtpDKSKTgl2', 'value' => Yii::$app->formatter->asDate('now', 'yyyy-MM-dd')]]),
                ":laporan" => FormField::combo('cmbDKSKTgl', ['config' => ['id' => 'cmbDKSKTgl', 'value' => 'Data Konsumen', 'data' => [
                    'ttdk.DKTgl' => 'Data Konsumen',
                    'ttsk.SKTgl' => 'Surat Jalan Konsumen',
                ]], 'extraOptions' => ['simpleCombo' => true]]),
                ":combo" => FormField::combo('combo', ['config' => ['id' => 'combo', 'value' => '', 'data' => [
                    "tdcustomer.CusNama" => "Nama Konsumen",
                    "ttdk.DKNo" => "No Data Konsumen",
                    "ttdk.SalesNama" => "Nama Sales",
                    "ttdk.DKJenis" => "Jenis",
                    "ttdk.LeaseKode" => "Leasing",
                    "tmotor.MotorType" => "Type Motor",
                    "tmotor.MotorWarna" => "Warna Motor",
                    "tmotor.MotorNoMesin" => "No Mesin",
                    "tmotor.MotorNoRangka" => "No Rangka",
                    "ttdk.DKMemo" => "Keterangan",
                    "ttdk.INNo" => "No Inden",
                    "ttdk.NamaHadiah" => "Nama Hadiah",
                    "ttdk.ProgramNama" => "Nama Program",
                    "ttdk.UserID" => "User",
                    "tmotor.SKNo" => "No Surat Jln Konsumen",
                    "ttdk.SPKNo" => "No SPK",
                ]], 'extraOptions' => ['simpleCombo' => true]]),
                ":filter" => Html::textInput('txtPenerima', '--', ['id' => 'txtPenerima', 'class' => 'form-control']).
                             Html::hiddenInput( 'DKSKNoList', '' ).
                             Html::hiddenInput( 'StatPrint', '1', [ 'id' => 'StatPrint' ] ),
                ":btnPrint"             => Html::button( '<i class="glyphicon glyphicon-print"></i>  Print ', [ 'class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:80px' ] ),

            ]
        );
        ActiveForm::end();
        ?>
    </div>

<?php
$urlPrint = Url::toRoute( [ 'report/print-dksk', 'id' => 'id' ] );
$this->registerJs( <<< JS
       
     window.reload = true;
     $('#detailGrid').utilJqGrid({
        url: '$urlDetail',
        height: 320,        
	    datatype: "json",
	    scrollrows : true,
        serializeRowData: function (postdata) {
            let header = $('#form_dksk_id').utilForm().getData();
            postdata = Object.assign(postdata, header);
            return postdata;
        },
       
        postData: $('#form_dksk_id').serialize(),   
        loadonce:true,
        rowNum: 100000,
        navButtonTambah: false,
        rownumbers: true,    
        multiselect:true,  
        colModel: [
            {
                name: 'DKSKNo',
                label: 'No DatKons',
                width: 90
            },
            {
                name: 'DKSKTgl',
                label: 'Tgl DK',
                width: 75
            },
            {
                name: 'CusNama',
                label: 'Nama Konsumen',
                width: 150,
            },
            {
                name: 'LeaseKode',
                label: 'Lease',
                width: 80,
            },
            {
                name: 'MotorType',
                label: 'Type Motor',
                width: 140,
            },
            {
                name: 'MotorWarna',
                label: 'Warna Motor',
                width: 100,
            },
            {
                name: 'MotorNoMesin',
                label: 'No Mesin',
                width: 100,
            },
            {
                name: 'SalesNama',
                label: 'Sales',
                width: 120,
            },
            {
                name: 'TeamKode',
                label: 'Team',
                width: 100,
            }
        ],
    }).init().fit($('.box-body'));
     
    function reloadGrid(load=true){
        if(!window.reload) return;
        var grid = $('#detailGrid'); 
        var CMB1 = $("#cmbDKSKTgl");
        var CMB1_value = CMB1.val();
         
        if (CMB1_value === 'ttsk.SKTgl'){
            grid.setColProp('DKSKNo',{jsonmap:'SKNo'}); 
            grid.jqGrid('setLabel', 'DKSKNo', 'No SK');
            grid.setColProp('DKSKTgl',{jsonmap:'SKTgl'}); 
            grid.jqGrid('setLabel', 'DKSKTgl', 'Tgl SK');   
        }else{
            grid.setColProp('DKSKNo',{jsonmap:'DKNo'}); 
            grid.jqGrid('setLabel', 'DKSKNo', 'No DatKons');
            grid.setColProp('DKSKTgl',{jsonmap:'DKTgl'}); 
            grid.jqGrid('setLabel', 'DKSKTgl', 'Tgl DK'); 
        }                
        if (load) grid.jqGrid('setGridParam',{datatype:'json',postData: $('#form_dksk_id').serialize()}).trigger('reloadGrid');
    }
    
    window.reloadGrid = reloadGrid;
    
    $('#dtpDKSKTgl1').on('change', function(){
        reloadGrid(); 
    });
    $('#dtpDKSKTgl2').on('change', function(){
        reloadGrid(); 
    });
    $('#cmbDKSKTgl').change(function() {
        reloadGrid();  
    });  
    
    
    // $('#cmbDKSKTgl','#dtpDKSKTgl1', 'dtpDKSKTgl2').change(function() {
    //     reloadGrid();  
    // });  
    reloadGrid();
    
    $('#btnPrint').click(function (event) {
        var records = [];
        var myGrid = $('#detailGrid'),
        selRowId = myGrid.jqGrid ('getGridParam', 'selarrrow');
        selRowId.forEach(function(itm,ind) {
            records.push($('#detailGrid').jqGrid ('getRowData', itm).DKSKNo);
        });
        if($('#cmbDKSKTgl').val() == 'ttsk.SKTgl'){
            $('#StatPrint').val(2);
        }
        $('[name=DKSKNoList]').val(records.join(';'));
        $('#form_dksk_id').attr('action','$urlPrint');
        $('#form_dksk_id').attr('target','_blank');
        $('#form_dksk_id').submit();
	  });
    
    

JS
);
