$("#tipe-id").change(function () {
    var tipe = this.value;
    if(tipe === 'DK - Status' || tipe === 'Piutang Via Kas & Bank' || tipe === 'Piutang Via Kasir' || tipe ===  'Piutang Tagih'){
        $("#bbn_lbl_id").html('Status');
        $("#bbn_cmb_id").val('Belum');
    }else{
        $("#bbn_lbl_id").html('BBN');
        $("#bbn_cmb_id").val('%');
    }
});

$("#tgl2_id").change(function () {
    var tgl1 = $('#tgl1_id').val();
    var tgl2 = this.value;
    console.log(tgl1);
    console.log(tgl2);
    console.log('tgl1 < tgl2', tgl1 < tgl2);
    if(tgl2 < tgl1){
        $('#tgl1_id').val(tgl2);
    }
});

$("#tgl2_id").trigger('change');
$("#tipe-id").trigger('change');