<?php
use aunit\components\FormField;
use aunit\models\Tdmotortype;
use aunit\models\Tdmotorwarna;
use aunit\models\Tdsales;
use aunit\models\Tdteam;
use kartik\date\DatePicker;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
$this->title                     = 'Daftar Data Konsumen';
$this->params[ 'breadcrumbs' ][] = $this->title;
$formatter                       = \Yii::$app->formatter;

$sqlTypeMotor = Yii::$app->db
    ->createCommand( "
	SELECT 'Semua' AS label, '%' AS value ,  '' AS MotorType
	UNION
	SELECT MotorType As label, MotorType AS value, MotorType FROM tdmotortype  
	ORDER BY MotorType " )
    ->queryAll();
$cmbTypeMotor = ArrayHelper::map( $sqlTypeMotor, 'value', 'label' );

$sqlWarnaMotor = Yii::$app->db
    ->createCommand( "
	SELECT 'Semua' AS label, '%' AS value ,  '' AS MotorWarna
	UNION
	SELECT MotorWarna As label, MotorWarna AS value, MotorWarna FROM tdmotorwarna  
	ORDER BY MotorWarna  " )
    ->queryAll();
$cmbWarnaMotor = ArrayHelper::map( $sqlWarnaMotor, 'value', 'label' );

?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
            <div class="box-body">
                <div class="container-fluid">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Laporan</label>
                            <div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Data Konsumen 1'        => 'Data Konsumen 1',
									'Data Konsumen 2'        => 'Data Konsumen 2',
									'Data Konsumen 3'        => 'Data Konsumen 3',
									'Data Konsumen 4'        => 'Data Konsumen 4',
									'Data Konsumen 5'        => 'Data Konsumen 5',
									'Scheme & SCP'           => 'Scheme & SCP',
									'DK - Status'            => 'DK - Status',
									'Repeat Order'           => 'Repeat Order',
									'Piutang Via Kas & Bank' => 'Piutang Via Kas & Bank',
									'Piutang Via Kasir'      => 'Piutang Via Kasir',
									'Piutang Tagih'          => 'Piutang Tagih',
									'BBN - Status'           => 'BBN - Status',
									'BBN Progresif'          => 'BBN Progresif',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Format</label>
                            <div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="container-fluid">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Tgl DK</label>
                        </div>
                    </div>
                    <div class="col-md-14">
                        <div class="form-group">
                            <div class="col-md-6">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
                                        'id'            => 'tgl1_id',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now', '01/MM/yyyy' ),
										'pluginOptions' => [
											'autoclose'      => true,
											'format'         => 'dd/mm/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                            <div class="col-md-6">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
                                        'id'            => 'tgl2_id',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now', 'dd/MM/yyyy' ),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                            <label class="control-label col-sm-1 jurnalHide">RO</label>
                            <div class="col-sm-2">
								<? try {
									echo NumberControl::widget( [
										'name'  => 'ro',
										'value' => 0,
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                            <label class="control-label col-sm-4 jurnalHide">Tgl Pelunasan</label>
                            <div class="col-md-5">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgllunas',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now', 'dd/MM/yyyy' ),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Team Sales</label>
                        </div>
                    </div>
                    <div class="col-md-14">
                        <div class="form-group">
                            <div class="col-sm-8">
	                            <? echo Select2::widget( [
		                            'name'         => 'TeamKode',
		                            'data'         => ArrayHelper::map(Tdteam::find()->select2( 'TeamKode', [ 'TeamKode' ], [ 'TeamKode' => SORT_ASC ], [ "condition" => '', "params" => [] ],true ), "value", 'label' ),
		                            'options'      => ['class' => 'form-control' ],
		                            'theme'        => Select2::THEME_DEFAULT,
		                            'pluginEvents' => [
			                            "change" => new JsExpression( "function(e) {	
											$('#SalesKode_id').utilSelect2().loadRemote('" . \yii\helpers\Url::toRoute(['tdsales/find']) . "', {  mode: 'combo', value: 'SalesKode', label: ['SalesNama'], condition : 'TeamKode = :TeamKode', params:{':TeamKode':this.value}, allOption:true }, true);
											}" )
		                            ]
	                            ] ); ?>
                            </div>
                            <div class="col-sm-16">
	                            <? echo Select2::widget( [
		                            'name'         => 'SalesKode',
		                            'data'         => ArrayHelper::map( Tdsales::find()->select2( 'SalesKode', [ 'SalesNama' ], [ 'SalesNama' => SORT_ASC ], [ "condition" => '', "params" => [] ],true ), "value", 'label' ),
		                            'options'      => [ 'id' => 'SalesKode_id', 'class' => 'form-control' ],
		                            'theme'        => Select2::THEME_DEFAULT
	                            ] ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Jenis</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="col-sm-24">
								<?= Html::dropDownList( 'jenis', null, [
									"%"      => "Semua",
									"Tunai"  => "Tunai",
									"Kredit" => "Kredit",
								], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Leasing</label>
                        </div>
                    </div>
                    <div class="col-md-14">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? echo FormField::combo( 'LeaseKode', [
									'name'         => 'leasing',
									'extraOptions' => [
										'allOption' => true,
										'altLabel'  => [ 'LeaseNama' ],
                                        'altOrder'=> ['LeaseStatus' => SORT_ASC, 'LeaseKode' => SORT_ASC],
                                        'altCondition'=> "LeaseStatus = 'A' "
									]
								] ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label id="bbn_lbl_id" class="control-label col-sm-24">BBN</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="col-sm-24">
								<?= Html::dropDownList( 'bbn', null, [
									"%"     => "Semua",
									"Sudah" => "Sudah",
									"Belum" => "Belum",
								], ['id'=> 'bbn_cmb_id', 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Type Motor</label>
                        </div>
                    </div>
                    <div class="col-md-14">
                        <div class="form-group">
                            <div class="col-sm-10">
	                            <? echo Select2::widget( [
		                            'name'         => 'MotorType',
		                            'data'         => ArrayHelper::map( Tdmotortype::find()->select2( 'MotorType', [ 'MotorType' ], [ 'MotorType' => SORT_ASC ], [ "condition" => '', "params" => [] ],true ), "value", 'label' ),
		                            'options'      => ['class' => 'form-control' ],
		                            'theme'        => Select2::THEME_DEFAULT,
		                            'pluginEvents' => [
			                            "change" => new JsExpression( "function(e) {	
											$('#MotorWarna_id').utilSelect2().loadRemote('" . \yii\helpers\Url::toRoute(['tdmotorwarna/find']) . "', {  mode: 'combo', value: 'MotorWarna', label: ['MotorWarna'], condition : 'MotorType = :MotorType', params:{':MotorType':this.value}, allOption:true }, true);
											}" )
		                            ]
	                            ] ); ?>
                            </div>
                            <label class="control-label col-sm-4">Warna Motor</label>
                            <div class="col-sm-10">
	                            <? echo Select2::widget( [
		                            'name'         => 'MotorWarna',
		                            'data'         => ArrayHelper::map( Tdmotorwarna::find()->select2( 'MotorWarna', [ 'MotorWarna' ], [ 'MotorWarna' => SORT_ASC ], [ "condition" => '', "params" => [] ],true ), "value", 'label' ),
		                            'options'      => [ 'id' => 'MotorWarna_id', 'class' => 'form-control' ],
		                            'theme'        => Select2::THEME_DEFAULT
	                            ] ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">No SK</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="col-sm-24">
								<?= Html::dropDownList( 'nosk', null, [
									"%"     => "Semua",
									"Sudah" => "Sudah",
									"Belum" => "Belum",
								], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Sort</label>
                        </div>
                    </div>
                    <div class="col-md-14">
                        <div class="form-group">
                            <div class="col-sm-10">
								<?= Html::dropDownList( 'sort', null, [
									"ttdk.DKNo"            => "No Data Konsumen",
									"tdcustomer.CusNama"   => "Nama Konsumen",
									"ttdk.CusKode"         => "Kode Konsumen",
									"ttdk.SalesKode"       => "Kode Sales ",
									"ttdk.LeaseKode"       => "Kode Lease",
									"ttdk.DKMemo"          => "Keterangan",
									"ttdk.DKJenis"         => "Jenis",
									"ttdk.DKLunas"         => "Lunas",
									"ttdk.INNo"            => "No Inden",
									"ttdk.DKTglTagih"      => "Tgl Tagih",
									"ttdk.DKPOTgl"         => "Tgl PO",
									"ttdk.DKPONo"          => "No PO",
									"KMTgl"                => "Tgl Cair",
									"ttdk.ProgramNama"     => "Nama Program",
									"ttdk.UserID"          => "User",
									"tmotor.SKNo"          => "No Surat Jln Konsumen",
									"ttdk.DKTgl"           => "Tgl Data Kons",
									"ttdk.DKHarga"         => "Harga OTR",
									"ttdk.DKHPP"           => "HPP",
									"ttdk.DKDPTotal"       => "DP Total",
									"ttdk.DKDPLLeasing"    => "DP Leasing",
									"ttdk.DKDPTerima"      => "DP Terima",
									"ttdk.DKNetto"         => "Netto",
									"ttdk.ProgramSubsidi"  => "Subsidi Program",
									"ttdk.PrgSubsSupplier" => "Subsidi Supplier",
									"ttdk.PrgSubsDealer"   => "Subsidi Dealer",
									"ttdk.PrgSubsFincoy"   => "Subsidi Fincoy",
									"ttdk.PotonganHarga"   => "Potongan Harga",
									"ttdk.ReturHarga"      => "Retur Harga",
									"ttdk.PotonganKhusus"  => "Potongan Khusus",
									"ttdk.InsentifSales"   => "Insentif Sales",
									"ttdk.BBN"             => "BBN",
								], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
                            <div class="col-sm-4">
								<?= Html::dropDownList( 'order', null, [
									"ASC"  => "ASC",
									"DESC" => "DESC",
								], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
                            <label class="control-label col-sm-3">Kabupaten</label>
                            <div class="col-sm-7">
								<? echo FormField::combo( 'Kabupaten', [
									'name'         => 'kabupaten',
									'extraOptions' => [
										'altLabel'  => [ 'Kabupaten' ],
										'altOrder'=> ['AreaStatus' => SORT_ASC, 'Kabupaten' => SORT_ASC],
										'altCondition'=> "AreaStatus IN ('A','1')",
										'allOption' => true, 'simpleCombo' => true
									]
								] ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Inden</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="col-sm-24">
								<?= Html::dropDownList( 'inden', null, [
									"%"   => "Semua",
									"IN" => "Ada",
									"--" => "Tidak",
								], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
$this->registerJs( $this->render( 'DaftarDataKonsumen.js' ) );