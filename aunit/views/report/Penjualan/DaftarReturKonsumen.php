<?php
use aunit\components\FormField;
use aunit\models\Tdmotortype;
use aunit\models\Tdmotorwarna;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
$this->title                   = 'Daftar Retur Konsumen';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;

$sqlLeasing = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, '' AS LeaseStatus, '' AS LeaseKode
	UNION
	SELECT LeaseKode AS value, LeaseNama As label , LeaseStatus , LeaseKode FROM tdLeasing 
	WHERE LeaseStatus = 'A' 
	ORDER BY LeaseStatus, LeaseKode" )
	->queryAll();
$cmbLeasing = ArrayHelper::map( $sqlLeasing, 'value', 'label' );

$sqlTypeMotor = Yii::$app->db
	->createCommand( "
	SELECT 'Semua' AS label, '%' AS value ,  '' AS MotorType
	UNION
	SELECT MotorType As label, MotorType AS value, MotorType FROM tdmotortype  
	ORDER BY MotorType " )
	->queryAll();
$cmbTypeMotor = ArrayHelper::map( $sqlTypeMotor, 'value', 'label' );

$sqlWarnaMotor = Yii::$app->db
	->createCommand( "
	SELECT 'Semua' AS label, '%' AS value ,  '' AS MotorWarna
	UNION
	SELECT MotorWarna As label, MotorWarna AS value, MotorWarna FROM tdmotorwarna  
	ORDER BY MotorWarna  " )
	->queryAll();
$cmbWarnaMotor = ArrayHelper::map( $sqlWarnaMotor, 'value', 'label' );

$sqlTeam = Yii::$app->db
	->createCommand( "
	SELECT 'Semua' AS label, '%' AS value,  '' AS TeamStatus, '' AS TeamKode
	UNION
	SELECT TeamKode As label, TeamKode AS value,  TeamStatus, TeamKode FROM tdteam  
	where TeamStatus IN ('A','1')
	ORDER BY TeamStatus, TeamKode " )
	->queryAll();
$cmbTeam = ArrayHelper::map( $sqlTeam, 'value', 'label' );

$sqlSales = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, '' AS SalesStatus, '' AS SalesKode
	UNION
	SELECT SalesKode AS value, SalesNama As label , SalesStatus , SalesKode FROM tdSales 
	WHERE SalesStatus = 'A' 
	ORDER BY SalesStatus, SalesKode" )
	->queryAll();
$cmbSales = ArrayHelper::map( $sqlSales, 'value', 'label' );

$sqlKabupaten = Yii::$app->db
	->createCommand( "
	SELECT 'Semua' AS label, '%' AS value,  '' AS AreaStatus, '' AS Kabupaten
	UNION
	SELECT Kabupaten As label, Kabupaten AS value,  AreaStatus, Kabupaten FROM tdarea  
	where AreaStatus IN ('A','1')
	GROUP BY Kabupaten
	ORDER BY AreaStatus, Kabupaten " )
	->queryAll();
$cmbKabupaten = ArrayHelper::map( $sqlKabupaten, 'value', 'label' );

$sqlwarehouse = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, '' AS LokasiStatus, '' AS LokasiJenis, '' AS LokasiKode
	UNION
	SELECT LokasiKode AS value, LokasiNama AS label, LokasiStatus, LokasiJenis, LokasiKode FROM tdlokasi 
	WHERE LokasiJenis <> 'Kas' 
	ORDER BY LokasiStatus, LokasiKode" )
	->queryAll();
$cmbwarehouse = ArrayHelper::map( $sqlwarehouse, 'value', 'label' );

$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
function formatDual(result) {
    return '<div class="row">' +
           '<div class="col-md-5">' + result.id + '</div>' +
           '<div class="col-md-19">' + result.text + '</div>' +
           '</div>';
}
function formatWarehouse(result) {
    return '<div class="row">' +
           '<div class="col-md-6">' + result.id + '</div>' +
           '<div class="col-md-18">' + result.text + '</div>' +
           '</div>';
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );

?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Retur Konsumen 1'  => 'Retur Konsumen 1',
									'Retur Konsumen 2'  => 'Retur Konsumen 2',
									'Retur Konsumen 3'  => 'Retur Konsumen 3',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-footer">
				<div class="container-fluid">
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-sm-24">
								<?= Html::dropDownList( 'jenis', null, [
									"ttrk.RKTgl" => "Tgl RK",
									"ttsk.SKTgl" => "Tgl SK",
									"ttdk.DKTgl" => "Tgl DK",
								], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
                        </div>
					</div>
					<div class="col-md-14">
                        <div class="form-group">
                            <div class="col-md-5">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
										'pluginOptions' => [
											'autoclose'      => true,
											'format'         => 'dd/mm/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                            <div class="col-md-5">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
							<label class="control-label col-sm-2">Sort</label>
							<div class="col-sm-8">
								<?= Html::dropDownList( 'sort', null, [
										"ttrk.RKNo"								    =>   	"No Retur Kons",    
										"ttsk.SKNo"                                 =>      "No Surat Jln",     
										"ttdk.DKNo"                                 =>      "No Dat Kons",      
										"ttrk.RKTgl"                                =>      "Tgl RK ",          
										"ttsk.SKTgl"                                =>      "Tgl SK ",          
										"ttdk.DKTgl"                                =>      "Tgl DK ",          
										"tdcustomer.CusNama"                        =>      "Nama Konsumen",    
										"ttdk.CusKode"                              =>      "Kode Konsumen",    
										"ttdk.SalesKode"                            =>      "Kode Sales ",      
										"ttdk.LeaseKode"                            =>      "Kode Lease",       
										"ttrk.RKMemo"                               =>      "Keterangan",       
										"ttrk.LokasiKode"                           =>      "Kode Lokasi",      
										"ttrk.UserID"                               =>      "User",             
										"tmotor.MotorNoMesin"                       =>      "NoMesin Motor",    
										"tmotor.MotorNoRangka"                      =>      "No Rangka Motor",  
										"tmotor.MotorWarna"                         =>      "Warna Motor",      
										"tmotor.MotorType"                          =>      "Type Motor",       
										"tmotor.MotorTahun"                         =>      "Tahun Motor",      
										"IFNULL(ttgeneralledgerhd.GLLink,'--')"     =>      "No JT",            
										"IFNULL(ttdk.DKHarga,0)"                    =>      "Harga OTR",        
										"IFNULL(ttdk.DKHPP,0)"                      =>      "HPP",              
										"IFNULL(ttdk.DKDPTotal,0)"                  =>      "DP Total",         
										"IFNULL(ttdk.DKDPLLeasing,0)"               =>      "DP Leasing",       
										"IFNULL(ttdk.DKDPTerima,0)"                 =>      "DP Terima",        
										"IFNULL(ttdk.DKNetto,0)"                    =>      "Netto",            
										"IFNULL(ttdk.ProgramSubsidi,0)"             =>      "Subsidi Program",  
										"IFNULL(ttdk.PrgSubsSupplier,0)"            =>      "Subsidi Supplier", 
										"IFNULL(ttdk.PrgSubsDealer,0)"              =>      "Subsidi Dealer",   
										"IFNULL(ttdk.PrgSubsFincoy,0)"              =>      "Subsidi Fincoy",   
										"IFNULL(ttdk.PotonganHarga,0)"              =>      "Potongan Harga",   
										"IFNULL(ttdk.ReturHarga,0)"                 =>      "Retur Harga",      
										"IFNULL(ttdk.PotonganKhusus,0)"             =>      "Potongan Khusus",  
										"IFNULL(ttdk.BBN,0)"                        =>      "BBN",              
										"IFNULL(ttdk.Jaket,0)"                      =>      "Jaket",                              
								], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
							<div class="col-sm-4">
								<?= Html::dropDownList( 'order', null, [
									"ASC" => "ASC",
									"DESC" => "DESC",
								], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
                        </div>
                    </div>
					<div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Kabupaten</label>
                        </div>
					</div>
					<div class="col-md-5">
                        <div class="form-group">
							<div class="col-sm-24">
								<? echo Select2::widget( [
									'name'          => 'kabupaten',
									'data'          => $cmbKabupaten,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
								] ); ?>
                            </div>
	                    </div>
                    </div>
                </div>
				<div class="container-fluid">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Sales</label>
                        </div>
					</div>
					<div class="col-md-14">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? echo Select2::widget( [
									'name'          => 'sales',
									'data'          => $cmbSales,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatDual' ),
										'templateSelection' => new JsExpression( 'formatDual' ),
										'matcher'           => new JsExpression( 'matchCustom' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
                            </div>
                        </div>
					</div>
					<div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">No DK</label>
                        </div>
					</div>
					<div class="col-md-5">
                        <div class="form-group">
							<div class="col-sm-24">
								<?= Html::dropDownList( 'nodk', null, [
									"%" => "Semua",
									"DK" => "Ada",
									"--" => "Tidak",
								], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
	                    </div>
                    </div>
                </div>
				<div class="container-fluid">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Warehouse</label>
                        </div>
					</div>
					<div class="col-md-14">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? echo Select2::widget( [
									'name'          => 'lokasi',
									'data'          => $cmbwarehouse,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatWarehouse' ),
										'templateSelection' => new JsExpression( 'formatWarehouse' ),
										'matcher'           => new JsExpression( 'matchCustom' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="container-fluid">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Type Motor</label>
                        </div>
					</div>
					<div class="col-md-14">
                        <div class="form-group">
                            <div class="col-sm-10">
	                            <? echo Select2::widget( [
		                            'name'         => 'MotorType',
		                            'data'         => ArrayHelper::map( Tdmotortype::find()->select2( 'MotorType', [ 'MotorType' ], [ 'MotorType' => SORT_ASC ], [ "condition" => '', "params" => [] ],true ), "value", 'label' ),
		                            'options'      => ['class' => 'form-control' ],
		                            'theme'        => Select2::THEME_DEFAULT,
		                            'pluginEvents' => [
			                            "change" => new JsExpression( "function(e) {	
											$('#MotorWarna_id').utilSelect2().loadRemote('" . \yii\helpers\Url::toRoute(['tdmotorwarna/find']) . "', {  mode: 'combo', value: 'MotorWarna', label: ['MotorWarna'], condition : 'MotorType = :MotorType', params:{':MotorType':this.value}, allOption:true }, true);
											}" )
		                            ]
	                            ] ); ?>
                            </div>
							<label class="control-label col-sm-4">Warna Motor</label>
                            <div class="col-sm-10">
	                            <? echo Select2::widget( [
		                            'name'         => 'MotorWarna',
		                            'data'         => ArrayHelper::map( Tdmotorwarna::find()->select2( 'MotorWarna', [ 'MotorWarna' ], [ 'MotorWarna' => SORT_ASC ], [ "condition" => '', "params" => [] ],true ), "value", 'label' ),
		                            'options'      => [ 'id' => 'MotorWarna_id', 'class' => 'form-control' ],
		                            'theme'        => Select2::THEME_DEFAULT
	                            ] ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php


 