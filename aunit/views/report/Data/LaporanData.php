<?php
use aunit\models\Tdarea;
use aunit\models\Tdbbn;
use aunit\models\Tdcustomer;
use aunit\models\Tddealer;
use aunit\models\Tdleasing;
use aunit\models\Tdlokasi;
use aunit\models\Tdmotortype;
use aunit\models\Tdmotorwarna;
use aunit\models\Tdprogram;
use aunit\models\Tdsales;
use aunit\models\Tdsupplier;
use aunit\models\Tuser;
use aunit\models\Tuakses;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
$this->title                     = 'Laporan Data';
$this->params[ 'breadcrumbs' ][] = $this->title;
$formatter                       = \Yii::$app->formatter;
$sql  = Yii::$app->db
	->createCommand( "SELECT CONCAT(MotorType,' - ',MotorNama) as label, MotorType as value FROM tdmotortype" )
	->queryAll();
$cmb  = ArrayHelper::map( $sql, 'value', 'label' );
$from = Yii::$app->formatter->asDate( 'now', '01/MM/yyyy' );
$to   = Yii::$app->formatter->asDate( 'now', 'dd/MM/yyyy' );
?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
            <div class="box-body">
                <div class="container-fluid">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Laporan</label>
                            <div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'tdmotortype'  => 'Type Motor',
									'tdmotorharga' => 'Harga Motor',
									'tdmotorWarna' => 'Warna Motor',
									'tdlokasi'     => 'WareHouse',
									'tdSupplier'   => 'Supplier',
									'tddealer'     => 'Dealer',
									'tdSales'      => 'Sales',
									'tdCustomer'   => 'Konsumen',
									'tdleasing'    => 'Leasing',
									'tdprogram'    => 'Program',
									'tdarea'       => 'Area',
									'tdbbn'        => 'BBN',
									'tuser'        => 'User',
									'tuakses'      => 'Hak Akses',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Format</label>
                            <div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="container-fluid">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Kode 1</label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <div class="col-sm-24">
								<?= Html::dropDownList( 'kode1', null, [],
									[ 'id' => 'kode1-id', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Tanggal</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'id'            => 'tgl1',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now', '01/MM/yyyy' ),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										],
										'pluginEvents'  => [
											"change" => new JsExpression("function(e) {
											 window.fillKonsumen();
											}")
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label col-sm-5">s/d</label>
                            <div class="col-sm-19">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'id'            => 'tgl2',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now', 'dd/MM/yyyy' ),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										],
										'pluginEvents'  => [
											"change" => new JsExpression("function(e) {
											 window.fillKonsumen();
											}")
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Kode 2</label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <div class="col-sm-24">
								<?= Html::dropDownList( 'kode2', null, [],
									[ 'id' => 'kode2-id', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Status</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="col-sm-24">
								<?= Html::dropDownList( 'status', null, [
									''  => 'Semua',
									'A' => 'Aktif',
									'N' => 'Non Aktif',
								], [ 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Sort</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-sm-16">
								<?= Html::dropDownList( 'sort', null, [],
									[ 'id' => 'sort-id', 'class' => 'form-control' ] ) ?>
                            </div>
                            <div class="col-sm-8">
								<?= Html::dropDownList( 'order', null, [
									'ASC'  => 'ASC',
									'DESC' => 'DESC'
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
$arr      = [
	'tdmotortype'  => [
		'cmb'  => Tdmotortype::find()->combo(),
		'sort' => [
			'MotorType'  => 'Type Motor',
			'TypeStatus' => 'Status',
		]
	],
	'tdmotorharga' => [
		'cmb'  => Tdmotortype::find()->combo(),
		'sort' => [
			'MotorType'  => 'Type Motor',
			'TypeStatus' => 'Status',
		]
	],
	'tdmotorWarna' => [
		'cmb'  => Tdmotorwarna::find()->combo(),
		'sort' => [
			'MotorWarna' => 'Warna Motor'
		]
	],
	'tdlokasi'     => [
		'cmb'  => Tdlokasi::find()->combo(),
		'sort' => [
			'LokasiKode' => 'Lokasi'
		]
	],
	'tdSupplier'   => [
		'cmb'  => Tdsupplier::find()->combo(),
		'sort' => [
			'SupKode' => 'Kode Supplier'
		]
	],
	'tddealer'     => [
		'cmb'  => Tddealer::find()->combo(),
		'sort' => [
			'DealerKode' => 'Kode Dealer'
		]
	],
	'tdSales'      => [
		'cmb'  => Tdsales::find()->combo(),
		'sort' => [
			'SalesKode' => 'Kode Sales'
		]
	],
	'tdCustomer'   => [
		'cmb'  => Tdcustomer::find()->combo( [ 'from' => $from, 'to' => $to ] ),
		'sort' => [
			'CusKode' => 'Kode Customer'
		]
	],
	'tdleasing'    => [
		'cmb'  => Tdleasing::find()->combo(),
		'sort' => [
			'LeaseKode' => 'Kode Leasing'
		]
	],
	'tdprogram'    => [
		'cmb'  => Tdprogram::find()->combo(),
		'sort' => [
			'ProgramNama' => 'Nama Progam'
		]
	],
	'tuser'        => [
		'cmb'  => Tuser::find()->combo(),
		'sort' => [
			'UserID' => 'UserID'
		]
	],
	'tdarea'       => [
		'cmb'  => Tdarea::find()->combo(),
		'sort' => [
			'Kabupaten' => 'Kabupaten'
		]
	],
	'tdbbn'        => [
		'cmb'  => Tdbbn::find()->combo(),
		'sort' => [
			'Kabupaten' => 'Kabupaten',
			'MotorType' => 'Tipe Motor',
		]
	],
    'tuakses'        => [
    'cmb'  => Tuakses::find()->combodata(),
    'sort' => [
        'KodeAkses' => 'Kode Akses',
        'combodata' => 'Kode Akses',
    ]
]
];
$urlCombo = \yii\helpers\Url::toRoute( [ 'tdcustomer/find-combo' ] );
$this->registerJsVar( 'setcmb', $arr );
$this->registerJs( $this->render( 'LaporanData.js', [
	'urlCombo' => $urlCombo
] ) );