<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;


$this->title = 'User Log';
$this->params['breadcrumbs'][] = $this->title;
$formatter = \Yii::$app->formatter;

$sqlBank                           = Yii::$app->db
    ->createCommand( "SELECT NoAccount as value, NamaAccount as label FROM traccount WHERE (NoParent IN ('11020100','11020200','11020300','11030000')) AND StatusAccount = 'A'ORDER BY StatusAccount, NoAccount" )
    ->queryAll();
$cmbBank                           = ArrayHelper::map( $sqlBank, 'value', 'label' );

$sqlUserID = Yii::$app->db
    ->createCommand("	SELECT 'Semua' AS label, '%' AS value ,  '' AS UserID
	UNION
	SELECT IF(UserNama='',UserID,UserNama) As label,  UserID AS value, UserID FROM tuser  
	ORDER BY UserID ")
    ->queryAll();
$cmbUserID = ArrayHelper::map($sqlUserID, 'value', 'label');

$format = <<< SCRIPT
function formatUserLog(result) {
    return '<div class="row" style="margin-left: 0;">' +
           '<div class="col-md-6">' + result.id + '</div>' +
           '<div class="col-md-18">' + result.text + '</div>' +
           '</div>';
}

function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
function formatDual(result) {
     return '<div class="row" style="margin-left: 0;">' +
           '<div class="col-md-6">' + result.id + '</div>' +
           '<div class="col-md-18">' + result.text + '</div>' +
           '</div>';
}
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);


?>
<div class="laporanuserlog-form">
<?php
$form = ActiveForm::begin();
\aunit\components\TUi::form(
[
	'class' => "form-group",
	'items' => 
	[
        'class' => "col-md-24",
        'items' =>
        [
			['class' => "form-group col-md-2",'items' => ['style' => "margin-bottom: 2px;",'items' => ":LabelLaporan"],],
			['class' => "form-group col-md-11",'items' => ['style' => "margin-bottom: 2px;",'items' => ":Laporan"],],
			['class' => "form-group col-md-2",'items' => ['style' => "margin-bottom: 2px;",'items' => ":LabelFileType"],],
			['class' => "form-group col-md-5",'items' => ['style' => "margin-bottom: 2px;margin-left:-8px;",'items' => ":FileType"],],
			['class' => "form-group col-md-3",'items' => ['style' => "margin-bottom: 2px;",'items' => ":btnSubmit"],],
        ]
	],
],

[
    'class' => "row-no-gutters",
    'items' =>
	[[
		'class' => "form-group col-md-24",
		'items' => 
		[
			['class' => "form-group col-md-3",'items' => ['style' => "margin-bottom: 5px;",'items' => ":LabelTanggal"]],
            ['class' => "form-group col-md-5",'items' => ['style' => "margin-bottom: 5px;",'items' => ":Tgl1"]],
            ['class' => "form-group col-md-1",'items' => ['style' => "margin-bottom: 5px;margin-left:6px;",'items' => ":LabelSD"]],
            ['class' => "form-group col-md-5",'items' => ['style' => "margin-bottom: 5px;",'items' => ":Tgl2"]],
            ['class' => "form-group col-md-3",'items' => ['style' => "margin-bottom: 5px;margin-left:20px;",'items' => ":LabelTyJenis"]],
            ['class' => "form-group col-md-7",'items' => ['style' => "margin-bottom: 5px;",'items' => ":TyJenis"]],
            ['class' => "form-group col-md-3",'items' => ['style' => "margin-bottom: 5px;margin-left:0px;",'items' => ":LabelTyNama"]],
            ['class' => "form-group col-md-11",'items' => ['style' => "margin-bottom: 5px;",'items' => ":TyNama"]],
            ['class' => "form-group col-md-3",'items' => ['style' => "margin-bottom: 5px;margin-left:20px;",'items' => ":LabelNoTrans"]],
            ['class' => "form-group col-md-7",'items' => ['style' => "margin-bottom: 5px;",'items' => ":NoTrans"]],
            ['class' => "form-group col-md-3",'items' => ['style' => "margin-bottom: 5px;",'items' => ":LabelUserID"]],
            ['class' => "form-group col-md-11",'items' => ['style' => "margin-bottom: 5px;",'items' => ":UserID"]],
            ['class' => "form-group col-md-3",'items' => ['style' => "margin-bottom: 5px;margin-left:20px;",'items' => ":LabelSort"]],
            ['class' => "form-group col-md-4",'items' => ['style' => "margin-bottom: 5px;",'items' => ":TySearch"]],
            ['class' => "form-group col-md-3",'items' => ['style' => "margin-bottom: 5px;",'items' => ":TypeOrder"]],
        ],
    ],]
],

[
	":LabelLaporan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Laporan</label>',
	":LabelTanggal" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tanggal</label>',
	":LabelSD" => '<label class="control-label" style="margin: 0; padding: 6px 0;">s/d</label>',
	":LabelTyJenis" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis Proses</label>',
	":LabelTyNama" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Trans</label>',
	":LabelNoTrans" => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Trans</label>',
	":LabelUserID" => '<label class="control-label" style="margin: 0; padding: 6px 0;">UserID</label>',
	":LabelSort" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sort</label>',

	":Laporan" => Html::dropDownList('tipe', null,
		[
			'User Log' => 'User Log',
			'User Log DB' => 'User Log DB',
			'Login' => 'Login',
		],
		['id' => 'tipe', 'text' => 'Pilih tipe laporan', 'class' => 'form-control']
	),

    ":Tgl1" => DatePicker::widget([
        'name' => 'Tgl1',
        'type' => DatePicker::TYPE_INPUT,
		'value' => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),                    
		'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd/mm/yyyy'
        ]
    ]),

    ":Tgl2" => DatePicker::widget([
        'name' => 'Tgl2',
        'type' => DatePicker::TYPE_INPUT,
		'value' => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),       
		'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd/mm/yyyy'
        ]
    ]),

    ":TyJenis" => Html::dropDownList('TyJenis', null,
        [
            ''       => 'Semua',
            'Tambah' => 'Tambah',
            'Edit'   => 'Edit',
            'Hapus'  => 'Hapus',
            'Login'  => 'Login',
        ],
        ['id' => 'tgl_type', 'text' => 'Pilih tipe laporan', 'class' => 'form-control']
    ),

	":TyNama" => Html::dropDownList('TyNama', null,
		[
			'' => 'Semua',
			'ttss' => 'Surat Jalan Supplier',
			'ttfb' => 'Faktur Beli',
			'ttin' => 'Data Inden',
			'ttdk' => 'Data Konsumen',
			'ttsk' => 'Surat Jalan Konsumen',
			'ttrk' => 'Retur Konsumen',
			'ttsm' => 'Surat Jalan Mutasi ke POS',
			'ttpb' => 'Penerimaan Barang dari POS',
			'ttsd' => 'Surat Jalan Mutasi ke Dealer',
			'ttpd' => 'Penerimaan Barang dari Dealer',
			'ttkm' => 'Kas Masuk',
			'ttkk' => 'Kas Keluar',
			'ttbm' => 'Bank Masuk',
			'ttbk' => 'Bank Keluar',
			'ttgeneralledger' => 'Jurnal',
		],
		['id' => 'TyNama', 'class' => 'form-control']
	),

	":NoTrans" => Html::textInput('NoTrans', null, array
	('id' => 'NoTrans', 'class' => 'form-control')),

	":UserID" => Select2::widget([
		'name'          => 'user',
		'data'          => $cmbUserID,
		'options'       => [ 'class' => 'form-control' ],
		'theme'         => Select2::THEME_DEFAULT,
		'pluginOptions' => [
			'dropdownAutoWidth' => true,
			'templateResult'    => new JsExpression( 'formatDual' ),
			'templateSelection' => new JsExpression( 'formatDual' ),
			'matcher'           => new JsExpression( 'matchCustom' ),
			'escapeMarkup'      => $escape,
		],
	]),

	":TySearch" => Html::dropDownList('TySearch', null,
		[
			'LogTglJam' => 'Tanggal',
			'UserID'    => 'UserID',
			'LogJenis'  => 'Jenis Proses',
			'NoTrans'   => 'No Transaksi',
			'LogTabel'  => 'Tabel Database',
		],
		['id' => 'TySearch', 'class' => 'form-control']
	),

	":TypeOrder" => Html::dropDownList('TypeOrder', null,
		[
			'ASC' => 'ASC',
			'DESC' => 'DESC',
		],
		['id' => 'TypeOrder', 'class' => 'form-control']
	),

	":LabelFileType" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Format</label>',
	":FileType" => Html::dropDownList('filetype', null,
		[
			'pdf' => 'PDF',
			'xlsr' => 'EXCEL RAW',
			'xls' => 'EXCEL',
			'doc' => 'WORD',
			'rtf' => 'RTF',
		],
		['id' => 'tgl_type', 'text' => 'Pilih tipe laporan', 'class' => 'form-control']
	),

	":btnSubmit" => Html::submitButton('<i class="glyphicon glyphicon-new-window"></i>  Tampil',
	['class' => 'btn btn-primary', 'id' => 'btnSave', 'formtarget' => '_blank']),

]);

ActiveForm::end();
?>
</div>
 
<?php
