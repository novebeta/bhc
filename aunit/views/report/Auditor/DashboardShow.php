<?php
use yii\helpers\Url;

\aunit\assets\AppAsset::register($this);
\aunit\assets\JqwidgetsAsset::register($this);
$this->title = 'SADISTOCK';
$formatter = \Yii::$app->formatter;
$this->registerCss(".jqx-widget-content { font-size: 12px; }");

$rowdata = $data;
$param = $data['param'];
$this->registerJs(<<< JS

 
JS
);

?>
    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
<style>
    .table td{
        border: lightgrey solid 1px !important;
    }
    .table th{
        border: lightgrey solid 1px !important;
    }
</style>
    <div class="laporan-query col-md-24" style="padding-left: 0;">
        <div class="box box-default">
            <button class="btn pull-right" id='btnExcel' type="button" style="background-color:limegreen;width: 40px;height: 30px;margin: 10px;position: absolute;top: -45px;right: 10px"><i class="fa fa-file-excel-o fa-lg"></i></button>
            <div class="box-body">
                <div class="col-md-24">
                    <div >
                        <div class="box-body">

                            <div style="position: relative;height: 550px;overflow: auto;">
                                <table class="table table-bordered table-striped mb-0" id="table1">
                                    <thead>
                                    <tr >
                                        <th rowspan="2" scope="col" style="text-align: center">Type Motors (Per Warna)
                                        </th>
                                        <th colspan="4" scope="col" style="text-align: center">M-1</th>
                                        <th colspan="4" scope="col" style="text-align: center">M</th>
                                        <th colspan="4" scope="col" style="text-align: center">Growth %</th>
                                    </tr>
                                    <tr>
                                        <th scope="col" >Sales</th>
                                        <th scope="col">Distribusi</th>
                                        <th scope="col">Stok</th>
                                        <th scope="col">Stok Day</th>
                                        <th scope="col">Sales</th>
                                        <th scope="col">Distribusi</th>
                                        <th scope="col">Stok</th>
                                        <th scope="col">Stok Day</th>
                                        <th scope="col">Sales</th>
                                        <th scope="col">Distribusi</th>
                                        <th scope="col">Stok</th>
                                        <th scope="col">Stok Day</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($rowdata['data'] as $key){
                                        echo '<tr><td>'.$key['MotorNama'].'</td>';
//                                        echo '<td style="text-align: right">'.$key['SalesMBefore'].'</td>';
                                        echo '<td style="text-align: right">'.substr($key['SalesMBefore'], 0, -3).'</td>';
                                        echo '<td style="text-align: right">'.substr($key['DisMBefore'], 0, -3).'</td>';
                                        echo '<td style="text-align: right">'.substr($key['SalesCBefore'], 0, -3).'</td>';
                                        echo '<td style="text-align: right">'.substr($key['StockMBefore'], 0, -3).'</td>';
                                        echo '<td style="text-align: right">'.substr($key['SalesMNow'], 0, -3).'</td>';
                                        echo '<td style="text-align: right">'.substr($key['DisMNow'], 0, -3).'</td>';
                                        echo '<td style="text-align: right">'.substr($key['SalesCNow'], 0, -3).'</td>';
                                        echo '<td style="text-align: right">'.substr($key['StockMNow'], 0, -3).'</td>';
                                        echo '<td style="text-align: right">'.number_format(substr($key['SalesGrowth'], 0, -3), 2,",",".").'</td>';
                                        echo '<td style="text-align: right">'.number_format(substr($key['DisGrowth'], 0, -3), 2,",",".").'</td>';
                                        echo '<td style="text-align: right">'.number_format(substr($key['StockGrowth'], 0, -3), 2,",",".").'</td>';
                                        echo '<td style="text-align: right">'.substr($key['StockCNow'], 0, -3).'</td>';
                                    }
                                    ?>

                                    </tbody>
                                </table>

                            </div><br>


                        </div>
                        <div class="box-footer"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php
$this->registerJs(<<< JS

 $(document).ready(function () {
      $("#btnExcel").click(function(){
        TableToExcel.convert(document.getElementById("table1"), {
            name: "Sadistock.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
  });

JS
);


