jQuery(function ($) {
    $("#tipe-id").change(function () {
        var tipe = this.value;
        if (tipe === "Penjualan 1" || tipe === "Penjualan 1" || tipe === "Penjualan 3") {
            $(".lokasihide").show();
        } else {
            $(".lokasihide").hide();
        }
        if (tipe === "Rugi Laba Margin") {
            $(".leasinghide").show();
        } else {
            $(".leasinghide").hide();
        }
    });
    $('#tipe-id').trigger('change');

    //don't delete this (role past, if submit : tgl-1 = tgl-2)
    $("#submit-id").click(function () {
        var tgl_1 = $('#tgl-1').val();
        var tgl_2 = $('#tgl-2').val();
        if (tgl_2 < tgl_1){
            $("#tgl-1").val(tgl_2);
        }
    });
});



