<?php

use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\checkbox\CheckboxX;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;

?>


<?php
$this->title = 'Dashboard Rasio Konsolidasi';
$this->params['breadcrumbs'][] = $this->title;
$formatter = \Yii::$app->formatter;

$urlQuery = Url::toRoute(['report/query-dashboard-rasio-konsolidasi']);
$format = <<< SCRIPT
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
$this->registerJs(<<< JS
     
          
    Chart.register(ChartDataLabels);
    Chart.register(ChartjsPluginStacked100.default);
    function reloadGrid1(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
        $('#ajaxspinner1').show();
        $('#ajaxspinner2').show();
        $('#ajaxspinner3').show();
        $('#ajaxspinner4').show();
        $('#ajaxspinner5').show();
        $('#ajaxspinner6').show();
        $('#ajaxspinner7').show();
        $('#ajaxspinner8').show();
        $('#ajaxspinner9').show();
        $('#ajaxspinner10').show();
                
        $.ajax({
            type: 'POST',
            url: '$urlQuery',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            success:function(response) {
                console.log(response['Cr1Kota']);                                               
                $('#CashRatio1').remove();
                $('.1').append('<canvas id="CashRatio1"><canvas>');
                $('#CashRatio2').remove();
                $('.2').append('<canvas id="CashRatio2"><canvas>'); 
                $('#QuickRatio').remove();
                $('.3').append('<canvas id="QuickRatio"><canvas>');
                $('#WcRatio').remove();
                $('.4').append('<canvas id="WcRatio"><canvas>');             
                $('#ARDays').remove();
                $('.5').append('<canvas id="ARDays"><canvas>');
                $('#InventoryDays').remove();
                $('.6').append('<canvas id="InventoryDays"><canvas>');
                $('#GrossProfitRatio').remove();
                $('.7').append('<canvas id="GrossProfitRatio"><canvas>');
                $('#OprtProfitRatio').remove();
                $('.8').append('<canvas id="OprtProfitRatio"><canvas>');
                $('#NetProfitRatio').remove();
                $('.9').append('<canvas id="NetProfitRatio"><canvas>');
                $('#NPBTRatio').remove();
                $('.10').append('<canvas id="NPBTRatio"><canvas>');
                
                                                
                //CashRatio1
                ctxCashRatio1 = document.getElementById("CashRatio1").getContext("2d");
                CashRatio1 = new Chart(ctxCashRatio1, {
                                                data: {
                                                    labels: response['Cr1Kota'],
                                                    datasets: [
                                                        {
                                                            type: "bar",
                                                            label: "Kas Bank",
                                                            backgroundColor: "magenta",
                                                            borderColor: "magenta",
                                                            data: response['Cr1Kasbank']
                                                        },
                                                        {
                                                            type: "bar",
                                                            label: "Current Liabilities",
                                                            backgroundColor: "blue",
                                                            borderColor: "blue",
                                                            data: response['Cr1CurrentLiabilities'],
                                                        },
                                                        {
                                                            type: "bar",
                                                            label: "",
                                                            backgroundColor: "",
                                                            borderColor: "",
                                                            data: response['Cr1CashRasio']
                                                        },
                                                        
                                                        
                                                    ]
                                                },
                                                plugins: [ChartDataLabels],
                                                options: {
                                                    scales: {
                                                        y: {
                                                            ticks: {
                                                                callback: function(value, index, ticks) {
                                                                    return value+'%';
                                                                }
                                                            },
                                                        },
                                                    },
                                                    plugins: {
                                                        events: false,
                                                        tooltip: {
                                                            enabled: false
                                                        },
                                                        stacked100:{
                                                          enable: true,
                                                          replaceTooltipLabel:false
                                                        },
                                                        datalabels: {
                                                            formatter: (_value, context) => {
                                                                const { datasetIndex, dataIndex } = context;
                                                                const value = context.chart.data.originalData[datasetIndex][dataIndex]
                                                                // or you can use `context.chart.data.calculatedData[datasetIndex][dataIndex]` as relative percentage.
                                                                return value || "";
                                                            },
                                                            labels: {
                                                                title: {
                                                                    color: 'black'
                                                                }
                                                            }
                                                        }
                                                      },
                                                    responsive: true,
                                                    legend: {
                                                        position: "top"
                                                    },
                                                }
                                            });            
                //CashRatio2
                ctxCashRatio2 = document.getElementById("CashRatio2").getContext("2d");
                CashRatio2 = new Chart(ctxCashRatio2, {
                                                data: {
                                                    labels: response['Cr2Kota'],
                                                    datasets: [
                                                        {
                                                            type: "bar",
                                                            label: "Kas Bank Intercompany",
                                                           backgroundColor: "yellow",
                                                            borderColor: "yellow",
                                                            data: response['Cr2Kasbank']
                                                        },
                                                        {
                                                            type: "bar",
                                                            label: "Current Liabilities",
                                                            backgroundColor: "blue",
                                                            borderColor: "blue",
                                                            data: response['Cr2CurrentLiabilities']
                                                        },
                                                    //     {
                                                    //        type: "bar",
                                                    //        label: "Cash Rasio 2",
                                                    //        backgroundColor: "grey",
                                                    //        borderColor: "grey",
                                                    //        data: response['Cr2CashRasio']
                                                    //    },
                                                        
                                                    ]
                                                },
                                                plugins: [ChartDataLabels],
                                                options: {
                                                    scales: {
                                                        y: {
                                                            ticks: {
                                                                callback: function(value, index, ticks) {
                                                                    return value+'%';
                                                                }
                                                            },
                                                        },
                                                    },
                                                    plugins: {
                                                        events: false,
                                                        tooltip: {
                                                            enabled: false
                                                        },
                                                        stacked100:{
                                                          enable: true,
                                                          replaceTooltipLabel:false
                                                        },
                                                        datalabels: {
                                                            formatter: (_value, context) => {
                                                                const { datasetIndex, dataIndex } = context;
                                                                const value = context.chart.data.originalData[datasetIndex][dataIndex]
                                                                // or you can use `context.chart.data.calculatedData[datasetIndex][dataIndex]` as relative percentage.
                                                                return value || "";
                                                            },
                                                            labels: {
                                                                title: {
                                                                    color: 'black'
                                                                }
                                                            }
                                                        }
                                                      },
                                                    responsive: true,
                                                    legend: {
                                                        position: "top"
                                                    },
                                                }
                                            });   
                //QuickRatio
                ctxQuickRatio = document.getElementById("QuickRatio").getContext("2d");
                QuickRatio = new Chart(ctxQuickRatio, {
                                                data: {
                                                    labels: response['QrKota'],
                                                    datasets: [
                                                        {
                                                            type: "bar",
                                                            label: "Quick Rasio",
                                                            backgroundColor: "grey",
                                                            borderColor: "grey",
                                                            data: response['QrCashRasio']
                                                        },
                                                        {
                                                            type: "bar",
                                                            label: "Kas Bank Piutang",
                                                            backgroundColor: "magenta",
                                                            borderColor: "magenta",
                                                            data: response['QrKasbank']
                                                        },
                                                        // {
                                                        //     type: "bar",
                                                        //     label: "Current Liabilities",
                                                        //     backgroundColor: "blue",
                                                        //     borderColor: "blue",
                                                        //     data: response['QrCurrentLiabilities']
                                                        // },
                                                    ]
                                                },
                                                plugins: [ChartDataLabels],
                                                options: {
                                                    scales: {
                                                        y: {
                                                            ticks: {
                                                                callback: function(value, index, ticks) {
                                                                    return value+'%';
                                                                }
                                                            },
                                                        },
                                                    },
                                                    plugins: {
                                                        events: false,
                                                        tooltip: {
                                                            enabled: false
                                                        },
                                                        stacked100:{
                                                          enable: true,
                                                          replaceTooltipLabel:false
                                                        },
                                                        datalabels: {
                                                            formatter: (_value, context) => {
                                                                const { datasetIndex, dataIndex } = context;
                                                                const value = context.chart.data.originalData[datasetIndex][dataIndex]
                                                                // or you can use `context.chart.data.calculatedData[datasetIndex][dataIndex]` as relative percentage.
                                                                return value || "";
                                                            },
                                                            labels: {
                                                                title: {
                                                                    color: 'black'
                                                                }
                                                            }
                                                        }
                                                      },
                                                    responsive: true,
                                                    legend: {
                                                        position: "top"
                                                    },
                                                }
                                            });
                //WcRatio
                ctxWcRatio = document.getElementById("WcRatio").getContext("2d");
                WcRatio = new Chart(ctxWcRatio, {
                                                data: {
                                                    labels: response['WcKota'],
                                                    datasets: [
                                                        {
                                                            type: "bar",
                                                            label: "Wa Ratio",
                                                            backgroundColor: "green",
                                                            borderColor: "green",
                                                            borderWidth: 1,
                                                            data: response['WcKasbank']
                                                        },
                                                        {
                                                            type: "bar",
                                                            label: "WC Rp(x10^9)",
                                                            backgroundColor: "red",
                                                            borderColor: "red",
                                                            borderWidth: 1,
                                                            data: response['WcCashRasio']
                                                        }
                                                    ]
                                                },
                                                plugins: [ChartDataLabels],
                                                options: {
                                                    plugins: {
                                                        events: false,
                                                        tooltip: {
                                                            enabled: false
                                                        },
                                                        datalabels: {
                                                            labels: {
                                                                title: {
                                                                    color: 'black'
                                                                }
                                                            },
                                                            anchor:'end',
                                                            align:'end',
                                                        }
                                                      },
                                                    responsive: true,
                                                    legend: {
                                                        position: "top"
                                                    },
                                                    title: {
                                                        display: true,
                                                        text: "Chart.js Bar Chart"
                                                    },
                                                    
                                                }
                                            });   
                
                //GrossProfitRatio
                ctxGpRatio = document.getElementById("GrossProfitRatio").getContext("2d");
                GpRatio = new Chart(ctxGpRatio, {
                                                data: {
                                                    labels: response['GrossKota'],
                                                    datasets: [
                                                        {
                                                            type: "bar",
                                                            label: "Gross Profit Rasio",
                                                            backgroundColor: "#AED6F1",
                                                            borderColor: "#AED6F1",
                                                            borderWidth: 1,
                                                            data: response['GrossCashRasio']
                                                        },
                                                        {
                                                            type: "bar",
                                                            label: "Net Profit Ratio",
                                                            backgroundColor: "#A569BD",
                                                            borderColor: "#A569BD",
                                                            borderWidth: 1,
                                                            data: response['NetCashRasio']
                                                        },
                                                        {
                                                            type: "bar",
                                                            label: "NPBT Rasio",
                                                            backgroundColor: "#F39C12",
                                                            borderColor: "#F39C12",
                                                            borderWidth: 1,
                                                            data: response['NPBTCashRasio']
                                                        },
                                                        {
                                                            type: "bar",
                                                            label: "Oprt Profit Ratio",
                                                            backgroundColor: "#EB00B6",
                                                            borderColor: "#EB00B6",
                                                            borderWidth: 1,
                                                            data: response['OprtCashRasio']
                                                        }
                                                    ]
                                                },
                                                plugins: [ChartDataLabels],
                                                options: {
                                                    plugins: {
                                                        datalabels: {
                                                            labels: {
                                                                title: {
                                                                    color: 'black'
                                                                }
                                                            },
                                                            anchor:'end',
                                                            align:'end',
                                                        }
                                                      },
                                                    responsive: true,
                                                    legend: {
                                                        position: "top"
                                                    },
                                                    title: {
                                                        display: true,
                                                        text: "Chart.js Bar Chart"
                                                    },
                                                    
                                                }
                                            });   
                                
                //OprtProfitRatio
                /*
                ctxOprtProfitRatio = document.getElementById("OprtProfitRatio").getContext("2d");
                OprtProfitRatio = new Chart(ctxOprtProfitRatio, {
                                                data: {
                                                    labels: response['OprtKota'],
                                                    datasets: [
                                                        {
                                                            type: "bar",
                                                            label: "Oprt Cash Rasio",
                                                            backgroundColor: "#A569BD",
                                                            borderColor: "#A569BD",
                                                            borderWidth: 1,
                                                            data: response['OprtCashRasio']
                                                        }
                                                    ]
                                                },
                                                plugins: [ChartDataLabels],
                                                options: {
                                                    plugins: {
                                                        datalabels: {
                                                          display: false
                                                        }
                                                      },
                                                    responsive: true,
                                                    legend: {
                                                        position: "top"
                                                    },
                                                    title: {
                                                        display: true,
                                                        text: "Chart.js Bar Chart"
                                                    },
                                                    
                                                }
                                            });
                
                //NetProfitRatio
                ctxNetProfitRatio = document.getElementById("NetProfitRatio").getContext("2d");
                NetProfitRatio = new Chart(ctxNetProfitRatio, {
                                                data: {
                                                    labels: response['NetKota'],
                                                    datasets: [
                                                        {
                                                            type: "bar",
                                                            label: "Net Cash Rasio",
                                                            backgroundColor: "#F39C12",
                                                            borderColor: "#F39C12",
                                                            borderWidth: 1,
                                                            data: response['NetCashRasio']
                                                        }
                                                    ]
                                                },
                                                plugins: [ChartDataLabels],
                                                options: {
                                                    plugins: {
                                                        datalabels: {
                                                          display: false
                                                        }
                                                      },
                                                    responsive: true,
                                                    legend: {
                                                        position: "top"
                                                    },
                                                    title: {
                                                        display: true,
                                                        text: "Chart.js Bar Chart"
                                                    },
                                                    
                                                }
                                            });
                
                //NPBTRatio
                ctxNPBTRatio = document.getElementById("NPBTRatio").getContext("2d");
                NPBTRatio = new Chart(ctxNPBTRatio, {
                                                data: {
                                                    labels: response['NPBTKota'],
                                                    datasets: [
                                                        {
                                                            type: "bar",
                                                            label: "NPBT Kota",
                                                            backgroundColor: "#EB00B6",
                                                            borderColor: "#EB00B6",
                                                            borderWidth: 1,
                                                            data: response['NPBTCashRasio']
                                                        }
                                                    ]
                                                },
                                                plugins: [ChartDataLabels],
                                                options: {
                                                    plugins: {
                                                        datalabels: {
                                                          display: false
                                                        }
                                                      },
                                                    responsive: true,
                                                    legend: {
                                                        position: "top"
                                                    },
                                                    title: {
                                                        display: true,
                                                        text: "Chart.js Bar Chart"
                                                    },
                                                    
                                                }
                                            });
                */
                //ArDays
                ctxArDays = document.getElementById("ARDays").getContext("2d");
                ArDays = new Chart(ctxArDays, {
                                                data: {
                                                    labels: response['ArDayKota'],
                                                    datasets: [
                                                        {
                                                            type: "bar",
                                                            label: "Ar Day",
                                                            backgroundColor: "#F7DC6F",
                                                            borderColor: "#F7DC6F",
                                                            borderWidth: 1,
                                                            data: response['ArDayCashRasio']
                                                        }
                                                    ]
                                                },
                                                plugins: [ChartDataLabels],
                                                options: {
                                                    plugins: {
                                                        events: false,
                                                        tooltip: {
                                                            enabled: false
                                                        },
                                                        datalabels: {
                                                            labels: {
                                                                title: {
                                                                    color: 'black'
                                                                }
                                                            },
                                                            anchor:'end',
                                                            align:'end',
                                                        }
                                                      },
                                                    responsive: true,
                                                    legend: {
                                                        position: "top"
                                                    },
                                                    title: {
                                                        display: true,
                                                        text: "Chart.js Bar Chart"
                                                    },
                                                    
                                                }
                                            });
                
                //InventoryDays
                ctxInventoryDays = document.getElementById("InventoryDays").getContext("2d");
                InventoryDays = new Chart(ctxInventoryDays, {
                                                data: {
                                                    labels: response['InventoryDayKota'],
                                                    datasets: [
                                                        {
                                                            type: "bar",
                                                            label: "Inventory Day",
                                                            backgroundColor: "#58D68D",
                                                            borderColor: "#58D68D",
                                                            borderWidth: 1,
                                                            data: response['InventoryDayCashRasio']
                                                        }
                                                    ]
                                                },
                                                plugins: [ChartDataLabels],
                                                options: {
                                                    plugins: {
                                                        events: false,
                                                        tooltip: {
                                                            enabled: false
                                                        },
                                                        datalabels: {
                                                            labels: {
                                                                title: {
                                                                    color: 'black'
                                                                }
                                                            },
                                                            anchor:'end',
                                                            align:'end',
                                                        }
                                                      },
                                                    responsive: true,
                                                    legend: {
                                                        position: "top"
                                                    },
                                                    title: {
                                                        display: true,
                                                        text: "Chart.js Bar Chart"
                                                    },
                                                    
                                                }
                                            });
                
                $('#ajaxspinner1').hide();
                $('#ajaxspinner2').hide();
                $('#ajaxspinner3').hide();
                $('#ajaxspinner4').hide();
                $('#ajaxspinner5').hide();
                $('#ajaxspinner6').hide();
                $('#ajaxspinner7').hide();
                $('#ajaxspinner8').hide();
                $('#ajaxspinner9').hide();
                $('#ajaxspinner10').hide();
            }
        })
    }    

       $("#tampil").click(function() {
            reloadGrid1();
       }); 

JS
);

?>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@4.4.1/dist/chart.umd.min.js"></script>
    <!--    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.0/dist/chart.min.js"></script>-->
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-chart-treemap@2.3.0/dist/chartjs-chart-treemap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-stacked100@1.0.0"></script>

    <style>
        .tombol {
            background: #2C97DF;
            color: white;
            border-top: 0;
            border-left: 0;
            border-right: 0;
            border-bottom: 0;
            padding: 5px 10px;
            text-decoration: none;
            font-family: sans-serif;
            font-size: 11pt;
            border-radius: 4px;
        }

        .outer {
            overflow-y: auto;
            height: 100px;
        }

        .outer table {
            width: 100%;
            table-layout: fixed;
            border: 1px solid black;
            border-spacing: 1px;
        }

        .outer table th {
            text-align: left;
            top: 0;
            position: sticky;
            background-color: white;
        }
    </style>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
            <?= Html::beginForm(); ?>
            <div class="box-body">
                <div class="col-md-24">
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-sm-24">
                                <label class="control-label col-sm-4">Tanggal</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <div class="col-md-12">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'tgl1',
                                        'id' => 'tgl1',
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => Yii::$app->formatter->asDate('now', '01/MM/yyyy'),
//                                                'value' => '01/02/2021',
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                            'todayHighlight' => true
                                        ],
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                            <div class="col-md-12">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'tgl2',
                                        'id' => 'tgl2',
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => date('t/m/Y', strtotime("today")),
//                                                'value' => '27/02/2021',
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                        ],
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                        </div>
                    </div>
                    <label class="control-label col-sm-1">Bulan</label>
                    <div class="col-md-3">
                        <? try {
                            echo DatePicker::widget([
                                'name' => 'TglKu',
                                'id' => 'TglKu',
                                'attribute2' => 'to_date',
                                'type' => DatePicker::TYPE_INPUT,
                                'value' => Yii::$app->formatter->asDate('now', 'MMMM  yyyy'),
                                'pluginOptions' => [
                                    'startView' => 'year',
                                    'minViewMode' => 'months',
                                    'format' => 'MM  yyyy'
                                ],
                                'pluginEvents' => [
                                    "change" => new JsExpression("
                                                function(e) {
                                                 let d = new Date(Date.parse('01 ' + $(this).val()));											 
                                                 var tgl1 = $('#tgl1');
                                                 var tgl2 = $('#tgl2');
                                                  tgl1.kvDatepicker('update', d);
                                                  tgl2.kvDatepicker('update',new Date(d.getFullYear(), d.getMonth()+1, 0));     
                                                }                                                                                            
                                                "),
                                ]
                            ]);
                        } catch (\yii\base\InvalidConfigException $e) {
                        } ?>
                    </div>
                    <div class="col-sm-2">
                        <? try {
                            echo CheckboxX::widget([
                                'name' => 'eom',
                                'value' => true,
                                'options' => ['id' => 'eom'],
                                'pluginOptions' => ['threeState' => false, 'size' => 'lg'],
                                'pluginEvents' => [
                                    "change" => new JsExpression("function(e) {
                                                 var today = new Date();
                                                 var tgl2 = $('#tgl2');
                                                 let tglku = tgl2.kvDatepicker('getDate');
                                                 var lastDayOfMonth = new Date(tglku.getFullYear(), tglku.getMonth()+1, 0);
                                                if($(this).val() == 1){
                                                    tgl2.kvDatepicker('update', lastDayOfMonth)
                                                 }else{
                                                    tgl2.kvDatepicker('update', new Date(today.getFullYear(), today.getMonth(), today.getDate()));
                                                 }
                                                }")
                                ]
                            ]);
                            echo '<label class="cbx-label" for="eom">EOM</label>';
                        } catch (\yii\base\InvalidConfigException $e) {
                        } ?>
                    </div>
                    <label class="control-label col-sm-1">Level</label>
                    <div class="col-sm-3">
                        <?= Html::dropDownList('level', null, [
                            'Dealer' => 'Dealer',
                            'Korwil' => 'Korwil',
                            'Nasional' => 'Nasional',
                            'Anper' => 'Anper',
                            'BHC' => 'BHC',
                        ], ['id' => 'level', 'text' => 'Pilih tipe laporan', 'class' => 'form-control']) ?>

                    </div>
                    <div class="col-sm-5" style="height: 50px" id="tampil">
                        <label class="control-label tombol"><i class="glyphicon glyphicon-new-window">
                                Tampil</i></label>
                    </div>

                </div>
                <div class="col-md-24"><br><br></div>

                <div class="row">
                    <!--Cash Ratio 1-->
                    <div class="col-md-12">
                        <div class="box box-primary direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Cash Ratio 1</h3>
                            </div>
                            <i id="ajaxspinner1" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-2"></div>
                                <div class="col-md-22">
                                    <div class="1">
                                        <canvas id="CashRatio1"></canvas>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                    <!--Cash Ratio 2-->
                    <div class="col-md-12">
                        <div class="box box-primary direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Cash Ratio 2</h3>
                            </div>
                            <i id="ajaxspinner2" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-2"></div>
                                <div class="col-md-22">
                                    <div class="2">
                                        <canvas id="CashRatio2"></canvas>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                </div>
                <div class="row">
                    <!--Quick Ratio-->
                    <div class="col-md-12">
                        <div class="box box-primary direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Quick Ratio</h3>
                            </div>
                            <i id="ajaxspinner3" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-2"></div>
                                <div class="col-md-22">
                                    <div class="3">
                                        <canvas id="QuickRatio"></canvas>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                    <!--WC Ratio-->
                    <div class="col-md-12">
                        <div class="box box-primary direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">WC Ratio</h3>
                            </div>
                            <i id="ajaxspinner4" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-2"></div>
                                <div class="col-md-22">
                                    <div class="4">
                                        <canvas id="WCRatio"></canvas>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                </div>
                <div class="row">
                    <!--AR Days-->
                    <div class="col-md-12">
                        <div class="box box-primary direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">AR Days</h3>
                            </div>
                            <i id="ajaxspinner5" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-2"></div>
                                <div class="col-md-22">
                                    <div class="5">
                                        <canvas id="ARDays"></canvas>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                    <!--Inventory Days-->
                    <div class="col-md-12">
                        <div class="box box-primary direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Inventory Days</h3>
                            </div>
                            <i id="ajaxspinner6" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-2"></div>
                                <div class="col-md-22">
                                    <div class="6">
                                        <canvas id="InventoryDays"></canvas>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                </div>
                <div class="row">
                    <!--Gross Profit Ratio-->
                    <div class="col-md-24">
                        <div class="box box-primary direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Gross Profit Ratio</h3>
                            </div>
                            <i id="ajaxspinner7" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-2"></div>
                                <div class="col-md-22">
                                    <div class="7">
                                        <canvas id="GrossProfitRatio"></canvas>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                    <!--Oprt Profit Ratio-->
                    <?php /*
                    <div class="col-md-12">
                        <div class="box box-primary direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Oprt Profit Ratio</h3>
                            </div>
                            <i id="ajaxspinner8" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-2"></div>
                                <div class="col-md-22">
                                    <div class="8">
                                        <canvas id="OprtProfitRatio"></canvas>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                    */ ?>
                </div>
                <?php /*
                <div class="row">
                    <!--Net Profit Ratio-->
                    <div class="col-md-12">
                        <div class="box box-primary direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Net Profit Ratio</h3>
                            </div>
                            <i id="ajaxspinner9" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-2"></div>
                                <div class="col-md-22">
                                    <div class="9">
                                        <canvas id="NetProfitRatio"></canvas>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                    <!--NPBT Ratio-->
                    <div class="col-md-12">
                        <div class="box box-primary direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">NPBT Ratio</h3>
                            </div>
                            <i id="ajaxspinner10" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-2"></div>
                                <div class="col-md-22">
                                    <div class="10">
                                        <canvas id="NPBTRatio"></canvas>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                </div> */ ?>

            </div>
            <div class="box-footer"></div>
            <?= Html::endForm(); ?>
        </div>
    </div>
<?php
