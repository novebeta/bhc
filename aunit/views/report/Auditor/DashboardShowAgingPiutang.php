<?php
use yii\helpers\Url;

\aunit\assets\AppAsset::register($this);
\aunit\assets\JqwidgetsAsset::register($this);
$this->title = 'AGING PIUTANG';
$formatter = \Yii::$app->formatter;
$this->registerCss(".jqx-widget-content { font-size: 12px; }");

$rowdata = $data;

$this->registerJs(<<< JS

 
JS
);

?>
    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
<style>
    .table td{
        border: lightgrey solid 1px !important;
    }
    .table th{
        border: lightgrey solid 1px !important;
    }
</style>
    <div class="laporan-query col-md-24" style="padding-left: 0;">
        <div class="box box-default">
            <button class="btn pull-right" id='btnExcel' type="button" style="background-color:limegreen;width: 40px;height: 30px;margin: 10px;position: absolute;top: -45px;right: 10px"><i class="fa fa-file-excel-o fa-lg"></i></button>
            <div class="box-body">
                <div class="col-md-24">
                    <div>
                            <div class="box-body">
                                <div style="position: relative;height: 300px;overflow: auto;display: block;">
                                    <table class="table table-bordered table-striped mb-0">
                                        <thead>
                                        <tr>
                                            <th scope="col" style="text-align: center">Leasing</th>
                                            <th scope="col" style="text-align: center">Amount</th>
                                            <th scope="col" style="text-align: center">Quantity</th>
                                            <th scope="col" style="text-align: center">R0102</th>
                                            <th scope="col" style="text-align: center">R0305</th>
                                            <th scope="col" style="text-align: center">R0607</th>
                                            <th scope="col" style="text-align: center">R0814</th>
                                            <th scope="col" style="text-align: center">R1521</th>
                                            <th scope="col" style="text-align: center">R2228</th>
                                            <th scope="col" style="text-align: center">R2999</th>
                                            <th scope="col" style="text-align: center">A0102</th>
                                            <th scope="col" style="text-align: center">A0305</th>
                                            <th scope="col" style="text-align: center">A0607</th>
                                            <th scope="col" style="text-align: center">A0814</th>
                                            <th scope="col" style="text-align: center">A1521</th>
                                            <th scope="col" style="text-align: center">A2228</th>
                                            <th scope="col" style="text-align: center">A2999</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($rowdata as $key){
                                            echo '<tr align="right"><td align="left">'.$key['Leasing'].'</td>';
                                            echo '<td>'.number_format(substr($key['Amount'],0,-3),0,'','.').'</td>';
                                            echo '<td>'.$key['Quantity'].'</td>';
                                            echo '<td>'.$key['R0102'].'</td>';
                                            echo '<td>'.$key['R0305'].'</td>';
                                            echo '<td>'.$key['R0607'].'</td>';
                                            echo '<td>'.$key['R0814'].'</td>';
                                            echo '<td>'.$key['R1521'].'</td>';
                                            echo '<td>'.$key['R2228'].'</td>';
                                            echo '<td>'.$key['R2999'].'</td>';
                                            echo '<td>'.number_format(substr($key['A0102'],0,-3),0,'','.').'</td>';
                                            echo '<td>'.number_format(substr($key['A0305'],0,-3),0,'','.').'</td>';
                                            echo '<td>'.number_format(substr($key['A0607'],0,-3),0,'','.').'</td>';
                                            echo '<td>'.number_format(substr($key['A0814'],0,-3),0,'','.').'</td>';
                                            echo '<td>'.number_format(substr($key['A1521'],0,-3),0,'','.').'</td>';
                                            echo '<td>'.number_format(substr($key['A2228'],0,-3),0,'','.').'</td>';
                                            echo '<td>'.number_format(substr($key['A2999'],0,-3),0,'','.').'</td></tr>';
                                        }
                                        ?>

                                        </tbody>

                                    </table>

                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
<?php
$this->registerJs(<<< JS

 $(document).ready(function () {
      $("#btnExcel").click(function(){
        TableToExcel.convert(document.getElementById("table1"), {
            name: "AgingPiutang.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
 });

JS
);


