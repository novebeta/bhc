<?php
use kartik\checkbox\CheckboxX;
use kartik\number\NumberControl;
use kartik\date\DatePicker;
use kartik\widgets\TimePicker;

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
$this->title                   = 'Laporan Harian';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;

$sqlLeasing = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, '' AS LeaseStatus, '' AS LeaseKode
	UNION
	SELECT LeaseKode AS value, LeaseNama As label , LeaseStatus , LeaseKode FROM tdLeasing 
	ORDER BY LeaseStatus, LeaseKode" )
	->queryAll();
$cmbLeasing = ArrayHelper::map( $sqlLeasing, 'value', 'label' );

$sqlLokasi = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, '' AS LokasiStatus, '' AS LokasiKode
	UNION
	SELECT LokasiKode AS value, LokasiNama As label , LokasiStatus , LokasiKode FROM tdlokasi 
	ORDER BY LokasiStatus, LokasiKode" )
	->queryAll();
$cmbLokasi = ArrayHelper::map( $sqlLokasi, 'value', 'label' );


$format = <<< SCRIPT
function formatDual(result) {
    return '<div class="row">' +
           '<div class="col-md-8">' + result.id + '</div>' +
           '<div class="col-md-16">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );

?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Rugi Laba Harian 1' => 'Rugi Laba Harian 1',
									'Biaya RLH' => 'Biaya RLH',
									'Pendapatan RLH' => 'Pendapatan RLH',
									'Penjualan 1' => 'Penjualan 1',
									'Penjualan 2' => 'Penjualan 2',
									'Penjualan 3' => 'Penjualan 3',
									'Kas Harian Saldo' => 'Kas Harian Saldo',
									'Kas Harian Operasional' => 'Kas Harian Operasional',
									'Pengeluaran Kas' => 'Pengeluaran Kas',
									'Rugi Laba Margin' => 'Rugi Laba Margin',
									'RLM Unit' => 'RLM Unit',
									'Rugi Laba Harian 2' => 'Rugi Laba Harian 2',
									'RLH ppn' => 'RLH ppn',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window" id="submit-id"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-footer">
                <div class="container-fluid">
                    <div class="col-md-8">
                        <div class="form-group">
							<label class="control-label col-sm-5">Tanggal</label>
                            <div class="col-md-9">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'type'          => DatePicker::TYPE_INPUT,
                                        'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
                                        'options'       => [
                                            'id'    => 'tgl-1',
                                        ],
										'pluginOptions' => [
											'autoclose'      => true,
											'format'         => 'dd/mm/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                            <div class="col-md-9">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
                                        'options'       => [
                                            'id'    => 'tgl-2',
                                        ],
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                        </div>
                    </div>
					<div class="col-md-11 lokasihide">
                        <div class="form-group">
							<label class="control-label col-sm-4">Lokasi</label>
                            <div class="col-md-20">
								<? echo Select2::widget( [
									'name'          => 'lokasi',
									'data'          => $cmbLokasi,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatDual' ),
										'templateSelection' => new JsExpression( 'formatDual' ),
										'matcher'           => new JsExpression( 'matchCustom' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
                            </div>
                        </div>
                    </div>
					<div class="col-md-11 leasinghide">
                        <div class="form-group">
							<label class="control-label col-sm-4">Leasing</label>
                            <div class="col-md-20">
								<? echo Select2::widget( [
									'name'          => 'leasing',
									'data'          => $cmbLeasing,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatDual' ),
										'templateSelection' => new JsExpression( 'formatDual' ),
										'matcher'           => new JsExpression( 'matchCustom' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
                            </div>
                        </div>
                    </div>
<div class="container">
    <div class="row">
        <div class='col-sm-3'>
            <div class="form-group">
			<? try {
				echo TimePicker::widget( [
					'name' => 'view_time',
					'pluginOptions' => [
						'showSeconds' => true,
						'showMeridian' => false,
						'minuteStep' => 1,
						'secondStep' => 5,
					],
				    'addonOptions' => [
					    'asButton' => true,
						'buttonOptions' => ['class' => 'btn btn-info']
    ]				] );
			} catch ( \yii\base\InvalidConfigException $e ) {
			} ?>
            </div>
        </div>
    </div>
</div>
                </div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
$this->registerJs( $this->render( 'LaporanHarian.js' ) );