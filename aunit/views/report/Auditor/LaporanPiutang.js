jQuery(function ($) {
    $("#tipe-id").change(function () {
        var tipe = this.value;
        if (tipe === "Saldo Piutang Leasing 1" || tipe === "Saldo Piutang Leasing 2" || tipe === "Saldo Piutang Non Leasing"  || tipe === "Umur Piutang - Curent" || tipe === "Umur Piutang - Overdue") {
            $(".tgl1Hide").hide();
        } else {
            $(".tgl1Hide").show();
        }
        if (tipe === "Saldo Piutang Leasing 1" || tipe === "Saldo Piutang Leasing 2" || tipe === "Saldo Piutang Non Leasing") {
            $(".lunasHide").show();
        } else {
            $(".lunasHide").hide();
        }
        if (tipe === "Saldo Piutang Leasing 1" || tipe === "Saldo Piutang Leasing 2") {
            $(".leasingHide").show();
        } else {
            $(".leasingHide").hide();
        }
        if (tipe === "Saldo Piutang Leasing 1" || tipe === "Saldo Piutang Leasing 2" || tipe === "Saldo Piutang Non Leasing" || tipe === "Belum Di Link") {
            $(".cbsensitifHide").show();
        } else {
            $(".cbsensitifHide").hide();
        }
        if (tipe === "Riwayat Piutang Leasing" || tipe === "Riwayat Piutang Non Leasing") {
            $(".nobuktiHide").show();
        } else {
            $(".nobuktiHide").hide();
        }

        if (tipe === "Umur Piutang - Curent" || tipe === "Umur Piutang - Overdue") {
            $(".lblRangeHide").show();
        } else {
            $(".lblRangeHide").hide();
        }
        if (tipe === "Umur Piutang - Overdue") {
            $(".numRangeHide").show();
        } else {
            $(".numRangeHide").hide();
        }
        if (tipe === "Umur Piutang - Curent") {
            $(".RangetglHide").show();
            $('#tgl2').trigger('change');
        } else {
            $(".RangetglHide").hide();
        }
    });
    $('#tipe-id').trigger('change');
    $('#r1b').change(function () {
        $('#r2a').utilNumberControl().val(parseInt($(this).val()) + 1);
    });
    $('#r2b').change(function () {
        $('#r3a').utilNumberControl().val(parseInt($(this).val()) + 1);
    });
});


