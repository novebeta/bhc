<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use kartik\checkbox\CheckboxX;
use yii\web\JsExpression;

\aunit\assets\AppAsset::register($this);
\aunit\assets\JqwidgetsAsset::register($this);
$this->title = 'SALES STRUCTURE (Unit)';
$formatter = \Yii::$app->formatter;
$this->registerCss(".jqx-widget-content { font-size: 12px; }");

$rowdata = $data;
$this->registerJs(<<< JS

 
JS
);

?>
    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
    <style>
        .table td{
            border: lightgrey solid 1px !important;
        }
        .table th{
            border: lightgrey solid 1px !important;
        }
    </style>
    <div class="laporan-query col-md-24" style="padding-left: 0;">
        <div class="box box-default">
            <button class="btn pull-right" id='btnExcel' type="button" style="background-color:limegreen;width: 40px;height: 30px;margin: 10px;position: absolute;top: -45px;right: 10px"><i class="fa fa-file-excel-o fa-lg"></i></button>
            <div class="box-body">
                <div class="col-md-24">
                    <div>
                        <div class="box-body">
                            <div style="position: relative;height: 400px;overflow: auto;">
                                <table class="table table-bordered table-striped mb-0" id="table1" style="">
                                    <thead>
                                    <tr>
                                        <th scope="col">TYPE</th>
                                        <th scope="col">M</th>
                                        <th scope="col">M-1</th>
                                        <th scope="col">GROWTH %</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($rowdata as $key) {
                                        echo '<tr><td>' . $key['label'] . '</td>';
                                        echo '<td style="text-align: right;">' . $key['MTDNow'] . '</td>';
                                        echo '<td style="text-align: right">' . $key['MTDBefore'] . '</td>';
                                        echo '<td style="text-align: right">' . $key['Growth'] . '</td>';
                                    }

                                    ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>

<!--                        <div class="box-body">-->
<!--                            <h3 class="box-title" style="text-align: center">KECAMATAN</h3>-->
<!---->
<!--                            <div style="position: relative;height: 200px;overflow: auto;">-->
<!--                                <table class="table table-bordered table-striped mb-0" id="table1">-->
<!--                                    <thead>-->
<!--                                    <tr>-->
<!--                                        <th scope="col">UNIT</th>-->
<!--                                        <th scope="col">JAN</th>-->
<!--                                        <th scope="col">FEB</th>-->
<!--                                        <th scope="col">MAR</th>-->
<!--                                        <th scope="col">APR</th>-->
<!--                                        <th scope="col">MEI</th>-->
<!--                                        <th scope="col">JUN</th>-->
<!--                                        <th scope="col">JUL</th>-->
<!--                                        <th scope="col">AGS</th>-->
<!--                                        <th scope="col">SEP</th>-->
<!--                                        <th scope="col">OKT</th>-->
<!--                                        <th scope="col">NOV</th>-->
<!--                                        <th scope="col">DES</th>-->
<!---->
<!--                                    </tr>-->
<!--                                    </thead>-->
<!--                                    <tbody>-->
<!--                                    --><?php
//                                    foreach ($rowdata['Unit'] as $key) {
//
//                                            echo '<tr><td>' . $key['CusKecamatan'] . '</td>';
//                                            echo '<td style="text-align: right">' . $key['Jan'] . '</td>';
//                                            echo '<td style="text-align: right">' . $key['Feb'] . '</td>';
//                                            echo '<td style="text-align: right">' . $key['Mar'] . '</td>';
//                                            echo '<td style="text-align: right">' . $key['Apr'] . '</td>';
//                                            echo '<td style="text-align: right">' . $key['Mei'] . '</td>';
//                                            echo '<td style="text-align: right">' . $key['Jun'] . '</td>';
//                                            echo '<td style="text-align: right">' . $key['Jul'] . '</td>';
//                                            echo '<td style="text-align: right">' . $key['Agu'] . '</td>';
//                                            echo '<td style="text-align: right">' . $key['Sep'] . '</td>';
//                                            echo '<td style="text-align: right">' . $key['Okt'] . '</td>';
//                                            echo '<td style="text-align: right">' . $key['Nov'] . '</td>';
//                                            echo '<td style="text-align: right">' . $key['Des'] . '</td>';
//
//                                    }
//
//                                    ?>
<!--                                    </tbody>-->
<!--                                </table>-->
<!---->
<!--                            </div>-->
<!--                            <br><br><br><br>-->
<!--                            <div style="position: relative;height: 200px;overflow: auto;">-->
<!---->
<!--                                <table class="table table-bordered table-striped mb-0">-->
<!--                                    <thead>-->
<!--                                    <tr>-->
<!--                                        <th scope="col">SHARE</th>-->
<!--                                        <th scope="col">JAN</th>-->
<!--                                        <th scope="col">FEB</th>-->
<!--                                        <th scope="col">MAR</th>-->
<!--                                        <th scope="col">APR</th>-->
<!--                                        <th scope="col">MEI</th>-->
<!--                                        <th scope="col">JUN</th>-->
<!--                                        <th scope="col">JUL</th>-->
<!--                                        <th scope="col">AGS</th>-->
<!--                                        <th scope="col">SEP</th>-->
<!--                                        <th scope="col">OKT</th>-->
<!--                                        <th scope="col">NOV</th>-->
<!--                                        <th scope="col">DES</th>-->
<!---->
<!--                                    </tr>-->
<!--                                    </thead>-->
<!--                                    <tbody>-->
<!--                                    --><?php
//                                    foreach ($rowdata['Share'] as $key) {
//
//                                        echo '<tr><td>' . $key['CusKecamatan'] . '</td>';
//                                        echo '<td style="text-align: right">' . $key['Jan'] . '</td>';
//                                        echo '<td style="text-align: right">' . $key['Feb'] . '</td>';
//                                        echo '<td style="text-align: right">' . $key['Mar'] . '</td>';
//                                        echo '<td style="text-align: right">' . $key['Apr'] . '</td>';
//                                        echo '<td style="text-align: right">' . $key['Mei'] . '</td>';
//                                        echo '<td style="text-align: right">' . $key['Jun'] . '</td>';
//                                        echo '<td style="text-align: right">' . $key['Jul'] . '</td>';
//                                        echo '<td style="text-align: right">' . $key['Agu'] . '</td>';
//                                        echo '<td style="text-align: right">' . $key['Sep'] . '</td>';
//                                        echo '<td style="text-align: right">' . $key['Okt'] . '</td>';
//                                        echo '<td style="text-align: right">' . $key['Nov'] . '</td>';
//                                        echo '<td style="text-align: right">' . $key['Des'] . '</td>';
//
//                                    }
//
//                                    ?>
<!--                                    </tbody>-->
<!--                                </table>-->
<!---->
<!--                            </div>-->
<!---->
<!---->
<!---->
<!--                        </div>-->
                        <div class="box-footer"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php
$this->registerJs(<<< JS

 $(document).ready(function () {
      $("#btnExcel").click(function(){
        TableToExcel.convert(document.getElementById("table1"), {
            name: "PenjualanKecamatan.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
  });

JS
);

