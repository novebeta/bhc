<?php
use kartik\date\DatePicker;
use yii\helpers\Html;
use kartik\checkbox\CheckboxX;
use yii\web\JsExpression;
\aunit\assets\AppAsset::register( $this );
\aunit\assets\JqwidgetsAsset::register( $this );
$this->title                     = 'Laporan Dashboard';
$this->params[ 'breadcrumbs' ][] = $this->title;
$formatter                       = \Yii::$app->formatter;
$this->registerCss( ".jqx-widget-content { font-size: 12px; }" );
?>
    <div class="laporan-query col-md-24" style="padding-left: 0;">
        <div class="box box-default">
            <?= Html::beginForm(); ?>

            <div class="box-footer">
                <div class="container-fluid">
                    <div class="col-md-24">
                        <div class="col-sm-5">
                            <?= Html::dropDownList( 'tipe', null, [
                                "C Sadistock" => "C Sadistock",
                                "C Kecamatan" => "C Kecamatan",
                                "C Pos vs Credit" => "C Pos vs Credit",
                                "C Pos vs Kecamatan" => "C Pos vs Kecamatan",
                                "C Pos vs Segment" => "C Pos vs Segment",
                                "C Pos vs Profit" => "C Pos vs Profit",
                                "C Fincoy vs Segment" => "C Fincoy vs Segment",
                                "C Fincoy vs Kecamatan" => "C Fincoy vs Kecamatan",
                                "C Fincoy vs DP" => "C Fincoy vs DP",
                                "C Fincoy vs Profit" => "C Fincoy vs Profit",
                                "C Potongan Per Fincoy" => "C Potongan Per Fincoy",
                                "C Fincoy Per Tipe" => "C Fincoy Per Tipe",
                                "C Potongan Per Sales" => "C Potongan Per Sales",
                                "C Profit Per Sales" => "C Profit Per Sales",
                                "C Potongan Per Tipe" => "C Potongan Per Tipe",
                                "C Profit Per Tipe" => "C Profit Per Tipe",
                                "D Per Salesman" => "D Per Salesman",
                                "D Per Kecamatan" => "D Per Kecamatan",
                                "D Per Pos" => "D Per Pos",
                                "D Sales Per Tipe" => "D Sales Per Tipe",
                                "YTD Per Sales" => "YTD Per Sales",
                                "YTD Per Pos" => "YTD Per Pos",
                                "YTD Per Fincoy" => "YTD Per Fincoy",
                                "YTD Per Kecamatan" => "YTD Per Kecamatan",
                                "YTD Sales Per Tipe" => "YTD Sales Per Tipe"

                            ], [ 'text' => '', 'class' => 'form-control', 'id' => 'tipe' ] ) ?>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'tgl1',
                                        'id' => 'tgl1',
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => Yii::$app->formatter->asDate('now', '01/MM/yyyy'),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                            'todayHighlight' => true
                                        ],
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                            <div class="col-md-3">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'tgl2',
                                        'id' => 'tgl2',
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => date('t/m/Y', strtotime("today")),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                        ],

                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                        </div>
                        <label class="control-label col-sm-1">Bulan</label>
                        <div class="col-md-3">
                            <? try {
                                echo DatePicker::widget([
                                    'name' => 'TglKu',
                                    'id' => 'TglKu',
                                    'attribute2' => 'to_date',
                                    'type' => DatePicker::TYPE_INPUT,
                                    'value' => Yii::$app->formatter->asDate('now', 'MMMM  yyyy'),
                                    'pluginOptions' => [
                                        'startView' => 'year',
                                        'minViewMode' => 'months',
                                        'format' => 'MM  yyyy'
                                    ],
                                    'pluginEvents' => [
                                        "change" => new JsExpression("
                                                function(e) {
                                                 let d = new Date(Date.parse('01 ' + $(this).val()));											 
                                                 var tgl1 = $('#tgl1');
                                                 var tgl2 = $('#tgl2');                                               
                                                  tgl1.kvDatepicker('update', d);
                                                  tgl2.kvDatepicker('update',new Date(d.getFullYear(), d.getMonth()+1, 0)); 
                                                }                                                                                            
                                                "),
                                    ]
                                ]);
                            } catch (\yii\base\InvalidConfigException $e) {
                            } ?>
                        </div>
                        <div class="col-sm-1">
                            <? try {
                                echo CheckboxX::widget([
                                    'name'=>'cek',
                                    'id'=>'cek',
                                    'value'=>1,
                                    'pluginOptions'=>['threeState'=>false, 'size'=>'lg']
                                ]);
                            } catch ( \yii\base\InvalidConfigException $e ) {
                            } ?>
                        </div>
                        <div class="form-group col-sm-6">
                            <input type="file" id="file-input" class="hidden"/>
                            <button class="btn btn-primary" id='btnTampil' type="button" style="width: 80px;height: 30px"><i class="fa fa-th fa-lg"></i>&nbsp Tampil</button>
                            <button class="btn btn-success" id='btnExcel' type="button" style="width: 80px;height: 30px"><i class="fa fa-file-excel-o fa-lg"></i>&nbsp Excel</button>
                        </div>
                    </div>

                    <div class="form-group col-sm-24">
                        <div class="box-body" id='jqxWidget'>
                            <div id="jqxgrid">
                            </div>
                        </div>
<!--                        <textarea class="form-control" id="sqlraw" rows="8">-->
<!---->
<!--                        </textarea>-->
                    </div>


                </div>
            </div>
            <?= Html::endForm(); ?>
        </div>
    </div>
<?php
$urlData = \yii\helpers\Url::toRoute( [ 'report/dashboard-query'] );
$this->registerJs( <<< JS
jQuery(function ($) {                
            
     var json = '[{ "columns": [] }, {"rows" : []}]';            
            var obj = $.parseJSON(json);
            var columns = obj[0].columns;
            var datafields = new Array();
            for (var i = 0; i < columns.length; i++) {
                datafields.push({ name: columns[i].datafield });
            }
            var rows = obj[1].rows;
            // prepare the data
            var source =
            {
                datatype: "json",
                datafields: datafields,
                id: 'id',
                localdata: rows
            };
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#jqxgrid").jqxGrid(            {
                width: '100%',
                rowsheight: 21,
                height: 475,
                sortable: true,
                selectionmode: 'multiplerowsextended',
                source: dataAdapter,
                columnsresize: true,
                columns: columns
            });
            function readSingleFile(e) {
              //  console.log(e);
              var file = e.target.files[0];
              if (!file) {
                return;
              }
              var reader = new FileReader();
              reader.onload = function(e) {
                var contents = e.target.result;
                $('#sqlraw').val(contents);
              };
              reader.readAsText(file);
            }
            document.getElementById('file-input').addEventListener('change', readSingleFile, false);
            
            $('#btnTampil').click(function() {
                var eom = $('#cek').val();
                var tipe = $('#tipe').val();
                var tgl1 = $('#tgl_start').val().split("/").reverse().join("-");
                var tgl2 = $('#tgl_end').val().split("/").reverse().join("-");
                
                $.ajax({
                        type: 'POST',
                        url: '$urlData',
                        data: {tipe:tipe,tgl1:tgl1, tgl2:tgl2, eom:eom}
                    }).then(function (json) {
                        var obj = json;
                        var columns = obj.columns;
                        var datafields = new Array();
                        columns.unshift({
                              text: '#', sortable: false, filterable: false, editable: false,
                              groupable: false, draggable: false, resizable: false,
                              datafields: '', columntype: 'number', width: 50,
                              cellsrenderer: function (row, column, value) {
                                  return "<div style='margin:4px;'>" + (value + 1) + "</div>";
                              }
                          });
                        datafields.push({name:'rowindex'});
                        for (var i = 0; i < columns.length; i++) {
                            datafields.push({ name: columns[i].datafield});
                        }
                        var rows = obj.rows;
                        // prepare the data
                        var source =
                        {
                            datatype: "json",
                            datafields: datafields,
                            id: 'id',
                            localdata: rows
                        };
                        var dataAdapter = new $.jqx.dataAdapter(source);
                        $("#jqxgrid").jqxGrid( {
                            width: '100%',
                            rowsheight: 21,
                            height: 330,
                            source: dataAdapter,
                            columnsresize: true,
                            columns: columns
                        });
                         $("#jqxgrid").jqxGrid('autoresizecolumns');
                    });
            });
             $("#btnExcel").click(function () {
                $("#jqxgrid").jqxGrid('exportdata', 'xlsx', 'LaporanQuery');           
            });
             
             
});
JS
);
