<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use kartik\checkbox\CheckboxX;
use yii\web\JsExpression;

\aunit\assets\AppAsset::register($this);
\aunit\assets\JqwidgetsAsset::register($this);
$this->title = 'PENJUALAN KECAMATAN';
$formatter = \Yii::$app->formatter;
$this->registerCss(".jqx-widget-content { font-size: 12px; }");

$rowdata = $data;
$this->registerJs(<<< JS

 
JS
);

?>
    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
    <style>
        .table td {
            border: lightgrey solid 1px !important;
        }

        .table th {
            border: lightgrey solid 1px !important;
        }
    </style>
    <div class="laporan-query col-md-24" style="padding-left: 0;">
        <div class="box box-default">
            <button class="btn pull-right" id='btnExcel' type="button"
                    style="background-color:limegreen;width: 40px;height: 30px;margin: 10px;position: absolute;top: -45px;right: 10px">
                <i class="fa fa-file-excel-o fa-lg"></i></button>
            <div class="col-md-24">
                <div class="box-body">
                    <div class="col-md-8">
                        <div style="position: relative;overflow: auto;">
                            <table class="table table-bordered table-striped mb-0" id="table1">
                                <thead>
                                <tr>
                                    <!--                                        <th scope="col">No</th>-->
                                    <th scope="col" style="width: 200px">Kecamatan</th>
                                    <th scope="col" style="text-align: center;width: 50px">M</th>
                                    <th scope="col" style="text-align: center;width: 50px">M-1</th>
                                    <th scope="col" style="text-align: center;">Growth %</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($rowdata['satu'] as $key) {
                                    echo '<tr><td>' . $key['CusKecamatan'] . '</td>';
                                    echo '<td style="text-align: right">' . $key['MN'] . '</td>';
                                    echo '<td style="text-align: right">' . $key['MB'] . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Growth'], 0, ",", ".") . '</td>';
                                }

                                ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <!--                        --><?php //if( isset($rowdata['kode'])): ?>
                    <?php foreach ($rowdata['kode'] as $key) {
                        $kode = $key;
                    }
                    if ($key == '1'): ?>
                    <div class="col-md-15">
                        <h3 class="box-title" style="text-align: center">KECAMATAN</h3>
                        <div style="position: relative;height: 200px;overflow: auto;">

                            <table class="table table-bordered table-striped mb-0" id="table1">
                                <thead>
                                <tr>
                                    <th scope="col">UNIT</th>
                                    <th scope="col">JAN</th>
                                    <th scope="col">FEB</th>
                                    <th scope="col">MAR</th>
                                    <th scope="col">APR</th>
                                    <th scope="col">MEI</th>
                                    <th scope="col">JUN</th>
                                    <th scope="col">JUL</th>
                                    <th scope="col">AGS</th>
                                    <th scope="col">SEP</th>
                                    <th scope="col">OKT</th>
                                    <th scope="col">NOV</th>
                                    <th scope="col">DES</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                foreach ($rowdata['dua'] as $key) {

                                    echo '<tr><td>' . $key['CusKecamatan'] . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Jan'], 2, ",", ".") . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Feb'], 2, ",", ".") . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Mar'], 2, ",", ".") . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Apr'], 2, ",", ".") . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Mei'], 2, ",", ".") . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Jun'], 2, ",", ".") . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Jul'], 2, ",", ".") . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Agu'], 2, ",", ".") . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Sep'], 2, ",", ".") . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Okt'], 2, ",", ".") . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Nov'], 2, ",", ".") . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Des'], 2, ",", ".") . '</td>';

                                }

                                ?>
                                </tbody>
                            </table>


                        </div>
                        <br><br>
                        <div style="position: relative;height: 200px;overflow: auto;">

                            <table class="table table-bordered table-striped mb-0">
                                <thead>
                                <tr>
                                    <th scope="col">SHARE</th>
                                    <th scope="col">JAN</th>
                                    <th scope="col">FEB</th>
                                    <th scope="col">MAR</th>
                                    <th scope="col">APR</th>
                                    <th scope="col">MEI</th>
                                    <th scope="col">JUN</th>
                                    <th scope="col">JUL</th>
                                    <th scope="col">AGS</th>
                                    <th scope="col">SEP</th>
                                    <th scope="col">OKT</th>
                                    <th scope="col">NOV</th>
                                    <th scope="col">DES</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($rowdata['tiga'] as $key) {

                                    echo '<tr><td>' . $key['CusKecamatan'] . '</td>';
                                    echo '<td style="text-align: right">' . $key['Jan'] . '</td>';
                                    echo '<td style="text-align: right">' . $key['Feb'] . '</td>';
                                    echo '<td style="text-align: right">' . $key['Mar'] . '</td>';
                                    echo '<td style="text-align: right">' . $key['Apr'] . '</td>';
                                    echo '<td style="text-align: right">' . $key['Mei'] . '</td>';
                                    echo '<td style="text-align: right">' . $key['Jun'] . '</td>';
                                    echo '<td style="text-align: right">' . $key['Jul'] . '</td>';
                                    echo '<td style="text-align: right">' . $key['Agu'] . '</td>';
                                    echo '<td style="text-align: right">' . $key['Sep'] . '</td>';
                                    echo '<td style="text-align: right">' . $key['Okt'] . '</td>';
                                    echo '<td style="text-align: right">' . $key['Nov'] . '</td>';
                                    echo '<td style="text-align: right">' . $key['Des'] . '</td>';

                                }

                                ?>
                                </tbody>
                            </table>
                            <?php endif; ?>
                        </div>
                    </div>


                </div>
                <div class="box-footer"></div>
            </div>
        </div>
    </div>
<?php
$this->registerJs(<<< JS

 $(document).ready(function () {
      $("#btnExcel").click(function(){
        TableToExcel.convert(document.getElementById("table1"), {
            name: "PenjualanKecamatan.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
  });

JS
);

