<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use kartik\checkbox\CheckboxX;
use yii\web\JsExpression;

\aunit\assets\AppAsset::register($this);
\aunit\assets\JqwidgetsAsset::register($this);
$this->title = 'PENJUALAN SEGMEN BULANAN';
$formatter = \Yii::$app->formatter;
$this->registerCss(".jqx-widget-content { font-size: 12px; }");

$rowdata = $data;
$this->registerJs(<<< JS

 
JS
);

?>
    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.0/dist/chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
    <style>
        .table td{
            border: lightgrey solid 1px !important;
        }
        .table th{
            border: lightgrey solid 1px !important;
        }
    </style>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<?= Html::beginForm(); ?>
    <div class="box box-default" style="padding-left: 0;">
        <button class="btn pull-right" id='btnExcel' type="button" style="background-color:limegreen;width: 40px;height: 30px;margin: 10px;position: absolute;top: -45px;right: 10px"><i class="fa fa-file-excel-o fa-lg"></i></button>
        <div class="box-body" style="padding-left: 0;">
            <h3 class="box-title" style="text-align: center">SEGMEN</h3>
            <div class="col-md-24">
                <div style="position: relative;height: 280px;overflow: auto;">
                    <table class="table table-bordered table-striped mb-0" id="table1">
                        <thead>
                        <tr>
                            <th scope="col">UNIT</th>
                            <th scope="col">JAN</th>
                            <th scope="col">FEB</th>
                            <th scope="col">MAR</th>
                            <th scope="col">APR</th>
                            <th scope="col">MEI</th>
                            <th scope="col">JUN</th>
                            <th scope="col">JUL</th>
                            <th scope="col">AGS</th>
                            <th scope="col">SEP</th>
                            <th scope="col">OKT</th>
                            <th scope="col">NOV</th>
                            <th scope="col">DES</th>
                            <th scope="col">TOTAL</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($rowdata['tabel'] as $key) {
                            echo '<tr><td>' . $key['MotorGroup'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Jan'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Feb'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Mar'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Apr'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Mei'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Jun'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Jul'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Agu'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Sep'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Okt'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Nov'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Des'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Total'] . '</td>';
                        }

                        ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    <div class="box box-default">
        <div class="box-body">
            <div class="row col-md-24" style="width: 1200px;height: 300px">
                <canvas id="myChart"></canvas>
                <?php
                //kategori
                foreach ($rowdata['grafik'] as $key) {
                    $kategori[] = $key['MotorKategori'];
                }

                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT High') {$athight_jan = $val['Jan'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT High') {$athight_feb = $val['Feb'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT High') {$athight_mar = $val['Mar'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT High') {$athight_apr = $val['Apr'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT High') {$athight_mei = $val['Mei'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT High') {$athight_jun = $val['Jun'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT High') {$athight_jul = $val['Jul'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT High') {$athight_agu = $val['Agu'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT High') {$athight_sep = $val['Sep'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT High') {$athight_okt = $val['Okt'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT High') {$athight_nov = $val['Nov'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT High') {$athight_des = $val['Des'];}}

                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Low') {$atlow_jan = $val['Jan'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Low') {$atlow_feb = $val['Feb'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Low') {$atlow_mar = $val['Mar'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Low') {$atlow_apr = $val['Apr'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Low') {$atlow_mei = $val['Mei'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Low') {$atlow_jun = $val['Jun'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Low') {$atlow_jul = $val['Jul'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Low') {$atlow_agu = $val['Agu'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Low') {$atlow_sep = $val['Sep'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Low') {$atlow_okt = $val['Okt'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Low') {$atlow_nov = $val['Nov'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Low') {$atlow_des = $val['Des'];}}

                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Mid') {$atmid_jan = $val['Jan'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Mid') {$atmid_feb = $val['Feb'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Mid') {$atmid_mar = $val['Mar'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Mid') {$atmid_apr = $val['Apr'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Mid') {$atmid_mei = $val['Mei'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Mid') {$atmid_jun = $val['Jun'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Mid') {$atmid_jul = $val['Jul'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Mid') {$atmid_agu = $val['Agu'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Mid') {$atmid_sep = $val['Sep'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Mid') {$atmid_okt = $val['Okt'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Mid') {$atmid_nov = $val['Nov'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'AT Mid') {$atmid_des = $val['Des'];}}

                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub High') {$cubhigh_jan = $val['Jan'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub High') {$cubhigh_feb = $val['Feb'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub High') {$cubhigh_mar = $val['Mar'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub High') {$cubhigh_apr = $val['Apr'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub High') {$cubhigh_mei = $val['Mei'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub High') {$cubhigh_jun = $val['Jun'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub High') {$cubhigh_jul = $val['Jul'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub High') {$cubhigh_agu = $val['Agu'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub High') {$cubhigh_sep = $val['Sep'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub High') {$cubhigh_okt = $val['Okt'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub High') {$cubhigh_nov = $val['Nov'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub High') {$cubhigh_des = $val['Des'];}}

                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Low') {$cublow_jan = $val['Jan'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Low') {$cublow_feb = $val['Feb'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Low') {$cublow_mar = $val['Mar'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Low') {$cublow_apr = $val['Apr'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Low') {$cublow_mei = $val['Mei'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Low') {$cublow_jun = $val['Jun'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Low') {$cublow_jul = $val['Jul'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Low') {$cublow_agu = $val['Agu'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Low') {$cublow_sep = $val['Sep'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Low') {$cublow_okt = $val['Okt'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Low') {$cublow_nov = $val['Nov'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Low') {$cublow_des = $val['Des'];}}

                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Mid') {$cubmid_jan = $val['Jan'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Mid') {$cubmid_feb = $val['Feb'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Mid') {$cubmid_mar = $val['Mar'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Mid') {$cubmid_apr = $val['Apr'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Mid') {$cubmid_mei = $val['Mei'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Mid') {$cubmid_jun = $val['Jun'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Mid') {$cubmid_jul = $val['Jul'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Mid') {$cubmid_agu = $val['Agu'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Mid') {$cubmid_sep = $val['Sep'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Mid') {$cubmid_okt = $val['Okt'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Mid') {$cubmid_nov = $val['Nov'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Cub Mid') {$cubmid_des = $val['Des'];}}

                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport High') {$sporthigh_jan = $val['Jan'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport High') {$sporthigh_feb = $val['Feb'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport High') {$sporthigh_mar = $val['Mar'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport High') {$sporthigh_apr = $val['Apr'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport High') {$sporthigh_mei = $val['Mei'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport High') {$sporthigh_jun = $val['Jun'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport High') {$sporthigh_jul = $val['Jul'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport High') {$sporthigh_agu = $val['Agu'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport High') {$sporthigh_sep = $val['Sep'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport High') {$sporthigh_okt = $val['Okt'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport High') {$sporthigh_nov = $val['Nov'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport High') {$sporthigh_des = $val['Des'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Low') {$sportlow_jan = $val['Jan'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Low') {$sportlow_feb = $val['Feb'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Low') {$sportlow_mar = $val['Mar'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Low') {$sportlow_apr = $val['Apr'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Low') {$sportlow_mei = $val['Mei'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Low') {$sportlow_jun = $val['Jun'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Low') {$sportlow_jul = $val['Jul'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Low') {$sportlow_agu = $val['Agu'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Low') {$sportlow_sep = $val['Sep'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Low') {$sportlow_okt = $val['Okt'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Low') {$sportlow_nov = $val['Nov'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Low') {$sportlow_des = $val['Des'];}}

                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Mid') {$sportmid_jan = $val['Jan'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Mid') {$sportmid_feb = $val['Feb'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Mid') {$sportmid_mar = $val['Mar'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Mid') {$sportmid_apr = $val['Apr'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Mid') {$sportmid_mei = $val['Mei'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Mid') {$sportmid_jun = $val['Jun'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Mid') {$sportmid_jul = $val['Jul'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Mid') {$sportmid_agu = $val['Agu'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Mid') {$sportmid_sep = $val['Sep'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Mid') {$sportmid_okt = $val['Okt'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Mid') {$sportmid_nov = $val['Nov'];}}
                foreach ($rowdata['grafik'] as $key => $val) {if ($val['MotorKategori'] == 'Sport Mid') {$sportmid_des = $val['Des'];}}






                ?>
                <script>
                    Chart.register(ChartDataLabels);

                    var var_kategori = <?php echo json_encode($kategori); ?>;
                    console.log(var_kategori);

                    var athight_jan = <?php echo json_encode($athight_jan); ?>;
                    var athight_feb = <?php echo json_encode($athight_feb); ?>;
                    var athight_mar = <?php echo json_encode($athight_mar); ?>;
                    var athight_apr = <?php echo json_encode($athight_apr); ?>;
                    var athight_mei = <?php echo json_encode($athight_mei); ?>;
                    var athight_jun = <?php echo json_encode($athight_jun); ?>;
                    var athight_jul = <?php echo json_encode($athight_jul); ?>;
                    var athight_agu = <?php echo json_encode($athight_agu); ?>;
                    var athight_sep = <?php echo json_encode($athight_sep); ?>;
                    var athight_okt = <?php echo json_encode($athight_okt); ?>;
                    var athight_nov = <?php echo json_encode($athight_nov); ?>;
                    var athight_des = <?php echo json_encode($athight_des); ?>;

                    var atlow_jan = <?php echo json_encode($atlow_jan); ?>;
                    var atlow_feb = <?php echo json_encode($atlow_feb); ?>;
                    var atlow_mar = <?php echo json_encode($atlow_mar); ?>;
                    var atlow_apr = <?php echo json_encode($atlow_apr); ?>;
                    var atlow_mei = <?php echo json_encode($atlow_mei); ?>;
                    var atlow_jun = <?php echo json_encode($atlow_jun); ?>;
                    var atlow_jul = <?php echo json_encode($atlow_jul); ?>;
                    var atlow_agu = <?php echo json_encode($atlow_agu); ?>;
                    var atlow_sep = <?php echo json_encode($atlow_sep); ?>;
                    var atlow_okt = <?php echo json_encode($atlow_okt); ?>;
                    var atlow_nov = <?php echo json_encode($atlow_nov); ?>;
                    var atlow_des = <?php echo json_encode($atlow_des); ?>;


                    var atmid_jan = <?php echo json_encode($atmid_jan); ?>;
                    var atmid_feb = <?php echo json_encode($atmid_feb); ?>;
                    var atmid_mar = <?php echo json_encode($atmid_mar); ?>;
                    var atmid_apr = <?php echo json_encode($atmid_apr); ?>;
                    var atmid_mei = <?php echo json_encode($atmid_mei); ?>;
                    var atmid_jun = <?php echo json_encode($atmid_jun); ?>;
                    var atmid_jul = <?php echo json_encode($atmid_jul); ?>;
                    var atmid_agu = <?php echo json_encode($atmid_agu); ?>;
                    var atmid_sep = <?php echo json_encode($atmid_sep); ?>;
                    var atmid_okt = <?php echo json_encode($atmid_okt); ?>;
                    var atmid_nov = <?php echo json_encode($atmid_nov); ?>;
                    var atmid_des = <?php echo json_encode($atmid_des); ?>;

                    var cubhigh_jan = <?php echo json_encode($cubhigh_jan); ?>;
                    var cubhigh_feb = <?php echo json_encode($cubhigh_feb); ?>;
                    var cubhigh_mar = <?php echo json_encode($cubhigh_mar); ?>;
                    var cubhigh_apr = <?php echo json_encode($cubhigh_apr); ?>;
                    var cubhigh_mei = <?php echo json_encode($cubhigh_mei); ?>;
                    var cubhigh_jun = <?php echo json_encode($cubhigh_jun); ?>;
                    var cubhigh_jul = <?php echo json_encode($cubhigh_jul); ?>;
                    var cubhigh_agu = <?php echo json_encode($cubhigh_agu); ?>;
                    var cubhigh_sep = <?php echo json_encode($cubhigh_sep); ?>;
                    var cubhigh_okt = <?php echo json_encode($cubhigh_okt); ?>;
                    var cubhigh_nov = <?php echo json_encode($cubhigh_nov); ?>;
                    var cubhigh_des = <?php echo json_encode($cubhigh_des); ?>;

                    var cublow_jan = <?php echo json_encode($cublow_jan); ?>;
                    var cublow_feb = <?php echo json_encode($cublow_feb); ?>;
                    var cublow_mar = <?php echo json_encode($cublow_mar); ?>;
                    var cublow_apr = <?php echo json_encode($cublow_apr); ?>;
                    var cublow_mei = <?php echo json_encode($cublow_mei); ?>;
                    var cublow_jun = <?php echo json_encode($cublow_jun); ?>;
                    var cublow_jul = <?php echo json_encode($cublow_jul); ?>;
                    var cublow_agu = <?php echo json_encode($cublow_agu); ?>;
                    var cublow_sep = <?php echo json_encode($cublow_sep); ?>;
                    var cublow_okt = <?php echo json_encode($cublow_okt); ?>;
                    var cublow_nov = <?php echo json_encode($cublow_nov); ?>;
                    var cublow_des = <?php echo json_encode($cublow_des); ?>;


                    var cubmid_jan = <?php echo json_encode($cubmid_jan); ?>;
                    var cubmid_feb = <?php echo json_encode($cubmid_feb); ?>;
                    var cubmid_mar = <?php echo json_encode($cubmid_mar); ?>;
                    var cubmid_apr = <?php echo json_encode($cubmid_apr); ?>;
                    var cubmid_mei = <?php echo json_encode($cubmid_mei); ?>;
                    var cubmid_jun = <?php echo json_encode($cubmid_jun); ?>;
                    var cubmid_jul = <?php echo json_encode($cubmid_jul); ?>;
                    var cubmid_agu = <?php echo json_encode($cubmid_agu); ?>;
                    var cubmid_sep = <?php echo json_encode($cubmid_sep); ?>;
                    var cubmid_okt = <?php echo json_encode($cubmid_okt); ?>;
                    var cubmid_nov = <?php echo json_encode($cubmid_nov); ?>;
                    var cubmid_des = <?php echo json_encode($cubmid_des); ?>;

                    var sporthigh_jan = <?php echo json_encode($sporthigh_jan); ?>;
                    var sporthigh_feb = <?php echo json_encode($sporthigh_feb); ?>;
                    var sporthigh_mar = <?php echo json_encode($sporthigh_mar); ?>;
                    var sporthigh_apr = <?php echo json_encode($sporthigh_apr); ?>;
                    var sporthigh_mei = <?php echo json_encode($sporthigh_mei); ?>;
                    var sporthigh_jun = <?php echo json_encode($sporthigh_jun); ?>;
                    var sporthigh_jul = <?php echo json_encode($sporthigh_jul); ?>;
                    var sporthigh_agu = <?php echo json_encode($sporthigh_agu); ?>;
                    var sporthigh_sep = <?php echo json_encode($sporthigh_sep); ?>;
                    var sporthigh_okt = <?php echo json_encode($sporthigh_okt); ?>;
                    var sporthigh_nov = <?php echo json_encode($sporthigh_nov); ?>;
                    var sporthigh_des = <?php echo json_encode($sporthigh_des); ?>;

                    var sportlow_jan = <?php echo json_encode($sportlow_jan); ?>;
                    var sportlow_feb = <?php echo json_encode($sportlow_feb); ?>;
                    var sportlow_mar = <?php echo json_encode($sportlow_mar); ?>;
                    var sportlow_apr = <?php echo json_encode($sportlow_apr); ?>;
                    var sportlow_mei = <?php echo json_encode($sportlow_mei); ?>;
                    var sportlow_jun = <?php echo json_encode($sportlow_jun); ?>;
                    var sportlow_jul = <?php echo json_encode($sportlow_jul); ?>;
                    var sportlow_agu = <?php echo json_encode($sportlow_agu); ?>;
                    var sportlow_sep = <?php echo json_encode($sportlow_sep); ?>;
                    var sportlow_okt = <?php echo json_encode($sportlow_okt); ?>;
                    var sportlow_nov = <?php echo json_encode($sportlow_nov); ?>;
                    var sportlow_des = <?php echo json_encode($sportlow_des); ?>;


                    var sportmid_jan = <?php echo json_encode($sportmid_jan); ?>;
                    var sportmid_feb = <?php echo json_encode($sportmid_feb); ?>;
                    var sportmid_mar = <?php echo json_encode($sportmid_mar); ?>;
                    var sportmid_apr = <?php echo json_encode($sportmid_apr); ?>;
                    var sportmid_mei = <?php echo json_encode($sportmid_mei); ?>;
                    var sportmid_jun = <?php echo json_encode($sportmid_jun); ?>;
                    var sportmid_jul = <?php echo json_encode($sportmid_jul); ?>;
                    var sportmid_agu = <?php echo json_encode($sportmid_agu); ?>;
                    var sportmid_sep = <?php echo json_encode($sportmid_sep); ?>;
                    var sportmid_okt = <?php echo json_encode($sportmid_okt); ?>;
                    var sportmid_nov = <?php echo json_encode($sportmid_nov); ?>;
                    var sportmid_des = <?php echo json_encode($sportmid_des); ?>;







                    var ctx = document.getElementById('myChart').getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ['JAN', 'FEB', 'MAR', 'APR', 'MEI', 'JUN', 'JUL', 'AGS', 'SEP', 'OKT', 'NOV', 'DES'],
                            datasets: [
                                {
                                    label: 'AT High',
                                    data: [athight_jan, athight_feb ,athight_mar ,athight_apr ,athight_mei ,athight_jun ,athight_jul ,athight_agu ,athight_sep ,athight_okt ,athight_nov ,athight_des],
                                    backgroundColor: [
                                        'red',
                                    ],
                                },
                                {
                                    label: 'AT Low',
                                    data: [atlow_jan, atlow_feb ,atlow_mar ,atlow_apr ,atlow_mei ,atlow_jun ,atlow_jul ,atlow_agu ,atlow_sep ,atlow_okt ,atlow_nov ,atlow_des],
                                    backgroundColor: [
                                        'blue',
                                    ],
                                },
                                {
                                    label: 'AT Mid',
                                    data: [atlow_jan, atlow_feb ,atlow_mar ,atlow_apr ,atlow_mei ,atlow_jun ,atlow_jul ,atlow_agu ,atlow_sep ,atlow_okt ,atlow_nov ,atlow_des],
                                    backgroundColor: [
                                        'green',
                                    ],
                                },
                                {
                                    label: 'Cub High',
                                    data: [cubhigh_jan, cubhigh_feb ,cubhigh_mar ,cubhigh_apr ,cubhigh_mei ,cubhigh_jun ,cubhigh_jul ,cubhigh_agu ,cubhigh_sep ,cubhigh_okt ,cubhigh_nov ,cubhigh_des],
                                    backgroundColor: [
                                        'yellow',
                                    ],
                                },
                                {
                                    label: 'Cub Low',
                                    data: [cublow_jan, cublow_feb ,cublow_mar ,cublow_apr ,cublow_mei ,cublow_jun ,cublow_jul ,cublow_agu ,cublow_sep ,cublow_okt ,cublow_nov ,cublow_des],
                                    backgroundColor: [
                                        'grey',
                                    ],
                                },
                                {
                                    label: 'Cub Mid',
                                    data: [cubmid_jan, cubmid_feb ,cubmid_mar ,cubmid_apr ,cubmid_mei ,cubmid_jun ,cubmid_jul ,cubmid_agu ,cubmid_sep ,cubmid_okt ,cubmid_nov ,cubmid_des],
                                    backgroundColor: [
                                        'black',
                                    ],
                                },
                                {
                                    label: 'Sport High',
                                    data: [sporthigh_jan, sporthigh_feb ,sporthigh_mar ,sporthigh_apr ,sporthigh_mei ,sporthigh_jun ,sporthigh_jul ,sporthigh_agu ,sporthigh_sep ,sporthigh_okt ,sporthigh_nov ,sporthigh_des],
                                    backgroundColor: [
                                        'purple',
                                    ],
                                },
                                {
                                    label: 'Sport Low',
                                    data: [sportlow_jan, sportlow_feb ,sportlow_mar ,sportlow_apr ,sportlow_mei ,sportlow_jun ,sportlow_jul ,sportlow_agu ,sportlow_sep ,sportlow_okt ,sportlow_nov ,sportlow_des],
                                    backgroundColor: [
                                        'Maroon',
                                    ],
                                },
                                {
                                    label: 'Sport Mid',
                                    data: [sportmid_jan, sportmid_feb ,sportmid_mar ,sportmid_apr ,sportmid_mei ,sportmid_jun ,sportmid_jul ,sportmid_agu ,sportmid_sep ,sportmid_okt ,sportmid_nov ,sportmid_des],
                                    backgroundColor: [
                                        'Aqua',
                                    ],
                                },
                            ]
                        },
                        options: {
                            responsive: true,
                            interaction: {
                                intersect: false,
                            },
                            maintainAspectRatio: false,
                            legend: {
                                position: "top"
                            },
                            title: {
                                display: true,
                                text: "Chart.js Bar Chart"
                            },
                            scales: {
                                    x: {
                                        stacked: true
                                    },
                                    y: {
                                        stacked: true
                                    }
                            }
                        }
                    });
                </script>
            </div>
        </div>
    </div>
<?= Html::endForm(); ?>
<?php
$this->registerJs(<<< JS

 $(document).ready(function () {
      $("#btnExcel").click(function(){
        TableToExcel.convert(document.getElementById("table1"), {
            name: "PenjualanSegmenBulanan.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
  });

JS
);

