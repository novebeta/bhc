<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use kartik\checkbox\CheckboxX;
use yii\web\JsExpression;

\aunit\assets\AppAsset::register($this);
\aunit\assets\JqwidgetsAsset::register($this);
$this->title = 'PROFIT';
$formatter = \Yii::$app->formatter;
$this->registerCss(".jqx-widget-content { font-size: 12px; }");

$rowdata = $data;
$this->registerJs(<<< JS

 
JS
);

?>
    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.0/dist/chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
    <style>
        .table td {
            border: lightgrey solid 1px !important;
        }

        .table th {
            border: lightgrey solid 1px !important;
        }
    </style>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<?= Html::beginForm(); ?>
    <div class="box box-default" style="padding-left: 0;">
        <button class="btn pull-right" id='btnExcel' type="button"
                style="background-color:limegreen;width: 40px;height: 30px;margin: 10px;position: absolute;top: -45px;right: 10px">
            <i class="fa fa-file-excel-o fa-lg"></i></button>
        <div class="box-body" style="padding-left: 0;">


            <div class="col-md-24">
                <div class="col-md-16">
                    <h3 class="box-title" style="text-align: center">PEOPLE</h3>
                    <div style="position: relative;height: 570px;overflow: auto;">
                        <table class="table table-bordered table-striped mb-0" id="table11">
                            <thead>
                            <tr>
                                <th scope="col" style="text-align: center;width: 170px">SALESMAN</th>
                                <th scope="col">JAN</th>
                                <th scope="col">FEB</th>
                                <th scope="col">MAR</th>
                                <th scope="col">APR</th>
                                <th scope="col">MEI</th>
                                <th scope="col">JUN</th>
                                <th scope="col">JUL</th>
                                <th scope="col">AGS</th>
                                <th scope="col">SEP</th>
                                <th scope="col">OKT</th>
                                <th scope="col">NOV</th>
                                <th scope="col">DES</th>
                                <th scope="col" style="width: 10px">AVERAGE</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            foreach ($rowdata['People'] as $key) {
                                if ($key['Tipe'] === 'SALESMAN') {
                                    echo '<tr><td>' . $key['Label'] . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Jan'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Feb'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Mar'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Apr'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Mei'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Jun'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Jul'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Agu'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Sep'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Okt'], 0, "", ".")  . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Nov'], 0, "", ".")  . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Des'], 0, "", ".")  . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Average'], 0, "", ".")  . '</td>';
                                }
                            }

                            ?>
                            </tbody>
                        </table>
                        <br>
                        <table class="table table-bordered table-striped mb-0" id="table12">
                            <thead>
                            <tr>
                                <th scope="col" style="text-align: center;width: 170px">COUNTERSALES</th>
                                <th scope="col">JAN</th>
                                <th scope="col">FEB</th>
                                <th scope="col">MAR</th>
                                <th scope="col">APR</th>
                                <th scope="col">MEI</th>
                                <th scope="col">JUN</th>
                                <th scope="col">JUL</th>
                                <th scope="col">AGS</th>
                                <th scope="col">SEP</th>
                                <th scope="col">OKT</th>
                                <th scope="col">NOV</th>
                                <th scope="col">DES</th>
                                <th scope="col" style="width: 10px">AVERAGE</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            foreach ($rowdata['People'] as $key) {
                                if ($key['Tipe'] === 'COUNTERSALES') {
                                    echo '<tr><td>' . $key['Label'] . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Jan'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Feb'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Mar'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Apr'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Mei'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Jun'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Jul'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Agu'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Sep'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Okt'], 0, "", ".")  . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Nov'], 0, "", ".")  . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Des'], 0, "", ".")  . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Average'], 0, "", ".")  . '</td>';
                                }
                            }

                            ?>
                            </tbody>
                        </table>
                        <br>
                        <table class="table table-bordered table-striped mb-0" id="table13">
                            <thead>
                            <tr>
                                <th scope="col" style="text-align: center;width: 170px">KONTRIBUSI</th>
                                <th scope="col">JAN</th>
                                <th scope="col">FEB</th>
                                <th scope="col">MAR</th>
                                <th scope="col">APR</th>
                                <th scope="col">MEI</th>
                                <th scope="col">JUN</th>
                                <th scope="col">JUL</th>
                                <th scope="col">AGS</th>
                                <th scope="col">SEP</th>
                                <th scope="col">OKT</th>
                                <th scope="col">NOV</th>
                                <th scope="col">DES</th>
                                <th scope="col" style="width: 10px">AVERAGE</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            foreach ($rowdata['People'] as $key) {
                                if ($key['Tipe'] === 'KONTRIBUSI') {
                                    echo '<tr><td>' . $key['Label'] . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Jan'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Feb'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Mar'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Apr'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Mei'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Jun'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Jul'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Agu'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Sep'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Okt'], 0, "", ".")  . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Nov'], 0, "", ".")  . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Des'], 0, "", ".")  . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Average'], 0, "", ".")  . '</td>';
                                }
                            }

                            ?>
                            </tbody>
                        </table>
                        <br>
                        <table class="table table-bordered table-striped mb-0" id="table14">
                            <thead>
                            <tr>
                                <th scope="col" style="text-align: center;width: 170px">KONTRIBUSI KREDIT</th>
                                <th scope="col">JAN</th>
                                <th scope="col">FEB</th>
                                <th scope="col">MAR</th>
                                <th scope="col">APR</th>
                                <th scope="col">MEI</th>
                                <th scope="col">JUN</th>
                                <th scope="col">JUL</th>
                                <th scope="col">AGS</th>
                                <th scope="col">SEP</th>
                                <th scope="col">OKT</th>
                                <th scope="col">NOV</th>
                                <th scope="col">DES</th>
                                <th scope="col" style="width: 10px">AVERAGE</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            foreach ($rowdata['People'] as $key) {
                                if ($key['Tipe'] === 'KONTRIBUSI KREDIT') {
                                    echo '<tr><td>' . $key['Label'] . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Jan'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Feb'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Mar'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Apr'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Mei'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Jun'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Jul'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Agu'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Sep'], 0, "", ".") . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Okt'], 0, "", ".")  . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Nov'], 0, "", ".")  . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Des'], 0, "", ".")  . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Average'], 0, "", ".")  . '</td>';
                                }
                            }

                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-8">
                    <h3 class="box-title" style="text-align: center;">AVERAGE/MONTH</h3>
                    <div style="position: relative;height: 570px;overflow: auto;">
                        <table class="table table-bordered table-striped mb-0" id="table21">
                            <thead>
                            <tr>
                                <th scope="col" style="text-align: center;width: 170px">SALESMAN</th>
                                <?php
                                foreach ($rowdata['Average'] as $key) {
                                    if ($key['Tipe'] === 'Judul') {
                                        echo '<td style="text-align: right">' .$key['Tahun1']. '</td>';
                                        echo '<td style="text-align: right">' .$key['Tahun2']. '</td>';
                                        echo '<td style="text-align: right">' .$key['Tahun3']. '</td>';
                                        echo '<td style="text-align: right">' .$key['Tahun4']. '</td>';
                                        echo '<td style="text-align: right">' .$key['Tahun5']. '</td>';
                                    }
                                }
                                ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            foreach ($rowdata['Average'] as $key) {
                                if ($key['Tipe'] === 'SALESMAN') {
                                    echo '<tr><td>' . $key['Label'] . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Tahun1'], 0, "", ".")   . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Tahun2'], 0, "", ".")    . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Tahun3'], 0, "", ".")    . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Tahun4'], 0, "", ".")    . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Tahun5'], 0, "", ".")    . '</td>';
                                }
                            }

                            ?>
                            </tbody>
                        </table>
                        <br>
                        <table class="table table-bordered table-striped mb-0" id="table22">
                            <thead>
                            <tr>
                                <th scope="col" style="text-align: center;width: 170px">COUNTERSALES</th>
                                <?php
                                foreach ($rowdata['Average'] as $key) {
                                    if ($key['Tipe'] === 'Judul') {
                                        echo '<td style="text-align: right">' .$key['Tahun1']. '</td>';
                                        echo '<td style="text-align: right">' .$key['Tahun2']. '</td>';
                                        echo '<td style="text-align: right">' .$key['Tahun3']. '</td>';
                                        echo '<td style="text-align: right">' .$key['Tahun4']. '</td>';
                                        echo '<td style="text-align: right">' .$key['Tahun5']. '</td>';
                                    }
                                }
                                ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            foreach ($rowdata['Average'] as $key) {
                                if ($key['Tipe'] === 'COUNTERSALES') {
                                    echo '<tr><td>' . $key['Label'] . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Tahun1'], 0, "", ".")   . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Tahun2'], 0, "", ".")    . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Tahun3'], 0, "", ".")    . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Tahun4'], 0, "", ".")    . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Tahun5'], 0, "", ".")    . '</td>';
                                }
                            }

                            ?>
                            </tbody>
                        </table>
                        <br>
                        <table class="table table-bordered table-striped mb-0" id="table23">
                            <thead>
                            <tr>
                                <th scope="col" style="text-align: center;width: 170px">KONTRIBUSI</th>
                                <?php
                                foreach ($rowdata['Average'] as $key) {
                                    if ($key['Tipe'] === 'Judul') {
                                        echo '<td style="text-align: right">' .$key['Tahun1']. '</td>';
                                        echo '<td style="text-align: right">' .$key['Tahun2']. '</td>';
                                        echo '<td style="text-align: right">' .$key['Tahun3']. '</td>';
                                        echo '<td style="text-align: right">' .$key['Tahun4']. '</td>';
                                        echo '<td style="text-align: right">' .$key['Tahun5']. '</td>';
                                    }
                                }
                                ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            foreach ($rowdata['Average'] as $key) {
                                if ($key['Tipe'] === 'KONTRIBUSI') {
                                    echo '<tr><td>' . $key['Label'] . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Tahun1'], 0, "", ".")   . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Tahun2'], 0, "", ".")    . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Tahun3'], 0, "", ".")    . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Tahun4'], 0, "", ".")    . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Tahun5'], 0, "", ".")    . '</td>';
                                }
                            }

                            ?>
                            </tbody>
                        </table>
                        <br>
                        <table class="table table-bordered table-striped mb-0" id="table24">
                            <thead>
                            <tr>
                                <th scope="col" style="text-align: center;width: 170px">KONTRIBUSI KREDIT</th>
                                <?php
                                foreach ($rowdata['Average'] as $key) {
                                    if ($key['Tipe'] === 'Judul') {
                                        echo '<td style="text-align: right">' .$key['Tahun1']. '</td>';
                                        echo '<td style="text-align: right">' .$key['Tahun2']. '</td>';
                                        echo '<td style="text-align: right">' .$key['Tahun3']. '</td>';
                                        echo '<td style="text-align: right">' .$key['Tahun4']. '</td>';
                                        echo '<td style="text-align: right">' .$key['Tahun5']. '</td>';
                                    }
                                }
                                ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            foreach ($rowdata['Average'] as $key) {
                                if ($key['Tipe'] === 'KONTRIBUSI KREDIT') {
                                    echo '<tr><td>' . $key['Label'] . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Tahun1'], 0, "", ".")   . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Tahun2'], 0, "", ".")    . '</td>';
                                    echo '<td style="text-align: right">' . number_format($key['Tahun3'], 0, "", ".")    . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Tahun4'], 0, "", ".")    . '</td>';
                                    echo '<td style="text-align: right">' .  number_format($key['Tahun5'], 0, "", ".")    . '</td>';
                                }
                            }

                            ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>


        </div>
    </div>

<?= Html::endForm(); ?>
<?php
$this->registerJs(<<< JS

 $(document).ready(function () {
      $("#btnExcel").click(function(){
        TableToExcel.convert(document.getElementById("table1"), {
            name: "Profit.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
  });

JS
);
