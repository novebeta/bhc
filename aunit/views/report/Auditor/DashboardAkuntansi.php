<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\checkbox\CheckboxX;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use aunit\components\Menu;
$this->title                   = 'Dashboard Akuntansi';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;


$sqlBank                           = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, '' AS NoParent, '' AS StatusAccount, '' AS NoAccount
	UNION
	SELECT NoAccount as value, NamaAccount as label, NoParent, StatusAccount, NoAccount FROM traccount 
	Where JenisAccount = 'Detail' AND (LEFT(NoAccount,2) = '24')
	ORDER BY StatusAccount, NoAccount" )
	->queryAll();
$cmbBank                           = ArrayHelper::map( $sqlBank, 'value', 'label' );

$sqlLeasing = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, '' AS LeaseStatus, '' AS LeaseKode
	UNION
	SELECT LeaseKode AS value, LeaseKode As label , LeaseStatus , LeaseKode FROM tdLeasing 
	WHERE LeaseStatus = 'A' 
	ORDER BY LeaseStatus, LeaseKode" )
	->queryAll();
$cmbLeasing = ArrayHelper::map( $sqlLeasing, 'value', 'label' );

$sqlKabupaten = Yii::$app->db
	->createCommand( "
	SELECT 'Semua' AS label, '%' AS value,  '' AS AreaStatus, '' AS Kabupaten
	UNION
	SELECT Kabupaten As label, Kabupaten AS value,  AreaStatus, Kabupaten FROM tdarea  
	where AreaStatus IN ('A','1')
	GROUP BY Kabupaten
	ORDER BY AreaStatus, Kabupaten " )
	->queryAll();
$cmbKabupaten = ArrayHelper::map( $sqlKabupaten, 'value', 'label' );
$akses = $_SESSION['_userProfile']['KodeAkses'];

$format = <<< SCRIPT
function formatBank(result) {
    return '<div class="row">' +
           '<div class="col-md-5">' + result.id + '</div>' +
           '<div class="col-md-19">' + result.text + '</div>' +
           '</div>';
}
function formatDual(result) {
    return '<div class="row">' +
           '<div class="col-md-3">' + result.id + '</div>' +
           '<div class="col-md-21">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );

?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Dashboard A1'  => 'Dashboard A1',
									'Dashboard A2'  => 'Dashboard A2',
									'NPBT'  => 'NPBT',
									'Budget'  => 'Budget',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button id="tampil" type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-footer">
                <div class="col-md-3">
                    <div class="form-group">
                        <div class="col-sm-24">
							<?= Html::dropDownList( 'tanggal', null, [
								"ttsk.SKTgl" => "Tgl SK",
								"ttdk.DKTgl" => "Tgl DK",
							], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <div class="col-md-12">
							<? try {
								echo DatePicker::widget( [
									'name'          => 'tgl1',
									'id'            => 'tgl1_id',
									'type'          => DatePicker::TYPE_INPUT,
									'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
									'pluginOptions' => [
										'autoclose'      => true,
										'format'         => 'dd/mm/yyyy',
										'todayHighlight' => true
									]
								] );
							} catch ( \yii\base\InvalidConfigException $e ) {
							} ?>
                        </div>
                        <div class="col-md-12">
							<? try {
								echo DatePicker::widget( [
									'name'          => 'tgl2',
									'id'            => 'tgl2_id',
									'type'          => DatePicker::TYPE_INPUT,
                                    'value'         => date('t/m/Y',strtotime("today")),
									'pluginOptions' => [
										'autoclose' => true,
										'format'    => 'dd/mm/yyyy',
									],
                                    'pluginEvents'  => [
                                        "change" => new JsExpression("function(e) {
											 var tgl1 = $('#tgl1_id');
											 var tgl_2 = $('#tgl2_id').val();
											 var date = '01/'+tgl_2.slice(3);
											 tgl1.kvDatepicker('update', date);
											}")
                                    ]
                                ] );
							} catch ( \yii\base\InvalidConfigException $e ) {
							} ?>
                        </div>
                    </div>
                </div>
				<div class="col-sm-6">
					<? try {
						echo CheckboxX::widget([
							'name'=>'eom',
                            'value'         => true,
							'options'=>['id'=>'eom'],
							'pluginOptions'=>['threeState'=>false, 'size'=>'lg'],
							'pluginEvents'  => [
											"change" => new JsExpression("function(e) {
											 var today = new Date();
											 var tgl2 = $('#tgl2_id');
											 let tglku = tgl2.kvDatepicker('getDate');
											 var lastDayOfMonth = new Date(tglku.getFullYear(), tglku.getMonth()+1, 0);
											if($(this).val() == 1){
											    tgl2.kvDatepicker('update', lastDayOfMonth)
											 }else{
											    tgl2.kvDatepicker('update', new Date(today.getFullYear(), today.getMonth(), today.getDate()));
											 }
											}")
										]
						]);
						echo '<label class="cbx-label" for="eom">EOM</label>';
					} catch ( \yii\base\InvalidConfigException $e ) {
					} ?>
				</div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
$this->registerJsVar( '__akses', $akses );
$this->registerJs( <<< JS

// $("#tipe-id").change(function () {
//     var tipe = this.value;
//     console.log(__akses);
//    
//         if (tipe === 'NPBT' ){
//         $("#tampil").prop("disabled", true);
//             if( __akses === 'Auditor' || __akses === 'Admin' || __akses === 'Korwil' || __akses === 'Branch Manager'){ 
//                 $("#tampil").prop("disabled", false);
//             }
//         }
//         else if (tipe === 'Dashboard A1' || tipe === 'Dashboard A2' ){
//             $("#tampil").prop("disabled", false);
//         }
// });
// $("#tipe-id").trigger('change');

JS
);
