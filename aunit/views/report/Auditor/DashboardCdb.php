<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\checkbox\CheckboxX;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\helpers\Url;

$this->title = 'Dashboard CDB';
$this->params['breadcrumbs'][] = $this->title;
$formatter = \Yii::$app->formatter;
$format = <<< SCRIPT

SCRIPT;
$urlQuery = Url::toRoute(['report/cdb']);
$urlQueryJenis = Url::toRoute(['report/cdb-jenis']);
$urlQuerySales = Url::toRoute(['report/cdb-sales']);
$urlQueryTenor = Url::toRoute(['report/cdb-tenor']);
$urlQueryUsia = Url::toRoute(['report/cdb-usia']);
$urlQueryPendidikan = Url::toRoute(['report/cdb-pendidikan']);
$urlQueryPayment = Url::toRoute(['report/cdb-payment']);
$urlQuerySes = Url::toRoute(['report/cdb-ses']);
$urlQueryKredit = Url::toRoute(['report/cdb-kredit']);
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
$urlQueryDDT = Url::toRoute(['report/marquery']);
$this->registerJs(<<< JS
    
    Chart.register(ChartDataLabels);

    function reloadGrid1(){
        $('#ajaxspinner1').show();
        var eom = $('#eom').val();
        var korwil = $('#korwil').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        $.ajax({
            type: 'POST',
            url: '$urlQueryJenis',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom, korwil:korwil},
            datatype: 'json',
            success:function(response) {
                    $('#cnvjenispekerjaan').remove(); 
                    $('.classjenispekerjaan').append('<canvas id="cnvjenispekerjaan"><canvas>');
                                   
    
                //JENIS PEKERJAAN
                ctx1 = document.getElementById("cnvjenispekerjaan").getContext("2d");
                jenispekerjaan = new Chart(ctx1, {
                                            type: "bar",
                                            data: {
                                                labels: response['labelpekerjaan'],
                                                datasets: [
                                                    {
                                                        label: "JENIS PEKERJAAN",
                                                        backgroundColor: "#FFA500",
                                                        data: response['isipekerjaan'],
                                                    }
                                                ]
                                            },
                                            plugins: [ChartDataLabels],
                                            options: {
                                                indexAxis: 'y',
                                                responsive: true,
                                                legend: {
                                                    position: "right"
                                                },
                                                title: {
                                                    display: true,
                                                    text: "Chart.js Bar Chart"
                                                },
                                                scales: {
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }]
                                                },
                                            }
                                        });                
                $('#ajaxspinner1').hide();
            }
            })
        
             
    }
    
    function reloadGrid2(){
        $('#ajaxspinner2').show();
        var eom = $('#eom').val();
        var korwil = $('#korwil').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        $.ajax({
            type: 'POST',
            url: '$urlQuerySales',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom, korwil:korwil},
            datatype: 'json',
            success:function(response) {
                    $('#cnvsbt').remove(); 
                    $('.classsbt').append('<canvas id="cnvsbt"><canvas>');
                //SALES BY TYPE
                ctx2 = document.getElementById("cnvsbt").getContext("2d");
                sbt = new Chart(ctx2, {
                                            type: "bar",
                                            data: {
                                                labels: response['labelsales'],
                                                datasets: [
                                                    {
                                                        label: "SALES BY TYPE",
                                                        backgroundColor: "#e9967a",
                                                        data: response['isisales'],
                                                    }
                                                ]
                                            },
                                            plugins: [ChartDataLabels],
                                            options: {
                                                indexAxis: 'y',
                                                responsive: true,
                                                legend: {
                                                    position: "right"
                                                },
                                                title: {
                                                    display: true,
                                                    text: "Chart.js Bar Chart"
                                                },
                                                scales: {
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }]
                                                },
                                            }
                                        });
                $('#ajaxspinner2').hide();
            }
            })            
    }
    
    function reloadGrid3(){
        $('#ajaxspinner3').show();
        var eom = $('#eom').val();
        var korwil = $('#korwil').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        $.ajax({
            type: 'POST',
            url: '$urlQueryTenor',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom, korwil:korwil},
            datatype: 'json',
            success:function(response) {                    
                    $('#cnvtenor').remove(); 
                    $('.classtenor').append('<canvas id="cnvtenor"><canvas>');
                                             
                //TENOR
                ctx3 = document.getElementById("cnvtenor").getContext("2d");
                tenor = new Chart(ctx3, {
                                            type: "bar",
                                            data: {
                                                labels: response['labeltenor'],
                                                datasets: [
                                                    {
                                                        label: "TENOR",
                                                        backgroundColor: "#f5deb3",
                                                        data: response['isitenor'],
                                                    }
                                                ]
                                            },
                                            plugins: [ChartDataLabels],
                                            options: {
                                                indexAxis: 'y',
                                                responsive: true,
                                                legend: {
                                                    position: "right"
                                                },
                                                title: {
                                                    display: true,
                                                    text: "Chart.js Bar Chart"
                                                },
                                                scales: {
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }]
                                                },
                                            }
                                        });
                $('#ajaxspinner3').hide();                                                 
            }
            })            
    }
        
    function reloadGrid4(){
        $('#ajaxspinner4').show();
        var eom = $('#eom').val();
        var korwil = $('#korwil').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        $.ajax({
            type: 'POST',
            url: '$urlQueryUsia',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom, korwil:korwil},
            datatype: 'json',
            success:function(response) {                    
                    $('#cnvusia').remove(); 
                    $('.classusia').append('<canvas id="cnvusia"><canvas>');
                    
                
                //RANGE USIA
                ctx4 = document.getElementById("cnvusia").getContext("2d");
                usia = new Chart(ctx4, {
                                            type: "bar",
                                            data: {
                                                labels: response['labelusia'],
                                                datasets: [
                                                    {
                                                        label: "RANGE USIA",
                                                        backgroundColor: "#add8e6",
                                                        data: response['isiusia'],
                                                    }
                                                ]
                                            },
                                            plugins: [ChartDataLabels],
                                            options: {
                                                responsive: true,
                                                legend: {
                                                    position: "top"
                                                },
                                                title: {
                                                    display: true,
                                                    text: "Chart.js Bar Chart"
                                                },
                                                scales: {
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }]
                                                },
                                            }
                                        });
                
                $('#ajaxspinner4').hide();                                                 
            }
            })            
    }
    
    function reloadGrid5(){
        $('#ajaxspinner5').show();
        var eom = $('#eom').val();
        var korwil = $('#korwil').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        $.ajax({
            type: 'POST',
            url: '$urlQueryPendidikan',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom, korwil:korwil},
            datatype: 'json',
            success:function(response) {
                    $('#cnvpendidikan').remove(); 
                    $('.classpendidikan').append('<canvas id="cnvpendidikan"><canvas>');
                    
                
                //PENDIDIKAN
                ctx5 = document.getElementById("cnvpendidikan").getContext("2d");
                pendidikan = new Chart(ctx5, {
                                            type: "bar",
                                            data: {
                                                labels: response['labelpendidikan'],
                                                datasets: [
                                                    {
                                                        label: "PENDIDIKAN",
                                                        backgroundColor: "#66FF00",
                                                        data: response['isipendidikan'],
                                                    }
                                                ]
                                            },
                                            plugins: [ChartDataLabels],
                                            options: {
                                                indexAxis: 'y',
                                                responsive: true,
                                                legend: {
                                                    position: "right"
                                                },
                                                title: {
                                                    display: true,
                                                    text: "Chart.js Bar Chart"
                                                },
                                                scales: {
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }]
                                                },
                                            }
                                        });
                $('#ajaxspinner5').hide();                                                  
            }
            })            
    }
    
    function reloadGrid6(){
        $('#ajaxspinner6').show();
        var eom = $('#eom').val();
        var korwil = $('#korwil').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        $.ajax({
            type: 'POST',
            url: '$urlQueryPayment',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom, korwil:korwil},
            datatype: 'json',
            success:function(response) {
                    $('#cnvpayment').remove(); 
                    $('.classpayment').append('<canvas id="cnvpayment"><canvas>');                   
                
                //DOWN PAYMENT
                ctx8 = document.getElementById("cnvpayment").getContext("2d");
                payment = new Chart(ctx8, {
                                            type: "bar",
                                            data: {
                                                labels: response['labelpayment'],
                                                datasets: [
                                                    {
                                                        label: "DOWN PAYMENT",
                                                        backgroundColor: "#FFFF50",
                                                        data: response['isipayment'],
                                                    }
                                                ]
                                            },
                                            plugins: [ChartDataLabels],
                                            options: {
                                                responsive: true,
                                                legend: {
                                                    position: "top"
                                                },
                                                title: {
                                                    display: true,
                                                    text: "Chart.js Bar Chart"
                                                },
                                                scales: {
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }]
                                                },
                                            }
                                        });                
                $('#ajaxspinner6').hide();                                               
            }
            })            
    }
    
    function reloadGrid7(){
        $('#ajaxspinner7').show();
        var eom = $('#eom').val();
        var korwil = $('#korwil').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        $.ajax({
            type: 'POST',
            url: '$urlQuerySes',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom, korwil:korwil},
            datatype: 'json',
            success:function(response) {                    
                    $('#cnvses').remove(); 
                    $('.classses').append('<canvas id="cnvses"><canvas>');
                //SES
                ctx6 = document.getElementById("cnvses").getContext("2d");
                ses = new Chart(ctx6, {
                                            type: "bar",
                                            data: {
                                                labels: response['labelpengeluaran'],
                                                datasets: [
                                                    {
                                                        label: "SES",
                                                        backgroundColor: "#FF6C4F",
                                                        data: response['isipengeluaran'],
                                                    }
                                                ]
                                            },
                                            plugins: [ChartDataLabels],
                                            options: {
                                                indexAxis: 'y',
                                                responsive: true,
                                                legend: {
                                                    position: "right"
                                                },
                                                title: {
                                                    display: true,
                                                    text: "Chart.js Bar Chart"
                                                },
                                                scales: {
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }]
                                                },
                                            }
                                        });
                $('#ajaxspinner7').hide();                                               
            }
            })            
    }
    
    function reloadGrid8(){
        $('#ajaxspinner8').show();
        var eom = $('#eom').val();
        var korwil = $('#korwil').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        $.ajax({
            type: 'POST',
            url: '$urlQueryKredit',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom, korwil:korwil},
            datatype: 'json',
            success:function(response) {
                    $('#cnvkredit').remove(); 
                    $('.classkredit').append('<canvas id="cnvkredit"><canvas>');
                //KREDIT
                ctx7 = document.getElementById("cnvkredit").getContext("2d");
                kredit = new Chart(ctx7, {
                                            type: "pie",
                                            data: {
                                                labels: response['labelkredit'],
                                                datasets: [
                                                    {
                                                        label: "CASH / KREDIT",
                                                        backgroundColor: ["#FFBF00","#FF69B4"],
                                                        data: response['isikredit'],
                                                    }
                                                ]
                                            },
                                            // plugins: [ChartDataLabels],
                                            options: {
                                                responsive: true,
                                                options: {
                                                    plugins: {
                                                      datalabels: {
                                                        display: true,
                                                        align: 'bottom',
                                                        backgroundColor: '#ccc',
                                                        borderRadius: 3,
                                                        font: {
                                                          size: 18,
                                                        }
                                                      },
                                                    }
                                                  }
                                            }
                                        });
                $('#ajaxspinner8').hide();                                             
            }
            })            
    }
    
    function reloadGrid(){
        $('#ajaxspinner9').show();
        var eom = $('#eom').val();
        var korwil = $('#korwil').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        $.ajax({
            type: 'POST',
            url: '$urlQuery',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom, korwil:korwil},
            datatype: 'json',
            success:function(response) {
                var male = '';
                        male +=  response['male'] + '%';
                $('#male').html(male);
                
                var famale = '';
                        famale +=  response['famale'] + '%';
                $('#famale').html(famale);
                $('#ajaxspinner9').hide();                                               
            }
            })            
    }
    
    
    
   $("#tampil").click(function() {
        reloadGrid1();
        reloadGrid2();
        reloadGrid3();
        reloadGrid4();
        reloadGrid5();
        reloadGrid6();
        reloadGrid7();
        reloadGrid8();
        reloadGrid();
   });
   

JS
);


?>
<script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
<!--<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>-->
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.0/dist/chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
<style>
    .tombol {
        background: #2C97DF;
        color: white;
        border-top: 0;
        border-left: 0;
        border-right: 0;
        border-bottom: 0;
        padding: 5px 10px;
        text-decoration: none;
        font-family: sans-serif;
        font-size: 11pt;
        border-radius: 4px;
    }
</style>
<div class="laporan-data col-md-24" style="padding-left: 0;">
    <div class="box box-primary">
        <?= Html::beginForm(); ?>

        <div class="box-body">
            <div id="myModal" class="modal fade" style="padding-top: 200px">
                <div class="text-center"><i class="fa fa-spin fa-spinner" style="font-size: 100px;color: #77d5f7"></i>
                </div>
            </div>
            <form id="tgl_query">
                <!--                FILTER-->
                <div class="col-md-20" style="margin-top: 5px">
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-sm-24">
                                <label class="control-label col-sm-4">Tanggal</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-12">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'tgl1',
                                        'id' => 'tgl1',
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => Yii::$app->formatter->asDate('now', '01/MM/yyyy'),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                            'todayHighlight' => true
                                        ],
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                            <div class="col-md-12">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'tgl2',
                                        'id' => 'tgl2',
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => date('t/m/Y', strtotime("today")),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                        ],

                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                        </div>
                    </div>
                    <label class="control-label col-sm-1">Bulan</label>
                    <div class="col-md-5">
                        <? try {
                            echo DatePicker::widget([
                                'name' => 'TglKu',
                                'id' => 'TglKu',
                                'attribute2' => 'to_date',
                                'type' => DatePicker::TYPE_INPUT,
                                'value' => Yii::$app->formatter->asDate('now', 'MMMM  yyyy'),
                                'pluginOptions' => [
                                    'startView' => 'year',
                                    'minViewMode' => 'months',
                                    'format' => 'MM  yyyy'
                                ],
                                'pluginEvents' => [
                                    "change" => new JsExpression("
                                                function(e) {
                                                 let d = new Date(Date.parse('01 ' + $(this).val()));											 
                                                 var tgl1 = $('#tgl1');
                                                 var tgl2 = $('#tgl2');                                               
                                                  tgl1.kvDatepicker('update', d);
                                                  tgl2.kvDatepicker('update',new Date(d.getFullYear(), d.getMonth()+1, 0)); 
                                                }                                                                                            
                                                "),
                                ]
                            ]);
                        } catch (\yii\base\InvalidConfigException $e) {
                        } ?>
                    </div>
                    <div class="col-sm-2">
                        <? try {
                            echo CheckboxX::widget([
                                'name' => 'eom',
                                'value' => true,
                                'options' => ['id' => 'eom'],
                                'pluginOptions' => ['threeState' => false, 'size' => 'lg'],
                                'pluginEvents' => [
                                    "change" => new JsExpression("function(e) {
                                                 var today = new Date();
                                                 var tgl2 = $('#tgl2');
                                                 let tglku = tgl2.kvDatepicker('getDate');
                                                 var lastDayOfMonth = new Date(tglku.getFullYear(), tglku.getMonth()+1, 0);
                                                if($(this).val() == 1){
                                                    tgl2.kvDatepicker('update', lastDayOfMonth)
                                                 }else{
                                                    tgl2.kvDatepicker('update', new Date(today.getFullYear(), today.getMonth(), today.getDate()));
                                                 }
                                                }")
                                ]
                            ]);
                            echo '<label class="cbx-label" for="eom">EOM</label>';
                        } catch (\yii\base\InvalidConfigException $e) {
                        } ?>
                    </div>
                    <label class="control-label col-sm-1">Korwil</label>
                    <div class="col-sm-7">
                        <select name="korwil" id="korwil" class="form-control" style="height: 30px">
                            <option value="Dealer">Dealer</option>
                            <option value="Korwil">Korwil</option>
                            <option value="Nasional">Nasional</option>
                            <option value="BHC">BHC</option>
                            <option value="Anper">Anper</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div id="tampil">
                        <label class="control-label tombol"><i class="glyphicon glyphicon-new-window">
                                Tampil</i></label>
                    </div>
                </div>

                <!--                ISI-->
                <div class="col-md-24">
                    <div class="col-md-12">
                        <br><br>
                        <div class="box box-warning direct-chat direct-chat-warning">

                            <div class="box-header with-border">
                                <div class="col-md-22"><h3 class="box-title">PROFILE CUSTOMER</h3></div>
                                <div class="box-tools pull-right">
                                    <button class="btn" id="jp" type="button" formtarget="_blank "><i
                                            class="fa fa-file-text"></i></button>
                                </div>
                            </div>
                            <i id="ajaxspinner1" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <div class="box-body">
                                <div>
                                    <div class="classjenispekerjaan" id="jenispekerjaan">
                                        <canvas id="cnvjenispekerjaan"></canvas>
                                    </div>
                                </div>
                                <script>
                                </script>
                            </div>
                            <div class="box-footer"></div>
                        </div>
                        <i id="ajaxspinner9" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                        <div class="box-body">
                            <h3 class="box-title" style="text-align: center">SEX</h3>
                            <div class="col-md-24" style="width: 500px; height:249px;text-align: center">
                                <div class="col-md-12"
                                ">
                                <span style="font-size:150px;text-align: center" class="iconify"
                                      data-icon="fontisto:male"><i></i></span><br>
                                <label id="male" style="font-size: 50px"></label>
                            </div>
                            <div class="col-md-12">
                                <span style="font-size:150px;text-align: center" class="iconify"
                                      data-icon="fontisto:famale"><i></i></span><br>
                                <label id="famale" style="font-size: 50px"></label>
                            </div>
                        </div>
                        <script>
                        </script>
                    </div>
                    <i id="ajaxspinner3" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                    <div class="box-body">
                        <div>
                            <div class="classtenor" id="tenor">
                                <canvas id="cnvtenor"></canvas>
                            </div>
                        </div>
                        <script>
                        </script>
                    </div>
                    <div class="box-footer"></div>

                </div>
                <div class="col-md-12">
                    <br><br>
                    <div class="box box-warning direct-chat direct-chat-warning">

                        <div class="box-header with-border">
                            <div class="col-md-22"><h3 class="box-title">PROFILE CUSTOMER</h3></div>
                            <div class="box-tools pull-right">
                                <button class="btn" id="sbt" type="button" formtarget="_blank "><i
                                        class="fa fa-file-text"></i></button>
                            </div>
                        </div>
                        <i id="ajaxspinner2" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                        <div class="box-body">
                            <div>
                                <div class="classsbt" id="sbt">
                                    <canvas id="cnvsbt"></canvas>
                                </div>
                            </div>
                            <script>
                            </script>
                        </div>
                        <div class="box-footer"></div>
                    </div>

                    <i id="ajaxspinner4" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                    <div class="box-body">
                        <div>
                            <div class="classusia" id="usia">
                                <canvas id="cnvusia"></canvas>
                            </div>
                        </div>
                        <script>
                        </script>
                    </div>
                    <i id="ajaxspinner6" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                    <div class="box-body">
                        <div>
                            <div class="classpayment" id="payment">
                                <canvas id="cnvpayment"></canvas>
                            </div>
                        </div>
                        <script>
                        </script>
                    </div>
                    <div class="box-footer"></div>
                </div>
                <div class="col-md-24">
                    <i id="ajaxspinner8" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                    <div class="col-md-8">
                        <div>
                            <div class="classkredit" id="kredit">
                                <canvas id="cnvkredit"></canvas>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-16">
                        <i id="ajaxspinner5" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                        <div class="box-body">
                            <div>
                                <div class="classpendidikan" id="pendidikan">
                                    <canvas id="cnvpendidikan"></canvas>
                                </div>
                            </div>
                            <script>
                            </script>
                        </div>
                    </div>
                </div>
                <div class="col-md-24">
                    <i id="ajaxspinner7" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                    <div class="box-body">
                        <div>
                            <div class="classses" id="ses">
                                <canvas id="cnvses"></canvas>
                            </div>
                        </div>
                        <script>
                        </script>
                    </div>
                </div>

        </div>

    </div>

    </form>


    <div class="box-footer"></div>
    <?= Html::endForm(); ?>
</div>
</div>
<?php
$urljp = Url::toRoute(['report/show-jenis-pekerjaan', 'param' => '"JenisPekerjaan"']);
$urlsbt = Url::toRoute(['report/show-sales-by-type', 'param' => '"SalesByType"']);
$user = $_SESSION['__id'];
$this->registerJsVar( 'userid', $user );

$this->registerJs(<<< JS
    
        $('#jp').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            var korwil = $('#korwil').val();
            var eom = $('#eom').val();
            window.open('{$urljp}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + korwil + '","","",""', '_blank');
        });
        $('#sbt').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            var korwil = $('#korwil').val();
            window.open('{$urlsbt}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + korwil + '","","",""', '_blank');
        });
        

JS
);








