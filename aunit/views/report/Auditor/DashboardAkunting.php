<?php

use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\checkbox\CheckboxX;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;

?>


<?php
$this->title = 'Dashboard Akuntansi';
$this->params['breadcrumbs'][] = $this->title;
$formatter = \Yii::$app->formatter;
$user = $_SESSION['__id'];
$urlQuery = Url::toRoute(['report/komponen-lr']);
$urlQueryMonthly = Url::toRoute(['report/komponen-lr-monthly']);
$urlpotongan = Url::toRoute(['report/akuntansi-detail-potongan', 'param' => '"KompPotongan"']);
$urlmo = Url::toRoute(['report/akuntansi-detail-mo', 'param' => '"KompMO"']);
$urlpendapatan = Url::toRoute(['report/akuntansi-detail-pendapatan', 'param' => '"KompPdptNonOprs"']);
$format = <<< SCRIPT
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJsVar( 'userid', $user );
$this->registerJs($format, View::POS_HEAD);
$this->registerJs(<<< JS
     
    
    
    
    Chart.register(ChartDataLabels);
function reloadGrid1(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
         $('#ajaxspinner1').show();
        $.ajax({
            type: 'POST',
            url: '$urlQueryMonthly',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            success:function(response) {
                ctxlabarugi6bulan = document.getElementById("labarugi6bulan");
                // ctxlabarugi6bulan = document.getElementById("labarugi6bulan").getContext("2d");
                // pointlabarugi6bulan = document.getElementById("labarugi6bulan");
                ctx6bulan = new Chart(ctxlabarugi6bulan, {
                                                data: {
                                                    labels: response['Bulan'],
                                                    datasets: [
                                                        {
                                                            type: "bar",
                                                            label: "Potongan",
                                                            backgroundColor: "dodgerblue",
                                                            borderColor: "dodgerblue",
                                                            borderWidth: 1,
                                                            data: response['Potongan'],
                                                            link: 'https://www.google.com',
                                                        },
                                                        {
                                                            type: "bar",
                                                            label: "LabaKotor",
                                                            backgroundColor: "coral",
                                                            borderColor: "coral",
                                                            borderWidth: 1,
                                                            data: response['LabaKotor']
                                                        },{
                                                            type: "bar",
                                                            label: "Opex",
                                                            backgroundColor: "grey",
                                                            borderColor: "grey",
                                                            borderWidth: 1,
                                                            data: response['Opex']
                                                        },{
                                                            type: "bar",
                                                            label: "PendapatanNonOP",
                                                            backgroundColor: "lightblue",
                                                            borderColor: "lightblue",
                                                            borderWidth: 1,
                                                            data: response['PendapatanNonOP']
                                                        },{
                                                            type: "bar",
                                                            label: "MO",
                                                            backgroundColor: "lightgreen",
                                                            borderColor: "lightgreen",
                                                            borderWidth: 1,
                                                            data: response['MO']
                                                        },{
                                                            type: "bar",
                                                            label: "ProfitNPBT",
                                                            backgroundColor: "green",
                                                            borderColor: "green",
                                                            borderWidth: 1,
                                                            data: response['ProfitNPBT']
                                                        }
                                                    ]
                                                },
                                                plugins: [ChartDataLabels],
                                                options: {
                                                    plugins: {
                                                        datalabels: {
                                                          display: false
                                                        }
                                                      },
                                                    responsive: true,
                                                    scales: {
                                                          x: {
                                                            stacked: true,
                                                          },
                                                          y: {
                                                            stacked: true
                                                          }
                                                        },
                                                    legend: {
                                                        position: "top",
                                                        display: true,
                                                    },
                                                    title: {
                                                        display: true,
                                                        text: "Chart.js Bar Chart"
                                                    },
                                                    
                                                }
                                            }); 
                $('#ajaxspinner1').hide();
                
                function clickHandler(click) {
                    const points = ctx6bulan.getElementsAtEventForMode(click, 'nearest',
                    {intersect: true}, true);
                    if(points.length){
                        const  firstPoint = points[0];
                        const  value = ctx6bulan.data.datasets[firstPoint.datasetIndex].data[firstPoint.index];
                        const  labelx = ctx6bulan.data.datasets[firstPoint.datasetIndex].label;
                        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
                        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
                        var level = $('#level').val();
                        console.log(labelx);
                        if (labelx == 'Potongan'){                            
                            window.open('{$urlpotongan}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","","",""', '_blank');
                        }else if (labelx == 'MO'){                            
                            window.open('{$urlmo}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","","",""', '_blank');
                        }
                        else if (labelx == 'PendapatanNonOP'){                            
                            window.open('{$urlpendapatan}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","","",""', '_blank');
                        }
                    }
                }    
                ctxlabarugi6bulan.onclick = clickHandler;
                
            }
        })
    }  
    

    
function reloadGrid2(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
         $('#ajaxspinner2').show();
        $.ajax({
            type: 'POST',
            url: '$urlQuery',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            success:function(response) {
                ctxlabarugi= document.getElementById("labarugi").getContext("2d");
                ctxlabarugi = new Chart(ctxlabarugi, {
                                                data: {
                                                    labels: response['Kota'],
                                                    datasets: [
                                                        {
                                                            type: "bar",
                                                            label: "Potongan",
                                                            backgroundColor: "dodgerblue",
                                                            borderColor: "dodgerblue",
                                                            borderWidth: 1,
                                                            data: response['Potongan']
                                                        },
                                                        {
                                                            type: "bar",
                                                            label: "LabaKotor",
                                                            backgroundColor: "coral",
                                                            borderColor: "coral",
                                                            borderWidth: 1,
                                                            data: response['LabaKotor']
                                                        },{
                                                            type: "bar",
                                                            label: "Opex",
                                                            backgroundColor: "grey",
                                                            borderColor: "grey",
                                                            borderWidth: 1,
                                                            data: response['Opex']
                                                        },{
                                                            type: "bar",
                                                            label: "PendapatanNonOP",
                                                            backgroundColor: "lightblue",
                                                            borderColor: "lightblue",
                                                            borderWidth: 1,
                                                            data: response['PendapatanNonOP']
                                                        },{
                                                            type: "bar",
                                                            label: "MO",
                                                            backgroundColor: "lightgreen",
                                                            borderColor: "lightgreen",
                                                            borderWidth: 1,
                                                            data: response['MO']
                                                        },{
                                                            type: "bar",
                                                            label: "ProfitNPBT",
                                                            backgroundColor: "green",
                                                            borderColor: "green",
                                                            borderWidth: 1,
                                                            data: response['ProfitNPBT']
                                                        }
                                                    ]
                                                },
                                                plugins: [ChartDataLabels],
                                                options: {
                                                    plugins: {
                                                        datalabels: {
                                                          display: false
                                                        }
                                                      },
                                                    responsive: true,
                                                    scales: {
                                                          x: {
                                                            stacked: true,
                                                          },
                                                          y: {
                                                            stacked: true
                                                          }
                                                        },
                                                    legend: {
                                                        position: "top"
                                                    },
                                                    title: {
                                                        display: true,
                                                        text: "Chart.js Bar Chart"
                                                    },
                                                    
                                                }
                                            }); 
                $('#ajaxspinner2').hide();
            }
        })
    }    

   $("#tampil").click(function() {
       $('#labarugi6bulan').remove();
        $('.1').append('<canvas id="labarugi6bulan"><canvas>'); 
        reloadGrid1();
        $('#labarugi').remove();
        $('.2').append('<canvas id="labarugi"><canvas>'); 
        reloadGrid2();
   }); 

JS
);

?>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@4.4.1/dist/chart.umd.min.js"></script>
<!--    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.0/dist/chart.min.js"></script>-->
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-chart-treemap@2.3.0/dist/chartjs-chart-treemap.min.js">

    </script>




    <style>
        .tombol {
            background: #2C97DF;
            color: white;
            border-top: 0;
            border-left: 0;
            border-right: 0;
            border-bottom: 0;
            padding: 5px 10px;
            text-decoration: none;
            font-family: sans-serif;
            font-size: 11pt;
            border-radius: 4px;
        }

        .outer {
            overflow-y: auto;
            height: 100px;
        }

        .outer table {
            width: 100%;
            table-layout: fixed;
            border: 1px solid black;
            border-spacing: 1px;
        }

        .outer table th {
            text-align: left;
            top: 0;
            position: sticky;
            background-color: white;
        }
    </style>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
            <?= Html::beginForm(); ?>
            <div class="box-body">
                <div class="col-md-24">
                <div class="col-md-2">
                    <div class="form-group">
                        <div class="col-sm-24">
                            <label class="control-label col-sm-4">Tanggal</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <div class="col-md-12">
                            <? try {
                                echo DatePicker::widget([
                                    'name' => 'tgl1',
                                    'id' => 'tgl1',
                                    'type' => DatePicker::TYPE_INPUT,
                                    'value' => Yii::$app->formatter->asDate('now', '01/MM/yyyy'),
//                                                'value' => '01/02/2021',
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'dd/mm/yyyy',
                                        'todayHighlight' => true
                                    ],
                                ]);
                            } catch (\yii\base\InvalidConfigException $e) {
                            } ?>
                        </div>
                        <div class="col-md-12">
                            <? try {
                                echo DatePicker::widget([
                                    'name' => 'tgl2',
                                    'id' => 'tgl2',
                                    'type' => DatePicker::TYPE_INPUT,
                                    'value' => date('t/m/Y', strtotime("today")),
//                                                'value' => '27/02/2021',
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'dd/mm/yyyy',
                                    ],
                                ]);
                            } catch (\yii\base\InvalidConfigException $e) {
                            } ?>
                        </div>
                    </div>
                </div>
                <label class="control-label col-sm-1">Bulan</label>
                <div class="col-md-3">
                    <? try {
                        echo DatePicker::widget([
                            'name' => 'TglKu',
                            'id' => 'TglKu',
                            'attribute2' => 'to_date',
                            'type' => DatePicker::TYPE_INPUT,
                            'value' => Yii::$app->formatter->asDate('now', 'MMMM  yyyy'),
                            'pluginOptions' => [
                                'startView' => 'year',
                                'minViewMode' => 'months',
                                'format' => 'MM  yyyy'
                            ],
                            'pluginEvents' => [
                                "change" => new JsExpression("
                                                function(e) {
                                                 let d = new Date(Date.parse('01 ' + $(this).val()));											 
                                                 var tgl1 = $('#tgl1');
                                                 var tgl2 = $('#tgl2');
                                                  tgl1.kvDatepicker('update', d);
                                                  tgl2.kvDatepicker('update',new Date(d.getFullYear(), d.getMonth()+1, 0));     
                                                }                                                                                            
                                                "),
                            ]
                        ]);
                    } catch (\yii\base\InvalidConfigException $e) {
                    } ?>
                </div>
                <div class="col-sm-2">
                    <? try {
                        echo CheckboxX::widget([
                            'name' => 'eom',
                            'value' => true,
                            'options' => ['id' => 'eom'],
                            'pluginOptions' => ['threeState' => false, 'size' => 'lg'],
                            'pluginEvents' => [
                                "change" => new JsExpression("function(e) {
                                                 var today = new Date();
                                                 var tgl2 = $('#tgl2');
                                                 let tglku = tgl2.kvDatepicker('getDate');
                                                 var lastDayOfMonth = new Date(tglku.getFullYear(), tglku.getMonth()+1, 0);
                                                if($(this).val() == 1){
                                                    tgl2.kvDatepicker('update', lastDayOfMonth)
                                                 }else{
                                                    tgl2.kvDatepicker('update', new Date(today.getFullYear(), today.getMonth(), today.getDate()));
                                                 }
                                                }")
                            ]
                        ]);
                        echo '<label class="cbx-label" for="eom">EOM</label>';
                    } catch (\yii\base\InvalidConfigException $e) {
                    } ?>
                </div>
                    <label class="control-label col-sm-1">Level</label>
                    <div class="col-sm-3">
                        <?= Html::dropDownList('level', null, [
                            'Dealer' => 'Dealer',
                            'Korwil' => 'Korwil',
                            'Nasional' => 'Nasional',
                            'Anper' => 'Anper',
                            'BHC' => 'BHC',
                        ], ['id' => 'level', 'text' => 'Pilih tipe laporan', 'class' => 'form-control']) ?>

                    </div>
                    <div class="col-sm-5" style="height: 50px" id="tampil">
                        <label class="control-label tombol"><i class="glyphicon glyphicon-new-window">
                                Tampil</i></label>
                    </div>

                </div>

                <div class="col-md-24"><br><br></div>

                <div class="row">
                    <div class="col-md-24">
                        <div class="box box-primary direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">PROSENTASE KOMPONEN LABA RUGI - 6 BULANAN</h3>
                            </div>
                            <i id="ajaxspinner1" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-2"></div>
                                <div class="col-md-22">
                                    <div class="1">
                                        <canvas id="labarugi6bulan"></canvas>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-24">
                        <div class="box box-primary direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">PROSENTASE KOMPONEN LABA RUGI</h3>
                            </div>
                            <i id="ajaxspinner2" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-2"></div>
                                <div class="col-md-22">
                                    <div class="2">
                                        <canvas id="labarugi"></canvas>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                </div>
                <br><br>
            </div>
            <div class="box-footer">

            </div>
            <?= Html::endForm(); ?>
        </div>
    </div>
<?php
