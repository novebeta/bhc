<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use kartik\checkbox\CheckboxX;
use yii\web\JsExpression;

\aunit\assets\AppAsset::register($this);
\aunit\assets\JqwidgetsAsset::register($this);
$this->title = 'PROFILE CUSTOMER - SALES BY TYPE';
$formatter = \Yii::$app->formatter;
$this->registerCss(".jqx-widget-content { font-size: 12px; }");

$rowdata = $data;
$this->registerJs(<<< JS

 
JS
);

?>
    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.0/dist/chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
    <style>
        .table td {
            border: lightgrey solid 1px !important;
        }

        .table th {
            border: lightgrey solid 1px !important;
        }
    </style>
<!--    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>-->
<?= Html::beginForm(); ?>
<div class="box box-warning direct-chat direct-chat-warning">
    <div class="box box-default">

        <canvas id="myChart"></canvas>
        <script>

            var isi = <?=json_encode($data['isi'])?>;
            var label = <?=json_encode($data['label'])?>;
            Chart.register(ChartDataLabels);

            ctx1 = document.getElementById("myChart").getContext("2d");
            jenispekerjaan = new Chart(ctx1, {
                type: "bar",
                data: {
                    labels: label,
                    datasets: [
                        {
                            label: "SALES BY TYPE",
                            backgroundColor: "#e9967a",
                            data: isi,
                        }
                    ]
                },
                plugins: [ChartDataLabels],
                options: {
                    indexAxis: 'y',
                    responsive: true,
                    legend: {
                        position: "right"
                    },
                    title: {
                        display: true,
                        text: "Chart.js Bar Chart"
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                }
            });

        </script>

    </div>
    <br><br>
</div>

<?= Html::endForm(); ?>


