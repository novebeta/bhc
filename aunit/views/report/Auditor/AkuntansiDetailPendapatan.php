<?php
use yii\helpers\Url;

\aunit\assets\AppAsset::register($this);
\aunit\assets\JqwidgetsAsset::register($this);
$this->title = 'PENDAPATAN NON OPERASIONAL';
$formatter = \Yii::$app->formatter;
$this->registerCss(".jqx-widget-content { font-size: 12px; }");

$rowdata = $data;
$arraydata = $data['data'];


foreach ($arraydata as $data => $key){
    $K9001[] = $key['K9001'];
    $K9002[] = $key['K9002'];
    $K9003[] = $key['K9003'];
    $K9004[] = $key['K9004'];
    $K9009[] = $key['K9009'];
    $bulan[] = $key['Bulan'];
}
$this->registerJsVar('K9001', $K9001);
$this->registerJsVar('K9002', $K9002);
$this->registerJsVar('K9003', $K9003);
$this->registerJsVar('K9004', $K9004);
$this->registerJsVar('K9009', $K9009);
$this->registerJsVar('bulan', $bulan);

$this->registerJs(<<< JS
    
    
    Chart.register(ChartDataLabels);
    
    ctxlabarugi6bulan = document.getElementById("labarugi6bulan");
                ctx6bulan = new Chart(ctxlabarugi6bulan, {
                                                data: {
                                                    labels: bulan,
                                                    datasets: [
                                                        {
                                                            type: "bar",
                                                            label: "K9001",
                                                            backgroundColor: "dodgerblue",
                                                            borderColor: "dodgerblue",
                                                            borderWidth: 1,
                                                            data: K9001,
                                                        },
                                                        {
                                                            type: "bar",
                                                            label: "K9002",
                                                            backgroundColor: "coral",
                                                            borderColor: "coral",
                                                            borderWidth: 1,
                                                            data: K9002
                                                        },{
                                                            type: "bar",
                                                            label: "K9004",
                                                            backgroundColor: "grey",
                                                            borderColor: "grey",
                                                            borderWidth: 1,
                                                            data: K9004
                                                        },{
                                                            type: "bar",
                                                            label: "K9009",
                                                            backgroundColor: "lightblue",
                                                            borderColor: "lightblue",
                                                            borderWidth: 1,
                                                            data: K9009
                                                        }
                                                    ]
                                                },
                                                plugins: [ChartDataLabels],
                                                options: {
                                                    plugins: {
                                                        datalabels: {
                                                          display: false
                                                        }
                                                      },
                                                    responsive: true,
                                                    scales: {
                                                          x: {
                                                            stacked: true,
                                                          },
                                                          y: {
                                                            stacked: true
                                                          }
                                                        },
                                                    legend: {
                                                        position: "top",
                                                        display: true,
                                                    },
                                                    title: {
                                                        display: true,
                                                        text: "Chart.js Bar Chart"
                                                    },
                                                    
                                                }
                                            }); 
JS
);

?>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@4.4.1/dist/chart.umd.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-chart-treemap@2.3.0/dist/chartjs-chart-treemap.min.js">

    </script>
<style>
    .table td{
        border: lightgrey solid 1px !important;
    }
    .table th{
        border: lightgrey solid 1px !important;
    }
</style>
    <div class="laporan-query col-md-24" style="padding-left: 0;">
        <div class="box box-default">

            <div class="box-body">
                <div class="col-md-24">
                    <div >
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-24">
                                    <div class="box box-primary direct-chat direct-chat-warning">
                                        <div class="box-body">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-22">
                                                <div class="1">
                                                    <canvas id="labarugi6bulan"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.box-body -->
                                        <div class="box-footer"></div>
                                        <!-- /.box-footer-->
                                    </div>
                                    <!--/.direct-chat -->
                                </div>
                            </div>

                            <div style="position: relative;height: 550px;overflow: auto;">
                                <table class="table table-bordered table-striped mb-0" id="table1">
                                    <thead>
                                    <tr>
                                        <td scope="col" style="text-align: center">No Urut</td>
                                        <td scope="col" style="text-align: center">Tgl1</td>
                                        <td scope="col" style="text-align: center">Tgl2</td>
                                        <td scope="col" style="text-align: center">Bulan</td>
                                        <td scope="col" style="text-align: center">K9001</td>
                                        <td scope="col" style="text-align: center">K9002</td>
                                        <td scope="col" style="text-align: center">K9003</td>
                                        <td scope="col" style="text-align: center">K9004</td>
                                        <td scope="col" style="text-align: center">K9009</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($rowdata['data'] as $key){
                                        echo '<tr><td>'.$key['NoUrut'].'</td>';
                                        echo '<td>'.$key['Tgl1'].'</td>';
                                        echo '<td>'.$key['Tgl2'].'</td>';
                                        echo '<td>'.$key['Bulan'].'</td>';
                                        echo '<td style="text-align: right">'.substr($key['K9001'], 0, -3).'</td>';
                                        echo '<td style="text-align: right">'.substr($key['K9002'], 0, -3).'</td>';
                                        echo '<td style="text-align: right">'.substr($key['K9003'], 0, -3).'</td>';
                                        echo '<td style="text-align: right">'.substr($key['K9004'], 0, -3).'</td>';
                                        echo '<td style="text-align: right">'.substr($key['K9009'], 0, -3).'</td></tr>';
                                    }
                                    ?>

                                    </tbody>
                                </table>

                            </div><br>

                        </div>
                        <div class="box-footer"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php
$this->registerJs(<<< JS


JS
);


