<?php
use yii\helpers\Url;

\aunit\assets\AppAsset::register($this);
\aunit\assets\JqwidgetsAsset::register($this);
$this->title = 'MO';
$formatter = \Yii::$app->formatter;
$this->registerCss(".jqx-widget-content { font-size: 12px; }");

$rowdata['data'] = $data;
$arraydata = $data;
$sof =[];
$muf=[];
$mpm = [];
$mega = [];
$mcf = [];
$fif = [];
$adira = [];

foreach ($arraydata as $key){
    if($key['Leasing'] == 'SOF'){
        $sof[]= $key['SumMO'];
    }else if($key['Leasing'] == 'MUF'){
        $muf[]= $key['SumMO'];
    }else if($key['Leasing'] == 'MPM'){
        $mpm[]= $key['SumMO'];
    }else if($key['Leasing'] == 'MEGA'){
        $mega[]= $key['SumMO'];
    }else if($key['Leasing'] == 'MCF'){
        $mcf[]= $key['SumMO'];
    }else if($key['Leasing'] == 'FIF'){
        $fif[]= $key['SumMO'];
    }else if($key['Leasing'] == 'ADIRA'){
        $adira[]= $key['SumMO'];
    }
    $bulan[] = $key['Bulan'];
}

$this->registerJsVar('datasof', $sof);
$this->registerJsVar('datamuf', $muf);
$this->registerJsVar('datampm', $mpm);
$this->registerJsVar('datamega', $mega);
$this->registerJsVar('datamcf', $mcf);
$this->registerJsVar('datafif', $fif);
$this->registerJsVar('dataadira', $adira);
$this->registerJsVar('bulan', $bulan);

$this->registerJs(<<< JS
    
    let dup = [...new Set(bulan)];
    console.log(bulan);
    console.log(dup);
    Chart.register(ChartDataLabels);
                
                
                ctxlabarugi6bulan = document.getElementById("labarugi6bulan");
                ctx6bulan = new Chart(ctxlabarugi6bulan, {
                                                data: {
                                                    labels: dup,
                                                    datasets: [
                                                        {
                                                            type: "bar",
                                                            label: "SOF",
                                                            backgroundColor: "dodgerblue",
                                                            borderColor: "dodgerblue",
                                                            borderWidth: 1,
                                                            data: datasof,
                                                        },
                                                        {
                                                            type: "bar",
                                                            label: "MUF",
                                                            backgroundColor: "coral",
                                                            borderColor: "coral",
                                                            borderWidth: 1,
                                                            data: datamuf
                                                        },{
                                                            type: "bar",
                                                            label: "MPM",
                                                            backgroundColor: "grey",
                                                            borderColor: "grey",
                                                            borderWidth: 1,
                                                            data: datampm
                                                        },{
                                                            type: "bar",
                                                            label: "MEGA",
                                                            backgroundColor: "lightblue",
                                                            borderColor: "lightblue",
                                                            borderWidth: 1,
                                                            data: datamega
                                                        },{
                                                            type: "bar",
                                                            label: "MCF",
                                                            backgroundColor: "lightgreen",
                                                            borderColor: "lightgreen",
                                                            borderWidth: 1,
                                                            data: datamcf
                                                        },{
                                                            type: "bar",
                                                            label: "FIF",
                                                            backgroundColor: "green",
                                                            borderColor: "green",
                                                            borderWidth: 1,
                                                            data: datafif
                                                        },{
                                                            type: "bar",
                                                            label: "ADIRA",
                                                            backgroundColor: "red",
                                                            borderColor: "red",
                                                            borderWidth: 1,
                                                            data: dataadira
                                                        }
                                                    ]
                                                },
                                                plugins: [ChartDataLabels],
                                                options: {
                                                    plugins: {
                                                        datalabels: {
                                                          display: false
                                                        }
                                                      },
                                                    responsive: true,
                                                    scales: {
                                                          x: {
                                                            stacked: true,
                                                          },
                                                          y: {
                                                            stacked: true
                                                          }
                                                        },
                                                    legend: {
                                                        position: "top",
                                                        display: true,
                                                    },
                                                    title: {
                                                        display: true,
                                                        text: "Chart.js Bar Chart"
                                                    },
                                                    
                                                }
                                            }); 
                

 
JS
);

?>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@4.4.1/dist/chart.umd.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-chart-treemap@2.3.0/dist/chartjs-chart-treemap.min.js">

    </script>
<style>
    .table td{
        border: lightgrey solid 1px !important;
    }
    .table th{
        border: lightgrey solid 1px !important;
    }
</style>
    <div class="laporan-query col-md-24" style="padding-left: 0;">
        <div class="box box-default">

            <div class="box-body">
                <div class="col-md-24">
                    <div >
                        <div class="box-body">

                            <div class="col-md-24"><br><br></div>
                            <div class="row">
                                <div class="col-md-24">
                                    <div class="box box-primary direct-chat direct-chat-warning">
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-22">
                                                <div class="1">
                                                    <canvas id="labarugi6bulan"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.box-body -->
                                        <div class="box-footer"></div>
                                        <!-- /.box-footer-->
                                    </div>
                                    <!--/.direct-chat -->
                                </div>
                            </div>


                            <div style="position: relative;height: 550px;overflow: auto;">
                                <table class="table table-bordered table-striped mb-0" id="table1">
                                    <thead>
                                    <tr>
                                        <td scope="col" style="text-align: center">BULAN</td>
                                        <td scope="col" style="text-align: center">LEASING</td>
                                        <td scope="col" style="text-align: center">GRAND TOTAL</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($rowdata['data'] as $key){
                                        echo '<tr><td>'.$key['Bulan'].'</td>';
                                        echo '<td>'.$key['Leasing'].'</td>';
                                        echo '<td style="text-align: right">'.substr($key['SumMO'], 0, -3).'</td></tr>';
                                    }
                                    ?>

                                    </tbody>
                                </table>

                            </div><br>

                        </div>
                        <div class="box-footer"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php
$this->registerJs(<<< JS


JS
);


