<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use kartik\checkbox\CheckboxX;
use yii\web\JsExpression;

\aunit\assets\AppAsset::register($this);
\aunit\assets\JqwidgetsAsset::register($this);
$this->title = 'DISTRIBUTION';
$formatter = \Yii::$app->formatter;
$this->registerCss(".jqx-widget-content { font-size: 12px; }");

$rowdata = $data;
$this->registerJs(<<< JS

 
JS
);

?>
    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
    <style>
        .table td{
            border: lightgrey solid 1px !important;
        }
        .table th{
            border: lightgrey solid 1px !important;
        }
    </style>
    <div class="laporan-query col-md-24" style="padding-left: 0;">
        <div class="box box-default">
            <button class="btn pull-right" id='btnExcel' type="button" style="background-color:limegreen;width: 40px;height: 30px;margin: 10px;position: absolute;top: -45px;right: 10px"><i class="fa fa-file-excel-o fa-lg"></i></button>
            <div class="box-body">
                <div class="col-md-24">
                    <div>
                        <div class="box-body">

                            <div style="position: relative;height: 620px;overflow: auto; width: 600px">
                                <table class="table table-bordered table-striped mb-0" id="table1">
                                    <thead>
                                    <tr>
                                        <th scope="col" rowspan="2" style="text-align: center">TYPE</th>
                                        <th scope="col" colspan="3" style="text-align: center">DISTRIBUTION</th>
                                    </tr>
                                    <tr>
                                        <th scope="col" style="text-align: center">M-1</th>
                                        <th scope="col" style="text-align: center">M</th>
                                        <th scope="col" style="text-align: center">GROWTH %</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($rowdata as $key){
                                        echo '<tr><td style="text-align: left;width: 500px">'.$key['MotorNama'].'</td>';
                                        echo '<td style="text-align: right;width: 200px">'.substr($key['DisMBefore'],0,-3).'</td>';
                                        echo '<td style="text-align: right;width: 200px">'.substr($key['DisMNow'],0,-3).'</td>';
                                        if(empty($key['Growth'])){
                                        echo '<td style="text-align: right;width: 200px">'.substr($key['DisGrowth'],0,-3).'</td>';
                                        }
                                        else{
                                            echo '<td style="text-align: right;width: 200px">'.substr($key['Growth'],0,-3).'</td>';
                                        }
                                    }
                                    ?>

                                    </tbody>
                                </table>

                            </div><br>

<!--                            <div style="position: relative;height: 550px;overflow: auto; width: 400px">-->
<!--                                <table class="table table-bordered table-striped mb-0" id="table1">-->
<!--                                    <thead>-->
<!--                                    <tr>-->
<!--                                        <th scope="col" style="text-align: center">Hari</th>-->
<!--                                        <th scope="col" style="text-align: center">Target</th>-->
<!--                                        <th scope="col" style="text-align: center">ACV</th>-->
<!--                                        <th scope="col" style="text-align: center">Persen</th>-->
<!--                                        <th scope="col" style="text-align: center">Hutang</th>-->
<!--                                    </tr>-->
<!--                                    </thead>-->
<!--                                    <tbody>-->
<!--                                    --><?php
//                                    foreach ($rowdata as $key){
//                                        echo '<tr><td style="text-align: left;width: 60px">'.$key['Hari'].'</td>';
//                                        echo '<td style="text-align: right;width: 60px">'.$key['Target'].'</td>';
//                                        echo '<td style="text-align: right;width: 60px">'.$key['ACV'].'</td>';
//                                        echo '<td style="text-align: right;width: 60px">'.$key['Persen'].'</td>';
//                                        echo '<td style="text-align: right;width: 60px">'.$key['Hutang'].'</td>';
//                                    }
//                                    ?>
<!---->
<!--                                    </tbody>-->
<!--                                </table>-->
<!---->
<!--                            </div><br>-->


                        </div>
                        <div class="box-footer"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php
$this->registerJs(<<< JS

 $(document).ready(function () {
      $("#btnExcel").click(function(){
        TableToExcel.convert(document.getElementById("table1"), {
            name: "PenjualanHarian.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
  });

JS
);

