<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\checkbox\CheckboxX;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\helpers\Url;

$this->title = 'Dashboard Marketing';
$this->params['breadcrumbs'][] = $this->title;
$formatter = \Yii::$app->formatter;

$format = <<< SCRIPT

SCRIPT;

$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
$urlQueryDDT = Url::toRoute(['report/marquery']);
$this->registerJs(<<< JS
    
    Chart.register(ChartDataLabels);

    function reloadGrid(){
        $("#myModal").modal('show');
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        $.ajax({
            type: 'POST',
            url: '$urlQueryDDT',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom},
            datatype: 'json',
            success:function(response) {
                    $('#myChartDDT').remove(); 
                    $('.2').append('<canvas id="myChartDDT"><canvas>');
                    $('#myChartDRST').remove(); 
                    $('.3').append('<canvas id="myChartDRST"><canvas>');
                    $('#myChartSBS').remove(); 
                    $('.4').append('<canvas id="myChartSBS"><canvas>');
                    $('#myChartSS').remove(); 
                    $('.1').append('<canvas id="myChartSS" height="360px"><canvas>');
                    $('#myChartSS2').remove(); 
                    $('.5').append('<canvas id="myChartSS2" height="80px"><canvas>');
                    $('#myChartSS3').remove(); 
                    $('.6').append('<canvas id="myChartSS3" height="80px"><canvas>');
                console.log(tgl1);
                console.log(tgl2);
                console.log(response['rowsddt']);
                console.log(response['rowdrst']);
                console.log(response['rows']);
                console.log(response['rowss']);
                console.log(response['rowss2']);
                console.log(response['agingpiutang']);
    
                //DAILY DISTRIBUTION TREND
                ctx1 = document.getElementById("myChartDDT").getContext("2d");
                myChartDDT = new Chart(ctx1, {
                                            // type: "bar",
                                            data: {
                                                labels: response['labelddt'],
                                                datasets: [
                                                    {
                                                        type: 'bar',
                                                        label: "Actual",
                                                        backgroundColor: "orange",
                                                        borderColor: "orange",
                                                        borderWidth: 1,
                                                        data: response['actualddt']
                                                    },
                                                    {
                                                        type: 'line',
                                                        label: "Daily Target",
                                                        backgroundColor: "Aquamarine",
                                                        borderColor: "Aquamarine",
                                                        borderWidth: 5,
                                                        data: response['targetddt']
                                                    },
                                                ]
                                            },
                                            plugins: [ChartDataLabels],
                                            options: {
                                                responsive: true,
                                                legend: {
                                                    position: "top"
                                                },
                                                title: {
                                                    display: true,
                                                    text: "Chart.js Bar Chart"
                                                },
                                                scales: {
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }]
                                                },
                                            }
                                        });
                
                //DAILY RETAIL SALES TREND
                ctx2 = document.getElementById("myChartDRST").getContext("2d");
                myChartDRST = new Chart(ctx2, {
                                            // type: "bar",
                                            data: {
                                                labels: response['labeldrst'],
                                                datasets: [
                                                    {
                                                        type: "bar",
                                                        label: "Actual",                                                        
                                                        backgroundColor: "Gold",
                                                        borderColor: "Gold",
                                                        borderWidth: 1,
                                                        data: response['actualdrst']
                                                    },
                                                    {
                                                        type: "line",
                                                        label: "Daily Target",
                                                        backgroundColor: "Aqua",
                                                        borderColor: "Aqua",
                                                        borderWidth: 5,
                                                        data: response['targetdrst']
                                                    },
                                                ]
                                            },
                                            options: {
                                                responsive: true,
                                                legend: {
                                                    position: "top"
                                                },
                                                title: {
                                                    display: true,
                                                    text: "Chart.js Bar Chart"
                                                },
                                                scales: {
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }]
                                                }
                                            }
                                        });
                
                //SALES BY SEGMEN                
                ctx3 = document.getElementById("myChartSBS").getContext("2d");
                myChartSBS = new Chart(ctx3, {
                                            type: "bar",
                                            data: {
                                                labels: response['label'],
                                                datasets: [
                                                    {
                                                        label: "M",
                                                        backgroundColor: "Blue",
                                                        borderColor: "Blue",
                                                        borderWidth: 1,
                                                        data: response['mtd']
                                                    },
                                                    {
                                                        label: "M-1",
                                                        backgroundColor: "Chocolate",
                                                        borderColor: "Chocolate",
                                                        borderWidth: 1,
                                                        data: response['mtd1']
                                                    },
                                                ]
                                            },
                                            options: {
                                                responsive: true,
                                                legend: {
                                                    position: "top"
                                                },
                                                title: {
                                                    display: true,
                                                    text: "Chart.js Bar Chart"
                                                },
                                                scales: {
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }]
                                                }
                                            }
                                        });
                                        
                //SALES STRUCTURE                
                ctx4 = document.getElementById("myChartSS").getContext("2d");
                myChartSS = new Chart(ctx4, {
                                            type: "bar",
                                            data: {
                                                labels: response['labelss'],
                                                datasets: [
                                                    {
                                                        label: "M",
                                                        backgroundColor: "deepskyblue",
                                                        borderColor: "deepskyblue",
                                                        borderWidth: 1,
                                                        data: response['cashss']
                                                    },
                                                    {
                                                        label: "M-1",
                                                        backgroundColor: "Orange",
                                                        borderColor: "Orange",
                                                        borderWidth: 1,
                                                        data: response['creditss']
                                                    },
                                                ]
                                            },
                                            options: 
                                            {
                                                indexAxis: 'y',
                                                barPercentage: 0.8,
                                                plugins: {
                                                  datalabels: {
                                                    anchor: 'end',
                                                    align: 'end',
                                                    offset: 8,
                                                  },
                                                },
                                                responsive: true,
                                                maintainAspectRatio: false,
                                                legend: {
                                                    position: "top",
                                                    
                                                },
                                                title: {
                                                    display: true,
                                                    text: "Chart.js Bar Chart"
                                                },
                                                scales: {
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true,
                                                            padding: 100
                                                        }
                                                    }]
                                                }
                                            }
                                        });  
                
                ctx5 = document.getElementById("myChartSS2").getContext("2d");
                myChartSS2 = new Chart(ctx5, {
                                            type: "pie",
                                            data: {
                                                // labels: ['Cash', ''],
                                                datasets: [
                                                    {
                                                        label: "CASH / KREDIT",
                                                        backgroundColor: ["#DE3163","#FFBF00"],
                                                        data: response['cashss2']
                                                    }
                                                ]
                                            },
                                            // plugins: [ChartDataLabels],
                                            options: {
                                                responsive: true,
                                                options: {
                                                    plugins: {
                                                      datalabels: {
                                                        display: true,
                                                        align: 'bottom',
                                                        backgroundColor: '#ccc',
                                                        borderRadius: 3,
                                                        font: {
                                                          size: 18,
                                                        }
                                                      },
                                                    }
                                                  }
                                            }
                                        });
                
                
                ctx6 = document.getElementById("myChartSS3").getContext("2d");
                myChartSS3 = new Chart(ctx6, {
                                            type: "pie",
                                            data: {
                                                // labels: ['', 'Credit'],
                                                datasets: [
                                                    {
                                                        label: "CASH / KREDIT",
                                                        backgroundColor: ["#DE3163","#FFBF00"],
                                                        data: response['creditss2']
                                                    }
                                                ]
                                            },
                                            // plugins: [ChartDataLabels],
                                            options: {
                                                responsive: true,
                                                options: {
                                                    plugins: {
                                                      datalabels: {
                                                        display: true,
                                                        align: 'bottom',
                                                        backgroundColor: '#ccc',
                                                        borderRadius: 3,
                                                        font: {
                                                          size: 18,
                                                        }
                                                      },
                                                    }
                                                  }
                                            }
                                        });
                
                //POST RANK
                var html = '';
                    var i;
                    for(i=0; i<response['posrank'].length; i++){
                        html += '<tr align="right">'+
                                '<td align="left">'+response['posrank'][i].rangk+'</td>'+
                                '<td align="left">'+response['posrank'][i].POSName+'</td>'+
                                '<td align="left">'+response['posrank'][i].area+'</td>'+
                                '<td>'+response['posrank'][i].sales+'</td>'+
                                '<td>'+response['posrank'][i].growth+'</td>'+
                                '<td>'+response['posrank'][i].cont+'</td>'+                                
                                '<td>'+response['posrank'][i].salesB+'</td>'+
                                '<td>'+response['posrank'][i].contB+'</td>'+
                                '</tr>';
                    }
                    $('#show_data').html(html);
                    
                //PEFORMANCE REPORT
                var peformance = '';
                var j;
                for(j=0; j<response['peformance'].length; j++){
                        peformance += '<tr align="right">'+
                                '<td align="left">'+response['peformance'][j].Kelompok+'</td>'+
                                '<td align="left">'+response['peformance'][j].Nama+'</td>'+
                                '<td>'+response['peformance'][j].Target+'</td>'+
                                '<td>'+response['peformance'][j].AcvSales+'</td>'+
                                '<td>'+response['peformance'][j].SalesVSTarget+'</td>'+
                                '<td>'+response['peformance'][j].SelisihTarget+'</td>'+
                                '</tr>';
                    }
                $('#peformance').html(peformance);
                
                
                //RANK
                var rank = '';
                var k;
                for(k=0; k<response['rank'].length; k++){
                        rank += '<tr align="right">'+
                                '<td align="left">'+response['rank'][k].act+'</td>'+
                                '<td>'+response['rank'][k].RetailTarget+'</td>'+
                                '<td>'+response['rank'][k].RetailActual+'</td>'+
                                '<td>'+response['rank'][k].RetailAvg+'</td>'+
                                '<td>'+response['rank'][k].RetailACV+'</td>'+
                                '<td>'+response['rank'][k].DealerTarget+'</td>'+
                                '<td>'+response['rank'][k].DealerActual+'</td>'+
                                '<td>'+response['rank'][k].DealerAvg+'</td>'+
                                '<td>'+response['rank'][k].DealerACV+'</td>'+
                                '<td>'+response['rank'][k].StockTarget+'</td>'+
                                '<td>'+response['rank'][k].StockOnHand+'</td>'+
                                '<td>'+response['rank'][k].StockInTransit+'</td>'+
                                '<td>'+response['rank'][k].Day+'</td>'+
                                '</tr>';
                    }
                $('#rank').html(rank);
                
                var rankgroup = '';
                        rankgroup += 
                                '<th scope="col" colspan="6">'+response['kolom1']+'</th>'+
                                '<th scope="col" colspan="7">'+response['kolom2']+'</th>';
                $('#rankgroup').html(rankgroup);
                
                var workday = '';
                        workday += 
                               '<th scope="col" colspan="6"> Retail Sales, Distribution & Stock Progress(MTD) </th>'+
                                '<th scope="col" colspan="7">'+response['kolom']+'</th>';
                $('#workday').html(workday);
                
                //AGING PIUTANG REPORT
                var agingpiutang = '';
                var l;
                for(l=0; l<response['agingpiutang'].length; l++){
                    
                    var a =response['agingpiutang'][l].Leasing ;
                    var b = response['agingpiutang'][l].Amount.split('.00');	
                    var	number_string = b.toString(),
                        sisa 	= number_string.length % 3,
                        rupiah 	= number_string.substr(0, sisa),
                        ribuan 	= number_string.substr(sisa).match(/\d{3}/g);                            
                    if (ribuan) {
                        separator = sisa ? '.' : '';
                        rupiah += separator + ribuan.join('.');
                    }
                    
                    var c = response['agingpiutang'][l].A2999.split('.00');	
                    var	cnumber_string = c.toString(),
                        csisa 	= cnumber_string.length % 3,
                        crupiah 	= cnumber_string.substr(0, csisa),
                        cribuan 	= cnumber_string.substr(csisa).match(/\d{3}/g);                            
                    if (cribuan) {
                        cseparator = csisa ? '.' : '';
                        crupiah += cseparator + cribuan.join('.');
                    }
                    
                    var d = response['agingpiutang'][l].A2228.split('.00');	
                    var	dnumber_string = d.toString(),
                        dsisa 	    = dnumber_string.length % 3,
                        drupiah 	= dnumber_string.substr(0, dsisa),
                        dribuan 	= dnumber_string.substr(dsisa).match(/\d{3}/g);                            
                    if (dribuan) {
                        dseparator = dsisa ? '.' : '';
                        drupiah += dseparator + dribuan.join('.');
                    }
                    
                    var e = response['agingpiutang'][l].A1521.split('.00');	
                    var	enumber_string = e.toString(),
                        esisa 	    = enumber_string.length % 3,
                        erupiah 	= enumber_string.substr(0, esisa),
                        eribuan 	= enumber_string.substr(esisa).match(/\d{3}/g);                            
                    if (eribuan) {
                        eseparator = esisa ? '.' : '';
                        erupiah += eseparator + eribuan.join('.');
                    }
                    
                    var f = response['agingpiutang'][l].A0814.split('.00');	
                    var	fnumber_string = f.toString(),
                        fsisa 	    = fnumber_string.length % 3,
                        frupiah 	= fnumber_string.substr(0, fsisa),
                        fribuan 	= fnumber_string.substr(fsisa).match(/\d{3}/g);                            
                    if (fribuan) {
                        fseparator = fsisa ? '.' : '';
                        frupiah += fseparator + fribuan.join('.');
                    }
                    
                    var g = response['agingpiutang'][l].A0607.split('.00');	
                    var	gnumber_string = g.toString(),
                        gsisa 	    = gnumber_string.length % 3,
                        grupiah 	= gnumber_string.substr(0, gsisa),
                        gribuan 	= gnumber_string.substr(gsisa).match(/\d{3}/g);                            
                    if (gribuan) {
                        gseparator = gsisa ? '.' : '';
                        grupiah += gseparator + gribuan.join('.');
                    }
                    
                    var h = response['agingpiutang'][l].A0305.split('.00');	
                    var	hnumber_string = h.toString(),
                        hsisa 	    = hnumber_string.length % 3,
                        hrupiah 	= hnumber_string.substr(0, hsisa),
                        hribuan 	= hnumber_string.substr(hsisa).match(/\d{3}/g);                            
                    if (hribuan) {
                        hseparator = hsisa ? '.' : '';
                        hrupiah += hseparator + hribuan.join('.');
                    }
                    
                    var i = response['agingpiutang'][l].A0102.split('.00');	
                    var	inumber_string = i.toString(),
                        isisa 	    = inumber_string.length % 3,
                        irupiah 	= inumber_string.substr(0, isisa),
                        iribuan 	= inumber_string.substr(isisa).match(/\d{3}/g);                            
                    if (iribuan) {
                        iseparator = isisa ? '.' : '';
                        irupiah += iseparator + iribuan.join('.');
                    }
                    
                    var j = response['agingpiutang'][l].R2999.split('.00');	
                    var	jnumber_string = j.toString(),
                        jsisa 	    = jnumber_string.length % 3,
                        jrupiah 	= jnumber_string.substr(0, jsisa),
                        jribuan 	= jnumber_string.substr(jsisa).match(/\d{3}/g);                            
                    if (jribuan) {
                        jseparator = jsisa ? '.' : '';
                        jrupiah += jseparator + jribuan.join('.');
                    }
                    
                    var k = response['agingpiutang'][l].R2228.split('.00');	
                    var	knumber_string = k.toString(),
                        ksisa 	    = knumber_string.length % 3,
                        krupiah 	= knumber_string.substr(0, ksisa),
                        kribuan 	= knumber_string.substr(ksisa).match(/\d{3}/g);                            
                    if (kribuan) {
                        kseparator = ksisa ? '.' : '';
                        krupiah += kseparator + kribuan.join('.');
                    }
                    
                    var q = response['agingpiutang'][l].R1521.split('.00');	
                    var	qnumber_string = q.toString(),
                        qsisa 	    = qnumber_string.length % 3,
                        qrupiah 	= qnumber_string.substr(0, qsisa),
                        qribuan 	= qnumber_string.substr(qsisa).match(/\d{3}/g);                            
                    if (qribuan) {
                        qseparator = qsisa ? '.' : '';
                        qrupiah += qseparator + qribuan.join('.');
                    }
                    
                    var m = response['agingpiutang'][l].R0814.split('.00');	
                    var	mnumber_string = m.toString(),
                        msisa 	    = mnumber_string.length % 3,
                        mrupiah 	= mnumber_string.substr(0, msisa),
                        mribuan 	= mnumber_string.substr(msisa).match(/\d{3}/g);                            
                    if (mribuan) {
                        mseparator = msisa ? '.' : '';
                        mrupiah += mseparator + mribuan.join('.');
                    }
                    
                    var n = response['agingpiutang'][l].R0607.split('.00');	
                    var	nnumber_string = n.toString(),
                        nsisa 	    = nnumber_string.length % 3,
                        nrupiah 	= nnumber_string.substr(0, nsisa),
                        nribuan 	= nnumber_string.substr(nsisa).match(/\d{3}/g);                            
                    if (nribuan) {
                        nseparator = nsisa ? '.' : '';
                        nrupiah += nseparator + nribuan.join('.');
                    }
                    
                    var o = response['agingpiutang'][l].R0305.split('.00');	
                    var	onumber_string = o.toString(),
                        osisa 	    = onumber_string.length % 3,
                        orupiah 	= onumber_string.substr(0, osisa),
                        oribuan 	= onumber_string.substr(osisa).match(/\d{3}/g);                            
                    if (oribuan) {
                        oseparator = osisa ? '.' : '';
                        orupiah += oseparator + oribuan.join('.');
                    }
                    
                    var p = response['agingpiutang'][l].R0102.split('.00');	
                    var	pnumber_string = p.toString(),
                        psisa 	    = pnumber_string.length % 3,
                        prupiah 	= pnumber_string.substr(0, psisa),
                        pribuan 	= pnumber_string.substr(psisa).match(/\d{3}/g);                            
                    if (pribuan) {
                        pseparator = psisa ? '.' : '';
                        prupiah += pseparator + pribuan.join('.');
                    }
                    
                    
                        agingpiutang += '<tr align="right">'+
                                '<td align="left">'+a+'</td>'+
                                '<td>'+rupiah+'</td>'+
                                '<td>'+response['agingpiutang'][l].Quantity+'</td>'+
                                '<td>'+prupiah+'</td>'+
                                '<td>'+orupiah+'</td>'+
                                '<td>'+nrupiah+'</td>'+
                                '<td>'+mrupiah+'</td>'+
                                '<td>'+qrupiah+'</td>'+
                                '<td>'+krupiah+'</td>'+
                                '<td>'+jrupiah+'</td>'+
                                '<td>'+irupiah+'</td>'+
                                '<td>'+hrupiah+'</td>'+
                                '<td>'+grupiah+'</td>'+
                                '<td>'+frupiah+'</td>'+
                                '<td>'+erupiah+'</td>'+
                                '<td>'+drupiah+'</td>'+
                                '<td>'+crupiah+'</td>'+
                                '</tr>';
                    }
                $('#agingpiutang').html(agingpiutang);
                          
                //SALES, DISCOUNT AND PROFIT
//                var html = '';
//                    var i;
//                    for(i=0; i<response['sdp'].length; i++){
//                        var mb = response['sdp'][i].MB.split('.00');	
//                        var	mbnumber_string = mb.toString(),
//                            mbsisa 	    = mbnumber_string.length % 3,
//                            mbrupiah 	= mbnumber_string.substr(0, mbsisa),
//                            mbribuan 	= mbnumber_string.substr(mbsisa).match(/\d{3}/g);                            
//                        if (mbribuan) {
//                            mbseparator = mbsisa ? '.' : '';
//                            mbrupiah += mbseparator + mbribuan.join('.');
//                        }
//                        var mn = response['sdp'][i].MN.split('.00');	
//                        var	mnnumber_string = mn.toString(),
//                            mnsisa 	    = mnnumber_string.length % 3,
//                            mnrupiah 	= mnnumber_string.substr(0, mnsisa),
//                            mnribuan 	= mnnumber_string.substr(mnsisa).match(/\d{3}/g);                            
//                        if (mnribuan) {
//                            mnseparator = mnsisa ? '.' : '';
//                            mnrupiah += mnseparator + mnribuan.join('.');
//                        }
//                    }
//                    for(i=0; i<response['sdp'].length; i++){
//                        html += '<tr align="right">'+
//                                '<td align="left">'+response['sdp'][i].Keterangan+'</td>'+
//                                '<td>'+mbrupiah+'</td>'+
//                                '<td>'+mnrupiah+'</td>'+
//                                '<td>'+response['sdp'][i].Growth+'</td>'+
//                                '</tr>';
//                    }
//                    $('#show_datasdp').html(html);
                    
                 //PIUTANG LEASING
//                var html = '';
//                    var i;
//                    for(i=0; i<response['pl'].length; i++){
//                        var mbpl = response['pl'][i].MB.split('.00');	
//                        var	mbplnumbpler_string = mbpl.toString(),
//                            mbplsisa 	    = mbplnumbpler_string.length % 3,
//                            mbplrupiah 	= mbplnumbpler_string.substr(0, mbplsisa),
//                            mbplribuan 	= mbplnumbpler_string.substr(mbplsisa).match(/\d{3}/g);                            
//                        if (mbplribuan) {
//                            mbplseparator = mbplsisa ? '.' : '';
//                            mbplrupiah += mbplseparator + mbplribuan.join('.');
//                        }
//                        var mnpl = response['pl'][i].MN.split('.00');	
//                        var	mnplnumbpler_string = mnpl.toString(),
//                            mnplsisa 	    = mnplnumbpler_string.length % 3,
//                            mnplrupiah 	= mnplnumbpler_string.substr(0, mnplsisa),
//                            mnplribuan 	= mnplnumbpler_string.substr(mnplsisa).match(/\d{3}/g);                            
//                        if (mnplribuan) {
//                            mnplseparator = mnplsisa ? '.' : '';
//                            mnplrupiah += mnplseparator + mnplribuan.join('.');
//                        }
//                    }
//                    for(i=0; i<response['pl'].length; i++){
//                        html += '<tr align="right">'+
//                                '<td align="left">'+response['pl'][i].Keterangan+'</td>'+
//                                '<td>'+mbplrupiah+'</td>'+
//                                '<td>'+mnplrupiah+'</td>'+
//                                '<td>'+response['pl'][i].Growth+'</td>'+
//                                '</tr>';
//                    }
//                    $('#show_datapl').html(html);
                
                
                $("#myModal").modal('hide');
            }
            
            })       
    }
    
    $('#tgl1').change(function(){
        reloadGrid();         
    });
    $('#tgl2').change(function(){
        reloadGrid();         
    });    
    
    $('#TglKu').change(function(){
        reloadGrid();             
    });
    
   $(document).ready(function() {        
       reloadGrid();  
   });
   

JS
);


?>
    <!--<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>-->
    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.0/dist/chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary" id="form-dash">
            <?= Html::beginForm(); ?>
            <div class="box-body">
                <div id="myModal" class="modal fade" style="padding-top: 200px">
                    <div class="text-center"><i class="fa fa-spin fa-spinner"
                                                style="font-size: 100px;color: #77d5f7"></i>
                    </div>
                </div>
                <form id="tgl_query">
                    <div class="box-body">
                        <div class="col-md-24">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="col-sm-24">
                                        <label class="control-label col-sm-4">Tanggal</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <? try {
                                            echo DatePicker::widget([
                                                'name' => 'tgl1',
                                                'id' => 'tgl1',
                                                'type' => DatePicker::TYPE_INPUT,
                                                'value' => Yii::$app->formatter->asDate('now', '01/MM/yyyy'),
                                                'pluginOptions' => [
                                                    'autoclose' => true,
                                                    'format' => 'dd/mm/yyyy',
                                                    'todayHighlight' => true
                                                ],
                                            ]);
                                        } catch (\yii\base\InvalidConfigException $e) {
                                        } ?>
                                    </div>
                                    <div class="col-md-12">
                                        <? try {
                                            echo DatePicker::widget([
                                                'name' => 'tgl2',
                                                'id' => 'tgl2',
                                                'type' => DatePicker::TYPE_INPUT,
                                                'value' => date('t/m/Y', strtotime("today")),
                                                'pluginOptions' => [
                                                    'autoclose' => true,
                                                    'format' => 'dd/mm/yyyy',
                                                ],

                                            ]);
                                        } catch (\yii\base\InvalidConfigException $e) {
                                        } ?>
                                    </div>
                                </div>
                            </div>
                            <label class="control-label col-sm-1">Bulan</label>
                            <div class="col-md-3">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'TglKu',
                                        'id' => 'TglKu',
                                        'attribute2' => 'to_date',
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => Yii::$app->formatter->asDate('now', 'MMMM  yyyy'),
                                        'pluginOptions' => [
                                            'startView' => 'year',
                                            'minViewMode' => 'months',
                                            'format' => 'MM  yyyy'
                                        ],
                                        'pluginEvents' => [
                                            "change" => new JsExpression("
                                                function(e) {
                                                 let d = new Date(Date.parse('01 ' + $(this).val()));											 
                                                 var tgl1 = $('#tgl1');
                                                 var tgl2 = $('#tgl2');                                               
                                                  tgl1.kvDatepicker('update', d);
                                                  tgl2.kvDatepicker('update',new Date(d.getFullYear(), d.getMonth()+1, 0)); 
                                                }                                                                                            
                                                "),
                                        ]
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                            <div class="col-sm-6">
                                <? try {
                                    echo CheckboxX::widget([
                                        'name' => 'eom',
                                        'value' => true,
                                        'options' => ['id' => 'eom'],
                                        'pluginOptions' => ['threeState' => false, 'size' => 'lg'],
                                        'pluginEvents' => [
                                            "change" => new JsExpression("function(e) {
                                                 var today = new Date();
                                                 var tgl2 = $('#tgl2');
                                                 let tglku = tgl2.kvDatepicker('getDate');
                                                 var lastDayOfMonth = new Date(tglku.getFullYear(), tglku.getMonth()+1, 0);
                                                if($(this).val() == 1){
                                                    tgl2.kvDatepicker('update', lastDayOfMonth)
                                                 }else{
                                                    tgl2.kvDatepicker('update', new Date(today.getFullYear(), today.getMonth(), today.getDate()));
                                                 }
                                                }")
                                        ]
                                    ]);
                                    echo '<label class="cbx-label" for="eom">EOM</label>';
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                            <div class="col-md-24"><br><br></div>
                        </div>
                    </div>
                </form>

                <div class="row">
                    <div class="col-md-24">
                        <div class="col-md-12">
                            <div class="box box-warning direct-chat direct-chat-warning">

                                <div class="box-header with-border">
                                    <div class="col-md-22"><h3 class="box-title">DAILY DISTRIBUTION TREND</h3></div>
                                    <div class="col-md-2">
                                        <button class="btn" id="ddt" type="button" formtarget="_blank "><i
                                                class="fa fa-file-text"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div>
                                        <div class="2">
                                            <canvas id="myChartDDT"></canvas>
                                        </div>
                                    </div>
                                    <script>
                                    </script>
                                </div>
                                <div class="box-footer"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box box-warning direct-chat direct-chat-warning">
                                <div class="box-header with-border">
                                    <div class="col-md-22"><h3 class="box-title">DAILY RETAIL SALES TREND</h3></div>
                                    <div class="col-md-2">
                                        <button class="btn" id="drst" type="button" formtarget="_blank "><i
                                                class="fa fa-file-text"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div>
                                        <div class="3">
                                            <canvas id="myChartDRST"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer"></div>
                                <!-- /.box-footer-->
                            </div>
                            <!--/.direct-chat -->
                        </div>
                    </div>

                    <div class="col-md-24">
                        <div class="col-md-12">
                            <div class="box box-warning direct-chat direct-chat-warning">
                                <div class="box-header with-border">
                                    <h3 class="box-title">SALES BY SEGMEN</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn" id="sbg" type="button" formtarget="_blank "><i
                                                class="fa fa-file-text"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="4">
                                        <canvas id="myChartSBS"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer"></div>
                        </div>
                        <div class="col-md-12">
                            <div class="box box-warning direct-chat direct-chat-warning">
                                <div class="box-header with-border">
                                    <h3 class="box-title">SALES STRUCTURE</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn" id="ss" type="button" formtarget="_blank "><i
                                                    class="fa fa-file-text"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="row col-md-24">
                                        <div class="col-md-5" style="margin-top: 50px">
                                            <div class="row column-md-12"><div class="col-md-2" style="width: 42px;height: 13px;background-color: #DE3163;margin-left: 30px"></div>&nbsp CASH</div><br>
                                            <div class="5" style="width:8vw;height: 8vw">
                                                <canvas id="myChartSS2"></canvas>
                                            </div>
                                            <br><div class="row column-md-12"><div class="col-md-2" style="width: 42px;height: 13px;background-color: white;margin-left: 20px;"></div><b>M</b></div><br>
                                        </div>
                                        <div class="col-md-6" style="margin-top: 50px">
                                            <div class="row column-md-12"><div class="col-md-2" style="width: 42px;height: 13px;background-color: #FFBF00;margin-left: 30px"></div>&nbsp KREDIT</div><br>
                                            <div class="6" style="width:8vw;height: 8vw;">
                                                <canvas id="myChartSS3"></canvas>
                                            </div>
                                            <br><div class="row column-md-12"><div class="col-md-2" style="width: 42px;height: 13px;background-color: white;margin-left: 20px;"></div><b>M-1</b></div><br>
                                        </div>
                                        <div class="col-md-13">
                                            <div class="1" style="height: 20vw;">
                                                <canvas id="myChartSS"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-24">
                        <div class="col-md-12">
                            <div class="box box-warning direct-chat direct-chat-warning">
                                <div class="box-header with-border">
                                    <h3 class="box-title">POS Rank</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn" id="excelpr" type="button" formtarget="_blank "><i
                                                    class="fas fa-download"></i></button>

                                        <button class="btn" id="pr" type="button" formtarget="_blank "><i
                                                    class="fa fa-file-text"></i></button>

                                    </div>
                                </div>
                                <div class="box-body">
                                    <div style="position: relative;height: 300px;overflow: auto;display: block;">
                                        <table class="table table-bordered table-striped mb-0" id="table3">
                                            <thead>
                                            <tr>
                                                <th scope="col">Rank</th>
                                                <th scope="col">Post Name</th>
                                                <th scope="col">Area</th>
                                                <th scope="col">Sales (MTD)</th>
                                                <th scope="col">Growth (MTD)</th>
                                                <th scope="col">Cont (MTD)</th>
                                                <th scope="col">Sales (M-1)</th>
                                                <th scope="col">Cont (M-1)</th>
                                            </tr>
                                            </thead>
                                            <tbody id="show_data"></tbody>
                                        </table>

                                    </div>


                                </div>
                                <div class="box-footer"></div>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="box box-warning direct-chat direct-chat-warning">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Sales VS Target</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn" id="excelst" type="button" formtarget="_blank "><i
                                                    class="fas fa-download"></i></button>
                                        <button class="btn" id="ts" type="button" formtarget="_blank "><i
                                                    class="fa fa-file-text"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div style="position: relative;height: 300px;overflow: auto;display: block;">
                                        <table class="table table-bordered table-striped mb-0" id="table2">
                                            <thead>
                                            <tr>
                                                <th scope="col" style="text-align: center">Kelompok</th>
                                                <th scope="col" style="text-align: center">Nama</th>
                                                <th scope="col" style="text-align: center">Target</th>
                                                <th scope="col" style="text-align: center">Acv Sales</th>
                                                <th scope="col" style="text-align: center">Sales VS Target</th>
                                                <th scope="col" style="text-align: center">Selisih Target</th>
                                            </tr>
                                            </thead>
                                            <tbody id="peformance"></tbody>
                                        </table>

                                    </div>
                                </div>
                                <div class="box-footer"></div>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-24">
                        <div class="col-md-24">
                            <div class="box box-warning direct-chat direct-chat-warning">
                                <div class="box-header with-border">
                                    <h3 class="box-title">SUMMARY SADISTOCK</h3>
                                    <div class="pull-right">
                                        <button class="btn" id="excelss" type="button" formtarget="_blank "><i
                                                    class="fas fa-download"></i></button>
                                        <button class="btn" id="rk" type="button" formtarget="_blank "><i
                                                class="fa fa-file-text"></i></button>


                                    </div>
                                </div>
                                <div class="box-body">
                                    <div style="position: relative;height: 200px;overflow: auto;display: block;">
                                        <table class="table table-bordered table-striped mb-0" id="table1">
                                            <thead>
                                            <tr id="rankgroup"></tr>
                                            <tr id="workday"></tr>
                                            <tr>
                                                <th scope="col" rowspan="2">Act</th>
                                                <th scope="col" colspan="4">Retail Sales</th>
                                                <th scope="col" colspan="4">Distribution</th>
                                                <th scope="col" colspan="4">Dealer Stock</th>
                                            </tr>
                                            <tr>
                                                <th scope="col">Target</th>
                                                <th scope="col">Actual</th>
                                                <th scope="col">Avg</th>
                                                <th scope="col">% ACV</th>
                                                <th scope="col">Target</th>
                                                <th scope="col">Actual</th>
                                                <th scope="col">Avg</th>
                                                <th scope="col">% ACV</th>
                                                <th scope="col">Target</th>
                                                <th scope="col">On Hand</th>
                                                <th scope="col">Intrnasit</th>
                                                <th scope="col">Day</th>
                                            </tr>
                                            </thead>
                                            <tbody id="rank"></tbody>
                                        </table>

                                    </div>


                                </div>
                                <div class="box-footer"></div>
                            </div>

                        </div>
                    </div>

<!--                    <div class="col-md-24">-->
<!--                        <div class="col-md-12">-->
<!--                            <div class="box box-warning direct-chat direct-chat-warning">-->
<!--                                <div class="box-header with-border">-->
<!--                                    <h3 class="box-title"> SALES, DISCOUNT AND PROFIT</h3>-->
<!--                                    <div class="box-tools pull-right">-->
<!--                                        <button class="btn" id="sdp" type="button" formtarget="_blank "><i-->
<!--                                                    class="fa fa-file-text"></i></button>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="box-body">-->
<!--                                    <div style="position: relative;height: 300px;overflow: auto;display: block;">-->
<!--                                        <table class="table table-bordered table-striped mb-0">-->
<!--                                            <thead>-->
<!--                                            <tr>-->
<!--                                                <th scope="col">Keterangan</th>-->
<!--                                                <th scope="col">M-1</th>-->
<!--                                                <th scope="col">M</th>-->
<!--                                                <th scope="col">Growth</th>-->
<!--                                            </tr>-->
<!--                                            </thead>-->
<!--                                            <tbody id="show_datasdp"></tbody>-->
<!--                                            <tbody ></tbody>-->
<!--                                        </table>-->
<!---->
<!--                                    </div>-->
<!---->
<!---->
<!--                                </div>-->
<!--                            </div>-->
<!---->
<!--                        </div>-->
<!--                        <div class="col-md-12">-->
<!--                            <div class="box box-warning direct-chat direct-chat-warning">-->
<!--                                <div class="box-header with-border">-->
<!--                                    <h3 class="box-title"> PIUTANG LEASING </h3>-->
<!--                                    <div class="box-tools pull-right">-->
<!--                                        <button class="btn" id="pl" type="button" formtarget="_blank "><i-->
<!--                                                    class="fa fa-file-text"></i></button>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="box-body">-->
<!--                                    <div style="position: relative;height: 300px;overflow: auto;display: block;">-->
<!--                                        <table class="table table-bordered table-striped mb-0" >-->
<!--                                            <thead>-->
<!--                                            <tr>-->
<!--                                                <th scope="col">Nama Leasing</th>-->
<!--                                                <th scope="col">M-1</th>-->
<!--                                                <th scope="col">M</th>-->
<!--                                                <th scope="col">Growth</th>-->
<!--                                            </tr>-->
<!--                                            </thead>-->
<!--                                            <tbody id="show_datapl"></tbody>-->
<!--                                            <tbody ></tbody>-->
<!--                                        </table>-->
<!---->
<!--                                    </div>-->
<!---->
<!---->
<!--                                </div>-->
<!--                            </div>-->
<!---->
<!--                        </div>-->
<!---->
<!--                    </div>-->
<!---->
<!--                    <div class="col-md-24" style="display:none">-->
<!--                        <div class="col-md-12">-->
<!--                            <div class="box box-warning direct-chat direct-chat-warning">-->
<!--                                <div class="box-header with-border">-->
<!--                                    <h3 class="box-title">Aging Piutang</h3>-->
<!--                                    <div class="box-tools pull-right">-->
<!--                                        <button class="btn" id="aging" type="button" formtarget="_blank "><i-->
<!--                                                class="fa fa-file-text"></i></button>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="box-body">-->
<!--                                    <div style="position: relative;height: 300px;overflow: auto;display: block;">-->
<!--                                        <table class="table table-bordered table-striped mb-0">-->
<!--                                            <thead>-->
<!--                                            <tr>-->
<!--                                                <th scope="col" style="text-align: center">Leasing</th>-->
<!--                                                <th scope="col" style="text-align: center">Amount</th>-->
<!--                                                <th scope="col" style="text-align: center">Quantity</th>-->
<!--                                                <th scope="col" style="text-align: center">R0102</th>-->
<!--                                                <th scope="col" style="text-align: center">R0305</th>-->
<!--                                                <th scope="col" style="text-align: center">R0607</th>-->
<!--                                                <th scope="col" style="text-align: center">R0814</th>-->
<!--                                                <th scope="col" style="text-align: center">R1521</th>-->
<!--                                                <th scope="col" style="text-align: center">R2228</th>-->
<!--                                                <th scope="col" style="text-align: center">R2999</th>-->
<!--                                                <th scope="col" style="text-align: center">A0102</th>-->
<!--                                                <th scope="col" style="text-align: center">A0305</th>-->
<!--                                                <th scope="col" style="text-align: center">A0607</th>-->
<!--                                                <th scope="col" style="text-align: center">A0814</th>-->
<!--                                                <th scope="col" style="text-align: center">A1521</th>-->
<!--                                                <th scope="col" style="text-align: center">A2228</th>-->
<!--                                                <th scope="col" style="text-align: center">A2999</th>-->
<!--                                            </tr>-->
<!--                                            </thead>-->
<!--                                            <tbody id="agingpiutang"></tbody>-->
<!--                                        </table>-->
<!---->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="box-footer"></div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
                </div>

                <?= Html::endForm(); ?>
            </div>
        </div>
<?php
$urlddt = Url::toRoute(['report/dashboard-show', 'param' => '"Sadistock"']);
$urldrst = Url::toRoute(['report/dashboard-show-penjualan', 'param' => '"PenjualanKecamatan"']);
$urlsbg = Url::toRoute(['report/dashboard-show-segmen', 'param' => '"PenjualanSegmenBulanan"']);
$urlpr = Url::toRoute(['report/dashboard-show-pos', 'param' => '"PenjualanSales"']);
//$urlss = Url::toRoute(['report/dashboard-show-cash-credit', 'param' => '"PenjualanKecamatanUnit"']);
$urlss = Url::toRoute(['report/dashboard-show-cash-credit', 'param' => '"SalesStructureUnit"']);
$urlts = Url::toRoute(['report/dashboard-show-target-sales', 'param' => '"PenjualanDealer"']);
//$urlrank = Url::toRoute(['report/dashboard-show-rank', 'param' => '"PenjualanHarian"']);
$urlrank = Url::toRoute(['report/dashboard-show-rank', 'param' => '"DistributionGrowth"']);
//$urlaging = Url::toRoute(['report/dashboard-show-aging', 'param' => '"ProfitTahun"']);
$urlaging = Url::toRoute(['report/dashboard-show-aging', 'param' => '"PeoplePenjualan"']);
$urlsdp = Url::toRoute(['report/dashboard-show-aging', 'param' => '"ProfitTahun"']);
$urlpl = Url::toRoute(['report/dashboard-show-pl', 'param' => '"AgingPiutang"']);

$this->registerJs(<<< JS
    
        $('#ddt').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
           // window.open('{$urlddt}'+ ',"' + tgl1 + '","' + tgl2 + '","1"', '_blank');
            window.open('{$urlrank}'+ ',"' + tgl1 + '","' + tgl2 + '","1"', '_blank');
        });
        $('#drst').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
//            window.open('{$urldrst}'+ ',"' + tgl1 + '","' + tgl2 + '","1"', '_blank');
            window.open('{$urlpr}'+ ',"' + tgl1 + '","' + tgl2 + '","1"', '_blank');
        });
        $('#sbg').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            window.open('{$urlsbg}'+ ',"' + tgl1 + '","' + tgl2 + '","1"', '_blank');
        });
        $('#pr').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
//            window.open('{$urlpr}'+ ',"' + tgl1 + '","' + tgl2 + '","1"', '_blank');
            window.open('{$urldrst}'+ ',"' + tgl1 + '","' + tgl2 + '","1"', '_blank');
        });
        $('#ss').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            window.open('{$urlss}'+ ',"' + tgl1 + '","' + tgl2 + '","1"', '_blank');
        });
        $('#ts').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            // window.open('{$urlts}'+ ',"' + tgl1 + '","' + tgl2 + '","1"', '_blank');
            window.open('{$urlaging}'+ ',"' + tgl1 + '","' + tgl2 + '","1"', '_blank');
        });
        $('#rk').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
//           window.open('{$urlrank}'+ ',"' + tgl1 + '","' + tgl2 + '","1"', '_blank');
             window.open('{$urlddt}'+ ',"' + tgl1 + '","' + tgl2 + '","1"', '_blank');
        });
        $('#aging').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            window.open('{$urlaging}'+ ',"' + tgl1 + '","' + tgl2 + '","1"', '_blank');
        });
        
        $('#sdp').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            window.open('{$urlsdp}'+ ',"' + tgl1 + '","' + tgl2 + '","1"', '_blank');
        });
        $('#pl').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            window.open('{$urlpl}'+ ',"' + tgl1 + '","' + tgl2 + '","1"', '_blank');
        });
        
        
        
        $(document).ready(function () {
      $("#excelpr").click(function(){
        TableToExcel.convert(document.getElementById("table3"), {
            name: "POS Rank.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
      
      
      $("#excelst").click(function(){
        TableToExcel.convert(document.getElementById("table2"), {
            name: "Sales VS Target.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
      
      $("#excelss").click(function(){
        TableToExcel.convert(document.getElementById("table1"), {
            name: "SUMMARY SADISTOCK.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
  });

JS
);







