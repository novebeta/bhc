<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use kartik\checkbox\CheckboxX;
use yii\web\JsExpression;

\aunit\assets\AppAsset::register($this);
\aunit\assets\JqwidgetsAsset::register($this);
$this->title = 'PROFIT';
$formatter = \Yii::$app->formatter;
$this->registerCss(".jqx-widget-content { font-size: 12px; }");

$rowdata = $data;
$this->registerJs(<<< JS

 
JS
);

?>
    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.0/dist/chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
    <style>
        .table td{
            border: lightgrey solid 1px !important;
        }
        .table th{
            border: lightgrey solid 1px !important;
        }
    </style>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<?= Html::beginForm(); ?>
    <div class="box box-default" style="padding-left: 0;">
        <button class="btn pull-right" id='btnExcel' type="button" style="background-color:limegreen;width: 40px;height: 30px;margin: 10px;position: absolute;top: -45px;right: 10px"><i class="fa fa-file-excel-o fa-lg"></i></button>
        <div class="box-body" style="padding-left: 0;">
            <h3 class="box-title" style="text-align: center">PROFIT</h3>
            <div class="col-md-24">
                <div style="position: relative;height: 400px;overflow: auto;">
                    <table class="table table-bordered table-striped mb-0" id="table1">
                        <thead>
                        <tr>
                            <th scope="col">TAHUN</th>
                            <th scope="col">JAN</th>
                            <th scope="col">FEB</th>
                            <th scope="col">MAR</th>
                            <th scope="col">APR</th>
                            <th scope="col">MEI</th>
                            <th scope="col">JUN</th>
                            <th scope="col">JUL</th>
                            <th scope="col">AGS</th>
                            <th scope="col">SEP</th>
                            <th scope="col">OKT</th>
                            <th scope="col">NOV</th>
                            <th scope="col">DES</th>
                            <th scope="col">TOTAL</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($rowdata['Tahun'] as $key) {

                            echo '<tr><td>' . $key['Tahun'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Jan'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Feb'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Mar'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Apr'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Mei'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Jun'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Jul'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Agu'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Sep'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Okt'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Nov'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Des'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Total'] . '</td>';

                        }

                        ?>


                        </tbody>
                    </table>
                    <br><br><br>
                    <table class="table table-bordered table-striped mb-0">
                        <thead>
                        <tr>
                            <th scope="col">vs TARGET</th>
                            <th scope="col">JAN</th>
                            <th scope="col">FEB</th>
                            <th scope="col">MAR</th>
                            <th scope="col">APR</th>
                            <th scope="col">MEI</th>
                            <th scope="col">JUN</th>
                            <th scope="col">JUL</th>
                            <th scope="col">AGS</th>
                            <th scope="col">SEP</th>
                            <th scope="col">OKT</th>
                            <th scope="col">NOV</th>
                            <th scope="col">DES</th>
                            <th scope="col">TOTAL</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($rowdata['Target'] as $key) {
                            echo '<tr><td>' . $key['Tahun'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Jan'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Feb'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Mar'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Apr'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Mei'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Jun'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Jul'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Agu'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Sep'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Okt'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Nov'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Des'] . '</td>';
                            echo '<td style="text-align: right">' . $key['Total'] . '</td>';

                        }

                        ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    <div class="box box-default">
        <div class="box-body">
            <div class="row col-md-24" style="width: 1200px;height: 300px">
                <canvas id="myChart"></canvas>
                <?php


                foreach ($rowdata['Tahun'] as $key){
                    if($key['Tahun'] == '2020'){
                        $th = $key;
                    }
                }

                $t1 = $th;

                ?>
                <script>
                    Chart.register(ChartDataLabels);
                    var ctx = document.getElementById('myChart').getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: ['JAN', 'FEB', 'MAR', 'APR', 'MEI', 'JUN', 'JUL', 'AGS', 'SEP', 'OKT', 'NOV', 'DES'],
                            datasets: [
                                {
                                    label: '2020',
                                    data: [420, 190, 290,190,187, 120, 190, 290,190,187, 200,300],
                                    backgroundColor: [
                                        'red',
                                    ],
                                    borderWidth: 1,
                                    fill: false,
                                    borderColor: 'red'
                                },
                                {
                                    label: '2021',
                                    data: [520, 290, 190,590,287, 220, 490, 190,140,117, 100,200],
                                    backgroundColor: [
                                        'blue',
                                    ],
                                    borderWidth: 1,
                                    fill: false,
                                    borderColor: 'blue'
                                },
                                {
                                    label: '2022',
                                    data: [620, 140, 290,490,187, 620, 340, 240,140,147, 320,220],
                                    backgroundColor: [
                                        'green',
                                    ],
                                    borderWidth: 1,
                                    fill: false,
                                    borderColor: 'green'
                                },
                                {
                                    label: 'GM-1',
                                    data: [620, 140, 290,490,187, 620, 340, 240,140,147, 320,220],
                                    backgroundColor: [
                                        'yellow',
                                    ],
                                    borderWidth: 1,
                                    fill: false,
                                    borderColor: 'yellow'
                                },
                                {
                                    label: 'GMTD',
                                    data: [620, 140, 290,490,187, 620, 340, 240,140,147, 320,220],
                                    backgroundColor: [
                                        'magenta',
                                    ],
                                    borderWidth: 1,
                                    fill: false,
                                    borderColor: 'magenta'
                                },

                            ]
                        },
                        options: {
                            responsive: true,
                            maintainAspectRatio: false,
                            legend: {
                                position: "top"
                            },
                            title: {
                                display: true,
                                text: "Chart.js Bar Chart"
                            },
                            scales: {
                                x: {
                                    stacked: true,
                                },
                                y: {
                                    stacked: true
                                }
                            }
                        }
                    });
                </script>
            </div>
        </div>
    </div>
<?= Html::endForm(); ?>
<?php
$this->registerJs(<<< JS

 $(document).ready(function () {
      $("#btnExcel").click(function(){
        TableToExcel.convert(document.getElementById("table1"), {
            name: "Profit.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
  });

JS
);
