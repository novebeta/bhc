<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\checkbox\CheckboxX;
use kartik\number\NumberControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
$this->title                   = 'Laporan Piutang';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;

$sqlBank                           = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, '' AS NoParent, '' AS StatusAccount, '' AS NoAccount
	UNION
	SELECT NoAccount as value, NamaAccount as label, NoParent, StatusAccount, NoAccount FROM traccount 
	WHERE JenisAccount = 'Detail' AND NOAccount BETWEEN '11060000' AND '11150000' 
    ORDER BY NoAccount" )
	->queryAll();
$cmbBank                           = ArrayHelper::map( $sqlBank, 'value', 'label' );

$sqlLeasing = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, '' AS LeaseStatus, '' AS LeaseKode
	UNION
	SELECT LeaseKode AS value, LeaseKode As label , LeaseStatus , LeaseKode FROM tdLeasing " )
	->queryAll();
$cmbLeasing = ArrayHelper::map( $sqlLeasing, 'value', 'label' );

$sqlKabupaten = Yii::$app->db
	->createCommand( "
	SELECT 'Semua' AS label, '%' AS value,  '' AS AreaStatus, '' AS Kabupaten
	UNION
	SELECT Kabupaten As label, Kabupaten AS value,  AreaStatus, Kabupaten FROM tdarea  
	where AreaStatus IN ('A','1')
	GROUP BY Kabupaten
	ORDER BY AreaStatus, Kabupaten " )
	->queryAll();
$cmbKabupaten = ArrayHelper::map( $sqlKabupaten, 'value', 'label' );

$format = <<< SCRIPT
function formatBank(result) {
    return '<div class="row">' +
           '<div class="col-md-5">' + result.id + '</div>' +
           '<div class="col-md-19">' + result.text + '</div>' +
           '</div>';
}
function formatDual(result) {
    return '<div class="row">' +
           '<div class="col-md-3">' + result.id + '</div>' +
           '<div class="col-md-21">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );

?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
                                    'Saldo Piutang Leasing 1'     => 'Saldo Piutang Leasing 1',
									'Saldo Piutang Leasing 2'     => 'Saldo Piutang Leasing 2',
									'Saldo Piutang Non Leasing'   => 'Saldo Piutang Non Leasing',
									'Mutasi Debet Piutang'        => 'Mutasi Debet Piutang',
									'Mutasi Kredit Piutang'       => 'Mutasi Kredit Piutang',
									'Umur Piutang - Curent'       => 'Umur Piutang - Curent',
									'Umur Piutang - Overdue'      => 'Umur Piutang - Overdue',
									'Riwayat Piutang Leasing'     => 'Riwayat Piutang Leasing',
									'Riwayat Piutang Non Leasing' => 'Riwayat Piutang Non Leasing',
									'Perincian Pelunasan'         => 'Perincian Pelunasan',
									'NoSin Pelunasan'             => 'NoSin Pelunasan',
									'BBN Progresif'               => 'BBN Progresif',
                                    'Belum Di Link'               => 'Belum Di Link',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-footer">
				<div class="container-fluid">
				    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Tanggal</label>
						</div>
                    </div>
					<div class="col-md-14">
						<div class="form-group">
							<div class="col-md-6 tgl1Hide">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format' => 'dd/mm/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
                            <div class="col-md-6 ">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'id'            => 'tgl2',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now', 'dd/MM/yyyy' ),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										],
										'pluginEvents'  => [
											"change" => new JsExpression( "function(e) {	
											 let range = 6;										
											 let d = $(this).kvDatepicker('getDate');	
											 d.setDate(d.getDate() + 1);		
											 $('#dr1a').kvDatepicker('update', d);		
											 d.setDate(d.getDate() + range);		
											 $('#dr1b').kvDatepicker('update', d);	
											 d.setDate(d.getDate() + 1);		
											 $('#dr2a').kvDatepicker('update', d);		
											 d.setDate(d.getDate() + range);		
											 $('#dr2b').kvDatepicker('update', d);
											 d.setDate(d.getDate() + 1);		
											 $('#dr3a').kvDatepicker('update', d);		
											 d.setDate(d.getDate() + range);		
											 $('#dr3b').kvDatepicker('update', d);		
											}" )
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
							<label class="control-label col-sm-4 lunasHide">&nbsp&nbsp&nbspStatus Lunas</label>
                            <div class="col-sm-4 lunasHide">
								<?= Html::dropDownList( 'status', null, [
									"Belum" => "Belum",
									"Sudah" => "Sudah",
									""      => "Semua",
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
                            </div>
							<label class="control-label col-sm-2 leasingHide">Leasing</label>
                            <div class="col-sm-4 leasingHide">
								<? echo Select2::widget( [
									'name'          => 'leasing',
									'data'          => $cmbLeasing,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
								] ); ?>
                            </div>
							<div class="col-sm-4 cbsensitifHide">
								<? try {
									echo CheckboxX::widget([
										'name'=>'sensitif',
										'value'=>0,
										'options'=>['id'=>'sensitif'],
										'pluginOptions'=>['threeState'=>false, 'size'=>'lg']
									]);
									echo '<label class="cbx-label" for="sensitif">Refill</label>';
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
							<label class="control-label col-sm-3 nobuktiHide">No Bukti</label>
                            <div class="col-sm-9 nobuktiHide">
								<input type="text" id="nobukti" name="nobukti" class="form-control"> </input>
							</div>
						</div>
					</div>
					<div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24 lblRangeHide">Range 1</label>
						</div>
                    </div>
                    <div class="col-sm-2 numRangeHide">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? echo NumberControl::widget( [
									'name'               => 'r1a',
									'value'              => 1,
									'maskedInputOptions' => [ 'digits' => 0 ],
								] ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2 numRangeHide">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? echo NumberControl::widget( [
									'name'               => 'r1b',
									'id'                 => 'r1b',
									'value'              => 7,
									'maskedInputOptions' => [ 'digits' => 0 ],
								] ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 RangetglHide">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'dr1a',
										'id'            => 'dr1a',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now', '01/MM/yyyy' ),
										'pluginOptions' => [
											'autoclose'      => true,
											'format'         => 'dd/mm/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 RangetglHide">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'dr1b',
										'id'            => 'dr1b',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now', '01/MM/yyyy' ),
										'pluginOptions' => [
											'autoclose'      => true,
											'format'         => 'dd/mm/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                        </div>
                    </div>
				</div>
				<div class="container-fluid">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Perkiraan 1</label>
                        </div>
                    </div>
                    <div class="col-md-14">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? echo Select2::widget( [
									'name'          => 'perkiraan1',
									'data'          => $cmbBank,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatBank' ),
										'templateSelection' => new JsExpression( 'formatBank' ),
										'matcher'           => new JsExpression( 'matchCustom' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
                            </div>
                        </div>
                    </div>
					<div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24 lblRangeHide">Range 2</label>
						</div>
                    </div>
                    <div class="col-sm-2 numRangeHide">
                        <div class="form-group">
                            <div class="col-sm-24 ">
								<? echo NumberControl::widget( [
									'name'               => 'r2a',
									'id'                 => 'r2a',
									'value'              => 8,
									'maskedInputOptions' => [ 'digits' => 0 ],
								] ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2 numRangeHide">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? echo NumberControl::widget( [
									'name'               => 'r2b',
									'id'                 => 'r2b',
									'value'              => 15,
									'maskedInputOptions' => [ 'digits' => 0 ],
								] ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 RangetglHide">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'dr2a',
										'id'            => 'dr2a',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now', '01/MM/yyyy' ),
										'pluginOptions' => [
											'autoclose'      => true,
											'format'         => 'dd/mm/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 RangetglHide">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'dr2b',
										'id'            => 'dr2b',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now', '01/MM/yyyy' ),
										'pluginOptions' => [
											'autoclose'      => true,
											'format'         => 'dd/mm/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                        </div>
                    </div>
				</div>
                <div class="container-fluid">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Perkiraan 2</label>
                        </div>
                    </div>
                    <div class="col-md-14">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? echo Select2::widget( [
									'name'          => 'perkiraan2',
									'data'          => $cmbBank,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatBank' ),
										'templateSelection' => new JsExpression( 'formatBank' ),
										'matcher'           => new JsExpression( 'matchCustom' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
                            </div>
                        </div>
                    </div>
					<div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24 lblRangeHide">Range 3</label>
						</div>
                    </div>
                    <div class="col-sm-2 numRangeHide">
                        <div class="form-group">
                            <div class="col-sm-24 ">
								<? echo NumberControl::widget( [
									'name'               => 'r3a',
									'id'                 => 'r3a',
									'value'              => 16,
									'maskedInputOptions' => [ 'digits' => 0 ],
								] ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2 numRangeHide">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? echo NumberControl::widget( [
									'name'               => 'r3b',
									'id'                 => 'r3b',
									'value'              => 23,
									'maskedInputOptions' => [ 'digits' => 0 ],
								] ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 RangetglHide">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'dr3a',
										'id'            => 'dr3a',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now', '01/MM/yyyy' ),
										'pluginOptions' => [
											'autoclose'      => true,
											'format'         => 'dd/mm/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 RangetglHide">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'dr3b',
										'id'            => 'dr3b',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now', '01/MM/yyyy' ),
										'pluginOptions' => [
											'autoclose'      => true,
											'format'         => 'dd/mm/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
$this->registerJs( $this->render( 'LaporanPiutang.js' ) );