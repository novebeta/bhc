<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\checkbox\CheckboxX;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\helpers\Url;


$this->title = 'Dashboard Marketing Konsolidasi';
$this->params['breadcrumbs'][] = $this->title;
$formatter = \Yii::$app->formatter;


$format = <<< SCRIPT

SCRIPT;

$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
$urlQueryDDT = Url::toRoute(['report/markonsol1']);
$urlQueryDRST = Url::toRoute(['report/markonsol2']);
$urlQuerySBS = Url::toRoute(['report/markonsol3']);
$urlQuerySS = Url::toRoute(['report/markonsol4']);
$urlQueryPR = Url::toRoute(['report/markonsol5']);
$urlQueryPE = Url::toRoute(['report/markonsol6']);
$urlQuerySK = Url::toRoute(['report/markonsol7']);
$url = Url::toRoute(['report/querycombo']);
$urlcmbkwl = Url::toRoute(['report/querycombokwl']);
$urlcmbmd = Url::toRoute(['report/querycombomd']);
$urlcmbdealer = Url::toRoute(['report/querycombodealer']);
$urlkwl = Url::toRoute(['report/getdatakwl']);

$this->registerJs(<<< JS
    
     Chart.register(ChartDataLabels);

    function reloadGrid1(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
        $('#ajaxspinner1').show();
                
        $.ajax({
            type: 'POST',
            url: '$urlQueryDDT',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            success:function(response) {
               
                    $('#myChartDDT').remove(); 
                    $('.2').append('<canvas id="myChartDDT"><canvas>');    
                //DAILY DISTRIBUTION TREND
                ctx1 = document.getElementById("myChartDDT").getContext("2d");
                myChartDDT = new Chart(ctx1, {
                                            // type: "bar",
                                            data: {
                                                labels: response['labelddt'],
                                                datasets: [
                                                    {
                                                        type: "bar",
                                                        label: "Actual",
                                                        backgroundColor: "orange",
                                                        borderColor: "orange",
                                                        borderWidth: 1,
                                                        data: response['actualddt']
                                                    },
                                                    {
                                                        type: "line",
                                                        label: "Daily Target",
                                                        backgroundColor: "Aquamarine",
                                                        borderColor: "Aquamarine",
                                                        borderWidth: 5,
                                                        data: response['targetddt']
                                                    },
                                                ]
                                            },
                                            plugins: [ChartDataLabels],
                                            options: {
                                                responsive: true,
                                                legend: {
                                                    position: "top"
                                                },
                                                title: {
                                                    display: true,
                                                    text: "Chart.js Bar Chart"
                                                },
                                                scales: {
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }]
                                                },
                                            }
                                        });      
                 $('#ajaxspinner1').hide();
            }
        }
        )
    }
    
    function reloadGrid2(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
        $('#ajaxspinner2').show();
                
        $.ajax({
            type: 'POST',
            url: '$urlQueryDRST',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            success:function(response) {               
                    $('#myChartDRST').remove(); 
                    $('.3').append('<canvas id="myChartDRST"><canvas>');
                //DAILY DISTRIBUTION TREND
                ctx2 = document.getElementById("myChartDRST").getContext("2d");
                myChartDRST = new Chart(ctx2, {
                                            // type: "bar",
                                            data: {
                                                labels: response['labeldrst'],
                                                datasets: [
                                                    {
                                                        type: "bar",
                                                        label: "Actual",                                                        
                                                        backgroundColor: "Gold",
                                                        borderColor: "Gold",
                                                        borderWidth: 1,
                                                        data: response['actualdrst']
                                                    },
                                                    {
                                                        type: "line",
                                                        label: "Daily Target",
                                                        backgroundColor: "Aqua",
                                                        borderColor: "Aqua",
                                                        borderWidth: 5,
                                                        data: response['targetdrst']
                                                    },
                                                ]
                                            },
                                            options: {
                                                responsive: true,
                                                legend: {
                                                    position: "top"
                                                },
                                                title: {
                                                    display: true,
                                                    text: "Chart.js Bar Chart"
                                                },
                                                scales: {
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }]
                                                }
                                            }
                                        });  
                $('#ajaxspinner2').hide();
            }
        }
        )
    }
    
    function reloadGrid3(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
        $('#ajaxspinner3').show();
                
        $.ajax({
            type: 'POST',
            url: '$urlQuerySBS',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            success:function(response) {               
                    $('#myChartSBS').remove(); 
                    $('.4').append('<canvas id="myChartSBS"><canvas>');
                    $('#myChartSBS2').remove(); 
                    $('.6').append('<canvas id="myChartSBS2"><canvas>');
                //DAILY DISTRIBUTION TREND
                ctx3 = document.getElementById("myChartSBS").getContext("2d");
                myChartSBS = new Chart(ctx3, {
                                            type: "pie",
                                            data: {
                                                labels: response['label'],
                                                datasets: [
                                                    {
                                                        label: "CASH / KREDIT",
                                                        backgroundColor: ["#FFBF00","#DE3163","#FF0000","#AAFF00","#1D007E", "#D2FF28","#FF9F29","#AF4F00","#4F00B0"],
                                                        data: response['mtd'],
                                                    }
                                                ]
                                            },
                                            // plugins: [ChartDataLabels],
                                            options: {
                                                
                                                responsive: true,
                                                options: {
                                                    plugins: {
                                                      datalabels: {
                                                        display: true,
                                                        align: 'bottom',
                                                        backgroundColor: '#ccc',
                                                        borderRadius: 3,
                                                        font: {
                                                          size: 18,
                                                        }
                                                      },
                                                    }
                                                  }
                                            }
                                        });               
                
                ctx6 = document.getElementById("myChartSBS2").getContext("2d");
                myChartSBS2 = new Chart(ctx6, {
                                            type: "pie",
                                            data: {
                                                labels: response['label'],
                                                datasets: [
                                                    {
                                                        label: "CASH / KREDIT",
                                                        backgroundColor: ["#FFBF00","#DE3163","#FF0000","#AAFF00","#1D007E", "#D2FF28","#FF9F29","#AF4F00","#4F00B0"],
                                                        data: response['mtd1'],
                                                    }
                                                ]
                                            },
                                            // plugins: [ChartDataLabels],
                                            options: {
                                                responsive: true,
                                                options: {
                                                    plugins: {
                                                      datalabels: {
                                                        display: true,
                                                        align: 'bottom',
                                                        backgroundColor: '#ccc',
                                                        borderRadius: 3,
                                                        font: {
                                                          size: 18,
                                                        }
                                                      },
                                                    }
                                                  }
                                            }
                                        });    
                $('#ajaxspinner3').hide();
            }
        }
        )
    }
    
    function reloadGrid4(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
        $('#ajaxspinner4').show();
                
        $.ajax({
            type: 'POST',
            url: '$urlQuerySS',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            success:function(response) {               
                   $('#myChartSS').remove(); 
                    $('.1').append('<canvas id="myChartSS" height="360px"><canvas>');
                    
					$('#myChartSS2').remove(); 
                    $('.5').append('<canvas id="myChartSS2" height="360px"><canvas>');
                    
                    $('#myChartSS3').remove(); 
                    $('.7').append('<canvas id="myChartSS3" height="80px"><canvas>');
            //SALES STRUCTURE                
                ctx4 = document.getElementById("myChartSS").getContext("2d");
                myChartSS = new Chart(ctx4, {
                                            type: "bar",
                                            data: {
                                                labels: response['labelss'],
                                                datasets: [
                                                    {
                                                        label: "M-1",
                                                        backgroundColor: "deepskyblue",
                                                        borderColor: "deepskyblue",
                                                        borderWidth: 1,
                                                        data: response['cashss']
                                                    },
                                                    {
                                                        
                                                        label: "M",
                                                        backgroundColor: "Orange",
                                                        borderColor: "Orange",
                                                        borderWidth: 1,
                                                        data: response['creditss']
                                                    },
                                                    ]
                                                    },
                                            options: {
                                                indexAxis: 'y',
                                                barPercentage: 0.8,
                                                plugins: {
                                                  datalabels: {
                                                    anchor: 'end',
                                                    align: 'end',
                                                    offset: 8,
                                                  },
                                                },
                                                responsive: true,
                                                maintainAspectRatio: false,
                                                legend: {
                                                    position: "top",
                                                    
                                                },
                                                title: {
                                                    display: true,
                                                    text: "Chart.js Bar Chart"
                                                },
                                                scales: {
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true,
                                                            padding: 100
                                                        }
                                                    }]
                                                }
                                            }
                                        });  
                
                ctx5 = document.getElementById("myChartSS2").getContext("2d");
                myChartSS2 = new Chart(ctx5, {
                                            type: "pie",
                                            data: {
                                                // labels: ['Cash', ''],
                                                datasets: [
                                                    {
                                                        label: "CASH / KREDIT",
                                                        backgroundColor: ["#DE3163","#FFBF00"],
                                                        data: response['cashss2']
                                                    }
                                                ]
                                            },
                                            // plugins: [ChartDataLabels],
                                            options: {
                                                responsive: true,
                                                options: {
                                                    plugins: {
                                                      datalabels: {
                                                        display: true,
                                                        align: 'bottom',
                                                        backgroundColor: '#ccc',
                                                        borderRadius: 3,
                                                        font: {
                                                          size: 18,
                                                        }
                                                      },
                                                    }
                                                  }
                                            }
                                        });
                
                
                ctx7 = document.getElementById("myChartSS3").getContext("2d");
                myChartSS3 = new Chart(ctx7, {
                                            type: "pie",
                                            data: {
                                                // labels: ['', 'Credit'],
                                                datasets: [
                                                    {
                                                        label: "CASH / KREDIT",
                                                        backgroundColor: ["#DE3163","#FFBF00"],
                                                        data: response['creditss2']
                                                    }
                                                ]
                                            },
                                            // plugins: [ChartDataLabels],
                                            options: {
                                                responsive: true,
                                                options: {
                                                    plugins: {
                                                      datalabels: {
                                                        display: true,
                                                        align: 'bottom',
                                                        backgroundColor: '#ccc',
                                                        borderRadius: 3,
                                                        font: {
                                                          size: 18,
                                                        }
                                                      },
                                                    }
                                                  }
                                            }
                                        });         
                
                $('#ajaxspinner4').hide();
            }
        }
        )
    }
    
    function reloadGrid5(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();      
        $('#ajaxspinner5').show();
                
        $.ajax({
            type: 'POST',
            url: '$urlQueryPR',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            success:function(response) {    
            //POST RANK
                var html = '';
                    var i;
                    for(i=0; i<response['posrank'].length; i++){
                        html += '<tr align="right">'+
                                '<td align="left">'+response['posrank'][i].rangk+'</td>'+
                                '<td align="left">'+response['posrank'][i].POSName+'</td>'+
                                '<td align="left">'+response['posrank'][i].area+'</td>'+
                                '<td>'+response['posrank'][i].sales+'</td>'+
                                '<td>'+response['posrank'][i].growth+'</td>'+
                                '<td>'+response['posrank'][i].cont+'</td>'+
                                '<td>'+response['posrank'][i].salesb+'</td>'+
                                '<td>'+response['posrank'][i].contb+'</td>'+
                                '</tr>';
                    }
                    $('#show_data').html(html);
                    $('#ajaxspinner5').hide();
                
            }
        })
    }
    
    function reloadGrid6(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
        $('#ajaxspinner6').show();
        
                
        $.ajax({
            type: 'POST',
            url: '$urlQueryPE',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            success:function(response) {    
            //PEFORMANCE REPORT
                var peformance = '';
                var j;
                for(j=0; j<response['peformance'].length; j++){
                        peformance += '<tr align="right">'+
                                '<td align="left">'+response['peformance'][j].Kelompok+'</td>'+
                                '<td align="left">'+response['peformance'][j].Nama+'</td>'+
                                '<td>'+response['peformance'][j].Target+'</td>'+
                                '<td>'+response['peformance'][j].AcvSales+'</td>'+
                                '<td>'+response['peformance'][j].SalesVSTarget+'</td>'+
                                '<td>'+response['peformance'][j].SelisihTarget+'</td>'+
                                '</tr>';
                    }
                $('#peformance').html(peformance);
                $('#ajaxspinner6').hide();
                
            }
        })
    }
    
    function reloadGrid7(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
        $('#ajaxspinner7').show();
        
                
        $.ajax({
            type: 'POST',
            url: '$urlQuerySK',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            success:function(response) {    
             //RANK
                var rank = '';
                var k;
                for(k=0; k<response['rank'].length; k++){
                        rank += '<tr align="right">'+
                                '<td align="left">'+response['rank'][k].act+'</td>'+
                                '<td>'+response['rank'][k].RetailTarget+'</td>'+
                                '<td>'+response['rank'][k].RetailActual+'</td>'+
                                '<td>'+response['rank'][k].RetailAvg+'</td>'+
                                '<td>'+response['rank'][k].RetailACV+'</td>'+
                                '<td>'+response['rank'][k].DealerTarget+'</td>'+
                                '<td>'+response['rank'][k].DealerActual+'</td>'+
                                '<td>'+response['rank'][k].DealerAvg+'</td>'+
                                '<td>'+response['rank'][k].DealerACV+'</td>'+
                                '<td>'+response['rank'][k].StockTarget+'</td>'+
                                '<td>'+response['rank'][k].StockOnHand+'</td>'+
                                '<td>'+response['rank'][k].StockInTransit+'</td>'+
                                '<td>'+response['rank'][k].Day+'</td>'+
                                '</tr>';
                    }
                $('#rank').html(rank);
                
                var rankgroup = '';
                        rankgroup += 
                                '<th scope="col" colspan="6">'+response['kolom1']+'</th>'+
                                '<th scope="col" colspan="7">'+response['kolom2']+'</th>';
                $('#rankgroup').html(rankgroup);
                
                var workday = '';
                        workday += 
                               '<th scope="col" colspan="6"> Retail Sales, Distribution & Stock Progress(MTD) </th>'+
                                '<th scope="col" colspan="7">'+response['kolom']+'</th>';
                $('#workday').html(workday);
                $('#ajaxspinner7').hide();
                
            }
        })
    }
    
    function combo(){
        var level = $("#level").val();         
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '$urlcmbkwl',
            data: { level: level},
            success: function(data){                
                console.log(data['data']);
                $("#korwil").select2({data: data['data']})
                $(".korwil-selected").select2({data: data['data']})    
                // $("#korwil").find('option').attr('selected',true);
                // $("#korwil").select2();
                mdcombo();
            }
        });
    }
    
    function mdcombo(){
        var korwil = $("#korwil").val();         
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '$urlcmbmd',
            data: { korwil: korwil},
            success: function(data){                
                console.log(data['data']);
                $("#md").select2({data: data['datamd']})
                $(".md-selected").select2({data: data['datamd']})                
            }
        });
    }
    
     function dealercombo(){
        var md = $("#korwil").val();         
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '$urlcmbdealer',
            data: { md: md},
            success: function(data){                
                console.log(data['data']);
                $("#iddealer").select2({data: data['datadealer']})
                $(".iddealer-selected").select2({data: data['datadealer']})                  
            }
        });
    }
    
    
   // $(document).ready(function() {
   //     combo();  
   //      $("#korwil").find('option').attr('selected',true);
   //      $("#korwil").select2();
   //      mdcombo();
   // });
   
   // $("#level").change(function() {
   //      combo();
   //      mdcombo();
   //      // $("#korwil").select2();
   //      // $("#korwil").find('option').attr('selected',true);
   //     
   // });
   
   // $("#korwil").change(function() {
   //      mdcombo();
   // });
   
   // $("#md").change(function() {
   //      dealercombo();
   //      $("#iddealer").find('option').attr('selected',true);
   //      $("#iddealer").select2();
   // });
   
   $("#tampil").click(function() {
        reloadGrid1();
        reloadGrid2();
        reloadGrid3();
        reloadGrid4();
        reloadGrid5();
        reloadGrid6();
        reloadGrid7();
   });
   
   
            
JS
);

$sqlMd = $datamd;
$cmbMd = ArrayHelper::map($sqlMd, 'MD', 'MD');

$sqlKorwil = $data;
$cmbKorwil = ArrayHelper::map($sqlKorwil, 'Korwil', 'Korwil');

$sqlDealer = $datadealer;
$cmbDealer = ArrayHelper::map($sqlDealer, 'Dealer', 'Dealer');
?>


    <!--<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>-->
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.0/dist/chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
<style>
    .tombol{
        background:#2C97DF;
        color:white;
        border-top:0;
        border-left:0;
        border-right:0;
        border-bottom:0;
        padding:5px 10px;
        text-decoration:none;
        font-family:sans-serif;
        font-size:11pt;
        border-radius: 4px;
    }

    .outer{
        overflow-y: auto;
        height:100px;
    }

    .outer table{
        width: 100%;
        table-layout: fixed;
        border : 1px solid black;
        border-spacing: 1px;
    }

    .outer table th {
        text-align: left;
        top:0;
        position: sticky;
        background-color: white;
    }
</style>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
            <?= Html::beginForm(); ?>

            <div class="box-body">

                <form id="tgl_query">
                    <div class="form-group">
                    <div class="box-body">
                        <div class="col-md-24">
                            <label class="control-label col-sm-1">Tanggal</label>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <? try {
                                            echo DatePicker::widget([
                                                'name' => 'tgl1',
                                                'id' => 'tgl1',
                                                'type' => DatePicker::TYPE_INPUT,
                                                'value' => Yii::$app->formatter->asDate('now', '01/MM/yyyy'),
                                                'pluginOptions' => [
                                                    'autoclose' => true,
                                                    'format' => 'dd/mm/yyyy',
                                                    'todayHighlight' => true
                                                ],
                                            ]);
                                        } catch (\yii\base\InvalidConfigException $e) {
                                        } ?>
                                    </div>
                                    <div class="col-md-12">
                                        <? try {
                                            echo DatePicker::widget([
                                                'name' => 'tgl2',
                                                'id' => 'tgl2',
                                                'type' => DatePicker::TYPE_INPUT,
                                                'value' => date('t/m/Y', strtotime("today")),
                                                'pluginOptions' => [
                                                    'autoclose' => true,
                                                    'format' => 'dd/mm/yyyy',
                                                ],

                                            ]);
                                        } catch (\yii\base\InvalidConfigException $e) {
                                        } ?>
                                    </div>
                                </div>
                            </div>
                            <label class="control-label col-sm-1">Bulan</label>
                            <div class="col-md-3" style="margin-left: 5px">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'TglKu',
                                        'id' => 'TglKu',
                                        'attribute2' => 'to_date',
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => Yii::$app->formatter->asDate('now', 'MMMM  yyyy'),
                                        'pluginOptions' => [
                                            'startView' => 'year',
                                            'minViewMode' => 'months',
                                            'format' => 'MM  yyyy'
                                        ],
                                        'pluginEvents' => [
                                            "change" => new JsExpression("
                                                function(e) {
                                                 let d = new Date(Date.parse('01 ' + $(this).val()));											 
                                                 var tgl1 = $('#tgl1');
                                                 var tgl2 = $('#tgl2');                                               
                                                  tgl1.kvDatepicker('update', d);
                                                  tgl2.kvDatepicker('update',new Date(d.getFullYear(), d.getMonth()+1, 0)); 
                                                }                                                                                            
                                                "),
                                        ]
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                            <div class="col-sm-2">
                                <? try {
                                    echo CheckboxX::widget([
                                        'name' => 'eom',
                                        'value' => true,
                                        'options' => ['id' => 'eom'],
                                        'pluginOptions' => ['threeState' => false, 'size' => 'lg'],
                                        'pluginEvents' => [
                                            "change" => new JsExpression("function(e) {
                                                 var today = new Date();
                                                 var tgl2 = $('#tgl2');
                                                 let tglku = tgl2.kvDatepicker('getDate');
                                                 var lastDayOfMonth = new Date(tglku.getFullYear(), tglku.getMonth()+1, 0);
                                                if($(this).val() == 1){
                                                    tgl2.kvDatepicker('update', lastDayOfMonth)
                                                 }else{
                                                    tgl2.kvDatepicker('update', new Date(today.getFullYear(), today.getMonth(), today.getDate()));
                                                 }
                                                }")
                                        ]
                                    ]);
                                    echo '<label class="cbx-label" for="eom">EOM</label>';
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>

                            <label class="control-label col-sm-1">Level</label>
                            <div class="col-sm-5">
                                <?= Html::dropDownList('level', null, [
                                    'Korwil' => 'Korwil',
                                    'Nasional' => 'Nasional',
                                    'Anper' => 'Anper',
                                    'BHC' => 'BHC',
                                ], ['id' => 'level', 'text' => 'Pilih tipe laporan', 'class' => 'form-control']) ?>

                            </div>
                            <div class="col-sm-5" style="height: 50px" id="tampil">
                                <label class="control-label tombol"  ><i class="glyphicon glyphicon-new-window"> Tampil</i></label>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="col-md-24">
                    <div class="col-md-12">
                        <div class="box box-warning direct-chat direct-chat-warning">

                            <div class="box-header with-border">
                                <div class="col-md-22"><h3 class="box-title">DAILY DISTRIBUTION TREND</h3></div>
                                <i id="ajaxspinner1" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                                <div class="col-md-2">
                                    <button class="btn" id="ddt" type="button" formtarget="_blank "><i
                                                class="fa fa-file-text"></i></button>
                                </div>
                            </div>
                            <div class="box-body">

                                <div>
                                    <div class="2">
                                        <canvas id="myChartDDT"></canvas>
                                    </div>
                                </div>
                                <script>
                                </script>
                            </div>
                            <div class="box-footer"></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">DAILY RETAIL SALES TREND</h3>
                                <i id="ajaxspinner2" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                                <div class="box-tools pull-right">
                                    <button class="btn" id="drst" type="button" formtarget="_blank "><i
                                                class="fa fa-file-text"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div>
                                    <div class="3">
                                        <canvas id="myChartDRST"></canvas>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                </div>
                <div class="col-md-24">
                    <div class="col-md-12">
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">SALES BY SEGMEN</h3>
                                <i id="ajaxspinner3" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                                <div class="box-tools pull-right">
                                    <button class="btn" id="sbg" type="button" formtarget="_blank "><i
                                                class="fa fa-file-text"></i></button>
                                </div>
                            </div>
                            <div class="box-body">

                                <div class="col-md-24">
                                    <div class="col-md-12">
                                        <h4>MTD</h4>
                                    <div class="4" style="width:20vw;height: 24vw">
                                    <canvas   id="myChartSBS"></canvas>
                                </div></div>
                                    <div class="col-md-12">
                                        <h4>MTD-1</h4>
                                        <div class="6" style="width:20vw;height: 24vw">
                                            <canvas   id="myChartSBS2"></canvas>
                                        </div>
                                    </div>
                                </div>




                            </div>
                        </div>
                        <div class="box-footer"></div>
                    </div>
                    <div class="col-md-12">
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">SALES STRUCTURE</h3>
                                <i id="ajaxspinner4" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                                <div class="box-tools pull-right">
                                    <button class="btn" id="ss" type="button" formtarget="_blank "><i
                                                class="fa fa-file-text"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="row col-md-24">
                                    <div class="col-md-5" style="margin-top: 80px">
                                        <div class="row column-md-12"><div class="col-md-2" style="width: 42px;height: 13px;background-color: #DE3163;margin-left: 30px"></div>&nbsp CASH</div><br>
                                        <div class="5" style="width:8vw;height: 8vw">
                                            <canvas id="myChartSS2"></canvas>
                                        </div>
                                        <br><div class="row column-md-12"><div class="col-md-2" style="width: 42px;height: 13px;background-color: white;margin-left: 20px;"></div><b>M</b></div><br>
                                    </div>
                                    <div class="col-md-6" style="margin-top: 80px">
                                        <div class="row column-md-12"><div class="col-md-2" style="width: 42px;height: 13px;background-color: #FFBF00;margin-left: 30px"></div>&nbsp KREDIT</div><br>
                                        <div class="7" style="width:8vw;height: 8vw;">
                                            <canvas id="myChartSS3"></canvas>
                                        </div>
                                        <br><div class="row column-md-12"><div class="col-md-2" style="width: 42px;height: 13px;background-color: white;margin-left: 20px;"></div><b>M-1</b></div><br>
                                    </div>
<!--                                    <div class="col-md-11">-->
<!--                                        <div class="5">-->
<!--                                            <canvas id="myChartSS2" ></canvas>-->
<!--                                        </div>-->
<!--                                    </div>-->
                                    <div class="col-md-13">
                                        <div class="1" style="height: 20vw;">
                                            <canvas id="myChartSS"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-24">
                    <div class="col-md-12">
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">POS Rank</h3>
                                <i id="ajaxspinner5" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                                <div class="box-tools pull-right">
                                    <button class="btn" id="excelpr" type="button" formtarget="_blank "><i
                                                class="fas fa-download"></i></button>
                                    <button class="btn" id="pr" type="button" formtarget="_blank "><i
                                                class="fa fa-file-text"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div style="position: relative;height: 300px;overflow: auto;display: block;">
                                    <table class="table table-bordered table-striped mb-0" id="table3">
                                        <thead>
                                        <tr>
                                            <th scope="col">Rank</th>
                                            <th scope="col">Post Name</th>
                                            <th scope="col">Area</th>
                                            <th scope="col">Sales (MTD)</th>
                                            <th scope="col">Growth (MTD)</th>
                                            <th scope="col">Cont (MTD)</th>
                                            <th scope="col">Sales (M-1)</th>
                                            <th scope="col">Cont (M-1)</th>
                                        </tr>
                                        </thead>
                                        <tbody id="show_data"></tbody>
                                    </table>

                                </div>


                            </div>
                            <div class="box-footer"></div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Sales VS Target</h3>
                                <i id="ajaxspinner6" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                                <div class="box-tools pull-right">
                                    <button class="btn" id="excelst" type="button" formtarget="_blank "><i
                                                class="fas fa-download"></i></button>
                                    <button class="btn" id="ts" type="button" formtarget="_blank "><i
                                                class="fa fa-file-text"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div style="position: relative;height: 300px;overflow: auto;display: block;">
                                    <table style = "overflow-y: auto;" class="table table-bordered table-striped mb-0" id="table2">
                                        <thead style="position: sticky;top: 0;">
                                        <tr style="background-color: white">
                                            <th scope="col" style="text-align: center">Kelompok</th>
                                            <th scope="col" style="text-align: center">Nama</th>
                                            <th scope="col" style="text-align: center">Target</th>
                                            <th scope="col" style="text-align: center">Acv Sales</th>
                                            <th scope="col" style="text-align: center">Sales VS Target</th>
                                            <th scope="col" style="text-align: center">Selisih Target</th>
                                        </tr>
                                        </thead>
                                        <tbody id="peformance"></tbody>
                                    </table>

                                </div>
                            </div>
                            <div class="box-footer"></div>
                        </div>

                    </div>
                </div>
                <div class="col-md-24">
                    <div class="col-md-24">
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">SUMMARY SADISTOCK</h3>
                                <i id="ajaxspinner7" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                                <div class="pull-right">
                                    <button class="btn" id="excelss" type="button" formtarget="_blank "><i
                                                class="fas fa-download"></i></button>
                                    <button class="btn" id="rk" type="button" formtarget="_blank "><i
                                                class="fa fa-file-text"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div style="position: relative;height: 200px;overflow: auto;display: block;">
                                    <table class="table table-bordered table-striped mb-0" id="table1">
                                        <thead>
                                        <tr id="rankgroup"></tr>
                                        <tr id="workday"></tr>
                                        <tr>
                                            <th scope="col" rowspan="2">Act</th>
                                            <th scope="col" colspan="4">Retail Sales</th>
                                            <th scope="col" colspan="4">Distribution</th>
                                            <th scope="col" colspan="4">Dealer Stock</th>
                                        </tr>
                                        <tr>
                                            <th scope="col">Target</th>
                                            <th scope="col">Actual</th>
                                            <th scope="col">Avg</th>
                                            <th scope="col">% ACV</th>
                                            <th scope="col">Target</th>
                                            <th scope="col">Actual</th>
                                            <th scope="col">Avg</th>
                                            <th scope="col">% ACV</th>
                                            <th scope="col">Target</th>
                                            <th scope="col">On Hand</th>
                                            <th scope="col">Intrnasit</th>
                                            <th scope="col">Day</th>
                                        </tr>
                                        </thead>
                                        <tbody id="rank"></tbody>
                                    </table>

                                </div>


                            </div>
                            <div class="box-footer"></div>
                        </div>

                    </div>
                </div>
                <?= Html::endForm(); ?>
            </div>
        </div>
<?php
$urlddt = Url::toRoute(['report/dashboard-show-konsol', 'param' => '"Sadistock"']);
$urldrst = Url::toRoute(['report/dashboard-show-penjualan-konsol', 'param' => '"PenjualanKecamatan"']);
$urlsbg = Url::toRoute(['report/dashboard-show-segmen-konsol', 'param' => '"PenjualanSegmenBulanan"']);
$urlpr = Url::toRoute(['report/dashboard-show-pos-konsol', 'param' => '"PenjualanSales"']);
$urlss = Url::toRoute(['report/dashboard-show-cash-credit-konsol', 'param' => '"SalesStructureUnit"']);
$urlts = Url::toRoute(['report/dashboard-show-target-sales-konsol', 'param' => '"PenjualanDealer"']);
$urlrank = Url::toRoute(['report/dashboard-show-rank-konsol', 'param' => '"DistributionGrowth"']);
$urlaging = Url::toRoute(['report/dashboard-show-aging-konsol', 'param' => '"PeoplePenjualan"']);
$urlsdp = Url::toRoute(['report/dashboard-show-aging-konsol', 'param' => '"ProfitTahun"']);
$urlpl = Url::toRoute(['report/dashboard-show-pl-konsol', 'param' => '"AgingPiutang"']);
$user = $_SESSION['__id'];
$this->registerJsVar( 'userid', $user );

$this->registerJs(<<< JS

        
    
        $('#ddt').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            var level = $('#level').val();
            var korwil = $('#korwil').val();
            var md = $('#md').val();
            var dealer = $('#iddealer').val();
            var eom = $('#eom').val();
            // window.open('{$urlddt}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","' + korwil + '","' + md +'","' + dealer +'"', '_blank');
            window.open('{$urlrank}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","' + korwil + '","' + md +'","' + dealer +'"', '_blank');
        });

        
        $('#drst').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            var level = $('#level').val();
            var korwil = $('#korwil').val();
            var md = $('#md').val();
            var dealer = $('#iddealer').val();
            // window.open('{$urldrst}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","' + korwil + '","' + md +'","' + dealer +'"', '_blank');
            window.open('{$urlpr}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","' + korwil + '","' + md +'","' + dealer +'"', '_blank');
        });
        
        $('#sbg').click(function (event) {  
           var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            var level = $('#level').val();
            var korwil = $('#korwil').val();
            var md = $('#md').val();
            var dealer = $('#iddealer').val();
            window.open('{$urlsbg}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","' + korwil + '","' + md +'","' + dealer +'"', '_blank');
        });
        $('#pr').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            var level = $('#level').val();
            var korwil = $('#korwil').val();
            var md = $('#md').val();
            var dealer = $('#iddealer').val();
            // window.open('{$urlpr}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","' + korwil + '","' + md +'","' + dealer +'"', '_blank');
            window.open('{$urldrst}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","' + korwil + '","' + md +'","' + dealer +'"', '_blank');
        });
        $('#ss').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            var level = $('#level').val();
            var korwil = $('#korwil').val();
            var md = $('#md').val();
            var dealer = $('#iddealer').val();
            window.open('{$urlss}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","' + korwil + '","' + md +'","' + dealer +'"', '_blank');
        });
        $('#ts').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            var level = $('#level').val();
            var korwil = $('#korwil').val();
            var md = $('#md').val();
            var dealer = $('#iddealer').val();
//            window.open('{$urlts}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","' + korwil + '","' + md +'","' + dealer +'"', '_blank');
            window.open('{$urlaging}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","' + korwil + '","' + md +'","' + dealer +'"', '_blank');
        });
        $('#rk').click(function (event) {  
            var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
            var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
            var level = $('#level').val();
            var korwil = $('#korwil').val();
            var md = $('#md').val();
            var dealer = $('#iddealer').val();
            window.open('{$urlddt}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","' + korwil + '","' + md +'","' + dealer +'"', '_blank');
//            window.open('{$urlrank}'+ ',"' + tgl1 + '","' + tgl2 + '","' + eom +'","' + userid +'","' + level + '","' + korwil + '","' + md +'","' + dealer +'"', '_blank');
        });


$(document).ready(function () {
      $("#excelpr").click(function(){
        TableToExcel.convert(document.getElementById("table3"), {
            name: "POS Rank.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
      
      
      $("#excelst").click(function(){
        TableToExcel.convert(document.getElementById("table2"), {
            name: "Sales VS Target.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
      
      $("#excelss").click(function(){
        TableToExcel.convert(document.getElementById("table1"), {
            name: "SUMMARY SADISTOCK.xlsx",
            sheet: {
            name: "Sheet1"
            }
          });
        });
  });
        

JS
);










