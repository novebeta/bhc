<?php

use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\checkbox\CheckboxX;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;

?>


<?php
$this->title = 'Dashboard Marketing Monitoring';
$this->params['breadcrumbs'][] = $this->title;
$formatter = \Yii::$app->formatter;

$urlQuery = Url::toRoute(['report/query-dashboard-marketing']);
$urlQueryLease = Url::toRoute(['report/query-dashboard-marketing-lease']);
$urlQuerySales = Url::toRoute(['report/query-dashboard-marketing-sales']);
$urlQuerySalesDo = Url::toRoute(['report/query-dashboard-marketing-sales-do']);
$format = <<< SCRIPT
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
$this->registerJs(<<< JS
       
    
    Chart.register(ChartDataLabels);
    function reloadGrid1(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
        $('#ajaxspinner1').show();
                
        $.ajax({
            type: 'POST',
            url: '$urlQuery',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            success:function(response) {      
                $('#TotalSalesMN').text(response['TotalSalesMN']);
                $('#TotalSalesMB').text(response['TotalSalesMB']);
                $('#CreditShareMN').text(response['CreditShareMN']+' %');
                $('#CreditShareMB').text(response['CreditShareMB']+' %');
                $('#NPBTMN').text(response['NPBTMN']);
                $('#NPBTMB').text(response['NPBTMB']);
                $('#SalesGrowth').text(response['SalesGrowth']+' %');
                var divSalesGrowth = document.getElementById( 'colorSalesGrowth' );
                if (response['SalesGrowth'] > 1) {
                    divSalesGrowth.style.backgroundColor = 'blue';
                }else{
                    divSalesGrowth.style.backgroundColor = 'red';
                }
                
                $('#TunaiGrowth').text(response['TunaiGrowth']+' %');
                var divTunaiGrowth = document.getElementById( 'colorTunaiGrowth' );
                if (response['TunaiGrowth'] > 1) {
                    divTunaiGrowth.style.backgroundColor = 'blue';
                }else{
                    divTunaiGrowth.style.backgroundColor = 'red';
                }
                
                $('#CreditGrowth').text(response['CreditGrowth']+' %');
                var divCreditGrowth = document.getElementById( 'colorCreditGrowth' );
                if (response['CreditGrowth'] > 1) {
                    divCreditGrowth.style.backgroundColor = 'blue';
                }else{
                    divCreditGrowth.style.backgroundColor = 'red';
                }
                
                $('#NPBTGrowth').text(response['NPBTGrowth']+' %');
                var divNPBTGrowth = document.getElementById( 'colorNPBTGrowth' );
                if (response['NPBTGrowth'] > 1) {
                    divNPBTGrowth.style.backgroundColor = 'blue';
                }else{
                    divNPBTGrowth.style.backgroundColor = 'red';
                }
                
                $('#DistribusiMN').text(response['DistribusiMN']);
                $('#DistribusiMB').text(response['DistribusiMB']);
                $('#DistribusiGrowth').text(response['DistribusiGrowth']+ ' %');
                var divDistribusiGrowth = document.getElementById( 'colorDistribusiGrowth' );
                if (response['DistribusiGrowth'] > 1) {
                    divDistribusiGrowth.style.backgroundColor = 'blue';
                }else{
                    divDistribusiGrowth.style.backgroundColor = 'red';
                }
                $('#ajaxspinner1').hide();
            }
        })
    }    
    
    function reloadGrid2(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
                $('#ajaxspinner2').show();
        $.ajax({
            type: 'POST',
            url: '$urlQuerySales',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            success:function(response) {
                //ctxmyChartSales
                   
                ctxmyChartSales = document.getElementById("myChartSales").getContext("2d");
                ctxmyChartSales = new Chart(ctxmyChartSales, {
                                                data: {
                                                    labels: response['Periode'],
                                                    datasets: [
                                                        {
                                                            type: "bar",
                                                            label: "SalesMN",
                                                            backgroundColor: "dodgerblue",
                                                            borderColor: "dodgerblue",
                                                            borderWidth: 1,
                                                            data: response['SalesMN']
                                                        },
                                                        {
                                                            type: "bar",
                                                            label: "SalesMB",
                                                            backgroundColor: "coral",
                                                            borderColor: "coral",
                                                            borderWidth: 1,
                                                            data: response['SalesMB']
                                                        },{
                                                            type: "bar",
                                                            label: "DOMN",
                                                            backgroundColor: "grey",
                                                            borderColor: "grey",
                                                            borderWidth: 1,
                                                            data: response['DOMN']
                                                        }
                                                    ]
                                                },
                                                plugins: [ChartDataLabels],
                                                options: {
                                                    plugins: {
                                                        datalabels: {
                                                          display: false
                                                        }
                                                      },
                                                    responsive: true,
                                                    legend: {
                                                        position: "top"
                                                    },
                                                    title: {
                                                        display: true,
                                                        text: "Chart.js Bar Chart"
                                                    },
                                                    
                                                }
                                            }); 
                $('#ajaxspinner2').hide();
                
            }
        })
    }
    
    function reloadGrid3(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
        
        
        $('#ajaxspinner3').show();
                
        $.ajax({
            type: 'POST',
            url: '$urlQuerySalesDo',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            success:function(response) {
                
                //myChartInsentif
                ctxmyChartSalesDo = document.getElementById("myChartSalesDo").getContext("2d");
                ctxmyChartSalesDo = new Chart(ctxmyChartSalesDo, {
                                                data: {
                                                    labels: response['Periode'],
                                                    datasets: [
                                                        {
                                                            type: "bar",
                                                            label: "SalesMN",
                                                            backgroundColor: "dodgerblue",
                                                            borderColor: "dodgerblue",
                                                            borderWidth: 1,
                                                            data: response['SalesMN']
                                                        },
                                                        {
                                                            type: "bar",
                                                            label: "SalesMB",
                                                            backgroundColor: "coral",
                                                            borderColor: "coral",
                                                            borderWidth: 1,
                                                            data: response['SalesMB']
                                                        },{
                                                            type: "bar",
                                                            label: "DOMN",
                                                            backgroundColor: "grey",
                                                            borderColor: "grey",
                                                            borderWidth: 1,
                                                            data: response['DOMN']
                                                        }
                                                    ]
                                                },
                                                plugins: [ChartDataLabels],
                                                options: {
                                                    plugins: {
                                                        datalabels: {
                                                          display: false
                                                        }
                                                      },
                                                    responsive: true,
                                                    legend: {
                                                        position: "top"
                                                    },
                                                    title: {
                                                        display: true,
                                                        text: "Chart.js Bar Chart"
                                                    },
                                                    
                                                }
                                            }); 
                $('#ajaxspinner3').hide();
                
            }
        })
    }   
    
    function reloadGrid4(){
        var eom = $('#eom').val();
        var tgl1 = $('#tgl1').val().split("/").reverse().join("-");
        var tgl2 = $('#tgl2').val().split("/").reverse().join("-");
        var level = $('#level').val();
                
        $.ajax({
            type: 'POST',
            url: '$urlQueryLease',
            data: { tgl1: tgl1, tgl2: tgl2, eom:eom,level:level},
            datatype: 'json',
            async: true,
            success:function(response) {
                
                console.log(response['leaseunit'].unshift('Unit'));
                console.log(response['leasems'].unshift('MS'));
                console.log(response['leasemscp'].unshift('MSCP'));
                var leaseunit = '';
                var leasems = '';
                var leasemscp = '';
                var j;
                for(j=0; j<response['leaseunit'].length; j++){
                    leaseunit += 
                    '<th>'+response['leaseunit'][j]+'</th>';
                }               
                $('#leaseunit').html(leaseunit);
                
                var k;
                for(k=0; k<response['leasems'].length; k++){
                    leasems += 
                    '<th>'+response['leasems'][k]+'</th>';
                }               
                $('#leasems').html(leasems);
                
                var l;
                for(l=0; l<response['leasemscp'].length; l++){
                    leasemscp += 
                    '<th>'+response['leasemscp'][l]+'</th>';
                }               
                $('#leasemscp').html(leasemscp);
            }
        })
    }    
    
    
       
    function countdown() {
        var fiveMinutes = 60 * 30,
            display = document.querySelector('#time');
        startTimer(fiveMinutes, display);
    };
    function startTimer(duration, display) {
        var timer = duration, minutes, seconds;
        setInterval(function () {
            minutes = parseInt(timer / 60, 10)
            seconds = parseInt(timer % 60, 10);
    
            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;
    
            display.textContent = minutes + ":" + seconds;
    
            if (--timer < 0) {
                timer = duration;
            }
        }, 1000);
    }
    

   $("#tampil").click(function() {
        
        reloadGrid1();
        $('#myChartSales').remove();
        $('.1').append('<canvas id="myChartSales"><canvas>'); 
        reloadGrid2();
        $('#myChartSalesDo').remove();
        $('.2').append('<canvas id="myChartSalesDo"><canvas>'); 
        reloadGrid3();
        reloadGrid4();
        countdown();
   }); 
    setInterval(function(){ 
        $("#tampil").click();
    },1800000);
    
    
    
    

JS
);

?>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@4.4.1/dist/chart.umd.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-chart-treemap@2.3.0/dist/chartjs-chart-treemap.min.js">
    </script>
    <style>
        .tombol {
            background: #2C97DF;
            color: white;
            border-top: 0;
            border-left: 0;
            border-right: 0;
            border-bottom: 0;
            padding: 5px 10px;
            text-decoration: none;
            font-family: sans-serif;
            font-size: 11pt;
            border-radius: 4px;
        }

        table, th, td,tr,thead {
            text-align: center;
            color: black;
            vertical-align: middle;
            border: 1px solid black;

        }
    </style>

    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">

            <?= Html::beginForm(); ?>
            <div class="box-body">
                <div class="col-md-24">
                <div class="col-md-2">
                    <div class="form-group">
                        <div class="col-sm-24">
                            <label class="control-label col-sm-4">Tanggal</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <div class="col-md-12">
                            <? try {
                                echo DatePicker::widget([
                                    'name' => 'tgl1',
                                    'id' => 'tgl1',
                                    'type' => DatePicker::TYPE_INPUT,
                                    'value' => Yii::$app->formatter->asDate('now', '01/MM/yyyy'),
//                                                'value' => '01/02/2021',
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'dd/mm/yyyy',
                                        'todayHighlight' => true
                                    ],
                                ]);
                            } catch (\yii\base\InvalidConfigException $e) {
                            } ?>
                        </div>
                        <div class="col-md-12">
                            <? try {
                                echo DatePicker::widget([
                                    'name' => 'tgl2',
                                    'id' => 'tgl2',
                                    'type' => DatePicker::TYPE_INPUT,
                                    'value' => date('t/m/Y', strtotime("today")),
//                                                'value' => '27/02/2021',
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'dd/mm/yyyy',
                                    ],
                                ]);
                            } catch (\yii\base\InvalidConfigException $e) {
                            } ?>
                        </div>
                    </div>
                </div>
                <label class="control-label col-sm-1">Bulan</label>
                <div class="col-md-3">
                    <? try {
                        echo DatePicker::widget([
                            'name' => 'TglKu',
                            'id' => 'TglKu',
                            'attribute2' => 'to_date',
                            'type' => DatePicker::TYPE_INPUT,
                            'value' => Yii::$app->formatter->asDate('now', 'MMMM  yyyy'),
                            'pluginOptions' => [
                                'startView' => 'year',
                                'minViewMode' => 'months',
                                'format' => 'MM  yyyy'
                            ],
                            'pluginEvents' => [
                                "change" => new JsExpression("
                                                function(e) {
                                                 let d = new Date(Date.parse('01 ' + $(this).val()));											 
                                                 var tgl1 = $('#tgl1');
                                                 var tgl2 = $('#tgl2');
                                                  tgl1.kvDatepicker('update', d);
                                                  tgl2.kvDatepicker('update',new Date(d.getFullYear(), d.getMonth()+1, 0));     
                                                }                                                                                            
                                                "),
                            ]
                        ]);
                    } catch (\yii\base\InvalidConfigException $e) {
                    } ?>
                </div>
                <div class="col-sm-2">
                    <? try {
                        echo CheckboxX::widget([
                            'name' => 'eom',
                            'value' => true,
                            'options' => ['id' => 'eom'],
                            'pluginOptions' => ['threeState' => false, 'size' => 'lg'],
                            'pluginEvents' => [
                                "change" => new JsExpression("function(e) {
                                                 var today = new Date();
                                                 var tgl2 = $('#tgl2');
                                                 let tglku = tgl2.kvDatepicker('getDate');
                                                 var lastDayOfMonth = new Date(tglku.getFullYear(), tglku.getMonth()+1, 0);
                                                if($(this).val() == 1){
                                                    tgl2.kvDatepicker('update', lastDayOfMonth)
                                                 }else{
                                                    tgl2.kvDatepicker('update', new Date(today.getFullYear(), today.getMonth(), today.getDate()));
                                                 }
                                                }")
                            ]
                        ]);
                        echo '<label class="cbx-label" for="eom">EOM</label>';
                    } catch (\yii\base\InvalidConfigException $e) {
                    } ?>
                </div>
                    <label class="control-label col-sm-1">Level</label>
                    <div class="col-sm-3">
                        <?= Html::dropDownList('level', null, [
                            'Dealer' => 'Dealer',
                            'Korwil' => 'Korwil',
                            'Nasional' => 'Nasional',
                            'Anper' => 'Anper',
                            'BHC' => 'BHC',
                        ], ['id' => 'level', 'text' => 'Pilih tipe laporan', 'class' => 'form-control']) ?>

                    </div>
                    <div class="col-sm-2" style="height: 50px" id="tampil">
                        <label class="control-label tombol"><i class="glyphicon glyphicon-new-window">
                                Tampil</i></label>
                    </div>
<!--                    <div id="countdown"></div>-->
                    <div style="font-size: 20px;margin-top: 5px">Reload Data in <strong id="time" >30:00</strong> minutes!</div>
                </div>

                <div class="col-md-24"><br><br></div>

                <div class="row">
                    <!-- TOTAL SALES-->
                    <div class="col-md-24">
                        <div class="box box-primary direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">TOTAL SALES & CREDIT SHARE</h3>
                            </div>
                            <i id="ajaxspinner1" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-24">
                                    <table class="table" id="table1">
                                        <thead style="text-align: center">
                                        <tr style="font-size: 35px" >
                                            <td colspan="13" style="background-color: yellow;"><strong>TOTAL SALES</strong></td>
                                            <td colspan="14" style="background-color: magenta;"><strong>CREDIT SHARE</strong></td>
                                        </tr>

                                        <tr style="font-size: 20px">
                                            <th colspan="6" style="border: 1px solid black;background-color: yellow">M</th>
                                            <th colspan="7" style="border: 1px solid black;background-color: yellow">M-1</th>
                                            <th colspan="7" style="border: 1px solid black;background-color: magenta">M</th>
                                            <th colspan="7" style="border: 1px solid black;background-color: magenta">M-1</th>
                                        </tr>
                                        <tr style="background-color: cadetblue;font-size: 40px">
                                            <th colspan="6" style="border: 1px solid black"><label id="TotalSalesMN"></label></th>
                                            <th colspan="7" style="border: 1px solid black"><label id="TotalSalesMB"></th>
                                            <th colspan="7" style="border: 1px solid black"><label id="CreditShareMN"></th>
                                            <th colspan="7" style="border: 1px solid black"><label id="CreditShareMB"></th>
                                        </tr>
                                        <tr><td colspan="15"></td></tr>
                                        <tr style="font-size: 16px">
                                            <th rowspan="2" style="background-color: lightcoral;border: 1px solid black">KET</th>
                                            <th colspan="2" style="background-color: coral;border: 1px solid black">TUNAI</th>
                                            <th colspan="2" style="background-color: lightskyblue;border: 1px solid black">KREDIT</th>
                                            <th colspan="2" style="background-color: red;border: 1px solid black">FIF</th>
                                            <th colspan="2" style="background-color: royalblue;border: 1px solid black">ADIRA</th>
                                            <th colspan="2" style="background-color: yellow;border: 1px solid black">SOF</th>
                                            <th colspan="2" style="background-color: lightgreen;border: 1px solid black">MMF</th>
                                            <th colspan="2" style="background-color: lightgrey;border: 1px solid black">MEGA</th>
                                            <th colspan="2" style="background-color: lightskyblue;border: 1px solid black">MCF</th>
                                            <th colspan="2" style="background-color: greenyellow;border: 1px solid black">MPM</th>
                                            <th colspan="2" style="background-color: tomato;border: 1px solid black">IMFI</th>
                                            <th colspan="2" style="background-color: powderblue;border: 1px solid black">MUF</th>
                                            <th colspan="2" style="background-color: palevioletred;border: 1px solid black">BMF</th>
                                            <th colspan="2" style="background-color: mediumpurple;border: 1px solid black">PARA_MEGA</th>
                                        </tr>
                                        <tr style="font-size: 16px">
                                            <th style="border: 1px solid black;background-color: coral">M</th>
                                            <th style="border: 1px solid black;background-color: coral">M-1</th>
                                            <th style="border: 1px solid black;background-color: lightskyblue">M</th>
                                            <th style="border: 1px solid black;background-color: lightskyblue">M-1</th>
                                            <th style="border: 1px solid black;background-color: red">M</th>
                                            <th style="border: 1px solid black;background-color: red">M-1</th>
                                            <th style="background-color: royalblue;border: 1px solid black;">M</th>
                                            <th style="background-color: royalblue;border: 1px solid black;">M-1</th>
                                            <th style="background-color: yellow;border: 1px solid black">M</th>
                                            <th style="background-color: yellow;border: 1px solid black">M-1</th>
                                            <th style="background-color: lightgreen;border: 1px solid black">M</th>
                                            <th style="background-color: lightgreen;border: 1px solid black">M-1</th>
                                            <th style="background-color: lightgrey;border: 1px solid black">M</th>
                                            <th style="background-color: lightgrey;border: 1px solid black">M-1</th>
                                            <th style="background-color: lightskyblue;border: 1px solid black">M</th>
                                            <th style="background-color: lightskyblue;border: 1px solid black">M-1</th>
                                            <th style="background-color: greenyellow;border: 1px solid black">M</th>
                                            <th style="background-color: greenyellow;border: 1px solid black">M-1</th>
                                            <th style="background-color: tomato;border: 1px solid black">M</th>
                                            <th style="background-color: tomato;border: 1px solid black">M-1</th>
                                            <th style="background-color: powderblue;border: 1px solid black">M</th>
                                            <th style="background-color: powderblue;border: 1px solid black">M-1</th>
                                            <th style="background-color: palevioletred;border: 1px solid black">M</th>
                                            <th style="background-color: palevioletred;border: 1px solid black">M-1</th>
                                            <th style="background-color: mediumpurple;border: 1px solid black">M</th>
                                            <th style="background-color: mediumpurple;border: 1px solid black">M-1</th>
                                        </tr>
                                        <tbody>
                                        <tr id="leaseunit"></tr>
                                        <tr id="leasems"></tr>
                                        <tr id="leasemscp"></tr>
                                        </tbody>
                                        <tr><td colspan="15"></td></tr>
                                        <tr style="font-size: 28px">
                                            <th colspan="4" style="background-color: darkseagreen;border: 1px solid black">NPBT M</th>
                                            <th colspan="5" style="background-color: darkgrey;border: 1px solid black"><label id="NPBTMN"></th>
                                            <th colspan="4" rowspan="2" style="border: 1px solid black;background-color: lightgrey;vertical-align: middle;">GROWTH</th>
                                            <th colspan="4" style="background-color: yellow;border: 1px solid black">SALES</th>
                                            <th colspan="4" style="background-color: coral;border: 1px solid black">TUNAI</th>
                                            <th colspan="4" style="background-color: deepskyblue;border: 1px solid black">CREDIT</th>
                                            <th colspan="4" style="background-color: lightgreen;border: 1px solid black">NPBT</th>
                                        </tr>
                                        <tr style="font-size: 28px">
                                            <th colspan="4" style="background-color: darkseagreen;border: 1px solid black">NPBT M-1</th>
                                            <th colspan="5" style="background-color: darkgrey;border: 1px solid black"><label id="NPBTMB"></th>
                                            <th colspan="4" id="colorSalesGrowth"><label id="SalesGrowth"></th>
                                            <th colspan="4" id="colorTunaiGrowth"><label id="TunaiGrowth"></th>
                                            <th colspan="4" id="colorCreditGrowth"><label id="CreditGrowth"></th>
                                            <th colspan="4" id="colorNPBTGrowth"><label id="NPBTGrowth"></th>
                                        </tr>
                                        <tr><td colspan="15"></td></tr>
                                        <tr style="font-size: 14px">
                                            <th rowspan="3" colspan="9" style="background-color: #b9def0;border: 1px solid black;vertical-align: middle;">DISTRIBUSI UNIT</th>
                                        </tr>
                                        <tr style="font-size: 14px">
                                            <th colspan="6" style="background-color: yellow;border: 1px solid black">BULAN M</th>
                                            <th colspan="6" style="background-color: cadetblue;border: 1px solid black">BULAN M-1</th>
                                            <th colspan="6" style="background-color: lightcoral;border: 1px solid black">GROWTH</th>
                                        </tr>
                                        <tr style="font-size: 14px">
                                            <th colspan="6" style="background-color: #b9bbbe;border: 1px solid black"><label id="DistribusiMN"></th>
                                            <th colspan="6" style="background-color: lightgrey;border: 1px solid black"><label id="DistribusiMB"></th>
                                            <th colspan="6" id="colorDistribusiGrowth"><label id="DistribusiGrowth"></th>
                                        </tr>

                                        </thead>
                                        <tbody id="rank"></tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                </div>

                <div class="row">
                    <!-- SALES DAN DISTRIBUSI 5 HARIAN -->
                    <div class="col-md-12">
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">SALES DAN DISTRIBUSI 5 HARIAN</h3>
                            </div>
                            <i id="ajaxspinner2" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-22">
                                    <div class="1">
                                        <canvas id="myChartSales" ></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer"></div>
                    </div>
                    <!-- SALES & DO LAST 5 DAY -->
                    <div class="col-md-12">
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">SALES & DO LAST 5 DAY</h3>
                            </div>
                            <i id="ajaxspinner3" class="fas fa-spinner fa-spin fa-3x fa-fw" style="display:none"></i>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-22">
                                    <div class="2">
                                        <canvas id="myChartSalesDo" ></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer"></div>
                    </div>
                </div>
            </div>
            <div class="box-footer">

            </div>
            <?= Html::endForm(); ?>
        </div>
    </div>
<?php
