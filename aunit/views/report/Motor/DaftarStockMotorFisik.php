<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\checkbox\CheckboxX;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
$this->title                   = 'Stock Motor';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;


$sqlTypeMotor = Yii::$app->db
	->createCommand( "
	SELECT 'Semua' AS label, '%' AS value ,  '' AS MotorType
	UNION
	SELECT MotorType As label, MotorType AS value, MotorType FROM tdmotortype  
	ORDER BY MotorType " )
	->queryAll();
$cmbTypeMotor = ArrayHelper::map( $sqlTypeMotor, 'value', 'label' );

$sqlWarnaMotor = Yii::$app->db
	->createCommand( "
	SELECT 'Semua' AS label, '%' AS value ,  '' AS MotorWarna
	UNION
	SELECT MotorWarna As label, MotorWarna AS value, MotorWarna FROM tdmotorwarna  
	ORDER BY MotorWarna  " )
	->queryAll();
$cmbWarnaMotor = ArrayHelper::map( $sqlWarnaMotor, 'value', 'label' );

$sqlDealer = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, '' AS DealerStatus, '' AS DealerKode
	UNION
	SELECT DealerKode AS value, DealerNama AS label, DealerStatus, DealerKode FROM tdDealer 
	ORDER BY DealerStatus, DealerKode" )
	->queryAll();
$cmbDealer = ArrayHelper::map( $sqlDealer, 'value', 'label' );

$sqlWarehouse = Yii::$app->db
	->createCommand( "
    SELECT '%' AS value, 'Semua' AS label, '' AS LokasiStatus, '' AS LokasiJenis, '' AS LokasiKode
    UNION
	SELECT LokasiKode AS value, LokasiNama AS label, LokasiStatus, LokasiJenis, LokasiKode FROM tdlokasi WHERE LokasiStatus IN ('A','1') 
    AND LokasiJenis <> 'Kas' ORDER BY LokasiStatus, LokasiKode" )
	->queryAll();
$cmbWarehouse = ArrayHelper::map( $sqlWarehouse, 'value', 'label' );

$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
function formatDual(result) {
    return '<div class="row">' +
           '<div class="col-md-5">' + result.id + '</div>' +
           '<div class="col-md-19">' + result.text + '</div>' +
           '</div>';
}
function formatWarehouse(result) {
    return '<div class="row">' +
           '<div class="col-md-7">' + result.id + '</div>' +
           '<div class="col-md-17">' + result.text + '</div>' +
           '</div>';
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );

?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Motor1' => 'Motor1',
									'Motor2' => 'Motor2',
									'Motor3' => 'Motor3',
									'Stock PS 2' => 'Stock PS 2',
									'Detail Motor Type' => 'Detail Motor Type',
									'Detail Motor Lokasi' => 'Detail Motor Lokasi',
									'Stock Opname' => 'Stock Opname',
									'Stock In Transit' => 'Stock In Transit',
									'Umur Motor' => 'Umur Motor',
									'Detail HPP' => 'Detail HPP',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-footer">
                <div class="container-fluid">
				    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Tanggal</label>
                        </div>
                    </div>
				    <div class="col-md-11">
                        <div class="form-group">
							<div class="col-md-8">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
                                        'options'       => ['id' => 'tgl1'],
										'attribute2'    =>'to_date',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy' ),
										'pluginOptions' => [
											'autoclose'      => true,
											'format'         => 'dd/mm/yyyy',
											'todayHighlight' => true]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
							<div class="col-md-8">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
                                        'options'       => ['id' => 'tgl2'],
										'attribute2'    =>'to_date',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy' ),
										'pluginOptions' => [
											'autoclose'      => true,
											'format'         => 'dd/mm/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
                        </div>
                        <div class="form-group" id ="cek1">
                            <div class="col-sm-6">
                                <? try {
                                    echo CheckboxX::widget([
                                        'name'=>'quick',
                                        'value' => 1,
                                        'options'=>['id'=>'s_1'],
                                        'pluginOptions'=>['threeState'=>false, 'size'=>'lg']
                                    ]);
                                    echo '<label class="cbx-label" for="s_1">Quick</label>';
                                } catch ( \yii\base\InvalidConfigException $e ) {
                                } ?>
                            </div>
                        </div>
                        <div class="form-group" id ="cek2">
                            <div class="col-sm-6">
                                <? try {
                                    echo CheckboxX::widget([
                                        'name'=>'fbpd',
                                        'value' => 0,
                                        'options'=>['id'=>'fbpd'],
                                        'pluginOptions'=>['threeState'=>false, 'size'=>'lg']
                                    ]);
                                    echo '<label class="cbx-label" for="fbpd" >FB/PD</label>';
                                } catch ( \yii\base\InvalidConfigException $e ) {
                                } ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" id="sort">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label col-sm-24 jurnalHide">Sort</label>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <div class="col-sm-18">
                                    <?= Html::dropDownList( 'sort', null, [
                                        "tdmotortype.MotorType"	  => 	"Type Motor",
                                        "tdmotortype.MotorNama"   =>    "Nama Motor",
                                        "tmotor.MotorTahun"       =>    "Tahun Motor",
                                        "FBHarga"                 =>    "Harga",
                                        "Jumlah"                  =>    "Jumlah",
                                        "LokasiMotor.LokasiKode"  =>    "Lokasi",
                                    ], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= Html::dropDownList( 'order', null, [
                                        "ASC" => "ASC",
                                        "DESC" => "DESC",
                                    ], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                                </div>
                            </div>
                        </div>
                    </div>



	            </div>
                <div class="container-fluid">
				    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Lokasi</label>
                        </div>
                    </div>
					<div class="col-md-11">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? echo Select2::widget( [
									'name'          => 'lokasi',
									'data'          => $cmbWarehouse,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatWarehouse' ),
										'templateSelection' => new JsExpression( 'formatWarehouse' ),
										'matcher'           => new JsExpression( 'matchCustom' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
                            </div>
                        </div>
                    </div>
					<div class="col-md-2">
                        <div class="form-group">
						     <label class="control-label col-sm-24" id = "tahun">Tahun</label>
                        </div>
                    </div>
					<div class="col-md-9">
                        <div class="form-group">
							<div class="col-md-6">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tahun1',
                                        'options'       => ['id' => 'tahun1'],
										'attribute2'    =>'to_date',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate('-5 year','yyyy'),
										'pluginOptions' => [
											'startView'=>'year',
											'minViewMode'=>'months',
											'format' => 'yyyy'										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
							<div class="col-md-6">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tahun2',
                                        'options'       => ['id' => 'tahun2'],
										'attribute2'    =>'to_date',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','yyyy' ),
										'pluginOptions' => [
											'startView'=>'year',
											'minViewMode'=>'months',
											'format' => 'yyyy'										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
                        </div>
                    </div>
	            </div>
                <div class="container-fluid">
				    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Dealer</label>
                        </div>
                    </div>
					<div class="col-md-11">
                        <div class="form-group">
                             <div class="col-sm-24">
								<? echo Select2::widget( [
									'name'          => 'dealer',
									'data'          => $cmbDealer,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatDual' ),
										'templateSelection' => new JsExpression( 'formatDual' ),
										'matcher'           => new JsExpression( 'matchCustom' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
                            </div>
						</div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Status Type</label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <div class="col-sm-10">
                                <?= Html::dropDownList( 'status', null, [
                                    "%"					=> 		"Semua",
                                    "A"                 =>       "Aktif",
                                    "N"                 =>       "Non Aktif",
                                ], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
                            <div class="col-sm-1">
                            </div>
                            <label class="control-label col-sm-4">Kondisi</label>
                            <div class="col-sm-9">
                                <?= Html::dropDownList( 'kondisi', null, [
                                    "1" => "IN & OUT Pos",
                                    "2" => "IN",
                                    "3" => "OUT",
                                    "4" => "OUT Pos",
                                ], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
                        </div>
                    </div>
	            </div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
$this->registerJs( $this->render( 'DaftarStockMotorFisik.js' ) );
 