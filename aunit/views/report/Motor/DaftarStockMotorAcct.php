<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\checkbox\CheckboxX;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
$this->title                   = 'Daftar Stock Motor Akuntansi';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;
?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
								'Motor Stock'     => 'Motor Stock',
								'Tabel Stock'     => 'Tabel Stock',
								'Detail Motor'    => 'Detail Motor',
								'Type Motor'      => 'Type Motor',
								'Track HPP'       => 'Track HPP',
								'Stok PS 1'       => 'Stok PS 1',
								'Stok PS 2'       => 'Stok PS 2',
								'Stock Intransit' => 'Stock Intransit',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-footer">
                <div class="container-fluid">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Tanggal</label>
                        </div>
                    </div>
                    <div class="col-md-16">
                        <div class="form-group">
                            <div class="col-md-5">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
										'pluginOptions' => [
											'autoclose'      => true,
											'format'         => 'dd/mm/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                            <div class="col-md-5">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
							<label class="control-label col-sm-2">Kondisi</label>
							<div class="col-md-5">
								<?= Html::dropDownList( 'kondisi', null, [
									 "%"        => "Semua",        
									 "IN"        => "IN",        
									 "OUT"        => "OUT",        
								], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
							</div>
							<div class="col-sm-4">
								<? try {
									echo CheckboxX::widget([
										'name'=>'ringkas',
										'options'=>['id'=>'ringkas'],
										'pluginOptions'=>['threeState'=>false, 'size'=>'lg']
									]);
									echo '<label class="cbx-label" for="ringkas">Ringkas</label>';
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
                        </div>
                    </div>
                </div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
