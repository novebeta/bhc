<?php

use aunit\components\FormField;
use aunit\models\Tdmotortype;
use aunit\models\Tdmotorwarna;
use aunit\models\Tdsales;
use aunit\models\Tdteam;
use kartik\date\DatePicker;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use kartik\checkbox\CheckboxX;

$this->title = 'Laporan STNK & BPKB';
$this->params['breadcrumbs'][] = $this->title;
$formatter = \Yii::$app->formatter;

$sqlLeasing = Yii::$app->db
    ->createCommand("
	SELECT '%' AS value, 'Semua' AS label, '' AS LeaseStatus, '' AS LeaseKode
	UNION
	SELECT LeaseKode AS value, LeaseKode As label , LeaseStatus , LeaseKode FROM tdLeasing 
	WHERE LeaseStatus = 'A' 
	ORDER BY LeaseStatus, LeaseKode")
    ->queryAll();
$cmbLeasing = ArrayHelper::map($sqlLeasing, 'value', 'label');

$sqlSales = Yii::$app->db
    ->createCommand("
	SELECT '%' AS value, 'Semua' AS label, '' AS SalesStatus, '' AS SalesKode
	UNION
	SELECT SalesKode AS value, SalesNama As label , SalesStatus , SalesKode FROM tdSales
	WHERE SalesStatus = 'A' 
	ORDER BY SalesStatus, SalesKode")
    ->queryAll();
$cmbSales = ArrayHelper::map($sqlSales, 'value', 'label');

$format = <<< SCRIPT
function formatDual(result) {
   return '<div class="row">' +
           '<div class="col-md-8" style="padding-left:10px; ">' + result.id + '</div>' +
           '<div class="col-md-16">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
            <?= Html::beginForm(); ?>
            <div class="box-body">
                <div class="container-fluid">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Laporan</label>
                            <div class="col-sm-20">
                                <?= Html::dropDownList('tipe', null, [
                                    'Faktur AHM' => 'Faktur AHM',
                                    'Pengajuan' => 'Pengajuan',
                                    'STNK' => 'STNK',
                                    'Notice' => 'Notice',
                                    'Plat Nomor' => 'Plat Nomor',
                                    'BPKB' => 'BPKB',
                                    'STNK - BPKB' => 'STNK - BPKB',
                                    'Notice - STNK' => 'Notice - STNK',
                                    'Notice Opname' => 'Notice Opname',
                                    'STNK Opname' => 'STNK Opname',
                                    'Plat Opname' => 'Plat Opname',
                                    'BPKB Opname' => 'BPKB Opname',
                                    'BPKB Opname 2' => 'BPKB Opname 2',
                                    'SRUT Opname' => 'SRUT Opname',
                                    'Pengajuan BBN' => 'Pengajuan BBN',
                                    'WIP BBN' => 'WIP BBN',
                                    'LT BBN - AB' => 'LT BBN - AB',
                                    'LT BBN - SK' => 'LT BBN - SK',
                                ], ['id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Format</label>
                            <div class="col-sm-20">
                                <?= Html::dropDownList('filetype', null, [
                                    'pdf' => 'PDF',
                                    'xlsr' => 'EXCEL RAW',
                                    'xls' => 'EXCEL',
                                    'doc' => 'WORD',
                                    'rtf' => 'RTF',
                                ], ['text' => 'Pilih format laporan', 'class' => 'form-control']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="container-fluid">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label col-sm-7"  id="lbl_1">Tgl SK</label>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3 cek1" data-toggle="tooltip" title="Semua" id="tooltip1">
                                <? try {
                                    echo CheckboxX::widget([
                                        'name'          => 'cek1',
                                        'options'       => ['id' => 'cek1',],
                                        'value'         => 0,
                                        'pluginOptions' => ['threeState' => true, 'size' => 'lg' ],
                                        'pluginEvents'  => [
                                            "change"    => new JsExpression("function(e) {
                                            var tgl_1 = $('#tgl1');
                                            var tgl_2 = $('#tgl2');
                                            var tooltip1 = $('#tooltip1');
                                            var tipe = $('#tipe-id').val();
											 if($(this).val() === '0'){
											    tgl_1.kvDatepicker('update', '01/01/1900')
											    tgl_2.kvDatepicker('update', '01/01/2078')
											    tooltip1.attr('title', 'Semua')                                                                                                                                                                                                   
											 }
											 else if($(this).val() === '1'){
											    tgl_1.kvDatepicker('update', '01/01/2000')
											    tgl_2.kvDatepicker('update', '01/01/2078')	
											    if(tipe == 'Faktur AHM'){
											    tooltip1.attr('title', 'Faktur AHM Sudah Diajukan')
											    }
											    else if (tipe == 'Pengajuan' || tipe == 'STNK'){
											    tooltip1.attr('title', 'STNK Sudah Masuk')
											    }
											    else if (tipe == 'Notice'){
											    tooltip1.attr('title', 'Notice Sudah Jadi')
											    }    
											    else if (tipe == 'Plat Nomor'){
											    tooltip1.attr('title', 'Plat Sudah Jadi')
											    }    	
											    else if (tipe == 'BPKB'){
											    tooltip1.attr('title', 'BPKB Sudah Jadi')
											    }
											    else if (tipe == 'STNK - BPKB'){
											    tooltip1.attr('title', 'STNK Sudah Jadi')
											    } 									    							    
											 }
											 else if($(this).val() === ''){
											    tgl_1.kvDatepicker('update', '01/01/1900')
											    tgl_2.kvDatepicker('update', '01/01/2078')
											    if(tipe == 'Faktur AHM'){
											    tooltip1.attr('title', 'Faktur AHM Belum Diajukan')
											    }
											    else if (tipe == 'Pengajuan' || tipe == 'STNK'){
											    tooltip1.attr('title', 'STNK Belum Masuk')
											    }
											    else if (tipe == 'Notice'){
											    tooltip1.attr('title', 'Notice Belum Jadi')
											    } 
											    else if (tipe == 'Plat Nomor'){
											    tooltip1.attr('title', 'Plat Belum Jadi')
											    } 
											    else if (tipe == 'BPKB'){
											    tooltip1.attr('title', 'BPKB Belum Jadi')
											    } 
											    else if (tipe == 'STNK - BPKB'){
											    tooltip1.attr('title', 'STNK Belum Jadi')
											    }
											 }
											}")
                                        ]
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                            <div class="col-md-7">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'tgl1',
                                        'options' => ['id' => 'tgl1'],
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => Yii::$app->formatter->asDate('01/01/1900', 'dd/MM/yyyy'),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                            'todayHighlight' => true,
                                            'todayBtn' => true,
                                        ]
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                            <div class="col-md-7">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'tgl2',
                                        'options' => ['id' => 'tgl2'],
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => Yii::$app->formatter->asDate('now', '01/01/2078'),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                            'todayHighlight' => true,
                                            'todayBtn' => true,
                                        ]
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label col-sm-7" id="lbl_2">Tgl Jadi Notice</label>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3 cek2" data-toggle="tooltip" title="Semua" id="tooltip2">
                                <? try {
                                    echo CheckboxX::widget([
                                        'name' => 'cek2',
                                        'options' => ['id' => 'cek2'],
                                        'value'   => 0,
                                        'pluginOptions' => ['threeState' => true, 'size' => 'lg'],
                                        'pluginEvents'  => [
                                            "change"    => new JsExpression("function(e) {
                                            var tgl_3 = $('#tgl3');
                                            var tgl_4 = $('#tgl4');
                                            var tooltip2 = $('#tooltip2');
                                            var tipe = $('#tipe-id').val();
											 if($(this).val() === '0'){
											    tgl_3.kvDatepicker('update', '01/01/1900')
											    tgl_4.kvDatepicker('update', '01/01/2078')
											    tooltip2.attr('title', 'Semua')
											 }
											 else if($(this).val() === '1'){
											    tgl_3.kvDatepicker('update', '01/01/2000')
											    tgl_4.kvDatepicker('update', '01/01/2078')
											    if(tipe == 'Pengajuan' || tipe == 'STNK'){
											        tooltip2.attr('title', 'STNK Sudah Jadi')
											    }
											    else if (tipe == 'Notice'){
											        tooltip2.attr('title', 'Notice Sudah Diambil')
											    }		
											    else if (tipe == 'Plat Nomor'){
											    tooltip2.attr('title', 'Plat Sudah Diambil')
											    }
											    else if (tipe == 'BPKB'){
											    tooltip2.attr('title', 'BPKB Sudah Diambil')
											    } 
											    else if (tipe == 'STNK - BPKB'){
											    tooltip2.attr('title', 'STNK Sudah Diambil')
											    } 
											    else if (tipe == 'Pengajuan BBN' || tipe ==  'WIP BBN' || tipe ==  'LT BBN - AB' || tipe ==  'LT BBN - SK'){
											    tooltip2.attr('title', 'Pengajuan Sudah Dibuat')
											    } 									    
											 }
											 else if($(this).val() === ''){
											    tgl_3.kvDatepicker('update', '01/01/1900')
											    tgl_4.kvDatepicker('update', '01/01/2078')											    
											    if(tipe == 'Pengajuan' || tipe == 'STNK'){
											        tooltip2.attr('title', 'STNK Belum Jadi')
											    }
											    else if (tipe == 'Notice'){
											        tooltip2.attr('title', 'Notice Belum Diambil')
											    }
											    else if (tipe == 'Plat Nomor'){
											    tooltip2.attr('title', 'Plat Belum Diambil')
											    } 
											    else if (tipe == 'BPKB'){
											    tooltip2.attr('title', 'BPKB Belum Diambil')
											    } 
											    else if (tipe == 'STNK - BPKB'){
											    tooltip2.attr('title', 'STNK Belum Diambil')
											    }
											    else if (tipe == 'Pengajuan BBN' || tipe ==  'WIP BBN' || tipe ==  'LT BBN - AB' || tipe ==  'LT BBN - SK'){
											    tooltip2.attr('title', 'Pengajuan Belum Dibuat')
											    } 
											 }
											}")
                                        ]
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                            <div class="col-md-7">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'tgl3',
                                        'options' => ['id' => 'tgl3'],
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => Yii::$app->formatter->asDate('01/01/1900', '01/MM/yyyy'),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                            'todayHighlight' => true,
                                            'todayBtn' => true,
                                        ]
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                            <div class="col-md-7">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'tgl4',
                                        'options' => ['id' => 'tgl4'],
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => Yii::$app->formatter->asDate('01/01/2078', 'dd/MM/yyyy'),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                            'todayHighlight' => true,
                                            'todayBtn' => true,
                                        ]
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label col-sm-7" id="lbl_3">Tgl Jadi BPKB</label>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3 cek3">
                                <? try {
                                    echo CheckboxX::widget([
                                        'name' => 'cek3',
                                        'options' => ['id' => 'cek3'],
                                        'value'=> 0,
                                        'pluginOptions' => ['threeState' => true, 'size' => 'lg'],
                                        'pluginEvents'  => [
                                            "change"    => new JsExpression("function(e) {
                                            var tgl_5 = $('#tgl5');
                                            var tgl_6 = $('#tgl6');
											 if($(this).val() === '0'){
											    tgl_5.kvDatepicker('update', '01/01/1900')
											    tgl_6.kvDatepicker('update', '01/01/2078')
											 }
											 else if($(this).val() === '1'){
											    tgl_5.kvDatepicker('update', '01/01/2000')
											    tgl_6.kvDatepicker('update', '01/01/2078')
											 }
											 else if($(this).val() === ''){
											    tgl_5.kvDatepicker('update', '01/01/1900')
											    tgl_6.kvDatepicker('update', '01/01/2078')
											 }
											}")
                                        ]
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                            <div class="col-md-7">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'tgl5',
                                        'options' => ['id' => 'tgl5'],
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => Yii::$app->formatter->asDate('01/01/1900', '01/MM/yyyy'),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                            'todayHighlight' => true,
                                            'todayBtn' => true,
                                        ]
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                            <div class="col-md-7">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'tgl6',
                                        'options' => ['id' => 'tgl6'],
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => Yii::$app->formatter->asDate('01/01/2078', 'dd/MM/yyyy'),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                            'todayHighlight' => true,
                                            'todayBtn' => true,
                                        ]
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label col-sm-7" id="lbl_4">Tgl Pengajuan</label>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3 cek4" data-toggle="tooltip" title="Semua" id="tooltip4">
                                <? try {
                                    echo CheckboxX::widget([
                                        'name' => 'cek4',
                                        'options' => ['id' => 'cek4'],
                                        'value' => 0,
                                        'pluginOptions' => ['threeState' => true, 'size' => 'lg'],
                                        'pluginEvents'  => [
                                            "change"    => new JsExpression("function(e) {
                                            var tgl_7 = $('#tgl7');
                                            var tgl_8 = $('#tgl8');
                                            var tooltip4 = $('#tooltip4');
                                            var tipe = $('#tipe-id').val();
											 if($(this).val() === '0'){
											    tgl_7.kvDatepicker('update', '01/01/1900')
											    tgl_8.kvDatepicker('update', '01/01/2078')
											    tooltip4.attr('title', 'Semua')	
											 }
											 else if($(this).val() === '1'){
											    tgl_7.kvDatepicker('update', '01/01/2000')
											    tgl_8.kvDatepicker('update', '01/01/2078')
											    if(tipe == 'Faktur AHM'){
											    tooltip4.attr('title', 'Faktur Sudah Jadi')
											    }
											    else if (tipe == 'Pengajuan' || tipe == 'STNK'){
											    tooltip4.attr('title', 'STNK Sudah Diambil')
											    }
											    else if (tipe == 'STNK - BPKB'){
											    tooltip4.attr('title', 'BPKB Sudah Jadi')
											    }
											 }
											 else if($(this).val() === ''){
											    tgl_7.kvDatepicker('update', '01/01/1900')
											    tgl_8.kvDatepicker('update', '01/01/2078')											    
											    if(tipe == 'Faktur AHM'){
											    tooltip4.attr('title', 'Faktur Belum Jadi')
											    }
											    else if (tipe == 'Pengajuan' || tipe == 'STNK'){
											    tooltip4.attr('title', 'STNK Belum Diambil')
											    }
											    else if (tipe == 'STNK - BPKB'){
											    tooltip4.attr('title', 'BPKB Belum Jadi')
											    }
											 }
											}")
                                        ]
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                            <div class="col-md-7">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'tgl7',
                                        'options' => ['id' => 'tgl7'],
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => Yii::$app->formatter->asDate('01/01/1900', '01/01/1900'),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                            'todayHighlight' => true,
                                            'todayBtn' => true,
                                        ]
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                            <div class="col-md-7">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'tgl8',
                                        'options' => ['id' => 'tgl8'],
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => Yii::$app->formatter->asDate('01/01/1900', '01/01/2078'),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                            'todayHighlight' => true,
                                            'todayBtn' => true,
                                        ]
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label col-sm-7" id="lbl_5">Tgl Jadi STNK</label>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3 cek5" data-toggle="tooltip" title="Semua" id="tooltip5">
                                <? try {
                                    echo CheckboxX::widget([
                                        'name' => 'cek5',
                                        'options' => ['id' => 'cek5'],
                                        'value' => 0,
                                        'pluginOptions' => ['threeState' => true, 'size' => 'lg'],
                                        'pluginEvents'  => [
                                            "change"    => new JsExpression("function(e) {
                                            var tgl_9 = $('#tgl9');
                                            var tgl_10 = $('#tgl10');
                                            var tooltip5 = $('#tooltip5');
                                            var tipe = $('#tipe-id').val();
											 if($(this).val() === '0'){
											    tgl_9.kvDatepicker('update', '01/01/1900')
											    tgl_10.kvDatepicker('update', '01/01/2078')
											    if(tipe == 'Faktur AHM'){
											        tooltip5.attr('title', 'Semua')
											    }
											    else if (tipe == 'Pengajuan' || tipe == 'STNK'){
											        tooltip5.attr('title', 'Bulan Ini')
											    }
											 }
											 else if($(this).val() === '1'){
											    tgl_9.kvDatepicker('update', '01/01/2000')
											    tgl_10.kvDatepicker('update', '01/01/2078')
											    if(tipe == 'Faktur AHM'){
											        tooltip5.attr('title', 'Faktur Sudah Diambil')
											    }
											    else if (tipe == 'Pengajuan' || tipe == 'STNK'){
											        tooltip5.attr('title', 'SK Semua')
											    }
											    else if (tipe == 'STNK - BPKB'){
											    tooltip5.attr('title', 'BPKB Sudah Diambil')
											    }
											 }
											 else if($(this).val() === ''){
											    tgl_9.kvDatepicker('update', '01/01/1900')
											    tgl_10.kvDatepicker('update', '01/01/2078')
											    if(tipe == 'Faktur AHM'){
											        tooltip5.attr('title', 'Faktur Belum Diambil')
											    }
											    else if (tipe == 'Pengajuan' || tipe == 'STNK'){
											        tooltip5.attr('title', '')
											    }
											    else if (tipe == 'STNK - BPKB'){
											        tooltip5.attr('title', 'BPKB Belum Diambil')
											    }
											 }
											}")
                                        ]
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                            <div class="col-md-7">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'tgl9',
                                        'options' => ['id' => 'tgl9'],
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => Yii::$app->formatter->asDate('01/01/1900', '01/01/1900'),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                            'todayHighlight' => true,
                                            'todayBtn' => true,
                                        ]
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                            <div class="col-md-7">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'tgl10',
                                        'options' => ['id' => 'tgl10'],
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => Yii::$app->formatter->asDate('01/01/1900', '01/01/2078'),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                            'todayHighlight' => true,
                                            'todayBtn' => true,
                                        ]
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label col-sm-7" id="lbl_6">Tgl Jadi Plat</label>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3 cek6" data-toggle="tooltip" title="SK Semua" id="tooltip6" >
                                <? try {
                                    echo CheckboxX::widget([
                                        'name'      => 'cek6',
                                        'options'   => ['id' => 'cek6'],
                                        'value'     => true,
                                        'pluginOptions' => ['threeState' => true, 'size' => 'lg'],
                                        'pluginEvents'  => [
                                            "change"    => new JsExpression("function(e) {
                                            var tgl_11 = $('#tgl11');
                                            var tgl_12 = $('#tgl12');
                                            var tooltip6 = $('#tooltip6');
											 if($(this).val() === '0'){
											    tgl_11.kvDatepicker('update', '01/01/1900')
											    tgl_12.kvDatepicker('update', '01/01/2078')
											    tooltip6.attr('title', 'Bulan Ini')
											 }
											 else if($(this).val() === '1'){
											    tgl_11.kvDatepicker('update', '01/01/2000')
											    tgl_12.kvDatepicker('update', '01/01/2078')
											    tooltip6.attr('title', 'SK Semua')
											 }
											 else if($(this).val() === ''){
											    tgl_11.kvDatepicker('update', '01/01/1900')
											    tgl_12.kvDatepicker('update', '01/01/2078')
											    tooltip6.attr('title', '')
											 }
											}")
                                        ]
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                            <div class="col-md-7">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'tgl11',
                                        'options' => ['id' => 'tgl11'],
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => Yii::$app->formatter->asDate('01/01/1900', '01/01/1900'),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                            'todayHighlight' => true,
                                            'todayBtn' => true,
                                        ]
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                            <div class="col-md-7">
                                <? try {
                                    echo DatePicker::widget([
                                        'name' => 'tgl12',
                                        'options' => ['id' => 'tgl12'],
                                        'type' => DatePicker::TYPE_INPUT,
                                        'value' => Yii::$app->formatter->asDate('01/01/1900', '01/01/2078'),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                            'todayHighlight' => true,
                                            'todayBtn' => true,
                                        ]
                                    ]);
                                } catch (\yii\base\InvalidConfigException $e) {
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label col-sm-7" id="lbl_7">Belum Jadi</label>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-17">
                                <?= Html::dropDownList('cboBelumJadi', null, [
                                    "Semua Belum" => "Semua Belum",
                                    "Notice" => "Notice",
                                    "STNK" => "STNK",
                                    "BPKB" => "BPKB",
                                    "Plat" => "Plat",
                                    "Semua" => "Semua",
                                    "Semua Jadi" => "Semua Jadi",
                                ], ['text' => '', 'class' => 'form-control', 'id' => 'blm_jadi']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label col-sm-7" id="lbl_8">Status</label>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-17">
                                <?= Html::dropDownList('status', null, [
                                    "Sudah" => "Sudah",
                                    "Belum" => "Belum",
                                    "Semua" => "Semua",
                                ], ['text' => '', 'class' => 'form-control status', 'id' => 'status']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="container-fluid">
                    <div class="col-md-1">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Sort</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-sm-16">
                                <?= Html::dropDownList('sort', null, [
                                    "MotorType" => "Type Motor",
                                    "CusKabupaten" => "Kabupaten",
                                    "MotorWarna" => "Warna Motor",
                                    "MotorTahun" => "Tahun",
                                    "DKNo" => "No Data Konsumen",
                                    "SKNo" => "No Surat Jln Konsumen",
                                    "DKTgl" => "Tgl Data Kons ",
                                    "CusNama" => "Nama Konsumen",
                                    "SalesNama" => "Nama Sales",
                                    "LeaseKode" => "Leasing",
                                    "DKJenis" => "Jenis",
                                    "FakturAHMNo" => "No Faktur AHM",
                                    "NoticeNo" => "No Notice",
                                    "STNKNo" => "No STNK",
                                    "PlatNo" => "No Plat",
                                    "BPKBNo" => "No BPKB",
                                    "STNKNama" => "Nama STNK",
                                    "STNKAlamat" => "Alamat STNK",
                                    "NoticePenerima" => "Penerima Notice",
                                    "STNKPenerima" => "Penerima STNK",
                                    "PlatPenerima" => "Penerima Plat",
                                    "BPKBPenerima" => "Penerima BPKB",
                                    "STNKTglBayar" => "Tgl Ajukan Faktur",
                                    "FakturAHMTgl" => "Tgl Jadi Faktur AHM ",
                                    "FakturAHMTglAmbil" => "Tgl Ambil Faktur AHM",
                                    "STNKTglMasuk" => "Tgl Ajukan STNK",
                                    "STNKTglJadi" => "Tgl Jadi STNK",
                                    "STNKTglAmbil" => "Tgl Ambil STNK",
                                    "PlatTgl" => "Tgl Jadi Plat ",
                                    "PlatTglAmbil" => "Tgl Ambil Plat",
                                    "NoticeTglJadi" => "Tgl Jadi Notice",
                                    "NoticeTglAmbil" => "Tgl Ambil Notice",
                                    "BPKBTglJadi" => "Tgl Jadi BPKB",
                                    "BPKBTglAmbil" => "Tgl Ambil BPKB",
                                    "DKMemo" => "Keterangan",
                                    "UserID" => "User",
                                    "CusKode" => "Kode Konsumen",
                                    "SalesKode" => "Kode Sales ",
                                    "DKHarga" => "Harga OTR",
                                    "DKHPP" => "HPP",
                                    "DKDPTotal" => "DP Total",
                                    "DKDPLLeasing" => "DP Leasing",
                                    "DKDPTerima" => "DP Terima",
                                    "DKNetto" => "Netto",
                                    "ProgramSubsidi" => "Subsidi Program",
                                    "PrgSubsSupplier" => "Subsidi Supplier",
                                    "PrgSubsDealer" => "Subsidi Dealer",
                                    "PrgSubsFincoy" => "Subsidi Fincoy",
                                    "PotonganHarga" => "Potongan Harga",
                                    "ReturHarga" => "Retur Harga",
                                    "PotonganKhusus" => "Potongan Khusus",
                                    "BBN" => "BBN",
                                    "INNo" => "No Inden",
                                    "NamaHadiah" => "Nama Hadiah",
                                    "ProgramNama" => "Nama Program",
                                    "DKLunas" => "Lunas",
                                ], ['text' => '', 'class' => 'form-control jurnalHide']) ?>
                            </div>
                            <div class="col-sm-8">
                                <?= Html::dropDownList('order', null, [
                                    "ASC" => "ASC",
                                    "DESC" => "DESC",
                                ], ['text' => '', 'class' => 'form-control']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Cari</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-sm-13">
                                <?= Html::dropDownList('cari', null, [
                                    "CusNama" => "Nama Konsumen",
                                    "STNKNama" => "Nama STNK",
                                    "STNKAlamat" => "Alamat STNK",
                                    "FakturAHMNo" => "No Faktur AHM",
                                    "SalesNama" => "Nama Sales",
                                    "MotorNoMesin" => "No Mesin",
                                    "MotorNoRangka" => "No Rangka",
                                    "MotorType" => "Type Motor",
                                    "MotorWarna" => "Warna Motor",
                                    "MotorTahun" => "Tahun",
                                    "STNKNo" => "No STNK",
                                    "NoticeNo" => "No Notice",
                                    "PlatNo" => "No Plat",
                                    "BPKBNo" => "No BPKB",
                                    "NoticePenerima" => "Penerima Notice",
                                    "STNKPenerima" => "Penerima STNK",
                                    "PlatPenerima" => "Penerima Plat",
                                    "BPKBPenerima" => "Penerima BPKB",
                                    "DKNo" => "No Data Konsumen",
                                    "SKNo" => "No Surat Jln Konsumen",
                                    "INNo" => "No Inden",
                                    "CusKode" => "Kode Konsumen",
                                    "SalesKode" => "Kode Sales ",
                                    "LeaseKode" => "Kode Lease",
                                    "DKMemo" => "Keterangan",
                                    "DKJenis" => "Jenis",
                                    "DKLunas" => "Lunas",
                                    "NamaHadiah" => "Nama Hadiah",
                                    "ProgramNama" => "Nama Program",
                                ], ['text' => '', 'class' => 'form-control jurnalHide']) ?>
                            </div>
                            <div class="col-sm-11">
                                <input type="text" id="caritxt" name="caritxt" class="form-control"> </input>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Leasing</label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-sm-24">
                                <?= Select2::widget([
                                    'name' => 'leasing',
                                    'data' => $cmbLeasing,
                                    'options' => ['class' => 'form-control'],
                                    'theme' => Select2::THEME_DEFAULT,

                                ]); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label class="control-label col-sm-24 jurnalHide">Sales</label>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <div class="col-sm-24">
                                <?= Select2::widget([
                                    'name' => 'sales',
                                    'data' => $cmbSales,
                                    'options' => ['class' => 'form-control'],
                                    'theme' => Select2::THEME_DEFAULT,
                                    'pluginOptions' => [
                                        'dropdownAutoWidth' => true,
                                        'templateResult' => new JsExpression('formatDual'),
                                        'templateSelection' => new JsExpression('formatDual'),
                                        'matcher' => new JsExpression('matchCustom'),
                                        'escapeMarkup' => $escape,
                                    ],
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?= Html::endForm(); ?>
        </div>
    </div>


<?php
$this->registerJs( $this->render( 'DaftarSTNKBPKB.js' ) );
