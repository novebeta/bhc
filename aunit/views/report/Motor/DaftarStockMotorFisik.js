$("#tipe-id").change(function () {
    var tipe = this.value;
    if(tipe === 'Motor1' || tipe === 'Motor2' || tipe === 'Motor3' || tipe ===  'Detail Motor Type' || tipe ===  'Detail Motor Lokasi'){
        $("#tgl2").hide();
        $("#cek2").hide();
        $("#tahun").show();
        $("#tahun1").show();
        $("#tahun2").show();
        $("#sort").show();
    }
    else if(tipe ===  'Stock PS 2' || tipe ===  'Stock In Transit'){
        $("#tgl2").show();
        $("#tahun").hide();
        $("#tahun1").hide();
        $("#tahun2").hide();
        $("#sort").show();
        $("#cek2").hide();
    }
    else if(tipe ===  'Stock Opname' || tipe ===  'Umur Motor'){
        $("#tgl2").hide();
        $("#tahun").hide();
        $("#tahun1").hide();
        $("#tahun2").hide();
        $("#sort").show();
        $("#cek2").hide();
    }
    else if(tipe ===  'Detail HPP'){
        $("#tgl2").hide();
        $("#cek2").show();
        $("#tahun").hide();
        $("#tahun1").hide();
        $("#tahun2").hide();
        $("#sort").hide();
    }
});

$("#tipe-id").trigger('change');