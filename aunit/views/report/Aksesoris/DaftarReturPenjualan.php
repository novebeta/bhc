<?php
use kartik\date\DatePicker;
use yii\helpers\Html;
$this->title                   = 'Daftar [SR] Retur Penjualan';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;
?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
			<div class="box box-primary">
				<?= Html::beginForm(); ?>
				<div class="box-body">
					<div class="container-fluid">
						<div class="col-md-10">
							<div class="form-group">
								<label class="control-label col-sm-4">Laporan</label>
								<div class="col-sm-20">
									<?= Html::dropDownList( 'tipe', null, [
										'Header1' => 'Header1',
										'Item1' => 'Item1',
									], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
								</div>
							</div>
						</div>
						<div class="col-md-7">
							<div class="form-group">
								<label class="control-label col-sm-4">Format</label>
								<div class="col-sm-20">
									<?= Html::dropDownList( 'filetype', null, [
										'pdf'  => 'PDF',
										'xlsr' => 'EXCEL RAW',
										'xls'  => 'EXCEL',
										'doc'  => 'WORD',
										'rtf'  => 'RTF',
									], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
								</div>
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Tanggal</label>
							<div class="col-sm-8">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
							<label class="control-label col-sm-4">s/d</label>
							<div class="col-sm-8">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
						</div>
					</div>
					<div class="col-md-14">
						<div class="form-group">
							<label class="control-label col-sm-2">Status</label>
							<div class="col-sm-5">
								<?= Html::dropDownList( 'status', null, [
								""                                   => "Semua",
								" AND ((SRTotal - SRTerbayar) = 0) " => "Lunas",
								" AND ((SRTotal - SRTerbayar) > 0) " => "Belum",     
							], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
							<label class="control-label col-sm-2">Sort</label>
							<div class="col-sm-8">
								<?= Html::dropDownList( 'Sort', null, [
								"ttsrhd.SRNo"             => "No Retur Jual [SR]",
								"ttsrhd.SRTgl"            => "Tgl  Retur Jual [SR]",
								"ttsrhd.SINo"             => "No Penjualan Inv [SI]",
								"ttsrhd.MotorNoPolisi"    => "No Polisi",
								"tdcustomer.CusNama"      => "Pemilik",
								"tdcustomer.MotorNoMesin" => "No Mesin",
								"ttsrhd.LokasiKode"       => "Lokasi",
								"ttsrhd.KarKode"          => "Kode Karyawan",
								"tdkaryawan.KarNama"      => "Nama Karyawan",
								"ttsrhd.NoGL"             => "No JT",
								"ttsrhd.KodeTrans"        => "TC",
								"ttsrhd.SRKeterangan"     => "Keterangan",
								"ttsrhd.Cetak"            => "Cetak",
								"ttsrhd.LokasiKode"       => "Lokasi",
								"ttsrhd.PosKode"          => "POS",
								"ttsrhd.UserID"           => "User",
								"ttsrhd.SRSubTotal"       => "Sub Total",
								"ttsrhd.SRDiscFinal"      => "Discount Final",
								"ttsrhd.SRTotalPajak"     => "Total Pajak",
								"ttsrhd.SRBiayaKirim"     => "Biaya Kirim",
								"ttsrhd.SRTotal"          => "Total Jual",
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
							<div class="col-sm-4">
								<?= Html::dropDownList( 'order', null, [
								"ASC" => 'ASC',
								'DESC' => 'DESC',
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
				</div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
