<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
$this->title                   = 'Daftar [SV] Invoice Surat Jalan';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;
$sqlPos                           = [];
$cmbPos                           = [];

?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
            <?= Html::beginForm(); ?>
            <div class="box-body">
                <div class="container-fluid">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Laporan</label>
                            <div class="col-sm-20">
                                <?= Html::dropDownList( 'tipe', null, [
                                    'Header1' => 'Header1',
                                    'Item1' => 'Item1',
                                    'Item2' => 'Item2',
                                    'Item3' => 'Item3',
                                ], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Format</label>
                            <div class="col-sm-20">
                                <?= Html::dropDownList( 'filetype', null, [
                                    'pdf'  => 'PDF',
                                    'xlsr' => 'EXCEL RAW',
                                    'xls'  => 'EXCEL',
                                    'doc'  => 'WORD',
                                    'rtf'  => 'RTF',
                                ], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="container-fluid">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Tanggal</label>
                            <div class="col-sm-9">
                                <? try {
                                    echo DatePicker::widget( [
                                        'name'          => 'tgl1',
                                        'type'          => DatePicker::TYPE_INPUT,
                                        'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format'    => 'dd/mm/yyyy'
                                        ]
                                    ] );
                                } catch ( \yii\base\InvalidConfigException $e ) {
                                } ?>
                            </div>
                            <label class="control-label col-sm-3">s/d</label>
                            <div class="col-sm-9">
                                <? try {
                                    echo DatePicker::widget( [
                                        'name'          => 'tgl2',
                                        'type'          => DatePicker::TYPE_INPUT,
                                        'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format'    => 'dd/mm/yyyy'
                                        ]
                                    ] );
                                } catch ( \yii\base\InvalidConfigException $e ) {
                                } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Status</label>
                            <div class="col-sm-15">
                                <?= Html::dropDownList( 'status', null, [
                                    "" => 'Semua',
                                    " AND ((SDTotalBiaya - SDTerbayar) = 0 OR KodeTrans = 'TC14') " => 'Lunas',
                                    " AND ((SDTotalBiaya - SDTerbayar) > 0 AND KodeTrans <> 'TC14') " => 'Belum'
                                ], [ 'text' => '', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label class="control-label col-sm-3">TC</label>
                            <div class="col-sm-15">
                                <?= Html::dropDownList( 'TC', null, [
                                    "" => "Semua",
                                    "TC11" => "TC11 - Penjualan Servis",
                                    "TC13" => "TC13 - Pengeluaran eks Claim C2",
                                    "TC14" => "TC14 - Pengeluaran eks KPB"
                                ], [ 'text' => '', 'class' => 'form-control' ] ) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= Html::dropDownList( 'ASS', null, [
                                    ""        => "ALL",
                                    "ASS1"    => "ASS1",
                                    "ASS2"    => "ASS2",
                                    "ASS3"    => "ASS3",
                                    "ASS4"    => "ASS4",
                                    "CS"      => "CS",
                                    "HR"      => "HR",
                                    "LS"      => "LS",
                                    "LR"      => "LR",
                                    "JR"      => "JR",
                                    "CLAIMC2" => "CLAIMC2",
                                    "OPL"     => "OPL",
                                    "OR+"     => "OR+",
                                    "Other"   => "Other",
                                ], [ 'text' => '', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Sort</label>
                            <div class="col-sm-15">
                                <?= Html::dropDownList( 'Sort', null, [
                                    "ttsdhd.SVNo"                 =>   "No Invoice [SV]",
                                    "ttsdhd.SVTgl"                =>   "Tgl Invoice [SV]",
                                    "ttsdhd.SDNo"                 =>   "No Service [SD]",
                                    "ttsdhd.SDTgl"                =>   "Tgl Daftar [SD]",
                                    "ttsdhd.MotorNoPolisi"        =>   "No Polisi",
                                    "tdcustomer.CusNama"          =>   "Pemilik",
                                    "ttsdhd.PKBNo"                =>   "No PKB",
                                    "tdcustomer.MotorNoMesin"     =>   "No Mesin",
                                    "ttsdhd.SDPembawaMotor"       =>   "Pembawa Motor",
                                    "ttsdhd.SDNoUrut"             =>   "No Urut",
                                    "ttsdhd.SENo"                 =>   "No Estimasi",
                                    "ttsdhd.NoGLSD"               =>   "No JT [SD]",
                                    "ttsdhd.NoGLSV"               =>   "No JT [SV]",
                                    "ttsdhd.KodeTrans"            =>   "TC",
                                    "ttsdhd.SDStatus"             =>   "Status",
                                    "ttsdhd.SDJenis"              =>   "Jenis",
                                    "ttsdhd.ASS"                  =>   "KPB / ASS",
                                    "ttsdhd.PrgNama"              =>   "Program",
                                    "ttsdhd.KarKode"              =>   "Kode Karyawan",
                                    "ttsdhd.SDStatus"             =>   "Status",
                                    "ttsdhd.SDKeluhan"            =>   "Keluhan",
                                    "ttsdhd.SDKeterangan"         =>   "Keterangan",
                                    "ttsdhd.SDHubunganPembawa"    =>   "Hubungan Pembawa",
                                    "ttsdhd.SDAlasanServis"       =>   "Alasan Servis",
                                    "ttsdhd.Cetak"                =>   "Cetak",
                                    "ttsdhd.KlaimKPB"             =>   "KlaimKPB",
                                    "ttsdhd.KlaimC2"              =>   "KlaimC2",
                                    "ttsdhd.UserID"               =>   "User",
                                    "ttsdhd.SDNoUrut"             =>   "No Urut",
                                    "ttsdhd.SDKmSekarang"         =>   "Km Sekarang",
                                    "ttsdhd.SDKmBerikut"          =>   "Km Berikut",
                                    "ttsdhd.SDTotalWaktu"         =>   "Total Waktu",
                                    "ttsdhd.SDTotalJasa"          =>   "Total Jasa",
                                    "ttsdhd.SDTotalQty"           =>   "Total Qty",
                                    "ttsdhd.SDTotalPart"          =>   "Total Part",
                                    "ttsdhd.SDTotalGratis"        =>   "Total Gratis",
                                    "ttsdhd.SDTotalNonGratis"     =>   "Total NonGratis",
                                    "ttsdhd.SDDiscFinal"          =>   "Disc Final",
                                    "ttsdhd.SDTotalPajak"         =>   "Total Pajak",
                                    "ttsdhd.SDTotalBiaya"         =>   "Total Biaya",
                                    "ttsdhd.SDDurasi"             =>   "Durasi",
                                ], [ 'text' => '', 'class' => 'form-control' ] ) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= Html::dropDownList( 'order', null, [
                                    "ASC" => 'ASC',
                                    'DESC' => 'DESC',
                                ], [ 'text' => '', 'class' => 'form-control' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label col-sm-3">POS</label>
                            <div class="col-sm-21">
                                <? echo Select2::widget( [
                                    'name'          => 'pos',
                                    'data'          => $cmbPos,
                                    'options'       => [ 'class' => 'form-control' ],
                                    'theme'         => Select2::THEME_DEFAULT,
                                ] ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?= Html::endForm(); ?>
        </div>
    </div>
<?php


