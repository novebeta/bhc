<?php
use kartik\date\DatePicker;
use yii\helpers\Html;
$this->title                   = 'Daftar [TA] Penyesuaian';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;
?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Header1' => 'Header1',
									'Item1' => 'Item1',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-footer">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Tanggal</label>
							<div class="col-md-10">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
							<div class="col-md-10">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">TC</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'TC', null, [
								"%"     => "Semua",
								"TC04"     => "TC04 - Penerimaan akibat selisih stock opname (plus)",
								"TC16"     => "TC16 - Pengeluaran akibat selisih stock opname (minus)",
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="form-group">
							<label class="control-label col-sm-4">Sort</label>
							<div class="col-sm-13">
								<?= Html::dropDownList( 'Sort', null, [
								"tttahd.TANo"         => "No Penyesuaian [TA]",
								"tttahd.TATgl"        => "Tanggal TA",
								"tttahd.KodeTrans"    => "TC",
								"tttahd.NoGL"         => "No JT",
								"tttahd.TAKeterangan" => "Keterangan",
								"tttahd.Cetak"        => "Cetak",
								"tttahd.LokasiKode"   => "Lokasi",
								"tttahd.PosKode"      => "POS",
								"tttahd.UserID"       => "User",
								"DATE(ttTAhd.TATgl)"  => "Tgl Penyesuaian",
								"ttTAhd.TATotal"      => "Total Penyesuaian",
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
							<div class="col-sm-7">
								<?= Html::dropDownList( 'order', null, [
								"ASC" => 'ASC',
								'DESC' => 'DESC',
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
				</div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
