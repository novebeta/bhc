<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
$this->title                   = 'Daftar [PI] Invoice Pembelian';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;

$sqlSupplier                           = Yii::$app->db
	->createCommand( "SELECT '%' AS value, 'Semua' AS label, 0 AS SupStatus, 0 AS SupKode
		UNION
		SELECT SupKode AS value, CONCAT(SupNama,' ', SupKode) AS label, SupStatus, SupKode FROM tdsupplier 
		WHERE SupStatus = 'A' OR SupStatus = 1 
		ORDER BY SupStatus, SupKode" )
	->queryAll();
$cmbSupplier                           = ArrayHelper::map( $sqlSupplier, 'value', 'label' );

$format = <<< SCRIPT
function formatBank(result) {
    return '<div class="row">' +
           '<div class="col-md-5">' + result.id + '</div>' +
           '<div class="col-md-19">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );

?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Header1' => ' Header1',
									'Item1' => ' Item1',
									'Item2' => ' Item2',
									'Price Variance' => ' Price Variance',
									'PV Detail' => 'PV Detail',
									'PV Rekap' => 'PV Rekap',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<div class="container-fluid">
					<div class="col-md-8">
						<div class="form-group">
							<div class="col-sm-8">
								<?= Html::dropDownList( 'tanggal', null, [
								"DATE(ttpihd.PITgl)"      => "Tgl PI", 
								"DATE(ttpihd.PITglTempo)" => "Tgl Tempo", 
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
							<div class="col-md-8">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
							<div class="col-md-8">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="form-group">
							<label class="control-label col-sm-4">Supplier</label>
							<div class="col-sm-20">
								<? echo Select2::widget( [
									'name'          => 'supplier',
									'data'          => $cmbSupplier,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatBank' ),
										'templateSelection' => new JsExpression( 'formatBank' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<div class="col-md-8">
						<div class="form-group">
							<label class="control-label col-sm-8">TC PS</label>
							<div class="col-sm-16">
								<?= Html::dropDownList( 'TC', null, [
								 ""      => "Semua",                                
								 "TC01"  => "TC01 - Penerimaan dari Supplier MD",   
								 "TC02"  => "TC02 - Penerimaan dari Supplier Lokal",
								 "TC03"  => "TC03 - Penerimaan Part Robbing",       
								 "TC05"  => "TC05 - Penerimaan dari Dealer Lain",   
								 "TC06"  => "TC06 - Penerimaan Olie KPB",           
								 "TC07"  => "TC07 - Penerimaan Part HO",            
								 "TC08"  => "TC08 - Penerimaan Part Claim",         
								 "TC09"  => "TC09 - Penerimaan Kekurangan Part/Oli",
								 "TC10"  => "TC10 - Penerimaan Lain Lain",  
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="form-group">
							<label class="control-label col-sm-4">Status</label>
							<div class="col-sm-8">
								<?= Html::dropDownList( 'status', null, [
									'' => 'Semua',
									' AND (PITotal - PITerbayar) <= 0 ' => 'Lunas',
									' AND (PITotal - PITerbayar) > 0  ' => 'Belum',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
							<label class="control-label col-sm-3">TC PI</label>
							<div class="col-sm-9">
								<?= Html::dropDownList( 'tcpi', null, [
									''  => 'Semua',
									'PIOL'  => 'PIOL - Oli',
									'PIPR'  => 'PIPR - Part',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-3">Sort</label>
							<div class="col-sm-14">
								<?= Html::dropDownList( 'Sort', null, [
									"ttpihd.PINo"                =>    "No Pembelian Inv [PI]", 
									"ttpihd.PITgl"               =>    "Tgl Pembelian Inv [PI]",
									"ttpihd.PINoRef"             =>    "No Faktur [NoRef]",     
									"ttpihd.PSNo"                =>    "No Penerimaan [PS]",    
									"ttpihd.NoGL"                =>    "No JT",                 
									"ttpihd.PIJenis"             =>    "Jenis",                 
									"ttpihd.KodeTrans"           =>    "TC",                    
									"ttpihd.SupKode"             =>    "Kode Supplier",         
									"tdSupplier.SupNama"         =>    "Nama Supplier",         
									"ttpihd.PIKeterangan"        =>    "Keterangan",            
									"ttpihd.LokasiKode"          =>    "Lokasi",                
									"ttpihd.PosKode"             =>    "POS",                   
									"ttpihd.Cetak"               =>    "Cetak",                 
									"ttpihd.UserID"              =>    "User",                  
									"ttpihd.PISubTotal"          =>    "Sub Total",             
									"ttpihd.PIDiscFinal"         =>    "Discount Final",        
									"ttpihd.PITotalPajak"        =>    "Total Pajak",           
									"ttpihd.PIBiayaKirim"        =>    "Biaya Kirim",           
									"ttpihd.PITotal"             =>    "Total Jual",            
									"ttpihd.PITerm"              =>    "Term",                  
									"DATE(ttpihd.PITglTempo)"    =>    "Tgl Tempo",                  
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
							<div class="col-sm-7">
								<?= Html::dropDownList( 'order', null, [
								"ASC" => 'ASC',
								'DESC' => 'DESC',
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
   