<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
$this->title                   = 'Daftar [SI] Invoice Penjualan';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;

//$sqlPos                           = Yii::$app->db
//	->createCommand( "	SELECT 'Semua' as label, '' as value, 0 as PosStatus, 0 As PosNomor FROM tdpos
//	UNION SELECT PosKode as label, PosKode as value, PosStatus, PosNomor FROM tdpos
//	ORDER BY PosStatus, PosNomor " )
//	->queryAll();
//$cmbPos                           = ArrayHelper::map( $sqlPos, 'value', 'label' );
$cmbPos                           = [];
$sqlPos                           = [];

?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Header1' => 'Header1',
									'Item1' => 'Item1',
									'Item2' => 'Item2',
									'Item3' => 'Item3',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-3">Tanggal</label>
							<div class="col-sm-9">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
							<label class="control-label col-sm-3">s/d</label>
							<div class="col-sm-9">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="form-group">
							<label class="control-label col-sm-3">Status</label>
							<div class="col-sm-15">
								<?= Html::dropDownList( 'status', null, [
								 ""                                                                  =>    "Semua",
								" AND ((SITotal - SITerbayar) = 0 OR ttsihd.KodeTrans = 'TC14') "   =>    "Lunas",
								" AND ((SITotal - SITerbayar) > 0 AND ttsihd.KodeTrans <> 'TC14') " =>    "Belum",
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-3">TC</label>
							<div class="col-sm-21">
								<?= Html::dropDownList( 'TC', null, [
								""     => "Semua",
								"TC12" => "TC12 - Penjualan Counter",
								"TC15" => "TC15 - Pengeluaran Keperluan Internal",
								"TC17" => "TC17 - Pengeluaran ke Dealer Lain",
								"TC18" => "TC18 - Pengembalian Part Robbing ke H1",
								"TC99" => "TC99 - Pengeluaran Lain Lain",
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="form-group">
							<label class="control-label col-sm-3">Sort</label>
							<div class="col-sm-15">
								<?= Html::dropDownList( 'sort', null, [
								"ttsihd.SINo"             => "No Penjualan Inv [SI]",
								"ttsihd.SITgl"            => "Tanggal Penjualan Inv [SI]",
								"ttsihd.SONo"             => "No Order Penjualan [SO]",
								"ttsihd.MotorNoPolisi"    => "No Polisi",
								"tdcustomer.CusNama"      => "Pemilik",
								"tdcustomer.MotorNoMesin" => "No Mesin",
								"ttsihd.NoGL"             => "No JT",
								"ttsihd.KarKode"          => "Kode Karyawan",
								"tdkaryawan.KarNama"      => "Nama Karyawan",
								"ttsihd.DealerKode"       => "Kode Dealer",
								"tdkaryawan.PrgNama"      => "Program",
								"ttsihd.SIJenis"          => "Jenis",
								"ttsihd.KodeTrans"        => "TC",
								"ttsihd.SIKeterangan"     => "Keterangan",
								"ttsihd.Cetak"            => "Cetak",
								"ttsihd.LokasiKode"       => "Lokasi",
								"ttsihd.PosKode"          => "POS",
								"ttsihd.UserID"           => "User",
								"DATE(ttsihd.SITgl)"      => "Tgl Jual",
								"ttsihd.SISubTotal"       => "Sub Total",
								"ttsihd.SIDiscFinal"      => "Discount Final",
								"ttsihd.SITotalPajak"     => "Total Pajak",
								"ttsihd.SIBiayaKirim"     => "Biaya Kirim",
								"ttsihd.SITotal"          => "Total Jual",
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
							<div class="col-sm-6">
								<?= Html::dropDownList( 'order', null, [
								"ASC" => 'ASC',
								'DESC' => 'DESC',
								], [ 'text' => '', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<label class="control-label col-sm-3">POS</label>
							<div class="col-sm-21">
								<? echo Select2::widget( [
									'name'          => 'pos',
									'data'          => $cmbPos,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
								] ); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
