<?php

use aunit\components\Menu;
use kartik\date\DatePicker;
use yii\helpers\Html;
use kartik\checkbox\CheckboxX;
\aunit\assets\AppAsset::register( $this );
\aunit\assets\JqwidgetsAsset::register( $this );
$this->title                     = 'Laporan Query';
$this->params[ 'breadcrumbs' ][] = $this->title;
$formatter                       = \Yii::$app->formatter;
$this->registerCss( ".jqx-widget-content { font-size: 12px; }" );
?>
    <div class="laporan-query col-md-24" style="padding-left: 0;">
        <div class="box box-default">
            <?= Html::beginForm(); ?>
            <div class="box-body" id='jqxWidget'>
                <div id="jqxgrid">
                </div>
            </div>
            <div class="box-footer">
                <div class="container-fluid">
                    <div class="form-group col-sm-18">
                        <textarea class="form-control" id="sqlraw" rows="8">
                            SELECT
  ttsk.SKNo, ttsk.SKTgl,  tdcustomer.CusKTP,tdcustomer.CusNPWP,tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, tdcustomer.CusPembeliNama AS NamaPemohon, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType, tmotor.MotorWarna, ttdk.DKHarga, ttdk.DKDPTotal, ttdk.ProgramSubsidi, ttdk.DKNetto, ttdk.ReturHarga, ttdk.PotonganKhusus, tdcustomer.CusTelepon, ttdk.LeaseKode, ttdk.InsentifSales ,tdsales.SalesNama, tdsales.TeamKode, ttdk.DKTglTagih, ttdk. DKTenor,  ttdk.DKAngsuran,
BPKBNo,BPKBTglJadi,BPKBTglAmbil,BPKBPenerima,FakturAHMNo,FakturAHMTgl,FakturAHMTglAmbil,STNKNo,STNKTglMasuk,STNKTglBayar,STNKTglJadi,STNKTglAmbil,STNKPenerima,NoticeNo,NoticeTglJadi,NoticeTglAmbil,NoticePenerima,PlatNo,PlatTgl,PlatTglAmbil,PlatPenerima, MotorMemo
FROM
  tmotor
  INNER JOIN ttdk
    ON tmotor.DKNo = ttdk.DKNo
  INNER JOIN tdcustomer
    ON ttdk.CusKode = tdcustomer.CusKode
  INNER JOIN ttsk
    ON tmotor.SKNo = ttsk.SKNo
  INNER JOIN tdsales
    ON ttdk.SalesKode = tdsales.SalesKode
  INNER JOIN
    (SELECT
      MAX(tmotor.MotorAutoN) AS MotorAutoN, tmotor.MotorNoMesin
    FROM
      tmotor
    GROUP BY tmotor.MotorNoMesin) Maksimum
    ON tmotor.MotorNoMesin = Maksimum.MotorNoMesin
    AND tmotor.MotorAutoN = Maksimum.MotorAutoN 
                        </textarea>
                    </div>
                    <div class="form-group col-sm-6">
                        <input type="file" id="file-input" class="hidden"/>
                        <button class="btn btn-primary" id='btnLoad' type="button" style="width: 80px;height: 64px"><i class="fa fa-folder-open fa-lg"></i>&nbsp Load</button>
                        <button class="btn btn-danger" id='btnTampil' type="button" style="width: 80px;height: 64px"><i class="fa fa-th fa-lg"></i>&nbsp Tampil</button>
                        <button class="btn btn-success" id='btnExcel' type="button" style="width: 80px;height: 64px"><i class="fa fa-file-excel-o fa-lg"></i>&nbsp Excel</button>
                        <button class="btn btn-success" id='btnTsv' type="button" style="width: 80px;height: 64px"><i class="fa fa-file-excel-o fa-lg"></i>&nbsp TSV</button>
                    </div>
                    <div class="col-md-6">
                        <div class="col-sm-18">
                            <?= Html::dropDownList( 'tipe', null, [
                                "ttsk.SKTgl" => "Tgl Srt Jln Kons",
                                "ttdk.DKTgl" => "Tgl Dat Kons",
                            ], [ 'text' => '', 'class' => 'form-control', 'id' => 'tipe' ] ) ?>
                        </div>
                        <div class="col-sm-4">
                            <? try {
                                echo CheckboxX::widget([
                                    'name'=>'cek',
                                    'id'=>'cek',
                                    'value'=>1,
                                    'pluginOptions'=>['threeState'=>false, 'size'=>'lg']
                                ]);
                            } catch ( \yii\base\InvalidConfigException $e ) {
                            } ?>
                        </div>
                        <br><br>
                        <div class="col-md-18">
                            <? try {
                                echo DatePicker::widget( [
                                    'name'          => 'tgl_start',
                                    'id'            => 'tgl_start',
                                    'type'          => DatePicker::TYPE_INPUT,
                                    'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
                                    'pluginOptions' => [
                                        'autoclose'      => true,
                                        'format'         => 'dd/mm/yyyy',
                                        'todayHighlight' => true
                                    ]
                                ] );
                            } catch ( \yii\base\InvalidConfigException $e ) {
                            } ?>
                        </div><br><br>
                        <div class="col-md-18">
                            <? try {
                                echo DatePicker::widget( [
                                    'name'          => 'tgl_end',
                                    'id'            => 'tgl_end',
                                    'type'          => DatePicker::TYPE_INPUT,
                                    'value'         => date('t/m/Y',strtotime("today")),
                                    'pluginOptions' => [
                                        'autoclose'      => true,
                                        'format'         => 'dd/mm/yyyy',
                                        'todayHighlight' => true
                                    ]
                                ] );
                            } catch ( \yii\base\InvalidConfigException $e ) {
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
            <?= Html::endForm(); ?>
        </div>
    </div>
<?php
$urlData = \yii\helpers\Url::toRoute( [ 'report/query' ] );
$urlExport = \yii\helpers\Url::toRoute( [ 'site/exportdata' ] );
$this->registerJsVar('isAdminOrAuditor',Menu::showOnlyHakAkses(['Admin','Auditor','GM','Korwil']));
$this->registerJs( <<< JS
jQuery(function ($) {                
            
     var json = '[{ "columns": [] }, {"rows" : []}]';            
            var obj = $.parseJSON(json);
            var columns = obj[0].columns;
            var datafields = new Array();
            for (var i = 0; i < columns.length; i++) {
                datafields.push({ name: columns[i].datafield });
            }
            var rows = obj[1].rows;
            // prepare the data
            var source =
            {
                datatype: "json",
                datafields: datafields,
                id: 'id',
                localdata: rows
            };
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#jqxgrid").jqxGrid(            {
                width: '100%',
                rowsheight: 21,
                height: 300,
                sortable: true,
                selectionmode: 'multiplerowsextended',
                source: dataAdapter,
                columnsresize: true,
                columns: columns
            });
            function readSingleFile(e) {
              //  console.log(e);
              var file = e.target.files[0];
              if (!file) {
                return;
              }
              var reader = new FileReader();
              reader.onload = function(e) {
                var contents = e.target.result;
                $('#sqlraw').val(contents);
              };
              reader.readAsText(file);
            }
            document.getElementById('file-input').addEventListener('change', readSingleFile, false);
            $('#btnLoad').click(function() {
                $('#file-input').trigger('click');
            });
            $('#btnTampil').click(function() {
                var sqlraw = $('#sqlraw').val();
                //SELECT atau CALL di bebaskan saja
                if(!isAdminOrAuditor){
//                    if(!sqlraw.toLowerCase().includes("select") || !sqlraw.toLowerCase().includes("call")){
//                        alert('Query harus mengandung kata SELECT atau CALL');
//                        return;
//                    }
                    if(sqlraw.toLowerCase().includes("update") || sqlraw.toLowerCase().includes("delete")){
                        alert('Anda tidak dapat  menggunakan perintah query tersebut');
                        return;
                    }
                }
                var cek = $('#cek').val();
                if ( cek === "1" ){
                    var tipe = $('#tipe').val();
                    var start_date = $('#tgl_start').val().split("/").reverse().join("-");
                    var end_date = $('#tgl_end').val().split("/").reverse().join("-");
                    if (sqlraw.includes('WHERE') || sqlraw.includes('where' || sqlraw.includes('Where')) ){
                        var query = sqlraw + " AND " + tipe + " BETWEEN '" + start_date + "' AND '" + end_date + "';";
                    }else{
                        var query = sqlraw + " WHERE " + tipe + " BETWEEN '" + start_date + "' AND '" + end_date + "';";
                    }
                }else{
                    var  query = $('#sqlraw').val();
                }
                $.ajax({
                        type: 'POST',
                        url: '$urlData',
                        data: {q:btoa(query)} // q:btoa(query)}
                        // data: query,
                    }).then(function (json) {
                        var obj = json;
                        var columns = obj.columns;
                        var datafields = new Array();
                        columns.unshift({
                              text: '#', sortable: false, filterable: false, editable: false,
                              groupable: false, draggable: false, resizable: false,
                              datafields: '', columntype: 'number', width: 50,
                              cellsrenderer: function (row, column, value) {
                                  return "<div style='margin:4px;'>" + (value + 1) + "</div>";
                              }
                          });
                        datafields.push({name:'rowindex'});
                        for (var i = 0; i < columns.length; i++) {
                            datafields.push({ name: columns[i].datafield});
                        }
                        var rows = obj.rows;
                        // prepare the data
                        var source =
                        {
                            datatype: "json",
                            datafields: datafields,
                            id: 'id',
                            localdata: rows
                        };
                        var dataAdapter = new $.jqx.dataAdapter(source);
                        $("#jqxgrid").jqxGrid( {
                            width: '100%',
                            rowsheight: 21,
                            height: 330,
                            source: dataAdapter,
                            columnsresize: true,
                            columns: columns
                        });
                         $("#jqxgrid").jqxGrid('autoresizecolumns');
                    });
            });
             $("#btnExcel").click(function () {
                $("#jqxgrid").jqxGrid('exportdata', 'xlsx','LaporanQuery');
            });
             $("#btnTsv").click(function () {
                $("#jqxgrid").jqxGrid('exportview', 'tsv', 'LaporanQuery');
            });
             
             
});
JS
);
