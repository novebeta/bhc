<?php
use kartik\datecontrol\DateControl;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\aunit\assets\AppAsset::register( $this );
\aunit\assets\JqwidgetsAsset::register( $this );
$this->title                     = 'Laporan Astra';
$this->params[ 'breadcrumbs' ][] = $this->title;
$formatter                       = \Yii::$app->formatter;
$sqlSales                        = Yii::$app->db
	->createCommand( "SELECT SalesKode as value, SalesNama as label FROM tdSales WHERE SalesStatus = 'A' ORDER BY SalesKode" )
	->queryAll();
$cmbSales                        = array_merge([''=>'Semua'],ArrayHelper::map( $sqlSales, 'value', 'label' ));
$sqlPos                          = Yii::$app->db
	->createCommand( "SELECT LokasiKode as value, LokasiNama as label FROM tdlokasi Where LokasiStatus IN ('A') AND LokasiJenis <> 'Kas' ORDER BY LokasiStatus, LokasiKode" )
	->queryAll();
$cmbPos                          = array_merge([''=>'Semua'],ArrayHelper::map( $sqlPos, 'value', 'label' ));
$format                          = <<< SCRIPT
function formatAstra(result) {
    return '<div class="row" style="margin-left: 2px">' +
           ' ' + result.id + '&emsp;' +
           ' ' + result.text + ' ' +
           '</div>';
}
SCRIPT;
$escape                          = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
$this->registerCss( ".jqx-widget-content { font-size: 12px; }" );
?>
    <div class="laporanastra-form col-md-24" >
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form-report-astra' ] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[
						'class' => "box box-default col-md-24",
						'items' => '<div class="box-body" id="jqxWidget"><div id="jqxgrid"/></div>'
					],
					[
						'class' => "col-md-17",
						'style' => "margin-top: 2px;",
						'items' =>
							[
								[
									'class' => "form-group col-md-6",
									'items' => [
										[
											'style' => "margin-bottom: 2px;",
											'items' => ":TglType"
										],
									]
								],
								[
									'class' => "form-group col-md-4",
									'items' => [
										[
											'style' => "margin-bottom: 2px;margin-left:2px;",
											'items' => ":TglStart"
										],
									]
								],
								[
									'class' => "form-group col-md-1",
									'items' => [
										[
											'style' => "margin-bottom: 2px;margin-left:10px;",
											'items' => ":LabelSD"
										],
									]
								],
								[
									'class' => "form-group col-md-4",
									'items' => [
										[
											'style' => "margin-bottom: 3px;",
											'items' => ":TglEnd"
										],
									]
								],
								[
									'class' => "form-group col-md-1",
									'items' => [
										[
											'style' => "margin-bottom: 3px;margin-left:20px;",
											'items' => ":CKAstra"
										],
									]
								],
								[
									'class' => "form-group col-md-1",
									'items' => [
										[
											'style' => "margin-bottom: 3px;",
											'items' => " :LabelCKAstra"
										],
									]
								],
								[
									'class' => "form-group col-md-2",
									'items' => [
										[
											'style' => "margin-bottom: 3px;",
											'items' => ":LabelJenisJual"
										],
									]
								],
								[
									'class' => "form-group col-md-5",
									'items' => [
										[
											'style' => "margin-bottom: 3px;",
											'items' => ":JenisJual"
										],
									]
								],
								[
									'class' => "form-group col-md-2",
									'items' => [
										[
											'style' => "margin-bottom: 3px;",
											'items' => ":LabelSales"
										],
									]
								],
								[
									'class' => "form-group col-md-22",
									'items' => [
										[
											'style' => "margin-bottom: 3px;",
											'items' => ":Sales"
										],
									]
								],
								[
									'class' => "form-group col-md-2",
									'items' => [
										[
											'style' => "margin-bottom: 3px;",
											'items' => ":LabelPOS"
										],
									]
								],
								[
									'class' => "form-group col-md-22",
									'items' => [
										[
											'style' => "margin-bottom: 3px;",
											'items' => ":POS"
										],
									]
								],
							]
					],
					[
						'style' => "margin: 15px;",
						'class' => "col-md-6",
						'items' => ":btnTampil :btnExcel"
					],
				]
			],
			[
				'class' => "form-group",
				'items' => ''
			],
			[
				":LabelSD"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">s/d</label>',
				":LabelJenisJual" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis Jual</label>',
				":LabelSales"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sales</label>',
				":LabelPOS"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">POS</label>',
				":LabelCKAstra"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Astra</label>',
				":TglType"        => Html::dropDownList( 'tgl_type', null,
					[
						'TTDK' => 'TglDatKons',
						'TTSK' => 'TglSrtJlnKons',
					],
					[ 'id' => 'tgl_type', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ]
				),
				":TglStart"       => DateControl::widget( [
					'name'          => 'tgl1',
					'type'          => DateControl::FORMAT_DATE,
					'value'         => Yii::$app->formatter->asDate( '-10 day', 'yyyy-MM-01' ),
					'options'       => [
						'class' => 'filter2 dt',
					],
					'pluginOptions' => [
						'autoclose' => true,
					],
				] ),
				":CKAstra"        => Html::checkbox( 'astra', true,
					[ 'label' => ' ', 'id' => 'astra', ]
				),
				":TglEnd"         => DateControl::widget( [
					'name'          => 'tgl2',
					'type'          => DateControl::FORMAT_DATE,
					'value'         => Yii::$app->formatter->asDate( 'now', 'yyyy-MM-01' ),
					'options'       => [
						'class' => 'filter2 dt',
					],
					'pluginOptions' => [
						'autoclose' => true,
					],
				] ),
				":JenisJual"      => Html::dropDownList( 'jenis_jual', null,
					[
						''       => 'Semua',
						'Tunai'  => 'Tunai',
						'Kredit' => 'Kredit',
					],
					[ 'id' => 'jenis_jual', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ]
				),
				":Sales"          => Select2::widget( [
					'name'          => 'sales',
					'data'          => $cmbSales,
					'options'       => [ 'class' => 'form-control' ],
					'theme'         => Select2::THEME_DEFAULT,
					'pluginOptions' => [
						'dropdownAutoWidth' => true,
						'templateResult'    => new JsExpression( 'formatAstra' ),
						'templateSelection' => new JsExpression( 'formatAstra' ),
						'escapeMarkup'      => $escape,
					],
				] ),
				":POS"            => Select2::widget( [
					'name'          => 'pos',
					'data'          => $cmbPos,
					'options'       => [ 'class' => 'form-control' ],
					'theme'         => Select2::THEME_DEFAULT,
					'pluginOptions' => [
						'dropdownAutoWidth' => true,
						'templateResult'    => new JsExpression( 'formatAstra' ),
						'templateSelection' => new JsExpression( 'formatAstra' ),
						'escapeMarkup'      => $escape,
					],
				] ),
				":btnTampil"      => '<button class="btn btn-danger" id="btnTampil" type="button" style="width: 80px;height: 64px"><i class="fa fa-th fa-lg"></i>&nbsp Tampil</button>',
				":btnExcel"       => '<button class="btn btn-success" id="btnExcel" type="button" style="width: 80px;height: 64px"><i class="fa fa-file-excel-o fa-lg"></i>&nbsp Excel</button>',
			]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlData = \yii\helpers\Url::toRoute( [ 'report/astra-query' ] );
$urlExport = \yii\helpers\Url::to( [ 'report/export-excel' ] );
$this->registerJs( <<< JS
jQuery(function ($) {
     var json = '[{ "columns": [] }, {"rows" : []}]';
            var obj = $.parseJSON(json);
            var columns = obj[0].columns;
            var datafields = new Array();
            for (var i = 0; i < columns.length; i++) {
                datafields.push({ name: columns[i].datafield });
            }
            var rows = obj[1].rows;
            // prepare the data
            var source =
            {
                datatype: "json",
                datafields: datafields,
                id: 'id',
                localdata: rows
            };
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#jqxgrid").jqxGrid(            {
                width: '100%',
                rowsheight: 21,
                height: 300,
                sortable: true,
                selectionmode: 'multiplerowsextended',
                source: dataAdapter,
                columnsresize: true,
                columns: columns
            });
            function readSingleFile(e) {
              //  console.log(e);
              var file = e.target.files[0];
              if (!file) {
                return;
              }
              var reader = new FileReader();
              reader.onload = function(e) {
                var contents = e.target.result;
                $('#sqlraw').val(contents);
              };
              reader.readAsText(file);
            }
            $('#btnTampil').click(function() {
                
                $.ajax({
                        type: 'POST',
                        url: '$urlData',
                        data: $( "#form-report-astra" ).serialize(),
                    }).then(function (json) {
                        // var json = '[{ "columns": [] }, {"rows" : []}]';
                        var obj = json;
                        var columns = obj.columns;
                        var datafields = new Array();
                        columns.unshift({
                              text: '#', sortable: false, filterable: false, editable: false,
                              groupable: false, draggable: false, resizable: false,
                              datafields: '', columntype: 'number', width: 50,
                              cellsrenderer: function (row, column, value) {
                                  return "<div style='margin:4px;'>" + (value + 1) + "</div>";
                              }
                          });
                        datafields.push({name:'rowindex'});
                        for (var i = 0; i < columns.length; i++) {
                            datafields.push({ name: columns[i].datafield});
                        }
                        var rows = obj.rows;
                        // prepare the data
                        var source =
                        {
                            datatype: "json",
                            datafields: datafields,
                            id: 'id',
                            localdata: rows
                        };
                        var dataAdapter = new $.jqx.dataAdapter(source);
                        $("#jqxgrid").jqxGrid( {
                            width: '100%',
                            rowsheight: 21,
                            height: 300,
                            source: dataAdapter,
                            columnsresize: true,
                            columns: columns
                        });
                         $("#jqxgrid").jqxGrid('autoresizecolumns');
                    });
            });
             $("#btnExcel").click(function () {
                $("#jqxgrid").jqxGrid('exportdata', 'xlsx', 'LaporanAstra');           
            });
});
JS
);