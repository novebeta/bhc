<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
$this->title                   = 'Daftar Faktur Beli';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;
$sqlBank = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, '' AS SupStatus, '' AS SupKode
	UNION
	SELECT SupKode AS value, SupNama AS label, SupStatus, SupKode FROM tdsupplier 
	WHERE SupStatus = 'A' OR SupStatus = '1' 
	ORDER BY SupStatus, SupKode" )
	->queryAll();
$cmbBank = ArrayHelper::map( $sqlBank, 'value', 'label' );

$sqlwarehouse = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, '' AS LokasiStatus, '' AS LokasiJenis, '' AS LokasiKode
	UNION
	SELECT LokasiKode AS value, LokasiNama AS label, LokasiStatus, LokasiJenis, LokasiKode FROM tdlokasi 
	WHERE LokasiJenis <> 'Kas' 
	ORDER BY LokasiStatus, LokasiKode" )
	->queryAll();
$cmbwarehouse = ArrayHelper::map( $sqlwarehouse, 'value', 'label' );

$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
function formatBank(result) {
    return '<div class="row">' +
           '<div class="col-md-6">' + result.id + '</div>' +
           '<div class="col-md-18">' + result.text + '</div>' +
           '</div>';
}
function formatWarehouse(result) {
    return '<div class="row">' +
           '<div class="col-md-6">' + result.id + '</div>' +
           '<div class="col-md-18">' + result.text + '</div>' +
           '</div>';
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'FB Header 1'  => 'FB Header 1',
									'FB Header 2'  => 'FB Header 2',
									'FB Detail'  => 'FB Detail',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-footer">
			    <div class="container-fluid">
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="col-sm-24">
								<?= Html::dropDownList( 'jenis', null, [
									"ttfb.FBTgl"                            => "Tgl Faktur Beli",
									"ttfb.FBTglTempo"                       => "Tgl Tempo",
									"IFNULL(ttss.SSTgl,DATE('1900-01-01'))" => "Tgl Surat Jalan",
								], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-14">
                        <div class="form-group">
                            <div class="col-md-5">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
										'pluginOptions' => [
											'autoclose'      => true,
											'format'         => 'dd/mm/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                            <div class="col-md-5">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
							<label class="control-label col-sm-2 jurnalHide">Sort</label>
                            <div class="col-sm-8">
								<?= Html::dropDownList( 'sort', null, [
									"ttfb.FBNo"								   => "No Faktur Beli",        
									"ttfb.SSNo"                                => "No Surat Jln Supplier", 
									"ttfb.FBTgl"                               => "Tgl Faktur Beli",       
									"ttfb.FBTglTempo"                          => "Tgl Tempo",             
									"IFNULL(ttss.SSTgl,DATE('1900-01-01'))"    => "Tgl Surat Jalan",       
									"ttfb.SupKode"                             => "Kode Supplier",        
									"IFNULL(tdsupplier.SupNama, '--')"         => "Nama Supplier",        
									"ttfb.FBLunas"                             => "Lunas",                 
									"ttfb.fbMemo"                              => "Keterangan",            
									"ttfb.UserID"                              => "User",                  
									"IFNULL(ttgeneralledgerhd.GLLink,'--')"    => "No JT",                 
									"ttfb.FBTermin"                            => "Termin",                
									"ttfb.FBTotal"                             => "Total",                 
									"IFNULL(tMotor.fbJum,0)"                   => "Jumlah Motor",          
									"ttfb.FBPSS"                               => "Potongan Supplier",     
									"ttfb.FBPtgLain"                           => "Potongan Lain-Lain",    
									"IFNULL(tmotor.FBSubTotal, 0)"             => "Sub Total",      
								], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
							<div class="col-sm-4">
								<?= Html::dropDownList( 'order', null, [
									"ASC" => "ASC",
									"DESC" => "DESC",
								], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="container-fluid">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Supplier</label>
                        </div>
					</div>
					<div class="col-md-14">
                        <div class="form-group">
                            <div class="col-sm-18">
								<? echo Select2::widget( [
									'name'          => 'supplier',
									'data'          => $cmbBank,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatBank' ),
										'templateSelection' => new JsExpression( 'formatBank' ),
										'matcher'           => new JsExpression( 'matchCustom' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
                            </div>
							<label class="control-label col-sm-2">No SS</label>
							<div class="col-sm-4">
								<?= Html::dropDownList( 'noss', null, [
									"%" => "Semua",
									"SS" => "Sudah",
									"--" => "Belum",
								], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="container-fluid">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Warehouse/POS</label>
                        </div>
					</div>
					<div class="col-md-14">
                        <div class="form-group">
                            <div class="col-sm-18">
								<? echo Select2::widget( [
									'name'          => 'lokasi',
									'data'          => $cmbwarehouse,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatWarehouse' ),
										'templateSelection' => new JsExpression( 'formatWarehouse' ),
										'matcher'           => new JsExpression( 'matchCustom' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
                            </div>
							<label class="control-label col-sm-2">Lunas</label>
							<div class="col-sm-4">
								<?= Html::dropDownList( 'lunas', null, [
									"%" => "Semua",
									"Lunas" => "Lunas",
									"Belum" => "Belum",
								], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php

