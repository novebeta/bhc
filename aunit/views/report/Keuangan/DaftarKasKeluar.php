<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;

$this->title                   = 'Daftar Kas Keluar';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;

$sqlWarehouse = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, '' AS LokasiStatus, '' AS LokasiJenis, '' AS LokasiKode
	UNION
	SELECT LokasiKode AS value, LokasiNama AS label, LokasiStatus, LokasiJenis, LokasiKode FROM tdlokasi 
	WHERE LokasiStatus = '1' 
	UNION
	SELECT LokasiKode AS value, LokasiNama AS label, LokasiStatus, LokasiJenis, LokasiKode FROM tdlokasi 
	INNER JOIN traccount ON traccount.NoAccount = tdlokasi.NoAccount
	WHERE LokasiStatus <> '1' 
	ORDER BY LokasiStatus, LokasiKode" )
	->queryAll();
$cmbWarehouse = ArrayHelper::map( $sqlWarehouse, 'value', 'label' );

$sqlDealer = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, '' AS DealerStatus, '' AS DealerKode
	UNION
	SELECT DealerKode AS value, DealerNama AS label, DealerStatus, DealerKode FROM tdDealer 
	ORDER BY DealerStatus, DealerKode" )
	->queryAll();
$cmbDealer = ArrayHelper::map( $sqlDealer, 'value', 'label' );

$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
function formatDual(result) {
    return '<div class="row">' +
           '<div class="col-md-5">' + result.id + '</div>' +
           '<div class="col-md-19">' + result.text + '</div>' +
           '</div>';
}
function formatWarehouse(result) {
    return '<div class="row">' +
           '<div class="col-md-6">' + result.id + '</div>' +
           '<div class="col-md-18">' + result.text + '</div>' +
           '</div>';
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );

?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Kas Keluar'  => 'Kas Keluar',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-footer">
                <div class="container-fluid">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Tanggal</label>
                        </div>
                    </div>
                    <div class="col-md-16">
                        <div class="form-group">
                            <div class="col-md-5">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
										'pluginOptions' => [
											'autoclose'      => true,
											'format'         => 'dd/mm/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                            <div class="col-md-5">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
										'pluginOptions' => [
											'autoclose' => true,
											'format'    => 'dd/mm/yyyy'
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
							<label class="control-label col-sm-2 jurnalHide">Sort</label>
                            <div class="col-sm-8">
								<?= Html::dropDownList( 'sort', null, [
									"KKTgl"		=> "Tanggal",    
									"KKNo"      => "No KK",      
									"KKNominal" => "Nominal",    
									"KKJenis"   => "Jenis",      
									"KKPerson"  => "Person",     
									"KKLink"    => "No Link",    
									"KKMemo"    => "Keterangan", 
									"UserID"    => "UserID",               
							], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
							<div class="col-sm-4">
								<?= Html::dropDownList( 'order', null, [
									"ASC" => "ASC",
									"DESC" => "DESC",
								], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="container-fluid">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-sm-24">POS</label>
                        </div>
					</div>
					<div class="col-md-10">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? echo Select2::widget( [
									'name'          => 'lokasi',
									'data'          => $cmbWarehouse,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatWarehouse' ),
										'templateSelection' => new JsExpression( 'formatWarehouse' ),
										'matcher'           => new JsExpression( 'matchCustom' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
                            </div>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Jenis</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'jenis', null, [
									"%"					  => "Semua",            
									"Retur Harga"         => "Retur Harga",      
									"Potongan Khusus"     => "Potongan Khusus",  
									"SubsidiDealer2"      => "SubsidiDealer2",   
									"BBN Plus"            => "BBN Plus",         
									"Umum"                => "Umum",             
									"KK Ke Bank"          => "KK Ke Bank",       
									"KK Dealer Ke Pos"    => "KK Dealer Ke Pos", 
									"KK Pos Ke Dealer"    => "KK Pos Ke Dealer", 
									"Insentif Sales"      => "Insentif Sales",   
									"KK Ke Bank"          => "KK Ke Bank",       
									"Pinjaman Karyawan"   => "Pinjaman Karyawan",         
							], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
                        </div>
					</div>
                </div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
