<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\checkbox\CheckboxX;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
$this->title                   = 'Daftar Saldo Kas & Bank';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;

$sqlBank                           = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, '' AS NoParent, '' AS StatusAccount, '' AS NoAccount
	UNION
	SELECT NoAccount as value, NamaAccount as label, NoParent, StatusAccount, NoAccount FROM traccount 
	WHERE (NoParent IN ('11020100','11020200','11020300','11030000')) AND StatusAccount = 'A'
	ORDER BY StatusAccount, NoAccount" )
		->queryAll();
$cmbBank                           = ArrayHelper::map( $sqlBank, 'value', 'label' );

$sqlLeasing = Yii::$app->db
	->createCommand( " 
	SELECT '%' AS value, 'Semua' AS label, '' AS LokasiStatus, '' AS LokasiKode
	UNION
	SELECT LokasiKode AS value, LokasiNama As label , LokasiStatus , LokasiKode 
	FROM tdlokasi 
	WHERE (NoAccount LIKE '11%')
	ORDER BY LokasiStatus, LokasiKode"  )
	->queryAll();
$cmbLeasing = ArrayHelper::map( $sqlLeasing, 'value', 'label' );

$format = <<< SCRIPT
function formatBank(result) {
    return '<div class="row">' +
           '<div class="col-md-5">' + result.id + '</div>' +
           '<div class="col-md-19">' + result.text + '</div>' +
           '</div>';
}
function formatDual(result) {
    return '<div class="row">' +
           '<div class="col-md-5">' + result.id + '</div>' +
           '<div class="col-md-19">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
<div class="laporan-data col-md-24" style="padding-left: 0;">
    <div class="box box-primary">
		<?= Html::beginForm(); ?>
        <div class="box-body">
            <div class="container-fluid">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="control-label col-sm-4">Laporan</label>
                        <div class="col-sm-20">
							<?= Html::dropDownList( 'tipe', null, [
								'Saldo Kas'  => 'Saldo Kas',
								'Saldo Bank'  => 'Saldo Bank',
								'Saldo Kas Bank'  => 'Saldo Kas Bank',
								'Kwitansi Kirim Tagih'  => 'Kwitansi Kirim Tagih',
							], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label class="control-label col-sm-4">Format</label>
                        <div class="col-sm-20">
							<?= Html::dropDownList( 'filetype', null, [
								'pdf'  => 'PDF',
								'xlsr' => 'EXCEL RAW',
								'xls'  => 'EXCEL',
								'doc'  => 'WORD',
								'rtf'  => 'RTF',
							], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
			<div class="container-fluid">
				<div class="col-md-2">
					<div class="form-group">
						<label class="control-label col-sm-24">Tanggal</label>
					</div>
				</div>
				<div class="col-md-16">
					<div class="form-group">
						<div class="col-md-5">
							<? try {
								echo DatePicker::widget( [
									'name'          => 'tgl1',
                                    'id'            => 'tgl1_id',
									'type'          => DatePicker::TYPE_INPUT,
									'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
									'pluginOptions' => [
										'autoclose'      => true,
										'format'         => 'dd/mm/yyyy',
										'todayHighlight' => true
									]
								] );
							} catch ( \yii\base\InvalidConfigException $e ) {
							} ?>
						</div>
						<div class="col-md-5">
							<? try {
								echo DatePicker::widget( [
									'name'          => 'tgl2',
                                    'id'            => 'tgl2_id',
									'type'          => DatePicker::TYPE_INPUT,
									'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
									'pluginOptions' => [
										'autoclose' => true,
										'format'    => 'dd/mm/yyyy'
									]
								] );
							} catch ( \yii\base\InvalidConfigException $e ) {
							} ?>
						</div>
						<div class="col-sm-6">
								<? try {
									echo CheckboxX::widget([
										'name'=>'jurnal',
										'value' => true,
										'options'=>['id'=>'jurnal'],
										'pluginOptions'=>['threeState'=>false, 'size'=>'lg']
									]);
									echo '<label class="cbx-label" for="jurnal">Jurnal</label>';
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
						</div>
					</div>
				</div>
			</div>
            <div class="container-fluid">
				<div class="col-md-2">
                    <div class="form-group">
                        <label class="control-label col-sm-24">Bank</label>
                    </div>
                </div>
                <div class="col-md-15">
                    <div class="form-group">
                        <div class="col-sm-24">
							<? echo Select2::widget( [
								'name'          => 'bank',
								'data'          => $cmbBank,
								'options'       => [ 'class' => 'form-control' ],
								'theme'         => Select2::THEME_DEFAULT,
								'pluginOptions' => [
									'dropdownAutoWidth' => true,
									'templateResult'    => new JsExpression( 'formatBank' ),
									'templateSelection' => new JsExpression( 'formatBank' ),
									'matcher'           => new JsExpression( 'matchCustom' ),
									'escapeMarkup'      => $escape,
								],
							] ); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
				<div class="col-md-2">
                    <div class="form-group">
                        <label class="control-label col-sm-24">POS</label>
                    </div>
                </div>
                <div class="col-md-15">
                    <div class="form-group">
                        <div class="col-sm-24">
							<? echo Select2::widget( [
								'name'          => 'lokasi',
								'data'          => $cmbLeasing,
								'options'       => [ 'class' => 'form-control' ],
								'theme'         => Select2::THEME_DEFAULT,
								'pluginOptions' => [
									'dropdownAutoWidth' => true,
									'templateResult'    => new JsExpression( 'formatDual' ),
									'templateSelection' => new JsExpression( 'formatDual' ),
									'matcher'           => new JsExpression( 'matchCustom' ),
									'escapeMarkup'      => $escape,
								],
							] ); ?>
                        </div>
                    </div>
                </div>

            </div>
		</div>
		<?= Html::endForm(); ?>
    </div>
</div>

<?php

$this->registerJs( <<< JS
       
    $("#tgl2_id").change(function () {
    var tgl1 = $('#tgl1_id').val();
    var tgl2 = this.value;
    console.log(tgl1);
    console.log(tgl2);
    if(tgl2 < tgl1){
        $('#tgl1_id').val(tgl2);
        }
    });

    $("#tgl2_id").trigger('change');
    
JS
);
    

