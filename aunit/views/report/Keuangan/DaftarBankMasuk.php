<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
$this->title                   = 'Daftar Bank Masuk';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;

$sqlBank                           = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, '' AS NoParent, '' AS StatusAccount, '' AS NoAccount
	UNION
	SELECT NoAccount as value, NamaAccount as label, NoParent, StatusAccount, NoAccount FROM traccount 
	WHERE (NoParent IN ('11020100','11020200','11020300','11030000')) AND StatusAccount = 'A'
	ORDER BY StatusAccount, NoAccount" )
		->queryAll();
$cmbBank                           = ArrayHelper::map( $sqlBank, 'value', 'label' );

$sqlLeasing = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, '' AS LeaseStatus, '' AS LeaseKode
	UNION
	SELECT LeaseKode AS value, LeaseNama As label , LeaseStatus , LeaseKode FROM tdLeasing 
	WHERE LeaseStatus = 'A' 
	ORDER BY LeaseStatus, LeaseKode" )
	->queryAll();
$cmbLeasing = ArrayHelper::map( $sqlLeasing, 'value', 'label' );

$format = <<< SCRIPT
function formatBank(result) {
    return '<div class="row">' +
           '<div class="col-md-5">' + result.id + '</div>' +
           '<div class="col-md-19">' + result.text + '</div>' +
           '</div>';
}
function formatDual(result) {
    return '<div class="row">' +
           '<div class="col-md-3">' + result.id + '</div>' +
           '<div class="col-md-21">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
<div class="laporan-data col-md-24" style="padding-left: 0;">
    <div class="box box-primary">
		<?= Html::beginForm(); ?>
        <div class="box-body">
            <div class="container-fluid">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="control-label col-sm-4">Laporan</label>
                        <div class="col-sm-20">
							<?= Html::dropDownList( 'tipe', null, [
								'BM Header'  => 'Bank Masuk Header',
								'BM Detail DK' => 'Bank Masuk Detail DK',
								'BM Detail KK' => 'Bank Masuk Detail KK',
						], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label class="control-label col-sm-4">Format</label>
                        <div class="col-sm-20">
							<?= Html::dropDownList( 'filetype', null, [
								'pdf'  => 'PDF',
								'xlsr' => 'EXCEL RAW',
								'xls'  => 'EXCEL',
								'doc'  => 'WORD',
								'rtf'  => 'RTF',
							], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
			<div class="container-fluid">
				<div class="col-md-2">
					<div class="form-group">
						<label class="control-label col-sm-24">Tanggal</label>
					</div>
				</div>
				<div class="col-md-16">
					<div class="form-group">
						<div class="col-md-5">
							<? try {
								echo DatePicker::widget( [
									'name'          => 'tgl1',
									'type'          => DatePicker::TYPE_INPUT,
									'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
									'pluginOptions' => [
										'autoclose'      => true,
										'format'         => 'dd/mm/yyyy',
										'todayHighlight' => true
									]
								] );
							} catch ( \yii\base\InvalidConfigException $e ) {
							} ?>
						</div>
						<div class="col-md-5">
							<? try {
								echo DatePicker::widget( [
                                        'name'          => 'tgl2',
									'type'          => DatePicker::TYPE_INPUT,
									'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
									'pluginOptions' => [
										'autoclose' => true,
										'format'    => 'dd/mm/yyyy'
									]
								] );
							} catch ( \yii\base\InvalidConfigException $e ) {
							} ?>
						</div>
						<label class="control-label col-sm-2 jurnalHide">Sort</label>
						<div class="col-sm-8">
							<?= Html::dropDownList( 'sort', null, [
								"BMTgl"       =>   "Tanggal",        
								"BMNo"        =>   "No BM",          
								"BMNominal"   =>   "Nominal",        
								"BMJenis"     =>   "Jenis",          
								"BMPerson"    =>   "Person",         
								"NoAccount"   =>   "No Account Bank",
								"BMCekNo"     =>   "No Cek",         
								"BMCekTempo"  =>   "Tgl Tempo",      
								"BMAC"        =>   "No AC",          
								"BMPerson"    =>   "Person",         
								"BMMemo"      =>   "Keterangan",     
								"LeaseKode"   =>   "Kode Leasing",   
								"UserID"      =>   "UserID",     
						], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
						</div>
						<div class="col-sm-4">
							<?= Html::dropDownList( 'order', null, [
								"ASC" => "ASC",
								"DESC" => "DESC",
							], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
						</div>
					</div>
				</div>
			</div>
            <div class="container-fluid">
				<div class="col-md-2">
                    <div class="form-group">
                        <label class="control-label col-sm-24">Bank</label>
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <div class="col-sm-24">
							<? echo Select2::widget( [
								'name'          => 'bank',
								'data'          => $cmbBank,
								'options'       => [ 'class' => 'form-control' ],
								'theme'         => Select2::THEME_DEFAULT,
								'pluginOptions' => [
									'dropdownAutoWidth' => true,
									'templateResult'    => new JsExpression( 'formatBank' ),
									'templateSelection' => new JsExpression( 'formatBank' ),
									'matcher'           => new JsExpression( 'matchCustom' ),
									'escapeMarkup'      => $escape,
								],
							] ); ?>
                        </div>
                    </div>
                </div>
				<div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-sm-4">Jenis</label>
						<div class="col-sm-20">
							<?= Html::dropDownList( 'jenis', null, [
								"%"				=> "Semua",             
								"Tunai"           => "Tunai",          
								"Kredit"          => "Kredit",         
								"Inden"           => "Inden",          
								"Leasing"         => "Leasing",        
								"Piutang Sisa"    => "Piutang Sisa",   
								"Piutang UM"      => "Piutang UM",     
								"Scheme"          => "Scheme",         
								"BBN Plus"        => "BBN Plus",       
								"Penjualan Multi" => "Penjualan Multi",
								"Bank Transfer"   => "Bank Transfer",  
								"Neraca Awal"     => "Neraca Awal",         
						], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                        </div>
                    </div>
				</div>
            </div>
            <div class="container-fluid">
				<div class="col-md-2">
                    <div class="form-group">
                        <label class="control-label col-sm-24">Leasing</label>
                    </div>
                </div>
                <div class="col-md-16">
                    <div class="form-group">
                        <div class="col-sm-24">
							<? echo Select2::widget( [
								'name'          => 'leasing',
								'data'          => $cmbLeasing,
								'options'       => [ 'class' => 'form-control' ],
								'theme'         => Select2::THEME_DEFAULT,
								'pluginOptions' => [
									'dropdownAutoWidth' => true,
									'templateResult'    => new JsExpression( 'formatDual' ),
									'templateSelection' => new JsExpression( 'formatDual' ),
									'matcher'           => new JsExpression( 'matchCustom' ),
									'escapeMarkup'      => $escape,
								],
							] ); ?>
                        </div>
                    </div>
                </div>

            </div>
		</div>
		<?= Html::endForm(); ?>
    </div>
</div>

