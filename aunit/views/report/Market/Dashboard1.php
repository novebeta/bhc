<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
$this->title                   = 'Dashboard 1';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;
?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Team Cash Credit' => 'Team Cash Credit',
									'Team Potongan - Segmen' => 'Team Potongan - Segmen',
									'Team DP Unit - Persen' => 'Team DP Unit - Persen',
									'Penjualan Sales' => 'Penjualan Sales',
									'Sales Cash Credit' => 'Sales Cash Credit',
									'Sales Potongan' => 'Sales Potongan',
									'Type Cash Credit' => 'Type Cash Credit',
									'Type DP Unit' => 'Type DP Unit',
									'Sales Leasing Harian' => 'Sales Leasing Harian',
									'Sales Leasing Akumulatif' => 'Sales Leasing Akumulatif',
									'Leasing Team' => 'Leasing Team',
									'Team Leasing' => 'Team Leasing',
                                    'RataRataPotonganPerUnit' => 'Rata Rata Potongan',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-footer">
                <div class="col-md-3">
                    <div class="form-group">
                        <div class="col-sm-24">
							<?= Html::dropDownList( 'stattgl', null, [
								"ttsk.SKTgl" => "Tgl SK",
								"ttdk.DKTgl" => "Tgl DK",
							], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="col-md-12">
							<? try {
								echo DatePicker::widget( [
									'name'          => 'tgl1',
									'type'          => DatePicker::TYPE_INPUT,
									'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
									'pluginOptions' => [
										'autoclose'      => true,
										'format'         => 'dd/mm/yyyy',
										'todayHighlight' => true
									]
								] );
							} catch ( \yii\base\InvalidConfigException $e ) {
							} ?>
                        </div>
                        <div class="col-md-12">
							<? try {
								echo DatePicker::widget( [
									'name'          => 'tgl2',
									'type'          => DatePicker::TYPE_INPUT,
									'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
									'pluginOptions' => [
										'autoclose' => true,
										'format'    => 'dd/mm/yyyy'
									]
								] );
							} catch ( \yii\base\InvalidConfigException $e ) {
							} ?>
                        </div>
                    </div>
                </div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
