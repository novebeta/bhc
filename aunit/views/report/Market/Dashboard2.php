<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\checkbox\CheckboxX;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
$this->title                   = 'Dashboard 2';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;
?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Sales Kecamatan In' => 'Sales Kecamatan In',
									'Sales Kecamatan Out' => 'Sales Kecamatan Out',
									'Umur Motor' => 'Umur Motor',
									'Sa Di Stock' => 'Sa Di Stock',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-footer">
                <div class="col-md-3">
                    <div class="form-group">
                        <div class="col-sm-24">
							<?= Html::dropDownList( 'tanggal', null, [
								"ttsk.SKTgl" => "Tgl SK",
								"ttdk.DKTgl" => "Tgl DK",
							], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <div class="col-md-6" Tgl1Hide>
							<? try {
								echo DatePicker::widget( [
									'name'          => 'tgl1',
									'type'          => DatePicker::TYPE_INPUT,
									'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
									'pluginOptions' => [
										'autoclose'      => true,
										'format'         => 'dd/mm/yyyy',
										'todayHighlight' => true
									]
								] );
							} catch ( \yii\base\InvalidConfigException $e ) {
							} ?>
                        </div>
                        <div class="col-md-6">
							<? try {
								echo DatePicker::widget( [
									'name'          => 'tgl2',
									'type'          => DatePicker::TYPE_INPUT,
                                    'value'         => date('t/m/Y',strtotime("today")),
									'pluginOptions' => [
										'autoclose' => true,
										'format'    => 'dd/mm/yyyy'
									]
								] );
							} catch ( \yii\base\InvalidConfigException $e ) {
							} ?>
                        </div>
						<div class="col-sm-4">
						<? try {
									echo CheckboxX::widget([
										'name'=>'eom',
										'options'=>['id'=>'eom'],
										'value'=>1,
										'pluginOptions'=>['threeState'=>false, 'size'=>'lg']
									]);
									echo '<label class="cbx-label" for="eom">EOM</label>';
								} catch ( \yii\base\InvalidConfigException $e ) {
							} ?>
						</div>
						<div class="col-sm-6">
							<? try {
								echo CheckboxX::widget([
									'name'=>'ringkas',
									'options'=>['id'=>'ringkas'],
									'value'=>1,
									'pluginOptions'=>['threeState'=>false, 'size'=>'lg']
								]);
								echo '<label class="cbx-label" for="ringkas">Ringkas</label>';
							} catch ( \yii\base\InvalidConfigException $e ) {
							} ?>
						</div>
                    </div>
                </div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
$this->registerJs( $this->render( 'Dashboard2.js' ) );


