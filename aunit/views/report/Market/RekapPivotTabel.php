<?php
use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\checkbox\CheckboxX;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
$this->title                   = 'Rekap Pivot Tabel';
$this->params['breadcrumbs'][] = $this->title;
$formatter                     = \Yii::$app->formatter;

$sqlSales = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, '' AS SalesStatus, '' AS SalesKode
	UNION
	SELECT SalesKode AS value, SalesNama As label , SalesStatus , SalesKode FROM tdSales 
	WHERE SalesStatus = 'A' 
	ORDER BY SalesStatus, SalesKode" )
	->queryAll();
$cmbSales = ArrayHelper::map( $sqlSales, 'value', 'label' );

$sqlWarehouse = Yii::$app->db
	->createCommand( "
	SELECT '%' AS value, 'Semua' AS label, '' AS LokasiStatus, '' AS LokasiJenis, '' AS LokasiKode
	UNION
	SELECT LokasiKode AS value, LokasiNama AS label, LokasiStatus, LokasiJenis, LokasiKode FROM tdlokasi 
	WHERE LokasiJenis <> 'Kas' 
	ORDER BY LokasiStatus, LokasiKode" )
	->queryAll();
$cmbWarehouse = ArrayHelper::map( $sqlWarehouse, 'value', 'label' );

$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
function formatDual(result) {
    return '<div class="row">' +
           '<div class="col-md-4">' + result.id + '</div>' +
           '<div class="col-md-20">' + result.text + '</div>' +
           '</div>';
}
function formatWarehouse(result) {
    return '<div class="row">' +
           '<div class="col-md-6">' + result.id + '</div>' +
           '<div class="col-md-18">' + result.text + '</div>' +
           '</div>';
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );

?>
    <div class="laporan-data col-md-24" style="padding-left: 0;">
        <div class="box box-primary">
			<?= Html::beginForm(); ?>
			<div class="box-body">
				<div class="container-fluid">
					<div class="col-md-10">
						<div class="form-group">
							<label class="control-label col-sm-4">Laporan</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'tipe', null, [
									'Kecamatan IN' => 'Kecamatan IN',
									'Kecamatan OUT' => 'Kecamatan OUT',
									'POS' => 'POS',
									'Sales' => 'Sales',
									'Segmen' => 'Segmen',
									'Area' => 'Area',
									'Tunai-Kredit' => 'Tunai-Kredit',
									'Tenor' => 'Tenor',
									'Uang Muka' => 'Uang Muka',
								], [ 'id' => 'tipe-id', 'text' => 'Pilih tipe laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="form-group">
							<label class="control-label col-sm-4">Format</label>
							<div class="col-sm-20">
								<?= Html::dropDownList( 'filetype', null, [
									'pdf'  => 'PDF',
									'xlsr' => 'EXCEL RAW',
									'xls'  => 'EXCEL',
									'doc'  => 'WORD',
									'rtf'  => 'RTF',
								], [ 'text' => 'Pilih format laporan', 'class' => 'form-control' ] ) ?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<button type="submit" formtarget="_blank" class="btn btn-primary glyphicon glyphicon-new-window"> Tampil</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-footer">
                <div class="container-fluid">
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="col-sm-24">
								<?= Html::dropDownList( 'stattgl', null, [
									"ttsk.SKTgl" => "Tgl SK",
									"ttdk.DKTgl" => "Tgl DK",
								], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-12">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl1',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','01/MM/yyyy'),
										'pluginOptions' => [
											'autoclose'      => true,
											'format'         => 'dd/mm/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                            <div class="col-md-12">
								<? try {
									echo DatePicker::widget( [
										'name'          => 'tgl2',
										'type'          => DatePicker::TYPE_INPUT,
										'value'         => Yii::$app->formatter->asDate( 'now','dd/MM/yyyy'),
										'pluginOptions' => [
											'autoclose'      => true,
											'format'         => 'dd/mm/yyyy',
											'todayHighlight' => true
										]
									] );
								} catch ( \yii\base\InvalidConfigException $e ) {
								} ?>
                            </div>
                        </div>
                    </div>
					<div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-sm-6">Jenis</label>
							<div class="col-sm-18">
								<?= Html::dropDownList( 'jenis', null, [
									"%" => "Semua",
									"Tunai" => "Tunai",
									"Kredit" => "Kredit",
								], [ 'text' => '', 'class' => 'form-control jurnalHide' ] ) ?>
                            </div>
                        </div>
                    </div>
					<div class="col-sm-4">
						<? try {
							echo CheckboxX::widget([
								'name'=>'delnol',
								'value' => true,
								'options'=>['id'=>'s_1'],
								'pluginOptions'=>['threeState'=>false, 'size'=>'lg']
							]);
							echo '<label class="cbx-label" for="s_1">Del 0</label>';
						} catch ( \yii\base\InvalidConfigException $e ) {
						} ?>
					</div>
                </div>
				<div class="container-fluid">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Sales</label>
                        </div>
					</div>
					<div class="col-md-14">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? echo Select2::widget( [
									'name'          => 'sales',
									'data'          => $cmbSales,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatDual' ),
										'templateSelection' => new JsExpression( 'formatDual' ),
										'matcher'           => new JsExpression( 'matchCustom' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="container-fluid">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-sm-24">Warehouse/POS</label>
                        </div>
					</div>
					<div class="col-md-14">
                        <div class="form-group">
                            <div class="col-sm-24">
								<? echo Select2::widget( [
									'name'          => 'lokasi',
									'data'          => $cmbWarehouse,
									'options'       => [ 'class' => 'form-control' ],
									'theme'         => Select2::THEME_DEFAULT,
									'pluginOptions' => [
										'dropdownAutoWidth' => true,
										'templateResult'    => new JsExpression( 'formatWarehouse' ),
										'templateSelection' => new JsExpression( 'formatWarehouse' ),
										'matcher'           => new JsExpression( 'matchCustom' ),
										'escapeMarkup'      => $escape,
									],
								] ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?= Html::endForm(); ?>
        </div>
    </div>
<?php
