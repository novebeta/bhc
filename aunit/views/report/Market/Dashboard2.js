jQuery(function ($) {
    $("#tipe-id").change(function () {
        var tipe = this.value;
        if (tipe === "Umur Motor" || tipe === "Umur Motor") {
            $(".Tgl1Hide").hide();
        } else {
            $(".Tgl1Hide").show();
        }
    });
    $('#tipe-id').trigger('change');
});