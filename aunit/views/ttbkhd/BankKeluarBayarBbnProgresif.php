<?php
use aunit\assets\AppAsset;
use common\components\Custom;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bank Keluar Bayar BBN Progresif';
$this->params['breadcrumbs'][] = $this->title;
$arr = [
    'cmbTxt'    => [
        'BKNo'       => 'No Bank Keluar',
        'BKPerson'   => 'Bayar Ke',
        'BKJenis'    => 'Jenis',
        'SupKode'    => 'Kode Sup',
        'NoAccount'  => 'Nomor Account',
        'BKCekNo'    => 'Nomor Cek',
        'BKAC'       => 'Nomor A/C',
        'BKMemo'     => 'Keterangan',
        'UserID'     => 'User ID',
        'NoGL'       => 'No Gl',	],
    'cmbTgl'    => [
        'BKTgl'      => 'Tgl Bank Keluar',
    ],
    'cmbNum'    => [
        'BKNominal'  => 'Nominal',
    ],
    'sortname'  => "BKTgl",
    'sortorder' => 'desc'
];


AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttbkhd::className(),
	'Bank Keluar Bayar BBN Progresif',
    [
        'url_update' => Url::toRoute( [ 'ttbkhd/bank-keluar-bayar-bbn-progresif-update' ,'action' => 'update'] ),
        'url_add'    => Url::toRoute( [ 'ttbkhd/bank-keluar-bayar-bbn-progresif-create','action' => 'create' ] ),
        'url_delete' => Url::toRoute( [ 'ttbkhd/delete' ] ),

//        'url_add' => Custom::url('ttbkhd/bank-keluar-bayar-bbn-progresif-create'),
//        'url_update' => Custom::url('ttbkhd/bank-keluar-bayar-bbn-progresif-update'),
//        'url_delete' => Custom::url('ttbkhd/td'),
    ]
    ), \yii\web\View::POS_READY );
