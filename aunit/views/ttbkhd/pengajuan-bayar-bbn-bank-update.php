<?php
use common\components\Custom;
use yii\helpers\Url;

//$params = '&id='.$id;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbkhd */
$params                          = '&id=' . $id . '&action=update&JenisBBN=Pengajuan';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode).' - Pengajuan Bayar BBN Via Bank : ' . $dsTUang['BKNoView'];
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Pengajuan Bayar BBN Via Bank', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = 'Edit';
?>
<div class="ttbkhd-update">
    <?= $this->render('_pengajuan-bayar-bbn-bank-form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'id'     => $id,
        'url' => [
            'update'    => Custom::url(\Yii::$app->controller->id.'/pengajuan-bayar-bbn-bank-update'.$params ),
            'print'     => Custom::url(\Yii::$app->controller->id.'/print'.$params ),
            'cancel'    => $cancel,
            'detail'    => Url::toRoute( [ 'ttbkit/index','action' => 'create' ] ),
        ]
    ]) ?>

</div>
