<?php
use common\components\Custom;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbkhd */
$params                          = '&id=' . $id . '&action=UpdateAdd';
$cancel                          = Custom::url( 'ttbkhd/bank-transfer-cancel'.$params );
$this->title                     = "Tambah - $title";
//$this->params[ 'breadcrumbs' ][] = [ 'label' => $title, 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="ttbkhd-create">
    <?= $this->render('_bank-transfer-form', [
        'dsTUang' => $dsTUang,
        'id'     => $id,
         'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/bank-transfer-update'. $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail'       => Url::toRoute( [ 'ttbkit/index','action' => 'create' ] ),
        ]
    ]) ?>

</div>
