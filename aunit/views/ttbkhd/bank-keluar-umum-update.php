<?php
use common\components\Custom;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbkhd */
$params                          = '&id=' . $id . '&action=update&BKJenis='.$model->BKJenis;
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode).'- Bank Keluar Umum : ' . $dsTUang['BKNoView'];
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Bank Keluar Umum', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = 'Edit';

//$params                          = '&id=' . $id . '&action=update';
//$cancel                          = Custom::url( \Yii::$app->controller->id . '/index' );
//$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode).'- Bank Keluar Umum : ' . $model->BKNo;
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Bank Keluar Umum', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = 'Edit';
?>
<div class="ttbkhd-update">
    <?= $this->render('_bank-keluar-umum-form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'id'     => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/bank-keluar-umum-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttbkitcoa/index','action' => 'create' ] ),
        ]
//        'url'    => [
//            'update' => Custom::url( \Yii::$app->controller->id . '/bank-keluar-umum-update' . $params ),
//            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
//            'cancel' => $cancel,
//            'detail' => Url::toRoute( [ 'ttbkitcoa/index','action' => 'create' ] ),
//        ]
    ]) ?>

</div>
