<?php
use common\components\Custom;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbkhd */
$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode).'- Bank Keluar Kas : ' . $dsTUang['BKNoView'];
?>
<div class="ttbkhd-update">
    <?= $this->render('_bank-keluar-kas-form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'id'     => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/bank-keluar-kas-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
        ]
    ]) ?>

</div>
