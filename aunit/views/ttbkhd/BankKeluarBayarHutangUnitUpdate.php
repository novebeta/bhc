<?php

use yii\helpers\Html;
use common\components\Custom;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbkhd */
$params                          = '&id=' . $id . '&action=update&BKJenis='.$model->BKJenis;
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode).'- Bank Keluar Hutang Unit : ' . $dsTUang['BKNoView'];
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Bank Keluar Hutang Unit', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = 'Edit';

//$this->title = str_replace('Menu','',\aunit\components\TUi::$actionMode).'Bank Keluar Hutang Unit : ' .$model->BKNo;
//$this->params['breadcrumbs'][] = ['label' => 'Bank Keluar Bayar Hutang Unit', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->BKNo, 'url' => ['view', 'id' => $model->BKNo]];
//$this->params['breadcrumbs'][] = 'Edit';
//$params = '&id='.$id;
?>
<div class="ttbkhd-update">
    <?= $this->render('_bank-keluar-unit-form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'id'     => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/bank-keluar-bayar-hutang-unit-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail'       => Url::toRoute( [ 'ttbkit/index','action' => 'update' ] ),
        ]
//        'url' => [
//            'update' => Url::toRoute([\Yii::$app->controller->id . '/update', 'id' => $id, 'action' => 'update']),
//            'print' => Url::toRoute([\Yii::$app->controller->id . '/print', 'id' => $id, 'action' => 'update']),
//            'cancel' => Url::toRoute([\Yii::$app->controller->id . '/cancel', 'id' => $id, 'action' => 'update']),
//            'detail'       => Url::toRoute( [ 'ttbkit/index','action' => 'update' ] ),
//        ]
    ]) ?>

</div>
