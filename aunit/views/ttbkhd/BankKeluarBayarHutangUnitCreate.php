<?php
use common\components\Custom;
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbkhd */
$params                          = '&id=' . $id . '&action=create&BKJenis='.$model->BKJenis;
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = "Tambah - Bank Keluar Bayar Hutang Unit";
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Bank Keluar Bayar Hutang Unit', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="ttbmhd-create">
    <?= $this->render('_bank-keluar-unit-form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'id'     => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/bank-keluar-bayar-hutang-unit-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail'       => Url::toRoute( [ 'ttbkit/index','action' => 'update' ] ),
        ]
    ]) ?>

</div>
