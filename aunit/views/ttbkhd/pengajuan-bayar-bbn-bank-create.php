<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbkhd */
$params                          = '&id=' . $id . '&action=create&JenisBBN=Pengajuan';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel' . $params );
$this->title                     = 'Tambah - Pengajuan Bayar BBN via Bank';
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Pengajuan Bayar BBN via Bank', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="ttbmhd-create">
	<?= $this->render( '_pengajuan-bayar-bbn-bank-form', [
		'model'   => $model,
		'dsTUang' => $dsTUang,
		'id'      => $id,
		'url'     => [
			'update' => Custom::url( \Yii::$app->controller->id . '/pengajuan-bayar-bbn-bank-update' . $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
			'cancel' => $cancel,
			'detail' => Url::toRoute( [ 'ttbkit/index', 'action' => 'create' ] ),
		]
	] ) ?>
</div>
