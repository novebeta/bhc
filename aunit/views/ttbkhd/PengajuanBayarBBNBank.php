<?php
use aunit\assets\AppAsset;
use aunit\models\Ttbkhd;
use yii\helpers\Url;
AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = 'Pengajuan Bayar BBN via Bank';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'BKNo'      => 'No Bank Keluar',
		'BKPerson'  => 'Bayar Ke',
		'BKJenis'   => 'Jenis',
		'SupKode'   => 'Kode Sup',
		'NoAccount' => 'Nomor Account',
		'BKCekNo'   => 'Nomor Cek',
		'BKAC'      => 'Nomor A/C',
		'BKMemo'    => 'Keterangan',
		'UserID'    => 'User ID',
		'NoGL'      => 'No Gl', ],
	'cmbTgl'    => [
		'BKTgl' => 'Tgl Bank Keluar',
	],
	'cmbNum'    => [
		'BKNominal' => 'Nominal',
	],
	'sortname'  => "BKTgl",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttbkhd::className(),
	'Pengajuan Bayar BBN via Bank', [
		'url_update' => Url::toRoute( [ 'ttbkhd/pengajuan-bayar-bbn-bank-update','JenisBBN'=>'Pengajuan', 'action' => 'update' ] ),
		'url_add'    => Url::toRoute( [ 'ttbkhd/pengajuan-bayar-bbn-bank-create','JenisBBN'=>'Pengajuan', 'action' => 'create' ] ),
		'url_delete' => Url::toRoute( [ 'ttbkhd/delete' ,'JenisBBN'=>'Pengajuan'] ),
		'colGrid'    => Ttbkhd::colGridPengajuanBayarBBNBank(),
	] ), \yii\web\View::POS_READY );



