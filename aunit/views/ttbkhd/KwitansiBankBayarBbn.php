<?php
use aunit\assets\AppAsset;
use aunit\components\TdUi;
use aunit\models\Ttbkhd;
use yii\helpers\Url;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = 'BK -> Kwitansi Bank Bayar BBN';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'BKNo'       => 'No BK',
		'BKMemo'     => 'Keterangan',
		'BKJenis'    => 'Jenis',
		'SupKode'    => 'Supplier',
		'BKCekNo'    => 'No Cek',
		'BKCekTempo' => 'Cek Tempo',
		'BKPerson'   => 'Bayar Ke',
		'BKAC'       => 'AC',
		'NoAccount'  => 'No Account',
		'UserID'     => 'User ID',
		'NoGL'       => 'No Gl', ],
	'cmbTgl'    => [
		'BKTgl' => 'Tanggal BK',
	],
	'cmbNum'    => [
		'BKNominal' => 'Nominal',
	],
	'sortname'  => "BKTgl",
	'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( TdUi::mainGridJs( Ttbkhd::class,
	'BK -> Kwitansi Bank Bayar BBN', [
		'btnAdd_Disabled'    => true,
		'btnDelete_Disabled' => true,
		'btnUpdate_Disabled' => true,
		'url_view'           => Url::toRoute( [ 'ttbkhd/bank-keluar-bayar-hutang-bbn-update', 'mode' => 'kwitansi-bank-bayar-bbn', 'action' => 'view' ] ),
	] ), View::POS_READY );
