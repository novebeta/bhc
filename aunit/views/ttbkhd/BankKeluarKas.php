<?php

use aunit\assets\AppAsset;
use yii\helpers\Html;
use yii\grid\GridView;
use common\components\Custom;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bank Keluar ke Kas';
$this->params['breadcrumbs'][] = $this->title;
$arr = [
    'cmbTxt' => [
        'BKNo' => 'No Bank Keluar',
        'BKPerson' => 'Bayar Ke',
        'BKJenis' => 'Jenis',
        'SupKode' => 'Kode Sup',
        'NoAccount' => 'Nomor Account',
        'BKCekNo' => 'Nomor Cek',
        'BKAC' => 'Nomor A/C',
        'BKMemo' => 'Keterangan',
        'UserID' => 'User ID',
        'NoGL' => 'No Gl',],
    'cmbTgl' => [
        'BKTgl' => 'Tgl Bank Keluar',
    ],
    'cmbNum' => [
        'BKNominal' => 'Nominal',
    ],
    'sortname' => "BKTgl",
    'sortorder' => 'desc'
];

AppAsset::register($this);
$this->registerJsVar('setcmb', $arr);
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render('../_search'); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs(\aunit\components\TdUi::mainGridJs(\aunit\models\Ttbkhd::className(),
    'Bank Keluar Ke Kas',    [
        'url_add' => \yii\helpers\Url::toRoute( ['ttbkhd/bank-keluar-kas-create', 'action' => 'create' ] ),
        'url_update' => \yii\helpers\Url::toRoute( ['ttbkhd/bank-keluar-kas-update', 'action' => 'update' ] ),
        'url_delete' => \yii\helpers\Url::toRoute( ['ttbkhd/delete'] ),
    ]
), \yii\web\View::POS_READY);
