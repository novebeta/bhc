<?php
use aunit\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
\aunit\assets\AppAsset::register( $this );

$url[ 'detail' ] = '';
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbkhd */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttbkhd-form">
		<?php
		$form = ActiveForm::begin( [ 'action' => $url[ 'update' ], 'id' => 'form_ttbkhd_id',  'fieldConfig' => [ 'template' => "{input}" ] ] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelNoBT" ],
						  [ 'class' => "col-sm-3", 'items' => ":NoBT" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelTglBT" ],
						  [ 'class' => "col-sm-3", 'items' => ":TglBT" ],
						  [ 'class' => "col-sm-3", 'items' => ":BTJam" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelNoBK" ],
						  [ 'class' => "col-sm-3", 'items' => ":NoBK" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelNoBM" ],
						  [ 'class' => "col-sm-3", 'items' => ":NoBM" ],
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelJenisBT" ],
						  [ 'class' => "col-sm-3", 'items' => ":JenisBT" ],
						  [ 'class' => "col-sm-10" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelJTBK" ],
						  [ 'class' => "col-sm-3", 'items' => ":JTBK" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelBMJT" ],
						  [ 'class' => "col-sm-3", 'items' => ":BMJT" ],
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelOutBank" ],
						  [ 'class' => "col-sm-11", 'items' => ":OutBank" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelBayarKe" ],
						  [ 'class' => "col-sm-8", 'items' => ":BayarKe" ],
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelInACNo" ],
						  [ 'class' => "col-sm-4", 'items' => ":InACNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelInCekNo" ],
						  [ 'class' => "col-sm-4", 'items' => ":InCekNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelInTglCair" ],
						  [ 'class' => "col-sm-3", 'items' => ":InTglCair" ],
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelINBank" ],
						  [ 'class' => "col-sm-11", 'items' => ":INBank" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelTerima" ],
						  [ 'class' => "col-sm-8", 'items' => ":Terima" ],
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelOutACNo" ],
						  [ 'class' => "col-sm-4", 'items' => ":OutACNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelOutCekNo" ],
						  [ 'class' => "col-sm-4", 'items' => ":OutCekNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelOutTglCair" ],
						  [ 'class' => "col-sm-3", 'items' => ":OutTglCair" ],
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelKeterangan" ],
						  [ 'class' => "col-sm-15", 'items' => ":Keterangan" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelNominal" ],
						  [ 'class' => "col-sm-4", 'items' => ":Nominal" ],
					  ],
					],
				]
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
                  [
                      'class' => "col-md-12 pull-left",
                      'items' => $this->render( '../_nav', [
                          'url'=> $_GET['r'],
                          'options'=> [
                              'BKNo'       => 'No Bank Keluar',
                              'BKPerson'   => 'Bayar Ke',
                              'BKJenis'    => 'Jenis',
                              'SupKode'    => 'Kode Sup',
                              'NoAccount'  => 'Nomor Account',
                              'BKCekNo'    => 'Nomor Cek',
                              'BKAC'       => 'Nomor A/C',
                              'BKMemo'     => 'Keterangan',
                              'UserID'     => 'User ID',
                              'NoGL'       => 'No Gl',
                          ],
                      ])
                  ],
				  [ 'class' => "col-md-24",
				    'items' => [
					    [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
				    ]
				  ]
			  ]
			],
			[
				/* label */
				":LabelNoBT"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No BT</label>',
				":LabelTglBT"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl BT</label>',
				":LabelNoBK"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No BK</label>',
				":LabelNoBM"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No BM</label>',
				":LabelJenisBT"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				":LabelJTBK"       => \common\components\General::labelGL( $dsTUang[ 'BKNoGL' ], 'JT BK' ),
				":LabelBMJT"       => \common\components\General::labelGL( $dsTUang[ 'BMNoGL' ], 'JT BM' ),
				":LabelINBank"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bank Masuk</label>',
				":LabelBayarKe"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bayar Ke</label>',
				":LabelInACNo"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">No A/C</label>',
				":LabelInCekNo"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Cek</label>',
				":LabelInTglCair"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Cair</label>',
				":LabelOutBank"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bank Keluar</label>',
				":LabelTerima"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Terima Dari</label>',
				":LabelOutACNo"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">No A/C</label>',
				":LabelOutCekNo"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Cek</label>',
				":LabelOutTglCair" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Cair</label>',
				":LabelKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelNominal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
				/* component */
				":NoBT"            => Html::textInput( 'BTNo', $dsTUang[ 'BTNo' ], [ 'class' => 'form-control', 'readonly' => true, 'tabindex' => '1' ] ),
				":NoBK"            => Html::textInput( 'BKNo', $dsTUang[ 'BKNo' ], [ 'class' => 'form-control', 'tabindex' => '3' ] ),
				":NoBM"            => Html::textInput( 'BMNo', $dsTUang[ 'BMNo' ], [ 'class' => 'form-control', 'tabindex' => '4' ] ),
				":JenisBT"         => Html::textInput( 'BKJenis', $dsTUang[ 'BKJenis' ], [ 'class' => 'form-control', 'tabindex' => '5' ] ),
				":JTBK"            => Html::textInput( 'BKNoGL', $dsTUang[ 'BKNoGL' ], [ 'class' => 'form-control', 'tabindex' => '6' ] ),
				":BMJT"            => Html::textInput( 'BMNoGL', $dsTUang[ 'BMNoGL' ], [ 'class' => 'form-control' , 'tabindex' => '7'] ),
				":BayarKe"         => Html::textInput( 'BKPerson', $dsTUang[ 'BKPerson' ], [ 'class' => 'form-control', 'tabindex' => '9' ] ),
				":InACNo"          => Html::textInput( 'BKAC', $dsTUang[ 'BKAC' ], [ 'class' => 'form-control' , 'tabindex' => '10'] ),
				":InCekNo"         => Html::textInput( 'BKCekNo', $dsTUang[ 'BKCekNo' ], [ 'class' => 'form-control' , 'tabindex' => '11'] ),
				":Terima"          => Html::textInput( 'BMPerson', $dsTUang[ 'BMPerson' ], [ 'class' => 'form-control', 'tabindex' => '14' ] ),
				":OutACNo"         => Html::textInput( 'BMAC', $dsTUang[ 'BMAC' ], [ 'class' => 'form-control', 'tabindex' => '15' ] ),
				":OutCekNo"        => Html::textInput( 'BMCekNo', $dsTUang[ 'BMCekNo' ], [ 'class' => 'form-control', 'tabindex' => '16' ] ),
				":Keterangan"      => Html::textInput( 'BKMemo', $dsTUang[ 'BKMemo' ], [ 'class' => 'form-control', 'tabindex' => '18' ] ),
				":Nominal"         => FormField::numberInput( [ 'name' => 'BKNominal', 'config' => [ 'value' => $dsTUang[ 'BKNominal' ] ] ] ),
				//                ":NoAccount"
				":OutBank"         => FormField::combo( 'NoAccount', [ 'name' => 'BKNoAccount', 'config' => [ 'value' => $dsTUang[ 'BKNoAccount' ], 'data' => \aunit\models\Traccount::find()->bank() ],'options' => ['tabindex' => '8'], ] ),
				":INBank"          => FormField::combo( 'NoAccount', [ 'name' => 'BMNoAccount', 'config' => [ 'value' => $dsTUang[ 'BMNoAccount' ], 'data' => \aunit\models\Traccount::find()->bank() ], 'options' => ['tabindex' => '13'], ] ),
				":TglBT"           => FormField::dateInput( [ 'name' => 'BTTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BKTgl' ] ) ] ] ),
				":InTglCair"       => FormField::dateInput( [ 'name' => 'BKCekTempo', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BKCekTempo' ] ) ] ] ),
				":OutTglCair"      => FormField::dateInput( [ 'name' => 'BMCekTempo', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BMCekTempo' ] ) ] ] ),
				":BTJam"           => FormField::timeInput( [ 'name' => 'BTJam' ] ),
				/* button */

                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),
			],
            [
                'url_main'         => 'ttbkhd',
                'url_delete'        => Url::toRoute(['ttbkhd/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlPrint = $url[ 'print' ];
$urlAdd   = \aunit\models\Ttbkhd::getRoute( 'Bank Transfer', [ 'action' => 'create' ] )[ 'create' ];
$urlIndex = \aunit\models\Ttbkhd::getRoute( 'Bank Transfer', [ 'id' => $id ] )[ 'index' ];
$url[ 'jurnal' ] = \yii\helpers\Url::toRoute(['ttbkhd/jurnal']);
$this->registerJs( <<< JS
    $('#btnPrint').click(function (event) {	      
        $('#form_ttbkhd_id').attr('action','$urlPrint');
        $('#form_ttbkhd_id').attr('target','_blank');
        $('#form_ttbkhd_id').utilForm().submit();
	  });

    $('#btnJurnal').click(function (event) {	
            bootbox.confirm({
                title: 'Konfirmasi',
                message: "Apakah ingin menjurnal ulang transaksi ini ?",
                size: "small",
                buttons: {
                    confirm: {
                        label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                        className: 'btn btn-warning'
                    },
                    cancel: {
                        label: '<span class="fa fa-ban"></span> Cancel',
                        className: 'btn btn-default'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $('#form_ttbkhd_id').attr('action','{$url[ 'jurnal' ]}');
                        $('#form_ttbkhd_id').utilForm().submit();
                    }
                }
            });
        }); 

     $('#btnCancel').click(function (event) {	      
       $('#form_ttbkhd_id').attr('action','{$url['cancel']}');
       $('#form_ttbkhd_id').utilForm().submit();
    });
     
     $('#BKTotal-disp').blur(function() {
        $('#BKNominal').utilNumberControl().val($('#BKTotal').utilNumberControl().val());
      }); 
     
      $('#btnSave').click(function (event) {
           let Nominal = parseFloat($('#BKNominal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Silahkan mengisi nominal', size: 'small'});
               return;
           }
        $('#form_ttbkhd_id').attr('action','{$url['update']}');
        $('#form_ttbkhd_id').utilForm().submit();
    }); 
       $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });    
    
    $('[name=BTTgl]').utilDateControl().cmp().attr('tabindex', '2');
    $('[name=BKTgl]').utilDateControl().cmp().attr('tabindex', '12');
    $('[name=BMTgl]').utilDateControl().cmp().attr('tabindex', '17');
    $('[name=BKNominal').utilNumberControl().cmp().attr('tabindex', '19');
    
JS
);

