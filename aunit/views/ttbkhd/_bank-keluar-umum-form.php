<?php
use aunit\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use aunit\components\Menu;
use yii\helpers\Url;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbkhd */
/* @var $form yii\widgets\ActiveForm */

$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttbkhd-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_ttbkhd_id', 'action' => $url[ 'update' ] ] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelBKNo" ],
						  [ 'class' => "col-sm-4", 'items' => ":BKNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelBKTgl" ],
						  [ 'class' => "col-sm-3", 'items' => ":BKTgl" ],
						  [ 'class' => "col-sm-2", 'items' => ":BKJam" ],
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelBKMemo" ],
						  [ 'class' => "col-sm-11", 'items' => ":BKMemo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelNominalBK" ],
						  [ 'class' => "col-sm-3", 'items' => ":NominalBK" ]
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelNoAccount" ],
						  [ 'class' => "col-sm-11", 'items' => ":NoAccount" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelBKPerson" ],
						  [ 'class' => "col-sm-8", 'items' => ":BKPerson" ]
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelBKAC" ],
						  [ 'class' => "col-sm-4", 'items' => ":BKAC" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelBKCekNo" ],
						  [ 'class' => "col-sm-5", 'style' => 'padding-left:8px', 'items' => ":BKCekNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelBKCekTempo" ],
						  [ 'class' => "col-sm-2", 'items' => ":BKCekTempo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelJenisBK" ],
						  [ 'class' => "col-sm-4", 'items' => ":JenisBK" ],
					  ],
					],
					[
						'class' => "row col-md-24",
						'style' => "margin-bottom: 6px;",
						'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
					],
				]
			],
			[
				'class' => "row-no-gutters",
				'items' => [
                    ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                        'items'  => [
                            ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":NoGL"],
                            ['class' => "col-sm-4 pull-right", 'items' => ":LabelNoGL"],
                        ],
                    ],
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'BKNo'       => 'No Bank Keluar',
                                'BKPerson'   => 'Bayar Ke',
                                'BKJenis'    => 'Jenis',
                                'SupKode'    => 'Kode Sup',
                                'NoAccount'  => 'Nomor Account',
                                'BKCekNo'    => 'Nomor Cek',
                                'BKAC'       => 'Nomor A/C',
                                'BKMemo'     => 'Keterangan',
                                'UserID'     => 'User ID',
                                'NoGL'       => 'No Gl',
                            ],
                        ])
                    ],
                    ['class' => "col-md-12 pull-right",
					  'items' => [
						  [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
					  ]
					]
				]
			],
			[
				/* label */
				":LabelBKNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No BK</label>',
				":LabelBKTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl BK</label>',
				":LabelBKMemo"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelNominalBK"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
				":LabelNoAccount"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bank</label>',
				":LabelBKPerson"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bayar Ke</label>',
				":LabelBKAC"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No A/C</label>',
				":LabelBKCekNo"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Cek</label>',
				":LabelBKCekTempo" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Cair</label>',
				":LabelJenisBK"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				":LabelNoGL"       => \common\components\General::labelGL( $dsTUang[ 'NoGL' ] ),
				/* component */
				":BKNo"            => Html::textInput( 'BKNoView', $dsTUang[ 'BKNoView' ], [ 'class' => 'form-control', 'tabindex' => '1'  ] ) .
				                      Html::textInput( 'BKNo', $dsTUang[ 'BKNo' ], [ 'class' => 'hidden' ] ),
				":BKTgl"           => FormField::dateInput( [ 'name' => 'BKTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BKTgl' ] ) ] ] ),
				":BKJam"           => FormField::timeInput( [ 'name' => 'BKJam' ] ),
				":BKCekTempo"      => FormField::dateInput( [ 'name' => 'BKCekTempo', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BKCekTempo' ] ) ] ] ),
				":BKMemo"          => Html::textInput( 'BKMemo', $dsTUang[ 'BKMemo' ], [ 'class' => 'form-control', 'tabindex' => '3' ] ),
				":NoGL"            => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NominalBK"       => FormField::numberInput( [ 'name' => 'BKNominal', 'config' => [ 'value' => $dsTUang[ 'BKNominal' ] ] ] ),
				":BKPerson"        => Html::textInput( 'BKPerson', $dsTUang[ 'BKPerson' ], [ 'class' => 'form-control' , 'tabindex' => '6'] ),
				":BKAC"            => Html::textInput( 'BKAC', $dsTUang[ 'BKAC' ], [ 'class' => 'form-control', 'tabindex' => '7' ] ),
				":BKCekNo"         => Html::textInput( 'BKCekNo', $dsTUang[ 'BKCekNo' ], [ 'class' => 'form-control', 'tabindex' => '8' ] ),
				":JenisBK"         => Html::textInput( 'BKJenis', $dsTUang[ 'BKJenis' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NoAccount"       => FormField::combo( 'NoAccount', [ 'config' => [ 'id' => 'NoAccount','value' => $dsTUang[ 'NoAccount' ] ,'data'=> \aunit\models\Traccount::find()->bank()],'options' => ['tabindex' => '5'], ] ),
				/* button */
                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),

			],
            [
                'url_main'         => 'ttbkhd',
                'url_delete'        => Url::toRoute(['ttbkhd/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php

$urlDetail = $url[ 'detail' ];
$urlPrint  = $url[ 'print' ];
$url[ 'jurnal' ] = \yii\helpers\Url::toRoute(['ttbkhd/jurnal']);
$readOnly  = ( ($_GET[ 'action' ] == 'view' || $_GET[ 'action' ] == 'display') ? 'true' : 'false' );
//$DtAccount = \common\components\General::cCmd( "SELECT NoAccount, NamaAccount, OpeningBalance, JenisAccount, StatusAccount, NoParent FROM traccount
//WHERE JenisAccount = 'Detail' ORDER BY NoAccount" )->queryAll();
$akses =  Menu::getUserLokasi()[ 'KodeAkses' ] ;
if($akses === "Admin" || $akses === "Auditor"){
    $DtAccount       = General::cCmd( "SELECT NoAccount, NamaAccount, OpeningBalance, JenisAccount, StatusAccount, NoParent FROM traccount 
 WHERE  StatusAccount = 'A' AND JenisAccount = 'Detail' AND LEFT(NoAccount,4) NOT IN ('1102','1103')  Order By StatusAccount, NoAccount" )->queryAll();
}else{
    $DtAccount       = General::cCmd( "SELECT NoAccount, NamaAccount, StatusAccount FROM traccount 
                WHERE StatusAccount = 'A' AND JenisAccount = 'Detail' AND NoAccount <> '11010100' 
                AND LEFT(NoAccount,4) NOT IN ('1102','1103','7000','8010','9010') Order By StatusAccount, NoAccount" )->queryAll();
}
$urlAdd   = \aunit\models\Ttbkhd::getRoute( $dsTUang[ 'BKJenis' ] ,['action'=>'create'])[ 'create' ];
$urlIndex = \aunit\models\Ttbkhd::getRoute( $dsTUang[ 'BKJenis' ], [ 'id' => $id ] )[ 'index' ];
$this->registerJsVar( '__dtAccount', json_encode( $DtAccount ) );
$this->registerJsVar('__update', $_GET['action']);
$this->registerJsVar( 'akses', Menu::getUserLokasi()[ 'KodeAkses' ] );
$this->registerJs( <<< JS
    var __NoAccount = JSON.parse(__dtAccount);
     __NoAccount = $.map(__NoAccount, function (obj) {
                      obj.id = obj.NoAccount;
                      obj.text = obj.NoAccount;
                      return obj;
                    });
     var __NamaAccount = JSON.parse(__dtAccount);
     __NamaAccount = $.map(__NamaAccount, function (obj) {
                      obj.id = obj.NamaAccount;
                      obj.text = obj.NamaAccount;
                      return obj;
                    });
     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 210,
        extraParams: {
            BKNo: $('input[name="BKNo"]').val()
        },
        rownumbers: true, 
        loadonce:true,
        rowNum: 1000,  
        readOnly: $readOnly,         
        colModel: [
            { template: 'actions' },
            {
                name: 'NoAccount',
                label: 'Kode',
                editable: true,
                width: 140,
                editor: {
                    type: 'select2',
                    data: __NoAccount,  
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    // console.log(data);
						    let rowid = $(this).attr('rowid');
                            $('[id="'+rowid+'_'+'NamaAccount'+'"]').val(data.NamaAccount); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'NamaAccount'+'"]').trigger('change');
						}
                    }                  
                }
            },
            {
                name: 'NamaAccount',
                label: 'Nama',
                width: 250,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __NamaAccount, 
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    // console.log(data);
						    let rowid = $(this).attr('rowid');
                            $('[id="'+rowid+'_'+'NoAccount'+'"]').val(data.NoAccount); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'NoAccount'+'"]').trigger('change');
						}
                    }                              
                }
            },
            {
                name: 'BKKeterangan',
                label: 'Keterangan',
                width: 320,
                editable: true,
            },
            {
                name: 'BKBayar',
                label: 'Debit',
                width: 200,
                editable: true,
                template: 'money'
            },
        ],    
        listeners: {
            afterLoad: function(){
                let TotalBKBayar = $('#detailGrid').jqGrid('getCol','BKBayar',false,'sum');
                $("#detailGrid-pager_right").html('<label class="control-label" style="margin-right:10px;">Sub Total COA </label>' +
                 '<label style="width:190px;margin-right:25px;">'+TotalBKBayar.toLocaleString("id-ID", {minimumFractionDigits:2})+'</label>');
            }
        }        
     }).init().fit($('.box-body')).load({url: '$urlDetail'});
     
      $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowId, orgClickEvent) {
        // console.log(orgClickEvent);
        if (orgClickEvent.extraparam.oper === 'add'){
            var BKKeterangan = $('[id="'+rowId+'_'+'BKKeterangan'+'"]');
            BKKeterangan.val($('[name="BKMemo"]').val());
        }        
    });     
     
    $('#btnPrint').click(function (event) {	      
        $('#form_ttbkhd_id').attr('action','$urlPrint');
        $('#form_ttbkhd_id').attr('target','_blank');
        $('#form_ttbkhd_id').utilForm().submit();
	  });  
    
    $('#btnJurnal').click(function (event) {	
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#form_ttbkhd_id').attr('action','{$url[ 'jurnal' ]}');
                    $('#form_ttbkhd_id').utilForm().submit();
                }
            }
        });
    }); 
    
     $('#btnCancel').click(function (event) {	      
       $('#form_ttbkhd_id').attr('action','{$url['cancel']}');
       $('#form_ttbkhd_id').utilForm().submit();
    });
     
     $('#BKTotal-disp').blur(function() {
        $('#BKNominal').utilNumberControl().val($('#BKTotal').utilNumberControl().val());
      }); 
     
     $('#btnSave').click(function (event) {	
        let TotalKMBayar = $('#detailGrid').jqGrid('getCol','BKBayar',false,'sum');
        let SubTotal = $('#BKNominal').utilNumberControl().val();
        let BMTgl = new Date($('#BMTgl').val());
        let tglNow = new Date();
        let diffTime = Math.abs(tglNow - BMTgl);
        let diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
        // console.log(TotalKMBayar + ' ' + SubTotal);
        // if (TotalKMBayar !== SubTotal){
        //     bootbox.alert({ message: "Jumlah nominal dengan subtotal coa berbeda.", size: 'small'});
        //     return;
        // }
        let param = ['Admin', 'Auditor'];
        if(!param.includes(akses)){
         if (diffDays > 3){
            bootbox.alert({ message: "Tgl Transaksi (" + BMTgl + ") lebih kecil dari Tgl Sistem (" + tglNow + ") Silakan Hhubungi Admin untuk melakukan proses Hapus atau Simpan" , size: 'small'});
            return;
         }
         $('#form_ttkk_id').attr('action','{$url['update']}');
            $('#form_ttkk_id').submit();
        }
        $('#form_ttbkhd_id').utilForm().submit('{$url['update']}','POST','_self');
    });
     
      $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  }); 
    
    $('[name=BKTgl]').utilDateControl().cmp().attr('tabindex', '2');
    $('[name=BKCekTempo]').utilDateControl().cmp().attr('tabindex', '9');
    $('[name=BKNominal').utilNumberControl().cmp().attr('tabindex', '4');
        
    //enable kode saat edit request 07/06/2024
	/*
    $( document ).ready(function() {
        if(__update == 'update'){
            $('#NoAccount').attr('disabled',true);
        }
    });
    */
    
JS
);

