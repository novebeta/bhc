<?php
use aunit\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbkhd */
/* @var $form yii\widgets\ActiveForm */

$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
$btnAttachment = '';
if($model->BKJenis == 'Bayar BBN'){
    $btnAttachment = ':btnAttachment';
}
?>
    <div class="ttbkhd-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_ttbkhd_id', 'action' => $url[ 'update' ] ] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-1", 'items' => ":LabelNoBK" ],
						  [ 'class' => "col-sm-3", 'items' => ":NoBK" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelTglBK" ],
						  [ 'class' => "col-sm-3", 'items' => ":TglBK" ],
						  [ 'class' => "col-sm-2", 'items' => ":BKJam" ],
						  [ 'class' => "col-sm-3" ],
						  [ 'class' => "col-sm-2", 'items' => ($dsTUang[ 'BKJenis' ] === 'Bayar BBN') ? "": ":LabelSupKode" ],
						  [ 'class' => "col-sm-8", 'style' => 'padding-right:10px', 'items' =>($dsTUang[ 'BKJenis' ] === 'Bayar BBN') ? "": ":SupKode" ],
					  ],
					],
					[
						'class' => "row col-md-24",
						'style' => "margin-bottom: 6px;",
						'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelBKMemo" ],
						  [ 'class' => "col-sm-10", 'items' => ":BKMemo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelBKTotal" ],
						  [ 'class' => "col-sm-2", 'items' => ":BKTotal" ],
						  [ 'class' => "col-sm-2" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelNominalBK" ],
						  [ 'class' => "col-sm-4", 'items' => ":NominalBK", 'style' => 'padding-left:10px' ]
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelNoAccount" ],
						  [ 'class' => "col-sm-10", 'items' => ":NoAccount" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelBKPerson" ],
						  [ 'class' => "col-sm-9", 'items' => ":BKPerson" ]
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelBKAC" ],
						  [ 'class' => "col-sm-4", 'items' => ":BKAC" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelBKCekNo" ],
						  [ 'class' => "col-sm-4", 'items' => ":BKCekNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelBKCekTempo" ],
						  [ 'class' => "col-sm-3", 'items' => ":BKCekTempo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelJenisBK" ],
						  [ 'class' => "col-sm-4", 'items' => ":JenisBK", 'style' => 'padding-left:10px' ]
					  ],
					],
				]
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
                  ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                      'items'  => [
                          ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":NoGL"],
                          ['class' => "col-sm-4 pull-right", 'items' => ":LabelNoGL"],
                      ],
                  ],
                  [
                      'class' => "col-md-12 pull-left",
                      'items' => $this->render( '../_nav', [
                          'url'=> $_GET['r'],
                          'options'=> [
                              'BKNo'       => 'No Bank Keluar',
                              'BKPerson'   => 'Bayar Ke',
                              'BKJenis'    => 'Jenis',
                              'SupKode'    => 'Kode Sup',
                              'NoAccount'  => 'Nomor Account',
                              'BKCekNo'    => 'Nomor Cek',
                              'BKAC'       => 'Nomor A/C',
                              'BKMemo'     => 'Keterangan',
                              'UserID'     => 'User ID',
                              'NoGL'       => 'No Gl',
                          ],
                      ])
                  ],
                  ['class' => "col-md-12 pull-right",
				    'items' => [
					    [ 'class' => "pull-right ", 'items' => $btnAttachment . " :btnJurnal :btnPrint :btnAction" ]
				    ]
				  ]
			  ]
			],
			[
				/* label */
				":LabelNoBK"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No BK</label>',
				":LabelTglBK"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl BK</label>',
				":LabelNoGL"       => \common\components\General::labelGL( $dsTUang[ 'NoGL' ] ),
				":LabelSupKode"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Supplier</label>',
				":LabelBKMemo"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelBKTotal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total</label>',
				":LabelNominalBK"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
				":LabelNoAccount"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bank</label>',
				":LabelBKPerson"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bayar Ke</label>',
				":LabelBKAC"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No A/C</label>',
				":LabelBKCekNo"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Cek</label>',
				":LabelBKCekTempo" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Cair</label>',
				":LabelJenisBK"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				/* component */
				":NoBK"            => Html::textInput( 'BKNoView', $dsTUang[ 'BKNoView' ], [ 'class' => 'form-control', 'readonly' => true, 'tabindex' => '1' ] ) .
                                      Html::textInput( 'StatPrint', 1, [ 'class' => 'hidden' ] ).
				                      Html::textInput( 'BKNo', $dsTUang[ 'BKNo' ], [ 'class' => 'hidden' ] ),
				":TglBK"           => FormField::dateInput( [ 'name' => 'BKTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BKTgl' ] ) ] ] ),
				":BKJam"           => FormField::timeInput( [ 'name' => 'BKJam' ] ),
				":BKCekTempo"      => FormField::dateInput( [ 'name' => 'BKCekTempo', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BKCekTempo' ] ) ] ] ),
				":BKMemo"          => Html::textInput( 'BKMemo', $dsTUang[ 'BKMemo' ], [ 'class' => 'form-control', 'tabindex' =>'4' ] ),
				":NoGL"            => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":BKTotal"         => FormField::numberInput( [ 'name' => 'BKTotal', 'config' => [ 'value' => $dsTUang[ 'BKTotal' ] ] ] ),
				":NominalBK"       => FormField::numberInput( [ 'name' => 'BKNominal', 'config' => [ 'value' => $dsTUang[ 'BKNominal' ] ] ] ),
				":BKPerson"        => Html::textInput( 'BKPerson', $dsTUang[ 'BKPerson' ], [ 'class' => 'form-control', 'tabindex' =>'8' ] ),
				":BKAC"            => Html::textInput( 'BKAC', $dsTUang[ 'BKAC' ], [ 'class' => 'form-control', 'tabindex' =>'9' ] ),
				":BKCekNo"         => Html::textInput( 'BKCekNo', $dsTUang[ 'BKCekNo' ], [ 'class' => 'form-control', 'tabindex' =>'10' ] ),
				":JenisBK"         => Html::textInput( 'BKJenis', $dsTUang[ 'BKJenis' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NoAccount"       => FormField::combo( 'NoAccount', [ 'config' => [ 'id' => 'NoAccount','value' => $dsTUang[ 'NoAccount' ], 'data' => \aunit\models\Traccount::find()->bank() ],'options' => ['tabindex' => '7'], ] ),
				":SupKode"         => FormField::combo( 'SupKode', [ 'config' => [ 'value' => $dsTUang[ 'SupKode' ]??'' ],'options' => ['tabindex' => '3'], 'extraOptions' => [ 'altLabel' => [ 'SupNama' ] ] ] ),
				/* button */
                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
                ":btnAttachment"       => Html::button('<i class="glyphicon glyphicon-file"><br><br>Lampiran</i>', ['class' => 'btn bg-black '.(($_GET[ 'action' ] ?? '') == 'create' ? 'hidden' : ''), 'id' => 'btnAttachment', 'style' => 'width:80px;height:60px']),
                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),
			],
            [
                'url_main'         => 'ttbkhd',
                'url_delete'        => Url::toRoute(['ttbkhd/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlDetail = $url[ 'detail' ];
$urlPrint  = $url[ 'print' ];
$urlAdd    = \aunit\models\Ttbkhd::getRoute( $dsTUang[ 'BKJenis' ], [ 'action' => 'create' ] )[ 'create' ];
$urlIndex  = \aunit\models\Ttbkhd::getRoute( $dsTUang[ 'BKJenis' ], [ 'id' => $id ] )[ 'index' ];
$readOnly  = ( ( $_GET[ 'action' ] == 'view' || $_GET[ 'action' ] == 'display' ) ? 'true' : 'false' );
$urlLampiran = Url::toRoute( [ 'ttbkhd/lampiran', 'id' => $id ] );
switch ( $dsTUang[ 'BKJenis' ] ) {
	case 'Bayar BBN':
		$urlDK = Url::toRoute( [ 'ttdk/select-bayar-bbn-bank', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ) ] );
		$DKNo  = 'FBNo';
		if ( isset( $_GET[ 'mode' ] ) ) {
		    // kwitansi cetak
			$urlIndex = Url::toRoute( [ 'ttbkhd/' . $_GET[ 'mode' ], 'id' => $id ] );
		}
		break;
	case 'BBN Plus':
		$urlDK = Url::toRoute( [ 'ttdk/select-bayar-bbn-plus', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ) ] );
		$DKNo  = 'FBNo';
		break;
	case 'SubsidiDealer2':
		$urlDK = Url::toRoute( [ 'ttdk/select-bk-order-penjualan', 'jenis' => 'SubsidiDealer2', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ) ] );
		$DKNo  = 'DKNo';
		break;
	case 'Retur Harga':
		$urlDK = Url::toRoute( [ 'ttdk/select-bk-order-penjualan', 'jenis' => 'ReturHarga', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ) ] );
		$DKNo  = 'DKNo';
		break;
	case 'Potongan Khusus':
		$urlDK = Url::toRoute( [ 'ttdk/select-bk-order-penjualan', 'jenis' => 'PotonganKhusus', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ) ] );
		$DKNo  = 'DKNo';
		break;
	case 'Insentif Sales':
		$urlDK = Url::toRoute( [ 'ttdk/select-bk-order-penjualan', 'jenis' => 'InsentifSales', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ) ] );
		$DKNo  = 'DKNo';
		break;
}
//$urlDK  = Url::toRoute(['ttdk/select-bayar-bbn-bank']);
$urlLoop         = Url::toRoute( [ 'ttbkit/index' ] );
$url[ 'jurnal' ] = \yii\helpers\Url::toRoute( [ 'ttbkhd/jurnal' ] );
$this->registerJsVar('__update', $_GET['action']);
$this->registerJs( <<< JS
     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 210,
        rowNum: 1000,
        extraParams: {
            BKNo: '$model->BKNo'
        },
        postData: {
            BKNo: '$model->BKNo'
        },
        rownumbers: true,            
        readOnly: $readOnly,  
        navButtonTambah: false,
        loadonce:true,
        colModel: [
            { template: 'actions' },
            {
                name: 'FBNo',
                label: 'No Dat Kon',
                editable: true,
                width: 90,
                editoptions:{
                    readOnly: true    
                },
            },
            {
                name: 'DKTgl',
                label: 'Tanggal DK',
                width: 80,
                editable: true,
                editoptions:{
                    readOnly: true    
                },
            },
            {
                name: 'CusNama',
                label: 'Nama Konsumen',
                width: 180,
                editable: true,
                editoptions:{
                    readOnly: true    
                },
            },
            {
                name: 'MotorType',
                label: 'Type',
                width: 150,
                editable: true,
                editoptions:{
                    readOnly: true    
                },
            },
            {
                name: 'BBN',
                label: 'BBN',
                width: 150,
                editable: true,
                template: 'money',
                editoptions:{
                    readOnly: true    
                },
            },
            {
                name: 'Terbayar',
                label: 'Terbayar',
                width: 95,
                editable: true,
                template: 'money',
                editoptions:{
                    readOnly: true    
                },
            },{
                name: 'BKBayar',
                label: 'Bayar',
                width: 95,
                editable: true,
                template: 'money',
                editrules:{
                    custom:true, 
                    custom_func: function(value) {
                        var BKBayar = parseFloat(window.util.number.getValueOfEuropeanNumber(value));
                        var BBN = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'BBN'+'"]').val()));
                        var Terbayar = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Terbayar'+'"]').val()));
                        if (BKBayar === 0) 
                            return [false,"Silahkan mengisi bayar."];  
                        else {      
                            var sisa = BBN - Terbayar;
                            if (BKBayar > sisa){
                                $('[id="'+window.cmp.rowid+'_'+'BKBayar'+'"]').val(sisa);
                               return [false,"Nilai Bayar maksimal : " + window.util.number.format(sisa)];   
                            }else{
                                return [true,""];  
                            }
                        }
                    }
                }
            },                       
            {
                name: 'Sisa',
                label: 'Sisa',
                width: 95,
                editable: true,
                template: 'money',
                editoptions:{
                    readOnly: true    
                },
            },
        ],     
         listeners: {
            afterLoad: function(grid, response) {
                let Total = $('#detailGrid').jqGrid('getCol','BKBayar',false,'sum');
                $('#BKTotal-disp').utilNumberControl().val(Total);
            },
        }  
        
     }).init().fit($('.box-body')).load({url: '$urlDetail'});

    $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
        window.cmp.rowid = rowid;    
    });
    
    $('#detailGrid').jqGrid('navButtonAdd',"#detailGrid-pager",{ 
        caption: ' Tambah',
        buttonicon: 'glyphicon glyphicon-plus',
        onClickButton:  function() {
            window.util.app.dialogListData.closeTriggerFromIframe = function(){ 
                var iframe = window.util.app.dialogListData.iframe();
                var selRowId = iframe.contentWindow.util.app.dialogListData.gridFn.getSelectedRow(true);
               // console.log(selRowId);                
                $.ajax({
                     type: "POST",
                     url: '$urlLoop',
                     data: {
                         oper: 'batch',
                         data: selRowId,
                         header: btoa($('#form_ttbkhd_id').serialize())
                     },
                     success: function(data) {
                        iframe.contentWindow.util.app.dialogListData.gridFn.mainGrid.jqGrid().trigger('reloadGrid');
                     },
                   });
                return false;
            }
            window.util.app.dialogListData.show({
                title: 'Daftar Data Konsumen Bank Keluar Nomor : {$dsTUang["BKNoView"]}',
                url: '$urlDK'
            },function(){
                 window.location.href = "{$url['update']}&oper=skip-load&BKNoView={$dsTUang['BKNoView']}";  
            });
        }, 
        position: "last", 
        title:"", 
        cursor: "pointer"
    });
    
    function submitPrint(StatPrint){
        $('[name=StatPrint]').val(StatPrint);
        $('#form_ttbkhd_id').attr('action','$urlPrint');
        $('#form_ttbkhd_id').attr('target','_blank');
        $('#form_ttbkhd_id').utilForm().submit();
    }

    $('#btnPrint').click(function (event) {	   
        var url = new URL($(location).attr('href'));
        var c = decodeURIComponent(url.searchParams.get("mode"));
        if(c === 'kwitansi-bank-bayar-bbn'){
                bootbox.dialog({
                    title: 'Input',
                    message: '<div class="form-group col-md-24">' +
                     '<div class="col-md-8 col-sm-8 col-lg-8 col-xs-8"><label class="control-label" style="margin: 0; padding: 6px 0;">Pembayaran</label></div>' +
                     '<div class="col-md-16 col-sm-16 col-lg-16 col-xs-16" style="margin-bottom: 2px;">' +
                      '<select id="Pembayaran" class="form-control">' +
                          '<option value="Full">Full</option>' +
                          '<option value="Sebagian">Sebagian</option>' +
                          '<option value="Pelunasan">Pelunasan</option>' +
                      '</select></div>' +
                     '<div class="col-md-8 col-sm-8 col-lg-8 col-xs-8"><label class="control-label" style="margin: 0; padding: 6px 0;">Penerima</label></div>' +
                     '<div class="col-md-16 col-sm-16 col-lg-16 col-xs-16" style="margin-bottom: 2px;"><input id="Penerima" class="form-control" /></div>' +
                      '</div>',
                    size: 'medium',
                    onEscape: true,
                    backdrop: true,
                    buttons: {
                        ok: {
                            label: '<i class="glyphicon glyphicon-ok"></i>&nbsp&nbsp OK &nbsp&nbsp',
                            className: 'btn-success',
                            callback: function(){ 
                               $("#form_ttbkhd_id").append("<input name='StatPrint' type='hidden' value=24 >");
                               $("#form_ttbkhd_id").append("<input name='Pembayaran' type='hidden' value='"+$("#Pembayaran").val()+"'>");
                               $("#form_ttbkhd_id").append("<input name='Penerima' type='hidden' value='"+$("#Penerima").val()+"'>");
                               $('#form_ttbkhd_id').attr('action','$urlPrint');
                                $('#form_ttbkhd_id').attr('target','_blank');
                                $('#form_ttbkhd_id').utilForm().submit();
                               location.reload(); 
                            }
                        },
                        cancel: {
                            label: '<i class="fa fa-ban"></i>&nbsp&nbsp Cancel &nbsp',
                            className: 'btn-danger',
                            callback: this.callback
                        }
                    }
                });
        }else{
            submitPrint(1);
            setTimeout(function() {submitPrint(2);},5000); 
        }
	  });
    
     $('#btnJurnal').click(function (event) {	
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#form_ttbkhd_id').attr('action','{$url['jurnal']}');
                    $('#form_ttbkhd_id').utilForm().submit();
                }
            }
        });
    }); 
    
     $('#btnCancel').click(function (event) {	      
       $('#form_ttbkhd_id').attr('action','{$url['cancel']}');
       $('#form_ttbkhd_id').utilForm().submit();
    });
     
     $('#BKTotal-disp').blur(function() {
        $('#BKNominal').utilNumberControl().val($('#BKTotal').utilNumberControl().val());
      }); 
     
      $('#btnSave').click(function (event) {
           let Nominal = parseFloat($('#BKNominal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Silahkan mengisi nominal', size: 'small'});
               return;
           }
           $('#form_ttbkhd_id').utilForm().submit('{$url['update']}','POST','_self');
    }); 
      
       $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });    
    
    $('#btnAttachment').click(function (event) {
        window.location.href = '{$urlLampiran }';
    });

     $('[name=BKTgl]').utilDateControl().cmp().attr('tabindex', '2');
     $('[name=BKCekTempo]').utilDateControl().cmp().attr('tabindex', '11');
    $('[name=BKTotal').utilNumberControl().cmp().attr('tabindex', '5');
    $('[name=BKNominal').utilNumberControl().cmp().attr('tabindex', '6');
     //enable kode saat edit request 07/06/2024
	/*
    $( document ).ready(function() {
        if(__update == 'update'){
            $('#NoAccount').attr('disabled',true);
        }
    });*/
    
    
JS
);

