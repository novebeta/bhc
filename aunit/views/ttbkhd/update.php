<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbkhd */
$params                          = '&id=' . $id . '&action=update&BKJenis=' . $model->BKJenis;
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel' . $params );
$this->title                     = str_replace( 'Menu', '', \aunit\components\TUi::$actionMode ) . " - $title : " . $dsTUang['BKNoView'];
?>
<div class="ttbkhd-update">
	<?= $this->render( isset( $templateForm ) ? $templateForm : '_form', [
		'model'   => $model,
		'dsTUang' => $dsTUang,
		'id'      => $id,
		'view'    => $view,
		'url'     => [
			'update' => Custom::url( \Yii::$app->controller->id . '/' . $view . '-update' . $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
			'cancel' => $cancel,
			'detail' => Url::toRoute( [ 'ttbkit/index', 'action' => 'create' ] ),
		]
	] ) ?>
</div>
