<?php
use aunit\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbkhd */
/* @var $form yii\widgets\ActiveForm */

$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttbkhd-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_ttbkhd_id', 'action' => $url[ 'update' ] ] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelBKNo" ],
						  [ 'class' => "col-sm-4", 'items' => ":BKNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelBKTgl" ],
						  [ 'class' => "col-sm-3", 'items' => ":BKTgl" ],
						  [ 'class' => "col-sm-2", 'items' => ":BKJam" ],
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelBKMemo" ],
						  [ 'class' => "col-sm-11", 'items' => ":BKMemo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelNominalBK" ],
						  [ 'class' => "col-sm-3", 'items' => ":NominalBK" ]
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelNoAccount" ],
						  [ 'class' => "col-sm-11", 'items' => ":NoAccount" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelBKPerson" ],
						  [ 'class' => "col-sm-8", 'items' => ":BKPerson" ]
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelBKAC" ],
						  [ 'class' => "col-sm-4", 'items' => ":BKAC" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelBKCekNo" ],
						  [ 'class' => "col-sm-5", 'style' => 'padding-left:8px', 'items' => ":BKCekNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelBKCekTempo" ],
						  [ 'class' => "col-sm-3", 'items' => ":BKCekTempo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelJenisBK" ],
						  [ 'class' => "col-sm-3", 'items' => ":JenisBK" ],
					  ],
					],
				]
			],
			[
				'class' => "row-no-gutters",
				'items' => [
                    ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                        'items'  => [
                            ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":NoGL"],
                            ['class' => "col-sm-4 pull-right", 'items' => ":LabelNoGL"],
                        ],
                    ],
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'BKNo'       => 'No Bank Keluar',
                                'BKPerson'   => 'Bayar Ke',
                                'BKJenis'    => 'Jenis',
                                'SupKode'    => 'Kode Sup',
                                'NoAccount'  => 'Nomor Account',
                                'BKCekNo'    => 'Nomor Cek',
                                'BKAC'       => 'Nomor A/C',
                                'BKMemo'     => 'Keterangan',
                                'UserID'     => 'User ID',
                                'NoGL'       => 'No Gl',
                            ],
                        ])
                    ],
                    ['class' => "col-md-12 pull-right",
					  'items' => [
						  [ 'class' => "pull-right", 'items' => ":btnJurnal  :btnPrint :btnAction" ]
					  ]
					]
				]
			],
			[
				/* label */
				":LabelBKNo"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No BK</label>',
				":LabelBKTgl"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl BK</label>',
				":LabelBKMemo"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelNominalBK"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
				":LabelNoAccount"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bank</label>',
				":LabelBKPerson"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bayar Ke</label>',
				":LabelBKAC"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No A/C</label>',
				":LabelBKCekNo"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Cek</label>',
				":LabelBKCekTempo"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Cair</label>',
				":LabelJenisBK"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				":LabelSubTotalCOA" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sub Total COA</label>',
				":LabelNoGL"        => \common\components\General::labelGL( $dsTUang[ 'NoGL' ] ),
				/* component */
				":BKNo"             => Html::textInput( 'BKNoView', $dsTUang[ 'BKNoView' ], [ 'class' => 'form-control', 'readonly' => true, 'tabindex' => '1' ] ) .
				                       Html::textInput( 'BKNo', $dsTUang[ 'BKNo' ], [ 'class' => 'hidden' ] ) .
                                       Html::textInput( 'StatPrint', 1, [ 'class' => 'hidden' ] ) .
				                       Html::hiddenInput( 'SupKode', $dsTUang[ 'SupKode' ], [ 'class' => 'hidden' ] ),
				":BKTgl"            => FormField::dateInput( [ 'name' => 'BKTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BKTgl' ] ) ] ] ),
				":BKJam"            => FormField::timeInput( [ 'name' => 'BKJam' ] ),
				":BKCekTempo"       => FormField::dateInput( [ 'name' => 'BKCekTempo', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BKCekTempo' ] ) ] ] ),
				":BKMemo"           => Html::textInput( 'BKMemo', $dsTUang[ 'BKMemo' ], [ 'class' => 'form-control', 'tabindex' => '3' ] ),
				":NoGL"             => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":BKTotal"          => FormField::numberInput( [ 'name' => 'BKTotal', 'config' => [ 'value' => $dsTUang[ 'BKTotal' ] ] ] ),
				":NominalBK"        => FormField::numberInput( [ 'name' => 'BKNominal', 'config' => [ 'value' => $dsTUang[ 'BKNominal' ] ] ] ),
				":BKPerson"         => Html::textInput( 'BKPerson', $dsTUang[ 'BKPerson' ], [ 'class' => 'form-control', 'tabindex' => '6'  ] ),
				":BKAC"             => Html::textInput( 'BKAC', $dsTUang[ 'BKAC' ], [ 'class' => 'form-control', 'tabindex' => '7'  ] ),
				":BKCekNo"          => Html::textInput( 'BKCekNo', $dsTUang[ 'BKCekNo' ], [ 'class' => 'form-control', 'tabindex' => '8'  ] ),
				":JenisBK"          => Html::textInput( 'BKJenis', $dsTUang[ 'BKJenis' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NoAccount"        => FormField::combo( 'NoAccount', [ 'config' => [ 'id' => 'NoAccount','value' => $dsTUang[ 'NoAccount' ], 'data' => \aunit\models\Traccount::find()->bank() ] ,'options' => ['tabindex' => '5'],] ),
				/* button */
                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),
			],
            [
                'url_main'         => 'ttbkhd',
                'url_delete'        => Url::toRoute(['ttbkhd/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlPrint = $url[ 'print' ];
$urlAdd   = \aunit\models\Ttbkhd::getRoute( $dsTUang[ 'BKJenis' ], [ 'action' => 'create' ] )[ 'create' ];
$urlIndex = \aunit\models\Ttbkhd::getRoute( $dsTUang[ 'BKJenis' ], [ 'id' => $id ] )[ 'index' ];
$url[ 'jurnal' ] = \yii\helpers\Url::toRoute(['ttbkhd/jurnal']);
$this->registerJsVar('__update', $_GET['action']);
$this->registerJs( <<< JS
    
    function submitPrint(StatPrint){
        $('[name=StatPrint]').val(StatPrint);
        $('#form_ttbkhd_id').attr('action','$urlPrint');
        $('#form_ttbkhd_id').attr('target','_blank');
        $('#form_ttbkhd_id').utilForm().submit();
    }
    
    $('#btnPrint').click(function (event) {	      
        submitPrint(1);
        setTimeout(function() {submitPrint(2);},5000);
	  });  
    
     $('#btnJurnal').click(function (event) {	
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#form_ttbkhd_id').attr('action','{$url[ 'jurnal' ]}');
                    $('#form_ttbkhd_id').utilForm().submit();                     
                }
            }
        });
    }); 

     $('#btnCancel').click(function (event) {	      
       $('#form_ttbkhd_id').attr('action','{$url['cancel']}');
       $('#form_ttbkhd_id').utilForm().submit();
    });
     
      $('#BKTotal-disp').blur(function() {
        $('#BKNominal').utilNumberControl().val($('#BKTotal').utilNumberControl().val());
      });     
     
      $('#btnSave').click(function (event) {
           let Nominal = parseFloat($('#BKNominal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Silahkan mengisi nominal', size: 'small'});
               return;
           }
		$('#form_ttbkhd_id').utilForm().submit('{$url['update']}','POST','_self');
    }); 
      
       $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });    
    
    $('[name=BKTgl]').utilDateControl().cmp().attr('tabindex', '2');
    $('[name=BKNominal').utilNumberControl().cmp().attr('tabindex', '4');
    $('[name=BKCekTempo').utilNumberControl().cmp().attr('tabindex', '9');
    //enable kode saat edit request 07/06/2024
	/*
    $( document ).ready(function() {
        if(__update == 'update'){
            $('#NoAccount').attr('disabled',true);
        }
    });*/
     
JS
);


