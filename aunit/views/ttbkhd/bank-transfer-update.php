<?php
use common\components\Custom;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbkhd */
$params                          = '&id=' . $id . '&action=UpdateEdit';
$cancel                          = Custom::url( 'ttbkhd/bank-transfer-cancel'.$params );
$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode)." - $title : "  . $dsTUang['BTNo'];
//$this->params[ 'breadcrumbs' ][] = [ 'label' => $title, 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = 'Edit';
?>
<div class="ttbkhd-update">
    <?= $this->render('_bank-transfer-form', [
        'dsTUang' => $dsTUang,
        'id'     => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/bank-transfer-update'. $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail'       => Url::toRoute( [ 'ttbkit/index','action' => 'create' ] ),
        ]
    ]) ?>

</div>
