<?php
use aunit\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = 'Bank Transfer';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'BKNo'      => 'No Bank Keluar',
		'BKPerson'  => 'Bayar Ke',
		'BKJenis'   => 'Jenis',
		'SupKode'   => 'Kode Sup',
		'NoAccount' => 'Nomor Account',
		'BKCekNo'   => 'Nomor Cek',
		'BKAC'      => 'Nomor A/C',
		'BKMemo'    => 'Keterangan',
		'UserID'    => 'User ID',
		'NoGL'      => 'No Gl', ],
	'cmbTgl'    => [
		'BKTgl' => 'Tgl Bank Keluar',
	],
	'cmbNum'    => [
		'BKNominal' => 'Nominal',
	],
	'sortname'  => "BKTgl",
	'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttbkhd::className(),
	'Bank Transfer', [
	    'colGrid'=> \aunit\models\Ttbkhd::colGridBankTransfer(),
		'url_add'    => Url::toRoute( [ 'ttbkhd/bank-transfer-create', 'action' => 'UpdateAdd' ] ),
		'url_update' => Url::toRoute( [ 'ttbkhd/bank-transfer-update', 'action' => 'update' ] )
	]
), \yii\web\View::POS_READY );
