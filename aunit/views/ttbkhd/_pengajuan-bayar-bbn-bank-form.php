<?php
use aunit\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttbkhd */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
$queryAcc = Yii::$app->db->createCommand( "SELECT NoAccount, NamaAccount FROM traccount WHERE (NoParent = '11020100' OR NoParent = '11020200' OR  NoParent = '11030000' )  AND StatusAccount = 'A' 
UNION SELECT NoAccount, NamaAccount FROM traccount WHERE (NoParent = '11020300')  AND StatusAccount = 'A' " )->queryAll();
$dtAcc    = ArrayHelper::map( $queryAcc, 'NoAccount', 'NamaAccount' );
?>
    <div class="ttbkhd-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_ttbkhd_id', 'action' => $url[ 'update' ] ] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-1", 'items' => ":LabelBKNo" ],
						  [ 'class' => "col-sm-3", 'items' => ":BKNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelBKTgl" ],
						  [ 'class' => "col-sm-3", 'items' => ":BKTgl" ],
						  [ 'class' => "col-sm-2", 'items' => ":BKJam" ],
						  [ 'class' => "col-sm-2" ],
						  [ 'class' => "col-sm-9", 'style' => 'padding-right:10px', 'items' => ":SupKode" ],
					  ],
					],
					[
						'class' => "row col-md-24",
						'style' => "margin-bottom: 6px;",
						'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelBKMemo" ],
						  [ 'class' => "col-sm-10", 'items' => ":BKMemo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelBKTotal" ],
						  [ 'class' => "col-sm-3", 'items' => ":BKTotal" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelNominalBK" ],
						  [ 'class' => "col-sm-3", 'items' => ":NominalBK" ]
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelNoAccount" ],
						  [ 'class' => "col-sm-10", 'items' => ":NoAccount" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelBKPerson" ],
						  [ 'class' => "col-sm-9", 'items' => ":BKPerson" ]
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelBKAC" ],
						  [ 'class' => "col-sm-4", 'items' => ":BKAC" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelBKCekNo" ],
						  [ 'class' => "col-sm-4", 'style' => 'padding-left:8px', 'items' => ":BKCekNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelBKCekTempo" ],
						  [ 'class' => "col-sm-3", 'items' => ":BKCekTempo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelJenisBK" ],
						  [ 'class' => "col-sm-4", 'items' => ":JenisBK" ],
					  ],
					],
				]
			],
			[
				'class' => "row-no-gutters",
				'items' => [
                    ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                        'items'  => [
                            ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":NoGL"],
                            ['class' => "col-sm-4 pull-right", 'items' => ":LabelNoGL"],
                        ],
                    ],
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'BKNo'      => 'No Bank Keluar',
                                'BKPerson'  => 'Bayar Ke',
                                'BKJenis'   => 'Jenis',
                                'SupKode'   => 'Kode Sup',
                                'NoAccount' => 'Nomor Account',
                                'BKCekNo'   => 'Nomor Cek',
                                'BKAC'      => 'Nomor A/C',
                                'BKMemo'    => 'Keterangan',
                                'UserID'    => 'User ID',
                                'NoGL'      => 'No Gl',
                            ],
                        ])
                    ],
                    ['class' => "col-md-12 pull-right",
					  'items' => [
						  [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
					  ]
					]
				]
			],
			[
				/* label */
				":LabelBKNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No BK</label>',
				":LabelBKTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl BK</label>',
				":LabelBKMemo"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelNominalBK"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
				":LabelNoAccount"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bank</label>',
				":LabelBKPerson"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bayar Ke</label>',
				":LabelBKAC"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No A/C</label>',
				":LabelBKCekNo"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Cek</label>',
				":LabelBKCekTempo" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Cair</label>',
				":LabelJenisBK"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				":LabelNoGL"       => \common\components\General::labelGL( $dsTUang[ 'NoGL' ] ),
				":LabelSupKode"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Supplier</label>',
				":LabelBKTotal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total</label>',
				/* component */
				":BKNo"            => Html::textInput( 'BKNoView', $dsTUang[ 'BKNoView' ], [ 'class' => 'form-control', 'readonly' => true, 'tabindex' => '1' ] ) .
				                      Html::textInput( 'BKNo', $dsTUang[ 'BKNo' ], [ 'class' => 'hidden' ] ),
				":BKTgl"           => FormField::dateInput( [ 'name' => 'BKTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BKTgl' ] ) ] ] ),
				":BKJam"           => FormField::timeInput( [ 'name' => 'BKJam' ] ),
				":BKCekTempo"      => FormField::dateInput( [ 'name' => 'BKCekTempo', 'config' => [ 'value' => General::asDate( $dsTUang[ 'BKCekTempo' ] ) ] ] ),
				":BKMemo"          => Html::textInput( 'BKMemo', $dsTUang[ 'BKMemo' ], [ 'class' => 'form-control', 'tabindex' => '3' ] ),
				":NoGL"            => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NominalBK"       => FormField::numberInput( [ 'name' => 'BKNominal', 'config' => [ 'value' => $dsTUang[ 'BKNominal' ], 'disabled'=>true ] ] ),
				":BKPerson"        => Html::textInput( 'BKPerson', $dsTUang[ 'BKPerson' ], [ 'class' => 'form-control', 'tabindex' => '6' ] ),
				":BKAC"            => Html::textInput( 'BKAC', $dsTUang[ 'BKAC' ], [ 'class' => 'form-control' , 'tabindex' => '7'] ),
				":BKCekNo"         => Html::textInput( 'BKCekNo', $dsTUang[ 'BKCekNo' ], [ 'class' => 'form-control', 'tabindex' => '8' ] ),
				":JenisBK"         => Html::textInput( 'BKJenis', $dsTUang[ 'BKJenis' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NoAccount"       => FormField::combo( 'NoAccount', [ 'config' => [ 'value' => $dsTUang[ 'NoAccount' ], 'data' => \aunit\models\Traccount::find()->bank() ], 'options' => ['tabindex'=>'5'], ] ),
				":SupKode"         => FormField::combo( 'SupKode', [ 'config' => [ 'id' => 'SupKode', 'value' => '' ], 'extraOptions' => [ 'altLabel' => [ 'SupNama' ] ] ] ),
				":BKTotal"         => FormField::numberInput( [ 'name' => 'BKTotal', 'config' => [ 'value' => $dsTUang[ 'BKTotal' ] ] ] ),
				/* button */
                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Konfirm</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),
            ],
            [
                '_akses' => 'Pengajuan Bayar BBN Via Bank',
                'url_main' => 'ttbkhd',
                'url_extra' => ['JenisBBN'=>'Pengajuan'],
                'url_id' => $_GET['id'] ?? '',
                'url_delete'        => Url::toRoute(['ttbkhd/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlDetail        = $url[ 'detail' ];
$urlPrint         = $url[ 'print' ];
$url[ 'konfirm' ] = Url::toRoute( [ 'ttbkhd/konfirm-pengajuan', 'id' => $id ] );
$readOnly         = ( ($_GET[ 'action' ] == 'view' || $_GET[ 'action' ] == 'display') ? 'true' : 'false' );
$urlDK            = Url::toRoute( [ 'ttdk/select-pengajuan-bayar-bbn-bank', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ) ] );
$urlLoop          = Url::toRoute( [ 'ttbkit/index' ] );
$urlAdd           = \aunit\models\Ttbkhd::getRoute( $dsTUang[ 'BKJenis' ], [ 'action' => 'create' ] )[ 'create' ];
$urlIndex = Url::toRoute( [ 'ttbkhd/pengajuan-bayar-bbn-bank', 'id' => $id ] );
$url[ 'jurnal' ] = \yii\helpers\Url::toRoute(['ttbkhd/jurnal','JenisBBN'=>'Pengajuan']);
$this->registerJs( <<< JS
     $('#SupKode').next(".select2-container").hide();
     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 260,
        extraParams: {
            BKNo: $('input[name="BKNo"]').val()
        },
        postData: {
            BKNo: $('input[name="BKNo"]').val()
        },
        rownumbers: true,        
        loadonce:true,
        rowNum: 1000,  
        readOnly: $readOnly,  
        navButtonTambah: false,
        colModel: [
            { template: 'actions' },
            {
                name: 'FBNo',
                label: 'No Dat Kon',
                width: 100,
                editable: true,
                editoptions:{
                    readOnly: true    
                },
            },
            {
                name: 'DKTgl',
                label: 'Tanggal DK',
                editable: true,
                width: 100,
                editoptions:{
                    readOnly: true    
                },
            },
            {
                name: 'CusNama',
                label: 'Nama Konsumen',
                width: 210,
                editable: true,
                editoptions:{
                    readOnly: true    
                },
            },
            {
                name: 'MotorType',
                label: 'Type',
                width: 120,
                editable: true,
                editoptions:{
                    readOnly: true    
                },
            },
            {
                name: 'BBN',
                label: 'BBN',
                width: 100,
                editable: true,
                template: 'money',
                editoptions:{
                    readOnly: true    
                },
            },
            {
                name: 'Terbayar',
                label: 'Terbayar',
                width: 100,
                editable: true,
                template: 'money',
                editoptions:{
                    readOnly: true    
                },
            },
            {
                name: 'BKBayar',
                label: 'Bayar',
                width: 100,
                editable: true,
                template: 'money',
                editrules:{
                    custom:true, 
                    custom_func: function(value) {
                        var BKBayar = parseFloat(window.util.number.getValueOfEuropeanNumber(value));
                        var BBN = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'BBN'+'"]').val()));
                        var Terbayar = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Terbayar'+'"]').val()));
                        if (BKBayar === 0) 
                            return [false,"Silahkan mengisi bayar."];  
                        else {      
                            var sisa = BBN - Terbayar;
                            if (BKBayar > sisa){
                                $('[id="'+window.cmp.rowid+'_'+'BKBayar'+'"]').val(sisa);
                               return [false,"Nilai Bayar maksimal : " + window.util.number.format(sisa)];   
                            }else{
                                return [true,""];  
                            }
                        }
                    }
                }
            },
            {
                name: 'Sisa',
                label: 'Sisa',
                width: 100,
                editable: true,
                template: 'money',
                editoptions:{
                    readOnly: true    
                },
            },
        ],    
        listeners: {
            afterLoad: function(grid, response) {
                let Total = $('#detailGrid').jqGrid('getCol','BKBayar',false,'sum');
                $('#BKTotal-disp').utilNumberControl().val(Total);
            },
        }          
     }).init().fit($('.box-body')).load({url: '$urlDetail'});
     
     $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
        window.cmp.rowid = rowid;    
    });
     
     $('#detailGrid').jqGrid('navButtonAdd',"#detailGrid-pager",{ 
        caption: ' Tambah',
        buttonicon: 'glyphicon glyphicon-plus',
        onClickButton:  function() {
            window.util.app.dialogListData.closeTriggerFromIframe = function(){ 
                var iframe = window.util.app.dialogListData.iframe();
                var selRowId = iframe.contentWindow.util.app.dialogListData.gridFn.getSelectedRow(true);
                $.ajax({
                     type: "POST",
                     url: '$urlLoop',
                     data: {
                         oper: 'batchPengajuan',
                         data: selRowId,
                         header: btoa($('#form_ttbkhd_id').serialize())
                     },
                     success: function(data) {
                        iframe.contentWindow.util.app.dialogListData.gridFn.mainGrid.jqGrid().trigger('reloadGrid');
                     },
                   });
                return false;
            }
            window.util.app.dialogListData.show({
                title: 'Daftar Data Konsumen untuk Pengajuan Bayar BBN Nomor : {$dsTUang["BKNoView"]}',
                url: '$urlDK'
            },function(){ $('#detailGrid').utilJqGrid().load(); });
        }, 
        position: "last", 
        title:"", 
        cursor: "pointer"
    });
     
      $('#btnJurnal').click(function (event) {	
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#form_ttbkhd_id').attr('action','{$url[ 'jurnal' ]}');
                    $('#form_ttbkhd_id').utilForm().submit();
                }
            }
        });
    }); 
     
     $('#btnPrint').click(function (event) {	      
        $('#form_ttbkhd_id').utilForm().submit("{$urlPrint}","POST","_blank");
	  }); 
     
     $('#btnCancel').click(function (event) {	      
       $('#form_ttbkhd_id').attr('action','{$url['cancel']}');
       $('#form_ttbkhd_id').utilForm().submit();
    });
     
     $('#BKTotal-disp').blur(function() {
        $('#BKNominal').utilNumberControl().val($('#BKTotal').utilNumberControl().val());
      }); 
     
      $('#btnSave').click(function (event) {
        
        $('#form_ttbkhd_id').attr('action','{$url['update']}');
        $('#form_ttbkhd_id').utilForm().submit();
    }); 
     
       $('#btnAdd').click(function (event) {	      
        $('#form_ttbkhd_id').attr('action','{$url['konfirm']}');
       $('#form_ttbkhd_id').utilForm().submit();
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });    
    
    $('[name=BKTgl]').utilDateControl().cmp().attr('tabindex', '2');
    $('[name=BKTotal]').utilNumberControl().cmp().attr('tabindex', '4');
    $('[name=BKCekTempo]').utilDateControl().cmp().attr('tabindex', '9');
    
    
JS
);

