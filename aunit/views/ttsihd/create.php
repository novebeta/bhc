<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttsihd */

$params                          = '&id=' . $id . '&action=create&jenis='.$model->KodeTrans;
$cancel                          = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = "Tambah - $title";
$this->params[ 'breadcrumbs' ][] = [ 'label' => $title, 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = $this->title;

?>
<div class="ttsihd-create">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTJual' => $dsTJual,
        'id'     => $id,
        'jenis' => $jenis,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . "/$view-update" . $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
			'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttsiit/index','action' => 'create', 'jenis' => $jenis ] ),
		]
    ]) ?>

</div>
