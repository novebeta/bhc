<?php
use aunit\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
$url[ 'cancel' ] = '';
$format          = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape          = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <table id="detailGrid"></table>
            <div id="detailGridPager"></div>
        </div>
    </div>
<?php
$urlDetail = Url::toRoute( [ 'ttsihd/status-otomatis-item', 'SINo' => $data[ 'SINo' ] ] );
$this->registerJs( <<< JS
     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 280,
        rownumbers: true,  
        loadonce:true,
        navButtonTambah: false,
        rowNum: 1000,
        colModel: [
            {
                name: 'KBMNo',
                label: 'No Trans',
                editable: true,
                width: 90,
            },            
             {
                name: 'KBMTgl',
                label: 'Tgl Trans',
                width: 120,
                editable: true,
                formatter : 'date', 
                formatoptions : {
                    'srcformat' : "ISO8601Long",
                    'newformat' : 'd/m/Y H:i:s'
                }
            },
            {
                name: 'Kode',
                label: 'Kode',
                width: 90,
                editable: true,
            },
            {
                name: 'KBMBayar',
                label: 'Nominal',
                width: 90,
                editable: true,
            },
            {
                name: 'Person',
                label: 'Person',
                width: 90,
                editable: true,
            },    
            {
                name: 'Memo',
                label: 'Memo',
                width: 340,
                editable: true,
            },             
        ], 
        listeners: {
            afterLoad: function (data) {
             
            }    
        },  
     }).init().fit($('.box-body')).load({url: '$urlDetail'});        
JS
);