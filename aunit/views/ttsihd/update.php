<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttsihd */
$params                          = '&id=' . $id . '&action=update&jenis='.$dsTJual['SINoView'];
$cancel                          = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = "Edit - $title : " . $dsTJual['SINoView'];
$this->params[ 'breadcrumbs' ][] = [ 'label' => $title, 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = 'Edit';
?>
<div class="ttsihd-update">
	<?= $this->render( '_form', [
		'model'   => $model,
		'dsTJual' => $dsTJual,
		'id'      => $id,
        'jenis' => $jenis,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . "/$view-update" . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttsiit/index','action' => 'create', 'jenis' => $jenis ] ),
        ]
	] ) ?>
</div>
