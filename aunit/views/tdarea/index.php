<?php
/* @var $this yii\web\View */
use aunit\assets\AppAsset;
AppAsset::register( $this );
$this->title                   = 'Area';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
        'Kelurahan'      => 'Kelurahan',
        'KodePos'        => 'Kode Pos',
        'Kecamatan'      => 'Kecamatan',
        'Kabupaten'      => 'Kabupaten',
		'Provinsi'       => 'Provinsi',
        'KabupatenAstra' => 'Kabupaten Astra',
        'ProvinsiAstra'  => 'Provinsi Astra',
		'AreaStatus'     => 'Area Status',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
	'sortname'  => "AreaStatus asc,Provinsi asc,Kabupaten",
	'sortorder' => 'asc'
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Tdarea::class,'Area' ),
    \yii\web\View::POS_READY );