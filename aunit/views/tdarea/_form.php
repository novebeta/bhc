<?php
use common\components\Custom;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tdarea */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="tdarea-form">
	<?php
	$form = ActiveForm::begin( [ 'id' => 'frm_tdarea_id' ] );
	\aunit\components\TUi::form(
		[ 'class' => "row-no-gutters",
		  'items' => [
			  [ 'class' => "form-group col-md-24",
			    'items' => [
				    [ 'class' => "col-sm-3", 'items' => ":LabelProvinsi" ],
				    [ 'class' => "col-sm-8", 'items' => ":Provinsi" ],
				    [ 'class' => "col-sm-1" ],
				    [ 'class' => "col-sm-3", 'items' => ":LabelKabupaten" ],
				    [ 'class' => "col-sm-8", 'items' => ":Kabupaten" ],
			    ],
			  ],
			  [ 'class' => "form-group col-md-24",
			    'items' => [
				    [ 'class' => "col-sm-3", 'items' => ":LabelKecamatan" ],
				    [ 'class' => "col-sm-8", 'items' => ":Kecamatan" ],
				    [ 'class' => "col-sm-1" ],
				    [ 'class' => "col-sm-3", 'items' => ":LabelKelurahan" ],
				    [ 'class' => "col-sm-8", 'items' => ":Kelurahan" ],
			    ],
			  ],
			  [ 'class' => "form-group col-md-24",
			    'items' => [
				    [ 'class' => "col-sm-3", 'items' => ":LabelKodePos" ],
				    [ 'class' => "col-sm-8", 'items' => ":KodePos" ],
				    [ 'class' => "col-sm-1" ],
				    [ 'class' => "col-sm-3", 'items' => ":LabelAreaStatus" ],
				    [ 'class' => "col-sm-8", 'items' => ":AreaStatus" ],
			    ],
			  ],
			  [ 'class' => "form-group col-md-24",
			    'items' => [
				    [ 'class' => "col-sm-3", 'items' => ":LabelProvAstra" ],
				    [ 'class' => "col-sm-8", 'items' => ":ProvAstra" ],
				    [ 'class' => "col-sm-1" ],
				    [ 'class' => "col-sm-3", 'items' => ":LabelKabAstra" ],
				    [ 'class' => "col-sm-8", 'items' => ":KabAstra" ],
			    ],
			  ],
              [ 'class' => "form-group col-md-24",
                  'items' => [
                      [ 'class' => "col-sm-3", 'items' => ":LabelKecAstra" ],
                      [ 'class' => "col-sm-8", 'items' => ":KecAstra" ],
                      [ 'class' => "col-sm-1" ],
                      [ 'class' => "col-sm-3", 'items' => ":LabelKelAstra" ],
                      [ 'class' => "col-sm-8", 'items' => ":KelAstra" ],
                  ],
              ],
		  ]
		],
        ['class' => "row-no-gutters",
            'items'  => [
                [
                    'class' => "col-md-12 pull-left",
                    'items' => $this->render( '../_nav', [
                        'url'=> $_GET['r'],
                        'options'=> [
                            'tdarea.Kelurahan'      => 'Kelurahan',
                            'tdarea.KodePos'        => 'Kode Pos',
                            'tdarea.Kecamatan'      => 'Kecamatan',
                            'tdarea.Kabupaten'      => 'Kabupaten',
                            'tdarea.Provinsi'       => 'Provinsi',
                            'tdarea.KabupatenAstra' => 'Kabupaten Astra',
                            'tdarea.ProvinsiAstra'  => 'Provinsi Astra',
                            'tdarea.AreaStatus'     => 'Area Status',
                        ],
                    ])
                ],
                ['class' => "col-md-12 pull-right",
                    'items'  => [
                        ['class' => "pull-right", 'items' => ":btnAction"],
                    ],
                ],
            ],
        ],
		[
			":LabelProvinsi"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Provinsi</label>',
			":LabelKabupaten"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kabupaten</label>',
			":LabelKecamatan"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kecamatan</label>',
			":LabelKelurahan"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kelurahan</label>',
			":LabelKodePos"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode POS</label>',
			":LabelAreaStatus" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Area Status</label>',
			":LabelProvAstra"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Provinsi Astra</label>',
			":LabelKabAstra"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kabupaten Astra</label>',
            ":LabelKecAstra"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kecamatan Astra</label>',
            ":LabelKelAstra"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kelurahan Astra</label>',
			":Provinsi"        => $form->field( $model, 'Provinsi', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true, ] ),
			":Kabupaten"       => $form->field( $model, 'Kabupaten', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true, ] ),
			":Kecamatan"       => $form->field( $model, 'Kecamatan', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true, ] ),
			":Kelurahan"       => $form->field( $model, 'Kelurahan', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true, ] ),
			":KodePos"         => $form->field( $model, 'KodePos', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true, ] ),
			":AreaStatus"      => $form->field( $model, 'AreaStatus', [ 'template' => '{input}' ] )
			                           ->dropDownList( [
				                           'A' => 'Aktif',
				                           'N' => 'Non Aktif',
				                           '1' => 'Kabupaten Dealer',
			                           ], [ 'maxlength' => true ] ),
			":ProvAstra"       => $form->field( $model, 'ProvinsiAstra', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true, ] ),
			":KabAstra"        => $form->field( $model, 'KabupatenAstra', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true, ] ),
            ":KecAstra"       => $form->field( $model, 'KecamatanAstra', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true, ] ),
            ":KelAstra"        => $form->field( $model, 'KelurahanAstra', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true, ] ),

            ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
            ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
            ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
            ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
            ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),

		],
        [
            '_akses' => 'Area',
            'url_main' => 'tdarea',
            'url_id' => $_GET['id'] ?? '',
        ]
	);
	ActiveForm::end();
	$urlAdd   = Url::toRoute( [ 'tdarea/create','id' => 'new','action'=>'create']);
	$urlIndex   = Url::toRoute( [ 'tdarea/index' ] );
	$this->registerJs( <<< JS
    
    $('#btnAdd').click(function (event) {	      
        window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });  

     $('#btnCancel').click(function (event) {	  
         window.location.href = '{$url['cancel']}';
    });

     $('#btnSave').click(function (event) {	      
//       $('#frm_tdarea_id').attr('action','{$url['update']}');
       $('#frm_tdarea_id').submit();
    });
     
     
     
JS
	);
	?>
</div>
