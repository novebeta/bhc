<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tdarea */
$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode).'Area: ' . $model->Provinsi;
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Area', 'url' => [ 'index' ] ];
$this->params[ 'breadcrumbs' ][] = [ 'label' => $model->Provinsi, 'url' => [ 'view', 'Provinsi' => $model->Provinsi, 'Kabupaten' => $model->Kabupaten, 'Kecamatan' => $model->Kecamatan, 'Kelurahan' => $model->Kelurahan, 'KodePos' => $model->KodePos ] ];
$this->params[ 'breadcrumbs' ][] = 'Edit';
//$params                          = '&id=' . $id . '&action=save';
$params                          = '&id=' . $id;
?>
<div class="tdarea-update">
	<?= $this->render( '_form', [
		'model' => $model,
		'id'    => $id,
		'url'   => [
			'update'   => Url::toRoute( [ 'tdarea/update', 'id' => $id ] ),
			'cancel' => Url::toRoute( [ 'tdarea/cancel', 'id' => $id,'action' => 'update' ] )
		]
	] ) ?>
</div>
