<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttpshd */

$params                          = '&id=' . $id . '&action=create';
//$cancel                          = Custom::url( \Yii::$app->controller->id . '/index' );
$cancel                          = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = 'Tambah - Penerimaan Barang Dari Supplier';
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Penerimaan Barang Dari Supplier', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="ttpshd-create">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTBeli' => $dsTBeli,
        'id'     => $id,
        'url'    => [
			'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
			'cancel' => $cancel,
            'detail'       => Url::toRoute( [ 'ttpsit/index','action' => 'create' ] ),
		]
    ]) ?>

</div>
