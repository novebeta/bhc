<?php
use aunit\components\FormField;
use aunit\models\Tdlokasi;
use common\components\Custom;
use kartik\datecontrol\DateControl;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttss */
/* @var $form yii\widgets\ActiveForm */

$DataLokasi = ArrayHelper::map( Tdlokasi::find()->select2( 'LokasiKode', [ 'LokasiNama' ], "LokasiStatus,  LokasiKode",
	          [ "condition" => "LokasiStatus <> 'N'", 'params' => [] ], false ), "value", 'label' );
$format     = <<< SCRIPT
                function formatttsd(result) {
                    return '<div class="row" style="margin-left: 2px">' +
                           '<div class="col-xs-10" style="text-align: left">' + result.id + '</div>' +
                           '<div class="col-xs-14">' + result.text + '</div>' +
                           '</div>';
                }
                function matchCustom(params, data) {
                    if ($.trim(params.term) === '') {
                      return data;
                    }
                    if (typeof data.text === 'undefined') {
                      return null;
                    }
                    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
                      var modifiedData = $.extend({}, data, true);
                      return modifiedData;
                    }
                    return null;
                }
                SCRIPT;
$escape     = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttpd-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_ttsmhd_id', 'action' => $url[ 'update' ] ] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelSMNo" ],
						  [ 'class' => "col-sm-5", 'items' => ":SMNo" ],
						  [ 'class' => "col-sm-2", 'items' => ":SMTgl" ],
						  [ 'class' => "col-sm-3", 'items' => ":SMJam" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelLokasiAsal" ],
						  [ 'class' => "col-sm-9", 'items' => ":LokasiAsal" ],
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelPP" ],
						  [ 'class' => "col-sm-5", 'items' => ":PPNo" ],
						  [ 'class' => "col-sm-6" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelLokasiTujuan" ],
						  [ 'class' => "col-sm-9", 'items' => ":LokasiTujuan" ],
					  ],
					],
					[ 'class' => "row col-md-24", 'style' => "margin-bottom: 3px;", 'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>' ],
					[ 'class' => "col-md-24",
					  'items' => [
						  [ 'class' => "form-group col-md-2", 'items' => ":LabelSMMemo" ],
						  [ 'class' => "form-group col-md-11", 'items' => ":SMMemo" ],
						  [ 'class' => "form-group col-md-1" ],
						  [ 'class' => "form-group col-md-2", 'items' => ":LabelJmlUnit" ],
						  [ 'class' => "form-group col-md-2", 'items' => ":JmlUnit" ],
						  [ 'class' => "form-group col-md-1" ],
					  ]
					],
				],
			],
            [ 'class' => "row-no-gutters",
                'items' => [
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'].'?'.$_GET['jenis'],
                            'options'=> [
                                'SMNo'         => 'No Mutasi [SM/MI]',
                                'LokasiAsal'   => 'Lokasi Asal',
                                'LokasiTujuan' => 'Lokasi Tujuan',
                                'SMMemo'       => 'Keterangan',
                                'UserID'       => 'User ID',
                            ],
                        ])
                    ],
			        ['class' => "pull-right", 'items' => ":btnPrint :btnAction"],
                ],
            ],
			[
				":LabelSMNo"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Surat Jalan</label>',
				":LabelLokasiAsal"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Asal</label>',
				":LabelPP"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">No PP</label>',
				":LabelLokasiTujuan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tujuan</label>',
				":LabelSMMemo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelJmlUnit"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jumlah Unit</label>',
				":LabelSMTotal"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total</label>',
				":SMNo"              => Html::textInput( 'SMNoView', $dsBeli[ 'SMNoView' ], [ 'class' => 'form-control', 'readOnly' => true, 'tabindex' => '1' ] ) .
				                        Html::textInput( 'SMNo', $dsBeli[ 'SMNo' ], [ 'class' => 'hidden' ] ),
				":PPNo"              => Html::textInput( 'PBNo', $dsBeli[ 'PBNo' ], [ 'class' => 'form-control', 'readOnly' => true ] ),
				":SMTgl"             => FormField::dateInput( [ 'name' => 'SMTgl', 'config' => [ 'value' => $dsBeli[ 'SMTgl' ] ] ] ),
				":SMJam"             => FormField::timeInput( [ 'name' => 'SMJam', 'config' => [ 'value' => $dsBeli[ 'SMJam' ] ] ] ),
				":LokasiAsal"        => FormField::combo( 'LokasiKode', [ 'name'   => 'LokasiAsal', 'config' => [ 'id' => 'LokasiAsal', 'value' => $dsBeli[ 'LokasiAsal' ],'options' => ['tabindex'=>'3'], 'data' => $DataLokasi, 'disabled'=> ($jenis == 'RepairOUT') ] ] ),
				":LokasiTujuan"      => FormField::combo( 'LokasiKode', [ 'name'   => 'LokasiTujuan', 'config' => [ 'id' => 'LokasiTujuan', 'value' => $dsBeli[ 'LokasiTujuan' ],'options' => ['tabindex'=>'4'], 'data' => $DataLokasi, 'disabled'=> ($jenis == 'RepairIN') ] ] ),
				":JmlUnit"           => FormField::integerInput( [ 'name' => 'JmlUnit', 'config' => [ 'value' => 0, 'readonly' => true ] ] ),
				":SMMemo"            => Html::textInput( 'SMMemo', $dsBeli[ 'SMMemo' ], [ 'class' => 'form-control', 'tabindex' => '5' ] ),
				":SMTotal"           => FormField::decimalInput( [ 'name' => 'SMTotal', 'config' => [ 'value' => $dsBeli[ 'SMTotal' ] ] ] ),
				":BtnSearchFB"       => '<button type="button" class="btn btn-default" id="BtnSearchFB"><i class="glyphicon glyphicon-search"></i></button>',

                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
			],
            [
                'url_main'      => 'ttsmhd',
                'url_delete'    => Url::toRoute(['ttsmhd/delete', 'id' => $_GET['id'] ?? '','jenis' => $jenis] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlAdd     = Url::toRoute( [ 'ttsmhd/create','action'=>'create' , 'jenis' => $jenis ] );
$urlIndex = \aunit\models\Ttsmhd::getRoute( $jenis, [ 'id' => $id ] )[ 'index' ];
$urlDetail  = $url[ 'detail' ];
$urlPrint   = $url[ 'print' ];
$url[ 'jurnal' ] = \yii\helpers\Url::toRoute(['ttsmhd/jurnal','jenis'=>$jenis]);
$urlData    = \yii\helpers\Url::toRoute( [ 'ttsmhd/fill-motor-grid-add-edit', 'jenis' => $jenis ] );
$hideHarga  = 'true';
$readOnly   = ( ($_GET[ 'action' ] == 'view' || $_GET[ 'action' ] == 'display') ? 'true' : 'false' );
$this->registerJsVar( '__model', $dsBeli );
$this->registerJs( <<< JS

     window.fillCombo = () => {
        $.ajax({
            type: 'POST',
            url: '$urlData',
            data: {
                 LokasiKode:  $('#LokasiAsal').find(':selected')[0].value, 
                 SMTgl: $('input[name=\"SMTgl\"]').val(), 
                 SMNo: $('input[name=\"SMNo\"]').val()
            }
        }).then(function (cusData) {
            window.dataRows = cusData.rows;            
        });       
    }
    
    $('#LokasiAsal').on('change',fillCombo);
    $('#LokasiAsal').trigger('change');

     $('#detailGrid').utilJqGrid({ 
        editurl: '$urlDetail',
        height: 300,
        extraParams: {
            SMNo: $('input[name="SMNo"]').val()
        },
        rownumbers: true,           
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,         
        colModel: [
            { template: 'actions', hidden: $readOnly  },
            {
                name: 'MotorType',
                label: 'Type',
                editable: true,
                width: 200,
                editor: {
                    type: $hideHarga ? 'select2' : 'text',
                    readOnly: !$hideHarga,    
                    data: [],
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowId = $(this).attr('rowid');
						    $('[id="'+rowId+'_'+'MotorTahun'+'"]').val(data.MotorTahun);
                            $('[id="'+rowId+'_'+'MotorNama'+'"]').val(data.MotorNama);
                            $('[id="'+rowId+'_'+'MotorAutoN'+'"]').val(data.MotorAutoN);
                            $('[id="'+rowId+'_'+'FBHarga'+'"]').val(data.FBHarga);
                            $('[id="'+rowId+'_'+'MotorNoMesin'+'"]').val(data.MotorNoMesin).trigger('change');
                            $('[id="'+rowId+'_'+'MotorNoRangka'+'"]').val(data.MotorNoRangka).trigger('change');
                            $('[id="'+rowId+'_'+'MotorWarna'+'"]').val(data.MotorWarna).trigger('change');
						}
                    },        
                }
            },
            {
                name: 'MotorNama',
                label: 'MotorNama',
                width: 220,
                hidden: !$hideHarga,
                editoptions:{
                    readOnly: true    
                },
                editable: true
            },
            {
                name: 'MotorTahun',
                label: 'Tahun',
                width: 60,                
                editoptions:{
                    readOnly: true    
                },
                editable: true
            },
            {
                name: 'MotorWarna',
                label: 'Warna',
                editable: true,
                width: 150,
                 editor: {
                    type: $hideHarga ? 'select2' : 'text',
                    readOnly: !$hideHarga,    
                    data: [],
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowId = $(this).attr('rowid');
						    $('[id="'+rowId+'_'+'MotorTahun'+'"]').val(data.MotorTahun);
                            $('[id="'+rowId+'_'+'MotorNama'+'"]').val(data.MotorNama);
                            $('[id="'+rowId+'_'+'MotorAutoN'+'"]').val(data.MotorAutoN);
                            $('[id="'+rowId+'_'+'FBHarga'+'"]').val(data.FBHarga);
                            $('[id="'+rowId+'_'+'MotorNoMesin'+'"]').val(data.MotorNoMesin).trigger('change');
                            $('[id="'+rowId+'_'+'MotorNoRangka'+'"]').val(data.MotorNoRangka).trigger('change');
                            $('[id="'+rowId+'_'+'MotorType'+'"]').val(data.MotorType).trigger('change');
						}
                    },        
                }
            },
            {
                name: 'MotorNoMesin',
                label: 'NoMesin',
                width: 125,
                editable: true,
                 editor: {
                    type: $hideHarga ? 'select2' : 'text',
                    readOnly: !$hideHarga,    
                    data: [],
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowId = $(this).attr('rowid');
						    $('[id="'+rowId+'_'+'MotorTahun'+'"]').val(data.MotorTahun);
                            $('[id="'+rowId+'_'+'MotorNama'+'"]').val(data.MotorNama);
                            $('[id="'+rowId+'_'+'MotorAutoN'+'"]').val(data.MotorAutoN);
                            $('[id="'+rowId+'_'+'FBHarga'+'"]').val(data.FBHarga);
                            $('[id="'+rowId+'_'+'MotorType'+'"]').val(data.MotorType).trigger('change');
                            $('[id="'+rowId+'_'+'MotorNoRangka'+'"]').val(data.MotorNoRangka).trigger('change');
                            $('[id="'+rowId+'_'+'MotorWarna'+'"]').val(data.MotorWarna).trigger('change');
						}
                    },        
                }
            },
            {
                name: 'MotorAutoN',
                label: 'NoMesin',
                width: 125,
                hidden: true ,
                editable: true
            },            
            {
                name: 'MotorNoRangka',
                label: 'NoRangka',
                width: 160,
                editable: true,
                 editor: {
                    type: $hideHarga ? 'select2' : 'text',
                    readOnly: !$hideHarga,    
                    data: [],
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowId = $(this).attr('rowid');
						    $('[id="'+rowId+'_'+'MotorTahun'+'"]').val(data.MotorTahun);
                            $('[id="'+rowId+'_'+'MotorNama'+'"]').val(data.MotorNama);
                            $('[id="'+rowId+'_'+'MotorAutoN'+'"]').val(data.MotorAutoN);
                            $('[id="'+rowId+'_'+'FBHarga'+'"]').val(data.FBHarga);
                            $('[id="'+rowId+'_'+'MotorNoMesin'+'"]').val(data.MotorNoMesin).trigger('change');
                            $('[id="'+rowId+'_'+'MotorType'+'"]').val(data.MotorType).trigger('change');
                            $('[id="'+rowId+'_'+'MotorWarna'+'"]').val(data.MotorWarna).trigger('change');
						}
                    },        
                }
            },
            {
                name: 'FBHarga',
                label: 'Harga',
                template: 'money',
                width: 250,
                hidden: $hideHarga ,
                editable: true
            },
        ],
        listeners: {
            afterLoad: function(grid, response) {
                let jQty = $('#detailGrid').jqGrid('getGridParam', 'records');
                $('input[name="JmlUnit"]').utilNumberControl().val(jQty);      
                let __memo = __model.SMMemo.replace(/~( )*Jumlah( )*Unit( )*:( )*\d*/g,'');
                $('input[name="SMMemo"]').val(__memo + ' ~ Jumlah Unit :  '+ jQty);   
                if (jQty > 0){
                    $("#LokasiAsal").attr('disabled', true);
                    $("#form_ttsmhd_id").append("<input id='LokasiAsalHidden' name='LokasiAsal' type='hidden' value='"+$("#LokasiAsal").val()+"'>");
                }else{
                    $("#LokasiAsal").attr('disabled', false);
                    $("#LokasiAsalHidden").remove();
                }
            },   
        }
    }).init().fit($('.box-body')).load({url: '$urlDetail'});
    $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
       var motorType = $('[id="'+rowid+'_'+'MotorType'+'"]');
        if(motorType[0].type != 'text'){
            motorType.select2('destroy').empty().select2({
            data: $.map(window.dataRows, function (obj) {
                      obj.id = obj.MotorType;
                      obj.text = obj.MotorType;
                      return obj;
                    })
            });
        }
        let motorWarna = $('[id="'+rowid+'_'+'MotorWarna'+'"]');
        if(motorWarna[0].type != 'text'){
            motorWarna.select2('destroy').empty().select2({
            data: $.map(window.dataRows, function (obj) {
                      obj.id = obj.MotorWarna;
                      obj.text = obj.MotorWarna;
                      return obj;
                    })
            });    
        }
        let motorNoMesin = $('[id="'+rowid+'_'+'MotorNoMesin'+'"]');
        if(motorNoMesin[0].type != 'text'){
            motorNoMesin.select2('destroy').empty().select2({
            data: $.map(window.dataRows, function (obj) {
                      obj.id = obj.MotorNoMesin;
                      obj.text = obj.MotorNoMesin;
                      return obj;
                    })
            });
        }
        let motorNoRangka = $('[id="'+rowid+'_'+'MotorNoRangka'+'"]');
        if(motorNoRangka[0].type != 'text'){
            motorNoRangka.select2('destroy').empty().select2({
            data: $.map(window.dataRows, function (obj) {
                      obj.id = obj.MotorNoRangka;
                      obj.text = obj.MotorNoRangka;
                      return obj;
                    })
            });
        }
        if (!rowid.includes('new_row')){
               var d = $('#detailGrid').utilJqGrid().getData(rowid);           
               $('[id="'+rowid+'_'+'MotorNoMesin'+'"]').val(d.data.MotorNoMesin).trigger('change');
               $('[id="'+rowid+'_'+'MotorTahun'+'"]').val(d.data.MotorTahun);
               $('[id="'+rowid+'_'+'MotorNama'+'"]').val(d.data.MotorNama);
               $('[id="'+rowid+'_'+'MotorAutoN'+'"]').val(d.data.MotorAutoN);
               $('[id="'+rowid+'_'+'FBHarga'+'"]').val(d.data.FBHarga);
               $('[id="'+rowid+'_'+'MotorType'+'"]').val(d.data.MotorType).trigger('change');
               $('[id="'+rowid+'_'+'MotorNoRangka'+'"]').val(d.data.MotorNoRangka).trigger('change');
               $('[id="'+rowid+'_'+'MotorWarna'+'"]').val(d.data.MotorWarna).trigger('change');
        }else{
            if (motorType[0].type != 'text' && motorType.select2('data').length > 0){
                var data = motorType.select2('data')[0];
                motorType.val(data.MotorType).trigger('change').trigger({
                    type: 'select2:select',
                    params: {
                        data: data
                    }
                });
            }
        }
        let __memo = __model.SMMemo.replace(/~( )*Jumlah( )*Unit( )*:( )*\d*/g,'');
        $('input[name="SMMemo"]').val(__memo + ' ~ Jumlah Unit :  '+ $('input[name="JmlUnit"]').utilNumberControl().val());
    });
    $('#btnSave').click(function (event) {
        if($('#LokasiAsal').val() === $('#LokasiTujuan').val()){
            bootbox.alert({ size: 'small', message : "Lokasi Asal dan Tujuan tidak boleh sama"});
            return;
        }
        $('#form_ttsmhd_id').submit();
    });  
    $('#btnPrint').click(function (event) {	      
        $('#form_ttsmhd_id').attr('action','$urlPrint');
        $('#form_ttsmhd_id').attr('target','_blank');
        $('#form_ttsmhd_id').submit();
	  }); 
    
    $('#btnJurnal').click(function (event) {	
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#form_ttsmhd_id').attr('action','{$url[ 'jurnal' ]}');
                    $('#form_ttsmhd_id').submit();                     
                }
            }
        });
    });   
    
     $('#btnCancel').click(function (event) {	      
       $('#form_ttsmhd_id').attr('action','{$url['cancel']}');
       $('#form_ttsmhd_id').submit();
    });
     
     $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });       
    
    $('[name=SMTgl]').utilDateControl().cmp().attr('tabindex', '2');
    $('[name=JmlUnit]').utilNumberControl().cmp().attr('tabindex', '6');
    
    
JS
);