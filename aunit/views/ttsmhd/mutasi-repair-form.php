<?php
use common\components\Custom;
use kartik\datecontrol\DateControl;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
$url[ 'cancel' ] = '';
$url[ 'detail' ] = '';
$sqlWarehouse    = Yii::$app->db
	->createCommand( "SELECT LokasiKode as value, LokasiNama as label FROM tdlokasi WHERE LokasiStatus IN ('A','\" & MyPOSLokasiStatus & \"') AND LokasiJenis <> 'Kas' ORDER BY LokasiStatus,LokasiKode" )
	->queryAll();
$cmbWarehouse    = ArrayHelper::map( $sqlWarehouse, 'value', 'label' );
$format          = <<< SCRIPT
function formatttsd(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-10" style="text-align: left">' + result.id + '</div>' +
           '<div class="col-xs-14">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape          = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
<div class="ttpd-form">
	<?php
	$form = ActiveForm::begin( [ 'id' => 'form_ttsmhd_id', 'action' => $url[ 'update' ] ] );
	\aunit\components\TUi::form(
		[
			'class' => "row-no-gutters",
			'items' => [
				[ 'class' => "form-group col-md-24",
				  'items' => [
					  [ 'class' => "col-sm-3", 'items' => ":LabelSMNo" ],
					  [ 'class' => "col-sm-5", 'items' => ":SMNo" ],
					  [ 'class' => "col-sm-2", 'items' => ":SMTgl" ],
					  [ 'class' => "col-sm-3", 'items' => ":SMJam" ],
					  [ 'class' => "col-sm-1" ],
					  [ 'class' => "col-sm-2", 'items' => ":LabelLokasiAsal" ],
					  [ 'class' => "col-sm-8", 'items' => ":LokasiAsal" ],
				  ],
				],
				[ 'class' => "form-group col-md-24",
				  'items' => [
					  [ 'class' => "col-sm-3", 'items' => ":LabelPP" ],
					  [ 'class' => "col-sm-5", 'items' => ":SMNo" ],
					  [ 'class' => "col-sm-6" ],
					  [ 'class' => "col-sm-2", 'items' => ":LabelLokasiTujuan" ],
					  [ 'class' => "col-sm-8", 'items' => ":LokasiTujuan" ],
				  ],
				],
				[ 'class' => "row col-md-24", 'style' => "margin-bottom: 3px;", 'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>' ],
				[ 'class' => "col-md-24",
				  'items' => [
					  [ 'class' => "form-group col-md-3", 'items' => ":LabelSMMemo" ],
					  [ 'class' => "form-group col-md-10", 'items' => ":SMMemo" ],
					  [ 'class' => "form-group col-md-1" ],
					  [ 'class' => "form-group col-md-2", 'items' => ":LabelJmlUnit" ],
					  [ 'class' => "form-group col-md-2", 'items' => ":JmlUnit" ],
				  ]
				],
			],
		],
		[
			'class' => "pull-right",
			'items' => ":btnJurnal :btnPrint :btnAction"
		],
		[
			":LabelSMNo"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Surat Jalan</label>',
			":LabelLokasiAsal"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Asal</label>',
			":LabelPP"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">No PP</label>',
			":LabelLokasiTujuan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tujuan</label>',
			":LabelSMMemo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
			":LabelJmlUnit"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jumlah Unit</label>',
			":SMNo"         => $form->field( $model, 'SMNo', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true ] ),
			":SMTgl"        => $form->field( $model, 'SMTgl', [ 'template' => '{input}' ] )
			                        ->widget( DateControl::className(), [
				                        'type'          => DateControl::FORMAT_DATE,
				                        'pluginOptions' => [ 'autoclose' => true ] ] ),
			":SMJam"        => $form->field( $model, 'SMJam', [ 'template' => '{input}' ] )
			                        ->widget( DateControl::className(), [
				                        'type'          => DateControl::FORMAT_TIME,
				                        'pluginOptions' => [ 'autoclose' => true ] ] ),
			":LokasiAsal"   => Select2::widget( [
				'name'          => 'LokasiAsal',
				'data'          => $cmbWarehouse,
				'options'       => [ 'class' => 'form-control' ],
				'theme'         => Select2::THEME_DEFAULT,
				'pluginOptions' => [
					'dropdownAutoWidth' => true,
					'templateResult'    => new JsExpression( 'formatttsd' ),
					'templateSelection' => new JsExpression( 'formatttsd' ),
					'escapeMarkup'      => $escape, ] ] ),
			":LokasiTujuan" => Select2::widget( [
				'name'          => 'LokasiTujuan',
				'data'          => $cmbWarehouse,
				'options'       => [ 'class' => 'form-control' ],
				'theme'         => Select2::THEME_DEFAULT,
				'pluginOptions' => [
					'dropdownAutoWidth' => true,
					'templateResult'    => new JsExpression( 'formatttsd' ),
					'templateSelection' => new JsExpression( 'formatttsd' ),
					'escapeMarkup'      => $escape, ] ] ),
			":JmlUnit"      => '<input type="text" id="JumlahUnit" class="form-control" name="JumlahUnit">',
			":SMMemo"       => $form->field( $model, 'SMMemo', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true ] ),
			":BtnSearchFB" => '<button type="button" class="btn btn-default" id="BtnSearchFB"><i class="glyphicon glyphicon-search"></i></button>',



            ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
            ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
//            ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
            ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
            ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
            ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
            ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),
		],
        [
            'url_main'      => 'ttsmhd',
            'url_delete'    => Url::toRoute(['ttsmhd/delete', 'id' => $_GET['id'] ?? '','jenis' => ''] ),
        ]

	);
	ActiveForm::end();
	?>
</div>
<?php
//$urlMotorWarna = $url['tdmotorwarna'];
$urlPrint = $url[ 'print' ];
$this->registerJs( \aunit\components\TUi::detailGridJs( \aunit\models\Tsaldomotor::className(), [
	'url'               => $url[ 'detail' ],
	'height'            => 140,
	'extraParams'       => [
		'SMNo' => $model->SMNo
	],
	'colModel'          => [
		[ 'name'     => 'MotorType', 'label' => 'Type', 'width' => 150, 'editable' => true,
		  'editor'   => [
			  'type' => 'select2', 'dataArray' => \aunit\models\Tdmotortype::find()->comboSelect2(), 'dropdownAutoWidth' => true
		  ],
		  'onchange' => new \yii\web\JsExpression( "function(e) {
                let motorWarna = $('[id=\"'+$(this).attr('rowid')+'_'+'MotorWarna'+'\"]');
                let motorTahun = $('[id=\"'+$(this).attr('rowid')+'_'+'MotorTahun'+'\"]');
                motorWarna.utilSelect2().loadRemote(
                '" . \common\components\Custom::url( 'tdmotorwarna/find' ) . "',
                { mode: 'combo', MotorType: this.value }, true);
                if(!motorTahun.val()) motorTahun.val(new Date().getFullYear());
            }" )
		],
		[ 'name' => 'MotorTahun', 'label' => 'Tahun', 'width' => 100, 'editable' => true ],
		[ 'name'   => 'MotorWarna', 'label' => 'Warna', 'width' => 100, 'editable' => true,
		  'editor' => [ 'type' => 'select2', 'dropdownAutoWidth' => true ]
		],
		[ 'name' => 'MotorNoMesin', 'label' => 'NoMesin', 'width' => 150, 'editable' => true ],
		[ 'name' => 'MotorNoRangka', 'label' => 'NoRangka', 'width' => 150, 'editable' => true ],
		[ 'name' => 'No Surat Jalan', 'label' => 'NoSuratJalan', 'width' => 146, 'editable' => true ],
		[ 'name'          => 'HargaBeli', 'label' => 'Motor Harga', 'editable' => true, 'align' => "right", 'formatter' => 'integer',
		  'formatoptions' => [ 'decimalSeparator' => ".", 'decimalPlaces' => 2, 'thousandsSeparator' => "," ],
		]
	],
	'funcAfterLoadGrid' => <<< JS
        function(grid, response) {
            var total = 0,
                count = 0;
            
            for(var i = 0; i < response.rows.length; i++) {
                // total += parseFloat(response.rows[i].MotorHarga);
                count++;
            }
            
            if (count === 0)
                $('#btnSave').attr('disabled', 'disabled');
            else
                $('#btnSave').removeAttr('disabled');
            
            $('#JumlahUnit').val(count);
            // $('#SubTotal').val(total);
        }
        $('#btnPrint').click(function (event) {	      
        $('#form_ttsmhd_id').attr('action','$urlPrint');
        $('#form_ttsmhd_id').attr('target','_blank');
        $('#form_ttsmhd_id').submit();
	  });
JS
] ) );
$this->registerJs( <<< JS
     $('#btnCancel').click(function (event) {	      
       $('#form_ttsmhd_id').attr('action','{$url['cancel']}');
       $('#form_ttsmhd_id').submit();
    });
     
JS
);