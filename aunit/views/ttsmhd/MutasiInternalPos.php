<?php
use aunit\assets\AppAsset;
use aunit\models\Ttsmhd;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = 'Surat Jalan Mutasi ke POS';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'SMNo'         => 'No Mutasi [SM/MI]',
		'LokasiAsal'   => 'Lokasi Asal',
		'LokasiTujuan' => 'Lokasi Tujuan',
		'SMMemo'       => 'Keterangan',
		'UserID'       => 'User ID',
	],
	'cmbTgl'    => [
		'SMTgl' => 'Tanggal [SM/MI]',
	],
	'cmbNum'    => [
		'SMTotal' => 'Total',
	],
	'sortname'  => "SMTgl",
	'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttsmhd::className(),
	'Mutasi Internal ke Pos', [
		'mode' => isset( $mode ) ? $mode : '',
		'colGrid'    => Ttsmhd::colGridMutasiInternalPos(),
		'url_add'    => Url::toRoute( [ 'ttsmhd/create', 'jenis' => 'MutasiPOS', 'action' => 'create' ] ),
		'url_update' => Url::toRoute( [ 'ttsmhd/update', 'jenis' => 'MutasiPOS' ,'action' => 'update'] ),
		'url_delete' => Url::toRoute( [ 'ttsmhd/delete', 'jenis' => 'MutasiPOS' ] ),
	] ), \yii\web\View::POS_READY );

$urlDetail = Url::toRoute( [ 'ttsmit/index'] );
$this->registerJs( <<< JS
jQuery(function ($) {
    if($('#jqGridDetail').length) {
        $('#jqGridDetail').utilJqGrid({
            url: '$urlDetail',
            height: 240,
            readOnly: true,
            rownumbers: true,
            rowNum: 1000,
            colModel: [
                {
                    name: 'MotorType',
                    label: 'Type',
                    width: 200
                },
               	{
	                name: 'MotorTahun',
	                label: 'Tahun',
	                width: 100,
	            },
	            {
	                name: 'MotorWarna',
	                label: 'Warna',
	                width: 100,
	            },
	            {
	                name: 'MotorNoMesin',
	                label: 'NoMesin',
	                width: 160,
	            },
	            {
	                name: 'MotorAutoN',
	                label: 'NoMesin',
	                width: 160,
	                hidden: true ,
	            },
	            {
	                name: 'MotorNoRangka',
	                label: 'NoRangka',
	                width: 160,
	            },
	            {
	                name: 'FBHarga',
	                label: 'HrgBeli',
	                width: 100,
	                template: 'money',
	            }
            ],
	        multiselect: gridFn.detail.multiSelect(),
	        onSelectRow: gridFn.detail.onSelectRow,
	        onSelectAll: gridFn.detail.onSelectAll,
	        ondblClickRow: gridFn.detail.ondblClickRow,
	        loadComplete: gridFn.detail.loadComplete,
	        listeners: {
                afterLoad: function(grid, response) {
                    $("#cb_jqGridDetail").trigger('click');
                }
            }
        }).init().setHeight(gridFn.detail.height);
    }
});
JS, \yii\web\View::POS_READY );