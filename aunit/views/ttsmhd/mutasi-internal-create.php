<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttsmhd */

$this->title = 'Tambah Surat Jalan Mutasi Ke POS';
$this->params['breadcrumbs'][] = ['label' => 'Mutasi Internal', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttsmhd-create">
    <?= $this->render('mutasi-internal-form', [
        'model' => $model,
    ]) ?>

</div>
