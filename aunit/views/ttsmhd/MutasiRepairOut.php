<?php
use aunit\assets\AppAsset;
use aunit\models\Ttsd;
use aunit\models\Ttsmhd;
use yii\helpers\Html;
use yii\grid\GridView;
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = 'Repair OUT';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'SMNo'         => 'No Mutasi [SM/RO]',
		'LokasiAsal'   => 'Lokasi Asal',
		'LokasiTujuan' => 'Lokasi Tujuan',
		'SMMemo'       => 'Keterangan',
		'UserID'       => 'User ID',
	],
	'cmbTgl'    => [
		'SMTgl' => 'Tanggal [SM/RO]',
	],
	'cmbNum'    => [
		'SMTotal' => 'Total',
	],
	'sortname'  => "SMTgl",
	'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttsmhd::className(),
	'Repair OUT', [
		'colGrid'    => Ttsmhd::colGridMutasiRepairOut(),
		'url_add'    => Url::toRoute( [ 'ttsmhd/create', 'jenis' => 'RepairOUT', 'action' => 'create' ] ),
		'url_update' => Url::toRoute( [ 'ttsmhd/update', 'jenis' => 'RepairOUT','action' => 'update' ] ),
		'url_delete' => Url::toRoute( [ 'ttsmhd/delete', 'jenis' => 'RepairOUT' ] ),
	] ), \yii\web\View::POS_READY );
