<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttsmhd */
switch ( $jenis ) {
	case "MutasiPOS":
		$this->title = "Surat Jalan Mutasi ke POS";
		break;
	case "RepairIN":
		$this->title = "Mutasi Repair IN";
		break;
	case "RepairOUT":
		$this->title = "Mutasi Repair OUT";
		break;
}
$title = $this->title;
$cancel = Url::toRoute( [ \Yii::$app->controller->id . '/cancel', 'jenis' => $jenis, 'id' => $id, 'action' => 'update' ] );
$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode).' - ' . $this->title . ' : ' . $model->SMNo;
$this->params[ 'breadcrumbs' ][] = [ 'label' => $title, 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = 'Edit';
?>
<div class="ttsmhd-update">
	<?= $this->render( '_form', [
		'model'  => $model,
		'dsBeli' => $dsBeli,
		'jenis'  => $jenis,
		'id'     => $id,
		'url'    => [
			'update' => Url::toRoute( [ \Yii::$app->controller->id . '/update', 'jenis' => $jenis, 'id' => $id, 'action' => 'update' ] ),
			'print'  => Url::toRoute( [ \Yii::$app->controller->id . '/print', 'id' => $id, 'action' => 'update' ] ),
			'cancel' => $cancel,
			'detail' => Url::toRoute( [ 'ttsmit/index', 'jenis' => $jenis, 'action' => 'update' ] ),
		]
	] ) ?>
</div>
