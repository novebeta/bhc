<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttsmhd */
switch ( $jenis ) {
	case "MutasiPOS":
		$this->title = "Surat Jalan Mutasi ke POS";
		break;
	case "RepairIN":
		$this->title = "Mutasi Repair IN";
		break;
	case "RepairOUT":
		$this->title = "Mutasi Repair OUT";
		break;
}
$title = $this->title;
$cancel = Url::toRoute( [ \Yii::$app->controller->id . '/cancel', 'jenis' => $jenis, 'id' => $id, 'action' => 'create' ] );
$this->params[ 'breadcrumbs' ][] = [ 'label' => $title, 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="ttsmhd-create">
	<?= $this->render( '_form', [
		'model'  => $model,
		'dsBeli' => $dsBeli,
		'jenis'  => $jenis,
		'id'     => $id,
		'url'    => [
			'update' => Url::toRoute( [ \Yii::$app->controller->id . '/update', 'jenis' => $jenis, 'id' => $id, 'action' => 'create' ] ),
			'print'  => Url::toRoute( [ \Yii::$app->controller->id . '/print', 'id' => $id, 'action' => 'create' ] ),
			'cancel' => $cancel,
			'detail' => Url::toRoute( [ 'ttsmit/index', 'jenis' => $jenis, 'action' => 'create' ] ),
		]
	] ) ?>
</div>
