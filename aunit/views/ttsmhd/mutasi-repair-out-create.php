<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttsmhd */

$this->title = 'Tambah Mutasi Repair Out';
$this->params['breadcrumbs'][] = ['label' => 'Ttsmhds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttsmhd-create">
    <?= $this->render('mutasi-repair-form', [
        'model' => $model,
    ]) ?>

</div>
