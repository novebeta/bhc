<?php
use aunit\components\FormField;
use common\components\Custom;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttrk */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function formattrk(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-6">' + result.id + '</div>' +
           '<div class="col-xs-18">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttrk-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_ttrk_id', 'action' => $url[ 'update' ] ] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group  col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelRKNo" ],
						  [ 'class' => "col-sm-6", 'items' => ":RKNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelSKNo" ],
						  [ 'class' => "col-sm-2", 'items' => ":SKNo" ],
						  [ 'class' => "col-sm-2", 'items' => ":SKTgl" ],
						  [ 'class' => "col-sm-1", 'items' => ":BtnSearchFB" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelDKNo" ],
						  [ 'class' => "col-sm-3", 'items' => ":DKNo" ],
						  [ 'class' => "col-sm-2", 'items' => ":DKTgl" ],
					  ],
					],
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelRKTgl" ],
						  [ 'class' => "col-sm-4", 'items' => ":RKTgl" ],
						  [ 'class' => "col-sm-2", 'items' => ":RKJam" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelWarehouse" ],
						  [ 'class' => "col-sm-13", 'items' => ":Warehouse" ],
					  ],
					],
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelRKMemo" ],
						  [ 'class' => "col-sm-22", 'items' => ":RKMemo" ],
					  ],
					],
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelCusNama" ],
						  [ 'class' => "col-sm-6", 'items' => ":CusNama" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelCusKTP" ],
						  [ 'class' => "col-sm-5", 'items' => ":CusKTP" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelCusKabupaten" ],
						  [ 'class' => "col-sm-5", 'items' => ":CusKabupaten" ],
					  ],
					],
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelCusAlamat" ],
						  [ 'class' => "col-sm-6", 'items' => ":CusAlamat" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelCusRT" ],
						  [ 'class' => "col-sm-2", 'items' => ":CusRT" ],
						  [ 'class' => "col-sm-1", 'style' => "text-align:center", 'items' => ":LabelCusRW" ],
						  [ 'class' => "col-sm-2", 'items' => ":CusRW" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelCusKecamatan" ],
						  [ 'class' => "col-sm-5", 'items' => ":CusKecamatan" ],
					  ],
					],
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelCusTelepon" ],
						  [ 'class' => "col-sm-6", 'items' => ":CusTelepon" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelKodePos" ],
						  [ 'class' => "col-sm-5", 'items' => ":CusKodePos" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelCusKelurahan" ],
						  [ 'class' => "col-sm-5", 'items' => ":CusKelurahan" ],
					  ],
					],
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelMotorTypeTahun" ],
						  [ 'class' => "col-sm-4", 'items' => ":MotorType" ],
						  [ 'class' => "col-sm-2", 'items' => ":MotorTahun" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelMotorWarna" ],
						  [ 'class' => "col-sm-5", 'items' => ":MotorWarna" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelHargaOTR" ],
						  [ 'class' => "col-sm-5", 'items' => ":HargaOTR" ],
					  ],
					],
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelNoMesin" ],
						  [ 'class' => "col-sm-6", 'items' => ":NoMesin" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelNoRangka" ],
						  [ 'class' => "col-sm-13", 'items' => ":NoRangka" ],
					  ],
					],
				]
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
                  ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                      'items'  => [
                          ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":NoGL"],
                          ['class' => "col-sm-4 pull-right", 'items' => ":LabelNoGL"],
                      ],
                  ],
                  [
                      'class' => "col-md-12 pull-left",
                      'items' => $this->render( '../_nav', [
                          'url'=> $_GET['r'],
                          'options'=> [
                              'ttrk.RKNo'         => 'No RK',
                              'DKNo'         => 'No DK',
                              'SKNo'         => 'No SK',
                              'MotorNoMesin' => 'No Mesin',
                              'LokasiKode'   => 'Lokasi',
                              'RKMemo'       => 'Keterangan',
                              'NoGL'         => 'No GL',
                              'UserID'       => 'UserID',
                          ],
                      ])
                  ],
                  ['class' => "col-md-12 pull-right",
				    'items' => [
					    [ 'class' => "pull-right", 'items' => ":btnJurnal  :btnPrint :btnAction" ]
				    ]
				  ]
			  ]
			],
			[
				/* label */
				":LabelRKNo"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Retur</label>',
				":LabelSKNo"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">No - Tgl SK</label>',
				":LabelDKNo"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">No - Tgl DK</label>',
				":LabelRKTgl"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tanggal</label>',
				":LabelWarehouse"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Warehouse</label>',
				":LabelRKMemo"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelNoGL"           => \common\components\General::labelGL( $dsJual[ 'NoGL' ] ),
				":LabelCusNama"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Konsumen</label>',
				":LabelCusAlamat"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
				":LabelCusKTP"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">KTP</label>',
				":LabelCusRT"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">RT / RW</label>',
				":LabelCusRW"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">/</label>',
				":LabelCusTelepon"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon</label>',
				":LabelKodePos"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">KodePos</label>',
				":LabelMotorTypeTahun" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Type / Tahun</label>',
				":LabelMotorWarna"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Warna</label>',
				":LabelHargaOTR"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Harga OTR</label>',
				":LabelNoMesin"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Mesin</label>',
				":LabelNoRangka"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Rangka</label>',
				":LabelCusKabupaten"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kabupaten</label>',
				":LabelCusKecamatan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kecamatan</label>',
				":LabelCusKelurahan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kelurahan</label>',
				/* componen */
				":RKJam"               => FormField::timeInput( [ 'name' => 'RKJam', 'config' => [ 'value' => $dsJual[ 'RKJam' ], 'readonly' => true ] ] ),
				":RKNo"                => Html::textInput( 'RKNoView', $dsJual[ 'RKNoView' ], [ 'class' => 'form-control', 'readonly' => true, 'tabindex' => '1' ] ) .
				                          Html::textInput( 'RKNo', $dsJual[ 'RKNo' ], [ 'class' => 'hidden' ] ) .
				                          Html::textInput( 'CusKode', $dsJual[ 'CusKode' ], [ 'class' => 'hidden' ] ),
				":SKNo"                => Html::textInput( 'SKNo', $dsJual[ 'SKNo' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":DKNo"                => Html::textInput( 'DKNo', $dsJual[ 'DKNo' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Warehouse"           => FormField::combo( 'LokasiKode', [ 'config' => [ 'value' => $dsJual[ 'LokasiKode' ], 'pluginOptions' => [ 'disabled' => true ] ], 'extraOptions' => [ 'altLabel' => [ 'LokasiNama' ], ] ] ),
				":RKMemo"              => Html::textInput( 'RKMemo', $dsJual[ 'RKMemo' ], [ 'class' => 'form-control', 'tabindex' => '3' ] ),
				":NoGL"                => Html::textInput( 'NoGL', $dsJual[ 'NoGL' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":CusAlamat"           => Html::textInput( 'CusAlamat', $dsJual[ 'CusAlamat' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":CusRT"               => Html::textInput( 'CusRT', $dsJual[ 'CusRT' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":CusRW"               => Html::textInput( 'CusRW', $dsJual[ 'CusRW' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":CusTelepon"          => Html::textInput( 'CusTelepon', $dsJual[ 'CusTelepon' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":CusNama"             => Html::textInput( 'CusNama', $dsJual[ 'CusNama' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":MotorType"           => Html::textInput( 'MotorType', $dsJual[ 'MotorType' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":MotorTahun"          => Html::textInput( 'MotorTahun', $dsJual[ 'MotorTahun' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":MotorWarna"          => Html::textInput( 'MotorWarna', $dsJual[ 'MotorWarna' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NoMesin"             => Html::textInput( 'MotorNoMesin', $dsJual[ 'MotorNoMesin' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NoRangka"            => Html::textInput( 'MotorNoRangka', $dsJual[ 'MotorNoRangka' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":HargaOTR"            => FormField::numberInput( [ 'name' => 'DKHarga', 'config' => [ 'value' => $dsJual[ 'DKHarga' ], 'readonly' => true ] ] ),
				":CusKTP"              => Html::textInput( 'CusKTP', $dsJual[ 'CusKTP' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":CusKodePos"          => Html::textInput( 'CusKodePos', $dsJual[ 'CusKodePos' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":CusKabupaten"        => Html::textInput( 'CusKabupaten', $dsJual[ 'CusKabupaten' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":CusKecamatan"        => Html::textInput( 'CusKecamatan', $dsJual[ 'CusKecamatan' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":CusKelurahan"        => Html::textInput( 'CusKelurahan', $dsJual[ 'CusKelurahan' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":SKTgl"               => FormField::dateInput( [ 'name' => 'SKTgl', 'config' => [ 'value' => $dsJual[ 'SKTgl' ], 'readonly' => true ] ] ),
				":DKTgl"               => FormField::dateInput( [ 'name' => 'DKTgl', 'config' => [ 'value' => $dsJual[ 'DKTgl' ], 'readonly' => true ] ] ),
				":RKTgl"               => FormField::dateInput( [ 'name' => 'RKTgl', 'config' => [ 'value' => $dsJual[ 'RKTgl' ], 'readonly' => true ] ] ),
				/* button */
				":BtnSearchFB"         => '<button type="button" class="btn btn-default" id="BtnSearchFB" tabindex="2"><i class="glyphicon glyphicon-search"></i></button>',

                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
//                ":btnImport"      => Html::button('<i class="fa fa-arrow-circle-down"><br><br>Import</i>   ', ['class' => 'btn bg-navy ', 'id' => 'btnImport', 'style' => 'width:60px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),


//                ":btnSave"             => Html::button( '<i class="glyphicon glyphicon-floppy-disk"></i> Simpan', [ 'class' => 'btn btn-info btn-primary ', 'id' => 'btnSave' . Custom::viewClass() ] ),
//				":btnCancel"           => Html::Button( '<i class="fa fa-ban"></i> Batal', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:80px' ] ),
//				":btnAdd"              => Html::button( '<i class="fa fa-plus-circle"></i> Tambah', [ 'class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:80px' ] ),
//				":btnEdit"             => Html::button( '<i class="fa fa-edit"></i> Edit', [ 'class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:80px' ] ),
//				":btnDaftar"           => Html::Button( '<i class="fa fa-list"></i> Daftar ', [ 'class' => 'btn btn-primary ', 'id' => 'btnDaftar', 'style' => 'width:80px' ] ),
//				":btnPrint"            => Html::button( '<i class="glyphicon glyphicon-print"></i>  Print ', [ 'class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:80px' ] ),
//				":btnJurnal"           => Html::button( '<i class="fa fa-balance-scale"></i>  Jurnal ', [ 'class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:80px' ] ),
			],
            [
                '_akses' => 'Retur Konsumen',
                'url_main' => 'ttrk',
                'url_id' => $_GET['id'] ?? '',
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlAdd    = Url::toRoute( [ 'ttrk/create', 'action' => 'create' ] );
$urlFB     = Url::toRoute( [ 'ttsk/select', 'SKNo' => $dsJual[ 'SKNo' ] ] );
$urlLoop   = Url::toRoute( [ 'ttrk/loop' ] );
$urlPrint  = $url[ 'print' ];
$urlIndex  = Url::toRoute( [ 'ttrk/index' ] );
$urlJurnal = Url::toRoute( [ 'ttrk/jurnal', 'action' => $_GET[ 'action' ] ] );
$this->registerJs( <<< JS
    $('#BtnSearchFB').click(function() {
        window.util.app.dialogListData.show({
            title: 'Daftar Surat Jalan Konsumen',
            url: '$urlFB'
        },
        function (data) {
            if(data.data == undefined){
                return ;
            }
            console.log(data);
             $.ajax({
                    type: 'POST',
                    url: '$urlLoop',
                    data: {
                       'RKNo': '{$dsJual['RKNo']}',
                        'SKNo': data.data.SKNo,
                        'RKJam': '{$dsJual['RKJam']}',
                        'RKTgl': '{$dsJual['RKTgl']}'
                    }
                }).then(function (cusData) { // console.log(data);
                    window.location.href = "{$url['update']}&oper=skip-load&RKNoView={$dsJual['RKNoView']}"; 
                });
        })
    });
    $('#btnSave').click(function (event) {
        $('#form_ttrk_id').submit();
    });  
    $('#btnPrint').click(function (event) {	      
        $('#form_ttrk_id').attr('action','$urlPrint');
        $('#form_ttrk_id').attr('target','_blank');
        $('#form_ttrk_id').submit();
	  });
    
    $('#btnJurnal').click(function (event) {	
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#form_ttrk_id').attr('action','{$urlJurnal}');
                    $('#form_ttrk_id').submit();                     
                }
            }
        });
    });       
    
     $('#btnCancel').click(function (event) {	      
       $('#form_ttrk_id').attr('action','{$url['cancel']}');
       $('#form_ttrk_id').submit();
    });
     
     $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });
     
    
JS
);
