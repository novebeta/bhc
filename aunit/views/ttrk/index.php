<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Retur Konsumen';
$this->params['breadcrumbs'][] = $this->title;
$arr = [
	'cmbTxt'    => [
			'RKNo'         => 'No RK',
			'DKNo'         => 'No DK',
			'SKNo'         => 'No SK',
			'MotorNoMesin' => 'No Mesin',
			'LokasiKode'   => 'Lokasi',
			'RKMemo'       => 'Keterangan',
			'NoGL'         => 'No GL',
			'UserID'       => 'UserID',
	],
	'cmbTgl'    => [
			'RKTgl'        => 'Tanggal RK',
	],
	'cmbNum'    => [
	],
	'sortname'  => "RKTgl",
	'sortorder' => 'desc'
];
use aunit\assets\AppAsset;
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttrk::className(),
	'Retur Konsumen',[
        'url_delete'    => Url::toRoute( [ 'ttrk/delete'] ),
	        ] ), \yii\web\View::POS_READY );
