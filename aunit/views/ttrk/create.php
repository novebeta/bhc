<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttrk */
$this->title                     = 'Tambah Retur Konsumen';
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Ttrks', 'url' => [ 'index' ] ];
//$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="ttrk-create">
	<?= $this->render( '_form', [
		'model'  => $model,
		'dsJual' => $dsJual,
		'url'    => [
			'update' => Url::toRoute( [ 'ttrk/update', 'id' => $id, 'action' => 'create' ] ),
			'print'  => Url::toRoute( [ 'ttrk/print', 'id' => $id, 'action' => 'create' ] ),
			'cancel' => Url::toRoute( [ 'ttrk/cancel', 'id' => $id, 'action' => 'create' ] ),
		]
	] ) ?>
</div>
