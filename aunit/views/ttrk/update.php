<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttrk */

$params = '&id='.$id.'&action=update';
$cancel = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title = str_replace('Menu','',\aunit\components\TUi::$actionMode).' - Retur Konsumen: ' . $dsJual[ 'RKNoView' ];
//$this->params['breadcrumbs'][] = ['label' => 'Retur Konsumen', 'url' => $cancel];
//$this->params['breadcrumbs'][] = 'Edit';

//$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode).'Retur Konsumen ' . $model->RKNo;
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Retur Konsumen', 'url' => [ 'index' ] ];
//$this->params[ 'breadcrumbs' ][] = [ 'label' => $model->RKNo, 'url' => [ 'update', 'id' => $model->RKNo ] ];
//$this->params[ 'breadcrumbs' ][] = 'Edit';
use yii\helpers\Url; ?>
<div class="ttrk-update">
	<?= $this->render( '_form', [
		'model'  => $model,
		'dsJual' => $dsJual,
		'url' => [
            'update'    => Custom::url(\Yii::$app->controller->id.'/update'.$params ),
            'print'     => Custom::url(\Yii::$app->controller->id.'/print'.$params ),
            'cancel'    => $cancel,
            'detail'    => Custom::url(\Yii::$app->controller->id.'/detail' )
        ]
//		'url'    => [
//			'update' => Url::toRoute( [ 'ttrk/update', 'id' => $id, 'action' => 'update' ] ),
//			'print'  => Url::toRoute( [ 'ttrk/print', 'id' => $id, 'action' => 'update' ] ),
//			'cancel' => Url::toRoute( [ 'ttrk/index', 'id' => $id, 'action' => 'update' ] ),
//		]
	] ) ?>
</div>
