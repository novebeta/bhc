<?php
use aunit\components\FormField;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel aunit\models\TtsdhdSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Panggil Antrian';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'SDNo'              => 'No SD',
	],
	'cmbTgl'    => [
		'SDTGL'           => 'Tgl SD',
	],
	'cmbNum'    => [
		'SDTotalBiaya'      => 'Total Biaya',
	],
	'sortname'  => "SDTgl",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
<div class="panggil-antrian-form">
	<?php
	$form = ActiveForm::begin([]);
	\aunit\components\TUi::form(
		[
			'class' => "row-no-gutters",
			'items' => [
				['class' => "form-group col-md-8",
					'items' => [
                        ['class' => "form-group col-md-24",
                            'items' => [
						        ['class' => "col-sm-24",'items' => ":LabelPanggil", 'style' => 'text-align:center;font-size:20px;'],
                            ],
                        ],
                        ['class' => "form-group col-md-24",
                            'items' => [
                                ['class' => "col-sm-6",'items' => ":LabelSDNo"],
                                ['class' => "col-sm-17",'items' => ":SDNo"],
                            ],
                        ],
                        ['class' => "form-group col-md-24",
                            'items' => [
                                ['class' => "col-sm-6",'items' => ":LabelNoUrut"],
                                ['class' => "col-sm-6",'items' => ":NoUrut"],
                                ['class' => "col-sm-1"],
                                ['class' => "col-sm-6",'items' => ":LabelJmlAntrian"],
                                ['class' => "col-sm-4",'items' => ":JmlAntrian"],
                            ],
                        ],
                        ['class' => "form-group col-md-24",
                            'items' => [
                                ['class' => "col-sm-6",'items' => ":LabelNoPol"],
                                ['class' => "col-sm-17",'items' => ":NoPol"],
                            ],
                        ],
                        ['class' => "form-group col-md-24",
                            'items' => [
                                ['class' => "col-sm-6",'items' => ":LabelNama"],
                                ['class' => "col-sm-17",'items' => ":Nama"],
                            ],
                        ],
                        ['class' => "form-group col-md-24",
                            'items' => [
                                ['class' => "col-sm-6",'items' => ":LabelKelurahan"],
                                ['class' => "col-sm-17",'items' => ":Kelurahan"],
                            ],
                        ],
                        ['class' => "form-group col-md-24",
                            'items' => [
                                ['class' => "col-sm-6",'items' => ":LabelKecamatan"],
                                ['class' => "col-sm-17",'items' => ":Kecamatan"],
                            ],
                        ],
                        ['class' => "form-group col-md-24",
                            'items' => [
                                ['class' => "col-sm-6",'items' => ":LabelMotor"],
                                ['class' => "col-sm-17",'items' => ":Motor"],
                            ],
                        ],
                        ['class' => "form-group col-md-24",
                            'items' => [
                                ['class' => "col-sm-6",'items' => ":LabelTujuan"],
                                ['class' => "col-sm-17",'items' => ":Tujuan"],
                            ],
                        ],
					],
				],

                ['class' => "form-group col-md-16",
                    'items' => [
                        ['class' => "form-group col-md-12",
                            'items' => [
                                ['class' => "form-group col-md-24",
                                    'items' => [
                                        ['class' => "col-sm-24",'items' => ":LabelKsr", 'style' => 'text-align:center;font-size:20px;'],
                                    ],
                                ],
                                ['class' => "form-group col-md-24",
                                    'items' => [
                                        ['class' => "col-sm-2"],
                                        ['class' => "col-sm-4",'items' => ":LabelNoKasir"],
                                        ['class' => "col-sm-17",'items' => ":NoKasir"],
                                    ],
                                ],
                                ['class' => "form-group col-md-24",
                                    'items' => [
                                        ['class' => "col-sm-2"],
                                        ['class' => "col-sm-4",'items' => ":LabelKasirNoUrut"],
                                        ['class' => "col-sm-17",'items' => ":KasirNoUrut"],
                                    ],
                                ],
                                ['class' => "form-group col-md-24",
                                    'items' => [
                                        ['class' => "col-sm-2"],
                                        ['class' => "col-sm-4",'items' => ":LabelKasirNoPol"],
                                        ['class' => "col-sm-17",'items' => ":KasirNoPol"],
                                    ],
                                ],
                                ['class' => "form-group col-md-24",
                                    'items' => [
                                        ['class' => "col-sm-2"],
                                        ['class' => "col-sm-4",'items' => ":LabelKasirNama"],
                                        ['class' => "col-sm-17",'items' => ":KasirNama"],
                                    ],
                                ],
                                ['class' => "form-group col-md-24",
                                    'items' => [
                                        ['class' => "col-sm-2"],
                                        ['class' => "col-sm-22",'items' => ":btnKasir"],
                                    ],
                                ],
                            ],
                        ],
                        ['class' => "form-group col-md-12",
                            'items' => [
                                ['class' => "form-group col-md-24",
                                    'items' => [
                                        ['class' => "col-sm-24",'items' => ":LabelMkn", 'style' => 'text-align:center;font-size:20px;'],
                                    ],
                                ],
                                ['class' => "form-group col-md-24",
                                    'items' => [
                                        ['class' => "col-sm-2"],
                                        ['class' => "col-sm-4",'items' => ":LabelNoMekanik"],
                                        ['class' => "col-sm-17",'items' => ":NoMekanik"],
                                    ],
                                ],
                                ['class' => "form-group col-md-24",
                                    'items' => [
                                        ['class' => "col-sm-2"],
                                        ['class' => "col-sm-4",'items' => ":LabelMekanikNoUrut"],
                                        ['class' => "col-sm-17",'items' => ":MekanikNoUrut"],
                                    ],
                                ],
                                ['class' => "form-group col-md-24",
                                    'items' => [
                                        ['class' => "col-sm-2"],
                                        ['class' => "col-sm-4",'items' => ":LabelMekanikNoPol"],
                                        ['class' => "col-sm-17",'items' => ":MekanikNoPol"],
                                    ],
                                ],
                                ['class' => "form-group col-md-24",
                                    'items' => [
                                        ['class' => "col-sm-2"],
                                        ['class' => "col-sm-4",'items' => ":LabelMekanikNama"],
                                        ['class' => "col-sm-17",'items' => ":MekanikNama"],
                                    ],
                                ],
                                ['class' => "form-group col-md-24",
                                    'items' => [
                                        ['class' => "col-sm-2"],
                                        ['class' => "col-sm-22",'items' => ":btnMkn"],
                                    ],
                                ],
                            ],
                        ],
                        ['class' => "form-group col-md-24",
                            'items' => [
                                ['class' => "col-sm-24"],['class' => "col-sm-24"], ['class' => "col-sm-24"], ['class' => "col-sm-24"],
                                ['class' => "form-group col-md-24",
                                    'items' => [
                                        ['class' => "col-sm-9"],
                                        ['class' => "col-sm-15",'items' => ":btnSudahDatang"],
                                    ],
                                ],
                            ],
                        ],
                    ]
                ],

			]
		],
        [ 'class' => "row-no-gutters",
            'items' => [
                [ 'class' => "form-group col-md-16",
                    'items' => [
                        [ 'class' => "col-sm-6", 'items' => ":btnPglNoUrut"],
                        [ 'class' => "col-sm-1"],
                        [ 'class' => "col-sm-6", 'items' => ":btnPglNoPol"],
                        [ 'class' => "col-sm-1"],
                        [ 'class' => "col-sm-6", 'items' => ":btnPglNama"],
                    ],
                ],
                [ 'class' => "col-md-8",
                    'items' => [
                        [ 'class' => "pull-right", 'items' => ":btnClose" ]
                    ]
                ]
            ]
        ],
		[
			/* label */
			":LabelPanggil"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Panggil</label>',
			":LabelSDNo"            => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor SD</label>',
			":LabelNoUrut"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Urut</label>',
			":LabelNoPol"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Polisi</label>',
			":LabelNama"            => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama</label>',
			":LabelKelurahan"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kelurahan</label>',
			":LabelKecamatan"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kecamatan</label>',
			":LabelMotor"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">Motor</label>',
			":LabelTujuan"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tujuan</label>',
			":LabelJmlAntrian"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jumlah Antrian</label>',
			":LabelKsr"             => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kasir</label>',
			":LabelNoKasir"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor</label>',
			":LabelKasirNoUrut"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Urut</label>',
			":LabelKasirNoPol"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Polisi</label>',
			":LabelKasirNama"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama</label>',
			":LabelMkn"             => '<label class="control-label" style="margin: 0; padding: 6px 0;">Mekasik SA</label>',
			":LabelNoMekanik"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor</label>',
			":LabelMekanikNoUrut"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Urut</label>',
			":LabelMekanikNoPol"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Polisi</label>',
			":LabelMekanikNama"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama</label>',


			":SDNo"         => FormField::combo( '',[ 'options' =>  [ 'id' => 'SDNo' ], 'extraOptions' => ['simpleCombo' => true]] ),
			":NoUrut"       => FormField::combo( '',[ 'options' =>  [ 'id' => 'SDNoUrut' ], 'extraOptions' => ['simpleCombo' => true]] ),
			":NoPol"        => FormField::combo( '',[ 'options' =>  [ 'id' => 'MotorNoPolisi' ], 'extraOptions' => ['simpleCombo' => true]] ),
			":Nama"         => FormField::combo( '',[ 'options' =>  [ 'id' => 'SDPembawaMotor' ], 'extraOptions' => ['simpleCombo' => true]] ),
			":Kelurahan"    => FormField::combo( '',[ 'options' =>  [ 'id' => 'CusKelurahan' ], 'extraOptions' => ['simpleCombo' => true]] ),
			":Kecamatan"    => FormField::combo( '',[ 'options' =>  [ 'id' => 'CusKecamatan' ], 'extraOptions' => ['simpleCombo' => true]] ),
			":Motor"        => FormField::combo( '',[ 'options' =>  [ 'id' => 'MotorNama' ], 'extraOptions' => ['simpleCombo' => true]] ),
			":Tujuan"       => '<select id="cboTujuan" class="form-control" name="state"><option value="Kasir-Bengkel">Kasir-Bengkel</option><option value="Mekanik S A">Mekanik S A</option></select>',

			":JmlAntrian"   => '<input type="number" id="JmlAntrian" class="form-control" readonly>',
			":NoKasir"      => '<input type="text" id="txtKasirSDNo" value="--" class="form-control"  readonly>',
			":KasirNoUrut"  => '<input type="text" id="txtKasirSDNoUrut" value="--" class="form-control"  readonly>',
			":KasirNoPol"   => '<input type="text" id="txtKasirMotorNoPolisi" value="--" class="form-control"  readonly>',
			":KasirNama"    => '<input type="text" id="txtKasirSDPembawaMotor" value="--" class="form-control"  readonly>',
			":NoMekanik"    => '<input type="text" id="txtMekanikSDNo" value="--" class="form-control"  readonly>',
			":MekanikNoUrut"=> '<input type="text" id="txtMekanikSDNoUrut" value="--" class="form-control"  readonly>',
			":MekanikNoPol" => '<input type="text" id="txtMekanikMotorNoPolisi" value="--" class="form-control"  readonly>',
			":MekanikNama"  => '<input type="text" id="txtMekanikSDPembawaMotor" value="--" class="form-control"  readonly>',

			":btnKasir"          => Html::button( '<i class="glyphicon glyphicon-ok-sign fa-lg"></i> &nbsp; &nbsp; Selesai', [ 'id' => 'btnSelesaiKasir', 'class' => 'btn btn-info', 'style' => 'width:306px;font-size:14px'] ),
            ":btnMkn"            => Html::button( '<i class="glyphicon glyphicon-ok-sign fa-lg"></i> &nbsp; &nbsp; Selesai', [ 'id' => 'btnSelesaiMekanik', 'class' => 'btn btn-info', 'style' => 'width:306px;font-size:14px'] ),
            ":btnSudahDatang"    => Html::button( '<i class="fas fa-user-friends fa-2x"></i><br>OK, Sudah Datang ', [ 'id' => 'btnDatang', 'class' => 'btn btn-info', 'style' => 'width:180px;height:90px;font-size:18px'] ),
            ":btnPglNoUrut"      => Html::button( '<i class="fa fa-sort-numeric-desc fa-2x"> &nbsp</i> </i> <i class="fas fa-volume-up fa-2x"> </i><br> Panggil No Urut', [ 'id' => 'btnNoUrut', 'class' => 'btn btn-info', 'style' => 'width:180px;height:90px;font-size:18px'] ),
            ":btnPglNoPol"       => Html::button( '<i class="fa fa-user fa-2x"> &nbsp</i> </i> <i class="fas fa-volume-up fa-2x"> </i><br> Panggil No Polisi', [ 'id' => 'btnNoPolisi', 'class' => 'btn btn-info', 'style' => 'width:180px;height:90px;font-size:18px'] ),
            ":btnPglNama"        => Html::button( '<i class="glyphicon glyphicon-user fa-2x"> </i> <i class="fas fa-volume-up fa-2x"> </i><br> Panggil Nama', [ 'id' => 'btnNama', 'class' => 'btn btn-info', 'style' => 'width:180px;height:90px;font-size:18px'] ),
            ":btnClose"          => Html::button( '<i class="fas fa-window-close fa-2x"> </i><br> Keluar', [ 'id' => 'btnKeluar', 'class' => 'btn btn-info', 'style' => 'width:140px;height:90px;font-size:18px'] ),
		]
	);
	ActiveForm::end();
	?>
</div>

<?php
$urlLoadData = Url::toRoute( [ 'ttsdhd/panggil-antrian-load-data' ] );
$urlMulaiPanggil = Url::toRoute( [ 'ttsdhd/panggil-antrian-mulai-panggil' ] );
$urlPanggilSudah = Url::toRoute( [ 'ttsdhd/panggil-antrian-panggil-sudah' ] );
$urlDatang = Url::toRoute( [ 'ttsdhd/panggil-antrian-datang' ] );
$urlSelesaiKasir = Url::toRoute( [ 'ttsdhd/panggil-antrian-selesai-kasir' ] );
$urlSelesaiMekanik = Url::toRoute( [ 'ttsdhd/panggil-antrian-selesai-mekanik' ] );

$this->registerJsVar('urlAudio', Url::to('@web/sound/'));
$this->registerJsVar('urlAudioAlphanumeric', Url::to('@web/sound/alphanumeric/'));
$this->registerJs(<<< JS
    $('#cboTujuan').select2();

    var firstTime = 1, isLoading = 0, setOffAjaxSpinner = 0, isSelecting = 0, selectedSDNo = '';
    function fillCombo(data) {
        if(isSelecting) return;
        
        $('#SDNo').utilSelect2().loadData(data.SDNo, true);
        $('#SDNoUrut').utilSelect2().loadData(data.SDNoUrut, true);
        $('#MotorNoPolisi').utilSelect2().loadData(data.MotorNoPolisi, true);
        $('#SDPembawaMotor').utilSelect2().loadData(data.SDPembawaMotor, true);
        $('#CusKelurahan').utilSelect2().loadData(data.CusKelurahan, true);
        $('#CusKecamatan').utilSelect2().loadData(data.CusKecamatan, true);
        $('#MotorNama').utilSelect2().loadData(data.MotorNama, true);
        
        var jmlAntrian = data.SDNo.length;
        $('#JmlAntrian').val(jmlAntrian);
        
        if(jmlAntrian) {
            if(firstTime) { // console.log('firstTime');
                $('#SDNo').prop('selectedIndex', 1).change();
                selectedSDNo = data.SDNo[0].id;
                firstTime = 0;
            }else if (selectedSDNo) {
                $('#SDNo').utilSelect2().setSelection(selectedSDNo);
            }
            // console.log(selectedSDNo);
        }
        
    }
    function loadCombo() {
        if(!setOffAjaxSpinner) {
            $(document).off( 'ajaxStart' );
            $(document).off( 'ajaxStop' );
            setOffAjaxSpinner = 1;
        }
        if(!isLoading || !isSelecting) {
            isLoading = 1;
            $.ajax({
                url: '$urlLoadData',
                type: "POST",
                data: {},
                success: function (res) {
                    // console.log(res);
                    isLoading = 0;
                    fillCombo(res);
                },
                error: function (jXHR, textStatus, errorThrown) {
                    // console.log(errorThrown);
                    isLoading = 0;
                }
            });
        }
    }
    function loadData(data) {
        let kasir = data !== undefined && data.hasOwnProperty('kasir') && data.kasir,
            mekanik = data !== undefined && data.hasOwnProperty('mekanik') && data.mekanik;
        
        $('#txtKasirSDNo').val(kasir? kasir.SDNo : "--");
        $('#txtKasirSDNoUrut').val(kasir? kasir.SDNoUrut : "--");
        $('#txtKasirMotorNoPolisi').val(kasir? kasir.MotorNoPolisi : "--");
        $('#txtKasirSDPembawaMotor').val(kasir? kasir.SDPembawaMotor : "--");
        
        $('#txtMekanikSDNo').val(mekanik? mekanik.SDNo : "--");
        $('#txtMekanikSDNoUrut').val(mekanik? mekanik.SDNoUrut : "--");
        $('#txtMekanikMotorNoPolisi').val(mekanik? mekanik.MotorNoPolisi : "--");
        $('#txtMekanikSDPembawaMotor').val(mekanik? mekanik.SDPembawaMotor : "--");
    }
    
    var timer = setInterval(function() {
        loadCombo();
    }, 1000);
    
    function select2DropDownOpen() { isSelecting = 1; }
    function select2DropDownClose() { isSelecting = 0; }
    function select2Select(e) { $('#SDNo').utilSelect2().setSelection(e.params.data.id); }
    
    $('#SDNo').bind({
        'change': function(e, o, n) { // console.log('change');
            var data = $('#SDNo').select2('data');
            if(data.length){
                selectedSDNo = $('#SDNo').select2('data')[0].id; // console.log('selectedSDNo', selectedSDNo);
                $('#SDNoUrut').utilSelect2().setSelection(selectedSDNo);
                $('#MotorNoPolisi').utilSelect2().setSelection(selectedSDNo);
                $('#SDPembawaMotor').utilSelect2().setSelection(selectedSDNo);
                $('#CusKelurahan').utilSelect2().setSelection(selectedSDNo);
                $('#CusKecamatan').utilSelect2().setSelection(selectedSDNo);
                $('#MotorNama').utilSelect2().setSelection(selectedSDNo);
            }
        }, 'select2:open': select2DropDownOpen, 'select2:close': select2DropDownClose
    })
    $('#SDNoUrut').bind({ 'select2:select': select2Select, 'select2:open': select2DropDownOpen, 'select2:close': select2DropDownClose });
    $('#MotorNoPolisi').bind({ 'select2:select': select2Select, 'select2:open': select2DropDownOpen, 'select2:close': select2DropDownClose });
    $('#SDPembawaMotor').bind({ 'select2:select': select2Select, 'select2:open': select2DropDownOpen, 'select2:close': select2DropDownClose });
    $('#CusKelurahan').bind({ 'select2:select': select2Select, 'select2:open': select2DropDownOpen, 'select2:close': select2DropDownClose });
    $('#CusKecamatan').bind({ 'select2:select': select2Select, 'select2:open': select2DropDownOpen, 'select2:close': select2DropDownClose });
    $('#MotorNama').bind({ 'select2:select': select2Select, 'select2:open': select2DropDownOpen, 'select2:close': select2DropDownClose });
    
    var audio = new Audio(), plSrc = [], plCallback; plCounter = 0;
    function playAudio(src) {
        if(src !== undefined) {
            audio.src = src;
            audio.load();
            audio.play();
        }
    }
    function panggilSudah() {
        $.ajax({
            url: '$urlPanggilSudah',
            type: "POST",
            success: function (res) {
                // console.log(res);
            },
            error: function (jXHR, textStatus, errorThrown) {
                // console.log(errorThrown);
                panggilSudah();
            }
        });
    }
    audio.onended = function(g, e) {
        plCounter++;
        if(plSrc.hasOwnProperty(plCounter)) {
            playAudio(plSrc[plCounter]);
        } else {
            if(plCallback instanceof Function)
                plCallback();
        }
    }
    function playAudioList(playList, callback) {
        if(playList !== undefined) {
            plSrc = playList;
            plCallback = callback;
            plCounter = 0;
            if(playList.hasOwnProperty(plCounter))
                playAudio(playList[plCounter]);
        }
    }
    function generatePlayList(panggilApa) {
        var playList = [ urlAudio + 'Bel.wav' ],
            tujuan = $('#cboTujuan').select2('data')[0].id;
       
        switch (panggilApa) {
            case 'NoUrut':
                playList.push(urlAudio + 'NomorUrut.wav');
                var NoUrut = $('#SDNoUrut').select2('data')[0].text;
                NoUrut = window.util.spellIndonesianNumber(parseInt(NoUrut)).split(" ");
                NoUrut.forEach(function(v) {
                    if(!window.util.isNullOrWhitespace(v))
                        playList.push(urlAudioAlphanumeric + v + '.wav');
                });
                break;
            case 'NoPolisi':
                playList.push(urlAudio + 'NomorPolisi.wav');
                var NoPolisi = $('#MotorNoPolisi').select2('data')[0].text.split("");
                NoPolisi.forEach(function(v) {
                    switch (v) {
                        case "1" : v = "Satu"; break;
                        case "2" : v = "Dua"; break;
                        case "3" : v = "Tiga"; break;
                        case "4" : v = "Empat"; break;
                        case "5" : v = "Lima"; break;
                        case "6" : v = "Enam"; break;
                        case "7" : v = "Tujuh"; break;
                        case "8" : v = "Delapan"; break;
                        case "9" : v = "Sembilan"; break;
                        case "0" : v = "Nol"; break;
                    }
                    if(!window.util.isNullOrWhitespace(v))
                        playList.push(urlAudioAlphanumeric + v + '.wav');
                });
                break;
        }
        
        playList.push(urlAudio + 'SilahkanMenujuKe.wav');
        
        if(tujuan == 'Kasir-Bengkel')
            playList.push(urlAudio + 'KasirBengkel.wav');
        else if(tujuan == 'Mekanik S A')
            playList.push(urlAudio + 'MekanikSA.wav');
        
        return playList;
    }
    function speechText(text, callback) {
        var msg = new SpeechSynthesisUtterance();
        var voices = window.speechSynthesis.getVoices();
        //msg.voice = voices[10];
        //msg.voiceURI = 'native';
        msg.volume = 1;
        msg.rate = 0.7;
        msg.pitch = 1;
        msg.text = text;
        msg.lang = 'id-ID';
        
        if(callback instanceof Function) msg.onend = callback;
        
        speechSynthesis.speak(msg);
    }
    
    $('#btnNoUrut').on('click', function() {
        var SDNo = $('#SDNo').select2('data')[0].id;
        if(SDNo == "") return;
        // window.ajaxStart();
        $.ajax({
            url: '$urlMulaiPanggil',
            type: "POST",
            data: {
                'SDNo': SDNo,
                'Tujuan': $('#cboTujuan').select2('data')[0].id,
                'PanggilApa': 'NoUrut'
            },
            success: function (res) {
                // window.ajaxStop();
                // console.log(res);
                playAudioList(generatePlayList('NoUrut'), panggilSudah);
            },
            error: function (jXHR, textStatus, errorThrown) {
                // window.ajaxStop();
                // console.log(errorThrown);
                $.notify({message: 'Panggil No Urut gagal! <br>Gagal melakukan koneksi ke server'},{type: 'danger'});
            }
        });
    })
    $('#btnNoPolisi').on('click', function() {
        var SDNo = $('#SDNo').select2('data')[0].id;
        if(SDNo == "") return;
        // window.ajaxStart();
        $.ajax({
            url: '$urlMulaiPanggil',
            type: "POST",
            data: {
                'SDNo': SDNo,
                'Tujuan': $('#cboTujuan').select2('data')[0].id,
                'PanggilApa': 'NoPolisi'
            },
            success: function (res) {
                // window.ajaxStop();
                // console.log(res);
                playAudioList(generatePlayList('NoPolisi'), panggilSudah);
            },
            error: function (jXHR, textStatus, errorThrown) {
                // window.ajaxStop();
                // console.log(errorThrown);
                $.notify({message: 'Panggil No Polisi gagal! <br>Gagal melakukan koneksi ke server'},{type: 'danger'});
            }
        });
    })
    $('#btnNama').on('click', function() {
        var SDNo = $('#SDNo').select2('data')[0].id;
        if(SDNo == "") return;
        // window.ajaxStart();
        $.ajax({
            url: '$urlMulaiPanggil',
            type: "POST",
            data: {
                'SDNo': SDNo,
                'Tujuan': $('#cboTujuan').select2('data')[0].id,
                'urlNama': $('#SDPembawaMotor').select2('data')[0].text,
                'urlKelurahan': $('#CusKelurahan').select2('data')[0].text,
                'urlKecamatan': $('#CusKecamatan').select2('data')[0].text,
                'urlTujuan': $('#cboTujuan').select2('data')[0].text.replace("-", " "),
                'PanggilApa': 'Nama'
            },
            success: function (res) {
                // window.ajaxStop();
                // console.log(res);
                playAudioList([ urlAudio + 'Bel.wav' ], function() {
                    speechText(
                        "Panggilan atas nama "+
                        $('#SDPembawaMotor').select2('data')[0].text + ", " +
                        $('#CusKelurahan').select2('data')[0].text + ", " +
                        $('#CusKecamatan').select2('data')[0].text.replace("- Kota", "") + ", " +
                        "silahkan menuju ke " +
                        $('#cboTujuan').select2('data')[0].text.replace("-", " ")
                    , panggilSudah);
                });
            },
            error: function (jXHR, textStatus, errorThrown) {
                // window.ajaxStop();
                // console.log(errorThrown);
                $.notify({message: 'Panggil Nama gagal! <br>Gagal melakukan koneksi ke server'},{type: 'danger'});
            }
        });
    })
    $('#btnDatang').on('click', function() {
        // window.ajaxStart();
        $.ajax({
            url: '$urlDatang',
            type: "POST",
            data: {
                'SDNo': $('#SDNo').select2('data')[0].id,
                'Tujuan': $('#cboTujuan').select2('data')[0].id
            },
            success: function (res) {
                // window.ajaxStop();
                // console.log(res);
                if(res.success) loadData(res)
            },
            error: function (jXHR, textStatus, errorThrown) {
                // window.ajaxStop();
                // console.log(errorThrown);
                $.notify({message: 'Proses Datang gagal! <br>Gagal melakukan koneksi ke server'},{type: 'danger'});
            }
        });
    })
    $('#btnSelesaiKasir').on('click', function() {
        // window.ajaxStart();
        $.ajax({
            url: '$urlSelesaiKasir',
            type: "POST",
            success: function (res) {
                // window.ajaxStop();
                // console.log(res);
                $('#txtKasirSDNo').val("--");
                $('#txtKasirSDNoUrut').val("--");
                $('#txtKasirMotorNoPolisi').val("--");
                $('#txtKasirSDPembawaMotor').val("--");
            },
            error: function (jXHR, textStatus, errorThrown) {
                // window.ajaxStop();
                // console.log(errorThrown);
                $.notify({message: 'Selesai Kasir gagal! <br>Gagal melakukan koneksi ke server'},{type: 'danger'});
            }
        });
    })
    $('#btnSelesaiMekanik').on('click', function() {
        // window.ajaxStart();
        $.ajax({
            url: '$urlSelesaiMekanik',
            type: "POST",
            success: function (res) {
                // window.ajaxStop();
                // console.log(res);
                $('#txtMekanikSDNo').val("--");
                $('#txtMekanikSDNoUrut').val("--");
                $('#txtMekanikMotorNoPolisi').val("--");
                $('#txtMekanikSDPembawaMotor').val("--");
            },
            error: function (jXHR, textStatus, errorThrown) {
                // window.ajaxStop();
                // console.log(errorThrown);
                $.notify({message: 'Selesai Mekanik SA gagal! <br>Gagal melakukan koneksi ke server'},{type: 'danger'});
            }
        });
    })
JS);
