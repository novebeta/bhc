<?php

\aunit\assets\AppAsset::register($this);

?>
    <div class="panel panel-default">
        <div class="panel-body">
            <table id="headerGridPL"></table><div id="headerGridPagerPL"></div>
            <table id="detailGridPL"></table><div id="detailGridPagerPL"></div>
        </div>
    </div>
<?php
$this->registerJs(<<< JS
    var headerGrid = $('#headerGridPL');
     headerGrid.utilJqGrid({
        height: 150,
        url: '{$urlHeaderGrid}',
        extraParams: {
            id: '$id'
        },
        postData: {
            id: '$id'
        },
        readOnly: true,
        rownumbers: true,
        colModel: [
            {
                name: 'SDPickingNo',
                label: 'No Picking',
                width: 100
            },
            {
                name: 'SDPickingDate',
                label: 'Tgl Picking',
                width: 100
            },
            {
                name: 'SDNo',
                label: 'No SD',
                width: 100
            },
            {
                name: 'SDTgl',
                label: 'Tgl SD',
                width: 100
            }, 
            {
                name: 'Cetak',
                label: 'Cetak',
                width: 100
            }
        ],
        onSelectRow: function(rowId) {
            console.log(rowId);
              $('#detailGridPL').utilJqGrid().load({ param: { id: rowId } });
             parent.window.util.app.dialogPrint.selectedItem = { id: rowId, data: headerGrid.getRowData(rowId) };
        }
     }).init().fit($('.panel-body')).load({url: '{$urlHeaderGrid}'});
     
    var detailGrid = $('#detailGridPL');
     detailGrid.utilJqGrid({
        url: '{$urlDetailGrid}',
        height: 170,
        readOnly: true,
        rownumbers: true,       
        colModel: [
            {
                name: 'BrgKode',
                label: 'BrgKode',
                width: 115
            },
            {
                name: 'BrgNama',
                label: 'BrgNama',
                width: 222
            },
            {
                name: 'BrgGroup',
                label: 'Group',
                width: 100
            },
            {
                name: 'SDQty',
                label: 'Qty.',
                template: 'number',
                width: 60
            }, 
            {
                name: 'BrgSatuan',
                label: 'Satuan',
                width: 80
            },
            {
                name: 'BrgRak1',
                label: 'Bin',
                width: 110
            },
            {
                name: 'LokasiKode',
                label: 'Lokasi',
                width: 110
            },
            {
                name: 'SaldoQty',
                label: 'Saldo',
                template: 'number',
                width: 110
            }
        ]  
     }).init().fit($('.panel-body'));
JS
);
