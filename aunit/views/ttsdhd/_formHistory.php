<?php
use aunit\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <table id="detailGrid"></table>
            <div id="detailGridPager"></div>
        </div>
    </div>
<?php
$urlDetail = Url::toRoute( [ 'ttsdhd/history-otomatis-item', 'MotorNoPolisi' => $data[ 'MotorNoPolisi' ] ] );
$this->registerJs( <<< JS
     $('#detailGrid').utilJqGrid({
        editurl: '{$urlDetail}',
        height: 360,
        rownumbers: true,  
        navButtonTambah: false,
        colModel: [
            {
                name: 'SDNo',
                label: 'No Daftar',
                editable: true,
                width: 120,
            },            
             {
                name: 'SVNo',
                label: 'No Invoice',
                width: 120,
                editable: true,
            },
            {
                name: 'SVTgl',
                label: 'Tgl Invoice',
                width: 120,
                editable: true,
            },
            {
                name: 'JasaNama',
                label: 'Jasa/Barang',
                width: 285,
                editable: true,
            },
            {
                name: 'SDHrgJual',
                label: 'Biaya',
                width: 70,
                editable: true,
                template: 'number'
            },                     
        ], 
        listeners: {
            afterLoad: function (data) {
             
            }    
        },  
     }).init().fit($('.box-body')).load({url: '{$urlDetail}'});        
JS
);