<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttsdhd */

$params                          = '&id=' . $id . '&action=update&jenis='.$Jenis;
if($Jenis === 'SD') { /* SD */
	$actionPrefix = 'daftar-servis';
	$title        = 'Surat Perintah Kerja Bengkel';
	$print          = Custom::url( \Yii::$app->controller->id . '/daftar-servis-print' . $params );
} else { /* SV */
	$actionPrefix = 'invoice-servis';
	$title        = 'Invoice Service';
	$print          = Custom::url( \Yii::$app->controller->id . '/invoice-servis-print' . $params );
}
$cancel                          = Custom::url(\Yii::$app->controller->id.'/'.$actionPrefix.'-cancel'.$params );
$this->title                     = "Edit - $title : " . $dsTServis['SDNoView'];
$this->params[ 'breadcrumbs' ][] = [ 'label' => $title, 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = 'Edit';

?>
<div class="ttsdhd-update">
    <?= $this->render('_form', [
        'model'     => $model,
        'dsTServis' => $dsTServis,
        'id'        => $id,
        'Jenis'     => $Jenis,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . "/$actionPrefix-update" . $params ),
            'print'  => $print,
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttsditbarang/index','action' => 'update'] ),
        ]
    ]) ?>

</div>
