<?php
use aunit\assets\AppAsset;
use aunit\models\Ttsdhd;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel aunit\models\TtsdhdSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '[SV] Invoice Servis';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'SVNo'              => 'No SV',
		'SDNo'              => 'No SD',
		'SENo'              => 'No SE',
		'NoGLSD'            => 'No GL SD',
		'NoGLSV'            => 'No GL SV',
		'KodeTrans'         => 'Kode Trans',
		'ASS'               => 'ASS',
		'PosKode'           => 'Pos Kode',
		'KarKode'           => 'Kar Kode',
		'PrgNama'           => 'Prg Nama',
		'PKBNo'             => 'Pkb No',
		'MotorNoPolisi'     => 'Motor No Polisi',
		'LokasiKode'        => 'Lokasi Kode',
		'SDNoUrut'          => 'No Urut',
		'SDJenis'           => 'Jenis',
		'SDStatus'          => 'Status',
		'SDKeluhan'         => 'Keluhan',
		'SDKeterangan'      => 'Keterangan',
		'SDPembawaMotor'    => 'Pembawa Motor',
		'SDHubunganPembawa' => 'Hubungan Pembawa',
		'SDAlasanSErvis'    => 'Alasan SErvis',
		'KlaimKPB'          => 'Klaim Kpb',
		'KlaimC2'           => 'Klaim C2',
		'Cetak'             => 'Cetak',
		'UserID'            => 'User ID',
	],
	'cmbTgl'    => [
		'SVTGL'           => 'Tgl SV ',
		'SDTGL'           => 'Tgl SD',
		'SDJamMasuk'      => 'Jam Masuk',
		'SDJamSelesai'    => 'Jam Selesai',
		'SDJamDikerjakan' => 'Jam Dikerjakan',
	],
	'cmbNum'    => [
		'SDTotalBiaya'      => 'Total Biaya',
		'SDTerbayar'        => 'Terbayar',
		'SDTotalJasa'       => 'Total Jasa',
		'SDTotalPart'       => 'Total Part',
		'SDDiscFinal'       => 'Disc Final',
		'SDTotalQty'        => 'Total Qty',
		'SDUangMuka'        => 'Uang Muka',
		'SDKasKeluar'       => 'Kas Keluar',
		'SDDurasi'          => 'Durasi',
		'SDTotalWaktu'      => 'Total Waktu',
		'SDTOP'             => 'TOP',
		'SDKmSekarang'      => 'Km Sekarang',
		'SDKmBerikut'       => 'Km Berikut',
	],
	'sortname'  => "SVTgl",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttsdhd::className(), '[SV] Invoice Servis',[
    'mode'    => isset( $mode ) ? $mode : '',
    'colGrid' => isset( $colGrid ) ? $colGrid : Ttsdhd::colGridInvoiceServisSelect()
] ), \yii\web\View::POS_READY );
