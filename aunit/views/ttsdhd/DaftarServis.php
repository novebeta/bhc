<?php
use aunit\assets\AppAsset;
use aunit\models\Ttsdhd;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '[SD] Daftar Servis';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'SDNo'              => 'No SD',
		'SVNo'              => 'No SV',
		'SENo'              => 'No SE',
		'NoGLSD'            => 'No GL SD',
		'NoGLSV'            => 'No GL SV',
		'KodeTrans'         => 'Kode Trans',
		'ASS'               => 'ASS',
		'PosKode'           => 'Pos Kode',
		'KarKode'           => 'Kar Kode',
		'PrgNama'           => 'Prg Nama',
		'PKBNo'             => 'Pkb No',
        'ttsdhd.MotorNoPolisi'     => 'Motor No Polisi',
		'LokasiKode'        => 'Lokasi Kode',
		'SDNoUrut'          => 'No Urut',
		'SDJenis'           => 'Jenis',
		'SDStatus'          => 'Status',
		'SDKeluhan'         => 'Keluhan',
		'SDKeterangan'      => 'Keterangan',
		'SDPembawaMotor'    => 'Pembawa Motor',
		'SDHubunganPembawa' => 'Hubungan Pembawa',
		'SDAlasanSErvis'    => 'Alasan SErvis',
		'KlaimKPB'          => 'Klaim Kpb',
		'KlaimC2'           => 'Klaim C2',
		'Cetak'             => 'Cetak',
		'UserID'            => 'User ID',
	],
	'cmbTgl'    => [
		'SDTGL'           => 'Tgl SD',
		'SVTGL'           => 'Tgl SV ',
		'SDJamMasuk'      => 'Jam Masuk',
		'SDJamSelesai'    => 'Jam Selesai',
		'SDJamDikerjakan' => 'Jam Dikerjakan',
	],
	'cmbNum'    => [
		'SDTotalBiaya'      => 'Total Biaya',
		'SDTerbayar'        => 'Terbayar',
		'SDTotalJasa'       => 'Total Jasa',
		'SDTotalPart'       => 'Total Part',
		'SDDiscFinal'       => 'Disc Final',
		'SDTotalQty'        => 'Total Qty',
		'SDUangMuka'        => 'Uang Muka',
		'SDKasKeluar'       => 'Kas Keluar',
		'SDDurasi'          => 'Durasi',
		'SDTotalWaktu'      => 'Total Waktu',
		'SDTOP'             => 'TOP',
		'SDKmSekarang'      => 'Km Sekarang',
		'SDKmBerikut'       => 'Km Berikut',
	],
	'sortname'  => "SDTgl",
	'sortorder' => 'desc'
];
if(\aunit\components\Menu::showOnlyHakAkses(['warehouse','gudang'])){
	$arr['cmbTxt']['PLCetak'] = 'PLCetak';
}
$this->registerJsVar( 'setcmb', $arr );
AppAsset::register( $this );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttsdhd::class, '[SD] Daftar Servis',    [
	'colGrid'         => Ttsdhd::colGridDaftarServis(),
    'url_add'         => Url::toRoute( [ 'ttsdhd/daftar-servis-create','action' =>'create' ] ),
    'url_update'      => Url::toRoute( [ 'ttsdhd/daftar-servis-update','action' =>'update' ] ),
    'url_view'        => Url::toRoute( [ 'ttsdhd/daftar-servis-update','action' =>'view' ] ),
     'url_delete'     => Url::toRoute( [ 'ttsdhd/delete' ] ),
] ), \yii\web\View::POS_READY );
