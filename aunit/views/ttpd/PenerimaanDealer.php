<?php
use aunit\assets\AppAsset;
use aunit\models\Ttpd;
use aunit\models\Ttsd;
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Penerimaan Dealer';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
    'cmbTxt'    => [
        'PDNo'       => 'No Terima Barang',
        'SDNo'       => 'No Surat Mutasi',
        'LokasiKode' => 'Kode Lokasi / POS ',
        'DealerKode' => 'Kode Dealer',
        'PDMemo'     => 'Keterangan',
        'NoGL'       => 'No GL',
        'UserID'     => 'User ID',
    ],
    'cmbTgl'    => [
        'PDTgl' => 'Pd Tgl',
    ],
    'cmbNum'    => [
        'PDTotal' => 'Pd Total',
    ],
    'sortname'  => "PDTgl",
    'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttpd::className(),
	'Penerimaan dari Dealer', [
		'colGrid' => Ttpd::colGridPenerimaanDealer(),
        'url_add'    => Url::toRoute( ['ttpd/create','jenis'=>'MutasiEksternal','action' => 'create' ]),
        'url_update'    => Url::toRoute([ 'ttpd/update','jenis'=>'MutasiEksternal', 'action' => 'update']),
        'url_delete'    => Url::toRoute( [ 'ttpd/delete'] ),
	] ), \yii\web\View::POS_READY );