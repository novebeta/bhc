<?php
use aunit\components\FormField;
use aunit\models\Tdlokasi;
use common\components\Custom;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttss */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function formatttpd(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-10" style="text-align: left">' + result.id + '</div>' +
           '<div class="col-xs-14">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;

$DataLokasi = ArrayHelper::map( Tdlokasi::find()->select2( 'LokasiKode', ['LokasiNama'], "LokasiStatus,  LokasiKode",
	[ "condition" => "LokasiStatus <> 'N'", 'params'=>[]], false ), "value", 'label' );
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
$isMutasi        = ($jenis == 'MutasiEksternal');
?>
<div class="ttpd-form">
	<?php
	$form = ActiveForm::begin( ['id' => 'form_ttpd_id', 'action' => $url[ 'update' ] ] );
	\aunit\components\TUi::form(
		[
			'class' => "row-no-gutters",
			'items' => [
				[ 'class' => "form-group col-md-24",
				  'items' => [
					  [ 'class' => "col-sm-2", 'items' => ":LabelPDNo" ],
					  [ 'class' => "col-sm-5", 'items' => ":PDNo" ],
					  [ 'class' => "col-sm-2", 'items' => ":PDTgl" ],
					  [ 'class' => "col-sm-3", 'items' => ":PDJam" ],
					  [ 'class' => "col-sm-2" ],
					  [ 'class' => "col-sm-2", 'items' => ":LabelDealerKode" ],
					  [ 'class' => "col-sm-8", 'items' => ":DealerKode" ],
				  ],
				],
				[ 'class' => "form-group col-md-24",
				  'items' => [
					  [ 'class' => "col-sm-2", 'items' => ":LabelSDNo" ],
					  [ 'class' => "col-sm-5", 'items' => ":SDNo" ],
					  [ 'class' => "col-sm-1", 'items' => ":BtnSearchFB" ],
					  [ 'class' => "col-sm-6" ],
					  [ 'class' => "col-sm-2", 'items' => ":LabelLokasiKode" ],
					  [ 'class' => "col-sm-8", 'items' => ":LokasiKode" ],
				  ],
				],
				[ 'class' => "row col-md-24", 'style' => "margin-bottom: 3px;", 'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>' ],
				[ 'class' => "col-md-24",
				  'items' => [
					  [ 'class' => "form-group col-md-2", 'items' => ":LabelPDMemo" ],
					  [ 'class' => "form-group col-md-10", 'items' => ":PDMemo" ],
					  [ 'class' => "form-group col-md-1" ],
					  [ 'class' => "form-group col-md-2", 'items' => ":LabelJmlUnit" ],
					  [ 'class' => "form-group col-md-2", 'items' => ":JmlUnit" ],
					  [ 'class' => "form-group col-md-2" ],
					  [ 'class' => "form-group col-md-1", 'items' => ":LabelPDTotal" ],
					  [ 'class' => "form-group col-md-4", 'items' => ":PDTotal" ],
				  ]
				],
			],
		],
		[
			'class' => "row-no-gutters",
			'items' => [
                ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                    'items'  => [
                        ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":NoGL"],
                        ['class' => "col-sm-4 pull-right", 'items' => ":LabelNoGL"],
                    ],
                ],
                [
                    'class' => "col-md-12 pull-left",
                    'items' => $this->render( '../_nav', [
                        'url'=> $_GET['r'].'?'.$_GET['jenis'],
                        'options'=> [
                            'PDNo'       => 'No Terima Barang',
                            'SDNo'       => 'No Surat Mutasi',
                            'LokasiKode' => 'Kode Lokasi / POS ',
                            'DealerKode' => 'Kode Dealer',
                            'PDMemo'     => 'Keterangan',
                            'NoGL'       => 'No GL',
                            'UserID'     => 'User ID',
                        ],
                    ])
                ],
                ['class' => "col-md-12 pull-right",
					'items' => [
						[
							'class' => "pull-right",
							'items' => ":btnJurnal :btnPrint :btnAction"
						]
					]
				]
			]
		],
		[
			":LabelPDNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Terima</label>',
			":LabelDealerKode" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Dealer Asal</label>',
			":LabelSDNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Surat Jalan</label>',
			":LabelLokasiKode" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Lokasi Tujuan</label>',
			":LabelPDMemo"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
			":LabelJmlUnit"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jumlah Unit</label>',
			":LabelNoGL"       => \common\components\General::labelGL( $dsBeli[ 'NoGL' ] ),

			":LabelPDTotal"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total</label>',
			":PDNo"       => Html::textInput( 'PDNoView', $dsBeli[ 'PDNoView' ], [ 'class' => 'form-control', 'readOnly'=>true, 'tabindex' => '1' ] ) .
			                 Html::textInput( 'PDNo', $dsBeli[ 'PDNo' ], [ 'class' => 'hidden' ] ) .
			                 Html::textInput( 'detil', '', [ 'class' => 'hidden' ] ).
                             Html::hiddenInput( 'StatPrint', null, [ 'id' => 'StatPrint' ] ),
			":SDNo"       => Html::textInput( 'SDNo', $dsBeli[ 'SDNo' ], [ 'class' => 'form-control','maxlength'=>10, 'tabindex' => '4' ] ),
			":PDTgl"      => FormField::dateInput( [ 'name' => 'PDTgl', 'config' => [ 'value' => $dsBeli[ 'PDTgl' ] ] ] ),
			":PDJam"      => FormField::timeInput( [ 'name' => 'PDJam', 'config' => [ 'value' => $dsBeli[ 'PDJam' ] ] ] ),
			":DealerKode" => FormField::combo( 'DealerKode', [ 'config' => ['id'=>'DealerKode', 'value' => $dsBeli['DealerKode'] ], 'options' => ['tabindex'=>'3'], 'extraOptions' => [ 'altLabel' => [ 'DealerNama' ] ] ] ),
			":LokasiKode" => FormField::combo( 'LokasiKode', [ 'config' => [ 'id'=>'LokasiKode','value' => $dsBeli['LokasiKode'],'data'=>$DataLokasi ], 'options' => ['tabindex'=>'6'] ] ),
			":JmlUnit"    => FormField::integerInput( [ 'name' => 'JmlUnit', 'config' => [ 'value' => 0, 'disabled' => true ] ] ),
			":PDMemo"     => Html::textInput( 'PDMemo', $dsBeli[ 'PDMemo' ], [ 'class' => 'form-control', 'tabindex' => '7' ] ),
			":NoGL"       => Html::textInput( 'NoGL', $dsBeli[ 'NoGL' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":PDTotal"    => FormField::decimalInput( [ 'name' => 'PDTotal', 'config' => [ 'id'=> 'PDTotal', 'value' => $dsBeli[ 'PDTotal' ] ] ] ),
			":BtnSearchFB" => '<button type="button" class="btn btn-default" id="BtnSearchFB" tabindex="5"><i class="glyphicon glyphicon-search"></i></button>',

            ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
            ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
            ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
            ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
            ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
            ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
            ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),
		],
        [
            'url_main'      => 'ttpd',
            'url_delete'    => Url::toRoute(['ttpd/delete', 'id' => $_GET['id'] ?? '', 'jenis' => $jenis] ),
        ]
	);
	ActiveForm::end();
	?>
</div>
<?php
$urlAdd        = Url::toRoute( [ 'ttpd/create','action'=>'create' ] );
$urlIndex       = Url::toRoute(['ttpd/'.( $isMutasi ? 'penerimaan-dealer' : 'invoice-penerimaan' )]);

$this->registerJsVar( '__Jenis', $jenis );
$PDNo = $dsBeli[ 'PDNo' ];
$urlPrint      = $url[ 'print' ];
$urlDatKonSelect = Url::toRoute(['ttsd/mutasi-eksternal-dealer-select','jenis'=>'MutasiEksternal']);
$urlDetailMutasiEksternal = Url::toRoute(['ttsd/detail']);
$urlDetail = $url[ 'detail' ];
$url[ 'jurnal' ] = \yii\helpers\Url::toRoute(['ttpd/jurnal','jenis'=>$jenis]);
$hideHarga = ( $jenis == 'MutasiEksternal' ? 'true' : 'false' );
$this->registerJsVar( 'tmotortype', \aunit\models\Tdmotortype::find()->all() );
$urlMotorWarna = \common\components\Custom::url( 'tdmotorwarna/find' );
$dataMotorType = json_encode( \aunit\models\Tdmotortype::find()->comboSelect2() );
$urlLoop = Url::toRoute(['ttpd/detail']);
$readOnly = ( $_GET['action'] == 'view' ? 'true' : 'false' );
$this->registerJsVar( '__model', $dsBeli );
$this->registerJsVar( '__urlFB', $urlDatKonSelect );
$this->registerJs( <<< JS

    window.getUrlFB = function(){
        let SDNo = $('input[name="SDNo"]').val();
        let DealerKode = $('#DealerKode').val();
        let LokasiKode = $('#LokasiKode').val();
        return __urlFB + '&DealerKode=' + DealerKode  + '&LokasiKode=' + LokasiKode +
        ((SDNo === '' || SDNo === '--') ? '':'&check=off&cmbTxt1=SDNo&txt1='+SDNo);
    }

     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 295,
        extraParams: {
            PDNo: $('input[name="PDNo"]').val()
        },
        navButtonTambah: $hideHarga,
        navButtonHapus: $hideHarga,
        rownumbers: true,    
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,                
        colModel: [
            { template: 'actions', hidden: $readOnly  },
            {
                name: 'MotorType',
                label: 'Type',
                editable: true,
                width: 223,
                editor: {
                     type: $hideHarga ? 'select2' : 'text',
                    readOnly: !$hideHarga,      
                    data: $dataMotorType,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    $('[id="'+$(this).attr('rowid')+'_'+'MotorWarna'+'"]')
                            .utilSelect2()
                            .loadRemote('$urlMotorWarna', { mode: 'combo', value: 'MotorWarna', label: ['MotorWarna'], condition: 'MotorType = :MotorType', params:{ ':MotorType': this.value},allOption:false }, true);   
						    $('[id="'+$(this).attr('rowid')+'_'+'MotorTahun'+'"]').val(new Date().getFullYear());
						    $('[id="'+$(this).attr('rowid')+'_'+'MotorNoMesin'+'"]').val(data.MotorNoMesin);
                            $('[id="'+$(this).attr('rowid')+'_'+'MotorNoRangka'+'"]').val(data.MotorNoRangka);
                            $('[id="'+$(this).attr('rowid')+'_'+'SSHarga'+'"]').val(data.MotorHrgBeli);
                            $('[id="'+$(this).attr('rowid')+'_'+'MotorNama'+'"]').val(data.MotorNama);
						}
                    },
                }
            },
            {
                name: 'MotorTahun',
                label: 'Tahun',
                width: 60,                
                editoptions:{
                    readOnly: !$hideHarga    
                },
                editable: true
            },
            {
                name: 'MotorNama',
                label: 'MotorNama',
                width: 250,
                hidden: !$hideHarga,
                editoptions:{
                    readOnly: true    
                },
                editable: true
            },
            {
                name: 'MotorWarna',
                label: 'Warna',
                editable: true,
                width: 145,
                editor: {
                     type: $hideHarga ? 'select2' : 'text',
                    readOnly: !$hideHarga,                        
                }
            },
            {
                name: 'MotorNoMesin',
                label: 'NoMesin',
                width: 120,
                editable: true,   
                editoptions:{
                    maxlength:"12",
                    readOnly: !$hideHarga    
                },
                editrules:{
                    custom:true, 
                    custom_func: function(value) {
                        if (value.length < 12)
                           return [false,"No Mesin kurang dari 12 karakter"];
                        else 
                           return [true,""];                        
                    }
                }
            },
            {
                name: 'MotorAutoN',
                label: 'NoMesin',
                width: 160,
                hidden: true ,
                editable: true
            },
            {
                name: 'MotorNoRangka',
                label: 'NoRangka',
                width: 160,
                editable: true,              
                editoptions:{
                    maxlength:"17",
                    readOnly: !$hideHarga    
                },                
                editrules:{
                    custom:true, 
                    custom_func: function(value) {
                        if (value.length < 17)
                           return [false,"No Rangka kurang dari 17 karakter"];
                        else 
                           return [true,""];                        
                    }
                }
            },         
            {
                name: 'DealerKode',
                label: 'HakMilik',
                width: 100,
                hidden: $hideHarga ,
                editable: true
            },
            {
                name: 'SSHarga',
                label: 'SSHarga',
                width: 105,
                template: 'money',
                hidden: $hideHarga ,
                editable: true,
                editrules:{
                    required:true
                }
            },
        ],
        listeners: {
            afterLoad: function(grid, response) {
                let PDTotal = $('#detailGrid').jqGrid('getCol','SSHarga',false,'sum');
                $('#PDTotal').utilNumberControl().val(PDTotal);
                let jQty = $('#detailGrid').jqGrid('getGridParam', 'records');
                $('input[name="JmlUnit"]').utilNumberControl().val(jQty);
                 if (jQty > 0){
                    $("#LokasiKode").attr('disabled', true);
                    $("#form_ttpd_id").append("<input id='LokasiKodeHidden' name='LokasiKode' type='hidden' value='"+$("#LokasiKode").val()+"'>");
                }else{
                    $("#LokasiKode").attr('disabled', false);
                    $("#LokasiKodeHidden").remove();
                }
                // let __memo = __model.PDMemo.replace(/~( )*Jumlah( )*Unit( )*:( )*\d*/g,'');
                if($.isEmptyObject(__model.PDMemo)){
                    $('input[name="PDMemo"]').val(' ~ Jumlah Unit :  '+ $('input[name="JmlUnit"]').utilNumberControl().val());
                }else{
                    $('input[name="PDMemo"]').val(__model.PDMemo);
                }
            },   
        }
    }).init().fit($('.box-body')).load({url: '$urlDetail'});
    
    $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
          if (orgClickEvent.extraparam.oper === 'add'){
            var cmbType = $('[id="'+rowId+'_'+'MotorType'+'"]');
            var data = cmbType.select2('data')[0];
            cmbType.val(data.MotorType).trigger('change').trigger({
                type: 'select2:select',
                params: {
                    data: data
                }
            });
        }else{
            window.rowData = $('#detailGrid').utilJqGrid().getData(rowId);  
            var cmbWarna = $('[id="'+rowId+'_'+'MotorWarna'+'"]');
            cmbWarna.utilSelect2()
                .loadRemote('$urlMotorWarna', { mode: 'combo', value: 'MotorWarna', label: ['MotorWarna'], 
                condition: 'MotorType = :MotorType', params:{ ':MotorType': window.rowData.data.MotorType},allOption:false }, true,function() {
                        cmbWarna.val(window.rowData.data.MotorWarna);
                        cmbWarna.trigger('change');
                });
        }     
        let __memo = __model.PDMemo.replace(/~( )*Jumlah( )*Unit( )*:( )*\d*/g,'');
        $('input[name="PDMemo"]').val(__memo + ' ~ Jumlah Unit :  '+ $('input[name="JmlUnit"]').utilNumberControl().val());
    });
    
    $('#btnSave').click(function (event) {
        var SDNo = $('input[name="SDNo"]').val();
        if(SDNo === '' || SDNo === '--'){
            bootbox.alert({ size: "small",message: "Silahkan memasukkan Nomor Surat Jalan"});
            return;
        }
        $('input[name="detil"]').val(JSON.stringify($('#detailGrid').getRowData()));
        $('#form_ttpd_id').submit();
    });  
    
     $('#BtnSearchFB').click(function () {
         let DealerKode = $('#DealerKode').val();
        window.util.app.dialogListData.show({
                title: 'Daftar Mutasi Eksternal untuk Penerimaan Nomor : $PDNo',
                url: getUrlFB()
            },
            function (data) {
                console.log(data);
                if (!Array.isArray(data)){
                     return;
                 }
                $('input[name="SDNo"]').val(data[0].header.data.SDNo);
                $.ajax({
                    type: 'POST',
                    url: '$urlLoop',
                    data: {
                         oper: 'batch',
                         data: data,
                         header: btoa($('#form_ttpd_id').serialize())
                    }
                }).then(function (cusData) { // console.log(data);
                    window.location.href = "{$url['update']}&oper=skip-load&PDNoView={$dsBeli['PDNoView']}"; 
                });
            })
        window.util.app.dialogListData.iframe().contentWindow.DealerKode = DealerKode;      
    });
   
    function submitPrint(mode){
        // let id = (mode === 1)? '#detailGrid':'#scnDetailGrid';
        // let jQty = jQuery(id).jqGrid('getGridParam', 'records');
        // if (jQty > 0){
            $('#StatPrint').val(mode);                
            $('#form_ttpd_id').submit();            
        // }
    }
    
    $('#btnPrint').click(function (event) {
        $('#form_ttpd_id').attr('action','$urlPrint');
        $('#form_ttpd_id').attr('target','_blank');
        if(__Jenis !== 'MutasiEksternal' ){
                submitPrint(1);
                setTimeout(function() {submitPrint(2);},5000);
            }else{
                $('#form_ttpd_id').submit();
            }  
    });
    
    $('#btnJurnal').click(function (event) {	
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#form_ttpd_id').attr('action','{$url[ 'jurnal' ]}');
                    $('#form_ttpd_id').submit();                     
                }
            }
        });
    });   
    
     $('#btnCancel').click(function (event) {	      
       $('#form_ttpd_id').attr('action','{$url['cancel']}');
       $('#form_ttpd_id').submit();
    });
     
     $('#btnAdd').click(function (event) {	      
        window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });  
     
    $('[name=PDTgl]').utilDateControl().cmp().attr('tabindex', '2');
    $('[name=PDTotal]').utilNumberControl().cmp().attr('tabindex', '8');
    
    
    
JS
);
?>
