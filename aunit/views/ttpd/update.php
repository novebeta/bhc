<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttpd */
$title                           = ( $jenis == 'MutasiEksternal' ) ? 'Penerimaan Barang Dealer' : 'Invoice Penerimaan';
$cancel                          = Url::toRoute( [ \Yii::$app->controller->id . '/cancel',
                                                   'id'    => $id, 'action' => 'update',
                                                   'index' => base64_encode( ( $jenis == 'MutasiEksternal' ) ? 'ttpd/penerimaan-dealer' : 'ttpd/invoice-penerimaan' ),
                                                   'jenis' => $jenis ] );
$this->title                     = str_replace( 'Menu', '', \aunit\components\TUi::$actionMode ) . ' - ' . $title . ' : ' . $dsBeli['PDNoView'];
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Penerimaan Dari Dealer', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = 'Edit';
?>
<div class="ttpd-update">
	<?= $this->render( '_form', [
		'model'  => $model,
		'dsBeli' => $dsBeli,
		'jenis'  => $jenis,
		'id'     => $id,
		'url'    => [
			'update' => Url::toRoute( [ \Yii::$app->controller->id . '/update', 'jenis' => $jenis, 'id' => $id, 'action' => 'update' ] ),
			'print'  => Url::toRoute( [ \Yii::$app->controller->id . '/print', 'id' => $id, 'action' => 'update' ] ),
			'cancel' => $cancel,
			'detail' => Url::toRoute( [ \Yii::$app->controller->id . '/detail', 'jenis' => $jenis, 'action' => 'update' ] ),
		]
	] ) ?>
</div>
