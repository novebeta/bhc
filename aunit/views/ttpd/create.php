<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttpd */
$cancel = Url::toRoute( [ \Yii::$app->controller->id . '/cancel',
                          'id' => $id,
                          'action' => 'update',
                          'jenis' => $jenis,
                          'index'=>base64_encode(( $jenis == 'MutasiEksternal' ) ? 'ttpd/penerimaan-dealer' : 'ttpd/invoice-penerimaan')] );
$this->title                     = 'Tambah Penerimaan Barang Dealer';
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Penerimaan Dari Dealer', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="ttpd-create">
	<?= $this->render( '_form', [
		'model'  => $model,
		'dsBeli' => $dsBeli,
		'jenis'  => $jenis,
		'id'     => $id,
		'url'    => [
			'update' => Url::toRoute( [ \Yii::$app->controller->id . '/update','jenis' => $jenis, 'id' => $id, 'action' => 'update' ] ),
			'print'  => Url::toRoute( [ \Yii::$app->controller->id . '/print', 'id' => $id, 'action' => 'update' ] ),
			'cancel' => $cancel,
			'detail' => Url::toRoute( [ \Yii::$app->controller->id . '/detail', 'jenis' => $jenis, 'action' => 'update' ] ),
		]
	] ) ?>
</div>
