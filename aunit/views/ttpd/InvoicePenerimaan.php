<?php
use aunit\assets\AppAsset;
use aunit\models\Ttpd;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = 'Invoice Penerimaan';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'PDNo'       => 'No Terima Barang',
		'SDNo'       => 'No Surat Mutasi',
        'LokasiKode' => 'Kode Lokasi / POS ',
		'DealerKode' => 'Kode Dealer',
		'PDMemo'     => 'Keterangan',
		'NoGL'       => 'No GL',
		'UserID'     => 'User ID',
	],
	'cmbTgl'    => [
		'PDTgl' => 'Pd Tgl',
	],
	'cmbNum'    => [
		'PDTotal' => 'Pd Total',
	],
	'sortname'  => "PDTgl",
	'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttpd::class,
	'Invoice Penerimaan', [
		'colGrid'         => Ttpd::colGridInvoicePenerimaan(),
		'url_update'    => Url::toRoute([ 'ttpd/update','jenis'=>'InvoicePenerimaan','action' => 'update' ]),
        'url_delete'    => Url::toRoute( [ 'ttpd/delete'] ),
		'btnAdd_Disabled' => true,
		'btnDelete_Disabled' => true,
	] ), \yii\web\View::POS_READY );