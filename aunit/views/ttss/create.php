<?php

use common\components\Custom;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttss */
$params = '&id='.$id.'&action=create';
$cancel = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title = 'Tambah Penerimaan Barang Dari Supplier';
//$this->params['breadcrumbs'][] = ['label' => 'Penerimaan Barang', 'url' => $cancel];
//$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ttss-create">

    <?= $this->render('_form', [
        'model' => $model,'dsBeli' => $dsBeli,
        'url' => [
            'update'    => Custom::url(\Yii::$app->controller->id.'/update'.$params ),
            'print'     => Custom::url(\Yii::$app->controller->id.'/print'.$params ),
            'cancel'    => $cancel,
            'detail'    => Custom::url(\Yii::$app->controller->id.'/detail' )
        ]
    ]) ?>

</div>
