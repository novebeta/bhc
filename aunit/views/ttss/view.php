<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Ttss */

$this->title = $model->SSNo;
$this->params['breadcrumbs'][] = ['label' => 'Ttsses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ttss-view">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->SSNo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->SSNo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'SSNo',
            'SSTgl',
            'FBNo',
            'SupKode',
            'LokasiKode',
            'SSMemo',
            'UserID',
            'SSJam',
            'SSNoTerima',
            'DONo',
            'NoGL',
            'FBTgl',
        ],
    ]) ?>

</div>
