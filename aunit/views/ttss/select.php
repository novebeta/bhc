<?php
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
use aunit\assets\AppAsset;
use yii\helpers\Url;
AppAsset::register( $this );
$this->title                     = 'Penerimaan Barang';
$this->params[ 'breadcrumbs' ][] = $this->title;
$jenis = isset( $_GET[ 'jenis' ] ) ? $_GET[ 'jenis' ] : '';
$arr                             = [
	'cmbTxt'    => [
		'SSNo'       => 'No Surat Jalan Supplier',
		'SSNoTerima' => 'No Bukti Terima',
		'FBNo'       => 'No Faktur Beli',
		'SupKode'    => 'Kode Supplier',
		'SupNama'         => 'Nama Supplier',
		'LokasiKode' => 'Kode Warehouse',
		'LokasiNama'      => 'Nama Warehouse',
		'DONo'       => 'No DO',
		'NoGL'       => 'No GL',
		'SSMemo'     => 'Keterangan',
		'UserID'     => 'User',
	],
	'cmbTgl'    => [
		'SSTgl' => 'Tanggal SS',
		'FBTgl' => 'Tanggal FB',
	],
	'cmbNum'    => [
	],
	'sortname'  => "SSTgl desc , SSNo",
	'sortorder' => ''
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttss::className(),
	'Penerimaan Barang', [
	        'mode' => isset( $mode ) ? $mode : '',
           'url_delete' => url::toRoute(['ttss/delete'])
    ] ), \yii\web\View::POS_READY );
$urlDetail = Url::toRoute( [ \Yii::$app->controller->id . '/detail', 'jenis' => $jenis ] );
$this->registerJs( <<< JS
jQuery(function ($) {
    if($('#jqGridDetail').length) {
        $('#jqGridDetail').utilJqGrid({
            url: '$urlDetail',
            height: 240,
            readOnly: true,
            rownumbers: true,
            rowNum: 1000,
            colModel: [
                {
                    name: 'MotorType',
                    label: 'Type',
                    width: 200
                },
               	{
	                name: 'MotorTahun',
	                label: 'Tahun',
	                width: 100,
	            },
	            {
	                name: 'MotorWarna',
	                label: 'Warna',
	                width: 100,
	            },
	            {
	                name: 'MotorNoMesin',
	                label: 'NoMesin',
	                width: 160,
	            },
	            {
	                name: 'MotorAutoN',
	                label: 'NoMesin',
	                width: 160,
	                hidden: true ,
	            },
	            {
	                name: 'MotorNoRangka',
	                label: 'NoRangka',
	                width: 160,
	            },
	            {
	                name: 'FBNo',
	                label: 'No Faktur',
	                width: 127,
	            },
	            {
	                name: 'SSNo',
	                label: 'SSNo',
	                width: 127,
	                hidden: true
	            },
	            {
	                name: 'SSHarga',
	                label: 'HrgBeli',
	                width: 100,
	                template: 'money',
	            }
            ],
	        multiselect: gridFn.detail.multiSelect(),
	        onSelectRow: gridFn.detail.onSelectRow,
	        onSelectAll: gridFn.detail.onSelectAll,
	        ondblClickRow: gridFn.detail.ondblClickRow,
	        loadComplete: gridFn.detail.loadComplete,
	        listeners: {
                afterLoad: function(grid, response) {
                    $("#cb_jqGridDetail").trigger('click');
                }
            }
        }).init().setHeight(gridFn.detail.height);
    }
});
JS, \yii\web\View::POS_READY );