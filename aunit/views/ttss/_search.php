<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TtssQuery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ttss-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'SSNo') ?>

    <?= $form->field($model, 'SSTgl') ?>

    <?= $form->field($model, 'FBNo') ?>

    <?= $form->field($model, 'SupKode') ?>

    <?= $form->field($model, 'LokasiKode') ?>

    <?php // echo $form->field($model, 'SSMemo') ?>

    <?php // echo $form->field($model, 'UserID') ?>

    <?php // echo $form->field($model, 'SSJam') ?>

    <?php // echo $form->field($model, 'SSNoTerima') ?>

    <?php // echo $form->field($model, 'DONo') ?>

    <?php // echo $form->field($model, 'NoGL') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
