<?php
/* @var $this yii\web\View */

/* @var $dataProvider yii\data\ActiveDataProvider */

use aunit\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
$this->title = 'UnitInbound from Purchase Order';
$this->params['breadcrumbs'][] = $this->title;
$arr = [
    'cmbTxt' => [
        'noShippingList' => 'No Shipping List',
        'poId' => 'PO Id',
    ],
    'cmbTxt2' => [
        'noShippingList' => 'No Shipping List',
        'mainDealerId' => 'Main Dealer Id',
        'dealerId' => 'Dealer Id',
        'noInvoice' => 'No Invoice',
        'statusShippingList' => 'Status Shipping List'
    ],
    'cmbTgl' => [],
    'cmbNum' => [],
    'sortname' => "poId",
    'sortorder' => 'desc'
];
$this->registerJsVar('setcmb', $arr);
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render('../_search'); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs(<<< JS
    window.gridParam_jqGrid = {
        readOnly: true,
        rownumbers: true,
        rowNum: 10000,
        pgbuttons: false,
        pginput: false,
        viewrecords: false,
        rowList: [],
        pgtext: ""
    }
    $("#jqGrid").bind("jqGridSelectRow", function (e, rowid, orgClickEvent) {
        window.util.app.dialogListData.gridFn.selectedHeaderData = null;
        window.util.app.dialogListData.gridFn.selectedHeaderRowId = null;
        window.util.app.dialogListData.gridFn.selectedItem = {};
        window.multiSelectID = [];
    });
    $('#filter-tgl-val1').change(function() {
        let _tgl1 = new Date($(this).val());      
        // _tgl1.setDate(_tgl1.getDate() + 5);
        _tgl1.setDate(_tgl1.getDate() + 1);
	    $('#filter-tgl-val2-disp').datepicker('update', _tgl1);
	    $('#filter-tgl-val2-disp').trigger('change');
    });
JS
);

$this->registerJs(\aunit\components\TdUi::mainGridJs(\aunit\models\Ttss::class,
    'Import Penerimaan Barang', [
        'colGrid' => $colGrid,
        'url' => Url::toRoute(['ttss/import-items']),
        'mode' => isset($mode) ? $mode : '',
        'multiselect'=> 0,
    ]), \yii\web\View::POS_READY);
$urlDetail = Url::toRoute(['ttss/import-details']);
$this->registerJs(<<< JS
jQuery(function ($) {
    if($('#jqGridDetail').length) {
        $('#jqGridDetail').utilJqGrid({
            url: '$urlDetail',
            height: 150,
            readOnly: true,
            rownumbers: true,
            rowNum: 1000,
            colModel: [
               	{
	                name: 'noGoodsReceipt',
	                label: 'No Goods Receipt',
	                width: 130,
	            },
                {
                    name: 'kodeTipeUnit',
                    label: 'Type Unit',
                    width: 80
                },
                {
                    name: 'Type',
                    label: 'Type',
                    width: 160
                },
	            {
	                name: 'kodeWarna',
	                label: 'Kode Warna',
	                width: 85,
	            },
	            {
	                name: 'Warna',
	                label: 'Warna',
	                width: 160,
	            },
	            {
	                name: 'noMesin',
	                label: 'No Mesin',
	                width: 120,
	            },
	            {
	                name: 'noRangka',
	                label: 'No Rangka',
	                width: 160,
	            },
	            {
	                name: 'kuantitasTerkirim',
	                label: 'kuantitas Terkirim',
	                width: 120,
	            },
	            {
	                name: 'kuantitasDiterima',
	                label: 'Kuantitas Diterima',
	                width: 120,
	            },
	            {
	                name: 'statusRFS',
	                label: 'Status RFS',
	                width: 80,
	            },	            
	            {
	                name: 'kelengkapanUnit',
	                label: 'Kelengkapan Unit',
	                width: 120,
	            },
	            {
	                name: 'docNRFSId',
	                label: 'Doc NRFS Id',
	                width: 90,
	            },
	            {
	                name: 'createdTime',
	                label: 'Created Time',
	                width: 150,
	            },
            ],
	        multiselect: gridFn.detail.multiSelect(),
	        onSelectRow: gridFn.detail.onSelectRow,
	        onSelectAll: gridFn.detail.onSelectAll,
	        ondblClickRow: gridFn.detail.ondblClickRow,
	        loadComplete: gridFn.detail.loadComplete,
	        listeners: {
                afterLoad: function(grid, response) {
                    $("#cb_jqGridDetail").trigger('click');
                }
            }
        }).init().setHeight(gridFn.detail.height);
    }
});
JS, \yii\web\View::POS_READY);