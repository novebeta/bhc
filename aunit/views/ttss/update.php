<?php
use yii\widgets\ActiveForm;
use common\components\Custom;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttss */
$params = '&id='.$id.'&action=update';
$cancel = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title = str_replace('Menu','',\aunit\components\TUi::$actionMode).'- Penerimaan Barang: ' . $model->SSNo;
?>
<!--<div align="right" style="margin-top: -2rem">--><?php //  echo \common\components\General::labelGL($model->NoGL); ?><!--<input type="text" value="--><?//=$model->NoGL?><!--" style="width:75px;margin-left: 10px;padding: 5px;height: 20px" readonly></div>-->
<div class="ttss-update">
    <?= $this->render('_form', [
        'model' => $model,
        'dsBeli' => $dsBeli,
        'url' => [
            'update'    => Custom::url(\Yii::$app->controller->id.'/update'.$params ),
            'print'     => Custom::url(\Yii::$app->controller->id.'/print'.$params ),
            'cancel'    => $cancel,
            'detail'    => Custom::url('ttss/detail' )
        ]
    ]) ?>
</div>
