<?php
use aunit\components\FormField;
use common\components\Custom;
use common\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
\aunit\assets\AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttss */
/* @var $form yii\widgets\ActiveForm */
?>
    <div class="ttss-form">
<!--        <div align="right" style="margin-top: -3rem;position: relative" ="form-group">--><?php //  echo \common\components\General::labelGL($dsBeli['NoGL']); ?><!--<input type="text" value="--><?//=$model->NoGL?><!--" style="width:90px;margin-left: 10px;padding: 5px;height: 25px" readonly></div>-->
		<?php
$form = ActiveForm::begin(['id' => 'form_ttss_id', 'action' => $url['update'], 'fieldConfig' => ['template' => "{input}"]]);
\aunit\components\TUi::form(
    [
        'class' => "row-no-gutters",
        'items' => [
            ['class' => "form-group col-md-24", 'style' => "margin-bottom: 2px;",
                'items'  => [
                    ['class' => "col-sm-3", 'items' => ":LabelSSNo"],
                    ['class' => "col-sm-3", 'style' => "padding-right: 1px;", 'items' => ":SSNo"],
                    ['class' => "col-sm-2", 'style' => "padding-left: 1px;padding-right: 1px;", 'items' => ":SSTgl"],
                    ['class' => "col-sm-3", 'style' => "padding-right: 1px;", 'items' => ":SSJam"],
                    ['class' => "col-sm-3", 'items' => ":NoTerima"],
                    ['class' => "col-sm-1"],
                    ['class' => "col-sm-2", 'items' => ":LabelSupplier"],
                    ['class' => "col-sm-7", 'items' => ":SupKode"],
                ],
            ],
            ['class' => "form-group col-md-24", 'style' => "margin-bottom: 2px;",
                'items'  => [
                    ['class' => "col-sm-2", 'style' => "padding-left:2px", 'items' => ":LabelFBNo"],
                    ['class' => "col-sm-1"],
                    ['class' => "col-sm-3", 'style' => "padding-right: 1px;", 'items' => ":FBNo"],
                    ['class' => "col-sm-2", 'style' => "padding-left: 1px;padding-right: 1px;", 'items' => ":FBTgl"],
                    ['class' => "col-sm-1", 'items' => ":BtnSearchFB"],
                    ['class' => "col-sm-1"],
                    ['class' => "col-sm-1", 'items' => ":LabelDONo"],
                    ['class' => "col-sm-3", 'items' => ":DONo"],
                    ['class' => "col-sm-1"],
                    ['class' => "col-sm-2", 'items' => ":LabelWarehouse"],
                    ['class' => "col-sm-7", 'items' => ":LokasiKode"],
                ],
            ],
            ['class' => "col-md-24", 'style' => "margin-bottom: 3px;", 'items' => '<table id="detailGrid"></table>'],
            ['class' => "col-md-24",
                'items'  => [
                    ['class' => "form-group col-md-3", 'items' => ":LabelMemo"],
                    ['class' => "form-group col-md-15", 'items' => ":SSMemo"],
                    ['class' => "form-group col-md-1"],
                    ['class' => "form-group col-md-2", 'items' => ":LabelJmlUnit"],
                    ['class' => "form-group col-md-3", 'items' => ":JmlUnit"],
                ],
            ],
            ['class' => "col-md-24",
                'items'  => [
                    ['class' => "form-group col-md-3", 'items' => ":LabelZnoInvoice"],
                    ['class' => "form-group col-md-5", 'items' => ":ZnoInvoice"],
                    ['class' => "form-group col-md-2"],
                    ['class' => "form-group col-md-3", 'items' => ":LabelZnoShippingList"],
                    ['class' => "form-group col-md-5", 'items' => ":ZnoShippingList"],
                    ['class' => "form-group col-md-1"],
                    ['class' => "form-group col-md-2", 'items' => ":LabelZtanggalTerima"],
                    ['class' => "form-group col-md-3", 'items' => ":ZtanggalTerima"],
                ],
            ],
        ],
    ],
    ['class' => "row-no-gutters",
        'items'  => [
            ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                'items'  => [
                    ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":NoGL"],
                    ['class' => "col-sm-4 pull-right", 'items' => ":LabelNoGL"],
                ],
            ],
            [
                'class' => "col-md-12 pull-left",
                'items' => $this->render( '../_nav', [
                    'url'=> $_GET['r'],
                    'options'=> [
                        'ttss.SSNo'       => 'No Surat Jalan Supplier',
                        'ttss.SSNoTerima' => 'No Bukti Terima',
                        'ttss.FBNo'       => 'No Faktur Beli',
                        'ttss.SupKode'    => 'Kode Supplier',
                        'SupNama'         => 'Nama Supplier',
                        'ttss.LokasiKode' => 'Kode Warehouse',
                        'LokasiNama'      => 'Nama Warehouse',
                        'ttss.DONo'       => 'No DO',
                        'ttss.NoGL'       => 'No GL',
                        'ttss.SSMemo'     => 'Keterangan',
                        'ttss.UserID'     => 'User',
                    ],
                ])
            ],
            ['class' => "col-md-12 pull-right",
                'items'  => [
                    ['class' => "pull-right", 'items' => ":btnImport :btnJurnal :btnPrint :btnAction"],
                ],
            ],
        ],
    ],
    [
        ":LabelSupplier"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Supplier</label>',
        ":LabelWarehouse" => \aunit\components\FormField::label('Warehouse'),
        ":LabelDONo"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">DO</label>',
        ":LabelFBNo"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Faktur Beli</label>',
        ":LabelSSNo"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Surat Jalan</label>',
        ":LabelMemo"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
        ":LabelJmlUnit"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jumlah Unit</label>',
        ":LabelNoGL"      => \common\components\General::labelGL($dsBeli['NoGL']),
        ":SSNo"           => Html::textInput('SSNoView', $dsBeli['SSNoView'], ['id' => 'SSNoView','class' => 'form-control', 'maxlength' => 30, 'tabindex' => '1']) .
        Html::textInput('SSNo', $dsBeli['SSNo'], ['class' => 'hidden']) .
        Html::textInput('SSNoLama', $dsBeli['SSNo'], ['class' => 'hidden']) .
        Html::textInput('SaveMode', 'ALL', ['class' => 'hidden']),
        ":SSTgl"          => FormField::dateInput(['name' => 'SSTgl', 'config' => ['value' => $dsBeli['SSTgl']], 'options' => ['tabindex' => '2']]),
        ":SSJam"          => FormField::timeInput(['name' => 'SSJam', 'config' => ['value' => $dsBeli['SSJam']], 'tabindex' => '3']),
        ":NoTerima"       => Html::textInput('SSNoTerimaView', str_replace('=', '-', $dsBeli['SSNoTerima']), ['class' => 'form-control', 'readOnly' => true]) .
        Html::textInput('SSNoTerima', $dsBeli['SSNoTerima'], ['class' => 'hidden']),
        ":FBNo"           => Html::textInput('FBNo', $dsBeli['FBNo'], ['class' => 'form-control', 'readOnly' => true]),
        ":FBTgl"          => FormField::dateInput(['name' => 'FBTgl', 'config' => ['id' => 'FBTgl', 'value' => $dsBeli['FBTgl']]]),
        ":DONo"           => Html::textInput('DONo', $dsBeli['DONo'], ['class' => 'form-control', 'tabindex' => '6']),
        ":SupKode"        => FormField::combo('SupKode', ['config' => ['value' => $dsBeli['SupKode']], 'options' => ['tabindex' => '4'], 'extraOptions' => ['altLabel' => ['SupNama'], 'altCondition' => "SupStatus = '1'"]]),
        ":LokasiKode"     => FormField::combo('LokasiKode', ['config' => ['id' => 'LokasiKode', 'value' => $dsBeli['LokasiKode']], 'options' => ['tabindex' => '7'], 'extraOptions' => ['altLabel' => ['LokasiNama']]]),
        ":SSMemo"         => Html::textInput('SSMemo', $dsBeli['SSMemo'], ['class' => 'form-control', 'tabindex' => '8']),
        ":JmlUnit"        => FormField::integerInput(['name' => 'SSJum', 'config' => ['value' => $dsBeli['SSJum'], 'disabled' => true]]),
        ":NoGL"           => Html::textInput('NoGL', $dsBeli['NoGL'], ['class' => 'form-control', 'readOnly' => true]),
//                ":BtnSearchFB"    => '<button type="button" class="btn btn-default" id="BtnSearchFB"><i class="glyphicon glyphicon-search"></i></button>',
         ":BtnSearchFB"    => Html::button('<i class="glyphicon glyphicon-search"></i>', ['class' => 'btn btn-default' . Custom::viewClass(), 'id' => 'BtnSearchFB', 'tabindex' => '5']),

        ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
        ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
        ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
        ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
        ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
        ":btnImport"      => Html::button('<i class="fa fa-arrow-circle-down"><br><br>Import</i>   ', ['class' => 'btn bg-navy ', 'id' => 'btnImport', 'style' => 'width:60px;height:60px']),
        ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
        ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),

//        ":btnSave"        => Html::button('<i class="glyphicon glyphicon-floppy-disk"></i> Simpan', ['class' => 'btn btn-info btn-primary ', 'id' => 'btnSave' . Custom::viewClass()]),
//        ":btnCancel"      => Html::Button('<i class="fa fa-ban"></i> Batal', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:80px']),
//        ":btnAdd"         => Html::button('<i class="fa fa-plus-circle"></i> Tambah', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:80px']),
//        ":btnEdit"        => Html::button('<i class="fa fa-edit"></i> Edit', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:80px']),
//        ":btnDaftar"      => Html::Button('<i class="fa fa-list"></i> Daftar ', ['class' => 'btn btn-primary ', 'id' => 'btnDaftar', 'style' => 'width:80px']),

        
        ":LabelZnoShippingList" => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Shipping List</label>',
        ":LabelZnoInvoice"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Invoice</label>',
        ":LabelZtanggalTerima"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tanggal Terima</label>',
        ":ZnoShippingList"      => Html::textInput('ZnoShippingList', $dsBeli['ZnoShippingList'], ['class' => 'form-control']),
        ":ZnoInvoice"           => Html::textInput('ZnoInvoice', $dsBeli['ZnoInvoice'], ['class' => 'form-control']),
        ":ZtanggalTerima"       => Html::textInput('ZtanggalTerima', $dsBeli['ZtanggalTerima'], ['class' => 'form-control']),


    ],
    [
        '_akses' => 'Penerimaan Barang',
        'url_main' => 'ttss',
        'url_id' => $_GET['id'] ?? '',
    ]
);
ActiveForm::end();
?>
    </div>
<?php
$this->registerJsVar('tmotortype', \aunit\models\Tdmotortype::find()->all());
$SSNoView        = $dsBeli['SSNoView'];
$action        = $_GET['action'] ?? '';
$urlAdd        = Url::toRoute(['ttss/create', 'action' => 'create' ]);
$urlCount        = Url::toRoute(['ttss/count']);
//$urlImport     = Url::toRoute(['ttss/import', 'tgl1' => Yii::$app->formatter->asDate('-5 day', 'yyyy-MM-dd')]);
$urlImport     = Url::toRoute(['ttss/import', 'tgl1' => Yii::$app->formatter->asDate('-1 day', 'yyyy-MM-dd')]);
$urlDetail     = $url['detail'];
$urlPrint      = $url['print'];
$urlUpdate     = $url['update'];
$urlUpdateLoop = Url::toRoute(['ttss/update']);
$urlIndex      = Url::toRoute(['ttss/index']);
$urlJurnal     = Url::toRoute(['ttss/jurnal', 'action' => $_GET['action'] ?? '']);
$urlFB         = Url::toRoute(['ttfb/select', 'jenis' => 'SS', 'tgl1' => Yii::$app->formatter->asDate('-90 day', 'yyyy-MM-dd')]);
$readOnly      = ($action == 'view' ? 'true' : 'false');
$urlMotorWarna = \common\components\Custom::url('tdmotorwarna/find');
$dataMotorType = json_encode(\aunit\models\Tdmotortype::find()->comboSelect2());
$this->registerJsVar('__urlFB', $urlFB);
$this->registerJsVar('__model', $dsBeli);
$this->registerJs(<<< JS
    var grid = $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 250,
        datatype: 'local',
        extraParams: {
            SSNo: '$model->SSNo',
        },
        onSaveExtraParams: {
            header: btoa($('#form_ttss_id').serialize())
        },
        loadonce:true,
        rowNum: 1000,
        rownumbers: true,
        readOnly: $readOnly,
        colModel: [
            { template: 'actions' },
            
            {
                name: 'MotorType',
                label: 'Type',
                editable: true,
                width: 200,
                editor: {
                    type: 'select2',
                    data: $dataMotorType,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    $('[id="'+$(this).attr('rowid')+'_'+'MotorWarna'+'"]')
                            .utilSelect2()
                            .loadRemote('$urlMotorWarna', { mode: 'combo', value: 'MotorWarna', label: ['MotorWarna'], condition: 'MotorType = :MotorType', params:{ ':MotorType': this.value},allOption:false }, true);
						    $('[id="'+$(this).attr('rowid')+'_'+'MotorTahun'+'"]').val(new Date().getFullYear());
						    $('[id="'+$(this).attr('rowid')+'_'+'KodeAHM'+'"]').val(data.KodeAHM);
						    $('[id="'+$(this).attr('rowid')+'_'+'MotorNoMesin'+'"]').val(data.MotorNoMesin);
                            $('[id="'+$(this).attr('rowid')+'_'+'MotorNoRangka'+'"]').val(data.MotorNoRangka);
                            $('[id="'+$(this).attr('rowid')+'_'+'SSHarga'+'"]').utilNumberControl().val(data.MotorHrgBeli);
                            $('[id="'+$(this).attr('rowid')+'_'+'FBNo'+'"]').val('--');
						}
                    },
                }
            },
            {
                name: 'MotorTahun',
                label: 'Tahun',
                width: 50,
                editable: true,
                template: 'tahun',
                editoptions:{
                    maxlength: "8"
                },
            },
            {
                name: 'MotorWarna',
                label: 'Warna',
                editable: true,
                width: 120,
                editor: {
                    type: 'select2',
                    editorConfig:{
                        tags: true
                    }
                },
            },
            {
                name: 'MotorNoMesin',
                label: 'NoMesin',
                width: 110,
                editable: true,
                editoptions:{
                    maxlength:"12",
                    style: "text-transform:uppercase"
                },
                editrules:{
                    custom:true,
                    custom_func: function(value) {
                        if (value.length < 12)
                           return [false,"No Mesin kurang dari 12 karakter"];
                        else
                           return [true,""];
                    }
                }
            },
            {
                name: 'MotorAutoN',
                label: 'MotorAutoN',
                width: 160,
                hidden: true ,
                editable: true
            },
            {
                name: 'MotorNoRangka',
                label: 'NoRangka',
                width: 140,
                editable: true,
                editoptions:{
                    maxlength:"17",
                    style: "text-transform:uppercase"
                },
                editrules:{
                    custom:true,
                    custom_func: function(value) {
                        if (value.length < 17)
                           return [false,"No Rangka kurang dari 17 karakter"];
                        else
                           return [true,""];
                    }
                }
            },
            {
                name: 'FBNo',
                label: 'No Faktur',
                width: 90,
                editable: true,
                editoptions:{
                    readOnly:true,
                    maxlength:"18"
                },
            },
            {
                name: 'SSHarga',
                label: 'HrgBeli',
                width: 100,
                template: 'money',
                editable: true,
                editrules:{
                    custom:true,
                    custom_func: function(value) {
                        if (parseFloat(value) === 0)
                            return [false,"Silahkan mengisi harga."];
                        else
                           return [true,""];
                    }
                }
            },
            {
                name: 'KodeAHM',
                label: 'Kode AHM',
                width: 70,
                editable: true
            },
        ],
        listeners: {
            afterLoad: function(grid, response) {
                let jQty = $('#detailGrid').jqGrid('getGridParam', 'records');
                $('input[name="SSJum"]').utilNumberControl().val(jQty);
//                if (jQty > 0){
//                    $("#LokasiKode").attr('disabled', true);
//                    $("#form_ttss_id").append("<input id='LokasiKodeHidden' name='LokasiKode' type='hidden' value='"+$("#LokasiKode").val()+"'>");
//
//                }else{
//                    $("#LokasiKode").attr('disabled', false);
//                    $("#LokasiKodeHidden").remove();
//                }
            }
        }
    }).init().fit($('.box-body')).load({ url: '$urlDetail'});
    $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowId, orgClickEvent) {
        // console.log(orgClickEvent);
        if (orgClickEvent.extraparam.oper === 'add'){
            var cmbType = $('[id="'+rowId+'_'+'MotorType'+'"]');
            var data = cmbType.select2('data')[0];
            cmbType.val(data.MotorType).trigger('change').trigger({
                type: 'select2:select',
                params: {
                    data: data
                }
            });
        }else{
            window.rowData = $('#detailGrid').utilJqGrid().getData(rowId);
             var cmbMotorType = $('[id="'+rowId+'_'+'MotorType'+'"]');
            var cmbWarna = $('[id="'+rowId+'_'+'MotorWarna'+'"]');
            cmbWarna.utilSelect2()
                .loadRemote('$urlMotorWarna', { mode: 'combo', value: 'MotorWarna', label: ['MotorWarna'],
                condition: 'MotorType = :MotorType', params:{ ':MotorType': window.rowData.data.MotorType},allOption:false }, true,function() {
                        cmbWarna.val(window.rowData.data.MotorWarna);
                        cmbWarna.trigger('change');
                });

             if (rowData.data.FBNo !== '' && rowData.data.FBNo !== '--'){
                // cmbWarna.prop('disabled',true);
                cmbMotorType.prop('disabled',true);
            }
        }
        let __memo = __model.SSMemo.replace(/~( )*Jumlah( )*Unit( )*:( )*\d*/g,'');
        $('input[name="SSMemo"]').val(__memo + ' ~ Jumlah Unit :  '+ $('input[name="SSJum"]').utilNumberControl().val());

    });
    $('#btnPrint').click(function (event) {
        $('#form_ttss_id').attr('action','$urlPrint');
        $('#form_ttss_id').attr('target','_blank');
        $('#form_ttss_id').submit();
    });

    $('#btnJurnal').click(function (event) {
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#form_ttss_id').attr('action','{$urlJurnal}');
                    $('#form_ttss_id').submit();
                }
            }
        });
    });

    $('#btnSave').click(function (event) {
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttss_id').attr('action','{$url['update']}');
        $('#form_ttss_id').submit();
    });

    $('#btnCancel').click(function (event) {
       $('#form_ttss_id').attr('action','{$url['cancel']}');
       $('#form_ttss_id').submit();
    });

    $('#btnAdd').click(function (event) {
        window.location.href = '{$urlAdd}';
	  });

    $('#btnEdit').click(function (event) {
        window.location.href = '{$url['update']}';
	  });

    $('#btnDaftar').click(function (event) {
        window.location.href = '{$urlIndex}';
	  });
    

     $("#NewCusId").click(function () {
        bootbox.confirm("Apakah Anda ingin memasukkan Data Konsumen Baru ?", function (result) {
            if (result) {
                $('input[name="CusKode"]').val('--');
                $('input[name="CusNama"]').val('');
                $('input[name="CusAlamat"]').val('');
            }
        });
    });

     window.getUrlFB = function(){
        let FBNo = $('input[name="FBNo"]').val();
        return __urlFB + ((FBNo === '' || FBNo === '--') ? '':'&check=off&cmbTxt1=FBNo&txt1='+FBNo);
    }

    $('#BtnSearchFB').click(function() {
        window.util.app.dialogListData.show({
            title: 'Daftar Faktur Beli',
            url: getUrlFB()
        },
        function (data) {
            console.log(data);
             if (!Array.isArray(data)){
                 return;
             }
             var itemData = [];
             data.forEach(function(itm,ind) {
                 console.log(itm);
                let cmp = $('input[name="FBNo"]');
                cmp.val(itm.header.data.FBNo);
                let FBTgl = $('#FBTgl-disp');
                let dtFBTgl = window.util.date.fromStringDate(itm.header.data.FBTgl)
                FBTgl.kvDatepicker('update', dtFBTgl);
                FBTgl.trigger('changeDate');
                itm.data.SSNo = __model.SSNo;
                itm.data.SSHarga = itm.data.FBHarga;
                itemData.push(itm.data);
            	 // $('#detailGrid').jqGrid("addRow",itemData);
            });
            // console.log($('#form_ttss_id').serialize());
            $.ajax({
             type: "POST",
             url: '$urlDetail',
             data: {
                 oper: 'batch',
                 data: itemData,
                 header: btoa($('#form_ttss_id').serialize())
             },
             success: function(data) {
            //    let id = new URLSearchParams(location.search).get('id');
            //    if (data.id != id){
            //    }else{
            //        data.notif.forEach(function(itm,ind) {
            //                $.notify({message: itm.keterangan},{type: (itm.status) ? 'success' : 'warning'});
            //        });
            //        $('#detailGrid').utilJqGrid().load({ url: '$urlDetail'});
            //    }
                //     data.notif.forEach(function(itm,ind) {
                //            $.notify({message: itm.keterangan},{type: (itm.status) ? 'success' : 'warning'});
                //    });
                //    $('#detailGrid').utilJqGrid().load({ url: '$urlDetail'});
                let SSNoView = $('[name=SSNoView]').val();
                // var str = window.location.search;
                //     str = window.util.url.replaceQueryParam('id', data.id, str);
                    // window.location = window.location.pathname + str;
                window.location.href = "{$urlUpdateLoop}&id="+data.id+"&action=update&oper=skip-load&SSNoView="+SSNoView;
             },
           });
        })
    });

    $('[name=SSTgl]').utilDateControl().cmp().attr('tabindex', '2');

    $('#btnImport').click(function (event) {
        window.util.app.dialogListData.show({
            title: 'UnitInbound from Purchase Order - UINB - SS',
            url: '{$urlImport}'
        },
        function (data) {
			if(!Array.isArray(data)) return;
             $.ajax({
             type: "POST",
             url: '$urlDetail',
             data: {
                 oper: 'import',
                 data: data,
                 header: btoa($('#form_ttss_id').serialize())
             },
             success: function(data) {
                let SSNoView = $('[name=SSNoView]').val();
                window.location.href = "{$url['update']}&oper=skip-load&SSNoView="+SSNoView;
             },
           });
        });
    });

    $('#SSNoView').on('change',function (event) {
        if($(this).val() != "$SSNoView"){
            $.ajax({
                type: "POST",
                url: "$urlCount",
                data: {
                    data: $(this).val(),
                },
                success: function(data) {
                    const urlParams = new URLSearchParams(window.location.search);
                    const action = urlParams.get('action');
                    const count = parseFloat(data.message);
                    if(count > 0){
                        bootbox.alert({
                            size: 'small',
                            message : 'Duplicate entry '+$('#SSNoView').val()+' for SSNo', 
                            callback: function(){ 
                                $('#SSNoView').focus();
                            }
                        });
                    }
                },
            });
        }
    })
    
JS
);
/** @var User $user */
