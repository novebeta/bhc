<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttmteamtarget */

$this->title = 'Update Ttmteamtarget: ' . $model->TeamKode;
$this->params['breadcrumbs'][] = ['label' => 'Ttmteamtargets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->TeamKode, 'url' => ['view', 'TeamKode' => $model->TeamKode, 'TargetTgl' => $model->TargetTgl]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ttmteamtarget-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
