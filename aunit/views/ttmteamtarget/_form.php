<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttmteamtarget */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ttmteamtarget-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'TeamKode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TargetTgl')->textInput() ?>

    <?= $form->field($model, 'TargetQty')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
