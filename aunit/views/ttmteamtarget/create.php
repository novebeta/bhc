<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttmteamtarget */

$this->title = 'Create Ttmteamtarget';
$this->params['breadcrumbs'][] = ['label' => 'Ttmteamtargets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttmteamtarget-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
