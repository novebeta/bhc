<?php
use aunit\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
AppAsset::register( $this );
$this->title                   = 'Market Share';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'SKNo'       => 'No SK',
		'LokasiKode' => 'Lokasi',
		'SKPengirim' => 'Pengirim',
		'SKMemo'     => 'Keterangan',
		'NoGL'       => 'No Gl',
		'UserID'     => 'User ID',
		'SKCetak'    => 'Cetak',
		'DKNo'       => 'No DK',	],
	'cmbTgl'    => [
		'SKTgl'      => 'Tanggal SK',
	],
	'cmbNum'    => [
	],
//	'sortname'  => "AAA.SKNo",
//	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttmteamtarget::className(),
	'Market Share',[
//		'mode' => isset( $mode ) ? $mode : '',
//		'url_delete'    => Url::toRoute( [ 'ttsk/delete'] ),
	] ), \yii\web\View::POS_READY );