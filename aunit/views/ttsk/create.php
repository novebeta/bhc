<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttsk */
$params                          = '&id=' . $id . '&action=create';
$cancel                          = Custom::url( \Yii::$app->controller->id .'/cancel'.$params );
$this->title                     = 'Tambah - Surat Jalan Konsumen';
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Surat Jalan Konsumen', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="ttsk-create">
	<?= $this->render( '_form', [
	    'id'=> $id,
		'model'  => $model,
		'dsJual' => $dsJual,
        'url' => [
            'update'    => Custom::url(\Yii::$app->controller->id.'/update'.$params ),
            'print'     => Custom::url(\Yii::$app->controller->id.'/print'.$params ),
            'cancel'    => $cancel,
            'detail'    => Custom::url(\Yii::$app->controller->id.'/detail' )
        ]
	] ) ?>
</div>
