<?php
use aunit\assets\AppAsset;
use aunit\components\FormField;
use common\components\Custom;
use kartik\checkbox\CheckboxX;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\components\General;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$urlDetail                = Url::toRoute( [ 'ttsk/details' ] );
$urlSerahTerimaUpdate     = Url::toRoute( [ 'ttsk/serah-terima-kolektif-update' ] );
$urlSerahTerimaUpdateLine = Url::toRoute( [ 'ttsk/serah-terima-kolektif-update-line' ] );
$escape                   = new JsExpression( "function(m) { return m; }" );
AppAsset::register( $this );
$this->title                     = 'Serah Terima Kolektif';
$this->params[ 'breadcrumbs' ][] = $this->title;
$querySales                      = Yii::$app->db->createCommand( "SELECT SalesKode, SalesNama FROM tdSales WHERE SalesStatus = 'A' ORDER BY SalesNama, SalesKode" )->queryAll();
$dtSales                         = array_merge( [ '' => 'Semua' ], ArrayHelper::map( $querySales, 'SalesKode', 'SalesNama' ) );
//$queryLeasing                    = Yii::$app->db->createCommand( "SELECT LeaseKode, LeaseNama FROM tdLeasing WHERE LeaseStatus = 'A'  ORDER BY LeaseKode" )->queryAll();
$queryLeasing                    = Yii::$app->db->createCommand( "SELECT 'Semua' AS LeaseKode, '' AS LeaseNama
                                    UNION SELECT LeaseKode, LeaseNama FROM tdLeasing WHERE LeaseKode <> 'TUNAI' UNION SELECT LeaseKode, LeaseNama FROM tdLeasing WHERE LeaseKode = 'TUNAI'" )->queryAll();
$dtLeasing                       = array_merge( [ '' => 'Semua' ], ArrayHelper::map( $queryLeasing, 'LeaseKode', 'LeaseKode' ) );
?>
    <div class="ttsk-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_ttsk_id' ] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-3", 'items' => ":CMB1" ],
						  [ 'class' => "col-sm-2", 'items' => ":AwalTgl" ],
						  [ 'class' => "col-sm-2", 'items' => ":AkhirTgl" ],
						  [ 'class' => "col-sm-1", 'items' => ":Cek", 'style' => 'padding-left:3px;', 'id' => 'showtitle', 'title' => 'Belum' ],
						  [ 'class' => "col-sm-3", 'items' => ":CMB2" ],
						  [ 'class' => "col-sm-2", 'items' => ":CTgl1" ],
						  [ 'class' => "col-sm-2", 'items' => ":CTgl2" ],
						  [ 'class' => "col-sm-1", 'items' => ":LblCari", 'style' => 'text-align:center;' ],
						  [ 'class' => "col-sm-3", 'items' => ":Cari" ],
						  [ 'class' => "col-sm-2", 'items' => ":Both" ],
						  [ 'class' => "col-sm-3", 'items' => ":ColBoth" ],
					  ],
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-1", 'items' => ":LabelSalesKode" ],
						  [ 'class' => "col-sm-6", 'items' => ":SalesKode" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelLeaseKode" ],
						  [ 'class' => "col-sm-6", 'items' => ":LeaseKode", 'style' => 'padding-left:7px;' ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelSort", 'style' => 'text-align:center;' ],
						  [ 'class' => "col-sm-5", 'items' => ":Sort" ],
						  [ 'class' => "col-sm-3", 'items' => ":TypeSort" ],
					  ],
					],
					[
						'class' => "row col-md-24",
						'style' => "margin-bottom: 3px;",
						'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
					],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-4", 'items' => ":LabelTujuan" ],
						  [ 'class' => "col-sm-5", 'items' => ":Tujuan" ],
						  [ 'class' => "col-sm-3", 'style' => 'padding-left:3px', 'items' => ":Tgl" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelPenerima" ],
						  [ 'class' => "col-sm-4", 'items' => ":Penerima" ],
						  [ 'class' => "pull-right", 'items' => ":btnUpdate :btnPrint" ]
					  ],
					],
				]
			],
			[
                [
                    'class' => "col-md-12 pull-left",
                    'items' => $this->render( '../_nav', [
                        'url'=> $_GET['r'],
                        'options'=> [
                            'SKNo'       => 'No SK',
                            'LokasiKode' => 'Lokasi',
                            'SKPengirim' => 'Pengirim',
                            'SKMemo'     => 'Keterangan',
                            'NoGL'       => 'No Gl',
                            'UserID'     => 'User ID',
                            'SKCetak'    => 'Cetak',
                            'DKNo'       => 'No DK',
                            'CusNama'    => 'Nama Konsumen',
                            'MotorNoMesin'   => 'No Mesin',
                        ],
                    ])
                ],
			],
			[
				/* label */
				":LabelSalesKode" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sales</label>',
				":LabelLeaseKode" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Leasing</label>',
				":LabelTujuan"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tujuan yang akan diupdate</label>',
				":LabelPenerima"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Penerima</label>',
				":LblCari"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Cari</label>',
				":LabelSort"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sort</label>',
				":CMB1"           => FormField::combo( 'cboDKSKTgl', [ 'config' => [ 'id' => 'cboDKSKTgl', 'value' => 'ttsk.SKTgl', 'data' => [
					'ttsk.SKTgl' => 'Tanggal SK',
					'ttdk.DKTgl' => 'Tanggal DK',
				] ], 'extraOptions'                                             => [ 'simpleCombo' => true ] ] ),
				":AwalTgl"        => FormField::dateInput( [ 'name' => 'dtpDKSKTgl1', 'config' => [ 'id' => 'dtpDKSKTgl1', 'value' => Yii::$app->formatter->asDate( '-3 month', 'yyyy-MM-dd' ) ] ] ),
				":AkhirTgl"       => FormField::dateInput( [ 'name' => 'dtpDKSKTgl2', 'config' => [ 'id' => 'dtpDKSKTgl2', 'value' => Yii::$app->formatter->asDate( 'now', 'yyyy-MM-dd' ) ] ] ),
				":CMB2"           => FormField::combo( 'TujuanUpdate', [ 'name' => 'TyTglDoc', 'config' => [ 'id' => 'TyTglDoc' ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":Cek"            => CheckboxX::widget( [
					'name'          => 'cbxDoc',
					'options'       => [ 'id' => 'cbxDoc' ],
					'pluginOptions' => [ 'threeState' => true, 'size' => 'lg' ],
				] ),
				":CTgl1"          => FormField::dateInput( [ 'name' => 'dtpDoc1', 'config' => [ 'id' => 'dtpDoc1', 'value' => '1900-01-01' ] ] ),
				":CTgl2"          => FormField::dateInput( [ 'name' => 'dtpDoc2', 'config' => [ 'id' => 'dtpDoc2', 'value' => '1900-01-01' ] ] ),
				":Cari"           => FormField::combo( 'Cari', [ 'config' => [ 'id' => 'cboSearch', 'value' => '', 'data' => [
					'CusNama'        => 'Nama Konsumen',
					'STNKNama'       => 'Nama STNK',
					'SalesNama'      => 'Nama Sales',
					'MotorNoMesin'   => 'Nomor Mesin',
					'MotorNama'      => 'Nama Motor',
					'FakturAHMNo'    => 'No Faktur AHM',
					'STNKNo'         => 'No STNK',
					'NoticeNo'       => 'No Notice',
					'PlatNo'         => 'No Plat',
					'BPKBNo'         => 'No BPKB',
					'NoticePenerima' => 'Penerima Notice',
					'STNKPenerima'   => 'Penerima STNK',
					'PlatPenerima'   => 'Penerima Plat',
					'BPKBPenerima'   => 'Penerima BPKB',
					'DKNo'           => 'No Data Konsumen',
					'SKNo'           => 'No Surat Jln Konsumen',
					'LeaseKode'      => 'Kode Lease',
				] ], 'extraOptions'                                       => [ 'simpleCombo' => true ] ] ),
				":Both"           => FormField::combo( 'Both', [ 'config' => [ 'id' => 'cboPil', 'value' => '', 'data' => [
					'Both'    => 'Both',
					'Left'    => 'Left',
					'Right'   => 'Right',
					'Exactly' => 'Exactly',
					'Not'     => 'Not',
				] ], 'extraOptions'                                       => [ 'simpleCombo' => true ] ] ),
				":ColBoth"        => Html::textInput( 'txtSearch', '', [ 'id' => 'txtSearch', 'class' => 'form-control' ] ),
				":SalesKode"      => FormField::combo( 'SalesKode', [ 'config' => [ 'id' => 'cboSalesKode', 'value' => 'SalesNama', 'data' => $dtSales ], 'extraOptions' => [ 'altLabel' => [ 'SalesNama' ] ] ] ),
				":LeaseKode"      => FormField::combo( 'LeaseKode', [ 'config' => [ 'id' => 'cboLeaseKode', 'value' => 'LeaseKode', 'data' => $dtLeasing ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":Sort"           => FormField::combo( 'Sort', [ 'config' => [ 'id' => 'cboSort', 'value' => 'DKNo', 'data' => [
					'DKNo'              => 'No Data Konsumen',
					'SKNo'              => 'No Surat Jln Konsumen',
					'DKTgl'             => 'Tgl Data Kons',
					'SKTgl'             => 'Tgl Surat Jln Konsumen',
					'MotorType'         => 'Type Motor',
					'MotorNama'         => 'Nama Motor',
					'MotorWarna'        => 'Warna Motor',
					'MotorTahun'        => 'Tahun Motor',
					'CusNama'           => 'Nama Konsumen',
					'SalesNama'         => 'Nama Sales',
					'LeaseKode'         => 'Leasing',
					'DKJenis'           => 'Jenis',
					'FakturAHMNo'       => 'No Faktur AHM',
					'NoticeNo'          => 'No Notice',
					'STNKNo'            => 'No STNK',
					'PlatNo'            => 'No Plat',
					'BPKBNo'            => 'No BPKB',
					'STNKNama'          => 'Nama STNK',
					'STNKAlamat'        => 'Alamat STNK',
					'NoticePenerima'    => 'Penerima Notice',
					'STNKPenerima'      => 'Penerima STNK',
					'PlatPenerima'      => 'Penerima Plat',
					'BPKBPenerima'      => 'Penerima BPKB',
					'STNKTglBayar'      => 'Tgl Ajukan Faktur',
					'FakturAHMTgl'      => 'Tgl Jadi Faktur AHM',
					'FakturAHMTglAmbil' => 'Tgl Ambil Faktur AHM ',
					'STNKTglMasuk'      => 'Tgl Ajukan STNK',
					'STNKTglJadi'       => 'Tgl Jadi STNK',
					'STNKTglAmbil'      => 'Tgl Ambil STNK',
					'PlatTgl'           => 'Tgl Jadi Plat',
					'PlatTglAmbil'      => 'Tgl Ambil Plat',
					'NoticeTglJadi'     => 'Tgl Jadi Notice',
					'NoticeTglAmbil'    => 'Tgl Ambil Notice',
					'BPKBTglJadi'       => 'Tgl Jadi BPKB',
					'BPKBTglAmbil'      => 'Tgl Ambil BPKB',
					'DKMemo'            => 'Keterangan',
					'UserID'            => 'User',
					'CusKode'           => 'Kode Konsumen',
					'SalesKode'         => 'Kode Sales',
					'DKHarga'           => 'Harga OTR',
					'DKHPP'             => 'HPP',
					'DKDPTotal'         => 'DP Total',
					'DKDPLLeasing'      => 'DP Leasing',
					'DKDPTerima'        => 'DP Terima',
					'DKNetto'           => 'Netto',
					'ProgramSubsidi'    => 'Subsidi Program',
					'PrgSubsSupplier'   => 'Subsidi Supplier',
					'PrgSubsDealer'     => 'Subsidi Dealer',
					'PrgSubsFincoy'     => 'Subsidi Fincoy',
					'PotonganHarga'     => 'Potongan Harga',
					'ReturHarga'        => 'Retur Harga',
					'PotonganKhusus'    => 'Potongan Khusus',
					'BBN'               => 'BBN',
					'INNo'              => 'No Inden',
					'NamaHadiah'        => 'Nama Hadiah',
					'ProgramNama'       => 'Nama Program',
					'DKLunas'           => 'Lunas',
				] ], 'extraOptions'                                       => [ 'simpleCombo' => true ] ] ),
				":TypeSort"       => FormField::combo( 'TypeSort', [ 'config' => [ 'id' => 'cboOrder', 'value' => 'ASC', 'data' => [
					'ASC'  => 'ASC',
					'DESC' => 'DESC',
				] ], 'extraOptions'                                           => [ 'simpleCombo' => true ] ] ),
				":Tujuan"         => FormField::combo( 'TujuanUpdate', [ 'name' => 'cboField', 'config' => [ 'id' => 'cboField', 'name' => 'cboField' ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":Tgl"            => FormField::dateInput( [ 'name' => 'dtpField', 'config' => [ 'id' => 'dtpField', 'value' =>  General::asDate( 'now' ), ] ] ),
				":Penerima"       => Html::textInput( 'txtPenerima', '--', [ 'id' => 'txtPenerima', 'class' => 'form-control' ] ) .
				                     Html::hiddenInput( 'DKSKNoList', '' ) .
				                     Html::hiddenInput( 'gcsDocNo__', '', [ 'id' => 'gcsDocNo' ] ) .
				                     Html::hiddenInput( 'gcsDocSyarat__', '', [ 'id' => 'gcsDocSyarat' ] ) .
				                     Html::hiddenInput( 'gcsDocTgl__', '', [ 'id' => 'gcsDocTgl' ] ) .
				                     Html::hiddenInput( 'gcsDocPenerima__', '', [ 'id' => 'gcsDocPenerima' ] ),
				":btnPrint"       => Html::button( '<i class="glyphicon glyphicon-print"></i> &nbsp&nbspPrint&nbsp&nbsp', [ 'class' => 'btn btn-warning ', 'id' => 'btnPrint' ] ),
				":btnUpdate"      => Html::button( '<i class="glyphicon glyphicon-refresh"></i> &nbsp&nbspUpdate&nbsp&nbsp', [ 'class' => 'btn btn-danger ' . Custom::viewClass(), 'id' => 'btnUpdate' ] ),
			]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlPrint = Url::toRoute( [ 'ttsk/print-kolektif', 'id' => 'id' ] );
$this->registerJs( <<< JS
     window.requestEdit = false;    
     window.reload = true;
     $('#detailGrid').utilJqGrid({
        url: '$urlDetail',
        editurl: '$urlSerahTerimaUpdateLine',
        height: 370,        
	    datatype: "json",
	    scrollrows : true,
        jsonReader : {
            repeatitems:false,
              id: "MotorNoMesin"
        },
        serializeRowData: function (postdata) {
            let header = $('#form_ttsk_id').utilForm().getData();
            postdata = Object.assign(postdata, header);
            // console.log(postdata);
            return postdata;
        },
        // ajaxRowOptions: {
        //     beforeSend: function () {
        //         $("#grid").block({message: "<h1>Saving the data...</h1>"});
        //     }
        // },
        postData: $('#form_ttsk_id').serialize(),   
        loadonce:true,
        rowNum: 100000,
        navButtonTambah: false,
        multiselect:true,  
        rownumbers: true,    
        colModel: [
            {
                name: 'gcsDKSKNo',
                label: 'No SK',
                width: 75,
                jsonmap:'SKNo'
            }, 
            {
                name: 'gcsDKSKTgl',
                label: 'Tgl SK',
                formatter: 'date',
                formatoptions:{
                    srcformat: 'ISO8601Long',
                    newformat: 'd/m/Y'
                },
                width: 75,
                jsonmap:'SKTgl'
            }, 
            {
                name: 'MotorNama',
                label: 'Nama Motor',
                width: 150,
            },
            {
                name: 'MotorNoMesin',
                label: 'No Mesin',
                width: 100,
                editable: true, 
                editoptions:{
                    readOnly:true,
                },
            },
            {
                name: 'MotorNoMesin',
                label: 'MotorNoMesin',
                hidden: true,
            },         
            {
                name: 'CusNama',
                label: 'Nama Konsumen',
                width: 150,
            },   
               
            {
                name: 'gcsDocNo',
                label: 'Tgl Ajukan',
                width: 120,
                editable: true, 
                editoptions:{
                    readOnly:true,
                },
            },
            {
                name: 'gcsDocSyarat',
                label: 'Tgl Jadi',
                width: 120,
                editable: true, 
                editoptions:{
                    readOnly:true,
                },
            },
            {
                name: 'gcsDocTgl',
                label: 'No Faktur',
                width: 75,
                editable: true, 
                editoptions:{
                    readOnly:true,
                },
            },
            {
                name: 'gcsDocPenerima',
                label: 'Nominal Faktur',
                width: 100,
                editable: true, 
                editoptions:{
                    readOnly:true,
                },
            },
            
            {
                name: 'LeaseKode',
                label: 'Leasing',
                width: 75,
                editable: true, 
                editoptions:{
                    readOnly:true,
                },
            },
            {
                name: 'gcsDocSales',
                label: 'Sales',
                width: 150,
                editable: true, 
                editoptions:{
                    readOnly:true,
                },
            },
            {
                name: 'MotorAutoN',
                label: 'MotorAutoN',
                editable: true,
                hidden: true,
            },
            {
                name: 'STNKNama',
                label: 'STNKNama',
                editable: true,
                hidden: true,
            },
            {
                name: 'gcsDocTgl',
                label: 'FakturAHMNo',
                editable: true,
                hidden: true,
            },
            {
                name: 'STNKNo',
                label: 'STNKNo',
                editable: true,
                hidden: true,
            },
            {
                name: 'NoticeNo',
                label: 'NoticeNo',
                editable: true,
                hidden: true,
            },
            {
                name: 'PlatNo',
                label: 'PlatNo',
                editable: true,
                hidden: true,
            },
            {
                name: 'NoticeNilai',
                label: 'NoticeNilai',
                editable: true,
                hidden: true,
            },
            {
                name: 'BPKBNo',
                label: 'BPKBNo',
                editable: true,
                hidden: true,
            },
            {
                name: 'NoticePenerima',
                label: 'NoticePenerima',
                editable: true,
                hidden: true,
            },
            {
                name: 'STNKPenerima',
                label: 'STNKPenerima',
                editable: true,
                hidden: true,
            },
            {
                name: 'PlatPenerima',
                label: 'PlatPenerima',
                editable: true,
                hidden: true,
            },
            {
                name: 'BPKBPenerima',
                label: 'BPKBPenerima',
                editable: true,
                hidden: true,
            },
            {
                name: 'DKNo',
                label: 'DKNo',
                editable: true,
                hidden: true,
            },
            {
                name: 'SKNo',
                label: 'SKNo',
                editable: true,
                hidden: true,
            },
        ],
        ondblClickRow: function(){
            var row_id = $("#detailGrid").getGridParam('selrow');
            jQuery('#detailGrid').editRow(row_id, true);
            window.requestEdit = true;
        },
        beforeRequest: function() {
            if (window.requestEdit){                
                window.requestEdit = false;
                reloadGrid(true);
                return false;
            }            
        },
        // beforeSubmit : function(postdata, formid) {
        //     console.log(postdata);
        // },
        gridComplete: function() {
            $('#gcsDocNo').val($('#detailGrid').getColProp('gcsDocNo').jsonmap);
            $('#gcsDocSyarat').val($('#detailGrid').getColProp('gcsDocSyarat').jsonmap);
            $('#gcsDocTgl').val($('#detailGrid').getColProp('gcsDocTgl').jsonmap);
            $('#gcsDocPenerima').val($('#detailGrid').getColProp('gcsDocPenerima').jsonmap);
            $('#gcsDocNotice').val($('#detailGrid').getColProp('gcsDocNotice').jsonmap);
            if(window.selected !== undefined){
                for (var i = 0; i < window.selected.length; i++) {
                    $("#detailGrid").setSelection(window.selected[i]);
                }
            }
        }
    }).init().fit($('.box-body'));
    function reloadGrid(load=true){
        if(!window.reload) return;
        var grid = $('#detailGrid'); 
        var CMB1 = $("#cboDKSKTgl");
        var TyTglDoc = $("#TyTglDoc");
        var CMB1_value = CMB1.val(); 
        var TyTglDoc_value = TyTglDoc.val();
        var TyTglDoc_txt = TyTglDoc.select2('data')[0].text;
        grid.setColProp('gcsDocPenerima',{align:'left',formatter: null,formatoptions: null}); 
        if (CMB1_value === 'ttsk.SKTgl'){
            grid.setColProp('gcsDKSKNo',{jsonmap:'SKNo'}); 
            grid.jqGrid('setLabel', 'gcsDKSKNo', 'No SK');
            grid.setColProp('gcsDKSKTgl',{jsonmap:'SKTgl'}); 
            grid.jqGrid('setLabel', 'gcsDKSKTgl', 'Tgl SK');   
        }else{
            grid.setColProp('gcsDKSKNo',{jsonmap:'DKNo'}); 
            grid.jqGrid('setLabel', 'gcsDKSKNo', 'No DK');
            grid.setColProp('gcsDKSKTgl',{jsonmap:'DKTgl'}); 
            grid.jqGrid('setLabel', 'gcsDKSKTgl', 'Tgl DK'); 
        }        
        function setGrup1() {           
            grid.setColProp('gcsDocNo',{jsonmap:'FakturAHMNo',editoptions:{readOnly:false}}); 
            grid.jqGrid('setLabel', 'gcsDocNo', 'No Faktur');
            grid.setColProp('gcsDocPenerima',{jsonmap:'FakturAHMNilai', align: "right",
                            formatter: "number",
                            formatoptions: {decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 2}}); 
            grid.jqGrid('setLabel', 'gcsDocPenerima', 'Nominal Faktur');
        }       
        function setGrup2() {         
            grid.setColProp('gcsDocNo',{jsonmap:'STNKNo',editoptions:{readOnly:false}}); 
            grid.jqGrid('setLabel', 'gcsDocNo', 'No STNK');
        }
        function setGrup3() {         
            grid.setColProp('gcsDocNo',{jsonmap:'NoticeNo',editoptions:{readOnly:false}}); 
            grid.jqGrid('setLabel', 'gcsDocNo', 'No Notice');
        }
        function setGrup4() {         
            grid.setColProp('gcsDocNo',{jsonmap:'PlatNo',editoptions:{readOnly:false}}); 
            grid.jqGrid('setLabel', 'gcsDocNo', 'No Plat');     
            grid.setColProp('gcsDocPenerima',{jsonmap:'PlatPenerima',editoptions:{readOnly:false}}); 
            grid.jqGrid('setLabel', 'gcsDocPenerima', 'Penerima Plat');
        }
        function setGrup5() {         
            grid.setColProp('gcsDocNo',{jsonmap:'BPKBNo',editoptions:{readOnly:false}}); 
            grid.jqGrid('setLabel', 'gcsDocNo', 'No BPKB');     
        }
        function setGrup6() {         
            grid.setColProp('gcsDocNo',{jsonmap:'SRUTNo',editoptions:{readOnly:false}}); 
            grid.jqGrid('setLabel', 'gcsDocNo', 'No SRUT');     
        }
        switch (TyTglDoc_value) {
            case 'STNKTglBayar'  :
                if (CMB1_value === 'ttdk.DKTgl'){
                    grid.setColProp('gcsDocSyarat',{jsonmap:'SKTgl',formatter: 'date',formatoptions:{srcformat: 'ISO8601Long',newformat: 'd/m/Y'}}); 
                    grid.jqGrid('setLabel', 'gcsDocSyarat', 'Tgl SK');    
                }else{
                    grid.setColProp('gcsDocSyarat',{jsonmap:'DKTgl',formatter: 'date',formatoptions:{srcformat: 'ISO8601Long',newformat: 'd/m/Y'}}); 
                    grid.jqGrid('setLabel', 'gcsDocSyarat', 'Tgl DK');
                } 
                grid.setColProp('gcsDocSales',{jsonmap:'SalesNama',editoptions:{readOnly:false}});
                setGrup1();
                break;
            case 'FakturAHMTgl':
                grid.setColProp('gcsDocSyarat',{jsonmap:'STNKTglBayar',formatter: 'date',formatoptions:{srcformat: 'ISO8601Long',newformat: 'd/m/Y'}}); 
                grid.jqGrid('setLabel', 'gcsDocSyarat', 'Tgl Ajukan');
                setGrup1();
                break;
            case 'FakturAHMTglAmbil':
                grid.setColProp('gcsDocSyarat',{jsonmap:'FakturAHMTgl',formatter: 'date',formatoptions:{srcformat: 'ISO8601Long',newformat: 'd/m/Y'}}); 
                grid.jqGrid('setLabel', 'gcsDocSyarat', 'Tgl Jadi');  
                setGrup1(); 
                break;
            case 'STNKTglMasuk':
                grid.setColProp('gcsDocSyarat',{jsonmap:'STNKTglBayar',formatter: 'date',formatoptions:{srcformat: 'ISO8601Long',newformat: 'd/m/Y'}}); 
                grid.jqGrid('setLabel', 'gcsDocSyarat', 'Tgl Aju Fakt');                
                grid.setColProp('gcsDocPenerima',{jsonmap:'STNKPenerima'}); 
                grid.jqGrid('setLabel', 'gcsDocPenerima', 'Penerima STNK');
                setGrup2(); 
                break;
            case 'STNKTglJadi':
                grid.setColProp('gcsDocSyarat',{jsonmap:'STNKTglMasuk',formatter: 'date',formatoptions:{srcformat: 'ISO8601Long',newformat: 'd/m/Y'}}); 
                grid.jqGrid('setLabel', 'gcsDocSyarat', 'Tgl Ajukan');                
                grid.setColProp('gcsDocPenerima',{jsonmap:'PlatNo'}); 
                grid.jqGrid('setLabel', 'gcsDocPenerima', 'No Plat');
                setGrup2(); 
                break;
            case 'STNKTglAmbil':
                grid.setColProp('gcsDocSyarat',{jsonmap:'STNKTglJadi',formatter: 'date',formatoptions:{srcformat: 'ISO8601Long',newformat: 'd/m/Y'}}); 
                grid.jqGrid('setLabel', 'gcsDocSyarat', 'Tgl Jadi');                
                grid.setColProp('gcsDocPenerima',{jsonmap:'STNKPenerima',editoptions:{readOnly:false}}); 
                grid.jqGrid('setLabel', 'gcsDocPenerima', 'Penerima STNK');
                setGrup2(); 
                break;
            case 'NoticeTglJadi':
                grid.setColProp('gcsDocSyarat',{jsonmap:'STNKTglMasuk',formatter: 'date',formatoptions:{srcformat: 'ISO8601Long',newformat: 'd/m/Y'}}); 
                grid.jqGrid('setLabel', 'gcsDocSyarat', 'Tgl Ajukan');                
                grid.setColProp('gcsDocPenerima',{jsonmap:'PlatNo'}); 
                grid.jqGrid('setLabel', 'gcsDocPenerima', 'No Plat');
                grid.setColProp('gcsDocSales',{jsonmap:'NoticeNilai',editoptions:{readOnly:false}}); 
                grid.jqGrid('setLabel', 'gcsDocSales', 'Notice Nilai');
                setGrup3(); 
                break;
            case 'NoticeTglAmbil':
                grid.setColProp('gcsDocSyarat',{jsonmap:'NoticeTglJadi',formatter: 'date',formatoptions:{srcformat: 'ISO8601Long',newformat: 'd/m/Y'}}); 
                grid.jqGrid('setLabel', 'gcsDocSyarat', 'Tgl Jadi');                
                grid.setColProp('gcsDocPenerima',{jsonmap:'NoticePenerima',editoptions:{readOnly:false}}); 
                grid.jqGrid('setLabel', 'gcsDocPenerima', 'Penerima Notice');
                setGrup3(); 
                break;
            case 'PlatTgl':
                grid.setColProp('gcsDocSyarat',{jsonmap:'STNKTglMasuk',formatter: 'date',formatoptions:{srcformat: 'ISO8601Long',newformat: 'd/m/Y'}}); 
                grid.jqGrid('setLabel', 'gcsDocSyarat', 'Tgl Ajukan');         
                setGrup4(); 
                break;
            case 'PlatTglAmbil':
                grid.setColProp('gcsDocSyarat',{jsonmap:'PlatTgl',formatter: 'date',formatoptions:{srcformat: 'ISO8601Long',newformat: 'd/m/Y'}}); 
                grid.jqGrid('setLabel', 'gcsDocSyarat', 'Tgl Jadi');         
                setGrup4(); 
                break;
            case 'BPKBTglJadi':
                grid.setColProp('gcsDocSyarat',{jsonmap:'STNKTglMasuk',formatter: 'date',formatoptions:{srcformat: 'ISO8601Long',newformat: 'd/m/Y'}}); 
                grid.jqGrid('setLabel', 'gcsDocSyarat', 'Tgl Ajukan');
                grid.setColProp('gcsDocPenerima',{jsonmap:'BPKBPenerima'}); 
                grid.jqGrid('setLabel', 'gcsDocPenerima', 'Penerima BPKB');         
                setGrup5(); 
                break;
            case 'BPKBTglAmbil':
                grid.setColProp('gcsDocSyarat',{jsonmap:'BPKBTglJadi',formatter: 'date',formatoptions:{srcformat: 'ISO8601Long',newformat: 'd/m/Y'}}); 
                grid.jqGrid('setLabel', 'gcsDocSyarat', 'Tgl Jadi');
                grid.setColProp('gcsDocPenerima',{jsonmap:'BPKBPenerima',editoptions:{readOnly:false}}); 
                grid.jqGrid('setLabel', 'gcsDocPenerima', 'Penerima BPKB');         
                setGrup5(); 
                break;
            case 'SRUTTglJadi':
                grid.setColProp('gcsDocSyarat',{jsonmap:'STNKTglMasuk',formatter: 'date',formatoptions:{srcformat: 'ISO8601Long',newformat: 'd/m/Y'}}); 
                grid.jqGrid('setLabel', 'gcsDocSyarat', 'Tgl Ajukan');
                grid.setColProp('gcsDocPenerima',{jsonmap:'PlatNo'}); 
                grid.jqGrid('setLabel', 'gcsDocPenerima', 'No Plat');         
                setGrup6(); 
                break;
            case 'SRUTTglAmbil':
                grid.setColProp('gcsDocSyarat',{jsonmap:'SRUTTglJadi',formatter: 'date',formatoptions:{srcformat: 'ISO8601Long',newformat: 'd/m/Y'}}); 
                grid.jqGrid('setLabel', 'gcsDocSyarat', 'Tgl Jadi');
                grid.setColProp('gcsDocPenerima',{jsonmap:'SRUTPenerima',editoptions:{readOnly:false}}); 
                grid.jqGrid('setLabel', 'gcsDocPenerima', 'Penerima SRUT');         
                setGrup6(); 
                break;                
        }
        
        grid.setColProp('gcsDocTgl',{jsonmap:TyTglDoc_value,formatter: 'date',formatoptions:{srcformat: 'ISO8601Long',newformat: 'd/m/Y'}}); 
        grid.jqGrid('setLabel', 'gcsDocTgl', TyTglDoc_txt);   
        $('#gcsDocNo').val(grid.getColProp('gcsDocNo').jsonmap);
        $('#gcsDocSyarat').val(grid.getColProp('gcsDocSyarat').jsonmap);
        $('#gcsDocTgl').val(grid.getColProp('gcsDocTgl').jsonmap);
        $('#gcsDocPenerima').val(grid.getColProp('gcsDocPenerima').jsonmap);
        $('#gcsDocNotice').val(grid.getColProp('gcsDocNotice').jsonmap);
        if (load) grid.jqGrid('setGridParam',{datatype:'json',postData: $('#form_ttsk_id').serialize()}).trigger('reloadGrid');
    }
    
    window.reloadGrid = reloadGrid;
    
    $('#TyTglDoc,#cboDKSKTgl,#dtpDoc2,#dtpDoc1,#dtpDKSKTgl1,#dtpDKSKTgl2,#cboOrder,#cboSort').change(function() {
        reloadGrid();  
    });   
     
    $('#cbxDoc').on('change', function() {
        // var tgl1 = $('#dtpDKSKTgl1-disp');
        // var tgl2 = $('#dtpDKSKTgl2-disp');
        var tgl1 = $('#dtpDoc1-disp');
        var tgl2 = $('#dtpDoc2-disp');

        switch(this.value){
            case '0' :
                tgl1.kvDatepicker('update', new Date('1900-01-01'));											 
                tgl2.kvDatepicker('update', new Date('2078-01-01'));
                $('#showtitle').attr("title", 'Semua');
                break; 
            case '1' :
                tgl1.kvDatepicker('update', new Date('2000-01-01'));											 
                tgl2.kvDatepicker('update', new Date('2078-01-01'));
                $('#showtitle').attr("title", 'Sudah');
                break;
            default :
                tgl1.kvDatepicker('update', new Date('1900-01-01'));											 
                tgl2.kvDatepicker('update', new Date('1900-01-01'));
                $('#showtitle').attr("title", 'Belum');
                break;
        }
        reloadGrid(); 
    });
        
    $('#cboLeaseKode').on('change', function(){
        reloadGrid(); 
    });
    
    reloadGrid(); 
    function changeButton(save){
        if(save){
            $('#btnUpdate').attr('class','btn btn-primary').html('<i class="glyphicon glyphicon-floppy-disk"></i> &nbsp;&nbsp;Save&nbsp;&nbsp;');            
            $('#btnPrint').attr('class','btn btn-danger').html('<i class="glyphicon glyphicon-ban-circle"></i> &nbsp;&nbsp;Cancel&nbsp;&nbsp;');            
        }else{
            $('#btnUpdate').attr('class','btn btn-danger').html('<i class="glyphicon glyphicon-refresh"></i> &nbsp;&nbsp;Update&nbsp;&nbsp;');        
            $('#btnPrint').attr('class','btn btn-warning').html('<i class="glyphicon glyphicon-print"></i> &nbsp;&nbsp;Print&nbsp;&nbsp;');     
        }
    }
    $('#btnUpdate').click(function (event) {
        var isSave = $(this).text().includes('Save');
        if(!isSave){
            changeButton(true);
            return;
        }
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah anda ingin mengupdate data yang tercek pada Datagrid ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {                    
                    var records = [];
                    var myGrid = $('#detailGrid'),
                    selRowId = myGrid.jqGrid ('getGridParam', 'selarrrow');
                    if(selRowId.length === 0) return;
                    window.selected = selRowId;
                    selRowId.forEach(function(itm,ind) {
                        records.push($('#detailGrid').jqGrid ('getRowData', itm));
                    });
                    var tgl = $("#dtpField").val();
                    var tujuan = $("#cboField").val();
                    var TyTglDoc = $("#TyTglDoc").val();
                    window.reload = false;
                   $.ajax({
                        type: "POST",
                        url: '$urlSerahTerimaUpdate',
                        data: {
                            TyTglDoc: TyTglDoc,
                            tujuan: tujuan,
                            tgl: tgl,
                            penerima: $('#txtPenerima').val(),
                            ids : records
                        },
                        success: function(response) {
                            $.notify({message: response.keterangan},{
                                   type: (response.status == 0)? 'success' : 'warning',                   
                               });
                            changeButton(false);                            
                            $('#dtpDoc1').utilDateControl().val(tgl);
                            $('#dtpDoc2').utilDateControl().val(tgl);  
                            $('#TyTglDoc').val(tujuan).trigger('change');      
                            setTimeout(function(){window.reload = true;reloadGrid();},1000);                             
                        },
                    });    
                }
            }
        });
    });  
    
    $('#btnPrint').click(function (event) {	  
        var isCancel = $(this).text().includes('Cancel');
        if(isCancel){
            changeButton(false);
            return;
        }
        var records = [];
        var myGrid = $('#detailGrid'),
        selRowId = myGrid.jqGrid ('getGridParam', 'selarrrow');
        selRowId.forEach(function(itm,ind) {
            records.push($('#detailGrid').jqGrid ('getRowData', itm).gcsDKSKNo);
        });
        $('[name=DKSKNoList]').val(records.join(';'));
        $('#form_ttsk_id').attr('action','$urlPrint');
        $('#form_ttsk_id').attr('target','_blank');
        $('#form_ttsk_id').submit();
	  });
    
    $('#txtSearch').on('keyup', function(e){
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code===13) {
            e.preventDefault();
        }else{
            return;
        }
        var grid = $('#detailGrid');
        var cmb = $('#cboSearch');
        var kolom = cmb.val();        
        var rowData = grid.getRowData();
        var cari = $(this).val().toLowerCase();
        var k;
        for (k = 0; k < rowData.length; ++k){
            if(rowData[k][kolom].toLowerCase().includes(cari)){
                grid.jqGrid('setSelection',rowData[k]['MotorNoMesin']);
                return;
            }
        }
    });
JS
);










