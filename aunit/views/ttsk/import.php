<?php
/* @var $this yii\web\View */

/* @var $dataProvider yii\data\ActiveDataProvider */

use aunit\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
$this->title = 'Manage Delivery Process';
$this->params['breadcrumbs'][] = $this->title;
$arr = [
    'cmbTxt' => [
        'deliveryDocumentId' => 'Delivery Document Id',
        'idSPK' => 'Id SPK',
        'idCustomer' => 'Id Customer',
    ],
    'cmbTxt2' => [
        'deliveryDocumentId' => 'Delivery Document Id',
        'idDriver' => 'Id Driver',
        'statusDeliveryDocument' => 'Status Delivery Document',
    ],
    'cmbTgl' => [],
    'cmbNum' => [],
    'sortname' => "deliveryDocumentId",
    'sortorder' => 'desc'
];
$this->registerJsVar('setcmb', $arr);
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render('../_search'); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs(<<< JS
    window.gridParam_jqGrid = {
        readOnly: true,
        rownumbers: true,
        rowNum: 10000,
        pgbuttons: false,
        pginput: false,
        viewrecords: false,
        rowList: [],
        pgtext: ""
    }
    // $('#filter-tgl-val1').change(function() {
    //     let _tgl1 = new Date($(this).val());      
    //     _tgl1.setDate(_tgl1.getDate() + 5);
	//     $('#filter-tgl-val2-disp').datepicker('update', _tgl1);
	//     $('#filter-tgl-val2-disp').trigger('change');
    // });
JS
);

$this->registerJs(\aunit\components\TdUi::mainGridJs(\aunit\models\Ttsk::class,
    'Import Manage Delevery Process', [
        'colGrid' => $colGrid,
        'url' => Url::toRoute(['ttsk/import-items']),
        'mode' => isset($mode) ? $mode : '',
    ]), \yii\web\View::POS_READY);
$urlDetail = Url::toRoute(['ttsk/import-details']);
$this->registerJs(<<< JS
jQuery(function ($) {
    if($('#jqGridDetail').length) {
        $('#jqGridDetail').utilJqGrid({
            url: '$urlDetail',
            height: 240,
            readOnly: true,
            rownumbers: true,
            rowNum: 1000,
            colModel: [
               	{
	                name: 'noSO',
	                label: 'No SO',
	                width: 120,
	            },
                {
                    name: 'idSPK',
                    label: 'Id SPK',
                    width: 130
                },
	            {
	                name: 'idCustomer',
	                label: 'Id Customer',
	                width: 120,
	            },
	            {
	                name: 'noMesin',
	                label: 'No Mesin',
	                width: 100,
	            },
	            {
	                name: 'noRangka',
	                label: 'No Rangka',
	                width: 120,
	            },
	            {
	                name: 'waktuPengiriman',
	                label: 'Waktu Pengiriman',
	                width: 120,
	            },
	            {
	                name: 'checklistKelengkapan',
	                label: 'Checklist Kelengkapan',
	                width: 260,
	            },
	            {
	                name: 'lokasiPengiriman',
	                label: 'Lokasi Pengiriman',
	                width: 200,
	            },
	            {
	                name: 'latitude',
	                label: 'Latitude',
	                width: 140,
	            },
	            {
	                name: 'longitude',
	                label: 'Longitude',
	                width: 140,
	            },
	            {
	                name: 'namaPenerima',
	                label: 'Nama Penerima',
	                width: 180,
	            },
	            {
	                name: 'noKontakPenerima',
	                label: 'No Kontak Penerima',
	                width: 130,
	            },
	            {
	                name: 'createdTime',
	                label: 'Created Time',
	                width: 130,
	            },
            ],
	        multiselect: gridFn.detail.multiSelect(),
	        onSelectRow: gridFn.detail.onSelectRow,
	        onSelectAll: gridFn.detail.onSelectAll,
	        ondblClickRow: gridFn.detail.ondblClickRow,
	        loadComplete: gridFn.detail.loadComplete,
	        listeners: {
                afterLoad: function(grid, response) {
                    $("#cb_jqGridDetail").trigger('click');
                }
            }
        }).init().setHeight(gridFn.detail.height);
    }
});
JS, \yii\web\View::POS_READY);