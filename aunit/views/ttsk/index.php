<?php
use aunit\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
AppAsset::register( $this );
$this->title                     = 'Surat Jalan Konsumen';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'SKNo'       => 'No SK',
		'LokasiKode' => 'Lokasi',
		'SKPengirim' => 'Pengirim',
		'SKMemo'     => 'Keterangan',
		'NoGL'       => 'No Gl',
		'UserID'     => 'User ID',
		'SKCetak'    => 'Cetak',
		'DKNo'       => 'No DK',
		'CusNama'    => 'Nama Konsumen',
		'MotorNoMesin'   => 'No Mesin',
	],
	'cmbTgl'    => [
		'SKTgl' => 'Tanggal SK',
	],
	'cmbNum'    => [
	],
	'sortname'  => "AAA.SKNo",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs(<<< JS
	var eventSource = new EventSource(window.location.href);
	eventSource.onmessage = function(event) {
		eventSource.close(); 
		var msg = JSON.parse(event.data);
		$.notify({message: msg.keterangan},{type: msg.status ? 'success' : 'warning'});
	};
JS
);
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttsk::class,
	'Surat Jalan Konsumen', [
		'mode'       => isset( $mode ) ? $mode : '',
		'url_delete' => Url::toRoute( [ 'ttsk/delete' ] ),
	] ), \yii\web\View::POS_READY );