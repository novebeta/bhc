<?php
use aunit\components\FormField;
use aunit\models\Ttsmhd;
use common\components\General;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttdk */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
$form = ActiveForm::begin( [ 'id' => 'frm_mo_id' ] );
\aunit\components\TUi::form(
	[ 'class' => "row-no-gutters",
	  'items' => [
		  [ 'class' => "form-group col-md-24",
		    'items' => [
			    [ 'class' => "col-md-4 col-sm-4 col-lg-4 col-xs-4", 'items' => ":LabelMONo" ],
			    [ 'class' => "col-md-6 col-sm-6 col-lg-6 col-xs-6", 'items' => ":MONo" ],
			    [ 'class' => "col-md-1 col-sm-1 col-lg-1 col-xs-1" ],
			    [ 'class' => "col-md-3 col-sm-3 col-lg-3 col-xs-3", 'items' => ":LabelMOTgl" ],
			    [ 'class' => "col-md-5 col-sm-5 col-lg-5 col-xs-5", 'items' => ":MOTgl" ],
			    [ 'class' => "col-md-5 col-sm-5 col-lg-5 col-xs-5", 'items' => ":MOJam" ],
		    ],
		  ],
		  [ 'class' => "form-group col-md-24", 'items' => [ 'class' => "row", 'items' => "" ] ],
		  [ 'class' => "form-group col-md-24",
		    'items' => [
			    [ 'class' => "col-md-4 col-sm-4 col-lg-4 col-xs-4", 'items' => ":LabelMOAsal" ],
			    [ 'class' => "col-md-20 col-sm-20 col-lg-20 col-xs-20", 'items' => ":MOAsal" ],
		    ],
		  ],
		  [ 'class' => "form-group col-md-24", 'items' => [ 'class' => "row", 'items' => "" ] ],
		  [ 'class' => "form-group col-md-24",
		    'items' => [
			    [ 'class' => "col-md-4 col-sm-4 col-lg-4 col-xs-4", 'items' => ":LabelMOTujuan" ],
			    [ 'class' => "col-md-20 col-sm-20 col-lg-20 col-xs-20", 'items' => ":MOTujuan" ],
		    ],
		  ],
		  [ 'class' => "form-group col-md-24", 'items' => [ 'class' => "row", 'items' => "" ] ],
	  ]
	],
	[
		'class' => "pull-right",
		'items' => ":btnSave :btnHapus"
	],
	[
		":LabelMONo"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Mutasi</label>',
		":LabelMOTgl"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tanggal</label>',
		":LabelMOAsal"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Asal</label>',
		":LabelMOTujuan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tujuan</label>',
		":br"            => '<label class="control-label" style="margin: 1px;"> </label>',
		":MONo"          => Html::textInput( 'MONo', $data[ 'MONo' ], [ 'class' => 'form-control', 'readonly' => true ] ) .
		                    Html::hiddenInput( 'SKNo', $data[ 'SKNo' ] ) .
		                    Html::hiddenInput( 'MotorNoMesin', $data[ 'MotorNoMesin' ] ) .
		                    Html::hiddenInput( 'MotorAutoN', $data[ 'MotorAutoN' ] ),
		":MOTgl"         => FormField::dateInput( [ 'name' => 'MOTgl', 'config' => [ 'readonly' => true ] ] ),
		":MOJam"         => FormField::timeInput( [ 'name' => 'MOJam', 'config' => [ 'readonly' => true ] ] ),
		":MOAsal"        => FormField::combo( 'LokasiKode', [ 'name' => 'MOAsal', 'config' => [ 'id' => 'MOAsal', 'value' => $data[ 'LokasiKode' ], 'disabled' => true ], 'extraOptions' => [ 'altLabel' => [ 'LokasiNama' ], ] ] ),
		":MOTujuan"      => FormField::combo( 'LokasiKode', [ 'name' => 'MOTujuan', 'config' => [ 'id' => 'MOTujuan' ], 'extraOptions' => [ 'altLabel' => [ 'LokasiNama' ], ] ] ),
		":btnSave"       => Html::button( '<i class="glyphicon glyphicon-floppy-disk"></i> Simpan', [ 'class' => 'btn btn-info btn-primary ', 'id' => 'btnSavePopup' ] ),
		":btnHapus"      => Html::button( '<i class="fa fa-ban"></i> Hapus', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnHapusPopup' ] ) .
		                    Html::Button( '<i class="fa fa-ban"></i>&nbspKeluar&nbsp&nbsp', [ 'data-dismiss' => "modal", 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancelPopup' ] ),
	],
	[
		'jsBtnDelete'=> false
	]
);
ActiveForm::end();
$urlSave  = Url::toRoute( [ 'ttsk/create-mo', 'SKId' => $data[ 'SKId' ], 'SKNoView' => $data[ 'SKNoView' ] ] );
$urlHapus = Url::toRoute( [ 'ttsk/delete-mo', 'SKId' => $data[ 'SKId' ], 'SKNoView' => $data[ 'SKNoView' ] ] );
$urlCek   = Url::toRoute( [ 'ttsk/cek-mo', 'SKNo' => $data[ 'SKNo' ], 'MONo' => $data[ 'MONo' ] ] );
$this->registerJs( <<< JS

	function reloadData(){
    	$.ajax({type: 'POST',url: '$urlCek'})
         .then(function (dt) {
             if (dt.ada){
                 $('#btnHapusPopup').show();
                 $('#btnCancelPopup').hide();
                 $('[name=MOTgl]').utilDateControl().setDate(new Date(dt.MOTgl));
                 $('[name=MOJam]').timepicker('setTime', util.date.sql2sTime(dt.MOJam));
                 $('#MOAsal').val(dt.MOAsal).trigger('change').prop("disabled", true);
                 $('#MOTujuan').val(dt.MOTujuan).trigger('change').prop("disabled", true);
                bootbox.alert({message:'Motor ini telah mengalami proses mutasi dengan Nomor : {$data['MONo']}<br>Tekan Tombol Hapus untuk menghapus Mutasi ini.',size:'small'});
			}else{
                 $('#btnHapusPopup').hide();
                 $('#btnCancelPopup').show();
                 $('[name=MOTgl]').utilDateControl().setDate(new Date());
                 $('[name=MOJam]').timepicker('setTime', (new Date()).toLocaleTimeString('en-GB', {hour: '2-digit', minute:'2-digit', second:'2-digit'}));
                 $('#MOAsal').val(dt.MOAsal).trigger('change').prop("disabled", true);
                 $('#MOTujuan').val(dt.MOTujuan).trigger('change').prop("disabled", false);
			}
         });
	}

	$('#btnSavePopup').click(function (event) {	  
	    if ($('#MOAsal').val() === $('#MOTujuan').val()){
	        bootbox.alert({message:'Lokasi asal harus berbeda dengan lokasi tujuan',size:'small'});
	        return;
	    }
	    $.ajax({type: 'POST',url: '$urlSave',data: $('#frm_mo_id').utilForm().getData(true)})
         .then(function (dt) {
             if (dt.status == '0'){
                 $("[name=LokasiKode]").val($('#MOTujuan').val());
             }
            //$.notify({message: dt.keterangan},{type: data.status ? 'success' : 'warning'});
            $('#modalMO').modal('hide');
             bootbox.alert({message:dt.keterangan,size:'small'});
         });
	  }); 

	$('#btnHapusPopup').click(function (event) {
        $.ajax({type: 'POST',url: '$urlHapus',data: $('#frm_mo_id').utilForm().getData(true)})
         .then(function (dt) {
             bootbox.alert({message:dt.keterangan,size:'small'});
             // $.notify({message: dt.keterangan},{type: data.status ? 'success' : 'warning'});
             if (dt.status == '0'){
                 $("[name=LokasiKode]").val($('#MOAsal').val());
             }
             reloadData();
         });
	  });	
        
        $('#modalMO').on('shown.bs.modal', function (e) {
             reloadData();
        });
	
JS
);
?>