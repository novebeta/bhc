<?php
use aunit\components\FormField;
use common\components\Custom;
use common\components\General;
use common\models\User;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttdk */
/* @var $form yii\widgets\ActiveForm */
$isCreate = isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] === 'create';
$showKwitansi = isset( $_GET[ 'action' ] ) && in_array($_GET[ 'action' ],['view','display']);
?>
    <div class="ttsk-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'frm_ttsk_id', 'action' => $url[ 'update' ], 'options' => [ 'autocomplete' => 'off', 'method' => 'post', 'fieldConfig' => [ 'template' => "{input}" ] ] ] );
		\aunit\components\TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelSKNo" ],
					    [ 'class' => "col-sm-6", 'items' => ":SKNo" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelDKNo" ],
					    [ 'class' => "col-sm-2", 'items' => ":DKNo" ],
					    [ 'class' => "col-sm-1", 'items' => ":BtnSearchDatKon" ],
					    [ 'class' => "col-sm-1", 'items' => ":LabelTglDK" ],
					    [ 'class' => "col-sm-2", 'items' => ":TglDK" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelJenisDK", 'style' => 'padding-left:20px' ],
					    [ 'class' => "col-sm-2", 'items' => ":JenisDK" ],
					    [ 'class' => "col-sm-1", 'items' => ":LabelNoSPK", 'style' => 'padding-left:2px' ],
					    [ 'class' => "col-sm-2", 'items' => ":NoSPK" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelTglSK" ],
					    [ 'class' => "col-sm-3", 'items' => ":TglSK" ],
					    [ 'class' => "col-sm-3", 'items' => ":JamSK" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelWarehouse" ],
					    [ 'class' => "col-sm-4", 'items' => ":Warehouse" ],
					    [ 'class' => "col-sm-1", 'items' => ":BtnSearchMutasi" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelCetak" ],
					    [ 'class' => "col-sm-5", 'items' => ":Cetak" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelNamaSales" ],
					    [ 'class' => "col-sm-6", 'items' => ":NamaSales" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelTeam" ],
					    [ 'class' => "col-sm-5", 'items' => ":Team" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelLeasing" ],
					    [ 'class' => "col-sm-5", 'items' => ":Leasing" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelKeterangan" ],
					    [ 'class' => "col-sm-14", 'items' => ":Keterangan" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelNoBuku" ],
					    [ 'class' => "col-sm-5", 'items' => ":NoBuku" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":labelKonsumen" ],
					    [ 'class' => "col-sm-6", 'items' => ":Konsumen" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":labelKTP" ],
					    [ 'class' => "col-sm-5", 'items' => ":KTP" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":labelKabupaten" ],
					    [ 'class' => "col-sm-5", 'items' => ":Kabupaten" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":labelAlamat" ],
					    [ 'class' => "col-sm-6", 'items' => ":Alamat" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":labelRTRW" ],
					    [ 'class' => "col-sm-2", 'items' => ":RT" ],
					    [ 'class' => "col-sm-1", 'style' => "text-align:center;", 'items' => ":labelSlash" ],
					    [ 'class' => "col-sm-2", 'items' => ":RW" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":labelKecamatan" ],
					    [ 'class' => "col-sm-5", 'items' => ":Kecamatan" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":labelTelepon" ],
					    [ 'class' => "col-sm-6", 'style' => "padding-right:1px", 'items' => ":Telepon" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":labelKodePos" ],
					    [ 'class' => "col-sm-5", 'items' => ":KodePos" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":labelKelurahan" ],
					    [ 'class' => "col-sm-5", 'items' => ":Kelurahan" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":labelPemohonKTP" ],
					    [ 'class' => "col-sm-3", 'items' => ":Pemohon" ],
					    [ 'class' => "col-sm-3", 'style' => "padding-left:3px;", 'items' => ":NoKTPPemohon" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":labelPmhAlamat" ],
					    [ 'class' => "col-sm-13", 'items' => ":PmhAlamat" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelTypeTahun" ],
					    [ 'class' => "col-sm-4", 'items' => ":TypeTahun" ],
					    [ 'class' => "col-sm-2", 'style' => "padding-left:3px;", 'items' => ":Tahun" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelWarna" ],
					    [ 'class' => "col-sm-5", 'items' => ":Warna" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelPengirim" ],
					    [ 'class' => "col-sm-5", 'items' => ":Pengirim" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelNoMesin" ],
					    [ 'class' => "col-sm-6", 'items' => ":NoMesin" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelNoRangka" ],
					    [ 'class' => "col-sm-5", 'items' => ":NoRangka" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelHargaOTR" ],
					    [ 'class' => "col-sm-5", 'items' => ":HargaOTR" ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "col-sm-2", 'items' => ":LabelNamaMotor" ],
					    [ 'class' => "col-sm-6", 'items' => ":NamaMotor" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelKategori" ],
					    [ 'class' => "col-sm-5", 'items' => ":Kategori" ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "col-sm-2", 'items' => ":LabelNoRetur" ],
					    [ 'class' => "col-sm-5", 'items' => ":NoRetur" ],
				    ],
				  ],
                  [ 'class' => "form-group col-md-24", 'items' => [ 'class' => "row", 'items' => "<hr style='margin: 1px 5px;'>" ], ],
                  ['class' => "col-md-24",
                      'items'  => [
                          ['class' => "form-group col-md-3", 'items' => ":LabelZdeliveryDocumentId"],
                          ['class' => "form-group col-md-5", 'items' => ":ZdeliveryDocumentId"],
                          [ 'class' => "col-sm-1" ],
                          ['class' => "form-group col-md-3", 'items' => ":LabelZnamaPenerima"],
                          ['class' => "form-group col-md-4", 'items' => ":ZnamaPenerima"],
                          [ 'class' => "col-sm-1" ],
                          ['class' => "form-group col-md-3", 'items' => ":LabelZnoKontakPenerima"],
                          ['class' => "form-group col-md-4", 'items' => ":ZnoKontakPenerima"],


                      ],
                  ],
                  ['class' => "col-md-24",
                      'items'  => [
                          ['class' => "form-group col-md-3", 'items' => ":LabelZlokasiPengiriman"],
                          ['class' => "form-group col-md-13", 'items' => ":ZlokasiPengiriman"],
                          [ 'class' => "col-sm-1" ],
                          ['class' => "form-group col-md-3", 'items' => ":LabelZlatitude"],
                          ['class' => "form-group col-md-4", 'items' => ":Zlatitude"]
                      ],
                  ],
                  ['class' => "col-md-24",
                      'items'  => [
                          ['class' => "form-group col-md-3", 'items' => ":LabelDriverID"],
                          ['class' => "form-group col-md-13", 'items' => ":DriverID"],
                          [ 'class' => "col-sm-1" ],
                          ['class' => "form-group col-md-3", 'items' => ":LabelZlongitude"],
                          ['class' => "form-group col-md-4", 'items' => ":Zlongitude"],
                      ],
                  ],
			  ]
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
                  ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                      'items'  => [
                          ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":NoGL"],
                          ['class' => "col-sm-4 pull-right", 'items' => ":LabelNoGL"],
                      ],
                  ],
                  [
                      'class' => "col-md-12 pull-left",
                      'items' => $this->render( '../_nav', [
                          'url'=> $_GET['r'],
                          'options'=> [
                              'SKNo'       => 'No SK',
                              'LokasiKode' => 'Lokasi',
                              'SKPengirim' => 'Pengirim',
                              'SKMemo'     => 'Keterangan',
                              'NoGL'       => 'No Gl',
                              'UserID'     => 'User ID',
                              'SKCetak'    => 'Cetak',
                              'DKNo'       => 'No DK',
                              'CusNama'    => 'Nama Konsumen',
                              'MotorNoMesin'   => 'No Mesin',
                          ],
                      ])
                  ],
                  ['class' => "col-md-12 pull-right",
				    'items' => [
					    [ 'class' => "pull-right", 'items' => ":btnAttachment :btnImport :btnJurnal :btnPrint :btnBukuServis :btnKwitansi :btnAction " ]
				    ]
				  ]
			  ]
			],
			[
				":LabelSKNo"            => '<label class="control-label" style="margin: 0; padding: 6px 0;">No SK</label>',
				":LabelDKNo"            => '<label class="control-label" style="margin: 0; padding: 6px 0;">No DK</label>',
				":LabelTglDK"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl DK</label>',
				":LabelJenisDK"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis DK</label>',
				":LabelNoSPK"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">No SPK</label>',
				":LabelTglSK"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tanggal SK</label>',
				":LabelWarehouse"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Warehouse</label>',
				":LabelCetak"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">Cetak</label>',
				":LabelNoGL"            => \common\components\General::labelGL( $dsJual[ 'NoGL' ] ),
				":LabelNamaSales"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Sales</label>',
				":LabelTeam"            => '<label class="control-label" style="margin: 0; padding: 6px 0;">Team</label>',
				":LabelLeasing"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Leasing</label>',
				":LabelKeterangan"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelNoBuku"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Buku Servis</label>',
				":LabelTypeTahun"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Type / Tahun</label>',
				":LabelWarna"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">Warna</label>',
				":LabelPengirim"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pengirim</label>',
				":LabelNoMesin"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Mesin</label>',
				":LabelNoRangka"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Rangka</label>',
				":LabelHargaOTR"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Harga OTR</label>',
				":LabelNamaMotor"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Motor</label>',
				":LabelKategori"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kategori</label>',
				":LabelNoRetur"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Retur</label>',
				":labelKonsumen"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Konsumen</label>',
				":labelKTP"             => '<label class="control-label" style="margin: 0; padding: 6px 0;">KTP</label>',
				":labelAlamat"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
				":labelRTRW"            => '<label class="control-label" style="margin: 0; padding: 6px 0;">RT/RW</label>',
				":labelSlash"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">/</label>',
				":labelTelepon"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon</label>',
				":labelKodePos"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode POS</label>',
				":labelEmail"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">Email</label>',
				":labelStatusRumah"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status : Rumah</label>',
				":labelKabupaten"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kabupaten</label>',
				":labelKecamatan"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kecamatan</label>',
				":labelKelurahan"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kelurahan</label>',
				":labelKodeKons"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode Kons</label>',
				":labelPemohonKTP"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pemohon-KTP</label>',
				":labelPmhAlamat"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
				":labelKK"              => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Kartu Klrg</label>',
				":labelRepeatOrder"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Repeat Order</label>',
				":labelMerkSebelumnya"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Merk Motor Sebelumnya</label>',
				":labelJenisSebelumnya" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis Motor Sebelumnya</label>',
				":labelDigunakan"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Motor Digunakan Untuk</label>',
				":labelMenggunakan"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Yang Menggunakan Motor</label>',
				":labelStrip"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">-</label>',
				":SKNo"                 => Html::textInput( 'SKNoView', $dsJual[ 'SKNoView' ], [ 'class' => 'form-control', 'readonly' => true, 'tabindex' => '1' ] ) .
				                           Html::hiddenInput( 'SKNo', $dsJual[ 'SKNo' ] ).
				                           Html::hiddenInput( 'MotorAutoN', $dsJual[ 'MotorAutoN' ] ).
                                           Html::hiddenInput( 'StatPrint', null, [ 'id' => 'StatPrint' ] ),
				":DKNo"                 => Html::textInput( 'DKNo', $dsJual[ 'DKNo' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":JenisDK"              => Html::textInput( 'DKJenis', $dsJual[ 'DKJenis' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NoSPK"                => Html::textInput( 'SPKNo', $dsJual[ 'SPKNo' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":JamSK"                => FormField::timeInput( [ 'name' => 'SKJam', 'config' => [ 'value' => $dsJual[ 'SKJam' ], 'readonly' => true ], 'tabindex' => '3' ] ),
				":Warehouse"            => Html::textInput( 'LokasiKode', $dsJual[ 'LokasiKode' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Cetak"                => Html::textInput( 'SKCetak', $dsJual[ 'SKCetak' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NoGL"                 => Html::textInput( 'NoGL', $dsJual[ 'NoGL' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":TglDK"                => FormField::dateInput( [ 'name' => 'DKTgl', 'config' => [ 'value' => $dsJual[ 'DKTgl' ], 'readonly' => true ] ] ),
				":TglSK"                => FormField::dateInput( [ 'name' => 'SKTgl', 'config' => [ 'value' => $dsJual[ 'SKTgl' ], 'readonly' => true ] ] ),
				":NamaSales"            => Html::textInput( 'SalesNama', $dsJual[ 'SalesNama' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Team"                 => Html::textInput( 'TeamKode', $dsJual[ 'TeamKode' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Leasing"              => Html::textInput( 'LeaseKode', $dsJual[ 'LeaseKode' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Keterangan"           => Html::textInput( 'SKMemo', $dsJual[ 'SKMemo' ], [ 'class' => 'form-control', 'tabindex' => '2' ] ),
				":NoBuku"               => Html::textInput( 'NoBukuServis', $dsJual[ 'NoBukuServis' ], [ 'class' => 'form-control', 'tabindex' => '3' ] ),
				":TypeTahun"            => Html::textInput( 'MotorType', $dsJual[ 'MotorType' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Tahun"                => Html::textInput( 'MotorTahun', $dsJual[ 'MotorTahun' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Warna"                => Html::textInput( 'MotorWarna', $dsJual[ 'MotorWarna' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Pengirim"             => Html::textInput( 'SKPengirim', $dsJual[ 'SKPengirim' ], [ 'class' => 'form-control', 'tabindex' => '4' ] ),
				":NoMesin"              => Html::textInput( 'MotorNoMesin', $dsJual[ 'MotorNoMesin' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NoRangka"             => Html::textInput( 'MotorNoRangka', $dsJual[ 'MotorNoRangka' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":HargaOTR"             => FormField::decimalInput( [ 'name' => 'DKHarga', 'config' => [ 'value' => $dsJual[ 'DKHarga' ], 'readonly' => true ] ] ),
				":NamaMotor"            => Html::textInput( 'MotorNama', $dsJual[ 'MotorNama' ], [ 'class' => 'form-control', 'readonly' => true ] ) .
				                           Html::textInput( 'BBN', $dsJual[ 'BBN' ], [ 'class' => 'hidden' ] ),
				":Kategori"             => Html::textInput( 'MotorKategori', $dsJual[ 'MotorKategori' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NoRetur"              => Html::textInput( 'RKNo', $dsJual[ 'RKNo' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Konsumen"             => Html::textInput( 'CusNama', $dsJual[ 'CusNama' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":KTP"                  => Html::textInput( 'CusKTP', $dsJual[ 'CusKTP' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Alamat"               => Html::textInput( 'CusAlamat', $dsJual[ 'CusAlamat' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":RT"                   => Html::textInput( 'CusRT', $dsJual[ 'CusRT' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":RW"                   => Html::textInput( 'CusRW', $dsJual[ 'CusRW' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Telepon"              => Html::textInput( 'CusTelepon', $dsJual[ 'CusTelepon' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":KodePos"              => Html::textInput( 'CusKodePos', $dsJual[ 'CusKodePos' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Pemohon"              => Html::textInput( 'CusPembeliNama', $dsJual[ 'CusPembeliNama' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NoKTPPemohon"         => Html::textInput( 'CusPembeliKTP', $dsJual[ 'CusPembeliKTP' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":PmhAlamat"            => Html::textInput( 'CusPembeliAlamat', $dsJual[ 'CusPembeliAlamat' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Kabupaten"            => Html::textInput( 'CusKabupaten', $dsJual[ 'CusKabupaten' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Kecamatan"            => Html::textInput( 'CusKecamatan', $dsJual[ 'CusKecamatan' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Kelurahan"            => Html::textInput( 'CusKelurahan', $dsJual[ 'CusKelurahan' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":BtnSearchDatKon"      => Html::button( '<i class="glyphicon glyphicon-search"></i>', [ 'class' => 'btn btn-default ' . Custom::viewClass(), 'id' => 'BtnSearchDatkon' ] ),
				":BtnSearchMutasi"      => Html::button( '<i class="glyphicon glyphicon-search"></i>', [ 'class' => 'btn btn-default ' . Custom::viewClass(), 'id' => 'BtnSearchMutasi' ] ),

                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
                ":btnImport"      => Html::button('<i class="fa fa-arrow-circle-down"><br><br>Import</i>   ', ['class' => 'btn bg-navy ', 'id' => 'btnImport', 'style' => 'width:60px;height:60px']),
				":btnAttachment"  => Html::button('<i class="glyphicon glyphicon-file"><br><br>Lampiran</i>', ['class' => 'btn bg-black '.(($_GET[ 'action' ] ?? '') == 'create' ? 'hidden' : ''), 'id' => 'btnAttachment', 'style' => 'width:80px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),
				
				
				//                ":btnImport" => Html::button( '<i class="fa fa-arrow-circle-down"></i>  Import ', [ 'class' => 'btn bg-navy ', 'id' => 'btnImport', 'style' => 'width:80px' ] ),
				//                ":btnSave"              => Html::button( '<i class="glyphicon glyphicon-floppy-disk"></i> Simpan', [ 'class' => 'btn btn-info btn-primary ', 'id' => 'btnSave' . Custom::viewClass() ] ),
				//				":btnCancel"            => Html::Button( '<i class="fa fa-ban"></i> Batal', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:80px' ] ),
				//				":btnAdd"               => Html::button( '<i class="fa fa-plus-circle"></i> Tambah', [ 'class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:80px' ] ),
				//				":btnEdit"              => Html::button( '<i class="fa fa-edit"></i> Edit', [ 'class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:80px' ] ),
				//				":btnDaftar"            => Html::Button( '<i class="fa fa-list"></i> Daftar ', [ 'class' => 'btn btn-primary ', 'id' => 'btnDaftar', 'style' => 'width:80px' ] ),
				//				":btnPrint"             => Html::button( '<i class="glyphicon glyphicon-print"></i>  Print ', [ 'class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:80px' ] ),
				//				":btnJurnal"            => Html::button( '<i class="fa fa-balance-scale"></i>  Jurnal ', [ 'class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:80px' ] ),
                ":btnBukuServis"        => Html::button( '<i class="glyphicon glyphicon-print"><br><br>Buku Servis</i>   ', [ 'class' => 'btn bg-navy ', 'id' => 'btnBukuServis', 'style' => 'width:120px;height:60px' ] ),
                ":btnKwitansi"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Kwitansi</i>   ', ['class' => 'btn bg-teal '.($showKwitansi ? '' : 'hidden'), 'id' => 'btnKwitansi', 'style' => 'width:80px;height:60px']),

                ":LabelZdeliveryDocumentId"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Delivery Document ID</label>',
                ":LabelZlokasiPengiriman"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Lokasi Pengiriman</label>',
                ":LabelZlatitude"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Latitude</label>',
                ":LabelZlongitude"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Longitude</label>',
                ":LabelZnamaPenerima"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Penerima</label>',
                ":LabelZnoKontakPenerima"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Kontak Penerima</label>',
                ":LabelDriverID"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Driver ID</label>',

                ":ZdeliveryDocumentId"           => Html::textInput('ZdeliveryDocumentId', $dsJual[ 'ZdeliveryDocumentId' ], ['class' => 'form-control']),
                ":ZlokasiPengiriman"           => Html::textInput('ZlokasiPengiriman', $dsJual[ 'ZlokasiPengiriman' ], ['class' => 'form-control']),
                ":Zlatitude"           => Html::textInput('Zlatitude', $dsJual[ 'Zlatitude' ], ['class' => 'form-control']),
                ":Zlongitude"           => Html::textInput('Zlongitude', $dsJual[ 'Zlongitude' ], ['class' => 'form-control']),
                ":ZnamaPenerima"           => Html::textInput('ZnamaPenerima', $dsJual[ 'ZnamaPenerima' ], ['class' => 'form-control']),
                ":ZnoKontakPenerima"           => Html::textInput('ZnoKontakPenerima', $dsJual[ 'ZnoKontakPenerima' ], ['class' => 'form-control']),
                ":DriverID"           => FormField::combo( 'SalesKode', [ 'config' => [ 'value' => $dsJual[ 'SalesKodeSK' ] ], 'extraOptions' => [ 'altLabel' => [ 'SalesNama' ] ] ] ),
			],
            [
                '_akses' => 'Surat Jalan Konsumen',
                'url_main' => 'ttsk',
                'url_id' => $_GET['id'] ?? '',
                'url_delete'    => Url::toRoute(['ttsk/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
/** @var User $user */
$urlAdd            = Url::toRoute( [ 'ttsk/create','action' => 'create'  ] );
$urlCekKwitansi    = Url::toRoute( [ 'ttsk/cek-print-kwitansi'] );
$user              = User::findOne( Yii::$app->user->id );
$urlIndex          = Url::toRoute( [ 'ttsk/index' ] );
$urlJurnal         = Url::toRoute( [ 'ttsk/jurnal' ] );
$urlMutasiOtomatis = Url::toRoute( [ 'ttsk/mutasi-otomatis', 'SKNo' => $dsJual[ 'SKNo' ], 'SKId' => $id, 'SKNoView' => $dsJual[ 'SKNoView' ] ] );
$urlDatKonSelect   = Url::toRoute( [ 'ttdk/select-surat-jalan-konsumen', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ) ] );
$urlLoopDK         = Url::toRoute( [ 'ttsk/loop-dk' ] );
$urlDetail         = Url::toRoute( [ 'ttsk/import-loop' ] );
$urlSubmit         = Url::toRoute( [ 'ttsk/update', 'id' => base64_encode( $dsJual[ 'SKNo' ] ), 'action' => 'update' ] );
$urlLampiran 	   = Url::toRoute( [ 'ttsk/lampiran', 'id' => $id ] );
//$urlImport        = Url::toRoute( [ 'ttsk/import','tgl1'    => Yii::$app->formatter->asDate( '-5 day', 'yyyy-MM-dd') ] );
$urlImport        = Url::toRoute( [ 'ttsk/import','tgl1'    => Yii::$app->formatter->asDate( 'now', 'yyyy-MM-dd') ] );
$this->registerJsVar( '__isCreate', $isCreate );
$this->registerJsVar( 'BKodeAkses', $user->KodeAkses );

$urlPrint = $url[ 'print' ];
$this->registerJs( <<< JS

    function prosesPrint(StatPrint){
        $('#frm_ttsk_id').attr('action','$urlPrint');
        $('#frm_ttsk_id').attr('target','_blank');
        $('#StatPrint').val(StatPrint);     
        $('#frm_ttsk_id').submit();
        $( document ).focus(function() {
          setTimeout(function (){location.reload();}, 100);
        });
    }
    
    $('#btnPrint').click(function (event) {
		prosesPrint(1);
	}); 
    
    $('#btnBukuServis').click(function (event) {
		prosesPrint(2);
	}); 

	$('#btnKwitansi').click(function () {
		$.ajax({
			type: 'POST',
			url: '$urlCekKwitansi',
			data: {
				'SKNo' : "{$dsJual[ 'SKNo' ]}"
			}
		}).then(function (data) {
			if(!data.success){
				bootbox.alert(data.message);
			}else{
				prosesPrint(3);
			}
		});
    });

    $('#btnJurnal').click(function (event) {	
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#frm_ttsk_id').attr('action','{$urlJurnal}');
                    $('#frm_ttsk_id').submit();                     
                }
            }
        });
    });   

    $('#BtnSearchMutasi').click(function() {
        $('#modalMO').modal('show');
    });

    window.geturlDatKonSelect = function(){
        let DKNo = $('input[name="DKNo"]').val();
        return '{$urlDatKonSelect}' + ((DKNo === '' || DKNo === '--') ? '':'&DKNo='+DKNo);
    } 

    $('#BtnSearchDatkon').click(function () {
        // if (!__isCreate) return;
        window.util.app.dialogListData.show({
                title: 'Daftar Data Konsumen',
                url: geturlDatKonSelect()
            },
            function (data) {
                console.log(data);
                
            if(data.data === undefined){
                return;
            }
            $('[name="DKNo"]').val(data.data.DKNo);
                $.ajax({
                    type: 'POST',
                    url: '$urlLoopDK',
                    data: $('#frm_ttsk_id').serialize()
                }).then(function (cusData) { // console.log(data);
                    window.location.href = "{$url['update']}&oper=skip-load&SKNoView={$dsJual['SKNoView']}"; 
                });
            })
    });
    $('#btnSave').click(function (event) {
        let DKNo = $('input[name="DKNo"]').val();
        let CusNama = $('input[name="CusNama"]').val();
        let CusAlamat = $('input[name="CusAlamat"]').val();
        if (DKNo === "--" || DKNo === "" || CusNama === "" || CusNama === "--" || CusAlamat === "") {
            bootbox.alert("Anda belum menentukan Data Konsumen");
            return '';
        }
        let SKTgl = $('input[name="SKTgl"]').val();
        let TglDK = $('input[name="TglDK"]').val();
        let dSKTgl = new Date(SKTgl);
        let dTglDK = new Date(TglDK);
        if (dSKTgl < dTglDK) {
            bootbox.alert("Tanggal SK : " + SKTgl + " Lebih Kecil Dari Tanggal DK : " + TglDK);
            return '';
        }
        let BBN = $('input[name="BBN"]').val();
        if (BBN == 1) {
            bootbox.alert("Off the road dari SK ini belum mendapat persetujuan dari Korwil");
            return '';
        }
        if ($('input[name="DKNetto"]').val() < 0) {
            bootbox.alert("DKNetto minus.");
            return;
        }
        $('#frm_ttsk_id').attr('action','$urlSubmit');
        $('#frm_ttsk_id').submit();
      
    });
    
    
     $('#btnCancel').click(function (event) {	      
       $('#frm_ttsk_id').attr('action','{$url['cancel']}');
       $('#frm_ttsk_id').submit();
    });
     
     $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });  
    
     $('#btnImport').click(function (event) {	      
        window.util.app.dialogListData.show({
            title: 'Manage Delivery Process - BAST - SK',
            url: '{$urlImport}'
        },
        function (data) {
			console.log(data);
			if(!Array.isArray(data)) return;
			$.ajax({
				type: "POST",
				url: '$urlDetail',
				data: {
					oper: 'import',
					data: data,
					header: btoa($('#frm_ttsk_id').serialize())
				},
				success: function(data) {
					window.location.href = "{$url['update']}&oper=skip-load&SKNoView={$dsJual['SKNoView']}"; 
				},
			});
    	});
	});

	$('#btnAttachment').click(function (event) {
        window.location.href = '{$urlLampiran }';
    });

JS
);
Modal::begin( [
	'header'  => '<h5 class="modal-title">Mutasi</h5>',
	'id'      => 'modalMO',
	'options' => [
		'class' => 'bootbox'
	]
] );
echo "<div id='modalContent'>";
echo $this->render( '_formMutasi', [
	'data' => [
		'SKId'       => $id,
		'SKNoView'   => $dsJual[ 'SKNoView' ],
		'SKNo'       => $dsJual[ 'SKNo' ],
		'LokasiKode' => $dsJual[ 'LokasiKode' ],
		'MotorAutoN' => $dsJual[ 'MotorAutoN' ],
		'MotorNoMesin' => $dsJual[ 'MotorNoMesin' ],
		'MONo'       => str_replace( 'SK', 'MO', $dsJual[ 'SKNo' ] ),
	]
] );
echo "</div>";
Modal::end();
