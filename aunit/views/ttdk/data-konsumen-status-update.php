<?php
use common\components\Custom;
$jenis = 'Status';
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttdk */
$params                          = '&id=' . $id . '&action=update';
$cancel                          = \yii\helpers\Url::toRoute( [ 'ttdk/cancel', 'id' => $id, 'action' => 'update', 'index' => base64_encode( 'ttdk/data-konsumen-status' ) ] );
$this->title                     = str_replace( 'Menu', '', \aunit\components\TUi::$actionMode ) . ' - Data Konsumen Status  : ' . $dsTJualDatKonFill[ 'DKNoView' ];
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Data Konsumen Status ', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = str_replace( 'Menu', '', \aunit\components\TUi::$actionMode );
?>
<div class="ttdk-update">
	<?= $this->render( 'data-konsumen-status-form', [
		'model'             => $model,
		'dsTJualDatKonFill' => $dsTJualDatKonFill,
		'jenis'             => $jenis,
		'url'               => [
			'update' => Custom::url( 'ttdk/data-konsumen-status-update' . $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
			'cancel' => $cancel,
		]
	] ) ?>
</div>
