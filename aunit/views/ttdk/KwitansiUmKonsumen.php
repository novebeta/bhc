<?php
use aunit\assets\AppAsset;
use aunit\models\Ttdk;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = 'DK -> Kwitansi UM Konsumen';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'tdcustomer.CusNama'        => 'Nama Konsumen',
		'tdcustomer.CusPembeliNama' => 'Nama Pemohon',
		'ttdk.DKNo'                 => 'No Data Konsumen',
		'tmotor.SKNo'               => 'No Surat Jln Kons',
		'tmotor.MotorNoMesin'       => 'No Mesin',
		'tmotor.MotorNoRangka'      => 'No Rangka',
		'tmotor.MotorType'          => 'Type Motor',
		'tmotor.MotorWarna'         => 'Warna Motor',
		'tmotor.MotorTahun'         => 'Tahun Motor',
		'ttdk.LeaseKode'            => 'Leasing',
		'ttdk.TeamKode'             => 'Team',
		'ttdk.SalesKode'            => 'Kode Sales',
		'tdsales.SalesNama'         => 'Nama Sales',
		'ttdk.INNo'                 => 'No Inden',
		'ttdk.DKJenis'              => 'Jenis',
		'ttdk.DKPONo'               => 'No PO',
		'ttdk.SPKNo'                => 'No SPK',
		'ttdk.CusKode'              => 'Kode Konsumen',
		'tdcustomer.CusAlamat'      => 'Alamat Konsumen',
		'tdcustomer.CusRT'          => 'RT Konsumen',
		'tdcustomer.CusRW'          => 'RW Konsumen',
		'tdcustomer.CusKelurahan'   => 'Kelurahan',
		'tdcustomer.CusKecamatan'   => 'Kecamatan',
		'tdcustomer.CusKabupaten'   => 'Kabupaten',
		'tdcustomer.CusTelepon'     => 'Telepon',
		'tdcustomer.CusTelepon2'    => 'Telepon 2',
		'ttdk.DKMemo'               => 'Keterangan DK',
		'tmotor.MotorMemo'          => 'Keterangan Motor',
		'tmotor.STNKNo'             => 'No STNK',
		'tmotor.PlatNo'             => 'No Plat',
		'tmotor.BPKBNo'             => 'No BPKB',
		'tmotor.FakturAHMNo'        => 'No Faktur AHM',
		'tmotor.STNKNama'           => 'Nama STNK',
		'tmotor.STNKAlamat'         => 'Alamat STNK',
		'ttdk.UserID'               => 'User ID',
		'ttdk.DKLunas'              => 'Status Lunas',
		'ttdk.ProgramNama'          => 'Nama Program',
		'ttdk.NamaHadiah'           => 'Nama Hadiah',
		'tmotor.SSNo'               => 'No Surat Jln Supplier',
		'tmotor.FBNo'               => 'No Faktur Beli',
		'tmotor.PDNo'               => 'No Penerimaan Dealer',
		'tmotor.SDNo'               => 'No Surat Jln Dealer',
		'tmotor.NoticePenerima'     => 'Penerima Notice',
		'tmotor.STNKPenerima'       => 'Penerima STNK',
		'tmotor.PlatPenerima'       => 'Penerima Plat',
		'tmotor.BPKBPenerima'       => 'Penerima BPKB',
	],
	'cmbTgl'    => [
		'ttdk.DKTgl' => 'Tanggal DK',
	],
	'cmbNum'    => [
		'ttdk.DKHarga'         => 'Harga OTR',
		'ttdk.DKHPP'           => 'HPP',
		'ttdk.DKDPTotal'       => 'DP Total',
		'ttdk.DKDPLLeasing'    => 'DP Leasing',
		'ttdk.DKDPTerima'      => 'DP Terima',
		'ttdk.DKNetto'         => 'Netto',
		'ttdk.ProgramSubsidi'  => 'Subsidi Program',
		'ttdk.PrgSubsSupplier' => 'Subsidi Supplier',
		'ttdk.PrgSubsDealer'   => 'Subsidi Dealer',
		'ttdk.PrgSubsFincoy'   => 'Subsidi Fincoy',
		'ttdk.PotonganHarga'   => 'Potongan Harga',
		'ttdk.ReturHarga'      => 'Retur Harga',
		'ttdk.PotonganKhusus'  => 'Potongan Khusus',
		'ttdk.BBN'             => 'BBN',
		'ttdk.Jaket'           => 'Jaket',
	],
	'sortname'  => "DKTgl",
	'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttdk::class,
	'DK -> Kwitansi UM Konsumen', [
		'colGrid'            => Ttdk::colGridKwitansiKirimTagih(),
		'btnAdd_Disabled'    => true,
		'btnDelete_Disabled' => true,
		'btnUpdate_Disabled' => true,
		'url_view'           => \yii\helpers\Url::toRoute( [ 'ttdk/data-konsumen-update', 'mode' => 'kwitansi-um-konsumen', 'action' => 'view' ] ),
	] ), \yii\web\View::POS_READY );
