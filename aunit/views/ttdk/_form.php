<?php

use aunit\components\FormField;
use aunit\models\Tdsales;
use common\components\Custom;
use common\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttdk */
/* @var $form yii\widgets\ActiveForm */
/** @var $app yii\web\Application */
$isTunai = ($jenis == 'Tunai');
$urlIndex = Url::toRoute(['ttdk/' . ($isTunai ? 'data-konsumen-tunai' : 'data-konsumen-kredit')]);
//$urlImport        = Url::toRoute( [ 'ttdk/importdk','tgl1'    => Yii::$app->formatter->asDate( '-5 day', 'yyyy-MM-dd') ] );
$urlImport = Url::toRoute(['ttdk/importdk', 'tgl1' => Yii::$app->formatter->asDate('now', 'yyyy-MM-dd')]);
$urlDetail = Url::toRoute(['ttdk/import-loop']);
$perusahaan = $_SESSION['_company']['nama'];
$user = User::findOne(Yii::$app->user->id);
$akses = $user->KodeAkses;
$format = <<< SCRIPT
function formatttdk(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-6">' + result.id + '</div>' +
           '<div class="col-xs-18">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
$add = ($_GET['action'] == 'create');
$query = \aunit\models\Ttdk::find()->ComboMotorAddEdit($add, [])->andWhere("((ttdk.DKNo IS NULL AND ttsd.SDNo IS NULL)  
AND (vmotorLastlokasi.Kondisi = 'IN' AND vmotorLastlokasi.LokasiKode <> 'Repair'  
AND vmotorLastlokasi.Jam <= :jam)) AND LEFT(tmotor.FakturAHMNo,2) <> 'RK' 
OR (ttdk.DKNo = :DKNo) AND (tmotor.SSNo IS NULL OR tmotor.SSNo NOT LIKE '%=%')", [':jam' => date('Y-m-d') . ' 23:59:59', ':DKNo' => $dsTJualDatKonFill['DKNo']])
    ->orderBy("tmotor.MotorType, ttss.SSTgl, ttfb.FBTgl");
$array_motorType = $query->asArray()->all();
$this->registerJsVar('__dtMotor', $array_motorType);
$array_Sales = Tdsales::find()->select2('SalesKode', ['SalesNama'], ['SalesNama' => SORT_ASC], [
    'condition' => "SalesStatus ='A' OR SalesKode = :SalesKode", 'params' => [':SalesKode' => $dsTJualDatKonFill['SalesKode']]]);
$this->registerJsVar('__dtSales', $array_Sales);
if (isset($_GET['mode']) && $_GET['mode'] === 'kwitansi-kirim-tagih') {
    $url['cancel'] = Url::toRoute(['ttdk/kwitansi-kirim-tagih']);
}
?>
    <style>
        .nav-tabs-custom {
            margin-bottom: 5px;
        }

        .box-footer {
            padding: 0px 10px 5px 5px;
        }
    </style>
    <div class="ttdk-form">
        <?php
        $form = ActiveForm::begin(['id' => 'frm_ttdk_id', 'fieldConfig' => ['template' => "{input}"]]);
        \aunit\components\TUi::form(
            ['class' => "row-no-gutters",
                'items' => [
                    ['class' => "col-md-24",
                        'items' => [
                            ['class' => "form-group col-md-24",
                                'items' => [
                                    ['class' => "col-sm-1", 'items' => ":labelNomor"],
                                    ['class' => "col-sm-4", 'items' => ":Nomor"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-2", 'items' => ":labelSales"],
                                    ['class' => "col-sm-9", 'items' => ":Sales"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-1", 'items' => ":labelTeam"],
                                    ['class' => "col-sm-5", 'style' => "padding-left:5px", 'items' => ":Team"],
                                ],
                            ],
                            ['class' => "form-group col-md-24",
                                'items' => [
                                    ['class' => "col-sm-1", 'items' => ":labelTgl"],
                                    ['class' => "col-sm-4", 'items' => ":Tgl"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-2", 'items' => ":labelNoInden"],
                                    ['class' => "col-sm-3", 'items' => ":NoInden"],
                                    ['class' => "col-sm-1", 'items' => ":BtnSearchInden"],
                                    ['class' => "col-sm-1", 'style' => "padding-left:5px", 'items' => ":labelLeasing"],
                                    ['class' => "col-sm-4", 'style' => 'padding-left:10px', 'items' => ":Leasing"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-1", 'items' => ":labelSPK"],
                                    ['class' => "col-sm-5", 'style' => "padding-left:5px", 'items' => ":SPK"],
                                ],
                            ],
                            //NAVBAR-MENU
                            ['class' => "nav-tabs-custom", 'style' => "height: 100%;",
                                'items' => [
                                    ['class' => "form-group col-md-13",
                                        'items' => [
                                            ['tag' => "ul", 'class' => "nav nav-tabs ",
                                                'items' => [
                                                    '<li class="active"><a href="#konsumen" data-toggle="tab">Konsumen</a></li>',
                                                    '<li><a href="#motor" data-toggle="tab">Motor</a></li>',
                                                ]
                                            ],
                                        ],
                                    ],
                                    ['class' => "form-group col-md-11",
                                        'items' => [
                                            ['class' => "form-group col-md-4", 'style' => "padding-left:5px", 'items' => ":labelSurveyor"],
                                            ['class' => "form-group col-md-7", 'style' => "padding-left:5px", 'items' => ":Surveyor"],
                                            ['class' => "col-sm-2"],
                                            ['class' => "form-group col-md-2", 'items' => ":labelNoSK"],
                                            ['class' => "form-group col-md-9", 'style' => "padding-left:5px;", 'items' => ":NoSK"],
                                        ],
                                    ],
                                    ['class' => "tab-content",
                                        'items' => [
                                            //KONSUMEN-FORM
                                            ['class' => "chart tab-pane active",
                                                'id' => "konsumen",
                                                'items' => [
                                                    ['class' => "form-group col-md-24",
                                                        'items' => [
                                                            ['class' => "form-group col-md-9",
                                                                'items' => [
                                                                    ['class' => "col-sm-7", 'items' => ":labelKonsumen"],
                                                                    ['class' => "col-sm-16", 'items' => ":Konsumen"],
                                                                    ['class' => "col-sm-1"],
                                                                ],
                                                            ],
                                                            ['class' => "form-group col-md-9",
                                                                'items' => [
                                                                    ['class' => "col-sm-3", 'items' => ":labelKTP"],
                                                                    ['class' => "col-sm-14", 'items' => ":KTP"],
                                                                    ['class' => "col-sm-3", 'items' => ":BtnNewCus"],
                                                                    ['class' => "col-sm-3", 'items' => ":BtnSearchFB"],
                                                                    ['class' => "col-sm-1"],
                                                                ],
                                                            ],
                                                            ['class' => "form-group col-md-6",
                                                                'items' => [
                                                                    ['class' => "col-sm-7", 'items' => ":labelKabupaten"],
                                                                    ['class' => "col-sm-17", 'items' => ":Kabupaten"],
                                                                ],
                                                            ],
                                                        ],
                                                    ],
                                                    ['class' => "form-group col-md-24",
                                                        'items' => [
                                                            ['class' => "form-group col-md-9",
                                                                'items' => [
                                                                    ['class' => "col-sm-7", 'items' => ":labelAlamat"],
                                                                    ['class' => "col-sm-16", 'items' => ":Alamat"],
                                                                    ['class' => "col-sm-1"],
                                                                ],
                                                            ],
                                                            ['class' => "form-group col-md-9",
                                                                'items' => [
                                                                    ['class' => "col-sm-3", 'items' => ":labelRTRW"],
                                                                    ['class' => "col-sm-9", 'items' => ":RT"],
                                                                    ['class' => "col-sm-2", 'style' => "text-align:center;", 'items' => ":labelSlash"],
                                                                    ['class' => "col-sm-9", 'items' => ":RW"],
                                                                    ['class' => "col-sm-1"],
                                                                ],
                                                            ],
                                                            ['class' => "form-group col-md-6",
                                                                'items' => [
                                                                    ['class' => "col-sm-7", 'items' => ":labelKecamatan"],
                                                                    ['class' => "col-sm-17", 'items' => ":Kecamatan"],
                                                                ],
                                                            ],
                                                        ],
                                                    ],
                                                    [
                                                        'class' => "form-group col-md-24",
                                                        'items' => [
                                                            [
                                                                'class' => "form-group col-md-9",
                                                                'items' => [
                                                                    ['class' => "col-sm-7", 'items' => ":labelTelepon"],
                                                                    ['class' => "col-sm-8", 'style' => "padding-right:1px", 'items' => ":Telepon"],
                                                                    ['class' => "col-sm-8", 'style' => "padding-right:1px", 'items' => ":CusTelepon2"],
                                                                    ['class' => "col-sm-1"],
                                                                ],
                                                            ],
                                                            [
                                                                'class' => "form-group col-md-9",
                                                                'items' => [
                                                                    ['class' => "col-sm-8", 'items' => ":JenisTelepon"],
                                                                    ['class' => "col-sm-1"],
                                                                    ['class' => "col-sm-5", 'items' => ":labelKodePos"],
                                                                    ['class' => "col-sm-9", 'items' => ":KodePos"],
                                                                    ['class' => "col-sm-1"],
                                                                ],
                                                            ],
                                                            [
                                                                'class' => "form-group col-md-6",
                                                                'items' => [
                                                                    ['class' => "col-sm-7", 'items' => ":labelKelurahan"],
                                                                    ['class' => "col-sm-17", 'items' => ":Kelurahan"],
                                                                ],
                                                            ],
                                                        ],
                                                    ],
                                                    ['class' => "form-group col-md-24",
                                                        'items' => [
                                                            ['class' => "form-group col-md-9",
                                                                'items' => [
                                                                    ['class' => "col-sm-7", 'items' => ":labelEmail"],
                                                                    ['class' => "col-sm-16", 'items' => ":Email"],
                                                                    ['class' => "col-sm-1"],
                                                                ],
                                                            ],
                                                            ['class' => "form-group col-md-9",
                                                                'items' => [
                                                                    ['class' => "col-sm-7", 'items' => ":labelStatusRumah"],
                                                                    ['class' => "col-sm-16", 'items' => ":StatusRumah"],
                                                                    ['class' => "col-sm-1"],
                                                                ],
                                                            ],
                                                            ['class' => "form-group col-md-6",
                                                                'items' => [
                                                                    ['class' => "col-sm-7", 'items' => ":labelKodeKons"],
                                                                    ['class' => "col-sm-17", 'items' => ":KodeKons"],
                                                                ],
                                                            ],
                                                        ],
                                                    ],
                                                    ['class' => "form-group col-md-24",
                                                        'items' => [
                                                            ['class' => "form-group col-md-9",
                                                                'items' => [
                                                                    ['class' => "col-sm-7", 'items' => ":labelJenisKelamin"],
                                                                    ['class' => "col-sm-16", 'items' => ":JenisKelamin"],
                                                                    ['class' => "col-sm-1"],
                                                                ],
                                                            ],
                                                            ['class' => "form-group col-md-9",
                                                                'items' => [
                                                                    ['class' => "col-sm-7", 'items' => ":labelTempatTglLahir"],
                                                                    ['class' => "col-sm-9", 'items' => ":TempatLahir"],
                                                                    ['class' => "col-sm-1", 'style' => "text-align:center;", 'items' => ":labelStrip"],
                                                                    ['class' => "col-sm-6", 'items' => ":CusTglLhr"],
                                                                    ['class' => "col-sm-1"],
                                                                ],
                                                            ],
                                                            ['class' => "form-group col-md-6",
                                                                'items' => [
                                                                    ['class' => "col-sm-7", 'items' => ":labelAgama"],
                                                                    ['class' => "col-sm-17", 'items' => ":Agama"],
                                                                ],
                                                            ],
                                                        ],
                                                    ],
                                                    ['class' => "form-group col-md-24",
                                                        'items' => [
                                                            ['class' => "form-group col-md-9",
                                                                'items' => [
                                                                    ['class' => "col-sm-7", 'items' => ":labelPekerjaan"],
                                                                    ['class' => "col-sm-16", 'items' => ":Pekerjaan"],
                                                                    ['class' => "col-sm-1"],
                                                                ],
                                                            ],
                                                            ['class' => "form-group col-md-9",
                                                                'items' => [
                                                                    ['class' => "col-sm-7", 'items' => ":labelPendidikan"],
                                                                    ['class' => "col-sm-9", 'items' => ":Pendidikan"],
                                                                    ['class' => "col-sm-2"],
                                                                    ['class' => "col-sm-5", 'items' => ":labelPengeluaran"],
                                                                ],
                                                            ],
                                                            ['class' => "form-group col-md-6",
                                                                'items' => [
                                                                    ['class' => "col-sm-24", 'items' => ":Pengeluaran"],
                                                                ],
                                                            ],
                                                        ],
                                                    ],
                                                    ['class' => "form-group col-md-13",
                                                        'items' => [
                                                            ['class' => "col-sm-5", 'items' => ":labelPemohonKTP"],
                                                            ['class' => "col-sm-11", 'style' => "margin-buttom: 3px;", 'items' => ":Pemohon"],
                                                            ['class' => "col-sm-1"],
                                                            ['class' => "col-sm-7", 'style' => "margin-buttom: 3px;", 'items' => ":NoKTPPemohon"],
                                                            ['class' => "col-sm-5", 'items' => ":labelKK"],
                                                            ['class' => "col-sm-11", 'style' => "margin-top: 3px;", 'items' => ":KK"],
                                                            ['class' => "col-sm-1"],
                                                            ['class' => "col-sm-4", 'items' => ":labelRepeatOrder"],
                                                            ['class' => "col-sm-3", 'style' => "margin-top: 3px;", 'items' => ":RepeatOrder"],
                                                        ],
                                                    ],
                                                    ['class' => "form-group col-md-11",
                                                        'items' => [
                                                            ['class' => "col-sm-1"],
                                                            ['class' => "col-sm-3", 'items' => ":labelPmhAlamat"],
                                                            ['class' => "col-sm-20", 'style' => "margin-top: 3px;", 'items' => ":PmhAlamat"],
                                                            ['class' => "col-sm-1"],
                                                            ['class' => "col-sm-3", 'items' => ":labelROKK"],
                                                            ['class' => "col-sm-4", 'style' => "margin-top: 3px;", 'items' => ":ROKK"],
                                                            ['class' => "col-sm-1"],
                                                            ['class' => "col-sm-3", 'items' => ":labelCusNPWP"],
                                                            ['class' => "col-sm-12", 'style' => "margin-top: 3px;", 'items' => ":CusNPWP"],
                                                        ],
                                                    ],
                                                    //								          [ 'class' => "form-group col-md-13",
                                                    //								            'items' => [
                                                    //									            [ 'class' => "col-sm-5", 'items' => ":labelKK" ],
                                                    //									            [ 'class' => "col-sm-11", 'style' => "margin-top: 3px;", 'items' => ":KK" ],
                                                    //									            [ 'class' => "col-sm-1" ],
                                                    //									            [ 'class' => "col-sm-4", 'items' => ":labelRepeatOrder" ],
                                                    //									            [ 'class' => "col-sm-3", 'style' => "margin-top: 3px;", 'items' => ":RepeatOrder" ],
                                                    //								            ],
                                                    //								          ],
                                                    //								          [ 'class' => "form-group col-md-11",
                                                    //								            'items' => [
                                                    //								            ],
                                                    //								          ],
                                                    ['class' => "form-group col-md-24", 'items' => ['class' => "row", 'items' => "<hr style='margin: 1px 5px;'>"],],
                                                    ['class' => "form-group col-md-24",
                                                        'items' => [
                                                            ['class' => "col-sm-4", 'items' => ":labelMerkSebelumnya"],
                                                            ['class' => "col-sm-4", 'items' => ":MerkSebelumnya"],
                                                            ['class' => "col-sm-1"],
                                                            ['class' => "col-sm-4", 'items' => ":labelDigunakan"],
                                                            ['class' => "col-sm-4", 'items' => ":Digunakan"],
                                                            ['class' => "col-sm-1"],
                                                            ['class' => "col-sm-2", 'items' => ":LabelZidSpk"],
                                                            ['class' => "col-sm-4", 'items' => ":ZidSpk"],
                                                        ],
                                                    ],
                                                    ['class' => "form-group col-md-24",
                                                        'items' => [
                                                            ['class' => "col-sm-4", 'items' => ":labelJenisSebelumnya"],
                                                            ['class' => "col-sm-4", 'items' => ":JenisSebelumnya"],
                                                            ['class' => "col-sm-1"],
                                                            ['class' => "col-sm-4", 'items' => ":labelMenggunakan"],
                                                            ['class' => "col-sm-4", 'items' => ":Menggunakan"],
                                                            ['class' => "col-sm-1"],
                                                            ['class' => "col-sm-2", 'items' => ":LabelZidProspect"],
                                                            ['class' => "col-sm-4", 'items' => ":ZidProspect"]
                                                        ],
                                                    ],
                                                ]
                                            ],
                                            //MOTOR-FORM
                                            ['class' => "chart tab-pane", 'id' => "motor",
                                                'items' => [
                                                    ['class' => "form-group col-md-24",
                                                        'items' => [
                                                            ['class' => "col-sm-3", 'items' => ":labelType"],
                                                            ['class' => "col-sm-4", 'items' => ":TypeMotor"],
                                                            ['class' => "col-sm-2", 'style' => "padding-left:2px;", 'items' => ":MotorTahun"],
                                                            ['class' => "col-sm-1"],
                                                            ['class' => "col-sm-2", 'items' => ":labelWarna"],
                                                            ['class' => "col-sm-4", 'items' => ":WarnaMotor"],
                                                            ['class' => "col-sm-1", 'items' => ":BtnSearchMotor"],
                                                            ['class' => "col-sm-1"],
                                                            ['class' => "col-sm-2", 'items' => ":labelSCP"],
                                                            ['class' => "col-sm-4", 'items' => ":SCP"],
                                                        ],
                                                    ],
                                                    ['class' => "form-group col-md-24",
                                                        'items' => [
                                                            ['class' => "col-sm-3", 'items' => ":labelMesinNo"],
                                                            ['class' => "col-sm-6", 'items' => ":NoMesin"],
                                                            ['class' => "col-sm-1"],
                                                            ['class' => "col-sm-2", 'items' => ":labelNoRangka"],
                                                            ['class' => "col-sm-5", 'items' => ":NoRangka"],
                                                            ['class' => "col-sm-1"],
                                                            ['class' => "col-sm-2", 'items' => ":labelScheme1"],
                                                            ['class' => "col-sm-4", 'items' => ":Scheme1"],
                                                        ],
                                                    ],
                                                    ['class' => "form-group col-md-24",
                                                        'items' => [
                                                            ['class' => "col-sm-3", 'items' => ":labelNamaMotor"],
                                                            ['class' => "col-sm-6", 'items' => ":NamaMotor"],
                                                            ['class' => "col-sm-1"],
                                                            ['class' => "col-sm-2", 'items' => ":labelKategori"],
                                                            ['class' => "col-sm-5", 'items' => ":Kategori"],
                                                            ['class' => "col-sm-1"],
                                                            ['class' => "col-sm-2", 'items' => ":labelScheme2"],
                                                            ['class' => "col-sm-4", 'items' => ":Scheme2"],
                                                        ],
                                                    ],
                                                    ['class' => "form-group col-md-24", 'items' => ['class' => "row", 'items' => "<hr style='margin: 1px 5px;'>"],],
                                                    ['class' => "form-group col-md-24",
                                                        'items' => [
                                                            ['class' => "col-sm-3", 'items' => ":labelProgram"],
                                                            ['class' => "col-sm-3", 'items' => ":Program"],
                                                            ['class' => "col-sm-1", 'items' => ":BtnSearchProg"],
                                                            ['class' => "col-sm-2", 'items' => ":labelPengajuanBBN"],
                                                            ['class' => "col-sm-3", 'items' => ":PengajuanBBN"],
                                                            ['class' => "col-sm-3", 'items' => ":labelDPPO", 'style' => "padding-left:25px;"],
                                                            ['class' => "col-sm-3", 'items' => ":DPPO"],
                                                            ['class' => "col-sm-3", 'items' => ":labelHargaOTR", 'style' => "padding-left:25px;"],
                                                            ['class' => "col-sm-3", 'items' => ":HargaOTR"],
                                                        ],
                                                    ],
                                                    ['class' => "form-group col-md-18", 'items' => ['class' => "row", 'items' => "<hr style='margin: 1px 5px;'>"],],
                                                    ['class' => "form-group col-md-24",
                                                        'items' => [
                                                            ['class' => "col-sm-3", 'items' => ":labelSubsidiSupplier"],
                                                            ['class' => "col-sm-3", 'items' => ":SubsidiSupplier"],
                                                            ['class' => "col-sm-3", 'items' => ":labelNetSubDealer1", 'style' => "padding-left:10px;"],
                                                            ['class' => "col-sm-3", 'items' => ":NetSubDealer"],
                                                            ['class' => "col-sm-3", 'items' => ":labelSubsidiFincoy", 'style' => "padding-left:10px;"],
                                                            ['class' => "col-sm-3", 'items' => ":SubsidiFincoy"],
                                                            ['class' => "col-sm-3", 'items' => ":labelSubsidiProgram", 'style' => "padding-left:10px;"],
                                                            ['class' => "col-sm-3", 'items' => ":SubsidiProgram"],
                                                        ],
                                                    ],
                                                    ['class' => "form-group col-md-24",
                                                        'items' => [
                                                            ['class' => "col-sm-3", 'items' => ":labelPiutangKonsUM"],
                                                            ['class' => "col-sm-3", 'items' => ":PiutangKonsUM"],
                                                            ['class' => "col-sm-3", 'items' => ":labelUMInden", 'style' => "padding-left:10px;"],
                                                            ['class' => "col-sm-3", 'items' => ":UMInden"],
                                                            ['class' => "col-sm-3", 'items' => ":labelUMDiterima", 'style' => "padding-left:10px;"],
                                                            ['class' => "col-sm-3", 'items' => ":UMDiterima"],
                                                            ['class' => "col-sm-3", 'items' => ":labelUMTotal", 'style' => "padding-left:10px;"],
                                                            ['class' => "col-sm-3", 'items' => ":UMTotal"],
                                                        ],
                                                    ],
                                                    ['class' => "form-group col-md-18", 'items' => ['class' => "row", 'items' => "<hr style='margin: 1px 5px;'>"],],
                                                    ['class' => "form-group col-md-24",
                                                        'items' => [
                                                            ['class' => "col-sm-3", 'items' => ":labelReturHarga"],
                                                            ['class' => "col-sm-3", 'items' => ":ReturHarga"],
                                                            ['class' => "col-sm-1", 'style' => "padding-left:10px;", 'items' => ":labelMinus"],
                                                            ['class' => "col-sm-5", 'items' => ":MinusM"],
                                                            ['class' => "col-sm-3", 'items' => ":labelSubDealer2", 'style' => "padding-left:25px;"],
                                                            ['class' => "col-sm-3", 'items' => ":SubDealer2"],
                                                            ['class' => "col-sm-3", 'items' => ":labelSisaPiutLease", 'style' => "padding-left:25px;"],
                                                            ['class' => "col-sm-3", 'items' => ":SisaPiutLease"],
                                                        ],
                                                    ],
                                                    ['class' => "form-group col-md-24",
                                                        'items' => [
                                                            ['class' => "col-sm-3", 'items' => ":labelInsentifSales"],
                                                            ['class' => "col-sm-3", 'items' => ":InsentifSales"],
                                                            ['class' => "col-sm-3", 'items' => ":labelBiayaBBN", 'style' => "padding-left:25px;"],
                                                            ['class' => "col-sm-3", 'items' => ":BiayaBBN"],
                                                            ['class' => "col-sm-3", 'items' => ":labelBBNProgresif", 'style' => "padding-left:25px;"],
                                                            ['class' => "col-sm-3", 'items' => ":BBNProgresif"],
                                                            ['class' => "col-sm-3", 'items' => ":labelTenor", 'style' => "padding-left:25px;"],
                                                            ['class' => "col-sm-2", 'items' => ":Tenor"],
                                                            ['class' => "col-sm-1", 'style' => "text-align:center", 'items' => ":labelBulan"],
                                                        ],
                                                    ],
                                                    ['class' => "form-group col-md-24", 'style' => "margin-bottom: 2px;",
                                                        'items' => [
                                                            ['class' => "col-sm-3", 'items' => ":labelPtgKhusus"],
                                                            ['class' => "col-sm-3", 'items' => ":PtgKhusus"],
                                                            ['class' => "col-sm-3", 'items' => ":labelBiayaJaket", 'style' => "padding-left:25px;"],
                                                            ['class' => "col-sm-3", 'items' => ":BiayaJaket"],
                                                            ['class' => "col-sm-3", 'items' => ":labelHadiah", 'style' => "padding-left:25px;"],
                                                            ['class' => "col-sm-3", 'items' => ":Hadiah"],
                                                            ['class' => "col-sm-3", 'items' => ":labelAngsuran", 'style' => "padding-left:25px;"],
                                                            ['class' => "col-sm-3", 'items' => ":Angsuran"],
                                                        ],
                                                    ],
                                                    ['class' => "form-group col-md-24",
                                                        'items' => [
                                                            ['class' => "col-sm-3", 'items' => ":labelKeterangan"],
                                                            ['class' => "col-sm-9", 'items' => ":Keterangan"],
                                                            ['class' => "col-sm-2", 'items' => ":labelDPTOP", 'style' => "padding-left:25px;"],
                                                            ['class' => "col-sm-1"],
                                                            ['class' => "col-sm-3", 'items' => ":Hadiah2"],
                                                            ['class' => "col-sm-3", 'items' => ":labelTOP", 'style' => "padding-left:25px;"],
                                                            ['class' => "col-sm-2", 'items' => ":TOP"],
                                                            ['class' => "col-sm-1"],
                                                            ['class' => "col-sm-1", 'style' => "text-align:center;", 'items' => ":labelHari"],
                                                        ],
                                                    ],
                                                ]
                                            ],
                                        ]
                                    ],
                                ],
                            ],
                        ],
                    ],
                ]
            ],
            ['class' => "row-no-gutters",
                'items' => [
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render('../_nav', [
                            'url' => $_GET['r'],
                            'options' => [
                                'CusNama' => 'Nama Konsumen',
                                'CusPembeliNama' => 'Nama Pemohon',
                                'DKNo' => 'No Data Konsumen',
                                'SKNo' => 'No Surat Jln Kons',
                                'MotorNoMesin' => 'No Mesin',
                                'MotorNoRangka' => 'No Rangka',
                                'MotorType' => 'Type Motor',
                                'MotorWarna' => 'Warna Motor',
                                'MotorTahun' => 'Tahun Motor',
                                'LeaseKode' => 'Leasing',
                                'TeamKode' => 'Team',
                                'SalesKode' => 'Kode Sales',
                                'tdsales.SalesNama' => 'Nama Sales',
                                'INNo' => 'No Inden',
                                'DKJenis' => 'Jenis',
                                'DKPONo' => 'No PO',
                                'SPKNo' => 'No SPK',
                                'CusKode' => 'Kode Konsumen',
                                'CusAlamat' => 'Alamat Konsumen',
                                'CusRT' => 'RT Konsumen',
                                'CusRW' => 'RW Konsumen',
                                'CusKelurahan' => 'Kelurahan',
                                'CusKecamatan' => 'Kecamatan',
                                'CusKabupaten' => 'Kabupaten',
                                'CusTelepon' => 'Telepon',
                                'CusTelepon2' => 'Telepon 2',
                                'DKMemo' => 'Keterangan DK',
                                'MotorMemo' => 'Keterangan Motor',
                                'STNKNo' => 'No STNK',
                                'PlatNo' => 'No Plat',
                                'BPKBNo' => 'No BPKB',
                                'FakturAHMNo' => 'No Faktur AHM',
                                'STNKNama' => 'Nama STNK',
                                'STNKAlamat' => 'Alamat STNK',
                                'UserID' => 'User ID',
                                'DKLunas' => 'Status Lunas',
                                'ProgramNama' => 'Nama Program',
                                'NamaHadiah' => 'Nama Hadiah',
                                'SSNo' => 'No Surat Jln Supplier',
                                'FBNo' => 'No Faktur Beli',
                                'PDNo' => 'No Penerimaan Dealer',
                                'SDNo' => 'No Surat Jln Dealer',
                                'NoticePenerima' => 'Penerima Notice',
                                'STNKPenerima' => 'Penerima STNK',
                                'PlatPenerima' => 'Penerima Plat',
                                'BPKBPenerima' => 'Penerima BPKB',
                            ],
                        ])
                    ],
                    ['class' => "pull-right", 'items' => ":btnAttachment :btnPrint :btnImport :btnAction"],
                ]
            ],

            [
                //customer-component
                ":labelMinus" => '<label class="control-label" style="margin: 0; padding: 6px 0;">-</label>',
                ":labelNomor" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor</label>',
                ":labelSales" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sales</label>',
                ":labelTeam" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Team</label>',
                ":labelTgl" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl</label>',
                ":labelNoInden" => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Inden</label>',
                ":labelLeasing" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Leasing</label>',
                ":labelSPK" => '<label class="control-label" style="margin: 0; padding: 6px 0;">No SPK</label>',
                ":labelNoSK" => '<label class="control-label" style="margin: 0; padding: 6px 0;">No SK</label>',
                ":labelSurveyor" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Surveyor</label>',
                ":labelKonsumen" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Konsumen</label>',
                ":labelKTP" => '<label class="control-label" style="margin: 0; padding: 6px 0;">KTP</label>',
                ":labelAlamat" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
                ":labelRTRW" => '<label class="control-label" style="margin: 0; padding: 6px 0;">RT/RW</label>',
                ":labelSlash" => '<label class="control-label" style="margin: 0; padding: 6px 0;">/</label>',
                ":labelTelepon" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon</label>',
                ":labelKodePos" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode POS</label>',
                ":labelEmail" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Email</label>',
                ":labelStatusRumah" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status : Rumah</label>',
                ":labelKabupaten" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kabupaten</label>',
                ":labelKecamatan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kecamatan</label>',
                ":labelKelurahan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kelurahan</label>',
                ":labelKodeKons" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode Kons</label>',
                ":labelJenisKelamin" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis Kelamin</label>',
                ":labelTempatTglLahir" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tempat - Tgl Lahir</label>',
                ":labelAgama" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Agama</label>',
                ":labelPekerjaan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pekerjaan</label>',
                ":labelPendidikan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pendidikan</label>',
                ":labelPengeluaran" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pengeluaran</label>',
                ":labelPemohonKTP" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pemohon-KTP</label>',
                ":labelPmhAlamat" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
                ":labelKK" => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Kartu Klrg</label>',
                ":labelCusNPWP" => '<label class="control-label" style="margin: 0; padding: 6px 0;">NPWP Kons</label>',
                ":labelRepeatOrder" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Repeat Order</label>',
                ":labelMerkSebelumnya" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Merk Motor Sebelumnya</label>',
                ":labelJenisSebelumnya" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis Motor Sebelumnya</label>',
                ":labelDigunakan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Motor Digunakan Untuk</label>',
                ":labelMenggunakan" => (strpos($perusahaan, 'CV. ANUGERAH PERDANA') !== false) ? '<label class="control-label" style="margin: 0; padding: 6px 0;">Biaya Plat Putih</label>' : '<label class="control-label" style="margin: 0; padding: 6px 0;">Yang Menggunakan Motor</label>',
                ":labelROKK" => '<label class="control-label" style="margin: 0; padding: 6px 0;">RO KK</label>',

                ":labelStrip" => '<label class="control-label" style="margin: 0; padding: 6px 0;">-</label>',
                ":Nomor" => Html::textInput('DKNoView', $dsTJualDatKonFill['DKNoView'], ['class' => 'form-control', 'tabindex' => '1']) .
                    Html::hiddenInput('DKNo', $dsTJualDatKonFill['DKNo'], ['id' => 'DKNo']) .
                    Html::hiddenInput('MotorAutoN', $dsTJualDatKonFill['MotorAutoN'], ['id' => 'MotorAutoN']) .
                    Html::hiddenInput('DKJenis', $dsTJualDatKonFill['DKJenis'], ['id' => 'DKJenis']) .
                    Html::hiddenInput('Nominal', 0, ['id' => 'nominal_kirim_tagih']) .
                    Html::hiddenInput('Keterangan', '', ['id' => 'keterangan_kirim_tagih']) .
                    Html::hiddenInput('StatPrint', ($jenis == 'Kredit') ? '1' : '2', ['id' => 'StatPrint']),
                ":Sales" => '<select id="SalesKode" name="SalesKode" style="width: 100%" tabindex="3"></select>',
                ":Tgl" => FormField::dateInput(['name' => 'DKTgl', 'config' => ['value' => $dsTJualDatKonFill['DKTgl'], 'options' => ['tabindex' => '2']]]),
                ":Team" => Html::textInput('TeamKode', $dsTJualDatKonFill['TeamKode'], ['id' => 'TeamKodeId', 'class' => 'form-control', 'tabindex' => '4', 'readonly' => true]),
                ":NoSPK" => Html::textInput('SPKNo', $dsTJualDatKonFill['SPKNo'], ['class' => 'form-control']),
                ":NoInden" => Html::textInput('INNo', $dsTJualDatKonFill['INNo'], ['class' => 'form-control', 'tabindex' => '5', 'readonly' => true]),
//                ":Leasing"              => ( $jenis == 'Tunai' ) ? FormField::combo( 'LeaseKode', [ 'config' => [ 'value' => $dsTJualDatKonFill[ 'LeaseKode' ], 'options' => [ 'tabindex' => '6' ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ) : FormField::combo( 'LeaseKode', [ 'config' => [ 'value' => $dsTJualDatKonFill[ 'LeaseKode' ], 'options' => ['tabindex'=>'6'] ], 'extraOptions' => [ 'simpleCombo' => true, 'altCondition' => "LeaseKode NOT LIKE 'TUNAI' " ] ] ),


                ":Leasing"              =>
                (($_GET['action'] == 'update' && $akses == 'Auditor' || $_GET['action'] == 'create' && $akses == 'Auditor' ) ?
                FormField::combo( 'LeaseKode', [ 'config' => [ 'value' => $dsTJualDatKonFill[ 'LeaseKode' ], 'options' => [ 'tabindex' => '6' ] ], 'extraOptions' => [ 'simpleCombo' => true, 'altCondition' => "LeaseStatus = 'A' " ] ] )
                : (($_GET['action'] == 'update' && $akses !== 'Auditor' ) ? Html::textInput( 'LeaseKode', $dsTJualDatKonFill[ 'LeaseKode' ], [ 'class' => 'form-control', 'readOnly' => true, 'tabindex' => '6' ] )
                : FormField::combo( 'LeaseKode', [ 'config' => [ 'value' => $dsTJualDatKonFill[ 'LeaseKode' ], 'options' => [ 'tabindex' => '6' ] ], 'extraOptions' => [ 'simpleCombo' => true, 'altCondition' => "LeaseStatus = 'A'"] ] ))),



//                ":Leasing" => ($akses == 'Auditor') ? FormField::combo('LeaseKode', ['config' => ['value' => $dsTJualDatKonFill['LeaseKode'], 'options' => ['id' => 'LeaseKode', 'tabindex' => '6']], 'extraOptions' => ['simpleCombo' => true]]) : Html::textInput('LeaseKode', $dsTJualDatKonFill['LeaseKode'], ['class' => 'form-control', 'readOnly' => true, 'tabindex' => '6','id' => 'LeaseKode']),
//				":Leasing"              => ( $jenis == 'Tunai' ) ? Html::textInput( 'LeaseKode', $dsTJualDatKonFill[ 'LeaseKode' ], [ 'class' => 'form-control', 'readOnly' => true, 'tabindex' => '6' ] ) : FormField::combo( 'LeaseKode', [ 'config' => [ 'value' => $dsTJualDatKonFill[ 'LeaseKode' ], 'options' => ['tabindex'=>'6'] ], 'extraOptions' => [ 'simpleCombo' => true, 'altCondition' => "LeaseKode NOT LIKE 'TUNAI' " ] ] ),
//				":Leasing"              => FormField::combo( 'LeaseKode', [ 'config' => [ 'value' => $dsTJualDatKonFill[ 'LeaseKode' ], 'options' => [ 'tabindex' => '6' ] ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
                ":SPK" => Html::textInput('SPKNo', $dsTJualDatKonFill['SPKNo'], ['class' => 'form-control', 'tabindex' => '7']),
                ":Surveyor" => Html::textInput('DKSurveyor', $dsTJualDatKonFill['DKSurveyor'], ['class' => 'form-control', 'tabindex' => '8']),
                ":NoSK" => Html::textInput('SKNo', $dsTJualDatKonFill['SKNo'], ['class' => 'form-control', 'tabindex' => '9']),
                ":Konsumen" => Html::textInput('CusNama', $dsTJualDatKonFill['CusNama'], ['class' => 'form-control', 'maxlength' => 50, 'tabindex' => '10']) . Html::textInput('CusKode', $dsTJualDatKonFill['CusKode'], ['class' => 'hidden']),
                ":KTP" => MaskedInput::widget(['name' => 'CusKTP', 'value' => $dsTJualDatKonFill['CusKTP'], 'mask' => '999999-999999-9999', 'options' => ['tabindex' => '11', 'class' => 'form-control']]),
                ":Alamat" => Html::textInput('CusAlamat', $dsTJualDatKonFill['CusAlamat'], ['class' => 'form-control', 'maxlength' => 75, 'tabindex' => '14']),
                ":RT" => Html::textInput('CusRT', $dsTJualDatKonFill['CusRT'], ['class' => 'form-control', 'maxlength' => 3, 'tabindex' => '15']),
                ":RW" => Html::textInput('CusRW', $dsTJualDatKonFill['CusRW'], ['class' => 'form-control', 'maxlength' => 3, 'tabindex' => '16']),
                ":Telepon" => Html::textInput('CusTelepon', $dsTJualDatKonFill['CusTelepon'], ['class' => 'form-control', 'maxlength' => 30, 'tabindex' => '21']),
                ":CusTelepon2" => Html::textInput('CusTelepon2', $dsTJualDatKonFill['CusTelepon2'], ['class' => 'form-control', 'maxlength' => 30, 'tabindex' => '22']),
                ":Email" => MaskedInput::widget(['name' => 'CusEmail', 'value' => $dsTJualDatKonFill['CusEmail'], 'clientOptions' => ['alias' => "email"], 'options' => ['tabindex' => '24', 'class' => 'form-control']]),
                ":StatusRumah" => FormField::combo('CusStatusRumah', ['name' => 'CusStatusRumah', 'config' => ['value' => $dsTJualDatKonFill['CusStatusRumah']], 'options' => ['tabindex' => '25'], 'extraOptions' => ['simpleCombo' => true]]),
                ":JenisKelamin" => FormField::combo('CusSex', ['name' => 'CusSex', 'config' => ['value' => $dsTJualDatKonFill['CusSex']], 'options' => ['tabindex' => '27'], 'extraOptions' => ['simpleCombo' => true]]),
                ":TempatLahir" => Html::textInput('CusTempatLhr', $dsTJualDatKonFill['CusTempatLhr'], ['class' => 'form-control', 'maxlength' => 30, 'tabindex' => '28']),
                ":CusTglLhr" => FormField::dateInput(['name' => 'CusTglLhr', 'config' => ['value' => $dsTJualDatKonFill['CusTglLhr'], 'options' => ['tabindex' => '29']]]),
                ":Agama" => FormField::combo('CusAgama', ['name' => 'CusAgama', 'config' => ['value' => $dsTJualDatKonFill['CusAgama']], 'options' => ['tabindex' => '30'], 'extraOptions' => ['simpleCombo' => true]]),
                ":Pekerjaan" => FormField::combo('CusPekerjaan', ['name' => 'CusPekerjaan', 'config' => ['value' => $dsTJualDatKonFill['CusPekerjaan']], 'options' => ['tabindex' => '31'], 'extraOptions' => ['simpleCombo' => true]]),
                ":Pendidikan" => FormField::combo('CusPendidikan', ['name' => 'CusPendidikan', 'config' => ['value' => $dsTJualDatKonFill['CusPendidikan']], 'options' => ['tabindex' => '32'], 'extraOptions' => ['simpleCombo' => true]]),
                ":Pengeluaran" => FormField::combo('CusPengeluaran', ['name' => 'CusPengeluaran', 'config' => ['value' => $dsTJualDatKonFill['CusPengeluaran']], 'options' => ['tabindex' => '33'], 'extraOptions' => ['simpleCombo' => true]]),
                ":Pemohon" => Html::textInput('CusPembeliNama', $dsTJualDatKonFill['CusPembeliNama'], ['class' => 'form-control', 'maxlength' => 50, 'tabindex' => '34']),
                ":NoKTPPemohon" => MaskedInput::widget(['name' => 'CusPembeliKTP', 'value' => $dsTJualDatKonFill['CusPembeliKTP'], 'mask' => '999999-999999-9999', 'options' => ['tabindex' => '35', 'class' => 'form-control']]),
                ":PmhAlamat" => Html::textInput('CusPembeliAlamat', $dsTJualDatKonFill['CusPembeliAlamat'], ['id' => 'CusPembeliAlamat', 'class' => 'form-control', 'rows' => '3', 'maxlength' => 125, 'tabindex' => '36']),
                ":KK" => Html::textInput('CusKK', $dsTJualDatKonFill['CusKK'], ['class' => 'form-control', 'maxlength' => 16, 'tabindex' => '42']),
                ":CusNPWP" => Html::textInput('CusNPWP', $dsTJualDatKonFill['CusNPWP'], ['class' => 'form-control', 'maxlength' => 20, 'tabindex' => '43']),
                ":RepeatOrder" => FormField::integerInput(['name' => 'ROCount', 'config' => ['value' => $dsTJualDatKonFill['ROCount'], 'readonly' => true], 'options' => ['tabindex' => '41']]),
                ":Kabupaten" => FormField::combo('Kabupaten', ['name' => 'CusKabupaten', 'config' => ['value' => $dsTJualDatKonFill['CusKabupaten']], 'options' => ['tabindex' => '17'], 'extraOptions' => ['KecamatanId' => 'Kecamatan_id', 'KecamatanValue' => $dsTJualDatKonFill['CusKecamatan'], 'simpleCombo' => true]]),
                ":Kecamatan" => FormField::combo('Kecamatan', ['name' => 'CusKecamatan', 'config' => ['value' => $dsTJualDatKonFill['CusKecamatan']], 'options' => ['id' => 'Kecamatan_id', 'tabindex' => '18'], 'extraOptions' => ['KelurahanId' => 'Kelurahan_id', 'KelurahanValue' => $dsTJualDatKonFill['CusKelurahan'], 'simpleCombo' => true]]),
                ":Kelurahan" => FormField::combo('Kelurahan', ['name' => 'CusKelurahan', 'config' => ['value' => $dsTJualDatKonFill['CusKelurahan']], 'options' => ['id' => 'Kelurahan_id', 'tabindex' => '19'], 'extraOptions' => ['KodePosId' => 'KodePos_id', 'KodePosValue' => $dsTJualDatKonFill['CusKodePos'], 'simpleCombo' => true]]),
                ":KodePos" => FormField::combo('KodePos', ['name' => 'CusKodePos', 'config' => ['value' => $dsTJualDatKonFill['CusKodePos']], 'options' => ['id' => 'KodePos_id', 'tabindex' => '20'], 'extraOptions' => ['simpleCombo' => true]]),
                ":JenisTelepon" => FormField::combo('CusStatusHP', ['name' => 'CusStatusHP', 'config' => ['value' => $dsTJualDatKonFill['CusStatusHP']], 'options' => ['tabindex' => '23'], 'extraOptions' => ['simpleCombo' => true]]),
                ":KodeKons" => FormField::combo('CusKodeKons', ['name' => 'CusKodeKons', 'config' => ['value' => $dsTJualDatKonFill['CusKodeKons']], 'options' => ['tabindex' => '26'], 'extraOptions' => ['simpleCombo' => true]]),
                ":MerkSebelumnya" => FormField::combo('DKMerk', ['config' => ['value' => $dsTJualDatKonFill['DKMerkSblmnya']], 'options' => ['tabindex' => '37'], 'extraOptions' => ['simpleCombo' => true]]),
                ":JenisSebelumnya" => FormField::combo('DKJenis', ['name' => 'DKJenisSblmnya', 'config' => ['value' => $dsTJualDatKonFill['DKJenisSblmnya']], 'options' => ['tabindex' => '38'], 'extraOptions' => ['simpleCombo' => true]]),
                ":Digunakan" => FormField::combo('DKMotorUntuk', ['config' => ['value' => $dsTJualDatKonFill['DKMotorUntuk']], 'options' => ['tabindex' => '39'], 'extraOptions' => ['simpleCombo' => true]]),
                ":Menggunakan" => FormField::combo('DKMotorOleh', ['config' => ['value' => $dsTJualDatKonFill['DKMotorOleh']], 'options' => ['tabindex' => '40'], 'extraOptions' => ['simpleCombo' => true]]),
                ":ROKK" => Html::textInput('ROKK', $dsTJualDatKonFill['CusNPWP'], ['class' => 'form-control', 'readonly' => true]),
                //motor-component
                ":labelType" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Type / Tahun</label>',
                ":labelWarna" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Warna</label>',
                ":labelSCP" => '<label class="control-label" style="margin: 0; padding: 6px 0;">SCP</label>',
                ":labelMesinNo" => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Mesin</label>',
                ":labelNoRangka" => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Rangka</label>',
                ":labelScheme1" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Scheme 1</label>',
                ":labelNamaMotor" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Motor</label>',
                ":labelKategori" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kategori</label>',
                ":labelScheme2" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Scheme 2</label>',
                ":labelProgram" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Program</label>',
                ":labelPengajuanBBN" => '<label class="control-label" style="margin: 0; padding: 6px 0;">PengajuanBBN</label>',
                ":labelDPPO" => '<label class="control-label" style="margin: 0; padding: 6px 0;">DP PO</label>',
                ":labelHargaOTR" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Harga OTR</label>',
                ":labelSubsidiSupplier" => '<label class="control-label" style="margin: 0; padding: 6px 0;"> Subsidi Supplier</label>',
                ":labelNetSubDealer1" => '<label class="control-label" style="margin: 0; padding: 6px 0;"> + &nbsp Net Sub Dealer 1</label>',
                ":labelSubsidiFincoy" => '<label class="control-label" style="margin: 0; padding: 6px 0;"> + &nbsp Subsidi Fincoy</label>',
                ":labelSubsidiProgram" => '<label class="control-label" style="margin: 0; padding: 6px 0;"> = &nbsp Subsidi Program</label>',
                ":labelPiutangKonsUM" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Piutang Kons UM</label>',
                ":labelUMInden" => '<label class="control-label" style="margin: 0; padding: 6px 0;"> + &nbsp UM Inden</label>',
                ":labelUMDiterima" => '<label class="control-label" style="margin: 0; padding: 6px 0;"> + &nbsp UM Diterima</label>',
                ":labelUMTotal" => '<label class="control-label" style="margin: 0; padding: 6px 0;"> = &nbsp UM Total</label>',
                ":labelReturHarga" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tamb P H-M</label>',
                ":labelSubDealer2" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sub Dealer 2</label>',
                ":labelSisaPiutLease" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sisa Piut Lease</label>',
                ":labelInsentifSales" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Insentif Sales</label>',
                ":labelBiayaBBN" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Biaya BBN</label>',
                ":labelTenor" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tenor</label>',
                ":labelPtgKhusus" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Ptg Khusus</label>',
                ":labelBiayaJaket" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Biaya Jaket</label>',
                ":labelHadiah" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Hadiah</label>',
                ":labelDPTOP" => '<label class="control-label" style="margin: 0; padding: 6px 0;">DP TOP</label>',
                ":labelAngsuran" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Angsuran</label>',
                ":labelKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
                ":labelTOP" => '<label class="control-label" style="margin: 0; padding: 6px 0;">TOP</label>',
                ":labelHari" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Hari</label>',
                ":labelBulan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bulan</label>',
                ":labelBBNProgresif" => '<label class="control-label" style="margin: 0; padding: 6px 0;">BBN Progresif</label>',
                ":TypeMotor" => '<select id="MotorType_id" name="MotorType" style="width: 100%"></select>',
                ":WarnaMotor" => '<select id="MotorWarna_id" name="MotorWarna" style="width: 100%"></select>',
                ":NoMesin" => '<select id="MotorNoMesin_id" name="MotorNoMesin" style="width: 100%"></select>',
                ":NoRangka" => '<select id="MotorNoRangka_id" name="MotorNoRangka" style="width: 100%"></select>',
                ":MotorTahun" => Html::textInput('MotorTahun', $dsTJualDatKonFill['MotorTahun'], ['id' => 'MotorTahun', 'class' => 'form-control']),
                ":NamaMotor" => Html::textInput('MotorNama', $dsTJualDatKonFill['MotorNama'], ['class' => 'form-control', 'readonly' => true]),
                ":Kategori" => Html::textInput('MotorKategori', $dsTJualDatKonFill['MotorKategori'], ['class' => 'form-control', 'readonly' => true]),
                ":SCP" => FormField::decimalInput(['name' => 'DKSCP', 'config' => ['value' => $dsTJualDatKonFill['DKSCP']]]),
                ":Scheme1" => $isTunai && $akses !== 'Admin' ?
                    FormField::decimalInput(['name' => 'DKScheme', 'config' => ['value' => $dsTJualDatKonFill['DKScheme'],'readonly' => true] ])
                    : FormField::decimalInput(['name' => 'DKScheme', 'config' => ['value' => $dsTJualDatKonFill['DKScheme']]]) ,
//                ":Scheme1" => FormField::decimalInput(['name' => 'DKScheme', 'config' => ['value' => $dsTJualDatKonFill['DKScheme']] ]) ,
                ":Scheme2" => $isTunai  && $akses !== 'Admin' ? FormField::decimalInput(['name' => 'DKScheme2', 'config' => ['value' => $dsTJualDatKonFill['DKScheme2'],'readonly' => true]]) : FormField::decimalInput(['name' => 'DKScheme2', 'config' => ['value' => $dsTJualDatKonFill['DKScheme2']]]),
//                ":Scheme2" => FormField::decimalInput(['name' => 'DKScheme2', 'config' => ['value' => $dsTJualDatKonFill['DKScheme2']]]),
                ":Program" => '<select id="ProgramNama" name="ProgramNama" style="width: 100%"></select>',
                ":PengajuanBBN" => Html::textInput('DKPengajuanBBN', $dsTJualDatKonFill['DKPengajuanBBN'], ['class' => 'form-control', 'readonly' => true]),
                ":DPPO" => FormField::decimalInput(['name' => 'NDPPO', 'config' => ['value' => 0, 'displayOptions' => ['class' => 'hitung'], 'readonly' => true]]),
                ":SubsidiSupplier" => FormField::decimalInput(['name' => 'PrgSubsSupplier', 'config' => ['value' => $dsTJualDatKonFill['PrgSubsSupplier'], 'displayOptions' => ['class' => 'hitung']]]),
                ":NetSubDealer" => FormField::decimalInput(['name' => 'PrgSubsDealer', 'config' => ['value' => $dsTJualDatKonFill['PrgSubsDealer'], 'displayOptions' => ['class' => 'hitung']]]),
                ":SubsidiFincoy" => $isTunai && $akses !== 'Admin' ? FormField::decimalInput(['name' => 'PrgSubsFincoy', 'config' => ['value' => $dsTJualDatKonFill['PrgSubsFincoy'], 'displayOptions' => ['class' => 'hitung'],'readonly' => true]])  : FormField::decimalInput(['name' => 'PrgSubsFincoy', 'config' => ['value' => $dsTJualDatKonFill['PrgSubsFincoy'], 'displayOptions' => ['class' => 'hitung']]]),
//                ":SubsidiFincoy" => FormField::decimalInput(['name' => 'PrgSubsFincoy', 'config' => ['value' => $dsTJualDatKonFill['PrgSubsFincoy'], 'displayOptions' => ['class' => 'hitung']]]),
                ":SubsidiProgram" => FormField::decimalInput(['name' => 'ProgramSubsidi', 'config' => ['value' => $dsTJualDatKonFill['ProgramSubsidi']]]),
                ":PiutangKonsUM" => FormField::decimalInput(['name' => 'DKDPLLeasing', 'config' => ['value' => $dsTJualDatKonFill['DKDPLLeasing'], 'displayOptions' => ['class' => 'hitung']]]),
                ":UMInden" => FormField::decimalInput(['name' => 'DKDPInden', 'config' => ['value' => $dsTJualDatKonFill['DKDPInden'], 'displayOptions' => ['class' => 'hitung'], 'readonly' => true]]),
                ":UMDiterima" => FormField::decimalInput(['name' => 'DKDPTerima', 'config' => ['value' => $dsTJualDatKonFill['DKDPTerima'], 'displayOptions' => ['class' => 'hitung']]]),
                ":UMTotal" => FormField::decimalInput(['name' => 'DKDPTotal', 'config' => ['value' => $dsTJualDatKonFill['DKDPTotal'], 'readonly' => true]]),
                ":HargaOTR" => FormField::decimalInput(['name' => 'DKHarga', 'config' => ['id' => 'DKHarga', 'value' => $dsTJualDatKonFill['DKHarga'], 'displayOptions' => ['class' => 'hitung']]]),
                ":ReturHarga" => FormField::decimalInput(['name' => 'ReturHarga', 'config' => ['value' => $dsTJualDatKonFill['ReturHarga']]]),
                ":MinusM" => Html::textInput('DKMediator', $dsTJualDatKonFill['DKMediator'], ['class' => 'form-control']),
                ":SubDealer2" => FormField::decimalInput(['name' => 'PotonganHarga', 'config' => ['value' => $dsTJualDatKonFill['PotonganHarga']]]),
                ":SisaPiutLease" => FormField::decimalInput(['name' => 'DKNetto', 'config' => ['value' => $dsTJualDatKonFill['DKNetto'], 'readonly' => true]]),
//				":Hadiah2"              => ' <input type="text"  class="form-control" name="Hadiah2">',
                ":Hadiah2" => FormField::decimalInput(['name' => 'DPTOP', 'config' => ['value' => $dsTJualDatKonFill['DPTOP']]]),
                ":InsentifSales" => FormField::decimalInput(['name' => 'InsentifSales', 'config' => ['value' => $dsTJualDatKonFill['InsentifSales']]]),
                ":BiayaBBN" => FormField::decimalInput(['name' => 'BBN', 'config' => ['id' => 'BBN', 'value' => $dsTJualDatKonFill['BBN'], 'disabled' => !\aunit\components\Menu::showOnlyHakAkses(['auditor', 'admin'])]]),
                ":BBNProgresif" => FormField::decimalInput(['name' => 'BBNPlus', 'config' => ['value' => $dsTJualDatKonFill['BBNPlus']]]),
                ":Tenor" => FormField::integerInput(['name' => 'DKTenor', 'config' => ['value' => $dsTJualDatKonFill['DKTenor']]]),
                ":PtgKhusus" => FormField::decimalInput(['name' => 'PotonganKhusus', 'config' => ['value' => $dsTJualDatKonFill['PotonganKhusus']]]),
                ":BiayaJaket" => FormField::decimalInput(['name' => 'Jaket', 'config' => ['value' => $dsTJualDatKonFill['Jaket']]]),
                ":Angsuran" => FormField::decimalInput(['name' => 'DKAngsuran', 'config' => ['value' => $dsTJualDatKonFill['DKAngsuran']]]),
                ":TOP" => FormField::integerInput(['name' => 'DKTOP', 'config' => ['value' => $dsTJualDatKonFill['DKTOP']]]),
                ":Hadiah" => Html::textInput('NamaHadiah', $dsTJualDatKonFill['NamaHadiah'], ['class' => 'form-control']),
                ":Keterangan" => Html::textInput('DKMemo', $dsTJualDatKonFill['DKMemo'], ['class' => 'form-control']),
                ":BtnNewCus" => Html::button('<i class="fa fa-user-plus"></i>', ['class' => 'btn btn-default' . Custom::viewClass(), 'id' => 'NewCusId', 'tabindex' => '12']),
                ":BtnSearchInden" => Html::button('<i class="glyphicon glyphicon-search"></i>', ['class' => 'btn btn-default' . Custom::viewClass(), 'id' => 'BtnSearchInden', 'tabindex' => '5']),
                ":BtnSearchFB" => Html::button('<i class="glyphicon glyphicon-search"></i>', ['class' => 'btn btn-default' . Custom::viewClass(), 'id' => 'BtnSearchFB', 'tabindex' => '13']),
                ":BtnSearchProg" => Html::button('<i class="glyphicon glyphicon-search"></i>', ['class' => 'btn btn-default' . Custom::viewClass(), 'id' => 'BtnSearchProg']),
                ":BtnSearchMotor" => Html::button('<i class="glyphicon glyphicon-search"></i>', ['class' => 'btn btn-default' . Custom::viewClass(), 'id' => 'BtnSearchMotor']),

                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
                ":btnAttachment"       => Html::button('<i class="glyphicon glyphicon-file"><br><br>Lampiran</i>', ['class' => 'btn bg-black '.(($_GET[ 'action' ] ?? '') == 'create' ? 'hidden' : ''), 'id' => 'btnAttachment', 'style' => 'width:80px;height:60px']),
                ":btnImport" => Html::button('<i class="fa fa-arrow-circle-down"><br><br>Import</i>   ', ['class' => 'btn bg-navy ', 'id' => 'btnImport', 'style' => 'width:60px;height:60px']),
                ":btnPrint" => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
//                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),


//                ":btnSave"              => Html::button( '<i class="glyphicon glyphicon-floppy-disk"></i> Simpan', [ 'class' => 'btn btn-info btn-primary ', 'id' => 'btnSave' . Custom::viewClass() ] ),
//				":btnCancel"            => Html::Button( '<i class="fa fa-ban"></i> Batal', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:80px' ] ),
//				":btnAdd"               => Html::button( '<i class="fa fa-plus-circle"></i> Tambah', [ 'class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:80px' ] ),
//				":btnEdit"              => Html::button( '<i class="fa fa-edit"></i> Edit', [ 'class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:80px' ] ),
//				":btnDaftar"            => Html::Button( '<i class="fa fa-list"></i> Daftar ', [ 'class' => 'btn btn-primary ', 'id' => 'btnDaftar', 'style' => 'width:80px' ] ),
//                ":btnImport"            => Html::button( '<i class="fa fa-arrow-circle-down"></i>  Import ', [ 'class' => 'btn bg-navy ', 'id' => 'btnImport', 'style' => 'width:80px' ] ),
//                ":btnPrint"             => Html::button( '<i class="glyphicon glyphicon-print"></i>  Print ', [ 'class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:80px' ] ),

                ":LabelZidSpk" => '<label class="control-label" style="margin: 0; padding: 6px 0;">ID SPK</label>',
                ":LabelZidProspect" => '<label class="control-label" style="margin: 0; padding: 6px 0;">PRSP</label>',
                ":ZidSpk" => Html::textInput('ZidSpk', $dsTJualDatKonFill['ZidSpk'], ['class' => 'form-control']),
                ":ZidProspect" => Html::textInput('ZidProspect', $dsTJualDatKonFill['ZidProspect'], ['class' => 'form-control']),
            ], [
                'url_delete' => Url::toRoute(['ttdk/delete', 'id' => $_GET['id'] ?? '', 'index' => base64_encode("ttdk/" . ($isTunai ? 'data-konsumen-tunai' : 'data-konsumen-kredit'))]),
            ]
        );
        ActiveForm::end();
        ?>
    </div>
<?php
/** @var User $user */
if (isset($_GET['mode'])) {
    $urlIndex = Url::toRoute(['ttdk/' . $_GET['mode']]);
}
$urlAdd = Url::toRoute(['ttdk/' . ($isTunai ? 'data-konsumen-tunai' : 'data-konsumen-kredit') . '-create', 'action' => 'create']);
$urlPrint = $url['print'];
//$user        = User::findOne( Yii::$app->user->id );
$urlBBNvalue = Url::toRoute(['tdbbn/get-bbn-value']);
$urlPrograms = Url::toRoute(['tdprogram/get-combo']);
$this->registerJsVar('BKodeAkses', $user->KodeAkses);
$this->registerJsVar('__Jenis', $jenis);
$this->registerJsVar('dsTJualDatKonFill', $dsTJualDatKonFill);
$this->registerJs($this->render('_form.js', [
    'url_submit' => Url::toRoute(['ttdk/data-konsumen-validate']),
    'urlCust' => Url::toRoute(['tdcustomer/select']),
    'urlDataCust' => Url::toRoute(['tdcustomer/td']),
    'urlDataInden' => Url::toRoute(['ttin/td']),
    'urlUpdate' => $url['update'],
]));
$urlFB = Url::toRoute([
    'tdcustomer/select',
    'cmbTgl1' => 'CusTglBeli',
    'tgl1' => Yii::$app->formatter->asDate('-60 day', 'yyyy-MM-dd')
]);
$urlInden = Url::toRoute(['ttin/select-dk', 'DKNo' => $dsTJualDatKonFill['DKNo'], 'tgl1' => Yii::$app->formatter->asDate('-90 day', 'yyyy-MM-dd')]);
$urlLoop = Url::toRoute(['ttdk/loop']);
$urlLoopIN = Url::toRoute(['ttdk/loop-in']);
//$urlLampiran = Url::toRoute( [ 'ttdk/lampiran', 'id' => $_GET['id'] ] );
$urlLampiran = Url::toRoute( [ 'ttdk/lampiran', 'id' => $_GET['id'] ?? '' ] );
$this->registerJs(<<< JS

    $('[name=CusPembeliNama]').keydown(function( event ) {
      if ( event.which !== 114 )  return;
         event.preventDefault();
         $(this).val($('[name=CusNama]').val());
          $('[name=CusPembeliKTP]').val($('[name=CusKTP]').val());
          $('[name=CusPembeliAlamat]').val($('[name=CusAlamat]').val()+' '+$('[name=CusRT]').val() + '/'+$('[name=CusRW]').val() +
          ', Kec '+$('[name=CusKecamatan]').val() + ', Kab '+$('[name=CusKabupaten]').val());
    });

    $('#BtnSearchFB').click(function() {
        window.util.app.dialogListData.show({
        title: 'Daftar Konsumen',
        url: '$urlFB'
    },
    function (data) {
            if(data.data === undefined){
                return;
            }
            $('input[name="CusKode"]').val(data.data.CusKode);
        $.ajax({
            type: 'POST',
            url: '$urlLoop',
            data: $('#frm_ttdk_id').serialize()
        }).then(function (result) {
           window.location.href = "{$url['update']}&oper=skip-load&DKNoView={$dsTJualDatKonFill['DKNoView']}";      
        });
        })
    });    

    $('#BtnSearchInden').click(function() {
        window.util.app.dialogListData.show({
                title: 'Daftar Inden',
                url: '{$urlInden}'
            },
            function (data) {
            if(data.data == undefined){
                return;
            }
                // console.log(data);
                
            $('input[name="INNo"]').val(data.data.INNo);
                 $.ajax({
                    type: 'POST',
                    url: '$urlLoopIN',
                    data: $('#frm_ttdk_id').serialize()
                }).then(function (result) {
                   window.location.href = "{$url['update']}&oper=skip-load&DKNoView={$dsTJualDatKonFill['DKNoView']}";      
                });
            })
    });

    $('[id="SalesKode"]').select2({
        data: $.map(__dtSales, function (obj) {
            obj.id = obj.value;
            obj.text = obj.label;
            return obj;
        }),
        templateResult: formatttdk,
        templateSelection: formatttdk,
        matcher: matchCustom,
        escapeMarkup: function(m) { return m; },
    }).on('select2:select', function (e) {
        var data = e.params.data;
        $('#TeamKodeId').val(data.TeamKode);
    });

    $('[id="MotorType_id"]').select2({
        data: $.map(__dtMotor, function (obj) {
            obj.id = obj.MotorType;
            obj.text = obj.MotorType;
            return obj;
        }),

    });
    
    $('[id="MotorWarna_id"]').select2({
        data: $.map(__dtMotor, function (obj) {
            obj.id = obj.MotorWarna;
            obj.text = obj.MotorWarna;
            return obj;
        }),
//        placeholder: {
//            id: '{$dsTJualDatKonFill["MotorWarna"]}'
//        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        $('[name="MotorTahun"]').val(data.MotorTahun);
        $('[name="MotorNama"]').val(data.MotorNama);
        $('[name="MotorKategori"]').val(data.MotorKategori);
        $('[name="MotorAutoN"]').val(data.MotorAutoN);
        $('[name="DKHarga"]').utilNumberControl().val(data.MotorHrgJual);
        $('[id="MotorNoMesin_id"]').val(data.MotorNoMesin).trigger('change');
        $('[id="MotorNoRangka_id"]').val(data.MotorNoRangka).trigger('change');
        $('[id="MotorType_id"]').val(data.MotorType).trigger('change');
        window.Hitung();
    });
    $('[id="MotorNoRangka_id"]').select2({
        data: $.map(__dtMotor, function (obj) {
            obj.id = obj.MotorNoRangka;
            obj.text = obj.MotorNoRangka;
            return obj;
        }),
    }).on('select2:select', function (e) {
        var data = e.params.data;
        $('[name="MotorTahun"]').val(data.MotorTahun);
        $('[name="MotorNama"]').val(data.MotorNama);
        $('[name="MotorKategori"]').val(data.MotorKategori);
        $('[name="MotorAutoN"]').val(data.MotorAutoN);
        $('[name="DKHarga"]').utilNumberControl().val(data.MotorHrgJual);
        $('[id="MotorNoMesin_id"]').val(data.MotorNoMesin).trigger('change');
        $('[id="MotorType_id"]').val(data.MotorType).trigger('change');
        $('[id="MotorWarna_id"]').val(data.MotorWarna).trigger('change');
        window.Hitung();
    });
    $('[id="MotorNoMesin_id"]').select2({
        data: $.map(__dtMotor, function (obj) {
            obj.id = obj.MotorNoMesin;
            obj.text = obj.MotorNoMesin;
            return obj;
        }),
    }).on('select2:select', function (e) {
        var data = e.params.data;
        console.log(data);
        $('[name="MotorTahun"]').val(data.MotorTahun);
        $('[name="MotorNama"]').val(data.MotorNama);
        $('[name="MotorKategori"]').val(data.MotorKategori);
        $('[name="MotorAutoN"]').val(data.MotorAutoN);
        $('[name="DKHarga"]').utilNumberControl().val(data.MotorHrgJual);
        $('[id="MotorType_id"]').val(data.MotorType).trigger('change');
        $('[id="MotorNoRangka_id"]').val(data.MotorNoRangka).trigger('change');
        $('[id="MotorWarna_id"]').val(data.MotorWarna).trigger('change');
        window.Hitung();
    });

	function setProgramCombo(result,autoSelect = false){
		var element = $('[id="ProgramNama"]');
		element.val("");
		element.find('option').remove();
	
		var dataProgram = $.map(result.rows, function (obj) {
			obj.id = obj.ProgramNama;
			obj.text = obj.ProgramNama;
			return obj;
		});
		$('[id="ProgramNama"]').select2({
			data: dataProgram
		}).on('select2:select', function (e) {
			// console.log(e);
			var data = e.params.data;
			// console.log(data);
			$('[name="PrgSubsDealer"]').utilNumberControl().val(data.PrgSubsDealer);
			$('[name="PrgSubsFincoy"]').utilNumberControl().val(data.PrgSubsFincoy);
			$('[name="PrgSubsSupplier"]').utilNumberControl().val(data.PrgSubsSupplier);
			$('[name="ProgramSubsidi"]').utilNumberControl().val(data.ProgramSubsidi);
			$('[name="DKNetto"]').utilNumberControl().val(data.DKNetto);
			window.Hitung();
		});
		
		if(autoSelect){
			if(result.rowsCount > 2){
				$('[id="ProgramNama"]').select2({data: dataProgram})
				.val(result.rows[1].ProgramNama)
				.trigger('change').trigger({
					type: 'select2:select',
					params: {
						data: result.rows[1]
					}
				});
				$('[id="ProgramNama"]').val(dsTJualDatKonFill['ProgramNama']).trigger('change');				
				$('[name="PrgSubsDealer"]').utilNumberControl().val(dsTJualDatKonFill['PrgSubsDealer']);
				$('[name="PrgSubsFincoy"]').utilNumberControl().val(dsTJualDatKonFill['PrgSubsFincoy']);
				$('[name="PrgSubsSupplier"]').utilNumberControl().val(dsTJualDatKonFill['PrgSubsSupplier']);
				$('[name="ProgramSubsidi"]').utilNumberControl().val('{$dsTJualDatKonFill['ProgramSubsidi']}',);
				$('[name="DKNetto"]').utilNumberControl().val('{$dsTJualDatKonFill['DKNetto']}',);
				var DPPO = parseInt($('[name=PrgSubsSupplier]').val()) + parseInt($('[name=PrgSubsDealer]').val()) + parseInt($('[name=PrgSubsFincoy]').val()) + parseInt($('[name=DKDPLLeasing]').val()) + parseInt($('[name=DKDPInden]').val()) + parseInt($('[name=DKDPTerima]').val())
	            $('[name="NDPPO"]').utilNumberControl().val(DPPO);
			}else{
				$('[id="ProgramNama"]').select2({data: dataProgram})
				.val(result.rows[0].ProgramNama)
				.trigger('change').trigger({
					type: 'select2:select',
					params: {
						data: result.rows[0]
					}
				});
				$('[id="ProgramNama"]').val(dsTJualDatKonFill['ProgramNama']).trigger('change');				
				$('[name="PrgSubsDealer"]').utilNumberControl().val(dsTJualDatKonFill['PrgSubsDealer']);
				$('[name="PrgSubsFincoy"]').utilNumberControl().val(dsTJualDatKonFill['PrgSubsFincoy']);
				$('[name="PrgSubsSupplier"]').utilNumberControl().val(dsTJualDatKonFill['PrgSubsSupplier']);
				$('[name="ProgramSubsidi"]').utilNumberControl().val('{$dsTJualDatKonFill['ProgramSubsidi']}',);
				$('[name="DKNetto"]').utilNumberControl().val('{$dsTJualDatKonFill['DKNetto']}',);
				var DPPO = parseInt($('[name=PrgSubsSupplier]').val()) + parseInt($('[name=PrgSubsDealer]').val()) + parseInt($('[name=PrgSubsFincoy]').val()) + parseInt($('[name=DKDPLLeasing]').val()) + parseInt($('[name=DKDPInden]').val()) + parseInt($('[name=DKDPTerima]').val())
	            $('[name="NDPPO"]').utilNumberControl().val(DPPO);
			}
		}
	}
	
	
    
     $('[id="MotorType_id"]').on("change", function (e) {
         //BBN=====
         
         $.ajax({
            type: 'POST',
            url: '$urlPrograms',
            data: {
                'MotorType': $('[id="MotorType_id"]').val(),
                'DKTgl': $('input[name="DKTgl"]').val()
            }
        }).then(function (result) {
            // console.log(result);
			setProgramCombo(result,true);			
        });
     });
    
	$.ajax({
		type: 'POST',
		url: '$urlPrograms',
		data: {
			'MotorType': $('[id="MotorType_id"]').val(),
			'DKTgl': $('input[name="DKTgl"]').val()
		}
	}).then(function (result) {
		// console.log(result);
		setProgramCombo(result);
	});
	
    $('[id="SalesKode"]').val(dsTJualDatKonFill['SalesKode']).trigger('change');
    $('[id="MotorNoMesin_id"]').val(dsTJualDatKonFill['MotorNoMesin']).trigger('change');
    $('[id="MotorNoRangka_id"]').val(dsTJualDatKonFill['MotorNoRangka']).trigger('change');
    $('[id="MotorType_id"]').val(dsTJualDatKonFill['MotorType']).trigger('change');
    $('[id="MotorWarna_id"]').val(dsTJualDatKonFill['MotorWarna']).trigger('change');
    $('[id="ProgramNama"]').val(dsTJualDatKonFill['ProgramNama']).trigger('change');
    // if ($('[id="ProgramNama"]').find("option[value='" + dsTJualDatKonFill['ProgramNama'] + "']").length) {
	// 	$('[id="ProgramNama"]').val(dsTJualDatKonFill['ProgramNama']).trigger('change');
	// } else { 
	// 	var newOption = new Option(dsTJualDatKonFill['ProgramNama'], dsTJualDatKonFill['ProgramNama'], true, true);
	// 	$('[id="ProgramNama"]').append(newOption).trigger('change');
	// } 

	

    $('[id="MotorType_id"]').on("change", function (e) {
         $.ajax({
            type: 'POST',
            url: '$urlBBNvalue',
            data: {
                'CusKabupaten': $('[name="CusKabupaten"]').val(),
                'MotorType_id': $('[id="MotorType_id"]').val(),
                'DKTgl': '{$dsTJualDatKonFill['DKTgl']}',
                'MotorTahun': $('[id="MotorTahun"]').val()
            }
        }).then(function (result) {
            $('input[name="BBN"]').utilNumberControl().val(result.message);
        });
         
         //Program======
     });
    
    $('[id="MotorType_id"]').on('select2:select', function (e) {
        var data = e.params.data;
        $('[name="MotorTahun"]').val(data.MotorTahun);
        $('[name="MotorNama"]').val(data.MotorNama);
        $('[name="MotorKategori"]').val(data.MotorKategori);
        $('[name="MotorAutoN"]').val(data.MotorAutoN);
        $('[name="DKHarga"]').utilNumberControl().val(data.MotorHrgJual);
        $('[id="MotorNoMesin_id"]').val(data.MotorNoMesin).trigger('change');
        $('[id="MotorNoRangka_id"]').val(data.MotorNoRangka).trigger('change');
        $('[id="MotorWarna_id"]').val(data.MotorWarna).trigger('change');
        window.Hitung();
    });
    
    $('#btnPrint').click(function (event) {	 
        var url = new URL($(location).attr('href'));
        var c = decodeURIComponent(url.searchParams.get("mode"));
        var StatPrint = 1;
        var defValue = 0;
        var defMemo = '';
        switch(c){
            case 'kwitansi-kirim-tagih':
            case 'kwitansi-um-konsumen':
            case 'kwitansi-bbn':
            case 'kwitansi-dp-po-leasing':
            case 'kwitansi-dp-konsumen':
            case 'kwitansi-penjualan-tunai':
            case 'kwitansi-penjualan-leasing':
            case 'kwitansi-jasa-perantara':
            case 'kwitansi-pelunasan-leasing':
                var messageOption = '<div class="form-group col-md-24">' +
                     '<div class="col-md-8 col-sm-8 col-lg-8 col-xs-8"><label class="control-label" style="margin: 0; padding: 6px 0;">Nominal Kirim Tagih</label></div>' +
                     '<div class="col-md-16 col-sm-16 col-lg-16 col-xs-16" style="margin-bottom: 2px;"><input id="nominalKirimTagih" class="form-control" /></div>';
                var noteField = '<div class="col-md-8 col-sm-8 col-lg-8 col-xs-8"><label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan Kirim Tagih</label></div>' +
                     '<div class="col-md-16 col-sm-16 col-lg-16 col-xs-16" style="margin-bottom: 2px;"><input id="memoKirimTagih" class="form-control" /></div>';                   
                if (c === 'kwitansi-kirim-tagih') {StatPrint = 20; defValue = $('[name=DKDPTerima]').inputmask('unmaskedvalue');}
                if (c === 'kwitansi-um-konsumen') {StatPrint = 21; defValue = $('[name=NDPPO]').inputmask('unmaskedvalue'); }
                if (c === 'kwitansi-pelunasan-leasing') { StatPrint = 22; defValue = $('[name=DKNetto]').inputmask('unmaskedvalue'); defMemo = 'Biaya Administrasi Dealer'; }
                if (c === 'kwitansi-bbn') { StatPrint = 23; defValue = $('[name=BBN]').inputmask('unmaskedvalue'); }
                if (c === 'kwitansi-dp-po-leasing') { StatPrint = 24; defValue = parseInt($('[name=PrgSubsSupplier]').val()) + parseInt($('[name=PrgSubsDealer]').val()) + parseInt($('[name=PrgSubsFincoy]').val()) + parseInt($('[name=DKDPLLeasing]').val()) + parseInt($('[name=DKDPInden]').val()) + parseInt($('[name=DKDPTerima]').val());}
                if (c === 'kwitansi-dp-konsumen') { StatPrint = 25; defValue = parseInt($('[name=PrgSubsSupplier]').val()) + parseInt($('[name=PrgSubsDealer]').val()) + parseInt($('[name=PrgSubsFincoy]').val()) + parseInt($('[name=DKDPLLeasing]').val()) + parseInt($('[name=DKDPInden]').val()) + parseInt($('[name=DKDPTerima]').val());}
                if (c === 'kwitansi-penjualan-tunai') { StatPrint = 26; defValue = $('[name=DKHarga]').inputmask('unmaskedvalue'); }
                if (c === 'kwitansi-jasa-perantara') { StatPrint = 27; defValue = $('[name=DKScheme]').inputmask('unmaskedvalue'); noteField = '<div class="col-md-8 col-sm-8 col-lg-8 col-xs-8">.</div>'; }
                if (c === 'kwitansi-penjualan-leasing') { StatPrint = 28; defValue = $('[name=DKHarga]').inputmask('unmaskedvalue'); }
                messageOption += noteField;
                messageOption += '</div>';
                if((c === 'kwitansi-bbn') || (c === 'kwitansi-dp-po-leasing') || (c === 'kwitansi-dp-konsumen') ||  (c === 'kwitansi-penjualan-tunai')){
                    $('#nominal_kirim_tagih').val($('#nominalKirimTagih').inputmask('unmaskedvalue'));
                   $('#keterangan_kirim_tagih').val($('#memoKirimTagih').val());
                   $('#StatPrint').val(StatPrint);
//                   $('#frm_ttdk_id').attr('action','$urlPrint');
//                   $('#frm_ttdk_id').attr('target','_blank');
//                   $('#frm_ttdk_id').submit();                   
                   $('#frm_ttdk_id').utilForm().submit('$urlPrint','post','_blank');
                   setTimeout(function() {location.reload(); },2000);
                }else{
                    bootbox.dialog({
                        title: 'Input',
                        message: messageOption,
                        size: 'medium',
                        onEscape: true,
                        backdrop: true,
                        buttons: {
                            ok: {
                                label: '<i class="glyphicon glyphicon-ok"></i>&nbsp&nbsp OK &nbsp&nbsp',
                                className: 'btn-success',
                                callback: function(){ 
                                   $('#nominal_kirim_tagih').val($('#nominalKirimTagih').inputmask('unmaskedvalue'));
                                   $('#keterangan_kirim_tagih').val($('#memoKirimTagih').val());
                                   $('#StatPrint').val(StatPrint);
                                   // $('#frm_ttdk_id').attr('action','$urlPrint');
                                   // $('#frm_ttdk_id').attr('target','_blank');
                                   $('#frm_ttdk_id').utilForm().submit('$urlPrint','post','_blank');
                                   setTimeout(function() {location.reload(); },2000);
                                }
                            },
                            cancel: {
                                label: '<i class="fa fa-ban"></i>&nbsp&nbsp Cancel &nbsp',
                                className: 'btn-danger',
                                callback: this.callback
                            }
                        }
                    });
                }                
                    $("#nominalKirimTagih").inputmask("numeric",{integerDigits:14,digits:2,digitsOptional:false,groupSeparator:".",radixPoint:",",autoGroup:true,autoUnmask:false});
                    $("#nominalKirimTagih").inputmask('setvalue',util.number.getValueOfEuropeanNumber(defValue));
                    $('#memoKirimTagih').val(defMemo);
                break;
            default:
                $('#frm_ttdk_id').attr('action','$urlPrint');
                $('#frm_ttdk_id').attr('target','_blank');
                $('#frm_ttdk_id').submit();
                setTimeout(function() {location.reload(); },2000);
                break;
        }
    }); 
    
     $('#btnCancel').click(function (event) {	      
       $('#frm_ttdk_id').attr('action','{$url['cancel']}');
       $('#frm_ttdk_id').submit();
    });
     
      $('#btnAdd').click(function (event) {	      
        $('#frm_ttdk_id').attr('action','{$urlAdd}');
        $('#frm_ttdk_id').submit();
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });  
    
     $('[name=DKTgl]').utilDateControl().cmp().attr('tabindex', '2');
     $('[name=CusTglLhr]').utilDateControl().cmp().attr('tabindex', '29');
     $('[name=ROCount]').utilNumberControl().cmp().attr('tabindex', '41');
     
     $('#btnImport').click(function (event) {	      
        window.util.app.dialogListData.show({
            title: 'Manage Dealing Process - SPK - DK',
            url: '{$urlImport}'
        },
        function (data) {
			if(!Array.isArray(data)) return;
			let iframe = window.util.app.dialogListData.iframe();
			let grid = iframe.contentWindow.$('#scnjqGridDetail');
			let ids = grid.jqGrid('getGridParam', 'selarrrow');
			var dtGrid2 = [];
			ids.forEach(elmt => {
				dtGrid2.push(grid.jqGrid('getRowData',elmt));
			}); 
			console.log(data,dtGrid2);
            $.ajax({
				type: "POST",
				url: '$urlDetail',
				data: {
					oper: 'import',
					data: data,
					dtGrid2: dtGrid2,
					header: btoa($('#frm_ttdk_id').serialize())
				},
				success: function(data) {
					window.location.href = "{$url['update']}&oper=skip-load&DKNoView={$dsTJualDatKonFill['DKNoView']}";     
				},
			});
        });
    });   
     
     
     $('#LeaseKode').on('change', function (e) {
         if ($('#LeaseKode').val() === 'TUNAI'){
                $('#DKJenis').val('Tunai');
                $('#DKJenis').trigger('change');
            }else{
                $('#DKJenis').val('Kredit');
                $('#DKJenis').trigger('change');
            }         
    });

    $('#btnAttachment').click(function (event) {
        window.location.href = '{$urlLampiran }';
    });
    
    $('#btnSave').click(function (event) {
          let BBN = $('#BBN-disp').val();
           if (BBN == null || BBN === ''){
               bootbox.alert({message:'BBN tidak boleh null', size: 'small'});
               return;
           }
        $('input[name="SaveMode"]').val('ALL');
        $('#frm_ttdk_id').attr('action','{$url['update']}');
        $('#frm_ttdk_id').submit();
	  });
     
JS
);
