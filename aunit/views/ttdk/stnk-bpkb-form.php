<?php

use aunit\components\FormField;
use common\components\Custom;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttdk */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function formatdk(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-10">' + result.id + '</div>' +
           '<div class="col-xs-14">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
?>
    <div class="ttdk-form">
        <?php
        $form = ActiveForm::begin(['id' => 'frm_ttdk_id', 'options' => ['autocomplete' => 'off', 'method' => 'post', 'fieldConfig' => ['template' => "{input}"]]]);
        \aunit\components\TUi::form(
            [
                'class' => "row-no-gutters",
                'items' => [
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "col-sm-2", 'items' => ":LabelNomor"],
                            ['class' => "col-sm-4", 'items' => ":Nomor"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-1", 'items' => ":LabelTanggal"],
                            ['class' => "col-sm-3", 'items' => ":Tanggal"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-1", 'items' => ":LabelLeasing"],
                            ['class' => "col-sm-3", 'items' => ":Leasing"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-2", 'items' => ":LabelSales"],
                            ['class' => "col-sm-5", 'items' => ":Sales"],
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "col-sm-2", 'items' => ":LabelKonsumen"],
                            ['class' => "col-sm-14", 'items' => ":Konsumen"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-2", 'items' => ":LabelKelurahan"],
                            ['class' => "col-sm-5", 'items' => ":Kelurahan"],
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "col-sm-2", 'items' => ":LabelAlamat"],
                            ['class' => "col-sm-9", 'items' => ":Alamat"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-1", 'items' => ":LabelRTRW"],
                            ['class' => "col-sm-1", 'items' => ":RT"],
                            ['class' => "col-sm-1", 'style' => 'text-align:center', 'items' => ":LabelSlash"],
                            ['class' => "col-sm-1", 'items' => ":RW"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-2", 'items' => ":LabelKecamatan"],
                            ['class' => "col-sm-5", 'items' => ":Kecamatan"],
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "col-sm-2", 'items' => ":LabelTelepon"],
                            ['class' => "col-sm-14", 'items' => ":Telepon"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-2", 'items' => ":LabelKabupaten"],
                            ['class' => "col-sm-5", 'items' => ":Kabupaten"]
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "col-sm-2", 'items' => ":LabelTypeMotor"],
                            ['class' => "col-sm-4", 'items' => ":TypeMotor"],
                            ['class' => "col-sm-2", 'style' => 'padding-left:3px', 'items' => ":Tahun"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-2", 'items' => ":LabelWarna"],
                            ['class' => "col-sm-5", 'items' => ":Warna"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-2", 'items' => ":LabelHargaOTR"],
                            ['class' => "col-sm-5", 'items' => ":HargaOTR"]
                        ]
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "col-sm-2", 'items' => ":LabelNoMesin"],
                            ['class' => "col-sm-6", 'items' => ":NoMesin"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-2", 'items' => ":LabelNoRangka"],
                            ['class' => "col-sm-5", 'items' => ":NoRangka"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-2", 'items' => ":LabelNilaiFaktur"],
                            ['class' => "col-sm-5", 'items' => ":NilaiFaktur"]
                        ]
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "col-sm-2", 'items' => ":LabelFakturAHM"],
                            ['class' => "col-sm-6", 'items' => ":FakturAHM"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-2", 'items' => ":LabelTglPengajuan"],
                            ['class' => "col-sm-2", 'items' => ":TglPengajuan", 'style' => 'padding-right:5px;'],
                            ['class' => "col-sm-1", 'items' => ":LabelTglJadi"],
                            ['class' => "col-sm-2", 'items' => ":TglJadi"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-2", 'items' => ":LabelTglAmbil"],
                            ['class' => "col-sm-5", 'items' => ":TglAmbil"]
                        ],
                    ],
                    ['class' => "form-group col-md-24", 'items' => ['class' => "row", 'items' => "<hr style='margin: 1px 5px;'>"]],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "col-sm-2", 'items' => ":LabelNamaSTNK"],
                            ['class' => "col-sm-11", 'items' => ":NamaSTNK"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-5", 'items' => ":LabelSTNKPengajuan"],
                            ['class' => "col-sm-5", 'items' => ":STNKPengajuan"],
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "col-sm-2", 'items' => ":LabelSTNKAlamat"],
                            ['class' => "col-sm-22", 'items' => ":STNKAlamat"],
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "form-group col-md-12",
                                'items' => [
                                    ['class' => "col-sm-4", 'items' => ":LabelNoSTNK"],
                                    ['class' => "col-sm-8", 'items' => ":NoSTNK"],
                                    ['class' => "col-sm-2"],
                                    ['class' => "col-sm-3", 'items' => ":LabelTglJadi"],
                                    ['class' => "col-sm-5", 'items' => ":TglSTNKJadi"]
                                ],
                            ],
                            ['class' => "form-group col-md-12",
                                'items' => [
                                    ['class' => "col-sm-3", 'items' => ":LabelTglAmbil"],
                                    ['class' => "col-sm-5", 'items' => ":TglSTNKAmbil"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-4", 'items' => ":LabelPenerima"],
                                    ['class' => "col-sm-9", 'items' => ":STNKPenerima"],
                                    ['class' => "col-sm-2", 'items' => ":StnkPrint", 'style' => 'padding-left:5px;'],
                                ],
                            ],
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "form-group col-md-12",
                                'items' => [
                                    ['class' => "col-sm-4", 'items' => ":LabelPlat"],
                                    ['class' => "col-sm-8", 'items' => ":Plat"],
                                    ['class' => "col-sm-2"],
                                    ['class' => "col-sm-3", 'items' => ":LabelTglJadi"],
                                    ['class' => "col-sm-5", 'items' => ":PlatJadi"]
                                ],
                            ],
                            ['class' => "form-group col-md-12",
                                'items' => [
                                    ['class' => "col-sm-3", 'items' => ":LabelTglAmbil"],
                                    ['class' => "col-sm-5", 'items' => ":PlatAmbil"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-4", 'items' => ":LabelPenerima"],
                                    ['class' => "col-sm-9", 'items' => ":PlatTerima"],
                                    ['class' => "col-sm-2", 'items' => ":PltPrint", 'style' => 'padding-left:5px;'],
                                ],
                            ],
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "form-group col-md-12",
                                'items' => [
                                    ['class' => "col-sm-4", 'items' => ":LabelBPKB"],
                                    ['class' => "col-sm-8", 'items' => ":BPKB"],
                                    ['class' => "col-sm-2"],
                                    ['class' => "col-sm-3", 'items' => ":LabelTglJadi"],
                                    ['class' => "col-sm-5", 'items' => ":BPKBJadi"]
                                ],
                            ],
                            ['class' => "form-group col-md-12",
                                'items' => [
                                    ['class' => "col-sm-3", 'items' => ":LabelTglAmbil"],
                                    ['class' => "col-sm-5", 'items' => ":BPKBAmbil"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-4", 'items' => ":LabelPenerima"],
                                    ['class' => "col-sm-9", 'items' => ":BPKBTerima"],
                                    ['class' => "col-sm-2", 'items' => ":BpkbPrint", 'style' => 'padding-left:5px;'],
                                ],
                            ],
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "form-group col-md-12",
                                'items' => [
                                    ['class' => "col-sm-4", 'items' => ":LabelSRUT"],
                                    ['class' => "col-sm-8", 'items' => ":SRUT"],
                                    ['class' => "col-sm-2"],
                                    ['class' => "col-sm-3", 'items' => ":LabelTglJadi"],
                                    ['class' => "col-sm-5", 'items' => ":SRUTJadi"]
                                ],
                            ],
                            ['class' => "form-group col-md-12",
                                'items' => [
                                    ['class' => "col-sm-3", 'items' => ":LabelTglAmbil"],
                                    ['class' => "col-sm-5", 'items' => ":SRUTAmbil"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-4", 'items' => ":LabelPenerima"],
                                    ['class' => "col-sm-9", 'items' => ":SRUTTerima"],
                                    ['class' => "col-sm-2", 'items' => ":SrutPrint", 'style' => 'padding-left:5px;'],
                                ],
                            ],
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "form-group col-md-12",
                                'items' => [
                                    ['class' => "col-sm-4", 'items' => ":LabelNotice"],
                                    ['class' => "col-sm-8", 'items' => ":Notice"],
                                    ['class' => "col-sm-2"],
                                    ['class' => "col-sm-3", 'items' => ":LabelTglJadi"],
                                    ['class' => "col-sm-5", 'items' => ":NoticeJadi"]
                                ],
                            ],
                            ['class' => "form-group col-md-12",
                                'items' => [
                                    ['class' => "col-sm-3", 'items' => ":LabelTglAmbil"],
                                    ['class' => "col-sm-5", 'items' => ":NoticeAmbil"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-4", 'items' => ":LabelPenerima"],
                                    ['class' => "col-sm-9", 'items' => ":NoticeTerima"],
                                    ['class' => "col-sm-2", 'items' => ":NtcPrint", 'style' => 'padding-left:5px;'],
                                ],
                            ],
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "form-group col-md-12",
                                'items' => [
                                    ['class' => "col-sm-4", 'items' => ":LabelNoticeNilai"],
                                    ['class' => "col-sm-8", 'items' => ":NoticeNilai"],
                                    ['class' => "col-sm-2"],
                                    ['class' => "col-sm-3", 'items' => ":LabelDendaNotice"],
                                    ['class' => "col-sm-5", 'items' => ":DendaNotice"]
                                ],
                            ],
                            ['class' => "form-group col-md-12",
                                'items' => [
                                    ['class' => "col-sm-3", 'items' => ":LabelTglPrint"],
                                    ['class' => "col-sm-5", 'items' => ":TglPrint"],
                                    ['class' => "col-sm-1"],
                                    ['class' => "col-sm-4", 'items' => ":LabelKons"],
                                    ['class' => "col-sm-11", 'items' => ":Kons", 'style' => 'padding-left:6px;'],
                                ],
                            ],
                        ],
                    ],
                    ['class' => "form-group col-md-24",
                        'items' => [
                            ['class' => "col-sm-2", 'items' => ":LabelKeterangan"],
                            ['class' => "col-sm-16", 'items' => ":Keterangan"],
                            ['class' => "col-sm-1"],
                            ['class' => "col-sm-2", 'items' => ":LabelZidSpk"],
                            ['class' => "col-sm-3", 'items' => ":ZidSpk"]
                        ],
                    ],
                ]
            ],
            [ 'class' => "row-no-gutters",
                'items' => [
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'tdcustomer.CusNama'        => 'Nama Konsumen',
                                'tdcustomer.CusPembeliNama' => 'Nama Pemohon',
                                'ttdk.DKNo'                 => 'No Data Konsumen',
                                'tmotor.SKNo'               => 'No Surat Jln Kons',
                                'tmotor.MotorNoMesin'       => 'No Mesin',
                                'tmotor.MotorNoRangka'      => 'No Rangka',
                                'tmotor.MotorType'          => 'Type Motor',
                                'tmotor.MotorWarna'         => 'Warna Motor',
                                'tmotor.MotorTahun'         => 'Tahun Motor',
                                'ttdk.LeaseKode'            => 'Leasing',
                                'ttdk.TeamKode'             => 'Team',
                                'ttdk.SalesKode'            => 'Kode Sales',
                                'tdsales.SalesNama'         => 'Nama Sales',
                                'ttdk.INNo'                 => 'No Inden',
                                'ttdk.DKJenis'              => 'Jenis',
                                'ttdk.DKPONo'               => 'No PO',
                                'ttdk.SPKNo'                => 'No SPK',
                                'ttdk.CusKode'              => 'Kode Konsumen',
                                'tdcustomer.CusAlamat'      => 'Alamat Konsumen',
                                'tdcustomer.CusRT'          => 'RT Konsumen',
                                'tdcustomer.CusRW'          => 'RW Konsumen',
                                'tdcustomer.CusKelurahan'   => 'Kelurahan',
                                'tdcustomer.CusKecamatan'   => 'Kecamatan',
                                'tdcustomer.CusKabupaten'   => 'Kabupaten',
                                'tdcustomer.CusTelepon'     => 'Telepon',
                                'tdcustomer.CusTelepon2'    => 'Telepon 2',
                                'ttdk.DKMemo'               => 'Keterangan DK',
                                'tmotor.MotorMemo'          => 'Keterangan Motor',
                                'tmotor.STNKNo'             => 'No STNK',
                                'tmotor.PlatNo'             => 'No Plat',
                                'tmotor.BPKBNo'             => 'No BPKB',
                                'tmotor.FakturAHMNo'        => 'No Faktur AHM',
                                'tmotor.STNKNama'           => 'Nama STNK',
                                'tmotor.STNKAlamat'         => 'Alamat STNK',
                                'ttdk.UserID'               => 'User ID',
                                'ttdk.DKLunas'              => 'Status Lunas',
                                'ttdk.ProgramNama'          => 'Nama Program',
                                'ttdk.NamaHadiah'           => 'Nama Hadiah',
                                'tmotor.SSNo'               => 'No Surat Jln Supplier',
                                'tmotor.FBNo'               => 'No Faktur Beli',
                                'tmotor.PDNo'               => 'No Penerimaan Dealer',
                                'tmotor.SDNo'               => 'No Surat Jln Dealer',
                                'tmotor.NoticePenerima'     => 'Penerima Notice',
                                'tmotor.STNKPenerima'       => 'Penerima STNK',
                                'tmotor.PlatPenerima'       => 'Penerima Plat',
                                'tmotor.BPKBPenerima'       => 'Penerima BPKB',
                            ],
                        ])
                    ],
                    ['class' => "pull-right", 'items' => ":btnAttachment :1stbtnPrint :2ndbtnPrint :btnImport :btnAction"],
             ]
            ],
            [
                /* label */
                ":LabelNomor" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor</label>',
                ":LabelTanggal" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tanggal</label>',
                ":LabelLeasing" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Leasing</label>',
                ":LabelSales" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sales</label>',
                ":LabelKonsumen" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Konsumen</label>',
                ":LabelKelurahan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kelurahan</label>',
                ":LabelAlamat" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
                ":LabelRTRW" => '<label class="control-label" style="margin: 0; padding: 6px 0;">RT / RW</label>',
                ":LabelKecamatan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kecamatan</label>',
                ":LabelTelepon" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon</label>',
                ":LabelKabupaten" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kabupaten</label>',
                ":LabelTypeMotor" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Type / Tahun</label>',
                ":LabelWarna" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Warna</label>',
                ":LabelHargaOTR" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Harga OTR</label>',
                ":LabelNoMesin" => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Mesin</label>',
                ":LabelNoRangka" => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Rangka</label>',
                ":LabelNilaiFaktur" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nilai Faktur</label>',
                ":LabelFakturAHM" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Faktur AHM</label>',
                ":LabelTglPengajuan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Pengajuan</label>',
                ":LabelTglJadi" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Jadi</label>',
                ":LabelTglAmbil" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Ambil</label>',
                ":LabelNamaSTNK" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama STNK</label>',
                ":LabelSTNKPengajuan" => '<label class="control-label" style="margin: 0; padding: 6px 0;"> Tgl Pengajuan STNK-Notice-Plat-BPKB</label>',
                ":LabelSTNKAlamat" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat STNK</label>',
                ":LabelNoSTNK" => '<label class="control-label" style="margin: 0; padding: 6px 0;">No STNK</label>',
                ":LabelPenerima" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Penerima</label>',
                ":LabelPlat" => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Plat</label>',
                ":LabelBPKB" => '<label class="control-label" style="margin: 0; padding: 6px 0;">No BPKB</label>',
                ":LabelSRUT" => '<label class="control-label" style="margin: 0; padding: 6px 0;">No SRUT</label>',
                ":LabelKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
                ":LabelKons" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kons</label>',
                ":LabelSlash" => '<label class="control-label" style="margin: 0; padding: 6px 0;"> / </label>',
                ":LabelNoticeNilai" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nilai Notice</label>',
                ":LabelDendaNotice" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Denda Notice</label>',
                ":LabelNotice" => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Notice</label>',
                ":LabelTglPrint" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Print</label>',
                /* componen */
                ":Nomor" => Html::textInput('DKNoView', $dsJual['DKNoView'], ['class' => 'form-control', 'readonly' => true]) .
                    Html::hiddenInput('DKNo', $dsJual['DKNo'], ['id' => 'DKNo']) .
                    Html::hiddenInput('StatPrint', null, ['id' => 'StatPrint']),
                ":Sales" => Html::textInput('SalesNama', $dsJual['SalesNama'], ['class' => 'form-control', 'readonly' => true]),
                ":Leasing" => Html::textInput('LeaseKode', $dsJual['LeaseKode'], ['class' => 'form-control', 'readonly' => true]),
                ":Konsumen" => Html::textInput('CusNama', $dsJual['CusNama'], ['class' => 'form-control', 'readonly' => true]),
                ":Alamat" => Html::textInput('CusAlamat', $dsJual['CusAlamat'], ['class' => 'form-control', 'readonly' => true]),
                ":RT" => Html::textInput('CusRT', $dsJual['CusRT'], ['class' => 'form-control', 'readonly' => true]),
                ":RW" => Html::textInput('CusRW', $dsJual['CusRW'], ['class' => 'form-control', 'readonly' => true]),
                ":Telepon" => Html::textInput('CusTelepon', $dsJual['CusTelepon'], ['class' => 'form-control', 'readonly' => true]),
                ":Kabupaten" => Html::textInput('CusKabupaten', $dsJual['CusKabupaten'], ['class' => 'form-control', 'readonly' => true]),
                ":Kecamatan" => Html::textInput('CusKecamatan', $dsJual['CusKecamatan'], ['class' => 'form-control', 'readonly' => true]),
                ":Kelurahan" => Html::textInput('CusKelurahan', $dsJual['CusKelurahan'], ['class' => 'form-control', 'readonly' => true]),
                ":Kons" => Html::textInput('CusKodeKons', $dsJual['CusKodeKons'], ['class' => 'form-control', 'readonly' => true]),
                ":TypeMotor" => Html::textInput('MotorType', $dsJual['MotorType'], ['class' => 'form-control', 'readonly' => true]),
                ":Warna" => Html::textInput('MotorWarna', $dsJual['MotorWarna'], ['class' => 'form-control', 'readonly' => true]),
                ":NoMesin" => Html::textInput('MotorNoMesin', $dsJual['MotorNoMesin'], ['id' => 'MotorNoMesin','class' => 'form-control', 'readonly' => true]),
                ":NoRangka" => Html::textInput('MotorNoRangka', $dsJual['MotorNoRangka'], ['class' => 'form-control', 'readonly' => true]),
                ":Tahun" => Html::textInput('MotorTahun', $dsJual['MotorTahun'], ['class' => 'form-control', 'readonly' => true]),
                ":HargaOTR" => FormField::decimalInput(['name' => 'DKHarga', 'config' => ['value' => $dsJual['DKHarga'], 'readonly' => true]]),
                ":Tanggal" => FormField::dateInput(['name' => 'DKTgl', 'config' => ['value' => $dsJual['DKTgl'], 'readonly' => true]]),
                ":TglPengajuan" => FormField::dateInput(['name' => 'STNKTglBayar', 'config' => ['value' => $dsJual['STNKTglBayar']]]),
                ":TglJadi" => FormField::dateInput(['name' => 'FakturAHMTgl', 'config' => ['value' => $dsJual['FakturAHMTgl']]]),
                ":TglAmbil" => FormField::dateInput(['name' => 'FakturAHMTglAmbil', 'config' => ['value' => $dsJual['FakturAHMTglAmbil']]]),
                ":NilaiFaktur" => FormField::decimalInput(['name' => 'FakturAHMNilai', 'config' => ['value' => $dsJual['FakturAHMNilai']]]),
                ":NoticeNilai" => FormField::decimalInput(['name' => 'NoticeNilai', 'config' => ['value' => $dsJual['NoticeNilai']]]),
                ":DendaNotice" => FormField::decimalInput(['name' => 'NoticeDenda', 'config' => ['value' => $dsJual['NoticeDenda']]]),
                ":FakturAHM" => Html::textInput('FakturAHMNo', $dsJual['FakturAHMNo'], ['class' => 'form-control', 'tabindex' => '2']),
                ":NamaSTNK" => Html::textInput('STNKNama', $dsJual['STNKNama'], ['class' => 'form-control', 'tabindex' => '6']),
                ":STNKAlamat" => Html::textInput('STNKAlamat', $dsJual['STNKAlamat'], ['class' => 'form-control', 'tabindex' => '8']),
                ":NoSTNK" => Html::textInput('STNKNo', $dsJual['STNKNo'], ['class' => 'form-control jadi', 'tabindex' => '9', 'style' => "text-transform:uppercase"]),
                ":Keterangan" => Html::textInput('MotorMemo', $dsJual['MotorMemo'], ['class' => 'form-control', 'tabindex' => '21']),
                ":STNKPenerima" => Html::textInput('STNKPenerima', $dsJual['STNKPenerima'], ['id' => 'STNKPenerima', 'class' => 'form-control Penerima', 'readOnly' => ($dsJual['STNKTglJadi'] == '1900-01-01'), 'style' => "text-transform:capitalize"]),
                ":STNKPengajuan" => FormField::dateInput(['name' => 'STNKTglMasuk', 'config' => ['value' => $dsJual['STNKTglMasuk']]]),
                ":TglPrint" => FormField::dateInput(['name' => 'NoticeTglPrint', 'config' => ['value' => $dsJual['NoticeTglPrint'] ]]),
                ":TglSTNKAmbil" => FormField::dateInput(['name' => 'STNKTglAmbil', 'config' => ['id' => 'STNKTglAmbil', 'value' => $dsJual['STNKTglAmbil'], 'disabled' => ($dsJual['STNKTglJadi'] == '1900-01-01')]]),
                ":TglSTNKJadi" => FormField::dateInput(['name' => 'STNKTglJadi', 'config' => ['id' => 'STNKTglJadi', 'value' => $dsJual['STNKTglJadi']]]),
                ":NoticeTerima" => Html::textInput('NoticePenerima', $dsJual['NoticePenerima'], ['id' => 'NoticePenerima', 'class' => 'form-control Penerima', 'readOnly' => ($dsJual['NoticeTglJadi'] == '1900-01-01'), 'style' => "text-transform:capitalize"]),
                ":NoticeAmbil" => FormField::dateInput(['name' => 'NoticeTglAmbil', 'config' => ['id' => 'NoticeTglAmbil', 'value' => $dsJual['NoticeTglAmbil'], 'disabled' => ($dsJual['NoticeTglJadi'] == '1900-01-01')]]),
                ":NoticeJadi" => FormField::dateInput(['name' => 'NoticeTglJadi', 'config' => ['id' => 'NoticeTglJadi', 'value' => $dsJual['NoticeTglJadi']]]),
                ":Notice" => Html::textInput('NoticeNo', $dsJual['NoticeNo'], ['class' => 'form-control jadi', 'tabindex' => '17', 'style' => "text-transform:uppercase"]),
                ":BPKBTerima" => Html::textInput('BPKBPenerima', $dsJual['BPKBPenerima'], ['id' => 'BPKBPenerima', 'class' => 'form-control Penerima', 'readOnly' => ($dsJual['BPKBTglJadi'] == '1900-01-01'), 'style' => "text-transform:capitalize"]),
                ":BPKBAmbil" => FormField::dateInput(['name' => 'BPKBTglAmbil', 'config' => ['id' => 'BPKBTglAmbil', 'value' => $dsJual['BPKBTglAmbil'], 'disabled' => ($dsJual['BPKBTglJadi'] == '1900-01-01')]]),
                ":BPKBJadi" => FormField::dateInput(['name' => 'BPKBTglJadi', 'config' => ['id' => 'BPKBTglJadi', 'value' => $dsJual['BPKBTglJadi']]]),
                ":BPKB" => Html::textInput('BPKBNo', $dsJual['BPKBNo'], ['class' => 'form-control jadi', 'tabindex' => '13', 'style' => "text-transform:uppercase"]),
                ":PlatTerima" => Html::textInput('PlatPenerima', $dsJual['PlatPenerima'], ['id' => 'PlatPenerima', 'class' => 'form-control Penerima', 'readOnly' => ($dsJual['PlatTgl'] == '1900-01-01'), 'style' => "text-transform:capitalize"]),
                ":PlatAmbil" => FormField::dateInput(['name' => 'PlatTglAmbil', 'config' => ['id' => 'PlatTglAmbil', 'value' => $dsJual['PlatTglAmbil'], 'disabled' => ($dsJual['PlatTgl'] == '1900-01-01')]]),
                ":PlatJadi" => FormField::dateInput(['name' => 'PlatTgl', 'config' => ['id' => 'PlatTgl', 'value' => $dsJual['PlatTgl']]]),
                ":Plat" => Html::textInput('PlatNo', $dsJual['PlatNo'], ['id' => 'PlatNo', 'class' => 'form-control', 'tabindex' => '11', 'style' => "text-transform:uppercase"]),
                ":SRUTTerima" => Html::textInput('SRUTPenerima', $dsJual['SRUTPenerima'], ['id' => 'SRUTPenerima', 'class' => 'form-control Penerima', 'readOnly' => ($dsJual['SRUTTglJadi'] == '1900-01-01'), 'style' => "text-transform:capitalize"]),
                ":SRUTAmbil" => FormField::dateInput(['name' => 'SRUTTglAmbil', 'config' => ['id' => 'SRUTTglAmbil', 'value' => $dsJual['SRUTTglAmbil'], 'disabled' => ($dsJual['SRUTTglJadi'] == '1900-01-01')]]),
                ":SRUTJadi" => FormField::dateInput(['name' => 'SRUTTglJadi', 'config' => ['id' => 'SRUTTglJadi', 'value' => $dsJual['SRUTTglJadi']]]),
                ":SRUT" => Html::textInput('SRUTNo', $dsJual['SRUTNo'], ['class' => 'form-control jadi', 'tabindex' => '15', 'style' => "text-transform:uppercase"]),
                ":StnkPrint" => Html::Button('<i class="glyphicon glyphicon-print" style="color: darkred;"></i>', ['class' => 'btn btn-primary-sm ', 'id' => 'btnStnkPrint']),
                ":NtcPrint" => Html::Button('<i class="glyphicon glyphicon-print" style="color: mediumorchid;"></i>', ['class' => 'btn btn-primary-sm ', 'id' => 'btnNtcPrint']),
                ":BpkbPrint" => Html::Button('<i class="glyphicon glyphicon-print" style="color: green;"></i>', ['class' => 'btn btn-primary-sm ', 'id' => 'btnBpkbPrint']),
                ":PltPrint" => Html::Button('<i class="glyphicon glyphicon-print" style="color: chocolate;"></i>', ['class' => 'btn btn-primary-sm ', 'id' => 'btnPltPrint']),
                ":SrutPrint" => Html::Button('<i class="glyphicon glyphicon-print" style="color: cornflowerblue;"></i>', ['class' => 'btn btn-primary-sm ', 'id' => 'btnSrutPrint']),
                /* button */
//                ":btnImport" => Html::button( '<i class="fa fa-arrow-circle-down"></i>  Import ', [ 'class' => 'btn bg-navy ', 'id' => 'btnImport', 'style' => 'width:80px' ] ),
//                ":btnCancel"          => Html::Button( '<i class="fa fa-ban"></i> Batal', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:80px' ] ),
                ":btnAdd" => "",
//				":btnEdit"            => Html::button( '<i class="fa fa-edit"></i> Edit', [ 'class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:80px' ] ),
//				":btnDaftar"          => Html::Button( '<i class="fa fa-list"></i> Daftar ', [ 'class' => 'btn btn-primary ', 'id' => 'btnDaftar', 'style' => 'width:80px' ] ),
                ":1stbtnPrint" => Html::button('<i class="glyphicon glyphicon-print"><br><br>Buku Servis 103</i> ', ['style' => 'height:60px', 'class' => 'btn btn-warning ' . Custom::viewClass(), 'id' => '1stbtnPrint']),
                ":2ndbtnPrint" => Html::button('<i class="glyphicon glyphicon-print"><br><br>Buku Servis 125</i> ', ['style' => 'height:60px', 'class' => 'btn btn-success' . Custom::viewClass(), 'id' => 'btnPrint2']),
                ":btnAttachment"       => Html::button('<i class="glyphicon glyphicon-file"><br><br>Lampiran</i>', ['class' => 'btn bg-black hidden', 'id' => 'btnAttachment', 'style' => 'width:80px;height:60px']),
                ":btnImport" => Html::button('<i class="fa fa-arrow-circle-down"><br><br>Import</i>   ', ['class' => 'btn bg-navy ', 'id' => 'btnImport', 'style' => 'width:60px;height:60px']),
                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
//                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
//                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
//                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),


                ":LabelZidSpk" => '<label class="control-label" style="margin: 0; padding: 6px 0;">ID SPK</label>',
                ":ZidSpk" => Html::textInput('ZidSpk', $dsJual['ZidSpk'], ['class' => 'form-control']),
            ]
        );
        ActiveForm::end();
        ?>
    </div>
<?php
$urlPlat = Url::toRoute(['ttdk/plat']);
$urlPrint = $url['print'];
$urlIndex = Url::toRoute(['ttdk/stnk-bpkb']);
$urlDetail = Url::toRoute(['ttdk/stnk-bpkb-import-loop']);
$urlLampiran = Url::toRoute( [ 'ttdk/lampiran', 'id' => $_GET['id'], 'view'=> 'stnkbpkb' ] );
//$urlImport        = Url::toRoute( [ 'ttdk/importstnk','tgl1'    => Yii::$app->formatter->asDate( '-5 day', 'yyyy-MM-dd') ] );
$urlImport = Url::toRoute(['ttdk/importstnk', 'tgl1' => Yii::$app->formatter->asDate('now', 'yyyy-MM-dd')]);
$this->registerJs(<<< JS

    
    $('[name=STNKNama]').keydown(function( event ) {
      if ( event.which !== 114 )  return;
         event.preventDefault();
         $(this).val($('[name=CusNama]').val());
          $('[name=STNKAlamat]').val($('[name=CusAlamat]').val()+' '+$('[name=CusRT]').val() + '/'+$('[name=CusRW]').val() +
          ', Kec '+$('[name=CusKecamatan]').val() + ', Kab '+$('[name=CusKabupaten]').val());
    });

    function prosesPrint(StatPrint){
        $('#frm_ttdk_id').attr('action','$urlPrint');
        $('#frm_ttdk_id').attr('target','_blank');
        $('#StatPrint').val(StatPrint);     
        $('#frm_ttdk_id').submit();
        $( document ).focus(function() {
          setTimeout(function (){location.reload();}, 100);
        });
    }
    
    $('.Penerima').change(function (){
        var propname = $(this).prop('name');
        if ($(this).val() === '' || $(this).val() === '--') return;
        // console.log(propname);
        switch (propname){
            case 'STNKPenerima':
                $('[name=STNKTglAmbil]').utilDateControl().setDate(new Date());
                break;
            case 'NoticePenerima':
                $('[name=NoticeTglAmbil]').utilDateControl().setDate(new Date());
                break;
            case 'BPKBPenerima':
                $('[name=BPKBTglAmbil]').utilDateControl().setDate(new Date());
                break;
            case 'PlatPenerima':
                $('[name=PlatTglAmbil]').utilDateControl().setDate(new Date());
                break;
            case 'SRUTPenerima':
                $('[name=SRUTTglAmbil]').utilDateControl().setDate(new Date());
                break;
        }
    });
    
    $('.jadi').change(function (){
        var propname = $(this).prop('name');
        if ($(this).val() === '' || $(this).val() === '--') return;
        // console.log(propname);
        switch (propname){
            case 'STNKNo':
                $('[name=STNKTglJadi]').utilDateControl().setDate(new Date());
                break;
            case 'NoticeNo':
                $('[name=NoticeTglJadi]').utilDateControl().setDate(new Date());
                break;
            case 'BPKBNo':
                $('[name=BPKBTglJadi]').utilDateControl().setDate(new Date());
                break;
            case 'SRUTNo':
                $('[name=SRUTTglJadi]').utilDateControl().setDate(new Date());
                break;
        }
    });

    $('#btnStnkPrint').click(function (event) {
        prosesPrint(6);
	  }); 
    $('#btnPltPrint').click(function (event) {
        prosesPrint(7);
	  }); 

    $('#btnBpkbPrint').click(function (event) {
        let CusKodeKons = $('[name=CusKodeKons]').val();
         if (CusKodeKons !== 'Individual Customer (Regular)'){
             bootbox.alert({
                message: "Kode Konsumen ini adalah " + CusKodeKons + " Silahkan dikonfirmasikan hak konsumen atas dokumen yang akan diambilnya",
                size: 'small',
                callback: function () {
                    prosesPrint(8);
                }
            })
         }else{
            prosesPrint(8);
         }             
	  }); 

    $('#btnSrutPrint').click(function (event) {
        prosesPrint(9);
	  }); 
    $('#btnNtcPrint').click(function (event) {
        prosesPrint(10);
	  }); 

    $('#1stbtnPrint').click(function (event) {	      
       prosesPrint(4);
	  }); 

    $('#btnPrint2').click(function (event) {	      
       prosesPrint(5);
	  });
    
    
     $('#btnSave').click(function (event) {
         let CusKodeKons = $('[name=CusKodeKons]').val();          
         let plat = $('#PlatNo').val();
         let nosin = $('#MotorNoMesin').val();
         let tglnotice = $('#NoticeTglJadi').val();
         let nonotice = $('#NoticeNo').val();
         let nilainotice = parseFloat($('#NoticeNilai').val());
         console.log(tglnotice);
         console.log(nonotice);
         console.log(nilainotice);
        //  if (tglnotice != null) {
        //     if (nonotice == null || nonotice == "") {
        //     bootbox.alert({message: "No Notice tidak boleh kosong.", size: 'small'});
        //     return '';
        //     }
        //     if (nilainotice == null || nilainotice === 0) {
        //     bootbox.alert({message: "Nilai Notice tidak boleh kosong.", size: 'small'});
        //     return '';
        //     }
        // }
         $.ajax({
				type: "POST",
				url: '$urlPlat',
				data: {plat: plat,nosin:nosin},
				success: function(data) {
				    if(data['plat'] === 'tidak')
                        if (CusKodeKons !== 'Individual Customer (Regular)'){
                             bootbox.alert({
                                message: "Kode Konsumen ini adalah " + CusKodeKons + " Silahkan dikonfirmasikan hak konsumen atas dokumen yang akan diambilnya",
                                size: 'small',
                                callback: function () {
                                    $('#frm_ttdk_id').attr('action','{$url['update']}');
                                    $('#frm_ttdk_id').submit();
                                }
                            })
                         }
                        else{
                            $('#frm_ttdk_id').attr('action','{$url['update']}');
                            $('#frm_ttdk_id').submit();    
                         }    
					else{
					    bootbox.alert({
                                message: "No Plat Telah dipakai oleh No Mesin : " + data['mesin'],
                                size: 'small',
                            })
					}
				},
			});
         
                               
    });
    
     $('#btnCancel').click(function (event) {	      
       $('#frm_ttdk_id').attr('action','{$url['cancel']}');
       $('#frm_ttdk_id').submit();
    });
     
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
    
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });        
    
    $('[name=FakturAHMNilai]').utilNumberControl().cmp().attr('tabindex', '1');
    $('[name=NoticeNilai]').utilNumberControl().cmp().attr('tabindex', '19');
    $('[name=NoticeDenda]').utilNumberControl().cmp().attr('tabindex', '20');
    
    $('[name=STNKTglBayar]').utilDateControl().cmp().attr('tabindex', '3');
    $('[name=FakturAHMTgl]').utilDateControl().cmp().attr('tabindex', '4');
    $('[name=FakturAHMTglAmbil]').utilDateControl().cmp().attr('tabindex', '5');
    $('[name=STNKTglMasuk]').utilDateControl().cmp().attr('tabindex', '7');
    $('[name=STNKTglJadi]').utilDateControl().cmp().attr('tabindex', '10');
    $('[name=PlatTgl]').utilDateControl().cmp().attr('tabindex', '12');
    $('[name=BPKBTglJadi]').utilDateControl().cmp().attr('tabindex', '14');
    $('[name=SRUTTglJadi]').utilDateControl().cmp().attr('tabindex', '16');
    $('[name=NoticeTglJadi]').utilDateControl().cmp().attr('tabindex', '18');
    
    $('#btnImport').click(function (event) {	      
        window.util.app.dialogListData.show({
            title: 'Manage Document Handling - DOCH - STNK',
            url: '{$urlImport}'
        },
        function (data) {
			if(!Array.isArray(data)) return;
			$.ajax({
				type: "POST",
				url: '$urlDetail',
				data: {
					oper: 'import',
					data: data,
					header: btoa($('#frm_ttdk_id').serialize())
				},
				success: function(data) {
					window.location.href = "{$url['update']}&oper=skip-load"; 
				},
			});
        });
    });   
    
    $('#btnAttachment').click(function (event) {	      
        window.location.href = '{$urlLampiran }';
    });

    $( document ).ready(function() {
        if(!$.isEmptyObject($('#MotorNoMesin').val())){
            $('#btnAttachment').removeClass('hidden');
        }
    });
JS
);


