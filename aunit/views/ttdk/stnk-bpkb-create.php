<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttdk */

$this->title = 'Tambah STNK - BPKB';
$this->params['breadcrumbs'][] = ['label' => 'STNK - BPKB', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttdk-create">
        <?= $this->render('stnk-bpkb-form', [
        'model' => $model,
    ]) ?>

</div>
