<?php
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use aunit\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
$this->title = 'Handle Leasing Requirement';
$this->params['breadcrumbs'][] = $this->title;
$arr = [
    'cmbTxt' => [
        'idSPK'	=>	'Id SPK',
        'idCustomer'	=>	'Id Customer',
    ],
    'cmbTxt2' => [
        'idDokumenPengajuan' => 'Id Dokumen Pengajuan',
        'idSPK' => 'Id SPK',
        'jumlahDP' => 'Jumlah DP',
        'tenor' => 'Tenor',
        'jumlahCicilan' => 'Jumlah Cicilan',
        'tanggalPengajuan' => 'Tanggal Pengajuan',
        'idFinanceCompany' => 'Id Finance Company',
        'namaFinanceCompany' => 'Nama Finance Company',
        'idPOFinanceCompany' => 'Id PO Finance Company'

    ],
    'cmbTgl' => [],
    'cmbNum' => [],
    'sortname' => "idSPK",
    'sortorder' => 'desc'
];
$this->registerJsVar('setcmb', $arr);
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render('../_search'); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs(<<< JS
    window.gridParam_jqGrid = {
        readOnly: true,
        rownumbers: true,
        rowNum: 10000,
        pgbuttons: false,
        pginput: false,
        viewrecords: false,
        rowList: [],
        pgtext: ""
    }
    // $('#filter-tgl-val1').change(function() {
    //     let _tgl1 = new Date($(this).val());      
    //     _tgl1.setDate(_tgl1.getDate() + 5);
	//     $('#filter-tgl-val2-disp').datepicker('update', _tgl1);
	//     $('#filter-tgl-val2-disp').trigger('change');
    // });
JS
);

$this->registerJs(\aunit\components\TdUi::mainGridJs(\aunit\models\Ttdk::class,
    'Import Handle Leasing Requirement', [
        'colGrid' => $colGrid,
        'url' => Url::toRoute(['ttdk/import-items','type'=>'dkstatus','apiType'=>'lsng']),
        'mode' => isset($mode) ? $mode : '',
    ]), \yii\web\View::POS_READY);