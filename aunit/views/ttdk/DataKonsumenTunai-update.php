<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttdk */
$jenis = 'Tunai';
$params = '&id='.$id.'&action=update&DKNoView='. $dsTJualDatKonFill['DKNoView'];
$cancel = \yii\helpers\Url::toRoute(['ttdk/cancel','id'=>$id,'action'=>'update','index'=>base64_encode('ttdk/data-konsumen-tunai')]);
$this->title = str_replace('Menu','',\aunit\components\TUi::$actionMode).' - Data Konsumen Tunai: ' . $dsTJualDatKonFill['DKNoView'];
$this->params['breadcrumbs'][] = ['label' => 'Data Konsumen Tunai', 'url' => $cancel];
$this->params['breadcrumbs'][] = 'Edit';
//$cancel                          = Custom::url( 'ttdk/data-konsumen-tunai' );
//$this->title                     = 'Update Data Konsumen Tunai: ' . $model->DKNo;
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Data Konsumen Tunai', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = 'Update';
//$params                          = '&id=' . $id;
?>
<div class="ttdk-update">
	<?= $this->render( '_form', [
		'model'             => $model,
		'dsTJualDatKonFill' => $dsTJualDatKonFill,
		'jenis' => $jenis,
		'url' => [
            'update'    => Custom::url('ttdk/data-konsumen-tunai-update'.$params ),
            'print'     => Custom::url(\Yii::$app->controller->id.'/print'.$params ),
            'cancel'    => $cancel,
            'detail'    => Custom::url(\Yii::$app->controller->id.'/detail' )
        ]
//		'url'               => [
//			'save'   => Custom::url( 'ttdk/data-konsumen-tunai-update' . $params ),
//			'cancel' => $cancel,
//		]
	] ) ?>
</div>
