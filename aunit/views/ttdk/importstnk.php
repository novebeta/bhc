<?php
/* @var $this yii\web\View */

/* @var $dataProvider yii\data\ActiveDataProvider */

use aunit\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
$this->title = 'Manage Document Handling';
$this->params['breadcrumbs'][] = $this->title;
$arr = [
    'cmbTxt' => [
        'idSPK' => 'Id SPK',
        'idCustomer' => 'Id Customer',
    ],
    'cmbTxt2' => [
        'idSO' => 'Id SO',
        'idSPK' => 'Id SPK',
    ],
    'cmbTgl' => [],
    'cmbNum' => [],
    'sortname' => "idSPK",
    'sortorder' => 'desc'
];
$this->registerJsVar('setcmb', $arr);
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render('../_search'); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs(<<< JS
    window.gridParam_jqGrid = {
        readOnly: true,
        rownumbers: true,
        rowNum: 10000,
        pgbuttons: false,
        pginput: false,
        viewrecords: false,
        rowList: [],
        pgtext: ""
    }
    // $('#filter-tgl-val1').change(function() {
    //     let _tgl1 = new Date($(this).val());      
    //     _tgl1.setDate(_tgl1.getDate() + 5);
	//     $('#filter-tgl-val2-disp').datepicker('update', _tgl1);
	//     $('#filter-tgl-val2-disp').trigger('change');
    // });
JS
);

$this->registerJs(\aunit\components\TdUi::mainGridJs(\aunit\models\Ttdk::class,
    'Import Pull Manage Document Handling', [
        'colGrid' => $colGrid,
        'url' => Url::toRoute(['ttdk/import-items','type'=>'stnk','apiType'=>'doch']),
        'mode' => isset($mode) ? $mode : '',
    ]), \yii\web\View::POS_READY);
$urlDetail = Url::toRoute(['ttdk/import-details','type'=>'stnk','keyCol'=>'idSO']);
$this->registerJs(<<< JS
jQuery(function ($) {
    if($('#jqGridDetail').length) {
        $('#jqGridDetail').utilJqGrid({
            url: '$urlDetail',
            height: 240,
            readOnly: true,
            rownumbers: true,
            rowNum: 1000,
            colModel: [
               	{
	                name: 'nomorFakturSTNK',
	                label: 'No Faktur STNK',
	                width: 100,
	            },
                {
                    name: 'tanggalPengajuanSTNKKeBiro',
                    label: 'Tgl Pengajuan STNK',
                    width: 130
                },
	            {
	                name: 'statusFakturSTNK',
	                label: 'Status Faktur STNK',
	                width: 120,
	            },
	            {
	                name: 'nomorSTNK',
	                label: 'Nomor STNK',
	                width: 100,
	            },
	            {
	                name: 'tanggalPenerimaanSTNKDariBiro',
	                label: 'Tgl Terima STNK',
	                width: 110,
	            },
	            {
	                name: 'platNomor',
	                label: 'Plat Nomor',
	                width: 80,
	            },
	            {
	                name: 'nomorBPKB',
	                label: 'Nomor BPKB',
	                width: 90,
	            },
	            {
	                name: 'tanggalPenerimaanBPKBDariBiro',
	                label: 'Tgl Terima BPKB',
	                width: 100,
	            },
	            {
	                name: 'tanggalTerimaSTNKOlehKonsumen',
	                label: 'Tgl Terima STNK Konsumen',
	                width: 155,
	            },
	            {
	                name: 'tanggalTerimaBPKBOlehKonsumen',
	                label: 'Tgl Terima BPKB Konsumen',
	                width: 155,
	            },
	            {
	                name: 'namaPenerima',
	                label: 'Nama Penerima',
	                width: 110,
	            },
	            {
	                name: 'jenisIdPenerima',
	                label: 'Jenis Id Penerima',
	                width: 110,
	            },
	            {
	                name: 'noIdPenerima',
	                label: 'No Id Penerima',
	                width: 110,
	            },
	            {
	                name: 'createdTime',
	                label: 'Created Time',
	                width: 130,
	            },
            ],
	        multiselect: gridFn.detail.multiSelect(),
	        onSelectRow: gridFn.detail.onSelectRow,
	        onSelectAll: gridFn.detail.onSelectAll,
	        ondblClickRow: gridFn.detail.ondblClickRow,
	        loadComplete: gridFn.detail.loadComplete,
	        listeners: {
                afterLoad: function(grid, response) {
                    $("#cb_jqGridDetail").trigger('click');
                }
            }
        }).init().setHeight(gridFn.detail.height);
    }
});
JS, \yii\web\View::POS_READY);