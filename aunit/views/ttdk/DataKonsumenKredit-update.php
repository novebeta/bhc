<?php
use common\components\Custom;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttdk */
$jenis = 'Kredit';
$params = '&id='.$id.'&action=update&DKNoView='. $dsTJualDatKonFill['DKNoView'];
$cancel = \yii\helpers\Url::toRoute(['ttdk/cancel','id'=>$id,'action'=>'update','index'=>base64_encode('ttdk/data-konsumen-kredit')]);
$this->title = str_replace('Menu','',\aunit\components\TUi::$actionMode).' - Data Konsumen Kredit: ' . $dsTJualDatKonFill['DKNoView'];
$this->params['breadcrumbs'][] = ['label' => 'Data Konsumen Kredit', 'url' => $cancel];
$this->params['breadcrumbs'][] = 'Edit';

//$cancel = Custom::url('ttdk/data-konsumen-kredit' );
//$this->title = 'Update Data Konsumen Kredit: ' . $model->DKNo;
//$this->params['breadcrumbs'][] = ['label' => 'Data Konsumen Kredit', 'url' => $cancel];
//$this->params['breadcrumbs'][] = 'Update';
//$params = '&id='.$id;
?>
<div class="ttdk-update">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTJualDatKonFill'=> $dsTJualDatKonFill,
        'jenis'=> $jenis,
        'url' => [
            'update'    => Custom::url(\Yii::$app->controller->id.'/data-konsumen-kredit-update'.$params ),
            'print'     => Custom::url(\Yii::$app->controller->id.'/print'.$params ),
            'cancel'    => $cancel,
            'detail'    => Custom::url(\Yii::$app->controller->id.'/detail' )
        ]
//        'url' => [
//            'save'    => Custom::url('ttdk/data-konsumen-kredit-update'.$params ),
//            'cancel'    => $cancel,
//        ]
    ]) ?>


</div>
