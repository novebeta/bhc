<?php
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use aunit\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
$this->title = 'Manage Dealing Process';
$this->params['breadcrumbs'][] = $this->title;
$arr = [
    'cmbTxt' => [
        'idProspect' =>	'Id Prospect',
        'idSalesPeople' =>	'Id Sales People',
        'idSpk' =>	'Id Spk',
    ],
    'cmbTxt2' => [
        'idProspect' => 'Id Prospect',
        'namaCustomer' => 'Nama Customer',
        'noKtp' => 'No Ktp',
        'alamat' => 'Alamat',
        'kodePropinsi' => 'Kode Propinsi',
        'kodeKota' => 'Kode Kota',
        'kodeKecamatan' => 'Kode Kecamatan',
        'kodeKelurahan' => 'Kode Kelurahan',
        'kodePos' => 'Kode Pos',
        'noKontak' => 'No Kontak',
        'namaBPKB' => 'Nama BPKB',
        'noKTPBPKB' => 'No KTP BPKB',
        'alamatBPKB' => 'Alamat BPKB',
        'kodePropinsiBPKB' => 'Kode Propinsi BPKB',
        'kodeKotaBPKB' => 'Kode Kota BPKB',
        'kodeKecamatanBPKB' => 'Kode Kecamatan BPKB',
        'kodeKelurahanBPKB' => 'Kode Kelurahan BPKB',
        'kodePosBPKB' => 'Kode Pos BPKB',
        'latitude' => 'Latitude',
        'longitude' => 'Longitude',
        'NPWP' => 'NPWP',
        'noKK' => 'No KK',
        'alamatKK' => 'Alamat KK',
        'kodePropinsiKK' => 'Kode Propinsi KK',
        'kodeKotaKK' => 'Kode Kota KK',
        'kodeKecamatanKK' => 'Kode Kecamatan KK',
        'kodeKelurahanKK' => 'Kode Kelurahan KK',
        'kodePosKK' => 'Kode Pos KK',
        'fax' => 'Fax',
        'email' => 'Email',
        'idSalesPeople' => 'Id Sales People',
        'idEvent' => 'Id Event',
        'tanggalPesanan' => 'Tanggal Pesanan',
        'statusSPK' => 'Status SPK'
    ],
    'cmbTgl' => [],
    'cmbNum' => [],
    'sortname' => "idProspect",
    'sortorder' => 'desc'
];
$this->registerJsVar('setcmb', $arr);
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render('../_search'); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs(<<< JS
    window.gridParam_jqGrid = {
        readOnly: true,
        rownumbers: true,
        rowNum: 10000,
        pgbuttons: false,
        pginput: false,
        viewrecords: false,
        rowList: [],
        pgtext: ""
    }
    // $('#filter-tgl-val1').change(function() {
    //     let _tgl1 = new Date($(this).val());      
    //     _tgl1.setDate(_tgl1.getDate() + 5);
	//     $('#filter-tgl-val2-disp').datepicker('update', _tgl1);
	//     $('#filter-tgl-val2-disp').trigger('change');
    // });
JS
);

$this->registerJs(\aunit\components\TdUi::mainGridJs(\aunit\models\Ttdk::class,
    'Import Manage Dealing Process', [
        'colGrid' => $colGrid,
        'url' => Url::toRoute(['ttdk/import-items','type'=>'dk','apiType'=>'spk']),
        'mode' => isset($mode) ? $mode : '',
    ]), \yii\web\View::POS_READY);
$urlDetail = Url::toRoute(['ttdk/import-details','type'=>'dk','keyCol'=>'idSpk']);
$urlDetailkk = Url::toRoute(['ttdk/import-details','type'=>'dk','keyCol'=>'idSpk','prop'=>'dataAnggotaKeluarga']);
$this->registerJs(<<< JS
jQuery(function ($) {
    if($('#jqGridDetail').length) {
        $('#jqGridDetail').utilJqGrid({
            url: '$urlDetail',
            height: 75,
            readOnly: true,
            rownumbers: true,
            rowNum: 1000,
            colModel: [
               	{
	                name: 'kodeTipeUnit',
	                label: 'Kode Tipe Unit',
	                width: 90,
	            },
               	{
	                name: 'kodeWarna',
	                label: 'Kode Warna',
	                width: 80,
	            },
               	{
	                name: 'quantity',
	                label: 'Quantity',
	                width: 80,
	            },
               	{
	                name: 'hargaJual',
	                label: 'Harga Jual',
	                width: 100,
	                template: 'money',
	            },
               	{
	                name: 'diskon',
	                label: 'Diskon',
	                width: 80,
	                template: 'money',
	                
	            },
               	{
	                name: 'kodePPN',
	                label: 'Kode PPN',
	                width: 70,
	            },
               	{
	                name: 'fakturPajak',
	                label: 'Faktur Pajak',
	                width: 80,
	            },
               	{
	                name: 'tipePembayaran',
	                label: 'Tipe Pembayaran',
	                width: 110,
	            },
               	{
	                name: 'jumlahTandaJadi',
	                label: 'Jumlah Tanda Jadi',
	                width: 120,
	                template: 'money',
	            },
               	{
	                name: 'tanggalPengiriman',
	                label: 'Tanggal Pengiriman',
	                width: 120,
	            },
               	{
	                name: 'idSalesProgram',
	                label: 'Id Sales Program',
	                width: 120,
	            },
               	{
	                name: 'idApparel',
	                label: 'Id Apparel',
	                width: 90,
	            },
               	{
	                name: 'createdTime',
	                label: 'Created Time',
	                width: 140,
	            },
            ],
	        multiselect: gridFn.detail.multiSelect(),
	        onSelectRow: gridFn.detail.onSelectRow,
	        onSelectAll: gridFn.detail.onSelectAll,
	        ondblClickRow: gridFn.detail.ondblClickRow,
	        loadComplete: gridFn.detail.loadComplete,
	        listeners: {
                afterLoad: function(grid, response) {
                    $("#cb_jqGridDetail").trigger('click');
                }
            }
        }).init();
		$('#scnjqGridDetail').utilJqGrid({
            url: '{$urlDetailkk}',
            height: 75,
            readOnly: true,
            rownumbers: true,
            colModel: [
                {
                    name: 'anggotaKK',
                    label: 'Anggota KK',
                    width: 100
                },
               	{
	                name: 'createdTime',
	                label: 'Created Time',
	                width: 130,
	            }
            ],
	        multiselect: gridFn.detail.multiSelect(),
	        onSelectRow: gridFn.detail.onSelectRow,
	        onSelectAll: gridFn.detail.onSelectAll,
	        ondblClickRow: gridFn.detail.ondblClickRow,
	        loadComplete: gridFn.detail.loadComplete,
	        listeners: {
                afterLoad: function(grid, response) {
                    $("#cb_scnjqGridDetail").trigger('click');
                }
            }
        }).init();
    }
	$("#jqGrid").bind("jqGridSelectRow", function (e, rowid, orgClickEvent) {
    var data = $(this).getRowData(rowid);
    $('#scnjqGridDetail').utilJqGrid().load({param: data});
});
});
JS, \yii\web\View::POS_READY);