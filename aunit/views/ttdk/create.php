<?php
use common\components\Custom;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttdk */

$this->title = 'Tambah Data Konsumen Kredit';
$this->params['breadcrumbs'][] = ['label' => 'Tambah Data Konsumen Kredit', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttdk-create">
        <?= $this->render('_form', [
        'model' => $model,
        'dsTJualDatKonFill'=> $dsTJualDatKonFill,
        'url' => [
	        'save'    => Custom::url('ttdk/data-konsumen-kredit-update' ),
	        'cancel'    => Custom::url('ttdk/data-konsumen-kredit' ),
        ]
    ]) ?>

</div>
