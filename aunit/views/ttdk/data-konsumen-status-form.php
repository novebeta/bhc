<?php
use aunit\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttdk */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function formatdk(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-10">' + result.id + '</div>' +
           '<div class="col-xs-14">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
if($dsTJualDatKonFill[ 'BBNPlus' ] > 0){
    $BBNPlus = true;
}
else{
    $BBNPlus = false;
}
$KMBMDK  = \aunit\models\Ttdk::find()->SelectKMBMDK( $dsTJualDatKonFill[ 'DKNo' ], '' );
$this->registerJsVar( 'KMBMDK', $KMBMDK );
$this->registerJsVar( 'KMBMDK_Index', array_column( $KMBMDK, 'KMJenis' ) );
$JA = '';
if($dsTJualDatKonFill[ 'JAPrgSubsSupplier' ] != 0 || $dsTJualDatKonFill[ 'JAPrgSubsDealer' ] != 0 ||
$dsTJualDatKonFill[ 'JAPrgSubsFincoy' ] != 0 || $dsTJualDatKonFill[ 'JADKHarga' ] != 0 ||
$dsTJualDatKonFill[ 'JADKDPTerima' ] != 0 || $dsTJualDatKonFill[ 'JADKDPLLeasing' ] != 0 ||
$dsTJualDatKonFill[ 'JABBN' ] != 0 || $dsTJualDatKonFill[ 'JAReturHarga' ] != 0 ||
$dsTJualDatKonFill[ 'JAPotonganHarga' ] != 0 || $dsTJualDatKonFill[ 'JAInsentifSales' ] != 0 ||
$dsTJualDatKonFill[ 'JAPotonganKhusus' ] != 0 ) $JA = '[JA]';
?>
    <div class="ttdk-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'frm_ttdk_id', 'action' => $url[ 'update' ] ] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelDKNo" ],
						  [ 'class' => "col-sm-3", 'items' => ":DKNo" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelSalesKode" ],
						  [ 'class' => "col-sm-6", 'items' => ":SalesKode" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelTeam" ],
						  [ 'class' => "col-sm-3", 'items' => ":Team" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelNoSPK" ],
						  [ 'class' => "col-sm-2", 'items' => ":NoSPK" ],
					  ],
					],
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelDKTgl" ],
						  [ 'class' => "col-sm-3", 'items' => ":DKTgl" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelINNo" ],
						  [ 'class' => "col-sm-2", 'items' => ":INNo" ],
						  [ 'class' => "col-sm-2", 'style' => "padding-left: 15px;", 'items' => ":LabelLeasing" ],
						  [ 'class' => "col-sm-2", 'items' => ":Leasing" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelSurveyor" ],
						  [ 'class' => "col-sm-3", 'items' => ":Surveyor" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelNoSK" ],
						  [ 'class' => "col-sm-2", 'items' => ":NoSK" ],
					  ],
					],
					[ 'class' => "form-group col-md-24", 'items' => [ 'class' => "row", 'items' => "<hr style='margin: 1px 5px;'>" ] ],
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelCusNama" ],
						  [ 'class' => "col-sm-5", 'items' => ":CusNama" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelCusKTP" ],
						  [ 'class' => "col-sm-5", 'items' => ":CusKTP" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelCusKabupaten" ],
						  [ 'class' => "col-sm-6", 'items' => ":CusKabupaten" ],
					  ],
					],
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelCusAlamat" ],
						  [ 'class' => "col-sm-5", 'items' => ":CusAlamat" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelCusRT" ],
						  [ 'class' => "col-sm-2", 'items' => ":CusRT" ],
						  [ 'class' => "col-sm-1", 'style' => "text-align:center", 'items' => ":LabelCusRW" ],
						  [ 'class' => "col-sm-2", 'items' => ":CusRW" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelCusKecamatan" ],
						  [ 'class' => "col-sm-6", 'items' => ":CusKecamatan" ],
					  ],
					],
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelCusTelepon" ],
						  [ 'class' => "col-sm-5", 'items' => ":CusTelepon" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelKodePos" ],
						  [ 'class' => "col-sm-5", 'items' => ":CusKodePos" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelCusKelurahan" ],
						  [ 'class' => "col-sm-6", 'items' => ":CusKelurahan" ],
					  ],
					],
					[ 'class' => "form-group col-md-24", 'style' => "",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelPemohon" ],
						  [ 'class' => "col-sm-5", 'items' => ":Pemohon" ],
						  [ 'class' => "col-sm-5", 'items' => ":KTPPemohon" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-2", 'items' => ":LabelAlamatPemohon" ],
						  [ 'class' => "col-sm-9", 'items' => ":AlamatPemohon" ],
					  ],
					],
					[ 'class' => "form-group col-md-24", 'items' => [ 'class' => "row", 'items' => "<hr style='margin: 1px 5px;'>" ] ],
					[ 'class' => "form-group col-md-8", 'style' => "padding-right:5px;",
					  'items' => [
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-17", 'items' => ":LabelHargaOTR" ],
							    [ 'class' => "col-sm-7", 'items' => ":HargaOTR" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-9", 'items' => ":LabelSubsidiSupplier" ],
							    [ 'class' => "col-sm-7", 'items' => ":SubsidiSupplier" ],
							    [ 'class' => "col-sm-8" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-9", 'items' => ":LabelNetSub" ],
							    [ 'class' => "col-sm-7", 'items' => ":NetSub" ],
							    [ 'class' => "col-sm-8" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-9", 'items' => ":LabelSubFin" ],
							    [ 'class' => "col-sm-7", 'items' => ":SubFin" ],
							    [ 'class' => "col-sm-8" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-9", 'items' => ":LabelPiutangKons" ],
							    [ 'class' => "col-sm-7", 'items' => ":PiutangKons" ],
							    [ 'class' => "col-sm-8" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-9", 'items' => ":LabelUMInden" ],
							    [ 'class' => "col-sm-7", 'items' => ":UMInden" ],
							    [ 'class' => "col-sm-8" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-9", 'items' => ":LabelUMDiterima" ],
							    [ 'class' => "col-sm-7", 'items' => ":UMDiterima" ],
							    [ 'class' => "col-sm-8" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-17", 'items' => ":LabelTotalDPPO" ],
							    [ 'class' => "col-sm-7", 'items' => ":TotalDPPO" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-17", 'items' => ":LabelSisaPiutang" ],
							    [ 'class' => "col-sm-7", 'items' => ":SisaPiutang" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24", 'items' => [ 'class' => "row", 'items' => "<hr style='margin: 1px 5px;'>" ] ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-17", 'items' => ":LabelReturHrgMediator" ],
							    [ 'class' => "col-sm-7", 'items' => ":ReturHrgMediator" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-17", 'items' => ":LabelSubsidiDealer2" ],
							    [ 'class' => "col-sm-7", 'items' => ":SubsidiDealer2" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-17", 'items' => ":LabelInsentifSales" ],
							    [ 'class' => "col-sm-7", 'items' => ":InsentifSales" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-17", 'items' => ":LabelBiayaBBN" ],
							    [ 'class' => "col-sm-7", 'items' => ":BiayaBBN" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-17", 'items' => ($BBNPlus ? ":LabelBBNPlus" : ":LabelPotonganKhusus") ],
							    [ 'class' => "col-sm-7", 'items' => $BBNPlus ? ":BBNPlus" : ":PotonganKhusus" ],
						    ],
						  ],
					  ],
					],

					[ 'class' => "form-group col-md-16", 'style' => "",
					  'items' => [
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-4", 'items' => ":LabelTypeMotor" ],
							    [ 'class' => "col-sm-6", 'items' => ":TypeMotor" ],
							    [ 'class' => "col-sm-3", 'items' => ":TahunMotor" ],
							    [ 'class' => "col-sm-1" ],
							    [ 'class' => "col-sm-3", 'items' => ":LabelWarnaMotor" ],
							    [ 'class' => "col-sm-7", 'items' => ":WarnaMotor" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-4", 'items' => ":LabelNoMesin" ],
							    [ 'class' => "col-sm-9", 'items' => ":NoMesin" ],
							    [ 'class' => "col-sm-1" ],
							    [ 'class' => "col-sm-3", 'items' => ":LabelNoRangka" ],
							    [ 'class' => "col-sm-7", 'items' => ":NoRangka" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-4", 'items' => ":LabelNamaMotor" ],
							    [ 'class' => "col-sm-9", 'items' => ":NamaMotor" ],
							    [ 'class' => "col-sm-1" ],
							    [ 'class' => "col-sm-3", 'items' => ":LabelKategoriMotor" ],
							    [ 'class' => "col-sm-7", 'items' => ":KategoriMotor" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24", 'items' => [ 'class' => "row", 'items' => "<hr style='margin: 1px 5px;'>" ] ],
						  [ 'class' => "form-group col-md-16",
						    'items' => [
							    [ 'class' => "form-group col-md-6",
							      'items' => [
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-20", 'items' => ":LabelTerbayar" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":1terbayar" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":2terbayar" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":3terbayar" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":blank" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":4terbayar" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ 'class' => "row", 'items' => "<hr style='margin: 1px 5px;'>" ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":5terbayar" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":6terbayar" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":7terbayar" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":8terbayar" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => $BBNPlus ? ":BBNPlusBayar" : ":9terbayar" ] ], ],
							      ],
							    ],
							    [ 'class' => "form-group col-md-6",
							      'items' => [
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-20", 'items' => ":LabelSisa" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":1sisa" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":2sisa" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":3sisa" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":blank" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":4sisa" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ 'class' => "row", 'items' => "<hr style='margin: 1px 5px;'>" ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":5sisa" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":6sisa" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":7sisa" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":8sisa" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => $BBNPlus ? ":BBNPlusSisa" : ":9sisa" ] ], ],
							      ],
							    ],
							    [ 'class' => "form-group col-md-6",
							      'items' => [
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-20", 'items' => ":LabelNoBukti" ] ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":1NoBukti" ] ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":2NoBukti" ] ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":3NoBukti" ] ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":blank" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":4NoBukti" ] ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ 'class' => "row", 'items' => "<hr style='margin: 1px 5px;'>" ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":5NoBukti" ] ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":6NoBukti" ] ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":7NoBukti" ] ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":8NoBukti" ] ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => $BBNPlus ? ":BBNPlusSisa" : ":9NoBukti" ] ] ],
							      ],
							    ],
							    [ 'class' => "form-group col-md-5",
							      'items' => [
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-20", 'items' => ":LabelTglBukti" ] ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":1TglBukti" ] ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":2TglBukti" ] ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":3TglBukti" ] ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":blank" ] ], ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":4TglBukti" ] ] ],
								      [ 'class' => "form-group col-md-20", 'items' => [ 'class' => "row", 'items' => "<hr style='margin: 1px 5px;'>" ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":5TglBukti" ] ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":6TglBukti" ] ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":7TglBukti" ] ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => ":8TglBukti" ] ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-23", 'items' => $BBNPlus ? ":BBNPlusTglBukti" : ":9TglBukti" ] ] ],
							      ],
							    ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-8",
						    'items' => [
							    [ 'class' => "form-group col-md-24",
							      'style' => "margin-top:15px",
							      'items' => [
								      [ 'class' => "form-group col-md-8", 'items' => [ [ 'class' => "col-sm-24", 'items' => ":LabelSCP" ] ] ],
								      [ 'class' => "form-group col-md-16", 'items' => [ [ 'class' => "col-sm-24", 'items' => ":SCP" ] ] ],
							      ],
							    ],
							    [ 'class' => "form-group col-md-24",
							      'items' => [
								      [ 'class' => "form-group col-md-8", 'items' => [ [ 'class' => "col-sm-24", 'items' => ":LabelSHM1" ] ] ],
								      [ 'class' => "form-group col-md-16", 'items' => [ [ 'class' => "col-sm-24", 'items' => ":SHM1" ] ] ],
							      ],
							    ],
							    [ 'class' => "form-group col-md-24",
							      'items' => [
								      [ 'class' => "form-group col-md-8", 'items' => [ [ 'class' => "col-sm-24", 'items' => ":LabelSHM2" ] ] ],
								      [ 'class' => "form-group col-md-16", 'items' => [ [ 'class' => "col-sm-24", 'items' => ":SHM2" ] ] ],
							      ],
							    ],
							    [ 'class' => "form-group col-md-24", 'items' => [ 'class' => "row", 'items' => "<hr style='margin: 1px 5px;'>" ] ],
							    [ 'class' => "form-group col-md-24",
							      'items' => [
								      [ 'class' => "form-group col-md-8", 'items' => [ [ 'class' => "col-sm-24", 'items' => ":LabelTenor" ] ] ],
								      [ 'class' => "form-group col-md-16", 'items' => [ [ 'class' => "col-sm-15", 'items' => ":Tenor" ], [ 'class' => "col-sm-1" ], [ 'class' => "col-sm-4", 'items' => ":Bulan" ] ] ],
							      ],
							    ],
							    [ 'class' => "form-group col-md-24",
							      'items' => [
								      [ 'class' => "form-group col-md-8", 'items' => [ [ 'class' => "col-sm-24", 'items' => ":LabelAngsuran" ] ] ],
								      [ 'class' => "form-group col-md-16", 'items' => [ [ 'class' => "col-sm-24", 'items' => ":Angsuran" ] ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ 'class' => "row", 'items' => "<hr style='margin: 1px 5px;'>" ] ],
								      [ 'class' => "form-group col-md-24", 'items' => [ [ 'class' => "col-sm-24", 'items' => ":Nominal" ] ] ],
								      [ 'class' => "form-group col-md-24",
								        'items' => [
									        [ 'class' => "col-sm-12", 'items' => ":LabelTOP" ],
									        [ 'class' => "col-sm-8", 'items' => ":TOP" ],
									        [ 'class' => "col-sm-4", 'style' => 'text-align:center', 'items' => ":LabelHari" ]
								        ],
								      ],
								      [ 'class' => "form-group col-md-24",
								        'items' => [
									        [ 'class' => "col-sm-12", 'items' => ":LabelNoPO" ],
									        [ 'class' => "col-sm-12", 'items' => ":NoPO" ],
								        ],
								      ],
								      [ 'class' => "form-group col-md-24",
								        'items' => [
									        [ 'class' => "col-sm-12", 'items' => ":LabelTglPO" ],
									        [ 'class' => "col-sm-12", 'items' => ":TglPO" ],
								        ],
								      ],
								      [ 'class' => "form-group col-md-24",
								        'items' => [
									        [ 'class' => "col-sm-12", 'items' => ":LabelTglTagih" ],
									        [ 'class' => "col-sm-12", 'items' => ":TglTagih" ],
								        ],
								      ],
							      ],
							    ],
						    ],
						  ],
					  ],
					],
					[ 'class' => "form-group col-md-24", 'items' => [ 'class' => "row", 'items' => "<hr style='margin: 1px 5px;'>" ] ],
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelKeterangan" ],
						  [ 'class' => "col-sm-15", 'items' => ":Keterangan" ],
						  [ 'class' => "col-sm-1"],
                          ['class' => "col-sm-2", 'items' => ":LabelZidSpk"],
                          ['class' => "col-sm-4", 'items' => ":ZidSpk"],
					  ],
					],
                    ['class' => "col-md-24",
                        'items'  => [

                        ],
                    ],
				]
			],
            [ 'class' => "row-no-gutters",
                'items' => [
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'CusNama'        => 'Nama Konsumen',
                                'DKNo'           => 'No Data Konsumen',
                                'SKNo'           => 'No Surat Jln Kons',
                                'MotorNoMesin'   => 'No Mesin',
                                'MotorNoRangka'  => 'No Rangka',
                                'MotorType'      => 'Type Motor',
                                'MotorWarna'     => 'Warna Motor',
                                'MotorTahun'     => 'Tahun Motor',
                                'LeaseKode'      => 'Leasing',
                                'TeamKode'       => 'Team',
                                'SalesKode'      => 'Kode Sales',
                                'SalesNama'      => 'Nama Sales',
                                'INNo'           => 'No Inden',
                                'DKJenis'        => 'Jenis',
                                'DKPONo'         => 'No PO',
                                'SPKNo'          => 'No SPK',
                                'CusKode'        => 'Kode Konsumen',
                                'CusAlamat'      => 'Alamat Konsumen',
                                'CusRT'          => 'RT Konsumen',
                                'CusRW'          => 'RW Konsumen',
                                'CusKelurahan'   => 'Kelurahan',
                                'CusKecamatan'   => 'Kecamatan',
                                'CusKabupaten'   => 'Kabupaten',
                                'CusTelepon'     => 'Telepon',
                                'CusTelepon2'    => 'Telepon 2',
                                'DKMemo'         => 'Keterangan DK',
                                'MotorMemo'      => 'Keterangan Motor',
                                'NoticeNo'       => 'No Notice',
                                'STNKNo'         => 'No STNK',
                                'PlatNo'         => 'No Plat',
                                'BPKBNo'         => 'No BPKB',
                                'FakturAHMNo'    => 'No Faktur AHM',
                                'NoticePenerima' => 'Penerima Notice',
                                'STNKPenerima'   => 'Penerima STNK',
                                'PlatPenerima'   => 'Penerima Plat',
                                'BPKBPenerima'   => 'Penerima BPKB',
                                'STNKNama'       => 'Nama STNK',
                                'STNKAlamat'     => 'Alamat STNK',
                                'UserID'         => 'User ID',
                                'DKLunas'        => 'Status Lunas',
                                'ProgramNama'    => 'Nama Program',
                                'NamaHadiah'     => 'Nama Hadiah',
                                'SSNo'           => 'No Surat Jln Supplier',
                                'FBNo'           => 'No Faktur Beli',
                                'PDNo'           => 'No Penerimaan Dealer',
                                'SDNo'           => 'No Surat Jln Dealer',
                            ],
                        ])
                    ],
                    ['class' => "pull-right", 'items' => ":btnImport :btnAction"],
                ]
            ],
			[
				/* label */
				":LabelDKNo"             => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor</label>',
				":LabelSalesKode"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sales</label>',
				":LabelTeam"             => '<label class="control-label" style="margin: 0; padding: 6px 0;">Team</label>',
				":LabelNoSPK"            => '<label class="control-label" style="margin: 0; padding: 6px 0;">No SPK</label>',
				":LabelDKTgl"            => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tanggal</label>',
				":LabelLeasing"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">Leasing</label>',
				":LabelSurveyor"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Surveyor</label>',
				":LabelNoSK"             => '<label class="control-label" style="margin: 0; padding: 6px 0;">No SK</label>',
				":LabelINNo"             => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Inden</label>',
				":LabelCusKabupaten"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kabupaten</label>',
				":LabelCusKecamatan"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kecamatan</label>',
				":LabelCusKelurahan"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kelurahan</label>',
				":LabelCusNama"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">Konsumen</label>',
				":LabelCusAlamat"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
				":LabelCusKTP"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">KTP</label>',
				":LabelCusRT"            => '<label class="control-label" style="margin: 0; padding: 6px 0;">RT / RW</label>',
				":LabelCusRW"            => '<label class="control-label" style="margin: 0; padding: 6px 0;">/</label>',
				":LabelCusTelepon"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon</label>',
				":LabelKodePos"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">KodePos</label>',
				":LabelPemohon"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pemohon-KTP</label>',
				":LabelAlamatPemohon"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
				":LabelHargaOTR"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Harga OTR</label>',
				":LabelSubsidiSupplier"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Subsidi Supplier</label>',
				":LabelNetSub"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">Net Sub Dealer 1</label>',
				":LabelSubFin"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">Subsidi Fincoy</label>',
				":LabelPiutangKons"      => '<a onclick="linkKasBankMasuk(\'Piutang UM\')" href="javascript:void(0)" ><label id="lblPiutangUM" class="control-label" style="margin: 0; padding: 6px 0;">Piutang Kons UM</label></a>',
				":LabelUMInden"          => '<a onclick="linkKasBankMasuk(\'Inden\')" href="javascript:void(0)"><label id="lblUMInden" class="control-label" style="margin: 0; padding: 6px 0;">UM Inden</label></a>',
				":LabelUMDiterima"       => '<a onclick="linkKasBankMasuk(\'' . $dsTJualDatKonFill[ 'DKJenis' ] . '\')" href="javascript:void(0)"><label id="lblUMDiterima" class="control-label" style="margin: 0; padding: 6px 0;">UM Diterima</label></a>',
				":LabelTotalDPPO"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total DP PO Leasing</label>',
				":LabelSisaPiutang"      => '<a onclick="linkKasBankMasuk(\'Leasing\')" href="javascript:void(0)"><label id="lblLeasing" class="control-label" style="margin: 0; padding: 6px 0;">Sisa Piutang</label></a>',
				":LabelTypeMotor"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Type / Tahun</label>',
				":LabelWarnaMotor"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Warna</label>',
				":LabelNoMesin"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Mesin</label>',
				":LabelNoRangka"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Rangka</label>',
				":LabelNamaMotor"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Motor</label>',
				":LabelKategoriMotor"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kategori</label>',
				":LabelTerbayar"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Terbayar</label>',
				":LabelSisa"             => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sisa</label>',
				":LabelNoBukti"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Bukti</label>',
				":LabelTglBukti"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl Bukti</label>',
				":blank"                 => '<label class="control-label" style="height:23px; margin: 0; padding: 6px 0;"></label>',
				":LabelSCP"              => '<label class="control-label" style="margin: 0; padding: 6px 0;">SCP</label>',
				":LabelSHM1"             => '<label class="control-label" style="margin: 0; padding: 6px 0;">Scheme 1</label>',
				":LabelSHM2"             => '<label class="control-label" style="margin: 0; padding: 6px 0;">Scheme 2</label>',
				":LabelTenor"            => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tenor</label>',
				":LabelAngsuran"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Angsuran</label>',
				":Bulan"                 => '<label class="control-label" style="margin: 0; padding: 6px 0;">Bulan</label>',
				":LabelReturHrgMediator" => '<a onclick="linkKasBankKeluar(\'Retur Harga\')" href="javascript:void(0)"><label id="lblReturHarga" class="control-label" style="margin: 0; padding: 6px 0;">Retur Harga - Mediator</label></a>',
				":LabelSubsidiDealer2"   => '<a onclick="linkKasBankKeluar(\'SubsidiDealer2\')" href="javascript:void(0)"><label id="lblSubsidiDealer2" class="control-label" style="margin: 0; padding: 6px 0;">Subsidi Dealer 2 '.$JA.'</label></a>',
				":LabelInsentifSales"    => '<a onclick="linkKasBankKeluar(\'Insentif Sales\')" href="javascript:void(0)"><label id="lblInsentifSales" class="control-label" style="margin: 0; padding: 6px 0;">Insentif Sales</label></a>',
				":LabelBiayaBBN"         => '<label  id="lblBiayaBBN" class="control-label" style="margin: 0; padding: 6px 0;">Biaya BBN</label>',
				":LabelPotonganKhusus"   => '<a onclick="linkKasBankKeluar(\'Potongan Khusus\')" href="javascript:void(0)"><label id="lblPotonganKhusus" class="control-label" style="margin: 0; padding: 6px 0;">Potongan Khusus</label></a>',
				":LabelBBNPlus"          => '<a onclick="linkKasBankKeluar(\'BBN Plus\')" href="javascript:void(0)"><label id="lblBBNPlus" class="control-label" style="margin: 0; padding: 6px 0;">BBN Progresif</label></a>',
				":LabelTOP"              => '<label class="control-label" style="margin: 0; padding: 6px 0;">TOP</label>',
				":LabelHari"             => '<label class="control-label" style="margin: 0; padding: 6px 0;">Hari</label>',
				":LabelNoPO"             => '<label class="control-label" style="margin: 0; padding: 6px 0;">No PO</label>',
				":LabelTglPO"            => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl PO</label>',
				":LabelTglTagih"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">TglTagih</label>',
				":LabelKeterangan"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',

				/* componen */
				":KTPPemohon"            => Html::textInput( 'CusPembeliKTP', $dsTJualDatKonFill[ 'CusPembeliKTP' ], [ 'class' => 'form-control', 'readonly' => false ] ),
				":CusAlamat"             => Html::textInput( 'CusAlamat', $dsTJualDatKonFill[ 'CusAlamat' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":CusRT"                 => Html::textInput( 'CusRT', $dsTJualDatKonFill[ 'CusRT' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":CusRW"                 => Html::textInput( 'CusRW', $dsTJualDatKonFill[ 'CusRW' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":CusTelepon"            => Html::textInput( 'CusTelepon', $dsTJualDatKonFill[ 'CusTelepon' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":CusNama"               => Html::textInput( 'CusNama', $dsTJualDatKonFill[ 'CusNama' ], [ 'class' => 'form-control', 'readonly' => true ] ) . Html::textInput( 'CusKode', $dsTJualDatKonFill[ 'CusKode' ], [ 'class' => 'hidden' ] ),
				":CusKTP"                => Html::textInput( 'CusKTP', $dsTJualDatKonFill[ 'CusKTP' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":CusKodePos"            => Html::textInput( 'CusNama', $dsTJualDatKonFill[ 'CusKodePos' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":CusKabupaten"          => Html::textInput( 'CusNama', $dsTJualDatKonFill[ 'CusKabupaten' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":CusKecamatan"          => Html::textInput( 'CusNama', $dsTJualDatKonFill[ 'CusKecamatan' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":CusKelurahan"          => Html::textInput( 'CusNama', $dsTJualDatKonFill[ 'CusKelurahan' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Pemohon"               => Html::textInput( 'CusPembeliNama', $dsTJualDatKonFill[ 'CusPembeliNama' ], [ 'class' => 'form-control', 'readonly' => false ] ),
				":AlamatPemohon"         => Html::textInput( 'CusPembeliAlamat', $dsTJualDatKonFill[ 'CusPembeliAlamat' ], [ 'class' => 'form-control', 'readonly' => false ] ),
				":HargaOTR"              => FormField::decimalInput( [ 'name' => 'DKHarga', 'config' => [ 'value' => $dsTJualDatKonFill[ 'DKHarga' ], 'readonly' => true ] ] ),
				":SubsidiSupplier"       => FormField::decimalInput( [ 'name' => 'PrgSubsSupplier', 'config' => [ 'value' => $dsTJualDatKonFill[ 'PrgSubsSupplier' ], 'readonly' => true ] ] ),
				":NetSub"                => FormField::decimalInput( [ 'name' => 'PrgSubsDealer', 'config' => [ 'value' => $dsTJualDatKonFill[ 'PrgSubsDealer' ], 'readonly' => true ] ] ),
				":SubFin"                => FormField::decimalInput( [ 'name' => 'PrgSubsFincoy', 'config' => [ 'value' => $dsTJualDatKonFill[ 'PrgSubsFincoy' ], 'readonly' => true ] ] ),
				":PiutangKons"           => FormField::decimalInput( [ 'name' => 'DKDPLLeasing', 'config' => [ 'value' => $dsTJualDatKonFill[ 'DKDPLLeasing' ], 'readonly' => true ] ] ),
				":UMInden"               => FormField::decimalInput( [ 'name' => 'DKDPInden', 'config' => [ 'value' => $dsTJualDatKonFill[ 'DKDPInden' ], 'readonly' => true ] ] ),
				":UMDiterima"            => FormField::decimalInput( [ 'name' => 'DKDPTerima', 'config' => [ 'value' => $dsTJualDatKonFill[ 'DKDPTerima' ], 'readonly' => true ] ] ),
				":TotalDPPO"             => FormField::decimalInput( [ 'name' => 'DPPO', 'config' => [ 'value' => $dsTJualDatKonFill[ 'DPPO' ], 'readonly' => true ] ] ),
				":SisaPiutang"           => FormField::decimalInput( [ 'name' => 'DKNetto', 'config' => [ 'value' => $dsTJualDatKonFill[ 'DKNetto' ], 'readonly' => true ] ] ),
				":TypeMotor"             => Html::textInput( 'MotorType', $dsTJualDatKonFill[ 'MotorType' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":TahunMotor"            => Html::textInput( 'MotorTahun', $dsTJualDatKonFill[ 'MotorTahun' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":WarnaMotor"            => Html::textInput( 'MotorWarna', $dsTJualDatKonFill[ 'MotorWarna' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NoMesin"               => Html::textInput( 'MotorNoMesin', $dsTJualDatKonFill[ 'MotorNoMesin' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NoRangka"              => Html::textInput( 'MotorNoRangka', $dsTJualDatKonFill[ 'MotorNoRangka' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NamaMotor"             => Html::textInput( 'MotorNama', $dsTJualDatKonFill[ 'MotorNama' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":KategoriMotor"         => Html::textInput( 'MotorKategori', $dsTJualDatKonFill[ 'MotorKategori' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":1terbayar"             => FormField::decimalInput( [ 'name' => 'UMPKBayar', 'config' => [ 'value' => $dsTJualDatKonFill[ 'UMPKBayar' ], 'readonly' => true ] ] ),
				":2terbayar"             => FormField::decimalInput( [ 'name' => 'UMIndenBayar', 'config' => [ 'value' => $dsTJualDatKonFill[ 'UMIndenBayar' ], 'readonly' => true ] ] ),
				":3terbayar"             => FormField::decimalInput( [ 'name' => 'UMDBayar', 'config' => [ 'value' => $dsTJualDatKonFill[ 'UMDBayar' ], 'readonly' => true ] ] ),
				":4terbayar"             => FormField::decimalInput( [ 'name' => 'DKNettoBayar', 'config' => [ 'value' => $dsTJualDatKonFill[ 'DKNettoBayar' ], 'readonly' => true ] ] ),
				":5terbayar"             => FormField::decimalInput( [ 'name' => 'RHBayar', 'config' => [ 'value' => $dsTJualDatKonFill[ 'RHBayar' ], 'readonly' => true ] ] ),
				":6terbayar"             => FormField::decimalInput( [ 'name' => 'SD2Bayar', 'config' => [ 'value' => $dsTJualDatKonFill[ 'SD2Bayar' ], 'readonly' => true ] ] ),
				":7terbayar"             => FormField::decimalInput( [ 'name' => 'ISBayar', 'config' => [ 'value' => $dsTJualDatKonFill[ 'ISBayar' ], 'readonly' => true ] ] ),
				":8terbayar"             => FormField::decimalInput( [ 'name' => 'BBNBayar', 'config' => [ 'value' => $dsTJualDatKonFill[ 'BBNBayar' ], 'readonly' => true ] ] ),
				":9terbayar"             => FormField::decimalInput( [ 'name' => 'PKBayar', 'config' => [ 'value' => $dsTJualDatKonFill[ 'PKBayar' ], 'readonly' => true ] ] ),
				":BBNPlusBayar"          => FormField::decimalInput( [ 'name' => 'BBNPlusBayar', 'config' => [ 'value' => $dsTJualDatKonFill[ 'BBNPlusBayar' ], 'readonly' => true ] ] ),
				":1sisa"                 => FormField::decimalInput( [ 'name' => 'UMPKSisa', 'config' => [ 'value' => $dsTJualDatKonFill[ 'UMPKSisa' ], 'readonly' => true ] ] ),
				":2sisa"                 => FormField::decimalInput( [ 'name' => 'UMIndenSisa', 'config' => [ 'value' => $dsTJualDatKonFill[ 'UMIndenSisa' ], 'readonly' => true ] ] ),
				":3sisa"                 => FormField::decimalInput( [ 'name' => 'UMDSisa', 'config' => [ 'value' => $dsTJualDatKonFill[ 'UMDSisa' ], 'readonly' => true ] ] ),
				":4sisa"                 => FormField::decimalInput( [ 'name' => 'DKNettoSisa', 'config' => [ 'value' => $dsTJualDatKonFill[ 'DKNettoSisa' ], 'readonly' => true ] ] ),
				":5sisa"                 => FormField::decimalInput( [ 'name' => 'RHSisa', 'config' => [ 'value' => $dsTJualDatKonFill[ 'RHSisa' ], 'readonly' => true ] ] ),
				":6sisa"                 => FormField::decimalInput( [ 'name' => 'SD2Sisa', 'config' => [ 'value' => $dsTJualDatKonFill[ 'SD2Sisa' ], 'readonly' => true ] ] ),
				":7sisa"                 => FormField::decimalInput( [ 'name' => 'ISSisa', 'config' => [ 'value' => $dsTJualDatKonFill[ 'ISSisa' ], 'readonly' => true ] ] ),
				":8sisa"                 => FormField::decimalInput( [ 'name' => 'BBNSisa', 'config' => [ 'value' => $dsTJualDatKonFill[ 'BBNSisa' ], 'readonly' => true ] ] ),
				":9sisa"                 => FormField::decimalInput( [ 'name' => 'PKSisa', 'config' => [ 'value' => $dsTJualDatKonFill[ 'PKSisa' ], 'readonly' => true ] ] ),
				":BBNPlusSisa"           => FormField::decimalInput( [ 'name' => 'BBNPlusSisa', 'config' => [ 'value' => $dsTJualDatKonFill[ 'BBNPlusSisa' ], 'readonly' => true ] ] ),
				":1NoBukti"              => Html::textInput( 'UMPKNoBukti', $dsTJualDatKonFill[ 'UMPKNoBukti' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":2NoBukti"              => Html::textInput( 'UMIndenNoBukti', $dsTJualDatKonFill[ 'UMIndenNoBukti' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":3NoBukti"              => Html::textInput( 'UMDNoBukti', $dsTJualDatKonFill[ 'UMDNoBukti' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":4NoBukti"              => Html::textInput( 'DKNettoNoBukti', $dsTJualDatKonFill[ 'DKNettoNoBukti' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":5NoBukti"              => Html::textInput( 'RHNoBukti', $dsTJualDatKonFill[ 'RHNoBukti' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":6NoBukti"              => Html::textInput( 'SD2NoBukti', $dsTJualDatKonFill[ 'SD2NoBukti' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":7NoBukti"              => Html::textInput( 'ISNoBukti', $dsTJualDatKonFill[ 'ISNoBukti' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":8NoBukti"              => Html::textInput( 'BBNNoBukti', $dsTJualDatKonFill[ 'BBNNoBukti' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":9NoBukti"              => Html::textInput( 'PKNoBukti', $dsTJualDatKonFill[ 'PKNoBukti' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":BBNPlusNoBukti"        => Html::textInput( 'BBNPlusNoBukti', $dsTJualDatKonFill[ 'BBNPlusNoBukti' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":1TglBukti"             => Html::textInput( 'UMPKTglBukti', General::asDate( $dsTJualDatKonFill[ 'UMPKTglBukti' ],'d/m/Y' ), [ 'class' => 'form-control', 'readonly' => true ] ),
				":2TglBukti"             => Html::textInput( 'UMIndenTglBukti', General::asDate( $dsTJualDatKonFill[ 'UMIndenTglBukti' ],'d/m/Y' ), [ 'class' => 'form-control', 'readonly' => true ] ),
				":3TglBukti"             => Html::textInput( 'UMDTglBukti', General::asDate( $dsTJualDatKonFill[ 'UMDTglBukti' ],'d/m/Y' ), [ 'class' => 'form-control', 'readonly' => true ] ),
				":4TglBukti"             => Html::textInput( 'DKNettoTglBukti', General::asDate( $dsTJualDatKonFill[ 'DKNettoTglBukti' ],'d/m/Y' ), [ 'class' => 'form-control', 'readonly' => true ] ),
				":5TglBukti"             => Html::textInput( 'RHTglBukti', General::asDate( $dsTJualDatKonFill[ 'RHTglBukti' ],'d/m/Y' ), [ 'class' => 'form-control', 'readonly' => true ] ),
				":6TglBukti"             => Html::textInput( 'SD2TglBukti', General::asDate( $dsTJualDatKonFill[ 'SD2TglBukti' ] ,'d/m/Y'), [ 'class' => 'form-control', 'readonly' => true ] ),
				":7TglBukti"             => Html::textInput( 'ISTglBukti', General::asDate( $dsTJualDatKonFill[ 'ISTglBukti' ] ,'d/m/Y'), [ 'class' => 'form-control', 'readonly' => true ] ),
				":8TglBukti"             => Html::textInput( 'BBNTglBukti', General::asDate( $dsTJualDatKonFill[ 'BBNTglBukti' ] ,'d/m/Y'), [ 'class' => 'form-control', 'readonly' => true ] ),
				":9TglBukti"             => Html::textInput( 'PKTglBukti', General::asDate( $dsTJualDatKonFill[ 'PKTglBukti' ] ,'d/m/Y'), [ 'class' => 'form-control', 'readonly' => true ] ),
				":BBNPlusTglBukti"       => Html::textInput( 'BBNPlusTglBukti', General::asDate( $dsTJualDatKonFill[ 'BBNPlusTglBukti' ],'d/m/Y' ), [ 'class' => 'form-control', 'readonly' => true ] ),
				":SCP"                   => FormField::decimalInput( [ 'name' => 'DKSCP', 'config' => [ 'value' => $dsTJualDatKonFill[ 'DKSCP' ], 'readonly' => true ] ] ),
				":SHM1"                  => FormField::decimalInput( [ 'name' => 'DKScheme', 'config' => [ 'value' => $dsTJualDatKonFill[ 'DKScheme' ], 'readonly' => true ] ] ),
				":SHM2"                  => FormField::decimalInput( [ 'name' => 'DKScheme2', 'config' => [ 'value' => $dsTJualDatKonFill[ 'DKScheme2' ], 'readonly' => true ] ] ),
				":Tenor"                 => FormField::integerInput( [ 'name' => 'DKTenor', 'config' => [ 'value' => $dsTJualDatKonFill[ 'DKTenor' ], 'readonly' => true ] ] ),
				":Angsuran"              => FormField::decimalInput( [ 'name' => 'DKAngsuran', 'config' => [ 'value' => $dsTJualDatKonFill[ 'DKAngsuran' ], 'readonly' => true ] ] ),
				":ReturHrgMediator"      => FormField::decimalInput( [ 'name' => 'ReturHarga', 'config' => [ 'value' => $dsTJualDatKonFill[ 'ReturHarga' ], 'readonly' => true ] ] ),
				":SubsidiDealer2"        => FormField::decimalInput( [ 'name' => 'PotonganHarga', 'config' => [ 'value' => $dsTJualDatKonFill[ 'PotonganHarga' ], 'readonly' => true ] ] ),
				":BiayaBBN"              => FormField::decimalInput( [ 'name' => 'BBN', 'config' => [ 'value' => $dsTJualDatKonFill[ 'BBN' ], 'readonly' => true ] ] ),
				":InsentifSales"         => FormField::decimalInput( [ 'name' => 'InsentifSales', 'config' => [ 'value' => $dsTJualDatKonFill[ 'InsentifSales' ], 'readonly' => true ] ] ),
				":PotonganKhusus"        => FormField::decimalInput( [ 'name' => 'PotonganKhusus', 'config' => [ 'value' => $dsTJualDatKonFill[ 'PotonganKhusus' ], 'readonly' => true ] ] ),
//				":BBNProgresif"          => FormField::decimalInput( [ 'name' => 'BBNPlus', 'config' => [ 'value' => $dsTJualDatKonFill[ 'BBNPlus' ], 'readonly' => true ] ] ),
				":BBNPlus"               => FormField::decimalInput( [ 'name' => 'BBNPlus', 'config' => [ 'value' => $dsTJualDatKonFill[ 'BBNPlus' ], 'readonly' => true ] ] ),
				":TOP"                   => FormField::integerInput( [ 'name' => 'DKTOP', 'config' => [ 'value' => $dsTJualDatKonFill[ 'DKTOP' ] ] ] ),
				":NoPO"                  => Html::textInput( 'DKPONo', $dsTJualDatKonFill[ 'DKPONo' ], [ 'class' => 'form-control', 'tabindex' => '5' ] ),
				":Nominal"               => Html::textInput( 'DKMediator', $dsTJualDatKonFill[ 'DKMediator' ], [ 'class' => 'form-control', 'tabindex' => '3' ] ),
				":Keterangan"            => Html::textInput( 'DKMemo', $dsTJualDatKonFill[ 'DKMemo' ], [ 'class' => 'form-control', 'readonly' => false ] ),
				":TglPO"                 => FormField::dateInput( [ 'name' => 'DKPOTgl', 'config' => [ 'value' => $dsTJualDatKonFill[ 'DKPOTgl' ] ] ] ),
				":TglTagih"              => FormField::dateInput( [ 'name' => 'DKTglTagih', 'config' => [ 'value' => $dsTJualDatKonFill[ 'DKTglTagih' ] ] ] ),
				":DKNo"                  => Html::textInput( 'DKNoView', $dsTJualDatKonFill[ 'DKNoView' ], [ 'class' => 'form-control', 'readonly' => true, 'tabindex' => '1' ] ) .
				                            Html::hiddenInput( 'DKNo', $dsTJualDatKonFill[ 'DKNo' ], [ 'id' => 'DKNo' ] ) .
				                            Html::hiddenInput( 'StatPrint', null, [ 'id' => 'StatPrint' ] ),
				":Team"                  => Html::textInput( 'TeamKode', $dsTJualDatKonFill[ 'TeamKode' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NoSPK"                 => Html::textInput( 'SPKNo', $dsTJualDatKonFill[ 'SPKNo' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":INNo"                  => Html::textInput( 'INNo', $dsTJualDatKonFill[ 'INNo' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Leasing"               => Html::textInput( 'LeaseKode', $dsTJualDatKonFill[ 'LeaseKode' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Surveyor"              => Html::textInput( 'DKSurveyor', $dsTJualDatKonFill[ 'DKSurveyor' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":NoSK"                  => Html::textInput( 'SKNo', $dsTJualDatKonFill[ 'SKNo' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":SalesKode"             => FormField::combo( 'SalesKode', [ 'config' => [ 'value' => $dsTJualDatKonFill[ 'SalesKode' ] ], 'options' => [ 'tabindex' => '2' ], 'extraOptions' => [ 'altLabel' => [ 'SalesNama' ] ] ] ),
				":DKTgl"                 => Html::textInput( 'DKTgl', General::asDate( $dsTJualDatKonFill[ 'DKTgl' ],'d/m/Y' ), [ 'class' => 'form-control', 'readonly' => true ] ),
				/* button */

                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
                ":btnImport"      => Html::button('<i class="fa fa-arrow-circle-down"><br><br>Import</i>   ', ['class' => 'btn bg-navy ', 'id' => 'btnImport', 'style' => 'width:60px;height:60px']),
//                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
//                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),


//                ":btnImport" => Html::button( '<i class="fa fa-arrow-circle-down"></i>  Import ', [ 'class' => 'btn bg-navy ', 'id' => 'btnImport', 'style' => 'width:80px' ] ),
//                ":btnSave"               => Html::button( '<i class="glyphicon glyphicon-floppy-disk"></i> Simpan', [ 'class' => 'btn btn-info btn-primary ', 'id' => 'btnSave' . Custom::viewClass() ] ),
//				":btnCancel"             => Html::Button( '<i class="fa fa-ban"></i> Batal', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:80px' ] ),
//				":btnAdd"                => "",
//				":btnEdit"               => Html::button( '<i class="fa fa-edit"></i> Edit', [ 'class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:80px' ] ),
//				":btnDaftar"             => Html::Button( '<i class="fa fa-list"></i> Daftar ', [ 'class' => 'btn btn-primary ', 'id' => 'btnDaftar', 'style' => 'width:80px' ] ),

                ":LabelZidSpk"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">ID SPK</label>',
                ":ZidSpk"           => Html::textInput('ZidSpk', $dsTJualDatKonFill[ 'ZidSpk' ], ['class' => 'form-control']),
			],
            [
                '_akses' => 'Data Konsumen Status',
                'url_main' => 'ttdk',
                'url_id' => $_GET['id'] ?? '',
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlIndex      = Url::toRoute( [ 'ttdk/data-konsumen-status' ] );
$urlProsesLink = Url::toRoute( [ 'ttdk/proses-link' ] );
$urlDetail = Url::toRoute( [ 'ttdk/status-import-loop' ] );
$urlListTransactions = Url::toRoute( [ 'ttdk/list-transactions','DKNo'=> $dsTJualDatKonFill[ 'DKNo' ]] );
$urlPrint      = $url[ 'print' ];
//$urlImport        = Url::toRoute( [ 'ttdk/importdkstatus','tgl1'    => Yii::$app->formatter->asDate( '-5 day', 'yyyy-MM-dd') ] );
$urlImport        = Url::toRoute( [ 'ttdk/importdkstatus','tgl1'    => Yii::$app->formatter->asDate( 'now', 'yyyy-MM-dd') ] );
$this->registerJsVar( '__Jenis', $jenis );
$this->registerJsVar( '__CSRFname', Yii::$app->request->csrfParam );
$this->registerJsVar( '__CSRFvalue', Yii::$app->request->csrfToken );
$this->registerJs( <<< JS

	$(document.body).keydown(function( event ) {
      if ( event.which !== 118 )  return;
         event.preventDefault();
		 window.util.app.dialogListData.show({
            title: 'Daftar Transaksi Keuangan yang Melibatkan DK Nomor : {$dsTJualDatKonFill[ 'DKNo' ]}',
            url: '{$urlListTransactions}'
        },
        function (data) {
             if (!Array.isArray(data)){
                 return;
             }
        });
    });

    function onFocus(){
        // console.log(window.BHC_tabproses.closed);
        if (window.BHC_tabproses.closed){
            location.reload();
        }else{            
            window.BHC_tabproses.focus();            
        }
    };

    function prosesLink(param){
        param.DKNo = $('[name="DKNo"]').val();
        param.INNo = $('[name="INNo"]').val();
        param.CusNama = $('[name="CusNama"]').val();
        param.DKMediator = $('[name="DKMediator"]').val();
        param.LeaseKode = $('[name="LeaseKode"]').val();
        console.log(JSON.stringify(param));
        var url = "{$urlProsesLink}&data="+Base64.encode(JSON.stringify(param));
        window.BHC_tabproses = window.open(url);
        $( document ).focus(function() {
          setTimeout(onFocus, 10);
          clearInterval(window.intFocus);
          window.intFocus = setInterval(onFocus, 500);
        });
        window.onfocus = function() {
          setTimeout(onFocus, 10);
          clearInterval(window.intFocus);
          window.intFocus = setInterval(onFocus, 500);
        }
    }
    window.prosesLink = prosesLink;
    function linkKasBankMasuk(jenis) {
        let searchParams = new URLSearchParams(window.location.search);
        if (searchParams.get('action') === 'view' || searchParams.get('action') === 'display'){
            return ;
        }
        var sisa = 0, nominal = 0, link = '--';
        switch (jenis) {
          case 'Piutang UM':
              nominal = $('#DKDPLLeasing').utilNumberControl().val();
              sisa = $('#UMPKSisa').utilNumberControl().val();  
              link = btoa($('[name="UMPKNoBukti"]').val());
              break;
          case 'Inden':
              nominal = $('#DKDPInden').utilNumberControl().val();
              sisa = $('#UMIndenSisa').utilNumberControl().val();     
              link = btoa($('[name="UMIndenNoBukti"]').val());             
              break;
          case 'Tunai':
          case 'Kredit':
              nominal = $('#DKDPTerima').utilNumberControl().val();
              sisa = $('#UMDSisa').utilNumberControl().val();  
              link = btoa($('[name="UMDNoBukti"]').val());                
              break;
          case 'Leasing':
              var LeaseKode = $('[name="LeaseKode"]').val();
              if(__Jenis.toLowerCase() === 'tunai' || LeaseKode.toLowerCase() === 'tunai'){
                  jenis = 'Piutang Sisa';
              } 
              nominal = $('#DKNetto').utilNumberControl().val();
              sisa = $('#DKNettoSisa').utilNumberControl().val();  
              link = btoa($('[name="DKNettoNoBukti"]').val());                
              break;
        }
        if (nominal === 0){
            return ;
        }
        var BHC_param = {
                sisa: sisa, 
                nominal: nominal,
                jenis: jenis ,
                link: link ,
                arus: 'masuk',
            }
        if (parseFloat(sisa) > 0){            
            var dialog = bootbox.dialog({
                title: 'Pilih Metode Pembayaran :',           
                message: '<button id="btnKas" type="button" class="btn bg-green btn-lg" style="width:130px;"><i class="fa fa-money"></i> Kas</button><button id="btnBank" type="button" class="btn bg-yellow btn-lg" style="width:130px;margin-left:5px;"><i class="fa fa-bank"></i> Bank</button>',
                size: 'small',
                closeButton: true
            });   
            $('#btnKas').click(function (event) {
                BHC_param.target = 'kas';
               prosesLink(BHC_param);
            });
            $('#btnBank').click(function (event) {
                BHC_param.target = 'bank';
               prosesLink(BHC_param);
            });
        }else{
            BHC_param.target = 'show';
           prosesLink(BHC_param);
        }
    }
    window.linkKasBankMasuk = linkKasBankMasuk;
    function linkKasBankKeluar(jenis) {        
        let searchParams = new URLSearchParams(window.location.search);
        if (searchParams.get('action') === 'view' || searchParams.get('action') === 'display'){
            return ;
        }
        var sisa = 0, nominal = 0, link = '--';
      switch (jenis) {
          case 'Retur Harga':
              nominal = $('#ReturHarga').utilNumberControl().val();
              sisa = $('#RHSisa').utilNumberControl().val();
              link = btoa($('[name="RHNoBukti"]').val());
              break;          
          case 'SubsidiDealer2':
              nominal = $('#PotonganHarga').utilNumberControl().val();
              sisa = $('#SD2Sisa').utilNumberControl().val();   
              link = btoa($('[name="SD2NoBukti"]').val());               
              break;
          case 'BBN Plus':
              nominal = $('#BBNPlus').utilNumberControl().val();
              sisa = $('#BBNPlusSisa').utilNumberControl().val(); 
              link = btoa($('[name="BBNPlusNoBukti"]').val());                 
              break;
          case 'Insentif Sales':
              nominal = $('#InsentifSales').utilNumberControl().val();
              sisa = $('#ISSisa').utilNumberControl().val();   
              link = btoa($('[name="ISNoBukti"]').val());               
              break;
          case 'Potongan Khusus':
              nominal = $('#PotonganKhusus').utilNumberControl().val();
              sisa = $('#PKSisa').utilNumberControl().val();   
              link = btoa($('[name="PKNoBukti"]').val());               
              break;
        }        
        if (nominal === 0){
            return ;
        }
        var BHC_param = {
                sisa: sisa, 
                nominal: nominal,
                jenis: jenis ,
                link: link ,
                arus: 'keluar'
            }
        if (parseFloat(sisa) > 0){            
            var dialog = bootbox.dialog({
                title: 'Pilih Metode Pembayaran :',           
                message: '<button id="btnKas" type="button" class="btn bg-green btn-lg" style="width:130px;"><i class="fa fa-money"></i> Kas</button><button id="btnBank" type="button" class="btn bg-yellow btn-lg" style="width:130px;margin-left:5px;"><i class="fa fa-bank"></i> Bank</button>',
                size: 'small',
                closeButton: true
            });   
            $('#btnKas').click(function (event) {
                BHC_param.target = 'kas';
               prosesLink(BHC_param);
            });
            $('#btnBank').click(function (event) {
                BHC_param.target = 'bank';
               prosesLink(BHC_param);
            });
        }else{
            BHC_param.target = 'show';
           prosesLink(BHC_param);
        }
    }
    window.linkKasBankKeluar = linkKasBankKeluar;
    function submitPrint(mode){
            $('#StatPrint').val(mode);                
            $('#frm_ttdk_id').submit();
    }   
    
    
    $('#btnPrint').click(function (event) {	      
        $('#frm_ttdk_id').attr('action','$urlPrint');
        $('#frm_ttdk_id').attr('target','_blank');
        submitPrint(3);
        $('#frm_ttdk_id').submit();
	  }); 


     $('#btnSave').click(function (event) {	      
       $('#frm_ttdk_id').attr('action','{$url['update']}');
       $('#frm_ttdk_id').attr('target','');
       $('#frm_ttdk_id').submit();
    });
     
     $('#btnCancel').click(function (event) {	      
       $('#frm_ttdk_id').attr('action','{$url['cancel']}');
       $('#frm_ttdk_id').attr('target','');
       $('#frm_ttdk_id').submit();
    });
     
     $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  }); 
     
     $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
     
     function setColor(){
         $('#lblPiutangUM').css({"color":"{$dsTJualDatKonFill['lblPiutangUM']}"});
         $('#lblReturHarga').css({"color":"{$dsTJualDatKonFill['lblReturHarga']}"});
         $('#lblSubsidiDealer2').css({"color":"{$dsTJualDatKonFill['lblSubsidiDealer2']}"});
         $('#lblInsentifSales').css({"color":"{$dsTJualDatKonFill['lblInsentifSales']}"});
         $('#lblPotonganKhusus').css({"color":"{$dsTJualDatKonFill['lblPotonganKhusus']}"});
         $('#lblBBNPlus').css({"color":"{$dsTJualDatKonFill['lblBBNPlus']}"});
         $('#lblUMInden').css({"color":"{$dsTJualDatKonFill['lblUMInden']}"});
         $('#lblUMDiterima').css({"color":"{$dsTJualDatKonFill['lblUMDiterima']}"});
         $('#lblLeasing').css({"color":"{$dsTJualDatKonFill['lblLeasing']}"});
         $('#lblBiayaBBN').css({"color":"{$dsTJualDatKonFill['lblBiayaBBN']}"});
     }
     
     setColor();
     
     $('[name=DKTOP]').utilNumberControl().cmp().attr('tabindex', '4');
     $('[name=DKPOTgl]').utilDateControl().cmp().attr('tabindex', '6');
     $('[name=DKTglTagih]').utilDateControl().cmp().attr('tabindex', '7');
     
     $('#btnImport').click(function (event) {	      
        window.util.app.dialogListData.show({
            title: 'Handle Leasing Requirement - LSNG - DK',
            url: '{$urlImport}'
        },
        function (data) {
			if(data.data === undefined) return;
            $.ajax({
				type: "POST",
				url: '$urlDetail',
				data: {
					oper: 'import',
					data: data,
					header: btoa($('#frm_ttdk_id').serialize())
				},
				success: function(data) {
					window.location.href = "{$url['update']}&oper=skip-load"; 
				},
			});
        });
    });   
     
     
JS
);




