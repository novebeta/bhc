<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttdk */
$jenis = 'Kredit';
$params                          = '&id=' . $id . '&action=create';
$cancel = \yii\helpers\Url::toRoute(['ttdk/cancel','id'=>$id,'action'=>'create','index'=>base64_encode('ttdk/data-konsumen-kredit')]);
$this->title                     = 'Tambah - Data Konsumen Kredit';
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Data Konsumen Kredit', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = $this->title;

//$cancel = \yii\helpers\Url::toRoute( [ 'ttdk/data-konsumen-kredit' ]);
//$this->title                     = 'Tambah ';
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Data Konsumen Kredit', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="ttdk-create">
	<?= $this->render( '_form', [
		'model'             => $model,
		'dsTJualDatKonFill' => $dsTJualDatKonFill,
        'jenis'             => $jenis,
		'url'    => [
			'update' => Custom::url( \Yii::$app->controller->id . '/data-konsumen-kredit-update' . $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
			'cancel' => $cancel,
		]
//		'url'               => [
//			'save'   => \yii\helpers\Url::toRoute( ['ttdk/data-konsumen-kredit-update','action'=>'create' ]),
//			'cancel' => $cancel,
//		]
	] ) ?>
</div>
