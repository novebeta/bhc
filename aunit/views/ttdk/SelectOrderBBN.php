<?php
use aunit\assets\AppAsset;
use aunit\models\Ttdk;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = 'Daftar Data Konsumen untuk Pengajuan Berkas';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'ttdk.DKNo'          => 'No Data Konsumen',
		'tdcustomer.CusNama' => 'Nama Konsumen',
		'ttdk.CusKode'       => 'Kode Konsumen',
		'ttdk.SalesKode'     => 'Kode Sales',
		'ttdk.LeaseKode'     => 'Kode Lease',
		'ttdk.DKMemo'        => 'Keterangan',
		'ttdk.DKJenis'       => 'Jenis',
		'ttdk.INNo'          => 'No Inden',
		'ttdk.NamaHadiah'    => 'Nama Hadiah',
		'ttdk.ProgramNama'   => 'Nama Program',
		'ttdk.DKLunas'       => 'Lunas',
		'ttdk.UserID'        => 'User',
	],
	'cmbTgl'    => [
		'ttdk.DKTgl' => 'Tgl Data Kons',
	],
	'cmbNum'    => [
		'ttdk.BBN'     => 'BBN',
		'ttdk.DKHarga' => 'Harga OTR',
	],
	'sortname'  => "DKTgl",
	'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttdk::className(),
	'Data Konsumen', [
		'mode'    => isset( $mode ) ? $mode : '',
		//		'url'     => \yii\helpers\Url::toRoute( [ 'ttdk/select'] ),
		'colGrid' => Ttdk::colGridSelectOrderBBN()
	] ), \yii\web\View::POS_READY );
