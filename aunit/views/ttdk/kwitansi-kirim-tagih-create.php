<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttdk */

$this->title = 'Tambah Kwitansi Kirim Tagih';
$this->params['breadcrumbs'][] = ['label' => 'Ttdks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttdk-create">
        <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
