<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttdk */

$this->title = str_replace('Menu','',\aunit\components\TUi::$actionMode).'Data Portal';
$this->params['breadcrumbs'][] = ['label' => 'Data Portal', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->DKNo, 'url' => ['view', 'id' => $model->DKNo]];
$this->params['breadcrumbs'][] = 'Edit';
$params = '&id='.$id;
?>
<div class="ttdk-update">
	<?= $this->render('DataPortal-form', [
		'model' => $model,
		'dsJual' => $dsJual,
		'url' => [
//			'save'    => Custom::url(\Yii::$app->controller->id.'/update'.$params ),
			'cancel'    => \yii\helpers\Url::toRoute(['ttdk/cancel','id'=>$id,'action'=>'update','index'=>base64_encode('ttdk/data-portal')])
		]
	]) ?>


</div>
