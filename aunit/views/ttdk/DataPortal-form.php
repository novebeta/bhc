<?php
use aunit\components\TUi;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttdk */
/* @var $form yii\widgets\ActiveForm */

?>
<style>
    .row{
        padding-bottom: 2px;
    }
</style>
<div class="ttdk-form" >
	<?php
	$form = ActiveForm::begin( [ 'id' => 'frm_ttdk_id', 'options' => [ 'method' => 'post' ,] , 'fieldConfig' => [ 'template' => "{input}" ] ] );
	TUi::form(
		[
			'class' => "row-no-gutters",
//			'style' => 'padding:0 10px',
			'items' => [
				[
					'class' => "col-md-12",
					'items' => [
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblCusKodeKons" ], [ 'class' => "col-sm-14", 'items' => ":txtCusKodeKons", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblDKJenis" ], [ 'class' => "col-sm-14", 'items' => ":txtDKJenis", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblLeaseKode" ], [ 'class' => "col-sm-14", 'items' => ":txtLeaseKode", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblDKDPTotal" ], [ 'class' => "col-sm-14", 'items' => ":txtDKDPTotal", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblDKAngsuran" ], [ 'class' => "col-sm-14", 'items' => ":txtDKAngsuran", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblDKTenor" ], [ 'class' => "col-sm-14", 'items' => ":txtDKTenor", ] ] ],
					],
				],
				[
					'class' => "col-md-12",
					'items' => [
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblLokasiKode" ], [ 'class' => "col-sm-14", 'items' => ":txtLokasiKode", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblSalesNama" ], [ 'class' => "col-sm-14", 'items' => ":txtSalesNama", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblSalesID" ], [ 'class' => "col-sm-14", 'items' => ":txtSalesID", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblDKPOTgl" ], [ 'class' => "col-sm-14", 'items' => ":txtDKPOTgl", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblDKPONo" ], [ 'class' => "col-sm-14", 'items' => ":txtDKPONo", ] ] ],
					],
				],
				[
					'class' => "col-md-24",
					'items' => "<hr/>"
				],
				[
					'class' => "col-md-12",
					'items' => [
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblCusPembeliNama" ], [ 'class' => "col-sm-14", 'items' => ":txtCusPembeliNama", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblCusKTP" ], [ 'class' => "col-sm-14", 'items' => ":txtCusKTP", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblCusNama" ], [ 'class' => "col-sm-14", 'items' => ":txtCusNama", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblCusSex" ], [ 'class' => "col-sm-14", 'items' => ":txtCusSex", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblCusTglLhr" ], [ 'class' => "col-sm-14", 'items' => ":txtCusTglLhr", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblCusAlamat" ], [ 'class' => "col-sm-14", 'items' => ":txtCusAlamat", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblCusRTRW" ], [ 'class' => "col-sm-6", 'items' => ":txtCusRT", ], [ 'class' => "col-sm-2", 'items' => ":sp", ] ,[ 'class' => "col-sm-6", 'items' => ":txtCusRW", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblCusKodePos" ], [ 'class' => "col-sm-14", 'items' => ":txtCusKodePos", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblCusKelurahan" ], [ 'class' => "col-sm-14", 'items' => ":txtCusKelurahan", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblCusKecamatan" ], [ 'class' => "col-sm-14", 'items' => ":txtCusKecamatan", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblCusKabupaten" ], [ 'class' => "col-sm-14", 'items' => ":txtCusKabupaten", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblCusProvinsi" ], [ 'class' => "col-sm-14", 'items' => ":txtCusProvinsi", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblCusTelepon" ], [ 'class' => "col-sm-14", 'items' => ":txtCusTelepon", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblNoKK" ], [ 'class' => "col-sm-14", 'items' => ":txtNoKK", ] ] ],
					],
				],
				[
					'class' => "col-md-12",
					'items' => [
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblCusStatusRumah" ], [ 'class' => "col-sm-14", 'items' => ":txtCusStatusRumah", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblSTNKNama" ], [ 'class' => "col-sm-14", 'items' => ":txtSTNKNama", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblCusPekerjaan" ], [ 'class' => "col-sm-14", 'items' => ":txtCusPekerjaan", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblCusAgama" ], [ 'class' => "col-sm-14", 'items' => ":txtCusAgama", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblCusPengeluaran" ], [ 'class' => "col-sm-14", 'items' => ":txtCusPengeluaran", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblCusPendidikan" ], [ 'class' => "col-sm-14", 'items' => ":txtCusPendidikan", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblDKMerkSblmnya" ], [ 'class' => "col-sm-14", 'items' => ":txtDKMerkSblmnya", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblDKJenisSblmnya" ], [ 'class' => "col-sm-14", 'items' => ":txtDKJenisSblmnya", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblDKMotorUntuk" ], [ 'class' => "col-sm-14", 'items' => ":txtDKMotorUntuk", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblDKMotorOleh" ], [ 'class' => "col-sm-14", 'items' => ":txtDKMotorOleh", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblMotorType" ], [ 'class' => "col-sm-14", 'items' => ":txtMotorType", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblMotorWarna" ], [ 'class' => "col-sm-14", 'items' => ":txtMotorWarna", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblMotorNoMesin" ], [ 'class' => "col-sm-14", 'items' => ":txtMotorNoMesin", ] ] ],
						[ 'class' => 'row', 'items' => [ [ 'class' => "col-sm-8", 'items' => ":lblMotorNoRangka" ], [ 'class' => "col-sm-14", 'items' => ":txtMotorNoRangka", ] ] ],
					],
				],
			]
		],
        [ 'class' => "row-no-gutters",
            'items' => [
                [
                    'class' => "col-md-12 pull-left",
                    'items' => $this->render( '../_nav', [
                        'url'=> $_GET['r'],
                        'options'=> [
                            'tdcustomer.CusNama'        => 'Nama Konsumen',
                            'tdcustomer.CusPembeliNama' => 'Nama Pemohon',
                            'ttdk.DKNo'                 => 'No Data Konsumen',
                            'tmotor.SKNo'               => 'No Surat Jln Kons',
                            'tmotor.MotorNoMesin'       => 'No Mesin',
                            'tmotor.MotorNoRangka'      => 'No Rangka',
                            'tmotor.MotorType'          => 'Type Motor',
                            'tmotor.MotorWarna'         => 'Warna Motor',
                            'tmotor.MotorTahun'         => 'Tahun Motor',
                            'ttdk.LeaseKode'            => 'Leasing',
                            'ttdk.TeamKode'             => 'Team',
                            'ttdk.SalesKode'            => 'Kode Sales',
                            'tdsales.SalesNama'         => 'Nama Sales',
                            'ttdk.INNo'                 => 'No Inden',
                            'ttdk.DKJenis'              => 'Jenis',
                            'ttdk.DKPONo'               => 'No PO',
                            'ttdk.SPKNo'                => 'No SPK',
                            'ttdk.CusKode'              => 'Kode Konsumen',
                            'tdcustomer.CusAlamat'      => 'Alamat Konsumen',
                            'tdcustomer.CusRT'          => 'RT Konsumen',
                            'tdcustomer.CusRW'          => 'RW Konsumen',
                            'tdcustomer.CusKelurahan'   => 'Kelurahan',
                            'tdcustomer.CusKecamatan'   => 'Kecamatan',
                            'tdcustomer.CusKabupaten'   => 'Kabupaten',
                            'tdcustomer.CusTelepon'     => 'Telepon',
                            'tdcustomer.CusTelepon2'    => 'Telepon 2',
                            'ttdk.DKMemo'               => 'Keterangan DK',
                            'tmotor.MotorMemo'          => 'Keterangan Motor',
                            'tmotor.STNKNo'             => 'No STNK',
                            'tmotor.PlatNo'             => 'No Plat',
                            'tmotor.BPKBNo'             => 'No BPKB',
                            'tmotor.FakturAHMNo'        => 'No Faktur AHM',
                            'tmotor.STNKNama'           => 'Nama STNK',
                            'tmotor.STNKAlamat'         => 'Alamat STNK',
                            'ttdk.UserID'               => 'User ID',
                            'ttdk.DKLunas'              => 'Status Lunas',
                            'ttdk.ProgramNama'          => 'Nama Program',
                            'ttdk.NamaHadiah'           => 'Nama Hadiah',
                            'tmotor.SSNo'               => 'No Surat Jln Supplier',
                            'tmotor.FBNo'               => 'No Faktur Beli',
                            'tmotor.PDNo'               => 'No Penerimaan Dealer',
                            'tmotor.SDNo'               => 'No Surat Jln Dealer',
                            'tmotor.NoticePenerima'     => 'Penerima Notice',
                            'tmotor.STNKPenerima'       => 'Penerima STNK',
                            'tmotor.PlatPenerima'       => 'Penerima Plat',
                            'tmotor.BPKBPenerima'       => 'Penerima BPKB',
                        ],
                    ])
                ],
		        ['class' => "pull-right", 'style' => 'padding:0 10px', 'items' => ":btnAction"],
            ]
        ],
		[
			":sp"    => '<label class="control-label" style="margin: 0; padding: 6px;text-align: center;width: 100%">/</label>',
			":lblCusKodeKons"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis Pembelian</label>',
			":lblDKJenisSblmnya" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Type Motor Sebelumnya</label>',
			":lblDKJenis"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Cara Pembelian</label>',
			":lblLeaseKode"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Finance Company</label>',
			":lblDKDPTotal"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jumlah DP</label>',
			":lblDKAngsuran"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Angsuran</label>',
			":lblDKTenor"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tenor</label>',
			":lblLokasiKode"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Dealer/POS</label>',
			":lblSalesNama"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Sales</label>',
			":lblSalesID"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">ID Sales</label>',
			":lblDKPOTgl"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl PO Fincoy</label>',
			":lblDKPONo"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">No PO Fincoy</label>',
			":lblCusPembeliNama" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Penjamin</label>',
			":lblCusKTP"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">No KTP</label>',
			":lblCusNama"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Pembeli</label>',
			":lblCusSex"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis Kelamin</label>',
			":lblCusTglLhr"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tanggal Lahir</label>',
			":lblCusAlamat"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
			":lblCusRTRW"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">RT / RW</label>',
			":lblCusKodePos"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode Pos</label>',
			":lblCusKelurahan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kelurahan</label>',
			":lblCusKecamatan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kecamatan</label>',
			":lblCusKabupaten"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kabupaten</label>',
			":lblCusProvinsi"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Propinsi</label>',
			":lblCusTelepon"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon</label>',
			":lblNoKK"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">No KK</label>',
			":lblCusStatusRumah" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status Rumah</label>',
			":lblSTNKNama"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama di STNK/BPKB</label>',
			":lblCusPekerjaan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pekerjaan</label>',
			":lblCusAgama"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Agama</label>',
			":lblCusPengeluaran" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pengeluaran Bulanan</label>',
			":lblCusPendidikan"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pendidikan Terakhir</label>',
			":lblDKMerkSblmnya"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Merk Motor Sebelumnya</label>',
			":lblDKMotorUntuk"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Penggunaan Spd Motor</label>',
			":lblDKMotorOleh"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pengguna Motor</label>',
			":lblMotorType"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Unit Type</label>',
			":lblMotorWarna"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Color</label>',
			":lblMotorNoMesin"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor Mesin</label>',
			":lblMotorNoRangka"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor Rangka</label>',
			":txtCusKodeKons"    => Html::textInput( 'CusKodeKons', $dsJual[ 'CusKodeKons' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtDKJenisSblmnya" => Html::textInput( 'DKJenisSblmnya', $dsJual[ 'DKJenisSblmnya' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtDKJenis"        => Html::textInput( 'DKJenis', $dsJual[ 'DKJenis' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtLeaseKode"      => Html::textInput( 'LeaseKode', $dsJual[ 'LeaseKode' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtDKDPTotal"      => Html::textInput( 'DKDPTotal', $dsJual[ 'DKDPTotal' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtDKAngsuran"     => Html::textInput( 'DKAngsuran', $dsJual[ 'DKAngsuran' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtDKTenor"        => Html::textInput( 'DKTenor', $dsJual[ 'DKTenor' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtLokasiKode"     => Html::textInput( 'LokasiKode', '', [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtSalesNama"      => Html::textInput( 'SalesNama', $dsJual[ 'SalesNama' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtSalesID"        => Html::textInput( 'SalesID', '', [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtDKPOTgl"        => Html::textInput( 'DKPOTgl', $dsJual[ 'DKPOTgl' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtDKPONo"         => Html::textInput( 'DKPONo', $dsJual[ 'DKPONo' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtCusPembeliNama" => Html::textInput( 'CusPembeliNama', $dsJual[ 'CusPembeliNama' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtCusKTP"         => Html::textInput( 'CusKTP', $dsJual[ 'CusKTP' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtCusNama"        => Html::textInput( 'CusNama', $dsJual[ 'CusNama' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtCusSex"         => Html::textInput( 'CusSex', $dsJual[ 'CusSex' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtCusTglLhr"      => Html::textInput( 'CusTglLhr', $dsJual[ 'CusTglLhr' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtCusAlamat"      => Html::textInput( 'CusAlamat', $dsJual[ 'CusAlamat' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtCusRT"          => Html::textInput( 'CusRT', $dsJual[ 'CusRT' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtCusRW"          => Html::textInput( 'CusRW', $dsJual[ 'CusRW' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtCusKodePos"     => Html::textInput( 'CusKodePos', $dsJual[ 'CusKodePos' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtCusKelurahan"   => Html::textInput( 'CusKelurahan', $dsJual[ 'CusKelurahan' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtCusKecamatan"   => Html::textInput( 'CusKecamatan', $dsJual[ 'CusKecamatan' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtCusKabupaten"   => Html::textInput( 'CusKabupaten', $dsJual[ 'CusKabupaten' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtCusProvinsi"    => Html::textInput( 'CusProvinsi', $dsJual[ 'CusProvinsi' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtCusTelepon"     => Html::textInput( 'CusTelepon', $dsJual[ 'CusTelepon' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtNoKK"           => Html::textInput( 'NoKK', '', [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtCusStatusRumah" => Html::textInput( 'CusStatusRumah', $dsJual[ 'CusStatusRumah' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtSTNKNama"       => Html::textInput( 'STNKNama', '', [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtCusPekerjaan"   => Html::textInput( 'CusPekerjaan', $dsJual[ 'CusPekerjaan' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtCusAgama"       => Html::textInput( 'CusAgama', $dsJual[ 'CusAgama' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtCusPengeluaran" => Html::textInput( 'CusPengeluaran', $dsJual[ 'CusPengeluaran' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtCusPendidikan"  => Html::textInput( 'CusPendidikan', $dsJual[ 'CusPendidikan' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtDKMerkSblmnya"  => Html::textInput( 'DKMerkSblmnya', $dsJual[ 'DKMerkSblmnya' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtDKMotorUntuk"   => Html::textInput( 'DKMotorUntuk', $dsJual[ 'DKMotorUntuk' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtDKMotorOleh"    => Html::textInput( 'DKMotorOleh', $dsJual[ 'DKMotorOleh' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtMotorType"      => Html::textInput( 'MotorType', $dsJual[ 'MotorType' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtMotorWarna"     => Html::textInput( 'MotorWarna', $dsJual[ 'MotorWarna' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtMotorNoMesin"   => Html::textInput( 'MotorNoMesin', $dsJual[ 'MotorNoMesin' ], [ 'class' => 'form-control', 'readonly' => true ] ),
			":txtMotorNoRangka"  => Html::textInput( 'MotorNoRangka', $dsJual[ 'MotorNoRangka' ], [ 'class' => 'form-control', 'readonly' => true ] ),

//            ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
            ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
//            ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
            ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
            ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
//            ":btnImport"      => Html::button('<i class="fa fa-arrow-circle-down"><br><br>Import</i>   ', ['class' => 'btn bg-navy ', 'id' => 'btnImport', 'style' => 'width:60px;height:60px']),
//            ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
//            ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),

            ":btnSave"              => "",
//            ":btnCancel"            => Html::Button( '<i class="fa fa-ban"></i> Batal', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel','style' => 'width:80px' ] ),
            ":btnAdd"               => "",
//            ":btnEdit"              => Html::button( '<i class="fa fa-edit"></i> Edit', [ 'class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:80px' ] ),
//            ":btnDaftar"            => Html::Button( '<i class="fa fa-list"></i> Daftar ', [ 'class' => 'btn btn-primary ', 'id' => 'btnDaftar' ,'style' => 'width:80px'] ),
		]
	);
	ActiveForm::end();

    $urlIndex   = Url::toRoute( [ 'ttdk/data-portal'] );
	$this->registerJs( <<< JS
     $('#btnCancel').click(function (event) {	      
       $('#frm_ttdk_id').attr('action','{$url['cancel']}');
       $('#frm_ttdk_id').submit();
    });

    $('#btnDaftar').click(function (event) {	      
            window.location.href = '{$urlIndex}';
          }); 
    
    
     
JS);

	?>
</div>


