<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttdk */
$params                          = '&id=' . $id . '&action=create';
$cancel = \yii\helpers\Url::toRoute(['ttdk/cancel','id'=>$id,'action'=>'create','index'=> base64_encode('ttdk/data-konsumen-tunai')]);
$this->title                     = 'Tambah Data Konsumen Tunai';
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Data Konsumen Tunai', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="ttdk-create">
	<?= $this->render( '_form', [
		'model'             => $model,
		'dsTJualDatKonFill' => $dsTJualDatKonFill,
		'jenis'             => 'Tunai',
		'url'               => [
			'update'   => Custom::url( 'ttdk/data-konsumen-tunai-update'. $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
			'cancel' => $cancel,
		]
	] ) ?>
</div>
