<?php
/* @var $this yii\web\View */

/* @var $dataProvider yii\data\ActiveDataProvider */

use aunit\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
$this->title = 'Pesan';
$this->params['breadcrumbs'][] = $this->title;

$arr = [
    'cmbTxt' => [
    ],
    'cmbTgl' => [
    ],
    'cmbNum' => [
    ],
    'sortname' => "NoTrans",
    'sortorder' => 'desc'
];
$this->registerJsVar('setcmb', $arr);
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php

$this->registerJs(<<< JS
    window.gridParam_jqGrid = {
        readOnly: true,
        rownumbers: true,
        rowNum: 10000,
        pgbuttons: false,
        pginput: false,
        viewrecords: false,
        rowList: [],
        pgtext: ""
    }
    $('#filter-tgl-val1').change(function() {
        let _tgl2 = new Date($(this).val());      
        _tgl2.setDate(_tgl2.getDate() + 7);
        $('[name=tgl2]').val(_tgl2.toISOString().slice(0, 10));
	    $('#filter-tgl-val2-disp').datepicker('update', _tgl2);
	    $('#filter-tgl-val2-disp').trigger('change');
    });
JS
);
$this->registerJs(\aunit\components\TdUi::mainGridJs(\aunit\models\Ttdk::class,
    'Daftar Transaksi Keuangan yang Melibatkan DK Nomor : ', [
        'colGrid' => $colGrid,
        'mode' => isset($mode) ? $mode : '',
    ]), \yii\web\View::POS_READY);