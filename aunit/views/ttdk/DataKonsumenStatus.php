<?php
use aunit\assets\AppAsset;
use aunit\models\Ttdk;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = 'Data Konsumen Status';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'CusNama'        => 'Nama Konsumen',
		'DKNo'           => 'No Data Konsumen',
		'SKNo'           => 'No Surat Jln Kons',
		'MotorNoMesin'   => 'No Mesin',
		'MotorNoRangka'  => 'No Rangka',
		'MotorType'      => 'Type Motor',
		'MotorWarna'     => 'Warna Motor',
		'MotorTahun'     => 'Tahun Motor',
		'LeaseKode'      => 'Leasing',
		'TeamKode'       => 'Team',
		'SalesKode'      => 'Kode Sales',
		'SalesNama'      => 'Nama Sales',
		'INNo'           => 'No Inden',
		'DKJenis'        => 'Jenis',
		'DKPONo'         => 'No PO',
		'SPKNo'          => 'No SPK',
		'CusKode'        => 'Kode Konsumen',
		'CusAlamat'      => 'Alamat Konsumen',
		'CusRT'          => 'RT Konsumen',
		'CusRW'          => 'RW Konsumen',
		'CusKelurahan'   => 'Kelurahan',
		'CusKecamatan'   => 'Kecamatan',
		'CusKabupaten'   => 'Kabupaten',
		'CusTelepon'     => 'Telepon',
		'CusTelepon2'    => 'Telepon 2',
		'DKMemo'         => 'Keterangan DK',
		'MotorMemo'      => 'Keterangan Motor',
		'NoticeNo'       => 'No Notice',
		'STNKNo'         => 'No STNK',
		'PlatNo'         => 'No Plat',
		'BPKBNo'         => 'No BPKB',
		'FakturAHMNo'    => 'No Faktur AHM',
		'NoticePenerima' => 'Penerima Notice',
		'STNKPenerima'   => 'Penerima STNK',
		'PlatPenerima'   => 'Penerima Plat',
		'BPKBPenerima'   => 'Penerima BPKB',
		'STNKNama'       => 'Nama STNK',
		'STNKAlamat'     => 'Alamat STNK',
		'UserID'         => 'User ID',
		'DKLunas'        => 'Status Lunas',
		'ProgramNama'    => 'Nama Program',
		'NamaHadiah'     => 'Nama Hadiah',
		'SSNo'           => 'No Surat Jln Supplier',
		'FBNo'           => 'No Faktur Beli',
		'PDNo'           => 'No Penerimaan Dealer',
		'SDNo'           => 'No Surat Jln Dealer',
	],
	'cmbTgl'    => [
		'DKTgl' => 'Tanggal DK',
	],
	'cmbNum'    => [
		'DKHarga'         => 'Harga OTR',
		'DKHPP'           => 'HPP',
		'DKDPTotal'       => 'DP Total',
		'DKDPLLeasing'    => 'DP Leasing',
		'DKDPTerima'      => 'DP Terima',
		'DKNetto'         => 'Netto',
		'ProgramSubsidi'  => 'Subsidi Program',
		'PrgSubsSupplier' => 'Subsidi Supplier',
		'PrgSubsDealer'   => 'Subsidi Dealer',
		'PrgSubsFincoy'   => 'Subsidi Fincoy',
		'PotonganHarga'   => 'Potongan Harga',
		'ReturHarga'      => 'Retur Harga',
		'PotonganKhusus'  => 'Potongan Khusus',
		'BBN'             => 'BBN',
		'Jaket'           => 'Jaket',
	],
	'sortname'  => "DKTgl",
	'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttdk::className(),
	'Data Konsumen Status', [
		'colGrid'            => Ttdk::colGridDataKonsumenStatus(),
		'btnAdd_Disabled'    => true,
		'url_update'         => Url::toRoute( [ 'ttdk/data-konsumen-status-update', 'action' => 'update' ] ),
		'url_view'           => Url::toRoute( [ 'ttdk/data-konsumen-status-update', 'action' => 'view' ] ),
		'url_delete'         => Url::toRoute( [ 'ttdk/delete' ] ),
		'btnDelete_Disabled' => true,
	] ), \yii\web\View::POS_READY );
