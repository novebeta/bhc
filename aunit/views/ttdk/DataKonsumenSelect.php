<?php
use aunit\assets\AppAsset;
use aunit\models\Ttdk;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = 'Data Konsumen';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'CusNama'        => 'Nama Konsumen',
		'CusPembeliNama' => 'Nama Pemohon',
		'DKNo'                 => 'No Data Konsumen',
		'SKNo'               => 'No Surat Jln Kons',
		'MotorNoMesin'       => 'No Mesin',
		'MotorNoRangka'      => 'No Rangka',
		'MotorType'          => 'Type Motor',
		'MotorWarna'         => 'Warna Motor',
		'MotorTahun'         => 'Tahun Motor',
		'LeaseKode'            => 'Leasing',
		'TeamKode'             => 'Team',
		'SalesKode'            => 'Kode Sales',
		'tdsales.SalesNama'         => 'Nama Sales',
		'INNo'                 => 'No Inden',
		'DKJenis'              => 'Jenis',
		'DKPONo'               => 'No PO',
		'SPKNo'                => 'No SPK',
		'CusKode'              => 'Kode Konsumen',
		'CusAlamat'      => 'Alamat Konsumen',
		'CusRT'          => 'RT Konsumen',
		'CusRW'          => 'RW Konsumen',
		'CusKelurahan'   => 'Kelurahan',
		'CusKecamatan'   => 'Kecamatan',
		'CusKabupaten'   => 'Kabupaten',
		'CusTelepon'     => 'Telepon',
		'CusTelepon2'    => 'Telepon 2',
		'DKMemo'               => 'Keterangan DK',
		'MotorMemo'          => 'Keterangan Motor',
		'STNKNo'             => 'No STNK',
		'PlatNo'             => 'No Plat',
		'BPKBNo'             => 'No BPKB',
		'FakturAHMNo'        => 'No Faktur AHM',
		'STNKNama'           => 'Nama STNK',
		'STNKAlamat'         => 'Alamat STNK',
		'UserID'               => 'User ID',
		'DKLunas'              => 'Status Lunas',
		'ProgramNama'          => 'Nama Program',
		'NamaHadiah'           => 'Nama Hadiah',
		'SSNo'               => 'No Surat Jln Supplier',
		'FBNo'               => 'No Faktur Beli',
		'PDNo'               => 'No Penerimaan Dealer',
		'SDNo'               => 'No Surat Jln Dealer',
		'NoticePenerima'     => 'Penerima Notice',
		'STNKPenerima'       => 'Penerima STNK',
		'PlatPenerima'       => 'Penerima Plat',
		'BPKBPenerima'       => 'Penerima BPKB',
	],
	'cmbTgl'    => [
		'DKTgl' => 'Tanggal DK',
	],
	'cmbNum'    => [
		'DKHarga'         => 'Harga OTR',
		'DKHPP'           => 'HPP',
		'DKDPTotal'       => 'DP Total',
		'DKDPLLeasing'    => 'DP Leasing',
		'DKDPTerima'      => 'DP Terima',
		'DKNetto'         => 'Netto',
		'ProgramSubsidi'  => 'Subsidi Program',
		'PrgSubsSupplier' => 'Subsidi Supplier',
		'PrgSubsDealer'   => 'Subsidi Dealer',
		'PrgSubsFincoy'   => 'Subsidi Fincoy',
		'PotonganHarga'   => 'Potongan Harga',
		'ReturHarga'      => 'Retur Harga',
		'PotonganKhusus'  => 'Potongan Khusus',
		'BBN'             => 'BBN',
		'Jaket'           => 'Jaket',
	],
	'sortname'  => "DKTgl",
	'sortorder' => 'desc'
];

if(in_array($_GET['r'],['ttdk/select-pengajuan-bayar-bbn-bank','ttdk/select-pengajuan-bayar-bbn-kas'])){
	$arr['cmbTxt']['ABNo'] = 'ABNo';
}
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <style>
        .minus{
            color: red;
        }
    </style>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php

if(isset($_GET['jenis']) && $_GET['jenis'] === 'PenjualanMulti') {
	$this->registerJs( <<<JS

    window.gridParam_jqGrid = {        
        gridview: true,
        rowattr: function (rd) {
             if (rd.Jenis ==='Retur Harga' || rd.Jenis ==='SubsidiDealer2' || rd.Jenis ==='Insentif Sales') {
                return {"class": "minus"};
            }
        },
    };

JS
	);
}

switch ($_GET['r']){
	case 'ttdk/select-bm-multi':
	case 'ttdk/select-bm-order-leasing':
	case 'ttdk/select-bk-order-penjualan':
	case 'ttdk/select-kk-bayar-bbn-bank':
	case 'ttdk/select-kk-bayar-bbn-plus':
	case 'ttdk/select-kk-bayar-bbn-acc':
	case 'ttdk/select-bayar-bbn-bank':
	case 'ttdk/select-pengajuan-bayar-bbn-kas':
	case 'ttdk/select-pengajuan-bayar-bbn-bank':
		$this->registerJs(<<<JS

        window.gridParam_jqGrid = {        
            cellEdit : true,                
            cellsubmit:'clientArray',
			onSelectAll: function(rowids, status){
				for(rowid of rowids){
					selectRow($('#jqGrid'),rowid,status);
				}
			},
        };

JS);
		break;
}

if(in_array($_GET['r'],['ttdk/select-pengajuan-bayar-bbn-bank','ttdk/select-pengajuan-bayar-bbn-kas'])){
	$colBayar = 'BKBayar';
	if($_GET['r'] == 'ttdk/select-pengajuan-bayar-bbn-kas'){
		$colBayar = 'KKBayar';
	}
	$this->registerJs(<<<JS

		window.selectRow = function(grid,rowid, status){
			var rowData = grid.jqGrid('getRowData', rowid);
			var Bayar = parseFloat(rowData["{$colBayar}"]);
			var BBN = parseFloat(rowData.BBN);
			var Terbayar = parseFloat(rowData.Terbayar);
			if(Bayar == 0){
				if(status){
					var Bayar = BBN - Terbayar;
					grid.jqGrid("setCell", rowid, "{$colBayar}", Bayar);
					grid.jqGrid("setCell", rowid, "Sisa", 0);
				}
			}
			if(!status){
				Bayar = 0;
				var Sisa = BBN - Terbayar - Bayar;
				grid.jqGrid("setCell", rowid, "{$colBayar}", 0);
				grid.jqGrid("setCell", rowid, "Sisa", Sisa);
			}
		};

JS);
}

$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttdk::class,
	'Data Konsumen', [
		'mode'    => isset( $mode ) ? $mode : '',
		'colGrid' => isset( $colGrid ) ? $colGrid : Ttdk::colGridDataKonsumenTunai()
	] ), \yii\web\View::POS_READY );

switch ($_GET['r']){
    case 'ttdk/select-bm-multi':
    case 'ttdk/select-bm-order-leasing':
	    $this->registerJs(<<<JS
        var grid = $('#jqGrid');
        grid.bind("jqGridAfterSaveCell", function (e, rowid, orgClickEvent) {
            var rowData = grid.jqGrid('getRowData', rowid);
            var NilaiAwal = parseFloat(rowData.NilaiAwal);
            var Terbayar = parseFloat(rowData.Terbayar);
            var Bayar = parseFloat(rowData.BMBayar);
            var Sisa = NilaiAwal - Terbayar - Bayar;
            if (Sisa < 0) return;
            grid.jqGrid("setCell", rowid, "Sisa", Sisa);
            grid.jqGrid('setSelection', rowid, true);
        });   
JS);
	    break;
    case 'ttdk/select-bk-order-penjualan':
	    $this->registerJs(<<<JS
        var grid = $('#jqGrid');
        grid.bind("jqGridAfterSaveCell", function (e, rowid, orgClickEvent) {
            var rowData = grid.jqGrid('getRowData', rowid);
            var NilaiAwal = parseFloat(rowData.Awal);
            var Terbayar = parseFloat(rowData.Terbayar);
            var Bayar = parseFloat(rowData.BKBayar);
            var Sisa = NilaiAwal - Terbayar - Bayar;
            if (Sisa < 0) return;
            grid.jqGrid("setCell", rowid, "Sisa", Sisa);
            grid.jqGrid('setSelection', rowid, true);
        });   
JS);
        break;
    case 'ttdk/select-kk-bayar-bbn-acc':
    case 'ttdk/select-kk-bayar-bbn-plus':
    case 'ttdk/select-kk-bayar-bbn-bank':
    case 'ttdk/select-pengajuan-bayar-bbn-kas':
	    $this->registerJs(<<<JS
        var grid = $('#jqGrid');
        grid.bind("jqGridAfterSaveCell", function (e, rowid, orgClickEvent) {
            var rowData = grid.jqGrid('getRowData', rowid);
            var BBN = parseFloat(rowData.BBN);
            var Terbayar = parseFloat(rowData.Terbayar);
            var Bayar = parseFloat(rowData.KKBayar);
            var Sisa = BBN - Terbayar - Bayar;
            if (Sisa < 0) return;
            grid.jqGrid("setCell", rowid, "Sisa", Sisa);
            grid.jqGrid('setSelection', rowid, true);
        });
		grid.bind("jqGridCellSelect", function(e, rowid, orgClickEvent){
			grid.jqGrid('setSelection', rowid, true);
		});
JS);
        break;

	case 'ttdk/select-bayar-bbn-plus':
	case 'ttdk/select-bayar-bbn-bank':
    case 'ttdk/select-pengajuan-bayar-bbn-bank':
	    $this->registerJs(<<<JS
        var grid = $('#jqGrid');
        grid.bind("jqGridAfterSaveCell", function (e, rowid, orgClickEvent) {
            var rowData = grid.jqGrid('getRowData', rowid);
            var BBN = parseFloat(rowData.BBN);
            var Terbayar = parseFloat(rowData.Terbayar);
            var Bayar = parseFloat(rowData.BKBayar);
            var Sisa = BBN - Terbayar - Bayar;
            if (Sisa < 0) return;
            grid.jqGrid("setCell", rowid, "Sisa", Sisa);
            grid.jqGrid('setSelection', rowid, true);
        });
		grid.bind("jqGridCellSelect", function(e, rowid, orgClickEvent){
			grid.jqGrid('setSelection', rowid, true);
		});
JS);
        break;
}

if(in_array($_GET['r'],['ttdk/select-pengajuan-bayar-bbn-bank','ttdk/select-pengajuan-bayar-bbn-kas'])){
	$this->registerJs(<<<JS
		var grid = $('#jqGrid');
		grid.bind("jqGridSelectRow", function(e, rowid, status){
			selectRow(grid,rowid,status);
		});
JS);
}