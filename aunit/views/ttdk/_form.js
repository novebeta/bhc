jQuery(function ($) {
    function Hitung() {
        $('input[name="ProgramSubsidi"]').utilNumberControl().val(parseFloat($('input[name="PrgSubsSupplier"]').val()) + parseFloat($('input[name="PrgSubsDealer"]').val()) + parseFloat($('input[name="PrgSubsFincoy"]').val()));
        $('input[name="DKDPTotal"]').utilNumberControl().val(parseFloat($('input[name="DKDPLLeasing"]').val()) + parseFloat($('input[name="DKDPInden"]').val()) + parseFloat($('input[name="DKDPTerima"]').val()));
        $('input[name="DKNetto"]').utilNumberControl().val(parseFloat($('input[name="DKHarga"]').val()) - parseFloat($('input[name="ProgramSubsidi"]').val()) - parseFloat($('input[name="DKDPTotal"]').val()));
        $('input[name="NDPPO"]').utilNumberControl().val(parseFloat($('input[name="DKDPTotal"]').val()) + parseFloat($('input[name="ProgramSubsidi"]').val()));
    }

    window.Hitung = Hitung;
    $('#btnSave').click(function (event) {
        let SalesKode = $('input[name="SalesKode"]').val();
        if (SalesKode === "") {
            bootbox.alert({message: "Anda belum memasukkan data sales.", size: 'small'});
            return '';
        }
        let CusKTP = $('input[name="CusKTP"]').val();
        let CusNama = $('input[name="CusNama"]').val();
        let CusAlamat = $('input[name="CusAlamat"]').val();
        if (CusKTP === "" || CusNama === "" || CusNama === "--" || CusAlamat === "") {
            bootbox.alert({message: "Anda belum memasukkan data konsumen dengan lengkap.", size: 'small'});
            return '';
        }
        let CusTelepon2 = $('input[name="CusTelepon2"]').val();
        if (CusTelepon2 === "") {
            bootbox.alert({message: "Anda belum memasukkan data Telepon 2.", size: 'small'});
            return '';
        }
        let CusKabupaten = $('input[name="CusKabupaten"]').val();
        let CusKelurahan = $('input[name="CusKelurahan"]').val();
        let CusKecamatan = $('input[name="CusKecamatan"]').val();
        let CusKodePos = $('input[name="CusKodePos"]').val();
        if (CusKabupaten === "" || CusKelurahan === "" || CusKecamatan === "" || CusKodePos === "") {
            bootbox.alert({message: "Anda belum memasukkan data konsumen dengan lengkap.", size: 'small'});
            return '';
        }
        let MotorType = $('#MotorType_id').val();
        let MotorNoMesin = $('#MotorNoMesin_id').val();
        let MotorNoRangka = $('#MotorNoRangka_id').val();
        if (MotorType == null || MotorNoMesin == null || MotorNoRangka == null) {
            bootbox.alert({message: "Motor Tidak Tersedia", size: 'small'});
            return '';
        }
        let SPKNo = $('[name=SPKNo]').val();
        if (SPKNo == null || SPKNo == '--' || SPKNo == '') {
            bootbox.alert({message: "SPKNo harus diisi", size: 'small'});
            return '';
        }
        let BBN = $('input[name="BBN"]').val();
        if (BBN <= 1) {
            if (BKodeAkses == 'Admin' || BKodeAkses == 'Auditor') {
                bootbox.confirm("Motor ini tidak memiliki Biaya BBN, apakah DK ini merupakan penjualan Off The Road ?", function (result) {
                    if (result) {
                        $('input[name="BBN"]').val(0);
                        Hitung();
                        if ($('input[name="DKNetto"]').val() < 0) {
                            bootbox.alert("DKNetto minus.");
                            return;
                        }
                        $('#frm_ttdk_id').attr('action', '<?=$urlUpdate;?>');
                        $('#frm_ttdk_id').submit();
                    }
                });
            } else {
                bootbox.alert({message: "Harap membuat memo persetujuan Korwil", size: 'small'});
            }
        } else {
            Hitung();
            if ($('input[name="DKNetto"]').val() < 0) {
                bootbox.alert({message: "DKNetto minus.", size: 'small'});
                return;
            }
            $('#frm_ttdk_id').attr('action', '<?=$urlUpdate;?>');
            $('#frm_ttdk_id').submit();
        }
    });
    $('.hitung').blur(function () {
        // console.log('blur');
        Hitung();
    });
    $("#NewCusId").click(function () {
        bootbox.confirm("Apakah Anda ingin memasukkan Data Konsumen Baru ?", function (result) {
            if (result) {
                $('input[name="CusKode"]').val('--');
                $('input[name="CusNama"]').val('');
                $('input[name="CusAlamat"]').val('');
            }
        });
    });
    Hitung();
});