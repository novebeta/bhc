<?php

use yii\helpers\Html;
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttdk */

$params                          = '&id=' . $id . '&action=update';
$cancel = \yii\helpers\Url::toRoute(['ttdk/cancel','id'=>$id,'action'=>'update','index'=>base64_encode('ttdk/stnk-bpkb')]);
$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode).'- STNK - BPKB  : ' . $model->DKNo;
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'STNK - BPKB ', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = 'Edit';

?>
<div class="ttdk-create">
        <?= $this->render('stnk-bpkb-form', [
            'model' => $model,
            'dsJual' => $dsJual,
            'url' => [
                'update'    => Custom::url(\Yii::$app->controller->id.'/stnk-bpkb-update'.$params ),
                'print'     => Custom::url(\Yii::$app->controller->id.'/print'.$params ),
                'cancel'    => $cancel,
            ]
    ]) ?>

</div>
