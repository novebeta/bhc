<?php

use common\components\Custom;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttamhd */

$this->title = str_replace('Menu','',\aunit\components\TUi::$actionMode).'- Penyesuaian Nilai Persediaan Motor: ' .$AMNoView;
//$this->params['breadcrumbs'][] = ['label' => 'Penyesuaian Nilai Persediaan Motor', 'url' => ['index']];
//$this->params['breadcrumbs'][] = 'Edit';

$params = '&id='.$id.'&action=update';
?>
<div class="ttamhd-update">

    <?= $this->render('_form', [
        'model' => $model,
        'AMNoView' => $AMNoView,
        'url' => [
            'update'    => Custom::url(\Yii::$app->controller->id.'/update'.$params ),
            'print'     => Custom::url(\Yii::$app->controller->id.'/print'.$params ),
            'cancel'    => Custom::url(\Yii::$app->controller->id.'/cancel'.$params ),
            'detail'    => Custom::url(\Yii::$app->controller->id.'/detail' ),

            'tdmotorwarna' => Custom::url('tdmotorwarna/find' ),
        ]
    ]) ?>

</div>
