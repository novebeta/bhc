<?php
use aunit\assets\AppAsset;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Penyesuaian Nilai Persediaan Motor';
$this->params['breadcrumbs'][] = $this->title;
$arr = [
	'cmbTxt'=> [
		'AMNo'          => 'No AM',
		'NoGL'          => 'No GL',
		'FBNo'          => 'No FB',
		'AMMemo'        => 'Keterangan',
		'UserID'        => 'User',
	],
	'cmbTgl'=> [
		'AMTgl'         => 'Tanggal AM',
	],
	'cmbNum'=> [
		'AMTotal'       => 'Total',
		'MotorTahun'    => 'Motor Tahun',
	],
	'sortname'  => "AMTgl",
	'sortorder' => 'desc',
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttamhd::className(),
	'Penyesuaian Nilai Persediaan' ), \yii\web\View::POS_READY );
