<?php
use common\components\Custom;
use kartik\datecontrol\DateControl;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttamhd */
/* @var $form yii\widgets\ActiveForm */
\aunit\assets\AppAsset::register( $this );
$format      = <<< SCRIPT
function formatttsd(result) {
return '<div class="row" style="margin-left: 2px">' +
    ' ' + result.id + '&emsp;' +
    ' ' + result.text + ' ' +
    '</div>';
}
SCRIPT;
$escape      = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttamhd-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'frm_ttamhd_id', 'action' => $url[ 'update' ] ] );
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[
						'class' => "form-inline col-md-24",
						'items' => [
							[
								'style' => "margin-bottom: 3px;",
								'items' => ":AMNo :AMTgl :AMJamView :MotorTahun_ :MotorTahun2"
							],
							[
								'style' => "margin-bottom: 6px;",
								'items' => ":FBNo"
							]
						]
					],
					[
						'class' => "row col-md-24",
						'style' => "margin-bottom: 6px;",
						'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
					],
					[
						'class' => "form-inline col-md-24",
						'items' => ":AMMemo :JumlahUnit :AMTotal"
					]
				]
			],
//			[
//				'class' => "pull-right",
//				'items' => ":btnJurnal :btnPrint :btnAction"
//			],
            [ 'class' => "row-no-gutters",
                'items' => [
                    ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                        'items'  => [
                            ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":NoGL"],
                            ['class' => "col-sm-4 pull-right", 'items' => ":LabelNoGL"],
                        ],
                    ],
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'AMNo'          => 'No AM',
                                'NoGL'          => 'No GL',
                                'FBNo'          => 'No FB',
                                'AMMemo'        => 'Keterangan',
                                'UserID'        => 'User',
                            ],
                        ])
                    ],
                    ['class' => "col-md-12 pull-right",
                        'items' => [
                            [ 'class' => "pull-right", 'items' => ":btnPrint :btnAction" ]
                        ]
                    ]
                ]
            ],
			[
				":AMNo"        => $form->field( $model, 'AMNo', [ 'template' => '{label} {input}' . Html::textInput( 'AMNo', $AMNoView, [ 'class' => 'form-control', 'readOnly' => true, 'tabindex' => '1' ] ) ] )
				                       ->textInput( [ 'maxlength' => true, 'placeholder' => 'AMNo', 'style' => 'width: 230px;', 'readonly' => true, 'class' => 'hidden' ] )
				                       ->label( 'No AM', [ 'style' => 'margin: 0; padding: 6px 0; width: 60px;' ] ),
				":AMTgl"       => $form->field( $model, 'AMTgl', [ 'template' => '{label}{input}' ] )
				                       ->widget( DateControl::className(), [
					                       'type'          => DateControl::FORMAT_DATE,
					                       'pluginOptions' => [ 'autoclose' => true ]
				                       ] )->label( 'Tanggal', [ 'style' => 'margin: 0; padding: 6px 0; width: 60px;' ] ),
				":AMJamView"   => '<div class="form-group">
                                    <label class="control-label" style="margin: 0; padding: 0; width: 0px;" for="AMJamView"></label>
                                    <input type="text" id="AMJamView" class="form-control" name="Ttbmhd[AMJamView]" value="' . ( DateTime::createFromFormat( 'Y-m-d H:i:s', $model->AMJam ) )->format( 'H:i' ) . '" placeholder="AMJamView" style="width: 60px;" readonly="readonly">
                                    </div>',
				":FBNo"        => $form->field( $model, 'FBNo', [ 'template' => '{label}{input}' ] )
				                       ->textInput( [ 'maxlength' => true, 'placeholder' => 'FBNo', 'style' => 'width: 230px;', 'tabindex' => '2' ] )
				                       ->label( 'No FB', [ 'style' => 'margin: 0; padding: 6px 0; width: 60px;' ] ),
				":NoGL"        => $form->field( $model, 'NoGL', [ 'template' => '{input}' ] )
				                       ->textInput( [ 'maxlength' => true, 'style' => 'width: 236px;', 'readonly' => '' ] ),
				":MotorTahun_" => $form->field( $model, 'MotorTahun', [ 'template' => '{label}{input}' ] )
				                       ->textInput( [ 'type' => 'number', 'maxlength' => true, 'style' => 'width: 70px;' ] )
				                       ->label( 'Tahun Motor', [ 'style' => 'margin: 0; padding: 6px 0; width: 80px;' ] ),
				":MotorTahun2" => '<div class="form-group field-ttamhd-motortahun2">
<label class="control-label" style="margin: 0; padding: 6px 0; width: 0;" for="ttamhd-motortahun2"></label><input type="number" id="ttamhd-motortahun2" class="form-control" name="Ttamhd[MotorTahun2]" style="width: 70px;" aria-invalid="false" value="' . $model->MotorTahun . '">
</div>',
				":AMMemo"      => $form->field( $model, 'AMMemo', [ 'template' => '{label}{input}' ] )
				                       ->textInput( [ 'maxlength' => true, 'style' => 'width: 450px;', 'tabindex' => '3' ] )
				                       ->label( 'Keterangan', [ 'style' => 'margin: 0; padding: 6px 0; width: 80px;' ] ),
				":JumlahUnit"  => '<div class="form-group">
<label class="control-label" for="selisih" style="margin: 0; padding: 6px 0; width: 80px;">Jumlah Unit</label>' .
				                  \kartik\number\NumberControl::widget( [
					                  'id'                 => 'JumlahUnit',
					                  'name'               => 'JumlahUnit',
					                  'value'              => 0,
					                  'readonly'           => true,
					                  'maskedInputOptions' => [
						                  'groupSeparator' => '.',
						                  'radixPoint'     => ','
					                  ]
				                  ] )
				                  . '</div>',
				":AMTotal"     => $form->field( $model, 'AMTotal', [ 'template' => '{label}{input}' ] )
				                       ->widget( \kartik\number\NumberControl::className(), [
					                       'value'              => 0,
					                       'readonly'           => true,
					                       'maskedInputOptions' => [
						                       'groupSeparator' => '.',
						                       'radixPoint'     => ','
					                       ],
					                       'displayOptions'     => [ 'class' => 'form-control' ]
				                       ] )
				                       ->label( 'Total', [ 'style' => 'margin: 0; padding: 6px 0 6px 2px; width: 40px;' ] ),
                ":btnSave"          => Html::button( '<i class="glyphicon glyphicon-floppy-disk"><br><br> Simpan</i>', [ 'class' => 'btn btn-info btn-primary ','style' => 'width:60px;height:60px', 'id' => 'btnSave'. Custom::viewClass() ] ),
                ":btnCancel"        => Html::Button( '<i class="fa fa-ban"><br><br> Batal</i>', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel','style' => 'width:60px;height:60px' ] ),
                ":btnAdd"          => Html::button( '<i class="fa fa-plus-circle"><br><br> Tambah</i>', [ 'class' => 'btn btn-light btn-success ', 'id' => 'btnAdd' ,'style' => 'width:60px;height:60px' ] ),
                ":btnEdit"         => Html::button( '<i class="fa fa-edit"><br><br>Edit</i>', [ 'class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px' ] ),
                ":btnDaftar"       => Html::Button( '<i class="fa fa-list"><br><br> Daftar </i>', [ 'class' => 'btn btn-primary ', 'id' => 'btnDaftar' ,'style' => 'width:60px;height:60px'] ),
                ":btnPrint"        => Html::button( '<i class="glyphicon glyphicon-print"><br><br>  Print </i>', [ 'class' => 'btn btn-warning ', 'id' => 'btnPrint','style' => 'width:60px;height:60px'  ] ),
                ":btnJurnal"       => Html::button( '<i class="fa fa-balance-scale"><br><br>  Jurnal </i>', [ 'class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal','style' => 'width:60px;height:60px' ] ),
                ":LabelNoGL"           => \common\components\General::labelGL( $model[ 'NoGL' ] ),
			],
            [
                '_akses' => 'Penyesuaian Nilai Persediaan Motor',
                'url_main' => 'ttamhd',
                'url_id' => $_GET['id'] ?? '',
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlAdd    = Url::toRoute( [ 'ttamhd/create', 'action' => 'create' ] );
$urlIndex  = Url::toRoute( [ 'ttamhd/index' ] );
$urlPrint  = $url[ 'print' ];
$urlDetail = Url::toRoute( [ 'ttamhd/detail' ] );
$urlMotorWarna = $url[ 'tdmotorwarna' ];
$urlData       = \yii\helpers\Url::toRoute( [ 'ttamhd/fill-combo' ] );
$readOnly      = ( ($_GET[ 'action' ] == 'view' || $_GET[ 'action' ] == 'display') ? 'true' : 'false' );
$this->registerJs( <<< JS

window.fillCombo = () => {
        $.ajax({
            type: 'POST',
            url: '{$urlData}',
            data: {
                 tgl:  $('#ttamhd-motortahun').val(), 
                 tgl2:$('#ttamhd-motortahun2').val(),
            }
        }).then(function (cusData) {
            window.dataRows = cusData.rows;            
        });       
    }
           
    $('#ttamhd-motortahun').on('change',fillCombo);
    $('#ttamhd-motortahun2').on('change',fillCombo);
    
     $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 250,
        loadonce:true,
        rowNum: 1000,
        extraParams: {
            AMNo: '$model->AMNo'
        },
        postData: {
            AMNo: '$model->AMNo'
        },
        rownumbers: true,   
        readOnly: $readOnly,       
        colModel: [
            { template: 'actions' },
            {
                name: 'MotorType',
                label: 'Type',
                editable: true,
                width: 200,
                editor: {
                    type: 'select2',
                    data: [],
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowId = $(this).attr('rowid');
						    $('[id="'+rowId+'_'+'MotorTahun'+'"]').val(data.MotorTahun);
                            $('[id="'+rowId+'_'+'MotorNama'+'"]').val(data.MotorNama);
                            $('[id="'+rowId+'_'+'MotorAutoN'+'"]').val(data.MotorAutoN);
                            $('[id="'+rowId+'_'+'MotorHarga'+'"]').val(0);
                            $('[id="'+rowId+'_'+'MotorNoMesin'+'"]').val(data.MotorNoMesin).trigger('change');
                            $('[id="'+rowId+'_'+'MotorNoRangka'+'"]').val(data.MotorNoRangka).trigger('change');
                            $('[id="'+rowId+'_'+'MotorWarna'+'"]').val(data.MotorWarna).trigger('change');
						}
                    },                       
                }
            },
            {
                name: 'MotorTahun',
                label: 'Tahun',
                width: 80,
                editable: true,
            },
            {
                name: 'MotorWarna',
                label: 'Warna',
                width: 100,
                editable: true,
                editor: {
                    type: 'select2',
                    data: [],
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowId = $(this).attr('rowid');
						    $('[id="'+rowId+'_'+'MotorTahun'+'"]').val(data.MotorTahun);
                            $('[id="'+rowId+'_'+'MotorNama'+'"]').val(data.MotorNama);
                            $('[id="'+rowId+'_'+'MotorAutoN'+'"]').val(data.MotorAutoN);
                            $('[id="'+rowId+'_'+'MotorHarga'+'"]').val(0);
                            $('[id="'+rowId+'_'+'MotorNoMesin'+'"]').val(data.MotorNoMesin).trigger('change');
                            $('[id="'+rowId+'_'+'MotorNoRangka'+'"]').val(data.MotorNoRangka).trigger('change');
                            $('[id="'+rowId+'_'+'MotorType'+'"]').val(data.MotorType).trigger('change');
						}
                    },        
                }
            },
            {
                name: 'MotorNoMesin',
                label: 'Nomor Mesin',
                width: 150,
                editable: true,
                editor: {
                    type: 'select2',
                    data: [],
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowId = $(this).attr('rowid');
						    $('[id="'+rowId+'_'+'MotorTahun'+'"]').val(data.MotorTahun);
                            $('[id="'+rowId+'_'+'MotorNama'+'"]').val(data.MotorNama);
                            $('[id="'+rowId+'_'+'MotorAutoN'+'"]').val(data.MotorAutoN);
                            $('[id="'+rowId+'_'+'MotorHarga'+'"]').val(0);
                            $('[id="'+rowId+'_'+'MotorType'+'"]').val(data.MotorType).trigger('change');
                            $('[id="'+rowId+'_'+'MotorNoRangka'+'"]').val(data.MotorNoRangka).trigger('change');
                            $('[id="'+rowId+'_'+'MotorWarna'+'"]').val(data.MotorWarna).trigger('change');
						}
                    },        
                }
            },
            {
                name: 'MotorNoRangka',
                label: 'No Rangka',
                width: 150,
                editable: true,
                editor: {
                    type: 'select2',
                    data: [],
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowId = $(this).attr('rowid');
						    $('[id="'+rowId+'_'+'MotorTahun'+'"]').val(data.MotorTahun);
                            $('[id="'+rowId+'_'+'MotorNama'+'"]').val(data.MotorNama);
                            $('[id="'+rowId+'_'+'MotorAutoN'+'"]').val(data.MotorAutoN);
                            $('[id="'+rowId+'_'+'MotorHarga'+'"]').val(0);
                            $('[id="'+rowId+'_'+'MotorNoMesin'+'"]').val(data.MotorNoMesin).trigger('change');
                            $('[id="'+rowId+'_'+'MotorType'+'"]').val(data.MotorType).trigger('change');
                            $('[id="'+rowId+'_'+'MotorWarna'+'"]').val(data.MotorWarna).trigger('change');
						}
                    },        
                }
            },
            {
                name: 'MMHarga',
                label: 'Harga',
                width: 100,
                editable: true,
                template: 'money',
            },
            {
                name: 'MotorAutoN',
                label: 'MotorAutoN',
                width: 100,
                editable: true,
                hidden: true,
            },           
        ],     
        
     listeners: {
            afterLoad: function(grid, response) {
            }
        }
    }).init().fit($('.box-body')).load({url: '$urlDetail'});
    
     $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {        
        var motorType = $('[id="'+rowid+'_'+'MotorType'+'"]');
        if(motorType[0].type != 'text'){
            motorType.select2('destroy').empty().select2({
            data: $.map(window.dataRows, function (obj) {
                      obj.id = obj.MotorType;
                      obj.text = obj.MotorType;
                      return obj;
                    })
            });
        }
        let motorWarna = $('[id="'+rowid+'_'+'MotorWarna'+'"]');
        if(motorWarna[0].type != 'text'){
            motorWarna.select2('destroy').empty().select2({
            data: $.map(window.dataRows, function (obj) {
                      obj.id = obj.MotorWarna;
                      obj.text = obj.MotorWarna;
                      return obj;
                    })
            });    
        }
        let motorNoMesin = $('[id="'+rowid+'_'+'MotorNoMesin'+'"]');
        if(motorNoMesin[0].type != 'text'){
            motorNoMesin.select2('destroy').empty().select2({
            data: $.map(window.dataRows, function (obj) {
                      obj.id = obj.MotorNoMesin;
                      obj.text = obj.MotorNoMesin;
                      return obj;
                    })
            });
        }
        let motorNoRangka = $('[id="'+rowid+'_'+'MotorNoRangka'+'"]');
        if(motorNoRangka[0].type != 'text'){
            motorNoRangka.select2('destroy').empty().select2({
            data: $.map(window.dataRows, function (obj) {
                      obj.id = obj.MotorNoRangka;
                      obj.text = obj.MotorNoRangka;
                      return obj;
                    })
            });
        }
        if (!rowid.includes('new_row')){
               var d = $('#detailGrid').utilJqGrid().getData(rowid);   
               console.log(d);
               $('[id="'+rowid+'_'+'MotorNoMesin'+'"]').val(d.data.MotorNoMesin).trigger('change');
               $('[id="'+rowid+'_'+'MotorTahun'+'"]').val(d.data.MotorTahun);
               $('[id="'+rowid+'_'+'MotorNama'+'"]').val(d.data.MotorNama);
               $('[id="'+rowid+'_'+'MotorAutoN'+'"]').val(d.data.MotorAutoN);
               $('[id="'+rowid+'_'+'MotorHarga'+'"]').val(d.data.MotorHarga);
               $('[id="'+rowid+'_'+'MotorType'+'"]').val(d.data.MotorType).trigger('change');
               $('[id="'+rowid+'_'+'MotorNoRangka'+'"]').val(d.data.MotorNoRangka).trigger('change');
               $('[id="'+rowid+'_'+'MotorWarna'+'"]').val(d.data.MotorWarna).trigger('change');
        }else{
            if (motorType[0].type != 'text' && motorType.select2('data').length > 0){
                var data = motorType.select2('data')[0];
                motorType.val(data.MotorType).trigger('change').trigger({
                    type: 'select2:select',
                    params: {
                        data: data
                    }
                });
            }
        }
    });
   
    $('#btnSave').click(function (event) {
        $('#frm_ttamhd_id').attr('action','{$url['update']}');
        $('#frm_ttamhd_id').submit();
      
    });
    
     $('#btnCancel').click(function (event) {	      
       $('#frm_ttamhd_id').attr('action','{$url['cancel']}');
       $('#frm_ttamhd_id').submit();
    });
     
     $('#btnPrint').click(function (event) {	      
        $('#frm_ttamhd_id').attr('action','$urlPrint');
        $('#frm_ttamhd_id').attr('target','_blank');
        $('#frm_ttamhd_id').submit();
	  }); 
     
     $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });   
    
      fillCombo();
    
JS
);