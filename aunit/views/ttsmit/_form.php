<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttsmit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ttsmit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'SMNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MotorNoMesin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MotorAutoN')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
