<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ttsmits';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttsmit-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ttsmit', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'SMNo',
            'MotorNoMesin',
            'MotorAutoN',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
