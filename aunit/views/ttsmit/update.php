<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttsmit */

$this->title = str_replace('Menu','',\aunit\components\TUi::$actionMode).'Ttsmit: ' . $model->SMNo;
$this->params['breadcrumbs'][] = ['label' => 'Ttsmits', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->SMNo, 'url' => ['view', 'SMNo' => $model->SMNo, 'MotorNoMesin' => $model->MotorNoMesin]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="ttsmit-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
