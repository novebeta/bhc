<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttsmit */

$this->title = 'Create Ttsmit';
$this->params['breadcrumbs'][] = ['label' => 'Ttsmits', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttsmit-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
