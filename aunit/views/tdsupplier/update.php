<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tdsupplier */
$this->title                   = str_replace('Menu','',\aunit\components\TUi::$actionMode).'Supplier: ' . $model->SupKode;
$this->params['breadcrumbs'][] = [ 'label' => 'Supplier', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->SupKode, 'url' => [ 'view', 'id' => $model->SupKode ] ];
$this->params['breadcrumbs'][] = 'Edit';
$params = '&id='.$id;
?>
<div class="tdsupplier-update">
	<?= $this->render( '_form', [
		'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdsupplier/update', 'id' => $id ] ),
            'cancel' => Url::toRoute( [ 'tdsupplier/cancel', 'id' => $id ,'action' => 'create'] )
        ]
//        'url' => [
//            'save'    => Custom::url(\Yii::$app->controller->id.'/update'.$params ),
//            'update'    => Custom::url(\Yii::$app->controller->id.'/update'.$params ),
//            'cancel'    => Custom::url(\Yii::$app->controller->id.'/' ),
//        ]
	] ) ?>
</div>
