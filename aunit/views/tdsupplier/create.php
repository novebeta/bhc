<?php
use yii\helpers\Html;
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tdsupplier */
$this->title                   = 'Tambah Supplier';
$this->params['breadcrumbs'][] = [ 'label' => 'Supplier', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdsupplier-create">
	<?= $this->render( '_form', [
		'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tdsupplier/update', 'id' => $id ] ),
            'cancel' => Url::toRoute( [ 'tdsupplier/cancel', 'id' => $id ,'action' => 'create'] )
        ]
//        'url' => [
//            'save'    => Custom::url(\Yii::$app->controller->id.'/create' ),
//            'update'    => Custom::url(\Yii::$app->controller->id.'/update' ),
//            'cancel'    => Custom::url(\Yii::$app->controller->id.'/' ),
//        ]
	] ) ?>
</div>
