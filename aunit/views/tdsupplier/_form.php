<?php
use common\components\Custom;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model aunit\models\Tdsupplier */
/* @var $form yii\widgets\ActiveForm */
//$url['cancel'] = Url::toRoute('tdsupplier/index');
?>

<div class="tdsupplier-form">
    <?php
    $form = ActiveForm::begin(['id' => 'frm_tdsupplier_id']);
    \aunit\components\TUi::form(
        [
            'class' => "row-no-gutters",
            'items' => [
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelSupKode"],
                        ['class' => "col-sm-5",'items' => ":SupKode"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelSupNama"],
                        ['class' => "col-sm-10",'items' => ":SupNama"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelSupContact"],
                        ['class' => "col-sm-8",'items' => ":SupContact"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelSupAlamat"],
                        ['class' => "col-sm-10",'items' => ":SupAlamat"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelSupKota"],
                        ['class' => "col-sm-8",'items' => ":SupKota"],
                    ],
                ],
                ['class' => "form-group col-md-24", 'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelSupTelepon"],
                        ['class' => "col-sm-6",'items' => ":SupTelepon"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-1",'items' => ":LabelSupFax"],
                        ['class' => "col-sm-5",'items' => ":SupFax"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-1",'items' => ":LabelSupEmail"],
                        ['class' => "col-sm-6",'items' => ":SupEmail"],
                    ],
                ],
            ]
        ],
        ['class' => "row-no-gutters",
            'items'  => [
                [
                    'class' => "col-md-12 pull-left",
                    'items' => $this->render( '../_nav', [
                        'url'=> $_GET['r'],
                        'options'=> [
                            'tdsupplier.SupNama'       => 'Nama Supplier',
                            'tdsupplier.SupKode'       => 'Kode Supplier',
                            'tdsupplier.SupContact'    => 'Kontak Person',
                            'tdsupplier.SupEmail'      => 'Email',
                            'tdsupplier.SupKeterangan' => 'Keterangan',
                            'tdsupplier.SupAlamat'     => 'Alamat',
                            'tdsupplier.SupKota'       => 'Kota',
                            'tdsupplier.SupTelepon'    => 'Telepon',
                            'tdsupplier.SupFax'        => 'Fax',
                            'tdsupplier.SupStatus'     => 'Status',
                        ],
                    ])
                ],
                ['class' => "col-md-12 pull-right",
                    'items'  => [
                        ['class' => "pull-right", 'items' => ":btnAction"],
                    ],
                ],
            ],
        ],
        [
            ":LabelSupKode"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode Supplier</label>',
            ":LabelSupNama"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Supplier</label>',
            ":LabelSupContact"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kontak</label>',
            ":LabelSupAlamat"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
            ":LabelSupKota"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kota</label>',
            ":LabelSupTelepon"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon</label>',
            ":LabelSupFax"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">Fax</label>',
            ":LabelSupEmail"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Email</label>',
            ":LabelSupKeterangan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
            ":LabelSupStatus"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',

            ":SupKode"          => $form->field($model, 'SupKode', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
            ":SupNama"          => $form->field($model, 'SupNama', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
            ":SupContact"       => $form->field($model, 'SupContact', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
            ":SupAlamat"        => $form->field($model, 'SupAlamat', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
            ":SupKota"          => $form->field($model, 'SupKota', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
            ":SupTelepon"       => $form->field($model, 'SupTelepon', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
            ":SupFax"           => $form->field($model, 'SupFax', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
            ":SupEmail"         => $form->field($model, 'SupEmail', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
            ":SupKeterangan"    => $form->field($model, 'SupKeterangan', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
//            ":SupNama"          => $form->field($model, 'SupNama', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
            ":SupStatus"        => $form->field($model, 'SupStatus', ['template' => '{input}'])
                                ->dropDownList([
                                    'A' => 'AKTIF',
                                    'N' => 'NON AKTIF',
                                    '1' => 'Supplier Utama',
                                ], ['maxlength' => true]),

            ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
            ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
            ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
            ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
            ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
        ],
            [
                '_akses' => 'Supplier',
                'url_main' => 'tdsupplier',
                'url_id' => $_GET['id'] ?? '',
            ]
    );
    ActiveForm::end();

    $urlAdd   = Url::toRoute( [ 'tdsupplier/create','id' => 'new','action'=>'create']);
    $urlIndex   = Url::toRoute( [ 'tdsupplier/index' ] );
    $this->registerJs( <<< JS
    $('#btnAdd').click(function (event) {	      
        window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });  

     $('#btnCancel').click(function (event) {	  
         window.location.href = '{$url['cancel']}';
    });

     $('#btnSave').click(function (event) {	      
//       $('#frm_tdsupplier_id').attr('action','{$url['update']}');
       $('#frm_tdsupplier_id').submit();
    });
     
     
    
JS);

    ?>
</div>
