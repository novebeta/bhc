<?php
use aunit\assets\AppAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel aunit\models\TdsupplierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
AppAsset::register( $this );
$this->title                   = 'Supplier';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
        'SupNama'       => 'Nama Supplier',
        'SupKode'       => 'Kode Supplier',
		'SupContact'    => 'Kontak Person',
        'SupEmail'      => 'Email',
        'SupKeterangan' => 'Keterangan',
        'SupAlamat'     => 'Alamat',
		'SupKota'       => 'Kota',
		'SupTelepon'    => 'Telepon',
		'SupFax'        => 'Fax',
		'SupStatus'     => 'Status',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
	'sortname'  => "SupStatus asc,SupKode asc,SupNama",
	'sortorder' => 'asc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Tdsupplier::className() ,'Supplier'), \yii\web\View::POS_READY );
