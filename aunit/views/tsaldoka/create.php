<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Tsaldoka */

$this->title = 'Create Tsaldoka';
$this->params['breadcrumbs'][] = ['label' => 'Tsaldokas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tsaldoka-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
