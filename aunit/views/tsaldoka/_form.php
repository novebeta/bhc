<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model aunit\models\Tsaldoka */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tsaldoka-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'SaldoKasTgl')->textInput() ?>

    <?= $form->field($model, 'LokasiKode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SaldoKasAwal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SaldoKasMasuk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SaldoKasKeluar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SaldoKasSaldo')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
