<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Tsaldoka */

$this->title = str_replace('Menu','',\aunit\components\TUi::$actionMode).'Tsaldoka: ' . $model->SaldoKasTgl;
$this->params['breadcrumbs'][] = ['label' => 'Saldo Kas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->SaldoKasTgl, 'url' => ['view', 'SaldoKasTgl' => $model->SaldoKasTgl, 'LokasiKode' => $model->LokasiKode]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="tsaldoka-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
