<?php
use aunit\components\FormField;
use aunit\models\Tdlokasi;
use aunit\models\Tdsupplier;
use common\components\Custom;
use common\components\General;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttprhd */
/* @var $form yii\widgets\ActiveForm */
$format          = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape          = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="ttprhd-form">
		<?php
		$dsTBeli[ 'PRDiscPersen' ] = 0;
		if ( $dsTBeli[ 'PRTotal' ] > 0 ) {
			$dsTBeli[ 'PRDiscPersen' ] = round( $dsTBeli[ 'PRDiscFinal' ] / $dsTBeli[ 'PRTotal' ] * 100 );
		}
		$SupplierKodeCmb = ArrayHelper::map( Tdsupplier::find()->select( [ 'SupKode', 'SupNama' ] )->where( "Supstatus = 'A' OR Supstatus = 1" )->orderBy( 'SupStatus,SupKode' )->all(), 'SupKode', 'SupNama' );
		$LokasiKodeCmb   = ArrayHelper::map( Tdlokasi::find()->select( [ 'LokasiKode' ] )->where( [ 'LokasiKode' => 'Retur' ] )->orderBy( 'LokasiStatus, LokasiKode' )->asArray()->all(), 'LokasiKode', 'LokasiKode' );
		$form            = ActiveForm::begin( [ 'id' => 'form_ttprhd_id', 'action' => $url[ 'update' ] ] );
		\aunit\components\TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-14",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelPRNo" ],
						      [ 'class' => "col-sm-8", 'items' => ":PRNo" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelPRTgl" ],
						      [ 'class' => "col-sm-4", 'items' => ":PRTgl" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelPRJam" ],
						      [ 'class' => "col-sm-3", 'items' => ":PRJam" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-10",
					      'items' => [
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-6", 'items' => ":LabelLokasi" ],
						      [ 'class' => "col-sm-17", 'items' => ":Lokasi" ],
					      ],
					    ],
				    ],
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-14",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelSupplier" ],
						      [ 'class' => "col-sm-21", 'items' => ":Supplier" ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-10",
					      'items' => [
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-6", 'items' => ":LabelTC" ],
						      [ 'class' => "col-sm-17", 'items' => ":TC" ]
					      ],
					    ],
				    ],
				  ],
				  [
					  'class' => "row col-md-24",
					  'style' => "margin-bottom: 6px;",
					  'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-4", 'items' => ":KodeBarang" ],
							        [ 'class' => "col-sm-10", 'items' => ":BarangNama", 'style' => 'padding-left:3px;' ],
							        [ 'class' => "col-sm-4", 'items' => ":Gudang", 'style' => 'padding-left:3px;' ],
							        [ 'class' => "col-sm-4", 'items' => ":Satuan", 'style' => 'padding-left:3px;' ],
							        [ 'class' => "col-sm-2", 'items' => ":BtnInfo" ],
						        ],
						      ],
						      [ 'class' => "form-group col-md-18",
						        'items' => [
							        [ 'class' => "col-sm-4", 'items' => ":LabelKeterangan" ],
							        [ 'class' => "col-sm-20", 'items' => ":Keterangan" ],
						        ],
						      ],
						      [ 'class' => "form-group col-md-6",
						        'items' => [
							        [ 'class' => "form-group col-md-24",
							          'items' => [
								          [ 'class' => "col-sm-1" ],
								          [ 'class' => "col-sm-4", 'items' => ":BtnBayar" ],
								          [ 'class' => "col-sm-1" ],
							          ],
							        ],
							        [ 'class' => "form-group col-md-24",
							          'items' => [
								          [ 'class' => "col-sm-1" ],
								          [ 'class' => "col-sm-8", 'items' => ":LabelCetak" ],
								          [ 'class' => "col-sm-12", 'items' => ":Cetak" ],
								          [ 'class' => "col-sm-1" ]
							          ],
							        ],
						        ],
						      ],
					      ],
					    ],
					    [ 'class' => "col-sm-1" ],
					    [ 'class' => "form-group col-md-12",
					      'items' => [
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-4", 'items' => ":LabelBayar" ],
							        [ 'class' => "col-sm-6", 'items' => ":Bayar" ],
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-3", 'items' => ":LabelSubTotal" ],
							        [ 'class' => "col-sm-9", 'items' => ":SubTotal" ],
						        ],
						      ],
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-4", 'items' => ":LabelSisa" ],
							        [ 'class' => "col-sm-6", 'items' => ":Sisa" ],
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-3", 'items' => ":LabelDisc" ],
							        [ 'class' => "col-sm-2", 'items' => ":Disc" ],
							        [ 'class' => "col-sm-1", 'items' => ":Persen", 'style' => 'padding-left:4px;' ],
							        [ 'class' => "col-sm-6", 'items' => ":Nominal" ],
						        ],
						      ],
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-4", 'items' => ":LabelStatus" ],
							        [ 'class' => "col-sm-4", 'items' => ":Status" ],
							        [ 'class' => "col-sm-2", 'items' => ":BtnStatus" ],
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-3", 'items' => ":LabelTotal" ],
							        [ 'class' => "col-sm-9", 'items' => ":Total" ]
						        ],
						      ],
					      ],
					    ],
				    ],
				  ],
			  ],
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-14",
				    'items' => [
					    [ 'class' => "col-sm-1", 'items' => ":LabelJT" ],
					    [ 'class' => "col-sm-4", 'items' => ":JT" ]
				    ],
				  ],
				  [ 'class' => "col-md-10",
				    'items' => [
					    [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
				    ]
				  ]
			  ]
			],
			[
				":LabelPRNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor PR</label>',
				":LabelPRTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tanggal</label>',
				":LabelPRJam"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jam</label>',
				":LabelTC"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">TC</label>',
				":LabelSubTotal"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sub Total</label>',
				":LabelKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelDisc"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Disc Final</label>',
				":Persen"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">%</label>',
				":LabelCetak"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Cetak</label>',
				":LabelTotal"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total</label>',
				":LabelSupplier"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Supplier</label>',
				":LabelLokasi"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Lokasi Keluar</label>',
				":LabelBayar"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total Bayar</label>',
				":LabelSisa"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sisa Bayar</label>',
				":LabelStatus"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status Bayar</label>',
				":LabelJT"         => \common\components\General::labelGL( $dsTBeli[ 'NoGL' ], 'JT' ),
				":PRNo"            => Html::textInput( 'PRNoView', $dsTBeli[ 'PRNoView' ], [ 'class' => 'form-control','readonly' => 'readonly' ] ) .
                                      Html::textInput( 'PRNo', $dsTBeli[ 'PRNo' ], [ 'class' => 'hidden' ] ).
				                      Html::textInput( 'PosKode', $dsTBeli[ 'PosKode' ], [ 'class' => 'hidden' ] ).
				                      Html::textInput( 'StokAll', 0, [ 'class' => 'hidden' ] ) .
				                      Html::textInput( 'PRTotalPajak', 0, [ 'id' => 'PRTotalPajak', 'class' => 'hidden' ] )  .
				                      Html::textInput( 'PRBiayaKirim', 0, [ 'id' => 'PRBiayaKirim', 'class' => 'hidden' ] ),
				":Keterangan"      => Html::textarea( 'PRKeterangan', $dsTBeli[ 'PRKeterangan' ], [ 'class' => 'form-control', 'maxlength' => '150' ] ),
				":Cetak"           => Html::textInput( 'Cetak', $dsTBeli[ 'Cetak' ], [ 'class' => 'form-control','readonly' => 'readonly' ] ),
				":JT"              => Html::textInput( 'NoGL', $dsTBeli[ 'NoGL' ], [ 'class' => 'form-control','readOnly'=>true ] ),
				":PRTgl"           => FormField::dateInput( [ 'name' => 'PRTgl', 'config' => [ 'value' => General::asDate( $dsTBeli[ 'PRTgl' ] ) ] ] ),
				":PRJam"           => FormField::timeInput( [ 'name' => 'PRJam', 'config' => [ 'value' => $dsTBeli[ 'PRTgl' ] ] ] ),
				":Lokasi"          => FormField::combo( 'LokasiKode', [ 'config' => [ 'value' => $dsTBeli['LokasiKode'], 'data' => $LokasiKodeCmb ], 'extraOptions' => [ 'simpleCombo' => true] ] ),
				":Supplier"        => FormField::combo( 'SupKode', [ 'config' => [ 'value' => $dsTBeli[ 'SupKode' ], 'data' => $SupplierKodeCmb ], 'extraOptions' => [ 'simpleCombo' => [ 'SupNama' ] ] ] ),
				":TC"              => FormField::combo( 'KodeTrans', [ 'config' => [ 'value' => $dsTBeli[ 'KodeTrans' ], 'data' => [
                                            'PR' => 'PR'
                                        ] ], 'extraOptions'  => [ 'simpleCombo' => true ] ] ),
				":Disc"            => FormField::integerInput( [ 'name' => 'PRDiscPersen', 'config' => [ 'id' => 'PRDiscPersen', 'value' => $dsTBeli[ 'PRDiscPersen' ] ] ] ),
				":KodeBarang"      => Html::textInput( 'KodeBarang', '', [ 'id' => 'KodeBarang', 'class' => 'form-control','readonly' => 'readonly' ] ),
				":BarangNama"      => Html::textInput( 'BarangNama', '', [ 'id' => 'BarangNama', 'class' => 'form-control' ,'readonly' => 'readonly'] ),
				":Gudang"          => Html::textInput( 'LokasiKodeTxt', $dsTBeli[ 'LokasiKode' ], [ 'id' => 'LokasiKodeTxt', 'class' => 'form-control' ,'readonly' => 'readonly'] ),
				":Status"          => Html::textInput( 'StatusBayar', $dsTBeli[ 'StatusBayar' ], [ 'id' => 'StatusBayar', 'class' => 'form-control','readonly' => 'readonly' ] ),
                ":Satuan"           => Html::textInput( 'lblStock', 0, [ 'type' => 'number', 'class' => 'form-control', 'style' => 'color: blue; font-weight: bold;', 'readonly' => 'readonly' ] ),

                ":SubTotal"        => FormField::numberInput( [ 'name' => 'PRSubTotal', 'config' => [ 'id' => 'PRSubTotal','value' => $dsTBeli[ 'PRSubTotal' ] ,'readonly' => 'readonly'] ] ),
                ":Total"           => FormField::numberInput( [ 'name' => 'PRTotal', 'config' => [ 'id' => 'PRTotal','value' => $dsTBeli[ 'PRTotal' ],'readonly' => 'readonly' ] ] ),
                ":Bayar"           => FormField::numberInput( [ 'name' => 'PRTerbayar', 'config' => [ 'id' => 'PRTerbayar','value' => $dsTBeli[ 'PRTerbayar' ],'readonly' => 'readonly' ] ] ),
                ":Nominal"         => FormField::numberInput( [ 'name' => 'PRDiscFinal', 'config' => [ 'id' => 'PRDiscFinal','value' => $dsTBeli[ 'PRDiscFinal' ] ] ] ),
                ":Sisa"            => FormField::numberInput( [ 'name' => 'PRSisa', 'config' => [ 'id' => 'SisaBayar','value' => 0,'readonly' => 'readonly' ] ] ),

				":BtnInfo"         => '<button type="button" class="btn btn-default"><i class="fa fa-info-circle fa-lg"></i></button>',
				":BtnBayar"        => Html::button( '<i class="fa fa-thumbs-up"></i> Bayar', [ 'class' => 'btn btn-success', 'style' => 'width:120px', 'id' => 'BtnBayar' ] ),
                ":BtnStatus"       => Html::button( '<i class="fa fa-refresh fa-lg"></i>', [ 'class' => 'btn btn-default', 'id' => 'BtnStatus' ] ),
			], [
				'url_main' => 'ttprhd',
				'url_id' => $id,
			]
		);
		ActiveForm::end();
		?>
    </div>
<?
$urlStatusOtomatis  = Url::toRoute( [ 'ttprhd/status-otomatis',  'PRNo' => $dsTBeli[ 'PRNo' ], ] );
$urlCheckStock      = Url::toRoute( [ 'site/check-stock-transaksi' ] );
$urlBayarOtomatis   = Url::toRoute( [ 'ttbmhd/bayar-otomatis', 'NominalBayar' => $dsTBeli[ 'PRTotal' ], ] );
$urlDetail          = $url[ 'detail' ];
$urlPrint           = $url[ 'print' ];
$readOnly           = ( $_GET['action'] == 'view' ? 'true' : 'false' );
$urlBayar           = Url::toRoute( [ 'bayar/save' ] );
$urlRefresh         = $url[ 'update' ] . '&oper=skip-load&PRNoView=' . $dsTBeli[ 'PRNoView' ];
$resultKas          = \aunit\models\Ttkmhd::find()->callSP( [ 'Action' => 'Insert' ] );
$resultBank         = \aunit\models\Ttbmhd::find()->callSP( [ 'Action' => 'Insert' ] );
$barang             = \common\components\General::cCmd( "SELECT tdbarang.BrgKode, tdbarang.BrgNama, tdbarang.BrgBarCode, tdbarang.BrgGroup, tdbarang.BrgSatuan, tdbarang.BrgHrgBeli, tdbarang.BrgHrgJual, tdbarang.BrgRak1, tdbarang.BrgMinStock, tdbarang.BrgMaxStock, tdbarang.BrgStatus , SUM(SaldoQty) AS SaldoQty, tdbaranglokasi.LokasiKode
                        FROM tdbarang
                        INNER JOIN tdbaranglokasi ON tdbaranglokasi.BrgKode = tdbarang.BrgKode WHERE LokasiKode = 'Retur'
                        GROUP BY tdbarang.BrgKode HAVING SUM(SaldoQty) > 0
                        UNION
                        SELECT tdbarang.BrgKode, tdbarang.BrgNama, tdbarang.BrgBarCode, tdbarang.BrgGroup, tdbarang.BrgSatuan, tdbarang.BrgHrgBeli, tdbarang.BrgHrgJual, tdbarang.BrgRak1, tdbarang.BrgMinStock, tdbarang.BrgMaxStock, tdbarang.BrgStatus , SUM(SaldoQty) AS SaldoQty, tdbaranglokasi.LokasiKode
                        FROM tdbarang
                        INNER JOIN tdbaranglokasi ON tdbaranglokasi.BrgKode = tdbarang.BrgKode
                        INNER JOIN ttPRit ON ttPRit.BrgKode = tdbarang.BrgKode
                        WHERE ttPRit.PRNo = :PRNo GROUP BY tdbarang.BrgKode
                        ORDER BY BrgStatus, BrgKode", [ ':PRNo' => $dsTBeli[ 'PRNo' ] ] )->queryAll();
$this->registerJsVar( '__Barang', json_encode( $barang ) );
$this->registerJs( <<< JS
	var __BrgKode = JSON.parse(__Barang);
     __BrgKode = $.map(__BrgKode, function (obj) {
                      obj.id = obj.BrgKode;
                      obj.text = obj.BrgKode;
                      return obj;
                    });
     var __BrgNama = JSON.parse(__Barang);
     __BrgNama = $.map(__BrgNama, function (obj) {
                      obj.id = obj.BrgNama;
                      obj.text = obj.BrgNama;
                      return obj;
                    });
     function HitungTotal(){
         	let PRTotalPajak = $('#detailGrid').jqGrid('getCol','PRPajak',false,'sum');
         	$('#PRTotalPajak').utilNumberControl().val(PRTotalPajak);
         	let PRSubTotal = $('#detailGrid').jqGrid('getCol','Jumlah',false,'sum');
         	$('#PRSubTotal').utilNumberControl().val(PRSubTotal);
         	HitungBawah();
         }
         window.HitungTotal = HitungTotal;
         function HitungBawah(){
         	let PRDiscPersen = parseFloat($('#PRDiscPersen').val());                 
         	let PRSubTotal = parseFloat($('#PRSubTotal').val());      
         	let PRDiscFinal = PRDiscPersen * PRSubTotal / 100;                 
         	$('#PRDiscFinal').utilNumberControl().val(PRDiscFinal);
         	let PRTotalPajak = parseFloat($('#PRTotalPajak').val());                 
         	let PRBiayaKirim = parseFloat($('#PRBiayaKirim').val());                 
         	let PRTotal = PRSubTotal + PRTotalPajak + PRBiayaKirim - PRDiscFinal;
         	$('#PRTotal').utilNumberControl().val(PRTotal);
         	let TotalBayar = parseFloat($('#PRTerbayar').val());   
         	let SisaBayar = PRTotal - TotalBayar;
         	$('#SisaBayar').utilNumberControl().val(SisaBayar);
         }         
         window.HitungBawah = HitungBawah;
         
         function checkStok(BrgKode,BarangNama){
           $.ajax({
                url: '$urlCheckStock',
                type: "POST",
                data: { 
                    BrgKode: BrgKode,
                },
                success: function (res) {
                    $('[name=LokasiKode]').val(res.LokasiKode);
                    $('#KodeBarang').val(BrgKode);
                    $('[name=BarangNama]').val(BarangNama);
                    $('[name=lblStock]').val(res.SaldoQty);
                },
                error: function (jXHR, textStatus, errorThrown) {
                    
                },
                async: false
            });
          }
          window.checkStok = checkStok;	
          
     $('#detailGrid').utilJqGrid({
     editurl: '$urlDetail',
        height: 215,
        extraParams: {
            PRNo: $('input[name="PRNo"]').val()
        },
        rownumbers: true,  
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,      
        colModel: [
            { template: 'actions'  },
            {
                name: 'BrgKode',
                label: 'Kode',
                width: 130,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __BrgKode,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'PRHrgJual'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgJual));
                            $('[id="'+rowid+'_'+'BrgGroup'+'"]').val(data.BrgGroup);
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').val(data.BrgNama); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').trigger('change');
                            checkStok($(this).val(),$('[id="'+rowid+'_'+'BrgNama'+'"]').val());
                            window.hitungLine();
						},
						'change': function(){      
                            let rowid = $(this).attr('rowid');
                            checkStok($(this).val(),$('[id="'+rowid+'_'+'BrgNama'+'"]').val());
						}
                    }                 
                }
            },
            {
                name: 'BrgNama',
                label: 'Nama Barang',
                width: 200,
                editable: true,
                editor: {
                    type: 'select2',
                    data: __BrgNama,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    console.log(data);
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'PRHrgJual'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgJual));
                            $('[id="'+rowid+'_'+'BrgGroup'+'"]').val(data.BrgGroup);
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').val(data.BrgKode); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').trigger('change');
                            window.hitungLine();
						}
                    }              
                }
            },
            {
                name: 'PRQty',
                label: 'Qty',
                editable: true,
                width: 100,
                template: 'number'
            },     
            {
                name: 'BrgSatuan',
                label: 'Satuan',
                editable: true,
                width: 100,
            },      
            {
                name: 'PRHrgJual',
                label: 'Harga Part',
                editable: true,
                width: 100,
                template: 'money'
            },   
            {
                name: 'Disc',
                label: 'Disc %',
                editable: true,
                width: 100,
                template: 'number'
            },     
            {
                name: 'PRDiscount',
                label: 'Nilai Disc',
                width: 100,
                editable: true,
                template: 'money'
            },
            {
                name: 'Jumlah',
                label: 'Jumlah',
                width: 100,
                editable: true,
                template: 'money'
            },
            {
                name: 'PRAuto',
                label: 'PRAuto',
                editable: true,
                hidden: true
            },
            {
                name: 'PRPajak',
                label: 'Pajak %',
                width: 100,
                editable: true,
                hidden: true,
                template: 'money'
            },
        ],  
        listeners: {
            afterLoad: function (data) {
             window.HitungTotal();
            }    
        },   
        
        onSelectRow : function(rowid,status,e){ 
            if (window.rowId !== -1) return;
            if (window.rowId_selrow === rowid) return;
            if(rowid.includes('new_row') === true){
                $('[name=KodeBarang]').val('--');
                $('[name=LokasiKode]').val('Kosong');
                $('[name=BarangNama]').val('--');
                $('[name=lblStock]').val(0);
                return ;
            };
            let r = $('#detailGrid').utilJqGrid().getData(rowid).data;
			checkStok(r.BrgKode,r.BrgNama);
			window.rowId_selrow = rowid;
        }      
     }).init().fit($('.box-body')).load({url: '$urlDetail'});
     window.rowId_selrow = -1;
     function hitungLine(){
            //console.log('hitung');   
            let Disc = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'Disc'+'"]').val()));
            let PRHrgJual = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'PRHrgJual'+'"]').val()));
            let PRQty = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'PRQty'+'"]').val()));
            let PRDiscount = Disc*PRHrgJual*PRQty / 100;
            $('[id="'+window.cmp.rowid+'_'+'PRDiscount'+'"]').utilNumberControl().val(PRDiscount);
            $('[id="'+window.cmp.rowid+'_'+'Jumlah'+'"]').utilNumberControl().val(PRHrgJual * PRQty - PRDiscount);
            window.HitungTotal();
         }
         window.hitungLine = hitungLine;
		
         $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
	     window.cmp.rowid = rowid;    
          if(rowid.includes('new_row')){
              var BrgKode = $('[id="'+rowid+'_'+'BrgKode'+'"]');
              var dtBrgKode = BrgKode.select2('data');
              if (dtBrgKode.length > 0){
                  var dt = dtBrgKode[0];
                  BrgKode.val(dt.BrgKode).trigger('change').trigger({
                    type: 'select2:select',
                    params: {
                        data: dt
                    }
                });
              }
            $('[id="'+rowid+'_'+'PRQty'+'"]').val(1);
          } 
                   
          $('[id="'+window.cmp.rowid+'_'+'PRQty'+'"]').on('keyup', function (event){
              hitungLine();
          });
          $('[id="'+window.cmp.rowid+'_'+'PRHrgJual'+'"]').on('keyup', function (event){
              hitungLine();
          });
          $('[id="'+window.cmp.rowid+'_'+'Disc'+'"]').on('keyup', function (event){
              hitungLine();
          });
         hitungLine();
	 });
      
     $('#btnSave').click(function (event) {
         let Nominal = parseFloat($('#PRSubTotal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Subtotal tidak boleh 0', size: 'small'});
               return;
           }    
        $('input[name="SaveMode"]').val('ALL');
        $('#form_ttprhd_id').attr('action','{$url['update']}');
        $('#form_ttprhd_id').submit();
      }); 
	  
	  $('#PRDiscPersen').change(function (event) {
	      HitungBawah();
	  }); 
	  
	    $('#BtnBayar').click(function() {	
	    let MotorNoPolisi = $('input[name="MotorNoPolisi"]').val();
	    let CusNama = $('input[name="CusNama"]').val();
	    let PRNoView = $('input[name="PRNoView"]').val();
	    let PRNo = $('input[name="PRNo"]').val();
        let SisaBayar = parseFloat($('#SisaBayar').utilNumberControl().val());        
        if(PRNo.includes('=')){
             bootbox.alert({message:'Silahkan simpan transaksi ini dulu.',size: 'small'});
	        return;
        }
	    if (SisaBayar <= 0){
	        bootbox.alert({message:'Transaksi ini sudah terlunas',size: 'small'});
	        return;
	    }
	    $('#MyKodeTrans').val('PR');
	    $('#MyKode').val(MotorNoPolisi);
	    $('#MyNama').val(CusNama);
	    $('#MyNoTransaksi').val(PRNo);
	    $('#BMMemo').val("Pembayaran [SE] Etimasi Biaya Servis No: "+PRNoView+" - "+MotorNoPolisi+" - "+CusNama);
	    $('#BMNominal').utilNumberControl().val(SisaBayar);
	    $('#BMBayar').utilNumberControl().val(SisaBayar);
        $('#modalBayar').modal('show');
    });
	
	$('#btnSavePopup').click(function(){
	    $.ajax({
            type: 'POST',
            url: '$urlBayar',
            data: $('#form_bayar_id').serialize()
        }).then(function (cusData) {
           console.log(cusData);
           window.location.href = "{$urlRefresh}"; 
        });
	});
	
	function onFocus(){
        window.location.href = "{$urlRefresh}"; 
    };	
	
	$('#BtnStatus').click(function() {
        window.util.app.popUpForm.show({
        title: "Daftar Transaksi Keuangan yang melibatkan Nomor PR : "+$('input[name="PRNo"]').val(),
        size: 'small',
        url: '$urlStatusOtomatis',
        width: 900,
        height: 320
        },
        function(data) {
            console.log(data);
        });
    });
    
	$('#BtnBayar').attr('disabled',false);

JS
);
echo $this->render( '../_formBayar' );
