<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttprhd */

$params                          = '&id=' . $id . '&action=update';
$cancel = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = 'Edit - Retur Pembelian: ' . $dsTBeli['PRNoView'];
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Retur Pembelian', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = 'Edit';

?>
<div class="ttprhd-update">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTBeli' => $dsTBeli,
        'id'     => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail' => Url::toRoute( [ 'ttprit/index','action' => 'update' ] ),
        ]
    ]) ?>

</div>
