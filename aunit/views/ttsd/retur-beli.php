<?php
use aunit\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = 'Retur Beli';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'TtSD.SDNo'       => 'No Mutasi',
		'TtSD.LokasiKode' => 'Kode Lokasi Asal',
		'LokasiNama'      => 'Nama Lokasi Asal',
		'TtSD.DealerKode' => 'Kode Dealer Tujuan',
		'DealerNama'      => 'Nama Dealer Tujuan',
		'TtSD.SDMemo'     => 'Keterangan',
		'ttsd.UserID'     => 'User',
		'ttsd.NoGL'       => 'No GL',
	],
	'cmbTgl'    => [
		'SDTgl' => 'Tanggal SD / ME',
	],
	'cmbNum'    => [
		'SDTotal' => 'Total',
	],
	'sortname'  => "SDTgl desc, SupNama",
	'sortorder' => 'asc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttsd::className(), 'Retur Beli', [
	'btnAdd_Disabled' => false,
	'url_add'    => Url::to( ['ttsd/retur-beli-create' ,'action'=>'create']),
	'url_update'    => Url::to( ['ttsd/retur-beli-update','action'=>'update' ]),
    'url_delete'    => Url::toRoute( [ 'ttsd/delete','KodeTrans'=>'SR'] ),
    'colGrid'=> \aunit\models\Ttsd::colGridReturBeli()
] ), \yii\web\View::POS_READY );