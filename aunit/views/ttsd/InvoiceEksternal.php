<?php
use aunit\assets\AppAsset;
use aunit\models\Ttsd;
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel aunit\models\TtsdSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Invoice Eksternal';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'TtSD.SDNo'             => 'No Mutasi',
		'TtSD.LokasiKode'       => 'Kode Lokasi Asal',
		'LokasiNama'            => 'Nama Lokasi Asal',
		'TtSD.DealerKode'       => 'Kode Dealer Tujuan',
		'DealerNama'            => 'Nama Dealer Tujuan',
		'TtSD.SDMemo'           => 'Keterangan',
		'ttsd.UserID'           => 'User',
		'ttsd.NoGL'             => 'No GL',
	],
	'cmbTgl'    => [
			'SDTgl'      => 'Tanggal SD / ME',
	],
	'cmbNum'    => [
			'SDTotal'    => 'Total',
	],
	'sortname'  => "SDTgl",
	'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttsd::className(),
	'Invoice Eksternal', [
		'url_update'      => Url::to( [ 'ttsd/invoice-eksternal-dealer-update', 'action' => 'update' ] ),
		'url_delete'    => Url::toRoute( [ 'ttsd/delete','KodeTrans'=>'IE'] ),
		'colGrid' => Ttsd::colGridInvoiceEksternal(),
		'btnAdd_Disabled' => true,
	] ), \yii\web\View::POS_READY );