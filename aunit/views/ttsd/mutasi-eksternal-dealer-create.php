<?php

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttsd */
$isInv = ( $jenis == 'InvoiceEksternal' );
$title = $isInv ? "Invoice Mutasi Eksternal" : "Mutasi Eksternal";
$view = $isInv ? "invoice-eksternal-dealer" : "mutasi-eksternal-dealer";
$cancel = Url::toRoute( [ \Yii::$app->controller->id . '/cancel', 'id' => $id, 'action' => 'create','StatPrint'=> ( $jenis == 'MutasiEksternal') ? '3':'4' ] );
$this->title = "Tambah $title Ke Dealer Lain";
//$this->params['breadcrumbs'][] = ['label' => $title, 'url' => $cancel];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttsd-create">
    <?= $this->render('mutasi-eksternal-dealer-form', [
        'model' => $model,
        'dsBeli' => $dsBeli,
        'jenis'  => $jenis,
        'url'    => [
	        'update'       => Url::toRoute( [ \Yii::$app->controller->id . "/$view-update", 'id' => $id, 'action' => 'create' ] ),
	        'print'        => Url::toRoute( [ \Yii::$app->controller->id . '/print', 'id' => $id, 'action' => 'create' ] ),
	        'cancel'       => $cancel,
	        'detail'       => Url::toRoute( [ \Yii::$app->controller->id . '/detail', 'jenis' => $jenis, 'action' => 'create' ] ),
        ]
    ]) ?>

</div>
