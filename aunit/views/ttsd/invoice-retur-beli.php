<?php
use aunit\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = 'Invoice Retur Beli';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'TtSD.SDNo'       => 'No Mutasi',
		'TtSD.LokasiKode' => 'Kode Lokasi Asal',
		'LokasiNama'      => 'Nama Lokasi Asal',
		'TtSD.DealerKode' => 'Kode Dealer Tujuan',
		'DealerNama'      => 'Nama Dealer Tujuan',
		'TtSD.SDMemo'     => 'Keterangan',
		'ttsd.UserID'     => 'User',
		'ttsd.NoGL'       => 'No GL',
	],
	'cmbTgl'    => [
		'SDTgl' => 'Tanggal SD / ME',
	],
	'cmbNum'    => [
		'SDTotal' => 'Total',
	],
	'sortname'  => "SDTgl",
	'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttsd::className(),
	'Invoice Retur Beli', [
		'url_update'      => Url::to( [ 'ttsd/invoice-retur-beli-update','action'=>'update' ] ),
		'colGrid'         => \aunit\models\Ttsd::colGridInvoiceReturBeli(),
		'url_delete'    => Url::toRoute( [ 'ttsd/delete','KodeTrans'=>'IR'] ),
		'btnAdd_Disabled' => true
	] ), \yii\web\View::POS_READY );