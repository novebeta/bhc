<?php
use abengkel\assets\AppAsset;
use aunit\models\Ttsd;
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                     = 'Mutasi Eksternal ke Dealer';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'TtSD.SDNo'       => 'No Mutasi',
		'TtSD.LokasiKode' => 'Kode Lokasi Asal',
		'LokasiNama'      => 'Nama Lokasi Asal',
		'TtSD.DealerKode' => 'Kode Dealer Tujuan',
		'DealerNama'      => 'Nama Dealer Tujuan',
		'TtSD.SDMemo'     => 'Keterangan',
		'ttsd.UserID'     => 'User',
		'ttsd.NoGL'       => 'No GL',
	],
	'cmbTgl'    => [
		'SDTgl' => 'Tanggal SD / ME',
	],
	'cmbNum'    => [
		'SDTotal' => 'Total',
	],
	'sortname'  => "SDTgl",
	'sortorder' => 'desc'
];
if ( isset( $mode ) && $mode != '' ) {
	$arr = [
		'cmbTxt'    => [
			'SDNo'       => 'No Mutasi',
			'LokasiKode' => 'Kode Lokasi Asal',
			'LokasiNama' => 'Nama Lokasi Asal',
			'DealerKode' => 'Kode Dealer Tujuan',
			'DealerNama' => 'Nama Dealer Tujuan',
			'SDMemo'     => 'Keterangan',
			'UserID'     => 'User',
			'NoGL'       => 'No GL',
		],
		'cmbTgl'    => [
			'SDTgl' => 'Tanggal SD / ME',
		],
		'cmbNum'    => [
			'SDTotal' => 'Total',
		],
		'sortname'  => "SDTgl",
		'sortorder' => 'desc'
	];
}
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttsd::className(),
	'Mutasi Eksternal ke Dealer', [
		'colGrid'    => ($mode == '') ? Ttsd::colGridMutasiEksternalDealer() : Ttsd::colGridMutasiEksternalDealerSelect(),
		'mode'       => isset( $mode ) ? $mode : '',
		//        'url_add'    => Custom::url( 'ttsd/mutasi-eksternal-dealer-create' ),
		//        'url_update'    => Custom::url( 'ttsd/mutasi-eksternal-dealer-update' ),
		'url_add'    => Url::toRoute( [ 'ttsd/mutasi-eksternal-dealer-create', 'action' => 'create' ] ),
		'url_update' => Url::toRoute( [ 'ttsd/mutasi-eksternal-dealer-update', 'action' => 'update' ] ),
		'url_delete' => Url::toRoute( [ 'ttsd/delete', 'KodeTrans' => 'ME' ] ),
	] ), \yii\web\View::POS_READY );
$urlDetail = Url::toRoute( [ \Yii::$app->controller->id . '/detail', 'jenis' => $jenis ] );
$this->registerJs( <<< JS
jQuery(function ($) {
    
    window.getUrl = function(){
        let addUrl = '';
        if (window.DealerKode !== undefined){
            addUrl = '&DealerKode=' + window.DealerKode;
        }
        return '{$urlDetail}' + addUrl;
    }           
    if($('#jqGridDetail').length) {        
        $('#jqGridDetail').utilJqGrid({
            url: getUrl(),
            height: 240,
            readOnly: true,
            rownumbers: true,
            rowNum: 1000,
            colModel: [
                {
                    name: 'MotorType',
                    label: 'Type',
                    width: 200
                },
                {
                    name: 'MotorTahun',
                    label: 'Tahun',
                    width: 100
                },
                {
                    name: 'MotorWarna',
                    label: 'Warna',
                    width: 150
                },
                {
                    name: 'MotorNoMesin',
                    label: 'NoMesin',
                    width: 200,
                    editable: true
                },
                {
                    name: 'MotorAutoN',
                    label: 'MotorAutoN',
                    width: 200,
                    hidden: true,
                    editable: true
                },
                {
                    name: 'MotorNoRangka',
                    label: 'NoRangka',
                    width: 200
                },
                {
                    name: 'FBNo',
                    label: 'No Faktur',
                    width: 200
                },
                // {
                //     name: 'SSHarga',
                //     label: 'HrgBeli',
                //     template: 'money',
                //     width: 200
                // },
                {
                    name: 'SDHarga',
                    label: 'HrgBeli',
                    template: 'money',
                    width: 200
                }
            ],
	        multiselect: gridFn.detail.multiSelect(),
	        onSelectRow: gridFn.detail.onSelectRow,
	        onSelectAll: gridFn.detail.onSelectAll,
	        ondblClickRow: gridFn.detail.ondblClickRow,
	        loadComplete: gridFn.detail.loadComplete,
	        listeners: {
                afterLoad: function(grid, response) {
                    $("#cb_jqGridDetail").trigger('click');
                }
            }
        }).init().setHeight(gridFn.detail.height);
    }
});
JS, \yii\web\View::POS_READY );