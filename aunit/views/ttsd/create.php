<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttsd */
$isSuratReturBeli = ($jenis == 'SuratReturBeli');
$label = ( $isSuratReturBeli ? 'Retur Beli' : 'Invoice Retur Beli' );
$urlIndex = ( $isSuratReturBeli ? 'retur-beli' : 'invoice-retur-beli' );
$cancel = Url::toRoute( [ \Yii::$app->controller->id . '/cancel', 'id' => $id, 'action' => 'create','StatPrint'=> ( $jenis == 'SuratReturBeli' ) ? '1' : '2' ] );
$this->title                     = 'Tambah '.$label;
//$this->params[ 'breadcrumbs' ][] = [ 'label' => $label, 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="ttsd-create">
	<?= $this->render( '_form', [
		'model'  => $model,
		'dsBeli' => $dsBeli,
		'jenis'  => $jenis,
		'url'    => [
			'update'       => Url::toRoute( [ \Yii::$app->controller->id . '/retur-beli-update', 'id' => $id, 'action' => 'create' ] ),
			'print'        => Url::toRoute( [ \Yii::$app->controller->id . '/print', 'id' => $id, 'action' => 'create' ] ),
			'cancel'       => $cancel,
			'detail'       => Url::toRoute( [ \Yii::$app->controller->id . '/detail', 'jenis' => $jenis, 'action' => 'create' ] ),
		]
	] ) ?>
</div>
