<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttsd */

$this->title = 'Tambah Invoice Retur Beli';
$this->params['breadcrumbs'][] = ['label' => 'Invoice Retur Beli', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttsd-create">
    <?= $this->render('_form', [
        'model' => $model,
        'dsBeli' => $dsBeli,
        'jenis'  => $jenis,
        'url'    => [
            'update'       => Url::toRoute( [ \Yii::$app->controller->id . '/invoice-retur-beli-update', 'id' => $id, 'action' => 'update' ] ),
            'print'        => Url::toRoute( [ \Yii::$app->controller->id . '/print', 'id' => $id, 'action' => 'update' ] ),
            'cancel'       => Url::toRoute( [ \Yii::$app->controller->id . '/retur-beli', 'id' => $id, 'action' => 'update' ] ),
            'detail'       => Url::toRoute( [ \Yii::$app->controller->id . '/detail', 'jenis' => $jenis, 'action' => 'update'  ] ),
        ]
    ]) ?>

</div>
