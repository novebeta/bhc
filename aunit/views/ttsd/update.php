<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttsd */
$isSuratReturBeli = ($jenis == 'SuratReturBeli');
$label = ( $isSuratReturBeli ? 'Retur Beli' : 'Invoice Retur Beli' );
$urlIndex = ( $isSuratReturBeli ? 'retur-beli' : 'invoice-retur-beli' );
$urlUpdate = ( $isSuratReturBeli ? 'retur-beli-update' : 'invoice-retur-beli-update' );
$cancel = Url::toRoute( [ \Yii::$app->controller->id . '/cancel', 'id' => $id, 'action' => 'update','StatPrint'=> ( $jenis == 'SuratReturBeli' ) ? '1' : '2'] );
$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode).' '.$label.': ' . $dsBeli[ 'SDNoView' ];
//$this->params[ 'breadcrumbs' ][] = [ 'label' => $label, 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = 'Edit';
?>
<div class="ttsd-update">
	<?= $this->render( '_form', [
		'model' => $model,
		'dsBeli' => $dsBeli,
		'jenis'  => $jenis,
		'url'    => [
			'update'       => Url::toRoute( [ \Yii::$app->controller->id . '/'.$urlUpdate, 'id' => $id, 'action' => 'update' ] ),
			'print'        => Url::toRoute( [ \Yii::$app->controller->id . '/print', 'id' => $id, 'action' => 'update' ] ),
			'cancel'       => $cancel,
			'detail'       => Url::toRoute( [ \Yii::$app->controller->id . '/detail', 'jenis' => $jenis, 'action' => 'update'  ] ),
		]
	] ) ?>
</div>
