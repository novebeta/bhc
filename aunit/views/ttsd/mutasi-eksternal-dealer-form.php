<?php
use aunit\components\FormField;
use aunit\models\Tdlokasi;
use common\components\Custom;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttsd */
/* @var $form yii\widgets\ActiveForm */
$urlData = \yii\helpers\Url::toRoute( [ 'ttsd/detail', 'jenis' => 'FillMotorGridAddEdit' ] );
$format     = <<< SCRIPT
function formatttsd(result) {
return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-10">' + result.id + '</div>' +
           '<div class="col-xs-14">' + result.text + '</div>' +
           '</div>';
}
SCRIPT;
$DataLokasi = ArrayHelper::map( Tdlokasi::find()->select2( 'LokasiKode', [ 'LokasiNama' ], "LokasiStatus,  LokasiKode",
	[ "condition" => "LokasiStatus <> 'N'", 'params' => [] ], false ), "value", 'label' );
$escape     = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
$isInv = ( $jenis == 'InvoiceEksternal' );
?>
<div class="ttss-form">
	<?php
	$form = ActiveForm::begin( [ 'id' => 'form_ttsd_id', 'action' => $url[ 'update' ] ] );
	\aunit\components\TUi::form(
		[
			'class' => "row-no-gutters",
			'items' => [
				[ 'class' => "col-md-24",
				  'items' => [
					  [ 'class' => "form-group col-md-24",
					    'items' => [
						    [ 'class' => "col-sm-2", 'items' => ":LabelSDNo" ],
						    [ 'class' => "col-sm-4", 'style' => "padding-right: 1px;", 'items' => ":SDNo" ],
						    [ 'class' => "col-sm-2", 'style' => "padding-left: 1px;padding-right: 1px;", 'items' => ":SDTgl" ],
						    [ 'class' => "col-sm-3", 'style' => "padding-right: 1px;", 'items' => ":SDJam" ],
						    [ 'class' => "col-sm-2" ],
						    [ 'class' => "col-sm-2", 'items' => ":LabelLokasiAsal" ],
						    [ 'class' => "col-sm-9", 'items' => ":LokasiAsal" ],
					    ],
					  ],
					  [ 'class' => "form-group col-md-24",
					    'items' => [
						    [ 'class' => "col-sm-13" ],
						    [ 'class' => "col-sm-2", 'items' => ":LabelLokasiTujuan" ],
						    [ 'class' => "col-sm-9", 'items' => ":LokasiTujuan" ],
					    ],
					  ],
				  ]
				],
				[ 'class' => "row col-md-24", 'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>' ],
				[ 'class' => "col-md-24",
				  'items' => [
					  [ 'class' => "col-md-24", 'style' => "margin-top:5px;",
					    'items' => [
						    [ 'class' => "form-group col-md-2", 'items' => ":LabelMemo" ],
						    [ 'class' => "form-group col-md-10", 'items' => ":SDMemo" ],
						    [ 'class' => "form-group col-md-1" ],
						    [ 'class' => "form-group col-md-2", 'items' => ":LabelJmlUnit" ],
						    [ 'class' => "form-group col-md-2", 'items' => ":JmlUnit" ],
						    [ 'class' => "form-group col-md-1" ],
						    [ 'class' => "form-group col-md-2", 'items' => ":LabelSDTotal" ],
						    [ 'class' => "form-group col-md-4", 'items' => ":SDTotal" ],
					    ]
					  ],
				  ]
				],
			],
		],
		[ 'class' => "row-no-gutters",
		  'items' => [
              ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                  'items'  => [
                      ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":NoGL"],
                      ['class' => "col-sm-4 pull-right", 'items' => ":LabelNoGL"],
                  ],
              ],
              [
                  'class' => "col-md-12 pull-left",
                  'items' => $this->render( '../_nav', [
                      'url'=> $_GET['r'],
                      'options'=> [
                          'TtSD.SDNo'       => 'No Mutasi',
                          'TtSD.LokasiKode' => 'Kode Lokasi Asal',
                          'LokasiNama'      => 'Nama Lokasi Asal',
                          'TtSD.DealerKode' => 'Kode Dealer Tujuan',
                          'DealerNama'      => 'Nama Dealer Tujuan',
                          'TtSD.SDMemo'     => 'Keterangan',
                          'ttsd.UserID'     => 'User',
                          'ttsd.NoGL'       => 'No GL',
                      ],
                  ])
              ],
              ['class' => "col-md-12 pull-right",
			    'items' => [
				    [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
			    ]
			  ]
		  ]
		],
		[
			":LabelSDNo"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Surat Jalan</label>',
			":LabelNoGL"         => \common\components\General::labelGL( $dsBeli[ 'NoGL' ] ),
			":LabelPOS"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">POS Asal</label>',
			":LabelMemo"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
			":LabelJmlUnit"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jumlah Unit</label>',
			":LabelSDTotal"      => ( $jenis == 'MutasiEksternal' ) ? '' : '<label class="control-label"  style="margin: 0; padding: 6px 0;">Total</label>',
			":LabelLokasiAsal"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Lokasi Asal</label>',
			":LabelLokasiTujuan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Lokasi Tujuan</label>',
			":SDNo"              => Html::textInput( 'SDNoView', $dsBeli[ 'SDNoView' ], [ 'class' => 'form-control', 'readOnly' => true, 'tabindex' => '1' ] ) .
			                        Html::hiddenInput( 'SDNo', $dsBeli[ 'SDNo' ] ) .
			                        Html::textInput( 'StatPrint', ( $jenis == 'MutasiEksternal' ) ? '3' : '4', [ 'class' => 'hidden' ] ),
			":NoGL"              => Html::textInput( 'NoGL', $dsBeli[ 'NoGL' ], [ 'class' => 'form-control', 'readOnly' => true ] ),
			":SDTgl"             => FormField::dateInput( [ 'name' => 'SDTgl', 'config' => [ 'value' => $dsBeli[ 'SDTgl' ] ] ] ),
			":SDJam"             => FormField::timeInput( [ 'name' => 'SDJam', 'config' => [ 'value' => $dsBeli[ 'SDJam' ] ] ] ),
			":LokasiAsal"        => FormField::combo( 'LokasiKode', [ 'config' => [ 'id' => 'LokasiKode_id', 'value' => $dsBeli[ 'LokasiKode' ], 'data' => $DataLokasi ], 'options' => ['tabindex'=>'3'], ] ),
			":LokasiTujuan"      => FormField::combo( 'DealerKode', [ 'config' => [ 'id' => 'DealerKode_id', 'value' => $dsBeli[ 'DealerKode' ] ],'options' => ['tabindex'=>'4'], 'extraOptions' => [ 'altLabel' => [ 'DealerNama' ], ] ] ),
			":SDMemo"            => Html::textInput( 'SDMemo', $dsBeli[ 'SDMemo' ], [ 'class' => 'form-control', 'tabindex' => '5' ] ),
			":JmlUnit"           => FormField::integerInput( [ 'name' => 'JmlUnit', 'config' => [ 'value' => 0, 'readonly' => true ] ] ),
			":SDTotal"           => FormField::decimalInput( [ 'name' => 'SDTotal', 'config' => [ 'id' => 'SDTotal', 'value' => $dsBeli[ 'SDTotal' ], 'displayOptions' => [ 'class' => ( $jenis == 'MutasiEksternal' ? 'hidden' : '' ) ] ] ] ),

            ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
            ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
            ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
            ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
            ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
            ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
            ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),

		],
        [
            'url_main'      => 'ttsd',
            'url_delete'    => Url::toRoute(['ttsd/delete', 'id' => $_GET['id'] ?? ''] ),
        ]
	);
	ActiveForm::end();
	?>
</div>
<?php
$urlAdd    = Url::toRoute( [ 'ttsd/mutasi-eksternal-dealer-create', 'action' => 'create' ] );
$urlDetail = $url[ 'detail' ];
$urlPrint  = $url[ 'print' ];
$urlJurnal = Url::toRoute( [ 'ttsd/jurnal', 'StatPrint' => ( $jenis == 'MutasiEksternal' ) ? '3' : '4' ] );
$urlIndex  = Url::toRoute( [ 'ttsd/' . ( $isInv ? "invoice-eksternal-dealer" : "mutasi-eksternal-dealer" ) ] );
$hideHarga = ( $jenis == 'MutasiEksternal' ? 'true' : 'false' );
$readOnly  = ( $_GET[ 'action' ] == 'view' ? 'true' : 'false' );
$this->registerJsVar( '__model', $dsBeli );
$this->registerJs( <<< JS

    window.fillCombo = () => {
        $.ajax({
            type: 'POST',
            url: '$urlData',
            data: {
                 LokasiKode:  $('#LokasiKode_id').find(':selected')[0].value, 
                 SDTgl: $('input[name=\"SDTgl\"]').val(), 
                 SDJam: $('input[name=\"SDJam\"]').val(), 
                 SDNo: $('input[name=\"SDNo\"]').val()
            }
        }).then(function (cusData) {
            window.dataRows = cusData.rows;            
        });       
    }
    
    
    
    $('#LokasiKode_id').on('change',fillCombo);
    $('#LokasiKode_id').trigger('change');

     $('#detailGrid').utilJqGrid({ 
        editurl: '$urlDetail',
        height: 295,
        extraParams: {
            SDNo: $('input[name="SDNo"]').val()
        },  
        loadonce:true,
        rowNum: 1000,
        navButtonTambah: $hideHarga,
        navButtonHapus: $hideHarga,
        readOnly: $readOnly,         
        rownumbers: true,        
        colModel: [
            { template: 'actions' },
            {
                name: 'MotorType',
                label: 'Type',
                editable: true,
                width: 200,
                editor: {
                    type: $hideHarga ? 'select2' : 'text',
                    readOnly: !$hideHarga,    
                    data: [],
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowId = $(this).attr('rowid');
						    $('[id="'+rowId+'_'+'MotorTahun'+'"]').val(data.MotorTahun);
                            $('[id="'+rowId+'_'+'MotorNama'+'"]').val(data.MotorNama);
                            $('[id="'+rowId+'_'+'MotorAutoN'+'"]').val(data.MotorAutoN);
                            $('[id="'+rowId+'_'+'SDHarga'+'"]').val(data.FBHarga);
                            $('[id="'+rowId+'_'+'MotorNoMesin'+'"]').val(data.MotorNoMesin).trigger('change');
                            $('[id="'+rowId+'_'+'MotorNoRangka'+'"]').val(data.MotorNoRangka).trigger('change');
                            $('[id="'+rowId+'_'+'MotorWarna'+'"]').val(data.MotorWarna).trigger('change');
						}
                    },        
                }
            },
            {
                name: 'MotorNama',
                label: 'MotorNama',
                width: 250,
                hidden: !$hideHarga,
                editoptions:{
                    readOnly: true    
                },
                editable: true
            },
            {
                name: 'MotorTahun',
                label: 'Tahun',
                width: 60,                
                editoptions:{
                    readOnly: true    
                },
                editable: true
            },
            {
                name: 'MotorWarna',
                label: 'Warna',
                editable: true,
                width: 120,
                 editor: {
                    type: $hideHarga ? 'select2' : 'text',
                    readOnly: !$hideHarga,    
                    data: [],
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowId = $(this).attr('rowid');
						    $('[id="'+rowId+'_'+'MotorTahun'+'"]').val(data.MotorTahun);
                            $('[id="'+rowId+'_'+'MotorNama'+'"]').val(data.MotorNama);
                            $('[id="'+rowId+'_'+'MotorAutoN'+'"]').val(data.MotorAutoN);
                            $('[id="'+rowId+'_'+'SDHarga'+'"]').val(data.FBHarga);
                            $('[id="'+rowId+'_'+'MotorNoMesin'+'"]').val(data.MotorNoMesin).trigger('change');
                            $('[id="'+rowId+'_'+'MotorNoRangka'+'"]').val(data.MotorNoRangka).trigger('change');
                            $('[id="'+rowId+'_'+'MotorType'+'"]').val(data.MotorType).trigger('change');
						}
                    },        
                }
            },
            {
                name: 'MotorNoMesin',
                label: 'NoMesin',
                width: 125,
                editable: true,
                 editor: {
                    type: $hideHarga ? 'select2' : 'text',
                    readOnly: !$hideHarga,    
                    data: [],
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowId = $(this).attr('rowid');
						    $('[id="'+rowId+'_'+'MotorTahun'+'"]').val(data.MotorTahun);
                            $('[id="'+rowId+'_'+'MotorNama'+'"]').val(data.MotorNama);
                            $('[id="'+rowId+'_'+'MotorAutoN'+'"]').val(data.MotorAutoN);
                            $('[id="'+rowId+'_'+'SDHarga'+'"]').val(data.FBHarga);
                            $('[id="'+rowId+'_'+'MotorType'+'"]').val(data.MotorType).trigger('change');
                            $('[id="'+rowId+'_'+'MotorNoRangka'+'"]').val(data.MotorNoRangka).trigger('change');
                            $('[id="'+rowId+'_'+'MotorWarna'+'"]').val(data.MotorWarna).trigger('change');
						}
                    },        
                }
            },
            {
                name: 'MotorAutoN',
                label: 'NoMesin',
                width: 125,
                hidden: true ,
                editable: true
            },            
            {
                name: 'MotorNoRangka',
                label: 'NoRangka',
                width: 170,
                editable: true,
                 editor: {
                    type: $hideHarga ? 'select2' : 'text',
                    readOnly: !$hideHarga,    
                    data: [],
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowId = $(this).attr('rowid');
						    $('[id="'+rowId+'_'+'MotorTahun'+'"]').val(data.MotorTahun);
                            $('[id="'+rowId+'_'+'MotorNama'+'"]').val(data.MotorNama);
                            $('[id="'+rowId+'_'+'MotorAutoN'+'"]').val(data.MotorAutoN);
                            $('[id="'+rowId+'_'+'SDHarga'+'"]').val(data.FBHarga);
                            $('[id="'+rowId+'_'+'MotorNoMesin'+'"]').val(data.MotorNoMesin).trigger('change');
                            $('[id="'+rowId+'_'+'MotorType'+'"]').val(data.MotorType).trigger('change');
                            $('[id="'+rowId+'_'+'MotorWarna'+'"]').val(data.MotorWarna).trigger('change');
						}
                    },        
                }
            },
            {
                name: 'SDHarga',
                label: 'Harga',
                template: 'money',
                width: 250,
                hidden: $hideHarga ,
                editable: true
            },
        ],
        listeners: {
            afterLoad: function(grid, response) {
                let SDTotal = $('#detailGrid').jqGrid('getCol','SDHarga',false,'sum');
                $('input[name="SDTotal"]').utilNumberControl().val(SDTotal);
                let jQty = $('#detailGrid').jqGrid('getGridParam', 'records');
                $('input[name="JmlUnit"]').utilNumberControl().val(jQty);
                let __memo = __model.SDMemo.replace(/~( )*Jumlah( )*Unit( )*:( )*\d*/g,'');
                $('input[name="SDMemo"]').val(__memo + ' ~ Jumlah Unit :  '+ jQty);    
                if (jQty > 0){
                    $("#LokasiKode_id").attr('disabled', true);   
                    $("#form_ttsd_id").append("<input id='LokasiKodeHidden' name='LokasiKode' type='hidden' value='"+$("#LokasiKode_id").val()+"'>");
                }else{
                    $("#LokasiKode_id").attr('disabled', false);
                    $("#LokasiKodeHidden").remove();
                }
                if (!$hideHarga) $(".ui-inline-del").hide();
            },   
        }
    }).init().fit($('.box-body')).load({url: '$urlDetail'});
    $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {        
        var motorType = $('[id="'+rowid+'_'+'MotorType'+'"]');
        if(motorType[0].type != 'text'){
            motorType.select2('destroy').empty().select2({
            data: $.map(window.dataRows, function (obj) {
                      obj.id = obj.MotorType;
                      obj.text = obj.MotorType;
                      return obj;
                    })
            });
        }
        let motorWarna = $('[id="'+rowid+'_'+'MotorWarna'+'"]');
        if(motorWarna[0].type != 'text'){
            motorWarna.select2('destroy').empty().select2({
            data: $.map(window.dataRows, function (obj) {
                      obj.id = obj.MotorWarna;
                      obj.text = obj.MotorWarna;
                      return obj;
                    })
            });    
        }
        let motorNoMesin = $('[id="'+rowid+'_'+'MotorNoMesin'+'"]');
        if(motorNoMesin[0].type != 'text'){
            motorNoMesin.select2('destroy').empty().select2({
            data: $.map(window.dataRows, function (obj) {
                      obj.id = obj.MotorNoMesin;
                      obj.text = obj.MotorNoMesin;
                      return obj;
                    })
            });
        }
        let motorNoRangka = $('[id="'+rowid+'_'+'MotorNoRangka'+'"]');
        if(motorNoRangka[0].type != 'text'){
            motorNoRangka.select2('destroy').empty().select2({
            data: $.map(window.dataRows, function (obj) {
                      obj.id = obj.MotorNoRangka;
                      obj.text = obj.MotorNoRangka;
                      return obj;
                    })
            });
        }
        if (!rowid.includes('new_row')){
               var d = $('#detailGrid').utilJqGrid().getData(rowid);   
               console.log(d);
               $('[id="'+rowid+'_'+'MotorNoMesin'+'"]').val(d.data.MotorNoMesin).trigger('change');
               $('[id="'+rowid+'_'+'MotorTahun'+'"]').val(d.data.MotorTahun);
               $('[id="'+rowid+'_'+'MotorNama'+'"]').val(d.data.MotorNama);
               $('[id="'+rowid+'_'+'MotorAutoN'+'"]').val(d.data.MotorAutoN);
               $('[id="'+rowid+'_'+'SDHarga'+'"]').val(d.data.SDHarga);
               $('[id="'+rowid+'_'+'MotorType'+'"]').val(d.data.MotorType).trigger('change');
               $('[id="'+rowid+'_'+'MotorNoRangka'+'"]').val(d.data.MotorNoRangka).trigger('change');
               $('[id="'+rowid+'_'+'MotorWarna'+'"]').val(d.data.MotorWarna).trigger('change');
        }else{
            if (motorType[0].type != 'text' && motorType.select2('data').length > 0){
                var data = motorType.select2('data')[0];
                motorType.val(data.MotorType).trigger('change').trigger({
                    type: 'select2:select',
                    params: {
                        data: data
                    }
                });
            }
        }
        let __memo = __model.SDMemo.replace(/~( )*Jumlah( )*Unit( )*:( )*\d*/g,'');
        $('input[name="SDMemo"]').val(__memo + ' ~ Jumlah Unit :  '+ $('input[name="JmlUnit"]').utilNumberControl().val());        
    });
    $('#btnSave').click(function (event) {
        $('#form_ttsd_id').attr('action','{$url['update']}');
        $('#form_ttsd_id').attr('target','');
        $('input[name="detil"]').val(JSON.stringify($('#detailGrid').getRowData()));
        $('#form_ttsd_id').submit();
    });  
    $('#btnPrint').click(function (event) {	      
        $('#form_ttsd_id').attr('action','$urlPrint');
        $('#form_ttsd_id').attr('target','_blank');
        $('#form_ttsd_id').submit();
	  }); 
    
    $('#btnJurnal').click(function (event) {	
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#form_ttsd_id').attr('action','{$urlJurnal}');
                    $('#form_ttsd_id').submit();                     
                }
            }
        });
    });   
   
     $('#btnCancel').click(function (event) {	      
       $('#form_ttsd_id').attr('action','{$url['cancel']}');
        $('#form_ttsd_id').attr('target','');
       $('#form_ttsd_id').submit();
    });
     
     $('#btnAdd').click(function (event) {	      
        $('#form_ttsd_id').attr('action','{$urlAdd}');
        $('#form_ttsd_id').attr('target','');
        $('#form_ttsd_id').submit();
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  }); 
    
    $('[name=SDTgl]').utilDateControl().cmp().attr('tabindex', '2');
    $('[name=JmlUnit]').utilNumberControl().cmp().attr('tabindex', '6');
   
    
JS
);
?>
