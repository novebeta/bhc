<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttmmarketshare */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ttmmarketshare-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Tanggal')->textInput() ?>

    <?= $form->field($model, 'Provinsi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kabupaten')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kecamatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Honda')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Semua')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
