<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttmmarketshare */

$this->title = 'Create Ttmmarketshare';
$this->params['breadcrumbs'][] = ['label' => 'Ttmmarketshares', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttmmarketshare-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
