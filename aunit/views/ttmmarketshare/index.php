<?php
use aunit\assets\AppAsset;
use aunit\models\Ttmmarketshare;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
AppAsset::register( $this );
$this->title                     = 'Market Share';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'Kabupaten'  => 'Kabupaten',
		'Kecamatan' => 'Kecamatan'
	],
	'cmbTgl'    => [
		'Tanggal' => 'Tanggal',
	],
	'cmbNum'    => [
		'Honda' => 'Honda',
		'Semua' => 'Semua'
	],
	'sortname'  => "Tanggal",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttmmarketshare::className(),
	'Market Share', [
//		'mode' => isset( $mode ) ? $mode : '',
//		'url_delete'    => Url::toRoute( [ 'ttsk/delete'] ),
	] ), \yii\web\View::POS_READY );