<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttmmarketshare */

$this->title = 'Update Ttmmarketshare: ' . $model->Tanggal;
$this->params['breadcrumbs'][] = ['label' => 'Ttmmarketshares', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Tanggal, 'url' => ['view', 'Tanggal' => $model->Tanggal, 'Provinsi' => $model->Provinsi, 'Kabupaten' => $model->Kabupaten, 'Kecamatan' => $model->Kecamatan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ttmmarketshare-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
