<?php

use aunit\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Stok Opname';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'LokasiKode' => 'LokasiKode',
		'CFMemo' => 'CFMemo',
		'SalesKode' => 'SalesKode',
		'UserID' => 'UserID',
		'CFNo'   => 'CFNo',
	],
	'cmbTgl'    => [
		'CFTgl' => 'CFTgl'
	],
	'cmbNum'    => [
		'StockBuku'=>'StockBuku',
		'StockFisik'=>'StockFisik',
		'NilaiBuku'=>'NilaiBuku',
		'NilaiFisik'=>'NilaiFisik',
	],
	'sortname'  => "CFNo",
	'sortorder' => 'desc',
];
$this->registerJsVar('setcmb', $arr);
?>
<div class="panel panel-default">
	<div class="panel-body">
		<?= $this->render('../_search'); ?>
		<table id="jqGrid"></table>
		<div id="jqGridPager"></div>
	</div>
</div>
<?php
$this->registerJs(\aunit\components\TdUi::mainGridJs(
	\aunit\models\Ttcfhd::class,
	'Cek Fisik Stock Opname',
	[
		'url_delete'    => Url::toRoute(['ttcfhd/delete']),
	]
), \yii\web\View::POS_READY);
