<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttcfhd */

$cancel = Url::toRoute( [ 'ttcfhd/cancel','id'=>$id, 'action' => 'create' ] );
$this->title                     = 'Tambah Stok Opname';
?>
<div class="ttcfhd-create">
	<?= $this->render( '_form', [
	    'id'=>$id,
        'model' => $model,
        'dsJual' => $dsJual,
        'url'   => [
            'update' => Url::toRoute( [ 'ttcfhd/update','id'=>$id, 'action' => 'create' ] ),
			'print'  => Url::toRoute( [ 'ttcfhd/print', 'action' => 'print' ] ),
			'cancel' => $cancel,
            'detail' => Url::toRoute( 'ttcfit/index' )
        ]
	] ) ?>
</div>
