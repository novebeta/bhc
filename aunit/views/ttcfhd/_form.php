<?php
use aunit\components\FormField;
use common\components\Custom;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;

$this->registerJsFile("https://unpkg.com/@zxing/library@0.18.6/umd/index.min.js", ['position' => \yii\web\View::POS_HEAD]);
\aunit\assets\AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttcfhd */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
var selectedDeviceId;
const codeReader = new ZXing.BrowserMultiFormatReader();
function formatttss(result) {
    return '<div class="row" style="margin-left: 2px">' +
           '<div class="col-xs-10" style="text-align: left">' + result.id + '</div>' +
           '<div class="col-xs-14">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
$urlAdd = Url::toRoute( [ 'ttcfhd/create','action' => 'create'  ] );
$urlIndex = Url::toRoute( [ 'ttcfhd/index'  ] );
$urlPrint = Url::toRoute( [ 'ttcfhd/print'  ] );
?>
<div class="ttcfhd-form">
    <?php
    $form = ActiveForm::begin(['id' => 'form_ttcfhd_id', 'action' => $url['update']]);
    \aunit\components\TUi::form(
        [
            'class' => "row-no-gutters",
            'items' => [
                [
                    'class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-1", 'items' => ":LabelNoCF"],
                        ['class' => "col-sm-3", 'items' => ":NoCF"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-1", 'items' => ":LabelTglCF"],
                        ['class' => "col-sm-3", 'items' => ":TglCF"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-1", 'items' => ":LabelJamCF"],
                        ['class' => "col-sm-3", 'items' => ":JamCF"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2", 'items' => ":LabelLokasi"],
                        ['class' => "col-sm-7", 'items' => ":Lokasi"],
                    ],
                ],
                [
                    'class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-1", 'items' => ":LabelMemoCF"],
                        ['class' => "col-sm-13", 'items' => ":MemoCF"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2", 'items' => ":LabelSales"],
                        ['class' => "col-sm-7", 'items' => ":SalesKode"],
                    ],
                ],
                [
                    'class' => "row col-md-24",
                    'style' => "margin-bottom: 6px;",
                    'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
                ],
            ]
        ],
        [
            'class' => "row-no-gutters",
            'items' => [
                [
                    'class' => "col-md-12 pull-left",
                    'items' => $this->render('../_nav', [
                        'url' => $_GET['r'],
                        'options' => [
                            'CFNo'   => 'CFNo',
                            'CFMeno' => 'Keterangan',
                            'LokasiKodeku' => 'Lokasi Kode',
                            'SalesKode' => 'Sales Kode',
                            'UserID' => 'UserID',
                            'CFTgl' => 'CFTgl',
                            'StockBuku' => 'Stock Buku',
                            'StockFisik' => 'Stock Fisik',
                            'NilaiBuku' => 'Nilai Buku',
                            'NilaiFisik' => 'Nilai Fisik',
                        ],
                    ])
                ],
                ['class' => "pull-right", 'items' => ":btnPrint :btnAction"],
            ]
        ],
        [
            /* label */
            ":LabelNoCF"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">No CF</label>',
            ":LabelTglCF"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl CF</label>',
            ":LabelJamCF"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jam CF</label>',
            ":LabelLokasi"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Lokasi</label>',
            ":LabelMemoCF" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Memo</label>',
            ":LabelSales" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Sales</label>',
            /* component */
            ":NoCF"        => Html::textInput('CFNoView', $dsJual['CFNoView'], ['class' => 'form-control', 'maxlength' => 18, 'tabindex' => '1']) .
                Html::textInput('CFNo', $dsJual['CFNo'], ['class' => 'hidden']),
            ":MemoCF"      => Html::textInput('CFMemo', $dsJual['CFMemo'], ['class' => 'form-control', 'maxlength' => 100, 'tabindex' => '3']),
            ":TglCF"       => FormField::dateInput(['name' => 'CFTgl', 'config' => ['value' => General::asDate( $dsJual['CFTgl'])]]),
            ":JamCF"           => FormField::timeInput(['name' => 'JamCF', 'config' => ['value' => $dsJual['CFTgl']]]),
            ":Lokasi"          => FormField::combo('LokasiKode', ['config' => ['value' => $dsJual['LokasiKode']], 'extraOptions' => ['simpleCombo' => true]]),
            ":SalesKode"      => FormField::combo('SalesKode', ['config' => ['id' => 'cboSalesKode', 'value' => 'SalesNama','value' => $dsJual['SalesKode']], 'extraOptions' => ['altLabel' => ['SalesNama']]]),
            /* button */

            ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
            ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
            ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
            ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
            ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
            ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
        ],
        [
            // '_akses' => 'Pengajuan Berkas',
            'url_main' => 'ttcfhd',
            'url_id' => $_GET['id'] ?? '',
            'url_delete'    => Url::toRoute(['ttcfhd/delete', 'id' => $_GET['id'] ?? '']),
        ]
    );
    ActiveForm::end();
    ?>
</div>
<div class="modal" id="barcodeScanner" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 330px;">
            <div class="modal-body">
                <div>
                    <video id="video" width="300" height="200" style="border: 1px solid gray"></video>
                    <canvas id="canvas" width="300" height="200" style="display: none"></canvas>
                </div>

                <div class="col-md-24" id="sourceSelectPanel" style="margin-bottom: 2px;display:none">
                    <label class="col-md-6" for="sourceSelect">Source</label>
                    <select class="col-md-18" id="sourceSelect"></select>
                </div>

                <div class="col-md-24">
                    <label class="col-md-6">No Mesin</label><input id="barcodeScannerMotorNoMesin" class="col-md-18" style="margin-bottom: 2px;" type="text" value="">
                </div>
                <div class="col-md-24">
                    <label class="col-md-6">No SPK</label><input id="barcodeScannerSPKNo" class="col-md-18" style="margin-bottom: 6px;" type="text" value="">
                </div>

                <code id="result" style="display: none;"></code>
            </div>
            <div class="modal-footer">
                <button type="button" id="captureButton" class="btn btn-primary pull-left">Capture</button>
                <button type="button" id="resetButton" class="btn btn-primary pull-right">Reset</button>
                <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php
$urlDetail     = $url['detail'];
$this->registerJs(
    <<< JS
    $('#detailGrid').utilJqGrid({
        editurl: '$urlDetail',
        height: 220,
        extraParams: {
            CFNo: $('input[name="CFNo"]').val()
        },
        rownumbers: true,
        loadonce:true,
        rowNum: 1000,
        // readOnly: true,
        rownumbers: true,
        navButtonTambah: false,
        navButtonEdit: false,
        colModel: [
            { template: 'actions'  },
            {
                name: 'MotorAutoN',
                label: 'Auto N',
                editable: true,
                width: 75,
                template: 'number'
            },
            {
                name: 'MotorNoMesin',
                label: 'No Mesin',
                editable: true,
                width: 100,
            },
            {
                name: 'MotorType',
                label: 'Type Motor',
                editable: true,
                width: 100,
            },
            {
                name: 'MotorWarna',
                label: 'Warna',
                editable: true,
                width: 100,
            },
            {
                name: 'MotorNama',
                label: 'Nama Motor',
                editable: true,
                width: 100,
            },
            {
                name: 'MotorKategori',
                label: 'Kategori Motor',
                editable: true,
                width: 100,
            },
            {
                name: 'FBHarga',
                label: 'FBHarga',
                width: 120,
                editable: true,
                template: 'money',
                hidden: true
            }
        ],
        listeners: {
            afterLoad: function (data) {
            //  window.HitungTotal();
            }
        },
        // onSelectRow : function(rowid,status,e){
        //     if (window.rowId !== -1) return;
        //     if (window.rowId_selrow === rowid) return;
        //     if(rowid.includes('new_row') === true){
        //         $('[name=KodeBarang]').val('--');
        //         $('[name=LokasiKodeTxt]').val('Kosong');
        //         $('[name=BarangNama]').val('--');
        //         $('[name=lblStock]').val(0);
        //         return ;
        //     };
        //     let r = $('#detailGrid').utilJqGrid().getData(rowid).data;
        //     checkStok(r.BrgKode,r.BrgNama);
        //     window.rowId_selrow = rowid;
        // }
    }).init().fit($('.box-body')).load({url: '$urlDetail'});

    function startScanner(){
        codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
            if (result) {
                $('#barcodeScanner').modal('hide');
                codeReader.reset();
                document.getElementById('result').textContent = result.text;
                data = {
                    oper:'add',
                    id:'new_row',
                    CFNo:$('input[name="CFNo"]').val(),
                    MotorAutoN:0,
                    MotorNoMesin: result.text
                    // MotorNoMesin:'JM04E2075024'
                }
                $.ajax({
                    type: "POST",
                    url: '$urlDetail',
                    data: data,
                    type: 'POST',
                    success: function(response) {
                        $.notify({message: response.message},{
                            type: (response.success) ? 'success' : 'warning',
                        });
                        $('#detailGrid').jqGrid('setGridParam',{postData: {CFNo: $('input[name="CFNo"]').val()}}).trigger('reloadGrid');
                    },
                });
            }
            if (err && !(err instanceof ZXing.NotFoundException)) {
                console.error(err)
            }
        });
    }
    
    $('#detailGrid').jqGrid('navButtonAdd',"#detailGrid-pager",{ 
        caption: ' Tambah',
        buttonicon: 'glyphicon glyphicon-plus',
        onClickButton:  function() {
            startScanner();
            $('#barcodeScannerMotorNoMesin').val('');
            $('#barcodeScannerSPKNo').val('');
            $('#barcodeScanner').modal('show');
        },
        position: "last", 
        title:"", 
        cursor: "pointer"
    });


JS
);

$this->registerJs(<<<JS
    codeReader.listVideoInputDevices()
        .then((videoInputDevices) => {
            const sourceSelect = document.getElementById('sourceSelect')
            selectedDeviceId = videoInputDevices[0].deviceId
            if (videoInputDevices.length >= 1) {
                videoInputDevices.forEach((element) => {
                    const sourceOption = document.createElement('option')
                    sourceOption.text = element.label
                    sourceOption.value = element.deviceId
                    sourceSelect.appendChild(sourceOption)
                })

                sourceSelect.onchange = () => {
                    selectedDeviceId = sourceSelect.value;
                    startScanner();
                };

                const sourceSelectPanel = document.getElementById('sourceSelectPanel')
                sourceSelectPanel.style.display = 'block'
            }

            document.getElementById('resetButton').addEventListener('click', () => {
                startScanner();
            })
        })
        .catch((err) => {
            console.error(err)
        });

    $('#barcodeScanner').on('hidden.bs.modal', function (e) {
        codeReader.reset();
    });

    $('#captureButton').on('click', function (e) {
        var MotorNoMesin = $('#barcodeScannerMotorNoMesin').val();
        var SPKNo = $('#barcodeScannerSPKNo').val();
        if(MotorNoMesin === '' || SPKNo === ''){
            bootbox.alert("No Mesin dan SPK harus diisi");
            return '';
        }
        var canvas = document.getElementById('canvas');
        var video = document.getElementById('video');
        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;
        canvas.getContext('2d').drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
        const dataURL = canvas.toDataURL();
        // console.log(dataURL);
        $('#barcodeScanner').modal('hide');
        $.ajax({
            type: "POST",
            url: '$urlDetail',
            data: {
                oper:'add',
                id:'new_row',
                CFNo:$('input[name="CFNo"]').val(),
                MotorAutoN:0,
                MotorNoMesin: MotorNoMesin,
                SPKNo: SPKNo,
                imgBase64: dataURL,
            },
            type: 'POST',
            success: function(response) {
                $.notify({message: response.message},{
                    type: (response.success) ? 'success' : 'warning',
                });
                $('#detailGrid').jqGrid('setGridParam',{postData: {CFNo: $('input[name="CFNo"]').val()}}).trigger('reloadGrid');
            },
        });
    });

    $('#btnCancel').click(function (event) {
        $('#form_ttcfhd_id').attr('action','{$url['cancel']}');
        $('#form_ttcfhd_id').utilForm().submit();
    });

    $('#btnSave').click(function (event) {
        $('#form_ttcfhd_id').attr('action','{$url['update']}');
        $('#form_ttcfhd_id').utilForm().submit('{$url['update']}','POST','_self');
    });

    $('#btnAdd').click(function (event) {
        window.location.href = '{$urlAdd}';
    });

    $('#btnEdit').click(function (event) {
        window.location.href = '{$url['update']}';
    });

    $('#btnDaftar').click(function (event) {
        window.location.href = '{$urlIndex}';
    });

    $('#btnPrint').click(function (event) {	      
        $('#form_ttcfhd_id').attr('action','$urlPrint');
        $('#form_ttcfhd_id').attr('target','_blank');
        $('#form_ttcfhd_id').submit();
    }); 

JS
, \yii\web\View::POS_READY);