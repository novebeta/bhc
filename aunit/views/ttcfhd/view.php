<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttcfhd */

$this->title = $model->CFNo;
$this->params['breadcrumbs'][] = ['label' => 'Ttcfhds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ttcfhd-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->CFNo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->CFNo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'CFNo',
            'CFTgl',
            'CFMemo',
            'LokasiKode',
            'SalesKode',
            'StockBuku',
            'StockFisik',
            'NilaiBuku',
            'NilaiFisik',
            'UserID',
        ],
    ]) ?>

</div>
