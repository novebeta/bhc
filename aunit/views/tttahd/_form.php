<?php
use aunit\components\FormField;
use common\components\General;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\aunit\assets\AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Tttahd */
/* @var $form yii\widgets\ActiveForm */
$format = <<< SCRIPT
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
?>
    <div class="tttahd-form">
		<?php
		$form = ActiveForm::begin( [ 'id' => 'form_tttahd_id', 'action' => $url[ 'update' ] ] );
		\aunit\components\TUi::form(
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-14",
					      'items' => [
						      [ 'class' => "col-sm-3", 'items' => ":LabelTANo" ],
						      [ 'class' => "col-sm-5", 'items' => ":TANo" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelTATgl" ],
						      [ 'class' => "col-sm-4", 'items' => ":TATgl" ],
						      [ 'class' => "col-sm-1" ],
						      [ 'class' => "col-sm-1", 'items' => ":LabelTAJam" ],
						      [ 'class' => "col-sm-4", 'items' => ":TAJam", 'style' => 'padding-left:3px;' ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-10",
					      'items' => [
						      [ 'class' => "col-sm-4" ],
						      [ 'class' => "col-sm-2", 'items' => ":LabelTC" ],
						      [ 'class' => "col-sm-18", 'items' => ":TC" ],
					      ],
					    ],
				    ],
				  ],
				  [
					  'class' => "row col-md-24",
					  'style' => "margin-bottom: 6px;",
					  'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
				  ],
				  [ 'class' => "form-group col-md-24",
				    'items' => [
					    [ 'class' => "form-group col-md-18",
					      'items' => [
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-4", 'items' => ":KodeBarang" ],
							        [ 'class' => "col-sm-10", 'items' => ":BarangNama", 'style' => 'padding-left:3px;' ],
							        [ 'class' => "col-sm-4", 'items' => ":Gudang", 'style' => 'padding-left:3px;' ],
							        [ 'class' => "col-sm-4", 'items' => ":Satuan", 'style' => 'padding-left:3px;' ],
							        [ 'class' => "col-sm-2", 'items' => ":BtnInfo" ],
						        ],
						      ],
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "form-group col-md-23",
							          'items' => [
								          [ 'class' => "col-sm-3", 'items' => ":LabelKeterangan" ],
								          [ 'class' => "col-sm-21", 'items' => ":Keterangan" ],
							          ],
							        ],
						        ],
						      ],
					      ],
					    ],
					    [ 'class' => "form-group col-md-6",
					      'items' => [
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-6", 'items' => ":LabelSubTotal" ],
							        [ 'class' => "col-sm-16", 'items' => ":SubTotal" ],
						        ],
						      ],
						      [ 'class' => "form-group col-md-24",
						        'items' => [
							        [ 'class' => "col-sm-1" ],
							        [ 'class' => "col-sm-6", 'items' => ":LabelCetak" ],
							        [ 'class' => "col-sm-16", 'items' => ":Cetak" ],
						        ],
						      ],
					      ],
					    ],
				    ],
				  ],
			  ],
			],
			[ 'class' => "row-no-gutters",
			  'items' => [
				  [ 'class' => "form-group col-md-14",
				    'items' => [
					    [ 'class' => "col-sm-1", 'items' => ":LabelJT" ],
					    [ 'class' => "col-sm-4", 'items' => ":JT" ],
				    ],
				  ],
				  [ 'class' => "col-md-10",
				    'items' => [
					    [ 'class' => "pull-right", 'items' => ":btnPrint :btnAction" ]
				    ]
				  ]
			  ]
			],
			[
				":LabelTANo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nomor TA</label>',
				":LabelTATgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tanggal</label>',
				":LabelTAJam"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jam</label>',
				":LabelTC"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">TC</label>',
				":LabelSubTotal"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Total</label>',
				":LabelKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelCetak"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Cetak</label>',
				":LabelJT"         => \common\components\General::labelGL( $dsTMutasi[ 'NoGL' ] ),
				":TANo"            => Html::textInput( 'TANoView', $dsTMutasi[ 'TANoView' ], [ 'class' => 'form-control', 'readonly' => 'readonly', 'tabindex' => '1' ] ) .
				                      Html::textInput( 'TANo', $dsTMutasi[ 'TANo' ], [ 'class' => 'hidden' ] ),
				":TATgl"           => FormField::dateInput( [ 'name' => 'TATgl', 'config' => [ 'value' => General::asDate( $dsTMutasi[ 'TATgl' ] ) ] ] ),
				":TAJam"           => FormField::timeInput( [ 'name' => 'TAJam', 'config' => [ 'value' => $dsTMutasi[ 'TATgl' ] ] ] ),
				":TC"              => FormField::combo( 'KodeTrans', [ 'config' => [ 'id' => 'KodeTrans', 'value' => $dsTMutasi[ 'KodeTrans' ], 'data' => [
					'TC04' => 'TC04 - Penerimaan akibat selisih stock opname (plus)',
					'TC16' => 'TC16 - Pengeluaran akibat selisih stock opname (minus)',
				] ],'options' => ['tabindex'=>'3'], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":JT"              => Html::textInput( 'NoGL', $dsTMutasi[ 'NoGL' ], [ 'class' => 'form-control', 'readOnly' => true ] ),
				":Cetak"           => Html::textInput( 'Cetak', $dsTMutasi[ 'Cetak' ], [ 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":Keterangan"      => Html::textInput( 'TAKeterangan', $dsTMutasi[ 'TAKeterangan' ], [ 'class' => 'form-control', 'maxlength' => '150', 'tabindex' => '6' ] ),
				":SubTotal"        => FormField::decimalInput( [ 'name' => 'TATotal', 'config' => [ 'id' => 'TATotal', 'value' => $dsTMutasi[ 'TATotal' ] ] ] ),
				":KodeBarang"      => Html::textInput( 'KodeBarang', '', [ 'id' => 'KodeBarang', 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":BarangNama"      => Html::textInput( 'BarangNama', '', [ 'id' => 'BarangNama', 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":Gudang"          => Html::textInput( 'LokasiKodeTxt', '', [ 'id' => 'LokasiKodeTxt', 'class' => 'form-control', 'readonly' => 'readonly' ] ),
				":Satuan"          => Html::textInput( 'lblStock', 0, [ 'type' => 'number', 'class' => 'form-control', 'style' => 'color: blue; font-weight: bold;', 'readonly' => 'readonly' ] ),
				":BtnInfo"         => '<button type="button" class="btn btn-default" tabindex="4"><i class="fa fa-info-circle fa-lg"></i></button>',
			], [
				'url_main' => 'tttahd',
				'url_id'   => $id,
			]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlCheckStock = Url::toRoute( [ 'site/check-stock' ] );
$urlDetail     = $url[ 'detail' ];
$urlPrint      = $url[ 'print' ];
$readOnly      = ( $_GET[ 'action' ] == 'view' ? 'true' : 'false' );
$urlData       = \yii\helpers\Url::toRoute( [ 'tttahd/fill-barang-add-edit' ] );
$Lokasi        = \common\components\General::cCmd( "SELECT * FROM tdLokasi Order By LokasiStatus, LokasiNomor" )->queryAll();
$this->registerJsVar( '__Lokasi', json_encode( $Lokasi ) );
$this->registerJs( <<< JS
   
     var __LokasiKode = JSON.parse(__Lokasi);
     __LokasiKode = $.map(__LokasiKode, function (obj) {
                      obj.id = obj.LokasiKode;
                      obj.text = obj.LokasiKode;
                      return obj;
                    });
      function HitungTotal(){
         	let TATotal = $('#detailGrid').jqGrid('getCol','Jumlah',false,'sum');
         	$('#TATotal').utilNumberControl().val(TATotal);
         	HitungBawah();
         }
         window.HitungTotal = HitungTotal;
         function HitungBawah(){
         }         
         window.HitungBawah = HitungBawah;
         function checkStok(BrgKode,BarangNama,LokasiAsal){
           $.ajax({
                url: '$urlCheckStock',
                type: "POST",
                data: { 
                    BrgKode: BrgKode,
                    LokasiKode: LokasiAsal
                },
                success: function (res) {
                    $('[name=LokasiKode]').val(res.LokasiKode);
                    $('#KodeBarang').val(BrgKode);
                    $('[name=BarangNama]').val(BarangNama);
                    $('[name=lblStock]').val(res.SaldoQtyReal);
                },
                error: function (jXHR, textStatus, errorThrown) {
                    
                },
                async: false
            });
      }
      window.checkStok = checkStok;
     $('#detailGrid').utilJqGrid({
     editurl: '$urlDetail',
        height: 255,
        extraParams: {
            TANo: $('input[name="TANo"]').val()
        },
        rownumbers: true,  
        loadonce:true,
        rowNum: 1000,
        readOnly: $readOnly,            
        colModel: [
            { template: 'actions'  },
            {
                name: 'BrgKode',
                label: 'Kode',
                editable: true,
                width: 155,
                editor: {
                    type: 'select2',
                    data: [],
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'TAHrgBeli'+'"]').utilNumberControl().val(data.parseFloat(BrgHrgBeli));
                            $('[id="'+rowid+'_'+'BrgRak1'+'"]').val(data.BrgRak1);
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').val(data.BrgNama); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgNama'+'"]').trigger('change');
                            window.hitungLine();
						},
						'change': function(){      
                            let rowid = $(this).attr('rowid');
                            checkStok($(this).val(),$('[id="'+rowid+'_'+'BrgNama'+'"]').val(),$('[id="'+rowid+'_'+'LokasiKode'+'"]').val());
						}
                    }                 
                }
            },       
            {
                name: 'BrgNama',
                label: 'Nama Barang',
                width: 200,
                editable: true,
                editor: {
                    type: 'select2',
                    data: [],
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    console.log(data);
						    let rowid = $(this).attr('rowid');
						    $('[id="'+rowid+'_'+'BrgSatuan'+'"]').val(data.BrgSatuan);
                            $('[id="'+rowid+'_'+'TAHrgBeli'+'"]').utilNumberControl().val(parseFloat(data.BrgHrgBeli));
                            $('[id="'+rowid+'_'+'BrgRak1'+'"]').val(data.BrgRak1);
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').val(data.BrgKode); // Select the option with a value of '1'
                            $('[id="'+rowid+'_'+'BrgKode'+'"]').trigger('change');
                            window.hitungLine();
						}
                    }              
                }
            },
            {
                name: 'BrgRak1',
                label: 'BIN',
                width: 100,
                editable: true,
            },
            {
                name: 'LokasiKode',
                label: 'Lokasi',
                width: 100,
                editable: true,
                 editor: {
                    type: 'select2',
                    data: __LokasiKode,
                    events:{
                        'select2:select': function (e) {
						    var data = e.params.data;
						    console.log(data);
						    let rowid = $(this).attr('rowid');
						    checkStok($('[id="'+rowid+'_'+'BrgKode'+'"]').val(),$('[id="'+rowid+'_'+'BrgNama'+'"]').val(),$(this).val());
						}
                    }              
                }
            },
            {
                name: 'TAQty',
                label: 'Qty',
                width: 80,
                editable: true,
                template: 'number',
            },        
            {
                name: 'BrgSatuan',
                label: 'Satuan',
                width: 100,
                editable: true,
            },
            {
                name: 'TAHrgBeli',
                label: 'Harga Part',
                width: 100,
                editable: true,
                template: 'money'
            },
            {
                name: 'Jumlah',
                label: 'Jumlah',
                width: 110,
                editable: true,
                template: 'money'
            },  
            {
                name: 'TAAuto',
                label: 'TAAuto',
                editable: true,
                hidden: true
            },                  
        ],  
        listeners: {
            afterLoad: function (data) {
                let jQty = $('#detailGrid').jqGrid('getGridParam', 'records');
                if (jQty > 0){
                    $("#KodeTrans").attr('disabled', true);
                    $("#form_tttahd_id").append("<input id='KodeTransHidden' name='KodeTrans' type='hidden' value='"+$("#KodeTrans").val()+"'>");
                }else{
                    $("#KodeTrans").attr('disabled', false);
                    $("#KodeTransHidden").remove();
                }
                window.HitungTotal();
            }    
        },  
        
        onSelectRow : function(rowid,status,e){ 
            if (window.rowId !== -1) return;
            if (window.rowId_selrow === rowid) return;
            if(rowid.includes('new_row') === true){
                $('[name=KodeBarang]').val('--');
                $('[name=LokasiKode]').val('Kosong');
                $('[name=BarangNama]').val('--');
                $('[name=lblStock]').val(0);
                return ;
            };
            let r = $('#detailGrid').utilJqGrid().getData(rowid).data;
            checkStok(r.BrgKode,r.BrgNama,r.LokasiKode);
            window.rowId_selrow = rowid;            
        }                  
     }).init().fit($('.box-body')).load({url: '$urlDetail'}); 
     window.rowId_selrow = -1;
     function hitungLine(){
            let TAHrgBeli = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'TAHrgBeli'+'"]').val()));
            let TAQty = parseFloat(window.util.number.getValueOfEuropeanNumber($('[id="'+window.cmp.rowid+'_'+'TAQty'+'"]').val()));
            $('[id="'+window.cmp.rowid+'_'+'Jumlah'+'"]').inputmask('setvalue',TAHrgBeli * TAQty);
            window.HitungTotal();
     }
     window.hitungLine = hitungLine;
   
     $('#detailGrid').bind("jqGridInlineEditRow", function (e, rowid, orgClickEvent) {
         window.cmp.rowid = rowid;    
         $.ajax({
            type: 'POST',
            url: '$urlData',
            data: {
                 KodeTrans:  $('#KodeTrans').find(':selected')[0].value, 
                 TANo: $('input[name="TANo"]').val()
            }
        }).then(function (cusData) {
            let dataRows = cusData;
             var __BrgKode = JSON.parse(dataRows);
             __BrgKode = $.map(__BrgKode, function (obj) {
                              obj.id = obj.BrgKode;
                              obj.text = obj.BrgKode;
                              return obj;
                            });
             var __BrgNama = JSON.parse(dataRows);
             __BrgNama = $.map(__BrgNama, function (obj) {
              obj.id = obj.BrgNama;
              obj.text = obj.BrgNama;
              return obj;
            });
            $('[id="'+rowid+'_'+'BrgKode'+'"]').select2('destroy').empty().select2({
            data: __BrgKode
            });
            $('[id="'+rowid+'_'+'BrgNama'+'"]').select2('destroy').empty().select2({
            data: __BrgNama
            });
           
           if(rowid.includes('new_row')){
              var BrgKode = $('[id="'+rowid+'_'+'BrgKode'+'"]');
              var dtBrgKode = __BrgKode;
              if (dtBrgKode.length > 0){
                  var dt = dtBrgKode[0];
                  BrgKode.val(dt.BrgKode).trigger('change').trigger({
                    type: 'select2:select',
                    params: {
                        data: dt
                    }
                });
              }
            $('[id="'+rowid+'_'+'TAQty'+'"]').val(0);
          }else{
               var d = $('#detailGrid').utilJqGrid().getData(rowid); 
               $('[id="'+rowid+'_'+'BrgKode'+'"]').val(d.data.BrgKode).trigger('change');
               $('[id="'+rowid+'_'+'BrgNama'+'"]').val(d.data.BrgNama).trigger('change');
          }
        });         
         
          $('[id="'+window.cmp.rowid+'_'+'TAQty'+'"]').on('keyup', function (event){
              hitungLine();
          });
          $('[id="'+window.cmp.rowid+'_'+'TAHrgBeli'+'"]').on('keyup', function (event){
              hitungLine();
          });
         hitungLine();
	 });
	
	$('#btnSave').click(function (event) {	
	    let Nominal = parseFloat($('#TATotal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Total tidak boleh 0', size: 'small'});
               return;
           }  
        $('input[name="SaveMode"]').val('ALL');
        $('#form_tttahd_id').attr('action','{$url['update']}');
        $('#form_tttahd_id').submit();
    });  
	
	$('[name=TATgl]').utilDateControl().cmp().attr('tabindex', '2');
	$('[name=TATotal]').utilNumberControl().cmp().attr('tabindex', '5');
JS
);



