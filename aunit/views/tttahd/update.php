<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tttahd */
$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = 'Edit - Penyesuaian Stock: ' . $dsTMutasi['TANoView'];
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Penyesuaian Stock', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = 'Edit';
?>
<div class="tttahd-update">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTMutasi' => $dsTMutasi,
        'id'     => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail'       => Url::toRoute( [ 'tttait/index','action' => 'create' ] ),
        ]
    ]) ?>

</div>
