<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tttahd */

$params                          = '&id=' . $id . '&action=create';
$cancel                          = Custom::url(\Yii::$app->controller->id.'/cancel'.$params );
$this->title                     = 'Tambah - Penyesuaian Stock';
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Penyesuaian Stock', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = $this->title;

?>
<div class="tttahd-create">
    <?= $this->render('_form', [
        'model' => $model,
        'dsTMutasi' => $dsTMutasi,
        'id'     => $id,
        'url'    => [
			'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
			'cancel' => $cancel,
            'detail'       => Url::toRoute( [ 'tttait/index','action' => 'create' ] ),
		]
    ]) ?>

</div>
