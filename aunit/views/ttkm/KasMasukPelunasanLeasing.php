<?php
use aunit\assets\AppAsset;
use common\components\Custom;
use aunit\models\Ttkm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'KAS MASUK - Pelunasan Leasing';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
    'cmbTxt'    => [
        'KMPerson'   => 'Penyetor',
        'KMNo'       => 'No Kas Masuk',
        'KMJenis'    => 'Jenis',
        'KMLink'     => 'No Transaksi',
        'KMMemo'     => 'Keterangan',
        'UserID'     => 'User',
        'NoGL'       => 'No GL',

    ],
    'cmbTgl'    => [
        'KMTgl'      => 'Tanggal Kas Masuk',
    ],
    'cmbNum'    => [
        'KMNominal'  => 'Nominal',
    ],
    'sortname'  => "KMTgl",
    'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttkm::className(),
	'Kas Masuk Pelunasan Leasing', [
        'url_add'    => \yii\helpers\Url::toRoute( [ 'ttkm/kas-masuk-pelunasan-leasing-create', 'action' => 'create' ] ),
        'url_update' => \yii\helpers\Url::toRoute( ['ttkm/kas-masuk-pelunasan-leasing-update', 'action' => 'update' ] ),
        'url_delete' => \yii\helpers\Url::toRoute( ['ttkm/delete' ] ),
    ] ), \yii\web\View::POS_READY );
