<?php

use common\components\Custom;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttkm */

$params                          = '&id=' . $id . '&action=create';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = "Tambah - $title";
//$this->params[ 'breadcrumbs' ][] = [ 'label' => $title, 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = $this->title;

?>
<div class="ttkm-create">

    <?= $this->render('_form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'JoinData' => $JoinData,
        'id'     => $id,
        'url' => [
            'update'    => Custom::url(\Yii::$app->controller->id."/$view-update".$params ),
            'print'     => Custom::url(\Yii::$app->controller->id."/$view" ),
            'cancel'    => $cancel,
        ]
    ]) ?>

</div>
