<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\components\Custom;

/* @var $this yii\web\View */
/* @var $model abengkel\models\Ttkm */
$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode).'- Kas Masuk Umum : ' . $dsTUang['KMNoView'];
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Kas Masuk Umum', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = 'Edit';

//$this->title = str_replace('Menu','',\aunit\components\TUi::$actionMode).'Kas Masuk Umum : ' . $model->KMNo;
//$this->params['breadcrumbs'][] = ['label' => 'Kas Masuk Umum', 'url' => ['index']];
//$this->params['breadcrumbs'][] = 'Edit';

?>
<div class="ttkm-update">
    <?= $this->render('_kas-masuk-umum-form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'JoinData' => $JoinData,
        'id'     => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/kas-masuk-umum-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail'       => Url::toRoute( [ 'ttkmitcoa/index','action' => 'create' ] ),
        ]
//        'url'    => [
//            'update'       => Url::toRoute( [ \Yii::$app->controller->id . '/update', 'id' => $id, 'action' => 'update' ] ),
//            'print'        => Url::toRoute( [ \Yii::$app->controller->id . '/print', 'id' => $id, 'action' => 'update' ] ),
//            'cancel'       => Url::toRoute( [ \Yii::$app->controller->id . '/cancel', 'id' => $id, 'action' => 'update' ] ),
//            'detail'       => Url::toRoute( [ 'ttkmitcoa/index','action' => 'create' ] ),
//        ]
    ]) ?>

</div>
