<?php

use aunit\models\Ttkm;
use aunit\assets\AppAsset;
use aunit\models\Ttkk;
use common\components\Custom;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$urlCreateAcc     = Url::toRoute( [ 'ttkk/kas-keluar-acc-create' ] );
$this->title = 'Kas Masuk WA';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
    'cmbTxt'    => [
        'KMNo'              => 'No Kas Masuk',
        'KMJenis'           => 'Jenis Kas Masuk',
        'CusTelepon'        => 'Customer Telepon',
        'KMPerson'          => 'Penerima',
        'KMLink'          => 'KM Link',
        'MotorNama'          => 'Nama Motor',
        'MotorWarna'          => 'Warna Motor',
        'KMMemo'            => 'Keterangan',
        'Konsumen'            => 'Konsumen',
        'KMStatus'            => 'Status',

    ],
    'cmbTgl'    => [
        'KMTgl'      => 'Tanggal Kas Masuk',
    ],
    'cmbNum'    => [
        'KMNominal'  => 'Nominal',
    ],
    'sortname'  => "KKTgl",
    'sortorder' => 'desc'
];

AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
$btnSend = Html::button('<i class="fa fa-envelope"> Kirim</i>   ', ['class' => 'btn bg-purple ' . Custom::viewClass(), 'id' => 'btnKirim', 'style' => 'width:80px;height:30px']);
$extendHMTL = $btnSend;
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search',['extendHMTL'=>$extendHMTL] ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs(<<< JS
window.gridParam_jqGrid = {
    multiselect: true,
}
JS
);
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttkm::class,
	'Kas Masuk WA' , [
        'colGrid'    => Ttkm::colGridKMWA(),
        'url'        => \yii\helpers\Url::toRoute( [ 'ttkm/kas-masuk-wa-index' ]),
        // 'url_update' => Url::toRoute( [ 'ttkk/kas-keluar-umum-update','action' => 'update','source'=>'KasKeluarAcc' ] ),
        'btnView_Disabled'=> true,
        'btnDelete_Disabled'=> true,
    ] ), \yii\web\View::POS_READY );
