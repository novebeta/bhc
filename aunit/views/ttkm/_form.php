<?php
use abengkel\models\Tdlokasi;
use aunit\assets\AppAsset;
use aunit\components\FormField;
use aunit\components\TUi;
use aunit\models\Ttkm;
use common\components\Custom;
use common\components\General;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttkm */
/* @var $form yii\widgets\ActiveForm */
?>
    <div class="ttkm-form">
		<?php
		$LokasiKodeCmb = ArrayHelper::map( Tdlokasi::find()->select( [ 'LokasiKode' ] )
		                                           ->orderBy( 'LokasiStatus, LokasiKode' )
		                                           ->asArray()->all(), 'LokasiKode', 'LokasiKode' );
		$form          = ActiveForm::begin( [ 'id' => 'form_ttkm_id', 'action' => $url[ 'update' ] ] );
		TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[ 'class' => "form-group col-md-24",
					  'items' => [
						  [ 'class' => "col-sm-2", 'items' => ":LabelKMNo" ],
						  [ 'class' => "col-sm-5", 'items' => ":NoKM" ],
						  [ 'class' => "col-sm-2" ],
						  [ 'class' => "col-sm-1", 'items' => ":LabelKMTgl" ],
						  [ 'class' => "col-sm-3", 'items' => ":KMTgl" ],
						  [ 'class' => "col-sm-3", 'items' => ":KMJam" ],
						  [ 'class' => "col-sm-1" ],
						  [ 'class' => "col-sm-3", 'items' => ":LabelNominalKM" ],
						  [ 'class' => "col-sm-4", 'items' => ":KMNominal" ],
					  ],
					],
					[ 'class' => "form-group col-md-16",
					  'items' => [
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-3", 'items' => ":LabelCusNama" ],
							    [ 'class' => "col-sm-9", 'items' => ":CusNama" ],
							    [ 'class' => "col-sm-6", 'style' => 'padding-left:3px', 'items' => ":KMLink" ],
							    [ 'class' => "col-sm-6", 'items' => ":CariTanggal" ],
						    ],
						  ],
						  [ 'class' => "form-group col-md-24",
						    'items' => [
							    [ 'class' => "col-sm-3" ],
							    [ 'class' => "col-sm-21", 'items' => ":CusAlamat" ],
							    [ 'class' => "col-sm-3" ],
							    [ 'class' => "col-sm-21", 'style' => 'padding-top:3px', 'items' => ":txtMotor" ],
							    [ 'class' => "col-sm-3", 'items' => ":LabelKMMemo" ],
							    [ 'class' => "col-sm-21", 'style' => 'padding-top:3px', 'items' => ":KMMemo" ],
						    ],
						  ],
					  ],
					],
					[ 'class' => "col-sm-1" ],
					[ 'class' => "form-group col-md-7",
					  'items' => [
						  [ 'class' => "col-sm-24", 'items' => ":LabelKMPerson" ],
						  [ 'class' => "col-sm-24", 'items' => ":KMPerson" ],
						  [ 'class' => "col-sm-6", 'style' => 'padding-top:3px', 'items' => ":LabelLokasiKode" ],
						  [ 'class' => "col-sm-18", 'style' => 'padding-top:3px', 'items' => ":LokasiKode" ],
						  [ 'class' => "col-sm-6", 'style' => 'padding-top:3px', 'items' => ":LabelLease" ],
						  [ 'class' => "col-sm-18", 'style' => 'padding-top:3px', 'items' => ":Lease" ],
						  [ 'class' => "col-sm-6", 'style' => 'padding-top:3px', 'items' => ":LabelKMJenis" ],
						  [ 'class' => "col-sm-18", 'style' => 'padding-top:3px', 'items' => ":KMJenis" ],
					  ],
					],
				]
			],
			[
				'class' => "row-no-gutters",
				'items' => [
                    ['class' => "form-group", 'style' => 'position: absolute;top: -35px;right:0px;',
                        'items'  => [
                            ['class' => "col-sm-13 pull-right",'style' => 'color:white', 'items' => ":NoGL"],
                            ['class' => "col-sm-4 pull-right", 'items' => ":LabelNoGL"],
                        ],
                    ],
                    [
                        'class' => "col-md-12 pull-left",
                        'items' => $this->render( '../_nav', [
                            'url'=> $_GET['r'],
                            'options'=> [
                                'KMPerson'   => 'Penyetor',
                                'KMNo'       => 'No Kas Masuk',
                                'KMJenis'    => 'Jenis',
                                'KMLink'     => 'No Transaksi',
                                'KMMemo'     => 'Keterangan',
                                'UserID'     => 'User',
                                'NoGL'       => 'No GL',
                            ],
                        ])
                    ],
                    ['class' => "col-md-12 pull-right",
					  'items' => [
						  [ 'class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction" ]
					  ]
					]
				]
			],
			[
				/* label */
				":LabelKMNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No KM</label>',
				":LabelKMTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tgl KM</label>',
				":LabelNominalKM"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
				":LabelCusNama"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Konsumen</label>',
				":LabelKMMemo"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
				":LabelKMPerson"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Terima Dari / Diserahkan Oleh</label>',
				":LabelLokasiKode" => '<label class="control-label" style="margin: 0; padding: 6px 0;">POS</label>',
				":LabelLease"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Lease</label>',
				":LabelKMJenis"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
				":LabelNoGL"       => General::labelGL( $dsTUang[ 'NoGL' ] ),
				":NoKM"            => Html::textInput( 'KMNoView', $dsTUang[ 'KMNoView' ], [ 'class' => 'form-control', 'readonly' => true, 'tabindex' => '1' ] ) .
				                      Html::textInput( 'KMNo', $dsTUang[ 'KMNo' ], [ 'class' => 'hidden' ] )
									  .Html::textInput( 'StatPrint', 1, [ 'class' => 'hidden statPrint' ] ),
				":KMTgl"           => FormField::dateInput( [ 'name' => 'KMTgl', 'config' => [ 'value' => General::asDate( $dsTUang[ 'KMTgl' ] ) ] ] ),
				":KMJam"           => FormField::timeInput( [ 'name' => 'KMJam', 'config' => [ 'value' => $dsTUang[ 'KMJam' ] ] ] ),
				":KMNominal"       => FormField::decimalInput( [ 'name' => 'KMNominal', 'config' => [ 'value' => $dsTUang[ 'KMNominal' ] ] ] ),
				":CusNama"         => Html::textInput( 'CusNama', $dsTUang[ 'CusNama' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":KMLink"          => Html::textInput( 'KMLink', $dsTUang[ 'KMLink' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":KMMemo"          => Html::textInput( 'KMMemo', $dsTUang[ 'KMMemo' ], [ 'class' => 'form-control', 'tabindex' => '5' ] ),
				":NoGL"            => Html::textInput( 'NoGL', $dsTUang[ 'NoGL' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":KMJenis"         => Html::textInput( 'KMJenis', $dsTUang[ 'KMJenis' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":LokasiKode"      => FormField::combo( 'LokasiKode', [ 'config' => [ 'value'                                                                                       => $dsTUang[ 'LokasiKode' ], 'data' => $LokasiKodeCmb,
				                                                                      'pluginEvents'                                                                                => [
					                                                                      "change" => "function() {
                                                            $('#LokasiKodeTxt').val(this.value);
                                                            }", ], 'disabled' => true ], 'extraOptions' => [ 'simpleCombo' => true ] ] ),
				":KMPerson"        => Html::textarea( 'KMPerson', $dsTUang[ 'KMPerson' ], [ 'class' => 'form-control', 'tabindex' => '4' ] ),
				":CusAlamat"       => Html::textarea( 'CusAlamat', $JoinData[ 'CusAlamat' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":txtMotor"        => Html::textarea( 'MotorType', $JoinData[ 'MotorType' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":Lease"           => Html::textInput( 'LeaseKode', $JoinData[ 'LeaseKode' ], [ 'class' => 'form-control', 'readonly' => true ] ),
				":CariTanggal"     => '<div class="input-group">' . FormField::dateInput( [ 'name' => 'dp_1', 'config' => [ 'value' => General::asDate( $JoinData[ 'KMLinkTgl' ] ), 'readonly' => true ] ] ) . '
                                    <span class="input-group-btn"><button id="BtnSearch" class="btn btn-default" type="button"><i class="glyphicon glyphicon-search" readonly></i></button></span></div>',

                ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
                ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
                ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
                ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
                ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
                ":btnPrint"       => Html::button('<i class="glyphicon glyphicon-print"><br><br>Print</i>   ', ['class' => 'btn btn-warning ', 'id' => 'btnPrint', 'style' => 'width:60px;height:60px']),
                ":btnJurnal"      => Html::button('<i class="fa fa-balance-scale"><br><br>Jurnal</i>   ', ['class' => 'btn bg-maroon ' . Custom::viewClass(), 'id' => 'btnJurnal', 'style' => 'width:60px;height:60px']),
			],
            [
                'url_main'         => 'ttkm',
                'url_delete'        => Url::toRoute(['ttkm/delete', 'id' => $_GET['id'] ?? ''] ),
            ]
		);
		ActiveForm::end();
		?>
    </div>
<?php
$urlPrint        = Url::toRoute( [ 'ttkm/print', 'id' => $id ] );
$url[ 'jurnal' ] = \yii\helpers\Url::toRoute( [ 'ttkm/jurnal' ] );
$urlLoopDK       = Url::toRoute( [ 'ttkm/loop-kas-masuk' ] );
$urlAdd          = Ttkm::getRoute( $dsTUang[ 'KMJenis' ], [ 'action' => 'create' ] )[ 'create' ];
$urlIndex        = Ttkm::getRoute( $dsTUang[ 'KMJenis' ], [ 'id' => $id ] )[ 'index' ];
$msg             = 'Silahkah Pilih Data Referensi terlebih dahulu';
$this->registerJsVar( 'KMJenis', $dsTUang[ 'KMJenis' ] );
switch ( $dsTUang[ 'KMJenis' ] ) {
	case 'Inden':
		$urlDatKonSelect = Url::toRoute( [ 'ttin/select-km-inden', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ), 'KMNo' => $dsTUang[ 'KMNo' ], 'KMLink' => $dsTUang[ 'KMLink' ] ] );
		$KMLink          = 'INNo';
		$msg             = "Silahkah Pilih Data Bank Keluar Terlebih Dahulu";
		break;
	case 'Leasing':
		$urlDatKonSelect = Url::toRoute( [ 'ttdk/select-km-order-dk', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ), 'KMNo' => $dsTUang[ 'KMNo' ], 'KMLink' => $dsTUang[ 'KMLink' ], 'jenis' => 'Leasing' ] );
		$KMLink          = 'DKNo';
		$msg             = "Silahkah Pilih Data Konsumen Piutang Leasing Terlebih Dahulu";
		break;
	case 'Kredit':
		$urlDatKonSelect = Url::toRoute( [ 'ttdk/select-km-order-dk', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ), 'KMNo' => $dsTUang[ 'KMNo' ], 'KMLink' => $dsTUang[ 'KMLink' ], 'jenis' => 'Kredit', 'jenispotongan' => 'KreditTanpaPotongan' ] );
		$KMLink          = 'DKNo';
		$msg             = "Silahkah Pilih Data Konsumen Penjualan Kredit Terlebih Dahulu";
		break;
	case 'Tunai':
		$urlDatKonSelect = Url::toRoute( [ 'ttdk/select-km-order-dk', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ), 'KMNo' => $dsTUang[ 'KMNo' ], 'KMLink' => $dsTUang[ 'KMLink' ], 'jenis' => 'Tunai', 'jenispotongan' => 'TunaiTanpaPotongan' ] );
		$KMLink          = 'DKNo';
		$msg             = "Silahkah Pilih Data Konsumen Penjualan Tunai Terlebih Dahulu";
		break;
	case 'Piutang UM':
		$urlDatKonSelect = Url::toRoute( [ 'ttdk/select-km-order-dk', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ), 'KMNo' => $dsTUang[ 'KMNo' ], 'KMLink' => $dsTUang[ 'KMLink' ], 'jenis' => 'PiutangUM' ] );
		$KMLink          = 'DKNo';
		$msg             = "Silahkah Pilih Data Konsumen Piutang Uang Muka Terlebih Dahulu";
		break;
	case 'Piutang Sisa':
		$urlDatKonSelect = Url::toRoute( [ 'ttdk/select-km-order-dk', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ), 'KMNo' => $dsTUang[ 'KMNo' ], 'KMLink' => $dsTUang[ 'KMLink' ], 'jenis' => 'PiutangSisa' ] );
		$KMLink          = 'DKNo';
		$msg             = "Silahkah Pilih Data Konsumen Piutang Konsumen Kredit Terlebih Dahulu";
		break;
	case 'Angsuran Kredit':
		$urlDatKonSelect = Url::toRoute( [ 'ttdk/select-km-order-dk', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ), 'KMNo' => $dsTUang[ 'KMNo' ], 'KMLink' => $dsTUang[ 'KMLink' ], 'jenis' => 'AngsuranKredit' ] );
		$KMLink          = 'DKNo';
		$msg             = "Silahkah Pilih Data Angsuran Kredit Terlebih Dahulu";
		break;
	case 'BBN Plus':
		$urlDatKonSelect = Url::toRoute( [ 'ttdk/select-km-order-dk', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ), 'KMNo' => $dsTUang[ 'KMNo' ], 'KMLink' => $dsTUang[ 'KMLink' ], 'jenis' => 'BBNPlus' ] );
		$KMLink          = 'DKNo';
		$msg             = "Silahkah Pilih Data BBN Plus Terlebih Dahulu";
		break;
	case 'BBN Acc':
		$urlDatKonSelect = Url::toRoute( [ 'ttdk/select-km-order-dk', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ), 'KMNo' => $dsTUang[ 'KMNo' ], 'KMLink' => $dsTUang[ 'KMLink' ], 'jenis' => 'BBNAcc' ] );
		$KMLink          = 'DKNo';
		$msg             = "Silahkah Pilih Data BBN Acc Terlebih Dahulu";
		break;
	case 'KM Dari Bank':
		$urlDatKonSelect = Url::toRoute( [ 'ttbkhd/select-km-dari-bank', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ), 'KMNo' => $dsTUang[ 'KMNo' ], 'KMLink' => $dsTUang[ 'KMLink' ], 'jenis' => 'KMDariBank' ] );
		$KMLink          = 'BKNo';
		$msg             = "Silahkah Pilih Data Bank Keluar Terlebih Dahulu";
		break;
	case 'KM Dari Bengkel':
		$urlDatKonSelect = Url::toRoute( [ 'ttkk/select-km-dari-bengkel', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ), 'KMNo' => $dsTUang[ 'KMNo' ], 'KMLink' => $dsTUang[ 'KMLink' ], 'jenis' => 'KMDariBengkel' ] );
		$KMLink          = 'KKNo';
		$urlLoopDK       = Url::toRoute( [ 'ttkk/loop-kas-masuk-dari-bengkel' ] );
		$msg             = "Silahkah Pilih Data Kas Keluar Bengkel/H2 Terlebih Dahulu";
		break;
	case 'Angsuran Karyawan':
		$urlDatKonSelect = Url::toRoute( [ 'tdsales/select', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ), 'KMNo' => $dsTUang[ 'KMNo' ], 'KMLink' => $dsTUang[ 'KMLink' ], 'jenis' => 'AngsuranKaryawan' ] );
		$KMLink          = 'SalesKode';
		$msg             = "Silahkah Pilih Data Karyawan Terlebih Dahulu";
		break;
	case 'KM Pos Dari Dealer':
	case 'KM Dealer Dari Pos':
		$urlDatKonSelect = Url::toRoute( [ 'ttkk/select-km-order-kk', 'tgl1' => Yii::$app->formatter->asDate( '-90 day', 'yyyy-MM-dd' ), 'KMNo' => $dsTUang[ 'KMNo' ], 'KMLink' => $dsTUang[ 'KMLink' ], 'jenis' => str_replace( ' ', '', $dsTUang[ 'KMJenis' ] ) ] );
		$KMLink          = 'KKNo';
		$msg             = ( $dsTUang[ 'KMJenis' ] == 'KM Pos Dari Dealer' ) ? "Silahkah Pilih Data Kas Keluar Dealer Terlebih Dahulu" : "Silahkah Pilih Data Kas Keluar Pos Terlebih Dahulu";
		break;
}
$this->registerJs( <<< JS

     $('#BtnSearch').click(function () {
        // if (!__isCreate) return;
        window.util.app.dialogListData.show({
                title: 'Daftar Data Kas Masuk',
                url: '$urlDatKonSelect'
            },
            function (data) {
                console.log(data);
                if (data.data === undefined) return;                
                $('[name=KMNominal]').utilNumberControl().val(data.data.Sisa);
                $('[name=KMLink]').val(data.data.{$KMLink});
                if (KMJenis === 'Inden'){                    
                    $('[name=KMPerson]').val(data.data.CusNama);
                }                
                $.ajax({
                    type: 'POST',
                    url: '$urlLoopDK',
                    data: $('#form_ttkm_id').serialize()
                }).then(function (cusData) { // console.log(data);
                    window.location.href = "{$url['update']}&oper=skip-load&KMNoView={$dsTUang['KMNoView']}"; 
                });
            })
    });

    $('#btnPrint').click(async function (event) {
		$('#form_ttkm_id').attr('action','$urlPrint');
        $('#form_ttkm_id').attr('target','_blank');
		// if($.inArray(KMJenis,['Inden','Leasing','Kredit','Tunai','Piutang UM','Piutang Sisa','BBN Plus','BBN Acc']) != -1){
		if($.inArray(KMJenis,['Inden','Piutang UM','Piutang Sisa','Kredit','Tunai']) != -1){
			for (let i = 1; i < 3; i++) {
				$('.statPrint').val(i);
				$('#form_ttkm_id').submit();
				await sleep(500);
			}
		}else{
			$('#form_ttkm_id').submit();
		}
	  });
    
    $('#btnJurnal').click(function (event) {	
        bootbox.confirm({
            title: 'Konfirmasi',
            message: "Apakah ingin menjurnal ulang transaksi ini ?",
            size: "small",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Ok',
                    className: 'btn btn-warning'
                },
                cancel: {
                    label: '<span class="fa fa-ban"></span> Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if (result) {
                    $('#form_ttkm_id').attr('action','{$url['jurnal']}');
                    $('#form_ttkm_id').submit();                     
                }
            }
        });
    }); 

     $('#btnCancel').click(function (event) {	      
       $('#form_ttkm_id').attr('action','{$url['cancel']}');
       $('#form_ttkm_id').submit();
    });
     
     
     $('#KMTotal-disp').blur(function() {
        $('#KMNominal').utilNumberControl().val($('#KMTotal').utilNumberControl().val());
      }); 
     
       $('#btnSave').click(function (event) {
            let KMLink = $('[name="KMLink"]').val();
           if (KMLink === '' || KMLink === '--' ){
               bootbox.alert({message:'{$msg}', size: 'small'});
               return;
           }
           let Nominal = parseFloat($('#KMNominal-disp').inputmask('unmaskedvalue'));
           if (Nominal === 0 ){
               bootbox.alert({message:'Silahkan mengisi nominal', size: 'small'});
               return;
           }
        $('#form_ttkm_id').attr('action','{$url['update']}');
        $('#form_ttkm_id').submit();
    }); 
       
        $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });    
    
    $('[name=KMTgl]').utilDateControl().cmp().attr('tabindex', '2');
    $('[name=KMNominal]').utilNumberControl().cmp().attr('tabindex', '3');
    
JS
);
