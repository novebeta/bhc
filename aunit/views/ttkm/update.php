<?php

use common\components\Custom;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttkm */
$params                          = '&id=' . $id . '&action=update';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = str_replace('Menu','',\aunit\components\TUi::$actionMode)." - $title : "  . $dsTUang['KMNoView'];
//$this->params[ 'breadcrumbs' ][] = [ 'label' => $title, 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = 'Edit';

?>
<div class="ttkm-update">

    <?= $this->render('_form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'JoinData' => $JoinData,
        'id'     => $id,
        'url' => [
            'update'    => Custom::url(\Yii::$app->controller->id."/$view-update".$params ),
            'print'     => Custom::url(\Yii::$app->controller->id."/$view".$params ),
            'cancel'    => $cancel
//            'cancel'    => Custom::url(\Yii::$app->controller->id."/$view" ),

        ]
    ]) ?>

</div>
