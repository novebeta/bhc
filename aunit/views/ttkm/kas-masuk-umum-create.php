<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttkm */

$params                          = '&id=' . $id . '&action=create';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = 'Tambah - Kas Masuk Umum';
//$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Kas Masuk Umum', 'url' => $cancel ];
//$this->params[ 'breadcrumbs' ][] = $this->title;

?>
<div class="ttkm-create">

    <?= $this->render('_kas-masuk-umum-form', [
        'model' => $model,
        'dsTUang' => $dsTUang,
        'JoinData' => $JoinData,
        'id'     => $id,
        'url'    => [
            'update' => Custom::url( \Yii::$app->controller->id . '/kas-masuk-umum-update' . $params ),
            'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
            'cancel' => $cancel,
            'detail'       => Url::toRoute( [ 'ttkmitcoa/index','action' => 'create' ] ),
        ]
    ]) ?>

</div>
