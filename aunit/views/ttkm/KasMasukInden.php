<?php
use aunit\assets\AppAsset;
use common\components\Custom;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'KAS MASUK - UM Inden';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
			'KMPerson'   => 'Penyetor',
            'KMNo'       => 'No Kas Masuk',
            'KMJenis'    => 'Jenis',
			'KMLink'     => 'No Transaksi',
            'KMMemo'     => 'Keterangan',
            'UserID'     => 'User',
			'NoGL'       => 'No GL',

	],
	'cmbTgl'    => [
			'KMTgl'      => 'Tanggal Kas Masuk',
	],
	'cmbNum'    => [
			'KMNominal'  => 'Nominal',
	],
	'sortname'  => "KMTgl",
	'sortorder' => 'desc'
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttkm::className(), 'Kas Masuk Inden' , [
    'url_update' => Url::toRoute( [ 'ttkm/kas-masuk-inden-update' ,'action' => 'update'] ),
    'url_add'    => Url::toRoute( [ 'ttkm/kas-masuk-inden-create','action' => 'create' ] ),
    'url_delete' => Url::toRoute( [ 'ttkm/delete' ] ),
//    'url_add'    => Custom::url( 'ttkm/kas-masuk-inden-create' ),
//        'url_update' => Custom::url( 'ttkm/kas-masuk-inden-update' ),
//        'url_delete' => Custom::url( 'ttkm/td' ),
    ] ), \yii\web\View::POS_READY );
