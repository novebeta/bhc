<?php

use common\components\Custom;

/* @var $this yii\web\View */
/* @var $model aunit\models\Tsaldomotor */

$this->title = 'Tambah - Saldo Awal Motor';
$this->params['breadcrumbs'][] = ['label' => 'Saldo Awal Motor', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$params = '&id='.$id.'&action=create';
?>
<div class="tsaldomotor-create">

    <?= $this->render('_form', [
        'model' => $model,
        'url' => [
            'update'   => Custom::url(\Yii::$app->controller->id.'/update'.$params ),
            'print'  => Custom::url(\Yii::$app->controller->id.'/print'.$params ),
            'cancel' => Custom::url(\Yii::$app->controller->id.'/cancel'.$params ),
            'detail' => Custom::url(\Yii::$app->controller->id.'/detail' ),

            'tdmotorwarna' => Custom::url('tdmotorwarna/find' ),
        ]
    ]) ?>

</div>
