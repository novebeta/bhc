<?php

use common\components\Custom;

/* @var $this yii\web\View */
/* @var $model aunit\models\Tsaldomotor */

$this->title = str_replace('Menu','',\aunit\components\TUi::$actionMode).'- Saldo Awal Motor: ' . $model->NoTrans;
$this->params['breadcrumbs'][] = ['label' => 'Saldo Awal Motor', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NoTrans, 'url' => ['view', 'NoTrans' => $model->NoTrans, 'TglTrans' => $model->TglTrans, 'MotorAutoN' => $model->MotorAutoN, 'MotorNoMesin' => $model->MotorNoMesin]];
$this->params['breadcrumbs'][] = 'Edit';

$params = '&id='.$id.'&action=update';
?>
<div class="tsaldomotor-update">

    <?= $this->render('_form', [
        'model' => $model,
        'url' => [
            'update'   => Custom::url(\Yii::$app->controller->id.'/update'.$params ),
            'print'  => Custom::url(\Yii::$app->controller->id.'/print'.$params ),
            'cancel' => Custom::url(\Yii::$app->controller->id.'/cancel'.$params ),
            'detail' => Custom::url(\Yii::$app->controller->id.'/detail' ),
            'tdmotorwarna' => Custom::url('tdmotorwarna/find' ),
        ]
    ]) ?>

</div>
