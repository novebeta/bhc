<?php

use kartik\datecontrol\DateControl;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model aunit\models\Tsaldomotor */
/* @var $form yii\widgets\ActiveForm */

\aunit\assets\AppAsset::register($this);
$url['cancel'] = Url::toRoute('tsaldomotor/index');
?>

<div class="tsaldomotor-form">

<?php
    $form = ActiveForm::begin(['id' => 'frm_tsaldomotor_id']);
    \aunit\components\TUi::form(
        [
            'class' => "row-no-gutters",
            'items' => [
                [
                    'class' => "form-inline col-md-24",
                    'items' => [
                        [
                            'style' => "margin-bottom: 6px;",
                            'items' => ":NoTrans :TglTrans"
                        ]
                    ]
                ],
                [
                    'class' => "row col-md-24",
                    'style' => "margin-bottom: 6px;",
                    'items' => '<table id="detailGrid"></table><div id="detailGridPager"></div>'
                ],
                [
                    'class' => "form-inline pull-right",
                    'items' => ":JumlahUnit :Total"
                ]
            ]
        ],
        [ 'class' => "row-no-gutters",
            'items' => [
                [
                    'class' => "col-md-12 pull-left",
                    'items' => $this->render( '../_nav', [
                        'url'=> $_GET['r'],
                        'options'=> [
                            'NoTrans'       => 'Nomor'
                        ],
                    ])
                ],
                [
                    'class' => "pull-right",
                    'items' => ":btnSave :btnPrint :btnCancel"
                ],
            ]
        ],

        [
            ":NoTrans" => $form->field($model, 'NoTrans', ['template' => '{label}{input}'])
                ->textInput(['maxlength' => true, 'placeholder' => 'No Jurnal', 'readonly'=>''])
                ->label('Nomor', ['style' => 'margin: 0; padding: 6px 0; width: 60px;']),
            ":TglTrans" => $form->field($model, 'TglTrans', ['template' => '{label}{input}'])
                ->widget(DateControl::className(), [
                    'type'          => DateControl::FORMAT_DATE,
                    'pluginOptions' => [ 'autoclose' => true ]
                ])->label('Tanggal', ['style' => 'margin: 0; padding: 6px 0; width: 85px;']),

            ":JumlahUnit" => '<div class="form-group">
<label class="control-label" for="selisih" style="margin: 0; padding: 6px 0; width: 80px;">Jumlah Unit</label>'.
                \kartik\number\NumberControl::widget([
                    'id' => 'JumlahUnit',
                    'name' => 'JumlahUnit',
                    'value' => 0,
                    'readonly' => true,
                    'maskedInputOptions' => [
                        'groupSeparator' => '.',
                        'radixPoint' => ','
                    ]
                ])
                .'</div>',
            ":Total" => '<div class="form-group">
<label class="control-label" for="Total" style="margin: 0; padding: 6px 0; width: 60px;">Total</label>'.
                \kartik\number\NumberControl::widget([
                    'id' => 'Total',
                    'name' => 'Total',
                    'value' => 0,
                    'readonly' => true,
                    'maskedInputOptions' => [
                        'groupSeparator' => '.',
                        'radixPoint' => ','
                    ]
                ])
                .'</div>',

            ":btnSave" => Html::submitButton( '<i class="glyphicon glyphicon-floppy-disk"> <br><br> Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'id' => 'btnSave','style' => 'width:60px;height:60px']),
            ":btnPrint" => '<a href="'.$url['print'].'"class="btn btn-danger btn-warning" role="button" style="width:60px;height:60px"><i class="glyphicon glyphicon-print"><br><br>Print </i></a>',
            ":btnCancel"       => Html::Button( '<i class="fa fa-ban"><br><br>Batal</i>', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel','style' => 'width:60px;height:60px' ] ),
        ],
        [
            '_akses' => 'Saldo Awal Motor',
            'url_main' => 'tsaldomotor',
            'url_id' => $_GET['id'] ?? '',
        ]
    );
    ActiveForm::end();
?>

</div>

<?php
$urlMotorWarna = $url['tdmotorwarna'];

$this->registerJs(\aunit\components\TUi::detailGridJs(\aunit\models\Tsaldomotor::className(), [
    'url' => $url['detail'],
    'height' => 320,
    'extraParams' => [
        'NoTrans' => $model->NoTrans,
        'TglTrans' => $model->TglTrans
    ],
    'colModel' => [
        [
            'name' => 'MotorType',
            'label' => 'Type',
            'width' => 190,
            'editable' => true,
            'editor' => [
                'type' => 'select2',
                'dataArray' => \aunit\models\Tdmotortype::find()->comboSelect2(),
                'dropdownAutoWidth' => true
            ],
            'onchange' => new \yii\web\JsExpression("function(e) {
                let motorWarna = $('[id=\"'+$(this).attr('rowid')+'_'+'MotorWarna'+'\"]');
                let motorTahun = $('[id=\"'+$(this).attr('rowid')+'_'+'MotorTahun'+'\"]');
                motorWarna.utilSelect2().loadRemote('$urlMotorWarna', { mode: 'combo', MotorType: this.value }, true);
                if(!motorTahun.val()) motorTahun.val(new Date().getFullYear());
            }")
        ],
        [
            'name' => 'MotorTahun',
            'label' => 'Tahun',
            'width' => 100,
            'editable' => true
        ],
        [
            'name' => 'MotorWarna',
            'label' => 'Warna',
            'width' => 150,
            'editable' => true,
            'editor' => [
                'type' => 'select2',
                'dropdownAutoWidth' => true
            ]
        ],
        [
            'name' => 'MotorNoMesin',
            'label' => 'NoMesin',
            'width' => 170,
            'editable' => true
        ],
        [
            'name' => 'MotorNoRangka',
            'label' => 'NoRangka',
            'width' => 170,
            'editable' => true
        ],
        [
            'name' => 'MotorHarga',
            'label' => 'Motor Harga',
            'width' => 130,
            'editable' => true,
            'editor' => [
                'type' => 'numbercontrol'
            ]
        ]
    ],
    'funcAfterLoadGrid' => <<< JS
        function(grid, response) {
            var total = 0,
                count = 0;
            
            for(var i = 0; i < response.rows.length; i++) {
                total += parseFloat(response.rows[i].MotorHarga);
                count++;
            }
            
            if (count === 0)
                $('#btnSave').attr('disabled', 'disabled');
            else
                $('#btnSave').removeAttr('disabled');
            
            $('#JumlahUnit').utilNumberControl().val(count);
            $('#Total').utilNumberControl().val(total);
        }
JS
]));


$this->registerJs( <<< JS
     $('#btnCancel').click(function (event) {	      
       $('#frm_tsaldomotor_id').attr('action','{$url['cancel']}');
       $('#frm_tsaldomotor_id').submit();
    });

    $('[name=Tgltrans]').utilDateControl().cmp().attr('tabindex', '1');
     
JS);