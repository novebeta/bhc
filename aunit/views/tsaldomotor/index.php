<?php
use aunit\assets\AppAsset;
/* @var $this yii\web\View */
/* @var $searchModel aunit\models\TsaldomotorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Saldo Awal Motor';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
		'NoTrans'       => 'Nomor'
	],
	'cmbTgl'    => [
//		'SSTgl' => 'Tgl SS'
	],
	'cmbNum'    => [
	],
	'sortname'  => "TglTrans",
	'sortorder' => 'desc',
];
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Tsaldomotor::className(),
	'Saldo Awal Motor' ), \yii\web\View::POS_READY );
