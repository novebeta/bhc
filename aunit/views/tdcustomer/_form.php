<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\datecontrol\DateControl;
use kartik\select2\Select2;
use aunit\models\Tdarea;
use kartik\date\DatePicker;
use common\components\Custom;

/* @var $this yii\web\View */
/* @var $model aunit\models\Tdcustomer */
/* @var $form yii\widgets\ActiveForm */

\aunit\assets\AppAsset::register($this);

//$url['cancel'] = Url::toRoute('tdcustomer/index');
$urlTdArea = Url::toRoute('tdarea/find');
?>

<div class="tdsale-form">
    <?php
    $form = ActiveForm::begin(['id'=>'frm_tdcustomer_id']);
    \aunit\components\TUi::form(
        [
            'class' => "row-no-gutters",
            'items' => [
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelCusKode"],
                        ['class' => "col-sm-6",'items' => ":CusKode"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-1", 'items' => ":LabelTglBeli"],
                        ['class' => "col-sm-5",'style' => "padding-left: 1px",'items' => ":TglBeli"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelKodeKons"],
                        ['class' => "col-sm-5",'style' => "padding-left: 1px",'items' => ":KodeKons"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelCusNama"],
                        ['class' => "col-sm-6",'items' => ":CusNama"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-1",'items' => ":LabelCusKTP"],
                        ['class' => "col-sm-5",'style' => "padding-left: 1px", 'items' => ":CusKTP"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelCusProvinsi"],
                        ['class' => "col-sm-5",'style' => "padding-left: 1px",'items' => ":CusProvinsi"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelCusAlamat"],
                        ['class' => "col-sm-6",'items' => ":CusAlamat"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-1",'items' => ":LabelCusRT"],
                        ['class' => "col-sm-2",'style' => "padding-left: 1px",'items' => ":CusRT"],
                        ['class' => "col-sm-1",'style' => "padding-left: 1px;text-align:center;",'items' => ":LabelCusRW"],
                        ['class' => "col-sm-2",'style' => "padding-left: 1px",'items' => ":CusRW"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelCusKabupaten"],
                        ['class' => "col-sm-5",'style' => "padding-left: 1px",'items' => ":CusKabupaten"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelCusTelepon"],
                        ['class' => "col-sm-3",'style' => "padding-right:2px", 'items' => ":CusTelepon"],
                        ['class' => "col-sm-3",'style' => "padding-right:2px", 'items' => ":2CusTelepon"],
                        ['class' => "col-sm-3",'style' => "padding-left:2px;", 'items' => ":Telepon2"],
//                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'style' => "padding-left: 1px",'style' => "padding-left: 20px",'items' => ":LabelKodePos"],
                        ['class' => "col-sm-2",'style' => "padding-left: 1px;text-align:center;",'items' => ":KodePos"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelCusKecamatan"],
                        ['class' => "col-sm-5",'style' => "padding-left: 1px",'items' => ":CusKecamatan"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelCusEmail"],
                        ['class' => "col-sm-6",'items' => ":CusEmail"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-3",'items' => ":LabelCusStatusRumah"],
                        ['class' => "col-sm-3",'style' => "padding-left: 1px", 'items' => ":CusStatusRumah"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelCusKelurahan"],
                        ['class' => "col-sm-5",'style' => "padding-left: 1px",'items' => ":CusKelurahan"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelCusSex"],
                        ['class' => "col-sm-3",'items' => ":CusSex"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-3",'items' => ":LabelCusTempatLhr"],
                        ['class' => "col-sm-3",'items' => ":CusTempatLhr"],
                        ['class' => "col-sm-3",'style' => "padding-left: 1px",'items' => ":CusTglLhr"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelCusAgama"],
                        ['class' => "col-sm-5",'style' => "padding-left: 1px",'items' => ":CusAgama"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelCusPekerjaan"],
                        ['class' => "col-sm-6",'items' => ":CusPekerjaan"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-3",'items' => ":LabelCusPendidikan"],
                        ['class' => "col-sm-3",'items' => ":CusPendidikan"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelCusPengeluaran"],
                        ['class' => "col-sm-5",'items' => ":CusPengeluaran"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelCusPembeliNama"],
                        ['class' => "col-sm-5",'style' => 'padding-right:1px;', 'items' => ":CusPembeliNama"],
                        ['class' => "col-sm-5",'style' => 'padding-left:1px;', 'items' => ":PembeliKTP"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelCusPembeliAlamat"],
                        ['class' => "col-sm-8",'items' => ":CusPembeliAlamat"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-3",'items' => ":LabelCusKeterangan"],
                        ['class' => "col-sm-13",'items' => ":CusKeterangan"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelIDCustomer"],
                        ['class' => "col-sm-5",'items' => ":IDCustomer"],
                    ],
                ],
            ]
        ],
        ['class' => "pull-right", 'items' => ":btnSave :btnCancel"
        ],
        [
            ":LabelCusKode"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">ID Konsumen</label>',
            ":LabelTglBeli"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Beli</label>',
            ":LabelCusNama"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama</label>',
            ":LabelCusKTP"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">KTP</label>',
            ":LabelCusAlamat"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
            ":LabelCusRT"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">RT/RW</label>',
            ":LabelCusRW"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">/</label>',
            ":LabelCusTelepon"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon</label>',
            ":LabelKodePos"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">KodePos</label>',
            ":LabelCusEmail"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Email</label>',
            ":LabelCusStatusRumah"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status : Rumah</label>',
            ":LabelKodeKons"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode Kons</label>',
            ":LabelCusProvinsi"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Provinsi</label>',
            ":LabelCusKabupaten"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kabupaten</label>',
            ":LabelCusKecamatan"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kecamatan</label>',
            ":LabelCusKelurahan"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kelurahan</label>',
            ":LabelCusSex"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis Kelamin</label>',
            ":LabelCusTempatLhr"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tempat - Tgl Lahir</label>',
            ":LabelCusTglLhr"       => '<label class="control-label" style="margin: 0; padding: 6px 0;"> - </label>',
            ":LabelCusAgama"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Agama</label>',
            ":LabelCusPekerjaan"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pekerjaan</label>',
            ":LabelCusPendidikan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pendidikan</label>',
            ":LabelCusPengeluaran"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pengeluaran</label>',
            ":LabelCusPembeliNama"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pembeli - KTP</label>',
            ":LabelCusPembeliAlamat"=> '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
            ":LabelCusKeterangan"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
            ":LabelIDCustomer"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Customer ID</label>',

            ":CusKode"          => Html::textInput('CusKodeView',str_replace('=','-',$model->CusKode),['class'=>'form-control']).
                                   $form->field($model, 'CusKode', ['template' => '{input}'])->hiddenInput(['maxlength' => true,]),
            ":CusNama"          => $form->field($model, 'CusNama', ['template' => '{input}'])->textInput(['maxlength' => true,]),
            ":CusKTP"           => $form->field($model, 'CusKTP', ['template' => '{input}'])->textInput(['maxlength' => true,]),
            ":CusAlamat"        => $form->field($model, 'CusAlamat', ['template' => '{input}'])->textInput(['maxlength' => true,]),
            ":CusRT"            => $form->field($model, 'CusRT', ['template' => '{input}'])->textInput(['maxlength' => true,]),
            ":CusRW"            => $form->field($model, 'CusRW', ['template' => '{input}'])->textInput(['maxlength' => true,]),
            ":CusTelepon"       => $form->field($model, 'CusTelepon', ['template' => '{input}'])->textInput(['maxlength' => true,]),
            ":2CusTelepon"       => $form->field($model, 'CusTelepon2', ['template' => '{input}'])->textInput(['maxlength' => true,]),
            ":CusEmail"         => $form->field($model, 'CusEmail', ['template' => '{input}'])->textInput(['maxlength' => true,]),
            ":CusPembeliNama"   => $form->field($model, 'CusPembeliNama', ['template' => '{input}'])->textInput(['maxlength' => true,]),
            ":PembeliKTP"       => $form->field($model, 'CusPembeliKTP', ['template' => '{input}'])->textInput(['maxlength' => true,]),
            ":CusPembeliAlamat" => $form->field($model, 'CusPembeliAlamat', ['template' => '{input}'])->textInput(['maxlength' => true,]),
            ":CusKeterangan"    => $form->field($model, 'CusKeterangan', ['template' => '{input}'])->textInput(['maxlength' => true,]),
            ":CusProvinsi"      => $form->field($model, 'CusProvinsi', ['template' => '{input}'])->textInput(['maxlength' => true,]),
            ":IDCustomer"      => $form->field($model, 'ZidCustomer', ['template' => '{input}'])->textInput(['maxlength' => true,]),
            ":TglBeli"          => DateControl::widget([
                                        'name' => "Tdcustomer[CusTglBeli]",
                                        'type' => DateControl::FORMAT_DATE,
                                        'value' => $model->CusTglBeli,
                                        ]),
            ":Telepon2"         => $form->field($model, 'CusTelepon2', ['template' => '{input}'])
                                        ->dropDownList([
                                            'Prabayar' => 'Prabayar',
                                            'Pascabayar' => 'Pascabayar',
                                        ], ['maxlength' => true]),
            ":CusStatusRumah"   => $form->field($model, 'CusStatusRumah', ['template' => '{input}'])
                                        ->dropDownList([
                                            'Rumah Sendiri' => 'Sendiri',
                                            'Rumah Ortu' => 'Ortu',
                                            'Rumah Sewa' => 'Sewa',
                                        ], ['maxlength' => true]),
            ":KodeKons"         => $form->field($model, 'CusKodeKons', ['template' => '{input}'])
                                    ->dropDownList([
                                        'Individual Customer (Regular)' => 'Individual Customer (Regular)',
                                        'Individual Customer (Joint Promo)' => 'Individual Customer (Joint Promo)',
                                        'Individual Customer (Kolektif)' => 'Individual Customer (Kolektif)',
                                        'Group Customer' => 'Group Customer',
                                    ], ['maxlength' => true]),
            ":CusKabupaten"     => $form->field($model, 'CusKabupaten', ['template' => '{input}'])
                                        ->widget(Select2::classname(), [
                                            'value' => $model->CusKabupaten,
                                            'theme' => Select2::THEME_DEFAULT,
                                            'pluginOptions' => ['highlight' => true],
                                            'data' => Tdarea::find()->kabupaten()
                                        ]),
            ":CusKelurahan"     => $form->field($model, 'CusKelurahan', ['template' => '{input}'])
                                    ->widget(Select2::classname(), [
                                        'value' => $model->CusKabupaten,
                                        'theme' => Select2::THEME_DEFAULT,
                                        'pluginOptions' => ['highlight' => true],
                                    ]),
            ":CusKecamatan"     => $form->field($model, 'CusKecamatan', ['template' => '{input}'])
                                    ->widget(Select2::classname(), [
                                        'value' => $model->CusKabupaten,
                                        'theme' => Select2::THEME_DEFAULT,
                                        'pluginOptions' => ['highlight' => true],
                                    ]),
            ":KodePos"          => $form->field($model, 'CusKodePos', ['template' => '{input}'])
                                    ->widget(Select2::classname(), [
                                        'value' => $model->CusKodePos,
                                        'theme' => Select2::THEME_DEFAULT,
                                        'pluginOptions' => ['highlight' => true],
                                        'data' => Tdarea::find()->KodePos()
                                    ]),
            ":CusSex"           => $form->field($model, 'CusSex', ['template' => '{input}'])
                                    ->dropDownList([
                                        'Perempuan' => 'Perempuan',
                                        'Laki-Laki' => 'Laki-Laki',
                                    ], ['maxlength' => true]),
            ":CusTempatLhr"     => $form->field($model, 'CusTempatLhr', ['template' => '{input}'])->textInput(['maxlength' => true,]),
            ":CusTglLhr"        => DateControl::widget([
                                    'name' => 'Tdcustomer[CusTglLhr]',
                                    'value' => $model->CusTglLhr,
//                                    'value' => '01/01/1900',
                                    'type' => DateControl::FORMAT_DATE]),
            ":CusAgama"         => $form->field($model, 'CusAgama', ['template' => '{input}'])
                                    ->dropDownList([
                                        'Islam' => 'Islam',
                                        'Kristen' => 'Kristen',
                                        'Katholik' => 'Katholik',
                                        'Hindu' => 'Hindu',
                                        'Budha' => 'Budha',
                                        'Lain-Lain' => 'Lain-Lain',
                                    ], ['maxlength' => true]),
            ":CusPekerjaan"     => $form->field($model, 'CusPekerjaan', ['template' => '{input}'])
                                    ->dropDownList([
                                        'Pegawai Negeri' => 'Pegawai Negeri',
                                        'Pegawai Swasta' => 'Pegawai Swasta',
                                        'Ojek' => 'Ojek',
                                        'Wiraswasta/Pedagang' => 'Wiraswasta/Pedagang',
                                        'Mahasiswa/ Pelajar' => 'Mahasiswa/ Pelajar',
                                        'Guru/Dosen' => 'Guru/Dosen',
                                        'TNI/Polri' => 'TNI/Polri',
                                        'Ibu Rumah Tangga' => 'Ibu Rumah Tangga',
                                        'Petani/Nelayan' => 'Petani/Nelayan',
                                        'Profesional (Dokter/Pengacara, dll)' => 'Profesional (Dokter/Pengacara, dll)',
                                        'Akuntan' => 'Akuntan',
                                        'Anggota BPK' => 'Anggota BPK',
                                        'Anggota DPD' => 'Anggota DPD',
                                        'Anggota DPRD Kabupaten/Kota' => 'Anggota DPRD Kabupaten/Kota',
                                        'Anggota DPRD Provinsi' => 'Anggota DPRD Provinsi',
                                        'Anggota DPR-RI' => 'Anggota DPR-RI',
                                        'Anggota Kabinet/Kementerian' => 'Anggota Kabinet/Kementerian',
                                        'Anggota Mahkamah Konstitusi' => 'Anggota Mahkamah Konstitusi',
                                        'Apoteker' => 'Apoteker',
                                        'Arsitek' => 'Arsitek',
                                        'Belum/Tidak Bekerja' => 'Belum/Tidak Bekerja',
                                        'Biarawati' => 'Biarawati',
                                        'Bidan' => 'Bidan',
                                        'Bupati' => 'Bupati',
                                        'Buruh Harian Lepas' => 'Buruh Harian Lepas',
                                        'Buruh Nelayan/Perikanan' => 'Buruh Nelayan/Perikanan',
                                        'Buruh Peternakan' => 'Buruh Peternakan',
                                        'Buruh Tani/Perkebunan' => 'Buruh Tani/Perkebunan',
                                        'Dokter' => 'Dokter',
                                        'Dosen' => 'Dosen',
                                        'Duta Besar' => 'Duta Besar',
                                        'Gubernur' => 'Gubernur',
                                        'Guru' => 'Guru',
                                        'Imam Mesjid' => 'Imam Mesjid',
                                        'Industri' => 'Industri',
                                        'Juru Masak' => 'Juru Masak',
                                        'Karyawan BUMD' => 'Karyawan BUMD',
                                        'Karyawan BUMN' => 'Karyawan BUMN',
                                        'Karyawan Honorer' => 'Karyawan Honorer',
                                        'Karyawan Swasta' => 'Karyawan Swasta',
                                        'Kepala Desa' => 'Kepala Desa',
                                        'Kepolisian RI' => 'Kepolisian RI',
                                        'Konstruksi' => 'Konstruksi',
                                        'Konsultan' => 'Konsultan',
                                        'Lainnya' => 'Lainnya',
                                        'Mekanik' => 'Mekanik',
                                        'Mengurus Rumah Tangga' => 'Mengurus Rumah Tangga',
                                        'Nelayan/Perikanan' => 'Nelayan/Perikanan',
                                        'Notaris' => 'Notaris',
                                        'Paraji' => 'Paraji',
                                        'Paranormal' => 'Paranormal',
                                        'Pastor' => 'Pastor',
                                        'Pedagang' => 'Pedagang',
                                        'Pegawai Negeri Sipil' => 'Pegawai Negeri Sipil',
                                        'Pelajar/Mahasiswa' => 'Pelajar/Mahasiswa',
                                        'Pelaut' => 'Pelaut',
                                        'Pembantu Rumah Tangga' => 'Pembantu Rumah Tangga',
                                        'Penata Busana' => 'Penata Busana',
                                        'Penata Rambut' => 'Penata Rambut',
                                        'Penata Rias' => 'Penata Rias',
                                        'Pendeta' => 'Pendeta',
                                        'Peneliti' => 'Peneliti',
                                        'Pengacara' => 'Pengacara',
                                        'Pensiunan' => 'Pensiunan',
                                        'Penterjemah' => 'Penterjemah',
                                        'Penyiar Radio' => 'Penyiar Radio',
                                        'Penyiar Televisi' => 'Penyiar Televisi',
                                        'Perancang Busana' => 'Perancang Busana',
                                        'Perangkat Desa' => 'Perangkat Desa',
                                        'Perawat' => 'Perawat',
                                        'Perdagangan' => 'Perdagangan',
                                        'Petani' => 'Petani',
                                        'Pekebun' => 'Pekebun',
                                        'Peternak' => 'Peternak',
                                        'Pialang' => 'Pialang',
                                        'Pilot' => 'Pilot',
                                        'Presiden' => 'Presiden',
                                        'Promotor Acara' => 'Promotor Acara',
                                        'Psikiater' => 'Psikiater',
                                        'Psikolog' => 'Psikolog',
                                        'Seniman' => 'Seniman',
                                        'Sopir' => 'Sopir',
                                        'Tabib' => 'Tabib',
                                        'Tentara Nasional Indonesia' => 'Tentara Nasional Indonesia',
                                        'Transportasi' => 'Transportasi',
                                        'Tukang Batu' => 'Tukang Batu',
                                        'Tukang Cukur' => 'Tukang Cukur',
                                        'Tukang Gigi' => 'Tukang Gigi',
                                        'Tukang Jahit' => 'Tukang Jahit',
                                        'Tukang Kayu' => 'Tukang Kayu',
                                        'Tukang Las/Pandai Besi' => 'Tukang Las/Pandai Besi',
                                        'Tukang Listrik' => 'Tukang Listrik',
                                        'Tukang Sol Sepatu' => 'Tukang Sol Sepatu',
                                        'Ustadz/Mubaligh' => 'Ustadz/Mubaligh',
                                        'Wakil Bupati' => 'Wakil Bupati',
                                        'Wakil Gubernur' => 'Wakil Gubernur',
                                        'Wakil Presiden' => 'Wakil Presiden',
                                        'Wakil Walikota' => 'Wakil Walikota',
                                        'Walikota' => 'Walikota',
                                        'Wartawan' => 'Wartawan',
                                        'Wiraswasta' => 'Wiraswasta'
                                    ], ['maxlength' => true]),
            ":CusPendidikan"    => $form->field($model, 'CusPendidikan', ['template' => '{input}'])
                                    ->dropDownList([
                                        'Tidak Tamat SD' => 'Tidak Tamat SD',
                                        'SD' => 'SD',
                                        'SLTP/SMP' => 'SLTP/SMP',
                                        'SLTA/SMU' => 'SLTA/SMU',
                                        'Akademi/Diploma' => 'Akademi/Diploma',
                                        'Sarjana' => 'Sarjana',
                                        'Pasca Sarjana' => 'Pasca Sarjana',
                                    ], ['maxlength' => true]),
            ":CusPengeluaran"   => $form->field($model, 'CusPengeluaran', ['template' => '{input}'])
                                    ->dropDownList([
                                        '<Rp 700.000,-' => '<Rp 700.000,-',
                                        'Rp 700.001,- s/d Rp 1.000.000,-' => 'Rp 700.001,- s/d Rp 1.000.000,-',
                                        'Rp 1.000.001,- s/d Rp 1.500.000,-' => 'Rp 1.000.001,- s/d Rp 1.500.000,-',
                                        'Rp 1.500.001,- s/d Rp 2.000.000,-' => 'Rp 1.500.001,- s/d Rp 2.000.000,-',
                                        'Rp 2.000.001,- s/d Rp 3.000.000,-' => 'Rp 2.000.001,- s/d Rp 3.000.000,-',
                                        'Rp 3.000.001,- s/d Rp 4.000.000,-' => 'Rp 3.000.001,- s/d Rp 4.000.000,-',
                                        '>Rp 4.000.000,-' => '>Rp 4.000.000,-'
                                    ], ['maxlength' => true]),


            ":btnSave"          => Html::submitButton( '<i class="glyphicon glyphicon-floppy-disk"></i> Simpan', [ 'class' => 'btn btn-info btn-primary ', 'id' => 'btnSave'. Custom::viewClass() ] ),
            ":btnCancel"        => Html::Button( '<i class="fa fa-ban"></i> Batal', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel','style' => 'width:80px' ] ),

        ],
        [
            '_akses' => 'Konsumen',
            'url_main' => 'tdcustomer',
            'url_id' => $_GET['id'] ?? '',
        ]
    );
    ActiveForm::end();
    ?>
</div>

<?php
$this->registerJs(<<< JS
    let CusKabupaten = $('#tdcustomer-cuskabupaten'),
        CusKecamatan = $('#tdcustomer-cuskecamatan'),
        CusKelurahan = $('#tdcustomer-cuskelurahan');
    CusKabupaten.on("change", function (e) { console.log()
        CusKecamatan.utilSelect2().loadRemote('$urlTdArea', { mode: 'combo', field: 'Kecamatan', Kabupaten: this.value }, true, function(response) {
            CusKecamatan.val(null).trigger('change');
            CusKelurahan.val(null).trigger('change');
        });
    });

    CusKecamatan.on("change", function (e) {
        CusKelurahan.utilSelect2().loadRemote('$urlTdArea', { mode: 'combo', field: 'Kelurahan', Kecamatan: this.value }, true, function(response) {
            CusKelurahan.val(null).trigger('change');
        });
    });
        
     $('#btnCancel').click(function (event) {	      
       $('#frm_tdcustomer_id').attr('action','{$url['cancel']}');
       $('#frm_tdcustomer_id').submit();
    });    
     
     
     
    
JS
);