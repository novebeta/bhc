<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tdcustomer */
$this->title                   = str_replace('Menu','',\aunit\components\TUi::$actionMode).'Konsumen: ' . $model->CusKode;
$this->params['breadcrumbs'][] = [ 'label' => 'Konsumen', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->CusKode, 'url' => [ 'view', 'id' => $model->CusKode ] ];
$this->params['breadcrumbs'][] = 'Edit';
$params = '&id='.$id;
?>
<div class="tdcustomer-update">
	<?= $this->render( '_form', [
		'model' => $model,
        'url' => [
            'save'    => Custom::url(\Yii::$app->controller->id.'/update'.$params ),
            'cancel'    => Custom::url(\Yii::$app->controller->id.'/' ),
        ]
	] ) ?>
</div>
