<?php
/* @var $this yii\web\View */
use aunit\assets\AppAsset;
AppAsset::register( $this );
$this->title                   = 'Konsumen';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
        'CusNama'      => 'Nama Konsumen',
		'CusKode'      => 'Kode Konsumen',
        'CusAlamat'    => 'Alamat',
        'CusTelepon'   => 'Telepon1',
        'CusTelepon2'  => 'Telepon2',
        'CusKeterangan'=> 'Keterangan',
        'CusKelurahan' => 'Kelurahan',
        'CusKecamatan' => 'Kecamatan',
        'CusKabupaten' => 'Kabupaten',
        'CusRT'        => 'Rt',
        'CusRW'        => 'Rw',

//		'CusKTP'       => 'Ktp',
//		'CusProvinsi'  => 'Provinsi',
//		'CusKodePos'   => 'Kode Pos',
//		'CusSex'       => 'Sex',
//		'CusTempatLhr' => 'Tempat Lhr',
//		'CusAgama'       => 'Agama',
//		'CusPekerjaan'   => 'Pekerjaan',
//		'CusPendidikan'  => 'Pendidikan',
//		'CusPengeluaran' => 'Pengeluaran',
//		'CusEmail'         => 'Email',
//		'CusKodeKons'      => 'Kode Kons',
//		'CusStatusHP'      => 'Status Hp',
//		'CusStatusRumah'   => 'Status Rumah',
//		'CusPembeliNama'   => 'Pembeli Nama',
//		'CusPembeliKTP'    => 'Pembeli Ktp',
//		'CusPembeliAlamat' => 'Pembeli Alamat',
//		'CusKK'            => 'Kk',

	],
	'cmbTgl' => [
		'CusTglBeli' => 'Tgl Beli',
		'CusTglLhr'  => 'Tgl Lhr',
	],
	'cmbNum' => [
	],
	'sortname'  => "CusKode",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Tdcustomer::className() ,
    'Konsumen', [ 'mode' => isset($mode) ? $mode : '' ]), \yii\web\View::POS_READY );