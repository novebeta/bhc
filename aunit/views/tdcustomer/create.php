<?php
use yii\helpers\Html;
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tdcustomer */
$this->title                   = 'Tambah Konsumen';
$this->params['breadcrumbs'][] = [ 'label' => 'Konsumen', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdcustomer-create">
	<?= $this->render( '_form', [
		'model' => $model,
        'url' => [
            'save'    => Custom::url(\Yii::$app->controller->id.'/create' ),
            'cancel'    => Custom::url(\Yii::$app->controller->id.'/' ),
        ]
	] ) ?>
</div>
