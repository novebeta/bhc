<?php
use aunit\assets\AppAsset;
use aunit\components\Menu;
use kartik\datecontrol\DateControl;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttgeneralledgerhd */
/* @var $form yii\widgets\ActiveForm */
AppAsset::register( $this );
$this->title                   = 'Validasi Jurnal';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="ttgeneralledgerhd-form">
        <div class="col-sm-12">
			<?php
			$form = ActiveForm::begin( [ 'id' => "validasi-jurnal-id" ] );
			\aunit\components\TUi::form(
				[
					'class' => "row-no-gutters",
					'items' => [
						[
							'class' => "form-inline",
							'items' => [
								[
									'style' => "margin-bottom: 3px;",
									'items' => ":lblTglSudah :TglSudah :modeHidden"
								],
								[
									'style' => "margin-bottom: 3px;",
									'items' => ":lblTglBelum :TglBelum"
								]
							]
						],
					]
				],
				[

					'items' => Menu::showOnlyHakAkses(['Admin','Auditor'],':btnValidate','')." ".Menu::showOnlyHakAkses(['Admin'],':btnUnValidate','')
				],
				[
					":lblTglSudah"   => '<label class="control-label col-sm-11" style="margin: 0; padding: 6px 0;">Jurnal Telah Tervalidasi Sebelum Tanggal</label>',
					":TglSudah"      => "<div class='pull-right' style='padding-bottom: 2px'>" . DateControl::widget( [
							'name'          => 'dtpTgl1',
							'value'         => $last,
							'pluginOptions' => [ 'autoclose' => true ],
						] ) . "</div>",
					":lblTglBelum"   => '<label class="control-label col-sm-11" style="margin: 0; padding: 6px 0; ">Jurnal Akan Divalidasi Sampai Tanggal</label>',
					":TglBelum"      => "<div class='pull-right'>" . DateControl::widget( [
							'name'          => 'dtpTgl2',
							'value'         => date( 'Y-m-d' ),
							'pluginOptions' => [ 'autoclose' => true ],
						] ) . "</div>",
					"modeHidden"     => '<input type="hidden" id="mode_id" name="mode" >',
					":btnValidate"   => '<button class="btn btn-success" id="validasi-btn" type="button"><i class="glyphicon glyphicon-ok-circle "></i>&nbsp &nbsp Validasi &nbsp&nbsp</button>',
					":btnUnValidate" => '<button class="btn btn-danger pull-right" id="unvalidasi-btn" type="button"><i class="glyphicon glyphicon-remove-circle"> Unvalidasi</i></button>'
				]
			);
			ActiveForm::end();
			?>
        </div>
    </div>
<?php
$this->registerJs( $this->render( 'validasi-jurnal.js' ) );
$this->registerJs( <<< JS
    
    $('[name=dtpTgl1]').utilDateControl().cmp().attr('tabindex', '1');
    $('[name=dtpTgl2]').utilDateControl().cmp().attr('tabindex', '2');
    
JS
);