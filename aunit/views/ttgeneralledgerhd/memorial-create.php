<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttgeneralledgerhd */

$params                          = '&id=' . $id . '&action=create';
$cancel                          = Custom::url( \Yii::$app->controller->id . '/cancel'.$params );
$this->title                     = 'Tambah - Jurnal Memorial';
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'Jurnal Memorial', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = $this->title;
//$this->title = 'Tambah - Jurnal Memorial';
//$this->params['breadcrumbs'][] = ['label' => 'Jurnal Memorial', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
//
//$params = '&id='.$id.'&action=create';
$dsJurnal['GLLink'] = $dsJurnal[ 'NoGLView' ];
?>
<div class="ttgeneralledgerhd-create">

    <?= $this->render('memorial-form', [
        'model' => $model,
        'dsJurnal' => $dsJurnal,
        'view'=>$view,
        'url' => [
                'update'    => Custom::url(\Yii::$app->controller->id.'/memorial-update'.$params ),
                'print'     => Custom::url(\Yii::$app->controller->id.'/memorial-print'.$params ),
                'cancel'    => Custom::url(\Yii::$app->controller->id.'/memorial-cancel'.$params ),
                'detail'    => Custom::url('ttgeneralledgerit/index'.$params )
        ]
    ]) ?>

</div>
