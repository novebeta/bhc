<?php

use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttgeneralledgerhd */

$this->title = "Tambah - $title";
$this->params['breadcrumbs'][] = ['label' => $title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$action = 'create';
?>
<div class="ttgeneralledgerhd-create">

    <?= $this->render('_form', [
        'model' => $model,
        'kodeTrans' => $kodeTrans,
        'url' => [
            'update'    => Url::toRoute([ \Yii::$app->controller->id."/$actionPrefix-update", 'id' => $id, 'action' => $action] ),
            'print'     => Url::toRoute([ \Yii::$app->controller->id."/$actionPrefix-print", 'id' => $id, 'action' => $action] ),
            'cancel'    => Url::toRoute([ \Yii::$app->controller->id."/$actionPrefix-cancel", 'id' => $id, 'action' => $action] ),
            'detail'    => Url::toRoute([ 'ttgeneralledgerit/index', 'id' => $id, 'action' => $action] ),
            'validation'    => Url::toRoute([ \Yii::$app->controller->id.'/validate', 'id' => $id, 'action' => $action] ),
        ]
    ]) ?>

</div>
