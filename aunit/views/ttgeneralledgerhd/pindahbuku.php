<?php
use common\components\Custom;
use aunit\assets\AppAsset;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
AppAsset::register( $this );
$this->title                   = 'Jurnal Pemindahan Bukuan';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt'    => [
        'NoGL'    => 'No. Jurnal',
        'MemoGL'  => 'Memo Jurnal',
        'GLLink'  => 'Link Transaksi',
        'HPLink'  => 'Link Hutang-Piutang',
        'NoAccount'  => 'No. Perkiraan',
        'NamaAccount'  => 'Nama Perkiraan',
        'KeteranganGL'  => 'Keterangan',
//		'GLValid' => 'Validasi',
	],
	'cmbTgl'    => [
		'TglGL' => 'Tgl Jurnal',
	],
	'cmbNum'    => [
		'TotalDebetGL'  => 'Debet',
		'TotalKreditGL' => 'Kredit',
	],
	'sortname'  => "TglGL",
	'sortorder' => 'desc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Ttgeneralledgerhd::className(), 'Jurnal Pemindah Bukuan', [
    'url_add' => Url::toRoute( [ 'ttgeneralledgerhd/pindahbuku-create' , 'action' => 'create' ] ),
    'url_update' => Url::toRoute( [ 'ttgeneralledgerhd/pindahbuku-update' , 'action' => 'update' ] ),
    'url_delete' => Url::toRoute( [ 'ttgeneralledgerhd/delete','type' => '' ] ),
//    'url_add'    => Custom::url( 'ttgeneralledgerhd/pindahbuku-create' ),
//	'url_update' => Custom::url( 'ttgeneralledgerhd/pindahbuku-update' ),
//	'url_delete' => Custom::url( 'ttgeneralledgerhd/td' ),
] ), \yii\web\View::POS_READY );
