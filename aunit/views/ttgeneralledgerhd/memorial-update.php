<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttgeneralledgerhd */

$this->title = str_replace('Menu','',\aunit\components\TUi::$actionMode).' - Jurnal Memorial: ' . $dsJurnal[ 'NoGLView' ];
$this->params['breadcrumbs'][] = ['label' => 'Jurnal Memorial', 'url' => ['index']];
$this->params['breadcrumbs'][] = str_replace('Menu','',\aunit\components\TUi::$actionMode);

$params = '&id='.$id.'&action=update';
?>
<div class="ttgeneralledgerhd-update">

    <?= $this->render('memorial-form', [
        'model' => $model,
        'dsJurnal' => $dsJurnal,
			'view'=>$view,
        'url' => [
            'update'    => Custom::url(\Yii::$app->controller->id.'/memorial-update'.$params ),
            'print'     => Custom::url(\Yii::$app->controller->id.'/print'.$params ),
            'cancel'    => Custom::url(\Yii::$app->controller->id.'/memorial-cancel'.$params ),
            'detail'    => Custom::url('ttgeneralledgerit/index'.$params )
        ]
    ]) ?>

</div>
