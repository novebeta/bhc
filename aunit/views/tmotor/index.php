<?php
/* @var $this yii\web\View */
/* @var $searchModel aunit\models\TmotorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
use aunit\assets\AppAsset;
use aunit\assets\JqwidgetsAsset;
use aunit\components\Menu;
use common\components\Custom;
use common\components\General;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
\aunit\assets\AppAsset::register( $this );
JqwidgetsAsset::register( $this );
$sqlType      = Yii::$app->db->createCommand( "SELECT MotorType  as value, MotorType  as label  FROM tdmotortype" )->queryAll();
$cmbType      = array_merge([''=>'Semua'],ArrayHelper::map( $sqlType, 'value', 'label' ));
$sqlWarna     = Yii::$app->db->createCommand( "SELECT MotorWarna  as value, MotorWarna  as label  FROM tdmotorwarna" )->queryAll();
$cmbWarna     = array_merge([''=>'Semua'],ArrayHelper::map( $sqlWarna, 'value', 'label' ));
$sqlWarehouse = Yii::$app->db
	->createCommand( "SELECT LokasiKode as value, LokasiNama as label FROM tdlokasi WHERE LokasiStatus IN ('A','\" & MyPOSLokasiStatus & \"') AND LokasiJenis <> 'Kas' ORDER BY LokasiStatus,LokasiKode" )
	->queryAll();
$cmbWarehouse = array_merge([''=>'Semua'],ArrayHelper::map( $sqlWarehouse, 'value', 'label' ));
$format       = <<< SCRIPT
function formatBank(result) {
    return '<div class="row" style="margin-left:2px ">' +
           '<div class="col-xs-10">' + result.id + '</div>' +
           '<div class="col-xs-14">' + result.text + '</div>' +
           '</div>';
}
function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
      return data;
    }
    if (typeof data.text === 'undefined') {
      return null;
    }
    if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1 || data.id.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      var modifiedData = $.extend({}, data, true);
      return modifiedData;
    }
    return null;
}
SCRIPT;
$escape       = new JsExpression( "function(m) { return m; }" );
$this->registerJs( $format, View::POS_HEAD );
AppAsset::register( $this );
$this->title                     = 'Motor';
$this->params[ 'breadcrumbs' ][] = $this->title;
$arr                             = [
	'cmbTxt'    => [
		'TeamKode'       => 'Kode',
		'TeamNama'       => 'Nama',
		'TeamAlamat'     => 'Alamat',
		'TeamTelepon'    => 'Telepon',
		'TeamKeterangan' => 'Keterangan',
		'TeamStatus'     => 'Status',
	],
	'cmbTgl'    => [
	],
	'cmbNum'    => [
	],
	'sortname'  => "MotorType, MotorWarna, MotorTahun, MotorNoMesin",
	'sortorder' => 'asc'
];
$this->registerJsVar( 'setcmb', $arr );
$starting_year  =date('Y' , strtotime('-1 year'));
$ending_year = date('Y' , strtotime('now'));
?>
<style>
    .box-footer {
        padding: 5px 10px 0px 10px;
    }

    .form-control {
        height: 34px;
    }
</style>
<?php $form = ActiveForm::begin( [ 'options' => [ 'autocomplete'=>'off','method' => 'post', 'id' => 'formIndexMotor' ] ] ); ?>
<div class="panel panel-default">
    <div class="tmotor-form">
		<?php
		\aunit\components\TUi::form(
			[
				'class' => "row-no-gutters",
				'items' => [
					[
						'class' => "col-md-8",
						'items' => [
							[
								'class' => 'row',
								'style' => 'margin-bottom: 5px',
								'items' => [
									[ 'class' => "col-sm-6", 'items' => ":LabelTahun" ],
									[ 'class' => "col-sm-8", 'items' => ":TahunStart" ],
									[ 'class' => "col-sm-1" ],
									[ 'class' => "col-sm-8", 'items' => ":TahunEnd" ],
								],
							],
							[
								'class' => 'row',
								'items' => [
									[ 'class' => "col-sm-10", 'style'=> 'margin-right: 2px', 'items' => ":cmbTxt1" ],
									[ 'class' => "col-sm-10", 'style'=> 'margin-right: 2px', 'items' => ":txt1" ],
									[ 'class' => "col-sm-2", 'items' => ":searchBtn" ],
								]
							]
						],
					],
					[
						'class' => "form-group col-md-14",
						'style' => "margin-bottom: -2px;",
						'items' => [
							[ 'class' => "col-sm-2", 'items' => ":LabelLokasi" ],
							[ 'class' => "col-sm-14", 'items' => ":Lokasi" ],
							[ 'class' => "col-sm-1" ],
							[ 'class' => "col-sm-2", 'items' => ":LabelKondisi" ],
							[ 'class' => "col-sm-5", 'items' => ":Kondisi" ],
							[ 'class' => "col-sm-2", 'items' => ":LabelType" ],
							[ 'class' => "col-sm-14", 'items' => ":Type" ],
							[ 'class' => "col-sm-1" ],
							[ 'class' => "col-sm-2", 'items' => ":LabelWarna" ],
							[ 'class' => "col-sm-5", 'items' => ":Warna" ],
						],
					],
			        ['class' => "pull-right", 'items' => ":btnRefresh"],
				]
			],
			[],
			[
				":LabelTahun"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tahun</label>',
				":LabelLokasi"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Lokasi</label>',
				":LabelKondisi" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kondisi</label>',
				":LabelType"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Type</label>',
				":LabelWarna"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Warna</label>',
				":Lokasi"       => Select2::widget( [
					'name'          => 'lokasi',
					'id'            => 'lokasi_id',
					'data'          => $cmbWarehouse,
					'options'       => [ 'class' => 'form-control chg' ],
					'theme'         => Select2::THEME_DEFAULT,
					'pluginOptions' => [
						'dropdownAutoWidth' => true,
						'templateResult'    => new JsExpression( 'formatBank' ),
						'templateSelection' => new JsExpression( 'formatBank' ),
						'matcher'           => new JsExpression( 'matchCustom' ),
						'escapeMarkup'      => $escape,
					]
				] ),
				":Type"         => Select2::widget( [
					'name'    => 'type',
					'id'      => 'type_id',
					'data'    => $cmbType,
					'options' => [ 'class' => 'form-control chg' ],
					'theme'   => Select2::THEME_DEFAULT,
				] ),
				":Warna"        => Select2::widget( [
					'name'    => 'warna',
					'id'      => 'warna_id',
					'data'    => $cmbWarna,
					'options' => [ 'class' => 'form-control chg' ],
					'theme'   => Select2::THEME_DEFAULT
				] ),
				":TahunStart"   => Html::dropDownList( 'thn1', $starting_year,
					General::generateTahun( 2010, 2030 ),
					[ 'id' => 'thn1_id', 'class' => 'form-control chg' ] ),
				":TahunEnd"     => Html::dropDownList( 'thn2', $ending_year,
					General::generateTahun( 2010, 2030 ),
					[ 'id' => 'thn2_id', 'class' => 'form-control chg' ] ),
				":Kondisi"      => '<div class="form-group"> ' . Html::dropDownList( 'TyStatus', '',
						[
							''        => 'Semua',
							'IN'      => 'IN',
							'OUT'     => 'OUT',
							'OUT Pos' => 'OUT Pos',
							'Transit' => 'Transit'
						],
						[
							'id'    => 'tyStatus_id',
							'class' => 'form-control chg'
						] ) . '</div>',
				":cmbTxt1"      => Html::dropDownList( '', 'MotorNoMesin',
					[
						'MotorNoMesin'  => 'No Mesin',
						'MotorNoRangka' => 'No. Rangka',
						'CusNama'       => 'Nama Konsumen',
					],
					[
						'id'    => 'cmbTxt1',
						'class' => 'form-control'
					] ),
				":txt1"         => ' <input type="text" id="txt1" class="form-control">',
				":searchBtn"    => ' <button class="btn btn-default" id="searchBtn" type="button"><i
                                                class="glyphicon glyphicon-search"></i></button>',
				":btnRefresh"      => Html::button( '<i class="glyphicon glyphicon-refresh"></i> &nbsp&nbspRefresh&nbsp&nbsp', [ 'class' => 'btn btn-danger ' . Custom::viewClass(), 'id' => 'btnRefresh', 'style' => 'height:60px' ] )
			]
		); ?>
    </div>
    <div class="panel panel-default">
        <div class="panel-body" style="padding-top: 5px">
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
</div>
<? ActiveForm::end(); ?>
<?php
$menuText       = $url_update = '';
$url            = Custom::url( 'tmotor/td' );
$url_update     = Custom::url( 'tmotor/update' );
$menuText       = 'Motor';
$button_update  = Menu::akses( $menuText, Menu::UPDATE ) ?
	"<a href=\"$url_update&id='+id+'&action=update&ro='+ro+'\" title=\"Edit\" aria-label=\"Update\" data-pjax=\"0\"><span class=\"glyphicon glyphicon-edit\"></span></a>" : "";
$button_view  = Menu::akses( $menuText, Menu::VIEW ) ?
	"<a href=\"$url_update&id='+id+'&action=view&ro='+ro+'\" title=\"View\" aria-label=\"View\" data-pjax=\"0\"><span class=\"fa fa-eye\"></span></a>" : "";
$cols           = [];
$gridColsProses = '';
if ( $button_update != '' ||  $button_view != '') {
	$gridColsProses = " {
                 label: 'Proses', 
                 name: 'actions', 
                 sortable: false,
                 width: 75, 
                 align: 'center',
                 frozen:true,
                 formatter: function(cellvalue, options, rowObject) {
                     let id = btoa(options.rowId);
                     let ro = btoa(JSON.stringify(rowObject))
                   return '$button_view $button_update';
                 }
             },";
}
foreach ( \aunit\models\Tmotor::colGrid() as $filed => $label ) {
	$is_array = is_array( $label );
	$col      = [];
	if ( $is_array ) {
		$col = $label;
	} else {
		$col[ 'name' ]  = $filed;
		$col[ 'label' ] = $label;
	}
	$cols[] = json_encode( $col );
}
$gridCols  = implode( ', ', $cols );
$gridCols  = preg_replace( '/[\'"](func\w+)[\'"]/', '$1', $gridCols );
$cols_type = $gridColsProses . $gridCols;
$this->registerJs( $this->render( 'index.js', [
	'cols' => $cols_type
] ) );
?>

