<?php
use common\components\Custom;
use common\components\General;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tmotor */
/* @var $form yii\widgets\ActiveForm */
//$this->params['breadcrumbs'][] = $this->title;
//$formatter = \Yii::$app->formatter;
$url['cancel'] = Url::toRoute( 'tmotor/index' );
$sqlType       = Yii::$app->db->createCommand( "SELECT MotorType  as value, MotorType  as label  FROM tdmotortype" )->queryAll();
$cmbType       = ArrayHelper::map( $sqlType, 'value', 'label' );
?>
<div class="tmotor-form">
	<?php
	$form = ActiveForm::begin( ['id' => 'frm_tmotor_id',  'fieldConfig' => [ 'template' => "{input}" ]  ] );
	\aunit\components\TUi::form(
		[
			'class' => "row-no-gutters",
			'items' => [
				[
					'class' => "form-group col-md-17",
					'style' => "",
					'items' => [
						[
							'class' => "row",
							'style' => "",
							'items' => [
								[ 'class' => "col-sm-3", 'style' => "", 'items' => ":LabelTypeTahun" ],
								[ 'class' => "col-sm-8", 'items' => ":Type" ],
								[ 'class' => "col-sm-2", 'style' => "padding-left:2px", 'items' => ":Tahun" ],
								[ 'class' => "col-sm-1" ],
								[ 'class' => "col-sm-3", 'items' => ":LabelWarna" ],
								[ 'class' => "col-sm-7", 'items' => ":Warna" ],
							]
						],
						[
							'class' => "row",
							'style' => "",
							'items' => [
								[ 'class' => "col-sm-3", 'style' => "", 'items' => ":LabelNoMesinNo" ],
								[ 'class' => "col-sm-8", 'items' => ":NoMesin" ],
								[ 'class' => "col-sm-2", 'style' => "padding-left:2px", 'items' => ":Nomor" ],
								[ 'class' => "col-sm-1" ],
								[ 'class' => "col-sm-3", 'items' => ":LabelNoRangka" ],
								[ 'class' => "col-sm-7", 'items' => ":NoRangka" ],
							]
						]
					],
				],
				[ 'class' => "form-group col-md-1", ],
				[
					'class' => "form-group col-md-6",
//					'style' => "margin-bottom: -2px;",
					'items' => [
						[ 'class' => "col-sm-24", 'style' => "margin-bottom: 5px;", 'items' => ":LabelDealer" ],
						[ 'class' => "col-sm-24", 'items' => ":Dealer" ],
					],
				],
				[
					'class' => "form-group col-md-24",
//					'style' => "margin-bottom: -2px;",
					'items' => [
						[ 'class' => "col-sm-2", 'items' => ":LabelNoSS" ],
						[ 'class' => "col-sm-4", 'items' => ":NoSS" ],
						[ 'class' => "col-sm-2", 'style' => "padding-left:2px", 'items' => ":TglNoSS" ],
						[ 'class' => "col-sm-1" ],
						[ 'class' => "col-sm-2", 'items' => ":LabelNoIN" ],
						[ 'class' => "col-sm-4", 'items' => ":NoIN" ],
						[ 'class' => "col-sm-2", 'style' => "padding-left:2px", 'items' => ":TglNoIN" ],
						[ 'class' => "col-sm-1" ],
						[ 'class' => "col-sm-2", 'items' => ":LabelNoSTNK" ],
						[ 'class' => "col-sm-4", 'items' => ":NoSTNK" ],
					],
				],
				[
					'class' => "form-group col-md-24",
//					'style' => "margin-bottom: -2px;",
					'items' => [
						[ 'class' => "col-sm-2", 'items' => ":LabelNoFB" ],
						[ 'class' => "col-sm-4", 'items' => ":NoFB" ],
						[ 'class' => "col-sm-2", 'style' => "padding-left:2px", 'items' => ":TglNoFB" ],
						[ 'class' => "col-sm-1" ],
						[ 'class' => "col-sm-2", 'items' => ":LabelNoDK" ],
						[ 'class' => "col-sm-4", 'items' => ":NoDK" ],
						[ 'class' => "col-sm-2", 'style' => "padding-left:2px", 'items' => ":TglNoDK" ],
						[ 'class' => "col-sm-1" ],
						[ 'class' => "col-sm-2", 'items' => ":LabelNoBPKB" ],
						[ 'class' => "col-sm-4", 'items' => ":NoBPKB" ],
					],
				],
				[
					'class' => "form-group col-md-24",
//					'style' => "margin-bottom: -2px;",
					'items' => [
						[ 'class' => "col-sm-2", 'items' => ":LabelNoPD" ],
						[ 'class' => "col-sm-4", 'items' => ":NoPD" ],
						[ 'class' => "col-sm-2", 'style' => "padding-left:2px", 'items' => ":TglNoPD" ],
						[ 'class' => "col-sm-1" ],
						[ 'class' => "col-sm-2", 'items' => ":LabelNoMESR" ],
						[ 'class' => "col-sm-4", 'items' => ":NoMESR" ],
						[ 'class' => "col-sm-2", 'style' => "padding-left:2px", 'items' => ":TglNoMESR" ],
						[ 'class' => "col-sm-1" ],
						[ 'class' => "col-sm-2", 'items' => ":LabelNoPlat" ],
						[ 'class' => "col-sm-4", 'items' => ":NoPlat" ],
					],
				],
				[
					'class' => "form-group col-md-24",
//					'style' => "margin-bottom: -2px;",
					'items' => [
						[ 'class' => "col-sm-2", 'items' => ":LabelNoRK" ],
						[ 'class' => "col-sm-4", 'items' => ":NoRK" ],
						[ 'class' => "col-sm-2", 'style' => "padding-left:2px", 'items' => ":TglNoRK" ],
						[ 'class' => "col-sm-1" ],
						[ 'class' => "col-sm-2", 'items' => ":LabelNoSK" ],
						[ 'class' => "col-sm-4", 'items' => ":NoSK" ],
						[ 'class' => "col-sm-2", 'style' => "padding-left:2px", 'items' => ":TglNoSK" ],
						[ 'class' => "col-sm-1" ],
						[ 'class' => "col-sm-2", 'items' => ":LabelNoFAHM" ],
						[ 'class' => "col-sm-4", 'items' => ":NoFAHM" ],
					],
				],
				[
					'class' => "form-group col-md-4",
					'style' => "margin-bottom: -2px;",
					'items' => [
						[ 'class' => "col-sm-24", 'items' => ":LabelNoTransaksi" ],
						[ 'class' => "col-sm-23", 'items' => ":NoTransaksi" ],
					],
				],
				[
					'class' => "form-group col-md-5",
					'style' => "margin-bottom: -2px;",
					'items' => [
						[ 'class' => "col-sm-24", 'items' => ":LabelLokasi" ],
						[ 'class' => "col-sm-23", 'items' => ":Lokasi" ],
					],
				],
				[
					'class' => "form-group col-md-3",
					'style' => "margin-bottom: -2px;",
					'items' => [
						[ 'class' => "col-sm-24", 'items' => ":LabelKondisi" ],
						[ 'class' => "col-sm-23", 'items' => ":Kondisi" ],
					],
				],
				[
					'class' => "form-group col-md-4",
					'style' => "margin-bottom: -2px;",
					'items' => [
						[ 'class' => "col-sm-24", 'items' => ":LabelTanggal" ],
						[ 'class' => "col-sm-23", 'items' => ":Tanggal" ],
					],
				],
				[
					'class' => "form-group col-md-8",
					'style' => "margin-bottom: -2px;",
					'items' => [
						[ 'class' => "col-sm-24", 'items' => ":LabelKonsumen" ],
						[ 'class' => "col-sm-24", 'items' => ":Konsumen" ],
					],
				],
			]
		],
		[
			'class' => "pull-right",
			'items' => ":btnSave :btnCancel "
		],
		[
			/* label */
			":LabelTypeTahun"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Type / Tahun</label>',
			":LabelWarna"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Warna</label>',
			":LabelNoMesinNo"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Mesin - No</label>',
			":LabelNoRangka"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Rangka</label>',
			":LabelDealer"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Dealer</label>',
			":LabelNoSS"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No SS</label>',
			":LabelNoFB"        => '<label class="control-label">No FB</label>',
			":LabelNoPD"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No PD</label>',
			":LabelNoRK"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No RK</label>',
			":LabelNoIN"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No IN</label>',
			":LabelNoDK"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No DK</label>',
			":LabelNoMESR"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">No ME/SR</label>',
			":LabelNoSK"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">No SK</label>',
			":LabelNoSTNK"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">No STNK</label>',
			":LabelNoBPKB"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">No BPKB</label>',
			":LabelNoPlat"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Plat</label>',
			":LabelNoFAHM"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">No FAHM</label>',
			":LabelNoTransaksi" => '<label class="control-label" style="margin: 0; padding: 6px 0;">No Transaksi</label>',
			":LabelLokasi"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Lokasi</label>',
			":LabelKondisi"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kondisi</label>',
			":LabelTanggal"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Tanggal</label>',
			":LabelKonsumen"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Konsumen</label>',
			/* component */
			":Type"             => Select2::widget( [
				'name'    => 'MotorType',
				'id'      => 'MotorType_id',
				'disabled' => !\aunit\components\Menu::showOnlyHakAkses(['Admin','Auditor']),
				'data'    => $cmbType,
				'value'   => $model->MotorType,
				'options' => [ 'class' => 'form-control chg' ],
				'theme'   => Select2::THEME_DEFAULT,
				'pluginEvents' => [
					"change" => new \yii\web\JsExpression( "function(e) {
                        $('#MotorWarna_id').utilSelect2().loadRemote('" . Custom::url( 'tdmotorwarna/find' ) . "', 
                        { mode: 'combo', condition:'MotorType = :MotorType', params: { ':MotorType': this.value} }, true, function(){
                            $('#MotorWarna_id').val('{$model->MotorWarna}').trigger('change');
                        });
                    }" )
				]
			] ),
			":Tahun"            => $form->field( $model, 'MotorTahun', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true] ),
			":Warna"            => Select2::widget( [
				'name'    => 'MotorWarna',
				'id'      => 'MotorWarna_id',
//				'data'    => '',
				'value'   => $model->MotorWarna,
				'options' => [ 'class' => 'form-control chg' ],
				'theme'   => Select2::THEME_DEFAULT
			] ),
			":NoMesin"          => $form->field( $model, 'MotorNoMesin', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true, ] ),
			":NoRangka"         => $form->field( $model, 'MotorNoRangka', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true, ] ),
			":Nomor"            => $form->field( $model, 'MotorAutoN', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true, 'readonly' => true ] ),
			":Dealer"           => $form->field( $model, 'DealerKode', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true, 'readonly' => true ] ),
			":NoSS"             => Html::textInput( 'SSNo', $ro->SSNo, [ 'class' => 'form-control', 'readonly' => true ] ),
			":TglNoSS"          => Html::textInput( 'SSTgl', General::asDate( $ro->SSTgl ), [ 'class' => 'form-control', 'readonly' => true ] ),
			":NoFB"             => Html::textInput( 'FBNo', $ro->FBNo, [ 'class' => 'form-control', 'readonly' => true ] ),
			":TglNoFB"          => Html::textInput( 'FBTgl', General::asDate( $ro->FBTgl ), [ 'class' => 'form-control', 'readonly' => true ] ),
			":NoPD"             => Html::textInput( 'PDNo', $ro->PDNo, [ 'class' => 'form-control', 'readonly' => true ] ),
			":TglNoPD"          => Html::textInput( 'PDTgl', General::asDate( $ro->PDTgl ), [ 'class' => 'form-control', 'readonly' => true ] ),
			":NoRK"             => Html::textInput( 'RKNo', $ro->RKNo, [ 'class' => 'form-control', 'readonly' => true ] ),
			":TglNoRK"          => Html::textInput( 'RKTgl', General::asDate( $ro->RKTgl ), [ 'class' => 'form-control', 'readonly' => true ] ),
			":NoIN"             => Html::textInput( 'INNo', $ro->INNo, [ 'class' => 'form-control', 'readonly' => true ] ),
			":TglNoIN"          => Html::textInput( 'INTgl', General::asDate( $ro->INTgl ), [ 'class' => 'form-control', 'readonly' => true ] ),
			":NoDK"             => Html::textInput( 'DKNo', $ro->DKNo, [ 'class' => 'form-control', 'readonly' => true ] ),
			":TglNoDK"          => Html::textInput( 'DKTgl', General::asDate( $ro->DKTgl ), [ 'class' => 'form-control', 'readonly' => true ] ),
			":NoMESR"           => Html::textInput( 'SDNo', $ro->SDNo, [ 'class' => 'form-control', 'readonly' => true ] ),
			":TglNoMESR"        => Html::textInput( 'SDTgl', General::asDate( $ro->SDTgl ), [ 'class' => 'form-control', 'readonly' => true ] ),
			":NoSK"             => Html::textInput( 'SKNo', $ro->SKNo, [ 'class' => 'form-control', 'readonly' => true ] ),
			":TglNoSK"          => Html::textInput( 'SKTgl', General::asDate( $ro->SKTgl ), [ 'class' => 'form-control', 'readonly' => true ] ),
			":NoSTNK"           => Html::textInput( 'STNKNo', $ro->STNKNo, [ 'class' => 'form-control', 'readonly' => true ] ),
			":NoBPKB"           => Html::textInput( 'BPKBNo', $ro->BPKBNo, [ 'class' => 'form-control', 'readonly' => true ] ),
			":NoPlat"           => Html::textInput( 'PlatNo', $ro->PlatNo, [ 'class' => 'form-control', 'readonly' => true ] ),
			":NoFAHM"           => Html::textInput( 'FakturAHMNo', $ro->NoTrans, [ 'class' => 'form-control', 'readonly' => true ] ),
			":NoTransaksi"      => Html::textInput( 'NoTrans', $ro->NoTrans, [ 'class' => 'form-control', 'readonly' => true ] ),
			":Lokasi"           => Html::textInput( 'LokasiKode', $ro->LokasiKode, [ 'class' => 'form-control', 'readonly' => true ] ),
			":Kondisi"          => Html::textInput( 'Kondisi', $ro->Kondisi, [ 'class' => 'form-control', 'readonly' => true ] ),
			":Tanggal"          => Html::textInput( 'Jam', General::asDate( $ro->Jam, 'd/m/yy H:m' ), [ 'class' => 'form-control', 'readonly' => true ] ),
			":Konsumen"         => Html::textInput( 'CusNama', $ro->CusNama, [ 'class' => 'form-control', 'readonly' => true ] ),
			/* button */
            ":btnSave"          => Html::button( '<i class="glyphicon glyphicon-floppy-disk"></i> Simpan', [ 'class' => 'btn btn-info btn-primary ', 'id' => 'btnSave' ] ),
            ":btnCancel"        => Html::Button( '<i class="fa fa-ban"></i> Batal', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel','style' => 'width:80px' ] ),
            ":btnAdd"              => Html::button( '<i class="fa fa-plus-circle"></i> Tambah', [ 'class' => 'btn btn-light btn-success ', 'id' => 'btnAdd' ,'style' => 'width:80px' ] ),
            ":btnEdit"         => Html::button( '<i class="fa fa-edit"></i> Edit', [ 'class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:80px' ] ),
            ":btnDaftar"           => Html::Button( '<i class="fa fa-list"></i> Daftar ', [ 'class' => 'btn btn-primary ', 'id' => 'btnDaftar' ,'style' => 'width:80px'] ),
		] );
	ActiveForm::end(); ?>
</div>
<?php

$this->registerJs( <<< JS
     $('#btnCancel').click(function (event) {	      
       $('#frm_tmotor_id').attr('action','{$url['cancel']}');
       $('#frm_tmotor_id').submit();
    });

     $('#btnSave').click(function (event) {	      
       $('#frm_tmotor_id').attr('action','{$url['save']}');
       $('#frm_tmotor_id').submit();
    });
    
    $('#MotorType_id').trigger('change');
        
    
JS);

?>