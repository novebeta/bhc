jQuery(function ($) {
    var ajaxRowOpt = {
        success: function (response, status, error) {
            $("#jqGrid_type").trigger("reloadGrid");
            $.notify(
                {
                    message: response.message
                },
                {
                    type: response.success ? 'success' : 'warning',
                    mouse_over: 'pause',
                    newest_on_top: true,
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    }
                }
            );
        },
        error: function (response, status, error) {
            $.notify(
                {
                    message: error + '<br> ' + response.responseJSON.message
                },
                {
                    type: 'danger',
                    mouse_over: 'pause',
                    newest_on_top: true,
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    }
                }
            );
        }
    };
    $.jgrid.defaults.styleUI = 'Bootstrap';
    $.jgrid.defaults.pgtext = "{0} of {1}";
    var url_string = $(location).attr('href');
    var url = new URL(url_string);
    var c = decodeURIComponent(url.searchParams.get("r"));
    var pageKey = c;
    var grid = $("#jqGrid");
    grid.jqGrid({
        url: 'index?r=tmotor/td',
        mtype: "POST",
        datatype: "json",
        jsonReader: {
            root: 'rows',
            page: 'currentPage',
            total: 'totalPages',
            records: 'rowsCount',
            id: 'rowId',
            repeatitems: false
        },
        rownumbers: true,
        colModel: [
            <?=$cols?>
        ],
        multiSort: true,
        sortname: setcmb.sortname,
        sortorder: setcmb.sortorder,
        search: true,
        postData: {
            query: function () {
                const search = {
                    cmbTxt1: $("#cmbTxt1").val(),
                    txt1: $("#txt1").val(),
                    cmbTxt2: '',
                    txt2: '',
                    check: 'off',
                    lokasi : $("#lokasi_id").val(),
                    tyStatus : $("#tyStatus_id").val(),
                    typeMotor : $("#type_id").val(),
                    warnaMotor : $("#warna_id").val(),
                    thn1 : $("#thn1_id").val(),
                    thn2 : $("#thn2_id").val(),
                    r: c
                };
                return JSON.stringify(search);
            }
        },
        altclass: 'row-zebra',
        altRows: true,
        pager: "#jqGridPager",
        viewrecords: true,
        rowNum: 15,
        rowList: [15, 30, 45, 60, 75],
        width: 780,
        height: 380,
        autowidth: false,
        shrinkToFit: false,
        styleUI: 'Bootstrap',
        // toppager:true,
        ajaxRowOptions: ajaxRowOpt,
        page: (document.cookie.match(new RegExp("(?:.*;)?\s*" + pageKey + "\s*=\s*([^;]+)(?:.*)?$")) || [, 1])[1],
        gridComplete: function () {
            $('a[data-confirm]').on('click', function (ev) {
                var href = $(this).attr('href');
                var rowId = $(this).attr('rowId');
                var dataConfirmModal = $('#dataConfirmModal');
                if (!dataConfirmModal.length) {
                    $('body').append('<div id="dataConfirmModal" class="modal bootstrap-dialog type-warning fade size-normal in" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true">' +
                        '<div class="modal-dialog"><div class="modal-content">' +
                        '<div class="modal-header"><div class="bootstrap-dialog-header"><div class="bootstrap-dialog-close-button" style="display: none;"><button class="close" data-dismiss="modal" aria-label="close">×</button></div><div class="bootstrap-dialog-title" id="w3_title">Confirmation</div></div></div>' +
                        '<form id="delete-form" method="post"><div class="modal-body"><div class="bootstrap-dialog-body"><div class="bootstrap-dialog-message">Are you sure you want to delete this item?</div></div></div>' +
                        '<div class="modal-footer"><div class="bootstrap-dialog-footer"><div class="bootstrap-dialog-footer-buttons"><button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><span class="fa fa-ban"></span>  Cancel</button><button class="btn btn-warning" type="submit" ><span class="glyphicon glyphicon-ok"></span> Ok</button></div></div></div></form></div></div></div>');
                }
                dataConfirmModal.find('.modal-body').text($(this).attr('data-confirm'));
                // $('#dataConfirmOK').attr('href', href);
                dataConfirmModal.modal({show: true});
                $('#delete-form').on('submit', function (e) {
                    e.preventDefault();
                    // $.notifyClose();
                    $.ajax({
                        url: href, //this is the submit URL
                        type: 'POST', //or POST
                        data: {
                            'oper': 'del',
                            'id': rowId
                        },
                        success: function (data) {
                            $.notify({
                                    message: data.message
                                },
                                {
                                    type: data.success ? 'success' : 'warning',
                                    allow_duplicates: false,
                                    mouse_over: 'pause',
                                    newest_on_top: true,
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    animate: {
                                        enter: 'animated fadeInDown',
                                        exit: 'animated fadeOutUp'
                                    }
                                });
                            $('#jqGrid_type').trigger('reloadGrid');
                        }
                    });
                    $('#dataConfirmModal').modal('hide');
                });
                return false;
            });
        }
    }).navGrid('#jqGridPager', {
        edit: false, add: false, del: false, search: false, refresh: false
    }).bindKeys();
    grid.jqGrid("setLabel", "rn", "No", {'text-align': 'center'});
    grid.jqGrid("setLabel", "actions", "Proses", {'text-align': 'center'});
    // grid warna


    function resizeGrid() {
        $('#jqGrid')
            .setGridWidth($('.panel-body').width(), false);
    }

    resizeGrid();
    $('.panel-body').resize(resizeGrid);

    function submitFilter(mode = null) {
        var url_string = $(location).attr('href');
        var url = new URL(url_string);
        var c = decodeURIComponent(url.searchParams.get("r"));
        const search = {
            cmbTxt1: $("#cmbTxt1").val(),
            txt1: $("#txt1").val(),
            cmbTxt2: '',
            txt2: '',
            check: 'off',
            lokasi : $("#lokasi_id").val(),
            tyStatus : $("#tyStatus_id").val(),
            typeMotor : $("#type_id").val(),
            warnaMotor : $("#warna_id").val(),
            thn1 : $("#thn1_id").val(),
            thn2 : $("#thn2_id").val(),
            r: c
        };
        if(mode == 'refresh'){
            search.refresh = true;
        }
        $('#jqGrid')
            .jqGrid('setGridParam', {
                sortname: setcmb.sortname,
                sortorder: setcmb.sortorder,
                postData: {
                    query: JSON.stringify(search)
                }
            }).trigger("reloadGrid");
        $('#gbox_grid .s-ico').css('display', 'none');
        $('#gbox_grid #jqgh_grid_id .s-ico').css('display', '');
        $('#gbox_grid #jqgh_grid_id .s-ico .ui-icon-triangle-1-s').removeClass('ui-state-disabled');
    }

    function findString(back) {
        var str = $('#txt-find-id').val();
        if (parseInt(navigator.appVersion) < 4) return;
        var strFound;
        if (window.find) {
            // CODE FOR BROWSERS THAT SUPPORT window.find
            strFound = self.find(str, 0, back);
            // if (!strFound) {
            //  strFound=self.find(str,0,1);
            //  while (self.find(str,0,1)) continue;
            // }
        } else if (navigator.appName.indexOf("Microsoft") != -1) {
            // EXPLORER-SPECIFIC CODE
            if (TRange != null) {
                TRange.collapse(false);
                strFound = TRange.findText(str);
                if (strFound) TRange.select();
            }
            if (TRange == null || strFound == 0) {
                TRange = self.document.body.createTextRange();
                strFound = TRange.findText(str);
                if (strFound) TRange.select();
            }
        } else if (navigator.appName == "Opera") {
            alert("Opera browsers not supported, sorry...")
            return;
        }
        if (!strFound) alert("String '" + str + "' not found!")
        return;
    }

    $('#formIndexMotor').on('submit', function (e) {
        e.preventDefault();
        submitFilter();
    });
    $('#searchBtn').on('click', function (e) {
        e.preventDefault();
        submitFilter();
    });
    // $('#find-up').on('click', function (e) {
    //     e.preventDefault();
    //     findString(1);
    // });
    // $('#find-down').on('click', function (e) {
    //     e.preventDefault();
    //     findString(0);
    // });
    $('.blur').on('blur', function (e) {
        submitFilter();
    });
    $('.chg').on('change', function (e) {
        setTimeout(submitFilter(), 500);
    });
    $('.blur').keydown(function (e) {
        if (e.keyCode == 13) {
            submitFilter();
        }
    });
    $('#btnRefresh').on('click', function (e) {
        e.preventDefault();
        submitFilter('refresh');
    });
    // jQuery("#data1").jqGrid("getRowData", 3)

});