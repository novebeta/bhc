<?php

use yii\helpers\Html;
use common\components\Custom;

/* @var $this yii\web\View */
/* @var $model aunit\models\Tmotor */

$this->title = 'Tambah Tmotor';
$this->params['breadcrumbs'][] = ['label' => 'Tmotors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tmotor-create">
    <?= $this->render('_form', [
        'model' => $model,
        'url' => [
            'save'    => Custom::url(\Yii::$app->controller->id.'/update'.$params ),
            'cancel'    => Custom::url(\Yii::$app->controller->id.'/' ),
        ]
    ]) ?>

</div>
