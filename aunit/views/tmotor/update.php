<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tmotor */
$this->title                   = str_replace('Menu','',\aunit\components\TUi::$actionMode).'Motor: ' . $model->MotorAutoN;
$this->params['breadcrumbs'][] = [ 'label' => 'Motor', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->MotorAutoN, 'url' => [ 'view', 'MotorAutoN' => $model->MotorAutoN, 'MotorNoMesin' => $model->MotorNoMesin ] ];
$this->params['breadcrumbs'][] = 'Edit';
$params                        = '&id=' . $id;
?>
<div class="tmotor-update">
	<?= $this->render( '_form', [
		'model' => $model,
		'ro'    => $ro,
		'url'   => [
			'save'   => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
			'cancel' => Custom::url( \Yii::$app->controller->id . '/' ),
		]
	] ) ?>
</div>
