<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model aunit\models\Ttkg */

$this->title = $model->KGNo;
$this->params['breadcrumbs'][] = ['label' => 'Ttkgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ttkg-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->KGNo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->KGNo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'KGNo',
            'KGTgl',
            'DKNo',
            'KGJenis',
            'KGNominal',
            'KGPengirim',
            'KGPenerima',
            'KGMemo:ntext',
            'KGStatus',
            'UserID',
        ],
    ]) ?>

</div>
