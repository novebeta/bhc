<?php

use aunit\assets\AppAsset;
use aunit\models\Ttkg;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                     = 'KG -> Kwitansi Kirim Tagih';
$this->params['breadcrumbs'][] = $this->title;
$arr                             = [
    'cmbTxt'    => [
        'KGNo'                    => 'No. Kwitansi Kirim Tagih',
        'DKNo'                    => 'No. DK',
        'CusNama'        => 'Nama Konsumen',
        'KGJenis'                => 'Jenis',
        'KGPengirim'            => 'Pengirim',
        'KGPenerima'            => 'Penerima',
        'KGMemo'                => 'Keterangan',
        'KGStatus'                => 'Status',
        'UserID'                => 'UserID',
        "CusAlamat"                => 'Alamat',
        "CusArea"                => 'CusArea',
        "TypeMotor"                => 'TypeMotor',
        "NoSinNoKa"                => 'NoSinNoKa',
    ],
    'cmbTgl'    => [
        'DATE(KGTgl)' => 'Tgl Kwitansi Kirim Tagih',
        'DKTgl' => 'Tgl DK',
    ],
    'cmbNum'    => [
        'KGNominal'         => 'KGNominal',
    ],
    'sortname'  => "KGNo",
    'sortorder' => 'asc'
];
AppAsset::register($this);
$this->registerJsVar('setcmb', $arr);
?>
<div class="panel panel-default">
    <div class="panel-body">
        <?= $this->render('../_search'); ?>
        <table id="jqGrid"></table>
        <div id="jqGridPager"></div>
    </div>
</div>
<?php
$this->registerJs(\aunit\components\TdUi::mainGridJs(
    \aunit\models\Ttkg::class,
    'KG -> Kwitansi Kirim Tagih',
    [
        'colGrid'            => Ttkg::colGrid(),
        // 'btnAdd_Disabled'    => true,
        // 'btnDelete_Disabled' => true,
        // 'btnUpdate_Disabled' => true,
        'url_view'           => \yii\helpers\Url::toRoute(['ttkg/index', 'action' => 'view']),
    ]
), \yii\web\View::POS_READY);
