<?php
use common\components\Custom;
$params                          = '&id=' . $id . '&action=update';
$cancel = \yii\helpers\Url::toRoute(['ttkg/cancel','id'=>$id,'action'=>'update','index'=>base64_encode('ttkg/index')]);
$this->title                     = 'Edit - KG -> Kwitansi Kirim Tagih';
$this->params[ 'breadcrumbs' ][] = [ 'label' => 'KG -> Kwitansi Kirim Tagih', 'url' => $cancel ];
$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="ttdk-update">
	<?= $this->render( '_form', [
		'model'             => $model,
		'dsTUang' => $data,
		'url'    => [
			'update' => Custom::url( \Yii::$app->controller->id . '/update' . $params ),
			'print'  => Custom::url( \Yii::$app->controller->id . '/print' . $params ),
			'cancel' => $cancel,
		]
	] ) ?>
</div>

