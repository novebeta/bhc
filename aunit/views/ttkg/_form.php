<?php

use aunit\models\Tdlokasi;
use aunit\assets\AppAsset;
use aunit\components\FormField;
use aunit\components\TUi;
use aunit\models\Ttkm;
use common\components\Custom;
use common\components\General;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttkm */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="ttkm-form">
    <?php
    $LokasiKodeCmb = ArrayHelper::map(Tdlokasi::find()->select(['LokasiKode'])
        ->orderBy('LokasiStatus, LokasiKode')
        ->asArray()->all(), 'LokasiKode', 'LokasiKode');
    $form          = ActiveForm::begin(['id' => 'form_ttkm_id', 'action' => $url['update']]);
    TUi::form(
        [
            'class' => "row-no-gutters",
            'items' => [
                [
                    'class' => "form-group col-md-16",
                    'items' => [
                        [
                            'class' => "form-group col-md-24",
                            'items' => [
                                ['class' => "col-sm-3", 'items' => ":LabelKMNo"],
                                ['class' => "col-sm-3", 'items' => ":NoKM"],
                                ['class' => "col-sm-6"],
                                ['class' => "col-sm-2 text-right", 'items' => ":LabelKMTgl"],
                                ['class' => "col-sm-3", 'items' => ":KMTgl"],
                                ['class' => "col-sm-3", 'items' => ":KMJam"],
                                ['class' => "col-sm-2"],
                                ['class' => "col-sm-3"],
                            ],
                        ],
                        [
                            'class' => "form-group col-md-24",
                            'items' => [
                                ['class' => "col-sm-3", 'items' => ":LabelCusNama"],
                                ['class' => "col-sm-9", 'items' => ":CusNama"],
                                ['class' => "col-sm-2 text-right", 'items' => ":LabelDKNo"],
                                ['class' => "col-sm-3", 'items' => ":KMLink"],
                                ['class' => "col-sm-4", 'items' => ":CariTanggal"],
                            ],
                        ],
                        [
                            'class' => "form-group col-md-24",
                            'items' => [
                                ['class' => "col-sm-3", 'items' => ":LabelCusAlamat"],
                                ['class' => "col-sm-21", 'items' => ":CusAlamat"],
                                ['class' => "col-sm-3", 'items' => ":LabelMotor"],
                                ['class' => "col-sm-21", 'style' => 'padding-top:3px', 'items' => ":txtMotor"],
                                ['class' => "col-sm-3", 'items' => ":LabelKMMemo"],
                                ['class' => "col-sm-21", 'style' => 'padding-top:3px', 'items' => ":KMMemo"],
                            ],
                        ],
                    ],
                ],
                ['class' => "col-sm-1"],
                [
                    'class' => "form-group col-md-7",
                    'items' => [
                        ['class' => "col-sm-24", 'items' => [
                            ['class' => "col-sm-6", 'style' => 'padding-top:3px', 'items' => ":LabelNominalKM"],
                            ['class' => "col-sm-18", 'style' => 'padding-top:3px', 'items' => ":KMNominal"],
                        ]],
                        ['class' => "col-sm-24"],
                        ['class' => "col-sm-24", 'items' => [
                            ['class' => "col-sm-6", 'style' => 'padding-top:3px', 'items' => ":LabelPengirim"],
                            ['class' => "col-sm-18", 'style' => 'padding-top:3px', 'items' => ":Pengirim"],
                        ]],
                        ['class' => "col-sm-24", 'items' => [
                            ['class' => "col-sm-6", 'style' => 'padding-top:3px', 'items' => ":LabelPenerima"],
                            ['class' => "col-sm-18", 'style' => 'padding-top:3px', 'items' => ":Penerima"],
                        ]],
                        ['class' => "col-sm-24", 'items' => [
                            ['class' => "col-sm-6", 'style' => 'padding-top:3px', 'items' => ":LabelJenis"],
                            ['class' => "col-sm-18", 'style' => 'padding-top:3px', 'items' => ":Jenis"],
                        ]],
                        ['class' => "col-sm-24", 'items' => [
                            ['class' => "col-sm-6", 'style' => 'padding-top:3px', 'items' => ":LabelStatus"],
                            ['class' => "col-sm-18", 'style' => 'padding-top:3px', 'items' => ":Status"],
                        ]]
                    ],
                ],
            ]
        ],
        [
            'class' => "row-no-gutters",
            'items' => [
                [
                    'class' => "col-md-12 pull-left",
                    'items' => $this->render('../_nav', [
                        'url' => $_GET['r'],
                        'options' => [
                            'KMPerson'   => 'Penyetor',
                            'KMNo'       => 'No Kas Masuk',
                            'KMJenis'    => 'Jenis',
                            'KMLink'     => 'No Transaksi',
                            'KMMemo'     => 'Keterangan',
                            'UserID'     => 'User',
                            'NoGL'       => 'No GL',
                        ],
                    ])
                ],
                [
                    'class' => "col-md-12 pull-right",
                    'items' => [
                        ['class' => "pull-right", 'items' => ":btnJurnal :btnPrint :btnAction"]
                    ]
                ]
            ]
        ],
        [
            /* label */
            ":btnJurnal"       => '',
            ":LabelKMNo"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">No KM</label>',
            ":LabelKMTgl"      => '<label class="control-label" style="margin: 0; padding: 6px 6px;">Tgl KM</label>',
            ":LabelNominalKM"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nominal</label>',
            ":LabelCusNama"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Konsumen</label>',
            ":LabelCusAlamat"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
            ":LabelMotor"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Motor</label>',
            ":LabelDKNo"       => '<label class="control-label" style="margin: 0; padding: 6px 6px;">No DK</label>',
            ":LabelKMMemo"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
            ":LabelPengirim"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Pengirim</label>',
            ":LabelPenerima"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Penerima</label>',
            ":LabelStatus"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',
            ":LabelJenis"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Jenis</label>',
            ":NoKM"            => Html::textInput('KGNoView', $dsTUang['KGNoView'], ['class' => 'form-control', 'readonly' => true, 'tabindex' => '1']) .
                Html::textInput('KGNo', $dsTUang['KGNo'], ['class' => 'hidden']).
                Html::textInput('KGTgl', $dsTUang['KGTgl'], ['class' => 'hidden']).
                Html::textInput('StatPrint', 1, ['class' => 'hidden statPrint']),
            ":KMTgl"           => FormField::dateInput(['name' => 'KGTglView', 'config' => ['value' => General::asDate($dsTUang['KGTgl']), 'readonly' => true]]),
            ":KMJam"           => FormField::timeInput(['name' => 'KGJam', 'config' => ['value' => General::asDate($dsTUang['KGTgl'], 'H:i:s'), 'readonly' => true]]),
            ":KMNominal"       => FormField::decimalInput(['name' => 'KGNominal', 'config' => ['value' => $dsTUang['KGNominal']]]),
            ":CusNama"         => Html::textInput('CusNama', $dsTUang['CusNama'], ['class' => 'form-control', 'readonly' => true]),
            ":KMLink"          => Html::textInput('DKNo', $dsTUang['DKNo'], ['class' => 'form-control', 'readonly' => true]),
            ":KMMemo"          => Html::textInput('KGMemo', $dsTUang['KGMemo'], ['class' => 'form-control', 'tabindex' => '5']),
            ":Jenis"         => Html::textInput('KGJenis', $dsTUang['KGJenis'], ['class' => 'form-control', 'readonly' => true]),
            ":Pengirim"        => Html::textInput('KGPengirim', $dsTUang['KGPengirim'], ['class' => 'form-control', 'tabindex' => '4']),
            ":Penerima"        => Html::textInput('KGPenerima', $dsTUang['KGPenerima'], ['class' => 'form-control', 'tabindex' => '4']),
            ":Status"        => Html::textInput('KGStatus', $dsTUang['KGStatus'], ['class' => 'form-control', 'tabindex' => '4', 'readonly' => true]),
            ":CusAlamat"       => Html::textarea('CusAlamat', $dsTUang['CusAlamat'], ['class' => 'form-control', 'readonly' => true]),
            ":txtMotor"        => Html::textarea('NoSinNoKa', $dsTUang['NoSinNoKa'], ['class' => 'form-control', 'readonly' => true]),
            ":CariTanggal"     => '<div class="input-group">' . FormField::dateInput(['name' => 'DKTgl', 'config' => ['value' => General::asDate($dsTUang['DKTgl']), 'readonly' => true]]) . '
                                    <span class="input-group-btn"><button id="BtnSearch" class="btn btn-default " '. Custom::viewClass().' type="button"><i class="glyphicon glyphicon-search" readonly></i></button></span></div>',
        ],
        [
            '_akses' => 'KG -> Kwitansi Kirim Tagih',
            'url_main' => 'ttkg',
            'url_id' => $_GET['id'] ?? '',
        ]
    );
    ActiveForm::end();
    ?>
</div>
<?php 
$urlAdd = Url::toRoute( [ 'ttkg/create','action' => 'create'  ] );
$urlIndex = Url::toRoute( [ 'ttkg/index'  ] );
$urlLoop = Url::toRoute( [ 'ttkg/select','KGNo'=>$dsTUang[ 'KGNo' ]] );
$urlLoopDK = Url::toRoute( [ 'ttkg/loop-dk' ] );
$urlSubmit         = Url::toRoute( [ 'ttkg/update', 'id' => base64_encode( $dsTUang[ 'KGNo' ] ), 'action' => 'update' ] );
$urlPrint = $url[ 'print' ];
$this->registerJs( <<< JS

    $('#btnSave').click(function (event) {
        let DKNo = $('input[name="DKNo"]').val();
        if (DKNo === "--" || DKNo === "") {
            bootbox.alert("Anda belum menentukan Data Konsumen");
            return '';
        }
        $('#form_ttkm_id').attr('action','$urlSubmit');
        $('#form_ttkm_id').submit();
      
    });

     $('#btnCancel').click(function (event) {	      
       $('#form_ttkm_id').attr('action','{$url['cancel']}');
       $('#form_ttkm_id').submit();
    });
     
     $('#btnAdd').click(function (event) {	      
       window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url['update']}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });

      $('#btnPrint').click(function (event) {	      
        $('#form_ttkm_id').attr('action','$urlPrint');
        $('#form_ttkm_id').attr('target','_blank');
        $('#form_ttkm_id').submit();
	  }); 
      
      $('#BtnSearch').click(function () {
        window.util.app.dialogListData.show({
                title: 'Daftar Program',
                url: '{$urlLoop}'
            },
            function (data) {
                console.log(data);
                if(data.data === undefined){
                    return;
                }
                $('[name="DKNo"]').val(data.data.DKNo);
                $('[name="KGPenerima"]').val(data.data.CusNama);
                $('[name="KGNominal"]').utilNumberControl().val(data.data.Nominal);
                $.ajax({
                    type: 'POST',
                    url: '$urlLoopDK',
                    data: $('#form_ttkm_id').serialize()
                }).then(function (cusData) { 
                    window.location.href = "{$url['update']}&oper=skip-load&KGNoView={$dsTUang['KGNoView']}"; 
                });
            })
    });
JS
);