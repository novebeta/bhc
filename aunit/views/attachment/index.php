<?php

use aunit\assets\AppAsset;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */

AppAsset::register($this);
$this->registerCssFile("https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css", [
    'position' => \yii\web\View::POS_HEAD
]);
$this->registerJsFile("https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js", [
    'position' => \yii\web\View::POS_END,
    'depends' => [\yii\web\JqueryAsset::class]
]);
$this->title                     = 'Attachment '.$titleId;
$this->params['breadcrumbs'][] = [
    'label' => $titleId,
    'url' => $redirectUrl
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div style="flex: 1 1 auto; margin-bottom: 0px;" class="box box-default">
    <span class="error"></span>
    <div class="box-body">
        <div class="row">
            
        </div>
        <div class="row">
            <div class='list-group gallery'>
                <div class='btnAction col-lg-6' style="height: 319px;padding: 5px;">
                    <button id="btnCamera" type="button" class="btn btn-default btn-lg" style="width: 100%;height: 50%;font-size: 2.2em;">
                        <span class="glyphicon glyphicon-camera" aria-hidden="true"></span> <br />Camera
                    </button>
                    <button id="btnUpload" type="button" class="btn btn-default btn-lg" style="width: 100%;height: 50%;font-size: 2.2em;">
                        <span class="glyphicon glyphicon-upload" aria-hidden="true"></span> <br />Upload
                    </button>
                </div>
                <?php foreach($listFiles as $file) :?>
                <div class='col-lg-6'>
                    <a class="thumbnail fancybox" rel="ligthbox" <?php echo $file['pathInfo']['extension'] == 'PDF' ? 'data-fancybox-type="iframe"' : '';?>  href="<?php echo $file['url']; ?>" style="color: unset;margin-bottom: 0px;">
                        <img class="img-responsive" alt="" src="<?php echo $file['pathInfo']['extension'] == 'PDF' ? getBaseUrl(). 'img/pdf.png' : $file['thumb']; ?>" />
                        <!-- text-right / end -->
                    </a>
                    <div>
                        <a href="<?php echo $file['url']; ?>" style="color:black;"><label class="control-label pull-left" style="margin-top: 0px;cursor:pointer;"><?php echo $file['pathInfo']['filename']; ?></label></a>
                        <?php if(\aunit\components\Menu::showOnlyHakAkses( [ 'Admin','Auditor' ] )): ?>
                        <a href="javascript:deletePhoto('<?php echo $file['pathInfo']['basename']; ?>');" class="pull-right" aria-label="Delete" data-pjax="0" data-confirm="Are you sure you want to delete this item?" data-method="post" style="padding-top: 0px;cursor:pointer;">Hapus</span></a>
                        <?php endif; ?>
                    </div>
                </div><!-- col-6 / end -->
                <?php endforeach; ?>
            </div> <!-- list-group / end -->
        </div> <!-- row / end -->
    </div>
</div>
<div class="modal" id="barcodeScanner" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-fullscreen" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div>
                    <video autoplay muted playsinline class="mediaElement" id="video" style="pointer-events: none;border: 1px solid gray;"></video>
                    <canvas id="canvas" class="mediaElement"  style="display: none"></canvas>
                    <img src="#" id="imageElmt" class="mediaElement" style="display: none"></img>
                </div>

                <div id="sourceSelectPanel" class="video-options" style="display:none">
                    <label for="sourceSelect">Change video source:</label>
                    <select id="sourceSelect" style="max-width:400px">
                    </select>
                </div>

                <code id="result" style="display: none;"></code>
            </div>
            <div class="modal-footer">
                <button type="button" id="resetButton" class="btn btn-primary" style="display: none;">Reset</button>
                <button type="button" id="uploadButton" class="btn btn-primary" style="display: none;">Upload</button>
                <button type="button" id="takeButton" class="btn btn-primary">Capture</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php
$urlUpload = Url::toRoute( [ 'attachment/upload'] );
$urlDelete = Url::toRoute( [ 'attachment/delete'] );
$this->registerCss(
    <<< CSS
.gallery
{
    margin-top: 20px;
}

@media only screen and (min-width: 768px){
    .modal-fullscreen{
      width: 75%;
    }
}
CSS
);
$this->registerJs(<<< JS
    const imagesDiv = document.querySelector("canvas");
    const videoElem = document.querySelector("video");
    const cameraOptions = document.querySelector('.video-options>select');
    const uploadButton = document.querySelector("button#uploadButton");
    const resetButton = document.querySelector("button#resetButton");
    const takeButton = document.querySelector("button#takeButton");
    const mime = ["application/pdf","image/jpeg"];
    let stream, dataURL;
    const constraints = {
        video: {
            deviceId: {
                exact: cameraOptions.value
            },
            width: {
                min: 1280,
                ideal: 1920,
                max: 2560,
            },
            height: {
                min: 720,
                ideal: 1080,
                max: 1440
            },
        }
    };

    $(document).ready(function(){
        // if (typeof MediaStreamTrackProcessor === "undefined") {
        //     const supportError =
        //         "MediaStreamTrackProcessor is not supported by your browser";
        //     const errorSpan = document.querySelector("span.error");
        //     errorSpan.style = "color:red;font-size:larger;font-weight:bold";
        //     errorSpan.innerText = supportError;
        //     console.error(supportError);
        // }

        try {
            navigator.mediaDevices.getUserMedia({video:true})
            .then(stream => {
                getCameraSelection();
                stream.getTracks().forEach((track) => track.stop());
            })
            .catch( (err) =>{
                console.log(err);
            });
        } catch (error) {
            console.error(error);
        }

        //https://github.com/fancyapps/fancyBox
        $(".fancybox").fancybox({
            openEffect: "none",
            closeEffect: "none"
        });

        $('.btnAction').css('height', $('.btnAction').width());

    });

    $('#btnCamera').on('click',function(){
        $('#barcodeScanner').modal('show');
        
    });

    $("#btnUpload").on("click",function (e){
        var fileDialog = $('<input type="file" accept="'+mime.join(',')+'">');
        fileDialog.click();
        fileDialog.on("change",onFileSelected);
        return false;
    });

    var onFileSelected = function(e){
        var file = $(this)[0].files[0];
        var mimeIndex = $.inArray(file.type, mime);
        if(mimeIndex == -1){
            $.notify({message: 'Only '+mime.join(',')+' files are allowed'},{
                    type: 'warning',
            });
            return;
        }
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            $.ajax({
            type: "POST",
            url: "{$urlUpload}",
            data: { 
                id:"{$id}",
                mime: mime[mimeIndex],
                imgBase64: reader.result
            }
            }).done(function(o) {
                document.location.reload();
            });
        };
        reader.onerror = function (error) {
            $.notify({message: error},{
                    type: 'warning',
            });
        };
    };

    const getCameraSelection = async () => {
        const devices = await navigator.mediaDevices.enumerateDevices();
        const videoDevices = devices.filter(device => device.kind === 'videoinput');
        const options = videoDevices.map(videoDevice => {
            return `<option value="\${videoDevice.deviceId}">\${videoDevice.label}</option>`;
        });
        cameraOptions.innerHTML = options.join('');
        if(videoDevices.length > 0){
            $('#sourceSelectPanel').show();
        }
    };

    cameraOptions.onchange = async () => {
        try {
            if(stream){
                stream.getTracks().forEach((track) => track.stop());
            }
            constraints.video.deviceId.exact = cameraOptions.value;
            stream = await navigator.mediaDevices.getUserMedia(constraints);
            videoElem.onplaying = () =>
                console.log("video playing stream:", videoElem.srcObject);
            videoElem.srcObject = stream;
        } catch (error) {
            console.error(error);
        }
    };

    $('#barcodeScanner').on('shown.bs.modal', function (e) {
        var width = $('#barcodeScanner div.modal-body').width();
        $('.mediaElement').width(width);
        cameraOptions.onchange();
        imagesDiv.innerHTML = '';
        videoElem.style.display = "";
        takeButton.style.display = "";
        uploadButton.style.display = "none";
        resetButton.style.display = "none";
        $('#imageElmt').css('display', 'none');
    });

    $('#barcodeScanner').on('hidden.bs.modal', function (e) {
        stream.getTracks().forEach((track) => track.stop());
    });

    $('#takeButton').on('click', async function (e) {
        // const [track] = stream.getVideoTracks();
        // const processor = new MediaStreamTrackProcessor(track);
        // const reader = await processor.readable.getReader();
        // const { value: frame, done } = await reader.read();
        // // value = frame
        // if (frame) {
        //     const bitmap = await createImageBitmap(frame);
        //     console.log(bitmap);
        //     canvas = document.createElement("canvas");
        //     canvas.setAttribute("id", "myCanvas");
        //     canvas.width = 320;
        //     // canvas.height = bitmap.height;
        //     const ctx = canvas.getContext("bitmaprenderer");
        //     ctx.transferFromImageBitmap(bitmap);
        //     imagesDiv.appendChild(canvas);
        //     videoElem.style.display = "none";
        //     takeButton.style.display = "none";
        //     uploadButton.style.display = "";
        //     resetButton.style.display = "";
        //     frame.close();
        // }
        
        imagesDiv.width = videoElem.videoWidth;
        imagesDiv.height = videoElem.videoHeight;
        imagesDiv.getContext('2d').drawImage(videoElem, 0, 0, videoElem.videoWidth, videoElem.videoHeight);
        imagesDiv.style.display = "block";
        dataURL = imagesDiv.toDataURL();
        imagesDiv.style.display = "none";
        $('#imageElmt').attr('src', dataURL).css('display', 'block');        
        videoElem.style.display = "none";
        takeButton.style.display = "none";
        uploadButton.style.display = "";
        resetButton.style.display = "";
        stream.getTracks().forEach((track) => track.stop());
    });

    $('#resetButton').on('click', async function (e) {
        $('#barcodeScanner').trigger('shown.bs.modal');
    });

    $('#uploadButton').on('click', async function (e) {
        $.ajax({
            type: "POST",
            url: "{$urlUpload}",
            data: { 
                id:"{$id}",
                imgBase64: dataURL
            }
        }).done(function(o) {
            document.location.reload();
        });
    });

    window.deletePhoto = function(params) {
        $.ajax({
            type: "POST",
            url: "{$urlDelete}",
            data: { 
                id:"{$id}",
                basename: params
            }
        }).done(function(o) {
            if(o.success == false){
                $.notify({message: o.message},{
                    type: 'warning',
                });
                return false;
            }
            document.location.reload();
        });
    }
JS,
    \yii\web\View::POS_READY
);
