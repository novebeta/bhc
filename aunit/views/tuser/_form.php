<?php
use aunit\assets\AppAsset;
use aunit\models\Tdlokasi;
use aunit\models\Tuakses;
use common\components\Custom;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
AppAsset::register( $this );
/* @var $this yii\web\View */
/* @var $model aunit\models\Ttgeneralledgerhd */
/* @var $form yii\widgets\ActiveForm */
$url[ 'cancel' ] = Url::toRoute( 'tuser/index' );
?>
<div class="ttgeneralledgerhd-form">
	<?php
	$form = ActiveForm::begin( [ 'id' => 'frm_user_id' ] );
	\aunit\components\TUi::form(
		[
			'class' => "row-no-gutters",
			'items' => [
				[ 'class' => "form-group col-md-24", 'style' => "",
				  'items' => [
					  [ 'class' => "col-sm-2", 'items' => ":LabelUserID" ],
					  [ 'class' => "col-sm-10", 'items' => ":UserID" ],
					  [ 'class' => "col-sm-1" ],
					  [ 'class' => "col-sm-3", 'items' => ":LabelOldPass" ],
					  [ 'class' => "col-sm-8", 'items' => ":old_pass" ],
				  ],
				],
				[ 'class' => "form-group col-md-24", 'style' => "",
				  'items' => [
					  [ 'class' => "col-sm-2", 'items' => ":LabelKodeAkses" ],
					  [ 'class' => "col-sm-10", 'items' => ":KodeAkses" ],
					  [ 'class' => "col-sm-1" ],
					  [ 'class' => "col-sm-3", 'items' => ":LabelNewPass" ],
					  [ 'class' => "col-sm-8", 'items' => ":new_pass" ],
				  ],
				],
				[ 'class' => "form-group col-md-24", 'style' => "",
				  'items' => [
					  [ 'class' => "col-sm-2", 'items' => ":LabelUserStatus" ],
					  [ 'class' => "col-sm-10", 'items' => ":UserStatus" ],
					  [ 'class' => "col-sm-1" ],
					  [ 'class' => "col-sm-3", 'items' => ":LabelConfirmPass" ],
					  [ 'class' => "col-sm-8", 'items' => ":confirm_pass" ],
				  ],
				],
				[ 'class' => "form-group col-md-24", 'style' => "",
				  'items' => [
					  [ 'class' => "col-sm-2", 'items' => ":LabelUserNama" ],
					  [ 'class' => "col-sm-11", 'items' => ":UserNama" ],
					  [ 'class' => "col-sm-1" ],
					  [ 'class' => "col-sm-2", 'items' => ":LabelLokasiKode" ],
					  [ 'class' => "col-sm-8", 'items' => ":LokasiKode" ],
				  ],
				],
				[ 'class' => "form-group col-md-24", 'style' => "",
				  'items' => [
					  [ 'class' => "col-sm-2", 'items' => ":LabelUserAlamat" ],
					  [ 'class' => "col-sm-11", 'items' => ":UserAlamat" ],
					  [ 'class' => "col-sm-1" ],
					  [ 'class' => "col-sm-2", 'items' => ":LabelUserTelepon" ],
					  [ 'class' => "col-sm-8", 'items' => ":UserTelepon" ],
				  ],
				],
				[ 'class' => "form-group col-md-24", 'style' => "",
				  'items' => [
					  [ 'class' => "col-sm-2", 'items' => ":LabelUserKeterangan" ],
					  [ 'class' => "col-sm-22", 'items' => ":UserKeterangan" ],
				  ],
				],
			]
		],
		[
			'class' => "pull-right",
			'items' => ":btnSave :btnCancel"
		],
		[
			/* label */
			":LabelUserID"         => '<label class="control-label" style="margin: 0; padding: 6px 0;">User ID</label>',
			":LabelKodeAkses"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Group Akses</label>',
			":LabelUserStatus"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',
			":LabelUserNama"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama</label>',
			":LabelUserAlamat"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
			":LabelUserKeterangan" => '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
			":LabelOldPass"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Password Lama</label>',
			":LabelNewPass"        => '<label class="control-label" style="margin: 0; padding: 6px 0;">Password Baru</label>',
			":LabelConfirmPass"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Konfirmasi Password</label>',
			":LabelLokasiKode"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">POS</label>',
			":LabelUserTelepon"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">No. Telepon</label>',
			/* component */
			":UserID"              => $form->field( $model, 'UserID', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true ] ),
			":KodeAkses"           => $form->field( $model, 'KodeAkses', [ 'template' => '{input}' ] )
			                               ->widget( Select2::className(), [
				                               'value'         => $model->KodeAkses,
				                               'theme'         => Select2::THEME_DEFAULT,
				                               'pluginOptions' => [ 'highlight' => true, ],
				                               'data'          => Tuakses::find()->select2(),
			                               ] ),
			":UserStatus"          => $form->field( $model, 'UserStatus', [ 'template' => '{input}' ] )->dropDownList( [ 'A' => 'AKTIF', 'N' => 'NON AKTIF' ], [ 'maxlength' => true, ] ),
			":UserNama"            => $form->field( $model, 'UserNama', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true, ] ),
			":UserAlamat"          => $form->field( $model, 'UserAlamat', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true, ] ),
			":UserKeterangan"      => $form->field( $model, 'UserKeterangan', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true, ] ),
			":old_pass"            => Html::passwordInput( 'old_pass', null, [ 'class' => 'form-control' ] ),
			":new_pass"            => Html::passwordInput( 'new_pass', null, [ 'class' => 'form-control' ] ),
			":confirm_pass"        => Html::passwordInput( 'confirm_pass', null, [ 'class' => 'form-control', 'style' => 'margin-top: -2px;' ] ),
			":LokasiKode"          => $form->field( $model, 'LokasiKode', [ 'template' => '{input}' ] )
			                               ->widget( Select2::classname(), [
				                               'value'         => $model->LokasiKode,
				                               'theme'         => Select2::THEME_DEFAULT,
				                               'pluginOptions' => [ 'highlight' => true ],
				                               'data'          => Tdlokasi::find()->LokasiKode(),
			                               ] ),
			":UserTelepon"         => $form->field( $model, 'UserTelepon', [ 'template' => '{input}' ] )->textInput( [ 'maxlength' => true, ] ),
			/* button */
			":btnSave"             => Html::submitButton( '<i class="glyphicon glyphicon-floppy-disk"></i> Simpan', [ 'class' => 'btn btn-info btn-primary ' . Custom::viewClass(), 'id' => 'btnSave' ] ),
			":btnCancel"           => Html::Button( '<i class="fa fa-ban"></i>&nbspBatal&nbsp&nbsp', [ 'class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel' ] ),
		]
	);
	ActiveForm::end();
	$this->registerJs( <<< JS
     $('#btnCancel').click(function (event) {	      
         window.location.href = "{$url['cancel']}";
    });
     
JS
	);
	?>
</div>