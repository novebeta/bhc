<?php
use common\components\Custom;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tuser */
$this->title                   = 'Tambah User';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="tuser-create">
	<?= $this->render( '_form', [
		'model' => $model,
        'url' => [
            'save'    => Custom::url(\Yii::$app->controller->id.'/create' ),
            'cancel'    => Custom::url(\Yii::$app->controller->id.'/' ),
        ]
	] ) ?>
</div>
