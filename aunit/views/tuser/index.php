<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Password';
$this->params['breadcrumbs'][] = $this->title;
$arr = [
	'cmbTxt'    => [
		'UserID' => 'UserID',
		'UserPass' => 'UserPass',
		'KodeAkses' => 'KodeAkses',
		'LokasiKode' => 'Kode Lokasi',
		'UserNama' => 'UserNama',
		'UserAlamat' => 'UserAlamat',
		'UserTelepon' => 'UserTelepon',
		'UserKeterangan' => 'UserKeterangan',
		'UserStatus' => 'UserStatus',
	],
	'cmbTgl'    => [
//		'SSTgl' => 'Tgl SS'
	],
	'cmbNum'    => [
	],
	'sortname'  => "",
	'sortorder' => ''
];
use aunit\assets\AppAsset;
AppAsset::register( $this );
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Tuser::className(),
	'User Password' ), \yii\web\View::POS_READY );