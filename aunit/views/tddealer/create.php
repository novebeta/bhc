<?php
use common\components\Custom;
use yii\helpers\Url;

$this->title                   = 'Tambah Dealer';
$this->params['breadcrumbs'][] = [ 'label' => 'Dealer', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tddealer-create">
	<?= $this->render( '_form', [
		'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tddealer/update', 'id' => $id ] ),
            'cancel' => Url::toRoute( [ 'tddealer/cancel', 'id' => $id ,'action' => 'create'] )
        ]
//        'url' => [
//            'save'    => Custom::url(\Yii::$app->controller->id.'/create' ),
//            'update'    => Custom::url(\Yii::$app->controller->id.'/update' ),
//            'cancel'    => Custom::url(\Yii::$app->controller->id.'/' ),
//        ]
	] ) ?>
</div>
