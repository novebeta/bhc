<?php
use common\components\Custom;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tddealer */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="tddealer-form">
    <?php
    $form = ActiveForm::begin(['id'=>'frm_tddealer_id']);
    \aunit\components\TUi::form(
        ['class' => "row-no-gutters",
            'items' => [
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelDealerKode"],
                        ['class' => "col-sm-5",'items' => ":DealerKode"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelDMain"],
                        ['class' => "col-sm-2",'items' => ":DMain"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelDAstra"],
                        ['class' => "col-sm-3",'items' => ":DAstra  "],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-1",'items' => ":LabelDealerStatus"],
                        ['class' => "col-sm-4",'items' => ":DealerStatus"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelDealerNama"],
                        ['class' => "col-sm-12",'items' => ":DealerNama"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelDealerContact"],
                        ['class' => "col-sm-7",'items' => ":DealerContact"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelDealerAlamat"],
                        ['class' => "col-sm-12",'items' => ":DealerAlamat"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelDealerKota"],
                        ['class' => "col-sm-7",'items' => ":DealerKota"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelDealerTelepon"],
                        ['class' => "col-sm-5",'items' => ":DealerTelepon"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-1",'items' => ":LabelDealerFax"],
                        ['class' => "col-sm-5",'items' => ":DealerFax"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelDealerProvinsi"],
                        ['class' => "col-sm-7",'items' => ":DealerProvinsi"],
                    ],
                ],
                ['class' => "form-group col-md-24",
                    'items' => [
                        ['class' => "col-sm-2",'items' => ":LabelDealerKeterangan"],
                        ['class' => "col-sm-12",'items' => ":DealerKeterangan"],
                        ['class' => "col-sm-1"],
                        ['class' => "col-sm-2",'items' => ":LabelDealerEmail"],
                        ['class' => "col-sm-7",'items' => ":DealerEmail"],
                    ],
                ],
            ]
        ],
        ['class' => "row-no-gutters",
            'items'  => [
                [
                    'class' => "col-md-12 pull-left",
                    'items' => $this->render( '../_nav', [
                        'url'=> $_GET['r'],
                        'options'=> [
                            'tddealer.DealerNama' => 'Nama Dealer',
                            'tddealer.DealerKode' => 'Kode Dealer',
                            'tddealer.DealerContact' => 'Kontak Person',
                            'tddealer.DealerEmail' => 'Email',
                            'tddealer.DealerKeterangan' => 'Keterangan',
                            'tddealer.DealerAlamat' => 'Alamat',
                            'tddealer.DealerKota' => 'Kota',
                            'tddealer.DealerProvinsi' => 'Provinsi',
                            'tddealer.DealerTelepon' => 'Telepon',
                            'tddealer.DealerFax' => 'Fax',
                            'tddealer.DealerStatus' => 'Status',
                        ],
                    ])
                ],
                ['class' => "col-md-12 pull-right",
                    'items'  => [
                        ['class' => "pull-right", 'items' => ":btnAction"],
                    ],
                ],
            ],
        ],
        [
	        ":LabelDealerKode"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode Dealer</label>',
	        ":LabelDMain"           => '<label class="control-label" style="margin: 0; padding: 6px 0;">Main Dealer</label>',
	        ":LabelDAstra"          => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kode Astra</label>',
	        ":LabelDealerStatus"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Status</label>',
	        ":LabelDealerNama"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Nama Dealer</label>',
	        ":LabelDealerContact"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kontak</label>',
	        ":LabelDealerAlamat"    => '<label class="control-label" style="margin: 0; padding: 6px 0;">Alamat</label>',
	        ":LabelDealerKota"      => '<label class="control-label" style="margin: 0; padding: 6px 0;">Kota</label>',
	        ":LabelDealerTelepon"   => '<label class="control-label" style="margin: 0; padding: 6px 0;">Telepon</label>',
	        ":LabelDealerFax"       => '<label class="control-label" style="margin: 0; padding: 6px 0;">Fax</label>',
	        ":LabelDealerProvinsi"  => '<label class="control-label" style="margin: 0; padding: 6px 0;">Provinsi</label>',
	        ":LabelDealerKeterangan"=> '<label class="control-label" style="margin: 0; padding: 6px 0;">Keterangan</label>',
	        ":LabelDealerEmail"     => '<label class="control-label" style="margin: 0; padding: 6px 0;">Email</label>',

	        ":DealerKode"       => $form->field($model, 'DealerKode', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
	        ":DMain"            => $form->field($model, 'DealerKodeDMain', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
	        ":DAstra"           => $form->field($model, 'DealerKodeDAstra', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
	        ":DealerStatus"     => $form->field($model, 'DealerStatus', ['template' => '{input}'])
                                    ->dropDownList([
                                        'A' => 'AKTIF',
                                        'N' => 'NON AKTIF',
                                        '1' => 'Dealer Utama',
                                    ], ['maxlength' => true]),
	        ":DealerNama"       => $form->field($model, 'DealerNama', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
	        ":DealerContact"    => $form->field($model, 'DealerContact', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
	        ":DealerAlamat"     => $form->field($model, 'DealerAlamat', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
	        ":DealerKota"       => $form->field($model, 'DealerKota', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
	        ":DealerTelepon"    => $form->field($model, 'DealerTelepon', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
	        ":DealerFax"        => $form->field($model, 'DealerFax', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
	        ":DealerProvinsi"   => $form->field($model, 'DealerProvinsi', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
	        ":DealerKeterangan" => $form->field($model, 'DealerKeterangan', ['template' => '{input}'])->textInput(['maxlength' => true, ]),
	        ":DealerEmail"      => $form->field($model, 'DealerEmail', ['template' => '{input}'])->textInput(['maxlength' => true, ]),

            ":btnSave" => Html::button('<i class="glyphicon glyphicon-floppy-disk"><br><br>Simpan</i>', ['class' => 'btn btn-info btn-primary ', 'style' => 'width:60px;height:60px', 'id' => 'btnSave' . Custom::viewClass()]),
            ":btnCancel" => Html::Button('<i class="fa fa-ban"><br><br>Batal</i>', ['class' => 'btn btn-danger btn-primary ', 'id' => 'btnCancel', 'style' => 'width:60px;height:60px']),
            ":btnAdd" => Html::button('<i class="fa fa-plus-circle"><br><br>Tambah</i>', ['class' => 'btn btn-light btn-success ', 'id' => 'btnAdd', 'style' => 'width:63px;height:60px']),
            ":btnEdit" => Html::button('<i class="fa fa-edit"><br><br>Edit</i>', ['class' => 'btn bg-purple', 'id' => 'btnEdit', 'style' => 'width:60px;height:60px']),
            ":btnDaftar" => Html::Button('<i class="fa fa-list"><br><br>Daftar</i>', ['class' => 'btn btn-primary', 'id' => 'btnDaftar', 'style' => 'width:60px;height:60px']),
        ],
        [
            '_akses' => 'Dealer',
            'url_main' => 'tddealer',
            'url_id' => $_GET['id'] ?? '',
        ]
    );
    ActiveForm::end();

    $urlAdd   = Url::toRoute( [ 'tddealer/create','id' => 'new','action'=>'create']);
    $urlIndex   = Url::toRoute( [ 'tddealer/index' ] );
    $this->registerJs( <<< JS
     
    $('#btnAdd').click(function (event) {	      
        window.location.href = '{$urlAdd}';
	  }); 
    
    $('#btnEdit').click(function (event) {	      
        window.location.href = '{$url[ 'update' ]}';
	  }); 
    
    $('#btnDaftar').click(function (event) {	      
        window.location.href = '{$urlIndex}';
	  });  

     $('#btnCancel').click(function (event) {	  
         window.location.href = '{$url['cancel']}';
    });

     $('#btnSave').click(function (event) {	      
       // $('#frm_tddealer_id').attr('action','{$url['update']}');
       $('#frm_tddealer_id').submit();
    });
     
     
     
    
JS);

    ?>
</div>

