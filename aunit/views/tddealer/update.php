<?php
use common\components\Custom;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model aunit\models\Tddealer */
$this->title                   = str_replace('Menu','',\aunit\components\TUi::$actionMode).'Dealer: ' . $model->DealerKode;
$this->params['breadcrumbs'][] = [ 'label' => 'Dealer', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->DealerKode, 'url' => [ 'view', 'id' => $model->DealerKode ] ];
$this->params['breadcrumbs'][] = 'Edit';
$params                          = '&id=' . $id . '&action=save';
?>
<div class="tddealer-update">
	<?= $this->render( '_form', [
		'model' => $model,
        'id'    => $id,
        'url'   => [
            'update'   => Url::toRoute( [ 'tddealer/update', 'id' => $id ] ),
            'cancel' => Url::toRoute( [ 'tddealer/cancel', 'id' => $id ,'action' => 'create'] )
        ]
//        'url' => [
//            'save'    => Custom::url(\Yii::$app->controller->id.'/update'.$params ),
//            'update'    => Custom::url(\Yii::$app->controller->id.'/update' ),
//            'cancel'    => Custom::url(\Yii::$app->controller->id.'/' ),
//        ]
	] ) ?>
</div>
