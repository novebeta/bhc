<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel aunit\models\TddealerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
use aunit\assets\AppAsset;
AppAsset::register($this);
$this->title = 'Dealer';
$this->params['breadcrumbs'][] = $this->title;
$arr                           = [
	'cmbTxt' => [
        'DealerNama' => 'Nama Dealer',
        'DealerKode' => 'Kode Dealer',
		'DealerContact' => 'Kontak Person',
        'DealerEmail' => 'Email',
        'DealerKeterangan' => 'Keterangan',
		'DealerAlamat' => 'Alamat',
		'DealerKota' => 'Kota',
		'DealerProvinsi' => 'Provinsi',
		'DealerTelepon' => 'Telepon',
		'DealerFax' => 'Fax',
		'DealerStatus' => 'Status',
	],
	'cmbTgl' => [
	],
	'cmbNum' => [
	],
	'sortname'  => "DealerStatus asc,DealerKode asc,DealerNama",
	'sortorder' => 'asc'
];
$this->registerJsVar( 'setcmb', $arr );
?>
    <div class="panel panel-default">
        <div class="panel-body">
			<?= $this->render( '../_search' ); ?>
            <table id="jqGrid"></table>
            <div id="jqGridPager"></div>
        </div>
    </div>
<?php
$this->registerJs( \aunit\components\TdUi::mainGridJs( \aunit\models\Tddealer::className() ,'Dealer'), \yii\web\View::POS_READY );