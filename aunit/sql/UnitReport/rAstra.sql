DELIMITER $$
DROP PROCEDURE IF EXISTS `rAstra`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rAstra`(IN xNmTgl VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,IN xAstra INT,IN xJenis VARCHAR(30),IN xSales VARCHAR(20),IN xPOS VARCHAR(20))
BEGIN
          /*
            Cara Akses SP :
            CALL rAstra(xNmTgl,xTgl1,xTgl2,xAstra,xJenis,xSales,xPOS)
            xNmTgl : TTDK,TTSK
            xAstra : 0 dan 1
           */
           SET xJenis = CONCAT(xJenis,'%');
           SET xSales = CONCAT(xSales,'%');
           SET xPOS = CONCAT(xPOS,'%');
           IF xAstra = 0 THEN
              IF xNmTgl = 'TTDK' THEN
                 SELECT  tmotor.MotorNoRangka AS NO_RANGKA, LEFT(tmotor.MotorNoMesin, 5) AS KODE_MESIN, 
                 RIGHT(tmotor.MotorNoMesin, 7) AS NO_MESIN, tdcustomer.CusNama AS NAMA1, '' AS NAMA2, CONCAT_WS('', tdcustomer.CusAlamat, '  RT ', tdcustomer.CusRT, ' RW ', tdcustomer.CusRW )   AS ALAMAT1, 
                 '' AS ALAMAT2, tdcustomer.CusKelurahan AS KELURAHAN, tdcustomer.CusKecamatan AS KECAMATAN, tdcustomer.CusKodePos AS KODEPOS, 
                 DATE_FORMAT(tdcustomer.CusTglLhr, '%d%m%Y') AS TGL_LAHIR, tdcustomer.CusSex AS SEX, tdcustomer.CusTelepon AS NO_TELEPON, 
                 CONCAT_WS('', tdcustomer.CusAlamat, '  ', tdcustomer.CusRT, '/', tdcustomer.CusRW)   AS ALAMAT_SURAT1, '' AS ALAMAT_SURAT2, 
                 tdcustomer.CusKelurahan AS KELURAHAN_SRT, tdcustomer.CusKecamatan AS KECAMATAN_SRT, tdcustomer.CusKodePos AS KODEPOS_SURAT, 
                 tdcustomer.ProvinsiAstra  AS PROPINSI, tdcustomer.KabupatenAstra AS KotaPemilik, ttdk.DKJenis AS JenisPenjualan, '' AS KodeDealer, 
                 tdcustomer.CusKTP AS NoKTP, tdcustomer.CusKodeKons AS KodeCustomer, tdcustomer.ProvinsiAstra  AS Propinsi_Surat, tdcustomer.KabupatenAstra AS Kota_Surat, 
                 tdcustomer.CusAgama AS Agama, tdcustomer.CusPekerjaan AS Pekerjaan, '' AS Pekerjaan_Lainnya, tdcustomer.CusPengeluaran AS Pengeluaran, tdcustomer.CusPendidikan AS Pendidikan, 
                 'N' AS NamaPenanggungJawab, tdcustomer.CusTelepon AS NoHP, 'Y-Yes' AS KebersediaanUntukDihubungi, ttdk.DKMerkSblmnya AS MerkMotorSebelumnya, ttdk.DKJenisSblmnya AS JenisMotorSebelumnya, 
                 ttdk.DKMotorUntuk AS MotorDigunakanUntuk, '' AS MotorDigunakanUntukLainnya, ttdk.DKMotorOleh AS MotorDigunakanOleh, '' AS MotorDigunakanOlehLainnya, 
                 CONCAT_WS('-',tdsales.SalesKodeAstra,tdsales.SalesHSOid ,UPPER(tdsales.SalesNama)) AS KodeSalesPerson, '' AS KodeMainDealer, ttdk.LeaseKode AS Leasing, (ttdk.DKDPTotal+ttdk.ProgramSubsidi) AS DownPayment, 
                 ttdk.DKAngsuran AS ActualCicilan ,(ttdk.DKTenor) AS Tenor, tdcustomer.CusEmail AS Email, tdcustomer.CusStatusRumah AS 'Status Rumah', tdcustomer.CusStatusHP AS 'Status No.HP', 
                 IFNULL(DATE_FORMAT(ttss.SSTGL, '%d%m%Y'), IFNULL(DATE_FORMAT(ttpd.PDTGL, '%d%m%Y'), DATE_FORMAT('1900-01-01', '%d%m%Y'))) AS 'TGL_IN DLR', 
                 '1-Regular' AS StatusKonsumen, ''AS ROData, ttdk.DKNo AS NoBillingDLR, DATE_FORMAT(ttdk.DKTgl, '%d%m%Y') AS TglBeliDLR, 
                 '' AS NoPOLeasing, DATE_FORMAT('1900-01-01', '%d%m%Y') AS TglPOLeasing,  
                 IFNULL(ttss.SupKode, IFNULL(ttpd.DealerKode, '--')) AS 'Asal'  
                 FROM ttdk 
                 LEFT OUTER JOIN 
                 (SELECT tdcustomer.CusKode, tdcustomer.CusKTP, tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusProvinsi, tdcustomer.CusKabupaten, tdcustomer.CusKecamatan, tdcustomer.CusKelurahan, tdcustomer.CusKodePos, tdcustomer.CusTelepon, tdcustomer.CusSex, tdcustomer.CusTempatLhr, tdcustomer.CusTglLhr, tdcustomer.CusAgama, tdcustomer.CusPekerjaan, tdcustomer.CusPendidikan, tdcustomer.CusPengeluaran, tdcustomer.CusTglBeli, tdcustomer.CusKeterangan, tdcustomer.CusEmail, tdcustomer.CusKodeKons, tdarea.KabupatenAstra, tdarea.ProvinsiAstra, tdcustomer.CusStatusRumah, tdcustomer.CusStatusHP 
                   FROM tdcustomer INNER JOIN 
                   (SELECT tdarea.Kabupaten, tdarea.KabupatenAstra, tdarea.ProvinsiAstra FROM tdarea GROUP BY tdarea.Kabupaten ) tdarea 
                    ON tdcustomer.CusKabupaten = tdarea.Kabupaten) tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                    LEFT OUTER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
                    LEFT OUTER JOIN ttss ON tmotor.SSNo = ttss.SSNo 
                    LEFT OUTER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo 
                    LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode 
                    LEFT OUTER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo 
                    WHERE ttdk.DKTgl BETWEEN xTgl1 AND xTgl2
                    AND ttdk.DKJenis LIKE xJenis
                    AND ttdk.SalesKode LIKE xSales
                    AND IFNULL(ttsk.LokasiKode,'') LIKE xPOS
                    AND tmotor.MotorNoMesin IS NOT NULL;
              ELSE
                 SELECT  tmotor.MotorNoRangka AS NO_RANGKA, LEFT(tmotor.MotorNoMesin, 5) AS KODE_MESIN, 
                 RIGHT(tmotor.MotorNoMesin, 7) AS NO_MESIN, tdcustomer.CusNama AS NAMA1, '' AS NAMA2, CONCAT_WS('', tdcustomer.CusAlamat, '  RT ', tdcustomer.CusRT, ' RW ', tdcustomer.CusRW )   AS ALAMAT1, 
                 '' AS ALAMAT2, tdcustomer.CusKelurahan AS KELURAHAN, tdcustomer.CusKecamatan AS KECAMATAN, tdcustomer.CusKodePos AS KODEPOS, 
                 DATE_FORMAT(tdcustomer.CusTglLhr, '%d%m%Y') AS TGL_LAHIR, tdcustomer.CusSex AS SEX, tdcustomer.CusTelepon AS NO_TELEPON, 
                 CONCAT_WS('', tdcustomer.CusAlamat, '  ', tdcustomer.CusRT, '/', tdcustomer.CusRW)   AS ALAMAT_SURAT1, '' AS ALAMAT_SURAT2, 
                 tdcustomer.CusKelurahan AS KELURAHAN_SRT, tdcustomer.CusKecamatan AS KECAMATAN_SRT, tdcustomer.CusKodePos AS KODEPOS_SURAT, 
                 tdcustomer.ProvinsiAstra  AS PROPINSI, tdcustomer.KabupatenAstra AS KotaPemilik, ttdk.DKJenis AS JenisPenjualan, '' AS KodeDealer, 
                 tdcustomer.CusKTP AS NoKTP, tdcustomer.CusKodeKons AS KodeCustomer, tdcustomer.ProvinsiAstra  AS Propinsi_Surat, tdcustomer.KabupatenAstra AS Kota_Surat, 
                 tdcustomer.CusAgama AS Agama, tdcustomer.CusPekerjaan AS Pekerjaan, '' AS Pekerjaan_Lainnya, tdcustomer.CusPengeluaran AS Pengeluaran, tdcustomer.CusPendidikan AS Pendidikan, 
                 'N' AS NamaPenanggungJawab, tdcustomer.CusTelepon AS NoHP, 'Y-Yes' AS KebersediaanUntukDihubungi, ttdk.DKMerkSblmnya AS MerkMotorSebelumnya, ttdk.DKJenisSblmnya AS JenisMotorSebelumnya, 
                 ttdk.DKMotorUntuk AS MotorDigunakanUntuk, '' AS MotorDigunakanUntukLainnya, ttdk.DKMotorOleh AS MotorDigunakanOleh, '' AS MotorDigunakanOlehLainnya, 
                 CONCAT_WS('-',tdsales.SalesKodeAstra,tdsales.SalesHSOid ,UPPER(tdsales.SalesNama)) AS KodeSalesPerson, '' AS KodeMainDealer, ttdk.LeaseKode AS Leasing, (ttdk.DKDPTotal+ttdk.ProgramSubsidi) AS DownPayment, 
                 ttdk.DKAngsuran AS ActualCicilan ,(ttdk.DKTenor) AS Tenor, tdcustomer.CusEmail AS Email, tdcustomer.CusStatusRumah AS 'Status Rumah', tdcustomer.CusStatusHP AS 'Status No.HP', 
                 IFNULL(DATE_FORMAT(ttss.SSTGL, '%d%m%Y'), IFNULL(DATE_FORMAT(ttpd.PDTGL, '%d%m%Y'), DATE_FORMAT('1900-01-01', '%d%m%Y'))) AS 'TGL_IN DLR', 
                 '1-Regular' AS StatusKonsumen, ''AS ROData, ttdk.DKNo AS NoBillingDLR, DATE_FORMAT(ttdk.DKTgl, '%d%m%Y') AS TglBeliDLR, 
                 '' AS NoPOLeasing, DATE_FORMAT('1900-01-01', '%d%m%Y') AS TglPOLeasing,  
                 IFNULL(ttss.SupKode, IFNULL(ttpd.DealerKode, '--')) AS 'Asal'  
                 FROM ttdk 
                 LEFT OUTER JOIN 
                 (SELECT tdcustomer.CusKode, tdcustomer.CusKTP, tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusProvinsi, tdcustomer.CusKabupaten, tdcustomer.CusKecamatan, tdcustomer.CusKelurahan, tdcustomer.CusKodePos, tdcustomer.CusTelepon, tdcustomer.CusSex, tdcustomer.CusTempatLhr, tdcustomer.CusTglLhr, tdcustomer.CusAgama, tdcustomer.CusPekerjaan, tdcustomer.CusPendidikan, tdcustomer.CusPengeluaran, tdcustomer.CusTglBeli, tdcustomer.CusKeterangan, tdcustomer.CusEmail, tdcustomer.CusKodeKons, tdarea.KabupatenAstra, tdarea.ProvinsiAstra, tdcustomer.CusStatusRumah, tdcustomer.CusStatusHP 
                   FROM tdcustomer INNER JOIN 
                   (SELECT tdarea.Kabupaten, tdarea.KabupatenAstra, tdarea.ProvinsiAstra FROM tdarea GROUP BY tdarea.Kabupaten ) tdarea 
                    ON tdcustomer.CusKabupaten = tdarea.Kabupaten) tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                    LEFT OUTER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
                    LEFT OUTER JOIN ttss ON tmotor.SSNo = ttss.SSNo 
                    LEFT OUTER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo 
                    LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode 
                    LEFT OUTER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo 
                    WHERE ttsk.SKTgl BETWEEN xTgl1 AND xTgl2
                    AND ttdk.DKJenis LIKE xJenis
                    AND ttdk.SalesKode LIKE xSales
                    AND IFNULL(ttsk.LokasiKode,'') LIKE xPOS
                    AND tmotor.MotorNoMesin IS NOT NULL;
              END IF;
           ELSE
              IF xNmTgl = 'TTDK' THEN
                 SELECT  tmotor.MotorNoRangka AS NO_RANGKA, LEFT(tmotor.MotorNoMesin, 5) AS KODE_MESIN, 
                 RIGHT(tmotor.MotorNoMesin, 7) AS NO_MESIN, tdcustomer.CusNama AS NAMA1, '' AS NAMA2, CONCAT_WS('', tdcustomer.CusAlamat, '  RT ', tdcustomer.CusRT, ' RW ', tdcustomer.CusRW )   AS ALAMAT1, 
                 '' AS ALAMAT2, tdcustomer.CusKelurahan AS KELURAHAN, tdcustomer.CusKecamatan AS KECAMATAN, tdcustomer.CusKodePos AS KODEPOS, 
                 DATE_FORMAT(tdcustomer.CusTglLhr, '%d%m%Y') AS TGL_LAHIR, tdcustomer.CusSex AS SEX, tdcustomer.CusTelepon AS NO_TELEPON, 
                 CONCAT_WS('', tdcustomer.CusAlamat, '  ', tdcustomer.CusRT, '/', tdcustomer.CusRW)   AS ALAMAT_SURAT1, '' AS ALAMAT_SURAT2, 
                 tdcustomer.CusKelurahan AS KELURAHAN_SRT, tdcustomer.CusKecamatan AS KECAMATAN_SRT, tdcustomer.CusKodePos AS KODEPOS_SURAT, 
                 tdcustomer.ProvinsiAstra  AS PROPINSI, tdcustomer.KabupatenAstra AS KotaPemilik, ttdk.DKJenis AS JenisPenjualan, '' AS KodeDealer, 
                 tdcustomer.CusKTP AS NoKTP, tdcustomer.CusKodeKons AS KodeCustomer, tdcustomer.ProvinsiAstra  AS Propinsi_Surat, tdcustomer.KabupatenAstra AS Kota_Surat, 
                 tdcustomer.CusAgama AS Agama, tdcustomer.CusPekerjaan AS Pekerjaan, '' AS Pekerjaan_Lainnya, tdcustomer.CusPengeluaran AS Pengeluaran, tdcustomer.CusPendidikan AS Pendidikan, 
                 'N' AS NamaPenanggungJawab, tdcustomer.CusTelepon AS NoHP, 'Y-Yes' AS KebersediaanUntukDihubungi, ttdk.DKMerkSblmnya AS MerkMotorSebelumnya, ttdk.DKJenisSblmnya AS JenisMotorSebelumnya, 
                 ttdk.DKMotorUntuk AS MotorDigunakanUntuk, '' AS MotorDigunakanUntukLainnya, ttdk.DKMotorOleh AS MotorDigunakanOleh, '' AS MotorDigunakanOlehLainnya, 
                 CONCAT_WS('-',tdsales.SalesKodeAstra,tdsales.SalesHSOid ,UPPER(tdsales.SalesNama)) AS KodeSalesPerson, '' AS KodeMainDealer, ttdk.LeaseKode AS Leasing, (ttdk.DKDPTotal+ttdk.ProgramSubsidi) AS DownPayment, 
                 ttdk.DKAngsuran AS ActualCicilan ,(ttdk.DKTenor) AS Tenor, tdcustomer.CusEmail AS Email, tdcustomer.CusStatusRumah AS 'Status Rumah', tdcustomer.CusStatusHP AS 'Status No.HP', 
                 IFNULL(DATE_FORMAT(ttss.SSTGL, '%d%m%Y'), IFNULL(DATE_FORMAT(ttpd.PDTGL, '%d%m%Y'), DATE_FORMAT('1900-01-01', '%d%m%Y'))) AS 'TGL_IN DLR', 
                 '1-Regular' AS StatusKonsumen, ''AS ROData, ttdk.DKNo AS NoBillingDLR, DATE_FORMAT(ttdk.DKTgl, '%d%m%Y') AS TglBeliDLR, 
                 '' AS NoPOLeasing, DATE_FORMAT('1900-01-01', '%d%m%Y') AS TglPOLeasing,  
                 IFNULL(ttss.SupKode, IFNULL(ttpd.DealerKode, '--')) AS 'Asal'  
                 FROM ttdk 
                 LEFT OUTER JOIN 
                 (SELECT tdcustomer.CusKode, tdcustomer.CusKTP, tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusProvinsi, tdcustomer.CusKabupaten, tdcustomer.CusKecamatan, tdcustomer.CusKelurahan, tdcustomer.CusKodePos, tdcustomer.CusTelepon, tdcustomer.CusSex, tdcustomer.CusTempatLhr, tdcustomer.CusTglLhr, tdcustomer.CusAgama, tdcustomer.CusPekerjaan, tdcustomer.CusPendidikan, tdcustomer.CusPengeluaran, tdcustomer.CusTglBeli, tdcustomer.CusKeterangan, tdcustomer.CusEmail, tdcustomer.CusKodeKons, tdarea.KabupatenAstra, tdarea.ProvinsiAstra, tdcustomer.CusStatusRumah, tdcustomer.CusStatusHP 
                   FROM tdcustomer INNER JOIN 
                   (SELECT tdarea.Kabupaten, tdarea.KabupatenAstra, tdarea.ProvinsiAstra FROM tdarea GROUP BY tdarea.Kabupaten ) tdarea 
                    ON tdcustomer.CusKabupaten = tdarea.Kabupaten) tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                    LEFT OUTER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
                    LEFT OUTER JOIN ttss ON tmotor.SSNo = ttss.SSNo 
                    LEFT OUTER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo 
                    LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode 
                    LEFT OUTER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo 
                    WHERE ttdk.DKTgl BETWEEN xTgl1 AND xTgl2
                    AND ttdk.DKJenis LIKE xJenis
                    AND ttdk.SalesKode LIKE xSales
                    AND IFNULL(ttsk.LokasiKode,'') LIKE xPOS
                    AND tmotor.MotorNoMesin IS NOT NULL
                    AND tmotor.SSNo <> '--' AND ttss.SupKode LIKE '%HSO%';
              ELSE
                 SELECT  tmotor.MotorNoRangka AS NO_RANGKA, LEFT(tmotor.MotorNoMesin, 5) AS KODE_MESIN, 
                 RIGHT(tmotor.MotorNoMesin, 7) AS NO_MESIN, tdcustomer.CusNama AS NAMA1, '' AS NAMA2, CONCAT_WS('', tdcustomer.CusAlamat, '  RT ', tdcustomer.CusRT, ' RW ', tdcustomer.CusRW )   AS ALAMAT1, 
                 '' AS ALAMAT2, tdcustomer.CusKelurahan AS KELURAHAN, tdcustomer.CusKecamatan AS KECAMATAN, tdcustomer.CusKodePos AS KODEPOS, 
                 DATE_FORMAT(tdcustomer.CusTglLhr, '%d%m%Y') AS TGL_LAHIR, tdcustomer.CusSex AS SEX, tdcustomer.CusTelepon AS NO_TELEPON, 
                 CONCAT_WS('', tdcustomer.CusAlamat, '  ', tdcustomer.CusRT, '/', tdcustomer.CusRW)   AS ALAMAT_SURAT1, '' AS ALAMAT_SURAT2, 
                 tdcustomer.CusKelurahan AS KELURAHAN_SRT, tdcustomer.CusKecamatan AS KECAMATAN_SRT, tdcustomer.CusKodePos AS KODEPOS_SURAT, 
                 tdcustomer.ProvinsiAstra  AS PROPINSI, tdcustomer.KabupatenAstra AS KotaPemilik, ttdk.DKJenis AS JenisPenjualan, '' AS KodeDealer, 
                 tdcustomer.CusKTP AS NoKTP, tdcustomer.CusKodeKons AS KodeCustomer, tdcustomer.ProvinsiAstra  AS Propinsi_Surat, tdcustomer.KabupatenAstra AS Kota_Surat, 
                 tdcustomer.CusAgama AS Agama, tdcustomer.CusPekerjaan AS Pekerjaan, '' AS Pekerjaan_Lainnya, tdcustomer.CusPengeluaran AS Pengeluaran, tdcustomer.CusPendidikan AS Pendidikan, 
                 'N' AS NamaPenanggungJawab, tdcustomer.CusTelepon AS NoHP, 'Y-Yes' AS KebersediaanUntukDihubungi, ttdk.DKMerkSblmnya AS MerkMotorSebelumnya, ttdk.DKJenisSblmnya AS JenisMotorSebelumnya, 
                 ttdk.DKMotorUntuk AS MotorDigunakanUntuk, '' AS MotorDigunakanUntukLainnya, ttdk.DKMotorOleh AS MotorDigunakanOleh, '' AS MotorDigunakanOlehLainnya, 
                 CONCAT_WS('-',tdsales.SalesKodeAstra,tdsales.SalesHSOid ,UPPER(tdsales.SalesNama)) AS KodeSalesPerson, '' AS KodeMainDealer, ttdk.LeaseKode AS Leasing, (ttdk.DKDPTotal+ttdk.ProgramSubsidi) AS DownPayment, 
                 ttdk.DKAngsuran AS ActualCicilan ,(ttdk.DKTenor) AS Tenor, tdcustomer.CusEmail AS Email, tdcustomer.CusStatusRumah AS 'Status Rumah', tdcustomer.CusStatusHP AS 'Status No.HP', 
                 IFNULL(DATE_FORMAT(ttss.SSTGL, '%d%m%Y'), IFNULL(DATE_FORMAT(ttpd.PDTGL, '%d%m%Y'), DATE_FORMAT('1900-01-01', '%d%m%Y'))) AS 'TGL_IN DLR', 
                 '1-Regular' AS StatusKonsumen, ''AS ROData, ttdk.DKNo AS NoBillingDLR, DATE_FORMAT(ttdk.DKTgl, '%d%m%Y') AS TglBeliDLR, 
                 '' AS NoPOLeasing, DATE_FORMAT('1900-01-01', '%d%m%Y') AS TglPOLeasing,  
                 IFNULL(ttss.SupKode, IFNULL(ttpd.DealerKode, '--')) AS 'Asal'  
                 FROM ttdk 
                 LEFT OUTER JOIN 
                 (SELECT tdcustomer.CusKode, tdcustomer.CusKTP, tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusProvinsi, tdcustomer.CusKabupaten, tdcustomer.CusKecamatan, tdcustomer.CusKelurahan, tdcustomer.CusKodePos, tdcustomer.CusTelepon, tdcustomer.CusSex, tdcustomer.CusTempatLhr, tdcustomer.CusTglLhr, tdcustomer.CusAgama, tdcustomer.CusPekerjaan, tdcustomer.CusPendidikan, tdcustomer.CusPengeluaran, tdcustomer.CusTglBeli, tdcustomer.CusKeterangan, tdcustomer.CusEmail, tdcustomer.CusKodeKons, tdarea.KabupatenAstra, tdarea.ProvinsiAstra, tdcustomer.CusStatusRumah, tdcustomer.CusStatusHP 
                   FROM tdcustomer INNER JOIN 
                   (SELECT tdarea.Kabupaten, tdarea.KabupatenAstra, tdarea.ProvinsiAstra FROM tdarea GROUP BY tdarea.Kabupaten ) tdarea 
                    ON tdcustomer.CusKabupaten = tdarea.Kabupaten) tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                    LEFT OUTER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
                    LEFT OUTER JOIN ttss ON tmotor.SSNo = ttss.SSNo 
                    LEFT OUTER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo 
                    LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode 
                    LEFT OUTER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo 
                    WHERE ttsk.SKTgl BETWEEN xTgl1 AND xTgl2
                    AND ttdk.DKJenis LIKE xJenis
                    AND ttdk.SalesKode LIKE xSales
                    AND IFNULL(ttsk.LokasiKode,'') LIKE xPOS
                    AND tmotor.MotorNoMesin IS NOT NULL 
                    AND tmotor.SSNo <> '--' AND ttss.SupKode LIKE '%HSO%';
              END IF;
           END IF;
	END$$

DELIMITER ;

