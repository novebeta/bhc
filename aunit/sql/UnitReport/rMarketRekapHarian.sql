DELIMITER $$
DROP PROCEDURE IF EXISTS `rMarketRekapHarian`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rMarketRekapHarian`(IN xTipe VARCHAR(25),IN xStatTgl VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,IN xJenis VARCHAR(10),IN xArea VARCHAR(10),IN xDelNol VARCHAR(1),IN xSales VARCHAR(30),IN xLokasi VARCHAR(30))
BEGIN
/*
       xTipe    : "harian1" -> Harian
                  "harian2" -> Fill Harian
                  "bulanan1" -> Bulanan
                  "bulanan2" -> Fill Bulanan
                  

       Cara Akses :

*/
       DECLARE xResultJenis VARCHAR(100);
       DECLARE xResultStatTgl VARCHAR(100);
       DECLARE xResultArea VARCHAR(100);

       /* SelectJenisJual */
       IF xJenis = "%" THEN
          SET xResultJenis = " AND ttdk.LeaseKode LIKE '%' ";
       ELSE
          IF xJenis = "Sudah" THEN
             SET xResultJenis = " AND (ttdk.LeaseKode = 'Tunai' OR ttdk.LeaseKode = 'KDS') ";
          ELSE
             SET xResultJenis = " AND (ttdk.LeaseKode <> 'Tunai' OR ttdk.LeaseKode <> 'KDS') ";
          END IF;
       END IF;
       
       /* SelectDK_SK */
       IF xStatTgl = "ttdk.DKTgl" THEN
          SET xResultStatTgl = " LEFT OUTER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo ";
       ELSE
          SET xResultStatTgl = " INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo ";
       END IF;
       
       /* Select Area */
       SELECT Kabupaten INTO @xKab FROM tdarea WHERE AreaStatus = '1' LIMIT 0;
       IF xArea = "%" THEN
          SET xResultArea = " AND tdcustomer.CusKabupaten LIKE '%' ";
       ELSE
          IF xArea = "IN" THEN
             SET xResultArea = CONCAT(" AND tdcustomer.CusKabupaten = '",@xKab,"' ");
          ELSE
             SET xResultArea = CONCAT(" AND tdcustomer.CusKabupaten <> '",@xKab,"' ");
          END IF;
       END IF;
       
       /* RETAIL HARIAN */
       IF xTipe = "harian1" THEN 
          SET @MyQuery = "(SELECT '01' AS MyGroup, MotorType, TypeStatus,
                          0.0 AS '01',0.0 AS '02',0.0 AS '03',0.0 AS '04',0.0 AS '05',0.0 AS '06',0.0 AS '07',0.0 AS '08',0.0 AS '09',0.0 AS '10', 
                          0.0 AS '11',0.0 AS '12',0.0 AS '13',0.0 AS '14',0.0 AS '15',0.0 AS '16',0.0 AS '17',0.0 AS '18',0.0 AS '19',0.0 AS '20', 
                          0.0 AS '21',0.0 AS '22',0.0 AS '23',0.0 AS '24',0.0 AS '25',0.0 AS '26',0.0 AS '27',0.0 AS '28',0.0 AS '29',0.0 AS '30',0.0 AS '31','' AS LokasiKode, 
                          MotorNama, tdmotortype.MotorKategori, MotorGroup FROM tdmotortype INNER JOIN tdmotorkategori ON tdmotortype.MotorKategori = tdmotorkategori.MotorKategori 
                          ORDER BY  MotorNo, tdmotortype.MotorKategori, tdmotortype.MotorType)
                          UNION
                          (SELECT '02' AS MyGroup, tdmotorkategori.MotorGroup AS MotorType, 'A' AS TypeStatus,
                          0.0 AS '01',0.0 AS '02',0.0 AS '03',0.0 AS '04',0.0 AS '05',0.0 AS '06',0.0 AS '07',0.0 AS '08',0.0 AS '09',0.0 AS '10', 
                          0.0 AS '11',0.0 AS '12',0.0 AS '13',0.0 AS '14',0.0 AS '15',0.0 AS '16',0.0 AS '17',0.0 AS '18',0.0 AS '19',0.0 AS '20', 
                          0.0 AS '21',0.0 AS '22',0.0 AS '23',0.0 AS '24',0.0 AS '25',0.0 AS '26',0.0 AS '27',0.0 AS '28',0.0 AS '29',0.0 AS '30',0.0 AS '31','' AS LokasiKode,
                          tdmotorkategori.MotorKategori AS MotorNama, tdmotorkategori.MotorGroup AS MotorKategori, tdmotorkategori.MotorGroup AS MotorGroup 
                          FROM tdmotorkategori ORDER BY MotorNo, MotorKategori)";
                          
       END IF;


       /* FILL HARIAN */
       IF xTipe = "harian2" THEN 
          SET @MyQuery = CONCAT("(SELECT tdmotortype.MotorType, tdmotortype.MotorNama, tdmotortype.TypeStatus, tdmotortype.MotorKategori, 
                         IFNULL(StockMotor.Jumlah, 0) AS Jumlah, IFNULL(StockMotor.FBHarga, 0) AS FBHarga, 
                         CAST(IFNULL(StockMotor.Bulan, 0) AS CHAR) AS Waktu FROM tdmotortype 
                         LEFT OUTER JOIN (SELECT tmotor.MotorType, COUNT(tmotor.MotorType) AS Jumlah, 
                         SUM(tmotor.FBHarga) AS FBHarga, MONTH(",xStatTgl,") AS Bulan 
                         FROM tmotor 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         ",xResultStatTgl," 
                         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                         WHERE ",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
                         AND ttdk.SalesKode Like '",xSales,"' ",xResultJenis," ",xResultArea," 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"' 
                         GROUP BY tmotor.MotorType, ",xStatTgl,") StockMotor 
                         ON tdmotortype.MotorType = StockMotor.MotorType 
                         INNER JOIN tdmotorkategori ON tdmotortype.MotorKategori = tdmotorkategori.MotorKategori 
                         ORDER BY tdmotorkategori.MotorNo, tdmotortype.MotorKategori, tdmotortype.MotorType)");
       END IF;

       /* RETAIL BULANAN */
       IF xTipe = "bulanan1" THEN 
          SET @MyQuery = "(SELECT '01' AS MyGroup, MotorType, TypeStatus,
                          0.0 AS 'Jan',0.0 AS 'Feb',0.0 AS 'Mar',0.0 AS 'Apr',0.0 AS 'Mei',0.0 AS 'Jun',0.0 AS 'Jul',0.0 AS 'Agust',0.0 AS 'Sep',0.0 AS 'Okt', 
                          0.0 AS 'Nop',0.0 AS 'Des','' AS LokasiKode, 
                          MotorNama, tdmotortype.MotorKategori, MotorGroup FROM tdmotortype INNER JOIN tdmotorkategori ON tdmotortype.MotorKategori = tdmotorkategori.MotorKategori 
                          ORDER BY  MotorNo, tdmotortype.MotorKategori, tdmotortype.MotorType)
                          UNION
                          (SELECT '02' AS MyGroup, tdmotorkategori.MotorGroup AS MotorType, 'A' AS TypeStatus,
                          0.0 AS 'Jan',0.0 AS 'Feb',0.0 AS 'Mar',0.0 AS 'Apr',0.0 AS 'Mei',0.0 AS 'Jun',0.0 AS 'Jul',0.0 AS 'Agust',0.0 AS 'Sep',0.0 AS 'Okt', 
                          0.0 AS 'Nop',0.0 AS 'Des','' AS LokasiKode, 
                          tdmotorkategori.MotorKategori AS MotorNama, tdmotorkategori.MotorGroup AS MotorKategori, tdmotorkategori.MotorGroup AS MotorGroup 
                          FROM tdmotorkategori ORDER BY MotorNo, MotorKategori)";
                          
       END IF;

       /* FILL BULANAN */
       IF xTipe = "bulanan2" THEN 
          SET @MyQuery = CONCAT("(SELECT tdmotortype.MotorType, tdmotortype.MotorNama, tdmotortype.TypeStatus, tdmotortype.MotorKategori, 
                         IFNULL(StockMotor.Jumlah, 0) AS Jumlah, IFNULL(StockMotor.FBHarga, 0) AS FBHarga, 
                         CAST(IFNULL(StockMotor.Bulan, 0) AS CHAR) AS Waktu FROM tdmotortype 
                         LEFT OUTER JOIN (SELECT tmotor.MotorType, COUNT(tmotor.MotorType) AS Jumlah, 
                         SUM(tmotor.FBHarga) AS FBHarga, MONTH(",xStatTgl,") AS Bulan 
                         FROM tmotor 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         ",xResultStatTgl," 
                         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                         WHERE ",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
                         AND ttdk.SalesKode Like '",xSales,"' ",xResultJenis," ",xResultArea," 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"' 
                         GROUP BY tmotor.MotorType, ",xStatTgl,") StockMotor 
                         ON tdmotortype.MotorType = StockMotor.MotorType 
                         INNER JOIN tdmotorkategori ON tdmotortype.MotorKategori = tdmotorkategori.MotorKategori 
                         ORDER BY tdmotorkategori.MotorNo, tdmotortype.MotorKategori, tdmotortype.MotorType)");
       END IF;

       PREPARE STMT FROM @MyQuery;
       EXECUTE Stmt;
    
    END$$

DELIMITER ;

