DELIMITER $$

DROP PROCEDURE IF EXISTS `rMarketDashboard2`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rMarketDashboard2`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE, IN xTanggal VARCHAR(25), IN xEOM VARCHAR(1), IN xRingkas VARCHAR(1))
BEGIN
/*
CALL rMarketDashboard2("Sa Di Stock",'2020-02-01','2020-02-29', 'ttsk.SKTgl','1','1');
*/ 
	DECLARE Tgl1Now, Tgl2Now, Tgl1Before, Tgl2Before DATE;
	
	IF xTipe = 'Sales Kecamatan In'  THEN
		SELECT  Kabupaten INTO @Kabupaten FROM tdarea WHERE AreaStatus = '1' LIMIT 0,1;
		DROP TEMPORARY TABLE IF EXISTS SalesTeamKec;
		CREATE TEMPORARY TABLE IF NOT EXISTS SalesTeamKec AS
		SELECT ttdk.TeamKode, tdteam.TeamNama, ttdk.SalesKode, tdsales.SalesNama, 
         tdcustomer.CusKecamatan AS Kecamatan, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS QTY
         FROM ttdk
         INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
         INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
         INNER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode
         INNER JOIN tdteam ON ttdk.TeamKode = tdteam.TeamKode
         WHERE ttsk.SKTgl BETWEEN xTgl1 AND xTgl2
         AND tdcustomer.CusKabupaten = @Kabupaten 
         GROUP BY TRIM(ttdk.TeamKode),ttdk.SalesKode,tdcustomer.CusKecamatan  
         ORDER BY tdteam.TeamNo, ttdk.TeamKode;
  
       INSERT  INTO SalesTeamKec 
         SELECT ttdk.TeamKode, tdteam.TeamNama, ttdk.SalesKode, tdsales.SalesNama, 
         'xOTHERS' AS Kecamatan, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS QTY
         FROM ttdk
         INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
         INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
         INNER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode
         INNER JOIN tdteam ON ttdk.TeamKode = tdteam.TeamKode
         WHERE ttsk.SKTgl BETWEEN xTgl1 AND xTgl2
         AND tdcustomer.CusKabupaten <> @Kabupaten
         GROUP BY TRIM(ttdk.TeamKode),ttdk.SalesKode 
         ORDER BY tdteam.TeamNo, ttdk.TeamKode;
			
		SET @MyQuery = CONCAT("SELECT * FROM SalesTeamKec;");		   
	END IF;

	IF xTipe = 'Sales Kecamatan Out'  THEN
			SELECT  Kabupaten INTO @Kabupaten FROM tdarea WHERE AreaStatus = '1' LIMIT 0,1;

		DROP TEMPORARY TABLE IF EXISTS SalesTeamKec;
		CREATE TEMPORARY TABLE IF NOT EXISTS SalesTeamKec AS
			SELECT ttdk.TeamKode, tdteam.TeamNama, ttdk.SalesKode, tdsales.SalesNama, 
         tdcustomer.CusKecamatan AS Kecamatan, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS QTY
         FROM ttdk
         INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
         INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
         INNER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode
         INNER JOIN tdteam ON ttdk.TeamKode = tdteam.TeamKode
         WHERE ttsk.SKTgl  BETWEEN xTgl1 AND xTgl2
          AND tdcustomer.CusKabupaten <> @Kabupaten 
          GROUP BY TRIM(ttdk.TeamKode),ttdk.SalesKode,tdcustomer.CusKecamatan  
          ORDER BY tdteam.TeamNo, ttdk.TeamKode;
         	
		SET @MyQuery = CONCAT("SELECT * FROM SalesTeamKec;");		   
	END IF;
	
	IF xTipe = 'Umur Motor avg'  THEN
  		SET @MyQuery = CONCAT("SELECT vmotorlastlokasi.LokasiKode, COUNT(vmotorlastlokasi.MotorNoMesin) AS Stock,
         AVG(DATEDIFF('",xTgl2,"', IFNULL(ttss.SStgl, IFNULL(ttpd.PDTgl, IFNULL(ttrk.RKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')))))) AS Umur
         FROM
         (SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi
          FROM tvmotorlokasi
          INNER JOIN
            (SELECT vmotorlokasi.MotorAutoN, vmotorlokasi.MotorNoMesin, MAX(vmotorlokasi.Jam) AS Jam FROM vmotorlokasi WHERE Jam <= CONCAT('",xTgl2,"', ' 23:59:59')
             GROUP BY vmotorlokasi.MotorNoMesin, vmotorlokasi.MotorAutoN
             ORDER BY MotorNoMesin, MotorAutoN, Jam) vmotorlastjam
          ON tvmotorlokasi.MotorNoMesin = vmotorlastjam.MotorNoMesin AND tvmotorlokasi.MotorAutoN = vmotorlastjam.MotorAutoN AND tvmotorlokasi.Jam = vmotorlastjam.Jam) vmotorlastlokasi
         INNER JOIN tmotor ON vmotorlastlokasi.MotorAutoN = tmotor.MotorAutoN AND vmotorlastlokasi.MotorNoMesin = tmotor.MotorNoMesin
         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
         INNER JOIN tdlokasi ON vmotorlastlokasi.LokasiKode = tdlokasi.LokasiKode
         LEFT OUTER JOIN ttss ON tmotor.SSNo = ttss.SSNo
         LEFT OUTER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo
         LEFT OUTER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo
         WHERE vmotorlastlokasi.Kondisi IN ('IN', 'OUT Pos')
         GROUP BY LokasiKode
         ORDER BY tdlokasi.LokasiNomor;
	");	   
	END IF;
	
	IF xTipe = 'Umur Motor motor'  THEN
		DROP TEMPORARY TABLE IF EXISTS MotorUmur;
		CREATE TEMPORARY TABLE IF NOT EXISTS MotorUmur AS
		SELECT vmotorlastlokasi.MotorAutoN, vmotorlastlokasi.MotorNoMesin,tmotor.MotorNoRangka, tmotor.MotorType,
			tmotor.MotorWarna, vmotorlastlokasi.LokasiKode, vmotorlastlokasi.NoTrans, vmotorlastlokasi.Jam,
			vmotorlastlokasi.Kondisi,tdmotortype.MotorNama,
			IFNULL(ttss.SStgl, IFNULL(ttpd.PDTgl, IFNULL(ttrk.RKTgl,STR_TO_DATE('01/01/1900', '%m/%d/%Y')))) AS 'DateIN', 0000000 AS Umur
			FROM
				(SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi
					FROM tvmotorlokasi
					INNER JOIN (SELECT vmotorlokasi.MotorAutoN, vmotorlokasi.MotorNoMesin, MAX(vmotorlokasi.Jam) AS Jam FROM vmotorlokasi
					WHERE Jam <= CONCAT(xTgl2, " 23:59:59")
					GROUP BY vmotorlokasi.MotorNoMesin, vmotorlokasi.MotorAutoN
					ORDER BY MotorNoMesin, MotorAutoN, Jam) vmotorlastjam
					ON tvmotorlokasi.MotorNoMesin = vmotorlastjam.MotorNoMesin
					AND tvmotorlokasi.MotorAutoN = vmotorlastjam.MotorAutoN
					AND tvmotorlokasi.Jam = vmotorlastjam.Jam ) 
			vmotorlastlokasi
			INNER JOIN tmotor ON vmotorlastlokasi.MotorAutoN = tmotor.MotorAutoN AND vmotorlastlokasi.MotorNoMesin = tmotor.MotorNoMesin
			INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
			LEFT OUTER JOIN ttss ON tmotor.SSNo = ttss.SSNo
			LEFT OUTER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo
			LEFT OUTER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo
			WHERE vmotorlastlokasi.Kondisi IN ('IN','OUT Pos')
			ORDER BY IFNULL(ttss.SStgl, IFNULL(ttpd.PDTgl, IFNULL(ttrk.RKTgl,STR_TO_DATE('01/01/1900', '%m/%d/%Y')))), vmotorlastlokasi.LokasiKode, tdmotortype.MotorType ;
		CREATE INDEX MotorNoMesin ON MotorUmur(MotorNoMesin);
		UPDATE MotorUmur SET Umur = DATEDIFF(xTgl2,DateIN);
		SET @MyQuery = CONCAT("SELECT * FROM MotorUmur;");		   
	END IF;

	IF xTipe = 'Sa Di Stock'  THEN
		SET xTgl1 = DATE_ADD(LAST_DAY(DATE_ADD(xTgl2, INTERVAL -1 MONTH)), INTERVAL 1 DAY);
		IF xEOM = '1' THEN
			SET xTgl2 = LAST_DAY(xTgl2);
			SET Tgl2Before = LAST_DAY(DATE_ADD(xTgl2, INTERVAL -1 MONTH));
		ELSE
			SET Tgl2Before = (DATE_ADD(xTgl2, INTERVAL -1 MONTH));
		END IF;
		SET Tgl1Now = xTgl1;
		SET Tgl2Now = xTgl2;
		SET Tgl1Before = DATE_ADD(xTgl1, INTERVAL -1 MONTH);

		DROP TEMPORARY TABLE IF EXISTS SaDiStock;
		CREATE TEMPORARY TABLE IF NOT EXISTS SaDiStock AS
		SELECT tdmotorkategori.MotorGroup, tdmotorkategori.MotorKategori, tdmotortype.MotorNoMesin, tdmotortype.MotorType, tdmotortype.MotorNama FROM tdmotortype
      INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
      ORDER BY tdmotorkategori.MotorNo, tdmotorkategori.MotorGroup, tdmotorkategori.MotorKategori, tdmotortype.MotorType, tdmotortype.MotorNama;
		ALTER IGNORE TABLE SaDiStock ADD COLUMN SalesMBefore DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE SaDiStock ADD COLUMN SalesMNow DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE SaDiStock ADD COLUMN SalesGrowth DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE SaDiStock ADD COLUMN SalesCBefore DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE SaDiStock ADD COLUMN SalesCNow DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE SaDiStock ADD COLUMN StockMBefore DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE SaDiStock ADD COLUMN StockMNow DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE SaDiStock ADD COLUMN StockGrowth DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE SaDiStock ADD COLUMN StockCBefore DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE SaDiStock ADD COLUMN StockCNow DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE SaDiStock ADD COLUMN DisMBefore DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE SaDiStock ADD COLUMN DisMNow DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE SaDiStock ADD COLUMN DisGrowth DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE SaDiStock ADD COLUMN DisCBefore DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE SaDiStock ADD COLUMN DisCNow DECIMAL(11,2) DEFAULT 0;
		CREATE INDEX MotorType ON SaDiStock(MotorType);

		DROP TEMPORARY TABLE IF EXISTS TFill;
		CREATE TEMPORARY TABLE IF NOT EXISTS TFill AS
		SELECT tdmotortype.MotorType, tdmotortype.MotorNama, IFNULL(StockMotor.Jumlah,0) AS Jumlah
		  FROM tdmotortype INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
		  LEFT OUTER JOIN (SELECT tmotor.MotorType, COUNT(LokasiMotor.Kondisi) AS Jumlah, SUM(tmotor.FBHarga) AS FBHarga
						FROM (SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi
								FROM tvmotorlokasi
											INNER JOIN (SELECT tvmotorlokasi_1.MotorAutoN, tvmotorlokasi_1.MotorNoMesin , MAX(tvmotorlokasi_1.Jam) AS Jam
															FROM tvmotorlokasi tvmotorlokasi_1
															WHERE tvmotorlokasi_1.Jam <= CONCAT(Tgl2Now, " 23:59:59")
															GROUP BY tvmotorlokasi_1.MotorNoMesin, tvmotorlokasi_1.MotorAutoN) B
											  ON tvmotorlokasi.MotorNoMesin = B.MotorNoMesin AND tvmotorlokasi.Jam = B.Jam AND tvmotorlokasi.MotorAutoN = B.MotorAutoN
										 WHERE tvmotorlokasi.Kondisi IN ('IN','OUT Pos') ) LokasiMotor
									INNER JOIN tmotor
									  ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN
								 GROUP BY tmotor.MotorType) StockMotor
			 ON tdmotortype.MotorType = StockMotor.MotorType
		ORDER BY tdmotorkategori.MotorNo, tdmotorkategori.MotorGroup, tdmotorkategori.MotorKategori, tdmotortype.MotorType, tdmotortype.MotorNama;
		CREATE INDEX MotorType ON TFill(MotorType);
		UPDATE SaDiStock INNER JOIN TFill ON TFill.MotorType = SaDiStock.MotorType SET StockMNow = Jumlah;

		DROP TEMPORARY TABLE IF EXISTS TFill;
		CREATE TEMPORARY TABLE IF NOT EXISTS TFill AS
      SELECT tdmotortype.MotorType, tdmotortype.MotorNama, IFNULL(StockMotor.Jumlah,0) AS Jumlah
        FROM tdmotortype INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
        LEFT OUTER JOIN (SELECT tmotor.MotorType, COUNT(LokasiMotor.Kondisi) AS Jumlah, SUM(tmotor.FBHarga) AS FBHarga
                  FROM (SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi
                        FROM tvmotorlokasi
                                 INNER JOIN (SELECT tvmotorlokasi_1.MotorAutoN, tvmotorlokasi_1.MotorNoMesin , MAX(tvmotorlokasi_1.Jam) AS Jam
                                             FROM tvmotorlokasi tvmotorlokasi_1
                                             WHERE tvmotorlokasi_1.Jam <= CONCAT(Tgl2Before, " 23:59:59")
                                             GROUP BY tvmotorlokasi_1.MotorNoMesin, tvmotorlokasi_1.MotorAutoN) B
                                   ON tvmotorlokasi.MotorNoMesin = B.MotorNoMesin AND tvmotorlokasi.Jam = B.Jam AND tvmotorlokasi.MotorAutoN = B.MotorAutoN
                               WHERE tvmotorlokasi.Kondisi IN ('IN','OUT Pos') ) LokasiMotor
                           INNER JOIN tmotor
                             ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN
                         GROUP BY tmotor.MotorType) StockMotor
          ON tdmotortype.MotorType = StockMotor.MotorType
      ORDER BY tdmotorkategori.MotorNo, tdmotorkategori.MotorGroup, tdmotorkategori.MotorKategori, tdmotortype.MotorType, tdmotortype.MotorNama;
		CREATE INDEX MotorType ON TFill(MotorType);
      UPDATE SaDiStock INNER JOIN TFill ON TFill.MotorType = SaDiStock.MotorType SET StockMBefore = Jumlah;

		DROP TEMPORARY TABLE IF EXISTS TFill;
		CREATE TEMPORARY TABLE IF NOT EXISTS TFill AS
		SELECT tdmotortype.MotorType, IFNULL(Jumlah,0) AS Jumlah
         FROM tdmotortype INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
         LEFT OUTER JOIN
            (SELECT tmotor.MotorType, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS Jumlah FROM tmotor
               INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
               INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
         WHERE ttsk.SKTgl BETWEEN  Tgl1Now AND Tgl2Now
         GROUP BY tmotor.MotorType) Jum
         ON tdmotortype.MotorType = Jum.MotorType
         ORDER BY tdmotorkategori.MotorNo, tdmotorkategori.MotorGroup, tdmotorkategori.MotorKategori, tdmotortype.MotorType, tdmotortype.MotorNama;
		CREATE INDEX MotorType ON TFill(MotorType);
      UPDATE SaDiStock INNER JOIN TFill ON TFill.MotorType = SaDiStock.MotorType SET SalesMNow = Jumlah;

		DROP TEMPORARY TABLE IF EXISTS TFill;
		CREATE TEMPORARY TABLE IF NOT EXISTS TFill AS
		SELECT tdmotortype.MotorType, IFNULL(Jumlah,0) AS Jumlah
         FROM tdmotortype INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
         LEFT OUTER JOIN
            (SELECT tmotor.MotorType, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS Jumlah FROM tmotor
               INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
               INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
          WHERE ttsk.SKTgl BETWEEN Tgl1Before  AND  Tgl2Before
         GROUP BY tmotor.MotorType) Jum
         ON tdmotortype.MotorType = Jum.MotorType
         ORDER BY tdmotorkategori.MotorNo, tdmotorkategori.MotorGroup, tdmotorkategori.MotorKategori, tdmotortype.MotorType, tdmotortype.MotorNama;
      CREATE INDEX MotorType ON TFill(MotorType);
      UPDATE SaDiStock INNER JOIN TFill ON TFill.MotorType = SaDiStock.MotorType SET SalesMBefore = Jumlah;

		DROP TEMPORARY TABLE IF EXISTS TFill;
		CREATE TEMPORARY TABLE IF NOT EXISTS TFill AS
		SELECT tdmotortype.MotorType, IFNULL(Jumlah,0) AS Jumlah
         FROM tdmotortype INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
         LEFT OUTER JOIN
            (SELECT tmotor.MotorType, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS Jumlah FROM tmotor
               INNER JOIN ttss ON tmotor.SSNo = ttSS.SSNo
         WHERE ttss.SSTgl BETWEEN Tgl1Now AND Tgl2Now
         GROUP BY tmotor.MotorType) Jum
         ON tdmotortype.MotorType = Jum.MotorType
         ORDER BY tdmotorkategori.MotorNo, tdmotorkategori.MotorGroup, tdmotorkategori.MotorKategori, tdmotortype.MotorType, tdmotortype.MotorNama;
		CREATE INDEX MotorType ON TFill(MotorType);
      UPDATE SaDiStock INNER JOIN TFill ON TFill.MotorType = SaDiStock.MotorType SET DisMNow = Jumlah;

		DROP TEMPORARY TABLE IF EXISTS TFill;
		CREATE TEMPORARY TABLE IF NOT EXISTS TFill AS
      SELECT tdmotortype.MotorType, IFNULL(Jumlah,0) AS Jumlah
         FROM tdmotortype INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
         LEFT OUTER JOIN
            (SELECT tmotor.MotorType, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS Jumlah FROM tmotor
               INNER JOIN ttss ON tmotor.SSNo = ttSS.SSNo
         WHERE ttss.SSTgl BETWEEN Tgl1Before  AND  Tgl2Before
         GROUP BY tmotor.MotorType) Jum
         ON tdmotortype.MotorType = Jum.MotorType
         ORDER BY tdmotorkategori.MotorNo, tdmotorkategori.MotorGroup, tdmotorkategori.MotorKategori, tdmotortype.MotorType, tdmotortype.MotorNama;
      CREATE INDEX MotorType ON TFill(MotorType);
      UPDATE SaDiStock INNER JOIN TFill ON TFill.MotorType = SaDiStock.MotorType SET DisMBefore = Jumlah;
      
      IF xRingkas = '1' THEN
			DROP TEMPORARY TABLE IF EXISTS SaDiStockGroup;
			CREATE TEMPORARY TABLE IF NOT EXISTS SaDiStockGroup AS
			SELECT MotorGroup , MotorKategori, MotorNoMesin, MotorType, MotorNama,
			SUM(SalesMBefore) AS SalesMBefore,
			SUM(SalesMNow) AS SalesMNow,
			SUM(SalesGrowth) AS SalesGrowth,
			SUM(SalesCBefore) AS SalesCBefore,
			SUM(SalesCNow) AS SalesCNow,
			SUM(StockMBefore) AS StockMBefore,
			SUM(StockMNow) AS StockMNow,
			SUM(StockGrowth) AS StockGrowth,
			SUM(StockCBefore) AS StockCBefore,
			SUM(StockCNow) AS StockCNow,
			SUM(DisMBefore) AS DisMBefore,
			SUM(DisMNow) AS DisMNow,
			SUM(DisGrowth) AS DisGrowth,
			SUM(DisCBefore) AS DisCBefore,
			SUM(DisCNow) AS DisCNow
			FROM SaDiStock
			GROUP BY SaDiStock.MotorNoMesin ;
			DROP TEMPORARY TABLE IF EXISTS SaDiStock;
			CREATE TEMPORARY TABLE IF NOT EXISTS SaDiStock AS
			SELECT * FROM SaDiStockGroup;
		END IF;

		DROP TEMPORARY TABLE IF EXISTS TFill;
		CREATE TEMPORARY TABLE IF NOT EXISTS TFill AS
      SELECT 'ALL' AS MotorGroup , 'ALL' AS MotorKategori, MotorGroup AS MotorNoMesin, MotorGroup AS MotorType, MotorGroup AS MotorNama,
		SUM(SalesMBefore) AS SalesMBefore,
		SUM(SalesMNow) AS SalesMNow,
		SUM(SalesGrowth) AS SalesGrowth,
		SUM(SalesCBefore) AS SalesCBefore,
		SUM(SalesCNow) AS SalesCNow,
		SUM(StockMBefore) AS StockMBefore,
		SUM(StockMNow) AS StockMNow,
		SUM(StockGrowth) AS StockGrowth,
		SUM(StockCBefore) AS StockCBefore,
		SUM(StockCNow) AS StockCNow,
		SUM(DisMBefore) AS DisMBefore,
		SUM(DisMNow) AS DisMNow,
		SUM(DisGrowth) AS DisGrowth,
		SUM(DisCBefore) AS DisCBefore,
		SUM(DisCNow) AS DisCNow
		FROM SaDiStock
      GROUP BY SaDiStock.MotorGroup ORDER BY IF(SaDiStock.MotorGroup='Cub',0,IF(MotorGroup='AT',1,2));
      INSERT INTO SaDiStock SELECT * FROM TFill;

      DELETE FROM SaDiStock WHERE
      SalesMBefore = 0 AND
      SalesMNow = 0 AND
      SalesGrowth = 0 AND
      SalesCBefore = 0 AND
      SalesCNow = 0 AND
      StockMBefore = 0 AND
      StockMNow = 0 AND
      StockGrowth = 0 AND
      StockCBefore = 0 AND
      StockCNow = 0 AND
      DisMBefore = 0 AND
      DisMNow = 0 AND
      DisGrowth = 0 AND
      DisCBefore = 0 AND
      DisCNow = 0 ;

		UPDATE SaDiStock SET StockGrowth = ROUND((StockMNow - StockMBefore) / StockMBefore * 100,2) WHERE StockMBefore <> 0;
		UPDATE SaDiStock SET StockGrowth = 100 WHERE StockMBefore = 0;
		UPDATE SaDiStock SET StockGrowth = 0 WHERE StockMBefore = 0 AND StockMNow = 0;

		UPDATE SaDiStock SET SalesGrowth = ROUND((SalesMNow - SalesMBefore) / SalesMBefore * 100,2) WHERE SalesMBefore <> 0;
		UPDATE SaDiStock SET SalesGrowth = 100 WHERE SalesMBefore = 0;
		UPDATE SaDiStock SET SalesGrowth = 0 WHERE SalesMBefore = 0 AND SalesMNow = 0;

		UPDATE SaDiStock SET DisGrowth = ROUND((DisMNow - DisMBefore) / DisMBefore * 100,2) WHERE DisMBefore <> 0;
		UPDATE SaDiStock SET DisGrowth = 100 WHERE DisMBefore = 0;
		UPDATE SaDiStock SET DisGrowth = 0 WHERE DisMBefore = 0 AND DisMNow = 0;

		DROP TEMPORARY TABLE IF EXISTS SUMari;
		CREATE TEMPORARY TABLE IF NOT EXISTS SUMari AS
		SELECT  MotorKategori,
		  SUM(SalesMBefore) AS SUMSalesMBefore, SUM(SalesMNow) AS SUMSalesMNow,
		  SUM(StockMBefore) AS SUMStockMBefore, SUM(StockMNow) AS SUMStockMNow,
		  SUM(DisMBefore) AS SUMDisMBefore, SUM(DisMNow) AS SUMDisMNow
		FROM SaDiStock GROUP BY MotorKategori;
		CREATE INDEX MotorKategori ON SUMari(MotorKategori);

		UPDATE SaDiStock INNER JOIN SUMari ON SUMari.MotorKategori = SaDiStock.MotorKategori SET SalesCBefore = ROUND(SalesMBefore / SUMSalesMBefore * 100,2) WHERE SUMSalesMBefore <> 0;
		UPDATE SaDiStock INNER JOIN SUMari ON SUMari.MotorKategori = SaDiStock.MotorKategori SET SalesCNow = ROUND(SalesMNow / SUMSalesMNow * 100,2) WHERE SUMSalesMNow <> 0;
		UPDATE SaDiStock INNER JOIN SUMari ON SUMari.MotorKategori = SaDiStock.MotorKategori SET StockCBefore = ROUND(StockMBefore / SUMStockMBefore * 100,2) WHERE SUMStockMBefore <> 0;
		UPDATE SaDiStock INNER JOIN SUMari ON SUMari.MotorKategori = SaDiStock.MotorKategori SET StockCNow = ROUND(StockMNow / SUMStockMNow * 100,2) WHERE SUMStockMNow <> 0;
		UPDATE SaDiStock INNER JOIN SUMari ON SUMari.MotorKategori = SaDiStock.MotorKategori SET DisCBefore = ROUND(DisMBefore / SUMDisMBefore * 100,2) WHERE SUMDisMBefore <> 0;
		UPDATE SaDiStock INNER JOIN SUMari ON SUMari.MotorKategori = SaDiStock.MotorKategori SET DisCNow = ROUND(DisMNow / SUMDisMNow * 100,2) WHERE SUMDisMNow <> 0;

		SET @MyQuery = CONCAT("SELECT * FROM SaDiStock;");		   
	END IF;
	
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$

DELIMITER ;


