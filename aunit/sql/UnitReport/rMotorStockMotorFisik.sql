DELIMITER $$
DROP PROCEDURE IF EXISTS `rMotorStockMotorFisik`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rMotorStockMotorFisik`(IN xTipe VARCHAR(25),IN xTgl1 DATE,IN xTgl2 DATE,IN xStatus VARCHAR(5),IN xStatKon VARCHAR(1),IN xLokasi VARCHAR(30),IN xTahun1 VARCHAR(4),IN xTahun2 VARCHAR(4),IN xQuick VARCHAR(1),IN xDealer VARCHAR(20),IN xSort VARCHAR(25),IN xOrder VARCHAR(10),IN xFBPD VARCHAR(1))
BEGIN
/*
       xTipe    : "TStockMotor" -> Motor1
                  "TStockMotorWarna1" -> Motor2
                  "TStockMotorWarna2" -> Motor3
                  "TDetailMotor1" -> Detail Motor Type
                  "TDetailMotor2" -> Detail Motor Lokasi
                  "TDetailMotor3" -> StockOpname
                  "TDetailMotor4" -> Detail HPP
                  "tmotorIntransit" -> Stock In Transit
                  "AverageUmur" -> Average Umur
                  "MotorUmur" -> Motor Umur             
                  
       xSort    :  "tdmotortype.MotorType"  => 	"Type Motor"
                   "tdmotortype.MotorNama"  =>  "Nama Motor"
                   "tmotor.MotorTahun"      =>  "Tahun Motor"
                   "FBHarga"                =>  "Harga"
                   "Jumlah"                 =>  "Jumlah"
                   "LokasiMotor.LokasiKode" =>  "Lokasi"
                   
     CALL rMotorStockMotorFisik('TStockMotorWarna1','2020-05-01','2020-05-30','%','1','%','2015','2020','1','%','tdmotortype.MotorType','ASC','1');

*/
	DECLARE xKondisi VARCHAR(20);
   DECLARE xMyPostStr VARCHAR(20);
   DECLARE xTglMin DATE;

	SET @Lokasi = CONCAT(xLokasi,"%");
	SET @Dealer = CONCAT(xDealer,"%");
	SET @Status = CONCAT(xStatus,"%");
	SET xTglMin = SUBDATE(xTgl1,1);

	CASE xStatKon
		WHEN "1" THEN SET xKondisi = "('IN','OUT Pos')";
		WHEN "2" THEN SET xKondisi = "('IN')";
		WHEN "3" THEN SET xKondisi = "('OUT')";
		WHEN "4" THEN SET xKondisi = "('OUT Pos')";
	END CASE;
       
 
	/* MOTOR 1 */
	IF xTipe = "TStockMotor" THEN
		DROP TEMPORARY TABLE IF EXISTS TStockMotor;
		IF xQuick = '1' THEN
			SET @MyQuery = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS TStockMotor SELECT tdmotortype.MotorType, tdmotortype.TypeStatus, tdmotortype.MotorNama, IFNULL(StockMotor.Jumlah,0) AS Jumlah, IFNULL(StockMotor.FBHarga,0) AS FBHarga, tdmotortype.KodeAHM  
							 FROM tdmotortype 
							 LEFT OUTER JOIN (SELECT tmotor.MotorType, COUNT(LokasiMotor.Kondisi) AS Jumlah, SUM(tmotor.FBHarga) AS FBHarga, tmotor.MotorTahun  
							 FROM (SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi 
							 FROM tvmotorlokasi 
							 INNER JOIN (SELECT tvmotorlokasi_1.MotorAutoN, tvmotorlokasi_1.MotorNoMesin , MAX(tvmotorlokasi_1.Jam) AS Jam 
							 FROM tvmotorlokasi tvmotorlokasi_1 
							 WHERE tvmotorlokasi_1.Jam <= '",xTgl2," 23:59:59'
							 GROUP BY tvmotorlokasi_1.MotorNoMesin, tvmotorlokasi_1.MotorAutoN) B 
							 ON tvmotorlokasi.MotorNoMesin = B.MotorNoMesin AND tvmotorlokasi.Jam = B.Jam AND tvmotorlokasi.MotorAutoN = B.MotorAutoN 
							 WHERE tvmotorlokasi.Kondisi IN ",xKondisi," ) LokasiMotor 
							 INNER JOIN tmotor 
							 ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN 
							 WHERE tmotor.DealerKode LIKE '",xDealer,"' AND
							 LokasiMotor.LokasiKode LIKE '",xLokasi,"' AND
							 tmotor.MotorTahun BETWEEN '",xTahun1,"' AND '",xTahun2,"' GROUP BY tmotor.MotorType) StockMotor 
							 ON tdmotortype.MotorType = StockMotor.MotorType WHERE tdmotortype.TypeStatus LIKE '",xStatus,"' 
							 ORDER BY ",xSort," ",xOrder);
			PREPARE STMT FROM @MyQuery;
			EXECUTE Stmt;
			DELETE FROM TStockMotor WHERE  Jumlah = 0 AND FBHarga = 0;
			SET @MyQuery = CONCAT("SELECT * FROM TStockMotor;");
		ELSE
			DELETE FROM tvmotorlokasi; 
			INSERT INTO tvmotorlokasi SELECT * FROM vmotorlokasi;
			SET @MyQuery = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS TStockMotor SELECT tdmotortype.MotorType, tdmotortype.TypeStatus, tdmotortype.MotorNama, IFNULL(StockMotor.Jumlah,0) AS Jumlah, IFNULL(StockMotor.FBHarga,0) AS FBHarga, tdmotortype.KodeAHM  
							 FROM tdmotortype 
							 LEFT OUTER JOIN (SELECT tmotor.MotorType, COUNT(LokasiMotor.Kondisi) AS Jumlah, SUM(tmotor.FBHarga) AS FBHarga, tmotor.MotorTahun  
							 FROM (SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi 
							 FROM tvmotorlokasi 
							 INNER JOIN (SELECT tvmotorlokasi_1.MotorAutoN, tvmotorlokasi_1.MotorNoMesin , MAX(tvmotorlokasi_1.Jam) AS Jam 
							 FROM tvmotorlokasi tvmotorlokasi_1 
							 WHERE tvmotorlokasi_1.Jam <= '",xTgl2," 23:59:59'
							 GROUP BY tvmotorlokasi_1.MotorNoMesin, tvmotorlokasi_1.MotorAutoN) B 
							 ON tvmotorlokasi.MotorNoMesin = B.MotorNoMesin AND tvmotorlokasi.Jam = B.Jam AND tvmotorlokasi.MotorAutoN = B.MotorAutoN 
							 WHERE tvmotorlokasi.Kondisi IN ",xKondisi," ) LokasiMotor 
							 INNER JOIN tmotor 
							 ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN 
							 WHERE tmotor.DealerKode LIKE '",xDealer,"' AND
							 LokasiMotor.LokasiKode LIKE '",xLokasi,"' AND
							 tmotor.MotorTahun BETWEEN '",xTahun1,"' AND '",xTahun2,"' GROUP BY tmotor.MotorType) StockMotor 
							 ON tdmotortype.MotorType = StockMotor.MotorType WHERE tdmotortype.TypeStatus LIKE '",xStatus,"' 
							 ORDER BY ",xSort," ",xOrder);
			PREPARE STMT FROM @MyQuery;
			EXECUTE Stmt;
			DELETE FROM TStockMotor WHERE  Jumlah = 0 AND FBHarga = 0;
			SET @MyQuery = CONCAT("SELECT * FROM TStockMotor;");
		END IF;
	END IF;
	/* MOTOR 2 */
	IF xTipe = "TStockMotorWarna1" THEN 
	 IF xQuick = '1' THEN
		 SET @MyQuery = CONCAT("SELECT tdmotortype.MotorType, tdmotortype.TypeStatus, tdmotortype.MotorNama, IFNULL(StockMotor.MotorWarna, '--') AS MotorWarna, 
							 IFNULL(StockMotor.Jumlah, 0) AS Jumlah, IFNULL(StockMotor.FBHarga, 0) AS FBHarga, tdmotortype.KodeAHM 
							 FROM tdmotortype 
							 LEFT OUTER JOIN (SELECT tmotor.MotorType, tmotor.MotorWarna, COUNT(LokasiMotor.Kondisi) AS Jumlah, SUM(tmotor.FBHarga) AS FBHarga, tmotor.MotorTahun  
							 FROM (SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi 
							 FROM tvmotorlokasi 
							 INNER JOIN (SELECT tvmotorlokasi_1.MotorAutoN, tvmotorlokasi_1.MotorNoMesin , MAX(tvmotorlokasi_1.Jam) AS Jam 
							 FROM tvmotorlokasi tvmotorlokasi_1 
							 WHERE tvmotorlokasi_1.Jam <= '",xTgl2,"yyyy/MM/dd 23:59:59'  
							 GROUP BY tvmotorlokasi_1.MotorNoMesin, tvmotorlokasi_1.MotorAutoN) B 
							 ON tvmotorlokasi.MotorNoMesin = B.MotorNoMesin AND tvmotorlokasi.Jam = B.Jam AND tvmotorlokasi.MotorAutoN = B.MotorAutoN 
							 WHERE tvmotorlokasi.Kondisi IN ",xKondisi," ) LokasiMotor 
							 INNER JOIN tmotor ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN 
							 WHERE tmotor.DealerKode LIKE '",xDealer,"' 
							 AND LokasiMotor.LokasiKode LIKE '",xLokasi,"' 
							 AND tmotor.MotorTahun BETWEEN ",xTahun1," AND ",xTahun2," 
							 GROUP BY tmotor.MotorType, tmotor.MotorWarna) StockMotor 
							 ON tdmotortype.MotorType = StockMotor.MotorType 
							 WHERE tdmotortype.TypeStatus LIKE '",xStatus,"' 
							 ORDER BY ",xSort," ",xOrder);
	 ELSE
		 SET @MyQuery = CONCAT("SELECT tdmotortype.MotorType, tdmotortype.TypeStatus, tdmotortype.MotorNama, IFNULL(StockMotor.MotorWarna, '--') AS MotorWarna, 
							 IFNULL(StockMotor.Jumlah, 0) AS Jumlah, IFNULL(StockMotor.FBHarga, 0) AS FBHarga, tdmotortype.KodeAHM, tdmotortype.KodeAHM 
							 FROM tdmotortype 
							 LEFT OUTER JOIN (SELECT tmotor.MotorType, tmotor.MotorWarna, COUNT(LokasiMotor.Kondisi) AS Jumlah, SUM(tmotor.FBHarga) AS FBHarga, tmotor.MotorTahun  
							 FROM (SELECT U.MotorAutoN, U.MotorNoMesin, U.LokasiKode, U.NoTrans, U.Jam, U.Kondisi 
							 FROM (SELECT vmotorlokasi.MotorAutoN, vmotorlokasi.MotorNoMesin, vmotorlokasi.LokasiKode, vmotorlokasi.NoTrans, vmotorlokasi.Jam, vmotorlokasi.Kondisi 
							 FROM vmotorlokasi 
							 WHERE vmotorlokasi.LokasiKode LIKE '%') U 
							 INNER JOIN (SELECT vmotorlokasi_1.MotorAutoN, vmotorlokasi_1.MotorNoMesin , MAX(vmotorlokasi_1.Jam) AS Jam FROM vmotorlokasi vmotorlokasi_1 
							 WHERE vmotorlokasi_1.Jam <= '",xTgl2,"yyyy/MM/dd 23:59:59' 
							 GROUP BY vmotorlokasi_1.MotorNoMesin, vmotorlokasi_1.MotorAutoN) B 
							 ON U.MotorNoMesin = B.MotorNoMesin AND U.Jam = B.Jam AND U.MotorAutoN = B.MotorAutoN 
							 WHERE U.Kondisi IN ",xKondisi," ) LokasiMotor 
							 INNER JOIN tmotor ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN 
							 WHERE tmotor.DealerKode LIKE '",xDealer,"' 
							 AND LokasiMotor.LokasiKode LIKE '",xLokasi,"' 
							 AND tmotor.MotorTahun BETWEEN ",xTahun1," AND ",xTahun2," 
							 GROUP BY tmotor.MotorType, tmotor.MotorWarna) StockMotor 
							 ON tdmotortype.MotorType = StockMotor.MotorType 
							 WHERE tdmotortype.TypeStatus LIKE '",xStatus,"' 
							 ORDER BY ",xSort," ",xOrder);
	 END IF;
	END IF;
	/* MOTOR 3 */
	IF xTipe = "TStockMotorWarna2" THEN 
	 IF xQuick = '1' THEN
		 SET @MyQuery = CONCAT("SELECT tdmotortype.MotorType, tdmotortype.TypeStatus, tdmotortype.MotorNama, IFNULL(StockMotor.MotorWarna, '--') AS MotorWarna, StockMotor.MotorTahun, 
							 IFNULL(StockMotor.Jumlah, 0) AS Jumlah, IFNULL(StockMotor.FBHarga, 0) AS FBHarga, tdmotortype.KodeAHM 
							 FROM tdmotortype 
							 LEFT OUTER JOIN (SELECT tmotor.MotorType, tmotor.MotorWarna, COUNT(LokasiMotor.Kondisi) AS Jumlah, SUM(tmotor.FBHarga) AS FBHarga, tmotor.MotorTahun 
							 FROM (SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi 
							 FROM tvmotorlokasi 
							 INNER JOIN (SELECT tvmotorlokasi_1.MotorAutoN, tvmotorlokasi_1.MotorNoMesin , MAX(tvmotorlokasi_1.Jam) AS Jam 
							 FROM tvmotorlokasi tvmotorlokasi_1 
							 WHERE tvmotorlokasi_1.Jam <= '",xTgl2," yyyy/MM/dd 23:59:59'  
							 GROUP BY tvmotorlokasi_1.MotorNoMesin, tvmotorlokasi_1.MotorAutoN) B 
							 ON tvmotorlokasi.MotorNoMesin = B.MotorNoMesin AND tvmotorlokasi.Jam = B.Jam AND tvmotorlokasi.MotorAutoN = B.MotorAutoN 
							 WHERE tvmotorlokasi.Kondisi IN ",xKondisi," ) LokasiMotor 
							 INNER JOIN tmotor ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN 
							 WHERE tmotor.DealerKode LIKE '",xDealer,"' 
							 AND LokasiMotor.LokasiKode LIKE '",xLokasi,"' 
							 AND tmotor.MotorTahun BETWEEN ",xTahun1," AND ",xTahun2," 
							 GROUP BY tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun) StockMotor 
							 ON tdmotortype.MotorType = StockMotor.MotorType 
							 WHERE tdmotortype.TypeStatus LIKE '",xStatus,"' 
							 ORDER BY ",xSort," ",xOrder);
	 ELSE
		 SET @MyQuery = CONCAT("SELECT tdmotortype.MotorType, tdmotortype.TypeStatus, tdmotortype.MotorNama, IFNULL(StockMotor.MotorWarna, '--') AS MotorWarna, StockMotor.MotorTahun, 
							 IFNULL(StockMotor.Jumlah, 0) AS Jumlah, IFNULL(StockMotor.FBHarga, 0) AS FBHarga, tdmotortype.KodeAHM 
							 FROM tdmotortype 
							 LEFT OUTER JOIN (SELECT tmotor.MotorType, tmotor.MotorWarna, COUNT(LokasiMotor.Kondisi) AS Jumlah, SUM(tmotor.FBHarga) AS FBHarga, tmotor.MotorTahun 
							 FROM (SELECT U.MotorAutoN, U.MotorNoMesin, U.LokasiKode, U.NoTrans, U.Jam, U.Kondisi 
							 FROM (SELECT vmotorlokasi.MotorAutoN, vmotorlokasi.MotorNoMesin, vmotorlokasi.LokasiKode, vmotorlokasi.NoTrans, vmotorlokasi.Jam, vmotorlokasi.Kondisi 
							 FROM vmotorlokasi 
							 WHERE vmotorlokasi.LokasiKode LIKE '%') U 
							 INNER JOIN (SELECT vmotorlokasi_1.MotorAutoN, vmotorlokasi_1.MotorNoMesin , MAX(vmotorlokasi_1.Jam) AS Jam FROM vmotorlokasi vmotorlokasi_1 
							 WHERE vmotorlokasi_1.Jam <= '",xTgl2," yyyy/MM/dd 23:59:59' 
							 GROUP BY vmotorlokasi_1.MotorNoMesin, vmotorlokasi_1.MotorAutoN) B 
							 ON U.MotorNoMesin = B.MotorNoMesin AND U.Jam = B.Jam AND U.MotorAutoN = B.MotorAutoN 
							 WHERE U.Kondisi IN ",xKondisi," ) LokasiMotor 
							 INNER JOIN tmotor ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN 
							 WHERE tmotor.DealerKode LIKE '",xDealer,"' 
							 AND LokasiMotor.LokasiKode LIKE '",xLokasi,"' 
							 AND tmotor.MotorTahun BETWEEN ",xTahun1," AND ",xTahun2," 
							 GROUP BY tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun) StockMotor 
							 ON tdmotortype.MotorType = StockMotor.MotorType 
							 WHERE tdmotortype.TypeStatus LIKE '",xStatus,"' 
							 ORDER BY ",xSort," ",xOrder); 
	 END IF;
	END IF;
	/* DETAIL MOTOR TYPE */
	IF xTipe = "TDetailMotor1" THEN 
	 IF xQuick = '1' THEN
		 SET @MyQuery = CONCAT("SELECT tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoRangka, LokasiMotor.MotorNoMesin, LokasiMotor.NoTrans, LokasiMotor.Jam, Tmotor.FBHarga, LokasiMotor.LokasiKode, LokasiMotor.Kondisi, tmotor.FBNo  
							 FROM (SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi 
							 FROM tvmotorlokasi 
							 INNER JOIN (SELECT tvmotorlokasi_1.MotorAutoN, tvmotorlokasi_1.MotorNoMesin , MAX(tvmotorlokasi_1.Jam) AS Jam 
							 FROM tvmotorlokasi tvmotorlokasi_1 
							 WHERE tvmotorlokasi_1.Jam <= '",xTgl2," yyyy/MM/dd 23:59:59' 
							 GROUP BY tvmotorlokasi_1.MotorNoMesin, tvmotorlokasi_1.MotorAutoN) B 
							 ON tvmotorlokasi.MotorNoMesin = B.MotorNoMesin AND tvmotorlokasi.Jam = B.Jam AND tvmotorlokasi.MotorAutoN = B.MotorAutoN 
							 WHERE tvmotorlokasi.Kondisi IN ",xKondisi,") LokasiMotor 
							 INNER JOIN tmotor 
							 ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN 
							 WHERE tmotor.DealerKode LIKE  '",xDealer,"' 
							 AND LokasiMotor.LokasiKode LIKE '",xLokasi,"' 
							 AND tmotor.MotorTahun BETWEEN ",xTahun1," AND ",xTahun2," 
							 ORDER BY MotorType, Jam, Notrans");
	 ELSE
		 SET @MyQuery = CONCAT("SELECT tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoRangka, LokasiMotor.MotorNoMesin, LokasiMotor.NoTrans, LokasiMotor.Jam, Tmotor.FBHarga, LokasiMotor.LokasiKode, LokasiMotor.Kondisi, tmotor.FBNo  
							 FROM (SELECT U.MotorAutoN, U.MotorNoMesin, U.LokasiKode, U.NoTrans, U.Jam, U.Kondisi 
							 FROM (SELECT vmotorlokasi.MotorAutoN, vmotorlokasi.MotorNoMesin, vmotorlokasi.LokasiKode, vmotorlokasi.NoTrans, vmotorlokasi.Jam, vmotorlokasi.Kondisi 
							 FROM vmotorlokasi 
							 WHERE vmotorlokasi.LokasiKode LIKE '%') U 
							 INNER JOIN (SELECT vmotorlokasi_1.MotorAutoN, vmotorlokasi_1.MotorNoMesin , MAX(vmotorlokasi_1.Jam) AS Jam 
							 FROM vmotorlokasi vmotorlokasi_1 
							 WHERE vmotorlokasi_1.Jam <= '",xTgl2," yyyy/MM/dd 23:59:59' 
							 GROUP BY vmotorlokasi_1.MotorNoMesin, vmotorlokasi_1.MotorAutoN) B 
							 ON U.MotorNoMesin = B.MotorNoMesin AND U.Jam = B.Jam AND U.MotorAutoN = B.MotorAutoN 
							 WHERE U.Kondisi IN ",xKondisi,") LokasiMotor 
							 INNER JOIN tmotor 
							 ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN 
							 WHERE tmotor.DealerKode LIKE  '",xDealer,"' 
							 AND LokasiMotor.LokasiKode LIKE '",xLokasi,"' 
							 AND tmotor.MotorTahun BETWEEN ",xTahun1," AND ",xTahun2," 
							 ORDER BY MotorType, Jam, Notrans"); 
	 END IF;
	END IF;
	/* DETAIL MOTOR LOKASI */
	IF xTipe = "TDetailMotor2" THEN 
	 IF xQuick = '1' THEN
		 SET @MyQuery = CONCAT("SELECT tmotor.MotorType,MotorNama, tmotor.MotorWarna, tmotor.MotorTahun-2000 AS MotorTahun, LokasiMotor.MotorNoMesin, LokasiMotor.NoTrans, LokasiMotor.Jam, Tmotor.FBHarga, LokasiMotor.LokasiKode, LokasiMotor.Kondisi, tmotor.FBNo, tmotor.MotorNoRangka  
							 FROM (SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi 
							 FROM tvmotorlokasi 
							 INNER JOIN (SELECT tvmotorlokasi_1.MotorAutoN, tvmotorlokasi_1.MotorNoMesin , MAX(tvmotorlokasi_1.Jam) AS Jam 
							 FROM tvmotorlokasi tvmotorlokasi_1 
							 WHERE tvmotorlokasi_1.Jam <= '",xTgl2," 23:59:59' 
							 GROUP BY tvmotorlokasi_1.MotorNoMesin, tvmotorlokasi_1.MotorAutoN) B 
							 ON tvmotorlokasi.MotorNoMesin = B.MotorNoMesin AND tvmotorlokasi.Jam = B.Jam AND tvmotorlokasi.MotorAutoN = B.MotorAutoN 
							 WHERE tvmotorlokasi.Kondisi IN ",xKondisi,") LokasiMotor 
							 INNER JOIN tmotor 
							 ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN 
							 INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
							 WHERE tmotor.DealerKode LIKE  '",xDealer,"' 
							 AND LokasiMotor.LokasiKode LIKE '",xLokasi,"' 
							 AND tmotor.MotorTahun BETWEEN ",xTahun1," AND ",xTahun2," 
							 ORDER BY LokasiMotor.LokasiKode, MotorType, Jam, Notrans");
	 ELSE
		 SET @MyQuery = CONCAT("SELECT tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoRangka, LokasiMotor.MotorNoMesin, LokasiMotor.NoTrans, LokasiMotor.Jam, Tmotor.FBHarga, LokasiMotor.LokasiKode, LokasiMotor.Kondisi, tmotor.FBNo  
							 FROM (SELECT U.MotorAutoN, U.MotorNoMesin, U.LokasiKode, U.NoTrans, U.Jam, U.Kondisi 
							 FROM (SELECT vmotorlokasi.MotorAutoN, vmotorlokasi.MotorNoMesin, vmotorlokasi.LokasiKode, vmotorlokasi.NoTrans, vmotorlokasi.Jam, vmotorlokasi.Kondisi 
							 FROM vmotorlokasi 
							 WHERE vmotorlokasi.LokasiKode LIKE '%') U 
							 INNER JOIN (SELECT vmotorlokasi_1.MotorAutoN, vmotorlokasi_1.MotorNoMesin , MAX(vmotorlokasi_1.Jam) AS Jam 
							 FROM vmotorlokasi vmotorlokasi_1 
							 WHERE vmotorlokasi_1.Jam <= '",xTgl2," 23:59:59' 
							 GROUP BY vmotorlokasi_1.MotorNoMesin, vmotorlokasi_1.MotorAutoN) B 
							 ON U.MotorNoMesin = B.MotorNoMesin AND U.Jam = B.Jam AND U.MotorAutoN = B.MotorAutoN 
							 WHERE U.Kondisi IN ",xKondisi,") LokasiMotor 
							 INNER JOIN tmotor 
							 ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN 
							 WHERE tmotor.DealerKode LIKE  '",xDealer,"' 
							 AND LokasiMotor.LokasiKode LIKE '",xLokasi,"' 
							 AND tmotor.MotorTahun BETWEEN ",xTahun1," AND ",xTahun2," 
							 ORDER BY MotorType, Jam, Notrans"); 
	 END IF;
	END IF;
	/* STOCKOPNAME */
	IF xTipe = "TDetailMotor3" THEN 
	 IF xQuick = '1' THEN
		 SET @MyQuery = CONCAT("SELECT tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun-2000 AS MotorTahun, LokasiMotor.MotorNoMesin, LokasiMotor.NoTrans, LokasiMotor.Jam, 
							 DATEDIFF('",xTgl2," 23:59:59',JamDatang) AS Durasi, Tmotor.FBHarga, LokasiMotor.LokasiKode, LokasiMotor.Kondisi, tmotor.FBNo, tmotor.MotorNoRangka,  tdmotortype.MotorNama  
							 FROM (SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi, JamDatang 
							 FROM tvmotorlokasi 
							 INNER JOIN (SELECT tvmotorlokasi_1.MotorAutoN, tvmotorlokasi_1.MotorNoMesin , MAX(tvmotorlokasi_1.Jam) AS Jam, MIN(tvmotorlokasi_1.Jam) AS JamDatang  
							 FROM tvmotorlokasi tvmotorlokasi_1 
							 WHERE tvmotorlokasi_1.Jam <= '",xTgl2," 23:59:59' 
							 GROUP BY tvmotorlokasi_1.MotorNoMesin, tvmotorlokasi_1.MotorAutoN) B 
							 ON tvmotorlokasi.MotorNoMesin = B.MotorNoMesin AND tvmotorlokasi.Jam = B.Jam AND tvmotorlokasi.MotorAutoN = B.MotorAutoN 
							 WHERE tvmotorlokasi.Kondisi IN ",xKondisi,") LokasiMotor 
							 INNER JOIN tmotor 
							 ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN 
							 INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
							 WHERE tmotor.DealerKode LIKE  '",xDealer,"' 
							 AND LokasiMotor.LokasiKode LIKE '",xLokasi,"' 
							 ORDER BY LokasiMotor.LokasiKode, MotorType, Jam, Notrans");
	 ELSE
		 SET @MyQuery = CONCAT("SELECT tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun-2000 AS MotorTahun, LokasiMotor.MotorNoMesin, LokasiMotor.NoTrans, LokasiMotor.Jam, DATEDIFF('",xTgl2," 23:59:59',JamDatang) AS Durasi , Tmotor.FBHarga, LokasiMotor.LokasiKode, LokasiMotor.Kondisi, tmotor.FBNo, tmotor.MotorNoRangka,  tdmotortype.MotorNama  
							 FROM (SELECT U.MotorAutoN, U.MotorNoMesin, U.LokasiKode, U.NoTrans, U.Jam, U.Kondisi 
							 FROM (SELECT vmotorlokasi.MotorAutoN, vmotorlokasi.MotorNoMesin, vmotorlokasi.LokasiKode, vmotorlokasi.NoTrans, vmotorlokasi.Jam, vmotorlokasi.Kondisi, JamDatang  
							 FROM vmotorlokasi 
							 WHERE vmotorlokasi.LokasiKode LIKE '%') U 
							 INNER JOIN (SELECT vmotorlokasi_1.MotorAutoN, vmotorlokasi_1.MotorNoMesin , MAX(vmotorlokasi_1.Jam) AS Jam, MIN(vmotorlokasi_1.Jam) AS JamDatang  
							 FROM vmotorlokasi vmotorlokasi_1 
							 WHERE vmotorlokasi_1.Jam <= '",xTgl2," 23:59:59' 
							 GROUP BY vmotorlokasi_1.MotorNoMesin, vmotorlokasi_1.MotorAutoN) B 
							 ON U.MotorNoMesin = B.MotorNoMesin AND U.Jam = B.Jam AND U.MotorAutoN = B.MotorAutoN 
							 WHERE U.Kondisi IN ",xKondisi,") LokasiMotor 
							 INNER JOIN tmotor 
							 ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN 
							 INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
							 WHERE tmotor.DealerKode LIKE  '",xDealer,"' 
							 AND LokasiMotor.LokasiKode LIKE '",xLokasi,"' 
							 ORDER BY LokasiMotor.LokasiKode, MotorType, Jam, Notrans"); 
	 END IF;
	END IF;
	/* DETAIL MOTOR HPP */
	IF xTipe = "TDetailMotor4" THEN 
		IF xFBPD = "1" THEN 
         SET @PostStr = " AND tmotor.FBNo <> '--' OR  tmotor.PDNo <> '--' ";
      ELSE 
			SET @PostStr = " ";
      END IF;
		IF xQuick = '1' THEN
			SET @MyQuery = CONCAT("SELECT tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, LokasiMotor.MotorNoMesin, LokasiMotor.NoTrans, LokasiMotor.Jam,  LokasiMotor.LokasiKode, LokasiMotor.Kondisi, 
			IF(tmotor.FBNo = '--',IF(tmotor.PDNo='--',tmotor.RKNo,tmotor.PDNo), tmotor.FBNo) AS FBNo, tmotor.MotorNoRangka, tmotor.FBHarga 
			FROM (SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi 
              FROM tvmotorlokasi 
                INNER JOIN (SELECT tvmotorlokasi_1.MotorAutoN, tvmotorlokasi_1.MotorNoMesin , MAX(tvmotorlokasi_1.Jam) AS Jam 
                            FROM tvmotorlokasi tvmotorlokasi_1 
                            WHERE tvmotorlokasi_1.Jam <= '",xTgl2," 23:59:59' 
                            GROUP BY tvmotorlokasi_1.MotorNoMesin, tvmotorlokasi_1.MotorAutoN) B 
                  ON tvmotorlokasi.MotorNoMesin = B.MotorNoMesin AND tvmotorlokasi.Jam = B.Jam AND tvmotorlokasi.MotorAutoN = B.MotorAutoN 
              WHERE tvmotorlokasi.Kondisi IN ",xKondisi," ) LokasiMotor 
          INNER JOIN tmotor 
            ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN 
         WHERE tmotor.DealerKode LIKE  '",xDealer,"' 
             AND LokasiMotor.LokasiKode LIKE '",xLokasi,"'  " , @PostStr ," ORDER BY MotorType, Jam, Notrans ;");
		ELSE
						SET @MyQuery = CONCAT("SELECT tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, LokasiMotor.MotorNoMesin, LokasiMotor.NoTrans, LokasiMotor.Jam,  LokasiMotor.LokasiKode, LokasiMotor.Kondisi, 
			IF(tmotor.FBNo = '--',IF(tmotor.PDNo='--',tmotor.RKNo,tmotor.PDNo), tmotor.FBNo) AS FBNo, tmotor.MotorNoRangka, tmotor.FBHarga 
			FROM (SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi 
              FROM tvmotorlokasi 
                INNER JOIN (SELECT tvmotorlokasi_1.MotorAutoN, tvmotorlokasi_1.MotorNoMesin , MAX(tvmotorlokasi_1.Jam) AS Jam 
                            FROM tvmotorlokasi tvmotorlokasi_1 
                            WHERE tvmotorlokasi_1.Jam <= '",xTgl2," 23:59:59' 
                            GROUP BY tvmotorlokasi_1.MotorNoMesin, tvmotorlokasi_1.MotorAutoN) B 
                  ON tvmotorlokasi.MotorNoMesin = B.MotorNoMesin AND tvmotorlokasi.Jam = B.Jam AND tvmotorlokasi.MotorAutoN = B.MotorAutoN 
              WHERE tvmotorlokasi.Kondisi IN ",xKondisi," ) LokasiMotor 
          INNER JOIN tmotor 
            ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN 
         WHERE tmotor.DealerKode LIKE  '",xDealer,"' 
             AND LokasiMotor.LokasiKode LIKE '",xLokasi,"'  " , @PostStr ," ORDER BY MotorType, Jam, Notrans ;");
		END IF;
	END IF;

	/* STOCK IN TRANSIT*/
	IF xTipe = "tmotorIntransit" THEN 
	 SET @MyQuery = CONCAT("SELECT  tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.FBNo, ttfb.FBTgl FROM tmotor 
						 INNER JOIN ttfb ON tmotor.FBNo = ttfb.FBNo  
						 WHERE tmotor.SSNo = '--' AND ttfb.FBTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
	END IF;
	/* AVERAGE UMUR */
	IF xTipe = "AverageUmur" THEN 
         SET @MyTgl2 = CONCAT(xTgl2 ," 23:59:59");
			SET @MyQuery = CONCAT("SELECT vmotorlastlokasi.LokasiKode, COUNT(vmotorlastlokasi.MotorNoMesin) AS Stock,
				AVG(DATEDIFF('",@MyTgl2,"', IFNULL(ttss.SStgl, IFNULL(ttpd.PDTgl, IFNULL(ttrk.RKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')))))) AS Umur
				FROM
				(SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi
				 FROM tvmotorlokasi
				 INNER JOIN
					(SELECT vmotorlokasi.MotorAutoN, vmotorlokasi.MotorNoMesin, MAX(vmotorlokasi.Jam) AS Jam FROM vmotorlokasi WHERE Jam <= CONCAT('",@MyTgl2,"')
					 GROUP BY vmotorlokasi.MotorNoMesin, vmotorlokasi.MotorAutoN
					 ORDER BY MotorNoMesin, MotorAutoN, Jam) vmotorlastjam
				 ON tvmotorlokasi.MotorNoMesin = vmotorlastjam.MotorNoMesin AND tvmotorlokasi.MotorAutoN = vmotorlastjam.MotorAutoN AND tvmotorlokasi.Jam = vmotorlastjam.Jam) vmotorlastlokasi
				INNER JOIN tmotor ON vmotorlastlokasi.MotorAutoN = tmotor.MotorAutoN AND vmotorlastlokasi.MotorNoMesin = tmotor.MotorNoMesin
				INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
				INNER JOIN tdlokasi ON vmotorlastlokasi.LokasiKode = tdlokasi.LokasiKode
				LEFT OUTER JOIN ttss ON tmotor.SSNo = ttss.SSNo
				LEFT OUTER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo
				LEFT OUTER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo
				WHERE vmotorlastlokasi.Kondisi IN ('IN', 'OUT Pos')
				GROUP BY LokasiKode
				ORDER BY tdlokasi.LokasiNomor;
			");	   
	END IF;
	/* MOTOR UMUR */
	IF xTipe = "MotorUmur" THEN 
         SET @MyTgl2 = CONCAT(xTgl2 ," 23:59:59");
			DROP TEMPORARY TABLE IF EXISTS MotorUmur;
			CREATE TEMPORARY TABLE IF NOT EXISTS MotorUmur AS
			SELECT vmotorlastlokasi.MotorAutoN, vmotorlastlokasi.MotorNoMesin,tmotor.MotorNoRangka, tmotor.MotorType,
				tmotor.MotorWarna, vmotorlastlokasi.LokasiKode, vmotorlastlokasi.NoTrans, vmotorlastlokasi.Jam,
				vmotorlastlokasi.Kondisi,tdmotortype.MotorNama,
				IFNULL(ttss.SStgl, IFNULL(ttpd.PDTgl, IFNULL(ttrk.RKTgl,STR_TO_DATE('01/01/1900', '%m/%d/%Y')))) AS 'DateIN', 0000000 AS Umur
				FROM
					(SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi
						FROM tvmotorlokasi
						INNER JOIN (SELECT vmotorlokasi.MotorAutoN, vmotorlokasi.MotorNoMesin, MAX(vmotorlokasi.Jam) AS Jam FROM vmotorlokasi
						WHERE Jam <= @MyTgl2
						GROUP BY vmotorlokasi.MotorNoMesin, vmotorlokasi.MotorAutoN
						ORDER BY MotorNoMesin, MotorAutoN, Jam) vmotorlastjam
						ON tvmotorlokasi.MotorNoMesin = vmotorlastjam.MotorNoMesin
						AND tvmotorlokasi.MotorAutoN = vmotorlastjam.MotorAutoN
						AND tvmotorlokasi.Jam = vmotorlastjam.Jam ) 
				vmotorlastlokasi
				INNER JOIN tmotor ON vmotorlastlokasi.MotorAutoN = tmotor.MotorAutoN AND vmotorlastlokasi.MotorNoMesin = tmotor.MotorNoMesin
				INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
				LEFT OUTER JOIN ttss ON tmotor.SSNo = ttss.SSNo
				LEFT OUTER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo
				LEFT OUTER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo
				WHERE vmotorlastlokasi.Kondisi IN ('IN','OUT Pos')
				ORDER BY IFNULL(ttss.SStgl, IFNULL(ttpd.PDTgl, IFNULL(ttrk.RKTgl,STR_TO_DATE('01/01/1900', '%m/%d/%Y')))), vmotorlastlokasi.LokasiKode, tdmotortype.MotorType ;
			CREATE INDEX MotorNoMesin ON MotorUmur(MotorNoMesin);
			UPDATE MotorUmur SET Umur = DATEDIFF(@MyTgl2,DateIN);
			SET @MyQuery = CONCAT("SELECT * FROM MotorUmur;");		   
	END IF;

	IF xTipe = "Stok PS 2" THEN
		CALL rGLStockHitung(xTgl1,xTgl2);
		SET @MyQuery = CONCAT("SELECT * FROM StockHarian;");		
 	END IF;
 	
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;

END$$

DELIMITER ;

