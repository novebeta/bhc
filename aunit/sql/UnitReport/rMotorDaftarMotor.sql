DELIMITER $$
DROP PROCEDURE IF EXISTS `rMotorDaftarMotor`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rMotorDaftarMotor`(IN xTipe VARCHAR(25),IN xTahun1 VARCHAR(4),IN xTahun2 VARCHAR(4),IN xQuick VARCHAR(1),IN xLokasi VARCHAR(30),IN xTypeMtr VARCHAR(25),IN xWarna VARCHAR(25),IN xKondisi VARCHAR(10),IN xDealer VARCHAR(20),IN xSort VARCHAR(25),IN xOrder VARCHAR(10))
BEGIN
/*
       xTipe    : "Motor" 
                  "Average Umur"
                  "Umur Motor" 
                  
       xSort    : "tmotor.MotorType"
                  "tmotor.MotorWarna"
                  "tmotor.MotorTahun"
                  "tmotor.MotorNoMesin"
                  "tmotor.MotorNoRangka"
                  "IFNULL(tdcustomer.CusNama, '--')"
                  "tmotor.STNKNo"
                  "tmotor.PlatNo"
                  "tmotor.BPKBNo"
                  "tmotor.STNKNama"
                  "tmotor.STNKAlamat"
                  "tmotor.FakturAHMNo"
                  "tmotor.BPKBTglJadi"
                  "tmotor.BPKBTglAmbil"
                  "tmotor.STNKTglMasuk"
                  "tmotor.STNKTglBayar"
                  "tmotor.STNKTglJadi"
                  "tmotor.STNKTglAmbil"
                  "tmotor.PlatTgl"

       Cara Akses :
       
       *** MOTOR ***
       CALL rMotorDaftarMotor('Motor',2018,2018,1,'%','%','%','%','%','tmotor.MotorType','ASC');
       
       *** AVERAGE MOTOR ***
       CALL rMotorDaftarMotor('Average Umur',2018,2018,1,'%','%','%','%','%','tmotor.MotorType','ASC');

       *** UMUR MOTOR ***
       CALL rMotorDaftarMotor('Umur Motor',2018,2018,1,'%','%','%','%','%','tmotor.MotorType','ASC');
*/

       SET xLokasi = CONCAT(xLokasi,"%");
       SET xDealer = CONCAT(xDealer,"%");
       SET xTypeMtr = CONCAT(xTypeMtr,"%");
       SET xWarna = CONCAT(xWarna,"%");
       SET xKondisi = CONCAT(xKondisi,"%");
              
       /* MOTOR */
       IF xTipe = "Motor" THEN 
          SET @MyQuery = "SELECT 
                          tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType, tmotor.MotorWarna, 
                          tmotor.MotorTahun, tmotor.MotorMemo, tmotor.DealerKode, tmotor.SSNo, tmotor.FBNo, tmotor.FBHarga, 
                          tmotor.DKNo, tmotor.SKNo, tmotor.PDNo, tmotor.SDNo, tmotor.BPKBNo, tmotor.BPKBTglJadi, tmotor.BPKBTglAmbil, 
                          tmotor.STNKNo, tmotor.STNKTglMasuk, tmotor.STNKTglBayar, tmotor.STNKTglJadi, tmotor.STNKTglAmbil, tmotor.STNKNama, 
                          tmotor.STNKAlamat, tmotor.PlatNo, tmotor.PlatTgl, tmotor.PlatTglAmbil, tmotor.FakturAHMNo, tmotor.FakturAHMTgl, tmotor.FakturAHMTglAmbil, 
                          LokasiMotor.LokasiKode, LokasiMotor.NoTrans, LokasiMotor.Jam, LokasiMotor.Kondisi, 
                          IFNULL(ttdk.DKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS DKTgl, IFNULL(ttss.SSTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SSTgl, IFNULL(ttfb.FBTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS FBTgl, 
                          IFNULL(ttpd.PDTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS  PDTgl, IFNULL(ttsd.SDTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS  SDTgl, IFNULL(ttsk.SKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS  SKTgl, 
                          IFNULL(tdcustomer.CusNama, '--') AS CusNama, tmotor.RKNo, IFNULL(ttrk.RKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS  RKTgl, IFNULL(ttin.INNo, '--') AS INNo, IFNULL(ttin.INTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS  INTgl  
                          FROM 
                          (SELECT vmotorlokasi.MotorAutoN, vmotorlokasi.MotorNoMesin, vmotorlokasi.LokasiKode, vmotorlokasi.NoTrans, vmotorlokasi.Jam, vmotorlokasi.Kondisi 
                          FROM vmotorlokasi 
                          INNER JOIN 
                          (SELECT vmotorlokasi_1.MotorAutoN, vmotorlokasi_1.MotorNoMesin , MAX(vmotorlokasi_1.Jam) AS Jam 
                          FROM vmotorlokasi vmotorlokasi_1 
                          GROUP BY vmotorlokasi_1.MotorNoMesin, vmotorlokasi_1.MotorAutoN) B 
                          ON vmotorlokasi.MotorNoMesin = B.MotorNoMesin AND vmotorlokasi.Jam = B.Jam AND vmotorlokasi.MotorAutoN = B.MotorAutoN 
                          WHERE ";
                          
          SET @MyQuery = CONCAT(@MyQuery," vmotorlokasi.Kondisi LIKE ('",xKondisi,"')) LokasiMotor 
                         INNER JOIN tmotor ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN 
                         LEFT OUTER JOIN ttss ON tmotor.SSNo = ttss.SSNo 
                         LEFT OUTER JOIN ttfb ON tmotor.FBNo = ttfb.FBNo 
                         LEFT OUTER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo 
                         LEFT OUTER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo 
                         LEFT OUTER JOIN ttsd ON tmotor.SDNo = ttsd.SDNo 
                         LEFT OUTER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         LEFT OUTER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo 
                         LEFT OUTER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                         LEFT OUTER JOIN ttin ON ttdk.INNo = ttin.INNo 
                         WHERE 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND 
                         (tmotor.MotorWarna LIKE '",xWarna,"') AND
                         (MotorTahun BETWEEN '",xTahun1,"' AND '",xTahun2,"') AND
                         (LokasiMotor.LokasiKode LIKE '",xLokasi,"')                          
                         ORDER BY tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin"); 
          IF xKondisi = "Transit" THEN
             SET @MyQuery = "SELECT 
                             tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorMemo, 
                             tmotor.DealerKode, tmotor.SSNo, tmotor.FBNo, tmotor.FBHarga, tmotor.DKNo, tmotor.SKNo, tmotor.PDNo, tmotor.SDNo, tmotor.BPKBNo,
                             tmotor.BPKBTglJadi, tmotor.BPKBTglAmbil, tmotor.STNKNo, tmotor.STNKTglMasuk, tmotor.STNKTglBayar, tmotor.STNKTglJadi, tmotor.STNKTglAmbil,
                             tmotor.STNKNama, tmotor.STNKAlamat, tmotor.PlatNo, tmotor.PlatTgl, tmotor.PlatTglAmbil, tmotor.FakturAHMNo, tmotor.FakturAHMTgl, 
                             tmotor.FakturAHMTglAmbil, 'In Transit' AS LokasiKode, '--' AS NoTrans, ttfb.FBTgl AS  Jam, 'Transit' AS Kondisi, 
                             IFNULL(ttdk.DKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS  DKTgl, IFNULL(ttss.SSTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS  SSTgl, IFNULL(ttfb.FBTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS  FBTgl, 
                             IFNULL(ttpd.PDTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS  PDTgl, IFNULL(ttsd.SDTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS  SDTgl, IFNULL(ttsk.SKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS  SKTgl, 
                             IFNULL(tdcustomer.CusNama, '--') AS CusNama, tmotor.RKNo, IFNULL(ttrk.RKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS  RKTgl, IFNULL(ttin.INNo, '--') AS INNo, IFNULL(ttin.INTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS  INTgl 
                             FROM tmotor 
                             INNER JOIN ttfb ON tmotor.FBNo = ttfb.FBNo 
                             LEFT OUTER JOIN ttss ON tmotor.SSNo = ttss.SSNo 
                             LEFT OUTER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo 
                             LEFT OUTER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo 
                             LEFT OUTER JOIN ttsd ON tmotor.SDNo = ttsd.SDNo 
                             LEFT OUTER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                             LEFT OUTER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo 
                             LEFT OUTER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                             LEFT OUTER JOIN ttin ON ttdk.INNo = ttin.INNo 
                             WHERE ";
                             
             SET @MyQuery = CONCAT(@MyQuery," (tmotor.MotorType LIKE '",xTypeMtr,"' AND tmotor.MotorWarna LIKE '",xWarna,"') AND
                            (MotorTahun BETWEEN '",xTahun1,"' AND '",xTahun2,"') AND
                            Tmotor.SSNo = '--' ORDER BY tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin");
          END IF;
          
          IF xQuick = "1" THEN
             SELECT REPLACE(@MyQuery,"vmotorlokasi","tvmotorlokasi") INTO @MyQuery;
          END IF;
       END IF;

       IF xTipe = "Average Umur" THEN 
         SET @MyTgl2 = CONCAT(xTahun2,DATE_FORMAT(CURDATE(),"-%m-%d 23:59:59"));
			SET @MyQuery = CONCAT("SELECT vmotorlastlokasi.LokasiKode, COUNT(vmotorlastlokasi.MotorNoMesin) AS Stock,
				AVG(DATEDIFF('",@MyTgl2,"', IFNULL(ttss.SStgl, IFNULL(ttpd.PDTgl, IFNULL(ttrk.RKTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')))))) AS Umur
				FROM
				(SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi
				 FROM tvmotorlokasi
				 INNER JOIN
					(SELECT vmotorlokasi.MotorAutoN, vmotorlokasi.MotorNoMesin, MAX(vmotorlokasi.Jam) AS Jam FROM vmotorlokasi WHERE Jam <= CONCAT('",@MyTgl2,"')
					 GROUP BY vmotorlokasi.MotorNoMesin, vmotorlokasi.MotorAutoN
					 ORDER BY MotorNoMesin, MotorAutoN, Jam) vmotorlastjam
				 ON tvmotorlokasi.MotorNoMesin = vmotorlastjam.MotorNoMesin AND tvmotorlokasi.MotorAutoN = vmotorlastjam.MotorAutoN AND tvmotorlokasi.Jam = vmotorlastjam.Jam) vmotorlastlokasi
				INNER JOIN tmotor ON vmotorlastlokasi.MotorAutoN = tmotor.MotorAutoN AND vmotorlastlokasi.MotorNoMesin = tmotor.MotorNoMesin
				INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
				INNER JOIN tdlokasi ON vmotorlastlokasi.LokasiKode = tdlokasi.LokasiKode
				LEFT OUTER JOIN ttss ON tmotor.SSNo = ttss.SSNo
				LEFT OUTER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo
				LEFT OUTER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo
				WHERE vmotorlastlokasi.Kondisi IN ('IN', 'OUT Pos')
				GROUP BY LokasiKode
				ORDER BY tdlokasi.LokasiNomor;
			");	   
       END IF;

       IF xTipe = "Umur Motor" THEN 
         SET @MyTgl2 = CONCAT(xTahun2,DATE_FORMAT(CURDATE(),"-%m-%d 23:59:59"));
			DROP TEMPORARY TABLE IF EXISTS MotorUmur;
			CREATE TEMPORARY TABLE IF NOT EXISTS MotorUmur AS
			SELECT vmotorlastlokasi.MotorAutoN, vmotorlastlokasi.MotorNoMesin,tmotor.MotorNoRangka, tmotor.MotorType,
				tmotor.MotorWarna, vmotorlastlokasi.LokasiKode, vmotorlastlokasi.NoTrans, vmotorlastlokasi.Jam,
				vmotorlastlokasi.Kondisi,tdmotortype.MotorNama,
				IFNULL(ttss.SStgl, IFNULL(ttpd.PDTgl, IFNULL(ttrk.RKTgl,STR_TO_DATE('01/01/1900', '%m/%d/%Y')))) AS 'DateIN', 0000000 AS Umur
				FROM
					(SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi
						FROM tvmotorlokasi
						INNER JOIN (SELECT vmotorlokasi.MotorAutoN, vmotorlokasi.MotorNoMesin, MAX(vmotorlokasi.Jam) AS Jam FROM vmotorlokasi
						WHERE Jam <= @MyTgl2
						GROUP BY vmotorlokasi.MotorNoMesin, vmotorlokasi.MotorAutoN
						ORDER BY MotorNoMesin, MotorAutoN, Jam) vmotorlastjam
						ON tvmotorlokasi.MotorNoMesin = vmotorlastjam.MotorNoMesin
						AND tvmotorlokasi.MotorAutoN = vmotorlastjam.MotorAutoN
						AND tvmotorlokasi.Jam = vmotorlastjam.Jam ) 
				vmotorlastlokasi
				INNER JOIN tmotor ON vmotorlastlokasi.MotorAutoN = tmotor.MotorAutoN AND vmotorlastlokasi.MotorNoMesin = tmotor.MotorNoMesin
				INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
				LEFT OUTER JOIN ttss ON tmotor.SSNo = ttss.SSNo
				LEFT OUTER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo
				LEFT OUTER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo
				WHERE vmotorlastlokasi.Kondisi IN ('IN','OUT Pos')
				ORDER BY IFNULL(ttss.SStgl, IFNULL(ttpd.PDTgl, IFNULL(ttrk.RKTgl,STR_TO_DATE('01/01/1900', '%m/%d/%Y')))), vmotorlastlokasi.LokasiKode, tdmotortype.MotorType ;
			CREATE INDEX MotorNoMesin ON MotorUmur(MotorNoMesin);
			UPDATE MotorUmur SET Umur = DATEDIFF(@MyTgl2,DateIN);
			SET @MyQuery = CONCAT("SELECT * FROM MotorUmur;");		   
       END IF;


       PREPARE STMT FROM @MyQuery;
       EXECUTE Stmt;
    
    END$$
DELIMITER ;

