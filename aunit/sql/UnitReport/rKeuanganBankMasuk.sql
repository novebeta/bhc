DELIMITER $$
DROP PROCEDURE IF EXISTS `rKeuanganBankMasuk`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rKeuanganBankMasuk`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,IN xJenis VARCHAR(30),IN xBank VARCHAR(30),IN xLeasing VARCHAR(30),IN xJurnal NUMERIC,IN xSort VARCHAR(20),IN xOrder VARCHAR(10))
BEGIN
/*
           Cara Akses SP :
           CALL rKeuanganBankMasuk(xTipe,xTgl1,xTgl2,xJenis,xBank,xLeasing,xJurnal,xSort,xOrder);
           xTipe ->ttbmhd,ttbmit,ttbmitKK
           CALL rKeuanganBankMasuk('ttbmhd','2014-04-30','2014-04-30','Neraca Awal','','TUNAI',0,'BMTgl','ASC');
           CALL rKeuanganBankMasuk('ttbmit','2014-04-30','2019-04-30','','','',0,'BMTgl','ASC');
           CALL rKeuanganBankMasuk('ttbmitKK','2014-04-30','2019-04-30','','','',0,'BMTgl','ASC');
*/
	   SET xJenis = CONCAT(xJenis,"%");
	   SET xBank = CONCAT(xBank,"%");
	   SET xLeasing = CONCAT(xLeasing,"%");
	   IF xTipe = "ttbmhd" THEN
	      SET @MyQuery = CONCAT("SELECT ttbmhd.BMNo, ttbmhd.BMTgl, ttbmhd.BMNominal,  ttbmhd.LeaseKode,
              ttbmhd.BMCekTempo, ttbmhd.NoAccount, ttbmhd.BMMemo, ttbmhd.UserID,
              ttbmhd.BMCekNo, ttbmhd.BMPerson,  ttbmhd.BMAC, ttbmhd.BMJenis, 
              IFNULL(tdleasing.LeaseNama, '--') AS LeaseNama, IFNULL(traccount.NamaAccount, '--') AS NamaAccount, 
              NoGL AS GLLINK 
              FROM ttbmhd 
              INNER JOIN tdleasing ON ttbmhd.LeaseKode = tdleasing.LeaseKode 
              INNER JOIN traccount ON ttbmhd.NoAccount = traccount.NoAccount 
              WHERE 
              (ttbmhd.BMTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
              (ttbmhd.BMJenis LIKE '",xJenis,"') AND 
              (ttbmhd.LeaseKode LIKE '",xLeasing,"') AND
              (ttbmhd.NoAccount LIKE '",xBank,"')");
              IF xJurnal = 1 THEN
                 SET @MyQuery = CONCAT(@MyQuery,"AND NoGL <> '--' ");
              ELSE
                  IF xJurnal = 2 THEN
                     SET @MyQuery = CONCAT(@MyQuery,"AND NoGL = '--' ");
                  END IF;
              END IF;
              SET @MyQuery = CONCAT(@MyQuery,"ORDER BY ",xSort," ",xOrder);
	   END IF;

	   IF xTipe = "ttbmit" THEN
	      SET @MyQuery = CONCAT("SELECT ttbmhd.BMTGl, ttbmit.BMNo, ttbmit.DKNo, ttdk.DKTgl, ttdk.CusKode, tdcustomer.CusNama, 
         CONCAT_WS('', tdcustomer.CusAlamat, '  RT/RW : ', tdcustomer.CusRT, '/',tdcustomer.CusRW) AS CusAlamat, 
         tmotor.MotorType, tmotor.MotorNoMesin, ttdk.LeaseKode, ttdk.DKNetto, BMPaid.Paid - ttbmit.BMBayar AS Terbayar, 
         ttbmit.BMBayar, ttdk.DKNetto - BMPaid.Paid AS Sisa 
         FROM ttbmit 
         INNER JOIN ttdk ON ttbmit.DKNo = ttdk.DKNo 
         INNER JOIN tmotor ON ttbmit.DKNo = tmotor.DKNo 
         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
         INNER JOIN ttbmhd ON ttbmit.BMNo = ttbmhd.BMNo 
         INNER JOIN 
         (SELECT  DKNo, SUM(BMBayar) AS Paid FROM ttbmit ttbmit_1 GROUP BY DKNo) BMPaid ON ttbmit.DKNo = BMPaid.DKNo 
         WHERE 
         (ttbmhd.BMTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
         (ttbmhd.BMJenis LIKE '",xJenis,"') AND 
         (ttbmhd.LeaseKode LIKE '",xLeasing,"') AND 
         (ttbmhd.NoAccount LIKE '",xBank,"')");
              IF xJurnal = 1 THEN
                 SET @MyQuery = CONCAT(@MyQuery," AND ( ttbmhd.NoGL <> '--' ) ");
              ELSE
                  IF xJurnal = 2 THEN
                     SET @MyQuery = CONCAT(@MyQuery," AND ( ttbmhd.NoGL = '--' ) ");
                  END IF;
              END IF;
              SET @MyQuery = CONCAT(@MyQuery,"ORDER BY ttbmit.BMNo,",xSort," ",xOrder);
	   END IF;

	   IF xTipe = "ttbmitKK" THEN
	      SET @MyQuery = CONCAT("SELECT ttbmit.BMNo, ttbmit.DKNo, ttbmit.BMBayar, ttkk.KKTgl, ttkk.KKNominal, 
         ttkk.KKPerson, ttkk.KKMemo, ttkk.KKJenis, ttkk.KKLink 
         FROM ttbmhd 
         INNER JOIN ttbmit ON ttbmhd.BMNo = ttbmit.BMNo 
         INNER JOIN ttkk ON ttbmit.DKNo = ttkk.KKNo 
          WHERE 
         (ttbmhd.BMTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
         (ttbmhd.BMJenis LIKE '",xJenis,"') AND 
         (ttbmhd.LeaseKode LIKE 'TUNAI') AND 
         (ttbmhd.NoAccount LIKE '",xBank,"')");
              IF xJurnal = 1 THEN
                 SET @MyQuery = CONCAT(@MyQuery," AND ( ttbmhd.NoGL <> '--' ) ");
              ELSE
                  IF xJurnal = 2 THEN
                     SET @MyQuery = CONCAT(@MyQuery," AND ( ttbmhd.NoGL = '--' ) ");
                  END IF;
              END IF;
              SET @MyQuery = CONCAT(@MyQuery,"ORDER BY ttbmit.BMNo,",xSort," ",xOrder);
	   END IF;

           PREPARE STMT FROM @MyQuery;
           EXECUTE Stmt;

	END$$

DELIMITER ;

