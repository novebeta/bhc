DELIMITER $$
DROP PROCEDURE IF EXISTS `rMutasiPPOS`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rMutasiPPOS`(IN xTipe VARCHAR(25),IN xTgl1 DATE,IN xTgl2 DATE,IN xLokasi VARCHAR(30),IN xDealer VARCHAR(20),IN xSort VARCHAR(25),IN xOrder VARCHAR(10))
BEGIN
/*
       xTipe    : "ttpbhd" -> Header
                  "ttpbit" -> Detail
                  
       xSort    : "ttPBhd.PBTgl"
                  "ttpbhd.PBNo"
                  "ttpbhd.SMNo"
                  "ttPBhd.LokasiAsal"
                  "IFNULL(tdlokasi.LokasiNama, '--')"
                  "ttpbhd.LokasiTujuan"
                  "IFNULL(tdlokasi_1.LokasiNama, '--')"
                  "ttPBhd.PBMemo"
                  "ttPBhd.PBTotal"
                  "ttpbhd.UserID"

       Cara Akses :
       
		 *** HEADER ***
		 CALL rMutasiPPOS('ttpbhd','2001-01-01','2019-12-31','','','ttPBhd.PBTgl','ASC');

		 *** DETAIL ***
		 CALL rMutasiPPOS('ttpbit','2001-01-01','2019-12-31','','','ttPBhd.PBTgl','ASC');

*/

       SET xLokasi = CONCAT(xLokasi,"%");
       SET xDealer = CONCAT(xDealer,"%");
       
       /* HEADER */
       IF xTipe = "ttpbhd" THEN 
          SET @MyQuery = "SELECT ttpbhd.PBNo, ttpbhd.PBTgl, ttpbhd.SMNo, ttpbhd.LokasiAsal, ttpbhd.LokasiTujuan, ttpbhd.PBMemo, ttpbhd.PBTotal, 
                          ttpbhd.UserID, IFNULL(tdlokasi.LokasiNama, '--') AS LokasiNamaAsal, IFNULL(tdlokasi_1.LokasiNama, '--') AS LokasiNamaTujuan, 
                          IFNULL(ttsmhd.SMTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SMTgl 
                          FROM ttpbhd 
                          LEFT OUTER JOIN ttsmhd ON ttpbhd.SMNo = ttsmhd.SMNo 
                          LEFT OUTER JOIN tdlokasi ON ttpbhd.LokasiAsal = tdlokasi.LokasiKode 
                          LEFT OUTER JOIN tdlokasi tdlokasi_1 ON ttpbhd.LokasiTujuan = tdlokasi_1.LokasiKode 
                          WHERE ";
                          
          SET @MyQuery = CONCAT(@MyQuery,"(ttpbhd.PBTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttpbhd.LokasiAsal LIKE '",xLokasi,"') AND 
                         (ttpbhd.LokasiTujuan LIKE '",xDealer,"') ORDER BY ",xSort," ",xOrder); 
       END IF;


       /* DETAIL */
       IF xTipe = "ttpbit" THEN 
          SET @MyQuery = "SELECT ttpbit.PBNo, ttpbit.MotorNoMesin, tmotor.MotorType, tmotor.MotorTahun, 
                          tmotor.MotorWarna, tmotor.MotorNoRangka, tmotor.FBHarga 
                          FROM ttpbit 
                          INNER JOIN tmotor ON ttpbit.MotorNoMesin = tmotor.MotorNoMesin AND ttpbit.MotorAutoN = tmotor.MotorAutoN 
                          INNER JOIN ttpbhd ON ttpbhd.PBNo = ttpbit.PBNo 
                          WHERE ";
                          
          SET @MyQuery = CONCAT(@MyQuery,"(ttpbhd.PBTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttpbhd.LokasiAsal LIKE '",xLokasi,"') AND 
                         (ttpbhd.LokasiTujuan LIKE '",xDealer,"') ORDER BY ",xSort," ",xOrder); 
       END IF;

       PREPARE STMT FROM @MyQuery;
       EXECUTE Stmt;
    
    END$$
DELIMITER ;

