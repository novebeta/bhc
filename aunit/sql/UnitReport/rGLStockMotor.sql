DELIMITER $$

DROP PROCEDURE IF EXISTS `rGLStockMotor`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rGLStockMotor`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE, IN xKondisi VARCHAR(3), IN xRingkas VARCHAR(1))
BEGIN
/*
CALL rGLStockMotor("Motor Stock",'2020-02-01','2020-02-29', '', '');
*/       
	DECLARE TanggalSaldo, TanggalAwal, TanggalAkhir DATE DEFAULT '1900-01-01';

	IF xTipe = "Motor Stock" OR xTipe = "Tabel Stock" OR xTipe = "Stok PS 1" THEN
		SET TanggalSaldo = '1900-01-01';
		SET TanggalAwal = '1900-01-02';
		SET TanggalAkhir = CURDATE();

		SELECT MAX(TglTrans) INTO TanggalSaldo FROM tsaldomotor WHERE TglTrans <= xTgl1;
		IF TanggalSaldo = '1900-01-01' THEN
			SELECT DATE_ADD(TglGL,INTERVAL -1 DAY) INTO TanggalSaldo FROM ttgeneralledgerhd WHERE KodeTrans = 'NA' AND GLLink = 'NA';
		END IF;

		SET TanggalAwal = DATE_ADD(TanggalSaldo, INTERVAL 1 DAY);
		SET TanggalAkhir = DATE_ADD(xTgl1, INTERVAL -1 DAY);

		DROP TEMPORARY TABLE IF EXISTS StockAcct;
		CREATE TEMPORARY TABLE IF NOT EXISTS StockAcct AS
		SELECT MotorType, MotorNama, SPACE(100) AS MotorWarna, 0000 AS MotorTahun,
		0000000 AS SALJum, 00000000000000.00 AS SALHrg ,
		0000000 AS SAJum, 00000000000000.00 AS SAHrg ,
		0000000 AS SSJum, 00000000000000.00 AS SSHrg ,
		0000000 AS FBJum, 00000000000000.00 AS FBHrg ,
		0000000 AS PDJum, 00000000000000.00 AS PDHrg ,
		0000000 AS RKJum, 00000000000000.00 AS RKHrg ,
		0000000 AS SKJum, 00000000000000.00 AS SKHrg ,
		0000000 AS MEJum, 00000000000000.00 AS MEHrg ,
		0000000 AS SRJum, 00000000000000.00 AS SRHrg ,
		0000000 AS MMJum, 00000000000000.00 AS MMHrg ,
		0000000 AS SARJum, 0000000000.00 AS SARHrg
		FROM tdmotortype ORDER BY MotorType;
		CREATE INDEX MotorType ON StockAcct(MotorType);

		/*Saldo Awal*/
		DROP TEMPORARY TABLE IF EXISTS StockSA;
		CREATE TEMPORARY TABLE IF NOT EXISTS StockSA AS
		SELECT tdmotortype.MotorType, IFNULL(Fill.Jum,0) AS Jum , IFNULL(Fill.Hrg,0) AS Hrg FROM tdmotortype
		LEFT OUTER JOIN
		(SELECT MotorType, COUNT(MotorNoMesin) AS Jum, SUM(MotorHarga) AS Hrg FROM tsaldomotor
		WHERE TglTrans BETWEEN TanggalSaldo AND TanggalSaldo GROUP BY MotorType ) Fill
		ON tdmotortype.MotorType = Fill.MotorType
		ORDER BY  tdmotortype.MotorType;
		CREATE INDEX MotorType ON StockSA(MotorType);

		DROP TEMPORARY TABLE IF EXISTS StockSS;
		CREATE TEMPORARY TABLE IF NOT EXISTS StockSS AS
		SELECT tdmotortype.MotorType, IFNULL(Fill.Jum,0) AS Jum , IFNULL(Fill.Hrg,0) AS Hrg FROM tdmotortype
		LEFT OUTER JOIN
		(SELECT MotorType, COUNT(tmotor.MotorNoMesin) AS Jum, (SUM(tmotor.SSHarga)) AS Hrg FROM tmotor
		INNER JOIN ttSS ON tmotor.SSNo = ttSS.SSNo WHERE ttSS.SSTgl BETWEEN TanggalAwal AND TanggalAkhir GROUP BY tmotor.MotorType) Fill
		ON tdmotortype.MotorType = Fill.MotorType
		ORDER BY  tdmotortype.MotorType;
		CREATE INDEX MotorType ON StockSS(MotorType);

		DROP TEMPORARY TABLE IF EXISTS StockFB;
		CREATE TEMPORARY TABLE IF NOT EXISTS StockFB AS
		SELECT tdmotortype.MotorType, IFNULL(Fill.Jum,0) AS Jum , IFNULL(Fill.Hrg,0) AS Hrg FROM tdmotortype
		LEFT OUTER JOIN
		(SELECT MotorType, COUNT(tmotor.MotorNoMesin) AS Jum, (SUM(tmotor.FBHarga)-SUM(tmotor.SSHarga)) AS Hrg FROM tmotor
		INNER JOIN ttfb ON tmotor.FBNo = ttfb.FbNo WHERE ttfb.FbTgl BETWEEN TanggalAwal AND TanggalAkhir GROUP BY tmotor.MotorType ) Fill
		ON tdmotortype.MotorType = Fill.MotorType
		ORDER BY  tdmotortype.MotorType;
		CREATE INDEX MotorType ON StockFB(MotorType);

		DROP TEMPORARY TABLE IF EXISTS StockPD;
		CREATE TEMPORARY TABLE IF NOT EXISTS StockPD AS
		SELECT tdmotortype.MotorType, IFNULL(Fill.Jum,0) AS Jum , IFNULL(Fill.Hrg,0) AS Hrg FROM tdmotortype
		LEFT OUTER JOIN
		(SELECT MotorType, COUNT(tmotor.MotorNoMesin) AS Jum, SUM(tmotor.SSHarga) AS Hrg FROM tmotor
		INNER JOIN ttPD ON tmotor.PDNo = ttPD.PDNo WHERE ttPD.PDTgl BETWEEN TanggalAwal AND TanggalAkhir GROUP BY tmotor.MotorType ) Fill
		ON tdmotortype.MotorType = Fill.MotorType
		ORDER BY  tdmotortype.MotorType;
		CREATE INDEX MotorType ON StockPD(MotorType);

		DROP TEMPORARY TABLE IF EXISTS StockRK;
		CREATE TEMPORARY TABLE IF NOT EXISTS StockRK AS
		SELECT tdmotortype.MotorType, IFNULL(Fill.Jum,0) AS Jum , IFNULL(Fill.Hrg,0) AS Hrg FROM tdmotortype
		LEFT OUTER JOIN
		(SELECT MotorType, COUNT(tmotor.MotorNoMesin) AS Jum, SUM(tmotor.RKHarga) AS Hrg FROM tmotor
		INNER JOIN ttRK ON tmotor.RKNo = ttRK.RKNo WHERE ttRK.RKTgl BETWEEN TanggalAwal AND TanggalAkhir GROUP BY tmotor.MotorType ) Fill
		ON tdmotortype.MotorType = Fill.MotorType
		ORDER BY  tdmotortype.MotorType;
		CREATE INDEX MotorType ON StockRK(MotorType);

		DROP TEMPORARY TABLE IF EXISTS StockSK;
		CREATE TEMPORARY TABLE IF NOT EXISTS StockSK AS
		SELECT tdmotortype.MotorType, IFNULL(Fill.Jum,0) AS Jum , IFNULL(Fill.Hrg,0) AS Hrg FROM tdmotortype
		LEFT OUTER JOIN
		(SELECT MotorType, COUNT(tmotor.MotorNoMesin) AS Jum, SUM(tmotor.SKHarga) AS Hrg
		FROM (SELECT MotorType, tmotor.MotorNoMesin, tmotor.SKHarga FROM tmotor
		INNER JOIN ttSK ON tmotor.SKNo = ttSK.SKNo
		WHERE ttSK.SKTgl BETWEEN TanggalAwal AND TanggalAkhir
		UNION ALL
		SELECT tmotor.MotorType, tmotor.MotorNoMesin, tmotor.SKHarga FROM tmotor
		INNER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo
		INNER JOIN ttsk ON ttrk.SKNo = ttsk.SKNo
		WHERE ttSK.SKTgl BETWEEN TanggalAwal AND TanggalAkhir
		AND (tmotor.SSNo <> '--' OR tmotor.FBNo <> '--' OR tmotor.PDNo <>'--')
		)tmotor
		GROUP BY tmotor.MotorType ) Fill
		ON tdmotortype.MotorType = Fill.MotorType
		ORDER BY  tdmotortype.MotorType;
		CREATE INDEX MotorType ON StockSK(MotorType);

		DROP TEMPORARY TABLE IF EXISTS StockME;
		CREATE TEMPORARY TABLE IF NOT EXISTS StockME AS
		SELECT tdmotortype.MotorType, IFNULL(Fill.Jum,0) AS Jum , IFNULL(Fill.Hrg,0) AS Hrg FROM tdmotortype
		LEFT OUTER JOIN
		(SELECT MotorType, COUNT(tmotor.MotorNoMesin) AS Jum, SUM(tmotor.SDHarga) AS Hrg FROM tmotor
		INNER JOIN ttSD ON tmotor.SDNo = ttSD.SDNo WHERE LEFT(ttSD.SDNo,2) = 'ME' AND ttSD.SDTgl BETWEEN TanggalAwal AND TanggalAkhir GROUP BY tmotor.MotorType ) Fill
		ON tdmotortype.MotorType = Fill.MotorType
		ORDER BY  tdmotortype.MotorType;
		CREATE INDEX MotorType ON StockME(MotorType);

		DROP TEMPORARY TABLE IF EXISTS StockSR;
		CREATE TEMPORARY TABLE IF NOT EXISTS StockSR AS
		SELECT tdmotortype.MotorType, IFNULL(Fill.Jum,0) AS Jum , IFNULL(Fill.Hrg,0) AS Hrg FROM tdmotortype
		LEFT OUTER JOIN
		(SELECT MotorType, COUNT(tmotor.MotorNoMesin) AS Jum, SUM(tmotor.SDHarga) AS Hrg FROM tmotor
		INNER JOIN ttSD ON tmotor.SDNo = ttSD.SDNo WHERE LEFT(ttSD.SDNo,2) = 'SR' AND ttSD.SDTgl BETWEEN TanggalAwal AND TanggalAkhir GROUP BY tmotor.MotorType ) Fill
		ON tdmotortype.MotorType = Fill.MotorType
		ORDER BY  tdmotortype.MotorType;
		CREATE INDEX MotorType ON StockSR(MotorType);

		DROP TEMPORARY TABLE IF EXISTS StockMM;
		CREATE TEMPORARY TABLE IF NOT EXISTS StockMM AS
		SELECT tdmotortype.MotorType, 0 AS Jum , IFNULL(Fill.Hrg,0) AS Hrg FROM tdmotortype
		LEFT OUTER JOIN
		(SELECT MotorType, SUM(tmotor.MMHarga) AS Hrg FROM ttamhd
		INNER JOIN ttamit ON ttamhd.AMNo = ttamit.AMNo
		INNER JOIN tmotor ON ttamit.MotorNoMesin = tmotor.MotorNoMesin  AND ttamit.MotorAutoN = tmotor.MotorAutoN
		WHERE ttamhd.amtgl BETWEEN TanggalAwal AND TanggalAkhir GROUP BY tmotor.MotorType ) Fill
		ON tdmotortype.MotorType = Fill.MotorType
		ORDER BY  tdmotortype.MotorType;
		CREATE INDEX MotorType ON StockMM(MotorType);

		UPDATE StockAcct INNER JOIN StockSA ON StockAcct.MotorType = StockSA.MotorType SET SAJum = Jum, SAHrg = Hrg;
		UPDATE StockAcct INNER JOIN StockSS ON StockAcct.MotorType = StockSS.MotorType SET SSJum = Jum, SSHrg = Hrg;
		UPDATE StockAcct INNER JOIN StockFB ON StockAcct.MotorType = StockFB.MotorType SET FBJum = Jum, FBHrg = Hrg;
		UPDATE StockAcct INNER JOIN StockPD ON StockAcct.MotorType = StockPD.MotorType SET PDJum = Jum, PDHrg = Hrg;
		UPDATE StockAcct INNER JOIN StockRK ON StockAcct.MotorType = StockRK.MotorType SET RKJum = Jum, RKHrg = Hrg;
		UPDATE StockAcct INNER JOIN StockSK ON StockAcct.MotorType = StockSK.MotorType SET SKJum = Jum, SKHrg = Hrg;
		UPDATE StockAcct INNER JOIN StockME ON StockAcct.MotorType = StockME.MotorType SET MEJum = Jum, MEHrg = Hrg;
		UPDATE StockAcct INNER JOIN StockSR ON StockAcct.MotorType = StockSR.MotorType SET SRJum = Jum, SRHrg = Hrg;
		UPDATE StockAcct INNER JOIN StockMM ON StockAcct.MotorType = StockMM.MotorType SET MMJum = Jum, MMHrg = Hrg;

		UPDATE StockAcct SET SALJum = SAJum + PDJum + RKJum - SKJum - MEJum - SRJum + SSJum;
		UPDATE StockAcct SET SALHrg = SAHrg + FBHrg + PDHrg + RKHrg - SKHrg - MEHrg - SRHrg + MMHrg + SSHrg;
		UPDATE StockAcct SET SAJum = 0, FBJum = 0, PDJum = 0, RKJum = 0, SKJum = 0, MEJum = 0, SRJum = 0, SSJum = 0, MMJum = 0;
		UPDATE StockAcct SET SAHrg = 0, FBHrg = 0, PDHrg = 0, RKHrg = 0, SKHrg = 0, MEHrg = 0, SRHrg = 0, SSHrg = 0, MMHrg = 0;

		SET TanggalAwal = xTgl1;
		SET TanggalAkhir = xTgl2;
		DROP TEMPORARY TABLE IF EXISTS StockSS;
		CREATE TEMPORARY TABLE IF NOT EXISTS StockSS AS
		SELECT tdmotortype.MotorType, IFNULL(Fill.Jum,0) AS Jum , IFNULL(Fill.Hrg,0) AS Hrg FROM tdmotortype
		LEFT OUTER JOIN
		(SELECT MotorType, COUNT(tmotor.MotorNoMesin) AS Jum, (SUM(tmotor.SSHarga)) AS Hrg FROM tmotor
		INNER JOIN ttSS ON tmotor.SSNo = ttSS.SSNo WHERE ttSS.SSTgl BETWEEN TanggalAwal AND TanggalAkhir GROUP BY tmotor.MotorType) Fill
		ON tdmotortype.MotorType = Fill.MotorType
		ORDER BY  tdmotortype.MotorType;
		CREATE INDEX MotorType ON StockSS(MotorType);

		DROP TEMPORARY TABLE IF EXISTS StockFB;
		CREATE TEMPORARY TABLE IF NOT EXISTS StockFB AS
		SELECT tdmotortype.MotorType, IFNULL(Fill.Jum,0) AS Jum , IFNULL(Fill.Hrg,0) AS Hrg FROM tdmotortype
		LEFT OUTER JOIN
		(SELECT MotorType, COUNT(tmotor.MotorNoMesin) AS Jum, (SUM(tmotor.FBHarga)-SUM(tmotor.SSHarga)) AS Hrg FROM tmotor
		INNER JOIN ttfb ON tmotor.FBNo = ttfb.FbNo WHERE ttfb.FbTgl BETWEEN TanggalAwal AND TanggalAkhir GROUP BY tmotor.MotorType ) Fill
		ON tdmotortype.MotorType = Fill.MotorType
		ORDER BY  tdmotortype.MotorType;
		CREATE INDEX MotorType ON StockFB(MotorType);

		DROP TEMPORARY TABLE IF EXISTS StockPD;
		CREATE TEMPORARY TABLE IF NOT EXISTS StockPD AS
		SELECT tdmotortype.MotorType, IFNULL(Fill.Jum,0) AS Jum , IFNULL(Fill.Hrg,0) AS Hrg FROM tdmotortype
		LEFT OUTER JOIN
		(SELECT MotorType, COUNT(tmotor.MotorNoMesin) AS Jum, SUM(tmotor.SSHarga) AS Hrg FROM tmotor
		INNER JOIN ttPD ON tmotor.PDNo = ttPD.PDNo WHERE ttPD.PDTgl BETWEEN TanggalAwal AND TanggalAkhir GROUP BY tmotor.MotorType ) Fill
		ON tdmotortype.MotorType = Fill.MotorType
		ORDER BY  tdmotortype.MotorType;
		CREATE INDEX MotorType ON StockPD(MotorType);

		DROP TEMPORARY TABLE IF EXISTS StockRK;
		CREATE TEMPORARY TABLE IF NOT EXISTS StockRK AS
		SELECT tdmotortype.MotorType, IFNULL(Fill.Jum,0) AS Jum , IFNULL(Fill.Hrg,0) AS Hrg FROM tdmotortype
		LEFT OUTER JOIN
		(SELECT MotorType, COUNT(tmotor.MotorNoMesin) AS Jum, SUM(tmotor.RKHarga) AS Hrg FROM tmotor
		INNER JOIN ttRK ON tmotor.RKNo = ttRK.RKNo WHERE ttRK.RKTgl BETWEEN TanggalAwal AND TanggalAkhir GROUP BY tmotor.MotorType ) Fill
		ON tdmotortype.MotorType = Fill.MotorType
		ORDER BY  tdmotortype.MotorType;
		CREATE INDEX MotorType ON StockRK(MotorType);

		DROP TEMPORARY TABLE IF EXISTS StockSK;
		CREATE TEMPORARY TABLE IF NOT EXISTS StockSK AS
		SELECT tdmotortype.MotorType, IFNULL(Fill.Jum,0) AS Jum , IFNULL(Fill.Hrg,0) AS Hrg FROM tdmotortype
		LEFT OUTER JOIN
		(SELECT MotorType, COUNT(tmotor.MotorNoMesin) AS Jum, SUM(tmotor.SKHarga) AS Hrg
		FROM (SELECT MotorType, tmotor.MotorNoMesin, tmotor.SKHarga FROM tmotor
		INNER JOIN ttSK ON tmotor.SKNo = ttSK.SKNo
		WHERE ttSK.SKTgl BETWEEN TanggalAwal AND TanggalAkhir
		UNION ALL
		SELECT tmotor.MotorType, tmotor.MotorNoMesin, tmotor.SKHarga FROM tmotor
		INNER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo
		INNER JOIN ttsk ON ttrk.SKNo = ttsk.SKNo
		WHERE ttSK.SKTgl BETWEEN TanggalAwal AND TanggalAkhir
		AND (tmotor.SSNo <> '--' OR tmotor.FBNo <> '--' OR tmotor.PDNo <>'--')
		)tmotor
		GROUP BY tmotor.MotorType ) Fill
		ON tdmotortype.MotorType = Fill.MotorType
		ORDER BY  tdmotortype.MotorType;
		CREATE INDEX MotorType ON StockSK(MotorType);

		DROP TEMPORARY TABLE IF EXISTS StockME;
		CREATE TEMPORARY TABLE IF NOT EXISTS StockME AS
		SELECT tdmotortype.MotorType, IFNULL(Fill.Jum,0) AS Jum , IFNULL(Fill.Hrg,0) AS Hrg FROM tdmotortype
		LEFT OUTER JOIN
		(SELECT MotorType, COUNT(tmotor.MotorNoMesin) AS Jum, SUM(tmotor.SDHarga) AS Hrg FROM tmotor
		INNER JOIN ttSD ON tmotor.SDNo = ttSD.SDNo WHERE LEFT(ttSD.SDNo,2) = 'ME' AND ttSD.SDTgl BETWEEN TanggalAwal AND TanggalAkhir GROUP BY tmotor.MotorType ) Fill
		ON tdmotortype.MotorType = Fill.MotorType
		ORDER BY  tdmotortype.MotorType;
		CREATE INDEX MotorType ON StockME(MotorType);

		DROP TEMPORARY TABLE IF EXISTS StockSR;
		CREATE TEMPORARY TABLE IF NOT EXISTS StockSR AS
		SELECT tdmotortype.MotorType, IFNULL(Fill.Jum,0) AS Jum , IFNULL(Fill.Hrg,0) AS Hrg FROM tdmotortype
		LEFT OUTER JOIN
		(SELECT MotorType, COUNT(tmotor.MotorNoMesin) AS Jum, SUM(tmotor.SDHarga) AS Hrg FROM tmotor
		INNER JOIN ttSD ON tmotor.SDNo = ttSD.SDNo WHERE LEFT(ttSD.SDNo,2) = 'SR' AND ttSD.SDTgl BETWEEN TanggalAwal AND TanggalAkhir GROUP BY tmotor.MotorType ) Fill
		ON tdmotortype.MotorType = Fill.MotorType
		ORDER BY  tdmotortype.MotorType;
		CREATE INDEX MotorType ON StockSR(MotorType);

		DROP TEMPORARY TABLE IF EXISTS StockMM;
		CREATE TEMPORARY TABLE IF NOT EXISTS StockMM AS
		SELECT tdmotortype.MotorType, 0 AS Jum , IFNULL(Fill.Hrg,0) AS Hrg FROM tdmotortype
		LEFT OUTER JOIN
		(SELECT MotorType, SUM(tmotor.MMHarga) AS Hrg FROM ttamhd
		INNER JOIN ttamit ON ttamhd.AMNo = ttamit.AMNo
		INNER JOIN tmotor ON ttamit.MotorNoMesin = tmotor.MotorNoMesin  AND ttamit.MotorAutoN = tmotor.MotorAutoN
		WHERE ttamhd.amtgl BETWEEN TanggalAwal AND TanggalAkhir GROUP BY tmotor.MotorType ) Fill
		ON tdmotortype.MotorType = Fill.MotorType
		ORDER BY  tdmotortype.MotorType;
		CREATE INDEX MotorType ON StockMM(MotorType);

		UPDATE StockAcct INNER JOIN StockSS ON StockAcct.MotorType = StockSS.MotorType SET SSJum = Jum, SSHrg = Hrg;
		UPDATE StockAcct INNER JOIN StockFB ON StockAcct.MotorType = StockFB.MotorType SET FBJum = Jum, FBHrg = Hrg;
		UPDATE StockAcct INNER JOIN StockPD ON StockAcct.MotorType = StockPD.MotorType SET PDJum = Jum, PDHrg = Hrg;
		UPDATE StockAcct INNER JOIN StockRK ON StockAcct.MotorType = StockRK.MotorType SET RKJum = Jum, RKHrg = Hrg;
		UPDATE StockAcct INNER JOIN StockSK ON StockAcct.MotorType = StockSK.MotorType SET SKJum = Jum, SKHrg = Hrg;
		UPDATE StockAcct INNER JOIN StockME ON StockAcct.MotorType = StockME.MotorType SET MEJum = Jum, MEHrg = Hrg;
		UPDATE StockAcct INNER JOIN StockSR ON StockAcct.MotorType = StockSR.MotorType SET SRJum = Jum, SRHrg = Hrg;
		UPDATE StockAcct INNER JOIN StockMM ON StockAcct.MotorType = StockMM.MotorType SET MMJum = Jum, MMHrg = Hrg;

		UPDATE StockAcct SET SARHrg = SALHrg + SAHrg + FBHrg + PDHrg + RKHrg - SKHrg - MEHrg - SRHrg + MMHrg + SSHrg;
		UPDATE StockAcct SET SARJum = SALJum + SAJum + PDJum + RKJum - SKJum - MEJum - SRJum + SSJum;

		/*Hilangkan Nol*/
		DELETE FROM StockAcct WHERE
		SALJum = 0 AND SARJum = 0 AND SAJum = 0 AND FBJum = 0 AND PDJum = 0 AND RKJum = 0 AND SKJum = 0 AND MEJum = 0 AND SRJum = 0 AND SSJum = 0 AND MMJum = 0 AND
		SALHrg = 0 AND SARHrg = 0 AND SAHrg = 0 AND FBHrg = 0 AND PDHrg = 0 AND RKHrg = 0 AND SKHrg = 0 AND MEHrg = 0 AND SRHrg = 0 AND SSHrg = 0 AND MMHrg = 0 ;

		SET @MyQuery = CONCAT("SELECT * FROM StockAcct;");		
		IF xTipe = "Stok PS 1" THEN
			DROP TEMPORARY TABLE IF EXISTS PersediaanHarian;
			CREATE TEMPORARY TABLE IF NOT EXISTS PersediaanHarian AS
			SELECT MotorType, TypeStatus, MotorNama,
			0000000 AS SALJum, 00000000000000.00  AS SALHrg, 0000000 AS FBJum, 00000000000000.00 AS FBHrg, 
			0000000 AS MRJum, 00000000000000.00 AS MRHrg, 0000000 AS SKJum, 00000000000000.00 AS  SKHrg,
			00000000 AS SARJum, 00000000000000.00 AS SARHrg 
			FROM tdmotortype ORDER BY MotorType;
			CREATE INDEX MotorType ON PersediaanHarian(MotorType);
								
			UPDATE PersediaanHarian INNER JOIN StockAcct ON StockAcct.MotorType = PersediaanHarian.MotorType SET PersediaanHarian.SALJum = StockAcct.SALJum;
         UPDATE PersediaanHarian INNER JOIN StockAcct ON StockAcct.MotorType = PersediaanHarian.MotorType SET PersediaanHarian.SALHrg = StockAcct.SALHrg;
         UPDATE PersediaanHarian INNER JOIN StockAcct ON StockAcct.MotorType = PersediaanHarian.MotorType SET PersediaanHarian.FBJum = StockAcct.SSJum;
         UPDATE PersediaanHarian INNER JOIN StockAcct ON StockAcct.MotorType = PersediaanHarian.MotorType SET PersediaanHarian.FBHrg = StockAcct.SSHrg + StockAcct.FBHrg;
         UPDATE PersediaanHarian INNER JOIN StockAcct ON StockAcct.MotorType = PersediaanHarian.MotorType SET PersediaanHarian.MRJum = StockAcct.PDJum + StockAcct.RKJum - StockAcct.MEJum - StockAcct.SRJum;
         UPDATE PersediaanHarian INNER JOIN StockAcct ON StockAcct.MotorType = PersediaanHarian.MotorType SET PersediaanHarian.MRHrg = StockAcct.PDHrg + StockAcct.RKHrg - StockAcct.MEHrg - StockAcct.SRHrg;
         UPDATE PersediaanHarian INNER JOIN StockAcct ON StockAcct.MotorType = PersediaanHarian.MotorType SET PersediaanHarian.SKJum = StockAcct.SKJum;
         UPDATE PersediaanHarian INNER JOIN StockAcct ON StockAcct.MotorType = PersediaanHarian.MotorType SET PersediaanHarian.SKHrg = StockAcct.SKHrg + StockAcct.MMHrg;
         UPDATE PersediaanHarian INNER JOIN StockAcct ON StockAcct.MotorType = PersediaanHarian.MotorType SET PersediaanHarian.SARJum = StockAcct.SARJum;
         UPDATE PersediaanHarian INNER JOIN StockAcct ON StockAcct.MotorType = PersediaanHarian.MotorType SET PersediaanHarian.SARHrg = StockAcct.SARHrg;
         
         		/*Hilangkan Nol*/
			DELETE FROM PersediaanHarian WHERE
			SALJum = 0 AND SARJum = 0 AND FBJum = 0 AND SKJum = 0 AND MRJum = 0 AND
			SALHrg = 0 AND SARHrg = 0 AND FBHrg = 0 AND SKHrg = 0 AND MRHrg = 0 ;

			SET @MyQuery = CONCAT("SELECT * FROM PersediaanHarian;");		
		END IF;

 	END IF;
	
	IF xTipe = "Detail Motor" THEN
		SET @MyQuery = CONCAT(" SELECT   tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorAutoN, tmotor.MotorNoRangka, LokasiMotor.MotorNoMesin,         
         LokasiMotor.LokasiKode, LokasiMotor.Kondisi AS STATUS, '--' AS KodeTrans,
         IF(tmotor.RKNo = '--',
         IF(ttfb.FBTgl <= 'MyTanggalku' , Tmotor.SSHarga + (Tmotor.FBHarga-Tmotor.SSHarga),  Tmotor.SSHarga),
         IF(ttfb.FBTgl <= 'MyTanggalku' , Tmotor.RKHarga + (Tmotor.FBHarga-Tmotor.RKHarga),  Tmotor.RKHarga)) AS Harga,
         IF(NoTrans=ttfb.FBNo, ttfb.NoGL,
         IF(NoTrans=ttss.SSNo, ttss.NoGL,
         IF(NoTrans=ttpd.PDNo, ttpd.NoGL,
         IF(NoTrans=ttrk.RKNo, ttrk.NoGL,
         IF(ttfb.NoGL<>'--',ttfb.NoGL,        
         IF(ttss.NoGL<>'--',ttss.NoGL, 
         IF(ttpd.NoGL<>'--',ttpd.NoGL, 
         IF(ttrk.NoGL<>'--',ttrk.NoGL, 
         '--')))))))) AS NoGL,
         IF(NoTrans=ttfb.FBNo, ttfb.FBNo,
         IF(NoTrans=ttss.SSNo, ttss.SSNo,
         IF(NoTrans=ttpd.PDNo, ttpd.PDNo,
         IF(NoTrans=ttrk.RKNo, ttrk.RKNo,
         IF(ttfb.FBNo<>'--',ttfb.FBNo,        
         IF(ttss.SSNo<>'--',ttss.SSNo, 
         IF(ttpd.PDNo<>'--',ttpd.PDNo, 
         IF(ttrk.RKNo<>'--',ttrk.RKNo, 
         '--')))))))) AS NoTrans,
         IF(NoTrans=ttfb.FBNo, ttfb.FBTgl,
         IF(NoTrans=ttss.SSNo, ttss.SSTgl,
         IF(NoTrans=ttpd.PDNo, ttpd.PDTgl,
         IF(NoTrans=ttrk.RKNo, ttrk.RKTgl,
         IF(ttfb.FBNo<>'--',ttfb.FBTgl,        
         IF(ttss.SSNo<>'--',ttss.SSTgl, 
         IF(ttpd.PDNo<>'--',ttpd.PDTgl, 
         IF(ttrk.RKNo<>'--',ttrk.RKTgl, 
         Jam)))))))) AS TglTrans
         FROM
         (SELECT 
            tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi 
            FROM
            tvmotorlokasi 
            INNER JOIN 
         (SELECT 
            tvmotorlokasi_1.MotorAutoN, tvmotorlokasi_1.MotorNoMesin, MAX(tvmotorlokasi_1.Jam) AS Jam 
            FROM
            tvmotorlokasi tvmotorlokasi_1 
            WHERE tvmotorlokasi_1.Jam <= 'MyTanggalku 24:00:00' 
            GROUP BY tvmotorlokasi_1.MotorNoMesin, tvmotorlokasi_1.MotorAutoN) B 
            ON tvmotorlokasi.MotorNoMesin = B.MotorNoMesin 
            AND tvmotorlokasi.Jam = B.Jam 
            AND tvmotorlokasi.MotorAutoN = B.MotorAutoN 
         WHERE tvmotorlokasi.Kondisi IN ('IN', 'OUT Pos')) LokasiMotor 
         INNER JOIN tmotor ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin  AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN 
			LEFT OUTER JOIN ttss ON tmotor.SSNo = ttss.SSNo         
         LEFT OUTER JOIN ttPD ON tmotor.PDNo = ttPD.PDNo
         LEFT OUTER JOIN ttRK ON tmotor.RKNo = ttRK.RKNo 
         LEFT OUTER JOIN ttfb ON tmotor.FBNo = ttfb.FBNo
         ORDER BY MotorType, Jam, Notrans;");		
			SET @MyQuery = REPLACE(@MyQuery, "MyTanggalku", xTgl2);
 	END IF;
	
	IF xTipe = "Type Motor" THEN
		SET @MyQuery = CONCAT("         SELECT   tmotor.MotorType, MotorNama, MotorKategori, MotorCC, COUNT(tmotor.MotorNoRangka) AS SARJum,
         SUM(IF(tmotor.RKNo = '--',
         IF(ttfb.FBTgl <= 'MyTanggalku' , Tmotor.SSHarga + (Tmotor.FBHarga-Tmotor.SSHarga),  Tmotor.SSHarga),
         IF(ttfb.FBTgl <= 'MyTanggalku' , Tmotor.RKHarga + (Tmotor.FBHarga-Tmotor.RKHarga),  Tmotor.RKHarga))) AS SARHrg
         FROM
         (SELECT 
            tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi 
            FROM
            tvmotorlokasi 
            INNER JOIN 
         (SELECT 
            tvmotorlokasi_1.MotorAutoN, tvmotorlokasi_1.MotorNoMesin, MAX(tvmotorlokasi_1.Jam) AS Jam 
            FROM
            tvmotorlokasi tvmotorlokasi_1 
            WHERE tvmotorlokasi_1.Jam <= 'MyTanggalku 24:00:00' 
            GROUP BY tvmotorlokasi_1.MotorNoMesin, tvmotorlokasi_1.MotorAutoN) B 
            ON tvmotorlokasi.MotorNoMesin = B.MotorNoMesin 
            AND tvmotorlokasi.Jam = B.Jam 
            AND tvmotorlokasi.MotorAutoN = B.MotorAutoN 
         WHERE tvmotorlokasi.Kondisi IN ('IN', 'OUT Pos')) LokasiMotor 
         INNER JOIN tmotor ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin  AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN 
         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
			LEFT OUTER JOIN ttss ON tmotor.SSNo = ttss.SSNo         
         LEFT OUTER JOIN ttPD ON tmotor.PDNo = ttPD.PDNo
         LEFT OUTER JOIN ttRK ON tmotor.RKNo = ttRK.RKNo 
         LEFT OUTER JOIN ttfb ON tmotor.FBNo = ttfb.FBNo
         GROUP BY MotorType
         ORDER BY MotorType, Jam, Notrans;");
			SET @MyQuery = REPLACE(@MyQuery, "MyTanggalku", xTgl2);		
 	END IF;
	
	IF xTipe = "Track HPP" THEN
		SET @MyQuery = CONCAT("SELECT tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.DKNo, tmotor.MMHarga, ttfb.FBTgl,tmotor.SSHarga, 
		   IF(tmotor.SSNo<>'--',tmotor.SSNo, tmotor.PDNo) AS PDNo, 
		   tmotor.FBNo, IF(tmotor.FBNo<>'--',tmotor.FBHarga,IF(tmotor.PDNo<>'--',tmotor.SSHarga,0)) As FBHarga, 
         tmotor.SKNo, IF(tmotor.SKNo<>'--',tmotor.SKHarga,0) AS SKHarga, 
         tmotor.SDNo, IF(tmotor.SDNo<>'--',tmotor.SDHarga,0) AS SDHarga,
         tmotor.RKNo, IF(tmotor.RKNo<>'--',tmotor.RKHarga,0) AS RKHarga,
         IF(IF(tmotor.FBNo<>'--',tmotor.FBHarga,IF(tmotor.PDNo<>'--',tmotor.SSHarga,0)) - SKHarga <> 0 AND tmotor.SKNo<>'--' AND tmotor.RKNo = '--', FBHarga - SKHarga +MMHarga , 
         IF(IF(tmotor.FBNo<>'--',tmotor.FBHarga,IF(tmotor.PDNo<>'--',tmotor.SSHarga,0)) - SDHarga <> 0 AND tmotor.SDNo<>'--' AND tmotor.RKNo = '--', FBHarga - SDHarga +MMHarga ,0)) AS Tanda1, 
         IFNULL(ttss.SSTgl, ttpd.PDTgl) AS Tanggal
         FROM tmotor 
         LEFT OUTER JOIN ttfb ON ttfb.FBNo = tmotor.FBNo 
         LEFT OUTER JOIN ttpd ON ttpd.PDNo = tmotor.PDNo
         LEFT OUTER JOIN ttss ON ttss.SSNo = tmotor.SSNo
         LEFT OUTER JOIN ttrk ON ttrk.RKNo = tmotor.RKNo
			Where  
		   ttss.SSTgl BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "' OR  
         ttfb.FBTgl BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "' OR  
         ttpd.PDTgl BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "' OR  
         (ttrk.RKTgl BETWEEN '" ,xTgl1 , "' AND '" , xTgl2 , "' AND Tmotor.PDNo ='--' AND Tmotor.FBNo ='--') 
         Order by Tanda1, tmotor.MotorType, tmotor.MotorWarna,tmotor.MotorNoMesin, tmotor.MotorAutoN, tmotor.FBNo, tmotor.PDNo;");		
 	END IF;

	IF xTipe = "Stock Intransit" THEN
		SET @MyQuery = CONCAT("         SELECT MotorAutoN, MotorNoMesin, MotorNoRangka, MotorType, MotorWarna, MotorTahun, 
         ttss.SSNo, ttss.SSTgl, SSHarga, ttFB.FBNo, ttfb.fbtgl, FBHarga,
         IF(ttfb.FBTgl<= 'MyTanggal',IFNULL(FBHArga,0),0)- IF(ttss.SSTgl<= 'MyTanggal',IFNULL(SSHArga,0),0) AS SaldoStockIntransit
         FROM tmotor
         LEFT OUTER JOIN ttss ON tmotor.ssno = ttss.SSNo
         LEFT OUTER JOIN ttfb ON tmotor.FBno = ttFB.FBNo
         WHERE ((ttss.SSTgl <= 'MyTanggal') OR (ttFB.FBTgl <= 'MyTanggal'))
         AND (IF(ttfb.FBTgl<= 'MyTanggal',FBHArga,0)- IF(ttss.SSTgl<= 'MyTanggal',SSHArga,0) < -10000000 OR IF(ttfb.FBTgl<= 'MyTanggal',FBHArga,0)- IF(ttss.SSTgl<= 'MyTanggal',SSHArga,0) > 10000000)
			AND (IF(ttfb.FBTgl <= 'MyTanggal', FBHArga, 0) = 0 OR IF(ttss.SSTgl<= 'MyTanggal',SSHArga,0) = 0);");		
		SET @MyQuery = REPLACE(@MyQuery, "MyTanggal", xTgl2);		
 	END IF;

	IF xTipe = "Stok PS 2" THEN
		CALL rGLStockHitung(xTgl1,xTgl2);
		SET @MyQuery = CONCAT("SELECT * FROM StockHarian;");		
 	END IF;

	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$

DELIMITER ;


