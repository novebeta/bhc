DELIMITER $$
DROP PROCEDURE IF EXISTS `rUserLog`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rUserLog`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,IN xUserID VARCHAR(20),IN xNmTrans VARCHAR(30),IN xJenis VARCHAR(20),IN xNoTrans VARCHAR(20),IN xSort VARCHAR(20),IN xOrder VARCHAR(10))
BEGIN
          /*
            Cara Akses SP :
            CALL rUserLog(xTipe,xTgl1,xTgl2,xUserID,xNmTrans,xJenis,xNoTrans,xSort,xOrder)
            xTipe : UserLog,UserLogDB,Login
           */
           SET xUserID = CONCAT('%',xUserID,'%');
           SET xJenis = CONCAT('%',xJenis,'%');
           SET xNmTrans = CONCAT('%',xNmTrans,'%');
           SET xNoTrans = CONCAT('%',xNoTrans,'%');
           
           IF xTipe = 'tuserlog' THEN
              IF xOrder = 'ASC' THEN
                 SELECT LogTglJam, UserID, LogJenis, LogNama, LogTabel, NoTrans FROM tuserlog
                 WHERE
                   (LogTglJam BETWEEN xTgl1 AND xTgl2) AND
                   (LogJenis LIKE xJenis) AND
                   (LogTabel LIKE xNmTrans) AND
                   (UserID LIKE xUserID) AND
                   (NoTrans LIKE xNoTrans)
                 ORDER BY xSort ASC;
              ELSE
                 SELECT LogTglJam, UserID, LogJenis, LogNama, LogTabel, NoTrans FROM tuserlog
                 WHERE
                   (LogTglJam BETWEEN xTgl1 AND xTgl2) AND
                   (LogJenis LIKE xJenis) AND
                   (LogTabel LIKE xNmTrans) AND
                   (UserID LIKE xUserID) AND
                   (NoTrans LIKE xNoTrans)
                 ORDER BY xSort DESC;
              END IF;
           END IF;

           IF xTipe = 'tuserlogdb' THEN
              IF xOrder = 'ASC' THEN 
                 SELECT NoTrans, LogTglJam, LogTabel, LogJenis, LogTrans, LogGL, LogMemo, LogNominal, UserID FROM tuserlogdb 
                 WHERE
                   (LogTglJam BETWEEN xTgl1 AND xTgl2) AND
                   (LogJenis LIKE xJenis) AND
                   (LogTabel LIKE xNmTrans) AND
                   (UserID LIKE xUserID) AND
                   (NoTrans LIKE xNoTrans)
                 ORDER BY xSort ASC;
              ELSE
                 SELECT NoTrans, LogTglJam, LogTabel, LogJenis, LogTrans, LogGL, LogMemo, LogNominal, UserID FROM tuserlogdb 
                 WHERE
                   (LogTglJam BETWEEN xTgl1 AND xTgl2) AND
                   (LogJenis LIKE xJenis) AND
                   (LogTabel LIKE xNmTrans) AND
                   (UserID LIKE xUserID) AND
                   (NoTrans LIKE xNoTrans)
                 ORDER BY xSort DESC;
              END IF;
           END IF;

           IF xTipe = 'tlogin' THEN
              SELECT A.UserID, A.LogNama, A.LogTglJam AS Login, IFNULL(B.LogTglJam,A.LogTglJam) AS Logout, TIMEDIFF(IFNULL(B.LogTglJam,A.LogTglJam),A.LogTglJam) AS Durasi  FROM 
                (SELECT MIN(LogTglJam) AS LogTglJam, UserID, LogJenis, LogNama, LogTabel, NoTrans
              FROM HMDS.tuserlog
              WHERE (LogJenis = 'Login')
              GROUP BY UserID, DATE(LogTglJam)) A
              LEFT OUTER JOIN
              (SELECT MAX(LogTglJam) AS LogTglJam, UserID, LogJenis, LogNama, LogTabel, NoTrans
              FROM HMDS.tuserlog
              WHERE (LogJenis = 'Logout')
              GROUP BY UserID, DATE(LogTglJam)) B
              ON DATE(A.LogTglJam) = DATE(B.LogTglJam) AND A.UserID = B.UserID 
              WHERE (DATE(A.LogTglJam) BETWEEN xTgl1 AND xTgl2) AND A.UserID LIKE xUserID 
              UNION
              SELECT A.UserID, A.LogNama, A.LogTglJam AS Login, IFNULL(B.LogTglJam,A.LogTglJam) AS Logout, TIMEDIFF(IFNULL(B.LogTglJam,A.LogTglJam),A.LogTglJam) AS Durasi  FROM 
                (SELECT MIN(LogTglJam) AS LogTglJam, UserID, LogJenis, LogNama, LogTabel, NoTrans
              FROM HMDS_LOG.tuserlog
              WHERE (LogJenis = 'Login')
              GROUP BY UserID, DATE(LogTglJam)) A
              LEFT OUTER JOIN
              (SELECT MAX(LogTglJam) AS LogTglJam, UserID, LogJenis, LogNama, LogTabel, NoTrans
              FROM HMDS_LOG.tuserlog
              WHERE (LogJenis = 'Logout')
              GROUP BY UserID, DATE(LogTglJam)) B
              ON DATE(A.LogTglJam) = DATE(B.LogTglJam) AND A.UserID = B.UserID 
              WHERE (DATE(A.LogTglJam) BETWEEN xTgl1 AND xTgl2) AND A.UserID LIKE xUserId
              ORDER BY UserID, Login;
           END IF;

	END$$

DELIMITER ;

