DELIMITER $$
DROP PROCEDURE IF EXISTS `rGLPiutang`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rGLPiutang`(IN xTipe VARCHAR(50),IN xTgl1 DATE,IN xTgl2 DATE,
IN xPerkiraan1 VARCHAR(10),IN xPerkiraan2 VARCHAR(10), IN xStatus VARCHAR(5), IN xSensitif VARCHAR(1), IN xNoBukti VARCHAR(30), IN xLeaseKode VARCHAR(30))
BEGIN
/*
	CALL rGLPiutang('Saldo Piutang Leasing 1','2020-01-01','2020-02-29','%','%','Belum','0','','');
*/
	DECLARE AcctLease VARCHAR(500);
	IF xTipe = "Saldo Piutang Leasing 1" OR xTipe = "Saldo Piutang Leasing 2" THEN
		IF xSensitif = "1" THEN
			DELETE FROM tvpiutangdebet ;
			INSERT INTO tvpiutangdebet  SELECT * FROM vpiutangdebet ;
		END IF;
		SELECT GROUP_CONCAT("'",NoAccount,"'") INTO AcctLease FROM traccount WHERE NoParent = '11080000';

		DROP TEMPORARY TABLE IF EXISTS tvpiutangbayar0 ;
		CREATE TEMPORARY TABLE IF NOT EXISTS tvpiutangbayar0 AS
		SELECT GLLink AS HPLink, NoAccount, SUM(HPNilai) AS SumKreditGL FROM ttglhpit
		WHERE (NOAccount BETWEEN '11060000' AND '11150000') AND HPJenis = 'Piutang' AND TglGL <= xTgl2
		GROUP BY HPLink, NoAccount ;
		CREATE INDEX HPLink ON tvpiutangbayar0(HPLink);
		CREATE INDEX NoAccount ON tvpiutangbayar0(NoAccount);

		CASE xStatus
			WHEN "Belum" THEN
				SET @LunasScript = " tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) <> 0 AND ";
			WHEN "Lunas" THEN
				SET @LunasScript = " tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) = 0 AND ";
			ELSE
				SET @LunasScript = " ";
		END CASE;

		DROP TEMPORARY TABLE IF EXISTS tPiutangLease;
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS tPiutangLease AS
			SELECT GLLink, NoGL, TglGL, NoAccount, NamaAccount, KeteranganGL, DebetGL, PiutangLunas, PiutangSisa, SalesNama, LeaseKode, MotorNoMesin, MotorNama ,
         IFNULL(SKNo,'--') AS SKNo, IFNULL(SKTgl,Piutang.TglGL) AS SKTgl, IFNULL(DKNo,'--') AS DKNo, IFNULL(DKTgl,Piutang.TglGL) AS DKTgl, IFNULL(DKTop,7) AS DKTop,
         IFNULL(TglTempo,DATE_ADD(Piutang.TglGL, INTERVAL 7 DAY)) AS TglTempo, IFNULL(MotorType,'--') AS MotorType, IFNULL(CusNama,'--') AS CusNama   FROM
          (SELECT tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount,
           tvpiutangdebet.NamaAccount, tvpiutangdebet.KeteranganGL, tvpiutangdebet.DebetGL,
           IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangLunas, tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangSisa,
           ttsk.SKNo, ttsk.SKTgl, ttdk.DKNo, ttdk.DKTgl, ttdk.DKTOP, DATE_ADD(ttsk.SKTgl, INTERVAL ttdk.DKTOP DAY) AS TglTempo, tmotor.MotorType,
           IF(TRIM(tdcustomer.CusNama)<>TRIM(tdcustomer.CusPembeliNama) AND tdcustomer.CusPembeliNama <>'--' AND tdcustomer.CusPembeliNama <>'',CONCAT_WS(' qq ',tdcustomer.CusNama,tdcustomer.CusPembeliNama),tdcustomer.CusNama) AS CusNama,
           tdsales.SalesNama, tmotor.MotorNoMesin,MotorNama, LeaseKode
           FROM tvpiutangdebet
           LEFT OUTER JOIN tvpiutangbayar0 
           ON tvpiutangdebet.GLLink = tvpiutangbayar0.HPLink AND tvpiutangdebet.NoAccount = tvpiutangbayar0.NoAccount
           LEFT OUTER JOIN ttsk ON ttsk.SKNo = tvpiutangdebet.GLLink
           LEFT OUTER JOIN tmotor ON ttsk.SKNo = tmotor.SKNo  
           LEFT OUTER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
           LEFT OUTER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
           LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode
           LEFT OUTER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
           WHERE ",@LunasScript,"  (tvpiutangdebet.NoAccount  IN (",AcctLease,") ) ) Piutang ");
			    SET @MyQuery = CONCAT(@MyQuery,"WHERE (NoAccount LIKE '%",xPerkiraan1,"') ");
         SET @MyQuery = CONCAT(@MyQuery,"AND IFNULL(LeaseKode,'') Like '%" , xLeaseKode , "' ");
         SET @MyQuery = CONCAT(@MyQuery,"AND (TglGL <= '" , xTgl2 , "') " );
         SET @MyQuery = CONCAT(@MyQuery,"ORDER BY NoAccount, SKTgl");
		ELSE
			SET @MyQuery = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS tPiutangLease AS
			SELECT GLLink, NoGL, TglGL, NoAccount, NamaAccount, KeteranganGL, DebetGL, PiutangLunas, PiutangSisa, SalesNama, LeaseKode, MotorNoMesin, MotorNama ,
         IFNULL(SKNo,'--') AS SKNo, IFNULL(SKTgl,Piutang.TglGL) AS SKTgl, IFNULL(DKNo,'--') AS DKNo, IFNULL(DKTgl,Piutang.TglGL) AS DKTgl, IFNULL(DKTop,7) AS DKTop,
         IFNULL(TglTempo,DATE_ADD(Piutang.TglGL, INTERVAL 7 DAY)) AS TglTempo, IFNULL(MotorType,'--') AS MotorType, IFNULL(CusNama,'--') AS CusNama   FROM
          (SELECT tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount,
           tvpiutangdebet.NamaAccount, tvpiutangdebet.KeteranganGL, tvpiutangdebet.DebetGL,
           IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangLunas, tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangSisa,
           ttsk.SKNo, ttsk.SKTgl, ttdk.DKNo, ttdk.DKTgl, ttdk.DKTOP, DATE_ADD(ttsk.SKTgl, INTERVAL ttdk.DKTOP DAY) AS TglTempo, tmotor.MotorType,
           IF(TRIM(tdcustomer.CusNama)<>TRIM(tdcustomer.CusPembeliNama) AND tdcustomer.CusPembeliNama <>'--' AND tdcustomer.CusPembeliNama <>'',CONCAT_WS(' qq ',tdcustomer.CusNama,tdcustomer.CusPembeliNama),tdcustomer.CusNama) AS CusNama,
           tdsales.SalesNama, tmotor.MotorNoMesin,MotorNama, LeaseKode
           FROM tvpiutangdebet
           LEFT OUTER JOIN tvpiutangbayar0 
           ON tvpiutangdebet.GLLink = tvpiutangbayar0.HPLink AND tvpiutangdebet.NoAccount = tvpiutangbayar0.NoAccount
           LEFT OUTER JOIN ttsk ON ttsk.SKNo = tvpiutangdebet.GLLink
           LEFT OUTER JOIN tmotor ON ttsk.SKNo = tmotor.SKNo  
           LEFT OUTER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
           LEFT OUTER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
           LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode
           LEFT OUTER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
           WHERE ",@LunasScript,"  (tvpiutangdebet.NoAccount  IN (",AcctLease,") ) ) Piutang ");
			    SET @MyQuery = CONCAT(@MyQuery,"WHERE (NoAccount BETWEEN '",xPerkiraan1,"' AND '",xPerkiraan2,"') ");
         SET @MyQuery = CONCAT(@MyQuery,"AND IFNULL(LeaseKode,'') Like '%" , xLeaseKode , "' ");
         SET @MyQuery = CONCAT(@MyQuery,"AND (TglGL <= '" , xTgl2 , "') " );
         SET @MyQuery = CONCAT(@MyQuery,"ORDER BY NoAccount, SKTgl");
		END IF;
		
		IF xPerkiraan1 = "11060100"  OR xPerkiraan2 = "11060100" THEN
			DROP TEMPORARY TABLE IF EXISTS tvpiutangbayar0 ;
			CREATE TEMPORARY TABLE IF NOT EXISTS tvpiutangbayar0 AS
			SELECT GLLink AS HPLink, NoAccount, SUM(HPNilai) AS SumKreditGL FROM ttglhpit
			WHERE (NOAccount BETWEEN '11060000' AND '11060100') AND HPJenis = 'Piutang' AND TglGL <= xTgl2
			GROUP BY HPLink, NoAccount ;
			CREATE INDEX HPLink ON tvpiutangbayar0(HPLink);
			CREATE INDEX NoAccount ON tvpiutangbayar0(NoAccount);

			SET @MyQuery = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS tPiutangLease AS
			SELECT GLLink, NoGL, TglGL, NoAccount, NamaAccount, KeteranganGL, DebetGL, PiutangLunas, PiutangSisa, SalesNama, LeaseKode,MotorNoMesin,MotorNama ,
			IFNULL(SKNo,'--') AS SKNo, IFNULL(SKTgl,Piutang.TglGL) AS SKTgl, IFNULL(DKNo,'--') AS DKNo, IFNULL(DKTgl,Piutang.TglGL) AS DKTgl, IFNULL(DKTop,7) AS DKTop, IFNULL(TglTempo,DATE_ADD(Piutang.TglGL, INTERVAL 7 DAY)) AS TglTempo, IFNULL(MotorType,'--') AS MotorType, IFNULL(CusNama,'--') AS CusNama FROM
			(SELECT tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount,
			tvpiutangdebet.NamaAccount, tvpiutangdebet.KeteranganGL, tvpiutangdebet.DebetGL,
			IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangLunas, tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangSisa,
			ttsk.SKNo, ttsk.SKTgl, ttdk.DKNo, ttdk.DKTgl, ttdk.DKTOP, DATE_ADD(ttsk.SKTgl, INTERVAL ttdk.DKTOP DAY) AS TglTempo, tmotor.MotorType,
			IF(TRIM(tdcustomer.CusNama)<>TRIM(tdcustomer.CusPembeliNama) AND tdcustomer.CusPembeliNama <>'--' AND tdcustomer.CusPembeliNama <>'',CONCAT_WS(' qq ',tdcustomer.CusNama,tdcustomer.CusPembeliNama),tdcustomer.CusNama) AS CusNama, tdsales.SalesNama, tmotor.MotorNoMesin,MotorNama,LeaseKode
			FROM tvpiutangdebet
			LEFT OUTER JOIN tvpiutangbayar0
			ON tvpiutangdebet.GLLink = tvpiutangbayar0.HPLink AND tvpiutangdebet.NoAccount = tvpiutangbayar0.NoAccount
			LEFT OUTER JOIN ttsk ON ttsk.SKNo = tvpiutangdebet.GLLink
			LEFT OUTER JOIN  tmotor ON ttsk.SKNo = tmotor.SKNo
			LEFT OUTER JOIN  ttdk ON tmotor.DKNo = ttdk.DKNo
			LEFT OUTER JOIN  tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
			LEFT OUTER JOIN  tdsales ON ttdk.SalesKode = tdsales.SalesKode
			LEFT OUTER JOIN  tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
			WHERE " , @LunasScript , "(tvpiutangdebet.NoAccount  = '11060100')) Piutang
			WHERE NoAccount = '11060100' AND (TglGL <= '" , xTgl2 , "' ) AND IFNULL(LeaseKode,'') Like '%" , xLeaseKode , "' ORDER BY NoAccount, SKTgl");
		END IF;

		IF xPerkiraan1 = "11110200"  OR xPerkiraan2 = "11110200" THEN
			DROP TEMPORARY TABLE IF EXISTS tvpiutangbayar0 ;
			CREATE TEMPORARY TABLE IF NOT EXISTS tvpiutangbayar0 AS
			SELECT GLLink AS HPLink, NoAccount, SUM(HPNilai) AS SumKreditGL FROM ttglhpit
			WHERE (NOAccount BETWEEN '11110200' AND '11110200') AND HPJenis = 'Piutang' AND TglGL <= xTgl2
			GROUP BY HPLink, NoAccount ;
			CREATE INDEX HPLink ON tvpiutangbayar0(HPLink);
			CREATE INDEX NoAccount ON tvpiutangbayar0(NoAccount);

			SET @MyQuery = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS tPiutangLease AS
			SELECT GLLink, NoGL, TglGL, NoAccount, NamaAccount, KeteranganGL, DebetGL, PiutangLunas, PiutangSisa, SalesNama, LeaseKode,MotorNoMesin,MotorNama ,
			IFNULL(SKNo,'--') AS SKNo, IFNULL(SKTgl,Piutang.TglGL) AS SKTgl, IFNULL(DKNo,'--') AS DKNo, IFNULL(DKTgl,Piutang.TglGL) AS DKTgl, IFNULL(DKTop,7) AS DKTop,
			IFNULL(TglTempo,DATE_ADD(Piutang.TglGL, INTERVAL 7 DAY)) AS TglTempo, IFNULL(MotorType,'--') AS MotorType, IFNULL(CusNama,'--') AS CusNama FROM
				(SELECT tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount,
					tvpiutangdebet.NamaAccount, tvpiutangdebet.KeteranganGL, tvpiutangdebet.DebetGL,
					IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangLunas, tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangSisa,
					ttsk.SKNo, ttsk.SKTgl, ttdk.DKNo, ttdk.DKTgl, ttdk.DKTOP, DATE_ADD(ttsk.SKTgl, INTERVAL ttdk.DKTOP DAY) AS TglTempo, tmotor.MotorType,
					IF(TRIM(tdcustomer.CusNama)<>TRIM(tdcustomer.CusPembeliNama) AND tdcustomer.CusPembeliNama <>'--' AND tdcustomer.CusPembeliNama <>'',CONCAT_WS(' qq ',tdcustomer.CusNama,tdcustomer.CusPembeliNama),tdcustomer.CusNama) AS CusNama,
					tdsales.SalesNama, tmotor.MotorNoMesin,MotorNama,LeaseKode
				 FROM tvpiutangdebet
				LEFT OUTER JOIN tvpiutangbayar0
				ON tvpiutangdebet.GLLink = tvpiutangbayar0.HPLink AND tvpiutangdebet.NoAccount = tvpiutangbayar0.NoAccount
				LEFT OUTER JOIN ttsk ON ttsk.SKNo = tvpiutangdebet.GLLink
				LEFT OUTER JOIN tmotor ON ttsk.SKNo = tmotor.SKNo
				LEFT OUTER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
				LEFT OUTER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
				LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode
				LEFT OUTER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
				WHERE ", @LunasScript , " (tvpiutangdebet.NoAccount  = '11110200' )) Piutang
				WHERE NoAccount = '11110200' AND (TglGL <= '" , xTgl2 , "') AND IFNULL(LeaseKode,'') Like '%" , xLeaseKode , "'  ORDER BY NoAccount, SKTgl");
		END IF;
        
		PREPARE STMT FROM @MyQuery;
		EXECUTE Stmt;
		SET @MyQuery = "SELECT * FROM tPiutangLease;";
	END IF;

	IF xTipe = "Saldo Piutang Non Leasing" THEN
		IF xSensitif = "1" THEN
			DELETE FROM tvpiutangdebet ;
			INSERT INTO tvpiutangdebet  SELECT * FROM vpiutangdebet ;
		END IF;
		SELECT GROUP_CONCAT("'",NoAccount,"'") INTO AcctLease FROM traccount WHERE NoParent = '11080000';

		DROP TEMPORARY TABLE IF EXISTS tvpiutangbayar0 ;
		CREATE TEMPORARY TABLE IF NOT EXISTS tvpiutangbayar0 AS
		SELECT GLLink AS HPLink, NoAccount, SUM(HPNilai) AS SumKreditGL FROM ttglhpit
		WHERE (NOAccount BETWEEN '11060000' AND '11150000') AND HPJenis = 'Piutang' AND TglGL <= xTgl2
		GROUP BY HPLink, NoAccount ;
		CREATE INDEX HPLink ON tvpiutangbayar0(HPLink);
		CREATE INDEX NoAccount ON tvpiutangbayar0(NoAccount);

		CASE xStatus
			WHEN "Belum" THEN
				SET @LunasScript = " tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) <> 0 AND ";
			WHEN "Lunas" THEN
				SET @LunasScript = " tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) = 0 AND ";
			ELSE
				SET @LunasScript = " ";
		END CASE;

		DROP TEMPORARY TABLE IF EXISTS tPiutangNonLease;
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS tPiutangNonLease AS
  			SELECT tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount,
        tvpiutangdebet.NamaAccount, tvpiutangdebet.KeteranganGL, tvpiutangdebet.DebetGL,
        IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangLunas, tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangSisa
        FROM tvpiutangdebet
        LEFT OUTER JOIN tvpiutangbayar0
        ON tvpiutangdebet.GLLink = tvpiutangbayar0.HPLink AND tvpiutangdebet.NoAccount = tvpiutangbayar0.NoAccount
        WHERE (tvpiutangdebet.NoAccount  NOT IN (" , AcctLease , ") ) AND ",@LunasScript,
        " tvpiutangdebet.NoAccount LIKE '%",xPerkiraan1, "' AND (tvpiutangdebet.TglGL <= '" , xTgl2 , "' )
        ORDER BY tvpiutangdebet.NoAccount, tvpiutangdebet.TglGL, tvpiutangdebet.GLLink ");
		ELSE
			SET @MyQuery = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS tPiutangNonLease AS
  			SELECT tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount,
        tvpiutangdebet.NamaAccount, tvpiutangdebet.KeteranganGL, tvpiutangdebet.DebetGL,
        IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangLunas, tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangSisa
        FROM tvpiutangdebet
        LEFT OUTER JOIN tvpiutangbayar0
        ON tvpiutangdebet.GLLink = tvpiutangbayar0.HPLink AND tvpiutangdebet.NoAccount = tvpiutangbayar0.NoAccount
        WHERE (tvpiutangdebet.NoAccount  NOT IN (" , AcctLease , ") ) AND ",@LunasScript,
        " (tvpiutangdebet.NoAccount BETWEEN '", xPerkiraan1 , "' AND  '", xPerkiraan2 , "') AND (tvpiutangdebet.TglGL <= '" , xTgl2 , "' )
        ORDER BY tvpiutangdebet.NoAccount, tvpiutangdebet.TglGL, tvpiutangdebet.GLLink ");
    END IF;

		IF xPerkiraan1 = "11060100"  OR xPerkiraan2 = "11060100" THEN
      SET @MyQuery = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS tPiutangNonLease AS
      SELECT tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount, tvpiutangdebet.NamaAccount,
      REPLACE(CONCAT(tvpiutangdebet.KeteranganGL,' - Team : ', IFNULL(ttdk.TeamKode,''), ' - ',IFNULL(SalesNama,'')),'Post SK - ','') AS KeteranganGL,
      tvpiutangdebet.DebetGL, IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangLunas, tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangSisa
            FROM tvpiutangdebet
            LEFT OUTER JOIN tvpiutangbayar0 ON tvpiutangdebet.GLLink = tvpiutangbayar0.HPLink AND tvpiutangdebet.NoAccount = tvpiutangbayar0.NoAccount
            LEFT OUTER JOIN tmotor ON tmotor.SKno = tvpiutangdebet.GLLink
            LEFT OUTER JOIN ttdk ON tmotor.DKno = ttdk.DKNo
            LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode
            WHERE (tvpiutangdebet.NoAccount NOT IN ('11080100', '11080200', '11080300', '11080400', '11080500', '11080600', '11080700', '11080800', '11080900', '11081000', '11081100', '11081200', '11081300', '11081400', '11089900'))
            AND ", @LunasScript, "
             (tvpiutangdebet.NoAccount BETWEEN '11060100' AND '11060100')
            AND (tvpiutangdebet.TglGL <= '", xTgl2 ,"')
            ORDER BY tvpiutangdebet.NoAccount ");
    END IF;
		PREPARE STMT FROM @MyQuery;
		EXECUTE Stmt;

    SELECT PerusahaanNama INTO @NamaPerusahaan FROM tzcompany;
    SELECT LokasiKode INTO @POSLokasiKode FROM tuser WHERE LOWER(UserID) = 'foen';
    IF @NamaPerusahaan = "CV. ANUGERAH PERDANA" AND @POSLokasiKode = "PlatMerah" THEN
      UPDATE tPiutangNonLease SET KeteranganGL = REPLACE(KeteranganGL, "Tunai - Sisa Piutang Kons -", "");
      UPDATE tPiutangNonLease SET KeteranganGL = REPLACE(KeteranganGL, "- Team : PM", "");
    END IF;

		SET @MyQuery = "SELECT * FROM tPiutangNonLease;";
	END IF;

	IF xTipe = "Mutasi Debet Piutang" THEN
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT("SELECT vglall.* FROM vglall
			WHERE vglall.KodeTrans <> 'NA' AND (vglall.NoAccount BETWEEN '11060000' AND '11150000') AND vglall.DebetGL > 0");
			SET @MyQuery = CONCAT(@MyQuery," AND vglall.TglGL BETWEEN '",xTgl1,"' AND '", xTgl2 , "'");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY   NoAccount , GLLink ");
		ELSE
			SET @MyQuery = CONCAT("SELECT vglall.* FROM vglall
			WHERE vglall.KodeTrans <> 'NA' AND (vglall.NoAccount BETWEEN '11060000' AND '11150000') AND vglall.DebetGL > 0");
			SET @MyQuery = CONCAT(@MyQuery," AND vglall.NoAccount BETWEEN '" , xPerkiraan1 , "' AND '", xPerkiraan2, "' ");
			SET @MyQuery = CONCAT(@MyQuery," AND vglall.TglGL BETWEEN '",xTgl1,"' AND '", xTgl2 , "'");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY   NoAccount , GLLink ");
		END IF;
	END IF;

	IF xTipe = "Mutasi Kredit Piutang" THEN
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT("SELECT vglall.* FROM vglall
			WHERE vglall.KodeTrans <> 'NA' AND (vglall.NoAccount BETWEEN '11060000' AND '11150000') AND vglall.KreditGL > 0 ");
			SET @MyQuery = CONCAT(@MyQuery," AND vglall.TglGL BETWEEN '",xTgl1,"' AND '", xTgl2 , "'");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY   NoAccount , GLLink ");
		ELSE
			SET @MyQuery = CONCAT("SELECT vglall.* FROM vglall
			WHERE vglall.KodeTrans <> 'NA' AND (vglall.NoAccount BETWEEN '11060000' AND '11150000') AND vglall.KreditGL > 0  ");
			SET @MyQuery = CONCAT(@MyQuery," AND vglall.NoAccount BETWEEN '" , xPerkiraan1 , "' AND '", xPerkiraan2, "' ");
			SET @MyQuery = CONCAT(@MyQuery," AND vglall.TglGL BETWEEN '",xTgl1,"' AND '", xTgl2 , "'");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY   NoAccount , GLLink ");
		END IF;
	END IF;

	IF xTipe = "Umur Piutang - Curent" OR xTipe = "Umur Piutang - Overdue" THEN
		IF xSensitif = "1" THEN
			DELETE FROM tvpiutangdebet ;
			INSERT INTO tvpiutangdebet  SELECT * FROM vpiutangdebet ;
		END IF;
		SELECT GROUP_CONCAT("'",NoAccount,"'") INTO AcctLease FROM traccount WHERE NoParent = '11080000';

		DROP TEMPORARY TABLE IF EXISTS tvpiutangbayar0 ;
		CREATE TEMPORARY TABLE IF NOT EXISTS tvpiutangbayar0 AS
		SELECT GLLink AS HPLink, NoAccount, SUM(HPNilai) AS SumKreditGL FROM ttglhpit
		WHERE (NOAccount BETWEEN '11060000' AND '11150000') AND HPJenis = 'Piutang' AND TglGL <= xTgl2
		GROUP BY HPLink, NoAccount ;
		CREATE INDEX HPLink ON tvpiutangbayar0(HPLink);
		CREATE INDEX NoAccount ON tvpiutangbayar0(NoAccount);

		DROP TEMPORARY TABLE IF EXISTS MyClient;
		CREATE TEMPORARY TABLE IF NOT EXISTS MyClient AS
		SELECT ttsk.SKNo, ttsk.SKTgl, ttdk.DKNo, ttdk.DKTgl, ttdk.DKTOP, DATE_ADD(ttsk.SKTgl, INTERVAL ttdk.DKTOP DAY) AS TglTempo, tmotor.MotorType, ttdk.SalesKode,
		IF(TRIM(tdcustomer.CusNama)<>TRIM(tdcustomer.CusPembeliNama) AND tdcustomer.CusPembeliNama <>'--' AND tdcustomer.CusPembeliNama <>'',CONCAT_WS(' qq ',tdcustomer.CusNama,tdcustomer.CusPembeliNama),tdcustomer.CusNama) AS CusNama
		FROM ttsk
		INNER JOIN tmotor ON ttsk.SKNo = tmotor.SKNo
		INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
		INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode;
		CREATE INDEX SKNo ON MyClient(SKNo);
		CREATE INDEX SalesKode ON MyClient(SalesKode);

		IF xPerkiraan1 = "11060100"  OR xPerkiraan2 = "11060100" THEN
			DROP TEMPORARY TABLE IF EXISTS Piutang;
			SET @MyQuery = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS Piutang AS
			SELECT tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount, tvpiutangdebet.NamaAccount, tvpiutangdebet.KeteranganGL, tvpiutangdebet.DebetGL, IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangLunas, tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangSisa,
			0 AS OD, 0 AS CR, 0 AS R1, 0 AS R2, 0 AS R3, 0 AS R4
			FROM tvpiutangdebet
			LEFT OUTER JOIN tvpiutangbayar0 ON tvpiutangdebet.GLLink = tvpiutangbayar0.HPLink AND tvpiutangdebet.NoAccount = tvpiutangbayar0.NoAccount
			WHERE (tvpiutangdebet.NoAccount = '11060100')
			AND tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) > 0
			AND tvpiutangdebet.KodeTrans = 'SK';");	
			PREPARE STMT FROM @MyQuery;
			EXECUTE Stmt;
			CREATE INDEX GLLink ON Piutang(GLLink);			
		ELSE
			DROP TEMPORARY TABLE IF EXISTS Piutang;
			SET @MyQuery = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS Piutang AS
			SELECT tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount, tvpiutangdebet.NamaAccount, tvpiutangdebet.KeteranganGL, tvpiutangdebet.DebetGL, IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangLunas, tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) AS PiutangSisa,
			0 AS OD, 0 AS CR, 0 AS R1, 0 AS R2, 0 AS R3, 0 AS R4
			FROM tvpiutangdebet
			LEFT OUTER JOIN tvpiutangbayar0 ON tvpiutangdebet.GLLink = tvpiutangbayar0.HPLink AND tvpiutangdebet.NoAccount = tvpiutangbayar0.NoAccount
			WHERE (tvpiutangdebet.NoAccount IN (" , AcctLease , "))
			AND tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar0.SumKreditGL, 0) > 0
			AND tvpiutangdebet.KodeTrans = 'SK';");
			PREPARE STMT FROM @MyQuery;
			EXECUTE Stmt;
			CREATE INDEX GLLink ON Piutang(GLLink);		
		END IF;
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT("SELECT Piutang.*, MyClient.*, SalesNama
			FROM Piutang INNER JOIN MyClient ON Piutang.GLLink = MyClient.SKNo INNER JOIN tdsales ON tdsales.SalesKode = MyClient.SalesKode ");
			SET @MyQuery = CONCAT(@MyQuery,"WHERE (TglGL <= '",xTgl2,"') ");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY NoAccount, SKTgl ");
		ELSE
			SET @MyQuery = CONCAT("SELECT Piutang.*, MyClient.*, SalesNama
			FROM Piutang INNER JOIN MyClient ON Piutang.GLLink = MyClient.SKNo INNER JOIN tdsales ON tdsales.SalesKode = MyClient.SalesKode ");
			SET @MyQuery = CONCAT(@MyQuery,"WHERE (TglGL <= '",xTgl2,"') ");
			SET @MyQuery = CONCAT(@MyQuery," AND NoAccount BETWEEN '" , xPerkiraan1 , "' AND '", xPerkiraan2, "' ");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY NoAccount, SKTgl ");
		END IF;
		
		PREPARE STMT FROM @MyQuery;
		EXECUTE Stmt;
	END IF;

	IF xTipe = "Riwayat Piutang Leasing" THEN
		IF xSensitif = "1" THEN
			DELETE FROM tvpiutangdebet ;
			INSERT INTO tvpiutangdebet  SELECT * FROM vpiutangdebet ;
		END IF;
		SELECT GROUP_CONCAT("'",NoAccount,"'") INTO AcctLease FROM traccount WHERE NoParent = '11080000';

		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			DROP TEMPORARY TABLE IF EXISTS vpiutangbayar;
			CREATE TEMPORARY TABLE IF NOT EXISTS vpiutangbayar AS
			SELECT ttgeneralledgerhd.GLLink AS NoPelunasan, ttglhpit.TglGL, ttglhpit.GLLink AS HPLink, NoAccount, HPNilai AS KreditGL, MemoGL
         FROM ttglhpit
               INNER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.NoGL = ttglhpit.NoGL AND ttgeneralledgerhd.TglGL = ttglhpit.TglGL
					WHERE (NOAccount BETWEEN '11060000' AND '11150000')
					AND HPJenis = 'Piutang' AND ttglhpit.TglGL BETWEEN xTgl1 AND xTgl2;
			CREATE INDEX HPLink ON vpiutangbayar(HPLink);
			CREATE INDEX NoAccount ON vpiutangbayar(NoAccount);

			SET @MyQuery = CONCAT("SELECT tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount, tvpiutangdebet.NamaAccount,
			tvpiutangdebet.KeteranganGL, tvpiutangdebet.DebetGL, IFNULL(vpiutangbayar.NoPelunasan,'--') AS NoPelunasan, vpiutangbayar.TglGL AS TglPelunasan,
			IFNULL(vpiutangbayar.MemoGL,'--') AS MemoPelunasan, IFNULL(vpiutangbayar.KreditGL,0) AS KreditGL
         FROM tvpiutangdebet
         LEFT OUTER JOIN vpiutangbayar ",
         "ON tvpiutangdebet.GLLink = vpiutangbayar.HPLink AND tvpiutangdebet.NoAccount = vpiutangbayar.NoAccount ");
			SET @MyQuery = CONCAT(@MyQuery," WHERE tvpiutangdebet.TglGL  BETWEEN '",xTgl1,"' AND '", xTgl2 , "'");
			SET @MyQuery = CONCAT(@MyQuery," AND tvpiutangdebet.NoAccount  IN (", AcctLease ,") ");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY NoAccount, TglGL ");
		ELSE
			DROP TEMPORARY TABLE IF EXISTS vpiutangbayar;
			CREATE TEMPORARY TABLE IF NOT EXISTS vpiutangbayar AS
			SELECT ttgeneralledgerhd.GLLink AS NoPelunasan, ttglhpit.TglGL, ttglhpit.GLLink AS HPLink, NoAccount, HPNilai AS KreditGL, MemoGL
         FROM ttglhpit
               INNER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.NoGL = ttglhpit.NoGL AND ttgeneralledgerhd.TglGL = ttglhpit.TglGL
					WHERE (NOAccount BETWEEN '11060000' AND '11150000')
					AND HPJenis = 'Piutang' AND ttglhpit.TglGL BETWEEN xTgl1 AND xTgl2;
			CREATE INDEX HPLink ON vpiutangbayar(HPLink);
			CREATE INDEX NoAccount ON vpiutangbayar(NoAccount);

			SET @MyQuery = CONCAT("SELECT tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount, tvpiutangdebet.NamaAccount,
			tvpiutangdebet.KeteranganGL, tvpiutangdebet.DebetGL, IFNULL(vpiutangbayar.NoPelunasan,'--') AS NoPelunasan, vpiutangbayar.TglGL AS TglPelunasan,
			IFNULL(vpiutangbayar.MemoGL,'--') AS MemoPelunasan, IFNULL(vpiutangbayar.KreditGL,0) AS KreditGL
         FROM tvpiutangdebet
         LEFT OUTER JOIN vpiutangbayar ",
			"ON tvpiutangdebet.GLLink = vpiutangbayar.HPLink AND tvpiutangdebet.NoAccount = vpiutangbayar.NoAccount ");
			SET @MyQuery = CONCAT(@MyQuery," WHERE tvpiutangdebet.TglGL  BETWEEN '",xTgl1,"' AND '", xTgl2 , "'");
			SET @MyQuery = CONCAT(@MyQuery," AND tvpiutangdebet.NoAccount BETWEEN '" , xPerkiraan1 , "' AND '", xPerkiraan2, "' ");
			SET @MyQuery = CONCAT(@MyQuery," AND tvpiutangdebet.NoAccount  IN (", AcctLease ,") ");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY NoAccount, TglGL ");
		END IF;
		IF xNoBukti <> "" AND LENGTH(xNoBukti) >= 10 THEN
		SET @MyQuery = CONCAT("SELECT vpiutangdebet.GLLink, vpiutangdebet.NoGL, vpiutangdebet.TglGL, vpiutangdebet.NoAccount, vpiutangdebet.NamaAccount,
			vpiutangdebet.KeteranganGL, vpiutangdebet.DebetGL, IFNULL(vpiutangbayar.NoPelunasan,'--') AS NoPelunasan, vpiutangbayar.TglGL AS TglPelunasan,
			IFNULL(vpiutangbayar.MemoGL,'--') AS MemoPelunasan, IFNULL(vpiutangbayar.KreditGL,0) AS KreditGL
         FROM vpiutangdebet
         LEFT OUTER JOIN
             (SELECT ttgeneralledgerhd.GLLink AS NoPelunasan, ttglhpit.TglGL, ttglhpit.GLLink AS HPLink, NoAccount, HPNilai AS KreditGL, MemoGL
               FROM ttglhpit
               INNER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.NoGL = ttglhpit.NoGL AND ttgeneralledgerhd.TglGL = ttglhpit.TglGL
					WHERE (NOAccount BETWEEN '11060000' AND '11150000')
					AND HPJenis = 'Piutang' AND ttglhpit.GLLink = '" , xNoBukti , "') vpiutangbayar ",
					"ON vpiutangdebet.GLLink = vpiutangbayar.HPLink AND vpiutangdebet.NoAccount = vpiutangbayar.NoAccount ");
			SET @MyQuery = CONCAT(@MyQuery," WHERE vpiutangdebet.GLLink = '" , xNoBukti , "'");
			SET @MyQuery = CONCAT(@MyQuery," AND vpiutangdebet.NoAccount  IN (", AcctLease ,") ");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY NoAccount, TglGL ");
		END IF;
	END IF;

	IF xTipe = "Riwayat Piutang Non Leasing" THEN
		IF xSensitif = "1" THEN
			DELETE FROM tvpiutangdebet ;
			INSERT INTO tvpiutangdebet  SELECT * FROM vpiutangdebet ;
		END IF;
		SELECT GROUP_CONCAT("'",NoAccount,"'") INTO AcctLease FROM traccount WHERE NoParent = '11080000';

		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			DROP TEMPORARY TABLE IF EXISTS vpiutangbayar;
			CREATE TEMPORARY TABLE IF NOT EXISTS vpiutangbayar AS
			SELECT ttgeneralledgerhd.GLLink AS NoPelunasan, ttglhpit.TglGL, ttglhpit.GLLink AS HPLink, NoAccount, HPNilai AS KreditGL, MemoGL
         FROM ttglhpit
               INNER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.NoGL = ttglhpit.NoGL AND ttgeneralledgerhd.TglGL = ttglhpit.TglGL
					WHERE (NOAccount BETWEEN '11060000' AND '11150000')
					AND HPJenis = 'Piutang' AND ttglhpit.TglGL BETWEEN xTgl1 AND xTgl2;
			CREATE INDEX HPLink ON vpiutangbayar(HPLink);
			CREATE INDEX NoAccount ON vpiutangbayar(NoAccount);

			SET @MyQuery = CONCAT("SELECT tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount, tvpiutangdebet.NamaAccount,
			tvpiutangdebet.KeteranganGL, tvpiutangdebet.DebetGL, IFNULL(vpiutangbayar.NoPelunasan,'--') AS NoPelunasan, vpiutangbayar.TglGL AS TglPelunasan,
			IFNULL(vpiutangbayar.MemoGL,'--') AS MemoPelunasan, IFNULL(vpiutangbayar.KreditGL,0) AS KreditGL
         FROM tvpiutangdebet
         LEFT OUTER JOIN vpiutangbayar ",
         "ON tvpiutangdebet.GLLink = vpiutangbayar.HPLink AND tvpiutangdebet.NoAccount = vpiutangbayar.NoAccount ");
			SET @MyQuery = CONCAT(@MyQuery," WHERE tvpiutangdebet.TglGL  BETWEEN '",xTgl1,"' AND '", xTgl2 , "'");
			SET @MyQuery = CONCAT(@MyQuery," AND tvpiutangdebet.NoAccount NOT IN (", AcctLease ,") ");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY NoAccount, TglGL ");
		ELSE
			DROP TEMPORARY TABLE IF EXISTS vpiutangbayar;
			CREATE TEMPORARY TABLE IF NOT EXISTS vpiutangbayar AS
			SELECT ttgeneralledgerhd.GLLink AS NoPelunasan, ttglhpit.TglGL, ttglhpit.GLLink AS HPLink, NoAccount, HPNilai AS KreditGL, MemoGL
         FROM ttglhpit
               INNER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.NoGL = ttglhpit.NoGL AND ttgeneralledgerhd.TglGL = ttglhpit.TglGL
					WHERE (NOAccount BETWEEN '11060000' AND '11150000')
					AND HPJenis = 'Piutang' AND ttglhpit.TglGL BETWEEN xTgl1 AND xTgl2;
			CREATE INDEX HPLink ON vpiutangbayar(HPLink);
			CREATE INDEX NoAccount ON vpiutangbayar(NoAccount);

			SET @MyQuery = CONCAT("SELECT tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount, tvpiutangdebet.NamaAccount,
			tvpiutangdebet.KeteranganGL, tvpiutangdebet.DebetGL, IFNULL(vpiutangbayar.NoPelunasan,'--') AS NoPelunasan, vpiutangbayar.TglGL AS TglPelunasan,
			IFNULL(vpiutangbayar.MemoGL,'--') AS MemoPelunasan, IFNULL(vpiutangbayar.KreditGL,0) AS KreditGL
         FROM tvpiutangdebet
         LEFT OUTER JOIN vpiutangbayar ",
			"ON tvpiutangdebet.GLLink = vpiutangbayar.HPLink AND tvpiutangdebet.NoAccount = vpiutangbayar.NoAccount ");
			SET @MyQuery = CONCAT(@MyQuery," WHERE tvpiutangdebet.TglGL  BETWEEN '",xTgl1,"' AND '", xTgl2 , "'");
			SET @MyQuery = CONCAT(@MyQuery," AND tvpiutangdebet.NoAccount BETWEEN '" , xPerkiraan1 , "' AND '", xPerkiraan2, "' ");
			SET @MyQuery = CONCAT(@MyQuery," AND tvpiutangdebet.NoAccount NOT IN (", AcctLease ,") ");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY NoAccount, TglGL ");
		END IF;
		IF xNoBukti <> "" AND LENGTH(xNoBukti) >= 10 THEN
		SET @MyQuery = CONCAT("SELECT vpiutangdebet.GLLink, vpiutangdebet.NoGL, vpiutangdebet.TglGL, vpiutangdebet.NoAccount, vpiutangdebet.NamaAccount,
			vpiutangdebet.KeteranganGL, vpiutangdebet.DebetGL, IFNULL(vpiutangbayar.NoPelunasan,'--') AS NoPelunasan, vpiutangbayar.TglGL AS TglPelunasan,
			IFNULL(vpiutangbayar.MemoGL,'--') AS MemoPelunasan, IFNULL(vpiutangbayar.KreditGL,0) AS KreditGL
         FROM vpiutangdebet
         LEFT OUTER JOIN
             (SELECT ttgeneralledgerhd.GLLink AS NoPelunasan, ttglhpit.TglGL, ttglhpit.GLLink AS HPLink, NoAccount, HPNilai AS KreditGL, MemoGL
               FROM ttglhpit
               INNER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.NoGL = ttglhpit.NoGL AND ttgeneralledgerhd.TglGL = ttglhpit.TglGL
					WHERE (NOAccount BETWEEN '11060000' AND '11150000')
					AND HPJenis = 'Piutang' AND ttglhpit.GLLink = '" , xNoBukti , "') vpiutangbayar ",
					"ON vpiutangdebet.GLLink = vpiutangbayar.HPLink AND vpiutangdebet.NoAccount = vpiutangbayar.NoAccount ");
			SET @MyQuery = CONCAT(@MyQuery," WHERE vpiutangdebet.GLLink = '" , xNoBukti , "'");
			SET @MyQuery = CONCAT(@MyQuery," AND vpiutangdebet.NoAccount NOT IN (", AcctLease ,") ");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY NoAccount, TglGL ");
		END IF;
	END IF;

	IF xTipe = "Perincian Pelunasan"  THEN
		SET @MyQuery = CONCAT("SELECT ttglhpit.NoGL, ttglhpit.TglGL, ttglhpit.GLLink AS HutangPiutang,  ttgeneralledgerhd.GLLink AS Pelunasan, ttglhpit.NoAccount, 
		ttgeneralledgerhd.MemoGL AS NamaAccount, HPJenis, HPNilai
      FROM ttglhpit INNER JOIN ttgeneralledgerhd ON ttglhpit.NoGL = ttgeneralledgerhd.NoGL
      WHERE HPJenis = 'Piutang'
      AND ttglhpit.TglGL  BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT(@MyQuery," ");	
		ELSE
			SET @MyQuery = CONCAT(@MyQuery," AND (ttglhpit.NoAccount BETWEEN '",xPerkiraan1,"' AND '",xPerkiraan2,"')");	
		END IF;
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY ttglhpit.NoGL, ttglhpit.TglGL, ttgeneralledgerhd.GLLink ");
	END IF;

	IF xTipe = "NoSin Pelunasan"  THEN
		SET @MyQuery = CONCAT("SELECT ttglhpit.NoGL, ttglhpit.TglGL, ttglhpit.GLLink AS HutangPiutang,  ttgeneralledgerhd.GLLink AS Pelunasan, ttglhpit.NoAccount, ttgeneralledgerhd.MemoGL AS NamaAccount, HPJenis, HPNilai, MotorNoMesin
         FROM ttglhpit
         INNER JOIN ttgeneralledgerhd ON ttglhpit.NoGL = ttgeneralledgerhd.NoGL
         LEFT OUTER JOIN tmotor ON tmotor.SKNo = ttglhpit.GLLink
         WHERE HPJenis = 'Piutang'
      AND ttglhpit.TglGL  BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT(@MyQuery," ");	
		ELSE
			SET @MyQuery = CONCAT(@MyQuery," AND (ttglhpit.NoAccount BETWEEN '",xPerkiraan1,"' AND '",xPerkiraan2,"')");	
		END IF;
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY ttglhpit.NoGL, ttglhpit.TglGL, ttgeneralledgerhd.GLLink ");
	END IF;

	IF xTipe = "Belum Di Link" THEN
		IF xSensitif <> "1" THEN
			UPDATE ttgeneralledgerhd
			INNER JOIN (SELECT ttgeneralledgerhd.NoGL, HPLink, ttglhpit.GLLink
			FROM ttgeneralledgerhd
			INNER JOIN ttglhpit ON ttglhpit.NoGL =  ttgeneralledgerhd.NoGL
			WHERE HPLink = '--' OR HPLink = ''
			GROUP BY ttgeneralledgerhd.NoGL HAVING COUNT(ttglhpit.NoGL) > 1) ttglhpit ON ttglhpit.NoGL =  ttgeneralledgerhd.NoGL
			SET ttgeneralledgerhd.HPLink = 'Many'
			WHERE ttgeneralledgerhd.HPLink = '--' OR ttgeneralledgerhd.HPLink = '';
		
			UPDATE ttgeneralledgerhd
			INNER JOIN (SELECT ttgeneralledgerhd.NoGL, HPLink, ttglhpit.GLLink
			FROM ttgeneralledgerhd
			INNER JOIN ttglhpit ON ttglhpit.NoGL =  ttgeneralledgerhd.NoGL
			WHERE HPLink = '--' OR HPLink = ''
			GROUP BY ttgeneralledgerhd.NoGL HAVING COUNT(ttglhpit.NoGL) = 1) ttglhpit ON ttglhpit.NoGL =  ttgeneralledgerhd.NoGL
			SET ttgeneralledgerhd.HPLink = ttglhpit.GLLink
			WHERE ttgeneralledgerhd.HPLink = '--' OR ttgeneralledgerhd.HPLink = '';
			
			SET @MyQuery = CONCAT("SELECT ttgeneralledgerit.NoGL, ttgeneralledgerit.TglGL, ttgeneralledgerhd.GLLink, 
			ttgeneralledgerit.NoAccount, ttgeneralledgerit.KeteranganGL, ttgeneralledgerit.KreditGL As Nilai
         FROM ttgeneralledgerit 
         INNER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.NoGL = ttgeneralledgerit.NoGL 
         WHERE ttgeneralledgerit.KreditGL > 0 
         AND (ttgeneralledgerit.NoAccount BETWEEN '11060000' AND '11150000') AND ttgeneralledgerhd.HPLink = '--'
			AND ttgeneralledgerit.TglGL   BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
			IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
				SET @MyQuery = CONCAT(@MyQuery," ");	
			ELSE
				SET @MyQuery = CONCAT(@MyQuery," AND (ttgeneralledgerit.NoAccount BETWEEN '",xPerkiraan1,"' AND '",xPerkiraan2,"')");	
			END IF;
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY ttgeneralledgerit.NoGL, ttgeneralledgerit.TglGL, ttgeneralledgerhd.GLLink ");		
		END IF;
		
		IF xSensitif = "1" THEN
			DROP TEMPORARY TABLE IF EXISTS tvpiutangkredit;
			IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
				CREATE TEMPORARY TABLE IF NOT EXISTS tvpiutangkredit AS  SELECT * FROM vpiutangkredit WHERE  TglGL  BETWEEN Tgl1 AND Tgl2 ;
			ELSE
				CREATE TEMPORARY TABLE IF NOT EXISTS tvpiutangkredit AS  SELECT * FROM vpiutangkredit WHERE  TglGL  BETWEEN Tgl1 AND Tgl2  AND (vpiutangkredit.NoAccount BETWEEN xPerkiraan1 AND xPerkiraan2);
			END IF;
			CREATE INDEX NoGL ON tvpiutangkredit(NoGL);
			CREATE INDEX NoAccount ON tvpiutangkredit(NoAccount);
				
			SET @MyQuery = CONCAT("
			 SELECT tvpiutangkredit.NoGL, tvpiutangkredit.TglGL, tvpiutangkredit.GLLink, tvpiutangkredit.NoAccount, tvpiutangkredit.KeteranganGL, 
            tvpiutangkredit.KreditGL-IFNULL(ttglhpit.HPNilai,0) AS Nilai, ttglhpit.*
            FROM tvpiutangkredit 
            LEFT OUTER JOIN 
            (SELECT NoGL, NoAccount, IFNULL(SUM(HPNilai),0) AS HPNilai FROM ttglhpit WHERE (ttglhpit.NoAccount BETWEEN '11060000' AND '11150000') AND ttglhpit.HPJenis = 'Piutang' GROUP BY NOGL, NOaccount) ttglhpit 
            ON tvpiutangkredit.NoGL = ttglhpit.NoGL AND tvpiutangkredit.NoAccount = ttglhpit.NoAccount 
            WHERE  ttglhpit.NoGL IS NULL OR ((tvpiutangkredit.KreditGL- ttglhpit.HPNilai) >0 AND KodeTrans <> 'JA')
          UNION
          SELECT tvpiutangkredit.NoGL, tvpiutangkredit.TglGL, tvpiutangkredit.GLLink, tvpiutangkredit.NoAccount, tvpiutangkredit.KeteranganGL, tvpiutangkredit.KreditGL - IFNULL(ttglhpit.HPNilai, 0) AS Nilai, ttglhpit.* 
            FROM tvpiutangkredit 
            LEFT OUTER JOIN 
            (SELECT GLLink, NoAccount, IFNULL(SUM(HPNilai), 0) AS HPNilai FROM ttglhpit 
             WHERE (ttglhpit.NoAccount BETWEEN '11060000'AND '11150000') AND ttglhpit.HPJenis = 'Piutang' 
             GROUP BY GLLink, NOaccount) ttglhpit 
            ON tvpiutangkredit.HPlink = ttglhpit.GLLink AND tvpiutangkredit.NoAccount = ttglhpit.NoAccount 
            WHERE (KodeTrans = 'JA') AND (tvpiutangkredit.KreditGL- ttglhpit.HPNilai) > 0
			");
		END IF;
	END IF;
	
/*            
   "BBN Progresif"                                  
	CALL rGLPiutang('Umur Piutang - Curent','2020-02-01','2020-02-29','%','%','Belum','0','','');
*/	
	IF xTipe = "BBN Progresif" THEN
		CASE xStatus
			WHEN "Belum" THEN
				SET @LunasScript = " INNER JOIN 
               (SELECT vtkk.KKNo FROM vtkk 
               LEFT OUTER  JOIN 
               (SELECT * FROM vtkm WHERE KMJenis = 'BBN Plus' AND KMtgl <= 'MyTgl') ttkm 
               ON KMLink = vtkk.KKLink 
               WHERE vtkk.KKJenis = 'BBN Plus' AND KKLink <> '--' 
               GROUP BY vtkk.KKNo
               HAVING SUM(vtkk.KKNominal) <> SUM(IFNULL(ttkm.KMNominal, 0))) MyStatus ON MyStatus.KKNo = vtkk.KKno ";
			WHEN "Lunas" THEN
				SET @LunasScript = " INNER JOIN 
               (SELECT vtkk.KKNo FROM vtkk 
               LEFT OUTER  JOIN 
               (SELECT * FROM vtkm WHERE KMJenis = 'BBN Plus' AND KMtgl <= 'MyTgl') ttkm 
               ON KMLink = vtkk.KKLink 
               WHERE vtkk.KKJenis = 'BBN Plus' AND KKLink <> '--' 
               GROUP BY vtkk.KKNo
               HAVING SUM(vtkk.KKNominal) = SUM(IFNULL(ttkm.KMNominal, 0))) MyStatus ON MyStatus.KKNo = vtkk.KKno";
			ELSE
				SET @LunasScript = " ";
		END CASE;
	
		SET @MyQuery = CONCAT("SELECT vtkk.KKNo, vtkk.KKTgl, vtkk.KKLink,CusNama, vtkk.KKJenis, vtkk.KKNominal,  IFNULL(ttkm.KMno,'--') AS KMNo,IFNULL(DATE_FORMAT(KMtgl, '%d/%m/%Y'),'--') AS KMTgl, IFNULL(ttkm.KMNominal,0) AS KMNominal, (vtkk.KKNominal- IFNULL(ttkm.KMNominal,0)) AS Sisa
      FROM vtkk
      INNER JOIN ttdk ON ttdk.DKNo = vtkk.KKLink
      INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
      LEFT OUTER JOIN 
      (SELECT * FROM vtkm WHERE KMJenis = 'BBN Plus' AND KMtgl <= '",xTg2,"' ) ttkm ON KMLink = vtkk.KKLink");
      SET @MyQuery = CONCAT(@MyQuery,@LunasScript, " WHERE vtkk.KKJenis = 'BBN Plus' AND KKLink <> '--'  ORDER BY vtkk.KKNo, vtkk.KKTgl, IFNULL(ttkm.KMno,'X')");

	END IF;	
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;  
END$$
DELIMITER ;

