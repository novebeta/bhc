DELIMITER $$

DROP PROCEDURE IF EXISTS `rGLDashboard`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rGLDashboard`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE, IN xTanggal VARCHAR(25), IN xEOM VARCHAR(1))
BEGIN
/*
CALL rGLDashboard("Dashboard A1 Motor",'2020-02-01','2020-02-29', 'ttsk.SKTgl', '1');
*/ 
	DECLARE Tgl1Now, Tgl2Now, Tgl1Before, Tgl2Before DATE;

	SET xTgl1 = DATE_ADD(LAST_DAY(DATE_ADD(xTgl2, INTERVAL -1 MONTH)), INTERVAL 1 DAY);
	IF xEOM = '1' THEN
		SET xTgl2 = LAST_DAY(xTgl2);
		SET Tgl2Before = LAST_DAY(DATE_ADD(xTgl2, INTERVAL -1 MONTH));	
	END IF;
   SET Tgl1Now = xTgl1;
   SET Tgl2Now = xTgl2;
   SET Tgl1Before = DATE_ADD(xTgl1, INTERVAL -1 MONTH);  
	
	IF xTipe = "Dashboard A1 Motor" THEN
		DROP TEMPORARY TABLE IF EXISTS TypeGrowth;
		CREATE TEMPORARY TABLE IF NOT EXISTS TypeGrowth AS
		SELECT tdmotortype.MotorKategori AS MyGroup, MotorType, MotorNama AS Keterangan,
		000000000000000 AS MBefore,
		000000000000000 AS MNow,
		000.00 AS Growth,
		000000000000000 AS Stock
		FROM tdmotorkategori
		INNER JOIN tdmotortype ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
		GROUP BY MotorType ORDER BY MotorNo, MotorType;
		CREATE INDEX MotorType ON TypeGrowth(MotorType);

		DROP TEMPORARY TABLE IF EXISTS MyNOW;
		CREATE TEMPORARY TABLE IF NOT EXISTS MyNOW AS
		SELECT tdmotortype.MotorKategori, tdmotortype.MotorType, IFNULL(Jumlah, 0) AS Jumlah
		FROM  tdmotortype INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
		LEFT OUTER JOIN
			(SELECT tdmotortype.MotorKategori, tmotor.MotorType, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah
			 FROM tmotor
			 INNER JOIN tdmotortype ON (tmotor.MotorType= tdmotortype.MotorType )
			 INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
			 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
			 INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
			 WHERE ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now
			 GROUP BY tdmotortype.MotorKategori, tmotor.MotorType) Jum
		ON tdmotortype.MotorType = Jum.MotorType AND tdmotortype.MotorKategori = Jum.MotorKategori
		ORDER BY tdmotorkategori.MotorNo, tdmotortype.MotorType;
		CREATE INDEX MotorType ON MyNOW(MotorType);
		UPDATE TypeGrowth INNER JOIN MyNow ON MyNow.MotorType = TypeGrowth.MotorType SET MNow = Jumlah;

		DROP TEMPORARY TABLE IF EXISTS MyBefore;
		CREATE TEMPORARY TABLE IF NOT EXISTS MyBefore AS
		SELECT tdmotortype.MotorKategori, tdmotortype.MotorType, IFNULL(Jumlah, 0) AS Jumlah
		FROM  tdmotortype INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
		LEFT OUTER JOIN
			(SELECT tdmotortype.MotorKategori, tmotor.MotorType, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah
			 FROM tmotor
			 INNER JOIN tdmotortype ON (tmotor.MotorType= tdmotortype.MotorType )
			 INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
			 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
			 INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
			 WHERE ttsk.SKTgl BETWEEN Tgl1Before AND Tgl2Before
			 GROUP BY tdmotortype.MotorKategori, tmotor.MotorType) Jum
		ON tdmotortype.MotorType = Jum.MotorType AND tdmotortype.MotorKategori = Jum.MotorKategori
		ORDER BY tdmotorkategori.MotorNo, tdmotortype.MotorType ;
		CREATE INDEX MotorType ON MyBefore(MotorType);
		UPDATE TypeGrowth INNER JOIN MyBefore ON MyBefore.MotorType = TypeGrowth.MotorType SET MBefore = Jumlah;

		DROP TEMPORARY TABLE IF EXISTS TStock;
		CREATE TEMPORARY TABLE IF NOT EXISTS TStock AS
		SELECT tdmotortype.MotorKategori, tdmotortype.MotorType, IFNULL(StockMotor.Jumlah,0) AS Jumlah
				  FROM  tdmotortype INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
				  LEFT OUTER JOIN (SELECT tmotor.MotorType, COUNT(LokasiMotor.Kondisi) AS Jumlah, SUM(tmotor.FBHarga) AS FBHarga
								FROM (SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi
										FROM tvmotorlokasi
													INNER JOIN (SELECT tvmotorlokasi_1.MotorAutoN, tvmotorlokasi_1.MotorNoMesin , MAX(tvmotorlokasi_1.Jam) AS Jam
																	FROM tvmotorlokasi tvmotorlokasi_1
																	WHERE tvmotorlokasi_1.Jam <= CONCAT(xTgl2, " 23:59:59")
																	GROUP BY tvmotorlokasi_1.MotorNoMesin, tvmotorlokasi_1.MotorAutoN) B
													  ON tvmotorlokasi.MotorNoMesin = B.MotorNoMesin AND tvmotorlokasi.Jam = B.Jam AND tvmotorlokasi.MotorAutoN = B.MotorAutoN
												 WHERE tvmotorlokasi.Kondisi IN ('IN','OUT Pos') ) LokasiMotor
											INNER JOIN tmotor
											  ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN
										 GROUP BY tmotor.MotorType) StockMotor
					 ON tdmotortype.MotorType = StockMotor.MotorType
				ORDER BY tdmotorkategori.MotorNo, tdmotortype.MotorType;
		CREATE INDEX MotorType ON TStock(MotorType);
		UPDATE TypeGrowth INNER JOIN TStock ON TStock.MotorType = TypeGrowth.MotorType SET Stock = Jumlah;


		DROP TEMPORARY TABLE IF EXISTS TGroup;
		CREATE TEMPORARY TABLE IF NOT EXISTS TGroup AS
		SELECT 'TOTAL' AS MyGroup, CONCAT_WS('','TOTAL ',REPLACE(MyGROUP,'Low','')) AS MotorType, CONCAT_WS('','TOTAL ',REPLACE(MyGROUP,'Low','')) AS Keterangan, SUM(MBefore) AS MBefore, SUM(MNow) AS MNow, 0 AS Growth, SUM(Stock) AS Stock FROM TypeGrowth GROUP BY LEFT(MyGROUP,3);

		INSERT INTO TypeGrowth SELECT * FROM TGroup;
		DELETE FROM TypeGrowth WHERE   MBefore = 0 AND MNow = 0 AND Stock = 0;
		UPDATE TypeGrowth SET Growth = ROUND((MNow - MBefore) / MBefore * 100,2) WHERE MBefore <> 0;
		UPDATE TypeGrowth SET Growth = 100 WHERE MBefore = 0;
		UPDATE TypeGrowth SET Growth = 0 WHERE MBefore = 0 AND MNow = 0;

		SET @MyQuery = CONCAT("SELECT * FROM TypeGrowth;");		
 	END IF;

	IF xTipe = "Dashboard A1 Growth" THEN
		DROP TEMPORARY TABLE IF EXISTS Growth;
		CREATE TEMPORARY TABLE IF NOT EXISTS Growth AS
			SELECT 'Keterangan' AS MyGroup, 'Sales'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'Credit Sales'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'Retur'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'Profit'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'Profit Per Unit'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'PDOP'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'SD 2 + Retur'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'SD 2 + Retur Per Unit'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'Cont'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'Stock'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'Stock Day'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'Piutang Karyawan'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'Piutang Promo'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth  UNION
			SELECT 'Keterangan' AS MyGroup, 'Piutang Konsumen'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'Piutang Leasing'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth ;
		ALTER TABLE Growth MODIFY COLUMN MBefore DECIMAL(19,2);
		ALTER TABLE Growth MODIFY COLUMN MNow DECIMAL(19,2);
		ALTER TABLE Growth MODIFY COLUMN Growth DECIMAL(15,2);
		ALTER TABLE Growth MODIFY COLUMN Keterangan VARCHAR(200);
		CREATE INDEX Keterangan ON Growth(Keterangan);
		CREATE INDEX MyGroup ON Growth(MyGroup);

		SELECT COUNT(tmotor.SKNo) INTO @SalesNow FROM tmotor INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo WHERE ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now;
		UPDATE Growth SET MNow = @SalesNow WHERE Keterangan = 'Sales';
		SELECT COUNT(tmotor.SKNo) INTO @SalesBefore FROM tmotor INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo WHERE ttsk.SKTgl BETWEEN Tgl1Before AND Tgl2Before;
		UPDATE Growth SET MBefore = @SalesBefore WHERE Keterangan = 'Sales';

		SELECT COUNT(tmotor.SKNo) INTO @Nilai FROM tmotor INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo WHERE ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now 
		AND LeaseKode <> 'TUNAI' AND LeaseKode <> 'KDS';
		UPDATE Growth SET MNow = @Nilai WHERE Keterangan = 'Credit Sales';
		SELECT COUNT(tmotor.SKNo) INTO @Nilai FROM tmotor INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo WHERE ttsk.SKTgl BETWEEN Tgl1Before 
		AND Tgl2Before AND LeaseKode <> 'TUNAI' AND LeaseKode <> 'KDS';
		UPDATE Growth SET MBefore = @Nilai WHERE Keterangan = 'Credit Sales';

		SELECT COUNT(tmotor.RKNo) INTO @Nilai FROM tmotor INNER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo WHERE ttrk.RKTgl BETWEEN Tgl1Now AND Tgl2Now;
		UPDATE Growth SET MNow = @Nilai WHERE Keterangan = 'Retur';
		SELECT COUNT(tmotor.RKNo) INTO @Nilai FROM tmotor INNER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo WHERE ttrk.RKTgl BETWEEN Tgl1Before AND Tgl2Before;
		UPDATE Growth SET MBefore = @Nilai WHERE Keterangan = 'Retur';

		SELECT SUM(ttdk.PotonganHarga + ttdk.ReturHarga) INTO @SDRKNow FROM tmotor INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo  
		WHERE ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now AND LeaseKode <> 'TUNAI' AND LeaseKode <> 'KDS';
		UPDATE Growth SET MNow = @SDRKNow WHERE Keterangan = 'SD 2 + Retur';
		SELECT SUM(ttdk.PotonganHarga + ttdk.ReturHarga) INTO @SDRKBefore FROM tmotor INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo 
		WHERE ttsk.SKTgl BETWEEN Tgl1Before AND Tgl2Before AND LeaseKode <> 'TUNAI' AND LeaseKode <> 'KDS';
		UPDATE Growth SET MBefore = @SDRKBefore WHERE Keterangan = 'SD 2 + Retur';

		UPDATE Growth SET MNow = ROUND(@SDRKNow/@SalesNow,2) WHERE Keterangan = 'SD 2 + Retur Per Unit';
		UPDATE Growth SET MBefore = ROUND(@SDRKBefore/@SalesBefore,2) WHERE Keterangan = 'SD 2 + Retur Per Unit';

		SELECT COUNT(LokasiMotor.Kondisi) INTO @JumNow FROM
			(SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi
			FROM tvmotorlokasi
			INNER JOIN (SELECT tvmotorlokasi_1.MotorAutoN, tvmotorlokasi_1.MotorNoMesin , MAX(tvmotorlokasi_1.Jam) AS Jam
							FROM tvmotorlokasi tvmotorlokasi_1
							WHERE tvmotorlokasi_1.Jam <= CONCAT(Tgl2Now, " 23:59:59")
							GROUP BY tvmotorlokasi_1.MotorNoMesin, tvmotorlokasi_1.MotorAutoN) B
			ON tvmotorlokasi.MotorNoMesin = B.MotorNoMesin AND tvmotorlokasi.Jam = B.Jam AND tvmotorlokasi.MotorAutoN = B.MotorAutoN
			WHERE tvmotorlokasi.Kondisi IN ('IN','OUT Pos') ) LokasiMotor
		INNER JOIN tmotor ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN;
		UPDATE Growth SET MNow = @JumNow WHERE Keterangan = 'Stock';

		SELECT COUNT(LokasiMotor.Kondisi) INTO @JumBefore FROM
			(SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi
			FROM tvmotorlokasi
			INNER JOIN (SELECT tvmotorlokasi_1.MotorAutoN, tvmotorlokasi_1.MotorNoMesin , MAX(tvmotorlokasi_1.Jam) AS Jam
							FROM tvmotorlokasi tvmotorlokasi_1
							WHERE tvmotorlokasi_1.Jam <= CONCAT(Tgl2Before, " 23:59:59")
							GROUP BY tvmotorlokasi_1.MotorNoMesin, tvmotorlokasi_1.MotorAutoN) B
			ON tvmotorlokasi.MotorNoMesin = B.MotorNoMesin AND tvmotorlokasi.Jam = B.Jam AND tvmotorlokasi.MotorAutoN = B.MotorAutoN
			WHERE tvmotorlokasi.Kondisi IN ('IN','OUT Pos') ) LokasiMotor
		INNER JOIN tmotor ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN;
		UPDATE Growth SET MBefore = @JumBefore WHERE Keterangan = 'Stock';

		UPDATE Growth SET MBefore = ROUND(@JumBefore/(@SalesBefore/DAY(DATE_ADD(Tgl2Now, INTERVAL -1 MONTH))),2) WHERE Keterangan = 'Stock Day';
		UPDATE Growth SET MNow = ROUND(@JumNow/(@SalesNow/DAY(Tgl2Now)),2) WHERE Keterangan = 'Stock Day';

		CALL rglNeraca(Tgl1Before, Tgl2Before, Tgl2Before);
		SELECT Kredit-Debet INTO @ProfitBefore FROM traccountsaldo WHERE NoAccount = '26040000' ;
		SELECT Saldo INTO @PDOPBefore FROM traccountsaldo WHERE NoAccount = '40000000' ;
		SELECT Saldo INTO @PiutangKaryawanBefore FROM traccountsaldo WHERE NoAccount = '11120100' ;
		SELECT Saldo INTO @PiutangPromoBefore FROM traccountsaldo WHERE NoAccount = '11110500' ;
		SELECT Saldo INTO @PiutangKonsumenBefore FROM traccountsaldo WHERE NoAccount = '11060100' ;
		SELECT Saldo INTO @PiutangLeasingBefore FROM traccountsaldo WHERE NoAccount = '11080000' ;
		UPDATE Growth SET MBefore = @ProfitBefore WHERE Keterangan = 'Profit';
		UPDATE Growth SET MBefore = ROUND(@ProfitBefore/@SalesBefore,2)  WHERE Keterangan = 'Profit Per Unit';
		UPDATE Growth SET MBefore = @PDOPBefore WHERE Keterangan = 'PDOP';
		UPDATE Growth SET MBefore = @PiutangKaryawanBefore WHERE Keterangan = 'Piutang Karyawan';
		UPDATE Growth SET MBefore = @PiutangPromoBefore WHERE Keterangan = 'Piutang Promo';
		UPDATE Growth SET MBefore = @PiutangKonsumenBefore WHERE Keterangan = 'Piutang Konsumen';
		UPDATE Growth SET MBefore = @PiutangLeasingBefore WHERE Keterangan = 'Piutang Leasing';

		CALL rglNeraca(Tgl1Now, Tgl2Now, Tgl2Now);
		SELECT Kredit-Debet INTO @ProfitNow FROM traccountsaldo WHERE NoAccount = '26040000' ;
		SELECT Saldo INTO @PDOPNow FROM traccountsaldo WHERE NoAccount = '40000000' ;
		SELECT Saldo INTO @PiutangKaryawanNow FROM traccountsaldo WHERE NoAccount = '11120100' ;
		SELECT Saldo INTO @PiutangPromoNow FROM traccountsaldo WHERE NoAccount = '11110500' ;
		SELECT Saldo INTO @PiutangKonsumenNow FROM traccountsaldo WHERE NoAccount = '11060100' ;
		SELECT Saldo INTO @PiutangLeasingNow FROM traccountsaldo WHERE NoAccount = '11080000' ;
		UPDATE Growth SET MNow = @ProfitNow WHERE Keterangan = 'Profit';
		UPDATE Growth SET MNow = ROUND(@ProfitNow/@SalesNow,2)  WHERE Keterangan = 'Profit Per Unit';
		UPDATE Growth SET MNow = @PDOPNow WHERE Keterangan = 'PDOP';
		UPDATE Growth SET MNow = @PiutangKaryawanNow WHERE Keterangan = 'Piutang Karyawan';
		UPDATE Growth SET MNow = @PiutangPromoNow WHERE Keterangan = 'Piutang Promo';
		UPDATE Growth SET MNow = @PiutangKonsumenNow WHERE Keterangan = 'Piutang Konsumen';
		UPDATE Growth SET MNow = @PiutangLeasingNow WHERE Keterangan = 'Piutang Leasing';

		INSERT INTO Growth SELECT 'Keterangan' AS MyGroup, NamaAccount AS Keterangan, OpeningBalance AS MBefore, Saldo AS MNow, 0 AS Growth FROM traccountsaldo WHERE NoParent = '11080000' AND JenisAccount = 'Detail';

		INSERT INTO Growth SELECT 'Team' AS MyGroup, tdteam.TeamKode AS Keterangan, IFNULL(JumBefore,0) AS MBefore, IFNULL(JumNow,0) AS MNow , 0 AS Growth FROM tdteam
      LEFT OUTER JOIN (SELECT ttdk.TeamKode, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS JumNow FROM tmotor
                        INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
                        INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo WHERE ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now
                        GROUP BY TRIM(ttdk.TeamKode)) TJumNow
                        ON tdteam.TeamKode = TJumNow.TeamKode
      LEFT OUTER JOIN (SELECT ttdk.TeamKode, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS JumBefore FROM tmotor
                        INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
                        INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo WHERE ttsk.SKTgl BETWEEN Tgl1Before AND Tgl2Before
                        GROUP BY TRIM(ttdk.TeamKode)) TJumBefore
                        ON tdteam.TeamKode = TJumBefore.TeamKode ORDER BY tdteam.TeamNo, tdteam.TeamKode;
		
		UPDATE Growth SET Growth = ROUND((MNow -  MBefore) / MBefore *100,2) WHERE MBefore <> 0;	
		UPDATE Growth SET Growth = 100 WHERE MBefore = 0;
		UPDATE Growth SET Growth = 0 WHERE MBefore = 0 AND MNow = 0;

		DELETE FROM Growth WHERE   MBefore = 0 AND MNow = 0 ;
	
		SET @MyQuery = CONCAT("SELECT * FROM Growth;");	
	END IF;
		
	IF xTipe = "Dashboard A2 Segmen" THEN
		DROP TEMPORARY TABLE IF EXISTS SegmenGrowth;
		CREATE TEMPORARY TABLE IF NOT EXISTS SegmenGrowth AS
			SELECT 'Category' AS MyDash, tdmotorkategori.MotorGroup AS MyGroup, tdmotorkategori.MotorKategori AS Keterangan,
			0 AS MBefore,
			0 AS MNow,
			0 AS Growth,
			0 AS CBefore,
			0 AS CNow
			FROM tdmotorkategori ORDER BY MotorNo;
		ALTER TABLE SegmenGrowth MODIFY COLUMN MyDash VARCHAR(20);
		ALTER TABLE SegmenGrowth MODIFY COLUMN MBefore DECIMAL(19,2);
		ALTER TABLE SegmenGrowth MODIFY COLUMN MNow DECIMAL(19,2);
		ALTER TABLE SegmenGrowth MODIFY COLUMN Growth DECIMAL(15,2);
		ALTER TABLE SegmenGrowth MODIFY COLUMN CBefore DECIMAL(19,2);
		ALTER TABLE SegmenGrowth MODIFY COLUMN CNow DECIMAL(19,2);
		ALTER TABLE SegmenGrowth MODIFY COLUMN Keterangan VARCHAR(20);
		CREATE INDEX Keterangan ON SegmenGrowth(Keterangan);
		CREATE INDEX MyDash ON SegmenGrowth(MyDash);
		CREATE INDEX MyGroup ON SegmenGrowth(MyGroup);

		DROP TEMPORARY TABLE IF EXISTS TFillNow;
		CREATE TEMPORARY TABLE IF NOT EXISTS TFillNow AS
			SELECT tdmotorkategori.MotorGroup, tdmotorkategori.MotorKategori, IFNULL(Jumlah, 0) AS Jumlah
			FROM  tdmotorkategori
			LEFT OUTER JOIN
				(SELECT tdmotorkategori.MotorGroup, tdmotortype.MotorKategori, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah
				 FROM tmotor
				 INNER JOIN tdmotortype ON (tmotor.MotorType= tdmotortype.MotorType )
				 INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
				 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
				 INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
				 WHERE ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now
				 GROUP BY tdmotorkategori.MotorGroup, tdmotorkategori.MotorKategori) Jum
			ON tdmotorkategori.MotorGroup = Jum.MotorGroup AND tdmotorkategori.MotorKategori = Jum.MotorKategori
			ORDER BY tdmotorkategori.MotorNo, tdmotorkategori.MotorKategori;
		CREATE INDEX MotorKategori ON TFillNow(MotorKategori);
		UPDATE SegmenGrowth INNER JOIN TFillNow ON SegmenGrowth.Keterangan = TFillNow.MotorKategori SET  MNow = Jumlah;

		DROP TEMPORARY TABLE IF EXISTS TFillBefore;
		CREATE TEMPORARY TABLE IF NOT EXISTS TFillBefore AS
			SELECT tdmotorkategori.MotorGroup, tdmotorkategori.MotorKategori, IFNULL(Jumlah, 0) AS Jumlah
			FROM  tdmotorkategori
			LEFT OUTER JOIN
				(SELECT tdmotorkategori.MotorGroup, tdmotortype.MotorKategori, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah
				 FROM tmotor
				 INNER JOIN tdmotortype ON (tmotor.MotorType= tdmotortype.MotorType )
				 INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
				 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
				 INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
				 WHERE ttsk.SKTgl BETWEEN Tgl1Before AND Tgl2Before
				 GROUP BY tdmotorkategori.MotorGroup, tdmotorkategori.MotorKategori) Jum
			ON tdmotorkategori.MotorGroup = Jum.MotorGroup AND tdmotorkategori.MotorKategori = Jum.MotorKategori
			ORDER BY tdmotorkategori.MotorNo, tdmotorkategori.MotorKategori;
		CREATE INDEX MotorKategori ON TFillBefore(MotorKategori);
		UPDATE SegmenGrowth INNER JOIN TFillBefore ON SegmenGrowth.Keterangan = TFillBefore.MotorKategori SET  MBefore = Jumlah;

		DROP TEMPORARY TABLE IF EXISTS TGroup;
		CREATE TEMPORARY TABLE IF NOT EXISTS TGroup AS
			SELECT MyDash, MyGroup, Keterangan, MBefore, MNow, Growth, CBefore, CNow FROM
			  (SELECT 'Sum Category' AS MyDash, 'Total All' AS MyGroup,  REPLACE(Keterangan,'Low','') AS Keterangan,
				SUM(MBefore) AS MBefore, SUM(MNow) AS MNow, 0 AS Growth,
				0 AS CBefore, 0 AS CNow FROM SegmenGrowth
				GROUP BY LEFT(Keterangan,3)) SegmenGrowth
			INNER JOIN
				(SELECT MotorNo, MotorGroup FROM tdmotorkategori GROUP BY MotorGroup) tdmotorkategori
			ON tdmotorkategori.MotorGroup = SegmenGrowth.Keterangan
			ORDER BY MotorNo;
		INSERT INTO SegmenGrowth SELECT * FROM TGroup;

		INSERT INTO SegmenGrowth
		SELECT '' AS MyDash,MyGroup, Sumber.LeaseKode AS Keterangan,  IFNULL(JumBefore, 0) AS MBefore,IFNULL(JumNow, 0) AS MNow, 0 AS Growth, 0 AS CBefore, 0 AS CNow FROM
			(SELECT 'Total Sales' AS MyGroup, 'CASH' AS LeaseKode
			UNION
			SELECT 'Total Sales' AS MyGroup, 'CREDIT' AS LeaseKode
			UNION
			SELECT 'Total Credit' AS MyGroup, LeaseKode FROM tdleasing WHERE LeaseKode <> 'TUNAI' AND LeaseKode <> 'KDS') Sumber
		LEFT OUTER JOIN
			(SELECT 'CASH' AS LeaseKode, COUNT(ttsk.SKNo) AS JumNow FROM tmotor INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
			WHERE (LeaseKode = 'TUNAI' OR LeaseKode = 'KDS') AND ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now
			UNION
			SELECT 'CREDIT' AS LeaseKode, COUNT(ttsk.SKNo) AS JumNow FROM tmotor INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
			WHERE LeaseKode <> 'TUNAI' AND LeaseKode <> 'KDS' AND ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now
			UNION
			SELECT ttdk.LeaseKode, COUNT(ttsk.SKNo) AS JumNow FROM tmotor INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
			WHERE LeaseKode <> 'TUNAI' AND LeaseKode <> 'KDS' AND ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now
			GROUP BY LeaseKode) TJumNow
		ON Sumber.LeaseKode = TJumNow.LeaseKode
		LEFT OUTER JOIN
			(SELECT 'CASH' AS LeaseKode, COUNT(ttsk.SKNo) AS JumBefore FROM tmotor INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
			WHERE (LeaseKode = 'TUNAI' OR LeaseKode = 'KDS') AND ttsk.SKTgl BETWEEN Tgl1Before AND Tgl2Before
			UNION
			SELECT 'CREDIT' AS LeaseKode, COUNT(ttsk.SKNo) AS JumBefore FROM tmotor INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
			WHERE LeaseKode <> 'TUNAI' AND LeaseKode <> 'KDS' AND ttsk.SKTgl BETWEEN Tgl1Before AND Tgl2Before
			UNION
			SELECT ttdk.LeaseKode, COUNT(ttsk.SKNo) AS JumBefore FROM tmotor INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
			WHERE LeaseKode <> 'TUNAI' AND LeaseKode <> 'KDS' AND ttsk.SKTgl BETWEEN Tgl1Before AND Tgl2Before
			GROUP BY LeaseKode) TJumBefore
		ON Sumber.LeaseKode = TJumBefore.LeaseKode;

		UPDATE SegmenGrowth SET Growth = ROUND((MNow -  MBefore) / MBefore *100,2) WHERE MBefore <> 0;
		UPDATE SegmenGrowth SET Growth = 100 WHERE MBefore = 0;
		UPDATE SegmenGrowth SET Growth = 0 WHERE MBefore = 0 AND MNow = 0;

		SELECT SUM(MNow) INTO @MNowCub FROM SegmenGrowth WHERE MyGroup = 'Cub';
		UPDATE SegmenGrowth SET CNow = ROUND(MNow/@MNowCub*100,2)  WHERE MYGroup = 'Cub';
		SELECT SUM(MNow) INTO @MNowAT FROM SegmenGrowth WHERE MyGroup = 'AT';
		UPDATE SegmenGrowth SET CNow = ROUND(MNow/@MNowAT*100,2) WHERE MYGroup = 'AT';
		SELECT SUM(MNow) INTO @MNowSport FROM SegmenGrowth WHERE MyGroup = 'Sport';
		UPDATE SegmenGrowth SET CNow = ROUND(MNow/@MNowSport*100,2) WHERE MYGroup = 'Sport';
		SELECT SUM(MNow) INTO @MNowTotal FROM SegmenGrowth WHERE MyGroup = 'Total All';
		UPDATE SegmenGrowth SET CNow = ROUND(MNow/@MNowTotal*100,2) WHERE MYGroup = 'Total All';

		SELECT SUM(MBefore) INTO @MBeforeCub FROM SegmenGrowth WHERE MyGroup = 'Cub';
		UPDATE SegmenGrowth SET CBefore = ROUND(MBefore/@MBeforeCub*100,2)  WHERE MYGroup = 'Cub';
		SELECT SUM(MBefore) INTO @MBeforeAT FROM SegmenGrowth WHERE MyGroup = 'AT';
		UPDATE SegmenGrowth SET CBefore = ROUND(MBefore/@MBeforeAT*100,2) WHERE MYGroup = 'AT';
		SELECT SUM(MBefore) INTO @MBeforeSport FROM SegmenGrowth WHERE MyGroup = 'Sport';
		UPDATE SegmenGrowth SET CBefore = ROUND(MBefore/@MBeforeSport*100,2) WHERE MYGroup = 'Sport';
		SELECT SUM(MBefore) INTO @MBeforeTotal FROM SegmenGrowth WHERE MyGroup = 'Total All';
		UPDATE SegmenGrowth SET CBefore = ROUND(MBefore/@MBeforeTotal*100,2) WHERE MYGroup = 'Total All';

		SELECT SUM(MNow) INTO @MNowSales FROM SegmenGrowth WHERE MyGroup = 'Total Sales';
		UPDATE SegmenGrowth SET CNow = ROUND(MNow/@MNowSales*100,2)  WHERE MYGroup = 'Total Sales';
		SELECT SUM(MNow) INTO @MNowCredit FROM SegmenGrowth WHERE MyGroup = 'Total Credit';
		UPDATE SegmenGrowth SET CNow = ROUND(MNow/@MNowCredit*100,2) WHERE MYGroup = 'Total Credit';

		SELECT SUM(MBefore) INTO @MBeforeSales FROM SegmenGrowth WHERE MyGroup = 'Total Sales';
		UPDATE SegmenGrowth SET CBefore = ROUND(MBefore/@MBeforeSales*100,2)  WHERE MYGroup = 'Total Sales';
		SELECT SUM(MBefore) INTO @MBeforeCredit FROM SegmenGrowth WHERE MyGroup = 'Total Credit';
		UPDATE SegmenGrowth SET CBefore = ROUND(MBefore/@MBeforeCredit*100,2) WHERE MYGroup = 'Total Credit';
    
		SET @MyQuery = CONCAT("SELECT * FROM SegmenGrowth;");		
 	END IF;

	IF xTipe = "Dashboard A2 Lokasi" THEN
		DROP TEMPORARY TABLE IF EXISTS Growth;
		CREATE TEMPORARY TABLE IF NOT EXISTS Growth AS
			SELECT 'Lokasi' AS MyGroup, LokasiKode, LokasiNama AS Keterangan,
			0 AS MBefore,
			0 AS MNow,
			0 AS Growth
			FROM tdlokasi ORDER BY LokasiStatus, LokasiKode;
			ALTER TABLE Growth MODIFY COLUMN MyGroup VARCHAR(20);
			ALTER TABLE Growth MODIFY COLUMN MBefore DECIMAL(19,2);
			ALTER TABLE Growth MODIFY COLUMN MNow DECIMAL(19,2);
			ALTER TABLE Growth MODIFY COLUMN Growth DECIMAL(15,2);
			ALTER TABLE Growth MODIFY COLUMN Keterangan VARCHAR(150);
		CREATE INDEX LokasiKode ON Growth(LokasiKode);

		DROP TEMPORARY TABLE IF EXISTS TFillNow;
		CREATE TEMPORARY TABLE IF NOT EXISTS TFillNow AS
			SELECT tdlokasi.LokasiKode, IFNULL(Jumlah, 0) AS Jumlah
			FROM  tdlokasi
			LEFT OUTER JOIN
				(SELECT ttsk.LokasiKode, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah
				 FROM tmotor
				 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
				 WHERE ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now
				 GROUP BY ttsk.LokasiKode) Jum
			ON tdlokasi.LokasiKode = Jum.LokasiKode
			ORDER BY tdlokasi.LokasiStatus, tdlokasi.LokasiKode ;
		CREATE INDEX LokasiKode ON TFillNow(LokasiKode);
		UPDATE Growth INNER JOIN TFillNow ON Growth.LokasiKode = TFillNow.LokasiKode SET  MNow = Jumlah;

		DROP TEMPORARY TABLE IF EXISTS TFillBefore;
		CREATE TEMPORARY TABLE IF NOT EXISTS TFillBefore AS
		SELECT tdlokasi.LokasiKode, IFNULL(Jumlah, 0) AS Jumlah
		FROM  tdlokasi
		LEFT OUTER JOIN
			(SELECT ttsk.LokasiKode, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah
			 FROM tmotor
			 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
			 WHERE ttsk.SKTgl BETWEEN Tgl1Before AND Tgl2Before
			 GROUP BY ttsk.LokasiKode) Jum
		ON tdlokasi.LokasiKode = Jum.LokasiKode
		ORDER BY tdlokasi.LokasiStatus, tdlokasi.LokasiKode ;
		CREATE INDEX LokasiKode ON TFillBefore(LokasiKode);
		UPDATE Growth INNER JOIN TFillBefore ON Growth.LokasiKode = TFillBefore.LokasiKode SET  MBefore = Jumlah;

		INSERT INTO Growth
		SELECT  'Kabupaten' AS MyGroup, Kabupaten, Kabupaten AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth  FROM tdarea GROUP BY Kabupaten ORDER BY AreaStatus, Kabupaten;

		DROP TEMPORARY TABLE IF EXISTS TFillNow;
		CREATE TEMPORARY TABLE IF NOT EXISTS TFillNow AS
		SELECT tdarea.Kabupaten, IFNULL(Jumlah, 0) AS Jumlah
		FROM tdarea
		LEFT OUTER JOIN
			(SELECT  tdcustomer.CusKabupaten, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah
			 FROM tmotor
			 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
			 INNER JOIN ttdk ON tmotor.DKNo = ttDk.DKNo
			 INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
			 WHERE ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now
			 GROUP BY tdcustomer.CusKabupaten) Jum
		ON tdarea.Kabupaten = Jum.CusKabupaten
		GROUP BY tdarea.Kabupaten
		ORDER BY tdarea.AreaStatus, tdarea.Kabupaten;
		CREATE INDEX Kabupaten ON TFillNow(Kabupaten);
		UPDATE Growth INNER JOIN TFillNow ON Growth.LokasiKode = TFillNow.Kabupaten SET  MNow = Jumlah;

		DROP TEMPORARY TABLE IF EXISTS TFillBefore;
		CREATE TEMPORARY TABLE IF NOT EXISTS TFillBefore AS
		SELECT tdarea.Kabupaten, IFNULL(Jumlah, 0) AS Jumlah
		FROM tdarea
		LEFT OUTER JOIN
			(SELECT  tdcustomer.CusKabupaten, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah
			 FROM tmotor
			 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
			 INNER JOIN ttdk ON tmotor.DKNo = ttDk.DKNo
			 INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
			 WHERE ttsk.SKTgl BETWEEN Tgl1Before AND Tgl2Before
			 GROUP BY tdcustomer.CusKabupaten) Jum
		ON tdarea.Kabupaten = Jum.CusKabupaten
		GROUP BY tdarea.Kabupaten
		ORDER BY tdarea.AreaStatus, tdarea.Kabupaten;
		CREATE INDEX Kabupaten ON TFillBefore(Kabupaten);
		UPDATE Growth INNER JOIN TFillBefore ON Growth.LokasiKode = TFillBefore.Kabupaten SET  MBefore = Jumlah;

		UPDATE Growth SET Growth = ROUND((MNow -  MBefore) / MBefore *100,2) WHERE MBefore <> 0;
		UPDATE Growth SET Growth = 100 WHERE MBefore = 0;
		UPDATE Growth SET Growth = 0 WHERE MBefore = 0 AND MNow = 0;

		DELETE FROM Growth WHERE   MBefore = 0 AND MNow = 0 ;

		SET @MyQuery = CONCAT("SELECT * FROM Growth;");		
 	END IF;

	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$

DELIMITER ;


