DELIMITER $$
DROP PROCEDURE IF EXISTS `rBeliSuratJalanSupplier`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rBeliSuratJalanSupplier`(IN xTipe VARCHAR(20),IN xStatTgl VARCHAR(25),IN xTgl1 DATE,IN xTgl2 DATE,IN xSupplier VARCHAR(20),IN xLokasi VARCHAR(30),IN xNoFB VARCHAR(10),IN xSort VARCHAR(25),IN xOrder VARCHAR(10))
BEGIN
       SET xSupplier = CONCAT(xSupplier,"%");
       SET xLokasi = CONCAT(xLokasi,"%");
       IF xTipe = "ttss" THEN
          SET @MyQuery = "SELECT IFNULL(ttfb.FBTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS FBTgl, ttss.FBNo, ttss.LokasiKode, 
                          ttss.SSJam, ttss.SSMemo, ttss.SSNo, ttss.SSTgl, ttss.SupKode, ttss.UserID, 
                          IFNULL(tdlokasi.LokasiNama, '--') AS LokasiNama, IFNULL(tmotor.SSJum, 0) AS SSJum, 
                          IFNULL(tdsupplier.SupNama, '--') AS SupNama 
                          FROM ttss 
                          LEFT OUTER JOIN ttfb ON ttss.FBNo = ttfb.FBNo 
                          LEFT OUTER JOIN tdsupplier ON ttss.SupKode = tdsupplier.SupKode 
                          LEFT OUTER JOIN tdlokasi ON ttss.LokasiKode = tdlokasi.LokasiKode 
                          LEFT OUTER JOIN 
                          (SELECT COUNT(MotorType) AS SSJum, SSNo FROM tmotor tmotor_1 GROUP BY SSNo) tmotor ON ttss.SSNo = tmotor.SSNo 
                          WHERE ";
          IF xNoFB = "FB" THEN
             SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND (ttss.SupKode LIKE '",xSupplier,"') AND (ttss.LokasiKode LIKE '",xLokasi,"') AND (IFNULL(ttfb.FbNo,'--') <> '--') ORDER BY ",xSort," ",xOrder); 
          ELSE
             SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND (ttss.SupKode LIKE '",xSupplier,"') AND (ttss.LokasiKode LIKE '",xLokasi,"') AND (IFNULL(ttfb.FbNo,'--') LIKE '",xNoFB,"') ORDER BY ",xSort," ",xOrder); 
          END IF;
       END IF;

       IF xTipe = "tmotor" THEN
          SET @MyQuery = "SELECT BPKBNo, BPKBTglAmbil, BPKBTglJadi, DKNo, DealerKode, FBHarga, tmotor.FBNo, FakturAHMNo, 
                          FakturAHMTgl, FakturAHMTglAmbil, MotorAutoN, MotorMemo, MotorNoMesin, MotorNoRangka, MotorTahun, MotorType, 
                          MotorWarna, PDNo, PlatNo, PlatTgl, PlatTglAmbil, SDNo, SKNo, tmotor.SSNo, STNKAlamat, STNKNama, STNKNo, 
                          STNKTglAmbil, STNKTglBayar, STNKTglJadi, STNKTglMasuk 
                          FROM tmotor 
                          INNER JOIN ttss ON ttss.SSNo = tMotor.SSNo 
                          LEFT OUTER JOIN ttfb ON ttss.FBNo = ttfb.FBNo 
                          WHERE ";
          IF xNoFB = "FB" THEN
             SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND (ttss.SupKode LIKE '",xSupplier,"') AND (ttss.LokasiKode  LIKE '",xLokasi,"') AND (IFNULL(ttfb.FbNo,'--') <> '--') ORDER BY ",xSort," ",xOrder); 
          ELSE
             SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND (ttss.SupKode LIKE '",xSupplier,"') AND (ttss.LokasiKode  LIKE '",xLokasi,"') AND (IFNULL(ttfb.FbNo,'--') LIKE '",xNoFB,"') ORDER BY ",xSort," ",xOrder); 
          END IF;
       END IF;

       PREPARE STMT FROM @MyQuery;
       EXECUTE Stmt;
    
    END$$

DELIMITER ;

