DELIMITER $$
DROP PROCEDURE IF EXISTS `rData`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rData`(IN xTipe VARCHAR(20),IN xStatus VARCHAR(20) ,IN xTgl1 DATE,IN xTgl2 DATE,IN xSort VARCHAR(20),IN xOrder VARCHAR(20),IN xKode1 VARCHAR(20),IN xKode2 VARCHAR(20))
BEGIN
          /*select xTipe,xStatus,xTgl1,xTgl2,xSort,xOrder,xKode1,xKode2;
            Cara Akses SP :
            Call rdaLaporanData(xTipe,xStatus,xTgl1,xTgl2,xSort,xOrder,xKode1,xKode2)
            xTipe : tdmotortype,tdmotorharga,tdmotorWarna,tdlokasi,tdSupplier,tddealer,tdSales,tdCustomer,tdleasing,tdprogram,tdarea,tdbbn,
                    tuser,tuakses
                    
            xStatus : '' -> Semua
                      'A' -> Aktif
                      'N' -> Non Aktif
            
            xTgl1 : '0000-00-00' -> Kalau Kosong
            
            xTgl2 : '0000-00-00' -> Kalau Kosong
            
            xSort : Field Sort
            
            xOrder : ASC & DESC
            
            xKode1 : Kode Awal
            
            xKode2 : Kode Akhir
             
          */
	  SET xStatus = CONCAT(xStatus,'%');
	  
          /******** TYPE MOTOR ********/
          IF xTipe = 'tdmotortype' THEN
             IF xKode1 = '' OR xKode2 = '' THEN
                IF xSort='MotorType' THEN
                   IF xOrder = 'ASC' THEN
                      SELECT * FROM tdmotortype WHERE (TypeStatus LIKE xStatus) ORDER BY MotorType;
                   ELSE
                      SELECT * FROM tdmotortype WHERE (TypeStatus LIKE xStatus) ORDER BY MotorType DESC;
                   END IF;
                END IF;
                IF xSort='TypeStatus' THEN
                   IF xOrder = 'ASC' THEN
                      SELECT * FROM tdmotortype WHERE (TypeStatus LIKE xStatus) ORDER BY TypeStatus;
                   ELSE
                      SELECT * FROM tdmotortype WHERE (TypeStatus LIKE xStatus) ORDER BY TypeStatus DESC;
                   END IF;
                END IF;
             ELSE
                IF xSort='MotorType' THEN
                   IF xOrder = 'ASC' THEN
                      SELECT * FROM tdmotortype WHERE (MotorType BETWEEN xKode1 AND xKode2) AND (TypeStatus LIKE xStatus) ORDER BY MotorType;
                   ELSE
                      SELECT * FROM tdmotortype WHERE (MotorType BETWEEN xKode1 AND xKode2) AND (TypeStatus LIKE xStatus) ORDER BY MotorType DESC;
                   END IF;
                END IF;
                IF xSort='TypeStatus' THEN
                   IF xOrder = 'ASC' THEN
                      SELECT * FROM tdmotortype WHERE (MotorType BETWEEN xKode1 AND xKode2) AND (TypeStatus LIKE xStatus) ORDER BY TypeStatus;
                   ELSE
                      SELECT * FROM tdmotortype WHERE (MotorType BETWEEN xKode1 AND xKode2) AND (TypeStatus LIKE xStatus) ORDER BY TypeStatus DESC;
                   END IF;
                END IF;
             END IF;
          END IF;
          
          /******** HARGA MOTOR ********/
          IF xTipe = 'tdmotorharga' THEN
             IF xKode1 = '' OR xKode2 = '' THEN
                IF xSort='MotorType' THEN
                   IF xOrder = 'ASC' THEN
                      SELECT * FROM tdmotortype WHERE (TypeStatus LIKE xStatus) ORDER BY MotorType;
                   ELSE
                      SELECT * FROM tdmotortype WHERE (TypeStatus LIKE xStatus) ORDER BY MotorType DESC;
                   END IF;
                END IF;
                IF xSort='TypeStatus' THEN
                   IF xOrder = 'ASC' THEN
                      SELECT * FROM tdmotortype WHERE (TypeStatus LIKE xStatus) ORDER BY TypeStatus;
                   ELSE
                      SELECT * FROM tdmotortype WHERE (TypeStatus LIKE xStatus) ORDER BY TypeStatus DESC;
                   END IF;
                END IF;
             ELSE
                IF xSort='MotorType' THEN
                   IF xOrder = 'ASC' THEN
                      SELECT * FROM tdmotortype WHERE (MotorType BETWEEN xKode1 AND xKode2) AND (TypeStatus LIKE xStatus) ORDER BY MotorType;
                   ELSE
                      SELECT * FROM tdmotortype WHERE (MotorType BETWEEN xKode1 AND xKode2) AND (TypeStatus LIKE xStatus) ORDER BY MotorType DESC;
                   END IF;
                END IF;
                IF xSort='TypeStatus' THEN
                   IF xOrder = 'ASC' THEN
                      SELECT * FROM tdmotortype WHERE (MotorType BETWEEN xKode1 AND xKode2) AND (TypeStatus LIKE xStatus) ORDER BY TypeStatus;
                   ELSE
                      SELECT * FROM tdmotortype WHERE (MotorType BETWEEN xKode1 AND xKode2) AND (TypeStatus LIKE xStatus) ORDER BY TypeStatus DESC;
                   END IF;
                END IF;
             END IF;
          END IF;

          /******** WARNA MOTOR ********/
          IF xTipe = 'tdmotorWarna' THEN
             IF xKode1 = '' OR xKode2 = '' THEN
                IF xOrder = 'ASC' THEN
                   SELECT tdmotorwarna.MotorWarna, tdmotorwarna.MotorType, tdmotortype.MotorNama
                       FROM tdmotorwarna INNER JOIN tdmotortype ON tdmotorwarna.MotorType = tdmotortype.MotorType ORDER BY tdmotorwarna.MotorWarna;
                ELSE
                   SELECT tdmotorwarna.MotorWarna, tdmotorwarna.MotorType, tdmotortype.MotorNama
                       FROM tdmotorwarna INNER JOIN tdmotortype ON tdmotorwarna.MotorType = tdmotortype.MotorType ORDER BY tdmotorwarna.MotorWarna DESC;
                END IF;
             ELSE
                IF xOrder = 'ASC' THEN
                   SELECT tdmotorwarna.MotorWarna, tdmotorwarna.MotorType, tdmotortype.MotorNama
                      FROM tdmotorwarna INNER JOIN tdmotortype ON tdmotorwarna.MotorType = tdmotortype.MotorType
                      WHERE MotorWarna BETWEEN xKode1 AND xKode2 ORDER BY tdmotorwarna.MotorWarna;
                ELSE
                   SELECT tdmotorwarna.MotorWarna, tdmotorwarna.MotorType, tdmotortype.MotorNama
                      FROM tdmotorwarna INNER JOIN tdmotortype ON tdmotorwarna.MotorType = tdmotortype.MotorType
                      WHERE MotorWarna BETWEEN xKode1 AND xKode2 ORDER BY tdmotorwarna.MotorWarna DESC;
                END IF;
             END IF;
          END IF;

          /******** WAREHOUSE ********/
          IF xTipe = 'tdlokasi' THEN
             IF xKode1 = '' OR xKode2 = '' THEN
                IF xOrder = 'ASC' THEN
                   SELECT tdlokasi.LokasiKode, tdlokasi.LokasiNama, tdlokasi.LokasiStatus, tdlokasi.LokasiPetugas, tdlokasi.LokasiAlamat, tdsales.SalesNama
                      FROM tdlokasi LEFT OUTER JOIN tdsales ON tdlokasi.LokasiPetugas = tdsales.SalesKode
                      WHERE (LokasiStatus LIKE xStatus )
                       ORDER BY LokasiKode;
                ELSE
                   SELECT tdlokasi.LokasiKode, tdlokasi.LokasiNama, tdlokasi.LokasiStatus, tdlokasi.LokasiPetugas, tdlokasi.LokasiAlamat, tdsales.SalesNama
                      FROM tdlokasi LEFT OUTER JOIN tdsales ON tdlokasi.LokasiPetugas = tdsales.SalesKode
                      WHERE (LokasiStatus LIKE xStatus )
                       ORDER BY LokasiKode DESC;
                END IF;
             ELSE
                IF xOrder = 'ASC' THEN
                   SELECT tdlokasi.LokasiKode, tdlokasi.LokasiNama, tdlokasi.LokasiStatus, tdlokasi.LokasiPetugas, tdlokasi.LokasiAlamat, tdsales.SalesNama
                      FROM tdlokasi LEFT OUTER JOIN tdsales ON tdlokasi.LokasiPetugas = tdsales.SalesKode
                      WHERE (LokasiStatus LIKE xStatus) AND (LokasiKode BETWEEN xKode1 AND xKode2)
                       ORDER BY LokasiKode;
                ELSE
                   SELECT tdlokasi.LokasiKode, tdlokasi.LokasiNama, tdlokasi.LokasiStatus, tdlokasi.LokasiPetugas, tdlokasi.LokasiAlamat, tdsales.SalesNama
                      FROM tdlokasi LEFT OUTER JOIN tdsales ON tdlokasi.LokasiPetugas = tdsales.SalesKode
                      WHERE (LokasiStatus LIKE xStatus) AND (LokasiKode BETWEEN xKode1 AND xKode2)
                       ORDER BY LokasiKode DESC;
                END IF;
             END IF;
          END IF;

          /******** SUPPLIER ********/
          IF xTipe = 'tdSupplier' THEN
             IF xKode1 = '' OR xKode2 = '' THEN
                IF xOrder = 'ASC' THEN
                   SELECT * FROM tdSupplier WHERE (SupStatus LIKE xStatus)
                        ORDER BY SupKode;
                ELSE
                   SELECT * FROM tdSupplier WHERE (SupStatus LIKE xStatus)
                        ORDER BY SupKode DESC;
                END IF;
             ELSE
                IF xOrder = 'ASC' THEN
                   SELECT * FROM tdSupplier WHERE (SupStatus LIKE xStatus) AND (SupKode BETWEEN xKode1 AND xKode2)
                        ORDER BY SupKode;
                ELSE
                   SELECT * FROM tdSupplier WHERE (SupStatus LIKE xStatus) AND (SupKode BETWEEN xKode1 AND xKode2)
                        ORDER BY SupKode DESC;
                END IF;
             END IF;
          END IF;

          /******** DEALER ********/
          IF xTipe = 'tddealer' THEN
             IF xKode1 = '' OR xKode2 = '' THEN
                IF xOrder = 'ASC' THEN
                   SELECT * FROM tdDealer WHERE (DealerStatus LIKE xStatus)
                        ORDER BY DealerKode;
                ELSE
                   SELECT * FROM tdDealer WHERE (DealerStatus LIKE xStatus)
                        ORDER BY DealerKode DESC;
                END IF;
             ELSE
                IF xOrder = 'ASC' THEN
                   SELECT * FROM tdDealer WHERE (DealerStatus LIKE xStatus) AND (DealerKode BETWEEN xKode1 AND xKode1)
                        ORDER BY DealerKode;
                ELSE
                   SELECT * FROM tdDealer WHERE (DealerStatus LIKE xStatus) AND (DealerKode BETWEEN xKode1 AND xKode1)
                        ORDER BY DealerKode DESC;
                END IF;
             END IF;
          END IF;

          /******** SALES ********/
          IF xTipe = 'tdSales' THEN
             IF xKode1 = '' OR xKode2 = '' THEN
                IF xOrder = 'ASC' THEN
                   SELECT * FROM tdSales WHERE (SalesStatus LIKE xStatus)
                        ORDER BY SalesKode;
                ELSE
                   SELECT * FROM tdSales WHERE (SalesStatus LIKE xStatus)
                        ORDER BY SalesKode DESC;
                END IF;
             ELSE
                IF xOrder = 'ASC' THEN
                   SELECT * FROM tdSales WHERE (SalesStatus LIKE xStatus) AND (SalesKode BETWEEN xKode1 AND xKode2)
                        ORDER BY SalesKode;
                ELSE
                   SELECT * FROM tdSales WHERE (SalesStatus LIKE xStatus) AND (SalesKode BETWEEN xKode1 AND xKode2)
                        ORDER BY SalesKode DESC;
                END IF;
             END IF;
          END IF;

          /******** KONSUMEN ********/
          IF xTipe = 'tdCustomer' THEN
             IF xKode1 = '' OR xKode2 = '' THEN
                IF xOrder = 'ASC' THEN
                   SELECT * FROM tdCustomer ORDER BY CusKode;
                ELSE
                   SELECT * FROM tdCustomer ORDER BY CusKode DESC;
                END IF;
             ELSE
                IF xOrder = 'ASC' THEN
                   SELECT * FROM tdCustomer WHERE (CusKode BETWEEN xKode1 AND xKode2) ORDER BY CusKode;
                ELSE
                   SELECT * FROM tdCustomer WHERE (CusKode BETWEEN xKode1 AND xKode2) ORDER BY CusKode DESC;
                END IF;
             END IF;
          END IF;

          /******** LEASING ********/
          IF xTipe = 'tdleasing' THEN
             IF xKode1 = '' OR xKode2 = '' THEN
                IF xOrder = 'ASC' THEN
                   SELECT * FROM tdLeasing WHERE (LeaseStatus LIKE xStatus) ORDER BY LeaseKode;
                ELSE
                   SELECT * FROM tdLeasing WHERE (LeaseStatus LIKE xStatus) ORDER BY LeaseKode DESC;
                END IF;
             ELSE
                IF xOrder = 'ASC' THEN
                   SELECT * FROM tdLeasing WHERE (LeaseStatus LIKE xStatus) AND (LeaseKode BETWEEN xKode1 AND xKode2) ORDER BY LeaseKode;
                ELSE
                   SELECT * FROM tdLeasing WHERE (LeaseStatus LIKE xStatus) AND (LeaseKode BETWEEN xKode1 AND xKode2) ORDER BY LeaseKode DESC;
                END IF;
             END IF;
          END IF;

          /******** PROGRAM ********/
          IF xTipe = 'tdprogram' THEN
             IF xKode1 = '' OR xKode2 = '' THEN
                IF xOrder = 'ASC' THEN
                   SELECT * FROM tdProgram WHERE (PrgTglAwal <= xTgl1) AND (PrgTglAkhir >= xTgl2) ORDER BY ProgramNama;
                ELSE
                   SELECT * FROM tdProgram WHERE (PrgTglAwal <= xTgl1) AND (PrgTglAkhir >= xTgl2) ORDER BY ProgramNama DESC;
                END IF;
             ELSE
                IF xOrder = 'ASC' THEN
                   SELECT * FROM tdProgram WHERE (ProgramNama BETWEEN xKode1 AND xKode2) AND (PrgTglAwal <= xTgl1) AND (PrgTglAkhir >= xTgl2) ORDER BY ProgramNama;
                ELSE
                   SELECT * FROM tdProgram WHERE (ProgramNama BETWEEN xKode1 AND xKode2) AND (PrgTglAwal <= xTgl1) AND (PrgTglAkhir >= xTgl2) ORDER BY ProgramNama DESC;
                END IF;
             END IF;
          END IF;

          /******** AREA ********/
          IF xTipe = 'tdarea' THEN
             IF xKode1 = '' OR xKode2 = '' THEN
                IF xOrder = 'ASC' THEN
                   SELECT * FROM tdArea WHERE (AreaStatus LIKE xStatus) ORDER BY Kabupaten;
                ELSE
                   SELECT * FROM tdArea WHERE (AreaStatus LIKE xStatus) ORDER BY Kabupaten DESC;
                END IF;
             ELSE
                IF xOrder = 'ASC' THEN
                   SELECT * FROM tdArea WHERE (AreaStatus LIKE xStatus) AND (kabupaten BETWEEN xKode1 AND xKode2) ORDER BY Kabupaten;
                ELSE
                   SELECT * FROM tdArea WHERE (AreaStatus LIKE xStatus) AND (kabupaten BETWEEN xKode1 AND xKode2) ORDER BY Kabupaten DESC;
                END IF;
             END IF;
          END IF;

          /******** BBN ********/
          IF xTipe = 'tdbbn' THEN
             IF xKode1 = '' OR xKode2 = '' THEN
                IF xOrder = 'ASC' THEN
                   SELECT Kabupaten, tdbbn.MotorType, BBNBiaya, MotorNama FROM tdbbn INNER JOIN tdmotortype ON tdbbn.MotorType = tdmotortype.MotorType ORDER BY Kabupaten;
                ELSE
                   SELECT * FROM tdArea WHERE (AreaStatus LIKE xStatus) ORDER BY Kabupaten DESC;
                END IF;
             ELSE
                IF xOrder = 'ASC' THEN
                   SELECT * FROM tdArea WHERE (AreaStatus LIKE xStatus) AND (kabupaten BETWEEN xKode1 AND xKode2) ORDER BY Kabupaten;
                ELSE
                   SELECT * FROM tdArea WHERE (AreaStatus LIKE xStatus) AND (kabupaten BETWEEN xKode1 AND xKode2) ORDER BY Kabupaten DESC;
                END IF;
             END IF;
          END IF;

          /******** USER ********/
          IF xTipe = 'tuser' THEN
             IF xKode1 = '' OR xKode2 = '' THEN
                IF xOrder = 'ASC' THEN
                   SELECT * FROM tuser ORDER BY UserId;
                ELSE
                   SELECT * FROM tuser ORDER BY UserId DESC;
                END IF;
             ELSE
                IF xOrder = 'ASC' THEN
                   SELECT * FROM tuser WHERE (UserId BETWEEN xKode1 AND xKode2) ORDER BY UserId;
                ELSE
                   SELECT * FROM tuser WHERE (UserId BETWEEN xKode1 AND xKode2) ORDER BY UserId DESC;
                END IF;
             END IF;
          END IF;

          /******** HAK AKSES ********/
          IF xTipe = 'tuakses' THEN
             SELECT * FROM tuakses WHERE (KodeAkses = xKode1) ORDER BY KodeAkses;
          END IF;

	END$$

DELIMITER ;

