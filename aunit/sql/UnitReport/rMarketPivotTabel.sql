DELIMITER $$
DROP PROCEDURE IF EXISTS `rMarketPivotTabel`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rMarketPivotTabel`(IN xTipe VARCHAR(25),IN xStatTgl VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,IN xJenis VARCHAR(20),IN xDelNol VARCHAR(1),IN xSales VARCHAR(20),IN xLokasi VARCHAR(30))
BEGIN
/*
       xTipe    : "tab1" -> Kecamatan IN
                  "tab2" -> Kecamatan OUT
                  "tab3" -> POS
                  "tab4" -> Sales
                  "tab5" -> Segmen
                  "tab6" -> Area
                  "tab7" -> Tunai-Kredit
                  "tab8" -> Tenor
                  "tab9" -> Uang Muka
                  
       Cara Akses :
       
       CALL rMarketPivotTabel('Tab1','ttdk.DKTgl','2000-01-01','2020-04-02','%','','%','%');

*/
       DECLARE xResultJenis VARCHAR(100);
       DECLARE xResultStatTgl VARCHAR(100);
       
       SET xResultJenis = "";
       SET xResultStatTgl = "";
       
       /* SelectJenisJual */
       IF xJenis = "%" THEN
          SET xResultJenis = " AND ttdk.LeaseKode LIKE '%' ";
       ELSE
          IF xJenis = "Sudah" THEN
             SET xResultJenis = " AND (ttdk.LeaseKode = 'Tunai' OR ttdk.LeaseKode = 'KDS') ";
          ELSE
             SET xResultJenis = " AND (ttdk.LeaseKode <> 'Tunai' OR ttdk.LeaseKode <> 'KDS') ";
          END IF;
       END IF;
       
       /* SelectDK_SK */
       IF xStatTgl = "ttdk.DKTgl" THEN
          SET xResultStatTgl = " LEFT OUTER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo ";
       ELSE
          SET xResultStatTgl = " INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo ";
       END IF;

       /* Kecamatan IN */
       IF xTipe = "tab1" THEN 
          SELECT  Kabupaten INTO @xKab FROM tdarea WHERE AreaStatus = '1' LIMIT 1;
          SET @MyQuery = CONCAT("SELECT tdmotortype.MotorType, tdmotortype.MotorNama, tdmotortype.TypeStatus, IFNULL(StockMotor.Jumlah, 0) AS Jumlah, 
                         IFNULL(StockMotor.FBHarga, 0) AS FBHarga, IFNULL(StockMotor.CusKecamatan,'') As Daerah 
                         FROM tdmotortype 
                         INNER JOIN 
                         (SELECT tmotor.MotorType, COUNT(tmotor.MotorType) AS Jumlah, SUM(tmotor.FBHarga) AS FBHarga, tdcustomer.CusKecamatan 
                         FROM tmotor 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
                         ",xResultStatTgl," 
                         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                         WHERE ",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
                         AND ttdk.SalesKode Like '",xSales,"' ",xResultJenis," 
                         AND tdcustomer.CusKabupaten = '",@xKab,"' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"'  
                         GROUP BY tmotor.MotorType, tdcustomer.CusKecamatan) StockMotor 
                         ON tdmotortype.MotorType = StockMotor.MotorType 
                         ORDER BY tdmotortype.MotorType"); 
       END IF;

       /* Kecamatan OUT */
       IF xTipe = "tab2" THEN 
          SELECT  Kabupaten INTO @xKab FROM tdarea WHERE AreaStatus = '1' LIMIT 1;
          SET @MyQuery = CONCAT("SELECT tdmotortype.MotorType, tdmotortype.MotorNama, tdmotortype.TypeStatus, IFNULL(StockMotor.Jumlah, 0) AS Jumlah, 
                         IFNULL(StockMotor.FBHarga, 0) AS FBHarga, IFNULL(StockMotor.CusKecamatan,'') As Daerah 
                         FROM tdmotortype 
                         INNER JOIN 
                         (SELECT tmotor.MotorType, COUNT(tmotor.MotorType) AS Jumlah, SUM(tmotor.FBHarga) AS FBHarga, tdcustomer.CusKecamatan 
                         FROM tmotor 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         ",xResultStatTgl," 
                         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                         WHERE ",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
                         AND ttdk.SalesKode Like '",xSales,"' ",xResultJenis," 
                         AND tdcustomer.CusKabupaten <> '",@xKab,"' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"' 
                         GROUP BY tmotor.MotorType, tdcustomer.CusKecamatan) StockMotor 
                         ON tdmotortype.MotorType = StockMotor.MotorType 
                         ORDER BY tdmotortype.MotorType");
       END IF;

       /* POS */
       IF xTipe = "tab3" THEN 
          SET @MyQuery = CONCAT("SELECT tdmotortype.MotorType, tdmotortype.MotorNama, tdmotortype.TypeStatus, IFNULL(StockMotor.Jumlah, 0) AS Jumlah, 
                         IFNULL(StockMotor.FBHarga, 0) AS FBHarga, IFNULL(StockMotor.LokasiKode,'Belum ada SK') As Daerah 
                         FROM tdmotortype 
                         LEFT OUTER JOIN 
                         (SELECT tmotor.MotorType, COUNT(tmotor.MotorType) AS Jumlah, SUM(tmotor.FBHarga) AS FBHarga, ttsk.LokasiKode 
                         FROM tmotor 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         ",xResultStatTgl," 
                         WHERE ",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
                         AND ttdk.SalesKode Like '",xSales,"' ",xResultJenis,"
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"' 
                         GROUP BY tmotor.MotorType, ttsk.LokasiKode) StockMotor 
                         ON tdmotortype.MotorType = StockMotor.MotorType 
                         ORDER BY tdmotortype.MotorType");
       END IF;

       /* SALES */
       IF xTipe = "tab4" THEN 
          SET @MyQuery = CONCAT("SELECT tdmotortype.MotorType, tdmotortype.MotorNama, tdmotortype.TypeStatus, IFNULL(StockMotor.Jumlah, 0) AS Jumlah, 
                         IFNULL(StockMotor.FBHarga, 0) AS FBHarga, IFNULL(tdsales.SalesNama,'--') As Daerah 
                         FROM tdmotortype 
                         LEFT OUTER JOIN 
                         (SELECT tmotor.MotorType, COUNT(tmotor.MotorType) AS Jumlah, SUM(tmotor.FBHarga) AS FBHarga, ttdk.SalesKode 
                         FROM tmotor 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         ",xResultStatTgl," 
                         WHERE ",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
                         AND ttdk.SalesKode Like '",xSales,"' ",xResultJenis," 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"' 
                         GROUP BY tmotor.MotorType, ttdk.SalesKode) StockMotor 
                         ON tdmotortype.MotorType = StockMotor.MotorType 
                         INNER JOIN  tdsales ON StockMotor.SalesKode = tdsales.SalesKode 
                         ORDER BY tdmotortype.MotorType");
       END IF;

       /* SEGMEN */
       IF xTipe = "tab5" THEN 
          SET @MyQuery = CONCAT("SELECT tdmotortype.MotorKategori AS MotorType, tdmotortype.MotorKategori As Daerah, 
                         COUNT(tmotor.MotorNoMesin) AS Jumlah 
                         FROM tmotor 
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
                         ",xResultStatTgl," 
                         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                         WHERE ",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
                         AND ttdk.SalesKode Like '",xSales,"' ",xResultJenis," 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"' 
                         GROUP BY LEFT(tdmotortype.MotorKategori,3), RIGHT(tdmotortype.MotorKategori,3) ");
       END IF;

       /* AREA */
       IF xTipe = "tab6" THEN 
          SET @MyQuery = CONCAT("SELECT tdmotortype.MotorKategori AS MotorType, tdcustomer.CusKecamatan As Daerah, 
                         COUNT(tmotor.MotorType) AS Jumlah 
                         FROM tmotor 
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
                         ",xResultStatTgl," 
                         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                         WHERE ",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
                         AND ttdk.SalesKode Like '",xSales,"'",xResultJenis," 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"' 
                         GROUP BY LEFT(tdmotortype.MotorKategori,3), tdcustomer.CusKecamatan ");
       END IF;

       /* TUNAI-KREDIT */
       IF xTipe = "tab7" THEN 
          SET @MyQuery = CONCAT("SELECT tdmotortype.MotorKategori AS MotorType, ttdk.DKJenis As Daerah, 
                         COUNT(tmotor.MotorType) AS Jumlah 
                         FROM tmotor 
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
                         ",xResultStatTgl," 
                         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                         WHERE ",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
                         AND ttdk.SalesKode Like '",xSales,"'",xResultJenis," 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"' 
                         GROUP BY LEFT(tdmotortype.MotorKategori,3), ttdk.DKJenis ");
       END IF;

       /* TENOR */
       IF xTipe = "tab8" THEN 
          SET @MyQuery = CONCAT("SELECT  fJangkaWaktu(ttdk.DKTenor) As MotorType, ttdk.LeaseKode As Daerah, 
                         COUNT(tmotor.MotorNoMesin) AS Jumlah 
                         FROM tmotor 
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
                         ",xResultStatTgl," 
                         LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode 
                         WHERE ",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"'",xResultJenis," 
                         AND ttdk.SalesKode Like '",xSales,"' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"' 
                         GROUP BY fJangkaWaktu(ttdk.DKTenor), ttdk.LeaseKode");
       END IF;

       /* UANG MUKA */
       IF xTipe = "tab9" THEN 
          SET @MyQuery = CONCAT("SELECT  fUangMuka(ttdk.DKDPTotal) As MotorType, ttdk.LeaseKode As Daerah, 
                         COUNT(tmotor.MotorNoMesin) AS Jumlah 
                         FROM tmotor 
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode
                         ",xResultStatTgl," 
                         WHERE ",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"'",xResultJenis," 
                         AND ttdk.SalesKode Like '",xSales,"' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"' 
                         GROUP BY fUangMuka(ttdk.DKDPTotal), ttdk.LeaseKode");
       END IF;

       PREPARE STMT FROM @MyQuery;
       EXECUTE Stmt;
    
    END$$

DELIMITER ;

