DELIMITER $$
DROP PROCEDURE IF EXISTS `rJualSuratJalanKonsumen`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rJualSuratJalanKonsumen`(IN xTipe VARCHAR(25),IN xStatTgl VARCHAR(25),IN xTgl1 DATE,IN xTgl2 DATE,IN xKab VARCHAR(25),IN xSales VARCHAR(20),IN xCetak VARCHAR(10),IN xNoPO VARCHAR(10),IN xLokasi VARCHAR(25),IN xNoRK VARCHAR(10),IN xLunas VARCHAR(10),IN xTypeMTR VARCHAR(20),IN xLeasing VARCHAR(20),IN xSort VARCHAR(25),IN xOrder VARCHAR(10))
BEGIN
/*
       xTipe    : "ttsk1" -> Surat Jalan Konsumen (SK 1 - 5)
                  "ttsk2" -> Surat Jalan Konsumen BBN
                  "ttsk3" -> Surat Jalan Konsumen Pemohon
                  "ttsk4" -> SK-RK
                  "ttsk5" -> CekList
                  "ttskTeam1" -> SK Piutang MD
                  "ttskTeam2" -> SK DP Net
                  "ttskTeam3" -> SK DP Net Saldo
                  "ttdkStatus1" -> AR Tagih Leasing & Lead Team Leasing
                  "ttdkStatus2" -> Tagihan Ke Leasing
                  "SelisihOTR" -> Selisih OTR
                  
       xStatTgl : "ttsk.SKTgl"
                  "ttdk.DKTgl"
       
       xSort    : "ttsk.SKNo"
                  "ttsk.SKTgl"
                  "ttdk.DKTgl"
                  "tdcustomer.CusNama"
                  "ttdk.CusKode"
                  "ttdk.SalesKode"
                  "ttdk.LeaseKode"
                  "ttsk.SKMemo"
                  "ttsk.LokasiKode"
                  "ttsk.UserID"
                  "IFNULL(ttgeneralledgerhd.GLLink,'--')"
                  "IFNULL(ttdk.DKHarga,0)"
                  "IFNULL(ttdk.DKHPP,0)"
                  "IFNULL(ttdk.DKDPTotal,0)"
                  "IFNULL(ttdk.DKDPLLeasing,0)"
                  "IFNULL(ttdk.DKDPTerima,0)"
                  "IFNULL(ttdk.DKNetto,0)"
                  "IFNULL(ttdk.ProgramSubsidi,0)"
                  "IFNULL(ttdk.PrgSubsSupplier,0)"
                  "IFNULL(ttdk.PrgSubsDealer,0)"
                  "IFNULL(ttdk.PrgSubsFincoy,0)"
                  "IFNULL(ttdk.PotonganHarga,0)"
                  "IFNULL(ttdk.ReturHarga,0)"
                  "IFNULL(ttdk.PotonganKhusus,0)"
                  "IFNULL(ttdk.BBN,0)"
                  "IFNULL(ttdk.Jaket,0)"

       xNoRK    : "RK"
                  "--"
                  ""
       xLunas   : "Sudah"
                  "Belum"
                  ""
       xNoPO    : "0%"
                  "--"
                  ""
       Cara Akses :
       
       *** Surat Jalan Konsumen/Surat Jalan Konsumen BBN/Pemohon/SK-RK/CekList ***
       CALL rJualSuratJalanKonsumen('ttsk1','ttsk.SKTgl','2001-01-01','2019-12-31','','','','','','','','','','ttsk.SKNo','ASC');

*/

          DECLARE done INT DEFAULT 0;
          DECLARE xAccount VARCHAR(10);
          DECLARE cur1 CURSOR FOR SELECT NoAccount FROM traccount WHERE NoParent = '11080000';
          DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

       SET xKab = CONCAT(xKab,"%");
       SET xSales = CONCAT(xSales,"%");
       SET xCetak = CONCAT(xCetak,"%");
       SET xNoPO = CONCAT(xNoPO,"%");
       SET xLokasi = CONCAT(xLokasi,"%");
       SET xNoRK = CONCAT(xNoRK,"%");
       SET xLunas = CONCAT(xLunas,"%");
       SET xTypeMtr = CONCAT(xTypeMtr,"%");
       SET xLeasing = CONCAT(xLeasing,"%");
       
       /* SURAT JALAN KONSUMEN */
       IF xTipe = "ttsk1" THEN 
          SET @MyQuery = "SELECT ttsk.SKNo, ttsk.SKTgl, ttsk.LokasiKode, ttsk.SKMemo, ttsk.UserID, ttdk.CusKode, 
                          tdcustomer.CusNama, tdcustomer.CusAlamat, LPAD(tdcustomer.CusRT,3,'0') AS CusRT, LPAD(tdcustomer.CusRW,3,'0')AS CusRW, tdcustomer.CusKelurahan, 
                          tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, tdcustomer.CusTelepon, tmotor.MotorType, 
                          tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, ttdk.DKNo, 
                          ttdk.DKHarga, tdlokasi.LokasiNama, ttsk.SKJam, tmotor.FBHarga, ttdk.DKTgl, ttdk.LeaseKode 
                          FROM ttsk LEFT OUTER JOIN 
                          tdlokasi ON ttsk.LokasiKode = tdlokasi.LokasiKode LEFT OUTER JOIN 
                          tmotor ON ttsk.SKNo = tmotor.SKNo LEFT OUTER JOIN 
                          ttdk ON tmotor.DKNo = ttdk.DKNo LEFT OUTER JOIN 
                          tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                          WHERE  ";
                          
          SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (ttsk.LokasiKode LIKE '",xLokasi,"') AND 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND 
                         (IFNULL(tmotor.RKNo, '--') LIKE '",xNoRK,"') AND (ttsk.SKCetak LIKE '",xCetak,"') AND 
                         (ttdk.DKPONo LIKE '",xNoPO,"') AND 
                         (ttdk.LeaseKode LIKE '",xLeasing,"') ORDER BY ",xSort," ",xOrder); 
           
           IF xLeasing = "KREDIT" THEN
              SELECT REPLACE(@MyQuery,CONCAT("(ttdk.LeaseKode LIKE '",xLeasing,"')"),"(ttdk.LeaseKode <> 'TUNAI')") INTO @MyQuery;
           END IF;
       END IF;

       /* SURAT JALAN KONSUMEN BBN*/
       IF xTipe = "ttsk2" THEN 
          SET @MyQuery = "SELECT ttsk.SKNo, ttsk.SKTgl AS DKTgl, ttsk.LokasiKode, ttsk.SKMemo, ttsk.UserID, ttdk.CusKode, 
                          tdcustomer.CusNama, tdcustomer.CusAlamat, LPAD(tdcustomer.CusRT,3,'0') AS CusRT, LPAD(tdcustomer.CusRW,3,'0')AS CusRW, tdcustomer.CusKelurahan, 
                          tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, tdcustomer.CusTelepon, tmotor.MotorType, 
                          tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, ttdk.DKNo, 
                          ttdk.DKHarga, tdlokasi.LokasiNama, ttsk.SKJam, tmotor.FBHarga, ttdk.DKTgl, 
                          ttdk.DKDPTotal, ttdk.DKDPLLeasing, ttdk.DKDPTerima, ttdk.DKNetto, ttdk.ProgramNama, ttdk.ProgramSubsidi, ttdk.BBN, ttdk.Jaket, ttdk.PrgSubsSupplier, 
                          ttdk.PrgSubsDealer, ttdk.PotonganHarga, ttdk.ReturHarga, ttdk.PotonganKhusus, ttdk.NamaHadiah, IFNULL(tmotor.MotorNoMesin, '--') AS MotorNoMesin 
                          FROM ttsk LEFT OUTER JOIN 
                          tdlokasi ON ttsk.LokasiKode = tdlokasi.LokasiKode LEFT OUTER JOIN 
                          tmotor ON ttsk.SKNo = tmotor.SKNo LEFT OUTER JOIN 
                          ttdk ON tmotor.DKNo = ttdk.DKNo LEFT OUTER JOIN 
                          tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                          WHERE ";
                          
          SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (ttsk.LokasiKode LIKE '",xLokasi,"') AND 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND 
                         (IFNULL(tmotor.RKNo, '--') LIKE '",xNoRK,"') AND (ttsk.SKCetak LIKE '",xCetak,"') AND 
                         (ttdk.DKPONo LIKE '",xNoPO,"') AND 
                         (ttdk.LeaseKode LIKE '",xLeasing,"') ORDER BY ",xSort," ",xOrder); 
           
           IF xLeasing = "KREDIT" THEN
              SELECT REPLACE(@MyQuery,CONCAT("(ttdk.LeaseKode LIKE '",xLeasing,"')"),"(ttdk.LeaseKode <> 'TUNAI')") INTO @MyQuery;
           END IF;
       END IF;
       
       /* SURAT JALAN KONSUMEN PEMOHON*/
       IF xTipe = "ttsk3" THEN 
          SET @MyQuery = "SELECT ttsk.SKNo, ttsk.SKTgl, tdcustomer.CusNama, tdcustomer.CusPembeliKTP, tdcustomer.CusPembeliNama, tdcustomer.CusPembeliAlamat, ttdk.LeaseKode 
                          FROM tmotor 
                          INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                          INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo 
                          INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode WHERE ";
                          
          SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (ttsk.LokasiKode LIKE '",xLokasi,"') AND 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND 
                         (IFNULL(tmotor.RKNo, '--') LIKE '",xNoRK,"') AND (ttsk.SKCetak LIKE '",xCetak,"') AND 
                         (ttdk.DKPONo LIKE '",xNoPO,"') AND 
                         (ttdk.LeaseKode LIKE '",xLeasing,"') ORDER BY ",xSort," ",xOrder); 
           
           IF xLeasing = "KREDIT" THEN
              SELECT REPLACE(@MyQuery,CONCAT("(ttdk.LeaseKode LIKE '",xLeasing,"')"),"(ttdk.LeaseKode <> 'TUNAI')") INTO @MyQuery;
           END IF;
       END IF;

       /* SKRK */
       IF xTipe = "ttsk4" THEN 
          SET @MyQuery = "SELECT SKNo,MotorType,MotorTahun,MotorNoMesin,MotorNoRangka,CusNama,DKHarga FROM (
                          SELECT ttsk.SKNo, ttsk.SKTgl, ttsk.LokasiKode, ttsk.SKMemo, ttsk.UserID, ttdk.CusKode, 
                          tdcustomer.CusNama, tdcustomer.CusAlamat, LPAD(tdcustomer.CusRT,3,'0') AS CusRT, LPAD(tdcustomer.CusRW,3,'0')AS CusRW, tdcustomer.CusKelurahan,
                          tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, tdcustomer.CusTelepon, tmotor.MotorType, 
                          tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, ttdk.DKNo, 
                          ttdk.DKHarga, tdlokasi.LokasiNama, ttsk.SKJam, tmotor.FBHarga, ttdk.DKTgl 
                          FROM ttsk 
                          INNER JOIN tdlokasi ON ttsk.LokasiKode = tdlokasi.LokasiKode 
                          INNER JOIN tmotor ON ttsk.SKNo = tmotor.SKNo 
                          INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                          INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                          WHERE  ";
                          
          SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (ttsk.LokasiKode LIKE '",xLokasi,"') AND 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND 
                         (ttsk.SKCetak LIKE '",xCetak,"') AND 
                         (ttdk.LeaseKode LIKE '",xLeasing,"') AND 
                         (ttdk.DKPONo LIKE '",xNoPO,"') 
                         UNION 
                         SELECT ttrk.RKNo AS SKNo, ttrk.RKTgl  AS SKTgl, ttsk.LokasiKode, ttsk.SKMemo, ttsk.UserID, ttdk.CusKode, tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, tdcustomer.CusTelepon, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, ttdk.DKNo, ttdk.DKHarga, tdlokasi.LokasiNama, ttsk.SKJam, tmotor.FBHarga, ttdk.DKTgl 
                         FROM ttsk 
                         INNER JOIN tmotor ON ttsk.SKNo = tmotor.SKNo 
                         INNER JOIN ttrk ON ttrk.RKNo = tmotor.RKNo 
                         INNER JOIN tdlokasi ON ttsk.LokasiKode = tdlokasi.LokasiKode 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                         WHERE "); 
          
          SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (ttsk.LokasiKode LIKE '",xLokasi,"') AND 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND 
                         (ttsk.SKCetak LIKE '",xCetak,"') AND
                         (ttdk.LeaseKode LIKE '",xLeasing,"') AND 
                         (ttdk.DKPONo LIKE '",xNoPO,"')) SKRK ORDER BY SKTgl ",xOrder);
                         
           IF xLeasing = "KREDIT" THEN
              SELECT REPLACE(@MyQuery,CONCAT("(ttdk.LeaseKode LIKE '",xLeasing,"')"),"(ttdk.LeaseKode <> 'TUNAI')") INTO @MyQuery;
           END IF;
       END IF;

       /* CekList */
       IF xTipe = "ttsk5" THEN 
          SET @MyQuery = "SELECT ttsk.SKNo, ttsk.SKTgl, ttsk.LokasiKode, ttsk.SKMemo, ttsk.UserID, ttdk.CusKode, 
                          tdcustomer.CusNama, tdcustomer.CusAlamat, LPAD(tdcustomer.CusRT,3,'0') AS CusRT, LPAD(tdcustomer.CusRW,3,'0')AS CusRW, tdcustomer.CusKelurahan, 
                          tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, tdcustomer.CusTelepon, tmotor.MotorType, 
                          tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, ttdk.DKNo, 
                          ttdk.DKHarga, tdlokasi.LokasiNama, ttsk.SKJam, tmotor.FBHarga, ttdk.DKTgl, ttdk.LeaseKode 
                          FROM ttsk LEFT OUTER JOIN 
                          tdlokasi ON ttsk.LokasiKode = tdlokasi.LokasiKode LEFT OUTER JOIN 
                          tmotor ON ttsk.SKNo = tmotor.SKNo LEFT OUTER JOIN 
                          ttdk ON tmotor.DKNo = ttdk.DKNo LEFT OUTER JOIN 
                          tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                          WHERE ";
                          
          SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (ttsk.LokasiKode LIKE '",xLokasi,"') AND 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND 
                         (IFNULL(tmotor.RKNo, '--') LIKE '",xNoRK,"') AND 
                         (ttsk.SKCetak LIKE '",xCetak,"') AND
                         (ttdk.DKPONo LIKE '",xNoPO,"') AND 
                         (ttdk.LeaseKode LIKE '",xLeasing,"') ORDER BY ",xSort," ",xOrder);
                         
           IF xLeasing = "KREDIT" THEN
              SELECT REPLACE(@MyQuery,CONCAT("(ttdk.LeaseKode LIKE '",xLeasing,"')"),"(ttdk.LeaseKode <> 'TUNAI')") INTO @MyQuery;
           END IF;
       END IF;
       
       /* SK Piutang MD */
       IF xTipe = "ttskTeam1" THEN 
          SET @MyQuery = "SELECT  ttsk.SKNo,ttsk.SKTgl, tmotor.MotorType, fAcronym(UPPER(tmotor.MotorWarna)) AS MotorWarna,  tmotor.MotorNoMesin, tmotor.MotorNoRangka, ttdk.DKHarga, ttdk.DKDPTotal+ttdk.ProgramSubsidi AS DPPO, ttdk.ProgramSubsidi, ttdk.PotonganHarga, ttdk.ReturHarga, ttdk.DKDPTotal-ttdk.ReturHarga-ttdk.PotonganHarga AS TagihkeMediator,  ttdk.LeaseKode, tdsales.SalesNama, ttdk.TeamKode,
                          IF(TRIM(tdcustomer.CusNama)<>TRIM(tdcustomer.CusPembeliNama) AND tdcustomer.CusPembeliNama <>'--' AND tdcustomer.CusPembeliNama <>'',CONCAT_WS(' qq ',tdcustomer.CusNama,tdcustomer.CusPembeliNama),tdcustomer.CusNama) AS CusNama
                          FROM tmotor
                          INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
                          INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
                          INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
                          INNER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode
                          WHERE  ";
                          
          SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND 
                         (IFNULL(tmotor.RKNo, '--') LIKE '",xNoRK,"') AND 
                         (ttsk.SKCetak LIKE '",xCetak,"') AND
                         (ttdk.DKPONo LIKE '",xNoPO,"') AND 
                         (ttdk.LeaseKode LIKE '",xLeasing,"') ");
                         
           IF xNoRK = "" THEN
              SET @MyQuery = CONCAT(@MyQuery," ORDER BY ",xSort," ",xOrder);
           ELSE
              IF xNoRK = "RK" THEN
                 SET @MyQuery = CONCAT(@MyQuery," AND (ttdk.DKDPTotal-ttdk.ReturHarga-ttdk.PotonganHarga <> 0) ORDER BY ",xSort," ",xOrder);
              ELSE
                 SET @MyQuery = CONCAT(@MyQuery," AND (ttdk.DKDPTotal-ttdk.ReturHarga-ttdk.PotonganHarga = 0) ORDER BY ",xSort," ",xOrder);
              END IF;
           END IF;
       END IF;
       
       /* SK DP Net */
       IF xTipe = "ttskTeam2" THEN 
          DELETE FROM tvpiutangbayar ;
          INSERT INTO tvpiutangbayar SELECT GLLink AS HPLink, NoAccount, SUM(HPNilai) AS SumKreditGL FROM ttglhpit 
             WHERE (NOAccount BETWEEN '11060000' AND '11150000') AND HPJenis = 'Piutang' 
             AND TglGL <= xTgl2 GROUP BY HPLink, NoAccount;
          
          IF xLunas = "Belum" THEN
             SET @MyLunasScriptPiutang = " tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar.SumKreditGL, 0) <> 0 AND ";
          ELSE
             IF xLunas = "Lunas" THEN
                SET @MyLunasScriptPiutang = " tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar.SumKreditGL, 0) = 0 AND ";
             ELSE
                SET @MyLunasScriptPiutang = " ";
             END IF;
          END IF;
          
          SET @MyQuery = "SELECT  tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount, tvpiutangdebet.NamaAccount, tvpiutangdebet.KeteranganGL, tvpiutangdebet.DebetGL AS SisaPiutang, IFNULL(tvpiutangbayar.SumKreditGL, 0) AS SisaPiutangTerbayar, tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar.SumKreditGL, 0) AS SisaPiutangSisa, IF (
                          tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar.SumKreditGL, 0) = 0, 'Lunas', 'Belum') AS STATUS, ttdk.DKNo, ttdk.DKTgl,
                          IF(TRIM(tdcustomer.CusNama) <> TRIM(tdcustomer.CusPembeliNama) AND tdcustomer.CusPembeliNama <> '--' AND tdcustomer.CusPembeliNama <> '', CONCAT_WS(' qq ', tdcustomer.CusNama, tdcustomer.CusPembeliNama), tdcustomer.CusNama) AS CusNama, 
                          CONCAT_WS('', tdcustomer.CusAlamat, '  RT/RW : ', LPAD(tdcustomer.CusRT, 3, '0'), '/', LPAD(tdcustomer.CusRW, 3, '0')) AS CusAlamat, 
                          tmotor.MotorNoMesin, ttdk.DKTglTagih, ttdk.DKPONo, ttdk.DKPOTgl, tdmotortype.MotorNama, 
                          ttsk.SKNo,ttsk.SKTgl, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorNoRangka,
                          ttdk.DKHarga, ttdk.DKDPTerima AS DPPO, ttdk.ProgramSubsidi, ttdk.PotonganHarga, ttdk.ReturHarga, 
                          ttdk.DKDPTerima - ttdk.ReturHarga - ttdk.PotonganHarga AS TagihkeMediator,  
                          ttdk.LeaseKode, tdsales.SalesNama, ttdk.TeamKode,ttdk.DKMediator, ttdk.InsentifSales
                          FROM  tvpiutangdebet 
                          LEFT OUTER JOIN tvpiutangbayar ON tvpiutangdebet.GLLink = tvpiutangbayar.HPLink AND tvpiutangdebet.NoAccount = tvpiutangbayar.NoAccount 
                          INNER JOIN ttsk ON ttsk.SKNo = tvpiutangdebet.GLLink 
                          INNER JOIN tmotor ON ttsk.SKNo = tmotor.SKNo 
                          INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                          INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                          INNER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode 
                          INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                          WHERE ";
                          
          SET @MyQuery = CONCAT(@MyQuery,@MyLunasScriptPiutang," tvpiutangdebet.NoAccount  = ('11060100') AND 
                         (tvpiutangdebet.TglGL <= '",xTgl2,"' ) AND 
                         (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND 
                         (ttsk.SKCetak LIKE '",xCetak,"') AND
                         (ttdk.DKPONo LIKE '",xNoPO,"') AND
                         (ttdk.LeaseKode LIKE '",xLeasing,"') AND
                         FakturAHMNo NOT LIKE 'RK%' ORDER BY tvpiutangdebet.GLLink");

           IF xLeasing = "KREDIT" THEN
              SELECT REPLACE(@MyQuery,CONCAT("(ttdk.LeaseKode LIKE '",xLeasing,"')"),"(ttdk.LeaseKode <> 'TUNAI')") INTO @MyQuery;
           END IF;

       END IF;

       /* SK DP Net Saldo */
       IF xTipe = "ttskTeam3" THEN 
          DELETE FROM tvpiutangbayar ;
          INSERT INTO tvpiutangbayar SELECT GLLink AS HPLink, NoAccount, SUM(HPNilai) AS SumKreditGL FROM ttglhpit 
             WHERE (NOAccount BETWEEN '11060000' AND '11150000') AND HPJenis = 'Piutang' 
             AND TglGL <= xTgl2 GROUP BY HPLink, NoAccount;
          
          DELETE FROM tvhutangbayar ;
          INSERT INTO tvhutangbayar SELECT GLLink AS HPLink, NoAccount, SUM(HPNilai) AS SumDebetGL FROM ttglhpit 
            WHERE (LEFT(NoAccount, 2) BETWEEN '24' AND '25') AND HPJenis = 'Hutang' 
            AND TglGL <= xTgl2
            GROUP BY HPLink, NoAccount ;
                   
          IF xLunas = "Belum" THEN
             SET @MyLunasScriptPiutang = " tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar.SumKreditGL, 0) <> 0 AND ";
             SET @MyLunasScriptHutang = "  tvhutangkredit.KreditGL - IFNULL(tvhutangbayar.SumDebetGL, 0) <> 0 AND ";
          ELSE
             IF xLunas = "Lunas" THEN
                SET @MyLunasScriptPiutang = " tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar.SumKreditGL, 0) = 0 AND ";
                SET @MyLunasScriptHutang = "  tvhutangkredit.KreditGL - IFNULL(tvhutangbayar.SumDebetGL, 0) = 0 AND ";
             ELSE
                SET @MyLunasScriptPiutang = " ";
                SET @MyLunasScriptHutang = " ";
             END IF;
          END IF;
          
          SET @MyQuery = "Select UM.GLLink, NoGL, TglGL, NoAccount, NamaAccount, KeteranganGL, SisaPiutang, SisaPiutangTerbayar, STATUS, DKNo, DKTgl,
                          CusNama, CusAlamat, MotorNoMesin, DKTglTagih, DKPONo, DKPOTgl, MotorNama, LeaseKode, SalesNama, TeamKode, DKMediator, InsentifSales,
                          SKNo, SKTgl, MotorType, MotorWarna, MotorNoRangka, DKHarga, ProgramSubsidi, 
                          DPPO, IFNULL(SD.HutangSisa,0) AS PotonganHarga, IFNULL(RH.HutangSisa,0) AS ReturHarga, 
                          DPPO - IFNULL(SD.HutangSisa,0) - IFNULL(RH.HutangSisa,0)  AS TagihkeMediator FROM 
                          (SELECT  tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount, tvpiutangdebet.NamaAccount, tvpiutangdebet.KeteranganGL, tvpiutangdebet.DebetGL AS SisaPiutang, IFNULL(tvpiutangbayar.SumKreditGL, 0) AS SisaPiutangTerbayar, tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar.SumKreditGL, 0) AS SisaPiutangSisa, 
                          IF (tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar.SumKreditGL, 0) = 0, 'Lunas', 'Belum') AS STATUS, ttdk.DKNo, ttdk.DKTgl,
                              IF(TRIM(tdcustomer.CusNama) <> TRIM(tdcustomer.CusPembeliNama) AND tdcustomer.CusPembeliNama <> '--' AND tdcustomer.CusPembeliNama <> '', CONCAT_WS(' qq ', tdcustomer.CusNama, tdcustomer.CusPembeliNama), tdcustomer.CusNama) AS CusNama, 
                              CONCAT_WS('', tdcustomer.CusAlamat, '  RT/RW : ', LPAD(tdcustomer.CusRT, 3, '0'), '/', LPAD(tdcustomer.CusRW, 3, '0')) AS CusAlamat, 
                              tmotor.MotorNoMesin, ttdk.DKTglTagih, ttdk.DKPONo, ttdk.DKPOTgl, tdmotortype.MotorNama, ttdk.LeaseKode, tdsales.SalesNama, ttdk.TeamKode,ttdk.DKMediator, ttdk.InsentifSales,
                              ttsk.SKNo,ttsk.SKTgl, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorNoRangka,
                              ttdk.DKHarga, ttdk.ProgramSubsidi, 
                              tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar.SumKreditGL, 0) AS DPPO, ttdk.PotonganHarga, ttdk.ReturHarga, 
                              ttdk.DKDPTerima - ttdk.ReturHarga - ttdk.PotonganHarga AS TagihkeMediator   
                          FROM  tvpiutangdebet 
                          LEFT OUTER JOIN tvpiutangbayar ON tvpiutangdebet.GLLink = tvpiutangbayar.HPLink AND tvpiutangdebet.NoAccount = tvpiutangbayar.NoAccount 
                          INNER JOIN ttsk ON ttsk.SKNo = tvpiutangdebet.GLLink 
                          INNER JOIN tmotor ON ttsk.SKNo = tmotor.SKNo 
                          INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                          INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                          INNER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode 
                          INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                          WHERE ";
                          
          SET @MyQuery = CONCAT(@MyQuery,@MyLunasScriptPiutang," tvpiutangdebet.NoAccount  = ('11060100') AND 
                         (tvpiutangdebet.TglGL <= '",xTgl2,"' ) AND 
                         (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND 
                         (ttsk.SKCetak LIKE '",xCetak,"') AND
                         (ttdk.DKPONo LIKE '",xNoPO,"') AND
                         (ttdk.LeaseKode LIKE '",xLeasing,"') AND
                         FakturAHMNo NOT LIKE 'RK%' ORDER BY tvpiutangdebet.GLLink) UM 
                         LEFT OUTER JOIN
                         (SELECT tvhutangkredit.GLLink, tvhutangkredit.KreditGL - IFNULL(tvhutangbayar.SumDebetGL, 0) AS HutangSisa FROM tvhutangkredit 
                         LEFT OUTER JOIN tvhutangbayar ON tvhutangkredit.GLLink = tvhutangbayar.HPLink AND tvhutangkredit.NoAccount = tvhutangbayar.NoAccount 
                         WHERE (tvhutangkredit.NoAccount BETWEEN '24040700' AND '24040700') AND tvhutangkredit.KodeTrans <> 'FB' 
                         AND (tvhutangkredit.TglGL <= 'MyTgl2') 
                         ORDER BY tvhutangkredit.GLLink) RH
                         ON UM.GLLink = RH.GLLink 
                         LEFT OUTER JOIN
                         (SELECT tvhutangkredit.GLLink, tvhutangkredit.KreditGL - IFNULL(tvhutangbayar.SumDebetGL, 0) AS HutangSisa FROM tvhutangkredit 
                         LEFT OUTER JOIN tvhutangbayar ON tvhutangkredit.GLLink = tvhutangbayar.HPLink AND tvhutangkredit.NoAccount = tvhutangbayar.NoAccount 
                         WHERE (tvhutangkredit.NoAccount BETWEEN '24040600' AND '24040600') AND tvhutangkredit.KodeTrans <> 'FB' 
                         AND (tvhutangkredit.TglGL <= '",xTgl2,"') 
                         ORDER BY tvhutangkredit.GLLink) SD
                         ON UM.GLLink = SD.GLLink ");

           IF xLeasing = "KREDIT" THEN
              SELECT REPLACE(@MyQuery,CONCAT("(ttdk.LeaseKode LIKE '",xLeasing,"')"),"(ttdk.LeaseKode <> 'TUNAI')") INTO @MyQuery;
           END IF;

       END IF;

       /* AR Tagih Leasing & Lead Team Leasing */
       IF xTipe = "ttdkStatus1" THEN 
          IF xLunas = "Belum" THEN
             SET @MyLunasScript = " AND IF(ttdk.DKNetto - IFNULL(KM_SisaPiutang.Bayar, 0) <= 0 , 'Lunas', 'Belum') = 'Belum' ";
          ELSE
             IF xLunas = "Lunas" THEN
                SET @MyLunasScript = " AND IF(ttdk.DKNetto - IFNULL(KM_SisaPiutang.Bayar, 0) <= 0 , 'Lunas', 'Belum') = 'Lunas' ";
             ELSE
                SET @MyLunasScript = " ";
             END IF;
          END IF;
          
          SET @MyQuery = "SELECT ttdk.DKNo, ttdk.DKTgl, tmotor.SKNo, ttsk.SKTgl,
                          IF(TRIM(tdcustomer.CusNama) <> TRIM(tdcustomer.CusPembeliNama)AND tdcustomer.CusPembeliNama <> '--' AND tdcustomer.CusPembeliNama <> '', CONCAT_WS(' qq ', tdcustomer.CusNama, tdcustomer.CusPembeliNama), tdcustomer.CusNama) AS CusNama, 
                          CONCAT_WS('', tdcustomer.CusAlamat, '  RT/RW : ', LPAD(tdcustomer.CusRT,3,'0') , '/', LPAD(tdcustomer.CusRW,3,'0')) AS CusAlamat, 
                          tdmotortype.MotorType, tmotor.MotorNoMesin, ttdk.LeaseKode, ttdk.DKNetto AS SisaPiutang, IFNULL(KM_SisaPiutang.Bayar, 0) AS SisaPiutangTerbayar, 
                          ttdk.DKNetto - IFNULL(KM_SisaPiutang.Bayar, 0) AS SisaPiutangSisa,  IFNULL(KM_SisaPiutang.KMTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS KMTgl, ttdk.DKTglTagih, ttdk.DKPONo, ttdk.DKPOTgl,
                          IF(ttdk.DKNetto - IFNULL(KM_SisaPiutang.Bayar, 0) <= 0 , 'Lunas', 'Belum') AS STATUS, tdmotortype.MotorNama, tdsales.SalesNama
                          FROM ttdk 
                             INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
                             INNER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo
                             INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
                             INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType
                             INNER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode
                             LEFT OUTER JOIN
                             (SELECT vtkm.KMLINK, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar, MAX(vtkm.KMTgl) AS KMTgl FROM vtkm  WHERE vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing' GROUP BY vtkm.KMLINK, vtkm.KMJenis) KM_SisaPiutang
                              ON ttdk.DKNo = KM_SisaPiutang.KMLINK
                          WHERE  ";
                          
          SET @MyQuery = CONCAT(@MyQuery,"(",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (ttsk.LokasiKode LIKE '",xLokasi,"') AND 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND 
                         (IFNULL(tmotor.RKNo, '--') LIKE '",xNoRK,"') AND 
                         (ttsk.SKCetak LIKE '",xCetak,"') AND 
                         (ttdk.LeaseKode <> 'TUNAI') AND 
                         (ttdk.LeaseKode LIKE '",xLeasing,"') AND 
                         (ttdk.DKPONo LIKE '",xNoPO,"') ",@MyLunasScript," ORDER BY ",xSort," ",xOrder);

           IF xLeasing = "KREDIT" THEN
              SELECT REPLACE(@MyQuery,CONCAT("(ttdk.LeaseKode LIKE '",xLeasing,"')"),"(ttdk.LeaseKode <> 'TUNAI')") INTO @MyQuery;
           END IF;

       END IF;

       /* Tagihan Ke Leasing */
       IF xTipe = "ttdkStatus2" THEN 
          IF xLunas = "Belum" THEN
             SET @MyLunasScript = " tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar.SumKreditGL, 0) <> 0  AND ";
          ELSE
             IF xLunas = "Lunas" THEN
                SET @MyLunasScript = " tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar.SumKreditGL, 0) = 0  AND ";
             ELSE
                SET @MyLunasScript = " ";
             END IF;
          END IF;
          
          OPEN cur1;
          SELECT COUNT(*) INTO @xCount FROM traccount WHERE NoParent = '11080000';
          SET @Counter = 0;
          SET @MyAccount = "";
          REPEAT
             FETCH cur1 INTO xAccount;
             IF NOT done THEN
                SET @Counter = @Counter + 1;
                IF @Counter < @xCount THEN
                   SET @MyAccount = CONCAT(@MyAccount,"'",xAccount,"',");
                ELSE
                   SET @MyAccount = CONCAT(@MyAccount,"'",xAccount,"'");
                END IF;
             END IF;
          UNTIL done END REPEAT;
          
          DELETE FROM tvpiutangbayar ;
          INSERT INTO tvpiutangbayar SELECT GLLink AS HPLink, NoAccount, SUM(HPNilai) AS SumKreditGL FROM ttglhpit 
             WHERE (NOAccount BETWEEN '11060000' AND '11150000') AND HPJenis = 'Piutang' 
             AND TglGL <= xTgl2
             GROUP BY HPLink, NoAccount ;
             
          SET @MyQuery = "SELECT  tvpiutangdebet.GLLink, tvpiutangdebet.NoGL, tvpiutangdebet.TglGL, tvpiutangdebet.NoAccount, tvpiutangdebet.NamaAccount, tvpiutangdebet.KeteranganGL, 
                          tvpiutangdebet.DebetGL AS SisaPiutang, IFNULL(tvpiutangbayar.SumKreditGL, 0) AS SisaPiutangTerbayar, tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar.SumKreditGL, 0) AS SisaPiutangSisa, 
                          IF (tvpiutangdebet.DebetGL - IFNULL(tvpiutangbayar.SumKreditGL, 0) = 0 , 'Lunas', 'Belum') AS STATUS, 
                          ttdk.DKNo, ttdk.DKTgl, tmotor.SKNo, ttsk.SKTgl,
                          IF(TRIM(tdcustomer.CusNama) <> TRIM(tdcustomer.CusPembeliNama)AND tdcustomer.CusPembeliNama <> '--' AND tdcustomer.CusPembeliNama <> '', CONCAT_WS(' qq ', tdcustomer.CusNama, tdcustomer.CusPembeliNama), tdcustomer.CusNama) AS CusNama, 
                          CONCAT_WS('', tdcustomer.CusAlamat, '  RT/RW : ', LPAD(tdcustomer.CusRT,3,'0') , '/', LPAD(tdcustomer.CusRW,3,'0')) AS CusAlamat, 
                          tdmotortype.MotorType, tmotor.MotorNoMesin, ttdk.LeaseKode, ttdk.DKTglTagih, ttdk.DKPONo, ttdk.DKPOTgl,
                          tdmotortype.MotorNama, tdsales.SalesNama, tdsales.TeamKode
                          FROM tvpiutangdebet 
                          LEFT OUTER JOIN tvpiutangbayar  ON tvpiutangdebet.GLLink = tvpiutangbayar.HPLink AND tvpiutangdebet.NoAccount = tvpiutangbayar.NoAccount 
                          INNER JOIN ttsk ON ttsk.SKNo = tvpiutangdebet.GLLink
                          INNER JOIN tmotor ON ttsk.SKNo = tmotor.SKNo  
                          INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                          INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                          INNER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode 
                          INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                          WHERE ";
                          
          SET @MyQuery = CONCAT(@MyQuery,@MyLunasScript," tvpiutangdebet.NoAccount  IN (",@MyAccount,") AND 
                         (tvpiutangdebet.TglGL <= '",xTgl2,"') AND 
                         (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND 
                         (ttsk.SKCetak LIKE '",xCetak,"') AND 
                         (ttdk.DKPONo LIKE '",xNoPO,"') AND 
                         (ttdk.LeaseKode LIKE '",xLeasing,"') AND 
                         (FakturAHMNo NOT LIKE 'RK%') ORDER BY ",xSort," ",xOrder);

           IF xLeasing = "KREDIT" THEN
              SELECT REPLACE(@MyQuery,CONCAT("(ttdk.LeaseKode LIKE '",xLeasing,"')"),"(ttdk.LeaseKode <> 'TUNAI')") INTO @MyQuery;
           END IF;

       END IF;

       /* Selisih OTR */
       IF xTipe = "selisihOTR" THEN 
          SET @MyQuery = "SELECT ttsk.SKNo, ttsk.SKTgl,tdmotortype.MotorNama,tdmotortype.MotorHrgJual, ttdk.DKNo, ttdk.DKTgl, ttdk.DKHarga, ttdk.DKHarga- tdmotortype.MotorHrgJual AS Selisih,ttdk.ProgramSubsidi + ttdk.DKDPTotal AS DPPO, ttdk.LeaseKode
                          FROM ttdk
                          INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
                          INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
                          INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType
                          INNER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode 
                          INNER JOIN ttsk ON ttSk.sKNo = tmotor.SKNo 
                          WHERE  
                          (ttdk.DKHarga- tdmotortype.MotorHrgJual) > 0 AND ";
                          
          SET @MyQuery = CONCAT(@MyQuery,"(",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND 
                         (ttsk.SKCetak LIKE '",xCetak,"') AND 
                         (ttdk.DKPONo LIKE '",xNoPO,"') AND 
                         (ttdk.LeaseKode LIKE '",xLeasing,"') AND (FakturAHMNo NOT LIKE 'RK%') 
                         ORDER BY ",xSort," ",xOrder);

       END IF;

       PREPARE STMT FROM @MyQuery;
       EXECUTE Stmt;
    
    END$$

DELIMITER ;

