DELIMITER $$
DROP PROCEDURE IF EXISTS `rJualReturKonsumen`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rJualReturKonsumen`(IN xStatTgl VARCHAR(25),IN xTgl1 DATE,IN xTgl2 DATE,IN xSales VARCHAR(20),IN xLokasi VARCHAR(30),IN xTypeMTR VARCHAR(20),IN xWarna VARCHAR(20),IN xNoDK VARCHAR(10),IN xSort VARCHAR(25),IN xOrder VARCHAR(10),IN xKab VARCHAR(30))
BEGIN
/*
       xStatTgl : "ttrk.RKTgl"
                  "ttsk.SKTgl"
                  "ttdk.DKTgl"
       
       xSort    : "ttrk.RKNo"
                  "ttsk.SKNo"
                  "ttdk.DKNo"
                  "ttrk.RKTgl"
                  "ttsk.SKTgl"
                  "ttdk.DKTgl"
                  "tdcustomer.CusNama"
                  "ttdk.CusKode"
                  "ttdk.SalesKode"
                  "ttdk.LeaseKode"
                  "ttrk.RKMemo"
                  "ttdk.DKPONo"
                  "ttrk.LokasiKode"
                  "ttrk.UserID"
                  "tmotor.MotorNoMesin"
                  "tmotor.MotorNoRangka"
                  "tmotor.MotorWarna"
                  "tmotor.MotorType"
                  "tmotor.MotorTahun"
                  "IFNULL(ttgeneralledgerhd.GLLink,'--')"
                  "IFNULL(ttdk.DKHarga,0)"
                  "IFNULL(ttdk.DKHPP,0)"
                  "IFNULL(ttdk.DKDPTotal,0)"
                  "IFNULL(ttdk.DKDPLLeasing,0)"
                  "IFNULL(ttdk.DKDPTerima,0)"
                  "IFNULL(ttdk.DKNetto,0)"
                  "IFNULL(ttdk.ProgramSubsidi,0)"
                  "IFNULL(ttdk.PrgSubsSupplier,0)"
                  "IFNULL(ttdk.PrgSubsDealer,0)"
                  "IFNULL(ttdk.PrgSubsFincoy,0)"
                  "IFNULL(ttdk.PotonganHarga,0)"
                  "IFNULL(ttdk.ReturHarga,0)"
                  "IFNULL(ttdk.PotonganKhusus,0)"
                  "IFNULL(ttdk.BBN,0)"
                  "IFNULL(ttdk.Jaket,0)"

       xNoDK    : "SK"
                  "--"
                  ""
       Cara Akses :
       CALL rJualReturKonsumen('ttrk.RKTgl','2001-01-01','2019-12-31','','','','','','ttrk.RKNo','ASC','');
*/
       SET xSales = CONCAT(xSales,"%");
       SET xLokasi = CONCAT(xLokasi,"%");
       SET xTypeMtr = CONCAT(xTypeMtr,"%");
       SET xWarna = CONCAT(xWarna,"%");
       SET xNoDK = CONCAT(xNoDK,"%");
       SET xKab = CONCAT(xKab,"%");
       
          SET @MyQuery = "SELECT ttsk.SKTgl, ttdk.CusKode, tdcustomer.CusNama, tdcustomer.CusAlamat, 
                          tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, 
                          tdcustomer.CusTelepon, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoRangka, ttdk.DKHarga, 
                          tdlokasi.LokasiNama, tmotor.FBHarga, ttdk.DKTgl, ttrk.RKNo, ttrk.RKTgl, ttrk.LokasiKode, ttrk.RKMemo, 
                          ttrk.UserID, ttrk.RKJam, ttrk.DKNo, ttrk.MotorNoMesin, ttrk.SKNo, ttdk.LeaseKode 
                          FROM ttrk 
                          LEFT OUTER JOIN tmotor ON ttrk.RKNo = tmotor.RKNo 
                          LEFT OUTER JOIN ttsk ON ttrk.SKNo = ttsk.SKNo 
                          LEFT OUTER JOIN tdlokasi ON ttrk.LokasiKode = tdlokasi.LokasiKode 
                          LEFT OUTER JOIN ttdk ON ttrk.DKNo = ttdk.DKNo 
                          LEFT OUTER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                          WHERE ";
                          
          SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttdk.SalesKode LIKE '",xSales,"') AND (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (ttdk.LeaseKode LIKE '",xLokasi,"') AND  
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND (tmotor.MotorWarna LIKE '",xWarna,"') AND 
                         (IFNULL(tmotor.DKNo, '--') LIKE '",xNoDK,"') ORDER BY ",xSort," ",xOrder); 
       PREPARE STMT FROM @MyQuery;
       EXECUTE Stmt;
    
    END$$

DELIMITER ;

