DELIMITER $$
DROP PROCEDURE IF EXISTS `rMarketRekapBulanan`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rMarketRekapBulanan`(IN xTipe VARCHAR(25),IN xStatTgl VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,IN xJenis VARCHAR(10),IN xSales VARCHAR(30),IN xLokasi VARCHAR(30))
BEGIN
/*
       xTipe    : "kecin1" -> Kecamatan In
                  "kecin2" -> Fill Kecamatan In
                  "kecout1" -> Kecamatan Out
                  "kecout2" -> Fill Kecamatan Out
                  "sales1" -> Sales
                  "sales2" -> Fill Sales
                  "type1" -> Type
                  "type2" -> Fill Type
                  "segunit11" -> Segmen 1 Unit & Persen
                  "segunit12" -> Fill Segmen 1 Unit & Persen
                  "segunit21" -> Segmen 2 Unit & Persen
                  "segunit22" -> Fill Segmen 2 Unit & Persen
                  "finpersen1" -> Fincoy Persen
                  "finpersen2" -> Fill Fincoy Persen
                  "finunit1" -> Fincoy Unit
                  "finunit2" -> Fill Fincoy Unit
                  "jangka1" -> Jangka Waktu
                  "jangka2" -> Fill Jangka Waktu
                  "umuka1" -> Uang Muka
                  "umuka2" -> Fill Uang Muka
                  

       Cara Akses :

*/
       DECLARE xResultJenis VARCHAR(100);
       DECLARE xResultStatTgl VARCHAR(100);

       /* SelectJenisJual */
       IF xJenis = "%" THEN
          SET xResultJenis = " AND ttdk.LeaseKode LIKE '%' ";
       ELSE
          IF xJenis = "Sudah" THEN
             SET xResultJenis = " AND (ttdk.LeaseKode = 'Tunai' OR ttdk.LeaseKode = 'KDS') ";
          ELSE
             SET xResultJenis = " AND (ttdk.LeaseKode <> 'Tunai' OR ttdk.LeaseKode <> 'KDS') ";
          END IF;
       END IF;
       
       /* SelectDK_SK */
       IF xStatTgl = "ttdk.DKTgl" THEN
          SET xResultStatTgl = " LEFT OUTER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo ";
       ELSE
          SET xResultStatTgl = " INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo ";
       END IF;

       SELECT Kabupaten INTO @xKab FROM tdarea WHERE AreaStatus = '1' LIMIT 1;
       
       /* KECAMATAN IN */
       IF xTipe = "kecin1" THEN 
          
          SET @MyQuery = CONCAT("SELECT tdcustomer.CusKecamatan As Type,'' As Nama, 'IN' As Kategori1, '' As Kategori2,
                          0.0 AS 'Jan',0.0 AS 'Feb',0.0 AS 'Mar',0.0 AS 'Apr',0.0 AS 'Mei',0.0 AS 'Jun',0.0 AS 'Jul',0.0 AS 'Agust',0.0 AS 'Sep',0.0 AS 'Okt', 
                          0.0 AS 'Nop',0.0 AS 'Des',0.0 As Total,0.0 AS 'Q1',0.0 AS 'Q2',0.0 AS 'Q3',0.0 AS 'Q4','' As MotorGroup,'' As MotorKategori
                         FROM tmotor 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode ",xResultStatTgl," 
                         WHERE YEAR(",xStatTgl,") = DATE_FORMAT('",xTgl1,"','%Y') ",xResultJenis," 
                         AND tdcustomer.CusKabupaten = '",@xKab,"' 
                         AND ttdk.SalesKode Like '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY tdcustomer.CusKecamatan"); 
                          
       END IF;


       /* FILL KECAMATAN IN */
       IF xTipe = "kecin2" THEN 
          SET @MyQuery = CONCAT("SELECT  IFNULL(Inti.MotorType,'--') AS MotorType,IFNULL(Fill.Jumlah,0) AS Jumlah FROM 
                         (SELECT tdcustomer.CusKecamatan AS MotorType FROM tmotor 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                         LEFT OUTER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo 
                         WHERE YEAR(",xStatTgl,") = DATE_FORMAT('",xTgl1,"','%Y') ",xResultJenis," 
                         AND tdcustomer.CusKabupaten = '",@xKab,"' 
                         AND ttdk.SalesKode Like '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY tdcustomer.CusKecamatan ) Inti 
                         LEFT OUTER JOIN 
                         (SELECT tdcustomer.CusKecamatan AS MotorType, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah FROM tmotor 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode ",xResultStatTgl," 
                         WHERE ",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"' ",xResultJenis," 
                         AND tdcustomer.CusKabupaten = '",@xKab,"' 
                         AND ttdk.SalesKode Like '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY tdcustomer.CusKecamatan, MONTH(",xStatTgl,")) Fill 
                         ON Fill.MotorType = Inti.MotorType");
       END IF;

       /* KECAMATAN OUT */
       IF xTipe = "kecout1" THEN 
          
          SET @MyQuery = CONCAT("SELECT tdcustomer.CusKecamatan As Type,'' As Nama, 'IN' As Kategori1, '' As Kategori2,
                          0.0 AS 'Jan',0.0 AS 'Feb',0.0 AS 'Mar',0.0 AS 'Apr',0.0 AS 'Mei',0.0 AS 'Jun',0.0 AS 'Jul',0.0 AS 'Agust',0.0 AS 'Sep',0.0 AS 'Okt', 
                          0.0 AS 'Nop',0.0 AS 'Des',0.0 As Total,0.0 AS 'Q1',0.0 AS 'Q2',0.0 AS 'Q3',0.0 AS 'Q4','' As MotorGroup,'' As MotorKategori
                         FROM tmotor 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode ",xResultStatTgl," 
                         WHERE YEAR(",xStatTgl,") = DATE_FORMAT('",xTgl1,"','%Y') ",xResultJenis," 
                         AND tdcustomer.CusKabupaten <> '",@xKab,"' 
                         AND ttdk.SalesKode Like '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY tdcustomer.CusKecamatan"); 
                          
       END IF;


       /* FILL KECAMATAN OUT */
       IF xTipe = "kecout2" THEN 
          SET @MyQuery = CONCAT("SELECT  IFNULL(Inti.MotorType,'--') AS MotorType,IFNULL(Fill.Jumlah,0) AS Jumlah FROM 
                         (SELECT tdcustomer.CusKecamatan AS MotorType FROM tmotor 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                         LEFT OUTER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo 
                         WHERE YEAR(",xStatTgl,") = DATE_FORMAT('",xTgl1,"','%Y') ",xResultJenis," 
                         AND tdcustomer.CusKabupaten <> '",@xKab,"' 
                         AND ttdk.SalesKode Like '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY tdcustomer.CusKecamatan ) Inti 
                         LEFT OUTER JOIN 
                         (SELECT tdcustomer.CusKecamatan AS MotorType, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah FROM tmotor 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode ",xResultStatTgl," 
                         WHERE ",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"' ",xResultJenis," 
                         AND tdcustomer.CusKabupaten <> '",@xKab,"' 
                         AND ttdk.SalesKode Like '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY tdcustomer.CusKecamatan, MONTH(",xStatTgl,")) Fill 
                         ON Fill.MotorType = Inti.MotorType");
       END IF;
       
       
       /* SALES */
       IF xTipe = "sales1" THEN 
          
          SET @MyQuery = CONCAT("SELECT tdsales.SalesNama As Type,'' As Nama, '' As Kategori1, '' As Kategori2,
                          0.0 AS 'Jan',0.0 AS 'Feb',0.0 AS 'Mar',0.0 AS 'Apr',0.0 AS 'Mei',0.0 AS 'Jun',0.0 AS 'Jul',0.0 AS 'Agust',0.0 AS 'Sep',0.0 AS 'Okt', 
                          0.0 AS 'Nop',0.0 AS 'Des',0.0 As Total,0.0 AS 'Q1',0.0 AS 'Q2',0.0 AS 'Q3',0.0 AS 'Q4','' As MotorGroup,'' As MotorKategori 
                         FROM tmotor 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode ",
                         xResultStatTgl," 
                         WHERE YEAR(",xStatTgl,") = DATE_FORMAT('",xTgl1,"','%Y') ",xResultJenis," 
                         AND ttdk.SalesKode Like '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY tdsales.SalesKode, tdsales.SalesNama "); 
                          
       END IF;


       /* FILL SALES */
       IF xTipe = "sales2" THEN 
          SET @MyQuery = CONCAT("SELECT  IFNULL(Inti.MotorType,'--') AS MotorType, IFNULL(Fill.Jumlah,0) AS Jumlah FROM 
                         (SELECT tdsales.SalesKode, tdsales.SalesNama As MotorType FROM tmotor 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode ",
                         xResultStatTgl," 
                         WHERE YEAR(",xStatTgl,") = DATE_FORMAT('",xTgl1,"','%Y') ",xResultJenis," 
                         AND ttdk.SalesKode Like '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY tdsales.SalesKode, tdsales.SalesNama ) Inti 
                         LEFT OUTER JOIN 
                         ( SELECT tdsales.SalesKode, tdsales.SalesNama As MotorType, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah FROM tmotor 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode ",
                         xResultStatTgl," 
                         WHERE ",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"' ",xResultJenis,"  
                         AND ttdk.SalesKode Like '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY tdsales.SalesKode,tdsales.SalesNama, MONTH(",xStatTgl,")) Fill 
                         ON Fill.SalesKode = Inti.SalesKode");
       END IF;
       
       /* TYPE */
       IF xTipe = "type1" THEN 
          SET @MyQuery = CONCAT("SELECT * FROM (SELECT tdmotortype.MotorType, tdmotortype.MotorNama As Type, tdmotortype.MotorKategori As Nama, '' As Kategori1, '' As Kategori2,
                          0.0 AS 'Jan',0.0 AS 'Feb',0.0 AS 'Mar',0.0 AS 'Apr',0.0 AS 'Mei',0.0 AS 'Jun',0.0 AS 'Jul',0.0 AS 'Agust',0.0 AS 'Sep',0.0 AS 'Okt', 
                          0.0 AS 'Nop',0.0 AS 'Des',0.0 As Total,0.0 AS 'Q1',0.0 AS 'Q2',0.0 AS 'Q3',0.0 AS 'Q4','' As MotorGroup,'' As MotorKategori
                         FROM tmotor 
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode ",
                         xResultStatTgl," 
                         WHERE YEAR(",xStatTgl,") = DATE_FORMAT('",xTgl1,"','%Y') ",xResultJenis," 
                         AND ttdk.SalesKode Like '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY  tdmotortype.MotorKategori, tmotor.MotorType) A Order BY A.Nama, A.MotorType "); 
                          
       END IF;


       /* FILL TYPE */
       IF xTipe = "type2" THEN 
          SET @MyQuery = CONCAT("SELECT  IFNULL(Inti.MotorType,'--') AS MotorType,IFNULL(Fill.Jumlah,0) AS Jumlah FROM 
                         (SELECT tmotor.MotorType As MotorType, tdmotortype.MotorKategori As Nama FROM tmotor 
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode ",
                         xResultStatTgl," 
                         WHERE YEAR(",xStatTgl,") = DATE_FORMAT('",xTgl1,"','%Y') ",xResultJenis," 
                         AND ttdk.SalesKode Like '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY tmotor.MotorType, tdmotortype.MotorKategori ORDER BY MotorKategori, MotorType ) Inti 
                         LEFT OUTER JOIN 
                         (SELECT tmotor.MotorType As MotorType, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah FROM tmotor 
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode ",
                         xResultStatTgl," 
                         WHERE ",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"' ",xResultJenis," 
                         AND ttdk.SalesKode Like '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY tmotor.MotorType, tdmotortype.MotorKategori, MONTH(",xStatTgl,")) Fill 
                         ON Fill.MotorType = Inti.MotorType");
       END IF;

       /* SEGMEN 1 UNIT */
       IF xTipe = "segunit11" THEN 
          SET @MyQuery = CONCAT("SELECT tdmotortype.MotorKategori As Type, 'In Unit' As Nama, '' As Kategori1, '' As Kategori2,
                          0.0 AS 'Jan',0.0 AS 'Feb',0.0 AS 'Mar',0.0 AS 'Apr',0.0 AS 'Mei',0.0 AS 'Jun',0.0 AS 'Jul',0.0 AS 'Agust',0.0 AS 'Sep',0.0 AS 'Okt', 
                          0.0 AS 'Nop',0.0 AS 'Des',0.0 As Total,0.0 AS 'Q1',0.0 AS 'Q2',0.0 AS 'Q3',0.0 AS 'Q4','' As MotorGroup,'' As MotorKategori 
                         FROM tmotor 
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode ",
                         xResultStatTgl," 
                         WHERE YEAR(",xStatTgl,") = DATE_FORMAT('",xTgl1,"','%Y') ",xResultJenis," 
                         AND ttdk.SalesKode Like '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY tdmotortype.MotorKategori"); 
                          
       END IF;


       /* FILL SEGMEN 1 UNIT */
       IF xTipe = "segunit12" THEN 
          SET @MyQuery = CONCAT("SELECT  IFNULL(Inti.MotorType,'--') AS MotorType,IFNULL(Fill.Jumlah,0) AS Jumlah FROM 
                        (SELECT tdmotortype.MotorKategori As MotorType, 'In Unit' As Nama FROM tmotor 
                        INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                        INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                        LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode ",
                        xResultStatTgl,"
                        WHERE YEAR(",xStatTgl,") = DATE_FORMAT('",xTgl1,"','%Y') ",xResultJenis,"
                        AND ttdk.SalesKode Like '",xSales,"%' 
                        AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                        GROUP BY tdmotortype.MotorKategori) Inti 
                        LEFT OUTER JOIN 
                        (SELECT tdmotortype.MotorKategori As MotorType, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah FROM tmotor 
                        INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                        INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                        LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode ",
                        xResultStatTgl,"
                        WHERE ",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"' ",xResultJenis," 
                        AND ttdk.SalesKode Like '",xSales,"%' 
                        AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                        GROUP BY tdmotortype.MotorKategori, MONTH(",xStatTgl,")) Fill 
                        ON Fill.MotorType = Inti.MotorType");
       END IF;

       /* SEGMEN 2 UNIT */
       IF xTipe = "segunit21" THEN 
          SET @MyQuery = CONCAT("SELECT IFNULL(tdmotortype.MotorKategori,'--') AS TYPE, 'In Unit' AS Nama, '' As Kategori1, '' As Kategori2,
                          0.0 AS 'Jan',0.0 AS 'Feb',0.0 AS 'Mar',0.0 AS 'Apr',0.0 AS 'Mei',0.0 AS 'Jun',0.0 AS 'Jul',0.0 AS 'Agust',0.0 AS 'Sep',0.0 AS 'Okt', 
                          0.0 AS 'Nop',0.0 AS 'Des',0.0 As Total,0.0 AS 'Q1',0.0 AS 'Q2',0.0 AS 'Q3',0.0 AS 'Q4','' As MotorGroup,'' As MotorKategori  
                         FROM tmotor 
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode ",
                         xResultStatTgl,"
                         WHERE YEAR(",xStatTgl,") = DATE_FORMAT('",xTgl1,"','%Y') ",xResultJenis," 
                         AND ttdk.SalesKode LIKE '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') LIKE '",xLokasi,"%' 
                         GROUP BY LEFT(tdmotortype.MotorKategori,2)"); 
                          
       END IF;


       /* FILL SEGMEN 2 UNIT */
       IF xTipe = "segunit22" THEN 
          SET @MyQuery = CONCAT("SELECT  IFNULL(Inti.MotorType,'--') AS MotorType,IFNULL(Fill.Jumlah,0) AS Jumlah FROM 
                         (SELECT IFNULL(tdmotortype.MotorKategori,'--') As MotorType, 'In Unit' As Nama FROM tmotor 
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode ",
                         xResultStatTgl,"
                         WHERE YEAR(",xStatTgl,") = DATE_FORMAT('",xTgl1,"','%Y') ",xResultJenis," 
                         AND ttdk.SalesKode Like '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY LEFT(tdmotortype.MotorKategori,2)) Inti 
                         LEFT OUTER JOIN 
                         (SELECT tdmotortype.MotorKategori As MotorType, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah FROM tmotor 
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode ",
                         xResultStatTgl,"
                         WHERE ",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"' ",xResultJenis," 
                         AND ttdk.SalesKode Like '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY LEFT(tdmotortype.MotorKategori,2), MONTH(",xStatTgl,")) Fill 
                         ON LEFT(Fill.MotorType,2) = LEFT(Inti.MotorType,2)");
       END IF;

       /* FINCOY UNIT & PERSEN*/
       IF xTipe = "finunit1" THEN 
          SET @MyQuery = CONCAT("SELECT ttdk.LeaseKode AS TYPE, 'In Unit' AS Nama, '' As Kategori1, '' As Kategori2,
                          0.0 AS 'Jan',0.0 AS 'Feb',0.0 AS 'Mar',0.0 AS 'Apr',0.0 AS 'Mei',0.0 AS 'Jun',0.0 AS 'Jul',0.0 AS 'Agust',0.0 AS 'Sep',0.0 AS 'Okt', 
                          0.0 AS 'Nop',0.0 AS 'Des',0.0 As Total,0.0 AS 'Q1',0.0 AS 'Q2',0.0 AS 'Q3',0.0 AS 'Q4','' As MotorGroup,'' As MotorKategori   
                         FROM tmotor 
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode ",
                         xResultStatTgl,"
                         WHERE YEAR(",xStatTgl,") = DATE_FORMAT('",xTgl1,"','%Y') ",xResultJenis," 
                         AND ttdk.SalesKode LIKE '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') LIKE '",xLokasi,"%' 
                         GROUP BY ttdk.LeaseKode"); 
                          
       END IF;


       /* FILL FINCOY UNIT & PERSEN */
       IF xTipe = "finunit2" THEN 
          SET @MyQuery = CONCAT("SELECT  IFNULL(Inti.MotorType,'--') AS MotorType,IFNULL(Fill.Jumlah,0) AS Jumlah FROM 
                         (SELECT ttdk.LeaseKode As MotorType, 'In Unit' As Nama FROM tmotor 
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode ",
                         xResultStatTgl,"
                         WHERE YEAR(",xStatTgl,") = DATE_FORMAT('",xTgl1,"','%Y') ",xResultJenis," 
                         AND ttdk.SalesKode Like '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY ttdk.LeaseKode) Inti
                         LEFT OUTER JOIN 
                         (SELECT ttdk.LeaseKode As MotorType, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah FROM tmotor 
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode ",
                         xResultStatTgl,"
                         WHERE ",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"' ",xResultJenis,"
                         AND ttdk.SalesKode Like '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY ttdk.LeaseKode, MONTH(",xStatTgl,")) Fill 
                         ON Fill.MotorType = Inti.MotorType");
       END IF;

       /* JANGKA WAKTU */
       IF xTipe = "jangka1" THEN 
          SET @MyQuery = CONCAT("SELECT fJangkaWaktu(ttdk.DKTenor) AS TYPE, ttdk.LeaseKode AS Nama, '' As Kategori1, '' As Kategori2,
                          0.0 AS 'Jan',0.0 AS 'Feb',0.0 AS 'Mar',0.0 AS 'Apr',0.0 AS 'Mei',0.0 AS 'Jun',0.0 AS 'Jul',0.0 AS 'Agust',0.0 AS 'Sep',0.0 AS 'Okt', 
                          0.0 AS 'Nop',0.0 AS 'Des',0.0 As Total,0.0 AS 'Q1',0.0 AS 'Q2',0.0 AS 'Q3',0.0 AS 'Q4','' As MotorGroup,'' As MotorKategori    
                         FROM tmotor 
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode ",
                         xResultStatTgl,"
                         WHERE YEAR(",xStatTgl,") = DATE_FORMAT('",xTgl1,"','%Y') ",xResultJenis," 
                         AND ttdk.SalesKode LIKE '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') LIKE '",xLokasi,"%' 
                         GROUP BY fJangkaWaktu(ttdk.DKTenor), ttdk.LeaseKode"); 
                          
       END IF;


       /* FILL JANGKA WAKTU */
       IF xTipe = "jangka2" THEN 
          SET @MyQuery = CONCAT("SELECT  IFNULL(Inti.MotorType,'--') AS MotorType,IFNULL(Fill.Jumlah,0) AS Jumlah FROM 
                         (SELECT fJangkaWaktu(ttdk.DKTenor) As MotorType, ttdk.LeaseKode As LeaseKode FROM tmotor
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode ",
                         xResultStatTgl,"
                         WHERE YEAR(",xStatTgl,") = DATE_FORMAT('",xTgl1,"','%Y') ",xResultJenis," 
                         AND ttdk.SalesKode Like '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY fJangkaWaktu(ttdk.DKTenor), ttdk.LeaseKode) Inti 
                         LEFT OUTER JOIN 
                         (SELECT fJangkaWaktu(ttdk.DKTenor) As MotorType, ttdk.LeaseKode, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah FROM tmotor 
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode ",
                         xResultStatTgl,"
                         WHERE ",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"' ",xResultJenis," 
                         AND ttdk.SalesKode Like '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY fJangkaWaktu(ttdk.DKTenor), ttdk.LeaseKode, MONTH(",xStatTgl,")) Fill 
                         ON Fill.MotorType = Inti.MotorType AND Fill.LeaseKode = Inti.LeaseKode");
       END IF;

       /* UANG MUKA */
       IF xTipe = "umuka1" THEN 
          SET @MyQuery = CONCAT("SELECT fUangMuka(ttdk.DKDPTotal) As Type, ttdk.LeaseKode As Nama, '' As Kategori1, '' As Kategori2,
                          0.0 AS 'Jan',0.0 AS 'Feb',0.0 AS 'Mar',0.0 AS 'Apr',0.0 AS 'Mei',0.0 AS 'Jun',0.0 AS 'Jul',0.0 AS 'Agust',0.0 AS 'Sep',0.0 AS 'Okt', 
                          0.0 AS 'Nop',0.0 AS 'Des',0.0 As Total,0.0 AS 'Q1',0.0 AS 'Q2',0.0 AS 'Q3',0.0 AS 'Q4','' As MotorGroup,'' As MotorKategori     
                         FROM tmotor 
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode ",
                         xResultStatTgl,"
                         WHERE YEAR(",xStatTgl,") = DATE_FORMAT('",xTgl1,"','%Y') ",xResultJenis,"  
                         AND ttdk.SalesKode Like '",xSales,"%'
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY LEFT(fUangMuka(ttdk.DKDPTotal),1), ttdk.LeaseKode"); 
                          
       END IF;


       /* FILL UANG MUKA */
       IF xTipe = "umuka2" THEN 
          SET @MyQuery = CONCAT("SELECT  IFNULL(Inti.MotorType,'--') AS MotorType, IFNULL(Fill.Jumlah,0) AS Jumlah FROM 
                         (SELECT fUangMuka(ttdk.DKDPTotal) As MotorType, ttdk.LeaseKode  FROM tmotor 
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode ",
                         xResultStatTgl,"
                         WHERE YEAR(",xStatTgl,") = DATE_FORMAT('",xTgl1,"','%Y') ",xResultJenis,"  
                         AND ttdk.SalesKode Like '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY fUangMuka(ttdk.DKDPTotal), ttdk.LeaseKode) Inti 
                         LEFT OUTER JOIN 
                         (SELECT fUangMuka(ttdk.DKDPTotal) As MotorType, ttdk.LeaseKode, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah FROM tmotor 
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
                         LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode ",
                         xResultStatTgl,"
                         WHERE ",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"' ",xResultJenis,"  
                         AND ttdk.SalesKode Like '",xSales,"%' 
                         AND IFNULL(ttsk.LokasiKode,'') Like '",xLokasi,"%' 
                         GROUP BY fUangMuka(ttdk.DKDPTotal),ttdk.LeaseKode, MONTH(",xStatTgl,")) Fill 
                         ON Fill.MotorType = Inti.MotorType  AND Fill.LeaseKode = Inti.LeaseKode");
       END IF;


/*       select @MyQuery; */
       PREPARE STMT FROM @MyQuery;
       EXECUTE Stmt;

    END$$

DELIMITER ;

