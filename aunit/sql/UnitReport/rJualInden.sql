DELIMITER $$
DROP PROCEDURE IF EXISTS `rJualInden`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rJualInden`(IN xStatTgl VARCHAR(25),IN xTgl1 DATE,IN xTgl2 DATE,IN xSales VARCHAR(20),IN xKab VARCHAR(30),IN xLeasing VARCHAR(20),IN xTypeMtr VARCHAR(20),xWarna VARCHAR(20),xNoDK VARCHAR(10),IN xSort VARCHAR(25),IN xOrder VARCHAR(10))
BEGIN
/*
       xStatTgl : "ttIN.INTgl"
       
       xSort    : "ttIN.INNo"
                  "ttIN.INTgl"
                  "tdcustomer.CusNama"
                  "ttIN.CusKode"
                  "ttIN.SalesKode"
                  "ttIN.LeaseKode"
                  "ttIN.INMemo"
                  "ttIN.INJenis"
                  "ttIN.UserID"
                  "IFNULL(ttdk.DKNo, '--')"
                  "ttIN.INHarga"
                  "ttIN.INDP"
                  "tdleasing.LeaseNama"
                  "tdsales.SalesNama"
                  "tdcustomer.CusAlamat"
                  "tdcustomer.CusRT"
                  "tdcustomer.CusRW"
                  "tdcustomer.CusKelurahan"
                  "tdcustomer.CusKecamatan"
                  "tdcustomer.CusKabupaten"
                  "tdcustomer.CusTelepon"

       xNoDK    : "DK"
                  "--"
                  ""
       Cara Akses :
       CALL rJualInden('ttIN.INTgl','2001-01-01','2019-12-31','','','','','','','ttIN.INNo','ASC');
*/
       SET xSales = CONCAT(xSales,"%");
       SET xKab = CONCAT(xKab,"%");
       SET xLeasing = CONCAT(xLeasing,"%");
       SET xTypeMtr = CONCAT(xTypeMtr,"%");
       SET xWarna = CONCAT(xWarna,"%");
       SET xNoDK = CONCAT(xNoDK,"%");
       
       SET @MyQuery = "SELECT ttin.INNo, ttin.INTgl, ttin.SalesKode, ttin.CusKode, ttin.LeaseKode, ttin.INJenis, ttin.INMemo, 
                       ttin.INDP, ttin.UserID, IFNULL(tdcustomer.CusNama, '--') AS CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, 
                       tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, IFNULL(tdcustomer.CusKabupaten, '--') AS CusKabupaten, tdcustomer.CusTelepon, 
                       IFNULL(tdsales.SalesNama, '--') AS SalesNama, IFNULL(tdleasing.LeaseNama, '--') AS LeaseNama, ttin.INHarga, 
                       ttin.MotorType, ttin.MotorWarna, ttin.DKNo FROM ttin 
                       LEFT OUTER JOIN tdleasing ON ttin.LeaseKode = tdleasing.LeaseKode  
                       LEFT OUTER JOIN tdsales ON ttin.SalesKode = tdsales.SalesKode  
                       LEFT OUTER JOIN tdcustomer ON ttin.CusKode = tdcustomer.CusKode  
                       LEFT OUTER JOIN ttdk ON ttin.INNo = ttdk.INNo  
                       WHERE ";
       SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                      (ttin.SalesKode LIKE '",xSales,"') AND (ttin.LeaseKode LIKE '",xLeasing,"') AND 
                      (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                      (ttin.MotorType LIKE '",xTypeMtr,"') AND (ttin.MotorWarna LIKE '",xWarna,"') AND 
                      (IFNULL(ttdk.DKNo, '--') LIKE '",xNoDK,"') ORDER BY ttin.MotorType, ttin.INTgl,",xSort," ",xOrder); 
       PREPARE STMT FROM @MyQuery;
       EXECUTE Stmt;
    
    END$$

DELIMITER ;

