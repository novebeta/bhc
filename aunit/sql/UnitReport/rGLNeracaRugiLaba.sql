DELIMITER $$

DROP PROCEDURE IF EXISTS `rGLNeracaRugiLaba`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rGLNeracaRugiLaba`(IN xTipe VARCHAR(20),IN TglAwal DATE,IN TglAkhir DATE, IN xJenis VARCHAR(50), IN xEOM VARCHAR(1))
BEGIN
/*
CALL rGLNeracaRugiLaba('Aktiva','2020-01-01','2020-01-31','0','1');
*/
   DECLARE X  INT;
	DECLARE BulanSebelum DOUBLE DEFAULT 0;
	DECLARE BulanBerjalan DOUBLE DEFAULT 0;
	DECLARE Pendapatan, PendapatanOp, HPP, Beban, Pendapatan2, Beban2 DOUBLE DEFAULT 0;
	DECLARE MyPenjualan DOUBLE DEFAULT 0;

	DECLARE UnitServis INTEGER DEFAULT 0;
	DECLARE UnitJual INTEGER DEFAULT 0;
	DECLARE UnitSaldo INTEGER DEFAULT 0;
	DECLARE UnitRetur INTEGER DEFAULT 0;
	DECLARE UnitJual0 INTEGER DEFAULT 0;
	DECLARE UnitSaldo0 INTEGER DEFAULT 0;
	DECLARE UnitRetur0 INTEGER DEFAULT 0;
	DECLARE UnitJual1 INTEGER DEFAULT 0;
	DECLARE UnitSaldo1 INTEGER DEFAULT 0;
	DECLARE UnitRetur1 INTEGER DEFAULT 0;
	DECLARE TambahanPotonganHarga DOUBLE DEFAULT 0;

   DECLARE Penjualan DOUBLE DEFAULT 0;
   DECLARE Retur DOUBLE DEFAULT 0;
   DECLARE SD1 DOUBLE DEFAULT 0;
   DECLARE SD2 DOUBLE DEFAULT 0;
   DECLARE BBN DOUBLE DEFAULT 0;
   DECLARE Gaji DOUBLE DEFAULT 0;
   DECLARE Insentif DOUBLE DEFAULT 0;
   DECLARE Pemeliharaan DOUBLE DEFAULT 0;
   DECLARE Promosi DOUBLE DEFAULT 0;
   DECLARE Pajak DOUBLE DEFAULT 0;
   DECLARE Other DOUBLE DEFAULT 0;
   DECLARE PendapatanNonOp DOUBLE DEFAULT 0;

	SET TambahanPotonganHarga = 0;
	SET BulanSebelum = 0;
	SET BulanBerjalan = 0;
   SET X = 0;
	SET MyPenjualan = 0;
	SET UnitServis = 0;
	SET UnitJual = 0;
	SET UnitSaldo = 0;
	SET UnitRetur = 0;
	
	SET UnitJual0 = 0;
	SET UnitSaldo0 = 0;
	SET UnitRetur0 = 0;
	SET UnitJual1 = 0;
	SET UnitSaldo1 = 0;
	SET UnitRetur1 = 0;

   SET Pendapatan = 0;
   SET PendapatanOp = 0;
   SET HPP = 0;
   SET Beban = 0;
   SET Pendapatan2 = 0;
   SET Beban2 = 0;

	UPDATE traccount SET LevelAccount = fCOALevel(NoAccount);
        
	IF xTipe = "Aktiva" THEN
 		CALL rglNeraca(TglAwal, TglAkhir, TglAkhir);             

		DROP TEMPORARY TABLE IF EXISTS NeracaAktiva1;
		CREATE TEMPORARY TABLE IF NOT EXISTS NeracaAktiva1 AS
		SELECT SPACE(300) AS Keterangan, 0000000000000000.00 AS Colom1, 0000000000000000.00 AS Colom2,
		SPACE(10) AS NoAccount, SPACE(10) AS GroupAccount, SPACE(150) AS NamaGroup;
		CREATE INDEX NoAccount ON NeracaAktiva1(NoAccount);
		DELETE FROM NeracaAktiva1;
		
		INSERT INTO NeracaAktiva1 SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
		traccountsaldo.NoAccount, traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
		WHERE traccountsaldo.NoAccount LIKE '11%' AND traccountsaldo.LevelAccount = 2;
		INSERT INTO NeracaAktiva1 SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
		traccountsaldo.NoAccount, traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount  AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
		WHERE traccountsaldo.NoAccount LIKE '12%' AND traccountsaldo.LevelAccount = 2;
		INSERT INTO NeracaAktiva1 SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
		traccountsaldo.NoAccount, traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount  AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
		WHERE traccountsaldo.NoAccount LIKE '13%' AND traccountsaldo.LevelAccount = 2;
		
		
		IF xJenis = "Prev Year to Date & Year to Date" THEN
			UPDATE NeracaAktiva1 SET Colom2 = Colom1;
			SET TglAwal = STR_TO_DATE(CONCAT(YEAR(DATE_ADD(TglAwal, INTERVAL -1 YEAR)),'-','01','-','01') ,'%Y-%m-%d');
			IF xEOM = '1' THEN
				SET TglAkhir = LAST_DAY(DATE_ADD(DATE_ADD(TglAkhir, INTERVAL -1 YEAR), INTERVAL 12-MONTH(NOW()) MONTH));
			ELSE
				SET TglAkhir = DATE_ADD(TglAkhir, INTERVAL -1 YEAR);
			END IF;
			
			DROP TEMPORARY TABLE IF EXISTS traccountsaldo;
			CALL rglNeraca(TglAwal, TglAkhir, TglAkhir);

			DROP TEMPORARY TABLE IF EXISTS NeracaAktiva0;
			CREATE TEMPORARY TABLE IF NOT EXISTS NeracaAktiva0 AS
			SELECT SPACE(300) AS Keterangan, 0000000000000000.00 AS Colom1, 0000000000000000.00 AS Colom2,
			SPACE(10) AS NoAccount, SPACE(10) AS GroupAccount, SPACE(150) AS NamaGroup;
			CREATE INDEX NoAccount ON NeracaAktiva0(NoAccount);
			DELETE FROM NeracaAktiva0;

			INSERT INTO NeracaAktiva0 SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
			traccountsaldo.NoAccount, traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
			WHERE traccountsaldo.NoAccount LIKE '11%' AND traccountsaldo.LevelAccount = 2;
			INSERT INTO NeracaAktiva0 SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
			traccountsaldo.NoAccount, traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount  AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
			WHERE traccountsaldo.NoAccount LIKE '12%' AND traccountsaldo.LevelAccount = 2;
			INSERT INTO NeracaAktiva0 SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
			traccountsaldo.NoAccount, traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount  AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
			WHERE traccountsaldo.NoAccount LIKE '13%' AND traccountsaldo.LevelAccount = 2;

			UPDATE NeracaAktiva1 INNER JOIN NeracaAktiva0 ON  NeracaAktiva1.NoAccount = NeracaAktiva0.NoAccount SET NeracaAktiva1.Colom1 = NeracaAktiva0.Colom1;

		END IF;
		
		SET @MyQuery = CONCAT("SELECT * FROM NeracaAktiva1;");		
 	END IF;
	
	IF xTipe = "Pasiva" THEN
		CALL rglNeraca(TglAwal, TglAkhir, TglAkhir);             
		DROP TEMPORARY TABLE IF EXISTS NeracaAktiva1;
		CREATE TEMPORARY TABLE IF NOT EXISTS NeracaAktiva1 AS
		SELECT SPACE(300) AS Keterangan, 0000000000000000.00 AS Colom1, 0000000000000000.00 AS Colom2,
		SPACE(10) AS NoAccount, SPACE(10) AS GroupAccount, SPACE(150) AS NamaGroup;
		CREATE INDEX NoAccount ON NeracaAktiva1(NoAccount);
		DELETE FROM NeracaAktiva1;
		INSERT INTO NeracaAktiva1 SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
		traccountsaldo.NoAccount, traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
		WHERE traccountsaldo.NoAccount LIKE '24%' AND traccountsaldo.LevelAccount = 2;
		INSERT INTO NeracaAktiva1 SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
		traccountsaldo.NoAccount, traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
		WHERE traccountsaldo.NoAccount LIKE '25%' AND traccountsaldo.LevelAccount = 2;
		INSERT INTO NeracaAktiva1 SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
		traccountsaldo.NoAccount, traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
		WHERE traccountsaldo.NoAccount LIKE '26%' AND traccountsaldo.LevelAccount = 2;
	
		SELECT OpeningBalance,Kredit-Debet INTO BulanSebelum,BulanBerjalan  FROM traccountsaldo WHERE NoAccount = '26040000';
		DELETE FROM NeracaAktiva1 WHERE NoAccount = '26040000';
		INSERT INTO NeracaAktiva1 SELECT 'Rugi Laba Sebelum Bulan Berjalan' AS Keterangan, BulanSebelum AS Colom1, 0 AS Colom2,
		'26040001' AS NoAccount, '26000000' AS GroupAccount, 'Ekuitas' AS NamaGroup ;
		INSERT INTO NeracaAktiva1 SELECT 'Rugi Laba Bulan Berjalan' AS Keterangan, BulanBerjalan AS Colom1, 0 AS Colom2,
		'26040002' AS NoAccount, '26000000' AS GroupAccount, 'Ekuitas' AS NamaGroup ;

		IF xJenis = "Prev Year to Date & Year to Date" THEN
			UPDATE NeracaAktiva1 SET Colom2 = Colom1;
			SET TglAwal = STR_TO_DATE(CONCAT(YEAR(DATE_ADD(TglAwal, INTERVAL -1 YEAR)),'-','01','-','01') ,'%Y-%m-%d');
			IF xEOM = '1' THEN
				SET TglAkhir = LAST_DAY(DATE_ADD(DATE_ADD(TglAkhir, INTERVAL -1 YEAR), INTERVAL 12-MONTH(NOW()) MONTH));
			ELSE
				SET TglAkhir = DATE_ADD(TglAkhir, INTERVAL -1 YEAR);
			END IF;
			
			DROP TEMPORARY TABLE IF EXISTS traccountsaldo;
			CALL rglNeraca(TglAwal, TglAkhir, TglAkhir);

			DROP TEMPORARY TABLE IF EXISTS NeracaAktiva0;
			CREATE TEMPORARY TABLE IF NOT EXISTS NeracaAktiva0 AS
			SELECT SPACE(300) AS Keterangan, 0000000000000000.00 AS Colom1, 0000000000000000.00 AS Colom2,
			SPACE(10) AS NoAccount, SPACE(10) AS GroupAccount, SPACE(150) AS NamaGroup;
			CREATE INDEX NoAccount ON NeracaAktiva0(NoAccount);
			DELETE FROM NeracaAktiva0;
			INSERT INTO NeracaAktiva0 SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
			traccountsaldo.NoAccount, traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
			WHERE traccountsaldo.NoAccount LIKE '24%' AND traccountsaldo.LevelAccount = 2;
			INSERT INTO NeracaAktiva0 SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
			traccountsaldo.NoAccount, traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
			WHERE traccountsaldo.NoAccount LIKE '25%' AND traccountsaldo.LevelAccount = 2;
			INSERT INTO NeracaAktiva0 SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
			traccountsaldo.NoAccount, traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
			WHERE traccountsaldo.NoAccount LIKE '26%' AND traccountsaldo.LevelAccount = 2;
	
			SELECT OpeningBalance,Kredit-Debet INTO BulanSebelum,BulanBerjalan  FROM traccountsaldo WHERE NoAccount = '26040000';
			DELETE FROM NeracaAktiva0 WHERE NoAccount = '26040000';
			INSERT INTO NeracaAktiva0 SELECT 'Rugi Laba Sebelum Bulan Berjalan' AS Keterangan, BulanSebelum AS Colom1, 0 AS Colom2,
			'26040001' AS NoAccount, '26000000' AS GroupAccount, 'Ekuitas' AS NamaGroup ;
			INSERT INTO NeracaAktiva0 SELECT 'Rugi Laba Bulan Berjalan' AS Keterangan, BulanBerjalan AS Colom1, 0 AS Colom2,
			'26040002' AS NoAccount, '26000000' AS GroupAccount, 'Ekuitas' AS NamaGroup ;

			UPDATE NeracaAktiva1 INNER JOIN NeracaAktiva0 ON  NeracaAktiva1.NoAccount = NeracaAktiva0.NoAccount SET NeracaAktiva1.Colom1 = NeracaAktiva0.Colom1;

		END IF;

		SET @MyQuery = CONCAT("SELECT * FROM NeracaAktiva1;");		
	END IF;

	IF xTipe = "Laba-Rugi" THEN
		/*Month to Date & Year to Date*/
		IF xJenis = "Year to Date" OR xJenis = "Prev Year to Date & Year to Date"  THEN
			CALL rglNeraca(TglAwal, TglAkhir, TglAkhir);
			SELECT COUNT(SKNO) INTO UnitJual1 FROM ttsk INNER JOIN ttgeneralledgerhd ON ttsk.NoGL = ttgeneralledgerhd.NoGL WHERE ttsk.SKTgl BETWEEN STR_TO_DATE(CONCAT(YEAR(TglAwal),'-','01','-','01') ,'%Y-%m-%d')  AND TglAkhir;
			SELECT IFNULL(JumUnit,0) INTO UnitSaldo1  FROM tsaldounit WHERE TglUnit BETWEEN STR_TO_DATE(CONCAT(YEAR(TglAwal),'-','01','-','01') ,'%Y-%m-%d') AND TglAkhir AND NoAccount = '30010000';
			SELECT COUNT(RKNO) INTO UnitRetur1 FROM ttrk INNER JOIN ttgeneralledgerhd ON ttrk.NoGL = ttgeneralledgerhd.NoGL WHERE ttrk.RKTgl BETWEEN STR_TO_DATE(CONCAT(YEAR(TglAwal),'-','01','-','01') ,'%Y-%m-%d') AND TglAkhir;
		END IF;
		IF xJenis = "Month to Date" OR xJenis = "Prev Month & Month To Date" OR xJenis = "Month To Date & Year to Date" THEN
			CALL rglNeraca(TglAwal, TglAkhir, TglAwal);
			SELECT COUNT(SKNO) INTO UnitJual1 FROM ttsk INNER JOIN ttgeneralledgerhd ON ttsk.NoGL = ttgeneralledgerhd.NoGL WHERE ttsk.SKTgl BETWEEN TglAwal AND TglAkhir;
			SELECT IFNULL(JumUnit,0) INTO UnitSaldo1  FROM tsaldounit WHERE TglUnit BETWEEN TglAwal AND TglAkhir AND NoAccount = '30010000';
			SELECT COUNT(RKNO) INTO UnitRetur1 FROM ttrk INNER JOIN ttgeneralledgerhd ON ttrk.NoGL = ttgeneralledgerhd.NoGL WHERE ttrk.RKTgl BETWEEN TglAwal AND TglAkhir;
		END IF;

		DROP TEMPORARY TABLE IF EXISTS TPosRugiLaba;
		CREATE TEMPORARY TABLE IF NOT EXISTS TPosRugiLaba AS
		SELECT SPACE(150) AS Keterangan, 0000000000000000.00 AS Colom1, 0000000000000000.00 AS Colom2,
		SPACE(10) AS NoAccount, SPACE(10) AS GroupAccount, SPACE(150) AS NamaGroup;
		CREATE INDEX NoAccount ON TPosRugiLaba(NoAccount);
		CREATE INDEX Keterangan ON TPosRugiLaba(Keterangan);
		DELETE FROM TPosRugiLaba;
		INSERT INTO TPosRugiLaba SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
		traccountsaldo.NoAccount, traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
		WHERE traccountsaldo.NoAccount LIKE '3%' AND traccountsaldo.LevelAccount = 1;
		INSERT INTO TPosRugiLaba SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
		traccountsaldo.NoAccount, '30000000' AS GroupAccount, 'Penjualan' AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
		WHERE traccountsaldo.NoAccount = '40000000' ;
		INSERT INTO TPosRugiLaba SELECT '               HARGA POKOK PENJUALAN' AS Keterangan, - Saldo AS Colom1, 0 AS Colom2,
		'-' AS NoAccount, 'A' AS GroupAccount, 'HARGA POKOK PENJUALAN' AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
		WHERE traccountsaldo.NoAccount = '50000000' ;
		SELECT Saldo INTO Pendapatan FROM traccountsaldo WHERE NoAccount = '30000000';
		SELECT Saldo INTO PendapatanOp FROM traccountsaldo WHERE NoAccount = '40000000';
		SELECT Saldo INTO HPP FROM traccountsaldo WHERE NoAccount = '50000000';
		SELECT Saldo INTO Beban FROM traccountsaldo WHERE NoAccount = '60000000';
		SELECT Saldo INTO Pendapatan2 FROM traccountsaldo WHERE NoAccount = '90000000';
		SELECT Saldo INTO Beban2 FROM traccountsaldo WHERE NoAccount = '80000000';
		INSERT INTO TPosRugiLaba SELECT '               LABA KOTOR' AS Keterangan, Pendapatan + PendapatanOp - HPP AS Colom1, 0 AS Colom2,
		'-' AS NoAccount, 'A' AS GroupAccount, 'LABA KOTOR' AS NamaGroup;
		INSERT INTO TPosRugiLaba SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
		traccountsaldo.NoAccount, traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
		WHERE traccountsaldo.NoAccount LIKE '6%' AND traccountsaldo.LevelAccount = 1;
		INSERT INTO TPosRugiLaba SELECT '               LABA OPERASIONAL' AS Keterangan, Pendapatan + PendapatanOp - HPP - Beban AS Colom1, 0 AS Colom2,
		'-' AS NoAccount, 'A' AS GroupAccount, 'LABA OPERASIONAL' AS NamaGroup;
		INSERT INTO TPosRugiLaba SELECT traccountsaldo.NamaAccount AS Keterangan, -Saldo AS Colom1, 0 AS Colom2,
		traccountsaldo.NoAccount, '80000000' AS GroupAccount, 'BIAYA DAN PENDAPATAN DILUAR USAHA' AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
		WHERE traccountsaldo.NoAccount = '80000000' ;
		INSERT INTO TPosRugiLaba SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
		traccountsaldo.NoAccount, '80000000' AS GroupAccount, 'BIAYA DAN PENDAPATAN DILUAR USAHA' AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
		WHERE traccountsaldo.NoAccount = '90000000' ;
		INSERT INTO TPosRugiLaba SELECT '               LABA BERSIH' AS Keterangan, Pendapatan + PendapatanOp - HPP- Beban + Pendapatan2 - Beban2 AS Colom1, 0 AS Colom2,
		'-' AS NoAccount, 'A' AS GroupAccount, 'LABA BERSIH' AS NamaGroup;

      SELECT Colom1 INTO TambahanPotonganHarga FROM TPosRugiLaba WHERE NoAccount = '60060000';
      UPDATE TPosRugiLaba SET Colom1 = 0 WHERE NoAccount ='60060000';
      UPDATE TPosRugiLaba SET Colom1 = -TambahanPotonganHarga WHERE NoAccount ='30050000';
      UPDATE TPosRugiLaba SET Colom1 = Pendapatan + PendapatanOp - HPP - TambahanPotonganHarga WHERE Keterangan ='               LABA KOTOR';

    /*Pre Year*/
		IF xJenis = "Prev Year to Date & Year to Date" THEN
			UPDATE TPosRugiLaba SET Colom2 = Colom1;
			SET TglAwal = STR_TO_DATE(CONCAT(YEAR(DATE_ADD(TglAwal, INTERVAL -1 YEAR)),'-','01','-','01') ,'%Y-%m-%d');
			IF xEOM = '1' THEN
				SET TglAkhir = LAST_DAY(DATE_ADD(DATE_ADD(TglAkhir, INTERVAL -1 YEAR), INTERVAL 12-MONTH(NOW()) MONTH));
			ELSE
				SET TglAkhir = DATE_ADD(TglAkhir, INTERVAL -1 YEAR);
			END IF;
			DROP TEMPORARY TABLE IF EXISTS traccountsaldo;
			CALL rglNeraca(TglAwal, TglAkhir, TglAkhir);
			SELECT COUNT(SKNO) INTO UnitJual0 FROM ttsk INNER JOIN ttgeneralledgerhd ON ttsk.NoGL = ttgeneralledgerhd.NoGL WHERE ttsk.SKTgl BETWEEN TglAwal AND TglAkhir;
			SELECT IFNULL(JumUnit,0) INTO UnitSaldo0  FROM tsaldounit WHERE TglUnit BETWEEN TglAwal AND TglAkhir AND NoAccount = '30010000';
			SELECT COUNT(RKNO) INTO UnitRetur0 FROM ttrk INNER JOIN ttgeneralledgerhd ON ttrk.NoGL = ttgeneralledgerhd.NoGL WHERE ttrk.RKTgl BETWEEN TglAwal AND TglAkhir;

			DROP TEMPORARY TABLE IF EXISTS TPosRugiLaba0;
			CREATE TEMPORARY TABLE IF NOT EXISTS TPosRugiLaba0 AS
			SELECT SPACE(150) AS Keterangan, 0000000000000000.00 AS Colom1, 0000000000000000.00 AS Colom2,
			SPACE(10) AS NoAccount, SPACE(10) AS GroupAccount, SPACE(150) AS NamaGroup;
			CREATE INDEX NoAccount ON TPosRugiLaba0(NoAccount);
			CREATE INDEX Keterangan ON TPosRugiLaba0(Keterangan);
			DELETE FROM TPosRugiLaba0;
			INSERT INTO TPosRugiLaba0 SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
			traccountsaldo.NoAccount, traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
			WHERE traccountsaldo.NoAccount LIKE '3%' AND traccountsaldo.LevelAccount = 1;
			INSERT INTO TPosRugiLaba0 SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
			traccountsaldo.NoAccount, '30000000' AS GroupAccount, 'Penjualan' AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
			WHERE traccountsaldo.NoAccount = '40000000' ;
			INSERT INTO TPosRugiLaba0 SELECT '               HARGA POKOK PENJUALAN' AS Keterangan, - Saldo AS Colom1, 0 AS Colom2,
			'-' AS NoAccount, 'A' AS GroupAccount, 'HARGA POKOK PENJUALAN' AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
			WHERE traccountsaldo.NoAccount = '50000000' ;
			SELECT Saldo INTO Pendapatan FROM traccountsaldo WHERE NoAccount = '30000000';
			SELECT Saldo INTO PendapatanOp FROM traccountsaldo WHERE NoAccount = '40000000';
			SELECT Saldo INTO HPP FROM traccountsaldo WHERE NoAccount = '50000000';
			SELECT Saldo INTO Beban FROM traccountsaldo WHERE NoAccount = '60000000';
			SELECT Saldo INTO Pendapatan2 FROM traccountsaldo WHERE NoAccount = '90000000';
			SELECT Saldo INTO Beban2 FROM traccountsaldo WHERE NoAccount = '80000000';
			INSERT INTO TPosRugiLaba0 SELECT '               LABA KOTOR' AS Keterangan, Pendapatan + PendapatanOp - HPP AS Colom1, 0 AS Colom2,
			'-' AS NoAccount, 'A' AS GroupAccount, 'LABA KOTOR' AS NamaGroup;
			INSERT INTO TPosRugiLaba0 SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
			traccountsaldo.NoAccount, traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
			WHERE traccountsaldo.NoAccount LIKE '6%' AND traccountsaldo.LevelAccount = 1;
			INSERT INTO TPosRugiLaba0 SELECT '               LABA OPERASIONAL' AS Keterangan, Pendapatan + PendapatanOp - HPP - Beban AS Colom1, 0 AS Colom2,
			'-' AS NoAccount, 'A' AS GroupAccount, 'LABA OPERASIONAL' AS NamaGroup;
			INSERT INTO TPosRugiLaba0 SELECT traccountsaldo.NamaAccount AS Keterangan, -Saldo AS Colom1, 0 AS Colom2,
			traccountsaldo.NoAccount, '80000000' AS GroupAccount, 'BIAYA DAN PENDAPATAN DILUAR USAHA' AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
			WHERE traccountsaldo.NoAccount = '80000000' ;
			INSERT INTO TPosRugiLaba0 SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
			traccountsaldo.NoAccount, '80000000' AS GroupAccount, 'BIAYA DAN PENDAPATAN DILUAR USAHA' AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
			WHERE traccountsaldo.NoAccount = '90000000' ;
			INSERT INTO TPosRugiLaba0 SELECT '               LABA BERSIH' AS Keterangan, Pendapatan + PendapatanOp - HPP- Beban + Pendapatan2 - Beban2 AS Colom1, 0 AS Colom2,
			'-' AS NoAccount, 'A' AS GroupAccount, 'LABA BERSIH' AS NamaGroup;

      SELECT Colom1 INTO TambahanPotonganHarga FROM TPosRugiLaba0 WHERE NoAccount = '60060000';
      UPDATE TPosRugiLaba0 SET Colom1 = 0 WHERE NoAccount ='60060000';
      UPDATE TPosRugiLaba0 SET Colom1 = -TambahanPotonganHarga WHERE NoAccount ='30050000';
      UPDATE TPosRugiLaba0 SET Colom1 = Pendapatan + PendapatanOp - HPP - TambahanPotonganHarga WHERE Keterangan ='               LABA KOTOR';

			UPDATE TPosRugiLaba INNER JOIN TPosRugiLaba0 ON  TPosRugiLaba.Keterangan = TPosRugiLaba0.Keterangan SET TPosRugiLaba.Colom1 = TPosRugiLaba0.Colom1;
		END IF;

    /*Prev Month*/
		IF xJenis = "Prev Month & Month To Date" THEN
			UPDATE TPosRugiLaba SET Colom2 = Colom1;
			SET TglAwal = DATE_ADD(TglAwal, INTERVAL -1 MONTH);
			IF xEOM = '1' THEN
				SET TglAkhir = LAST_DAY(DATE_ADD(TglAkhir, INTERVAL -1 MONTH));
			ELSE
				SET TglAkhir = DATE_ADD(TglAkhir, INTERVAL -1 MONTH);
			END IF;
			DROP TEMPORARY TABLE IF EXISTS traccountsaldo;
			CALL rglNeraca(TglAwal, TglAkhir, TglAwal);
			SELECT COUNT(SKNO) INTO UnitJual0 FROM ttsk INNER JOIN ttgeneralledgerhd ON ttsk.NoGL = ttgeneralledgerhd.NoGL WHERE ttsk.SKTgl BETWEEN TglAwal AND TglAkhir;
			SELECT IFNULL(JumUnit,0) INTO UnitSaldo0  FROM tsaldounit WHERE TglUnit BETWEEN TglAwal AND TglAkhir AND NoAccount = '30010000';
			SELECT COUNT(RKNO) INTO UnitRetur0 FROM ttrk INNER JOIN ttgeneralledgerhd ON ttrk.NoGL = ttgeneralledgerhd.NoGL WHERE ttrk.RKTgl BETWEEN TglAwal AND TglAkhir;

			DROP TEMPORARY TABLE IF EXISTS TPosRugiLaba0;
			CREATE TEMPORARY TABLE IF NOT EXISTS TPosRugiLaba0 AS
			SELECT SPACE(150) AS Keterangan, 0000000000000000.00 AS Colom1, 0000000000000000.00 AS Colom2,
			SPACE(10) AS NoAccount, SPACE(10) AS GroupAccount, SPACE(150) AS NamaGroup;
			CREATE INDEX NoAccount ON TPosRugiLaba0(NoAccount);
			CREATE INDEX Keterangan ON TPosRugiLaba0(Keterangan);
			DELETE FROM TPosRugiLaba0;
			INSERT INTO TPosRugiLaba0 SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
			traccountsaldo.NoAccount, traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
			WHERE traccountsaldo.NoAccount LIKE '3%' AND traccountsaldo.LevelAccount = 1;
			INSERT INTO TPosRugiLaba0 SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
			traccountsaldo.NoAccount, '30000000' AS GroupAccount, 'Penjualan' AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
			WHERE traccountsaldo.NoAccount = '40000000' ;
			INSERT INTO TPosRugiLaba0 SELECT '               HARGA POKOK PENJUALAN' AS Keterangan, - Saldo AS Colom1, 0 AS Colom2,
			'-' AS NoAccount, 'A' AS GroupAccount, 'HARGA POKOK PENJUALAN' AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
			WHERE traccountsaldo.NoAccount = '50000000' ;
			SELECT Saldo INTO Pendapatan FROM traccountsaldo WHERE NoAccount = '30000000';
			SELECT Saldo INTO PendapatanOp FROM traccountsaldo WHERE NoAccount = '40000000';
			SELECT Saldo INTO HPP FROM traccountsaldo WHERE NoAccount = '50000000';
			SELECT Saldo INTO Beban FROM traccountsaldo WHERE NoAccount = '60000000';
			SELECT Saldo INTO Pendapatan2 FROM traccountsaldo WHERE NoAccount = '90000000';
			SELECT Saldo INTO Beban2 FROM traccountsaldo WHERE NoAccount = '80000000';
			INSERT INTO TPosRugiLaba0 SELECT '               LABA KOTOR' AS Keterangan, Pendapatan + PendapatanOp - HPP AS Colom1, 0 AS Colom2,
			'-' AS NoAccount, 'A' AS GroupAccount, 'LABA KOTOR' AS NamaGroup;
			INSERT INTO TPosRugiLaba0 SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
			traccountsaldo.NoAccount, traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
			WHERE traccountsaldo.NoAccount LIKE '6%' AND traccountsaldo.LevelAccount = 1;
			INSERT INTO TPosRugiLaba0 SELECT '               LABA OPERASIONAL' AS Keterangan, Pendapatan + PendapatanOp - HPP - Beban AS Colom1, 0 AS Colom2,
			'-' AS NoAccount, 'A' AS GroupAccount, 'LABA OPERASIONAL' AS NamaGroup;
			INSERT INTO TPosRugiLaba0 SELECT traccountsaldo.NamaAccount AS Keterangan, -Saldo AS Colom1, 0 AS Colom2,
			traccountsaldo.NoAccount, '80000000' AS GroupAccount, 'BIAYA DAN PENDAPATAN DILUAR USAHA' AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
			WHERE traccountsaldo.NoAccount = '80000000' ;
			INSERT INTO TPosRugiLaba0 SELECT traccountsaldo.NamaAccount AS Keterangan, Saldo AS Colom1, 0 AS Colom2,
			traccountsaldo.NoAccount, '80000000' AS GroupAccount, 'BIAYA DAN PENDAPATAN DILUAR USAHA' AS NamaGroup FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
			WHERE traccountsaldo.NoAccount = '90000000' ;
			INSERT INTO TPosRugiLaba0 SELECT '               LABA BERSIH' AS Keterangan, Pendapatan + PendapatanOp - HPP- Beban + Pendapatan2 - Beban2 AS Colom1, 0 AS Colom2,
			'-' AS NoAccount, 'A' AS GroupAccount, 'LABA BERSIH' AS NamaGroup;

			SELECT Colom1 INTO TambahanPotonganHarga FROM TPosRugiLaba0 WHERE NoAccount = '60060000';
			UPDATE TPosRugiLaba0 SET Colom1 = 0 WHERE NoAccount ='60060000';
			UPDATE TPosRugiLaba0 SET Colom1 = -TambahanPotonganHarga WHERE NoAccount ='30050000';
			UPDATE TPosRugiLaba0 SET Colom1 = Pendapatan + PendapatanOp - HPP - TambahanPotonganHarga WHERE Keterangan ='               LABA KOTOR';
      
			UPDATE TPosRugiLaba INNER JOIN TPosRugiLaba0 ON  TPosRugiLaba.Keterangan = TPosRugiLaba0.Keterangan SET TPosRugiLaba.Colom1 = TPosRugiLaba0.Colom1;
		END IF;

		IF xJenis = "Year to Date" OR  xJenis = "Month to Date" THEN
			UPDATE TPosRugiLaba SET Keterangan = CONCAT(Keterangan," [",FORMAT(UnitJual1+UnitSaldo1, 0),"unit ]") WHERE NoAccount = '30010000';
			UPDATE TPosRugiLaba SET Keterangan = CONCAT(Keterangan," [",FORMAT(-UnitRetur1, 0),"unit ]") WHERE NoAccount = '30020000';
		END IF;
		IF xJenis = "Prev Year to Date & Year to Date"  OR xJenis = "Prev Month & Month To Date" OR xJenis = "Month To Date & Year to Date" THEN
			UPDATE TPosRugiLaba SET Keterangan = CONCAT(Keterangan," [",FORMAT(UnitJual0+UnitSaldo0, 0)," / ",FORMAT(UnitJual1+UnitSaldo1, 0)," unit ]") WHERE NoAccount = '30010000';
			UPDATE TPosRugiLaba SET Keterangan = CONCAT(Keterangan," [",FORMAT(-UnitRetur0, 0)," / ",FORMAT(-UnitRetur1, 0),"unit ]") WHERE NoAccount = '30020000';
		END IF;
		
		SET @MyQuery = CONCAT("SELECT * FROM TPosRugiLaba;");	
	END IF;

	IF xTipe = "Pendapatan" THEN
		IF xJenis = "Year to Date" OR xJenis = "Prev Year to Date & Year to Date"  THEN
			CALL rglNeraca(TglAwal, TglAkhir, TglAkhir);
		END IF;
		IF xJenis = "Month to Date" OR xJenis = "Prev Month & Month To Date" OR "Month To Date & Year to Date" THEN
			CALL rglNeraca(TglAwal, TglAkhir, TglAwal);
		END IF;

		DROP TEMPORARY TABLE IF EXISTS Perkiraan;
		CREATE TEMPORARY TABLE IF NOT EXISTS Perkiraan AS
		SELECT SPACE(10) AS NoAccount, SPACE(150) AS NamaAccount,SPACE(6) AS JenisAccount,SPACE(1) AS StatusAccount, SPACE(10) AS NoParent,
		0000000000000000.00 AS OpeningBalance, 0000000000000000.00 AS Saldo,00 AS LEVEL,0000000000000000.00 AS Debet,0000000000000000.00 AS Kredit,
		SPACE(10) AS MyNoAccount;
		CREATE INDEX NoAccount ON Perkiraan(NoAccount);
		CREATE INDEX NamaAccount ON Perkiraan(NamaAccount);
		DELETE FROM Perkiraan;

		INSERT INTO Perkiraan SELECT traccountsaldo.NoAccount, CONCAT(REPEAT(SPACE(19),LevelAccount-1),NamaAccount) AS NamaAccount,
		traccountsaldo.JenisAccount, traccountsaldo.StatusAccount, traccountsaldo.NoParent,
		traccountsaldo.OpeningBalance, traccountsaldo.Saldo, traccountsaldo.LevelAccount AS LEVEL,
		traccountsaldo.Debet, traccountsaldo.Kredit,traccountsaldo.NoAccount AS MyAccount
		FROM traccountsaldo
		WHERE traccountsaldo.NoAccount LIKE '4%' AND traccountsaldo.NoParent <> '00000000';
		UPDATE Perkiraan SET OpeningBalance =0, Saldo = 0, Debet = 0, Kredit = 0 WHERE JenisAccount = 'Header';

		IF xJenis = "Prev Year to Date & Year to Date" THEN
			UPDATE Perkiraan SET OpeningBalance = Saldo;
			SET TglAwal = DATE_ADD(TglAwal, INTERVAL -1 YEAR);
			IF xEOM = '1' THEN
				SET TglAkhir = LAST_DAY(DATE_ADD(DATE_ADD(TglAkhir, INTERVAL -1 YEAR), INTERVAL 12-MONTH(NOW()) MONTH));
			ELSE
				SET TglAkhir = DATE_ADD(TglAkhir, INTERVAL -1 YEAR);
			END IF;
			DROP TEMPORARY TABLE IF EXISTS traccountsaldo;
			CALL rglNeraca(TglAwal, TglAkhir, TglAkhir);

			DROP TEMPORARY TABLE IF EXISTS Perkiraan0;
			CREATE TEMPORARY TABLE IF NOT EXISTS Perkiraan0 AS
			SELECT SPACE(10) AS NoAccount, SPACE(150) AS NamaAccount,SPACE(6) AS JenisAccount,SPACE(1) AS StatusAccount, SPACE(10) AS NoParent,
			0000000000000000.00 AS OpeningBalance, 0000000000000000.00 AS Saldo,00 AS LEVEL,0000000000000000.00 AS Debet,0000000000000000.00 AS Kredit,
			SPACE(10) AS MyNoAccount;
			CREATE INDEX NoAccount ON Perkiraan0(NoAccount);
			CREATE INDEX NamaAccount ON Perkiraan0(NamaAccount);
			DELETE FROM Perkiraan0;

			INSERT INTO Perkiraan0 SELECT traccountsaldo.NoAccount, traccountsaldo.NamaAccount,
			traccountsaldo.JenisAccount, traccountsaldo.StatusAccount, traccountsaldo.NoParent,
			traccountsaldo.OpeningBalance, traccountsaldo.Saldo, traccountsaldo.LevelAccount AS LEVEL,
			traccountsaldo.Debet, traccountsaldo.Kredit,traccountsaldo.NoAccount AS MyAccount
			FROM traccountsaldo
			WHERE traccountsaldo.NoAccount LIKE '4%' AND traccountsaldo.NoParent <> '00000000';

			UPDATE Perkiraan INNER JOIN Perkiraan0 ON  Perkiraan.NoAccount = Perkiraan0.NoAccount SET Perkiraan.Saldo = Perkiraan0.Saldo;
		END IF;

		IF xJenis = "Prev Month & Month To Date" THEN
			UPDATE Perkiraan SET OpeningBalance = Saldo;
			SET TglAwal = DATE_ADD(TglAwal, INTERVAL -1 MONTH);
			IF xEOM = '1' THEN
				SET TglAkhir = LAST_DAY(DATE_ADD(TglAkhir, INTERVAL -1 MONTH));
			ELSE
				SET TglAkhir = DATE_ADD(TglAkhir, INTERVAL -1 MONTH);
			END IF;
			DROP TEMPORARY TABLE IF EXISTS traccountsaldo;
			CALL rglNeraca(TglAwal, TglAkhir, TglAwal);

			DROP TEMPORARY TABLE IF EXISTS Perkiraan0;
			CREATE TEMPORARY TABLE IF NOT EXISTS Perkiraan0 AS
			SELECT SPACE(10) AS NoAccount, SPACE(150) AS NamaAccount,SPACE(6) AS JenisAccount,SPACE(1) AS StatusAccount, SPACE(10) AS NoParent,
			0000000000000000.00 AS OpeningBalance, 0000000000000000.00 AS Saldo,00 AS LEVEL,0000000000000000.00 AS Debet,0000000000000000.00 AS Kredit,
			SPACE(10) AS MyNoAccount;
			CREATE INDEX NoAccount ON Perkiraan0(NoAccount);
			CREATE INDEX NamaAccount ON Perkiraan0(NamaAccount);
			DELETE FROM Perkiraan0;

			INSERT INTO Perkiraan0 SELECT traccountsaldo.NoAccount, traccountsaldo.NamaAccount,
			traccountsaldo.JenisAccount, traccountsaldo.StatusAccount, traccountsaldo.NoParent,
			traccountsaldo.OpeningBalance, traccountsaldo.Saldo, traccountsaldo.LevelAccount AS LEVEL,
			traccountsaldo.Debet, traccountsaldo.Kredit,traccountsaldo.NoAccount AS MyAccount
			FROM traccountsaldo
			WHERE traccountsaldo.NoAccount LIKE '4%' AND traccountsaldo.NoParent <> '00000000';

			UPDATE Perkiraan INNER JOIN Perkiraan0 ON  Perkiraan.NoAccount = Perkiraan0.NoAccount SET Perkiraan.Saldo = Perkiraan0.Saldo;
		END IF;

		IF xJenis = "Month To Date & Year to Date" THEN
			UPDATE Perkiraan SET OpeningBalance = Saldo;
			DROP TEMPORARY TABLE IF EXISTS traccountsaldo;
			CALL rglNeraca(TglAwal, TglAkhir, TglAkhir);

			DROP TEMPORARY TABLE IF EXISTS Perkiraan0;
			CREATE TEMPORARY TABLE IF NOT EXISTS Perkiraan0 AS
			SELECT SPACE(10) AS NoAccount, SPACE(150) AS NamaAccount,SPACE(6) AS JenisAccount,SPACE(1) AS StatusAccount, SPACE(10) AS NoParent,
			0000000000000000.00 AS OpeningBalance, 0000000000000000.00 AS Saldo,00 AS LEVEL,0000000000000000.00 AS Debet,0000000000000000.00 AS Kredit,
			SPACE(10) AS MyNoAccount;
			CREATE INDEX NoAccount ON Perkiraan0(NoAccount);
			CREATE INDEX NamaAccount ON Perkiraan0(NamaAccount);
			DELETE FROM Perkiraan0;

			INSERT INTO Perkiraan0 SELECT traccountsaldo.NoAccount, traccountsaldo.NamaAccount,
			traccountsaldo.JenisAccount, traccountsaldo.StatusAccount, traccountsaldo.NoParent,
			traccountsaldo.OpeningBalance, traccountsaldo.Saldo, traccountsaldo.LevelAccount AS LEVEL,
			traccountsaldo.Debet, traccountsaldo.Kredit,traccountsaldo.NoAccount AS MyAccount
			FROM traccountsaldo
			WHERE traccountsaldo.NoAccount LIKE '4%' AND traccountsaldo.NoParent <> '00000000';

			UPDATE Perkiraan INNER JOIN Perkiraan0 ON  Perkiraan.NoAccount = Perkiraan0.NoAccount SET Perkiraan.Saldo = Perkiraan0.Saldo;
		END IF;	
		SET @MyQuery = CONCAT("SELECT * FROM Perkiraan;");	
	END IF;
	
	IF xTipe = "Neraca T" THEN
		CALL rglNeraca(TglAwal, TglAkhir, TglAkhir);
		DROP TEMPORARY TABLE IF EXISTS NeracaT ;
		CREATE TEMPORARY TABLE IF NOT EXISTS NeracaT  AS
		SELECT 00 AS num, SPACE(10) AS GroupAccount1, SPACE(150) AS GroupNama1,
		SPACE(10) AS NoAccount1, SPACE(150) AS NamaAccount1, 0000000000000000.00 AS Colom1,
		SPACE(10) AS GroupAccount2, SPACE(150) AS GroupNama2,
		SPACE(10) AS NoAccount2, SPACE(150) AS NamaAccount2, 0000000000000000.00 AS Colom2;
		CREATE INDEX num ON NeracaT(num);
		DELETE FROM NeracaT;

      SET @row_number = 0;

		INSERT INTO NeracaT SELECT (@row_number:=@row_number + 1) AS num, '11000000' AS GroupAccount1, 'Aktiva Lancar' AS GroupNama1,
      NoAccount AS NoAccount1, NamaAccount AS NamaAccount1, Saldo AS Colom1,
      SPACE(10) AS GroupAccount2, SPACE(150) AS GroupNama2,
      SPACE(10) AS NoAccount2, SPACE(150) AS NamaAccount2, 0000000000000000.00 AS Colom2
		FROM traccountsaldo
		WHERE traccountsaldo.NoAccount LIKE '11%' AND LevelAccount = '2';

		INSERT INTO NeracaT SELECT (@row_number:=@row_number + 1) AS num, '12000000' AS GroupAccount1, 'Aktiva Tetap' AS GroupNama1,
      NoAccount AS NoAccount1, NamaAccount AS NamaAccount1, Saldo AS Colom1,
      SPACE(10) AS GroupAccount2, SPACE(150) AS GroupNama2,
      SPACE(10) AS NoAccount2, SPACE(150) AS NamaAccount2, 0000000000000000.00 AS Colom2
		FROM traccountsaldo
		WHERE traccountsaldo.NoAccount LIKE '12%' AND LevelAccount = '2';

		INSERT INTO NeracaT SELECT (@row_number:=@row_number + 1) AS num, '13000000' AS GroupAccount1, 'Aktiva Lain Lain' AS GroupNama1,
      NoAccount AS NoAccount1, NamaAccount AS NamaAccount1, Saldo AS Colom1,
      SPACE(10) AS GroupAccount2, SPACE(150) AS GroupNama2,
      SPACE(10) AS NoAccount2, SPACE(150) AS NamaAccount2, 0000000000000000.00 AS Colom2
		FROM traccountsaldo
		WHERE traccountsaldo.NoAccount LIKE '13%' AND LevelAccount = '2';

		SET X = 0;
		loop_label:  LOOP
			IF  X < 4 THEN
				INSERT INTO NeracaT
				SELECT (@row_number:=@row_number + 1)  AS Num, '13000000' AS GroupAccount1, 'Aktiva Lain2' AS GroupNama1,
				SPACE(10) AS NoAccount1, SPACE(150) AS NamaAccount1, 0000000000000000.00 AS Colom1,
				SPACE(10) AS GroupAccount2, SPACE(150) AS GroupNama2,
				SPACE(10) AS NoAccount2, SPACE(150) AS NamaAccount2, 0000000000000000.00 AS Colom2;
				SET  X = X + 1;
				ITERATE  loop_label;
			ELSE
				LEAVE  loop_label;
			END IF;
		END LOOP;

		DROP TEMPORARY TABLE IF EXISTS Neraca2 ;
		CREATE TEMPORARY TABLE IF NOT EXISTS Neraca2  AS
		SELECT 00 AS num, SPACE(10) AS GroupAccount1, SPACE(150) AS GroupNama1,
		SPACE(10) AS NoAccount1, SPACE(150) AS NamaAccount1, 0000000000000000.00 AS Colom1;
		CREATE INDEX num ON Neraca2(num);
		DELETE FROM Neraca2;

      SET @row_number = 0;
		INSERT INTO Neraca2 SELECT (@row_number:=@row_number + 1) AS num, '24000000' AS GroupAccount1, 'Hutang Lancar' AS GroupNama1,
      NoAccount AS NoAccount1, NamaAccount AS NamaAccount1, Saldo AS Colom1
		FROM traccountsaldo
		WHERE traccountsaldo.NoAccount LIKE '24%' AND LevelAccount = '2';

		SET X = 0;
		loop_label:  LOOP
			IF  X < 12 THEN
				INSERT INTO Neraca2
				SELECT (@row_number:=@row_number + 1)  AS Num, '24000000' AS GroupAccount1, 'Hutang Lancar' AS GroupNama1,
				SPACE(10) AS NoAccount1, SPACE(150) AS NamaAccount1, 0000000000000000.00 AS Colom1;
				SET  X = X + 1;
				ITERATE  loop_label;
			ELSE
				LEAVE  loop_label;
			END  IF;
		END LOOP;

		INSERT INTO Neraca2 SELECT (@row_number:=@row_number + 1) AS num, '25000000' AS GroupAccount1, 'Hutang Jangka Panjang' AS GroupNama1,
      NoAccount AS NoAccount1, NamaAccount AS NamaAccount1, Saldo AS Colom1
		FROM traccountsaldo
		WHERE traccountsaldo.NoAccount LIKE '25%' AND LevelAccount = '2';

      SET @row_number:=@row_number + 3 ;

		INSERT INTO Neraca2 SELECT (@row_number:=@row_number + 1) AS num, '26000000' AS GroupAccount1, 'Ekuitas' AS GroupNama1,
      NoAccount AS NoAccount1, NamaAccount AS NamaAccount1, Saldo AS Colom1
		FROM traccountsaldo
		WHERE traccountsaldo.NoAccount LIKE '26%' AND LevelAccount = '2' AND NoAccount <> '26040000';

		SELECT OpeningBalance,Kredit-Debet INTO BulanSebelum,BulanBerjalan  FROM traccountsaldo WHERE NoAccount = '26040000';
		INSERT INTO Neraca2 SELECT (@row_number:=@row_number + 1) AS num, '26000000' AS GroupAccount1, 'Ekuitas' AS GroupNama1,
		'26040001' AS NoAccount1, 'Rugi Laba Sebelum Bulan Berjalan' AS NamaAccount1, BulanSebelum AS Colom1;
		INSERT INTO Neraca2 SELECT (@row_number:=@row_number + 1) AS num, '26000000' AS GroupAccount1, 'Ekuitas' AS GroupNama1,
		'26040002' AS NoAccount1, 'Rugi Laba Bulan Berjalan' AS NamaAccount1, BulanBerjalan AS Colom1;

      UPDATE Neraca2 INNER JOIN  NeracaT ON Neraca2.num = NeracaT.num
      SET NeracaT.GroupAccount2 = Neraca2.GroupAccount1,
      NeracaT.GroupNama2 = Neraca2.GroupNama1,
      NeracaT.NoAccount2 = Neraca2.NoAccount1,
      NeracaT.NamaAccount2 = Neraca2.NamaAccount1,
      NeracaT.Colom2 = Neraca2.Colom1;

		SET @MyQuery = CONCAT("SELECT * FROM NeracaT;");	
	END IF;

	IF xTipe = "Laba-Rugi Tri" THEN
		DROP TEMPORARY TABLE IF EXISTS LabaRugi ;
		CREATE TEMPORARY TABLE IF NOT EXISTS LabaRugi  AS
		SELECT SPACE(50) AS GroupAccount, SPACE(150) AS GroupNama,
		SPACE(50) AS NoAccount, SPACE(150) AS NamaAccount,
		000000 AS Unit1, 0000000000000000.00 AS Colom1, 000.00 AS Persen1,
		000000 AS Unit2, 0000000000000000.00 AS Colom2, 000.00 AS Persen2,
		000000 AS Unit3, 0000000000000000.00 AS Colom3, 000.00 AS Persen3,
		000000 AS Unit4, 0000000000000000.00 AS Colom4, 000.00 AS Persen4,
		000000 AS Unit5, 0000000000000000.00 AS Colom5, 000.00 AS Persen5,
		000000 AS Unit6, 0000000000000000.00 AS Colom6, 000.00 AS Persen6,
		000000 AS Unit7, 0000000000000000.00 AS Colom7, 000.00 AS Persen7,
		000000 AS Unit8, 0000000000000000.00 AS Colom8, 000.00 AS Persen8,
		000000 AS Unit9, 0000000000000000.00 AS Colom9, 000.00 AS Persen9,
		000000 AS Unit10, 0000000000000000.00 AS Colom10, 000.00 AS Persen10,
		000000 AS Unit11, 0000000000000000.00 AS Colom11, 000.00 AS Persen11,
		000000 AS Unit12, 0000000000000000.00 AS Colom12, 000.00 AS Persen12,
		000000 AS UnitT, 0000000000000000.00 AS ColomT, 000.00 AS PersenT;
		CREATE INDEX NoAccount ON LabaRugi(NoAccount);
		DELETE FROM LabaRugi;

		SET X = 0;
		loop_label:  LOOP
			IF  X < 3 THEN
				SET  X = X + 1;
				CALL rglNeraca(TglAwal, TglAkhir, TglAwal);
				SELECT COUNT(SKNO) INTO UnitJual FROM ttsk INNER JOIN ttgeneralledgerhd ON ttsk.NoGL = ttgeneralledgerhd.NoGL WHERE ttsk.SKTgl BETWEEN TglAwal AND TglAkhir;
				SELECT IFNULL(JumUnit,0) INTO UnitSaldo FROM tsaldounit WHERE TglUnit BETWEEN TglAwal AND TglAkhir AND NoAccount = '30010000';
				SELECT COUNT(RKNO) INTO UnitRetur FROM ttrk INNER JOIN ttgeneralledgerhd ON ttrk.NoGL = ttgeneralledgerhd.NoGL WHERE ttrk.RKTgl BETWEEN TglAwal AND TglAkhir;
				DROP TEMPORARY TABLE IF EXISTS LabaRugiTemp ;
				CREATE TEMPORARY TABLE IF NOT EXISTS LabaRugiTemp  AS
				SELECT SPACE(50) AS GroupAccount, SPACE(150) AS GroupNama,
				SPACE(50) AS NoAccount, SPACE(150) AS NamaAccount,
				000000 AS Unit1, 0000000000000000.00 AS Colom1, 00.00 AS Persen1;
				CREATE INDEX NoAccount ON LabaRugiTemp(NoAccount);
				DELETE FROM LabaRugiTemp;
				INSERT INTO LabaRugiTemp SELECT traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount AS GroupNama ,
				traccountsaldo.NoAccount, traccountsaldo.NamaAccount , 0 AS Unit1, Saldo AS Colom1, 00.00 AS Persen1
				FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
				WHERE traccountsaldo.NoAccount LIKE '3%' AND traccountsaldo.LevelAccount = 1;

				INSERT INTO LabaRugiTemp SELECT '30000000' AS GroupAccount, 'Penjualan' AS GroupNama ,
				traccountsaldo.NoAccount, traccountsaldo.NamaAccount , 0 AS Unit1, Saldo AS Colom1, 00.00 AS Persen1
				FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
				WHERE traccountsaldo.NoAccount = '40000000' ;

        INSERT INTO LabaRugiTemp SELECT 'Harga Pokok Penjualan' AS GroupAccount, 'Harga Pokok Penjualan' AS GroupNama ,
				'Harga Pokok Penjualan' AS NoAccount, 'Harga Pokok Penjualan' AS NamaAccount , 0 AS Unit1, -Saldo AS Colom1, 00.00 AS Persen1
				FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
				WHERE traccountsaldo.NoAccount = '50000000' ;

        SELECT Saldo INTO Pendapatan FROM traccountsaldo WHERE NoAccount = '30000000';
				SELECT Saldo INTO PendapatanOp FROM traccountsaldo WHERE NoAccount = '40000000';
				SELECT Saldo INTO HPP FROM traccountsaldo WHERE NoAccount = '50000000';
				SELECT Saldo INTO Beban FROM traccountsaldo WHERE NoAccount = '60000000';
				SELECT Saldo INTO Pendapatan2 FROM traccountsaldo WHERE NoAccount = '90000000';
				SELECT Saldo INTO Beban2 FROM traccountsaldo WHERE NoAccount = '80000000';

        /* MyPenjualan untuk Pembagi */
        SELECT Saldo INTO MyPenjualan FROM traccountsaldo WHERE NoAccount = '30010000';

        INSERT INTO LabaRugiTemp SELECT 'Laba Kotor' AS GroupAccount, 'Laba Kotor' AS GroupNama ,
				'Laba Kotor' AS NoAccount, 'Laba Kotor' AS NamaAccount , 0 AS Unit1, Pendapatan + PendapatanOp - HPP  AS Colom1, 00.00 AS Persen1;
				INSERT INTO LabaRugiTemp SELECT traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount AS GroupNama ,
				traccountsaldo.NoAccount, traccountsaldo.NamaAccount , 0 AS Unit1, Saldo AS Colom1, 00.00 AS Persen1
				FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
				WHERE traccountsaldo.NoAccount LIKE '6%' AND traccountsaldo.LevelAccount = 1;
				INSERT INTO LabaRugiTemp SELECT 'Laba Operasi' AS GroupAccount, 'Laba Operasi' AS GroupNama ,
				'Laba Operasi' AS NoAccount, 'Laba Operasi' AS NamaAccount , 0 AS Unit1, Pendapatan + PendapatanOp - HPP - Beban  AS Colom1, 00.00 AS Persen1;
				INSERT INTO LabaRugiTemp SELECT '80000000' AS GroupAccount, 'Biaya dan Pendapatan Diluar Operasi' AS GroupNama ,
				traccountsaldo.NoAccount AS NoAccount, traccountsaldo.NamaAccount AS NamaAccount , 0 AS Unit1, Saldo AS Colom1, 00.00 AS Persen1
				FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
				WHERE traccountsaldo.NoAccount = '80000000' ;
				INSERT INTO LabaRugiTemp SELECT '80000000' AS GroupAccount, 'Biaya dan Pendapatan Diluar Operasi' AS GroupNama ,
				traccountsaldo.NoAccount AS NoAccount, traccountsaldo.NamaAccount AS NamaAccount , 0 AS Unit1, Saldo AS Colom1, 00.00 AS Persen1
				FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
				WHERE traccountsaldo.NoAccount = '90000000' ;
				INSERT INTO LabaRugiTemp SELECT 'Laba Bersih' AS GroupAccount, 'Laba Bersih' AS GroupNama ,
				'Laba Bersih' AS NoAccount, 'Laba Bersih' AS NamaAccount , 0 AS Unit1, Pendapatan + PendapatanOp - HPP- Beban + Pendapatan2 - Beban2  AS Colom1, 00.00 AS Persen1;

        SELECT Saldo INTO TambahanPotonganHarga FROM traccountsaldo WHERE NoAccount = '60060000';
        UPDATE LabaRugiTemp SET Colom1 = 0 WHERE NoAccount ='60060000';
        UPDATE LabaRugiTemp SET Colom1 = -TambahanPotonganHarga WHERE NoAccount ='30050000';
        UPDATE LabaRugiTemp SET Colom1 = Pendapatan + PendapatanOp - HPP - TambahanPotonganHarga WHERE NoAccount ='Laba Kotor';

			IF X = 1 THEN
				INSERT INTO LabaRugi
      		SELECT LabaRugiTemp.GroupAccount, LabaRugiTemp.GroupNama,
      		LabaRugiTemp.NoAccount, LabaRugiTemp.NamaAccount,
				000000 AS Unit1, 0000000000000000.00 AS Colom1, 000.00 AS Persen1,
				000000 AS Unit2, 0000000000000000.00 AS Colom2, 000.00 AS Persen2,
				000000 AS Unit3, LabaRugiTemp.Colom1 AS Colom3, 000.00 AS Persen3,
				000000 AS Unit4, 0000000000000000.00 AS Colom4, 000.00 AS Persen4,
				000000 AS Unit5, 0000000000000000.00 AS Colom5, 000.00 AS Persen5,
				000000 AS Unit6, 0000000000000000.00 AS Colom6, 000.00 AS Persen6,
				000000 AS Unit7, 0000000000000000.00 AS Colom7, 000.00 AS Persen7,
				000000 AS Unit8, 0000000000000000.00 AS Colom8, 000.00 AS Persen8,
				000000 AS Unit9, 0000000000000000.00 AS Colom9, 000.00 AS Persen9,
				000000 AS Unit10, 0000000000000000.00 AS Colom10, 000.00 AS Persen10,
				000000 AS Unit11, 0000000000000000.00 AS Colom11, 000.00 AS Persen11,
				000000 AS Unit12, 0000000000000000.00 AS Colom12, 000.00 AS Persen12,
				000000 AS UnitT, 0000000000000000.00 AS ColomT, 000.00 AS PersenT
				FROM LabaRugiTemp;
				UPDATE LabaRugi SET Unit3 = UnitJual + UnitSaldo WHERE NoAccount = '30010000';
				UPDATE LabaRugi SET Unit3 = -UnitRetur WHERE NoAccount = '30020000';
				UPDATE LabaRugi SET Persen3 = CAST((Colom3/MyPenjualan*100 ) AS DECIMAL(5,2));
			END IF;
			IF X = 2 THEN
				UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Colom2 = LabaRugiTemp.Colom1;
				UPDATE LabaRugi SET Unit2 = UnitJual  + UnitSaldo WHERE NoAccount = '30010000';
				UPDATE LabaRugi SET Unit2 = -UnitRetur WHERE NoAccount = '30020000';
				UPDATE LabaRugi SET Persen2 = CAST((Colom2/MyPenjualan*100 ) AS DECIMAL(5,2));
			END IF;
			IF X = 3 THEN
				UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Colom1 = LabaRugiTemp.Colom1;
				UPDATE LabaRugi SET Unit1 = UnitJual  + UnitSaldo WHERE NoAccount = '30010000';
				UPDATE LabaRugi SET Unit1 = -UnitRetur WHERE NoAccount = '30020000';
				UPDATE LabaRugi SET Persen1 = CAST((Colom1/MyPenjualan*100 ) AS DECIMAL(5,2));
			END IF;
			SET TglAwal = DATE_ADD(LAST_DAY(DATE_ADD(TglAwal, INTERVAL -2 MONTH)),INTERVAL 1 DAY);
		   SET TglAkhir = LAST_DAY(DATE_ADD(TglAkhir, INTERVAL -1 MONTH));

      	ITERATE  loop_label;
			ELSE
				LEAVE  loop_label;
			END  IF;
		END LOOP;

		UPDATE LabaRugi SET ColomT = Colom1 + Colom2 + Colom3;
		UPDATE LabaRugi SET UnitT = Unit1 + Unit2 + Unit3;
		UPDATE LabaRugi SET PersenT = CAST(((Persen1 + Persen2 + Persen3)/3) AS DECIMAL(5,2));

		SET @MyQuery = CONCAT("SELECT * FROM LabaRugi;");
	END IF;	
     
	IF xTipe = "Laba-Rugi Semester" THEN
		IF MONTH(TglAwal) < 7 THEN
			SET TglAwal  = STR_TO_DATE( CONCAT( YEAR( TglAwal ) , '-', '06' , '-', '01' ) , '%Y-%m-%d' ) ;
			SET TglAkhir = STR_TO_DATE( CONCAT( YEAR( TglAwal ) , '-', '06' , '-', '30' ) , '%Y-%m-%d' ) ;
		ELSE
			SET TglAwal  = STR_TO_DATE( CONCAT( YEAR( TglAwal ) , '-', '12' , '-', '01' ) , '%Y-%m-%d' ) ;
			SET TglAkhir = STR_TO_DATE( CONCAT( YEAR( TglAwal ) , '-', '12' , '-', '31' ) , '%Y-%m-%d' ) ;
		END IF;

		DROP TEMPORARY TABLE IF EXISTS LabaRugi ;
		CREATE TEMPORARY TABLE IF NOT EXISTS LabaRugi  AS
		SELECT SPACE(50) AS GroupAccount, SPACE(150) AS GroupNama,
		SPACE(50) AS NoAccount, SPACE(150) AS NamaAccount,
		000000 AS Unit1, 0000000000000000.00 AS Colom1, 000.00 AS Persen1,
		000000 AS Unit2, 0000000000000000.00 AS Colom2, 000.00 AS Persen2,
		000000 AS Unit3, 0000000000000000.00 AS Colom3, 000.00 AS Persen3,
		000000 AS Unit4, 0000000000000000.00 AS Colom4, 000.00 AS Persen4,
		000000 AS Unit5, 0000000000000000.00 AS Colom5, 000.00 AS Persen5,
		000000 AS Unit6, 0000000000000000.00 AS Colom6, 000.00 AS Persen6,
		000000 AS Unit7, 0000000000000000.00 AS Colom7, 000.00 AS Persen7,
		000000 AS Unit8, 0000000000000000.00 AS Colom8, 000.00 AS Persen8,
		000000 AS Unit9, 0000000000000000.00 AS Colom9, 000.00 AS Persen9,
		000000 AS Unit10, 0000000000000000.00 AS Colom10, 000.00 AS Persen10,
		000000 AS Unit11, 0000000000000000.00 AS Colom11, 000.00 AS Persen11,
		000000 AS Unit12, 0000000000000000.00 AS Colom12, 000.00 AS Persen12,
		000000 AS UnitT, 0000000000000000.00 AS ColomT, 000.00 AS PersenT;
		CREATE INDEX NoAccount ON LabaRugi(NoAccount);
		DELETE FROM LabaRugi;

		SET X = 0;
		loop_label:  LOOP
			IF  X < 6 THEN
				SET  X = X + 1;
				CALL rglNeraca(TglAwal, TglAkhir, TglAwal);

				SELECT COUNT(SKNO) INTO UnitJual FROM ttsk INNER JOIN ttgeneralledgerhd ON ttsk.NoGL = ttgeneralledgerhd.NoGL WHERE ttsk.SKTgl BETWEEN TglAwal AND TglAkhir;
				SELECT IFNULL(JumUnit,0) INTO UnitSaldo FROM tsaldounit WHERE TglUnit BETWEEN TglAwal AND TglAkhir AND NoAccount = '30010000';
				SELECT COUNT(RKNO) INTO UnitRetur FROM ttrk INNER JOIN ttgeneralledgerhd ON ttrk.NoGL = ttgeneralledgerhd.NoGL WHERE ttrk.RKTgl BETWEEN TglAwal AND TglAkhir;
				
				DROP TEMPORARY TABLE IF EXISTS LabaRugiTemp ;
				CREATE TEMPORARY TABLE IF NOT EXISTS LabaRugiTemp  AS
				SELECT SPACE(50) AS GroupAccount, SPACE(150) AS GroupNama,
				SPACE(50) AS NoAccount, SPACE(150) AS NamaAccount,
				000000 AS Unit1, 0000000000000000.00 AS Colom1, 00.00 AS Persen1;
				CREATE INDEX NoAccount ON LabaRugiTemp(NoAccount);
				DELETE FROM LabaRugiTemp;
				INSERT INTO LabaRugiTemp SELECT traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount AS GroupNama ,
				traccountsaldo.NoAccount, traccountsaldo.NamaAccount , 0 AS Unit1, Saldo AS Colom1, 00.00 AS Persen1
				FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
				WHERE traccountsaldo.NoAccount LIKE '3%' AND traccountsaldo.LevelAccount = 1;
				INSERT INTO LabaRugiTemp SELECT '30000000' AS GroupAccount, 'Penjualan' AS GroupNama ,
				traccountsaldo.NoAccount, traccountsaldo.NamaAccount , 0 AS Unit1, Saldo AS Colom1, 00.00 AS Persen1
				FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
				WHERE traccountsaldo.NoAccount = '40000000' ;
				INSERT INTO LabaRugiTemp SELECT 'Harga Pokok Penjualan' AS GroupAccount, 'Harga Pokok Penjualan' AS GroupNama ,
				'Harga Pokok Penjualan' AS NoAccount, 'Harga Pokok Penjualan' AS NamaAccount , 0 AS Unit1, Saldo AS Colom1, 00.00 AS Persen1
				FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
				WHERE traccountsaldo.NoAccount = '50000000' ;
				SELECT Saldo INTO Pendapatan FROM traccountsaldo WHERE NoAccount = '30000000';
				SELECT Saldo INTO PendapatanOp FROM traccountsaldo WHERE NoAccount = '40000000';
				SELECT Saldo INTO HPP FROM traccountsaldo WHERE NoAccount = '50000000';
				SELECT Saldo INTO Beban FROM traccountsaldo WHERE NoAccount = '60000000';
				SELECT Saldo INTO Pendapatan2 FROM traccountsaldo WHERE NoAccount = '90000000';
				SELECT Saldo INTO Beban2 FROM traccountsaldo WHERE NoAccount = '80000000';
				
				/* MyPenjualan untuk Pembagi */
				SELECT Saldo INTO MyPenjualan FROM traccountsaldo WHERE NoAccount = '30010000';
        
				INSERT INTO LabaRugiTemp SELECT 'Laba Kotor' AS GroupAccount, 'Laba Kotor' AS GroupNama ,
				'Laba Kotor' AS NoAccount, 'Laba Kotor' AS NamaAccount , 0 AS Unit1, Pendapatan + PendapatanOp - HPP  AS Colom1, 00.00 AS Persen1;
				INSERT INTO LabaRugiTemp SELECT traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount AS GroupNama ,
				traccountsaldo.NoAccount, traccountsaldo.NamaAccount , 0 AS Unit1, Saldo AS Colom1, 00.00 AS Persen1
				FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
				WHERE traccountsaldo.NoAccount LIKE '6%' AND traccountsaldo.LevelAccount = 1;
				INSERT INTO LabaRugiTemp SELECT 'Laba Operasi' AS GroupAccount, 'Laba Operasi' AS GroupNama ,
				'Laba Operasi' AS NoAccount, 'Laba Operasi' AS NamaAccount , 0 AS Unit1, Pendapatan + PendapatanOp - HPP - Beban  AS Colom1, 00.00 AS Persen1;
				INSERT INTO LabaRugiTemp SELECT '80000000' AS GroupAccount, 'Biaya dan Pendapatan Diluar Operasi' AS GroupNama ,
				traccountsaldo.NoAccount AS NoAccount, traccountsaldo.NamaAccount AS NamaAccount , 0 AS Unit1, Saldo AS Colom1, 00.00 AS Persen1
				FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
				WHERE traccountsaldo.NoAccount = '80000000' ;
				INSERT INTO LabaRugiTemp SELECT '80000000' AS GroupAccount, 'Biaya dan Pendapatan Diluar Operasi' AS GroupNama ,
				traccountsaldo.NoAccount AS NoAccount, traccountsaldo.NamaAccount AS NamaAccount , 0 AS Unit1, Saldo AS Colom1, 00.00 AS Persen1
				FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
				WHERE traccountsaldo.NoAccount = '90000000' ;
				INSERT INTO LabaRugiTemp SELECT 'Laba Bersih' AS GroupAccount, 'Laba Bersih' AS GroupNama ,
				'Laba Bersih' AS NoAccount, 'Laba Bersih' AS NamaAccount , 0 AS Unit1, Pendapatan + PendapatanOp - HPP- Beban + Pendapatan2 - Beban2  AS Colom1, 00.00 AS Persen1;
        
            SELECT Saldo INTO TambahanPotonganHarga FROM traccountsaldo WHERE NoAccount = '60060000';
				UPDATE LabaRugiTemp SET Colom1 = 0 WHERE NoAccount ='60060000';
				UPDATE LabaRugiTemp SET Colom1 = -TambahanPotonganHarga WHERE NoAccount ='30050000';
				UPDATE LabaRugiTemp SET Colom1 = Pendapatan + PendapatanOp - HPP - TambahanPotonganHarga WHERE NoAccount ='Laba Kotor';
        
        IF X = 1 THEN
				INSERT INTO LabaRugi
      		SELECT LabaRugiTemp.GroupAccount, LabaRugiTemp.GroupNama,
      		LabaRugiTemp.NoAccount, LabaRugiTemp.NamaAccount,
				000000 AS Unit1, 0000000000000000.00 AS Colom1, 000.00 AS Persen1,
				000000 AS Unit2, 0000000000000000.00 AS Colom2, 000.00 AS Persen2,
				000000 AS Unit3, 0000000000000000.00 AS Colom3, 000.00 AS Persen3,
				000000 AS Unit4, 0000000000000000.00 AS Colom4, 000.00 AS Persen4,
				000000 AS Unit5, 0000000000000000.00 AS Colom5, 000.00 AS Persen5,
				000000 AS Unit6, LabaRugiTemp.Colom1 AS Colom6, 000.00 AS Persen6,
				000000 AS Unit7, 0000000000000000.00 AS Colom7, 000.00 AS Persen7,
				000000 AS Unit8, 0000000000000000.00 AS Colom8, 000.00 AS Persen8,
				000000 AS Unit9, 0000000000000000.00 AS Colom9, 000.00 AS Persen9,
				000000 AS Unit10, 0000000000000000.00 AS Colom10, 000.00 AS Persen10,
				000000 AS Unit11, 0000000000000000.00 AS Colom11, 000.00 AS Persen11,
				000000 AS Unit12, 0000000000000000.00 AS Colom12, 000.00 AS Persen12,
				000000 AS UnitT, 0000000000000000.00 AS ColomT, 000.00 AS PersenT
				FROM LabaRugiTemp;
				UPDATE LabaRugi SET Unit6 = UnitJual + UnitSaldo WHERE NoAccount = '30010000';
				UPDATE LabaRugi SET Unit6 = -UnitRetur WHERE NoAccount = '30020000';
				UPDATE LabaRugi SET Persen6 = CAST((Colom6/MyPenjualan*100 ) AS DECIMAL(5,2));
			END IF;
			IF X = 2 THEN
				UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Colom5 = LabaRugiTemp.Colom1;
				UPDATE LabaRugi SET Unit5 = UnitJual + UnitSaldo WHERE NoAccount = '30010000';
				UPDATE LabaRugi SET Unit5 = -UnitRetur WHERE NoAccount = '30020000';
				UPDATE LabaRugi SET Persen5 = CAST((Colom5/MyPenjualan*100 ) AS DECIMAL(5,2));
			END IF;
			IF X = 3 THEN
				UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Colom4 = LabaRugiTemp.Colom1;
				UPDATE LabaRugi SET Unit4 = UnitJual + UnitSaldo WHERE NoAccount = '30010000';
				UPDATE LabaRugi SET Unit4 = -UnitRetur WHERE NoAccount = '30020000';
				UPDATE LabaRugi SET Persen4 = CAST((Colom4/MyPenjualan*100 ) AS DECIMAL(5,2));
			END IF;
			IF X = 4 THEN
				UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Colom3 = LabaRugiTemp.Colom1;
				UPDATE LabaRugi SET Unit3 = UnitJual + UnitSaldo WHERE NoAccount = '30010000';
				UPDATE LabaRugi SET Unit3 = -UnitRetur WHERE NoAccount = '30020000';
				UPDATE LabaRugi SET Persen3 = CAST((Colom3/MyPenjualan*100 ) AS DECIMAL(5,2));
			END IF;
			IF X = 5 THEN
				UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Colom2 = LabaRugiTemp.Colom1;
				UPDATE LabaRugi SET Unit2 = UnitJual + UnitSaldo WHERE NoAccount = '30010000';
				UPDATE LabaRugi SET Unit2 = -UnitRetur WHERE NoAccount = '30020000';
				UPDATE LabaRugi SET Persen2 = CAST((Colom2/MyPenjualan*100 ) AS DECIMAL(5,2));
			END IF;
			IF X = 6 THEN
				UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Colom1 = LabaRugiTemp.Colom1;
				UPDATE LabaRugi SET Unit1 = UnitJual + UnitSaldo WHERE NoAccount = '30010000';
				UPDATE LabaRugi SET Unit1 = -UnitRetur WHERE NoAccount = '30020000';
				UPDATE LabaRugi SET Persen1 = CAST((Colom1/MyPenjualan*100 ) AS DECIMAL(5,2));
			END IF;

			SET TglAwal = DATE_ADD(LAST_DAY(DATE_ADD(TglAwal, INTERVAL -2 MONTH)),INTERVAL 1 DAY);
		   SET TglAkhir = LAST_DAY(DATE_ADD(TglAkhir, INTERVAL -1 MONTH));

      	ITERATE  loop_label;
			ELSE
				LEAVE  loop_label;
			END  IF;
		END LOOP;

		UPDATE LabaRugi SET ColomT = Colom1 + Colom2 + Colom3+Colom4 + Colom5 + Colom6;
		UPDATE LabaRugi SET UnitT = Unit1 + Unit2 + Unit3+Unit4 + Unit5 + Unit6;
		UPDATE LabaRugi SET PersenT = CAST(((Persen1 + Persen2 + Persen3+Persen4 + Persen5 + Persen6)/6) AS DECIMAL(5,2));
	

		SET @MyQuery = CONCAT("SELECT * FROM LabaRugi;");
	END IF;	

	IF xTipe = "Pendapatan Tri" THEN
		DROP TEMPORARY TABLE IF EXISTS LabaRugi ;
		CREATE TEMPORARY TABLE IF NOT EXISTS LabaRugi  AS
		SELECT SPACE(50) AS GroupAccount, SPACE(150) AS GroupNama,
		SPACE(50) AS NoAccount, SPACE(150) AS NamaAccount,
		000000 AS Unit1, 0000000000000000.00 AS Colom1, 000.00 AS Persen1,
		000000 AS Unit2, 0000000000000000.00 AS Colom2, 000.00 AS Persen2,
		000000 AS Unit3, 0000000000000000.00 AS Colom3, 000.00 AS Persen3,
		000000 AS Unit4, 0000000000000000.00 AS Colom4, 000.00 AS Persen4,
		000000 AS Unit5, 0000000000000000.00 AS Colom5, 000.00 AS Persen5,
		000000 AS Unit6, 0000000000000000.00 AS Colom6, 000.00 AS Persen6,
		000000 AS Unit7, 0000000000000000.00 AS Colom7, 000.00 AS Persen7,
		000000 AS Unit8, 0000000000000000.00 AS Colom8, 000.00 AS Persen8,
		000000 AS Unit9, 0000000000000000.00 AS Colom9, 000.00 AS Persen9,
		000000 AS Unit10, 0000000000000000.00 AS Colom10, 000.00 AS Persen10,
		000000 AS Unit11, 0000000000000000.00 AS Colom11, 000.00 AS Persen11,
		000000 AS Unit12, 0000000000000000.00 AS Colom12, 000.00 AS Persen12,
		000000 AS UnitT, 0000000000000000.00 AS ColomT, 000.00 AS PersenT;
		CREATE INDEX NoAccount ON LabaRugi(NoAccount);
		DELETE FROM LabaRugi;

		SET X = 0;
		loop_label:  LOOP
			IF  X < 3 THEN
				SET  X = X + 1;
				CALL rglNeraca(TglAwal, TglAkhir, TglAwal);
				DROP TEMPORARY TABLE IF EXISTS LabaRugiTemp ;
				CREATE TEMPORARY TABLE IF NOT EXISTS LabaRugiTemp  AS
				SELECT SPACE(50) AS GroupAccount, SPACE(150) AS GroupNama,
				SPACE(50) AS NoAccount, SPACE(150) AS NamaAccount,
				000000 AS Unit1, 0000000000000000.00 AS Colom1, 00.00 AS Persen1;
				CREATE INDEX NoAccount ON LabaRugiTemp(NoAccount);

				DELETE FROM LabaRugiTemp;
				INSERT INTO LabaRugiTemp SELECT traccountsaldo.NoParent AS GroupAccount, traccount.NamaAccount AS GroupNama ,
				traccountsaldo.NoAccount, CONCAT(REPEAT(SPACE(19),traccountsaldo.LevelAccount-1),traccountsaldo.NamaAccount) AS NamaAccount , 0 AS Unit1, Saldo AS Colom1, 00.00 AS Persen1
				FROM traccountsaldo INNER JOIN traccount ON traccount.NoAccount = traccountsaldo.NoParent
				WHERE traccountsaldo.NoAccount LIKE '4%' AND traccountsaldo.NoParent <> '00000000';
				
				SELECT Saldo INTO MyPenjualan FROM traccountsaldo WHERE NoAccount = '40000000';

				IF X = 1 THEN
					INSERT INTO LabaRugi
					SELECT LabaRugiTemp.GroupAccount, LabaRugiTemp.GroupNama,
					LabaRugiTemp.NoAccount, LabaRugiTemp.NamaAccount,
					000000 AS Unit1, 0000000000000000.00 AS Colom1, 000.00 AS Persen1,
					000000 AS Unit2, 0000000000000000.00 AS Colom2, 000.00 AS Persen2,
					000000 AS Unit3, LabaRugiTemp.Colom1 AS Colom3, 000.00 AS Persen3,
					000000 AS Unit4, 0000000000000000.00 AS Colom4, 000.00 AS Persen4,
					000000 AS Unit5, 0000000000000000.00 AS Colom5, 000.00 AS Persen5,
					000000 AS Unit6, 0000000000000000.00 AS Colom6, 000.00 AS Persen6,
					000000 AS Unit7, 0000000000000000.00 AS Colom7, 000.00 AS Persen7,
					000000 AS Unit8, 0000000000000000.00 AS Colom8, 000.00 AS Persen8,
					000000 AS Unit9, 0000000000000000.00 AS Colom9, 000.00 AS Persen9,
					000000 AS Unit10, 0000000000000000.00 AS Colom10, 000.00 AS Persen10,
					000000 AS Unit11, 0000000000000000.00 AS Colom11, 000.00 AS Persen11,
					000000 AS Unit12, 0000000000000000.00 AS Colom12, 000.00 AS Persen12,
					000000 AS UnitT, 0000000000000000.00 AS ColomT, 000.00 AS PersenT
					FROM LabaRugiTemp;
					UPDATE LabaRugi SET Persen3 = CAST((Colom2/MyPenjualan*100 ) AS DECIMAL(5,2));
				END IF;
				IF X = 2 THEN
					UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Colom2 = LabaRugiTemp.Colom1;
					UPDATE LabaRugi SET Persen2 = CAST((Colom2/MyPenjualan*100 ) AS DECIMAL(5,2));
				END IF;
				IF X = 3 THEN
					UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Colom1 = LabaRugiTemp.Colom1;
					UPDATE LabaRugi SET Persen1 = CAST((Colom1/MyPenjualan*100 ) AS DECIMAL(5,2));
				END IF;
				SET TglAwal = DATE_ADD(LAST_DAY(DATE_ADD(TglAwal, INTERVAL -2 MONTH)),INTERVAL 1 DAY);
				SET TglAkhir = LAST_DAY(DATE_ADD(TglAkhir, INTERVAL -1 MONTH));
      	ITERATE  loop_label;
			ELSE
				LEAVE  loop_label;
			END  IF;
		END LOOP;
		
		UPDATE LabaRugi SET ColomT = Colom1 + Colom2 + Colom3;
		UPDATE LabaRugi SET UnitT = Unit1 + Unit2 + Unit3;
		UPDATE LabaRugi SET PersenT = CAST(((Persen1 + Persen2 + Persen3)/3) AS DECIMAL(5,2));

		SET @MyQuery = CONCAT("SELECT * FROM LabaRugi;");
	END IF;	

	IF xTipe = "Laba-Rugi Mini" THEN
		IF MONTH(TglAwal) < 7 THEN
			SET TglAwal  = STR_TO_DATE( CONCAT( YEAR( TglAwal ) , '-', '06' , '-', '01' ) , '%Y-%m-%d' ) ;
			SET TglAkhir = STR_TO_DATE( CONCAT( YEAR( TglAwal ) , '-', '06' , '-', '30' ) , '%Y-%m-%d' ) ;
		ELSE
			SET TglAwal  = STR_TO_DATE( CONCAT( YEAR( TglAwal ) , '-', '12' , '-', '01' ) , '%Y-%m-%d' ) ;
			SET TglAkhir = STR_TO_DATE( CONCAT( YEAR( TglAwal ) , '-', '12' , '-', '31' ) , '%Y-%m-%d' ) ;
		END IF;

		DROP TEMPORARY TABLE IF EXISTS LabaRugi ;
		CREATE TEMPORARY TABLE IF NOT EXISTS LabaRugi  AS
		SELECT SPACE(50) AS GroupAccount, SPACE(150) AS GroupNama,
		SPACE(50) AS NoAccount, SPACE(150) AS NamaAccount,
		000000 AS Unit1, 0000000000000000.00 AS Colom1, 000.00 AS Persen1,
		000000 AS Unit2, 0000000000000000.00 AS Colom2, 000.00 AS Persen2,
		000000 AS Unit3, 0000000000000000.00 AS Colom3, 000.00 AS Persen3,
		000000 AS Unit4, 0000000000000000.00 AS Colom4, 000.00 AS Persen4,
		000000 AS Unit5, 0000000000000000.00 AS Colom5, 000.00 AS Persen5,
		000000 AS Unit6, 0000000000000000.00 AS Colom6, 000.00 AS Persen6,
		000000 AS Unit7, 0000000000000000.00 AS Colom7, 000.00 AS Persen7,
		000000 AS Unit8, 0000000000000000.00 AS Colom8, 000.00 AS Persen8,
		000000 AS Unit9, 0000000000000000.00 AS Colom9, 000.00 AS Persen9,
		000000 AS Unit10, 0000000000000000.00 AS Colom10, 000.00 AS Persen10,
		000000 AS Unit11, 0000000000000000.00 AS Colom11, 000.00 AS Persen11,
		000000 AS Unit12, 0000000000000000.00 AS Colom12, 000.00 AS Persen12,
		000000 AS UnitT, 0000000000000000.00 AS ColomT, 000.00 AS PersenT;
		CREATE INDEX NoAccount ON LabaRugi(NoAccount);
		CREATE INDEX NamaAccount ON LabaRugi(NamaAccount);
		DELETE FROM LabaRugi;

		SET X = 0;
		loop_label:  LOOP
			IF  X < 6 THEN
				SET  X = X + 1;

				SELECT IFNULL(COUNT(SKNO),0) INTO UnitJual FROM ttsk INNER JOIN ttgeneralledgerhd ON ttsk.NoGL = ttgeneralledgerhd.NoGL WHERE ttsk.SKTgl BETWEEN  TglAwal AND TglAkhir;
				SELECT IFNULL(JumUnit,0) INTO UnitServis FROM tsaldounit WHERE TglUnit BETWEEN  TglAwal AND TglAkhir AND NoAccount = '30010000';
				SET UnitJual = UnitJual + UnitServis;
				SELECT COUNT(RKNO) INTO UnitRetur FROM ttrk INNER JOIN ttgeneralledgerhd ON ttRk.NoGL = ttgeneralledgerhd.NoGL WHERE ttrk.RKTgl BETWEEN  TglAwal AND TglAkhir;
				SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO Penjualan FROM ttgeneralledgerit
				INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
				WHERE NoAccount = '30010000'  AND ttgeneralledgerit.TglGL  BETWEEN  TglAwal AND TglAkhir;
				SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO Retur FROM ttgeneralledgerit
				INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
				WHERE NoAccount = '30020000'  AND ttgeneralledgerit.TglGL  BETWEEN  TglAwal AND TglAkhir;

				SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO SD1 FROM ttgeneralledgerit
				INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
				WHERE NoAccount = '30030000'  AND ttgeneralledgerit.TglGL  BETWEEN  TglAwal AND TglAkhir;

				SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO SD2 FROM ttgeneralledgerit
				INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
				WHERE (NoAccount = '30040000' OR  NoAccount = '60060000') AND ttgeneralledgerit.TglGL  BETWEEN  TglAwal AND TglAkhir;

				SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO PendapatanOp FROM ttgeneralledgerit
				INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
				WHERE LEFT(NoAccount,1) = '4'  AND ttgeneralledgerit.TglGL  BETWEEN  TglAwal AND TglAkhir;

				SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO HPP FROM ttgeneralledgerit
				INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
				WHERE LEFT(NoAccount,1) = '5'  AND ttgeneralledgerit.TglGL  BETWEEN  TglAwal AND TglAkhir;

				SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO BBN FROM ttgeneralledgerit
				INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
				WHERE (NoAccount = '60050000' OR NoAccount = '60140000') AND ttgeneralledgerit.TglGL  BETWEEN  TglAwal AND TglAkhir;

				SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO Insentif FROM ttgeneralledgerit
				INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
				WHERE (NoAccount = '60020000' OR NoAccount = '60030000')  AND ttgeneralledgerit.TglGL  BETWEEN  TglAwal AND TglAkhir;

				SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO Gaji FROM ttgeneralledgerit
				INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
				WHERE  LEFT(NoAccount,4) = '6001' AND ttgeneralledgerit.TglGL  BETWEEN  TglAwal AND TglAkhir;

				SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO Pemeliharaan FROM ttgeneralledgerit
				INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
				WHERE  LEFT(NoAccount,4) = '6027'  AND ttgeneralledgerit.TglGL  BETWEEN  TglAwal AND TglAkhir;

				SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO Promosi FROM ttgeneralledgerit
				INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
				WHERE LEFT(NoAccount,4) = '6009'  AND ttgeneralledgerit.TglGL  BETWEEN   TglAwal AND TglAkhir;

				SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO Pajak FROM ttgeneralledgerit
				INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
				WHERE LEFT(NoAccount,4) = '6030'  AND ttgeneralledgerit.TglGL  BETWEEN  TglAwal AND TglAkhir;

				SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO Other FROM ttgeneralledgerit
				INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
				WHERE NoAccount NOT IN ('60020000','60030000','60050000','60140000',60060000) AND LEFT(NoAccount,1) = '6'
				AND LEFT(NoAccount,4) <> '6001' AND LEFT(NoAccount,4) <> '6027'  AND LEFT(NoAccount,4) <> '6009'   AND LEFT(NoAccount,4) <> '6030'
				AND ttgeneralledgerit.TglGL  BETWEEN  TglAwal AND TglAkhir;

				SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO PendapatanNonOp FROM ttgeneralledgerit
				INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
				WHERE LEFT(NoAccount,1) = '9' AND ttgeneralledgerit.TglGL  BETWEEN  TglAwal AND TglAkhir;

				DROP TEMPORARY TABLE IF EXISTS LabaRugiTemp ;
				CREATE TEMPORARY TABLE IF NOT EXISTS LabaRugiTemp LIKE LabaRugi;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "QUANTITY SALES ( UNIT TERJUAL )" AS NamaAccount   ,UnitJual - UnitRetur,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "PENJUALAN ( Minus Retur )" AS NamaAccount         ,0,Penjualan + Retur,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "           SD1" AS NamaAccount                    ,SD1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "           SD2 + RETUR HARGA" AS NamaAccount      ,SD2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "           PENDAPATAN OPERASIONAL" AS NamaAccount ,PendapatanOp,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "     TOTAL POTONGAN" AS NamaAccount               ,0, SD1 + SD2 + PendapatanOp,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "     NET SALES / PENJUALAN BERSIH" AS NamaAccount ,0,Penjualan + Retur + SD1 + SD2 + PendapatanOp,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "HPP DAN BBN" AS NamaAccount                       ,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "           HPP" AS NamaAccount                    ,0,HPP,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "           BBN ( STCK, CEK FISIK dsb.)" AS NamaAccount,0,BBN,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "     TOTAL HPP + BBN" AS NamaAccount              ,0,HPP + BBN,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "     GROSS PROFIT  / LABA KOTOR" AS NamaAccount   ,0,Penjualan + Retur + SD1 + SD2 + PendapatanOp - HPP - BBN,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "BIAYA OPERASIONAL" AS NamaAccount                 ,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "           BIAYA GAJI, TUNJ, LEMBUR" AS NamaAccount,0,Gaji,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "           BIAYA INSENTIF" AS NamaAccount         ,0,Insentif,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "           BIAYA PEMELIHARAAN" AS NamaAccount     ,0,Pemeliharaan,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "           BIAYA PROMOSI" AS NamaAccount          ,0,Promosi,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "           BIAYA PAJAK & PERIJINAN" AS NamaAccount,0,Pajak,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "           OTHERS(LAIN - LAIN)" AS NamaAccount    ,0,Other,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "     TOTAL BIAYA OPERASIONAL" AS NamaAccount      ,0,Gaji + Insentif + Pemeliharaan + Promosi + Pajak + Other,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "     PENDAPATAN NON OPERASIONAL" AS NamaAccount   ,0,PendapatanNonOp,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				INSERT INTO LabaRugiTemp SELECT SPACE(50) , SPACE(150) ,SPACE(50) ,  "  NET PROFIT / LABA BERSIH" AS NamaAccount   ,0, Penjualan + Retur + SD1 + SD2 + PendapatanOp + PendapatanNonOp - HPP - BBN - (Gaji + Insentif + Pemeliharaan + Promosi + Pajak + Other),0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
				UPDATE LabaRugiTemp SET NoAccount = NamaAccount;
				IF (Penjualan + Retur) > 0 THEN
					UPDATE LabaRugiTemp SET Persen1 = (Unit1 + Colom1)/(Penjualan + Retur) * 100;
				ELSE
					UPDATE LabaRugiTemp SET Persen1 = 0;
				END IF;
				SET MyPenjualan = 1;
				IF X = 1 THEN
					INSERT INTO LabaRugi
					SELECT LabaRugiTemp.GroupAccount, LabaRugiTemp.GroupNama,
					LabaRugiTemp.NoAccount, LabaRugiTemp.NamaAccount,
					000000 AS Unit1, 0000000000000000.00 AS Colom1, 000.00 AS Persen1,
					000000 AS Unit2, 0000000000000000.00 AS Colom2, 000.00 AS Persen2,
					000000 AS Unit3, 0000000000000000.00 AS Colom3, 000.00 AS Persen3,
					000000 AS Unit4, 0000000000000000.00 AS Colom4, 000.00 AS Persen4,
					000000 AS Unit5, 0000000000000000.00 AS Colom5, 000.00 AS Persen5,
					LabaRugiTemp.Unit1 AS Unit6, LabaRugiTemp.Colom1 AS Colom6, LabaRugiTemp.Persen1 AS Persen6,
					000000 AS Unit7, 0000000000000000.00 AS Colom7, 000.00 AS Persen7,
					000000 AS Unit8, 0000000000000000.00 AS Colom8, 000.00 AS Persen8,
					000000 AS Unit9, 0000000000000000.00 AS Colom9, 000.00 AS Persen9,
					000000 AS Unit10, 0000000000000000.00 AS Colom10, 000.00 AS Persen10,
					000000 AS Unit11, 0000000000000000.00 AS Colom11, 000.00 AS Persen11,
					000000 AS Unit12, 0000000000000000.00 AS Colom12, 000.00 AS Persen12,
					000000 AS UnitT, 0000000000000000.00 AS ColomT, 000.00 AS PersenT
					FROM LabaRugiTemp;
					UPDATE LabaRugi SET Persen6 = CAST((Colom6/MyPenjualan*100 ) AS DECIMAL(5,2));
				END IF;
				IF X = 2 THEN
					UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Colom5 = LabaRugiTemp.Colom1;
					UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Unit5 = LabaRugiTemp.Unit1;
					UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Persen5 = LabaRugiTemp.Persen1;
				END IF;
				IF X = 3 THEN
					UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Colom4 = LabaRugiTemp.Colom1;
					UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Unit4 = LabaRugiTemp.Unit1;
					UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Persen4 = LabaRugiTemp.Persen1;
				END IF;
				IF X = 4 THEN
					UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Colom3 = LabaRugiTemp.Colom1;
					UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Unit3 = LabaRugiTemp.Unit1;
					UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Persen3 = LabaRugiTemp.Persen1;
				  END IF;
				IF X = 5 THEN
					UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Colom2 = LabaRugiTemp.Colom1;
					UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Unit2 = LabaRugiTemp.Unit1;
					UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Persen2 = LabaRugiTemp.Persen1;
				  END IF;
				IF X = 6 THEN
					UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Colom1 = LabaRugiTemp.Colom1;
					UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Unit1 = LabaRugiTemp.Unit1;
					UPDATE LabaRugi INNER JOIN LabaRugiTemp ON  LabaRugi.NoAccount = LabaRugiTemp.NoAccount SET LabaRugi.Persen1 = LabaRugiTemp.Persen1;
				  END IF;

				SET TglAwal = DATE_ADD(LAST_DAY(DATE_ADD(TglAwal, INTERVAL -2 MONTH)),INTERVAL 1 DAY);
				SET TglAkhir = LAST_DAY(DATE_ADD(TglAkhir, INTERVAL -1 MONTH));

				ITERATE  loop_label;
			ELSE
				LEAVE  loop_label;
			END  IF;
		END LOOP;

		UPDATE LabaRugi SET ColomT = Colom1 + Colom2 + Colom3+Colom4 + Colom5 + Colom6;
		UPDATE LabaRugi SET UnitT = Unit1 + Unit2 + Unit3+Unit4 + Unit5 + Unit6;
		UPDATE LabaRugi SET PersenT = CAST(((Persen1 + Persen2 + Persen3+Persen4 + Persen5 + Persen6)/6) AS DECIMAL(5,2));

		SET @MyQuery = CONCAT("SELECT * FROM LabaRugi;");
	END IF;

	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$

DELIMITER ;


