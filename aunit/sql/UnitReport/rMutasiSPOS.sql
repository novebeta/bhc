DELIMITER $$
DROP PROCEDURE IF EXISTS `rMutasiSPOS`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rMutasiSPOS`(IN xTipe VARCHAR(25),IN xTgl1 DATE,IN xTgl2 DATE,IN xLokasi VARCHAR(30),IN xDealer VARCHAR(20),IN xSort VARCHAR(25),IN xOrder VARCHAR(10))
BEGIN
/*
       Cara Akses :      
       *** HEADER ***
       CALL rMutasiSPOS('ttsmhd','2001-01-01','2019-12-31','','','ttsmhd.SMTgl','ASC');

       *** DETAIL ***
       CALL rMutasiSPOS('ttsmit','2001-01-01','2019-12-31','','','ttsmhd.SMTgl','ASC');
*/

       SET xLokasi = CONCAT(xLokasi,"%");
       SET xDealer = CONCAT(xDealer,"%");
       
       /* HEADER */
       IF xTipe = "ttsmhd" THEN 
          SET @MyQuery = "SELECT ttsmhd.LokasiAsal, ttsmhd.LokasiTujuan, ttsmhd.SMJam, ttsmhd.SMMemo, ttsmhd.SMNo, 
                          ttsmhd.SMTgl, ttsmhd.SMTotal, ttsmhd.UserID, IFNULL(tdlokasi.LokasiNama, '--') AS LokasiNamaAsal, 
                          IFNULL(tdlokasi_1.LokasiNama, '--') AS LokasiNamaTujuan, IFNULL(ttpbhd.PBNo, '--') AS PBNo 
                          FROM ttsmhd 
                          LEFT OUTER JOIN tdlokasi ON ttsmhd.LokasiAsal = tdlokasi.LokasiKode 
                          LEFT OUTER JOIN tdlokasi tdlokasi_1 ON ttsmhd.LokasiTujuan = tdlokasi_1.LokasiKode 
                          LEFT OUTER JOIN ttpbhd ON ttsmhd.SMNo = ttpbhd.SMNo 
                          WHERE ";
                          
          SET @MyQuery = CONCAT(@MyQuery,"(ttsmhd.SMTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttsmhd.LokasiAsal LIKE '",xLokasi,"') AND 
                         (ttsmhd.LokasiTujuan LIKE '",xDealer,"') ORDER BY ",xSort," ",xOrder); 
       END IF;


       /* DETAIL */
       IF xTipe = "ttsmit" THEN 
          SET @MyQuery = "SELECT ttsmit.SMNo, ttsmit.MotorNoMesin, ttsmit.MotorAutoN, tmotor.MotorType, tmotor.MotorTahun, 
                          tmotor.MotorWarna, tmotor.MotorNoRangka, tmotor.FBHarga 
                          FROM ttsmit 
                          INNER JOIN tmotor ON ttsmit.MotorNoMesin = tmotor.MotorNoMesin AND ttsmit.MotorAutoN = tmotor.MotorAutoN 
                          INNER JOIN ttsmhd ON ttsmhd.SMNo = ttsmit.SMNo 
                          WHERE ";
                          
          SET @MyQuery = CONCAT(@MyQuery,"(ttsmhd.SMTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttsmhd.LokasiAsal LIKE '",xLokasi,"') AND 
                         (ttsmhd.LokasiTujuan LIKE '",xDealer,"') ORDER BY ",xSort," ",xOrder); 
       END IF;

       PREPARE STMT FROM @MyQuery;
       EXECUTE Stmt;
    
END$$
DELIMITER ;

