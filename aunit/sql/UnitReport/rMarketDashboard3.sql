DELIMITER $$

DROP PROCEDURE IF EXISTS `rMarketDashboard3`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rMarketDashboard3`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE, IN xTanggal VARCHAR(25), IN xEOM VARCHAR(1), IN xRingkas VARCHAR(1))
BEGIN
/*
CALL rGLDashboard("Dashboard A1 Motor",'2020-02-01','2020-02-29', 'ttsk.SKTgl', '1');
*/ 
	DECLARE Tgl1Now, Tgl2Now, Tgl1Before, Tgl2Before DATE;
	DECLARE X  INT;

	SET xTgl1 = DATE_ADD(LAST_DAY(DATE_ADD(xTgl2, INTERVAL -1 MONTH)), INTERVAL 1 DAY);
	IF xEOM = '1' THEN
		SET xTgl2 = LAST_DAY(xTgl2);
		SET Tgl2Before = LAST_DAY(DATE_ADD(xTgl2, INTERVAL -1 MONTH));
	ELSE
		SET Tgl2Before = (DATE_ADD(xTgl2, INTERVAL -1 MONTH));
	END IF;
   SET Tgl1Now = xTgl1;
   SET Tgl2Now = xTgl2;
   SET Tgl1Before = DATE_ADD(xTgl1, INTERVAL -1 MONTH);  
	
	IF xTipe = "H1StockDay" AND xRingkas = "0" THEN
		DROP TEMPORARY TABLE IF EXISTS TypeSalesStok;
		CREATE TEMPORARY TABLE IF NOT EXISTS TypeSalesStok AS
		SELECT MotorGroup, tdmotortype.MotorKategori, tdmotortype.MotorType, tdmotortype.MotorNama,
		000000000 AS Sales, 000000000 AS Stock, 000.00 AS StockDay
		FROM tdmotortype
		INNER JOIN tdmotorkategori ON tdmotortype.MotorKategori = tdmotorkategori.MotorKategori
		ORDER BY tdmotorkategori.MotorNo, tdmotortype.MotorKategori, tdmotortype.MotorType;
		CREATE INDEX MotorType ON TypeSalesStok(MotorType);

		DROP TEMPORARY TABLE IF EXISTS TFill;
		CREATE TEMPORARY TABLE IF NOT EXISTS TFill AS
		SELECT tdmotortype.MotorKategori, tdmotortype.MotorType, IFNULL(Jumlah,0) AS Jumlah
		FROM tdmotortype
		INNER JOIN tdmotorkategori ON tdmotortype.MotorKategori = tdmotorkategori.MotorKategori
			LEFT OUTER JOIN
				(SELECT tmotor.MotorType, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS Jumlah FROM tmotor
					INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
					INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
					INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
			WHERE ttsk.SKTgl BETWEEN xTgl1 AND xTgl2
			GROUP BY MotorKategori, tmotor.MotorType) Jum
		ON tdmotortype.MotorType = Jum.MotorType ORDER BY tdmotorkategori.MotorNo, tdmotortype.MotorKategori, tdmotortype.MotorType ;
		CREATE INDEX MotorType ON TFill(MotorType);
		UPDATE TypeSalesStok INNER JOIN TFill ON TFill.MotorType = TypeSalesStok.MotorType SET Sales = Jumlah;

		DROP TEMPORARY TABLE IF EXISTS TFill;
		CREATE TEMPORARY TABLE IF NOT EXISTS TFill AS
		SELECT tdmotortype.MotorKategori, tdmotortype.MotorType, tdmotortype.MotorNama, IFNULL(StockMotor.Jumlah,0) AS Jumlah
				FROM tdmotortype INNER JOIN tdmotorkategori ON tdmotortype.MotorKategori = tdmotorkategori.MotorKategori
		  LEFT OUTER JOIN (SELECT tmotor.MotorType, COUNT(LokasiMotor.Kondisi) AS Jumlah, SUM(tmotor.FBHarga) AS FBHarga
						FROM (SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi
								FROM tvmotorlokasi
											INNER JOIN (SELECT tvmotorlokasi_1.MotorAutoN, tvmotorlokasi_1.MotorNoMesin , MAX(tvmotorlokasi_1.Jam) AS Jam
															FROM tvmotorlokasi tvmotorlokasi_1
															WHERE tvmotorlokasi_1.Jam <= CONCAT(xTgl2, " 23:59:59")
															GROUP BY tvmotorlokasi_1.MotorNoMesin, tvmotorlokasi_1.MotorAutoN) B
											  ON tvmotorlokasi.MotorNoMesin = B.MotorNoMesin AND tvmotorlokasi.Jam = B.Jam AND tvmotorlokasi.MotorAutoN = B.MotorAutoN
										 WHERE tvmotorlokasi.Kondisi IN ('IN','OUT Pos') ) LokasiMotor
									INNER JOIN tmotor
									  ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN
									INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
								 GROUP BY tdmotortype.MotorKategori,tmotor.MotorType) StockMotor
			 ON tdmotortype.MotorType = StockMotor.MotorType
		ORDER BY tdmotorkategori.MotorNo, tdmotortype.MotorKategori, tdmotortype.MotorType;
		CREATE INDEX MotorType ON TFill(MotorType);
		UPDATE TypeSalesStok INNER JOIN TFill ON TFill.MotorType = TypeSalesStok.MotorType SET Stock = Jumlah;

		DELETE FROM TypeSalesStok WHERE Sales = 0 AND Stock = 0;

		DROP TEMPORARY TABLE IF EXISTS TGroup;
		CREATE TEMPORARY TABLE IF NOT EXISTS TGroup AS
		SELECT '--' AS MotorGroup, 'TOTAL' AS MotorKategori, CONCAT_WS('',REPLACE(TypeSalesStok.MOtorKategori,'Low','')) AS MotorType, CONCAT_WS('','TOTAL ',REPLACE(TypeSalesStok.MOtorKategori,'Low','')) AS MotorNama,
		SUM(Sales) AS Sales, SUM(Stock) AS Stock, 0 AS StockDay FROM TypeSalesStok
		INNER JOIN
				(SELECT MotorNo, MotorGroup FROM tdmotorkategori GROUP BY MotorGroup) tdmotorkategori
		ON tdmotorkategori.MotorGroup = TypeSalesStok.MotorGroup
		GROUP BY TypeSalesStok.MotorGroup;
		INSERT INTO TypeSalesStok SELECT * FROM TGroup;

		UPDATE TypeSalesStok SET StockDay =  ROUND(Stock / (Sales / DAY(xTgl2)),2) WHERE Sales > 0;
		SET @MyQuery = CONCAT("SELECT * FROM TypeSalesStok;");		
 	END IF;

	IF xTipe = "H1StockDay" AND xRingkas = "1" THEN
		DROP TEMPORARY TABLE IF EXISTS TypeSalesStok;
		CREATE TEMPORARY TABLE IF NOT EXISTS TypeSalesStok AS
		SELECT MotorGroup, tdmotortype.MotorKategori, tdmotortype.MotorType, tdmotortype.MotorNama,
		000000000 AS Sales, 000000000 AS Stock, 000.00 AS StockDay
		FROM tdmotortype
		INNER JOIN tdmotorkategori ON tdmotortype.MotorKategori = tdmotorkategori.MotorKategori
		GROUP BY MotorNoMesin
		ORDER BY tdmotorkategori.MotorNo, tdmotortype.MotorKategori, tdmotortype.MotorType, MotorNoMesin;
		CREATE INDEX MotorType ON TypeSalesStok(MotorType);

		DROP TEMPORARY TABLE IF EXISTS TFill;
		CREATE TEMPORARY TABLE IF NOT EXISTS TFill AS
		SELECT  tdmotortype.MotorKategori, tdmotortype.MotorType,IFNULL(Jumlah,0) AS Jumlah FROM
		(SELECT tdmotorkategori.MotorNo, tdmotortype.MotorNoMesin, tdmotortype.MotorKategori, tdmotortype.MotorType FROM tdmotortype
		INNER JOIN tdmotorkategori ON tdmotortype.MotorKategori = tdmotorkategori.MotorKategori
		GROUP BY MotorNoMesin) tdmotortype
		LEFT OUTER JOIN
				(SELECT tdmotortype.MotorNoMesin, tmotor.MotorType, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS Jumlah FROM tmotor
					INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
					INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
					INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
					WHERE ttsk.SKTgl BETWEEN xTgl1 AND xTgl2
				 GROUP BY  tdmotortype.MotorNoMesin) Jum
			ON tdmotortype.MotorNoMesin = Jum.MotorNoMesin
			ORDER BY MotorNo, MotorKategori, tdmotortype.MotorType , tdmotortype.MotorNoMesin;
		CREATE INDEX MotorType ON TFill(MotorType);
		UPDATE TypeSalesStok INNER JOIN TFill ON TFill.MotorType = TypeSalesStok.MotorType SET Sales = Jumlah;

		DROP TEMPORARY TABLE IF EXISTS TFill;
		CREATE TEMPORARY TABLE IF NOT EXISTS TFill AS
		SELECT tdmotortype.MotorNoMesin, tdmotortype.MotorNo, tdmotortype.MotorKategori, tdmotortype.MotorType, tdmotortype.MotorNama, IFNULL(StockMotor.Jumlah, 0) AS Jumlah  FROM
		(SELECT tdmotortype.MotorNoMesin, tdmotorkategori.MotorNo, tdmotortype.MotorKategori, tdmotortype.MotorType, tdmotortype.MotorNama
		FROM tdmotortype INNER JOIN tdmotorkategori ON tdmotortype.MotorKategori = tdmotorkategori.MotorKategori
		GROUP BY MotorNoMesin) tdmotortype
		LEFT OUTER JOIN 
		(SELECT tdmotortype.MotorNoMesin, tmotor.MotorType, COUNT(LokasiMotor.Kondisi) AS Jumlah, SUM(tmotor.FBHarga) AS FBHarga FROM
		(SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi 
		 FROM tvmotorlokasi INNER JOIN 
			 (SELECT tvmotorlokasi_1.MotorAutoN, tvmotorlokasi_1.MotorNoMesin, MAX(tvmotorlokasi_1.Jam) AS Jam FROM tvmotorlokasi tvmotorlokasi_1 
			  WHERE tvmotorlokasi_1.Jam <= CONCAT(xTgl2, " 23:59:59")
			  GROUP BY tvmotorlokasi_1.MotorNoMesin, tvmotorlokasi_1.MotorAutoN) B 
		 ON tvmotorlokasi.MotorNoMesin = B.MotorNoMesin AND tvmotorlokasi.Jam = B.Jam AND tvmotorlokasi.MotorAutoN = B.MotorAutoN 
		 WHERE tvmotorlokasi.Kondisi IN ('IN', 'OUT Pos')) LokasiMotor 
		INNER JOIN tmotor ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN 
		INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
		GROUP BY tdmotortype.MotorNoMesin) StockMotor 
		ON tdmotortype.MotorNoMesin = StockMotor.MotorNoMesin 
		ORDER BY tdmotortype.MotorNo, tdmotortype.MotorKategori,tdmotortype.MotorType, tdmotortype.MotorNoMesin;
		CREATE INDEX MotorType ON TFill(MotorType);
		UPDATE TypeSalesStok INNER JOIN TFill ON TFill.MotorType = TypeSalesStok.MotorType SET Stock = Jumlah;

		DELETE FROM TypeSalesStok WHERE Sales = 0 AND Stock = 0;

		DROP TEMPORARY TABLE IF EXISTS TGroup;
		CREATE TEMPORARY TABLE IF NOT EXISTS TGroup AS
		SELECT '--' AS MotorGroup, 'TOTAL' AS MotorKategori, CONCAT_WS('',REPLACE(TypeSalesStok.MOtorKategori,'Low','')) AS MotorType, CONCAT_WS('','TOTAL ',REPLACE(TypeSalesStok.MOtorKategori,'Low','')) AS MotorNama,
		SUM(Sales) AS Sales, SUM(Stock) AS Stock, 0 AS StockDay FROM TypeSalesStok
		INNER JOIN
				(SELECT MotorNo, MotorGroup FROM tdmotorkategori GROUP BY MotorGroup) tdmotorkategori
		ON tdmotorkategori.MotorGroup = TypeSalesStok.MotorGroup
		GROUP BY TypeSalesStok.MotorGroup;
		INSERT INTO TypeSalesStok SELECT * FROM TGroup;

		UPDATE TypeSalesStok SET StockDay =  ROUND(Stock / (Sales / DAY(xTgl2)),2) WHERE Sales > 0;
		SET @MyQuery = CONCAT("SELECT * FROM TypeSalesStok;");	
	END IF;
	
	IF xTipe = "H1Growth Salah" THEN
		DROP TEMPORARY TABLE IF EXISTS Growth;
		CREATE TEMPORARY TABLE IF NOT EXISTS Growth AS
		SELECT tdmotortype.MotorKategori AS MyGroup, MotorNoMesin, MotorNama AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth  FROM tdmotorkategori
		INNER JOIN tdmotortype ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
		GROUP BY MotorNoMesin
		ORDER BY MotorNo,MotorNoMesin;
		ALTER TABLE Growth MODIFY COLUMN MBefore DECIMAL(19,2);
		ALTER TABLE Growth MODIFY COLUMN MNow DECIMAL(19,2);
		ALTER TABLE Growth MODIFY COLUMN Growth DECIMAL(15,2);
		ALTER TABLE Growth MODIFY COLUMN Keterangan VARCHAR(200);
		ALTER TABLE Growth MODIFY COLUMN MotorNoMesin VARCHAR(20);
		CREATE INDEX Keterangan ON Growth(Keterangan);
		CREATE INDEX MotorNoMesin ON Growth(MotorNoMesin);
		CREATE INDEX MyGroup ON Growth(MyGroup);

		DROP TEMPORARY TABLE IF EXISTS MyNOW;
		CREATE TEMPORARY TABLE IF NOT EXISTS MyNOW AS
		SELECT tdmotortype.MotorKategori AS MyGroup, tdmotortype.MotorNoMesin, MotorNama AS Keterangan, IFNULL(Jumlah, 0) AS Jumlah FROM
		(SELECT MotorNo, MotorNoMesin, tdmotortype.MotorKategori, MotorNama  FROM tdmotorkategori
		INNER JOIN tdmotortype ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
		GROUP BY MotorNoMesin) tdmotortype
		LEFT OUTER JOIN
		(SELECT tdmotortype.MotorNoMesin, tdmotortype.MotorKategori, tmotor.MotorType, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah
		FROM tmotor
		INNER JOIN tdmotortype ON (tmotor.MotorType= tdmotortype.MotorType )
		INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
		INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
		INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
		WHERE ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now
		GROUP BY tdmotortype.MotorNoMesin) Jum
		ON tdmotortype.MotorNoMesin = Jum.MotorNoMesin
		ORDER BY tdmotortype.MotorNo, tdmotortype.MotorNoMesin;
		CREATE INDEX MotorNoMesin ON MyNOW(MotorNoMesin);
		UPDATE Growth INNER JOIN MyNow ON MyNow.MotorNoMesin = Growth.MotorNoMesin SET MNow = Jumlah;


		DROP TEMPORARY TABLE IF EXISTS MyBefore;
		CREATE TEMPORARY TABLE IF NOT EXISTS MyBefore AS
		SELECT tdmotortype.MotorKategori AS MyGroup, tdmotortype.MotorNoMesin, MotorNama AS Keterangan, IFNULL(Jumlah, 0) AS Jumlah FROM
		(SELECT MotorNo, MotorNoMesin, tdmotortype.MotorKategori, MotorNama  FROM tdmotorkategori
		INNER JOIN tdmotortype ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
		GROUP BY MotorNoMesin) tdmotortype
		LEFT OUTER JOIN
		(SELECT tdmotortype.MotorNoMesin, tdmotortype.MotorKategori, tmotor.MotorType, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah
		FROM tmotor
		INNER JOIN tdmotortype ON (tmotor.MotorType= tdmotortype.MotorType )
		INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
		INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
		INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
		WHERE ttsk.SKTgl BETWEEN Tgl1Before AND Tgl2Before
		GROUP BY tdmotortype.MotorNoMesin) Jum
		ON tdmotortype.MotorNoMesin = Jum.MotorNoMesin
		ORDER BY tdmotortype.MotorNo, tdmotortype.MotorNoMesin;
		CREATE INDEX MotorNoMesin ON MyBefore(MotorNoMesin);
		UPDATE Growth INNER JOIN MyBefore ON MyBefore.MotorNoMesin = Growth.MotorNoMesin SET MBefore = Jumlah;

		DELETE FROM Growth WHERE MBefore = 0 AND MNow = 0;


		DROP TEMPORARY TABLE IF EXISTS TGroup;
		CREATE TEMPORARY TABLE IF NOT EXISTS TGroup AS
		SELECT TGroup.* FROM
		(SELECT 'TOTAL' AS MyGroup, REPLACE(MyGROUP,'Low','') AS MotorNoMesin, CONCAT_WS('','TOTAL ',REPLACE(MyGROUP,'Low','')) AS Keterangan,
		 SUM(MBefore) AS MBefore, SUM(MNow) AS MNow, 0 AS Growth FROM Growth GROUP BY LEFT(MyGROUP,3)) TGroup
		  INNER JOIN
		(SELECT MotorNo, MotorGroup FROM tdmotorkategori GROUP BY MotorGroup) tdmotorkategori
		ON tdmotorkategori.MotorGroup = MotorNoMesin ORDER BY MotorNo;
		INSERT INTO Growth SELECT * FROM TGroup;

		UPDATE Growth SET Growth = ROUND((MNow - MBefore) / MBefore * 100,2) WHERE MBefore <> 0;
		UPDATE Growth SET Growth = 100 WHERE MBefore = 0;
		UPDATE Growth SET Growth = 0 WHERE MBefore = 0 AND MNow = 0;

		SET @MyQuery = CONCAT("SELECT * FROM Growth;");	
	END IF;

	IF xTipe = "H1Growth" THEN
		DROP TEMPORARY TABLE IF EXISTS Growth;
		CREATE TEMPORARY TABLE IF NOT EXISTS Growth AS
			SELECT 'Keterangan' AS MyGroup, 'Sales'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'Credit Sales'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'Retur'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'Profit'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'Profit Per Unit'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'PDOP'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'SD 2 + Retur'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'SD 2 + Retur Per Unit'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'Cont'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'Stock'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'Stock Day'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'Piutang Karyawan'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'Piutang Promo'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth  UNION
			SELECT 'Keterangan' AS MyGroup, 'Piutang Konsumen'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth UNION
			SELECT 'Keterangan' AS MyGroup, 'Piutang Leasing'  AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth ;
		ALTER TABLE Growth MODIFY COLUMN MBefore DECIMAL(19,2);
		ALTER TABLE Growth MODIFY COLUMN MNow DECIMAL(19,2);
		ALTER TABLE Growth MODIFY COLUMN Growth DECIMAL(15,2);
		ALTER TABLE Growth MODIFY COLUMN Keterangan VARCHAR(200);
		CREATE INDEX Keterangan ON Growth(Keterangan);
		CREATE INDEX MyGroup ON Growth(MyGroup);

		SELECT COUNT(tmotor.SKNo) INTO @SalesNow FROM tmotor INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo WHERE ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now;
		UPDATE Growth SET MNow = @SalesNow WHERE Keterangan = 'Sales';
		SELECT COUNT(tmotor.SKNo) INTO @SalesBefore FROM tmotor INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo WHERE ttsk.SKTgl BETWEEN Tgl1Before AND Tgl2Before;
		UPDATE Growth SET MBefore = @SalesBefore WHERE Keterangan = 'Sales';

		SELECT COUNT(tmotor.SKNo) INTO @Nilai FROM tmotor INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo WHERE ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now 
		AND LeaseKode <> 'TUNAI' AND LeaseKode <> 'KDS';
		UPDATE Growth SET MNow = @Nilai WHERE Keterangan = 'Credit Sales';
		SELECT COUNT(tmotor.SKNo) INTO @Nilai FROM tmotor INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo WHERE ttsk.SKTgl BETWEEN Tgl1Before 
		AND Tgl2Before AND LeaseKode <> 'TUNAI' AND LeaseKode <> 'KDS';
		UPDATE Growth SET MBefore = @Nilai WHERE Keterangan = 'Credit Sales';

		SELECT COUNT(tmotor.RKNo) INTO @Nilai FROM tmotor INNER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo WHERE ttrk.RKTgl BETWEEN Tgl1Now AND Tgl2Now;
		UPDATE Growth SET MNow = @Nilai WHERE Keterangan = 'Retur';
		SELECT COUNT(tmotor.RKNo) INTO @Nilai FROM tmotor INNER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo WHERE ttrk.RKTgl BETWEEN Tgl1Before AND Tgl2Before;
		UPDATE Growth SET MBefore = @Nilai WHERE Keterangan = 'Retur';

		SELECT SUM(ttdk.PotonganHarga + ttdk.ReturHarga) INTO @SDRKNow FROM tmotor INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo  
		WHERE ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now AND LeaseKode <> 'TUNAI' AND LeaseKode <> 'KDS';
		UPDATE Growth SET MNow = @SDRKNow WHERE Keterangan = 'SD 2 + Retur';
		SELECT SUM(ttdk.PotonganHarga + ttdk.ReturHarga) INTO @SDRKBefore FROM tmotor INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo 
		WHERE ttsk.SKTgl BETWEEN Tgl1Before AND Tgl2Before AND LeaseKode <> 'TUNAI' AND LeaseKode <> 'KDS';
		UPDATE Growth SET MBefore = @SDRKBefore WHERE Keterangan = 'SD 2 + Retur';

		UPDATE Growth SET MNow = ROUND(@SDRKNow/@SalesNow,2) WHERE Keterangan = 'SD 2 + Retur Per Unit';
		UPDATE Growth SET MBefore = ROUND(@SDRKBefore/@SalesBefore,2) WHERE Keterangan = 'SD 2 + Retur Per Unit';

		SELECT COUNT(LokasiMotor.Kondisi) INTO @JumNow FROM
			(SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi
			FROM tvmotorlokasi
			INNER JOIN (SELECT tvmotorlokasi_1.MotorAutoN, tvmotorlokasi_1.MotorNoMesin , MAX(tvmotorlokasi_1.Jam) AS Jam
							FROM tvmotorlokasi tvmotorlokasi_1
							WHERE tvmotorlokasi_1.Jam <= CONCAT(Tgl2Now, " 23:59:59")
							GROUP BY tvmotorlokasi_1.MotorNoMesin, tvmotorlokasi_1.MotorAutoN) B
			ON tvmotorlokasi.MotorNoMesin = B.MotorNoMesin AND tvmotorlokasi.Jam = B.Jam AND tvmotorlokasi.MotorAutoN = B.MotorAutoN
			WHERE tvmotorlokasi.Kondisi IN ('IN','OUT Pos') ) LokasiMotor
		INNER JOIN tmotor ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN;
		UPDATE Growth SET MNow = @JumNow WHERE Keterangan = 'Stock';

		SELECT COUNT(LokasiMotor.Kondisi) INTO @JumBefore FROM
			(SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi
			FROM tvmotorlokasi
			INNER JOIN (SELECT tvmotorlokasi_1.MotorAutoN, tvmotorlokasi_1.MotorNoMesin , MAX(tvmotorlokasi_1.Jam) AS Jam
							FROM tvmotorlokasi tvmotorlokasi_1
							WHERE tvmotorlokasi_1.Jam <= CONCAT(Tgl2Before, " 23:59:59")
							GROUP BY tvmotorlokasi_1.MotorNoMesin, tvmotorlokasi_1.MotorAutoN) B
			ON tvmotorlokasi.MotorNoMesin = B.MotorNoMesin AND tvmotorlokasi.Jam = B.Jam AND tvmotorlokasi.MotorAutoN = B.MotorAutoN
			WHERE tvmotorlokasi.Kondisi IN ('IN','OUT Pos') ) LokasiMotor
		INNER JOIN tmotor ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN;
		UPDATE Growth SET MBefore = @JumBefore WHERE Keterangan = 'Stock';

		UPDATE Growth SET MBefore = ROUND(@JumBefore/(@SalesBefore/DAY(DATE_ADD(Tgl2Now, INTERVAL -1 MONTH))),2) WHERE Keterangan = 'Stock Day';
		UPDATE Growth SET MNow = ROUND(@JumNow/(@SalesNow/DAY(Tgl2Now)),2) WHERE Keterangan = 'Stock Day';

		CALL rglNeraca(Tgl1Before, Tgl2Before, Tgl2Before);
		SELECT Kredit-Debet INTO @ProfitBefore FROM traccountsaldo WHERE NoAccount = '26040000' ;
		SELECT Saldo INTO @PDOPBefore FROM traccountsaldo WHERE NoAccount = '40000000' ;
		SELECT Saldo INTO @PiutangKaryawanBefore FROM traccountsaldo WHERE NoAccount = '11120100' ;
		SELECT Saldo INTO @PiutangPromoBefore FROM traccountsaldo WHERE NoAccount = '11110500' ;
		SELECT Saldo INTO @PiutangKonsumenBefore FROM traccountsaldo WHERE NoAccount = '11060100' ;
		SELECT Saldo INTO @PiutangLeasingBefore FROM traccountsaldo WHERE NoAccount = '11080000' ;
		UPDATE Growth SET MBefore = @ProfitBefore WHERE Keterangan = 'Profit';
		UPDATE Growth SET MBefore = ROUND(@ProfitBefore/@SalesBefore,2)  WHERE Keterangan = 'Profit Per Unit';
		UPDATE Growth SET MBefore = @PDOPBefore WHERE Keterangan = 'PDOP';
		UPDATE Growth SET MBefore = @PiutangKaryawanBefore WHERE Keterangan = 'Piutang Karyawan';
		UPDATE Growth SET MBefore = @PiutangPromoBefore WHERE Keterangan = 'Piutang Promo';
		UPDATE Growth SET MBefore = @PiutangKonsumenBefore WHERE Keterangan = 'Piutang Konsumen';
		UPDATE Growth SET MBefore = @PiutangLeasingBefore WHERE Keterangan = 'Piutang Leasing';

		CALL rglNeraca(Tgl1Now, Tgl2Now, Tgl2Now);
		SELECT Kredit-Debet INTO @ProfitNow FROM traccountsaldo WHERE NoAccount = '26040000' ;
		SELECT Saldo INTO @PDOPNow FROM traccountsaldo WHERE NoAccount = '40000000' ;
		SELECT Saldo INTO @PiutangKaryawanNow FROM traccountsaldo WHERE NoAccount = '11120100' ;
		SELECT Saldo INTO @PiutangPromoNow FROM traccountsaldo WHERE NoAccount = '11110500' ;
		SELECT Saldo INTO @PiutangKonsumenNow FROM traccountsaldo WHERE NoAccount = '11060100' ;
		SELECT Saldo INTO @PiutangLeasingNow FROM traccountsaldo WHERE NoAccount = '11080000' ;
		UPDATE Growth SET MNow = @ProfitNow WHERE Keterangan = 'Profit';
		UPDATE Growth SET MNow = ROUND(@ProfitNow/@SalesNow,2)  WHERE Keterangan = 'Profit Per Unit';
		UPDATE Growth SET MNow = @PDOPNow WHERE Keterangan = 'PDOP';
		UPDATE Growth SET MNow = @PiutangKaryawanNow WHERE Keterangan = 'Piutang Karyawan';
		UPDATE Growth SET MNow = @PiutangPromoNow WHERE Keterangan = 'Piutang Promo';
		UPDATE Growth SET MNow = @PiutangKonsumenNow WHERE Keterangan = 'Piutang Konsumen';
		UPDATE Growth SET MNow = @PiutangLeasingNow WHERE Keterangan = 'Piutang Leasing';

		INSERT INTO Growth SELECT 'Keterangan' AS MyGroup, NamaAccount AS Keterangan, OpeningBalance AS MBefore, Saldo AS MNow, 0 AS Growth FROM traccountsaldo WHERE NoParent = '11080000' AND JenisAccount = 'Detail';

		INSERT INTO Growth SELECT 'Team' AS MyGroup, tdteam.TeamKode AS Keterangan, IFNULL(JumBefore,0) AS MBefore, IFNULL(JumNow,0) AS MNow , 0 AS Growth FROM tdteam
      LEFT OUTER JOIN (SELECT ttdk.TeamKode, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS JumNow FROM tmotor
                        INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
                        INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo WHERE ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now
                        GROUP BY TRIM(ttdk.TeamKode)) TJumNow
                        ON tdteam.TeamKode = TJumNow.TeamKode
      LEFT OUTER JOIN (SELECT ttdk.TeamKode, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS JumBefore FROM tmotor
                        INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
                        INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo WHERE ttsk.SKTgl BETWEEN Tgl1Before AND Tgl2Before
                        GROUP BY TRIM(ttdk.TeamKode)) TJumBefore
                        ON tdteam.TeamKode = TJumBefore.TeamKode ORDER BY tdteam.TeamNo, tdteam.TeamKode;
		
		UPDATE Growth SET Growth = ROUND((MNow -  MBefore) / MBefore *100,2) WHERE MBefore <> 0;	
		UPDATE Growth SET Growth = 100 WHERE MBefore = 0;
		UPDATE Growth SET Growth = 0 WHERE MBefore = 0 AND MNow = 0;

		DELETE FROM Growth WHERE   MBefore = 0 AND MNow = 0 ;
	
		SET @MyQuery = CONCAT("SELECT * FROM Growth;");	
	END IF;
		
	IF xTipe = "H2TypeGrowth" THEN
		DROP TEMPORARY TABLE IF EXISTS SegmenGrowth;
		CREATE TEMPORARY TABLE IF NOT EXISTS SegmenGrowth AS
			SELECT 'Category' AS MyDash, tdmotorkategori.MotorGroup AS MyGroup, tdmotorkategori.MotorKategori AS Keterangan,
			0 AS MBefore,
			0 AS MNow,
			0 AS Growth,
			0 AS CBefore,
			0 AS CNow
			FROM tdmotorkategori ORDER BY MotorNo;
		ALTER TABLE SegmenGrowth MODIFY COLUMN MyDash VARCHAR(20);
		ALTER TABLE SegmenGrowth MODIFY COLUMN MBefore DECIMAL(19,2);
		ALTER TABLE SegmenGrowth MODIFY COLUMN MNow DECIMAL(19,2);
		ALTER TABLE SegmenGrowth MODIFY COLUMN Growth DECIMAL(15,2);
		ALTER TABLE SegmenGrowth MODIFY COLUMN CBefore DECIMAL(19,2);
		ALTER TABLE SegmenGrowth MODIFY COLUMN CNow DECIMAL(19,2);
		ALTER TABLE SegmenGrowth MODIFY COLUMN Keterangan VARCHAR(20);
		CREATE INDEX Keterangan ON SegmenGrowth(Keterangan);
		CREATE INDEX MyDash ON SegmenGrowth(MyDash);
		CREATE INDEX MyGroup ON SegmenGrowth(MyGroup);

		DROP TEMPORARY TABLE IF EXISTS TFillNow;
		CREATE TEMPORARY TABLE IF NOT EXISTS TFillNow AS
			SELECT tdmotorkategori.MotorGroup, tdmotorkategori.MotorKategori, IFNULL(Jumlah, 0) AS Jumlah
			FROM  tdmotorkategori
			LEFT OUTER JOIN
				(SELECT tdmotorkategori.MotorGroup, tdmotortype.MotorKategori, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah
				 FROM tmotor
				 INNER JOIN tdmotortype ON (tmotor.MotorType= tdmotortype.MotorType )
				 INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
				 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
				 INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
				 WHERE ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now
				 GROUP BY tdmotorkategori.MotorGroup, tdmotorkategori.MotorKategori) Jum
			ON tdmotorkategori.MotorGroup = Jum.MotorGroup AND tdmotorkategori.MotorKategori = Jum.MotorKategori
			ORDER BY tdmotorkategori.MotorNo, tdmotorkategori.MotorKategori;
		CREATE INDEX MotorKategori ON TFillNow(MotorKategori);
		UPDATE SegmenGrowth INNER JOIN TFillNow ON SegmenGrowth.Keterangan = TFillNow.MotorKategori SET  MNow = Jumlah;

		DROP TEMPORARY TABLE IF EXISTS TFillBefore;
		CREATE TEMPORARY TABLE IF NOT EXISTS TFillBefore AS
			SELECT tdmotorkategori.MotorGroup, tdmotorkategori.MotorKategori, IFNULL(Jumlah, 0) AS Jumlah
			FROM  tdmotorkategori
			LEFT OUTER JOIN
				(SELECT tdmotorkategori.MotorGroup, tdmotortype.MotorKategori, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah
				 FROM tmotor
				 INNER JOIN tdmotortype ON (tmotor.MotorType= tdmotortype.MotorType )
				 INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
				 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
				 INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
				 WHERE ttsk.SKTgl BETWEEN Tgl1Before AND Tgl2Before
				 GROUP BY tdmotorkategori.MotorGroup, tdmotorkategori.MotorKategori) Jum
			ON tdmotorkategori.MotorGroup = Jum.MotorGroup AND tdmotorkategori.MotorKategori = Jum.MotorKategori
			ORDER BY tdmotorkategori.MotorNo, tdmotorkategori.MotorKategori;
		CREATE INDEX MotorKategori ON TFillBefore(MotorKategori);
		UPDATE SegmenGrowth INNER JOIN TFillBefore ON SegmenGrowth.Keterangan = TFillBefore.MotorKategori SET  MBefore = Jumlah;

		DROP TEMPORARY TABLE IF EXISTS TGroup;
		CREATE TEMPORARY TABLE IF NOT EXISTS TGroup AS
			SELECT MyDash, MyGroup, Keterangan, MBefore, MNow, Growth, CBefore, CNow FROM
			  (SELECT 'Sum Category' AS MyDash, 'Total All' AS MyGroup,  REPLACE(Keterangan,'Low','') AS Keterangan,
				SUM(MBefore) AS MBefore, SUM(MNow) AS MNow, 0 AS Growth,
				0 AS CBefore, 0 AS CNow FROM SegmenGrowth
				GROUP BY LEFT(Keterangan,3)) SegmenGrowth
			INNER JOIN
				(SELECT MotorNo, MotorGroup FROM tdmotorkategori GROUP BY MotorGroup) tdmotorkategori
			ON tdmotorkategori.MotorGroup = SegmenGrowth.Keterangan
			ORDER BY MotorNo;
		INSERT INTO SegmenGrowth SELECT * FROM TGroup;

		INSERT INTO SegmenGrowth
		SELECT '' AS MyDash,MyGroup, Sumber.LeaseKode AS Keterangan,  IFNULL(JumBefore, 0) AS MBefore,IFNULL(JumNow, 0) AS MNow, 0 AS Growth, 0 AS CBefore, 0 AS CNow FROM
			(SELECT 'Total Sales' AS MyGroup, 'CASH' AS LeaseKode
			UNION
			SELECT 'Total Sales' AS MyGroup, 'CREDIT' AS LeaseKode
			UNION
			SELECT 'Total Credit' AS MyGroup, LeaseKode FROM tdleasing WHERE LeaseKode <> 'TUNAI' AND LeaseKode <> 'KDS') Sumber
		LEFT OUTER JOIN
			(SELECT 'CASH' AS LeaseKode, COUNT(ttsk.SKNo) AS JumNow FROM tmotor INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
			WHERE (LeaseKode = 'TUNAI' OR LeaseKode = 'KDS') AND ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now
			UNION
			SELECT 'CREDIT' AS LeaseKode, COUNT(ttsk.SKNo) AS JumNow FROM tmotor INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
			WHERE LeaseKode <> 'TUNAI' AND LeaseKode <> 'KDS' AND ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now
			UNION
			SELECT ttdk.LeaseKode, COUNT(ttsk.SKNo) AS JumNow FROM tmotor INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
			WHERE LeaseKode <> 'TUNAI' AND LeaseKode <> 'KDS' AND ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now
			GROUP BY LeaseKode) TJumNow
		ON Sumber.LeaseKode = TJumNow.LeaseKode
		LEFT OUTER JOIN
			(SELECT 'CASH' AS LeaseKode, COUNT(ttsk.SKNo) AS JumBefore FROM tmotor INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
			WHERE (LeaseKode = 'TUNAI' OR LeaseKode = 'KDS') AND ttsk.SKTgl BETWEEN Tgl1Before AND Tgl2Before
			UNION
			SELECT 'CREDIT' AS LeaseKode, COUNT(ttsk.SKNo) AS JumBefore FROM tmotor INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
			WHERE LeaseKode <> 'TUNAI' AND LeaseKode <> 'KDS' AND ttsk.SKTgl BETWEEN Tgl1Before AND Tgl2Before
			UNION
			SELECT ttdk.LeaseKode, COUNT(ttsk.SKNo) AS JumBefore FROM tmotor INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
			WHERE LeaseKode <> 'TUNAI' AND LeaseKode <> 'KDS' AND ttsk.SKTgl BETWEEN Tgl1Before AND Tgl2Before
			GROUP BY LeaseKode) TJumBefore
		ON Sumber.LeaseKode = TJumBefore.LeaseKode;

		UPDATE SegmenGrowth SET Growth = ROUND((MNow -  MBefore) / MBefore *100,2) WHERE MBefore <> 0;
		UPDATE SegmenGrowth SET Growth = 100 WHERE MBefore = 0;
		UPDATE SegmenGrowth SET Growth = 0 WHERE MBefore = 0 AND MNow = 0;

		SELECT SUM(MNow) INTO @MNowCub FROM SegmenGrowth WHERE MyGroup = 'Cub';
		UPDATE SegmenGrowth SET CNow = ROUND(MNow/@MNowCub*100,2)  WHERE MYGroup = 'Cub';
		SELECT SUM(MNow) INTO @MNowAT FROM SegmenGrowth WHERE MyGroup = 'AT';
		UPDATE SegmenGrowth SET CNow = ROUND(MNow/@MNowAT*100,2) WHERE MYGroup = 'AT';
		SELECT SUM(MNow) INTO @MNowSport FROM SegmenGrowth WHERE MyGroup = 'Sport';
		UPDATE SegmenGrowth SET CNow = ROUND(MNow/@MNowSport*100,2) WHERE MYGroup = 'Sport';
		SELECT SUM(MNow) INTO @MNowTotal FROM SegmenGrowth WHERE MyGroup = 'Total All';
		UPDATE SegmenGrowth SET CNow = ROUND(MNow/@MNowTotal*100,2) WHERE MYGroup = 'Total All';

		SELECT SUM(MBefore) INTO @MBeforeCub FROM SegmenGrowth WHERE MyGroup = 'Cub';
		UPDATE SegmenGrowth SET CBefore = ROUND(MBefore/@MBeforeCub*100,2)  WHERE MYGroup = 'Cub';
		SELECT SUM(MBefore) INTO @MBeforeAT FROM SegmenGrowth WHERE MyGroup = 'AT';
		UPDATE SegmenGrowth SET CBefore = ROUND(MBefore/@MBeforeAT*100,2) WHERE MYGroup = 'AT';
		SELECT SUM(MBefore) INTO @MBeforeSport FROM SegmenGrowth WHERE MyGroup = 'Sport';
		UPDATE SegmenGrowth SET CBefore = ROUND(MBefore/@MBeforeSport*100,2) WHERE MYGroup = 'Sport';
		SELECT SUM(MBefore) INTO @MBeforeTotal FROM SegmenGrowth WHERE MyGroup = 'Total All';
		UPDATE SegmenGrowth SET CBefore = ROUND(MBefore/@MBeforeTotal*100,2) WHERE MYGroup = 'Total All';

		SELECT SUM(MNow) INTO @MNowSales FROM SegmenGrowth WHERE MyGroup = 'Total Sales';
		UPDATE SegmenGrowth SET CNow = ROUND(MNow/@MNowSales*100,2)  WHERE MYGroup = 'Total Sales';
		SELECT SUM(MNow) INTO @MNowCredit FROM SegmenGrowth WHERE MyGroup = 'Total Credit';
		UPDATE SegmenGrowth SET CNow = ROUND(MNow/@MNowCredit*100,2) WHERE MYGroup = 'Total Credit';

		SELECT SUM(MBefore) INTO @MBeforeSales FROM SegmenGrowth WHERE MyGroup = 'Total Sales';
		UPDATE SegmenGrowth SET CBefore = ROUND(MBefore/@MBeforeSales*100,2)  WHERE MYGroup = 'Total Sales';
		SELECT SUM(MBefore) INTO @MBeforeCredit FROM SegmenGrowth WHERE MyGroup = 'Total Credit';
		UPDATE SegmenGrowth SET CBefore = ROUND(MBefore/@MBeforeCredit*100,2) WHERE MYGroup = 'Total Credit';
    
		SET @MyQuery = CONCAT("SELECT * FROM SegmenGrowth;");		
 	END IF;

	IF xTipe = "H2SegmenGrowth" AND xRingkas = "1" THEN
		DROP TEMPORARY TABLE IF EXISTS Growth;
		CREATE TEMPORARY TABLE IF NOT EXISTS Growth AS
		SELECT tdmotortype.MotorKategori AS MyGroup, MotorNoMesin, MotorNama AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth  FROM tdmotorkategori
		INNER JOIN tdmotortype ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
		GROUP BY MotorNoMesin
		ORDER BY MotorNo,MotorNoMesin;
		ALTER TABLE Growth MODIFY COLUMN MBefore DECIMAL(19,2);
		ALTER TABLE Growth MODIFY COLUMN MNow DECIMAL(19,2);
		ALTER TABLE Growth MODIFY COLUMN Growth DECIMAL(15,2);
		ALTER TABLE Growth MODIFY COLUMN Keterangan VARCHAR(200);
		ALTER TABLE Growth MODIFY COLUMN MotorNoMesin VARCHAR(20);
		CREATE INDEX Keterangan ON Growth(Keterangan);
		CREATE INDEX MotorNoMesin ON Growth(MotorNoMesin);
		CREATE INDEX MyGroup ON Growth(MyGroup);

		DROP TEMPORARY TABLE IF EXISTS MyNOW;
		CREATE TEMPORARY TABLE IF NOT EXISTS MyNOW AS
		SELECT tdmotortype.MotorKategori AS MyGroup, tdmotortype.MotorNoMesin, MotorNama AS Keterangan, IFNULL(Jumlah, 0) AS Jumlah FROM
		(SELECT MotorNo, MotorNoMesin, tdmotortype.MotorKategori, MotorNama  FROM tdmotorkategori
		INNER JOIN tdmotortype ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
		GROUP BY MotorNoMesin) tdmotortype
		LEFT OUTER JOIN
		(SELECT tdmotortype.MotorNoMesin, tdmotortype.MotorKategori, tmotor.MotorType, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah
		FROM tmotor
		INNER JOIN tdmotortype ON (tmotor.MotorType= tdmotortype.MotorType )
		INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
		INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
		INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
		WHERE ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now
		GROUP BY tdmotortype.MotorNoMesin) Jum
		ON tdmotortype.MotorNoMesin = Jum.MotorNoMesin
		ORDER BY tdmotortype.MotorNo, tdmotortype.MotorNoMesin;
		CREATE INDEX MotorNoMesin ON MyNOW(MotorNoMesin);
		UPDATE Growth INNER JOIN MyNow ON MyNow.MotorNoMesin = Growth.MotorNoMesin SET MNow = Jumlah;

		DROP TEMPORARY TABLE IF EXISTS MyBefore;
		CREATE TEMPORARY TABLE IF NOT EXISTS MyBefore AS
		SELECT tdmotortype.MotorKategori AS MyGroup, tdmotortype.MotorNoMesin, MotorNama AS Keterangan, IFNULL(Jumlah, 0) AS Jumlah FROM
		(SELECT MotorNo, MotorNoMesin, tdmotortype.MotorKategori, MotorNama  FROM tdmotorkategori
		INNER JOIN tdmotortype ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
		GROUP BY MotorNoMesin) tdmotortype
		LEFT OUTER JOIN
		(SELECT tdmotortype.MotorNoMesin, tdmotortype.MotorKategori, tmotor.MotorType, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah
		FROM tmotor
		INNER JOIN tdmotortype ON (tmotor.MotorType= tdmotortype.MotorType )
		INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
		INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
		INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
		WHERE ttsk.SKTgl BETWEEN Tgl1Before AND Tgl2Before
		GROUP BY tdmotortype.MotorNoMesin) Jum
		ON tdmotortype.MotorNoMesin = Jum.MotorNoMesin
		ORDER BY tdmotortype.MotorNo, tdmotortype.MotorNoMesin;
		CREATE INDEX MotorNoMesin ON MyBefore(MotorNoMesin);
		UPDATE Growth INNER JOIN MyBefore ON MyBefore.MotorNoMesin = Growth.MotorNoMesin SET MBefore = Jumlah;

		DELETE FROM Growth WHERE MBefore = 0 AND MNow = 0;

		DROP TEMPORARY TABLE IF EXISTS TGroup;
		CREATE TEMPORARY TABLE IF NOT EXISTS TGroup AS
		SELECT TGroup.* FROM
      (SELECT 'TOTAL' AS MyGroup, REPLACE(MyGROUP,'Low','') AS MotorNoMesin, CONCAT_WS('','TOTAL ',REPLACE(MyGROUP,'Low','')) AS Keterangan,
       SUM(MBefore) AS MBefore, SUM(MNow) AS MNow, 0 AS Growth FROM Growth GROUP BY LEFT(MyGROUP,3)) TGroup
        INNER JOIN
      (SELECT MotorNo, MotorGroup FROM tdmotorkategori GROUP BY MotorGroup) tdmotorkategori
		ON tdmotorkategori.MotorGroup = MotorNoMesin ORDER BY MotorNo;
		INSERT INTO Growth SELECT * FROM TGroup;

		UPDATE Growth SET Growth = ROUND((MNow - MBefore) / MBefore * 100,2) WHERE MBefore <> 0;
		UPDATE Growth SET Growth = 100 WHERE MBefore = 0;
		UPDATE Growth SET Growth = 0 WHERE MBefore = 0 AND MNow = 0;

		SET @MyQuery = CONCAT("SELECT * FROM Growth;");		
 	END IF;

	IF xTipe = "H2SegmenGrowth" AND xRingkas = "0" THEN
		DROP TEMPORARY TABLE IF EXISTS Growth;
		CREATE TEMPORARY TABLE IF NOT EXISTS Growth AS
		SELECT tdmotortype.MotorKategori AS MyGroup, MotorType, MotorNama AS Keterangan, 0 AS MBefore, 0 AS MNow, 0 AS Growth  FROM tdmotorkategori
		INNER JOIN tdmotortype ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
		GROUP BY MotorType
		ORDER BY MotorNo,MotorType;
		ALTER TABLE Growth MODIFY COLUMN MBefore DECIMAL(19,2);
		ALTER TABLE Growth MODIFY COLUMN MNow DECIMAL(19,2);
		ALTER TABLE Growth MODIFY COLUMN Growth DECIMAL(15,2);
		ALTER TABLE Growth MODIFY COLUMN Keterangan VARCHAR(200);
		ALTER TABLE Growth MODIFY COLUMN MotorType VARCHAR(70);
		CREATE INDEX Keterangan ON Growth(Keterangan);
		CREATE INDEX MotorType ON Growth(MotorType);
		CREATE INDEX MyGroup ON Growth(MyGroup);

		DROP TEMPORARY TABLE IF EXISTS MyNOW;
		CREATE TEMPORARY TABLE IF NOT EXISTS MyNOW AS
		SELECT tdmotortype.MotorKategori AS MyGroup, tdmotortype.MotorType, MotorNama AS Keterangan, IFNULL(Jumlah, 0) AS Jumlah FROM
		(SELECT MotorNo, MotorType, tdmotortype.MotorKategori, MotorNama  FROM tdmotorkategori
		INNER JOIN tdmotortype ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
		GROUP BY MotorType) tdmotortype
		LEFT OUTER JOIN
		(SELECT tdmotortype.MotorType, tdmotortype.MotorKategori, IFNULL(COUNT(tmotor.MotorType), 0) AS Jumlah
		FROM tmotor
		INNER JOIN tdmotortype ON (tmotor.MotorType= tdmotortype.MotorType )
		INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
		INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
		INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
		WHERE ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now
		GROUP BY tdmotortype.MotorType) Jum
		ON tdmotortype.MotorType = Jum.MotorType
		ORDER BY tdmotortype.MotorNo, tdmotortype.MotorType;
		CREATE INDEX MotorType ON MyNOW(MotorType);
		UPDATE Growth INNER JOIN MyNow ON MyNow.MotorType = Growth.MotorType SET MNow = Jumlah;

		DROP TEMPORARY TABLE IF EXISTS MyBefore;
		CREATE TEMPORARY TABLE IF NOT EXISTS MyBefore AS
		SELECT tdmotortype.MotorKategori AS MyGroup, tdmotortype.MotorType, MotorNama AS Keterangan, IFNULL(Jumlah, 0) AS Jumlah FROM
		(SELECT MotorNo, MotorType, tdmotortype.MotorKategori, MotorNama  FROM tdmotorkategori
		INNER JOIN tdmotortype ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
		GROUP BY MotorType) tdmotortype
		LEFT OUTER JOIN
		(SELECT tdmotortype.MotorType, tdmotortype.MotorKategori, IFNULL(COUNT(tmotor.MotorType), 0) AS Jumlah
		FROM tmotor
		INNER JOIN tdmotortype ON (tmotor.MotorType= tdmotortype.MotorType )
		INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
		INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
		INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
		WHERE ttsk.SKTgl BETWEEN Tgl1Before AND Tgl2Before
		GROUP BY tdmotortype.MotorType) Jum
		ON tdmotortype.MotorType = Jum.MotorType
		ORDER BY tdmotortype.MotorNo, tdmotortype.MotorType;
		CREATE INDEX MotorType ON MyBefore(MotorType);
		UPDATE Growth INNER JOIN MyBefore ON MyBefore.MotorType = Growth.MotorType SET MBefore = Jumlah;

		DELETE FROM Growth WHERE MBefore = 0 AND MNow = 0;


		DROP TEMPORARY TABLE IF EXISTS TGroup;
		CREATE TEMPORARY TABLE IF NOT EXISTS TGroup AS
		SELECT TGroup.* FROM
      (SELECT 'TOTAL' AS MyGroup, REPLACE(MyGROUP,'Low','') AS MotorType, CONCAT_WS('','TOTAL ',REPLACE(MyGROUP,'Low','')) AS Keterangan,
       SUM(MBefore) AS MBefore, SUM(MNow) AS MNow, 0 AS Growth FROM Growth GROUP BY LEFT(MyGROUP,3)) TGroup
        INNER JOIN
      (SELECT MotorNo, MotorGroup FROM tdmotorkategori GROUP BY MotorGroup) tdmotorkategori
		ON tdmotorkategori.MotorGroup = MotorType ORDER BY MotorNo;
		INSERT INTO Growth SELECT * FROM TGroup;

		UPDATE Growth SET Growth = ROUND((MNow - MBefore) / MBefore * 100,2) WHERE MBefore <> 0;
		UPDATE Growth SET Growth = 100 WHERE MBefore = 0;
		UPDATE Growth SET Growth = 0 WHERE MBefore = 0 AND MNow = 0;

		SET @MyQuery = CONCAT("SELECT * FROM Growth;");		
 	END IF;

	IF xTipe = "Analisa RL" THEN
		DROP TEMPORARY TABLE IF EXISTS TAnalisaRL;
		CREATE TEMPORARY TABLE IF NOT EXISTS TAnalisaRL AS
			SELECT 'Tahun1' AS Tahun, '1' AS MyGroup, 'Penjualan'  AS Jenis, 0 AS Jan, 0 AS Feb, 0 AS Mar, 0 AS Apr, 0 AS Mei, 0 AS Jun, 0 AS Jul, 0 AS Agust, 0 AS Sep, 0 AS Okt, 0 AS Nop, 0 AS Des UNION
			SELECT 'Tahun1' AS Tahun, '2' AS MyGroup, 'RH/Unit'  AS Jenis, 0 AS Jan, 0 AS Feb, 0 AS Mar, 0 AS Apr, 0 AS Mei, 0 AS Jun, 0 AS Jul, 0 AS Agust, 0 AS Sep, 0 AS Okt, 0 AS Nop, 0 AS Des UNION
			SELECT 'Tahun1' AS Tahun, '2' AS MyGroup, 'SD1 / Unit'  AS Jenis, 0 AS Jan, 0 AS Feb, 0 AS Mar, 0 AS Apr, 0 AS Mei, 0 AS Jun, 0 AS Jul, 0 AS Agust, 0 AS Sep, 0 AS Okt, 0 AS Nop, 0 AS Des UNION
			SELECT 'Tahun1' AS Tahun, '2' AS MyGroup, 'SD2 / Unit'  AS Jenis, 0 AS Jan, 0 AS Feb, 0 AS Mar, 0 AS Apr, 0 AS Mei, 0 AS Jun, 0 AS Jul, 0 AS Agust, 0 AS Sep, 0 AS Okt, 0 AS Nop, 0 AS Des UNION
			SELECT 'Tahun1' AS Tahun, '2' AS MyGroup, 'Total RH + SD1 + SD2'  AS Jenis, 0 AS Jan, 0 AS Feb, 0 AS Mar, 0 AS Apr, 0 AS Mei, 0 AS Jun, 0 AS Jul, 0 AS Agust, 0 AS Sep, 0 AS Okt, 0 AS Nop, 0 AS Des UNION
			SELECT 'Tahun1' AS Tahun, '3' AS MyGroup, 'PDOP / Unit'  AS Jenis, 0 AS Jan, 0 AS Feb, 0 AS Mar, 0 AS Apr, 0 AS Mei, 0 AS Jun, 0 AS Jul, 0 AS Agust, 0 AS Sep, 0 AS Okt, 0 AS Nop, 0 AS Des UNION
			SELECT 'Tahun1' AS Tahun, '4' AS MyGroup, 'Gaji & Tunj / Unit'  AS Jenis, 0 AS Jan, 0 AS Feb, 0 AS Mar, 0 AS Apr, 0 AS Mei, 0 AS Jun, 0 AS Jul, 0 AS Agust, 0 AS Sep, 0 AS Okt, 0 AS Nop, 0 AS Des UNION
			SELECT 'Tahun1' AS Tahun, '4' AS MyGroup, 'Insentif / Unit'  AS Jenis, 0 AS Jan, 0 AS Feb, 0 AS Mar, 0 AS Apr, 0 AS Mei, 0 AS Jun, 0 AS Jul, 0 AS Agust, 0 AS Sep, 0 AS Okt, 0 AS Nop, 0 AS Des UNION
			SELECT 'Tahun1' AS Tahun, '4' AS MyGroup, 'Total Gaji & Tunj + Inst'  AS Jenis, 0 AS Jan, 0 AS Feb, 0 AS Mar, 0 AS Apr, 0 AS Mei, 0 AS Jun, 0 AS Jul, 0 AS Agust, 0 AS Sep, 0 AS Okt, 0 AS Nop, 0 AS Des UNION
			SELECT 'Tahun1' AS Tahun, '5' AS MyGroup, 'Biaya Bensin / Unit'  AS Jenis, 0 AS Jan, 0 AS Feb, 0 AS Mar, 0 AS Apr, 0 AS Mei, 0 AS Jun, 0 AS Jul, 0 AS Agust, 0 AS Sep, 0 AS Okt, 0 AS Nop, 0 AS Des UNION
			SELECT 'Tahun2' AS Tahun, '1' AS MyGroup, 'Penjualan'  AS Jenis, 0 AS Jan, 0 AS Feb, 0 AS Mar, 0 AS Apr, 0 AS Mei, 0 AS Jun, 0 AS Jul, 0 AS Agust, 0 AS Sep, 0 AS Okt, 0 AS Nop, 0 AS Des UNION
			SELECT 'Tahun2' AS Tahun, '2' AS MyGroup, 'RH/Unit'  AS Jenis, 0 AS Jan, 0 AS Feb, 0 AS Mar, 0 AS Apr, 0 AS Mei, 0 AS Jun, 0 AS Jul, 0 AS Agust, 0 AS Sep, 0 AS Okt, 0 AS Nop, 0 AS Des UNION
			SELECT 'Tahun2' AS Tahun, '2' AS MyGroup, 'SD1 / Unit'  AS Jenis, 0 AS Jan, 0 AS Feb, 0 AS Mar, 0 AS Apr, 0 AS Mei, 0 AS Jun, 0 AS Jul, 0 AS Agust, 0 AS Sep, 0 AS Okt, 0 AS Nop, 0 AS Des UNION
			SELECT 'Tahun2' AS Tahun, '2' AS MyGroup, 'SD2 / Unit'  AS Jenis, 0 AS Jan, 0 AS Feb, 0 AS Mar, 0 AS Apr, 0 AS Mei, 0 AS Jun, 0 AS Jul, 0 AS Agust, 0 AS Sep, 0 AS Okt, 0 AS Nop, 0 AS Des UNION
			SELECT 'Tahun2' AS Tahun, '2' AS MyGroup, 'Total RH + SD1 + SD2'  AS Jenis, 0 AS Jan, 0 AS Feb, 0 AS Mar, 0 AS Apr, 0 AS Mei, 0 AS Jun, 0 AS Jul, 0 AS Agust, 0 AS Sep, 0 AS Okt, 0 AS Nop, 0 AS Des UNION
			SELECT 'Tahun2' AS Tahun, '3' AS MyGroup, 'PDOP / Unit'  AS Jenis, 0 AS Jan, 0 AS Feb, 0 AS Mar, 0 AS Apr, 0 AS Mei, 0 AS Jun, 0 AS Jul, 0 AS Agust, 0 AS Sep, 0 AS Okt, 0 AS Nop, 0 AS Des UNION
			SELECT 'Tahun2' AS Tahun, '4' AS MyGroup, 'Gaji & Tunj / Unit'  AS Jenis, 0 AS Jan, 0 AS Feb, 0 AS Mar, 0 AS Apr, 0 AS Mei, 0 AS Jun, 0 AS Jul, 0 AS Agust, 0 AS Sep, 0 AS Okt, 0 AS Nop, 0 AS Des UNION
			SELECT 'Tahun2' AS Tahun, '4' AS MyGroup, 'Insentif / Unit'  AS Jenis, 0 AS Jan, 0 AS Feb, 0 AS Mar, 0 AS Apr, 0 AS Mei, 0 AS Jun, 0 AS Jul, 0 AS Agust, 0 AS Sep, 0 AS Okt, 0 AS Nop, 0 AS Des UNION
			SELECT 'Tahun2' AS Tahun, '4' AS MyGroup, 'Total Gaji & Tunj + Inst'  AS Jenis, 0 AS Jan, 0 AS Feb, 0 AS Mar, 0 AS Apr, 0 AS Mei, 0 AS Jun, 0 AS Jul, 0 AS Agust, 0 AS Sep, 0 AS Okt, 0 AS Nop, 0 AS Des UNION
			SELECT 'Tahun2' AS Tahun, '5' AS MyGroup, 'Biaya Bensin / Unit'  AS Jenis, 0 AS Jan, 0 AS Feb, 0 AS Mar, 0 AS Apr, 0 AS Mei, 0 AS Jun, 0 AS Jul, 0 AS Agust, 0 AS Sep, 0 AS Okt, 0 AS Nop, 0 AS Des ;
		CREATE INDEX Jenis ON TAnalisaRL(Jenis);
		CREATE INDEX MyGroup ON TAnalisaRL(MyGroup);
		ALTER TABLE TAnalisaRL MODIFY COLUMN Jan DECIMAL(19,2);
		ALTER TABLE TAnalisaRL MODIFY COLUMN Feb DECIMAL(19,2);
		ALTER TABLE TAnalisaRL MODIFY COLUMN Mar DECIMAL(19,2);
		ALTER TABLE TAnalisaRL MODIFY COLUMN Apr DECIMAL(19,2);
		ALTER TABLE TAnalisaRL MODIFY COLUMN Mei DECIMAL(19,2);
		ALTER TABLE TAnalisaRL MODIFY COLUMN Jun DECIMAL(19,2);
		ALTER TABLE TAnalisaRL MODIFY COLUMN Jul DECIMAL(19,2);
		ALTER TABLE TAnalisaRL MODIFY COLUMN Agust DECIMAL(19,2);
		ALTER TABLE TAnalisaRL MODIFY COLUMN Okt DECIMAL(19,2);
		ALTER TABLE TAnalisaRL MODIFY COLUMN Nop DECIMAL(19,2);
		ALTER TABLE TAnalisaRL MODIFY COLUMN Des DECIMAL(19,2);
		UPDATE TAnalisaRL SET Tahun = YEAR(xTgl1)-1 WHERE Tahun = 'Tahun1';
		UPDATE TAnalisaRL SET Tahun = YEAR(xTgl1) WHERE Tahun = 'Tahun2';

		SET X = 0;
		loop_label:  LOOP
			IF  X < 12 THEN
				SET  X = X + 1;
				SET Tgl1Before = STR_TO_DATE(CONCAT( YEAR(xTgl1)-1 , '-', X , '-', '01' ) , '%Y-%m-%d' ) ;
				SET Tgl2Before = LAST_DAY(Tgl1Before);
				DROP TEMPORARY TABLE IF EXISTS TFill;
				CREATE TEMPORARY TABLE IF NOT EXISTS TFill AS
				SELECT SPACE(25) AS Jenis, 0 AS Nilai;
				ALTER TABLE TFill MODIFY COLUMN Nilai DECIMAL(19,2);
				CREATE INDEX Jenis ON TFill(Jenis);
				DELETE FROM TFill;
				SELECT COUNT(tmotor.SKNo) INTO @Penjualan FROM tmotor
				INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
				INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
				AND  ttsk.SKtgl BETWEEN Tgl1Before AND Tgl2Before;
				INSERT INTO TFill SELECT 'Penjualan' AS Jenis,  @Penjualan  AS Nilai;
				SELECT SUM(DebetGL)-SUM(KreditGL) INTO @ReturHarga FROM ttgeneralledgerit WHERE NoAccount = '60060000'
				AND  TglGL BETWEEN Tgl1Before AND Tgl2Before;
				INSERT INTO TFill SELECT 'RH/Unit' AS Jenis,  ROUND(@ReturHarga/@Penjualan,2) AS Nilai;
				SELECT SUM(DebetGL)-SUM(KreditGL) INTO @SD1 FROM ttgeneralledgerit WHERE NoAccount = '30030000'
				AND  TglGL BETWEEN Tgl1Before AND Tgl2Before;
				INSERT INTO TFill SELECT 'SD1 / Unit' AS Jenis,  ROUND(@SD1/@Penjualan,2) AS Nilai;
				SELECT SUM(DebetGL)-SUM(KreditGL) INTO @SD2 FROM ttgeneralledgerit WHERE NoAccount = '30040000'
				AND  TglGL BETWEEN Tgl1Before AND Tgl2Before;
				INSERT INTO TFill SELECT 'SD2 / Unit' AS Jenis,  ROUND(@SD2/@Penjualan,2) AS Nilai;
				INSERT INTO TFill SELECT 'Total RH + SD1 + SD2' AS Jenis,  ROUND((@ReturHarga+@SD1+@SD2)/@Penjualan,2) AS Nilai;
				SELECT SUM(KreditGL)-SUM(DebetGL) INTO @PDOP FROM ttgeneralledgerit WHERE LEFT(NoAccount,1) =  '4'
				AND  TglGL BETWEEN Tgl1Before AND Tgl2Before;
				INSERT INTO TFill SELECT 'PDOP / Unit' AS Jenis,  ROUND(@PDOP/@Penjualan,2) AS Nilai;
				SELECT SUM(DebetGL)-SUM(KreditGL) INTO @GajiTunj FROM ttgeneralledgerit WHERE LEFT(NoAccount,4) =  '6001'
				AND  TglGL BETWEEN Tgl1Before AND Tgl2Before;
				INSERT INTO TFill SELECT 'Gaji & Tunj / Unit' AS Jenis,  ROUND(@GajiTunj/@Penjualan,2) AS Nilai;
				SELECT SUM(DebetGL)-SUM(KreditGL) INTO @Insentif FROM ttgeneralledgerit WHERE NoAccount =  '60020000'
				AND  TglGL BETWEEN Tgl1Before AND Tgl2Before;
				INSERT INTO TFill SELECT 'Insentif / Unit' AS Jenis,  ROUND(@Insentif/@Penjualan,2) AS Nilai;
				INSERT INTO TFill SELECT 'Total Gaji & Tunj + Inst' AS Jenis,  ROUND((@GajiTunj+@Insentif)/@Penjualan,2) AS Nilai;
				SELECT SUM(DebetGL)-SUM(KreditGL) INTO @Besin FROM ttgeneralledgerit WHERE NoAccount =  '60240000'
				AND  TglGL BETWEEN Tgl1Before AND Tgl2Before;
				INSERT INTO TFill SELECT 'Biaya Bensin / Unit' AS Jenis,  ROUND(@Besin/@Penjualan,2) AS Nilai;
				SET @MyQuery = CONCAT("UPDATE TAnalisaRL INNER JOIN TFill ON TAnalisaRL.Jenis = TFill.Jenis SET ",
				ELT(X,'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agust','Sep','Okt','Nop','Des')," = Nilai WHERE Tahun = YEAR('",xTgl1,"')-1;");
				PREPARE STMT FROM @MyQuery;
				EXECUTE Stmt;
				ITERATE  loop_label;
			ELSE
			  LEAVE  loop_label;
			END  IF;
		END LOOP;
		
		SET X = 0;
		loop_label:  LOOP
			IF  X < 12 THEN
				SET  X = X + 1;
				SET Tgl1Now = STR_TO_DATE(CONCAT( YEAR(xTgl1) , '-', X , '-', '01' ) , '%Y-%m-%d' ) ;
				SET Tgl2Now = LAST_DAY(Tgl1Now);
				DROP TEMPORARY TABLE IF EXISTS TFill;
				CREATE TEMPORARY TABLE IF NOT EXISTS TFill AS
				SELECT SPACE(25) AS Jenis, 0 AS Nilai;
				ALTER TABLE TFill MODIFY COLUMN Nilai DECIMAL(19,2);
				CREATE INDEX Jenis ON TFill(Jenis);
				DELETE FROM TFill;
				SELECT COUNT(tmotor.SKNo) INTO @Penjualan FROM tmotor
				INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
				INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
				WHERE ttsk.SKTgl BETWEEN Tgl1Now AND Tgl2Now;
				INSERT INTO TFill SELECT 'Penjualan' AS Jenis,  @Penjualan  AS Nilai;
				SELECT SUM(DebetGL)-SUM(KreditGL) INTO @ReturHarga FROM ttgeneralledgerit WHERE NoAccount = '60060000'
				AND  TglGL BETWEEN Tgl1Now AND Tgl2Now;
				INSERT INTO TFill SELECT 'RH/Unit' AS Jenis,  ROUND(@ReturHarga/@Penjualan,2) AS Nilai;
				SELECT SUM(DebetGL)-SUM(KreditGL) INTO @SD1 FROM ttgeneralledgerit WHERE NoAccount = '30030000'
				AND  TglGL BETWEEN Tgl1Now AND Tgl2Now;
				INSERT INTO TFill SELECT 'SD1 / Unit' AS Jenis,  ROUND(@SD1/@Penjualan,2) AS Nilai;
				SELECT SUM(DebetGL)-SUM(KreditGL) INTO @SD2 FROM ttgeneralledgerit WHERE NoAccount = '30040000'
				AND  TglGL BETWEEN Tgl1Now AND Tgl2Now;
				INSERT INTO TFill SELECT 'SD2 / Unit' AS Jenis,  ROUND(@SD2/@Penjualan,2) AS Nilai;
				INSERT INTO TFill SELECT 'Total RH + SD1 + SD2' AS Jenis,  ROUND((@ReturHarga+@SD1+@SD2)/@Penjualan,2) AS Nilai;
				SELECT SUM(KreditGL)-SUM(DebetGL) INTO @PDOP FROM ttgeneralledgerit WHERE LEFT(NoAccount,1) =  '4'
				AND  TglGL BETWEEN Tgl1Now AND Tgl2Now;
				INSERT INTO TFill SELECT 'PDOP / Unit' AS Jenis,  ROUND(@PDOP/@Penjualan,2) AS Nilai;
				SELECT SUM(DebetGL)-SUM(KreditGL) INTO @GajiTunj FROM ttgeneralledgerit WHERE LEFT(NoAccount,4) =  '6001'
				AND  TglGL BETWEEN Tgl1Now AND Tgl2Now;
				INSERT INTO TFill SELECT 'Gaji & Tunj / Unit' AS Jenis,  ROUND(@GajiTunj/@Penjualan,2) AS Nilai;
				SELECT SUM(DebetGL)-SUM(KreditGL) INTO @Insentif FROM ttgeneralledgerit WHERE NoAccount =  '60020000'
				AND  TglGL BETWEEN Tgl1Now AND Tgl2Now;
				INSERT INTO TFill SELECT 'Insentif / Unit' AS Jenis,  ROUND(@Insentif/@Penjualan,2) AS Nilai;
				INSERT INTO TFill SELECT 'Total Gaji & Tunj + Inst' AS Jenis,  ROUND((@GajiTunj+@Insentif)/@Penjualan,2) AS Nilai;
				SELECT SUM(DebetGL)-SUM(KreditGL) INTO @Besin FROM ttgeneralledgerit WHERE NoAccount =  '60240000'
				AND  TglGL BETWEEN Tgl1Now AND Tgl2Now;
				INSERT INTO TFill SELECT 'Biaya Bensin / Unit' AS Jenis,  ROUND(@Besin/@Penjualan,2) AS Nilai;
				SET @MyQuery = CONCAT("UPDATE TAnalisaRL INNER JOIN TFill ON TAnalisaRL.Jenis = TFill.Jenis SET ",ELT(X,'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agust','Sep','Okt','Nop','Des')," = IFNULL(Nilai,0) WHERE Tahun = YEAR('",xTgl1,"');");
				PREPARE STMT FROM @MyQuery;
				EXECUTE Stmt;
				ITERATE  loop_label;
			ELSE
				LEAVE  loop_label;
			END  IF;
		END LOOP;
		SET @MyQuery = CONCAT("SELECT * FROM TAnalisaRL;");		   
	END IF;

	IF xTipe = "Analisa Opex" THEN
		SET xTgl1 = DATE_ADD(LAST_DAY(DATE_ADD(xTgl2, INTERVAL -1 MONTH)), INTERVAL 1 DAY);
		IF xEOM = '1' THEN
			SET xTgl2 = LAST_DAY(xTgl2);
		END IF;
		SET Tgl1Now = xTgl1;
		SET Tgl2Now = xTgl2;
		SET Tgl1Before = DATE_ADD(xTgl1, INTERVAL -1 MONTH);
		SET Tgl2Before = LAST_DAY(DATE_ADD(xTgl2, INTERVAL -1 MONTH));

		DROP TEMPORARY TABLE IF EXISTS TOpex;
		CREATE TEMPORARY TABLE IF NOT EXISTS TOpex AS
		SELECT '01' AS Nomor, 'PENJUALAN / UNIT' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT  UNION
		SELECT '02' AS Nomor, 'HPP / UNIT' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT UNION
		SELECT '03' AS Nomor, 'PROFIT / UNIT (SEBELUM BBN)' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT UNION
		SELECT '04' AS Nomor, 'BY BBN / UNIT' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT UNION
		SELECT '05' AS Nomor, 'PROFIT / UNIT (SETELAH BBN)' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT UNION
		SELECT '06' AS Nomor, '--1' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT UNION
		SELECT '07' AS Nomor, 'BY OPERASIONAL (OPEX)' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT UNION
		SELECT '08' AS Nomor, 'PROFIT - OPEX (PROFIT A)' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT UNION
		SELECT '09' AS Nomor, '--2' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT UNION
		SELECT '10' AS Nomor, 'BY. GAJI (UPAH)' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT UNION
		SELECT '11' AS Nomor, 'PROFIT A - By. GAJI (B)' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT UNION
		SELECT '12' AS Nomor, '--3' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT UNION
		SELECT '13' AS Nomor, 'SD-1' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT UNION
		SELECT '14' AS Nomor, 'SD-2' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT UNION
		SELECT '15' AS Nomor, 'RETUR' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT UNION
		SELECT '16' AS Nomor, 'TOT. POTONGAN (C)' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT UNION
		SELECT '17' AS Nomor, '--4' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT UNION
		SELECT '18' AS Nomor, 'PROFIT MURNI (B-C)' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT UNION
		SELECT '19' AS Nomor, '--5' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT UNION
		SELECT '20' AS Nomor, 'PDOP / UNIT' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT UNION
		SELECT '21' AS Nomor, '--6' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT UNION
		SELECT '22' AS Nomor, 'PROFIT SETELAH PDOP (PER UNIT)' AS Keterangan, 0 AS Colom1, 0 AS Persen1, 0 AS Colom2, 0 AS Persen2, 0 AS Colom3, 0 AS Persen3, 0 AS Colom4, 0 AS Persen4, 0 AS Colom5, 0 AS Persen5, 0 AS Colom6, 0 AS Persen6, 0 AS ColomT, 0 AS PersenT;
		CREATE INDEX Keterangan ON TOpex(Keterangan);
		ALTER TABLE TOpex MODIFY COLUMN Colom1 DECIMAL(19,2);
		ALTER TABLE TOpex MODIFY COLUMN Colom2 DECIMAL(19,2);
		ALTER TABLE TOpex MODIFY COLUMN Colom3 DECIMAL(19,2);
		ALTER TABLE TOpex MODIFY COLUMN Colom4 DECIMAL(19,2);
		ALTER TABLE TOpex MODIFY COLUMN Colom5 DECIMAL(19,2);
		ALTER TABLE TOpex MODIFY COLUMN Colom6 DECIMAL(19,2);
		ALTER TABLE TOpex MODIFY COLUMN ColomT DECIMAL(19,2);
		ALTER TABLE TOpex MODIFY COLUMN Persen1 DECIMAL(5,2);
		ALTER TABLE TOpex MODIFY COLUMN Persen2 DECIMAL(5,2);
		ALTER TABLE TOpex MODIFY COLUMN Persen3 DECIMAL(5,2);
		ALTER TABLE TOpex MODIFY COLUMN Persen4 DECIMAL(5,2);
		ALTER TABLE TOpex MODIFY COLUMN Persen5 DECIMAL(5,2);
		ALTER TABLE TOpex MODIFY COLUMN Persen6 DECIMAL(5,2);
		ALTER TABLE TOpex MODIFY COLUMN PersenT DECIMAL(5,2);

		IF MONTH(xTgl1) < 7 THEN
			SET Tgl1Now  = STR_TO_DATE( CONCAT( YEAR( xTgl1 ) , '-', '01' , '-', '01' ) , '%Y-%m-%d' ) ;
			SET Tgl2Now = STR_TO_DATE( CONCAT( YEAR( xTgl1 ) , '-', '01' , '-', '31' ) , '%Y-%m-%d' ) ;
		ELSE
			SET Tgl1Now  = STR_TO_DATE( CONCAT( YEAR( xTgl1 ) , '-', '07' , '-', '01' ) , '%Y-%m-%d' ) ;
			SET Tgl2Now = STR_TO_DATE( CONCAT( YEAR( xTgl1 ) , '-', '07' , '-', '31' ) , '%Y-%m-%d' ) ;
		END IF;

		SET X = 0;
		loop_label:  LOOP
			IF  X < 5 THEN
				SET  X = X + 1;
				SELECT COUNT(tmotor.SKNo) INTO @MyUnit FROM tmotor
				INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
				INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
				AND  ttsk.SKtgl BETWEEN Tgl1Now AND Tgl2Now;
				CALL rglNeraca(Tgl1Now, Tgl2Now, Tgl2Now);
				DROP TEMPORARY TABLE IF EXISTS TFill;
				CREATE TEMPORARY TABLE IF NOT EXISTS TFill AS
				SELECT SPACE(2) AS Jenis, 0 Persen, 0 AS Nilai;
				ALTER TABLE TFill MODIFY COLUMN Nilai DECIMAL(19,2);
				ALTER TABLE TFill MODIFY COLUMN Persen DECIMAL(5,2);
				CREATE INDEX Jenis ON TFill(Jenis);
				DELETE FROM TFill;
				SELECT Saldo - OpeningBalance INTO @PENJUALAN FROM traccountsaldo WHERE NoAccount = '30010000' ;
				INSERT INTO TFill SELECT '01' AS Jenis, 0 AS Persen, ROUND(@PENJUALAN / @MyUnit,2) AS Nilai;
				SELECT Saldo - OpeningBalance INTO @HPP FROM traccountsaldo WHERE NoAccount = '50000000' ;
				INSERT INTO TFill SELECT '02' AS Jenis, 0 AS Persen, ROUND(@HPP / @MyUnit,2) AS Nilai;
				UPDATE TFILL SET Persen = (@HPP / @MyUnit)/ (@Penjualan/@MyUnit) * 100 WHERE Jenis = '02';
				INSERT INTO TFill SELECT '03' AS Jenis, 0 AS Persen, ROUND(@PENJUALAN / @MyUnit,2)-ROUND(@HPP / @MyUnit,2) AS Nilai;
				SELECT Saldo - OpeningBalance INTO @BBN FROM traccountsaldo WHERE NoAccount = '60050000' ;
				INSERT INTO TFill SELECT '04' AS Jenis, 0 AS Persen, ROUND(@BBN / @MyUnit,2) AS Nilai;
				UPDATE TFILL SET Persen = (@BBN / @MyUnit)/ (@Penjualan/@MyUnit) * 100 WHERE Jenis = '04';
				INSERT INTO TFill SELECT '05' AS Jenis, 0 AS Persen, ROUND(@PENJUALAN / @MyUnit,2)-ROUND(@HPP / @MyUnit,2)-ROUND(@BBN / @MyUnit,2) AS Nilai;
				SELECT SUM(Saldo - OpeningBalance) INTO @BYOPERASIONAL FROM traccountsaldo
				WHERE NoAccount LIKE '6%' AND NoAccount NOT LIKE '6001%' AND NoAccount <> '60020000' AND NoAccount <> '60050000' AND NoAccount <> '60060000' AND   JenisAccount = 'Detail';
				INSERT INTO TFill SELECT '07' AS Jenis, 0 AS Persen, ROUND(@BYOPERASIONAL / @MyUnit,2) AS Nilai;
				UPDATE TFILL SET Persen = ((@BYOPERASIONAL / @MyUnit)/ (@Penjualan/@MyUnit)) * 100 WHERE Jenis = '07';
				INSERT INTO TFill SELECT '08' AS Jenis, 0 AS Persen, ROUND(@PENJUALAN / @MyUnit,2)-ROUND(@HPP / @MyUnit,2)-ROUND(@BBN / @MyUnit,2)-ROUND(@BYOPERASIONAL / @MyUnit,2) AS Nilai;
				SELECT SUM(Saldo - OpeningBalance) INTO @BYGAJI FROM traccountsaldo
				WHERE NoAccount = '60010000' OR NoAccount = '60020000' AND   JenisAccount = 'Detail';
				INSERT INTO TFill SELECT '10' AS Jenis, 0 AS Persen, ROUND(@BYGAJI / @MyUnit,2) AS Nilai;
				UPDATE TFILL SET Persen = ((@BYGAJI / @MyUnit)/ (@Penjualan/@MyUnit)) * 100 WHERE Jenis = '10';
				INSERT INTO TFill SELECT '11' AS Jenis, 0 AS Persen, ROUND(@PENJUALAN / @MyUnit,2)-ROUND(@HPP / @MyUnit,2)-ROUND(@BBN / @MyUnit,2)-ROUND(@BYOPERASIONAL / @MyUnit,2)-ROUND(@BYGAJI / @MyUnit,2) AS Nilai;
				SELECT Saldo - OpeningBalance INTO @SD1 FROM traccountsaldo WHERE NoAccount = '30030000' ;
				INSERT INTO TFill SELECT '13' AS Jenis, 0 AS Persen, -ROUND(@SD1 / @MyUnit,2) AS Nilai;
				SELECT Saldo - OpeningBalance INTO @SD2 FROM traccountsaldo WHERE NoAccount = '30040000' ;
				INSERT INTO TFill SELECT '14' AS Jenis, 0 AS Persen, -ROUND(@SD2 / @MyUnit,2) AS Nilai;
				SELECT Saldo - OpeningBalance INTO @RETUR FROM traccountsaldo WHERE NoAccount = '60060000' ;
				INSERT INTO TFill SELECT '15' AS Jenis, 0 AS Persen, ROUND(@RETUR / @MyUnit,2) AS Nilai;
				INSERT INTO TFill SELECT '16' AS Jenis, 0 AS Persen, ROUND(@RETUR / @MyUnit,2)-ROUND(@SD2 / @MyUnit,2)-ROUND(@SD1 / @MyUnit,2) AS Nilai;
				UPDATE TFILL SET Persen = ((ROUND(@RETUR / @MyUnit,2)-ROUND(@SD2 / @MyUnit,2)-ROUND(@SD1 / @MyUnit,2))/ (@Penjualan/@MyUnit)) * 100 WHERE Jenis = '16';

				INSERT INTO TFill SELECT '18' AS Jenis, 0 AS Persen,
					ROUND(@PENJUALAN / @MyUnit,2)-ROUND(@HPP / @MyUnit,2) - ROUND(@BBN / @MyUnit,2) - ROUND(@BYOPERASIONAL / @MyUnit,2) - ROUND(@BYGAJI / @MyUnit,2)
				 -(ROUND(@RETUR / @MyUnit,2)-ROUND(@SD2 / @MyUnit,2)-ROUND(@SD1 / @MyUnit,2)) AS Nilai;
				SELECT Saldo - OpeningBalance INTO @PDOP FROM traccountsaldo WHERE NoAccount = '40000000' ;
				INSERT INTO TFill SELECT '20' AS Jenis, 0 AS Persen, ROUND(@PDOP / @MyUnit,2) AS Nilai;
				UPDATE TFILL SET Persen = ((ROUND(@PDOP / @MyUnit,2))/ (@Penjualan/@MyUnit)) * 100 WHERE Jenis = '20';

				INSERT INTO TFill SELECT '22' AS Jenis, 0 AS Persen,
					ROUND(@PENJUALAN / @MyUnit,2) - ROUND(@HPP / @MyUnit,2) - ROUND(@BBN / @MyUnit,2) - ROUND(@BYOPERASIONAL / @MyUnit,2) - ROUND(@BYGAJI / @MyUnit,2)
				 -(ROUND(@RETUR / @MyUnit,2)-ROUND(@SD2 / @MyUnit,2)-ROUND(@SD1 / @MyUnit,2))
				 + ROUND(@PDOP / @MyUnit,2)
				 AS Nilai;

				SET @MyQuery = CONCAT("UPDATE TOpex INNER JOIN TFill ON TOpex.Nomor = TFill.Jenis
				SET ",ELT(X,'Colom1','Colom2','Colom3','Colom4','Colom5','Colom6')," = IFNULL(Nilai,0),
				", ELT(X,'Persen1','Persen2','Persen3','Persen4','Persen5','Persen6')," = IFNULL(Persen,0)
				;");
				PREPARE STMT FROM @MyQuery;
				EXECUTE Stmt;

				SET Tgl1Now = DATE_ADD(LAST_DAY(DATE_ADD(Tgl1Now, INTERVAL 0 MONTH)),INTERVAL 1 DAY);
				SET Tgl2Now = LAST_DAY(DATE_ADD(Tgl2Now, INTERVAL 1 MONTH));
				ITERATE  loop_label;
			ELSE
				LEAVE  loop_label;
			END  IF;
		END LOOP;
		SET @Pembagi = 0;
		SET @Count = 0; SELECT 1 INTO @Count FROM TOpex WHERE Colom1 <> 0 LIMIT 0,1; SET @Pembagi = @Pembagi + @Count;
		SET @Count = 0; SELECT 1 INTO @Count FROM TOpex WHERE Colom2 <> 0 LIMIT 0,1;  SET @Pembagi = @Pembagi + @Count;
		SET @Count = 0; SELECT 1 INTO @Count FROM TOpex WHERE Colom3 <> 0 LIMIT 0,1;  SET @Pembagi = @Pembagi + @Count;
		SET @Count = 0; SELECT 1 INTO @Count FROM TOpex WHERE Colom4 <> 0 LIMIT 0,1;  SET @Pembagi = @Pembagi + @Count;
		SET @Count = 0; SELECT 1 INTO @Count FROM TOpex WHERE Colom5 <> 0 LIMIT 0,1;  SET @Pembagi = @Pembagi + @Count;
		SET @Count = 0; SELECT 1 INTO @Count FROM TOpex WHERE Colom6 <> 0 LIMIT 0,1;  SET @Pembagi = @Pembagi + @Count;
		UPDATE TOpex SET ColomT = ROUND((Colom1+Colom2+Colom3+Colom4+Colom5+Colom6)/@Pembagi,2);
		UPDATE TOpex SET PersenT = ROUND((Persen1+Persen2+Persen3+Persen4+Persen5+Persen6)/@Pembagi,2);
		SET @MyQuery = CONCAT("SELECT * FROM TOpex  ;");		   
	END IF;
	
	IF xTipe = "Profit Unit" THEN
  SET Tgl1Now = STR_TO_DATE( CONCAT( YEAR( xTgl1 ) , '-', '01' , '-', '01' ) , '%Y-%m-%d' ) ;
  SET Tgl2Now = STR_TO_DATE( CONCAT( YEAR( xTgl1 ) , '-', '01' , '-', '31' ) , '%Y-%m-%d' ) ;

		DROP TEMPORARY TABLE IF EXISTS TProvitUnit;
		CREATE TEMPORARY TABLE IF NOT EXISTS TProvitUnit AS
SELECT  Kiri.MotorGroup, Kiri.MotorKategori, Kiri.MotorNama, IFNULL(Jan.Profit, 0) AS Jan, IFNULL(Feb.Profit, 0) AS Feb, IFNULL(Mar.Profit, 0) AS Mar, IFNULL(Apr.Profit, 0) AS Apr, IFNULL(Mei.Profit, 0) AS Mei, IFNULL(Jun.Profit, 0) AS Jun, IFNULL(Jul.Profit, 0) AS Jul, IFNULL(Agust.Profit, 0) AS Agust, IFNULL(Sep.Profit, 0) AS Sep, IFNULL(Okt.Profit, 0) AS Okt, IFNULL(Nop.Profit, 0) AS Nop, IFNULL(Des.Profit, 0) AS Des
FROM
  (SELECT tdmotorkategori.MotorGroup, tdmotorkategori.MotorKategori, tdmotortype.MotorNama, tdmotortype.MotorType FROM tdmotortype
    INNER JOIN tdmotorkategori ON (tdmotorkategori.MotorKategori = tdmotortype.MotorKategori)
  ORDER BY tdmotorkategori.MotorNo, tdmotorkategori.MotorGroup, tdmotorkategori.MotorKategori, tdmotortype.MotorNama) Kiri
  LEFT OUTER JOIN
    (SELECT Motortype, (AVG(ttdk.DKHarga) - AVG(tmotor.SKHarga) - AVG(ttdk.BBN)) AS Profit FROM tmotor
      INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
      INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
    WHERE ttdk.BBN > 0 AND ttsk.SKtgl BETWEEN Tgl1Now AND Tgl2Now
    GROUP BY Motortype) Jan
    ON Kiri.MotorType = Jan.MotorType
  LEFT OUTER JOIN
    (SELECT Motortype, (AVG(ttdk.DKHarga) - AVG(tmotor.SKHarga) - AVG(ttdk.BBN)) AS Profit FROM tmotor
      INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
      INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
    WHERE ttdk.BBN > 0 AND ttsk.SKtgl BETWEEN DATE_ADD(Tgl1Now, INTERVAL +1 MONTH)AND LAST_DAY(DATE_ADD(Tgl2Now, INTERVAL +1 MONTH))
    GROUP BY Motortype) Feb
    ON Kiri.MotorType = Feb.MotorType
  LEFT OUTER JOIN 
    (SELECT Motortype, (AVG(ttdk.DKHarga) - AVG(tmotor.SKHarga) - AVG(ttdk.BBN)) AS Profit FROM tmotor
      INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
      INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
    WHERE ttdk.BBN > 0 AND ttsk.SKtgl BETWEEN DATE_ADD(Tgl1Now, INTERVAL +2 MONTH)AND LAST_DAY(DATE_ADD(Tgl2Now, INTERVAL +2 MONTH))
    GROUP BY Motortype) Mar 
    ON Kiri.MotorType = Mar.MotorType 
  LEFT OUTER JOIN 
    (SELECT Motortype, (AVG(ttdk.DKHarga) - AVG(tmotor.SKHarga) - AVG(ttdk.BBN)) AS Profit FROM tmotor
      INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
      INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
    WHERE ttdk.BBN > 0 AND ttsk.SKtgl BETWEEN DATE_ADD(Tgl1Now, INTERVAL +3 MONTH)AND LAST_DAY(DATE_ADD(Tgl2Now, INTERVAL +3 MONTH))
    GROUP BY Motortype) Apr
    ON Kiri.MotorType = Apr.MotorType 
  LEFT OUTER JOIN 
    (SELECT Motortype, (AVG(ttdk.DKHarga) - AVG(tmotor.SKHarga) - AVG(ttdk.BBN)) AS Profit FROM tmotor
      INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
      INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo 
    WHERE ttdk.BBN > 0 AND ttsk.SKtgl BETWEEN DATE_ADD(Tgl1Now, INTERVAL +4 MONTH)AND LAST_DAY(DATE_ADD(Tgl2Now, INTERVAL +4 MONTH))
    GROUP BY Motortype) Mei 
    ON Kiri.MotorType = Mei.MotorType 
  LEFT OUTER JOIN 
    (SELECT Motortype, (AVG(ttdk.DKHarga) - AVG(tmotor.SKHarga) - AVG(ttdk.BBN)) AS Profit FROM tmotor
      INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
      INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
    WHERE ttdk.BBN > 0 AND ttsk.SKtgl BETWEEN DATE_ADD(Tgl1Now, INTERVAL +5 MONTH)AND LAST_DAY(DATE_ADD(Tgl2Now, INTERVAL +5 MONTH))
    GROUP BY Motortype) Jun
    ON Kiri.MotorType = Jun.MotorType
  LEFT OUTER JOIN
    (SELECT Motortype, (AVG(ttdk.DKHarga) - AVG(tmotor.SKHarga) - AVG(ttdk.BBN)) AS Profit FROM tmotor
      INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
      INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
    WHERE ttdk.BBN > 0 AND ttsk.SKtgl BETWEEN DATE_ADD(Tgl1Now, INTERVAL +6 MONTH)AND LAST_DAY(DATE_ADD(Tgl2Now, INTERVAL +6 MONTH))
    GROUP BY Motortype) Jul
    ON Kiri.MotorType = Jul.MotorType
  LEFT OUTER JOIN
    (SELECT Motortype, (AVG(ttdk.DKHarga) - AVG(tmotor.SKHarga) - AVG(ttdk.BBN)) AS Profit FROM tmotor
      INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
      INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
    WHERE ttdk.BBN > 0 AND ttsk.SKtgl BETWEEN DATE_ADD(Tgl1Now, INTERVAL +7 MONTH)AND LAST_DAY(DATE_ADD(Tgl2Now, INTERVAL +7 MONTH))
    GROUP BY Motortype) Agust
    ON Kiri.MotorType = Agust.MotorType
  LEFT OUTER JOIN
    (SELECT Motortype, (AVG(ttdk.DKHarga) - AVG(tmotor.SKHarga) - AVG(ttdk.BBN)) AS Profit FROM tmotor
      INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
      INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
    WHERE ttdk.BBN > 0 AND ttsk.SKtgl BETWEEN DATE_ADD(Tgl1Now, INTERVAL +8 MONTH)AND LAST_DAY(DATE_ADD(Tgl2Now, INTERVAL +8 MONTH))
    GROUP BY Motortype) Sep
    ON Kiri.MotorType = Sep.MotorType
  LEFT OUTER JOIN
    (SELECT Motortype, (AVG(ttdk.DKHarga) - AVG(tmotor.SKHarga) - AVG(ttdk.BBN)) AS Profit FROM tmotor
      INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
      INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
    WHERE ttdk.BBN > 0 AND ttsk.SKtgl BETWEEN DATE_ADD(Tgl1Now, INTERVAL +9 MONTH)AND LAST_DAY(DATE_ADD(Tgl2Now, INTERVAL +9 MONTH))
    GROUP BY Motortype) Okt
    ON Kiri.MotorType = Okt.MotorType
  LEFT OUTER JOIN
    (SELECT Motortype, (AVG(ttdk.DKHarga) - AVG(tmotor.SKHarga) - AVG(ttdk.BBN)) AS Profit FROM tmotor
      INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
      INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
    WHERE ttdk.BBN > 0 AND ttsk.SKtgl BETWEEN DATE_ADD(Tgl1Now, INTERVAL +10 MONTH)AND LAST_DAY(DATE_ADD(Tgl2Now, INTERVAL +10 MONTH))
    GROUP BY Motortype) Nop
    ON Kiri.MotorType = Nop.MotorType
  LEFT OUTER JOIN
    (SELECT Motortype, (AVG(ttdk.DKHarga) - AVG(tmotor.SKHarga) - AVG(ttdk.BBN)) AS Profit FROM tmotor
      INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
      INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
    WHERE ttdk.BBN > 0 AND ttsk.SKtgl BETWEEN DATE_ADD(Tgl1Now, INTERVAL +11 MONTH)AND LAST_DAY(DATE_ADD(Tgl2Now, INTERVAL +11 MONTH))
    GROUP BY Motortype) Des
    ON Kiri.MotorType = Des.MotorType;

		DROP TEMPORARY TABLE IF EXISTS TProvitGROUP;
		CREATE TEMPORARY TABLE IF NOT EXISTS TProvitGROUP AS
    SELECT 'ALL' AS MotorGroup , 'ALL' AS MotorKategori, MotorGroup AS MotorNama,
    SUM(Jan) AS Jan, SUM(Feb) AS Feb, SUM(Mar) AS Mar, SUM(Apr) AS Apr, SUM(Mei) AS Mei, SUM(Jun) AS Jun, SUM(Jul) AS Jul, SUM(Agust) AS Agust, SUM(Sep) AS Sep, SUM(Okt) AS Okt, SUM(Nop) AS Nop, SUM(Des) AS Des
    FROM TProvitUnit
    GROUP BY TProvitUnit.MotorGroup
    ORDER BY IF(MotorGroup='Cub',0,IF(MotorGroup='AT',1,2));

    INSERT INTO TProvitUnit SELECT * FROM TProvitGROUP;

    DELETE FROM TProvitUnit WHERE Jan = 0 AND Feb = 0 AND Mar = 0 AND Apr = 0 AND Mei = 0 AND Jun = 0 AND Jul = 0 AND Agust = 0 AND Sep = 0 AND Okt = 0 AND Nop = 0 AND Des = 0;
   

		SET @MyQuery = CONCAT("SELECT * FROM TProvitUnit;");		   
	END IF;
	
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$

DELIMITER ;


