DELIMITER $$

DROP PROCEDURE IF EXISTS `rGLStockHitung`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rGLStockHitung`(IN xTgl1 DATE,IN xTgl2 DATE)
BEGIN
/*
CALL rGLStockHitung('2020-02-01','2020-02-29');
*/       
	DROP TEMPORARY TABLE IF EXISTS StockHarian;
	CREATE TEMPORARY TABLE IF NOT EXISTS StockHarian AS
	SELECT tdmotortype.MotorType, tdmotortype.TypeStatus, tdmotortype.MotorNama, IFNULL(StockMotor.Jumlah,0) AS SINJum, IFNULL(StockMotor.FBHarga,0) AS SINHrg,
	0000000 AS SALJum,	00000000000000.00 AS SALHrg,
	0000000 AS INJum,	00000000000000.00 AS INHrg,
	0000000 AS SBJum,	00000000000000.00 AS SBHrg,
	0000000 AS SSJum,	00000000000000.00 AS SSHrg,
	0000000 AS FBJum,	00000000000000.00 AS FBHrg,
	0000000 AS PDJum,	00000000000000.00 AS PDHrg,
	0000000 AS RKJum,	00000000000000.00 AS RKHrg,
	0000000 AS SDJum,	00000000000000.00 AS SDHrg,
	0000000 AS SKJum,	00000000000000.00 AS SKHrg,
	0000000 AS SARJum,00000000000000.00 AS SARHrg
   FROM tdmotortype 
           LEFT OUTER JOIN (SELECT tmotor.MotorType, COUNT(LokasiMotor.Kondisi) AS Jumlah, SUM(tmotor.FBHarga) AS FBHarga 
                      FROM ( SELECT tvmotorlokasi.MotorAutoN, tvmotorlokasi.MotorNoMesin, tvmotorlokasi.LokasiKode, tvmotorlokasi.NoTrans, tvmotorlokasi.Jam, tvmotorlokasi.Kondisi 
                             FROM tvmotorlokasi 
                             INNER JOIN (SELECT tvmotorlokasi_1.MotorAutoN, tvmotorlokasi_1.MotorNoMesin , MAX(tvmotorlokasi_1.Jam) AS Jam 
                                         FROM tvmotorlokasi tvmotorlokasi_1 
                                         WHERE tvmotorlokasi_1.Jam <=  DATE_ADD(xTgl1, INTERVAL -1 SECOND)  
                                         GROUP BY tvmotorlokasi_1.MotorNoMesin, tvmotorlokasi_1.MotorAutoN) B 
                                      ON tvmotorlokasi.MotorNoMesin = B.MotorNoMesin AND tvmotorlokasi.Jam = B.Jam AND tvmotorlokasi.MotorAutoN = B.MotorAutoN 
                                  WHERE tvmotorlokasi.Kondisi IN ('IN','OUT Pos') ) LokasiMotor 
                              INNER JOIN tmotor 
                                ON tmotor.MotorNoMesin = LokasiMotor.MotorNoMesin AND tmotor.MotorAutoN = LokasiMotor.MotorAutoN 
                            WHERE tmotor.DealerKode LIKE '%' 
                                AND LokasiMotor.LokasiKode LIKE '%' 
                            GROUP BY tmotor.MotorType) StockMotor 
             ON tdmotortype.MotorType = StockMotor.MotorType 
         ORDER BY tdmotortype.MotorType;
	CREATE INDEX MotorType ON StockHarian(MotorType);

	UPDATE StockHarian SET SALJum = SINJum, SALHrg = SINHrg;

	DROP TEMPORARY TABLE IF EXISTS Stock;
	CREATE TEMPORARY TABLE IF NOT EXISTS Stock AS
	SELECT tdmotortype.MotorType, IFNULL(Stock.Jum, 0) AS Jum, IFNULL(Stock.Hrg, 0) AS Hrg 
      FROM tdmotortype 
        LEFT OUTER JOIN 
        (SELECT tdmotortype.MotorType, COUNT(tmotor.SSNo) AS Jum, SUM(tmotor.SSHarga) AS Hrg 
         FROM tdmotortype 
         INNER JOIN tmotor ON tdmotortype.MotorType = tmotor.MotorType 
         INNER JOIN ttSS ON tmotor.SSNo = ttSS.SSNo 
        WHERE ttSS.SSTgl BETWEEN CONCAT(xTgl1, " 00:00:00") AND CONCAT(xTgl2, " 23:59:59")
         GROUP BY tdmotortype.MotorType) Stock ON tdmotortype.MotorType = Stock.MotorType 
      ORDER BY tdmotortype.MotorType;
   CREATE INDEX MotorType ON Stock(MotorType);
	UPDATE StockHarian INNER JOIN Stock ON StockHarian.MotorType = Stock.MotorType SET SSJum = Jum, SSHrg = Hrg;

	DROP TEMPORARY TABLE IF EXISTS Stock;
	CREATE TEMPORARY TABLE IF NOT EXISTS Stock AS
	SELECT tdmotortype.MotorType, IFNULL(Fill.Jum,0) AS Jum , IFNULL(Fill.Hrg,0) AS Hrg 
      FROM tdmotortype 
      LEFT OUTER JOIN 
      (SELECT MotorType, COUNT(tmotor.MotorNoMesin) AS Jum, (SUM(tmotor.FBHarga)-SUM(tmotor.SSHarga)) AS Hrg 
       FROM tmotor 
       INNER JOIN ttfb ON tmotor.FBNo = ttfb.FbNo WHERE ttfb.FbTgl BETWEEN CONCAT(xTgl1, " 00:00:00") AND CONCAT(xTgl2, " 23:59:59")
       GROUP BY tmotor.MotorType ) Fill 
      ON tdmotortype.MotorType = Fill.MotorType 
      ORDER BY  tdmotortype.MotorType;
   CREATE INDEX MotorType ON Stock(MotorType);
	UPDATE StockHarian INNER JOIN Stock ON StockHarian.MotorType = Stock.MotorType SET FBJum = Jum, FBHrg = Hrg;

	DROP TEMPORARY TABLE IF EXISTS Stock;
	CREATE TEMPORARY TABLE IF NOT EXISTS Stock AS
	SELECT tdmotortype.MotorType, IFNULL(Stock.Jum, 0) AS Jum, IFNULL(Stock.Hrg, 0) AS Hrg 
      FROM tdmotortype 
        LEFT OUTER JOIN
        (SELECT tdmotortype.MotorType, COUNT(tmotor.PDNo) AS Jum, SUM(tmotor.FBHarga) AS Hrg 
         FROM tdmotortype 
          INNER JOIN  tmotor ON tdmotortype.MotorType = tmotor.MotorType 
          INNER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo 
        WHERE ttpd.PDTgl BETWEEN CONCAT(xTgl1, " 00:00:00") AND CONCAT(xTgl2, " 23:59:59")
         GROUP BY tdmotortype.MotorType) Stock ON tdmotortype.MotorType = Stock.MotorType 
      ORDER BY tdmotortype.MotorType;
   CREATE INDEX MotorType ON Stock(MotorType);
	UPDATE StockHarian INNER JOIN Stock ON StockHarian.MotorType = Stock.MotorType SET PDJum = Jum, PDHrg = Hrg;
	
	DROP TEMPORARY TABLE IF EXISTS Stock;
	CREATE TEMPORARY TABLE IF NOT EXISTS Stock AS
	SELECT tdmotortype.MotorType, IFNULL(Stock.Jum, 0) AS Jum, IFNULL(Stock.Hrg, 0) AS Hrg 
      FROM tdmotortype 
        LEFT OUTER JOIN 
        (SELECT tdmotortype.MotorType, COUNT(tmotor.RKNo) AS Jum, SUM(tmotor.FBHarga) AS Hrg 
         FROM tdmotortype
          INNER JOIN tmotor ON tdmotortype.MotorType = tmotor.MotorType 
          INNER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo 
        WHERE ttRK.RKTgl BETWEEN CONCAT(xTgl1, " 00:00:00") AND CONCAT(xTgl2, " 23:59:59")
         GROUP BY tdmotortype.MotorType) Stock ON tdmotortype.MotorType = Stock.MotorType 
      ORDER BY tdmotortype.MotorType ;
   CREATE INDEX MotorType ON Stock(MotorType);
	UPDATE StockHarian INNER JOIN Stock ON StockHarian.MotorType = Stock.MotorType SET RKJum = Jum, RKHrg = Hrg;

	DROP TEMPORARY TABLE IF EXISTS Stock;
	CREATE TEMPORARY TABLE IF NOT EXISTS Stock AS
	SELECT tdmotortype.MotorType, IFNULL(Stock.Jum, 0) AS Jum, IFNULL(Stock.Hrg, 0) AS Hrg 
      FROM tdmotortype 
        LEFT OUTER JOIN 
        (SELECT tdmotortype.MotorType, COUNT(tmotor.SDNo) AS Jum, SUM(tmotor.FBHarga) AS Hrg 
         FROM tdmotortype 
          INNER JOIN  tmotor ON tdmotortype.MotorType = tmotor.MotorType 
          INNER JOIN ttsd ON tmotor.SDNo = ttSd.SDNo 
        WHERE ttsd.SDTgl BETWEEN CONCAT(xTgl1, " 00:00:00") AND CONCAT(xTgl2, " 23:59:59")
         GROUP BY tdmotortype.MotorType) Stock ON tdmotortype.MotorType = Stock.MotorType 
      ORDER BY tdmotortype.MotorType;
   CREATE INDEX MotorType ON Stock(MotorType);
	UPDATE StockHarian INNER JOIN Stock ON StockHarian.MotorType = Stock.MotorType SET SDJum = Jum, SDHrg = Hrg;
	
	DROP TEMPORARY TABLE IF EXISTS Stock;
	CREATE TEMPORARY TABLE IF NOT EXISTS Stock AS
	SELECT tdmotortype.MotorType, IFNULL(Stock.Jum, 0) AS Jum, IFNULL(Stock.Hrg, 0) AS Hrg 
      FROM tdmotortype 
        LEFT OUTER JOIN 
        (SELECT tdmotortype.MotorType, COUNT(tmotor.SKNo) AS Jum, SUM(tmotor.FBHarga) AS Hrg 
         FROM tdmotortype 
          INNER JOIN  tmotor ON tdmotortype.MotorType = tmotor.MotorType 
          INNER JOIN ttSK ON tmotor.SKNo = ttSK.SKNo 
        WHERE ttSK.SKTgl BETWEEN CONCAT(xTgl1, " 00:00:00") AND CONCAT(xTgl2, " 23:59:59")
         GROUP BY tdmotortype.MotorType) Stock ON tdmotortype.MotorType = Stock.MotorType 
      ORDER BY tdmotortype.MotorType;
   CREATE INDEX MotorType ON Stock(MotorType);
	UPDATE StockHarian INNER JOIN Stock ON StockHarian.MotorType = Stock.MotorType SET SKJum = Jum, SKHrg = Hrg;
	
	
	
	DROP TEMPORARY TABLE IF EXISTS Stock;
	CREATE TEMPORARY TABLE IF NOT EXISTS Stock AS
	SELECT tdmotortype.MotorType, IFNULL(Stock.Jum, 0) AS Jum, IFNULL(Stock.Hrg, 0) AS Hrg 
      FROM tdmotortype 
        LEFT OUTER JOIN 
        (SELECT tdmotortype.MotorType, COUNT(tmotor.SKNo) AS Jum, SUM(tmotor.FBHarga) AS Hrg 
         FROM tdmotortype 
         INNER JOIN tmotor ON tdmotortype.MotorType = tmotor.MotorType 
         INNER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo 
         INNER JOIN ttsk ON ttrk.SKNo = ttsk.SKNo 
        WHERE ttSK.SKTgl BETWEEN CONCAT(xTgl1, " 00:00:00") AND CONCAT(xTgl2, " 23:59:59") AND MotorAutoN = 0 
         GROUP BY tdmotortype.MotorType) Stock ON tdmotortype.MotorType = Stock.MotorType 
      ORDER BY tdmotortype.MotorType;
   CREATE INDEX MotorType ON Stock(MotorType);
	UPDATE StockHarian INNER JOIN Stock ON StockHarian.MotorType = Stock.MotorType SET SKJum = SKJum+Jum, SKHrg = SKHrg +Hrg;	
	
	
	UPDATE StockHarian SET SARJum = SALJum + SSJum + PDJum + RKJum - SDJum - SKJum;
	UPDATE StockHarian SET SARHrg = SALHrg + SSHrg + PDHrg + RKHrg - SDHrg - SKHrg;
	
	DELETE FROM StockHarian WHERE SINJum = 0 AND SSJum = 0 AND PDJum = 0 AND RKJum = 0 AND SDJum = 0 AND SKJum = 0;

END$$

DELIMITER ;


