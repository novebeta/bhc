DELIMITER $$
DROP PROCEDURE IF EXISTS `rKeuanganKasMasuk`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rKeuanganKasMasuk`(IN xTgl1 DATE,IN xTgl2 DATE,IN xJenis VARCHAR(30),IN xLokasi VARCHAR(30),IN xJurnal NUMERIC,IN xSort VARCHAR(20),IN xOrder VARCHAR(10))
BEGIN
/*
           Cara Akses SP :
           CALL rKeuanganKasMasuk(xTgl1,xTgl2,xJenis,xLokasi,xJurnal,xSort,xOrder);
           xJurnal = 0 -> Indeterminate
           xJurnal = 1 -> Checked
           xJurnal = 2 -> Unchecked
           CALL rKeuanganKasMasuk('2014-05-02','2014-05-02','Umum','Muara Enim',0,'KMTgl','ASC');
*/
           SET xJenis = CONCAT(xJenis,'%');
           SET xLokasi = CONCAT(xLokasi,'%');
           SET @MyQuery = CONCAT("SELECT KMNo, KMTgl, KMMemo, KMNominal, KMJenis, KMPerson, KMLink, ttKM.UserID , NoGL AS GLLINK  FROM ttkm WHERE (ttKM.KMJenis LIKE '",xJenis,"') AND LokasiKode LIKE '",xLokasi,"' AND ( KMTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"') ");
           IF xJurnal = 1 THEN
              SET @MyQuery = CONCAT(@MyQuery,"AND NoGL <> '--' ");
           ELSE 
              IF xJurnal = 2 THEN
                 SET @MyQuery = CONCAT(@MyQuery,"AND NoGL = '--' ");
              END IF;
           END IF;
           SET @MyQuery = CONCAT(@MyQuery," ORDER BY ",xSort," ",xOrder);
           PREPARE STMT FROM @MyQuery;
           EXECUTE Stmt;
	END$$

DELIMITER ;

