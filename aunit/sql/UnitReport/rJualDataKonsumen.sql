DELIMITER $$
DROP PROCEDURE IF EXISTS `rJualDataKonsumen`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rJualDataKonsumen`(IN xTipe VARCHAR(25),IN xStatTgl VARCHAR(25),IN xTgl1 DATE,IN xTgl2 DATE,IN xRO INT,IN xTgl3 DATE,IN xTeamSales VARCHAR(30),IN xSales VARCHAR(20),IN xJenis VARCHAR(10),IN xLeasing VARCHAR(10),IN xLunas VARCHAR(10),IN xTypeMTR VARCHAR(20),IN xWarna VARCHAR(20),IN xNoSK VARCHAR(10),IN xSort VARCHAR(25),IN xOrder VARCHAR(10),IN xKab VARCHAR(30),IN xInden VARCHAR(10))
BEGIN
/*
       xTipe    : "ttdk1" -> Data Konsumen dan Scheme & SCP
                  "ttdk2" -> RO (Repeat Order)
                  "ttdk3" -> BBNStatus
                  "ttdkStatus1" -> DK Status
                  "ttdkStatus2" -> DK Status PL
                  "ttdkStatus3" -> PiutangViaKasBank
                  "ttdkStatus4" -> PiutangViaKasBankPL
                  "ttdkStatus5" -> PiutangViaKasir
                  "ttdkStatus6" -> PiutangViaKasirPL
                  "ttdkStatus7" -> PiutangTagih
                  "bbnpro" -> BBNProgresif
                  
                  
       xStatTgl : "ttDK.DKTgl"
       
       xSort    : "ttdk.DKNo"
                  "tdcustomer.CusNama"
                  "ttdk.CusKode"
                  "ttdk.SalesKode"
                  "ttdk.LeaseKode"
                  "ttdk.DKMemo"
                  "ttdk.DKJenis"
                  "ttdk.DKLunas"
                  "ttdk.INNo"
                  "ttdk.DKTglTagih"
                  "ttdk.DKPOTgl"
                  "ttdk.DKPONo"
                  "KMTgl"
                  "ttdk.ProgramNama"
                  "ttdk.UserID"
                  "tmotor.SKNo"
                  "ttdk.DKTgl"
                  "ttdk.DKHarga"
                  "ttdk.DKHPP"
                  "ttdk.DKDPTotal"
                  "ttdk.DKDPLLeasing"
                  "ttdk.DKDPTerima"
                  "ttdk.DKNetto"
                  "ttdk.ProgramSubsidi"
                  "ttdk.PrgSubsSupplier"
                  "ttdk.PrgSubsDealer"
                  "ttdk.PrgSubsFincoy"
                  "ttdk.PotonganHarga"
                  "ttdk.ReturHarga"
                  "ttdk.PotonganKhusus"
                  "ttdk.InsentifSales"
                  "ttdk.BBN"

       xNoSK    : "SK"
                  "--"
                  ""
       xInden   : "IN"
                  "--"
                  ""
       xLunas   : "Sudah"
                  "Belum"
                  ""
       xJenis   : "Tunai"
                  "Kredit"
                  ""
       Cara Akses :
       *** Data Konsumen ***
       CALL rJualDataKonsumen('ttdk1','ttDK.DKTgl','2001-01-01','2019-12-31',0,'2020-02-20','','','','','','','','','ttdk.DKNo','ASC','','');
       
       *** Repeat Order ***
       CALL rJualDataKonsumen('ttdk2','ttDK.DKTgl','2001-01-01','2019-12-31',0,'2020-02-20','','','','','','','','','ttdk.DKNo','ASC','','');
       
       *** BBN Status ***
       CALL rJualDataKonsumen('ttdk3','ttDK.DKTgl','2001-01-01','2019-12-31',0,'2020-02-20','','','','','Belum','','','','ttdk.DKNo','ASC','','Tidak');
       
       *** DK Status ***
       CALL rJualDataKonsumen('ttdkStatus1','ttDK.DKTgl','2001-01-01','2019-12-31',0,'2020-02-20','','','','','','','','SK','ttdk.DKNo','ASC','','');
       
       *** DK Status PL***
       CALL rJualDataKonsumen('ttdkStatus2','ttDK.DKTgl','2001-01-01','2019-12-31',0,'2020-02-20','','','','','','','','SK','ttdk.DKNo','ASC','','');
       
       *** PiutangViaKasBank***
       CALL rJualDataKonsumen('ttdkStatus3','ttDK.DKTgl','2001-01-01','2019-12-31',0,'2020-02-20','','','','','','','','SK','ttdk.DKNo','ASC','','');

       *** PiutangViaKasBank PL***
       CALL rJualDataKonsumen('ttdkStatus4','ttDK.DKTgl','2001-01-01','2019-12-31',0,'2020-02-20','','','','','','','','SK','ttdk.DKNo','ASC','','');
       
       *** PiutangViaKasir***
       CALL rJualDataKonsumen('ttdkStatus5','ttDK.DKTgl','2001-01-01','2019-12-31',0,'2020-02-20','','','','','','','','SK','ttdk.DKNo','ASC','','');
       
       *** PiutangViaKasir PL***
       CALL rJualDataKonsumen('ttdkStatus6','ttDK.DKTgl','2001-01-01','2019-12-31',0,'2020-02-20','','','','','','','','SK','ttdk.DKNo','ASC','','');
       
       *** PiutangTagih ***
       CALL rJualDataKonsumen('ttdkStatus7','ttDK.DKTgl','2001-01-01','2019-12-31',0,'2020-02-20','','','','','','','','SK','ttdk.DKNo','ASC','','');
       
       *** BBN Progresif ***
       CALL rJualDataKonsumen('bbnpro','ttDK.DKTgl','2001-01-01','2019-12-31',0,'2020-02-20','','','','','','','','','ttdk.DKNo','ASC','','');

*/
       SET xTeamSales = CONCAT(xTeamSales,"%");
       SET xSales = CONCAT(xSales,"%");
       SET xJenis = CONCAT(xJenis,"%");
       SET xLeasing = CONCAT(xLeasing,"%");
       SET xLunas = CONCAT(xLunas,"%");
       SET xTypeMtr = CONCAT(xTypeMtr,"%");
       SET xWarna = CONCAT(xWarna,"%");
       SET xNoSK = CONCAT(xNoSK,"%");
       SET xKab = CONCAT(xKab,"%");
       SET xInden = CONCAT(xInden,"%");
       
       /* DATA KONSUMEN */
       IF xTipe = "ttdk1" THEN 
          SET @MyQuery = "SELECT ttdk.DKNo, ttdk.DKTgl, ttdk.SalesKode, ttdk.CusKode, ttdk.LeaseKode, ttdk.DKJenis, ttdk.DKMemo, ttdk.DKLunas, ttdk.UserID, ttdk.DKHPP, ttdk.BBNPlus, ttdk.InsentifSales, ttdk.Jaket , 
                          ttdk.DKScheme, ttdk.DKSCP, ttdk.DKPengajuanBBN, ttdk.DKScheme2, (ttdk.DKDPTotal - ttdk.PotonganHarga) AS DPNet, (ttdk.ProgramSubsidi + ttdk.DKDPTotal) AS DPPO, 
                          (ttdk.PrgSubsSupplier + ttdk.JAPrgSubsSupplier) AS PrgSubsSupplier, 
                          (ttdk.PrgSubsDealer + ttdk.JAPrgSubsDealer) AS PrgSubsDealer, 
                          (ttdk.PrgSubsFincoy + ttdk.JAPrgSubsFincoy) AS PrgSubsFincoy, 
                          (ttdk.ProgramSubsidi + ttdk.JAPrgSubsSupplier + ttdk.JAPrgSubsDealer + ttdk.JAPrgSubsFincoy) AS ProgramSubsidi, 
                          (ttdk.DKDPLLeasing+JADKDPLLeasing) AS DKDPLLeasing, 
                          (ttdk.DKDPTerima + ttdk.JADKDPTerima) AS DKDPTerima, 
                          (ttdk.DKDPTotal + ttdk.JADKDPTerima + JADKDPLLeasing) AS DKDPTotal, 
                          (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) AS DKNetto, 
                          (ttdk.DKHarga+ttdk.JADKHarga) AS DKHarga, 
                          (ttdk.BBN+ttdk.JABBN) AS BBN, 
                          (ttdk.ReturHarga+ttdk.JAReturHarga) AS ReturHarga, 
                          (ttdk.PotonganHarga+ttdk.JAPotonganHarga) AS PotonganHarga, 
                          (ttdk.InsentifSales+ttdk.JAInsentifSales) AS InsentifSales, 
                          (ttdk.PotonganKhusus+ttdk.JAPotonganKhusus) AS PotonganKhusus, 
                          ttdk.NamaHadiah, IFNULL(tmotor.MotorNoMesin, '--') AS MotorNoMesin, 
                          IFNULL(tmotor.MotorAutoN, 0) AS MotorAutoN, IFNULL(tmotor.MotorType, '--') AS MotorType, IFNULL(tmotor.MotorWarna, '--') AS MotorWarna, 
                          IFNULL(tmotor.MotorNoRangka, '--') AS MotorNoRangka, tmotor.MotorTahun, IF(TRIM(tdcustomer.CusNama)<>TRIM(tdcustomer.CusPembeliNama) AND tdcustomer.CusPembeliNama <>'--' AND tdcustomer.CusPembeliNama <>'',CONCAT_WS(' qq ',tdcustomer.CusNama,tdcustomer.CusPembeliNama),tdcustomer.CusNama) AS CusNama, tdcustomer.CusAlamat, 
                          LPAD(tdcustomer.CusRT,3,'0') AS CusRT, LPAD(tdcustomer.CusRW,3,'0')AS CusRW, tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, IFNULL(tdcustomer.CusKabupaten, '--') AS CusKabupaten, 
                          tdcustomer.CusTelepon, IFNULL(tdsales.SalesNama, '--') AS SalesNama, IFNULL(tdleasing.LeaseNama, '--') AS LeaseNama, ttdk.INNo, ttdk.PrgSubsFincoy, tmotor.SKNo ,
                          ttdk.DKTenor, ttdk.DKAngsuran, ttdk.DKTOP 
                          FROM ttdk 
                          LEFT OUTER JOIN tdleasing ON ttdk.LeaseKode = tdleasing.LeaseKode 
                          LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode 
                          LEFT OUTER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
                          LEFT OUTER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                          WHERE  ";
                          
          SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttdk.TeamKode LIKE '",xTeamSales,"') AND (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (ttdk.LeaseKode LIKE '",xLeasing,"') AND (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND (tmotor.MotorWarna LIKE '",xWarna,"') AND 
                         (ttdk.DKPengajuanBBN LIKE '",xLunas,"') AND (ttdk.DKJenis LIKE '",xJenis,"') AND 
                         (ttdk.INNo LIKE '",xInden,"') AND 
                         (IFNULL(tmotor.SKNo, '--') LIKE '",xNoSK,"') ORDER BY ",xSort," ",xOrder); 
       END IF;

       /* REPEAT ORDER (RO) */
       IF xTipe = "ttdk2" THEN 
          SET @MyQuery = "SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusKode, tdcustomer.CusKTP, tdcustomer.CusNama, CusAlamat, tdcustomer.CusPembeliKTP, tdcustomer.CusPembeliNama, CusPembeliAlamat, ttdk.ROCount, tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, tdcustomer.CusKabupaten 
                          FROM ttdk 
                          INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
                          INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
                          WHERE ";
                          
          SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttdk.TeamKode LIKE '",xTeamSales,"') AND (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (ttdk.LeaseKode LIKE '",xLeasing,"') AND (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND (tmotor.MotorWarna LIKE '",xWarna,"') AND 
                         (ttdk.DKPengajuanBBN LIKE '",xLunas,"') AND (ttdk.DKJenis LIKE '",xJenis,"') AND 
                         (ttdk.INNo LIKE '",xInden,"') AND 
                         (IFNULL(tmotor.SKNo, '--') LIKE '",xNoSK,"') AND (ROCount >= ",xRO,") ORDER BY CusKTP, CusPembeliKTP, DKNo,",xSort," ",xOrder); 
       END IF;

       /* BBN STATUS */
       IF xTipe = "ttdk3" THEN 
          SET @MyQuery = "SELECT ttdk.DKNo, DKTgl, CusNama, LeaseKode, TeamKode, DKPengajuanBBN, ttbkit.BKNo AS INNo, STNKTglMasuk AS SKTgl1, STNKTglBayar , MotorNoMesin, DKJenis, tmotor.SKNo
                          FROM ttdk
                          INNER JOIN tdcustomer ON tdcustomer.CusKode = ttdk.CusKode
                          INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
                          LEFT OUTER JOIN ttbkit ON ttdk.DKNo = ttbkit.FBNo
                          WHERE ";

          SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttdk.TeamKode LIKE '",xTeamSales,"') AND (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (ttdk.LeaseKode LIKE '",xLeasing,"') AND (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND (tmotor.MotorWarna LIKE '",xWarna,"') AND 
                         (ttdk.DKPengajuanBBN LIKE '",xLunas,"') AND (ttdk.DKJenis LIKE '",xJenis,"') ");
          
          IF xNoSK = "Sudah" THEN
             SET @MyQuery = CONCAT(@MyQuery," AND (ttbkit.FBNo Is NULL = False  AND LEFT(ttbkit.bKno,2) = 'BP')");
          ELSE
             IF xNoSK = "Belum" THEN
                SET @MyQuery = CONCAT(@MyQuery," AND ttbkit.FBNo Is NULL = True ");
             END IF;
          END IF;
          
          IF xInden = "Ada" THEN
             SET @MyQuery = CONCAT(@MyQuery,"AND (ttbkit.FBNo Is NULL = False  AND LEFT(ttbkit.bKno,2) = 'BK')");
          ELSE
             IF xInden = "Tidak" THEN
                SET @MyQuery = CONCAT(@MyQuery," AND ttbkit.FBNo Is NULL = True ");
             END IF;
          END IF;
          
          SET @MyQuery = CONCAT(@MyQuery," ORDER BY ",xSort," ",xOrder);
       END IF;

       /* DK STATUS */
       IF xTipe = "ttdkStatus1" THEN 
          SET @MyQuery = "SELECT ttdk.DKNo, ttdk.DKTgl,  tdmotortype.MotorType, tmotor.MotorNoMesin, 
                          IF(TRIM(tdcustomer.CusNama)<>TRIM(tdcustomer.CusPembeliNama) AND tdcustomer.CusPembeliNama <>'--' AND tdcustomer.CusPembeliNama <>'',CONCAT_WS(' qq ',tdcustomer.CusNama,tdcustomer.CusPembeliNama),tdcustomer.CusNama) AS CusNama,
                          (ttdk.DKDPTerima + ttdk.JADKDPTerima) AS UMD, 
                          IFNULL(KM_UMD.Bayar, 0) AS UMDTerbayar, 
                          ttdk.DKDPTerima + ttdk.JADKDPTerima - IFNULL(KM_UMD.Bayar, 0) AS UMDSisa, 
                          (ttdk.DKDPLLeasing + JADKDPLLeasing) AS UMPK, 
                          IFNULL(KM_UMPK.Bayar, 0) AS UMPKTerbayar, 
                          (ttdk.DKDPLLeasing + JADKDPLLeasing) - IFNULL(KM_UMPK.Bayar, 0) AS UMPKSisa, 
                          (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) AS SisaPiutang, 
                          IFNULL(KM_SisaPiutang.Bayar, 0) AS SisaPiutangTerbayar, 
                          (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(KM_SisaPiutang.Bayar, 0) AS SisaPiutangSisa, 
                          ttdk.DKDPInden AS UMInden, 
                          IFNULL(KM_UMInden.Bayar, 0) AS UMIndenTerbayar, 
                          ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) AS UMIndenSisa, 
                          IF (ttdk.DKDPTerima + ttdk.JADKDPTerima - IFNULL(KM_UMD.Bayar, 0) <= 0 AND ttdk.DKDPLLeasing + JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) <= 0 AND (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(KM_SisaPiutang.Bayar, 0) <= 0 AND ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) <= 0, 'Lunas', 'Belum') AS STATUS,
                          tdcustomer.CusPembeliNama
                          FROM ttdk
                          INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
                          INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
                          INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType
                          LEFT OUTER JOIN
                          (SELECT vtkm.KMLINK, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          WHERE vtkm.KMJenis = 'Kredit' OR vtkm.KMJenis = 'Tunai'
                          GROUP BY vtkm.KMLINK, vtkm.KMJenis) KM_UMD ON ttdk.DKNo = KM_UMD.KMLINK
                          LEFT OUTER JOIN
                          (SELECT vtkm.KMLINK, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          WHERE vtkm.KMJenis = 'Piutang UM' 
                          GROUP BY vtkm.KMLINK, vtkm.KMJenis) KM_UMPK ON ttdk.DKNo = KM_UMPK.KMLINK
                          LEFT OUTER JOIN
                          (SELECT vtkm.KMLINK, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          WHERE vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing'
                          GROUP BY vtkm.KMLINK, vtkm.KMJenis) KM_SisaPiutang ON ttdk.DKNo = KM_SisaPiutang.KMLINK
                          LEFT OUTER JOIN
                          (SELECT ttin.DKNo, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          INNER JOIN ttin ON vtkm.KMLINK = ttin.INNo
                          WHERE vtkm.KMJenis = 'Inden' GROUP BY ttin.DKNo) KM_UMInden ON ttdk.DKNo = KM_UMInden.DKNo
                          WHERE ";
         
          SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttdk.TeamKode LIKE '",xTeamSales,"') AND (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (ttdk.LeaseKode LIKE '",xLeasing,"') AND (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND (tmotor.MotorWarna LIKE '",xWarna,"') AND 
                         (ttdk.DKJenis LIKE '",xJenis,"') AND (ttdk.INNo LIKE '",xInden,"') AND (IFNULL(tmotor.SKNo, '--') LIKE '",xNoSK,"')");
          
          IF xLunas = "Sudah" THEN
             SET @MyQuery = CONCAT(@MyQuery," AND IF (ttdk.DKDPTerima + ttdk.JADKDPTerima - IFNULL(KM_UMD.Bayar, 0) <= 0 AND ttdk.DKDPLLeasing + JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) <= 0 AND (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(KM_SisaPiutang.Bayar, 0) <= 0 AND ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) <= 0, 'Lunas', 'Belum') = 'Lunas' ");
          ELSE
             IF xLunas = "Belum" THEN
                SET @MyQuery = CONCAT(@MyQuery," AND IF (ttdk.DKDPTerima + ttdk.JADKDPTerima - IFNULL(KM_UMD.Bayar, 0) <= 0 AND ttdk.DKDPLLeasing + JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) <= 0 AND (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(KM_SisaPiutang.Bayar, 0) <= 0 AND ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) <= 0, 'Lunas', 'Belum') = 'Belum' ");
             END IF;
          END IF;
          
          SET @MyQuery = CONCAT(@MyQuery," ORDER BY ",xSort," ",xOrder);
          
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Kredit' OR vtkm.KMJenis = 'Tunai'", 
                 CONCAT("WHERE (vtkm.KMJenis = 'Kredit' OR vtkm.KMJenis = 'Tunai') AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
                 
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Piutang UM'", 
                 CONCAT("WHERE vtkm.KMJenis = 'Piutang UM' AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
                 
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Inden'", 
                 CONCAT("WHERE vtkm.KMJenis = 'Inden' AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
                 
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing'", 
                 CONCAT("WHERE (vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing') AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
       END IF;

       /* DK STATUS PL */
       IF xTipe = "ttdkStatus2" THEN 
          SET @MyQuery = "SELECT ttdk.DKNo, ttdk.DKTgl,  tdmotortype.MotorType, tmotor.MotorNoMesin, 
                          IF(TRIM(tdcustomer.CusNama)<>TRIM(tdcustomer.CusPembeliNama) AND tdcustomer.CusPembeliNama <>'--' AND tdcustomer.CusPembeliNama <>'',CONCAT_WS(' qq ',tdcustomer.CusNama,tdcustomer.CusPembeliNama),tdcustomer.CusNama) AS CusNama,
                          ttdk.DKDPTerima + ttdk.JADKDPTerima - ttdk.ReturHarga - ttdk.InsentifSales - ttdk.PotonganHarga AS UMD, 
                          IFNULL(KM_UMD.Bayar, 0) AS UMDTerbayar, 
                          ttdk.DKDPTerima + ttdk.JADKDPTerima - ttdk.ReturHarga - ttdk.InsentifSales - ttdk.PotonganHarga - IFNULL(KM_UMD.Bayar, 0) AS UMDSisa, 
                          ttdk.DKDPLLeasing +JADKDPLLeasing AS UMPK, 
                          IFNULL(KM_UMPK.Bayar, 0) AS UMPKTerbayar, 
                          ttdk.DKDPLLeasing +JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) AS UMPKSisa, 
                          (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) AS SisaPiutang, IFNULL(KM_SisaPiutang.Bayar, 0) AS SisaPiutangTerbayar, 
                          ttdk.DKNetto - IFNULL(KM_SisaPiutang.Bayar, 0) AS SisaPiutangSisa, 
                          ttdk.DKDPInden AS UMInden, 
                          IFNULL(KM_UMInden.Bayar, 0) AS UMIndenTerbayar, 
                          ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) AS UMIndenSisa, 
                          IF(ttdk.DKDPTerima + ttdk.JADKDPTerima - ttdk.ReturHarga - ttdk.InsentifSales - ttdk.PotonganHarga - IFNULL(KM_UMD.Bayar, 0) <= 0 AND ttdk.DKDPLLeasing +JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) <= 0 AND (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(KM_SisaPiutang.Bayar, 0) <= 0 AND ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) <= 0, 'Lunas', 'Belum') AS STATUS,
                          tdcustomer.CusPembeliNama
                          FROM ttdk
                          INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
                          INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
                          INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType
                          LEFT OUTER JOIN
                          (SELECT vtkm.KMLINK, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          WHERE vtkm.KMJenis = 'Kredit' OR vtkm.KMJenis = 'Tunai'
                          GROUP BY vtkm.KMLINK, vtkm.KMJenis) KM_UMD ON ttdk.DKNo = KM_UMD.KMLINK
                          LEFT OUTER JOIN
                          (SELECT vtkm.KMLINK, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          WHERE vtkm.KMJenis = 'Piutang UM' 
                          GROUP BY vtkm.KMLINK, vtkm.KMJenis) KM_UMPK ON ttdk.DKNo = KM_UMPK.KMLINK
                          LEFT OUTER JOIN
                          (SELECT vtkm.KMLINK, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          WHERE vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing'
                          GROUP BY vtkm.KMLINK, vtkm.KMJenis) KM_SisaPiutang ON ttdk.DKNo = KM_SisaPiutang.KMLINK
                          LEFT OUTER JOIN
                          (SELECT ttin.DKNo, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          INNER JOIN ttin ON vtkm.KMLINK = ttin.INNo
                          WHERE vtkm.KMJenis = 'Inden' GROUP BY ttin.DKNo) KM_UMInden ON ttdk.DKNo = KM_UMInden.DKNo
                          WHERE  ";
                  
          SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttdk.TeamKode LIKE '",xTeamSales,"') AND (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (ttdk.LeaseKode LIKE '",xLeasing,"') AND (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND (tmotor.MotorWarna LIKE '",xWarna,"') AND 
                         (ttdk.DKJenis LIKE '",xJenis,"') AND (ttdk.INNo LIKE '",xInden,"') AND (IFNULL(tmotor.SKNo, '--') LIKE '",xNoSK,"')");
          
          IF xLunas = "Sudah" THEN
             SET @MyQuery = CONCAT(@MyQuery," AND IF(ttdk.DKDPTerima + ttdk.JADKDPTerima - ttdk.ReturHarga - ttdk.InsentifSales - ttdk.PotonganHarga - IFNULL(KM_UMD.Bayar, 0) <= 0 AND ttdk.DKDPLLeasing +JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) <= 0 AND (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(KM_SisaPiutang.Bayar, 0) <= 0 AND ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) <= 0, 'Lunas', 'Belum') = 'Lunas' ");
          ELSE
             IF xLunas = "Belum" THEN
                SET @MyQuery = CONCAT(@MyQuery," AND IF(ttdk.DKDPTerima + ttdk.JADKDPTerima - ttdk.ReturHarga - ttdk.InsentifSales - ttdk.PotonganHarga - IFNULL(KM_UMD.Bayar, 0) <= 0 AND ttdk.DKDPLLeasing +JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) <= 0 AND (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(KM_SisaPiutang.Bayar, 0) <= 0 AND ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) <= 0, 'Lunas', 'Belum') = 'Belum' ");
             END IF;
          END IF;
          
          SET @MyQuery = CONCAT(@MyQuery," ORDER BY ",xSort," ",xOrder);
          
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Kredit' OR vtkm.KMJenis = 'Tunai'", 
                 CONCAT("WHERE (vtkm.KMJenis = 'Kredit' OR vtkm.KMJenis = 'Tunai') AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
                 
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Piutang UM'", 
                 CONCAT("WHERE vtkm.KMJenis = 'Piutang UM' AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
                 
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Inden'", 
                 CONCAT("WHERE vtkm.KMJenis = 'Inden' AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
                 
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing'", 
                 CONCAT("WHERE (vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing') AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
       END IF;

       /* PiutangViaKasBank */
       IF xTipe = "ttdkStatus3" THEN 
          SET @MyQuery = "SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tdmotortype.MotorType, tmotor.MotorNoMesin, 
                          ttdk.DKDPTerima + ttdk.JADKDPTerima AS UMD, 
                          IFNULL(KM_UMD.Bayar, 0) AS UMDTerbayar, 
                          ttdk.DKDPTerima + ttdk.JADKDPTerima - IFNULL(KM_UMD.Bayar, 0) AS UMDSisa, 
                          ttdk.DKDPLLeasing + JADKDPLLeasing AS UMPK, 
                          IFNULL(KM_UMPK.Bayar, 0) AS UMPKTerbayar, 
                          ttdk.DKDPLLeasing + JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) AS UMPKSisa, 
                          (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) AS SisaPiutang, IFNULL(KM_SisaPiutang.Bayar, 0) AS SisaPiutangTerbayar, 
                          (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(KM_SisaPiutang.Bayar, 0) AS SisaPiutangSisa, 
                          ttdk.DKDPInden AS UMInden, 
                          IFNULL(KM_UMInden.Bayar, 0) AS UMIndenTerbayar, 
                          ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) AS UMIndenSisa, 
                          IF(ttdk.DKDPTerima + ttdk.JADKDPTerima - IFNULL(KM_UMD.Bayar, 0) <= 0 AND ttdk.DKDPLLeasing + JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) <= 0 AND (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(KM_SisaPiutang.Bayar, 0) <= 0 AND ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) <= 0, 'Lunas', 'Belum') AS STATUS, 
                          ttdk.LeaseKode, tdsales.SalesNama, tdcustomer.CusPembeliNama
                          FROM ttdk
                          INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
                          INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
                          INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType
                          INNER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode
                          LEFT OUTER JOIN
                          (SELECT vtkm.KMLINK, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          WHERE vtkm.KMJenis = 'Kredit' OR vtkm.KMJenis = 'Tunai'
                          GROUP BY vtkm.KMLINK, vtkm.KMJenis) KM_UMD ON ttdk.DKNo = KM_UMD.KMLINK
                          LEFT OUTER JOIN
                          (SELECT vtkm.KMLINK, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          WHERE vtkm.KMJenis = 'Piutang UM' 
                          GROUP BY vtkm.KMLINK, vtkm.KMJenis) KM_UMPK ON ttdk.DKNo = KM_UMPK.KMLINK
                          LEFT OUTER JOIN
                          (SELECT vtkm.KMLINK, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          WHERE vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing'
                          GROUP BY vtkm.KMLINK, vtkm.KMJenis) KM_SisaPiutang ON ttdk.DKNo = KM_SisaPiutang.KMLINK
                          LEFT OUTER JOIN
                          (SELECT ttin.DKNo, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          INNER JOIN ttin ON vtkm.KMLINK = ttin.INNo
                          WHERE vtkm.KMJenis = 'Inden' GROUP BY ttin.DKNo) KM_UMInden ON ttdk.DKNo = KM_UMInden.DKNo
                          WHERE ";
                  
          SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttdk.TeamKode LIKE '",xTeamSales,"') AND (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (ttdk.LeaseKode LIKE '",xLeasing,"') AND (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND (tmotor.MotorWarna LIKE '",xWarna,"') AND 
                         (ttdk.DKJenis LIKE '",xJenis,"') AND (ttdk.INNo LIKE '",xInden,"') AND (IFNULL(tmotor.SKNo, '--') LIKE '",xNoSK,"')");
          
          IF xLunas = "Sudah" THEN
             SET @MyQuery = CONCAT(@MyQuery," AND IF(ttdk.DKDPTerima + ttdk.JADKDPTerima - IFNULL(KM_UMD.Bayar, 0) <= 0 AND ttdk.DKDPLLeasing + JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) <= 0 AND (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(KM_SisaPiutang.Bayar, 0) <= 0 AND ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) <= 0, 'Lunas', 'Belum') = 'Lunas' ");
          ELSE
             IF xLunas = "Belum" THEN
                SET @MyQuery = CONCAT(@MyQuery," AND IF(ttdk.DKDPTerima + ttdk.JADKDPTerima - IFNULL(KM_UMD.Bayar, 0) <= 0 AND ttdk.DKDPLLeasing + JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) <= 0 AND (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(KM_SisaPiutang.Bayar, 0) <= 0 AND ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) <= 0, 'Lunas', 'Belum') = 'Belum' ");
             END IF;
          END IF;
          
          SET @MyQuery = CONCAT(@MyQuery," ORDER BY ",xSort," ",xOrder);
          
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Kredit' OR vtkm.KMJenis = 'Tunai'", 
                 CONCAT("WHERE (vtkm.KMJenis = 'Kredit' OR vtkm.KMJenis = 'Tunai') AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
                 
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Piutang UM'", 
                 CONCAT("WHERE vtkm.KMJenis = 'Piutang UM' AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
                 
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Inden'", 
                 CONCAT("WHERE vtkm.KMJenis = 'Inden' AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
                 
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing'", 
                 CONCAT("WHERE (vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing') AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
       END IF;
       
       /* PiutangViaKasBank PL*/
       IF xTipe = "ttdkStatus4" THEN 
          SET @MyQuery = "SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tdmotortype.MotorType, tmotor.MotorNoMesin, 
                          ttdk.DKDPTerima + ttdk.JADKDPTerima - ttdk.ReturHarga - ttdk.InsentifSales - ttdk.PotonganHarga AS UMD, 
                          IFNULL(KM_UMD.Bayar, 0) AS UMDTerbayar, 
                          ttdk.DKDPTerima + ttdk.JADKDPTerima - ttdk.ReturHarga - ttdk.InsentifSales - ttdk.PotonganHarga - IFNULL(KM_UMD.Bayar, 0) AS UMDSisa, 
                          ttdk.DKDPLLeasing +JADKDPLLeasing AS UMPK, IFNULL(KM_UMPK.Bayar, 0) AS UMPKTerbayar, 
                          ttdk.DKDPLLeasing +JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) AS UMPKSisa, 
                          (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) AS SisaPiutang, 
                          IFNULL(KM_SisaPiutang.Bayar, 0) AS SisaPiutangTerbayar, 
                          (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(KM_SisaPiutang.Bayar, 0) AS SisaPiutangSisa, 
                          ttdk.DKDPInden AS UMInden, 
                          IFNULL(KM_UMInden.Bayar, 0) AS UMIndenTerbayar, 
                          ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) AS UMIndenSisa, 
                          IF(ttdk.DKDPTerima + ttdk.JADKDPTerima - ttdk.ReturHarga - ttdk.InsentifSales - ttdk.PotonganHarga - IFNULL(KM_UMD.Bayar, 0) <= 0 AND ttdk.DKDPLLeasing +JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) <= 0 AND (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(KM_SisaPiutang.Bayar, 0) <= 0 AND ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) <= 0, 'Lunas', 'Belum') AS STATUS,
                          ttdk.LeaseKode, tdsales.SalesNama, tdcustomer.CusPembeliNama
                          FROM ttdk
                          INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
                          INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
                          INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType
                          INNER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode
                          LEFT OUTER JOIN
                          (SELECT vtkm.KMLINK, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          WHERE vtkm.KMJenis = 'Kredit' OR vtkm.KMJenis = 'Tunai'
                          GROUP BY vtkm.KMLINK, vtkm.KMJenis) KM_UMD ON ttdk.DKNo = KM_UMD.KMLINK
                          LEFT OUTER JOIN
                          (SELECT vtkm.KMLINK, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          WHERE vtkm.KMJenis = 'Piutang UM' 
                          GROUP BY vtkm.KMLINK, vtkm.KMJenis) KM_UMPK ON ttdk.DKNo = KM_UMPK.KMLINK
                          LEFT OUTER JOIN
                          (SELECT vtkm.KMLINK, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          WHERE vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing'
                          GROUP BY vtkm.KMLINK, vtkm.KMJenis) KM_SisaPiutang ON ttdk.DKNo = KM_SisaPiutang.KMLINK
                          LEFT OUTER JOIN
                          (SELECT ttin.DKNo, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          INNER JOIN ttin ON vtkm.KMLINK = ttin.INNo
                          WHERE vtkm.KMJenis = 'Inden' GROUP BY ttin.DKNo) KM_UMInden ON ttdk.DKNo = KM_UMInden.DKNo
                          WHERE ";
                  
          SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttdk.TeamKode LIKE '",xTeamSales,"') AND (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (ttdk.LeaseKode LIKE '",xLeasing,"') AND (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND (tmotor.MotorWarna LIKE '",xWarna,"') AND 
                         (ttdk.DKJenis LIKE '",xJenis,"') AND (ttdk.INNo LIKE '",xInden,"') AND (IFNULL(tmotor.SKNo, '--') LIKE '",xNoSK,"')");
          
          IF xLunas = "Sudah" THEN
             SET @MyQuery = CONCAT(@MyQuery," AND IF(ttdk.DKDPTerima + ttdk.JADKDPTerima - ttdk.ReturHarga - ttdk.InsentifSales - ttdk.PotonganHarga - IFNULL(KM_UMD.Bayar, 0) <= 0 AND ttdk.DKDPLLeasing +JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) <= 0 AND (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(KM_SisaPiutang.Bayar, 0) <= 0 AND ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) <= 0, 'Lunas', 'Belum') = 'Lunas' ");
          ELSE
             IF xLunas = "Belum" THEN
                SET @MyQuery = CONCAT(@MyQuery," AND IF(ttdk.DKDPTerima + ttdk.JADKDPTerima - ttdk.ReturHarga - ttdk.InsentifSales - ttdk.PotonganHarga - IFNULL(KM_UMD.Bayar, 0) <= 0 AND ttdk.DKDPLLeasing +JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) <= 0 AND (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(KM_SisaPiutang.Bayar, 0) <= 0 AND ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) <= 0, 'Lunas', 'Belum') = 'Belum' ");
             END IF;
          END IF;
          
          SET @MyQuery = CONCAT(@MyQuery," ORDER BY ",xSort," ",xOrder);
          
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Kredit' OR vtkm.KMJenis = 'Tunai'", 
                 CONCAT("WHERE (vtkm.KMJenis = 'Kredit' OR vtkm.KMJenis = 'Tunai') AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
                 
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Piutang UM'", 
                 CONCAT("WHERE vtkm.KMJenis = 'Piutang UM' AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
                 
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Inden'", 
                 CONCAT("WHERE vtkm.KMJenis = 'Inden' AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
                 
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing'", 
                 CONCAT("WHERE (vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing') AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
       END IF;

       /* PiutangViaKasir */
       IF xTipe = "ttdkStatus5" THEN 
          SET @MyQuery = "SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tdmotortype.MotorType, tmotor.MotorNoMesin, 
                          ttdk.DKDPTerima + ttdk.JADKDPTerima AS UMD, 
                          IFNULL(KM_UMD.Bayar, 0) AS UMDTerbayar, 
                          ttdk.DKDPTerima + ttdk.JADKDPTerima - IFNULL(KM_UMD.Bayar, 0) AS UMDSisa, 
                          ttdk.DKDPLLeasing +JADKDPLLeasing AS UMPK, 
                          IFNULL(KM_UMPK.Bayar, 0) AS UMPKTerbayar, 
                          ttdk.DKDPLLeasing +JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) AS UMPKSisa, 
                          (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) AS SisaPiutang, IFNULL(KM_SisaPiutang.Bayar, 0) AS SisaPiutangTerbayar, 
                          (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(KM_SisaPiutang.Bayar, 0) AS SisaPiutangSisa, 
                          ttdk.DKDPInden AS UMInden, 
                          IFNULL(KM_UMInden.Bayar, 0) AS UMIndenTerbayar, 
                          ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) AS UMIndenSisa, 
                          IF(ttdk.DKDPTerima + ttdk.JADKDPTerima - IFNULL(KM_UMD.Bayar, 0) <= 0 AND ttdk.DKDPLLeasing + JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) <= 0 AND ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) <= 0, 'Lunas', 'Belum') AS STATUS, 
                          ttdk.LeaseKode, tdsales.SalesNama, tdcustomer.CusPembeliNama, ttdk.DKMediator, ttdk.TeamKode
                          FROM ttdk
                          INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
                          INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
                          INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType
                          INNER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode
                          LEFT OUTER JOIN
                          (SELECT vtkm.KMLINK, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          WHERE vtkm.KMJenis = 'Kredit' OR vtkm.KMJenis = 'Tunai'
                          GROUP BY vtkm.KMLINK, vtkm.KMJenis) KM_UMD ON ttdk.DKNo = KM_UMD.KMLINK
                          LEFT OUTER JOIN
                          (SELECT vtkm.KMLINK, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          WHERE vtkm.KMJenis = 'Piutang UM' 
                          GROUP BY vtkm.KMLINK, vtkm.KMJenis) KM_UMPK ON ttdk.DKNo = KM_UMPK.KMLINK
                          LEFT OUTER JOIN
                          (SELECT vtkm.KMLINK, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          WHERE vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing'
                          GROUP BY vtkm.KMLINK, vtkm.KMJenis) KM_SisaPiutang ON ttdk.DKNo = KM_SisaPiutang.KMLINK
                          LEFT OUTER JOIN
                          (SELECT ttin.DKNo, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          INNER JOIN ttin ON vtkm.KMLINK = ttin.INNo
                          WHERE vtkm.KMJenis = 'Inden' GROUP BY ttin.DKNo) KM_UMInden ON ttdk.DKNo = KM_UMInden.DKNo
                          WHERE ";
                  
          SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttdk.TeamKode LIKE '",xTeamSales,"') AND (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (ttdk.LeaseKode LIKE '",xLeasing,"') AND (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND (tmotor.MotorWarna LIKE '",xWarna,"') AND 
                         (ttdk.DKJenis LIKE '",xJenis,"') AND (ttdk.INNo LIKE '",xInden,"') AND (IFNULL(tmotor.SKNo, '--') LIKE '",xNoSK,"')");
          
          IF xLunas = "Sudah" THEN
             SET @MyQuery = CONCAT(@MyQuery," AND IF(ttdk.DKDPTerima + ttdk.JADKDPTerima - IFNULL(KM_UMD.Bayar, 0) <= 0 AND ttdk.DKDPLLeasing + JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) <= 0 AND ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) <= 0, 'Lunas', 'Belum') = 'Lunas' ");
          ELSE
             IF xLunas = "Belum" THEN
                SET @MyQuery = CONCAT(@MyQuery," AND IF(ttdk.DKDPTerima + ttdk.JADKDPTerima - IFNULL(KM_UMD.Bayar, 0) <= 0 AND ttdk.DKDPLLeasing + JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) <= 0 AND ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) <= 0, 'Lunas', 'Belum') = 'Belum' ");
             END IF;
          END IF;
          
          SET @MyQuery = CONCAT(@MyQuery," ORDER BY ",xSort," ",xOrder);
          
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Kredit' OR vtkm.KMJenis = 'Tunai'", 
                 CONCAT("WHERE (vtkm.KMJenis = 'Kredit' OR vtkm.KMJenis = 'Tunai') AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
                 
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Piutang UM'", 
                 CONCAT("WHERE vtkm.KMJenis = 'Piutang UM' AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
                 
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Inden'", 
                 CONCAT("WHERE vtkm.KMJenis = 'Inden' AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
                 
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing'", 
                 CONCAT("WHERE (vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing') AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
       END IF;

       /* PiutangViaKasir PL */
       IF xTipe = "ttdkStatus6" THEN 
          SET @MyQuery = "SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tdmotortype.MotorType, tmotor.MotorNoMesin,
                          ttdk.DKDPTerima + ttdk.JADKDPTerima - ttdk.ReturHarga - ttdk.InsentifSales - ttdk.PotonganHarga AS UMD, 
                          IFNULL(KM_UMD.Bayar, 0) AS UMDTerbayar, 
                          ttdk.DKDPTerima + ttdk.JADKDPTerima - ttdk.ReturHarga - ttdk.InsentifSales - ttdk.PotonganHarga - IFNULL(KM_UMD.Bayar, 0) AS UMDSisa, 
                          ttdk.DKDPLLeasing + JADKDPLLeasing AS UMPK, 
                          IFNULL(KM_UMPK.Bayar, 0) AS UMPKTerbayar, 
                          ttdk.DKDPLLeasing + JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) AS UMPKSisa, 
                          (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) AS SisaPiutang, 
                          IFNULL(KM_SisaPiutang.Bayar, 0) AS SisaPiutangTerbayar, 
                          (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(KM_SisaPiutang.Bayar, 0) AS SisaPiutangSisa, 
                          ttdk.DKDPInden AS UMInden, 
                          IFNULL(KM_UMInden.Bayar, 0) AS UMIndenTerbayar, 
                          ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) AS UMIndenSisa, 
                          IF(ttdk.DKDPTerima  + ttdk.JADKDPTerima - ttdk.ReturHarga - ttdk.InsentifSales - ttdk.PotonganHarga - IFNULL(KM_UMD.Bayar, 0) <= 0 AND ttdk.DKDPLLeasing + JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) <= 0 AND ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) <= 0, 'Lunas', 'Belum') AS STATUS,
                          ttdk.LeaseKode, tdsales.SalesNama, tdcustomer.CusPembeliNama, ttdk.DKMediator, ttdk.TeamKode
                          FROM ttdk
                          INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
                          INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
                          INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType
                          INNER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode
                          LEFT OUTER JOIN
                          (SELECT vtkm.KMLINK, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          WHERE vtkm.KMJenis = 'Kredit' OR vtkm.KMJenis = 'Tunai'
                          GROUP BY vtkm.KMLINK, vtkm.KMJenis) KM_UMD ON ttdk.DKNo = KM_UMD.KMLINK
                          LEFT OUTER JOIN
                          (SELECT vtkm.KMLINK, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          WHERE vtkm.KMJenis = 'Piutang UM' 
                          GROUP BY vtkm.KMLINK, vtkm.KMJenis) KM_UMPK ON ttdk.DKNo = KM_UMPK.KMLINK
                          LEFT OUTER JOIN
                          (SELECT vtkm.KMLINK, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          WHERE vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing'
                          GROUP BY vtkm.KMLINK, vtkm.KMJenis) KM_SisaPiutang ON ttdk.DKNo = KM_SisaPiutang.KMLINK
                          LEFT OUTER JOIN
                          (SELECT ttin.DKNo, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          INNER JOIN ttin ON vtkm.KMLINK = ttin.INNo
                          WHERE vtkm.KMJenis = 'Inden' GROUP BY ttin.DKNo) KM_UMInden ON ttdk.DKNo = KM_UMInden.DKNo
                          WHERE ";
                  
          SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttdk.TeamKode LIKE '",xTeamSales,"') AND (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (ttdk.LeaseKode LIKE '",xLeasing,"') AND (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND (tmotor.MotorWarna LIKE '",xWarna,"') AND 
                         (ttdk.DKJenis LIKE '",xJenis,"') AND (ttdk.INNo LIKE '",xInden,"') AND (IFNULL(tmotor.SKNo, '--') LIKE '",xNoSK,"')");
          
          IF xLunas = "Sudah" THEN
             SET @MyQuery = CONCAT(@MyQuery," AND IF(ttdk.DKDPTerima  + ttdk.JADKDPTerima - ttdk.ReturHarga - ttdk.InsentifSales - ttdk.PotonganHarga - IFNULL(KM_UMD.Bayar, 0) <= 0 AND ttdk.DKDPLLeasing + JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) <= 0 AND ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) <= 0, 'Lunas', 'Belum') = 'Lunas' ");
          ELSE
             IF xLunas = "Belum" THEN
                SET @MyQuery = CONCAT(@MyQuery," AND IF(ttdk.DKDPTerima  + ttdk.JADKDPTerima - ttdk.ReturHarga - ttdk.InsentifSales - ttdk.PotonganHarga - IFNULL(KM_UMD.Bayar, 0) <= 0 AND ttdk.DKDPLLeasing + JADKDPLLeasing - IFNULL(KM_UMPK.Bayar, 0) <= 0 AND ttdk.DKDPInden - IFNULL(KM_UMInden.Bayar, 0) <= 0, 'Lunas', 'Belum') = 'Belum' ");
             END IF;
          END IF;
          
          SET @MyQuery = CONCAT(@MyQuery," ORDER BY ",xSort," ",xOrder);
          
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Kredit' OR vtkm.KMJenis = 'Tunai'", 
                 CONCAT("WHERE (vtkm.KMJenis = 'Kredit' OR vtkm.KMJenis = 'Tunai') AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
                 
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Piutang UM'", 
                 CONCAT("WHERE vtkm.KMJenis = 'Piutang UM' AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
                 
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Inden'", 
                 CONCAT("WHERE vtkm.KMJenis = 'Inden' AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
                 
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing'", 
                 CONCAT("WHERE (vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing') AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
       END IF;

       /* PiutangTagih */
       IF xTipe = "ttdkStatus7" THEN 
          SET @MyQuery = "SELECT ttdk.DKNo, ttdk.DKTgl, 
                          IF(TRIM(tdcustomer.CusNama) <> TRIM(tdcustomer.CusPembeliNama)AND tdcustomer.CusPembeliNama <> '--' AND tdcustomer.CusPembeliNama <> '', CONCAT_WS(' qq ', tdcustomer.CusNama, tdcustomer.CusPembeliNama), tdcustomer.CusNama) AS CusNama, 
                          CONCAT_WS('', tdcustomer.CusAlamat, '  RT/RW : ', LPAD(tdcustomer.CusRT,3,'0'), '/', LPAD(tdcustomer.CusRW,3,'0')) AS CusAlamat, 
                          tdmotortype.MotorType, tmotor.MotorNoMesin, ttdk.LeaseKode, 
                          IFNULL(KM_SisaPiutang.KMTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS KMTgl, ttdk.DKTglTagih, ttdk.DKPONo, ttdk.DKPOTgl,
                          (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) AS SisaPiutang, 
                          IFNULL(KM_SisaPiutang.Bayar, 0) AS SisaPiutangTerbayar, 
                          (ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(KM_SisaPiutang.Bayar, 0) AS SisaPiutangSisa,  
                          IF((ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(KM_SisaPiutang.Bayar, 0) <= 0 , 'Lunas', 'Belum') AS STATUS
                          FROM ttdk 
                          INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
                          INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
                          INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType
                          INNER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode
                          LEFT OUTER JOIN
                          (SELECT vtkm.KMLINK, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar, MAX(vtkm.KMTgl) AS KMTgl FROM vtkm  WHERE vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing' GROUP BY vtkm.KMLINK, vtkm.KMJenis) KM_SisaPiutang
                          ON ttdk.DKNo = KM_SisaPiutang.KMLINK
                          WHERE ";
                  
          SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttdk.TeamKode LIKE '",xTeamSales,"') AND (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (ttdk.LeaseKode LIKE '",xLeasing,"') AND (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND (tmotor.MotorWarna LIKE '",xWarna,"') AND 
                         (ttdk.DKJenis LIKE '",xJenis,"') AND (ttdk.INNo LIKE '",xInden,"') AND (IFNULL(tmotor.SKNo, '--') LIKE '",xNoSK,"')");
          
          IF xLunas = "Sudah" THEN
             SET @MyQuery = CONCAT(@MyQuery," AND IF((ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(KM_SisaPiutang.Bayar, 0) <= 0 , 'Lunas', 'Belum') = 'Lunas' ");
          ELSE
             IF xLunas = "Belum" THEN
                SET @MyQuery = CONCAT(@MyQuery," AND IF((ttdk.DKNetto + ttdk.JADKHarga - ttdk.JADKDPTerima - ttdk.JADKDPLLeasing - ttdk.JAPrgSubsSupplier - ttdk.JAPrgSubsDealer - ttdk.JAPrgSubsFincoy) - IFNULL(KM_SisaPiutang.Bayar, 0) <= 0 , 'Lunas', 'Belum') = 'Belum' ");
             END IF;
          END IF;
          
          SET @MyQuery = CONCAT(@MyQuery," ORDER BY ",xSort," ",xOrder);
          
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Kredit' OR vtkm.KMJenis = 'Tunai'", 
                 CONCAT("WHERE (vtkm.KMJenis = 'Kredit' OR vtkm.KMJenis = 'Tunai') AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
                 
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Piutang UM'", 
                 CONCAT("WHERE vtkm.KMJenis = 'Piutang UM' AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
                 
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Inden'", 
                 CONCAT("WHERE vtkm.KMJenis = 'Inden' AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
                 
          SELECT REPLACE(@MyQuery,"WHERE vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing'", 
                 CONCAT("WHERE (vtkm.KMJenis = 'Piutang Sisa' OR vtkm.KMJenis = 'Leasing') AND vtkm.KMtgl <= '",xTgl3,"' ")) INTO @MyQuery;
          
          IF xLeasing = "KREDIT" THEN
              SELECT REPLACE(@MyQuery,CONCAT("(ttdk.LeaseKode LIKE '",xLeasing,"'"),(ttdk.LeaseKode <> 'TUNAI')) INTO @MyQuery;
          END IF;
       END IF;

       /* BBNProgresif */
       IF xTipe = "bbnpro" THEN 
          SET @MyQuery = "SELECT ttdk.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tdmotortype.MotorType, tmotor.MotorNoMesin, BBN, 
                          ttdk.LeaseKode, ttdk.BBNPlus,  IF(ttdk.BBNPlus=0,'--',IF((ttdk.BBNPlus-KM_BBNPlus.Bayar)=0,'Lunas','Belum')) AS MyStatus, STNKTglAmbil
                          FROM ttdk
                          INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
                          INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
                          INNER JOIN tdmotortype ON tdmotortype.MotorType = tmotor.MotorType
                          INNER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode
                          LEFT OUTER JOIN
                          (SELECT vtkm.KMLINK, IFNULL(SUM(vtkm.KMNominal), 0) AS Bayar FROM vtkm
                          WHERE vtkm.KMJenis = 'BBN Plus' 
                          GROUP BY vtkm.KMLINK, vtkm.KMJenis) KM_BBNPlus ON ttdk.DKNo = KM_BBNPlus.KMLINK
                          LEFT OUTER JOIN
                          (SELECT vtkk.kkLINK, IFNULL(SUM(vtkk.kkNominal), 0) AS Bayar FROM vtkk
                          WHERE vtkk.kkJenis = 'BBN Plus' 
                          GROUP BY vtkk.kkLINK, vtkk.kkJenis) kk_BBNPlus ON ttdk.DKNo = kk_BBNPlus.kkLINK
                          WHERE ";
                  
          SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttdk.TeamKode LIKE '",xTeamSales,"') AND (ttdk.SalesKode LIKE '",xSales,"') AND 
                         (ttdk.LeaseKode LIKE '",xLeasing,"') AND (IFNULL(tdcustomer.CusKabupaten, '--') LIKE '",xKab,"') AND 
                         (tmotor.MotorType LIKE '",xTypeMtr,"') AND (tmotor.MotorWarna LIKE '",xWarna,"') AND 
                         (ttdk.DKJenis LIKE '",xJenis,"') AND (ttdk.BBNPLus > 0) AND 
                         IF(ttdk.BBNPlus=0,'--',IF((ttdk.BBNPlus-KK_BBNPlus.Bayar)=0,'Lunas','Belum')) LIKE '%",xNoSK,"' AND 
                         IF(ttdk.BBNPlus=0,'--',IF((ttdk.BBNPlus-KM_BBNPlus.Bayar)=0,'Lunas','Belum')) LIKE '%",xLunas,"'");
          
       END IF;

       PREPARE STMT FROM @MyQuery;
       EXECUTE Stmt;
    
    END$$

DELIMITER ;

