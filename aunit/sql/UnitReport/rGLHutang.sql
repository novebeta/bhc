DELIMITER $$

DROP PROCEDURE IF EXISTS `rGLHutang`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rGLHutang`(IN xTipe VARCHAR(50),IN xTgl1 DATE,IN xTgl2 DATE,
IN xPerkiraan1 VARCHAR(10),IN xPerkiraan2 VARCHAR(10), IN xStatus VARCHAR(5), IN xSensitif VARCHAR(1), IN xNoBukti VARCHAR(30))
BEGIN
/* CALL rGLHutang('Mutasi Kredit Hutang','2020-02-01','2020-02-29','%','%','Belum','0','',''); */
								 
	IF xTipe = 'Saldo Hutang Supplier' THEN
		IF xSensitif = "1" THEN
			DELETE FROM tvhutangdebet ;
			INSERT INTO tvhutangdebet  SELECT * FROM vhutangdebet ;
		END IF;
		DROP TEMPORARY TABLE IF EXISTS tvhutangbayar0 ;
		CREATE TEMPORARY TABLE IF NOT EXISTS tvhutangbayar0 AS
		SELECT GLLink AS HPLink, NoAccount, SUM(HPNilai) AS SumDebetGL FROM ttglhpit
		WHERE (LEFT(NoAccount, 2) BETWEEN '24' AND '24') AND HPJenis = 'Hutang' AND TglGL <= xTgl2
		GROUP BY HPLink, NoAccount ;
		CREATE INDEX HPLink ON tvhutangbayar0(HPLink);
		CREATE INDEX NoAccount ON tvhutangbayar0(NoAccount);

		CASE xStatus
			WHEN "Belum" THEN
				SET @LunasScript = " tvhutangkredit.KreditGL - IFNULL(tvhutangbayar0.SumDebetGL, 0) <> 0 AND ";
			WHEN "Lunas" THEN
				SET @LunasScript = " tvhutangkredit.KreditGL - IFNULL(tvhutangbayar0.SumDebetGL, 0) = 0 AND ";
			ELSE
				SET @LunasScript = " ";
		END CASE;

		DROP TEMPORARY TABLE IF EXISTS tHutangSupplier;
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
      SET @MyQuery = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS tHutangSupplier AS
        SELECT tvhutangkredit.GLLink, tvhutangkredit.NoGL, tvhutangkredit.TglGL, tvhutangkredit.NoAccount,
        tvhutangkredit.NamaAccount, tvhutangkredit.KeteranganGL, tvhutangkredit.KreditGL,
        IFNULL(tvhutangbayar0.SumDebetGL, 0) AS HutangLunas, tvhutangkredit.KreditGL - IFNULL(tvhutangbayar0.SumDebetGL, 0) AS HutangSisa,
        IFNULL(ttfb.FBNo,'--') AS FBNo, IFNULL(ttfb.FBTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS FBTgl, IFNULL(ttfb.SSNo,'--') AS SSNo,
        IFNULL(ttfb.FBTermin,0) AS FBTermin, ttfb.FBTglTempo, tdsupplier.SupNama, ttfb.FBMemo, ttfb.FBLunas
        FROM tvhutangkredit
				LEFT OUTER JOIN tvhutangbayar0
        ON tvhutangkredit.GLLink = tvhutangbayar0.HPLink AND tvhutangkredit.NoAccount = tvhutangbayar0.NoAccount
        LEFT OUTER JOIN ttfb ON ttfb.FBNo =  tvhutangkredit.GLLink
        LEFT OUTER JOIN tdsupplier ON ttfb.SupKode =  tdsupplier.SupKode
        WHERE  ",@LunasScript);
			SET @MyQuery = CONCAT(@MyQuery," (tvhutangkredit.KodeTrans = 'FB') ");
			SET @MyQuery = CONCAT(@MyQuery," AND (tvhutangkredit.TglGL <= '",xTgl2,"') ");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY tvhutangkredit.NoAccount, tvhutangkredit.TglGL, tvhutangkredit.GLLink; ");
		ELSE
      SET @MyQuery = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS tHutangSupplier AS
        SELECT tvhutangkredit.GLLink, tvhutangkredit.NoGL, tvhutangkredit.TglGL, tvhutangkredit.NoAccount,
        tvhutangkredit.NamaAccount, tvhutangkredit.KeteranganGL, tvhutangkredit.KreditGL,
        IFNULL(tvhutangbayar0.SumDebetGL, 0) AS HutangLunas, tvhutangkredit.KreditGL - IFNULL(tvhutangbayar0.SumDebetGL, 0) AS HutangSisa,
        IFNULL(ttfb.FBNo,'--') AS FBNo, IFNULL(ttfb.FBTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS FBTgl, IFNULL(ttfb.SSNo,'--') AS SSNo,
        IFNULL(ttfb.FBTermin,0) AS FBTermin, ttfb.FBTglTempo, tdsupplier.SupNama, ttfb.FBMemo, ttfb.FBLunas
        FROM tvhutangkredit
				LEFT OUTER JOIN tvhutangbayar0
        ON tvhutangkredit.GLLink = tvhutangbayar0.HPLink AND tvhutangkredit.NoAccount = tvhutangbayar0.NoAccount
        LEFT OUTER JOIN ttfb ON ttfb.FBNo =  tvhutangkredit.GLLink
        LEFT OUTER JOIN tdsupplier ON ttfb.SupKode =  tdsupplier.SupKode
        WHERE  ",@LunasScript);
			SET @MyQuery = CONCAT(@MyQuery," (tvhutangkredit.KodeTrans = 'FB') ");
			SET @MyQuery = CONCAT(@MyQuery," AND (tvhutangkredit.TglGL <= '",xTgl2,"') ");
			SET @MyQuery = CONCAT(@MyQuery," AND (tvhutangkredit.NoAccount BETWEEN '" , xPerkiraan1 , "' AND '" , xPerkiraan2 , "') ");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY tvhutangkredit.NoAccount, tvhutangkredit.TglGL, tvhutangkredit.GLLink; ");
		END IF;
		PREPARE STMT FROM @MyQuery;
		EXECUTE Stmt;
		SET @MyQuery = "SELECT * FROM tHutangSupplier;";
	END IF;

	IF xTipe = 'Saldo Hutang Non Supplier' THEN
		IF xSensitif = "1" THEN
			DELETE FROM tvhutangdebet ;
			INSERT INTO tvhutangdebet  SELECT * FROM vhutangdebet ;
		END IF;
		DROP TEMPORARY TABLE IF EXISTS tvhutangbayar0 ;
		CREATE TEMPORARY TABLE IF NOT EXISTS tvhutangbayar0 AS
		SELECT GLLink AS HPLink, NoAccount, SUM(HPNilai) AS SumDebetGL FROM ttglhpit
		WHERE (LEFT(NoAccount, 2) BETWEEN '24' AND '24') AND HPJenis = 'Hutang' AND TglGL <= xTgl2
		GROUP BY HPLink, NoAccount ;
		CREATE INDEX HPLink ON tvhutangbayar0(HPLink);
		CREATE INDEX NoAccount ON tvhutangbayar0(NoAccount);

		CASE xStatus
			WHEN "Belum" THEN
				SET @LunasScript = " tvhutangkredit.KreditGL - IFNULL(tvhutangbayar0.SumDebetGL, 0) <> 0 AND ";
			WHEN "Lunas" THEN
				SET @LunasScript = " tvhutangkredit.KreditGL - IFNULL(tvhutangbayar0.SumDebetGL, 0) = 0 AND ";
			ELSE
				SET @LunasScript = " ";
		END CASE;

		DROP TEMPORARY TABLE IF EXISTS tHutangNonSupplier;
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS tHutangNonSupplier AS
			SELECT tvhutangkredit.GLLink, tvhutangkredit.NoGL, tvhutangkredit.TglGL, tvhutangkredit.NoAccount,
			tvhutangkredit.NamaAccount, tvhutangkredit.KeteranganGL, tvhutangkredit.KreditGL,
			IFNULL(tvhutangbayar0.SumDebetGL, 0) AS HutangLunas, tvhutangkredit.KreditGL - IFNULL(tvhutangbayar0.SumDebetGL, 0) AS HutangSisa
			FROM tvhutangkredit
			LEFT OUTER JOIN tvhutangbayar0
			ON tvhutangkredit.GLLink = tvhutangbayar0.HPLink AND tvhutangkredit.NoAccount = tvhutangbayar0.NoAccount
			WHERE MyLunasScript
			(tvhutangkredit.NoAccount LIKE 'Kode1') AND
			tvhutangkredit.KodeTrans <> 'FB'
			AND (tvhutangkredit.TglGL <= 'MyTgl' )
			ORDER BY tvhutangkredit.NoAccount, tvhutangkredit.TglGL, tvhutangkredit.GLLink ");
			SET @MyQuery = REPLACE(@MyQuery, "Kode1", "%");
			SET @MyQuery = REPLACE(@MyQuery, "MyTgl", xTgl2);
			SET @MyQuery = REPLACE(@MyQuery, "MyLunasScript", @LunasScript);
		ELSE
			SET @MyQuery = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS tHutangNonSupplier AS
			SELECT tvhutangkredit.GLLink, tvhutangkredit.NoGL, tvhutangkredit.TglGL, tvhutangkredit.NoAccount,
			tvhutangkredit.NamaAccount, tvhutangkredit.KeteranganGL, tvhutangkredit.KreditGL,
			IFNULL(tvhutangbayar0.SumDebetGL, 0) AS HutangLunas, tvhutangkredit.KreditGL - IFNULL(tvhutangbayar0.SumDebetGL, 0) AS HutangSisa
			FROM tvhutangkredit
			LEFT OUTER JOIN tvhutangbayar0
			ON tvhutangkredit.GLLink = tvhutangbayar0.HPLink AND tvhutangkredit.NoAccount = tvhutangbayar0.NoAccount
			WHERE MyLunasScript  (tvhutangkredit.NoAccount BETWEEN 'Kode1' AND 'Kode2')
			AND tvhutangkredit.KodeTrans <> 'FB'
			AND (tvhutangkredit.TglGL <= 'MyTgl')
			ORDER BY tvhutangkredit.NoAccount, tvhutangkredit.TglGL, tvhutangkredit.GLLink");
			SET @MyQuery = REPLACE(@MyQuery, "Kode1", xPerkiraan1);
			SET @MyQuery = REPLACE(@MyQuery, "Kode2", xPerkiraan2);
			SET @MyQuery = REPLACE(@MyQuery, "MyTgl", xTgl2);
			SET @MyQuery = REPLACE(@MyQuery, "MyLunasScript", @LunasScript);
		END IF;
		PREPARE STMT FROM @MyQuery;
		EXECUTE Stmt;
		SET @MyQuery = "SELECT * FROM tHutangNonSupplier;";
	END IF;

	IF xTipe = 'Mutasi Debet Hutang' THEN
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT("SELECT vglall.* FROM vglall
			WHERE vglall.KodeTrans <> 'NA' AND (LEFT(vglall.NoAccount, 2) BETWEEN '24' AND '25') AND vglall.DebetGL > 0  ");
			SET @MyQuery = CONCAT(@MyQuery," AND vglall.TglGL BETWEEN '",xTgl1,"' AND '", xTgl2 , "'");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY NoAccount , GLLink ;");
		ELSE
			SET @MyQuery = CONCAT("SELECT vglall.* FROM vglall
			WHERE vglall.KodeTrans <> 'NA' AND (LEFT(vglall.NoAccount, 2) BETWEEN '24' AND '25') AND vglall.DebetGL > 0  ");
			SET @MyQuery = CONCAT(@MyQuery," AND vglall.NoAccount BETWEEN '" , xPerkiraan1 , "' AND '", xPerkiraan2, "' ");
			SET @MyQuery = CONCAT(@MyQuery," AND vglall.TglGL BETWEEN '",xTgl1,"' AND '", xTgl2 , "'");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY NoAccount , GLLink ;");
		END IF;
	END IF;
	
	IF xTipe = 'Mutasi Kredit Hutang' THEN
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT("SELECT vglall.* FROM vglall
			WHERE vglall.KodeTrans <> 'NA' AND (LEFT(vglall.NoAccount, 2) BETWEEN '24' AND '25') AND vglall.KreditGL > 0  ");
			SET @MyQuery = CONCAT(@MyQuery," AND vglall.TglGL BETWEEN '",xTgl1,"' AND '", xTgl2 , "'");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY NoAccount , GLLink ;");
		ELSE
			SET @MyQuery = CONCAT("SELECT vglall.* FROM vglall
			WHERE vglall.KodeTrans <> 'NA' AND (LEFT(vglall.NoAccount, 2) BETWEEN '24' AND '25') AND vglall.KreditGL > 0  ");
			SET @MyQuery = CONCAT(@MyQuery," AND vglall.NoAccount BETWEEN '" , xPerkiraan1 , "' AND '", xPerkiraan2, "' ");
			SET @MyQuery = CONCAT(@MyQuery," AND vglall.TglGL BETWEEN '",xTgl1,"' AND '", xTgl2 , "'");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY NoAccount , GLLink ;");
		END IF;
	END IF;

	IF xTipe = 'Umur Hutang - Curent' OR xTipe = 'Umur Hutang - Overdue' THEN
		IF xSensitif = "1" THEN
			DELETE FROM tvhutangkredit ;
			INSERT INTO tvhutangkredit  SELECT * FROM vhutangkredit ;
		END IF;
		DROP TEMPORARY TABLE IF EXISTS tvhutangbayar0 ;
		CREATE TEMPORARY TABLE IF NOT EXISTS tvhutangbayar0 AS
		SELECT GLLink AS HPLink, NoAccount, SUM(HPNilai) AS SumDebetGL FROM ttglhpit
		WHERE (LEFT(NoAccount, 2) BETWEEN '24' AND '24') AND HPJenis = 'Hutang' AND TglGL <= xTgl2
		GROUP BY HPLink, NoAccount ;
		CREATE INDEX HPLink ON tvhutangbayar0(HPLink);
		CREATE INDEX NoAccount ON tvhutangbayar0(NoAccount);

		DROP TEMPORARY TABLE IF EXISTS Hutang ;
		CREATE TEMPORARY TABLE IF NOT EXISTS Hutang AS
		SELECT tvhutangkredit.GLLink, tvhutangkredit.NoGL, tvhutangkredit.TglGL, tvhutangkredit.NoAccount,
         tvhutangkredit.NamaAccount, tvhutangkredit.KeteranganGL, tvhutangkredit.KreditGL,
         IFNULL(tvhutangbayar.SumDebetGL, 0) AS HutangLunas, tvhutangkredit.KreditGL - IFNULL(tvhutangbayar.SumDebetGL, 0) AS HutangSisa,
         000000000000000000.00 AS OD, 000000000000000000.00 AS CR, 000000000000000000.00 AS R1, 000000000000000000.00 AS R2, 000000000000000000.00 AS R3, 000000000000000000.00 AS R4
         FROM tvhutangkredit
         LEFT OUTER JOIN tvhutangbayar
         ON tvhutangkredit.GLLink = tvhutangbayar.HPLink AND tvhutangkredit.NoAccount = tvhutangbayar.NoAccount
         WHERE tvhutangkredit.KreditGL - IFNULL(tvhutangbayar.SumDebetGL, 0) > 0
         AND tvhutangkredit.KodeTrans = 'FB';
		CREATE INDEX GLLink ON Hutang(GLLink);

		DROP TEMPORARY TABLE IF EXISTS Supplier ;
		CREATE TEMPORARY TABLE IF NOT EXISTS Supplier AS
		SELECT ttfb.FBNo, ttfb.FBTgl, ttfb.SSNo, ttfb.FBTermin, ttfb.FBTglTempo, tdsupplier.SupNama, ttfb.FBMemo, ttfb.FBLunas
		FROM ttfb INNER JOIN tdsupplier ON ttfb.SupKode = tdsupplier.SupKode ;
		CREATE INDEX FBNo ON Supplier(FBNo);

		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT("SELECT * FROM Hutang
			INNER JOIN  Supplier ON Hutang.GLLink = Supplier.FBNo
			WHERE NoAccount LIKE '%" , xPerkiraan1  , "'
			AND (TglGL <= '" , xTgl2 , "' )
			ORDER BY NoAccount, TglGL, GLLink ");
		ELSE
			SET @MyQuery = CONCAT("SELECT * FROM Hutang
			INNER JOIN  Supplier ON Hutang.GLLink = Supplier.FBNo
			WHERE (NoAccount BETWEEN '" , xPerkiraan1 , "' AND '" , xPerkiraan2 , "')
			AND (TglGL <= '" , xTgl2 , "' )
			ORDER BY NoAccount, TglGL, GLLink ");
		END IF;
	END IF;

	
	IF xTipe = 'Riwayat Hutang Supplier' THEN
		IF xSensitif = "1" THEN
			DELETE FROM tvhutangkredit ;
			INSERT INTO tvhutangkredit  SELECT * FROM vhutangkredit ;
		END IF;

		DROP TEMPORARY TABLE IF EXISTS vhutangbayar;
		CREATE TEMPORARY TABLE IF NOT EXISTS vhutangbayar AS
		SELECT ttgeneralledgerhd.GLLink AS NoPelunasan, ttglhpit.TglGL, ttglhpit.GLLink AS HPLink, NoAccount, HPNilai AS DebetGL, MemoGL
		FROM ttglhpit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.NoGL = ttglhpit.NoGL AND ttgeneralledgerhd.TglGL = ttglhpit.TglGL
		WHERE (LEFT(NoAccount, 2) BETWEEN '24' AND '24') AND HPJenis = 'Hutang'
		AND ttglhpit.TglGL BETWEEN xTgl1  AND xTgl2;
		CREATE INDEX HPLink ON vhutangbayar(HPLink);
		CREATE INDEX NoAccount ON vhutangbayar(NoAccount);

		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT("SELECT tvhutangkredit.GLLink, tvhutangkredit.NoGL, tvhutangkredit.TglGL, tvhutangkredit.NoAccount, tvhutangkredit.NamaAccount, tvhutangkredit.KeteranganGL, tvhutangkredit.KreditGL, 
            IFNULL(vhutangbayar.NoPelunasan, '--') AS NoPelunasan, vhutangbayar.TglGL AS TglPelunasan, IFNULL(vhutangbayar.MemoGL, '--') AS MemoPelunasan, IFNULL(vhutangbayar.DebetGL, 0) AS DebetGL 
             FROM tvhutangkredit 
             LEFT OUTER JOIN vhutangbayar ON tvhutangkredit.GLLink = vhutangbayar.HPLink AND tvhutangkredit.NoAccount = vhutangbayar.NoAccount 
            WHERE tvhutangkredit.KodeTrans = 'FB'
            AND (tvhutangkredit.TglGL  BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "')  
            ORDER BY NoAccount, vhutangbayar.TglGL ");
		ELSE
			SET @MyQuery = CONCAT("SELECT tvhutangkredit.GLLink, tvhutangkredit.NoGL, tvhutangkredit.TglGL, tvhutangkredit.NoAccount, tvhutangkredit.NamaAccount, tvhutangkredit.KeteranganGL, tvhutangkredit.KreditGL, 
            IFNULL(vhutangbayar.NoPelunasan, '--') AS NoPelunasan, vhutangbayar.TglGL AS TglPelunasan, IFNULL(vhutangbayar.MemoGL, '--') AS MemoPelunasan, IFNULL(vhutangbayar.DebetGL, 0) AS DebetGL 
             FROM tvhutangkredit 
             LEFT OUTER JOIN vhutangbayar ON tvhutangkredit.GLLink = vhutangbayar.HPLink AND tvhutangkredit.NoAccount = vhutangbayar.NoAccount 
            WHERE tvhutangkredit.KodeTrans = 'FB'
            AND (tvhutangkredit.TglGL  BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "') 
            AND (ttvhutangkredit.NoAccount BETWEEN '",xPerkiraan1,"' AND '",xPerkiraan2,"') 
            ORDER BY NoAccount, vhutangbayar.TglGL ");           
		END IF;

		IF xNoBukti <> "" AND LENGTH(xNoBukti) >= 10 THEN
		SET @MyQuery = CONCAT("SELECT vhutangkredit.GLLink, vhutangkredit.NoGL, vhutangkredit.TglGL, vhutangkredit.NoAccount, vhutangkredit.NamaAccount, vhutangkredit.KeteranganGL, vhutangkredit.KreditGL, 
			IFNULL(vhutangbayar.NoPelunasan, '--') AS NoPelunasan, vhutangbayar.TglGL AS TglPelunasan, IFNULL(vhutangbayar.MemoGL, '--') AS MemoPelunasan, IFNULL(vhutangbayar.DebetGL, 0) AS DebetGL 
         FROM vhutangkredit LEFT OUTER JOIN vhutangbayar ON vhutangkredit.GLLink = vhutangbayar.HPLink AND vhutangkredit.NoAccount = vhutangbayar.NoAccount 
         WHERE vhutangkredit.KodeTrans = 'FB'
			AND vhutangkredit.GLLink = '" , xNoBukti , "' 
			ORDER BY NoAccount, vhutangbayar.TglGL  ");
		END IF;
	END IF;

	IF xTipe = 'Riwayat Hutang Non Supplier'  THEN
		IF xSensitif = "1" THEN
			DELETE FROM tvhutangkredit ;
			INSERT INTO tvhutangkredit  SELECT * FROM vhutangkredit ;
		END IF;

		DROP TEMPORARY TABLE IF EXISTS vhutangbayar;
		CREATE TEMPORARY TABLE IF NOT EXISTS vhutangbayar AS
		SELECT ttgeneralledgerhd.GLLink AS NoPelunasan, ttglhpit.TglGL, ttglhpit.GLLink AS HPLink, NoAccount, HPNilai AS DebetGL, MemoGL
		FROM ttglhpit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.NoGL = ttglhpit.NoGL AND ttgeneralledgerhd.TglGL = ttglhpit.TglGL
		WHERE (LEFT(NoAccount, 2) BETWEEN '24' AND '24') AND HPJenis = 'Hutang'
		AND ttglhpit.TglGL BETWEEN xTgl1  AND xTgl2;
		CREATE INDEX HPLink ON vhutangbayar(HPLink);
		CREATE INDEX NoAccount ON vhutangbayar(NoAccount);

		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT("SELECT tvhutangkredit.GLLink, tvhutangkredit.NoGL, tvhutangkredit.TglGL, tvhutangkredit.NoAccount, tvhutangkredit.NamaAccount, tvhutangkredit.KeteranganGL, tvhutangkredit.KreditGL, 
            IFNULL(vhutangbayar.NoPelunasan, '--') AS NoPelunasan, vhutangbayar.TglGL AS TglPelunasan, IFNULL(vhutangbayar.MemoGL, '--') AS MemoPelunasan, IFNULL(vhutangbayar.DebetGL, 0) AS DebetGL 
             FROM tvhutangkredit 
             LEFT OUTER JOIN vhutangbayar ON tvhutangkredit.GLLink = vhutangbayar.HPLink AND tvhutangkredit.NoAccount = vhutangbayar.NoAccount 
            WHERE tvhutangkredit.KodeTrans <> 'FB'
            AND (tvhutangkredit.TglGL  BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "')  
            ORDER BY NoAccount, vhutangbayar.TglGL ");
		ELSE
			SET @MyQuery = CONCAT("SELECT tvhutangkredit.GLLink, tvhutangkredit.NoGL, tvhutangkredit.TglGL, tvhutangkredit.NoAccount, tvhutangkredit.NamaAccount, tvhutangkredit.KeteranganGL, tvhutangkredit.KreditGL, 
            IFNULL(vhutangbayar.NoPelunasan, '--') AS NoPelunasan, vhutangbayar.TglGL AS TglPelunasan, IFNULL(vhutangbayar.MemoGL, '--') AS MemoPelunasan, IFNULL(vhutangbayar.DebetGL, 0) AS DebetGL 
             FROM tvhutangkredit 
             LEFT OUTER JOIN vhutangbayar ON tvhutangkredit.GLLink = vhutangbayar.HPLink AND tvhutangkredit.NoAccount = vhutangbayar.NoAccount 
            WHERE tvhutangkredit.KodeTrans <> 'FB'
            AND (tvhutangkredit.TglGL  BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "') 
            AND (ttvhutangkredit.NoAccount BETWEEN '",xPerkiraan1,"' AND '",xPerkiraan2,"') 
            ORDER BY NoAccount, vhutangbayar.TglGL ");           
		END IF;

		IF xNoBukti <> "" AND LENGTH(xNoBukti) >= 10 THEN
		SET @MyQuery = CONCAT("SELECT vhutangkredit.GLLink, vhutangkredit.NoGL, vhutangkredit.TglGL, vhutangkredit.NoAccount, vhutangkredit.NamaAccount, vhutangkredit.KeteranganGL, vhutangkredit.KreditGL, 
			IFNULL(vhutangbayar.NoPelunasan, '--') AS NoPelunasan, vhutangbayar.TglGL AS TglPelunasan, IFNULL(vhutangbayar.MemoGL, '--') AS MemoPelunasan, IFNULL(vhutangbayar.DebetGL, 0) AS DebetGL 
         FROM vhutangkredit LEFT OUTER JOIN vhutangbayar ON vhutangkredit.GLLink = vhutangbayar.HPLink AND vhutangkredit.NoAccount = vhutangbayar.NoAccount 
         WHERE vhutangkredit.KodeTrans <> 'FB'
			AND vhutangkredit.GLLink = '" , xNoBukti , "' 
			ORDER BY NoAccount, vhutangbayar.TglGL  ");
		END IF;
	END IF;

        
	IF xTipe = 'Perincian Pelunasan' THEN
		SET @MyQuery = CONCAT("SELECT ttglhpit.NoGL, ttglhpit.TglGL, ttglhpit.GLLink AS HutangPiutang,  ttgeneralledgerhd.GLLink AS Pelunasan, ttglhpit.NoAccount, 
		ttgeneralledgerhd.MemoGL As NamaAccount, HPJenis, HPNilai
      FROM ttglhpit
      INNER JOIN ttgeneralledgerhd ON ttglhpit.NoGL = ttgeneralledgerhd.NoGL
      WHERE HPJenis = 'Hutang'
      AND ttglhpit.TglGL  BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT(@MyQuery," ");	
		ELSE
			SET @MyQuery = CONCAT(@MyQuery," AND (ttglhpit.NoAccount BETWEEN '",xPerkiraan1,"' AND '",xPerkiraan2,"')");	
		END IF;
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY ttglhpit.NoGL, ttglhpit.TglGL, ttgeneralledgerhd.GLLink ");
	END IF;

	IF xTipe = 'Pelunasan BBN' THEN
		SET @MyQuery = CONCAT("SELECT ttglhpit.NoGL, ttglhpit.TglGL, tmotor.DKNo AS HutangPiutang,  ttgeneralledgerhd.GLLink AS Pelunasan, ttglhpit.NoAccount, ttgeneralledgerhd.MemoGL AS NamaAccount, HPJenis, HPNilai, IFNULL(DKNo,'--') AS DKNo
         FROM ttglhpit
         INNER JOIN ttgeneralledgerhd ON ttglhpit.NoGL = ttgeneralledgerhd.NoGL
         INNER JOIN tmotor ON ttglhpit.GLLink = tmotor.SKNo
         WHERE HPJenis = 'Hutang'
      AND ttglhpit.TglGL  BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT(@MyQuery," ");	
		ELSE
			SET @MyQuery = CONCAT(@MyQuery," AND (ttglhpit.NoAccount BETWEEN '",xPerkiraan1,"' AND '",xPerkiraan2,"')");	
		END IF;
      SET @MyQuery = CONCAT(@MyQuery," ORDER BY ttglhpit.NoGL, ttglhpit.TglGL, ttgeneralledgerhd.GLLink ");
	END IF;

	IF xTipe = "Belum Di Link" THEN
		IF xSensitif <> "1" THEN
			UPDATE ttgeneralledgerhd
			INNER JOIN (SELECT ttgeneralledgerhd.NoGL, HPLink, ttglhpit.GLLink
			FROM ttgeneralledgerhd
			INNER JOIN ttglhpit ON ttglhpit.NoGL =  ttgeneralledgerhd.NoGL
			WHERE HPLink = '--' OR HPLink = ''
			GROUP BY ttgeneralledgerhd.NoGL HAVING COUNT(ttglhpit.NoGL) > 1) ttglhpit ON ttglhpit.NoGL =  ttgeneralledgerhd.NoGL
			SET ttgeneralledgerhd.HPLink = 'Many'
			WHERE ttgeneralledgerhd.HPLink = '--' OR ttgeneralledgerhd.HPLink = '';
			UPDATE ttgeneralledgerhd
			INNER JOIN (SELECT ttgeneralledgerhd.NoGL, HPLink, ttglhpit.GLLink
			FROM ttgeneralledgerhd
			INNER JOIN ttglhpit ON ttglhpit.NoGL =  ttgeneralledgerhd.NoGL
			WHERE HPLink = '--' OR HPLink = ''
			GROUP BY ttgeneralledgerhd.NoGL HAVING COUNT(ttglhpit.NoGL) = 1) ttglhpit ON ttglhpit.NoGL =  ttgeneralledgerhd.NoGL
			SET ttgeneralledgerhd.HPLink = ttglhpit.GLLink
			WHERE ttgeneralledgerhd.HPLink = '--' OR ttgeneralledgerhd.HPLink = '';
			
			SET @MyQuery = CONCAT("SELECT ttgeneralledgerit.NoGL, ttgeneralledgerit.TglGL, ttgeneralledgerhd.GLLink, ttgeneralledgerit.NoAccount, 
			ttgeneralledgerit.KeteranganGL, ttgeneralledgerit.DebetGL As Nilai
         FROM ttgeneralledgerit 
         INNER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.NoGL = ttgeneralledgerit.NoGL 
         WHERE ttgeneralledgerit.DebetGL > 0 
         AND (LEFT(ttgeneralledgerit.NoAccount,2) BETWEEN '24' AND '24') AND ttgeneralledgerhd.HPLink = '--'
			AND ttgeneralledgerit.TglGL   BETWEEN '",xTgl1,"' AND '",xTgl2,"'");
			IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
				SET @MyQuery = CONCAT(@MyQuery," ");	
			ELSE
				SET @MyQuery = CONCAT(@MyQuery," AND (ttgeneralledgerit.NoAccount BETWEEN '",xPerkiraan1,"' AND '",xPerkiraan2,"')");	
			END IF;
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY ttgeneralledgerit.NoGL, ttgeneralledgerit.TglGL, ttgeneralledgerhd.GLLink ");		
		END IF;
		
		IF xSensitif = "1" THEN
			DROP TEMPORARY TABLE IF EXISTS tvhutangdebet ; 
			IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
				CREATE TEMPORARY TABLE IF NOT EXISTS tvhutangdebet AS SELECT * FROM vhutangdebet WHERE TglGL  BETWEEN Tgl1 AND Tgl2 ;
			ELSE
				CREATE TEMPORARY TABLE IF NOT EXISTS tvhutangdebet AS SELECT * FROM vhutangdebet WHERE TglGL BETWEEN Tgl1 AND Tgl2  AND (vhutangdebet.NoAccount BETWEEN xPerkiraan1 AND xPerkiraan2);
			END IF;
			CREATE INDEX NoGL ON tvhutangdebet(NoGL);
			CREATE INDEX NoAccount ON tvhutangdebet(NoAccount);
			
			SET @MyQuery = CONCAT("
			 SELECT tvhutangdebet.NoGL, tvhutangdebet.TglGL, tvhutangdebet.GLLink, tvhutangdebet.NoAccount, tvhutangdebet.KeteranganGL, 
            (tvhutangdebet.DebetGL - IFNULL(ttglhpit.HPNilai,0)) AS Nilai,  
            ttglhpit.* 
            FROM tvhutangdebet 
            LEFT OUTER JOIN 
            (SELECT NoGL, NoAccount, IFNULL(SUM(HPNilai),0) AS HPNilai FROM ttglhpit WHERE (LEFT(NoAccount, 2) BETWEEN '24' AND '24') AND HPJenis = 'Hutang'  GROUP BY NOGL, NOaccount) ttglhpit
            ON tvhutangdebet.NoGL = ttglhpit.NoGL AND tvhutangdebet.NoAccount = ttglhpit.NoAccount 
            WHERE ttglhpit.NoGL IS NULL OR (tvhutangdebet.DebetGL- ttglhpit.HPNilai) >0
			");
		END IF;
	END IF;
	
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;  
END$$

DELIMITER ;

