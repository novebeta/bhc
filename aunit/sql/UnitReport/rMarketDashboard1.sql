DELIMITER $$
DROP PROCEDURE IF EXISTS `rMarketDashboard1`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rMarketDashboard1`(IN xTipe VARCHAR(25),IN xStatTgl VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,IN xLeasing VARCHAR(20))
BEGIN
/*
       xTipe    : "teamjual1" -> Team Jual
                  "tmotor" -> Detail
                  
CALL rMarketDashboard1("Team Leasing","",'2020-02-01','2020-02-29','');
*/

	DECLARE xResultStatTgl VARCHAR(100);
	DECLARE xTgl3,xTgl4 VARCHAR(20);


	DECLARE XINC INT DEFAULT 0;
	DECLARE MyTeamKode VARCHAR(20);
	DECLARE MyLeaseKode VARCHAR(20);
   DECLARE done0 INT DEFAULT 0;
	DECLARE cur1 CURSOR FOR
	SELECT LeaseKode FROM tmotor
	INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
	INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
	WHERE ttsk.SKTgl BETWEEN xTgl1 AND xTgl2 AND LeaseKode <> 'TUNAI' AND LeaseKode <> 'KDS'
	GROUP BY LeaseKode ;
	DECLARE cur0 CURSOR FOR
	SELECT ttdk.TeamKode FROM tmotor
	INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
	INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
	WHERE ttsk.SKTgl BETWEEN xTgl1 AND xTgl2
	AND ttdk.LeaseKode <> 'TUNAI' AND ttdk.LeaseKode <> 'KDS'
	GROUP BY ttdk.TeamKode;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done0 = 1;


	/* SelectDK_SK */
	IF xStatTgl = "ttdk.DKTgl" THEN
		SET xResultStatTgl = " LEFT OUTER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo ";
	ELSE
		SET xResultStatTgl = " INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo ";
	END IF;

	/* TEAM JUAL */
	IF xTipe = "teamjual1" THEN 
		SET @MyQuery = "SELECT TeamKode, TeamNama, 0.0 As Target,0.0 As JualUnit,0.0 As JualPersen,
						  0.0 AS '01',0.0 AS '02',0.0 AS '03',0.0 AS '04',0.0 AS '05',0.0 AS '06',0.0 AS '07',0.0 AS '08',0.0 AS '09',0.0 AS '10', 
						  0.0 AS '11',0.0 AS '12',0.0 AS '13',0.0 AS '14',0.0 AS '15',0.0 AS '16',0.0 AS '17',0.0 AS '18',0.0 AS '19',0.0 AS '20', 
						  0.0 AS '21',0.0 AS '22',0.0 AS '23',0.0 AS '24',0.0 AS '25',0.0 AS '26',0.0 AS '27',0.0 AS '28',0.0 AS '29',0.0 AS '30',0.0 AS '31'
						  FROM tdteam order by TeamNo, TeamKode"; 
	END IF;

	/* FILL TEAM JUAL */
	IF xTipe = "teamjual2" THEN 
		SET @MyQuery = CONCAT("SELECT tdteam.TeamKode, IFNULL(Jumlah,0) AS Jumlah 
						 FROM tdteam
						 LEFT OUTER JOIN
						 (SELECT ttdk.TeamKode, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS Jumlah FROM tmotor
						 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 GROUP BY TRIM(ttdk.TeamKode)) Jum 
						 ON tdteam.TeamKode = Jum.TeamKode ORDER BY tdteam.TeamNo, tdteam.TeamKode"); 
	END IF;

	/* FILL TARGET TEAM JUAL */
	IF xTipe = "teamjual3" THEN 
		SET @MyQuery = CONCAT("SELECT tdteam.TeamKode As Kode, IFNULL(tmteamtarget.TargetQty, 0) AS Jumlah FROM tdteam 
						 LEFT OUTER JOIN 
						 (SELECT  TeamKode, SalesTgl , SUM(SalesQty) As TargetQty FROM  ttmsalestarget WHERE SalesTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' GROUP BY TeamKode, SalesTgl ) tmteamtarget 
						 ON tdteam.TeamKode = tmteamtarget.TeamKode 
						 ORDER BY  tdteam.TeamNo, tdteam.TeamKode"); 
	END IF;


	/* FILL TARGET TEAM JUAL */
	IF xTipe = "teamjual4" THEN 
	 SET @MyQuery = "SELECT TeamKode, TeamNama,0.0 As CashUnit,0.0 As CashPersen,0.0 As CreditUnit,0.0 As CreditPersen,
						 0.0 As L01Qty,0.0 As L01MS,0.0 As L01CS,0.0 As L02Qty,0.0 As L02MS,0.0 As L02CS,
						 0.0 As L03Qty,0.0 As L03MS,0.0 As L03CS,0.0 As L04Qty,0.0 As L04MS,0.0 As L04CS,
						 0.0 As L05Qty,0.0 As L05MS,0.0 As L05CS,0.0 As L06Qty,0.0 As L06MS,0.0 As L06CS,
						 0.0 As L07Qty,0.0 As L07MS,0.0 As L07CS,0.0 As L08Qty,0.0 As L08MS,0.0 As L08CS,
						 0.0 As L09Qty,0.0 As L09MS,0.0 As L09CS,0.0 As L10Qty,0.0 As L10MS,0.0 As L10CS 
						 FROM tdteam order by TeamNo, TeamKode"; 
	END IF;

	/* FILL TEAM CASH */
	IF xTipe = "teamjual5" THEN 
	 SET @MyQuery = CONCAT("SELECT tdteam.TeamKode, IFNULL(Jumlah,0) AS Jumlah 
						 FROM tdteam
						 LEFT OUTER JOIN
						 (SELECT ttdk.TeamKode, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS Jumlah FROM tmotor
						 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 AND (ttdk.LeaseKode = 'TUNAI' OR ttdk.LeaseKode = 'KDS') 
						 GROUP BY TRIM(ttdk.TeamKode)) Jum 
						 ON tdteam.TeamKode = Jum.TeamKode ORDER BY tdteam.TeamNo, tdteam.TeamKode"); 
	END IF;

	/* FILL TEAM CASH */
	IF xTipe = "teamjual6" THEN 
	 SET @MyQuery = CONCAT("SELECT tdteam.TeamKode, IFNULL(Jumlah,0) AS Jumlah 
						  FROM tdteam
						  LEFT OUTER JOIN
						  (SELECT ttdk.TeamKode, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS Jumlah FROM tmotor
						  INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						  INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
						  WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						  AND ttdk.LeaseKode LIKE '",xLeasing,"%'
						  GROUP BY TRIM(ttdk.TeamKode)) Jum 
						  ON tdteam.TeamKode = Jum.TeamKode ORDER BY tdteam.TeamNo, tdteam.TeamKode ");
	END IF;

	/* TEAM POTONGAN */
	IF xTipe = "teampotong1" THEN 
	 SET @MyQuery = "SELECT TeamKode, TeamNama, 0.0 As Total,0.0 As Cash, 0.0 As Credit, 0.0 As JualUnit FROM tdteam order by TeamNo, TeamKode"; 
	END IF;

	/* FILL POTONGAN CREDIT */
	IF xTipe = "teampotong2" THEN 
	 SET @MyQuery = CONCAT("SELECT tdteam.TeamKode, IFNULL(Cash/Jum,0) As Jumlah
						 FROM tdteam
						 LEFT OUTER JOIN
						 (SELECT TeamKode, SUM(ttdk.ReturHarga) + SUM(ttdk.PotonganHarga) AS Cash, COUNT(ttdk.DKno) As Jum
						 FROM ttdk
						 INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
						 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						 WHERE
						 ttdk.LeaseKode <> 'TUNAI' AND ttdk.LeaseKode <> 'KDS' 
						 AND ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 GROUP BY TRIM(ttdk.TeamKode)) Cash ON tdteam.TeamKode = Cash.TeamKode 
						 ORDER By tdteam.TeamNo, tdteam.TeamKode"); 
	END IF;

	/* FILL POTONGAN CASH */
	IF xTipe = "teampotong3" THEN 
	 SET @MyQuery = CONCAT("SELECT tdteam.TeamKode, IFNULL(Cash/Jum,0) AS Jumlah
	FROM tdteam
	LEFT OUTER JOIN
	(SELECT TeamKode, SUM(ttdk.ReturHarga) + SUM(ttdk.PotonganHarga) AS Cash, COUNT(ttdk.DKno) AS Jum
		FROM ttdk
		INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
		INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
		WHERE
		(ttdk.LeaseKode = 'TUNAI' OR ttdk.LeaseKode = 'KDS') 
	AND ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"'
	GROUP BY TRIM(ttdk.TeamKode)) Cash ON tdteam.TeamKode = Cash.TeamKode 
	ORDER BY tdteam.TeamNo, tdteam.TeamKode"); 
	END IF;


	/* FILL JUAL UNIT */
	IF xTipe = "teampotong4" THEN 
	 SET @MyQuery = CONCAT("SELECT tdteam.TeamKode, IFNULL(Jumlah,0) As Jumlah
						 FROM tdteam
						 LEFT OUTER JOIN
						 (SELECT TeamKode, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS Jumlah
						 FROM ttdk
						 INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
						 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo 
						 AND ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 GROUP BY TRIM(ttdk.TeamKode)) Cash ON tdteam.TeamKode = Cash.TeamKode 
						 ORDER By tdteam.TeamNo, tdteam.TeamKode"); 
	END IF;

	/* FILL TEAM SEGMEN */
	IF xTipe = "teampotong5" THEN 
	 SET @MyQuery = CONCAT("SELECT tdteam.TeamKode, IFNULL(Jumlah,0) AS Jumlah 
						 FROM tdteam
						 LEFT OUTER JOIN
						 (SELECT ttdk.TeamKode, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah
						 FROM tmotor
						 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
						 INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 AND LEFT(tdmotortype.MotorKategori,2) = '",xLeasing,"' 
						 GROUP BY TRIM(ttdk.TeamKode)) Jum 
						 ON tdteam.TeamKode = Jum.TeamKode ORDER BY tdteam.TeamNo, tdteam.TeamKode"); 
	END IF;

	/* DP UNIT */
	IF xTipe = "dpunit1" THEN 
	 SET @MyQuery = "SELECT TeamKode, TeamNama ,
						  0.0 As L0120,0.0 As L012030,0.0 As L0130,
						  0.0 As L0220,0.0 As L022030,0.0 As L0230,
						  0.0 As L0320,0.0 As L032030,0.0 As L0330,
						  0.0 As L0420,0.0 As L042030,0.0 As L0430,
						  0.0 As L0520,0.0 As L052030,0.0 As L0530,
						  0.0 As L0620,0.0 As L062030,0.0 As L0630,
						  0.0 As L0720,0.0 As L072030,0.0 As L0730,
						  0.0 As L0820,0.0 As L082030,0.0 As L0830,
						  0.0 As L0920,0.0 As L092030,0.0 As L0930,
						  0.0 As L1020,0.0 As L102030,0.0 As L1030 
						  FROM tdteam order by TeamNo, TeamKode"; 
	END IF;

	/* DP UNIT 1 */
	IF xTipe = "dpunit2" THEN 
	 SET @MyQuery = CONCAT("SELECT tdteam.TeamKode, IFNULL(Jumlah,0) AS Jumlah 
						 FROM tdteam
						 LEFT OUTER JOIN
						 (SELECT ttdk.TeamKode, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS Jumlah FROM tmotor
						 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 AND ttdk.LeaseKode LIKE '",xLeasing,"%' 
						 AND DKjenis = 'Kredit' 
						 AND (((DKDPTotal+ProgramSubsidi) / DKHarga) < 0.2)
						 GROUP BY TRIM(ttdk.TeamKode)) Jum
						 ON tdteam.TeamKode = Jum.TeamKode ORDER BY tdteam.TeamNo, tdteam.TeamKode"); 
	END IF;

	/* DP UNIT 2 */
	IF xTipe = "dpunit3" THEN 
	 SET @MyQuery = CONCAT("SELECT tdteam.TeamKode, IFNULL(Jumlah,0) AS Jumlah 
						 FROM tdteam
						 LEFT OUTER JOIN
						 (SELECT ttdk.TeamKode, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS Jumlah FROM tmotor
						 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 AND ttdk.LeaseKode LIKE '",xLeasing,"%' 
						 AND DKjenis = 'Kredit' 
						 AND (((DKDPTotal+ProgramSubsidi) / DKHarga) BETWEEN 0.2 AND 0.3) 
						 GROUP BY TRIM(ttdk.TeamKode)) Jum 
						 ON tdteam.TeamKode = Jum.TeamKode ORDER BY tdteam.TeamNo, tdteam.TeamKode"); 
	END IF;

	/* DP UNIT 3 */
	IF xTipe = "dpunit4" THEN 
	 SET @MyQuery = CONCAT("SELECT tdteam.TeamKode, IFNULL(Jumlah,0) AS Jumlah 
						 FROM tdteam
						 LEFT OUTER JOIN
						 (SELECT ttdk.TeamKode, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS Jumlah FROM tmotor
						 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 AND ttdk.LeaseKode LIKE '",xLeasing,"%' 
						 AND DKjenis = 'Kredit' 
						 AND ((DKDPTotal+ProgramSubsidi)/DKHarga > 0.3) 
						 GROUP BY TRIM(ttdk.TeamKode)) Jum 
						 ON tdteam.TeamKode = Jum.TeamKode ORDER BY tdteam.TeamNo, tdteam.TeamKode"); 
	END IF;


	/* SALES JUAL */
	IF xTipe = "salesjual1" THEN 
	 SET @MyQuery = CONCAT("SELECT CurrentTeamSales.TeamKode, tdteam.TeamNama, CurrentTeamSales.SalesKode, tdsales.SalesNama,
						 0.0 AS Target,0.0 AS JualUnit,0.0 AS JualPersen,0.0 AS HarusUnit,0.0 AS HarusPersen,0.0 AS PotensiSales,
						 0.0 AS '01',0.0 AS '02',0.0 AS '03',0.0 AS '04',0.0 AS '05',0.0 AS '06',0.0 AS '07',0.0 AS '08',0.0 AS '09',0.0 AS '10', 
						 0.0 AS '11',0.0 AS '12',0.0 AS '13',0.0 AS '14',0.0 AS '15',0.0 AS '16',0.0 AS '17',0.0 AS '18',0.0 AS '19',0.0 AS '20', 
						 0.0 AS '21',0.0 AS '22',0.0 AS '23',0.0 AS '24',0.0 AS '25',0.0 AS '26',0.0 AS '27',0.0 AS '28',0.0 AS '29',0.0 AS '30',0.0 AS '31'
						 FROM
						 (SELECT ttdk.TeamKode, ttdk.SalesKode FROM  tmotor
						 INNER JOIN  ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN  ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"'
						 GROUP BY TRIM(ttdk.TeamKode), ttdk.SalesKode) CurrentTeamSales
						 INNER JOIN  tdsales ON CurrentTeamSales.SalesKode = tdsales.SalesKode
						 INNER JOIN  tdteam ON CurrentTeamSales.TeamKode = tdteam.TeamKode
						 ORDER BY tdteam.TeamNo, tdteam.TeamKode, tdsales.SalesNama"); 
	END IF;

	/* FILL SALES JUAL */
	IF xTipe = "salesjual2" THEN
	 SET xTgl3 = CONCAT(SUBSTR(xLeasing,7,4),"-",SUBSTR(xLeasing,4,2),"-",LEFT(xLeasing,2));
	 SET xTgl4 = CONCAT(RIGHT(xLeasing,4),"-",SUBSTR(xLeasing,14,2),"-",SUBSTR(xLeasing,11,2));

	 SET @MySales = CONCAT("SELECT tdteam.TeamNo, CurrentTeamSales.TeamKode, tdteam.TeamNama, CurrentTeamSales.SalesKode, tdsales.SalesNama
						 FROM
						 (SELECT ttdk.TeamKode, ttdk.SalesKode FROM  tmotor
						 INNER JOIN  ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN  ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl3,"' AND '",xTgl4,"'
						 GROUP BY TRIM(ttdk.TeamKode), ttdk.SalesKode) CurrentTeamSales
						 INNER JOIN  tdsales ON CurrentTeamSales.SalesKode = tdsales.SalesKode
						 INNER JOIN  tdteam ON CurrentTeamSales.TeamKode = tdteam.TeamKode
						 ORDER BY tdteam.TeamNo, tdteam.TeamKode, tdsales.SalesNama");
						 
	 SET @MyQuery = CONCAT("SELECT QSales.SalesKode, IFNULL(Jum.Jumlah,0) AS Jumlah FROM 
						 ( ",@MySales," ) QSales 
						 LEFT OUTER JOIN 
						 (SELECT ttdk.TeamKode, ttdk.SalesKode, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS Jumlah FROM tmotor 
						 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo 
						 INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 GROUP BY TRIM(ttdk.TeamKode), ttdk.SalesKode) Jum 
						 ON QSales.TeamKode = Jum.TeamKode AND QSales.SalesKode = Jum.SalesKode  
						 ORDER BY QSales.TeamNo, QSales.TeamKode, QSales.SalesNama");
	END IF;

	/* FILL TARGET SALES JUAL */
	IF xTipe = "salesjual3" THEN
	 SET xTgl3 = CONCAT(SUBSTR(xLeasing,7,4),"-",SUBSTR(xLeasing,4,2),"-",LEFT(xLeasing,2));
	 SET xTgl4 = CONCAT(RIGHT(xLeasing,4),"-",SUBSTR(xLeasing,14,2),"-",SUBSTR(xLeasing,11,2));

	 SET @MySales = CONCAT("SELECT tdteam.TeamNo, CurrentTeamSales.TeamKode, tdteam.TeamNama, CurrentTeamSales.SalesKode, tdsales.SalesNama
						 FROM
						 (SELECT ttdk.TeamKode, ttdk.SalesKode FROM  tmotor
						 INNER JOIN  ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN  ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl3,"' AND '",xTgl4,"'
						 GROUP BY TRIM(ttdk.TeamKode), ttdk.SalesKode) CurrentTeamSales
						 INNER JOIN  tdsales ON CurrentTeamSales.SalesKode = tdsales.SalesKode
						 INNER JOIN  tdteam ON CurrentTeamSales.TeamKode = tdteam.TeamKode
						 ORDER BY tdteam.TeamNo, tdteam.TeamKode, tdsales.SalesNama");
						 
	 SET @MyQuery = CONCAT("SELECT  A.SalesKode AS Kode, IFNULL(B.SalesQty, 0) AS Jumlah FROM 
						 ( ",@MySales," ) A 
						 LEFT OUTER JOIN 
						 (SELECT ttmsalestarget.TeamKode, ttmsalestarget.SalesKode, ttmsalestarget.SalesQty FROM ttmsalestarget 
						 WHERE ttmsalestarget.SalesTgl BETWEEN '",xTgl3,"' AND '",xTgl4,"') B ON A.TeamKode = B.TeamKode AND A.SalesKode = B.SalesKode 
						 ORDER BY A.TeamNo, A.TeamKode, A.SalesNama");
	END IF;

	/* SALES CASH CREDIT */
	IF xTipe = "cashcredit1" THEN
	 SET @MyQuery = CONCAT("SELECT CurrentTeamSales.TeamKode, tdteam.TeamNama, CurrentTeamSales.SalesKode, tdsales.SalesNama,
						 0.0 AS CashUnit,0.0 AS CashPersen,0.0 AS CreditUnit,0.0 AS CreditPersen,
						 0.0 As L01Qty,0.0 As L01MS,0.0 As L01CS,0.0 As L02Qty,0.0 As L02MS,0.0 As L02CS,
						 0.0 As L03Qty,0.0 As L03MS,0.0 As L03CS,0.0 As L04Qty,0.0 As L04MS,0.0 As L04CS,
						 0.0 As L05Qty,0.0 As L05MS,0.0 As L05CS,0.0 As L06Qty,0.0 As L06MS,0.0 As L06CS,
						 0.0 As L07Qty,0.0 As L07MS,0.0 As L07CS,0.0 As L08Qty,0.0 As L08MS,0.0 As L08CS,
						 0.0 As L09Qty,0.0 As L09MS,0.0 As L09CS,0.0 As L10Qty,0.0 As L10MS,0.0 As L10CS 
						 FROM
						 (SELECT ttdk.TeamKode, ttdk.SalesKode FROM  tmotor
						 INNER JOIN  ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN  ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 GROUP BY TRIM(ttdk.TeamKode), ttdk.SalesKode) CurrentTeamSales
						 INNER JOIN  tdsales ON CurrentTeamSales.SalesKode = tdsales.SalesKode
						 INNER JOIN  tdteam ON CurrentTeamSales.TeamKode = tdteam.TeamKode
						 ORDER BY tdteam.TeamNo, tdteam.TeamKode, tdsales.SalesNama");
	END IF;

	/* FILL SALES CASH */
	IF xTipe = "cashcredit2" THEN
	 SET @MySales = CONCAT("SELECT tdteam.TeamNo, CurrentTeamSales.TeamKode, tdteam.TeamNama, CurrentTeamSales.SalesKode, tdsales.SalesNama
						 FROM
						 (SELECT ttdk.TeamKode, ttdk.SalesKode FROM  tmotor
						 INNER JOIN  ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN  ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 GROUP BY TRIM(ttdk.TeamKode), ttdk.SalesKode) CurrentTeamSales
						 INNER JOIN  tdsales ON CurrentTeamSales.SalesKode = tdsales.SalesKode
						 INNER JOIN  tdteam ON CurrentTeamSales.TeamKode = tdteam.TeamKode
						 ORDER BY tdteam.TeamNo, tdteam.TeamKode, tdsales.SalesNama");
						 
	 SET @MyQuery = CONCAT("SELECT  A.SalesKode AS Kode, IFNULL(Jum.Jumlah, 0) AS Jumlah FROM 
						 ( ",@MySales," ) A 
						 LEFT OUTER JOIN
						 (SELECT ttdk.TeamKode, ttdk.SalesKode, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS Jumlah FROM tmotor
						 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 AND (ttdk.LeaseKode = 'TUNAI' OR ttdk.LeaseKode = 'KDS') 
						 GROUP BY TRIM(ttdk.TeamKode), ttdk.SalesKode) Jum 
						 ON A.TeamKode = Jum.TeamKode AND A.SalesKode = Jum.SalesKode 
						 ORDER BY A.TeamNo, A.TeamKode, A.SalesNama");
	END IF;

	/* FILL SALES CREDIT */
	IF xTipe = "cashcredit3" THEN
	 SET @MySales = CONCAT("SELECT tdteam.TeamNo, CurrentTeamSales.TeamKode, tdteam.TeamNama, CurrentTeamSales.SalesKode, tdsales.SalesNama
						 FROM
						 (SELECT ttdk.TeamKode, ttdk.SalesKode FROM  tmotor
						 INNER JOIN  ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN  ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 GROUP BY TRIM(ttdk.TeamKode), ttdk.SalesKode) CurrentTeamSales
						 INNER JOIN  tdsales ON CurrentTeamSales.SalesKode = tdsales.SalesKode
						 INNER JOIN  tdteam ON CurrentTeamSales.TeamKode = tdteam.TeamKode
						 ORDER BY tdteam.TeamNo, tdteam.TeamKode, tdsales.SalesNama");
						 
	 SET @MyQuery = CONCAT("SELECT  A.SalesKode AS Kode, IFNULL(Jum.Jumlah, 0) AS Jumlah FROM 
						 ( ",@MySales," ) A 
						 LEFT OUTER JOIN
						 (SELECT ttdk.TeamKode, ttdk.SalesKode, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS Jumlah FROM tmotor
						 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 AND ttdk.LeaseKode like '",xLeasing,"%' 
						 GROUP BY TRIM(ttdk.TeamKode), ttdk.SalesKode) Jum 
						 ON A.TeamKode = Jum.TeamKode AND A.SalesKode = Jum.SalesKode 
						 ORDER BY A.TeamNo, A.TeamKode, A.SalesNama");
	END IF;

	/* SALES POTONGAN */
	IF xTipe = "salespotong1" THEN
	 SET @MyQuery = CONCAT("SELECT CurrentTeamSales.TeamKode, tdteam.TeamNama, CurrentTeamSales.SalesKode, tdsales.SalesNama,
						 0.0 AS Total,0.0 AS Cash,0.0 AS Credit,0.0 AS JualUnit,0.0 AS CashUnit,0.0 AS CreditUnit,0.0 AS CashJum,0.0 AS CreditJum 
						 FROM
						 (SELECT ttdk.TeamKode, ttdk.SalesKode FROM  tmotor
						 INNER JOIN  ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN  ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 GROUP BY TRIM(ttdk.TeamKode), ttdk.SalesKode) CurrentTeamSales
						 INNER JOIN  tdsales ON CurrentTeamSales.SalesKode = tdsales.SalesKode
						 INNER JOIN  tdteam ON CurrentTeamSales.TeamKode = tdteam.TeamKode
						 ORDER BY tdteam.TeamNo, tdteam.TeamKode, tdsales.SalesNama");
	END IF;

	/* FILL SALES POTONGAN CREDIT */
	IF xTipe = "salespotong2" THEN
	 SET @MySales = CONCAT("SELECT tdteam.TeamNo, CurrentTeamSales.TeamKode, tdteam.TeamNama, CurrentTeamSales.SalesKode, tdsales.SalesNama
						 FROM
						 (SELECT ttdk.TeamKode, ttdk.SalesKode FROM  tmotor
						 INNER JOIN  ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN  ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 GROUP BY TRIM(ttdk.TeamKode), ttdk.SalesKode) CurrentTeamSales
						 INNER JOIN  tdsales ON CurrentTeamSales.SalesKode = tdsales.SalesKode
						 INNER JOIN  tdteam ON CurrentTeamSales.TeamKode = tdteam.TeamKode
						 ORDER BY tdteam.TeamNo, tdteam.TeamKode, tdsales.SalesNama");
						 
	 SET @MyQuery = CONCAT("SELECT A.SalesKode, IFNULL(Cash.Cash,0) as Nominal, IFNULL(Cash.Jum,0) as Count, IFNULL(Cash.Cash/ Cash.Jum,0) AS Jumlah FROM 
						 ( ",@MySales," ) A 
						 LEFT OUTER JOIN
						 (SELECT ttdk.TeamKode, ttdk.SalesKode, IFNULL(SUM(ttdk.ReturHarga) + SUM(ttdk.PotonganHarga),0) AS Cash, IFNULL(COUNT(ttdk.DKNo),1) as Jum FROM ttdk
						 INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
						 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						 WHERE (ttdk.LeaseKode <> 'TUNAI' AND ttdk.LeaseKode <> 'KDS') 
						 AND ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 GROUP BY TRIM(ttdk.TeamKode), ttdk.SalesKode) Cash 
						 ON A.TeamKode = Cash.TeamKode AND A.SalesKode = Cash.SalesKode 
						 ORDER By A.TeamNo, A.TeamKode, A.SalesNama");
	END IF;

	/* FILL SALES POTONGAN CASH */
	IF xTipe = "salespotong3" THEN
	 SET @MySales = CONCAT("SELECT tdteam.TeamNo, CurrentTeamSales.TeamKode, tdteam.TeamNama, CurrentTeamSales.SalesKode, tdsales.SalesNama
						 FROM
						 (SELECT ttdk.TeamKode, ttdk.SalesKode FROM  tmotor
						 INNER JOIN  ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN  ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 GROUP BY TRIM(ttdk.TeamKode), ttdk.SalesKode) CurrentTeamSales
						 INNER JOIN  tdsales ON CurrentTeamSales.SalesKode = tdsales.SalesKode
						 INNER JOIN  tdteam ON CurrentTeamSales.TeamKode = tdteam.TeamKode
						 ORDER BY tdteam.TeamNo, tdteam.TeamKode, tdsales.SalesNama");
						 
	 SET @MyQuery = CONCAT("SELECT A.SalesKode, IFNULL(Cash.Cash,0) as Nominal, IFNULL(Cash.Jum,0) as Count, IFNULL(Cash.Cash/ Cash.Jum,0) AS Jumlah FROM 
						 ( ",@MySales," ) A 
						 LEFT OUTER JOIN
						 (SELECT ttdk.TeamKode, ttdk.SalesKode, IFNULL(SUM(ttdk.ReturHarga) + SUM(ttdk.PotonganHarga),0) AS Cash, IFNULL(COUNT(ttdk.DKNo),1) as Jum FROM ttdk
						 INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
						 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						 WHERE (ttdk.LeaseKode = 'TUNAI' OR ttdk.LeaseKode = 'KDS') 
						 AND ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 GROUP BY TRIM(ttdk.TeamKode), ttdk.SalesKode) Cash 
						 ON A.TeamKode = Cash.TeamKode AND A.SalesKode = Cash.SalesKode 
						 ORDER By A.TeamNo, A.TeamKode, A.SalesNama");
	END IF;

	/* FILL SALES POTONGAN UNIT */
	IF xTipe = "salespotong4" THEN
	 SET @MySales = CONCAT("SELECT tdteam.TeamNo, CurrentTeamSales.TeamKode, tdteam.TeamNama, CurrentTeamSales.SalesKode, tdsales.SalesNama
						 FROM
						 (SELECT ttdk.TeamKode, ttdk.SalesKode FROM  tmotor
						 INNER JOIN  ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN  ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 GROUP BY TRIM(ttdk.TeamKode), ttdk.SalesKode) CurrentTeamSales
						 INNER JOIN  tdsales ON CurrentTeamSales.SalesKode = tdsales.SalesKode
						 INNER JOIN  tdteam ON CurrentTeamSales.TeamKode = tdteam.TeamKode
						 ORDER BY tdteam.TeamNo, tdteam.TeamKode, tdsales.SalesNama");
						 
	 SET @MyQuery = CONCAT("SELECT A.SalesKode, IFNULL(Cash.Cash,0) AS Jumlah FROM  
						 ( ",@MySales," ) A 
						 LEFT OUTER JOIN
						 (SELECT ttdk.TeamKode, ttdk.SalesKode, COUNT(ttdk.SalesKode) AS Cash
						 FROM ttdk
						 INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
						 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 GROUP BY TRIM(ttdk.TeamKode), ttdk.SalesKode) Cash 
						 ON A.TeamKode = Cash.TeamKode AND A.SalesKode = Cash.SalesKode 
						 ORDER By  A.TeamNo, A.TeamKode, A.SalesNama");
	END IF;

	/* TYPE CASH CREDIT */
	IF xTipe = "typecashcredit1" THEN
	 SET @MyQuery = CONCAT("SELECT MotorType, MotorNama, 
						 0.0 AS CashUnit,0.0 AS CashPersen,0.0 AS CreditUnit,0.0 AS CreditPersen,
						 0.0 As L01Qty,0.0 As L01MS,0.0 As L01CS,0.0 As L02Qty,0.0 As L02MS,0.0 As L02CS,
						 0.0 As L03Qty,0.0 As L03MS,0.0 As L03CS,0.0 As L04Qty,0.0 As L04MS,0.0 As L04CS,
						 0.0 As L05Qty,0.0 As L05MS,0.0 As L05CS,0.0 As L06Qty,0.0 As L06MS,0.0 As L06CS,
						 0.0 As L07Qty,0.0 As L07MS,0.0 As L07CS,0.0 As L08Qty,0.0 As L08MS,0.0 As L08CS,
						 0.0 As L09Qty,0.0 As L09MS,0.0 As L09CS,0.0 As L10Qty,0.0 As L10MS,0.0 As L10CS,tdmotortype.MotorKategori  
						 FROM tdmotortype 
						 INNER JOIN tdmotorkategori ON tdmotortype.MotorKategori = tdmotorkategori.MotorKategori ORDER BY MotorNo, MotorType");
						 
	END IF;

	/* FILL TYPE CASH */
	IF xTipe = "typecashcredit2" THEN
	 SET @MyQuery = CONCAT("SELECT tdmotortype.MotorType, IFNULL(Jumlah,0) AS Jumlah 
						 FROM tdmotortype 
						 INNER JOIN tdmotorkategori ON tdmotortype.MotorKategori = tdmotorkategori.MotorKategori 
						 LEFT OUTER JOIN
						 (SELECT tmotor.MotorType, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS Jumlah FROM tmotor
						 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 AND (ttdk.LeaseKode = 'TUNAI' OR ttdk.LeaseKode = 'KDS') 
						 GROUP BY tmotor.MotorType) Jum 
						 ON tdmotortype.MotorType = Jum.MotorType 
						 ORDER BY tdmotorkategori.MotorNo, tdmotortype.MotorType");
						 
	END IF;

	/* FILL TYPE CASH */
	IF xTipe = "typecashcredit3" THEN
	 SET @MyQuery = CONCAT("SELECT tdmotortype.MotorType, IFNULL(Jumlah,0) AS Jumlah 
						 FROM tdmotortype 
						 INNER JOIN tdmotorkategori ON tdmotortype.MotorKategori = tdmotorkategori.MotorKategori 
						 LEFT OUTER JOIN
						 (SELECT tmotor.MotorType, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS Jumlah FROM tmotor
						 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 AND ttdk.LeaseKode like '",xLeasing,"%'  
						 GROUP BY tmotor.MotorType) Jum 
						 ON tdmotortype.MotorType = Jum.MotorType 
						 ORDER BY tdmotorkategori.MotorNo, tdmotortype.MotorType");
						 
	END IF;

	/* TYPE DP UNIT*/
	IF xTipe = "typedpunit1" THEN
	 SET @MyQuery = CONCAT("SELECT MotorType, MotorNama, 
						  0.0 As L0120,0.0 As L012030,0.0 As L0130,
						  0.0 As L0220,0.0 As L022030,0.0 As L0230,
						  0.0 As L0320,0.0 As L032030,0.0 As L0330,
						  0.0 As L0420,0.0 As L042030,0.0 As L0430,
						  0.0 As L0520,0.0 As L052030,0.0 As L0530,
						  0.0 As L0620,0.0 As L062030,0.0 As L0630,
						  0.0 As L0720,0.0 As L072030,0.0 As L0730,
						  0.0 As L0820,0.0 As L082030,0.0 As L0830,
						  0.0 As L0920,0.0 As L092030,0.0 As L0930,
						  0.0 As L1020,0.0 As L102030,0.0 As L1030,tdmotortype.MotorKategori  
						 FROM tdmotortype 
						 INNER JOIN tdmotorkategori ON tdmotortype.MotorKategori = tdmotorkategori.MotorKategori ORDER BY MotorNo, MotorType");
						 
	END IF;

	/* TYPE FILL DP UNIT 1 */
	IF xTipe = "typedpunit2" THEN
	 SET @MyQuery = CONCAT("SELECT tdmotortype.MotorType, IFNULL(Jumlah,0) AS Jumlah 
						 FROM tdmotortype
						 INNER JOIN tdmotorkategori ON tdmotortype.MotorKategori = tdmotorkategori.MotorKategori 
						 LEFT OUTER JOIN
						 (SELECT tmotor.MotorType, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS Jumlah FROM tmotor
						 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 AND ttdk.LeaseKode LIKE '",xLeasing,"%' 
						 AND DKjenis = 'Kredit' 
						 AND ((DKDPTotal+ProgramSubsidi) / DKHarga) < 0.2 
						 GROUP BY tmotor.MotorType) Jum 
						 ON tdmotortype.MotorType = Jum.MotorType ORDER BY MotorNo, tdmotortype.MotorType");
						 
	END IF;

	/* TYPE FILL DP UNIT 2 */
	IF xTipe = "typedpunit3" THEN
	 SET @MyQuery = CONCAT("SELECT tdmotortype.MotorType, IFNULL(Jumlah,0) AS Jumlah 
						 FROM tdmotortype
						 INNER JOIN tdmotorkategori ON tdmotortype.MotorKategori = tdmotorkategori.MotorKategori 
						 LEFT OUTER JOIN
						 (SELECT tmotor.MotorType, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS Jumlah FROM tmotor
						 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 AND ttdk.LeaseKode LIKE '",xLeasing,"%' 
						 AND DKjenis = 'Kredit' 
						 AND (((DKDPTotal+ProgramSubsidi) / DKHarga) BETWEEN 0.2 AND 0.3) 
						 GROUP BY tmotor.MotorType) Jum 
						 ON tdmotortype.MotorType = Jum.MotorType ORDER BY MotorNo, tdmotortype.MotorType");
						 
	END IF;

	/* TYPE FILL DP UNIT 3 */
	IF xTipe = "typedpunit4" THEN
		SET @MyQuery = CONCAT("SELECT tdmotortype.MotorType, IFNULL(Jumlah,0) AS Jumlah 
						 FROM tdmotortype
						 INNER JOIN tdmotorkategori ON tdmotortype.MotorKategori = tdmotorkategori.MotorKategori 
						 LEFT OUTER JOIN
						 (SELECT tmotor.MotorType, IFNULL(COUNT(tmotor.MotorNoMesin),0) AS Jumlah FROM tmotor
						 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
						 INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
						 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 AND ttdk.LeaseKode LIKE '",xLeasing,"%' 
						 AND DKjenis = 'Kredit' 
						 AND (((DKDPTotal+ProgramSubsidi) / DKHarga) > 0.3) 
						 GROUP BY tmotor.MotorType) Jum 
						 ON tdmotortype.MotorType = Jum.MotorType ORDER BY MotorNo, tdmotortype.MotorType");					 
	END IF;

	/* SALES LEASE HARIAN */
	IF xTipe = "saleslease1" THEN
		SET @MyQuery = CONCAT("SELECT ttdk.LeaseKode, tdleasing.LeaseNama, tdsales.TeamKode, tdteam.TeamNama, tdsales.SalesKode, tdsales.SalesNama,
						 0.0 AS '01',0.0 AS '02',0.0 AS '03',0.0 AS '04',0.0 AS '05',0.0 AS '06',0.0 AS '07',0.0 AS '08',0.0 AS '09',0.0 AS '10', 
						 0.0 AS '11',0.0 AS '12',0.0 AS '13',0.0 AS '14',0.0 AS '15',0.0 AS '16',0.0 AS '17',0.0 AS '18',0.0 AS '19',0.0 AS '20', 
						 0.0 AS '21',0.0 AS '22',0.0 AS '23',0.0 AS '24',0.0 AS '25',0.0 AS '26',0.0 AS '27',0.0 AS '28',0.0 AS '29',0.0 AS '30',0.0 AS '31',
						 0.0 AS '01T',0.0 AS '02T',0.0 AS '03T',0.0 AS '04T',0.0 AS '05T',0.0 AS '06T',0.0 AS '07T',0.0 AS '08T',0.0 AS '09T',0.0 AS '10T', 
						 0.0 AS '11T',0.0 AS '12T',0.0 AS '13T',0.0 AS '14T',0.0 AS '15T',0.0 AS '16T',0.0 AS '17T',0.0 AS '18T',0.0 AS '19T',0.0 AS '20T', 
						 0.0 AS '21T',0.0 AS '22T',0.0 AS '23T',0.0 AS '24T',0.0 AS '25T',0.0 AS '26T',0.0 AS '27T',0.0 AS '28T',0.0 AS '29T',0.0 AS '30T',0.0 AS '31T'
						 FROM ttdk
						 INNER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode
						 INNER JOIN tdleasing ON ttdk.LeaseKode = tdleasing.LeaseKode
						 INNER JOIN tdteam ON ttdk.TeamKode = tdteam.TeamKode
						 INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
						 INNER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo 
						 WHERE  ttsk.sKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
						 AND ttdk.LeaseKode <> 'TUNAI' AND ttdk.LeaseKode <> 'KDS'
						 GROUP BY ttdk.LeaseKode,ttdk.TeamKode, ttdk.SalesKode 
						 ORDER BY ttdk.LeaseKode, ttdk.TeamKode, tdsales.SalesNama");					 
	END IF;

	/* FILL SALES LEASE HARIAN */
	IF xTipe = "saleslease2" THEN
		SET xTgl3 = CONCAT(SUBSTR(xLeasing,7,4),"-",SUBSTR(xLeasing,4,2),"-",LEFT(xLeasing,2));
		SET xTgl4 = CONCAT(RIGHT(xLeasing,4),"-",SUBSTR(xLeasing,14,2),"-",SUBSTR(xLeasing,11,2));
		SET @MyQuery = CONCAT("SELECT A.LeaseKode, A.TeamKode, A.SalesKode, A.SalesNama, IFNULL(B.Jumlah,0) As Jumlah
					 FROM
					 (SELECT ttdk.LeaseKode,  ttdk.TeamKode, ttdk.SalesKode, tdsales.SalesNama
					 FROM ttdk
					 INNER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode
					 INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo
					 INNER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo 
					 WHERE  ttsk.sKTgl BETWEEN '",xTgl3,"' AND '",xTgl4,"' 
					 AND ttdk.LeaseKode <> 'TUNAI' AND ttdk.LeaseKode <> 'KDS'
					 GROUP BY ttdk.LeaseKode,ttdk.TeamKode, ttdk.SalesKode ) A 
					 LEFT OUTER JOIN 
					 (SELECT ttdk.LeaseKode, ttdk.TeamKode, ttdk.SalesKode, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah 
					 FROM tmotor 
					 INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo 
					 INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
					 WHERE ttsk.SKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"' 
					 GROUP BY ttdk.LeaseKode, ttdk.TeamKode, ttdk.SalesKode) B 
					 ON A.LeaseKode = B.LeaseKode AND A.TeamKode = B.TeamKode AND A.SalesKode = B.SalesKode 
					 ORDER BY A.LeaseKode, A.TeamKode, A.SalesNama");					 
	END IF;

	IF xTipe = "Leasing Team" THEN
		DROP TEMPORARY TABLE IF EXISTS TeamSegmenLease;
		CREATE TEMPORARY TABLE IF NOT EXISTS TeamSegmenLease AS
		SELECT tdteam.TeamKode AS LeaseKode, tdteam.TeamNama AS LeaseNama, tdmotorkategori.MotorKategori
		FROM tdteam, tdmotorkategori
		GROUP BY tdteam.TeamKode, tdmotorkategori.MotorKategori
		ORDER BY tdteam.TeamKode,tdmotorkategori.MotorNo;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  TotalQty		 DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  TotalPersen   DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team01Qty     DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team01Persen  DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team02Qty     DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team02Persen  DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team03Qty     DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team03Persen  DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team04Qty     DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team04Persen  DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team05Qty     DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team05Persen  DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team06Qty     DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team06Persen  DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team07Qty     DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team07Persen  DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team08Qty     DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team08Persen  DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team09Qty     DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team09Persen  DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team10Qty     DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team10Persen  DECIMAL(11,2) DEFAULT 0;
		CREATE INDEX LeaseKode ON TeamSegmenLease(LeaseKode);
		CREATE INDEX LeaseNama ON TeamSegmenLease(LeaseNama);

		OPEN cur1;
		REPEAT FETCH cur1 INTO MyLeaseKode;
			IF ! done0 THEN
				DROP TEMPORARY TABLE IF EXISTS FillTeamSegmenLease;
				CREATE TEMPORARY TABLE IF NOT EXISTS FillTeamSegmenLease AS
				SELECT ttdk.TeamKode, tdmotortype.MotorKategori, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah
				FROM tmotor
					INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
					INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
					INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
				WHERE ttsk.SKTgl BETWEEN xTgl1 AND xTgl2
				AND ttdk.LeaseKode = MyLeaseKode
				GROUP BY ttdk.TeamKode, tdmotortype.MotorKategori;
				CREATE INDEX TeamKode ON FillTeamSegmenLease(TeamKode);
				CREATE INDEX MotorKategori ON FillTeamSegmenLease(MotorKategori);
				
				DROP TEMPORARY TABLE IF EXISTS TFill;
				CREATE TEMPORARY TABLE IF NOT EXISTS TFill AS
				SELECT TeamKode, TeamSegmenLease.MotorKategori, IFNULL(Jumlah,0) AS Jumlah FROM  TeamSegmenLease
				LEFT OUTER JOIN FillTeamSegmenLease ON
				TeamSegmenLease.LeaseKode = FillTeamSegmenLease.TeamKode AND
				TeamSegmenLease.MotorKategori = FillTeamSegmenLease.MotorKategori ;
				
				SET XINC = XINC + 1;
				SET @MyQuery = CONCAT("UPDATE TeamSegmenLease INNER JOIN TFill ON TeamSegmenLease.LeaseKode = TFill.TeamKode AND
				TeamSegmenLease.MotorKategori = TFill.MotorKategori
				SET ", ELT(XINC,'Team01Qty','Team02Qty','Team03Qty','Team04Qty','Team05Qty','Team06Qty','Team07Qty','Team08Qty','Team09Qty','Team10Qty')," = Jumlah ");
				PREPARE STMT FROM @MyQuery;
				EXECUTE Stmt;
			END IF;
		UNTIL done0 END REPEAT;
		CLOSE cur1;	
		
		UPDATE TeamSegmenLease SET TotalQty =   Team01Qty + Team02Qty + Team03Qty + Team04Qty + Team05Qty + Team06Qty + Team07Qty + Team08Qty + Team09Qty + Team10Qty;
		DROP TEMPORARY TABLE IF EXISTS SUMari;
		CREATE TEMPORARY TABLE IF NOT EXISTS SUMari AS
		SELECT  LeaseKode,
		SUM(Team01Qty) AS SUMTeam01Qty,
		SUM(Team02Qty) AS SUMTeam02Qty,
		SUM(Team03Qty) AS SUMTeam03Qty,
		SUM(Team04Qty) AS SUMTeam04Qty,
		SUM(Team05Qty) AS SUMTeam05Qty,
		SUM(Team06Qty) AS SUMTeam06Qty,
		SUM(Team07Qty) AS SUMTeam07Qty,
		SUM(Team08Qty) AS SUMTeam08Qty,
		SUM(Team09Qty) AS SUMTeam09Qty,
		SUM(Team10Qty) AS SUMTeam10Qty,
		SUM(TotalQty) AS SUMTotalQty
		FROM TeamSegmenLease GROUP BY LeaseKode;
		CREATE INDEX LeaseKode ON SUMari(LeaseKode);
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET Team01Persen = ROUND(Team01Qty / SUMTeam01Qty * 100,2) WHERE SUMTeam01Qty <> 0;
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET Team02Persen = ROUND(Team02Qty / SUMTeam02Qty * 100,2) WHERE SUMTeam02Qty <> 0;
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET Team03Persen = ROUND(Team03Qty / SUMTeam03Qty * 100,2) WHERE SUMTeam03Qty <> 0;
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET Team04Persen = ROUND(Team04Qty / SUMTeam04Qty * 100,2) WHERE SUMTeam04Qty <> 0;
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET Team05Persen = ROUND(Team05Qty / SUMTeam05Qty * 100,2) WHERE SUMTeam05Qty <> 0;
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET Team06Persen = ROUND(Team06Qty / SUMTeam06Qty * 100,2) WHERE SUMTeam06Qty <> 0;
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET Team07Persen = ROUND(Team07Qty / SUMTeam07Qty * 100,2) WHERE SUMTeam07Qty <> 0;
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET Team08Persen = ROUND(Team08Qty / SUMTeam08Qty * 100,2) WHERE SUMTeam08Qty <> 0;
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET Team09Persen = ROUND(Team09Qty / SUMTeam09Qty * 100,2) WHERE SUMTeam09Qty <> 0;
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET Team10Persen = ROUND(Team10Qty / SUMTeam10Qty * 100,2) WHERE SUMTeam10Qty <> 0;
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET TotalPersen = ROUND(TotalQty / SUMTotalQty * 100,2) WHERE SUMTotalQty <> 0;
		DELETE FROM TeamSegmenLease WHERE LeaseKode IN (SELECT LeaseKode FROM SUMari WHERE  SUMTotalQty = 0 );

		SET @MyQuery = CONCAT("SELECT * from TeamSegmenLease;");	
	END IF;

	IF xTipe = "Team Leasing" THEN
		DROP TEMPORARY TABLE IF EXISTS TeamSegmenLease;
		CREATE TEMPORARY TABLE IF NOT EXISTS TeamSegmenLease AS
		SELECT tdleasing.LeaseKode, tdleasing.LeaseNama, tdmotorkategori.MotorKategori
		FROM tdleasing, tdmotorkategori
		WHERE tdleasing.LeaseKode <> 'TUNAI' AND tdleasing.LeaseKode <> 'KDS'
		GROUP BY tdleasing.LeaseKode, tdmotorkategori.MotorKategori
		ORDER BY tdleasing.LeaseNo, tdleasing.LeaseKode, tdmotorkategori.MotorNo;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  TotalQty     DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  TotalPersen   DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team01Qty     DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team01Persen  DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team02Qty     DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team02Persen  DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team03Qty     DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team03Persen  DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team04Qty     DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team04Persen  DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team05Qty     DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team05Persen  DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team06Qty     DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team06Persen  DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team07Qty     DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team07Persen  DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team08Qty     DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team08Persen  DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team09Qty     DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team09Persen  DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team10Qty     DECIMAL(11,2) DEFAULT 0;
		ALTER IGNORE TABLE TeamSegmenLease ADD COLUMN  Team10Persen  DECIMAL(11,2) DEFAULT 0;
		CREATE INDEX LeaseKode ON TeamSegmenLease(LeaseKode);
		CREATE INDEX LeaseNama ON TeamSegmenLease(LeaseNama);
		
		OPEN cur0;
		REPEAT FETCH cur0 INTO MyTeamKode;
			IF ! done0 THEN
				DROP TEMPORARY TABLE IF EXISTS FillTeamSegmenLease;
				CREATE TEMPORARY TABLE IF NOT EXISTS FillTeamSegmenLease AS
				SELECT A.LeaseKode,A.MotorKategori, IFNULL(B.Jumlah,0) AS Jumlah FROM
				(SELECT tdleasing.LeaseNo, tdleasing.LeaseKode, tdleasing.LeaseNama, tdmotorkategori.MotorNo, tdmotorkategori.MotorKategori FROM tdleasing, tdmotorkategori
				WHERE tdleasing.LeaseKode <> 'TUNAI' AND tdleasing.LeaseKode <> 'KDS'
				GROUP BY tdleasing.LeaseKode, tdmotorkategori.MotorKategori
				ORDER BY tdleasing.LeaseKode, tdmotorkategori.MotorNo) A
				LEFT OUTER JOIN
				(SELECT ttdk.LeaseKode, tdmotortype.MotorKategori, IFNULL(COUNT(tmotor.MotorNoMesin), 0) AS Jumlah
				 FROM tmotor
					INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo
					INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo
					INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
				WHERE ttsk.SKTgl BETWEEN xTgl1 AND xTgl2
				AND ttdk.TeamKode = MyTeamKode
				GROUP BY ttdk.LeaseKode, tdmotortype.MotorKategori ) B
				ON A.LeaseKode = B.LeaseKode AND A.MotorKategori = B.MotorKategori
				ORDER BY A.LeaseNo, A.LeaseKode, A.MotorNo;

				DROP TEMPORARY TABLE IF EXISTS TFill;
				CREATE TEMPORARY TABLE IF NOT EXISTS TFill AS
				SELECT FillTeamSegmenLease.LeaseKode, FillTeamSegmenLease.MotorKategori, IFNULL(Jumlah,0) AS Jumlah FROM  TeamSegmenLease
				LEFT OUTER JOIN FillTeamSegmenLease ON
				TeamSegmenLease.LeaseKode = FillTeamSegmenLease.LeaseKode AND
				TeamSegmenLease.MotorKategori = FillTeamSegmenLease.MotorKategori ;

				SET XINC = XINC + 1;
				SET @MyQuery = CONCAT("UPDATE TeamSegmenLease INNER JOIN TFill ON TeamSegmenLease.LeaseKode = TFill.LeaseKode AND
				TeamSegmenLease.MotorKategori = TFill.MotorKategori
				SET ", ELT(XINC,'Team01Qty','Team02Qty','Team03Qty','Team04Qty','Team05Qty','Team06Qty','Team07Qty','Team08Qty','Team09Qty','Team10Qty')," = Jumlah ");
				PREPARE STMT FROM @MyQuery;
				EXECUTE Stmt;
			END IF;
		UNTIL done0 END REPEAT;
		CLOSE cur0;	

		UPDATE TeamSegmenLease SET TotalQty =   Team01Qty + Team02Qty + Team03Qty + Team04Qty + Team05Qty + Team06Qty + Team07Qty + Team08Qty + Team09Qty + Team10Qty;
		DROP TEMPORARY TABLE IF EXISTS SUMari;
		CREATE TEMPORARY TABLE IF NOT EXISTS SUMari AS
		SELECT  LeaseKode,
		SUM(Team01Qty) AS SUMTeam01Qty,
		SUM(Team02Qty) AS SUMTeam02Qty,
		SUM(Team03Qty) AS SUMTeam03Qty,
		SUM(Team04Qty) AS SUMTeam04Qty,
		SUM(Team05Qty) AS SUMTeam05Qty,
		SUM(Team06Qty) AS SUMTeam06Qty,
		SUM(Team07Qty) AS SUMTeam07Qty,
		SUM(Team08Qty) AS SUMTeam08Qty,
		SUM(Team09Qty) AS SUMTeam09Qty,
		SUM(Team10Qty) AS SUMTeam10Qty,
		SUM(TotalQty) AS SUMTotalQty
		FROM TeamSegmenLease GROUP BY LeaseKode;
		CREATE INDEX LeaseKode ON SUMari(LeaseKode);
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET Team01Persen = ROUND(Team01Qty / SUMTeam01Qty * 100,2) WHERE SUMTeam01Qty <> 0;
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET Team02Persen = ROUND(Team02Qty / SUMTeam02Qty * 100,2) WHERE SUMTeam02Qty <> 0;
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET Team03Persen = ROUND(Team03Qty / SUMTeam03Qty * 100,2) WHERE SUMTeam03Qty <> 0;
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET Team04Persen = ROUND(Team04Qty / SUMTeam04Qty * 100,2) WHERE SUMTeam04Qty <> 0;
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET Team05Persen = ROUND(Team05Qty / SUMTeam05Qty * 100,2) WHERE SUMTeam05Qty <> 0;
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET Team06Persen = ROUND(Team06Qty / SUMTeam06Qty * 100,2) WHERE SUMTeam06Qty <> 0;
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET Team07Persen = ROUND(Team07Qty / SUMTeam07Qty * 100,2) WHERE SUMTeam07Qty <> 0;
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET Team08Persen = ROUND(Team08Qty / SUMTeam08Qty * 100,2) WHERE SUMTeam08Qty <> 0;
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET Team09Persen = ROUND(Team09Qty / SUMTeam09Qty * 100,2) WHERE SUMTeam09Qty <> 0;
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET Team10Persen = ROUND(Team10Qty / SUMTeam10Qty * 100,2) WHERE SUMTeam10Qty <> 0;
		UPDATE TeamSegmenLease INNER JOIN SUMari ON SUMari.LeaseKode = TeamSegmenLease.LeaseKode SET TotalPersen = ROUND(TotalQty / SUMTotalQty * 100,2) WHERE SUMTotalQty <> 0;
		DELETE FROM TeamSegmenLease WHERE LeaseKode IN (SELECT LeaseKode FROM SUMari WHERE  SUMTotalQty = 0 );


		SET @MyQuery = CONCAT("SELECT * from TeamSegmenLease;");	
	END IF;

	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$
DELIMITER ;

