DELIMITER $$
DROP PROCEDURE IF EXISTS `rMutasiSDealer`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rMutasiSDealer`(IN xTipe VARCHAR(25),IN xTgl1 DATE,IN xTgl2 DATE,IN xLokasi VARCHAR(30),IN xDealer VARCHAR(20),IN xSort VARCHAR(25),IN xOrder VARCHAR(10))
BEGIN
/*
       xTipe    : "ttsd" -> Header
                  "tmotor" -> Detail
                  
       xSort    : "ttsd.SDTgl"
                  "ttsd.SDNo"
                  "ttsd.LokasiKode"
                  "ttsd.DealerKode"
                  "tdlokasi.LokasiNama"
                  "tdDealer.DealerNama"
                  "ttsd.SDMemo"
                  "ttsd.UserID"
                  "IFNULL(ttgeneralledgerhd.GLLink,'--')"
                  "ttsd.SDTotal"

       Cara Akses :
       
       *** HEADER ***
       CALL rMutasiSDealer('ttsd','2001-01-01','2019-12-31','','','ttsd.SDTgl','ASC');

       *** DETAIL ***
       CALL rMutasiSDealer('tmotor','2001-01-01','2019-12-31','','','ttsd.SDTgl','ASC');

*/

       SET xLokasi = CONCAT(xLokasi,"%");
       SET xDealer = CONCAT(xDealer,"%");
       
       /* HEADER */
       IF xTipe = "ttsd" THEN 
          SET @MyQuery = "SELECT ttsd.SDNo, ttsd.SDTgl, ttsd.LokasiKode, ttsd.DealerKode, ttsd.SDMemo, ttsd.SDTotal, 
                          ttsd.UserID, tdlokasi.LokasiNama, tddealer.DealerNama, ttsd.SDJam 
                          FROM ttsd 
                          INNER JOIN tdlokasi ON ttsd.LokasiKode = tdlokasi.LokasiKode 
                          INNER JOIN tddealer ON ttsd.DealerKode = tddealer.DealerKode 
                          WHERE ";
                          
          SET @MyQuery = CONCAT(@MyQuery,"(ttsd.SDTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttsd.LokasiKode LIKE '",xLokasi,"') AND 
                         (ttsd.DealerKode LIKE '",xDealer,"') ORDER BY ",xSort," ",xOrder); 
       END IF;


       /* DETAIL */
       IF xTipe = "tmotor" THEN 
          SET @MyQuery = "SELECT BPKBNo, BPKBTglAmbil, BPKBTglJadi, DKNo, ttsd.DealerKode, FBHarga, FBNo, FakturAHMNo, 
                          FakturAHMTgl, FakturAHMTglAmbil, MotorAutoN, MotorMemo, MotorNoMesin, MotorNoRangka, MotorTahun, MotorType, 
                          MotorWarna, PDNo, PlatNo, PlatTgl, PlatTglAmbil, ttsd.SDNo, ttsd.SDTgl, SKNo, SSNo, STNKAlamat, STNKNama, STNKNo, 
                          STNKTglAmbil, STNKTglBayar, STNKTglJadi, STNKTglMasuk 
                          FROM ttsd 
                          INNER JOIN tmotor ON ttsd.SDNo = tmotor.SDNo   
                          WHERE ";
                          
          SET @MyQuery = CONCAT(@MyQuery,"(ttsd.SDTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttsd.LokasiKode LIKE '",xLokasi,"') AND 
                         (ttsd.DealerKode LIKE '",xDealer,"') ORDER BY ",xSort," ",xOrder); 
       END IF;

       PREPARE STMT FROM @MyQuery;
       EXECUTE Stmt;
    
END$$
DELIMITER ;

