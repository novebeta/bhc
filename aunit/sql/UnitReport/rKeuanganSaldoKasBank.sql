DELIMITER $$
DROP PROCEDURE IF EXISTS `rKeuanganSaldoKasBank`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rKeuanganSaldoKasBank`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,IN xLokasi VARCHAR(30),IN xBank VARCHAR(30),IN xJurnal NUMERIC)
BEGIN
	DECLARE Tanggal DATE;

	DECLARE MyDateAwalSaldo DATE;
	DECLARE xTglMin DATE;
	DECLARE xSaldoAwal,xSaldoAkhirKas,xSaldoAkhirBank,xSaldoAkhirKasBank,xKasKeluar DOUBLE;
	SET xTglMin = SUBDATE(xTgl1,1);
	SET xSaldoAwal = 0;
	SET xSaldoAkhirKas = 0;
	SET xSaldoAkhirBank = 0;
	SET xSaldoAkhirKasBank = 0;
	SET xKasKeluar = 0;
	
	/* SALDO KAS vtkassaldo */
	IF xTipe = 'Saldo Kas' THEN
		/* SALDO KAS AWAL */
		SET MyDateAwalSaldo = '1900-01-01';
		SELECT IFNULL(MAX(SaldoKasTgl), STR_TO_DATE('01/01/1900', '%m/%d/%Y')) INTO MyDateAwalSaldo FROM tsaldokas WHERE SaldoKasTgl <= xTglMin AND LokasiKode LIKE CONCAT('%',xLokasi);
		SET @TanggalSaldo = MyDateAwalSaldo;
		SET @TanggalAwal = ADDDATE(@TanggalSaldo,1);
		SET @TanggalAkhir = xTglMin;
		SELECT IFNULL(SUM(SaldoKasSaldo),0) INTO xSaldoAwal FROM tsaldokas WHERE  SaldoKasTgl = MyDateAwalSaldo AND LokasiKode LIKE CONCAT("%",xLokasi);
		SET @MyQuery = CONCAT("SELECT IFNULL(SUM(KMNominal),0) INTO @KasMasuk FROM ttkm WHERE KMTgl BETWEEN '",@TanggalAwal,"' AND '",@TanggalAkhir,"' AND LokasiKode LIKE '%",xLokasi,"' ");
		IF xJurnal = 1 THEN
			SET @MyQuery = CONCAT(@MyQuery," AND ( ttkm.NoGL <> '--' )");
		ELSE
			SET @MyQuery = CONCAT(@MyQuery," AND ( ttkm.NoGL = '--' )");
		END IF;
		PREPARE STMT FROM @MyQuery;  EXECUTE Stmt;
		SET @MyQuery = CONCAT("SELECT IFNULL(SUM(KKNominal),0) INTO @KasKeluar FROM ttkk WHERE  KKTgl BETWEEN '",@TanggalAwal,"' AND '",@TanggalAkhir,"' AND LokasiKode LIKE '%",xLokasi,"' ");
		IF xJurnal = 1 THEN
			SET @MyQuery = CONCAT(@MyQuery," AND ( ttkk.NoGL <> '--' )");
		ELSE
			SET @MyQuery = CONCAT(@MyQuery," AND ( ttkk.NoGL = '--' )");
		END IF;
		PREPARE STMT FROM @MyQuery;  EXECUTE Stmt;
		SET xSaldoAkhirKas = xSaldoAwal + @KasMasuk - @KasKeluar;

		DROP TEMPORARY TABLE IF EXISTS `vtkassaldo`;
		IF xJurnal = 1 THEN
			CREATE TEMPORARY TABLE IF NOT EXISTS `vtkassaldo` AS
			SELECT '--' AS KasNo, xTglMin AS KasTgl, xTglMin AS KasJam, 'Saldo Awal' AS KasMemo,
			0 AS KasDebet, 0 AS KasKredit,'Saldo Awal' AS KasJenis,'System' AS KasPerson,
			'--' AS KasLink,
			'--' AS UserID, xLokasi AS LokasiKode,'--' AS NoGL, xSaldoAkhirKas AS KasSaldo
			UNION
			SELECT KMNo AS KasNo, KMTgl AS KasTgl, KMJam AS KasJam, KMMemo AS KasMemo, KMNominal AS KasDebet, 0 AS KasKredit, KMJenis AS KasJenis, KMPerson AS KasPerson, KMLink AS KasLink, UserID, LokasiKode, NoGL, 0 AS KasSaldo FROM ttkm
			WHERE (LEFT(KMNo,2) <> 'KP') AND (KMTgl BETWEEN xTgl1 AND xTgl2) AND ( NoGL <> '--' ) AND LokasiKode LIKE CONCAT('%',xLokasi)
			UNION
			SELECT KKNo AS KasNo, KKTgl AS KasTgl, KKJam AS KasJam, KKMemo AS KasMemo, 0 AS KasDebet, KKNominal AS KasKredit, KKJenis AS KasJenis, KKPerson AS KasPerson, KKLink AS KasLink, UserID, LokasiKode, NoGL, 0 AS KasSaldo FROM ttkk
			WHERE (LEFT(KKNo,2) <> 'KP') AND (KKTgl BETWEEN xTgl1 AND xTgl2) AND ( NoGL <> '--' ) AND LokasiKode LIKE CONCAT('%',xLokasi)
			ORDER BY KasTgl, KasJam,  KasNo;
		ELSE
			CREATE TEMPORARY TABLE IF NOT EXISTS `vtkassaldo` AS
			SELECT '--' AS KasNo, xTglMin AS KasTgl, xTglMin AS KasJam, 'Saldo Awal' AS KasMemo,
			0 AS KasDebet, 0 AS KasKredit,'Saldo Awal' AS KasJenis,'System' AS KasPerson,
			'--' AS KasLink,
			'--' AS UserID, xLokasi AS LokasiKode,'--' AS NoGL, xSaldoAkhirKas AS KasSaldo
			UNION
			SELECT KMNo AS KasNo, KMTgl AS KasTgl, KMJam AS KasJam, KMMemo AS KasMemo, KMNominal AS KasDebet, 0 AS KasKredit, KMJenis AS KasJenis, KMPerson AS KasPerson, KMLink AS KasLink, UserID, LokasiKode, NoGL, 0 AS KasSaldo FROM ttkm
			WHERE (LEFT(KMNo,2) <> 'KP')
			AND (KMTgl BETWEEN xTgl1 AND xTgl2)
			AND LokasiKode LIKE CONCAT('%',xLokasi)
			UNION
			SELECT KKNo AS KasNo, KKTgl AS KasTgl, KKJam AS KasJam, KKMemo AS KasMemo, 0 AS KasDebet, KKNominal AS KasKredit, KKJenis AS KasJenis, KKPerson AS KasPerson, KKLink AS KasLink, UserID, LokasiKode, NoGL, 0 AS KasSaldo FROM ttkk
			WHERE (LEFT(KKNo,2) <> 'KP')
			AND (KKTgl BETWEEN xTgl1 AND xTgl2)
			AND LokasiKode LIKE CONCAT('%',xLokasi)
			ORDER BY KasTgl, KasJam,  KasNo;
		END IF;
		SET @MyQuery = CONCAT("SELECT * FROM vtkassaldo ORDER BY KasTgl, KasJam, KasNo;");
	END IF;
	
	IF xTipe = 'Saldo Bank' THEN
		SET MyDateAwalSaldo = '1900-01-01';
		SELECT IFNULL(MAX(SaldoBankTgl), STR_TO_DATE('01/01/1900', '%m/%d/%Y')) INTO MyDateAwalSaldo FROM tsaldobank WHERE SaldoBankTgl <= xTglMin AND NoAccount LIKE CONCAT('%',xBank);
		SET @TanggalSaldo = MyDateAwalSaldo;
		SET @TanggalAwal = ADDDATE(@TanggalSaldo,1);
		SET @TanggalAkhir = xTglMin;
		SELECT IFNULL(SUM(SaldoBankSaldo),0) INTO xSaldoAwal FROM tsaldobank WHERE  SaldoBankTgl = MyDateAwalSaldo AND NoAccount LIKE CONCAT("%",xBank);

		SET @MyQuery = CONCAT("SELECT IFNULL(SUM(BMNominal),0) INTO @BankMasuk FROM ttBMhd WHERE BMTgl BETWEEN '",@TanggalAwal,"' AND '",@TanggalAkhir,"' AND NoAccount LIKE '%",xBank,"' ");
		IF xJurnal = 1 THEN
			SET @MyQuery = CONCAT(@MyQuery," AND ( ttBMhd.NoGL <> '--' )");
		ELSE
			SET @MyQuery = CONCAT(@MyQuery," AND ( ttBMhd.NoGL = '--' )");
		END IF;
		PREPARE STMT FROM @MyQuery;  EXECUTE Stmt;
		SET @MyQuery = CONCAT("SELECT IFNULL(SUM(BKNominal),0) INTO @BankKeluar FROM ttBKhd WHERE  BKTgl BETWEEN '",@TanggalAwal,"' AND '",@TanggalAkhir,"' AND NoAccount LIKE '%",xBank,"' ");
		IF xJurnal = 1 THEN
			SET @MyQuery = CONCAT(@MyQuery," AND ( ttBKhd.NoGL <> '--' )");
		ELSE
			SET @MyQuery = CONCAT(@MyQuery," AND ( ttBKhd.NoGL = '--' )");
		END IF;
		PREPARE STMT FROM @MyQuery;  EXECUTE Stmt;
		SET xSaldoAkhirBank = xSaldoAwal + @BankMasuk - @BankKeluar;
		
		DROP TEMPORARY TABLE IF EXISTS `vtbanksaldo`;
		IF xJurnal = 1 THEN
			CREATE TEMPORARY TABLE IF NOT EXISTS `vtbanksaldo` AS
			SELECT '--' AS BankNo, xTglMin AS BankTgl, xTglMin AS BankJam, 'Saldo Awal' AS BankMemo, 0 AS BankDebet, 0 AS BankKredit,'Saldo Awal' AS BankJenis,
			'' AS BankSupLease, '' AS BankCekNo, '' AS BankCekTempo, '' AS BankPerson, '' AS BankAC,
			xBank AS NoAccount, '--' AS UserID, '--' AS NoGL, xSaldoAkhirBank AS BankSaldo
			UNION
			SELECT BKNo AS BankNo, BKTgl AS BankTgl, BKJam AS BankJam, BKMemo AS BankMemo, 0 AS BankDebet, BKNominal AS BankKredit, BKJenis AS BankJenis,
			SupKode AS BankSupLease, BKCekNo AS BankCekNo, BKCekTempo  AS BankCekTempo, BKPerson AS BankPerson, BKAC  AS BankAC, NoAccount, UserID, NoGL , 0 AS BankSaldo FROM ttbkhd
			WHERE (LEFT(BKNo,2) <> 'BP') AND (BKTgl BETWEEN xTgl1 AND xTgl2) AND ( NoGL <> '--' ) AND NoAccount LIKE CONCAT('%',xBank)
			UNION
			SELECT BMNo AS BankNo, BMTgl AS BankTgl, BMJam AS BankJam, BMMemo AS BankMemo, BMNominal AS BankDebet, 0 AS BankKredit, BMJenis AS BankJenis,
			LeaseKode AS BankSupLease, BMCekNo AS BankCekNo, BMCekTempo AS BankCekTempo, BMPerson AS BankPerson, BMAC AS BankAC, NoAccount, UserID, NoGL , 0 AS BankSaldo  FROM ttbmhd
			WHERE (LEFT(BMNo,2) <> 'BP') AND (BMTgl BETWEEN xTgl1 AND xTgl2) AND ( NoGL <> '--' )AND NoAccount LIKE CONCAT('%',xBank)
			ORDER BY BankTgl, BankJam, BankNo;
		ELSE
			CREATE TEMPORARY TABLE IF NOT EXISTS `vtbanksaldo` AS
			SELECT '--' AS BankNo, xTglMin AS BankTgl, xTglMin AS BankJam, 'Saldo Awal' AS BankMemo, 0 AS BankDebet, 0 AS BankKredit,'Saldo Awal' AS BankJenis,
			'' AS BankSupLease, '' AS BankCekNo, '' AS BankCekTempo, '' AS BankPerson, '' AS BankAC,
			xBank AS NoAccount, '--' AS UserID, '--' AS NoGL, xSaldoAkhirBank AS BankSaldo
			UNION
			SELECT BKNo AS BankNo, BKTgl AS BankTgl, BKJam AS BankJam, BKMemo AS BankMemo, 0 AS BankDebet, BKNominal AS BankKredit, BKJenis AS BankJenis,
			SupKode AS BankSupLease, BKCekNo AS BankCekNo, BKCekTempo  AS BankCekTempo, BKPerson AS BankPerson, BKAC  AS BankAC, NoAccount, UserID, NoGL , 0 AS BankSaldo FROM ttbkhd
			WHERE (LEFT(BKNo,2) <> 'BP')
			AND (BKTgl BETWEEN xTgl1 AND xTgl2)
			AND NoAccount LIKE CONCAT('%',xBank)
			UNION
			SELECT BMNo AS BankNo, BMTgl AS BankTgl, BMJam AS BankJam, BMMemo AS BankMemo, BMNominal AS BankDebet, 0 AS BankKredit, BMJenis AS BankJenis,
			LeaseKode AS BankSupLease, BMCekNo AS BankCekNo, BMCekTempo AS BankCekTempo, BMPerson AS BankPerson, BMAC AS BankAC, NoAccount, UserID, NoGL , 0 AS BankSaldo  FROM ttbmhd
			WHERE (LEFT(BMNo,2) <> 'BP')
			AND (BMTgl BETWEEN xTgl1 AND xTgl2)
			AND NoAccount LIKE CONCAT('%',xBank)
			ORDER BY BankTgl, BankJam, BankNo;
		END IF;
		SET @MyQuery = CONCAT("SELECT * from vtbanksaldo ORDER BY BankTgl, BankJam, BankNo;");
	END IF;

	IF xTipe = 'Saldo Kas Bank' THEN
		SET MyDateAwalSaldo = '1900-01-01';
		SELECT IFNULL(MAX(SaldoKasTgl), STR_TO_DATE('01/01/1900', '%m/%d/%Y')) INTO MyDateAwalSaldo FROM tsaldokas WHERE SaldoKasTgl <= xTglMin AND LokasiKode LIKE CONCAT('%',xLokasi);
		SET @TanggalSaldo = MyDateAwalSaldo;
		SET @TanggalAwal = ADDDATE(@TanggalSaldo,1);
		SET @TanggalAkhir = xTglMin;
		SELECT IFNULL(SUM(SaldoKasSaldo),0) INTO xSaldoAwal FROM tsaldokas WHERE  SaldoKasTgl = MyDateAwalSaldo AND LokasiKode LIKE CONCAT("%",xLokasi);
		SET @MyQuery = CONCAT("SELECT IFNULL(SUM(KMNominal),0) INTO @KasMasuk FROM ttkm WHERE KMTgl BETWEEN '",@TanggalAwal,"' AND '",@TanggalAkhir,"' AND LokasiKode LIKE '%",xLokasi,"' ");
		IF xJurnal = 1 THEN
			SET @MyQuery = CONCAT(@MyQuery," AND ( ttkm.NoGL <> '--' )");
		ELSE
			SET @MyQuery = CONCAT(@MyQuery," AND ( ttkm.NoGL = '--' )");
		END IF;
		PREPARE STMT FROM @MyQuery;  EXECUTE Stmt;
		SET @MyQuery = CONCAT("SELECT IFNULL(SUM(KKNominal),0) INTO @KasKeluar FROM ttkk WHERE  KKTgl BETWEEN '",@TanggalAwal,"' AND '",@TanggalAkhir,"' AND LokasiKode LIKE '%",xLokasi,"' ");
		IF xJurnal = 1 THEN
			SET @MyQuery = CONCAT(@MyQuery," AND ( ttkk.NoGL <> '--' )");
		ELSE
			SET @MyQuery = CONCAT(@MyQuery," AND ( ttkk.NoGL = '--' )");
		END IF;
		PREPARE STMT FROM @MyQuery;  EXECUTE Stmt;
		SET xSaldoAkhirKas = xSaldoAwal + @KasMasuk - @KasKeluar;

		SET MyDateAwalSaldo = '1900-01-01';
		SELECT IFNULL(MAX(SaldoBankTgl), STR_TO_DATE('01/01/1900', '%m/%d/%Y')) INTO MyDateAwalSaldo FROM tsaldobank WHERE SaldoBankTgl <= xTglMin AND NoAccount LIKE CONCAT('%',xBank);
		SET @TanggalSaldo = MyDateAwalSaldo;
		SET @TanggalAwal = ADDDATE(@TanggalSaldo,1);
		SET @TanggalAkhir = xTglMin;
		SELECT IFNULL(SUM(SaldoBankSaldo),0) INTO xSaldoAwal FROM tsaldobank WHERE  SaldoBankTgl = MyDateAwalSaldo AND NoAccount LIKE CONCAT("%",xBank);

		SET @MyQuery = CONCAT("SELECT IFNULL(SUM(BMNominal),0) INTO @BankMasuk FROM ttBMhd WHERE BMTgl BETWEEN '",@TanggalAwal,"' AND '",@TanggalAkhir,"' AND NoAccount LIKE '%",xBank,"' ");
		IF xJurnal = 1 THEN
			SET @MyQuery = CONCAT(@MyQuery," AND ( ttBMhd.NoGL <> '--' )");
		ELSE
			SET @MyQuery = CONCAT(@MyQuery," AND ( ttBMhd.NoGL = '--' )");
		END IF;
		PREPARE STMT FROM @MyQuery;  EXECUTE Stmt;
		SET @MyQuery = CONCAT("SELECT IFNULL(SUM(BKNominal),0) INTO @BankKeluar FROM ttBKhd WHERE  BKTgl BETWEEN '",@TanggalAwal,"' AND '",@TanggalAkhir,"' AND NoAccount LIKE '%",xBank,"' ");
		IF xJurnal = 1 THEN
			SET @MyQuery = CONCAT(@MyQuery," AND ( ttBKhd.NoGL <> '--' )");
		ELSE
			SET @MyQuery = CONCAT(@MyQuery," AND ( ttBKhd.NoGL = '--' )");
		END IF;
		PREPARE STMT FROM @MyQuery;  EXECUTE Stmt;
		SET xSaldoAkhirBank = xSaldoAwal + @BankMasuk - @BankKeluar;

		DROP TEMPORARY TABLE IF EXISTS vtKBsaldo;
		CREATE TEMPORARY TABLE IF NOT EXISTS `vtKBsaldo` AS
		SELECT 'Saldo Awal' AS KBNo, xTglMin AS Tgl, xTglMin AS Jam, 'Saldo Awal' AS Memo, 0 AS Debet, 0 AS Kredit, '' AS Jenis, '' AS Person, xSaldoAkhirBank+xSaldoAkhirKas AS Saldo,
		'' AS UserID, '--' AS Link, '' AS Kode, '' AS BankAC, '' AS NoAccount, '' AS BankCekNo, STR_TO_DATE('01/01/1900', '%m/%d/%Y') AS BankCekTempo, '' AS NoGL
		UNION
		SELECT BKNo AS KBNo, BKTgl AS Tgl, BKJam AS Jam, BKMemo AS Memo, 0 AS Debet, BKNominal AS Kredit, BKJenis AS Jenis, BKPerson AS Person, 000000000000000000.00 AS Saldo,
		UserID, '--' AS Link, SupKode AS Kode, BKAC  AS BankAC, NoAccount,BKCekNo AS BankCekNo, BKCekTempo  AS BankCekTempo, NoGL FROM ttbkhd
		WHERE (LEFT(BKNo,2) <> 'BP') AND (BKTgl BETWEEN xTgl1 AND xTgl2) AND ( NoGL <> '--' ) AND NoAccount LIKE CONCAT('%',xBank)
		UNION
		SELECT BMNo AS KBNo, BMTgl AS Tgl, BMJam AS Jam, BMMemo AS Memo, BMNominal AS Debet, 0 AS Kredit, BMJenis AS Jenis, BMPerson AS Person, 000000000000000000.00  AS Saldo,
		UserID, '--' AS Link, LeaseKode AS Kode, BMAC AS BankAC, NoAccount, BMCekNo AS BankCekNo, BMCekTempo AS BankCekTempo, NoGL FROM ttbmhd
		WHERE (LEFT(BMNo,2) <> 'BP') AND (BMTgl BETWEEN xTgl1 AND xTgl2) AND ( NoGL <> '--' )AND NoAccount LIKE CONCAT('%',xBank)
		UNION
		SELECT KMNo AS KBNo, KMTgl AS Tgl, KMJam AS Jam, KMMemo AS Memo, KMNominal AS Debet, 0 AS Kredit, KMJenis AS Jenis, KMPerson AS Person, 000000000000000000.00  AS Saldo,
		UserID, KMLink AS Link, LokasiKode AS Kode, '--' AS BankAC, '--' AS NoAccount, '--' AS BankCekNo, STR_TO_DATE('01/01/1900', '%m/%d/%Y') AS BankCekTempo, NoGL  FROM ttkm
		WHERE (LEFT(KMNo,2) <> 'KP') AND (KMTgl BETWEEN xTgl1 AND xTgl2) AND ( NoGL <> '--' ) AND LokasiKode LIKE CONCAT('%',xLokasi)
		UNION
		SELECT KKNo AS KBNo, KKTgl AS Tgl, KKJam AS Jam, KKMemo AS Memo, 0 AS Debet, KKNominal AS Kredit, KKJenis AS Jenis, KKPerson AS Person, 000000000000000000.00  AS Saldo,
		UserID, KKLink AS Link, LokasiKode AS Kode, '--' AS BankAC, '--' AS NoAccount, '--' AS BankCekNo, STR_TO_DATE('01/01/1900', '%m/%d/%Y') AS BankCekTempo, NoGL  FROM ttkk
		WHERE (LEFT(KKNo,2) <> 'KP') AND (KKTgl BETWEEN xTgl1 AND xTgl2) AND ( NoGL <> '--' ) AND LokasiKode LIKE CONCAT('%',xLokasi)
		ORDER BY Tgl, Jam, KBNo;

		SET @MyQuery = CONCAT("SELECT * from vtKBsaldo ORDER BY Tgl, Jam, KBNo ;");
	END IF;
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$

DELIMITER ;

