DELIMITER $$

DROP PROCEDURE IF EXISTS `rGLJurnalUmum`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rGLJurnalUmum`(IN xTipe VARCHAR(20),IN TglAwal DATE,IN TglAkhir DATE,
IN xPerkiraan1 VARCHAR(10),IN xPerkiraan2 VARCHAR(10), XTrans1 VARCHAR(10), XTrans2 VARCHAR(10), 
IN xSort VARCHAR(150),IN xValidasi VARCHAR(10))
BEGIN
/*
		CALL rGLJurnalUmum('Jurnal Umum','2020-03-01','2020-03-31','%','%','01SS','14JA','Jam','');
*/


	DECLARE MyTglGL  DATE;
	DECLARE MyNoGL  VARCHAR(10);

	DECLARE MyNoAccount  VARCHAR(10);
	DECLARE MyNamaAccount  VARCHAR(150);
	DECLARE MyKeteranganGL  VARCHAR(300);
	DECLARE MyDebetGL  DECIMAL(14,2);
	DECLARE MyKreditGL  DECIMAL(14,2);
	DECLARE MyKodeTrans  VARCHAR(10);
	DECLARE MySaldo  DECIMAL(14,2);
	DECLARE MySaldo1  DECIMAL(14,2);
	DECLARE MyGLLink  VARCHAR(20);
	DECLARE MyNoAccountSebelum  VARCHAR(10);
	DECLARE MyOpeningBalance DECIMAL(14,2) DEFAULT 0;
	DECLARE done INT DEFAULT FALSE;
	DECLARE maxdone INT DEFAULT 0;

	DECLARE Hasil VARCHAR(200);
	DECLARE MyJenis VARCHAR(4);
	DECLARE A INT;
	DECLARE B INT;

	DECLARE cur1 CURSOR FOR SELECT TglGL, NoGL ,NoAccount,NamaAccount,KeteranganGL,DebetGL,KreditGL,KodeTrans,Saldo,Saldo1,GLLink FROM AktivitasGLT;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

	SET A = SUBSTRING(XTrans1, 1, 2);
	SET B = SUBSTRING(XTrans2, 1, 2);
	SET Hasil = '';
	loop_label:  LOOP
		IF  A <= B THEN
			SELECT ELT(A,
			"'SS'",
			"'FB'",
			"'SK'",
			"'RK'",
			"'IP'",
			"'IE'",
			"'KM'",
			"'KK'",
			"'BM'",
			"'BK'",
			"'GL'",
			"'AM'",
			"'JP'",
			"'JA'"
			) INTO MyJenis;

			IF A <= B - 1 THEN
				SET Hasil = CONCAT(Hasil,MyJenis,",");
			ELSE
				SET Hasil = CONCAT(Hasil,MyJenis,"");
			END IF;
			SET  A = A + 1;
			ITERATE  loop_label;
		ELSE
			LEAVE  loop_label;
		END  IF;
	END LOOP;
	SET Hasil = CONCAT("(",Hasil,")");
 
  
	IF xTipe = "Jurnal Umum" THEN
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT("
			SELECT TTGeneralLedgerIt.NoGL, TTGeneralLedgerIt.NoAccount, TTGeneralLedgerIt.KeteranganGL, 
			TTGeneralLedgerIt.DebetGL, TTGeneralLedgerIt.KreditGL, TRAccount.NamaAccount, TTGeneralLedgerIt.TglGL, GLLink 
			FROM  TTGeneralLedgerIt 
			INNER JOIN TRAccount ON TTGeneralLedgerIt.NoAccount = TRAccount.NoAccount 
			INNER JOIN TTGeneralLedgerHd ON TTGeneralLedgerIt.NoGL = TTGeneralLedgerHd.NoGL AND TTGeneralLedgerIt.TglGL = TTGeneralLedgerHd.TglGL 
			INNER JOIN 
				(SELECT TTGeneralLedgerHd.NoGL FROM TTGeneralLedgerHd 
					INNER JOIN TTGeneralLedgerIt ON TTGeneralLedgerHd.NoGL = TTGeneralLedgerIt.NoGL AND TTGeneralLedgerIt.TglGL = TTGeneralLedgerHd.TglGL 
					WHERE (TTGeneralLedgerIt.TglGL BETWEEN '",TglAwal,"' AND '",TglAkhir,"') AND (TTGeneralLedgerIt.NoAccount LIKE '%') GROUP BY TTGeneralLedgerHd.NoGL) MyFilter 
			ON MyFilter.NoGL = TTGeneralLedgerHd.NoGL 
			WHERE (TTGeneralLedgerIt.TglGL BETWEEN '",TglAwal,"' AND '",TglAkhir,"') ");
			SET @MyQuery = CONCAT(@MyQuery," AND TTGeneralLedgerHd.KodeTrans IN " , Hasil , " ");
			SET @MyQuery = CONCAT(@MyQuery," AND TTGeneralLedgerHd.GLValid Like ('%" , xValidasi , "') ");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY TTGeneralLedgerHd.TglGL , TTGeneralLedgerIt.NoGL, TTGeneralLedgerIt.GLAutoN ");
		ELSE
			SET @MyQuery = CONCAT("
			SELECT TTGeneralLedgerIt.NoGL, TTGeneralLedgerIt.NoAccount, TTGeneralLedgerIt.KeteranganGL, 
			TTGeneralLedgerIt.DebetGL, TTGeneralLedgerIt.KreditGL, TRAccount.NamaAccount, TTGeneralLedgerIt.TglGL, GLLink 
			FROM  TTGeneralLedgerIt 
			INNER JOIN TRAccount ON TTGeneralLedgerIt.NoAccount = TRAccount.NoAccount 
			INNER JOIN TTGeneralLedgerHd ON TTGeneralLedgerIt.NoGL = TTGeneralLedgerHd.NoGL AND TTGeneralLedgerIt.TglGL = TTGeneralLedgerHd.TglGL 
			INNER JOIN 
				(SELECT TTGeneralLedgerHd.NoGL FROM TTGeneralLedgerHd 
					INNER JOIN TTGeneralLedgerIt ON TTGeneralLedgerHd.NoGL = TTGeneralLedgerIt.NoGL AND TTGeneralLedgerIt.TglGL = TTGeneralLedgerHd.TglGL 
					WHERE (TTGeneralLedgerIt.TglGL BETWEEN '",TglAwal,"' AND '",TglAkhir,"') 
					AND  (TTGeneralLedgerIt.NoAccount BETWEEN '" , xPerkiraan1 , "' AND '", xPerkiraan2, "') GROUP BY TTGeneralLedgerHd.NoGL) MyFilter 
			ON MyFilter.NoGL = TTGeneralLedgerHd.NoGL 
			WHERE (TTGeneralLedgerIt.TglGL BETWEEN '",TglAwal,"' AND '",TglAkhir,"') ");
			SET @MyQuery = CONCAT(@MyQuery," AND TTGeneralLedgerHd.KodeTrans IN " , Hasil , " ");
			SET @MyQuery = CONCAT(@MyQuery," AND TTGeneralLedgerHd.GLValid Like    ('%" , xValidasi , "')) ");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY TTGeneralLedgerHd.TglGL , TTGeneralLedgerIt.NoGL, TTGeneralLedgerIt.GLAutoN ");	
		END IF;
 	END IF;
	
	IF xTipe = "Jurnal Perkiraan" THEN
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT("
			SELECT TTGeneralLedgerIt.NoGL, TTGeneralLedgerIt.NoAccount, 
         TTGeneralLedgerIt.KeteranganGL, TTGeneralLedgerIt.DebetGL, TTGeneralLedgerIt.KreditGL, 
         TRAccount.NamaAccount, TTGeneralLedgerHd.TglGL, TTGeneralLedgerHd.GLLink 
         FROM TTGeneralLedgerIt 
         INNER JOIN TRAccount ON TTGeneralLedgerIt.NoAccount = TRAccount.NoAccount 
         INNER JOIN TTGeneralLedgerHd ON TTGeneralLedgerIt.NoGL = TTGeneralLedgerHd.NoGL AND TTGeneralLedgerIt.TglGL = TTGeneralLedgerHd.TglGL 
         WHERE TTGeneralLedgerHd.TglGL BETWEEN '",TglAwal,"' AND '",TglAkhir,"'");
			SET @MyQuery = CONCAT(@MyQuery," AND TTGeneralLedgerHd.KodeTrans IN " , Hasil , " ");
			SET @MyQuery = CONCAT(@MyQuery," AND TTGeneralLedgerHd.GLValid Like    ('%" , xValidasi , "') ");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY TTGeneralLedgerHd.TglGL , TTGeneralLedgerIt.NoGL, TTGeneralLedgerIt.GLAutoN ");
		ELSE
			SET @MyQuery = CONCAT("
			SELECT TTGeneralLedgerIt.NoGL, TTGeneralLedgerIt.NoAccount, 
         TTGeneralLedgerIt.KeteranganGL, TTGeneralLedgerIt.DebetGL, TTGeneralLedgerIt.KreditGL, 
         TRAccount.NamaAccount, TTGeneralLedgerHd.TglGL, TTGeneralLedgerHd.GLLink 
         FROM TTGeneralLedgerIt 
         INNER JOIN TRAccount ON TTGeneralLedgerIt.NoAccount = TRAccount.NoAccount 
         INNER JOIN TTGeneralLedgerHd ON TTGeneralLedgerIt.NoGL = TTGeneralLedgerHd.NoGL AND TTGeneralLedgerIt.TglGL = TTGeneralLedgerHd.TglGL 
         WHERE TTGeneralLedgerHd.TglGL BETWEEN '",TglAwal,"' AND '",TglAkhir,"'");
			SET @MyQuery = CONCAT(@MyQuery," AND TTGeneralLedgerIt.NoAccount BETWEEN '" , xPerkiraan1 , "' AND '", xPerkiraan2, "' ");
			SET @MyQuery = CONCAT(@MyQuery," AND TTGeneralLedgerHd.KodeTrans IN " , Hasil , " ");
			SET @MyQuery = CONCAT(@MyQuery," AND TTGeneralLedgerHd.GLValid Like    ('%" , xValidasi , "') ");
			SET @MyQuery = CONCAT(@MyQuery," ORDER BY TTGeneralLedgerHd.TglGL , TTGeneralLedgerIt.NoGL, TTGeneralLedgerIt.GLAutoN ");
		END IF;
	END IF;

	IF xTipe = "Jurnal Header" THEN
		SET @MyQuery = CONCAT("
		SELECT * FROM TTGeneralLedgerHd 
      WHERE TTGeneralLedgerHd.TglGL  BETWEEN '",TglAwal,"' AND '",TglAkhir,"'");
		SET @MyQuery = CONCAT(@MyQuery," AND TTGeneralLedgerHd.KodeTrans IN " , Hasil , " ");
		SET @MyQuery = CONCAT(@MyQuery," AND TTGeneralLedgerHd.GLValid Like ('%" , xValidasi , "') ");
		SET @MyQuery = CONCAT(@MyQuery," ORDER BY TTGeneralLedgerHd.TglGL  ");
	END IF;

	IF xTipe = "Jurnal Tidak Balance" THEN

		UPDATE ttgeneralledgerhd JOIN
			(SELECT ttgeneralledgerit.NoGL, ttgeneralledgerit.TglGL, SUM(DebetGL) AS SumDebetGL, SUM(KreditGL) AS SumKreditGL FROM ttgeneralledgerit 
			WHERE ttgeneralledgerit.TglGL  BETWEEN TglAwal AND TglAkhir GROUP BY ttgeneralledgerit.NoGL, ttgeneralledgerit.TGlgl) Item
      ON (ttgeneralledgerhd.NoGL = Item.NoGL AND ttgeneralledgerhd.TglGL = Item.TglGL)
      SET ttgeneralledgerhd.TotalDebetGL = Item.SumDebetGL, ttgeneralledgerhd.TotalKreditGL = Item.SumKreditGL
      WHERE ttgeneralledgerhd.TglGL BETWEEN TglAwal AND TglAkhir ;
      
		SET @MyQuery = CONCAT("
			SELECT TTGeneralLedgerIt.NoGL, TTGeneralLedgerIt.NoAccount, TTGeneralLedgerIt.KeteranganGL, 
         TTGeneralLedgerIt.DebetGL, TTGeneralLedgerIt.KreditGL, TRAccount.NamaAccount, TTGeneralLedgerIt.TglGL, TTGeneralLedgerHd.GLLink 
         FROM TTGeneralLedgerIt INNER JOIN 
         TRAccount ON TTGeneralLedgerIt.NoAccount = TRAccount.NoAccount INNER JOIN 
         TTGeneralLedgerHd ON TTGeneralLedgerIt.NoGL = TTGeneralLedgerHd.NoGL AND 
         TTGeneralLedgerIt.TglGL = TTGeneralLedgerHd.TglGL 
         WHERE  TTGeneralLedgerIt.NoGL IN (SELECT * FROM Generalledgernobalance) ");
			SET @MyQuery = CONCAT(@MyQuery," AND TTGeneralLedgerHd.TglGL BETWEEN '",TglAwal,"' AND '",TglAkhir,"'");
			SET @MyQuery = CONCAT(@MyQuery," AND TTGeneralLedgerIt.NoAccount LIKE '%" , xPerkiraan1 , "' ");
         
	END IF;

	IF xTipe = "Belum Terposting" THEN
	      UPDATE ttsdhd INNER JOIN ttgeneralledgerhd ON SDNo = GLLink SET ttsdhd.NoGLSD = ttgeneralledgerhd.NoGL WHERE LEFT(ttsdhd.NoGLSD,2) <> 'JT' AND YEAR(SDTgl) = YEAR(NOW());
         UPDATE ttsdhd INNER JOIN ttgeneralledgerhd ON SVNo = GLLink SET ttsdhd.NoGLSV = ttgeneralledgerhd.NoGL WHERE LEFT(ttsdhd.NoGLSV,2) <> 'JT' AND YEAR(SDTgl) = YEAR(NOW());
         UPDATE ttsihd INNER JOIN ttgeneralledgerhd ON siNo = GLLink SET ttsihd.NoGL = ttgeneralledgerhd.NoGL WHERE LEFT(ttsihd.NoGL,2) <> 'JT' AND YEAR(siTgl) = YEAR(NOW());
         UPDATE ttsrhd INNER JOIN ttgeneralledgerhd ON srNo = GLLink SET ttsrhd.NoGL = ttgeneralledgerhd.NoGL WHERE LEFT(ttsrhd.NoGL,2) <> 'JT' AND YEAR(srTgl) = YEAR(NOW());
         UPDATE ttpshd INNER JOIN ttgeneralledgerhd ON psNo = GLLink SET ttpshd.NoGL = ttgeneralledgerhd.NoGL WHERE LEFT(ttpshd.NoGL,2) <> 'JT' AND YEAR(psTgl) = YEAR(NOW());
         UPDATE ttprhd INNER JOIN ttgeneralledgerhd ON prNo = GLLink SET ttprhd.NoGL = ttgeneralledgerhd.NoGL WHERE LEFT(ttprhd.NoGL,2) <> 'JT' AND YEAR(prTgl) = YEAR(NOW());
         UPDATE ttpihd INNER JOIN ttgeneralledgerhd ON piNo = GLLink SET ttpihd.NoGL = ttgeneralledgerhd.NoGL WHERE LEFT(ttpihd.NoGL,2) <> 'JT' AND YEAR(piTgl) = YEAR(NOW());
         UPDATE tttahd INNER JOIN ttgeneralledgerhd ON taNo = GLLink SET tttahd.NoGL = ttgeneralledgerhd.NoGL WHERE LEFT(tttahd.NoGL,2) <> 'JT' AND YEAR(taTgl) = YEAR(NOW());
         UPDATE tttihd INNER JOIN ttgeneralledgerhd ON tiNo = GLLink SET tttihd.NoGL = ttgeneralledgerhd.NoGL WHERE LEFT(tttihd.NoGL,2) <> 'JT' AND YEAR(tiTgl) = YEAR(NOW());
         UPDATE ttuchd INNER JOIN ttgeneralledgerhd ON ucNo = GLLink SET ttuchd.NoGL = ttgeneralledgerhd.NoGL WHERE LEFT(ttuchd.NoGL,2) <> 'JT' AND YEAR(ucTgl) = YEAR(NOW());
         UPDATE ttkkhd INNER JOIN ttgeneralledgerhd ON KKNo = GLLink SET ttkkhd.NoGL = ttgeneralledgerhd.NoGL WHERE LEFT(ttkkhd.NoGL,2) <> 'JT' AND YEAR(KKTgl) = YEAR(NOW());
         UPDATE ttkmhd INNER JOIN ttgeneralledgerhd ON kmNo = GLLink SET ttkmhd.NoGL = ttgeneralledgerhd.NoGL WHERE LEFT(ttkmhd.NoGL,2) <> 'JT' AND YEAR(KMTgl) = YEAR(NOW());
         UPDATE ttbkhd INNER JOIN ttgeneralledgerhd ON bkNo = GLLink SET ttbkhd.NoGL = ttgeneralledgerhd.NoGL WHERE LEFT(ttbkhd.NoGL,2) <> 'JT' AND YEAR(BKTgl) = YEAR(NOW());
         UPDATE ttbmhd INNER JOIN ttgeneralledgerhd ON bmNo = GLLink SET ttbmhd.NoGL = ttgeneralledgerhd.NoGL WHERE LEFT(ttbmhd.NoGL,2) <> 'JT' AND YEAR(BMTgl) = YEAR(NOW());
         UPDATE ttckhd INNER JOIN ttgeneralledgerhd ON ckNo = GLLink SET ttckhd.NoGL = ttgeneralledgerhd.NoGL WHERE LEFT(ttckhd.NoGL,2) <> 'JT' AND YEAR(ckTgl) = YEAR(NOW());
         UPDATE ttcchd INNER JOIN ttgeneralledgerhd ON CCNo = GLLink SET ttcchd.NoGL = ttgeneralledgerhd.NoGL WHERE LEFT(ttcchd.NoGL,2) <> 'JT' AND YEAR(CCTgl) = YEAR(NOW());    
         
		SET @MyQuery = CONCAT("
SELECT TransNo,TransTgl,TransMemo,TransJenis,Prefix,UserID,TransMyJenis,KodeTrans, IFNULL(NoGL,'--') AS NoGL,IFNULL(GLLink,'--') AS GLLink,NoGLTrans FROM (
SELECT ttSDhd.SDNo AS TransNo, ttSDhd.SDTgl AS TransTgl, ttSDhd.SDKeterangan AS TransMemo, 'Daftar Servis' AS TransJenis, '01SD' AS Prefix, ttSDhd.UserID, ttSDhd.SDJenis AS TransMyJenis, ttsdhd.KodeTrans, ttgeneralledgerhd.NoGL , ttgeneralledgerhd.GLLink, ttSDhd.NoGLSD AS NoGLTrans   FROM ttSDhd 
LEFT OUTER JOIN ttgeneralledgerhd ON ttSDhd.NoGLSD = ttgeneralledgerhd.NoGL 
WHERE ttSDhd.NoGLSD = '--' OR ttSDhd.SDNo <> GLLink 
UNION
SELECT IF(ttSDhd.SVNo ='--',ttSDhd.SDNo,ttSDhd.SVNo ) AS TransNo, ttSDhd.SVTgl AS TransTgl, ttSDhd.SDKeterangan AS TransMemo, 'Invoice Servis' AS TransJenis, '02SV' AS Prefix, ttSDhd.UserID, ttSDhd.SDJenis AS TransMyJenis, ttsdhd.KodeTrans, ttgeneralledgerhd.NoGL , ttgeneralledgerhd.GLLink, ttSDhd.NoGLSV AS NoGLTrans   FROM ttSDhd 
LEFT OUTER JOIN ttgeneralledgerhd ON ttSDhd.NoGLSV = ttgeneralledgerhd.NoGL 
WHERE ttSDhd.NoGLSV = '--' OR ttSDhd.SVNo <> GLLink 
UNION
SELECT ttSIhd.SINo AS TransNo, ttSIhd.SITgl AS TransTgl, ttSIhd.SIKeterangan AS TransMemo, 'Invoice Penjualan' AS TransJenis, '03SI' AS Prefix, ttSIhd.UserID, ttSIhd.SIJenis AS TransMyJenis, ttsihd.KodeTrans, ttgeneralledgerhd.NoGL , ttgeneralledgerhd.GLLink, ttSIhd.NoGL AS NoGLTrans   FROM ttSIhd 
LEFT OUTER JOIN ttgeneralledgerhd ON ttSIhd.NoGL = ttgeneralledgerhd.NoGL 
WHERE (ttSIhd.NoGL = '--' OR ttSIhd.SINo <> GLLink) 
UNION
SELECT ttSRhd.SRNo AS TransNo, ttSRhd.SRTgl AS TransTgl, ttSRhd.SRKeterangan AS TransMemo, 'Retur Penjualan' AS TransJenis, '04SR' AS Prefix, ttSRhd.UserID, '--' AS TransMyJenis, ttsrhd.KodeTrans, ttgeneralledgerhd.NoGL , ttgeneralledgerhd.GLLink, ttSRhd.NoGL AS NoGLTrans    FROM ttSRhd 
LEFT OUTER JOIN ttgeneralledgerhd ON ttSRhd.NoGL = ttgeneralledgerhd.NoGL 
WHERE ttSRhd.NoGL = '--' OR ttSRhd.SRNo <> GLLink 
UNION
SELECT ttPShd.PSNo AS TransNo, ttPShd.PSTgl AS TransTgl, ttPShd.PSKeterangan AS TransMemo, 'Penerimaan Barang' AS TransJenis, '05PS' AS Prefix, ttPShd.UserID, '--' AS TransMyJenis, ttPShd.KodeTrans, ttgeneralledgerhd.NoGL , ttgeneralledgerhd.GLLink, ttPShd.NoGL AS NoGLTrans    FROM ttPShd 
LEFT OUTER JOIN ttgeneralledgerhd ON ttPShd.NoGL = ttgeneralledgerhd.NoGL 
WHERE ttPShd.NoGL = '--' OR ttPShd.PSNo <> GLLink 
UNION
SELECT ttPIhd.PINo AS TransNo, ttPIhd.PITgl AS TransTgl, ttPIhd.PIKeterangan AS TransMemo, 'Invoice Pembelian' AS TransJenis, '06PI' AS Prefix, ttPIhd.UserID, ttPIhd.PIJenis AS TransMyJenis, ttPIhd.KodeTrans, ttgeneralledgerhd.NoGL , ttgeneralledgerhd.GLLink, ttPIhd.NoGL AS NoGLTrans    FROM ttPIhd 
LEFT OUTER JOIN ttgeneralledgerhd ON ttPIhd.NoGL = ttgeneralledgerhd.NoGL 
WHERE ttPIhd.NoGL = '--' OR ttPIhd.PINo <> GLLink 
UNION
SELECT ttPRhd.PRNo AS TransNo, ttPRhd.PRTgl AS TransTgl, ttPRhd.PRKeterangan AS TransMemo, 'Retur Pembelian' AS TransJenis, '07PR' AS Prefix, ttPRhd.UserID, '--' AS TransMyJenis, ttPRhd.KodeTrans, ttgeneralledgerhd.NoGL , ttgeneralledgerhd.GLLink, ttPRhd.NoGL AS NoGLTrans    FROM ttPRhd 
LEFT OUTER JOIN ttgeneralledgerhd ON ttPRhd.NoGL = ttgeneralledgerhd.NoGL 
WHERE ttPRhd.NoGL = '--' OR ttPRhd.PRNo <> GLLink 
UNION
SELECT ttTIhd.TINo AS TransNo, ttTIhd.TITgl AS TransTgl, ttTIhd.TIKeterangan AS TransMemo, 'Transfer Item' AS TransJenis, '08TI' AS Prefix, ttTIhd.UserID, '--' AS TransMyJenis, ttTIhd.KodeTrans, ttgeneralledgerhd.NoGL , ttgeneralledgerhd.GLLink, ttTIhd.NoGL AS NoGLTrans    FROM ttTIhd 
LEFT OUTER JOIN ttgeneralledgerhd ON ttTIhd.NoGL = ttgeneralledgerhd.NoGL 
WHERE ttTIhd.NoGL <> '--' OR ttTIhd.TINo <> GLLink 
UNION
SELECT ttTAhd.TANo AS TransNo, ttTAhd.TATgl AS TransTgl, ttTAhd.TAKeterangan AS TransMemo, 'Transfer Adjustment' AS TransJenis, '09TA' AS Prefix, ttTAhd.UserID, '--' AS TransMyJenis, ttTAhd.KodeTrans, ttgeneralledgerhd.NoGL , ttgeneralledgerhd.GLLink, ttTAhd.NoGL AS NoGLTrans    FROM ttTAhd 
LEFT OUTER JOIN ttgeneralledgerhd ON ttTAhd.NoGL = ttgeneralledgerhd.NoGL 
WHERE ttTAhd.NoGL = '--' OR ttTAhd.TANo <> GLLink 
UNION
SELECT ttKMhd.KMNo AS TransNo, ttKMhd.KMTgl AS TransTgl, ttKMhd.KMMemo AS TransMemo, 'Kas Masuk' AS TransJenis, '10KM' AS Prefix, ttKMhd.UserID, ttKMhd.KMJenis AS TransMyJenis, ttKMhd.KodeTrans, ttgeneralledgerhd.NoGL , ttgeneralledgerhd.GLLink, ttKMhd.NoGL AS NoGLTrans   FROM ttKMhd 
LEFT OUTER JOIN ttgeneralledgerhd ON ttKMhd.NoGL = ttgeneralledgerhd.NoGL 
WHERE ttKMhd.NoGL = '--' OR ttKMhd.KMNo <> GLLink 
UNION
SELECT ttKKhd.KKNo AS TransNo, ttKKhd.KKTgl AS TransTgl, ttKKhd.KKMemo AS TransMemo, 'Kas Keluar' AS TransJenis, '11KK' AS Prefix, ttKKhd.UserID, ttKKhd.KKJenis AS TransMyJenis, ttKKhd.KodeTrans, ttgeneralledgerhd.NoGL , ttgeneralledgerhd.GLLink, ttKKhd.NoGL AS NoGLTrans   FROM ttKKhd 
LEFT OUTER JOIN ttgeneralledgerhd ON ttKKhd.NoGL = ttgeneralledgerhd.NoGL 
WHERE ttKKhd.NoGL = '--' OR ttKKhd.KKNo <> GLLink 
UNION
SELECT ttBMhd.BMNo AS TransNo, ttBMhd.BMTgl AS TransTgl, ttBMhd.BMMemo AS TransMemo, 'Bank Masuk' AS TransJenis, '12BM' AS Prefix, ttBMhd.UserID, ttBMhd.BMJenis AS TransMyJenis, ttBMhd.KodeTrans, ttgeneralledgerhd.NoGL , ttgeneralledgerhd.GLLink, ttBMhd.NoGL AS NoGLTrans   FROM ttBMhd 
LEFT OUTER JOIN ttgeneralledgerhd ON ttBMhd.NoGL = ttgeneralledgerhd.NoGL 
WHERE ttBMhd.NoGL = '--' OR ttBMhd.BMNo <> GLLink 
UNION
SELECT ttBKhd.BKNo AS TransNo, ttBKhd.BKTgl AS TransTgl, ttBKhd.BKMemo AS TransMemo, 'Bank Keluar' AS TransJenis, '13BK' AS Prefix, ttBKhd.UserID, ttBKhd.BKJenis AS TransMyJenis, ttBKhd.KodeTrans, ttgeneralledgerhd.NoGL , ttgeneralledgerhd.GLLink, ttBKhd.NoGL AS NoGLTrans   FROM ttBKhd 
LEFT OUTER JOIN ttgeneralledgerhd ON ttBKhd.NoGL = ttgeneralledgerhd.NoGL 
WHERE (ttBKhd.NoGL = '--' OR ttBKhd.BKNo <> GLLink) 
UNION
SELECT ttCKhd.CKNo AS TransNo, ttCKhd.CKTgl AS TransTgl, ttCKhd.CKMemo AS TransMemo, 'Klaim KPB' AS TransJenis, '14CK' AS Prefix, ttCKhd.UserID, '--' AS TransMyJenis, 'CK' AS KodeTrans, ttgeneralledgerhd.NoGL , ttgeneralledgerhd.GLLink, ttCKhd.NoGL AS NoGLTrans    FROM ttCKhd 
LEFT OUTER JOIN ttgeneralledgerhd ON ttCKhd.NoGL = ttgeneralledgerhd.NoGL 
WHERE ttCKhd.NoGL = '--' OR ttCKhd.CKNo <> GLLink 
UNION
SELECT ttCChd.CCNo AS TransNo, ttCChd.CCTgl AS TransTgl, ttCChd.CCMemo AS TransMemo, 'Klaim C2' AS TransJenis, '15CC' AS Prefix, ttCChd.UserID, '--' AS TransMyJenis, 'CC' AS KodeTrans, ttgeneralledgerhd.NoGL , ttgeneralledgerhd.GLLink, ttCChd.NoGL AS NoGLTrans    FROM ttCChd 
LEFT OUTER JOIN ttgeneralledgerhd ON ttCChd.NoGL = ttgeneralledgerhd.NoGL 
WHERE ttCChd.NoGL = '--' OR ttCChd.CCNo <> GLLink 
UNION
SELECT ttUChd.UCNo AS TransNo, ttUChd.UCTgl AS TransTgl, ttUChd.UCKeterangan AS TransMemo, 'Update Standard Cost' AS TransJenis, '15UC' AS Prefix, ttUChd.UserID, '--' AS TransMyJenis,  ttUChd.KodeTrans , ttgeneralledgerhd.NoGL , ttgeneralledgerhd.GLLink, ttUChd.NoGL AS NoGLTrans   FROM ttUChd 
LEFT OUTER JOIN ttgeneralledgerhd ON ttUChd.NoGL = ttgeneralledgerhd.NoGL 
WHERE ttUChd.NoGL = '--' OR ttUChd.UCNo <> GLLink                 
         ) TransBlmGL ");
			SET @MyQuery = CONCAT(@MyQuery," WHERE TransTgl BETWEEN '",TglAwal,"' AND '",TglAkhir,"'");
			
			
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			SET @MyQuery = CONCAT(@MyQuery," ");		
		ELSE
			SET @MyQuery = CONCAT(@MyQuery," AND Prefix BETWEEN '" , xPerkiraan1 , "' AND '", xPerkiraan2, "' ");	
		END IF;

		IF xSort = "Jam"  THEN
			SET @MyQuery = CONCAT(@MyQuery,"  ORDER BY TransTgl, Prefix, TransNo ");
		ELSE
			SET @MyQuery = CONCAT(@MyQuery,"   ORDER BY Prefix, TransNo, TransTgl ");
		END IF;

	END IF;

	IF xTipe = "Ledger Detail" THEN
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			DROP TEMPORARY TABLE IF EXISTS vttgeneralledgerit;
			CREATE TEMPORARY TABLE IF NOT EXISTS vttgeneralledgerit AS
				SELECT ttgeneralledgerit.NoGL, ttgeneralledgerit.TglGL, ttgeneralledgerit.NoAccount,
				ttgeneralledgerit.KeteranganGL, ttgeneralledgerit.DebetGL, ttgeneralledgerit.KreditGL, ttgeneralledgerhd.KodeTrans, TTGeneralLedgerHd.GLLink
				FROM ttgeneralledgerhd INNER JOIN ttgeneralledgerit
				ON ttgeneralledgerhd.NoGL = ttgeneralledgerit.NoGL AND ttgeneralledgerhd.TglGL = ttgeneralledgerit.TglGL
				WHERE (TTGeneralLedgerIt.TglGL BETWEEN TglAwal AND TglAkhir );
			CREATE INDEX NoAccount ON vttgeneralledgerit(NoAccount);

			DROP TEMPORARY TABLE IF EXISTS AktivitasGLT;
			CREATE TEMPORARY TABLE IF NOT EXISTS AktivitasGLT AS
			SELECT  IFNULL(vttgeneralledgerit.TglGL, TglAwal) AS TglGL, IFNULL(vttgeneralledgerit.NoGL, '-') AS NoGL,
			traccount.NoAccount, traccount.NamaAccount, IFNULL(vttgeneralledgerit.KeteranganGL, '-') AS KeteranganGL,
			IFNULL(vttgeneralledgerit.DebetGL, 0) AS DebetGL, IFNULL(vttgeneralledgerit.KreditGL, 0) AS KreditGL,IFNULL(vttgeneralledgerit.KodeTrans,'NA') AS KodeTrans,
			0000000000000.00 AS Saldo,0000000000000.00 AS Saldo1, IFNULL(vttgeneralledgerit.GLLink, '-') AS GLLink
			FROM  traccount LEFT OUTER JOIN
			vttgeneralledgerit
			ON traccount.NoAccount = vttgeneralledgerit.NoAccount
			WHERE traccount.JenisAccount = 'Detail'
			ORDER BY  traccount.NoAccount, TglGL, IFNULL(vttgeneralledgerit.NoGL, '-');
		ELSE
			DROP TEMPORARY TABLE IF EXISTS vttgeneralledgerit;
			CREATE TEMPORARY TABLE IF NOT EXISTS vttgeneralledgerit AS
				SELECT ttgeneralledgerit.NoGL, ttgeneralledgerit.TglGL, ttgeneralledgerit.NoAccount,
				ttgeneralledgerit.KeteranganGL, ttgeneralledgerit.DebetGL, ttgeneralledgerit.KreditGL, ttgeneralledgerhd.KodeTrans, TTGeneralLedgerHd.GLLink
				FROM ttgeneralledgerhd INNER JOIN ttgeneralledgerit
				ON ttgeneralledgerhd.NoGL = ttgeneralledgerit.NoGL AND ttgeneralledgerhd.TglGL = ttgeneralledgerit.TglGL
				WHERE (TTGeneralLedgerIt.TglGL BETWEEN TglAwal AND TglAkhir );
			CREATE INDEX NoAccount ON vttgeneralledgerit(NoAccount);

			DROP TEMPORARY TABLE IF EXISTS AktivitasGLT;
			CREATE TEMPORARY TABLE IF NOT EXISTS AktivitasGLT AS
			SELECT  IFNULL(vttgeneralledgerit.TglGL, TglAwal) AS TglGL, IFNULL(vttgeneralledgerit.NoGL, '-') AS NoGL,
			traccount.NoAccount, traccount.NamaAccount, IFNULL(vttgeneralledgerit.KeteranganGL, '-') AS KeteranganGL,
			IFNULL(vttgeneralledgerit.DebetGL, 0) AS DebetGL, IFNULL(vttgeneralledgerit.KreditGL, 0) AS KreditGL,IFNULL(vttgeneralledgerit.KodeTrans,'NA') AS KodeTrans,
			0000000000000.00 AS Saldo,0000000000000.00 AS Saldo1, IFNULL(vttgeneralledgerit.GLLink, '-') AS GLLink
			FROM  traccount LEFT OUTER JOIN
			vttgeneralledgerit
			ON traccount.NoAccount = vttgeneralledgerit.NoAccount
			WHERE traccount.JenisAccount = 'Detail'
			AND (traccount.NoAccount BETWEEN  xPerkiraan1  AND  xPerkiraan2 )
			ORDER BY  traccount.NoAccount, TglGL, IFNULL(vttgeneralledgerit.NoGL, '-');
		END IF;
		
		SET @MyQuery = CONCAT("SELECT * FROM AktivitasGLT;");
	END IF;


	IF xTipe = "Ledger Harian" THEN
		IF xPerkiraan1 = "%"  OR xPerkiraan2 = "%" THEN
			DROP TEMPORARY TABLE IF EXISTS vttgeneralledgerit;
			CREATE TEMPORARY TABLE IF NOT EXISTS vttgeneralledgerit AS
				SELECT ttgeneralledgerit.NoGL, ttgeneralledgerit.TglGL, ttgeneralledgerit.NoAccount,
				ttgeneralledgerit.KeteranganGL, ttgeneralledgerit.DebetGL, ttgeneralledgerit.KreditGL, ttgeneralledgerhd.KodeTrans, TTGeneralLedgerHd.GLLink
				FROM ttgeneralledgerhd INNER JOIN ttgeneralledgerit
				ON ttgeneralledgerhd.NoGL = ttgeneralledgerit.NoGL AND ttgeneralledgerhd.TglGL = ttgeneralledgerit.TglGL
				WHERE (TTGeneralLedgerIt.TglGL BETWEEN TglAwal AND TglAkhir );
			CREATE INDEX NoAccount ON vttgeneralledgerit(NoAccount);

			DROP TEMPORARY TABLE IF EXISTS AktivitasGLT;
			CREATE TEMPORARY TABLE IF NOT EXISTS AktivitasGLT AS
			SELECT  traccount.NoAccount, traccount.NamaAccount, IFNULL(vttgeneralledgerit.KodeTrans,'NA') AS KodeTrans,  
         IFNULL(vttgeneralledgerit.NoGL, '-') AS NoGL, IFNULL(vttgeneralledgerit.TglGL, TglAwal) AS TglGL,
         IFNULL(vttgeneralledgerit.KeteranganGL, '-') AS KeteranganGL, 
         IFNULL(SUM(vttgeneralledgerit.DebetGL), 0000000000000.00) AS DebetGL, IFNULL(SUM(vttgeneralledgerit.KreditGL), 0000000000000.00) AS KreditGL, 
         0000000000000.00 AS Saldo, 0000000000000.00 AS Saldo1, 
         IFNULL(vttgeneralledgerit.GLLink, '-') AS GLLink 
         FROM  traccount LEFT OUTER JOIN
			vttgeneralledgerit
			ON traccount.NoAccount = vttgeneralledgerit.NoAccount
			WHERE traccount.JenisAccount = 'Detail'
			GROUP BY traccount.NamaAccount,IFNULL(vttgeneralledgerit.TglGL, TglAwal)
			ORDER BY  traccount.NoAccount, TglGL, IFNULL(vttgeneralledgerit.NoGL, '-');
		ELSE
			DROP TEMPORARY TABLE IF EXISTS vttgeneralledgerit;
			CREATE TEMPORARY TABLE IF NOT EXISTS vttgeneralledgerit AS
				SELECT ttgeneralledgerit.NoGL, ttgeneralledgerit.TglGL, ttgeneralledgerit.NoAccount,
				ttgeneralledgerit.KeteranganGL, ttgeneralledgerit.DebetGL, ttgeneralledgerit.KreditGL, ttgeneralledgerhd.KodeTrans, TTGeneralLedgerHd.GLLink
				FROM ttgeneralledgerhd INNER JOIN ttgeneralledgerit
				ON ttgeneralledgerhd.NoGL = ttgeneralledgerit.NoGL AND ttgeneralledgerhd.TglGL = ttgeneralledgerit.TglGL
				WHERE (TTGeneralLedgerIt.TglGL BETWEEN TglAwal AND TglAkhir );
			CREATE INDEX NoAccount ON vttgeneralledgerit(NoAccount);

			DROP TEMPORARY TABLE IF EXISTS AktivitasGLT;
			CREATE TEMPORARY TABLE IF NOT EXISTS AktivitasGLT AS
			SELECT  traccount.NoAccount, traccount.NamaAccount, IFNULL(vttgeneralledgerit.KodeTrans,'NA') AS KodeTrans,  
         IFNULL(vttgeneralledgerit.NoGL, '-') AS NoGL, IFNULL(vttgeneralledgerit.TglGL, TglAwal) AS TglGL,
         IFNULL(vttgeneralledgerit.KeteranganGL, '-') AS KeteranganGL, 
         IFNULL(SUM(vttgeneralledgerit.DebetGL), 0000000000000.00) AS DebetGL, IFNULL(SUM(vttgeneralledgerit.KreditGL), 0000000000000.00) AS KreditGL, 
         0000000000000.00 AS Saldo, 0000000000000.00 AS Saldo1, 
         IFNULL(vttgeneralledgerit.GLLink, '-') AS GLLink 
         FROM  traccount LEFT OUTER JOIN
			vttgeneralledgerit
			ON traccount.NoAccount = vttgeneralledgerit.NoAccount
			WHERE traccount.JenisAccount = 'Detail'
			AND (traccount.NoAccount BETWEEN  xPerkiraan1  AND  xPerkiraan2 )
			GROUP BY traccount.NamaAccount,IFNULL(vttgeneralledgerit.TglGL, TglAwal)
			ORDER BY  traccount.NoAccount, TglGL, IFNULL(vttgeneralledgerit.NoGL, '-');
		END IF;
		
		SET @MyQuery = CONCAT("SELECT * FROM AktivitasGLT;");
	END IF;

	
	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;
END$$

DELIMITER ;

