DELIMITER $$
DROP PROCEDURE IF EXISTS `rKeuanganBankKeluar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rKeuanganBankKeluar`(IN xTipe VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,IN xJenis VARCHAR(30),IN xBank VARCHAR(30),IN xLeasing VARCHAR(30),IN xJurnal NUMERIC,IN xSort VARCHAR(20),IN xOrder VARCHAR(10))
BEGIN
/*
           Cara Akses SP :
           CALL rKeuanganBankKeluar(xTipe,xTgl1,xTgl2,xJenis,xBank,xLeasing,xJurnal,xSort,xOrder);
           xTipe ->ttbkhd,ttbkit,ttbkitBBN
           CALL rKeuanganBankKeluar('ttbkhd','2014-04-30','2014-04-30','Neraca Awal','','TUNAI',0,'BKTgl','ASC');
           CALL rKeuanganBankKeluar('ttbkit','2014-04-30','2019-04-30','','','',0,'ttbkhd.BKTgl','ASC');
           CALL rKeuanganBankKeluar('ttbkitBBN','2014-04-30','2019-04-30','','','',0,'BMTgl','ASC');
*/
	   SET xJenis = CONCAT(xJenis,"%");
	   SET xBank = CONCAT(xBank,"%");
	   SET xLeasing = CONCAT(xLeasing,"%");
	   IF xTipe = "ttbkhd" THEN
	      SET @MyQuery = CONCAT("SELECT ttbkhd.BKNo, ttbkhd.BKTgl, ttbkhd.BKMemo, ttbkhd.BKNominal, ttbkhd.BKJenis,
         ttbkhd.SupKode, ttbkhd.BKCekNo, ttbkhd.BKCekTempo, ttbkhd.BKPerson, 
         ttbkhd.BKAC, ttbkhd.NoAccount, ttbkhd.UserID, IFNULL(tdsupplier.SupNama, '--') AS SupNama, 
         IFNULL(traccount.NamaAccount, '--') AS NamaAccount 
         FROM ttbkhd 
         LEFT OUTER JOIN tdsupplier ON ttbkhd.SupKode = tdsupplier.SupKode 
         LEFT OUTER JOIN traccount ON ttbkhd.NoAccount = traccount.NoAccount 
         WHERE 
         (ttbkhd.BKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
         (ttbkhd.BKJenis LIKE '",xJenis,"') AND 
         (ttbkhd.SupKode LIKE '",xLeasing,"') AND 
         (LEFT(ttbkhd.BKNo,2) <> 'BP') AND 
         (ttbkhd.NoAccount LIKE '",xBank,"')");
              IF xJurnal = 1 THEN
                 SET @MyQuery = CONCAT(@MyQuery,"AND NoGL <> '--' ");
              ELSE
                  IF xJurnal = 2 THEN
                     SET @MyQuery = CONCAT(@MyQuery,"AND NoGL = '--' ");
                  END IF;
              END IF;
              SET @MyQuery = CONCAT(@MyQuery,"ORDER BY ",xSort," ",xOrder);
	   END IF;

	   IF xTipe = "ttbkitBBN" THEN
	      SET @MyQuery = CONCAT("SELECT ttbkit.BKNo, ttbkit.FBNo, ttdk.DKTgl, ttdk.CusKode, tdcustomer.CusNama, 
         CONCAT_WS('', tdcustomer.CusAlamat, '  RT/RW : ', tdcustomer.CusRT, '/', tdcustomer.CusRW) AS CusAlamat, 
         tmotor.MotorType, tmotor.MotorNoMesin, ttdk.LeaseKode, ttdk.BBN, 0 AS Terbayar, 
         ttbkit.BKBayar, 0 AS Sisa, SUBSTRING_INDEX(BKMemo, ' ', -1) AS BKMemo 
         FROM ttdk 
         INNER JOIN tmotor ON Tmotor.DKNo = ttdk.DKNo 
         INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
         INNER JOIN ttbkit  ON ttbkit.FBNo = ttdk.DKNo  
         INNER JOIN ttbkhd  ON ttbkit.BKNo = ttbkhd.BKNo  
         WHERE 
         (ttbkhd.BKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
         (ttbkhd.BKJenis LIKE '",xJenis,"') AND 
         (ttbkhd.SupKode LIKE '",xLeasing,"') AND 
         (ttbkhd.BKNo LIKE 'BK%') AND 
         (ttbkhd.NoAccount LIKE '",xBank,"')");
              IF xJurnal = 1 THEN
                 SET @MyQuery = CONCAT(@MyQuery," AND ( ttbkhd.NoGL <> '--' ) ");
              ELSE
                  IF xJurnal = 2 THEN
                     SET @MyQuery = CONCAT(@MyQuery," AND ( ttbkhd.NoGL = '--' ) ");
                  END IF;
              END IF;
              SET @MyQuery = CONCAT(@MyQuery,"ORDER BY ttbkit.BKNo, ttbkit.FBNo,",xSort," ",xOrder);
	   END IF;

	   IF xTipe = "ttbkit" THEN
	      SET @MyQuery = CONCAT("SELECT ttbkit.BKNo, ttbkit.FBNo, ttfb.FBTgl, ttfb.SSNo, ttfb.SupKode, tdsupplier.SupNama, 
         ttfb.FBTotal, IFNULL(ttss.SSTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SSTgl, ttbkit.BKBayar 
         FROM ttbkit 
         INNER JOIN ttbkhd ON ttbkit.BKNo = ttbkhd.BKNo 
         LEFT OUTER JOIN ttfb ON ttbkit.FBNo = ttfb.FBNo 
         LEFT OUTER JOIN tdsupplier ON ttfb.SupKode = tdsupplier.SupKode 
         LEFT OUTER JOIN ttss ON ttfb.SSNo = ttss.SSNo 
          WHERE 
         (ttbkhd.BKTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
         (ttbkhd.BKJenis LIKE '",xJenis,"') AND 
         (ttbkhd.SupKode LIKE '",xLeasing,"') AND 
         (ttbkhd.BKNo LIKE 'BK%') AND 
         (ttbkhd.NoAccount LIKE '",xBank,"')");
              IF xJurnal = 1 THEN
                 SET @MyQuery = CONCAT(@MyQuery," AND ( ttbkhd.NoGL <> '--' ) ");
              ELSE
                  IF xJurnal = 2 THEN
                     SET @MyQuery = CONCAT(@MyQuery," AND ( ttbkhd.NoGL = '--' ) ");
                  END IF;
              END IF;
              SET @MyQuery = CONCAT(@MyQuery,"ORDER BY ttbkit.BKNo,",xSort," ",xOrder);
	   END IF;

           PREPARE STMT FROM @MyQuery;
           EXECUTE Stmt;

	END$$

DELIMITER ;

