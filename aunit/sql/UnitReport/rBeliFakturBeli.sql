DELIMITER $$
DROP PROCEDURE IF EXISTS `rBeliFakturBeli`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rBeliFakturBeli`(IN xTipe VARCHAR(20),IN xStatTgl VARCHAR(20),IN xTgl1 DATE,IN xTgl2 DATE,IN xSupplier VARCHAR(20),IN xLokasi VARCHAR(30),IN xNoSS VARCHAR(10),xLunas VARCHAR(10),IN xSort VARCHAR(20),IN xOrder VARCHAR(10))
BEGIN
       SET xSupplier = CONCAT(xSupplier,"%");
       SET xLokasi = CONCAT(xLokasi,"%");
       IF xTipe = "ttfb" THEN
          SET @MyQuery = "SELECT IFNULL(tmotor.FBJum, 0) AS FBJum, ttfb.FBPSS + ttfb.FBPtgLain AS FBPtgTotal, IFNULL(tmotor.FBSubTotal, 0) AS FBSubTotal, 
                          ttfb.FBLunas, ttfb.FBMemo, ttfb.FBNo, ttfb.FBPSS, ttfb.FBPtgLain, ttfb.FBTermin, ttfb.FBTgl, 
                          ttfb.FBTglTempo, ttfb.FBTotal, ttfb.SSNo, ttfb.SupKode, ttfb.UserID, IFNULL(ttss.SSTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SSTgl,
                          IFNULL(tdsupplier.SupNama, '--') AS SupNama 
                          FROM ttfb
                          LEFT OUTER JOIN ttss ON ttfb.SSNo = ttss.SSNo 
                          LEFT OUTER JOIN tdsupplier ON ttfb.SupKode = tdsupplier.SupKode
                          LEFT OUTER JOIN
                          (SELECT COUNT(MotorType) AS FBJum, SUM(FBHarga) AS FBSubTotal, FBNo FROM tmotor tmotor_1 GROUP BY FBNo) 
                          tmotor ON ttfb.FBNo = tmotor.FBNo 
                          WHERE";
          IF xNoSS = "SS" THEN
             SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND (ttfb.SupKode LIKE '",xSupplier,"') AND (IFNULL(ttss.LokasiKode,'--') LIKE '",xLokasi,"') AND (IFNULL(ttss.SSNo,'--') <> '--') ORDER BY ",xSort," ",xOrder); 
          ELSE
             SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND (ttfb.SupKode LIKE '",xSupplier,"') AND (IFNULL(ttss.LokasiKode,'--') LIKE '",xLokasi,"') AND (IFNULL(ttss.SSNo,'--') LIKE '",xNoSS,"') ORDER BY ",xSort," ",xOrder); 
          END IF;
       END IF;
       IF xTipe = "tmotor" THEN
          SET @MyQuery = "SELECT BPKBNo, BPKBTglAmbil, BPKBTglJadi, tmotor.DKNo, tmotor.DealerKode, FBHarga, tmotor.FBNo, 
                          FakturAHMNo, FakturAHMTgl, FakturAHMTglAmbil, MotorAutoN, MotorMemo, 
                          MotorNoMesin, MotorNoRangka, MotorTahun, MotorType, MotorWarna, PDNo, PlatNo, PlatTgl, PlatTglAmbil, 
                          SDNo, SKNo, tmotor.SSNo, STNKAlamat, STNKNama, STNKNo, STNKTglAmbil, STNKTglBayar, STNKTglJadi, STNKTglMasuk 
                          FROM tmotor 
                          INNER JOIN ttfb ON ttfb.FBNo = tMotor.FBNo 
                          LEFT OUTER JOIN ttss ON ttfb.SSNo = ttss.SSNo 
                          WHERE ";
          IF xNoSS = "SS" THEN
             SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND (ttfb.SupKode LIKE '",xSupplier,"') AND (IFNULL(ttss.LokasiKode,'--') LIKE '",xLokasi,"') AND (IFNULL(ttss.SSNo,'--') <> '--') ORDER BY ",xSort," ",xOrder); 
          ELSE
             SET @MyQuery = CONCAT(@MyQuery," (",xStatTgl," BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND (ttfb.SupKode LIKE '",xSupplier,"') AND (IFNULL(ttss.LokasiKode,'--') LIKE '",xLokasi,"') AND (IFNULL(ttss.SSNo,'--') LIKE '",xNoSS,"') ORDER BY ",xSort," ",xOrder); 
          END IF;
       END IF;
       PREPARE STMT FROM @MyQuery;
       EXECUTE Stmt;
    
    END$$

DELIMITER ;

