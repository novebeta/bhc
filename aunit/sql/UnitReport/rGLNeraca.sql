DELIMITER $$
DROP PROCEDURE IF EXISTS `rGLNeraca`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rGLNeraca`(IN  MyTgl1 DATE, MyTgl2 DATE , MyTglSaldo DATE)
BEGIN
DECLARE MyLevel INT;
DECLARE MaxLevel INT;
DECLARE MyPenjualan, MyHPP, MyBiayaOp, MyPendapatanOp, MyPendapatanLain, MyBiayaLain DOUBLE DEFAULT 0.00;
DECLARE MyTotalDebet, MyTotalKredit DOUBLE DEFAULT 0.00;

DECLARE  MyTglAwal, MyTglAkhir DATE;
IF MyTglSaldo = MyTgl1 THEN
	SET MyTglSaldo = MyTgl1;
ELSE
	SELECT MAX(TglSaldo) INTO MyTglSaldo  FROM tsaldoaccount WHERE  TglSaldo  < MyTgl1;
END IF;
SET MyTglAwal = DATE_ADD(MyTglSaldo, INTERVAL 1 DAY);
SET MyTglAkhir = DATE_ADD(MyTgl1, INTERVAL -1 DAY);

DROP TEMPORARY TABLE IF EXISTS `TGeneral`;
CREATE TEMPORARY TABLE IF NOT EXISTS `TGeneral` AS
SELECT TTGeneralLedgerIt.NoAccount, SUM(TTGeneralLedgerIt.DebetGL) AS DebetTotal, SUM(TTGeneralLedgerIt.KreditGL) AS KreditTotal
    FROM TTGeneralLedgerIt
    INNER JOIN TTGeneralLedgerHd ON TTGeneralLedgerIt.NoGL = TTGeneralLedgerHd.NoGL AND TTGeneralLedgerIt.TglGL = TTGeneralLedgerHd.TglGL
    WHERE (TTGeneralLedgerHd.TglGL BETWEEN MyTglAwal AND MyTglAkhir)
    GROUP BY TTGeneralLedgerIt.NoAccount;
CREATE INDEX NoAccount ON TGeneral(NoAccount);
DROP TEMPORARY TABLE IF EXISTS `traccountsaldo`;
CREATE TEMPORARY TABLE IF NOT EXISTS `traccountsaldo` AS
SELECT TRAccount.NoAccount, TRAccount.NamaAccount, (IFNULL(TSaldoAccount.SaldoAccount, 0) + TRAccount.OpeningBalance) AS OpeningBalance,
	TRAccount.JenisAccount, TRAccount.StatusAccount, TRAccount.NoParent, IFNULL(TGeneral.DebetTotal, 0) AS Debet, IFNULL(TGeneral.KreditTotal, 0) AS Kredit,
	IFNULL(TSaldoAccount.TglSaldo, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS TglSaldo, IFNULL(TSaldoAccount.SaldoAccount, 0) AS Saldo , TRAccount.LevelAccount
FROM TRAccount
LEFT OUTER JOIN TGeneral
    ON TRAccount.NoAccount = TGeneral.NoAccount
LEFT OUTER JOIN
    (SELECT * FROM TSaldoAccount WHERE TglSaldo = MyTglSaldo) TSaldoAccount
    ON TRAccount.NoAccount = TSaldoAccount.NoAccount
ORDER BY TRAccount.NoAccount;
CREATE INDEX NoAccount ON traccountsaldo(NoAccount);
CREATE INDEX NoParent ON traccountsaldo(NoParent);

DROP TEMPORARY TABLE IF EXISTS `traccountsaldotemp`;
CREATE TEMPORARY TABLE IF NOT EXISTS `traccountsaldotemp` AS SELECT * FROM traccountsaldo;
CREATE INDEX NoAccount ON traccountsaldotemp(NoAccount);
CREATE INDEX NoParent ON traccountsaldotemp(NoParent);

SELECT MAX(LevelAccount) INTO MaxLevel  FROM traccount ;
SET MyLevel = MaxLevel;
loop_label:  LOOP
  IF  MyLevel < 0 THEN
    LEAVE  loop_label;
  END  IF;
  UPDATE traccountsaldo
  INNER JOIN
  (SELECT  NoParent, SUM(Debet) AS MyDebet, SUM(Kredit) AS MyKredit
	  FROM traccountsaldotemp
  	WHERE LevelAccount = MyLevel GROUP BY NoParent) A ON A.NoParent = traccountsaldo.NoAccount
  SET traccountsaldo.Debet = A.MyDebet, traccountsaldo.Kredit = A.MyKredit;
  UPDATE traccountsaldotemp
  INNER JOIN  traccountsaldo ON traccountsaldo.NoAccount = traccountsaldotemp.NoAccount
  SET traccountsaldotemp.Debet = traccountsaldo.Debet, traccountsaldotemp.Kredit = traccountsaldo.Kredit;
  SET MyLevel = MyLevel - 1;
  ITERATE  loop_label;
END LOOP;
UPDATE traccountsaldo SET Saldo = OpeningBalance + Debet - Kredit WHERE NoAccount LIKE '1%' OR NoAccount LIKE '5%' OR NoAccount LIKE '6%' OR NoAccount LIKE '8%';
UPDATE traccountsaldo SET Saldo = OpeningBalance + Kredit - Debet WHERE NoAccount LIKE '2%' OR NoAccount LIKE '3%' OR NoAccount LIKE '4%' OR NoAccount LIKE '9%';

SELECT Saldo INTO MyPenjualan FROM traccountsaldo WHERE NoAccount = '30000000';
SELECT Saldo INTO MyHPP FROM traccountsaldo WHERE NoAccount = '50000000';
SELECT Saldo INTO MyBiayaOp FROM traccountsaldo WHERE NoAccount = '60000000';
SELECT Saldo INTO MyPendapatanOp FROM traccountsaldo WHERE NoAccount = '40000000';
SELECT Saldo INTO MyPendapatanLain FROM traccountsaldo WHERE NoAccount = '90000000';
SELECT Saldo INTO MyBiayaLain FROM traccountsaldo WHERE NoAccount = '80000000';
UPDATE traccountsaldo SET Saldo = MyPenjualan + MyPendapatanLain + MyPendapatanOp - MyHPP - MyBiayaOp - MyBiayaLain, Debet = MyHPP + MyBiayaOp + MyBiayaLain, Kredit = MyPenjualan + MyPendapatanOp + MyPendapatanLain WHERE NoAccount = '26040000';

SELECT MAX(LevelAccount) INTO MaxLevel  FROM traccount ;
SET MyLevel = MaxLevel;
loop_label:  LOOP
  IF  MyLevel < 0 THEN
    LEAVE  loop_label;
  END  IF;
  UPDATE traccountsaldo
  INNER JOIN
  (SELECT  NoParent, SUM(Debet) AS MyDebet, SUM(Kredit) AS MyKredit
	  FROM traccountsaldotemp
  	WHERE LevelAccount = MyLevel GROUP BY NoParent) A ON A.NoParent = traccountsaldo.NoAccount
  SET traccountsaldo.Debet = A.MyDebet, traccountsaldo.Kredit = A.MyKredit;
  UPDATE traccountsaldotemp
  INNER JOIN  traccountsaldo ON traccountsaldo.NoAccount = traccountsaldotemp.NoAccount
  SET traccountsaldotemp.Debet = traccountsaldo.Debet, traccountsaldotemp.Kredit = traccountsaldo.Kredit;
  SET MyLevel = MyLevel - 1;
  ITERATE  loop_label;
END LOOP;
UPDATE traccountsaldo SET Saldo = OpeningBalance + Debet - Kredit WHERE NoAccount LIKE '1%' OR NoAccount LIKE '5%' OR NoAccount LIKE '6%' OR NoAccount LIKE '8%';
UPDATE traccountsaldo SET Saldo = OpeningBalance + Kredit - Debet WHERE NoAccount LIKE '2%' OR NoAccount LIKE '3%' OR NoAccount LIKE '4%' OR NoAccount LIKE '9%';

SELECT SUM(Debet), SUM(Kredit) INTO MyTotalDebet, MyTotalKredit FROM traccountsaldo WHERE NoAccount = '10000000' OR NoAccount = '20000000' ;
UPDATE traccountsaldo SET Saldo = 0, Kredit = MyTotalKredit, Debet = MyTotalDebet WHERE NoAccount = '00000000';

UPDATE traccountsaldo SET OpeningBalance = Saldo, Debet = 0, Kredit = 0, Saldo = 0;


DROP TEMPORARY TABLE IF EXISTS `TGeneral`;
CREATE TEMPORARY TABLE IF NOT EXISTS `TGeneral` AS
SELECT TTGeneralLedgerIt.NoAccount, SUM(TTGeneralLedgerIt.DebetGL) AS DebetTotal, SUM(TTGeneralLedgerIt.KreditGL) AS KreditTotal
    FROM TTGeneralLedgerIt
    INNER JOIN TTGeneralLedgerHd ON TTGeneralLedgerIt.NoGL = TTGeneralLedgerHd.NoGL AND TTGeneralLedgerIt.TglGL = TTGeneralLedgerHd.TglGL
    WHERE (TTGeneralLedgerHd.TglGL BETWEEN MyTgl1 AND MyTgl2)
    GROUP BY TTGeneralLedgerIt.NoAccount;
CREATE INDEX NoAccount ON TGeneral(NoAccount);
DROP TEMPORARY TABLE IF EXISTS `traccountsaldoNow`;
CREATE TEMPORARY TABLE IF NOT EXISTS `traccountsaldoNow` AS
SELECT TRAccount.NoAccount, TRAccount.NamaAccount, 0 AS OpeningBalance,
	TRAccount.JenisAccount, TRAccount.StatusAccount, TRAccount.NoParent, IFNULL(TGeneral.DebetTotal, 0) AS Debet, IFNULL(TGeneral.KreditTotal, 0) AS Kredit,
	STR_TO_DATE('01/01/1900', '%m/%d/%Y') AS TglSaldo, 0 AS Saldo , TRAccount.LevelAccount
FROM TRAccount
LEFT OUTER JOIN TGeneral
    ON TRAccount.NoAccount = TGeneral.NoAccount
ORDER BY TRAccount.NoAccount;
CREATE INDEX NoAccount ON traccountsaldoNow(NoAccount);
CREATE INDEX NoParent ON traccountsaldoNow(NoParent);

DROP TEMPORARY TABLE IF EXISTS `traccountsaldotemp`;
CREATE TEMPORARY TABLE IF NOT EXISTS `traccountsaldotemp` AS SELECT * FROM traccountsaldoNow;
CREATE INDEX NoAccount ON traccountsaldotemp(NoAccount);
CREATE INDEX NoParent ON traccountsaldotemp(NoParent);

UPDATE traccountsaldo INNER JOIN traccountsaldoNow ON  traccountsaldo.NoAccount =  traccountsaldoNow.NoAccount
SET traccountsaldo.Debet = traccountsaldoNow.Debet,traccountsaldo.Kredit =traccountsaldoNow.Kredit;

SELECT MAX(LevelAccount) INTO MaxLevel  FROM traccount ;
SET MyLevel = MaxLevel;
loop_label:  LOOP
  IF  MyLevel < 0 THEN
    LEAVE  loop_label;
  END  IF;
  UPDATE traccountsaldo
  INNER JOIN
  (SELECT  NoParent, SUM(Debet) AS MyDebet, SUM(Kredit) AS MyKredit
	  FROM traccountsaldotemp
  	WHERE LevelAccount = MyLevel GROUP BY NoParent) A ON A.NoParent = traccountsaldo.NoAccount
  SET traccountsaldo.Debet = A.MyDebet, traccountsaldo.Kredit = A.MyKredit;
  UPDATE traccountsaldotemp
  INNER JOIN  traccountsaldo ON traccountsaldo.NoAccount = traccountsaldotemp.NoAccount
  SET traccountsaldotemp.Debet = traccountsaldo.Debet, traccountsaldotemp.Kredit = traccountsaldo.Kredit;
  SET MyLevel = MyLevel - 1;
  ITERATE  loop_label;
END LOOP;
UPDATE traccountsaldo SET Saldo = OpeningBalance + Debet - Kredit WHERE NoAccount LIKE '1%' OR NoAccount LIKE '5%' OR NoAccount LIKE '6%' OR NoAccount LIKE '8%';
UPDATE traccountsaldo SET Saldo = OpeningBalance + Kredit - Debet WHERE NoAccount LIKE '2%' OR NoAccount LIKE '3%' OR NoAccount LIKE '4%' OR NoAccount LIKE '9%';

SELECT Kredit - Debet INTO MyPenjualan FROM traccountsaldo WHERE NoAccount = '30000000';
SELECT Debet - Kredit INTO MyHPP FROM traccountsaldo WHERE NoAccount = '50000000';
SELECT Debet - Kredit INTO MyBiayaOp FROM traccountsaldo WHERE NoAccount = '60000000';
SELECT Kredit - Debet INTO MyPendapatanOp FROM traccountsaldo WHERE NoAccount = '40000000';
SELECT Kredit - Debet INTO MyPendapatanLain FROM traccountsaldo WHERE NoAccount = '90000000';
SELECT Debet - Kredit INTO MyBiayaLain FROM traccountsaldo WHERE NoAccount = '80000000';
UPDATE traccountsaldo SET Saldo = MyPenjualan + MyPendapatanLain + MyPendapatanOp - MyHPP - MyBiayaOp - MyBiayaLain, Debet = MyHPP + MyBiayaOp + MyBiayaLain, Kredit = MyPenjualan + MyPendapatanOp + MyPendapatanLain WHERE NoAccount = '26040000';

SELECT MAX(LevelAccount) INTO MaxLevel  FROM traccount ;
SET MyLevel = MaxLevel;
loop_label:  LOOP
  IF  MyLevel < 0 THEN
    LEAVE  loop_label;
  END  IF;
  UPDATE traccountsaldo
  INNER JOIN
  (SELECT  NoParent, SUM(Debet) AS MyDebet, SUM(Kredit) AS MyKredit
	  FROM traccountsaldotemp
  	WHERE LevelAccount = MyLevel GROUP BY NoParent) A ON A.NoParent = traccountsaldo.NoAccount
  SET traccountsaldo.Debet = A.MyDebet, traccountsaldo.Kredit = A.MyKredit;
  UPDATE traccountsaldotemp
  INNER JOIN  traccountsaldo ON traccountsaldo.NoAccount = traccountsaldotemp.NoAccount
  SET traccountsaldotemp.Debet = traccountsaldo.Debet, traccountsaldotemp.Kredit = traccountsaldo.Kredit;
  SET MyLevel = MyLevel - 1;
  ITERATE  loop_label;
END LOOP;
UPDATE traccountsaldo SET Saldo = OpeningBalance + Debet - Kredit WHERE NoAccount LIKE '1%' OR NoAccount LIKE '5%' OR NoAccount LIKE '6%' OR NoAccount LIKE '8%';
UPDATE traccountsaldo SET Saldo = OpeningBalance + Kredit - Debet WHERE NoAccount LIKE '2%' OR NoAccount LIKE '3%' OR NoAccount LIKE '4%' OR NoAccount LIKE '9%';

SELECT SUM(Debet), SUM(Kredit) INTO MyTotalDebet, MyTotalKredit FROM traccountsaldo WHERE NoAccount = '10000000' OR NoAccount = '20000000' ;
UPDATE traccountsaldo SET Saldo = 0, Kredit = MyTotalKredit, Debet = MyTotalDebet WHERE NoAccount = '00000000';

END$$
DELIMITER ;

