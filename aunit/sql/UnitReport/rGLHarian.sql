DELIMITER $$

DROP PROCEDURE IF EXISTS `rGLHarian`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rGLHarian`(IN xTipe VARCHAR(50),IN xTgl1 DATE,IN xTgl2 DATE, IN xLeaseKode VARCHAR(10), IN xLokasiKode VARCHAR(15),
OUT BiayaOp DECIMAL(18,2), OUT Pendapatan DECIMAL(18,2), OUT SaldoAwal DECIMAL(18,2), 
OUT KoreksiPenjualan DECIMAL(18,2),OUT KoreksiHPP DECIMAL(18,2),OUT BiayaNonOp DECIMAL(18,2),OUT PendapatanNonOp DECIMAL(18,2),
OUT BiayaSD2Op DOUBLE, OUT BiayaSD1Op DOUBLE, OUT PendapatanOpLain DOUBLE, OUT ReturPenjualan DOUBLE, OUT SumLR1 DOUBLE, OUT SumLR2 DOUBLE, OUT SumLR DOUBLE,
OUT JumSK DOUBLE, OUT JumRK DOUBLE)
BEGIN
/* CALL rGLHarian('Pendapatan RLH','2020-02-01','2020-02-29',
	@BiayaOp , @Pendapatan ,@SaldoAwal , @KoreksiPenjualan ,@KoreksiHPP ,@BiayaNonOp ,@PendapatanNonOp,
	@BiayaSD2Op, @BiayaSD1Op, @PendapatanOpLain, @ReturPenjualan, @SumLR1, @SumLR2, @SumLR, @JumSK, @JumRK); */
   DECLARE Net, KasMasuk, KasKeluar, SaldoAkhir DOUBLE DEFAULT 0;
	
   DECLARE  xTgl0, xTglx DATE;
   DECLARE SumPenjualan, SumHPP DOUBLE;
	SET SaldoAwal =0;
	SET BiayaOp = 0;
	SET Pendapatan = 0;
	SET KoreksiPenjualan = 0;
	SET KoreksiHPP = 0;
	SET BiayaNonOp = 0;
	SET PendapatanNonOp = 0;
	SET SumPenjualan = 0;
	SET SumHPP = 0;
  	SET BiayaSD2Op = 0;
  	SET BiayaSD1Op = 0;
  	SET PendapatanOpLain = 0;
  	SET ReturPenjualan = 0;
   SET SumLR = 0;
  	SET SumLR1 = 0;
  	SET SumLR2 = 0;
  	SET JumSK = 0;
  	SET JumRK = 0;
  	
  							 
	SELECT PerusahaanNama INTO @NamaPerusahaan FROM tzcompany;
   SELECT LokasiKode INTO @POSLokasiKode FROM tuser WHERE LOWER(UserID) = 'foen';
										
	IF (xTipe = "Rugi Laba Harian 1" OR xTipe = "Rugi Laba Harian 2") THEN
		IF @NamaPerusahaan = "CV. ANUGERAH PERDANA" AND @POSLokasiKode = "PlatMerah" THEN
			DROP TEMPORARY TABLE IF EXISTS LabaRugi;
			CREATE TEMPORARY TABLE IF NOT EXISTS LabaRugi AS
			SELECT tvharian.DKNo, tdmotortype.MotorType, MotorNama, tvharian.DKHarga, tvharian.DKHPP, tvharian.BBN, tvharian.Jaket, tvharian.PrgSubsDealer, tvharian.SKNo,
			tvharian.PotonganHarga, tvharian.ReturHarga, tvharian.InsentifSales, tvharian.CusNama, tvharian.LeaseKode, DKScheme + DKSCP + DKScheme2 AS Scheme,
			tvharian.DKHarga - tvharian.DKHPP - tvharian.BBN - tvharian.Jaket - tvharian.PrgSubsDealer - tvharian.PotonganHarga - tvharian.ReturHarga - tvharian.InsentifSales + DKScheme + DKSCP + DKScheme2 AS Laba
			FROM tvharian
			INNER JOIN tdmotortype ON tvharian.MotorType = tdmotortype.MotorType
			INNER JOIN ttdk ON tvharian.DKNo = ttdk.DKNo
			WHERE (SKTgl BETWEEN  xTgl1 AND xTgl2 )
			ORDER BY SKNo,SKTgl,SKJam, MotorType;
		ELSE
			DROP TEMPORARY TABLE IF EXISTS LabaRugi;
			 CREATE TEMPORARY TABLE IF NOT EXISTS LabaRugi AS
			SELECT DKNo, tdmotortype.MotorType, MotorNama, DKHarga, DKHPP, BBN, Jaket, PrgSubsDealer,SKNo,
			PotonganHarga, ReturHarga, InsentifSales, CusNama, LeaseKode, Scheme,
			DKHarga - DKHPP - BBN - Jaket - PrgSubsDealer - PotonganHarga - ReturHarga - InsentifSales + Scheme AS Laba
			FROM tvharian
			INNER JOIN tdmotortype ON tvharian.MotorType = tdmotortype.MotorType
			WHERE (SKTgl BETWEEN  xTgl1 AND xTgl2 )
			ORDER BY SKNo,SKTgl,SKJam, MotorType;
		END IF;

		IF @NamaPerusahaan = "CV. ANUGERAH PERDANA" AND @POSLokasiKode = "PlatMerah" THEN
			UPDATE LabaRugi SET Scheme = -Scheme WHERE SKNo LIKE 'RK%';
			UPDATE LabaRugi SET Laba = Laba + Scheme + Scheme WHERE SKNo LIKE 'RK%';
		END IF;

		/* Hitung Saldo Awal */
		SET xTgl0 = DATE_ADD(LAST_DAY(DATE_ADD(xTgl1, INTERVAL -1 MONTH)), INTERVAL 1 DAY);
		SET xTglx = DATE_ADD(xTgl1, INTERVAL -1 DAY);
		IF xTgl1 = DATE_ADD(LAST_DAY(DATE_ADD(xTgl1, INTERVAL -1 MONTH)), INTERVAL 1 DAY) THEN
			SET xTglx = DATE_ADD(LAST_DAY(DATE_ADD(xTgl1, INTERVAL -1 MONTH)), INTERVAL 1 DAY);
		END IF;

		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO BiayaOp
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '60'
		AND ( NoAccount NOT IN('60050000', '60050000', '60060000', '60070000', '60020000','60380000') OR LEFT(GLLink,2) <> 'SK')
		AND ( NoAccount NOT IN('60050000', '60050000', '60060000', '60070000', '60020000','60380000') OR LEFT(GLLink,2) <> 'RK')
		AND ttgeneralledgerit.TglGL  BETWEEN  xTgl0 AND xTglx;

		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO BiayaSD2Op
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE  (NoAccount = '30040000'
		AND LEFT(ttgeneralledgerhd.GLLink,2) <> 'SK' AND LEFT(ttgeneralledgerhd.GLLink,2) <> 'KK' AND LEFT(ttgeneralledgerhd.GLLink,2) <> 'RK' )
		AND ttgeneralledgerit.TglGL  BETWEEN  xTgl0 AND xTglx;
		SET BiayaOp = BiayaOp + BiayaSD2Op;

		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO BiayaSD2Op
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		INNER JOIN ttkk ON ttgeneralledgerhd.GLLink = ttkk.KKNo 
		WHERE  (NoAccount = '30040000'
		AND LEFT(ttgeneralledgerhd.GLLink,2) <> 'SK' AND LEFT(ttgeneralledgerhd.GLLink,2) <> 'RK'  )
		AND ttkk.KKJenis <> 'SubsidiDealer2'
		AND ttgeneralledgerit.TglGL  BETWEEN  xTgl0 AND xTglx;
		SET BiayaOp = BiayaOp + BiayaSD2Op;

		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO BiayaSD1Op
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE  (NoAccount = '30030000'
		AND (LEFT(ttgeneralledgerhd.GLLink,2) <> 'SK' AND LEFT(ttgeneralledgerhd.GLLink,2) <> 'RK' OR LEFT(ttgeneralledgerhd.GLLink,2) = 'NA'))
		AND ttgeneralledgerit.TglGL  BETWEEN  xTgl0 AND xTglx;
		SET BiayaOp = BiayaOp + BiayaSD1Op;

		SELECT  IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO BiayaNonOp
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE  LEFT(NoAccount,2) = '80'
		AND ttgeneralledgerit.TglGL  BETWEEN  xTgl0 AND xTglx;

		IF @NamaPerusahaan = "CV. ANUGERAH PERDANA" AND @POSLokasiKode = "PlatMerah" THEN
			SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO Pendapatan
			FROM ttgeneralledgerit
			INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
			WHERE LEFT(NoAccount,2) = '40'
			AND NoAccount <> '40990000' AND NoAccount <> '40990000' AND LEFT(NoAccount,4) <> '4001' AND LEFT(NoAccount,4) <> '4002' AND NoAccount <> '40040500' AND NoAccount <> '40040200' AND NoAccount <> '40040400'
			AND ttgeneralledgerit.TglGL  BETWEEN  xTgl0 AND xTglx;
		ELSE
			SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO Pendapatan
			FROM ttgeneralledgerit
			INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
			WHERE LEFT(NoAccount,2) = '40'
			AND NoAccount <> '40990000' AND NoAccount <> '40990000' AND LEFT(NoAccount,4) <> '4001' AND LEFT(NoAccount,4) <> '4002'
			AND ttgeneralledgerit.TglGL  BETWEEN  xTgl0 AND xTglx;
		END IF;

		IF @NamaPerusahaan = "CV. ANUGERAH PERDANA" AND @POSLokasiKode = "PlatMerah" THEN
			SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO PendapatanOpLain
			FROM ttgeneralledgerit
			INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
			WHERE (NoAccount = '40990000' OR NoAccount = '40990000' OR LEFT(NoAccount,4) = '4001' OR LEFT(NoAccount,4) = '4002' OR NoAccount = '40040500' OR NoAccount = '40040200' OR NoAccount = '40040400'  )
			AND (LEFT(ttgeneralledgerhd.GLLink,2) <> 'SK' AND LEFT(ttgeneralledgerhd.GLLink,2) <> 'RK'  OR LEFT(ttgeneralledgerhd.GLLink,2) = 'NA')
			AND ttgeneralledgerit.TglGL  BETWEEN  xTgl0 AND xTglx;
		ELSE
			SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO PendapatanOpLain
			FROM ttgeneralledgerit
			INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
			WHERE (NoAccount = '40990000' OR NoAccount = '40990000' OR LEFT(NoAccount,4) = '4001' OR LEFT(NoAccount,4) = '4002')
			AND (LEFT(ttgeneralledgerhd.GLLink,2) <> 'SK' AND LEFT(ttgeneralledgerhd.GLLink,2) <> 'RK'  OR LEFT(ttgeneralledgerhd.GLLink,2) = 'NA')
			AND ttgeneralledgerit.TglGL  BETWEEN  xTgl0 AND xTglx;
		END IF;
		SET Pendapatan = Pendapatan + PendapatanOpLain;

		SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO PendapatanNonOp
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '90'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl0 AND xTglx
		AND ttgeneralledgerit.TglGL  = xTgl1;

		SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO KoreksiPenjualan
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,5) = '30010' AND LEFT(GLLink,2) <> 'SK'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl0 AND xTglx;

		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO ReturPenjualan
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,5) = '30020' AND LEFT(GLLink,2) <> 'RK'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl0 AND xTglx;

		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO KoreksiHPP
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,5) = '50000' AND LEFT(GLLink,2) <> 'SK' AND LEFT(GLLink,2) <> 'RK'
		AND ttgeneralledgerit.TglGL BETWEEN xTgl0 AND xTglx;

		IF @NamaPerusahaan = "CV. ANUGERAH PERDANA" AND @POSLokasiKode = "PlatMerah" THEN
			SELECT IFNULL(SUM(tvharian.DKHarga - tvharian.DKHPP - tvharian.BBN - tvharian.Jaket - tvharian.PrgSubsDealer - tvharian.PotonganHarga - tvharian.ReturHarga - tvharian.InsentifSales + DKScheme + DKSCP + DKScheme2),0) INTO SumLR1
			FROM tvharian
			INNER JOIN ttdk ON tvharian.DKNo = ttdk.DKNo
			WHERE (SKTgl BETWEEN xTgl0 AND xTglx)  AND SKNO NOT LIKE 'RK%';

			SELECT IFNULL(SUM(tvharian.DKHarga - tvharian.DKHPP - tvharian.BBN - tvharian.Jaket - tvharian.PrgSubsDealer - tvharian.PotonganHarga - tvharian.ReturHarga - tvharian.InsentifSales + DKScheme + DKSCP + DKScheme2 -(2*(DKScheme + DKSCP + DKScheme2))), 0) INTO SumLR2
			FROM tvharian
			INNER JOIN ttdk ON tvharian.DKNo = ttdk.DKNo
			WHERE (SKTgl BETWEEN xTgl0 AND xTglx)  AND SKNo LIKE 'RK%';
			SET SumLR = SumLR1 + SumLR2;
		ELSE
			SELECT IFNULL(SUM(DKHarga - DKHPP - BBN - Jaket - PrgSubsDealer - PotonganHarga - ReturHarga - InsentifSales + Scheme ),0) INTO  SumLR
			FROM tvharian WHERE (SKTgl BETWEEN xTgl0 AND xTglx );
		END IF;

		SET SaldoAwal = SumLR + Pendapatan + PendapatanNonOp - BiayaOp - BiayaNonOp + KoreksiPenjualan - ReturPenjualan - KoreksiHPP;

		IF MONTH(DATE_ADD(xTgl1, INTERVAL -1 DAY)) <> MONTH(xTgl1) THEN
			SET SaldoAwal = 0;
		END IF;


		SET BiayaOp = 0;
		SET Pendapatan = 0;
		SET KoreksiPenjualan = 0;
		SET KoreksiHPP = 0;
		SET BiayaNonOp = 0;
		SET PendapatanNonOp = 0;
		SET SumPenjualan = 0;
		SET SumHPP = 0;
		SET BiayaSD2Op = 0;
		SET BiayaSD1Op = 0;
		SET PendapatanOpLain = 0;
		SET ReturPenjualan = 0;
		SET SumLR1 = 0;
		SET SumLR2 = 0;
		SET SumLR = 0;

		/*Hitung Periode */
		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO BiayaOp
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '60'
		AND ( NoAccount NOT IN('60050000', '60050000', '60060000', '60070000', '60020000','60380000') OR LEFT(GLLink,2) <> 'SK')
		AND ( NoAccount NOT IN('60050000', '60050000', '60060000', '60070000', '60020000','60380000') OR LEFT(GLLink,2) <> 'RK')
		AND ttgeneralledgerit.TglGL  BETWEEN  xTgl1 AND xTgl2;

		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO BiayaSD2Op
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE  (NoAccount = '30040000'
		AND LEFT(ttgeneralledgerhd.GLLink,2) <> 'SK' AND LEFT(ttgeneralledgerhd.GLLink,2) <> 'KK' AND LEFT(ttgeneralledgerhd.GLLink,2) <> 'RK' )
		AND ttgeneralledgerit.TglGL  BETWEEN  xTgl1 AND xTgl2;
		SET BiayaOp = BiayaOp + BiayaSD2Op;

		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO BiayaSD2Op
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		INNER JOIN ttkk ON ttgeneralledgerhd.GLLink = ttkk.KKNo 
		WHERE  (NoAccount = '30040000'
		AND LEFT(ttgeneralledgerhd.GLLink,2) <> 'SK' AND LEFT(ttgeneralledgerhd.GLLink,2) <> 'RK'  )
		AND ttkk.KKJenis <> 'SubsidiDealer2'
		AND ttgeneralledgerit.TglGL  BETWEEN  xTgl1 AND xTgl2;
		SET BiayaOp = BiayaOp + BiayaSD2Op;

		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO BiayaSD1Op
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE  (NoAccount = '30030000'
		AND (LEFT(ttgeneralledgerhd.GLLink,2) <> 'SK' AND LEFT(ttgeneralledgerhd.GLLink,2) <> 'RK' OR LEFT(ttgeneralledgerhd.GLLink,2) = 'NA'))
		AND ttgeneralledgerit.TglGL  BETWEEN  xTgl1 AND xTgl2;
		SET BiayaOp = BiayaOp + BiayaSD1Op;

		SELECT  IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO BiayaNonOp
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE  LEFT(NoAccount,2) = '80'
		AND ttgeneralledgerit.TglGL  BETWEEN  xTgl1 AND xTgl2;

		IF @NamaPerusahaan = "CV. ANUGERAH PERDANA" AND @POSLokasiKode = "PlatMerah" THEN
			SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO Pendapatan
			FROM ttgeneralledgerit
			INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
			WHERE LEFT(NoAccount,2) = '40'
			AND NoAccount <> '40990000' AND NoAccount <> '40990000' AND LEFT(NoAccount,4) <> '4001' AND LEFT(NoAccount,4) <> '4002' AND NoAccount <> '40040500' AND NoAccount <> '40040200' AND NoAccount <> '40040400'
			AND ttgeneralledgerit.TglGL  BETWEEN  xTgl1 AND xTgl2;
		ELSE
			SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO Pendapatan
			FROM ttgeneralledgerit
			INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
			WHERE LEFT(NoAccount,2) = '40'
			AND NoAccount <> '40990000' AND NoAccount <> '40990000' AND LEFT(NoAccount,4) <> '4001' AND LEFT(NoAccount,4) <> '4002'
			AND ttgeneralledgerit.TglGL  BETWEEN  xTgl1 AND xTgl2;
		END IF;

		IF @NamaPerusahaan = "CV. ANUGERAH PERDANA" AND @POSLokasiKode = "PlatMerah" THEN
			SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO PendapatanOpLain
			FROM ttgeneralledgerit
			INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
			WHERE (NoAccount = '40990000' OR NoAccount = '40990000' OR LEFT(NoAccount,4) = '4001' OR LEFT(NoAccount,4) = '4002' OR NoAccount = '40040500' OR NoAccount = '40040200' OR NoAccount = '40040400'  )
			AND (LEFT(ttgeneralledgerhd.GLLink,2) <> 'SK' AND LEFT(ttgeneralledgerhd.GLLink,2) <> 'RK'  OR LEFT(ttgeneralledgerhd.GLLink,2) = 'NA')
			AND ttgeneralledgerit.TglGL  BETWEEN  xTgl1 AND xTgl2;
		ELSE
			SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO PendapatanOpLain
			FROM ttgeneralledgerit
			INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
			WHERE (NoAccount = '40990000' OR NoAccount = '40990000' OR LEFT(NoAccount,4) = '4001' OR LEFT(NoAccount,4) = '4002')
			AND (LEFT(ttgeneralledgerhd.GLLink,2) <> 'SK' AND LEFT(ttgeneralledgerhd.GLLink,2) <> 'RK'  OR LEFT(ttgeneralledgerhd.GLLink,2) = 'NA')
			AND ttgeneralledgerit.TglGL  BETWEEN  xTgl1 AND xTgl2;
		END IF;
		SET Pendapatan = Pendapatan + PendapatanOpLain;

		SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO PendapatanNonOp
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,2) = '90'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl1 AND xTgl2
		AND ttgeneralledgerit.TglGL  = xTgl1;

		SELECT IFNULL(SUM(ttgeneralledgerit.KreditGL)-SUM(ttgeneralledgerit.DebetGL),0) INTO KoreksiPenjualan
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,5) = '30010' AND LEFT(GLLink,2) <> 'SK'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl1 AND xTgl2;

		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO ReturPenjualan
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,5) = '30020' AND LEFT(GLLink,2) <> 'RK'
		AND ttgeneralledgerit.TglGL  BETWEEN xTgl1 AND xTgl2;

		SELECT IFNULL(SUM(ttgeneralledgerit.DebetGL)-SUM(ttgeneralledgerit.KreditGL),0) INTO KoreksiHPP
		FROM ttgeneralledgerit
		INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
		WHERE LEFT(NoAccount,5) = '50000' AND LEFT(GLLink,2) <> 'SK' AND LEFT(GLLink,2) <> 'RK'
		AND ttgeneralledgerit.TglGL BETWEEN xTgl1 AND xTgl2;

		SELECT IFNULL(SUM(DKHarga - DKHPP - BBN - Jaket - PrgSubsDealer - PotonganHarga - ReturHarga - InsentifSales ),0) INTO SumLR1
		FROM tvharian WHERE SKTgl BETWEEN xTgl1 AND xTgl2;
		IF @NamaPerusahaan = "CV. ANUGERAH PERDANA" AND @POSLokasiKode = "PlatMerah" THEN
			SET SumLR2 = 0;
			SELECT SUM(Laba) INTO SumLR2 FROM LabaRugi;
		ELSE
			SELECT IFNULL(SUM(DKHarga - DKHPP - BBN - Jaket - PrgSubsDealer - PotonganHarga - ReturHarga - InsentifSales + Scheme),0) INTO SumLR2
			FROM tvharian WHERE (SKTgl BETWEEN  xTgl1 AND xTgl2 );
		END IF;

		SELECT COUNT(SKNo) INTO JumSK FROM tvharian WHERE LEFT(SKNo,2) = 'SK' AND
		(SKTgl BETWEEN  DATE_ADD(LAST_DAY(DATE_ADD(xTgl1, INTERVAL -1 MONTH)), INTERVAL 1 DAY)  AND xTgl2);
		SELECT COUNT(SKNo) INTO JumRK FROM tvharian WHERE LEFT(SKNo,2) = 'RK' AND
		(SKTgl BETWEEN  DATE_ADD(LAST_DAY(DATE_ADD(xTgl1, INTERVAL -1 MONTH)), INTERVAL 1 DAY)  AND xTgl2);
		
		SET @MyQuery = CONCAT("SELECT * FROM LabaRugi;");
		
	END IF;

	IF xTipe = "Biaya RLH" THEN
    SET @MyQuery = CONCAT("    SELECT 'BIAYA OPERASIONAL' AS JenisBiaya, NoAccount, NamaAccount, GLLink, TglGL, KeteranganGL, DebetGL-KreditGL As Nominal, CONCAT(CAST(GLAutoN AS CHAR),NoAccount) AS GroupCOA
         FROM vglall
         WHERE LEFT(NoAccount,2) = '60'
         AND ( NoAccount NOT IN('60050000', '60050000', '60060000', '60070000', '60020000','60380000') OR LEFT(GLLink,2) <> 'SK')
         AND ( NoAccount NOT IN('60050000', '60050000', '60060000', '60070000', '60020000','60380000') OR LEFT(GLLink,2) <> 'RK')
         AND TglGL BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "'
         UNION
         SELECT 'BIAYA OPERASIONAL' AS JenisBiaya, NoAccount, NamaAccount, GLLink, TglGL, KeteranganGL, DebetGL - KreditGL AS Nominal, CONCAT(CAST(GLAutoN AS CHAR),NoAccount) AS GroupCOA
         FROM vglall WHERE NoAccount = '30040000' AND LEFT(GLLink, 2) <> 'SK' AND LEFT(GLLink, 2) <> 'KK' AND LEFT(GLLink, 2) <> 'RK'
         AND TglGL BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "'
         UNION
         SELECT 'BIAYA OPERASIONAL' AS JenisBiaya, ttgeneralledgerit.NoAccount, NamaAccount, GLLink, ttgeneralledgerhd.TglGL, KeteranganGL, DebetGL - KreditGL AS Nominal,
         CONCAT(CAST(GLAutoN AS CHAR), ttgeneralledgerit.NoAccount) AS GroupCOA
         FROM ttgeneralledgerit
         INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL
         INNER JOIN ttkk ON ttgeneralledgerhd.GLLink = ttkk.KKNo AND ttkk.NoGL = ttgeneralledgerhd.NoGL
         INNER JOIN traccount ON ttgeneralledgerit.NoAccount = traccount.NoAccount
         WHERE (ttgeneralledgerit.NoAccount = '30040000' AND LEFT(ttgeneralledgerhd.GLLink, 2) <> 'SK'  AND LEFT(ttgeneralledgerhd.GLLink, 2) <> 'RK'  )
         AND ttkk.KKJenis <> 'SubsidiDealer2'
         AND ttgeneralledgerit.TglGL BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "'
         UNION
         SELECT 'BIAYA OPERASIONAL' AS JenisBiaya, NoAccount, NamaAccount, GLLink, TglGL, KeteranganGL, DebetGL - KreditGL AS Nominal, CONCAT(CAST(GLAutoN AS CHAR),NoAccount) AS GroupCOA
         FROM vglall WHERE (NoAccount = '30030000' AND (LEFT(GLLink, 2) <> 'SK' AND LEFT(GLLink, 2) <> 'RK'  OR LEFT(GLLink, 2) = 'NA'))
         AND TglGL BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "'
         UNION
         SELECT 'BIAYA NON OPERASIONAL' AS JenisBiaya, NoAccount, NamaAccount, GLLink, TglGL, KeteranganGL, DebetGL-KreditGL As Nominal, CONCAT(CAST(GLAutoN AS CHAR),NoAccount) AS GroupCOA
         FROM vglall
         WHERE LEFT(NoAccount,2) = '80'
         AND TglGL BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "'
         ORDER BY TglGL , JenisBiaya DESC, NoAccount");
	END IF;
	IF xTipe = "Pendapatan RLH" THEN
		IF @NamaPerusahaan = "CV. ANUGERAH PERDANA" AND @POSLokasiKode = "PlatMerah" THEN
			SET @MyQuery = CONCAT("SELECT 'PENDAPATAN OPERASIONAL' AS JenisBiaya, NoAccount, NamaAccount, GLLink, TglGL, KeteranganGL, KreditGL - DebetGL As Nominal 
            FROM vglall 
            WHERE (LEFT(NoAccount,2) = '40' AND NoAccount <> '40040500' AND NoAccount <> '40040200' AND NoAccount <> '40040400')
            AND TglGL BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "'  
            UNION 
            SELECT 'PENDAPATAN OPERASIONAL' AS JenisBiaya, NoAccount, NamaAccount, GLLink, TglGL, KeteranganGL, KreditGL - DebetGL As Nominal 
            FROM vglall 
            WHERE (NoAccount = '40990000' OR NoAccount = '40990000' OR LEFT(NoAccount,4) = '4001' OR LEFT(NoAccount,4) = '4002' OR NoAccount = '40040500' OR NoAccount = '40040200' OR NoAccount = '40040400') 
            AND (LEFT(GLLink,2) <> 'SK' AND LEFT(GLLink,2) <> 'RK'  OR LEFT(GLLink,2) = 'NA') 
            AND TglGL BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "' 
            UNION 
            SELECT 'PENDAPATAN NON OPERASIONAL' AS JenisBiaya, NoAccount, NamaAccount, GLLink, TglGL, KeteranganGL, KreditGL - DebetGL As Nominal 
            FROM vglall 
            WHERE LEFT(NoAccount,2) = '90'  
            AND TglGL BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "' 
            ORDER BY TglGL, JenisBiaya DESC, NoAccount;");
		ELSE
			SET @MyQuery = CONCAT("SELECT 'PENDAPATAN OPERASIONAL' AS JenisBiaya, NoAccount, NamaAccount, GLLink, TglGL, KeteranganGL, KreditGL - DebetGL As Nominal 
            FROM vglall 
            WHERE LEFT(NoAccount,2) = '40' 
            AND TglGL BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "' 
            UNION 
            SELECT 'PENDAPATAN OPERASIONAL' AS JenisBiaya, NoAccount, NamaAccount, GLLink, TglGL, KeteranganGL, KreditGL - DebetGL As Nominal 
            FROM vglall 
            WHERE (NoAccount = '40990000' OR NoAccount = '40990000' OR LEFT(NoAccount,4) = '4001' OR LEFT(NoAccount,4) = '4002') 
            AND (LEFT(GLLink,2) <> 'SK' AND LEFT(GLLink,2) <> 'RK'  OR LEFT(GLLink,2) = 'NA') 
            AND TglGL BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "' 
            UNION 
            SELECT 'PENDAPATAN NON OPERASIONAL' AS JenisBiaya, NoAccount, NamaAccount, GLLink, TglGL, KeteranganGL, KreditGL - DebetGL As Nominal 
            FROM vglall 
            WHERE LEFT(NoAccount,2) = '90'  
            AND TglGL BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "' 
            ORDER BY TglGL, JenisBiaya DESC, NoAccount ;");
		END IF;
	END IF;
	
	IF xTipe = "Penjualan 1" OR xTipe = "Penjualan 2" OR xTipe = "Penjualan 3" THEN
		DROP TEMPORARY TABLE IF EXISTS Penjualan;
		SET @MyQuery = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS Penjualan AS 
		SELECT DKJenis, tdmotortype.MotorType,tdmotortype.MotorNama, CusNama, DKHarga, DKDPTerima, PrgSubsSupplier, PrgSubsDealer,PrgSubsFincoy,ProgramSubsidi,  
      DKNettoHTK, DKDP2140500HTK,  DKDPPKTerbayarTotal, DKNettoKMPKons ,  
      DKNettoPK, DKDP1130100PK AS DKDP1130100PK, 
      DKNetto, DKNettoKMPLease, SKNo,  
      0 AS Terbayar, 
      0 AS PiutangKons, 
      0 AS PiutangLease, 
      LeaseKode, tvharian.MotorNoMesin 
      FROM tvharian  
      INNER JOIN tdmotortype ON tvharian.MotorType = tdmotortype.MotorType 
      WHERE (SKTgl BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "' )
      AND LokasiKode LIKE '%" , xLokasiKode , "' 
      Order By  SKTgl, SKJam, MotorType ;");
		PREPARE STMT FROM @MyQuery;
		EXECUTE Stmt;  
		
		UPDATE Penjualan SET Terbayar = DKNettoHTK + DKDP2140500HTK + DKDPPKTerbayarTotal + DKNettoKMPKons;
		UPDATE Penjualan SET PiutangKons = DKNettoHTK + DKNettoPK + DKDP1130100PK - DKDPPKTerbayarTotal - DKNettoKMPKons WHERE DKJenis = 'Tunai';
		UPDATE Penjualan SET PiutangKons = DKNettoPK +  DKDP1130100PK - DKDPPKTerbayarTotal  WHERE DKJenis <> 'Tunai';
		UPDATE Penjualan SET PiutangLease = 0 WHERE DKJenis = 'Tunai';
		UPDATE Penjualan SET PiutangLease = DKNetto -  DKNettoKMPLease WHERE DKJenis <> 'Tunai';
		
		SET @MyQuery = CONCAT("SELECT * FROM Penjualan;");
	END IF;

	IF xTipe = "Kas Harian Saldo" THEN
		DROP TEMPORARY TABLE IF EXISTS KasJenis;
		CREATE TEMPORARY TABLE IF NOT EXISTS KasJenis AS
		SELECT KMNo, KasGroup, KMJenis AS Jenis, KMPerson AS Person, MotorType, KMNominal AS Nominal, LeaseKode, KMMemo AS Memo
		FROM vtkmjenis
		WHERE (KMTgl BETWEEN xTgl1 AND xTgl2)
		AND NoGL <> '--'
		ORDER BY KMJenis, KMTgl;
		CREATE INDEX KMNo ON KasJenis(KMNo);
		ALTER IGNORE TABLE KasJenis MODIFY  COLUMN KasGroup VARCHAR(50);

		DROP TEMPORARY TABLE IF EXISTS TypeGrowth;
		CREATE TEMPORARY TABLE IF NOT EXISTS TypeGrowth AS
		SELECT ttkm.KMNo AS MyGroup, tmotor.MotorType AS Keterangan, KMTgl, KMJenis, NoGL FROM ttkm INNER JOIN tmotor ON ttkm.KMLink =  tmotor.DKNo
		WHERE (KMTgl BETWEEN xTgl1 AND xTgl2 AND NoGL <> '--' AND KMJenis <> 'KM Dari Bank' AND tmotor.DKNo <> '--' )
		ORDER BY KMJenis, KMNo;
		CREATE INDEX MyGroup ON TypeGrowth(MyGroup);

		UPDATE KasJenis
		INNER JOIN TypeGrowth ON KasJenis.KMNo = TypeGrowth.MyGroup
		SET KasJenis.MotorType = TypeGrowth.Keterangan;

		INSERT INTO KasJenis
		SELECT KKNo, 'PENGELUARAN KAS' AS KasGroup, 'Pengeluaran Kas' AS Jenis, traccount.NamaAccount  AS Person,'' AS TypeMotor, SUM(ttgeneralledgerit.DebetGL) AS Nominal, '' AS LeaseKode, '' AS Memo
		FROM ttgeneralledgerhd
		INNER JOIN Ttgeneralledgerit ON ttgeneralledgerhd.NoGL = ttgeneralledgerit.NoGL
		INNER JOIN traccount  ON traccount.NoAccount = ttgeneralledgerit.NoAccount
		INNER JOIN ttkk ON ttkk.NoGL = ttgeneralledgerhd.NoGL
		WHERE ttgeneralledgerit.DebetGL > 0 AND
		ttgeneralledgerhd.TglGL BETWEEN xTgl1 AND xTgl2
		AND LEFT(ttgeneralledgerhd.GLLink,2) ='KK'
		GROUP BY  ttgeneralledgerit.NoAccount
		ORDER BY traccount.NamaAccount;

		SELECT SUM(Nominal) INTO KasMasuk FROM KasJenis WHERE KasGroup = "PENERIMAAN KAS";
		SELECT SUM(Nominal) INTO KasKeluar FROM KasJenis WHERE KasGroup = "PENGELUARAN KAS";
		SET  Net = KasMasuk - KasKeluar;

		SET SaldoAwal = 0;
		SET KasMasuk = 0;
		SET KasKeluar = 0;
		SET xTgl0 = DATE_ADD(xTgl1, INTERVAL -10000 DAY);
		SELECT IFNULL(MAX(SaldoKasTgl),xTgl0) INTO xTgl0  FROM tsaldokas WHERE  SaldoKasTgl  < xTgl1 AND LokasiKode LIKE '%';
		SELECT IFNULL(SaldoKasSaldo,0) INTO SaldoAwal FROM tsaldokas
		WHERE  SaldoKasTgl = xTgl0 AND LokasiKode LIKE CONCAT('%',xLokasiKode) ;
		SET xTgl0 = DATE_ADD(xTgl0, INTERVAL 1 DAY);
		SET xTglx = DATE_ADD(xTgl1, INTERVAL -1 DAY);
		SELECT IFNULL(SUM(KKNominal),0) INTO KasKeluar FROM ttkk
		WHERE  (KKTgl BETWEEN xTgl0 AND xTglx) AND (LokasiKode LIKE CONCAT('%',xLokasiKode))
		AND ttkk.NoGL <> '--';
		SELECT IFNULL(SUM(KMNominal),0) INTO KasMasuk FROM ttkm
		WHERE  KMTgl BETWEEN xTgl0 AND xTglx
		AND LokasiKode LIKE CONCAT('%',xLokasiKode) AND ttkm.NoGL <> '--';
		SET SaldoAwal = SaldoAwal + KasMasuk - KasKeluar;

		SET SaldoAkhir = SaldoAwal + Net;

		SET BiayaOp = Net;
		SET Pendapatan = SaldoAkhir;

		SET @MyQuery = CONCAT("SELECT * FROM KasJenis ; ");
	END IF;
	
	IF xTipe = "Kas Harian Operasional" THEN
		DROP TEMPORARY TABLE IF EXISTS KasJenis;
		CREATE TEMPORARY TABLE IF NOT EXISTS KasJenis AS
		SELECT KMNo, KasGroup, KMJenis AS Jenis, KMPerson AS Person, MotorType, KMNominal AS Nominal, LeaseKode, KMMemo AS Memo
		FROM vtkmjenis
		WHERE (KMTgl BETWEEN xTgl1 AND xTgl2)
		AND NoGL <> '--' AND KMJenis <> 'KM Dari Bank'
		ORDER BY KMJenis, KMNo;
		CREATE INDEX KMNo ON KasJenis(KMNo);
		ALTER IGNORE TABLE KasJenis MODIFY  COLUMN KasGroup VARCHAR(50);

		DROP TEMPORARY TABLE IF EXISTS TypeGrowth;
		CREATE TEMPORARY TABLE IF NOT EXISTS TypeGrowth AS
		SELECT ttkm.KMNo AS MyGroup, tmotor.MotorType AS Keterangan, KMTgl, KMJenis, NoGL FROM ttkm INNER JOIN tmotor ON ttkm.KMLink =  tmotor.DKNo
		WHERE (KMTgl BETWEEN xTgl1 AND xTgl2 AND NoGL <> '--' AND KMJenis <> 'KM Dari Bank' AND tmotor.DKNo <> '--' )
		ORDER BY KMJenis, KMNo;
		CREATE INDEX MyGroup ON TypeGrowth(MyGroup);

		UPDATE KasJenis
		INNER JOIN TypeGrowth ON KasJenis.KMNo = TypeGrowth.MyGroup
		SET KasJenis.MotorType = TypeGrowth.Keterangan;

		INSERT INTO KasJenis
		SELECT KKNo, 'PENGELUARAN KAS' AS KasGroup, 'Pengeluaran Kas' AS Jenis, traccount.NamaAccount  AS Person,'' AS MotorType, SUM(ttgeneralledgerit.DebetGL) AS Nominal, '' AS LeaseKode, '' AS Memo
		FROM ttgeneralledgerhd
		INNER JOIN Ttgeneralledgerit ON ttgeneralledgerhd.NoGL = ttgeneralledgerit.NoGL
		INNER JOIN traccount  ON traccount.NoAccount = ttgeneralledgerit.NoAccount
		INNER JOIN ttkk ON ttkk.NoGL = ttgeneralledgerhd.NoGL
		WHERE ttgeneralledgerit.DebetGL > 0 AND
		ttgeneralledgerhd.TglGL BETWEEN xTgl1 AND xTgl2
		AND LEFT(ttgeneralledgerhd.GLLink,2) ='KK'
		AND ttkk.kkJenis <> 'KK Ke Bank'
		GROUP BY  ttgeneralledgerit.NoAccount
		ORDER BY traccount.NamaAccount;

		SELECT SUM(Nominal) INTO KasMasuk FROM KasJenis WHERE KasGroup = "PENERIMAAN KAS";
		SELECT SUM(Nominal) INTO KasKeluar FROM KasJenis WHERE KasGroup = "PENGELUARAN KAS";
		SET  Net = KasMasuk - KasKeluar;

		SET SaldoAwal = 0;
		SET KasMasuk = 0;
		SET KasKeluar = 0;
		SET xTgl0 = DATE_ADD(xTgl1, INTERVAL -10000 DAY);
		SELECT IFNULL(MAX(SaldoKasTgl),xTgl0) INTO xTgl0  FROM tsaldokas WHERE  SaldoKasTgl  < xTgl1 AND LokasiKode LIKE '%';
		SELECT IFNULL(SaldoKasSaldo,0) INTO SaldoAwal FROM tsaldokas
		WHERE  SaldoKasTgl = xTgl0 AND LokasiKode LIKE CONCAT('%',xLokasiKode) ;
		SET xTgl0 = DATE_ADD(xTgl0, INTERVAL 1 DAY);
		SET xTglx = DATE_ADD(xTgl1, INTERVAL -1 DAY);
		SELECT IFNULL(SUM(KKNominal),0) INTO KasKeluar FROM ttkk
		WHERE  (KKTgl BETWEEN xTgl0 AND xTglx) AND (LokasiKode LIKE CONCAT('%',xLokasiKode))
		AND ttkk.NoGL <> '--';
		SELECT IFNULL(SUM(KMNominal),0) INTO KasMasuk FROM ttkm
		WHERE  KMTgl BETWEEN xTgl0 AND xTglx
		AND LokasiKode LIKE CONCAT('%',xLokasiKode) AND ttkm.NoGL <> '--';
		SET SaldoAwal = SaldoAwal + KasMasuk - KasKeluar;

		SET SaldoAkhir = SaldoAwal + Net;

		SET BiayaOp = Net;
		SET Pendapatan = SaldoAkhir;

		SET @MyQuery = CONCAT("SELECT * FROM KasJenis ; ");
   END IF;
	
	IF xTipe = "Pengeluaran Kas" THEN
		SET @MyQuery = CONCAT("SELECT 'BIAYA OPERASIONAL' AS JenisBiaya, NoAccount, NamaAccount, GLLink, TglGL, KeteranganGL, DebetGL-KreditGL As Nominal, CONCAT(CAST(GLAutoN AS CHAR),NoAccount) AS GroupCOA 
         FROM vglall 
         WHERE LEFT(NoAccount,2) = '60' 
         AND ( NoAccount NOT IN('60050000', '60050000', '60060000', '60070000', '60020000','60380000') OR LEFT(GLLink,2) <> 'SK')  
         AND ( NoAccount NOT IN('60050000', '60050000', '60060000', '60070000', '60020000','60380000') OR LEFT(GLLink,2) <> 'RK')  
         AND TglGL BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "' 
         UNION 
         SELECT 'BIAYA OPERASIONAL' AS JenisBiaya, NoAccount, NamaAccount, GLLink, TglGL, KeteranganGL, DebetGL - KreditGL AS Nominal, CONCAT(CAST(GLAutoN AS CHAR),NoAccount) AS GroupCOA 
         FROM vglall WHERE NoAccount = '30040000' AND LEFT(GLLink, 2) <> 'SK' AND LEFT(GLLink, 2) <> 'KK' AND LEFT(GLLink, 2) <> 'RK'  
         AND TglGL BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "' 
         UNION 
         SELECT 'BIAYA OPERASIONAL' AS JenisBiaya, ttgeneralledgerit.NoAccount, NamaAccount, GLLink, ttgeneralledgerhd.TglGL, KeteranganGL, DebetGL - KreditGL AS Nominal, 
         CONCAT(CAST(GLAutoN AS CHAR), ttgeneralledgerit.NoAccount) AS GroupCOA 
         FROM ttgeneralledgerit 
         INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL AND ttgeneralledgerit.TglGL = ttgeneralledgerhd.TglGL 
         INNER JOIN ttkk ON ttgeneralledgerhd.GLLink = ttkk.KKNo AND ttkk.NoGL = ttgeneralledgerhd.NoGL  
         INNER JOIN traccount ON ttgeneralledgerit.NoAccount = traccount.NoAccount  
         WHERE (ttgeneralledgerit.NoAccount = '30040000' AND LEFT(ttgeneralledgerhd.GLLink, 2) <> 'SK'  AND LEFT(ttgeneralledgerhd.GLLink, 2) <> 'RK'  ) 
         AND ttkk.KKJenis <> 'SubsidiDealer2' 
         AND ttgeneralledgerit.TglGL BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "' 
         UNION 
         SELECT 'BIAYA OPERASIONAL' AS JenisBiaya, NoAccount, NamaAccount, GLLink, TglGL, KeteranganGL, DebetGL - KreditGL AS Nominal, CONCAT(CAST(GLAutoN AS CHAR),NoAccount) AS GroupCOA 
         FROM vglall WHERE (NoAccount = '30030000' AND (LEFT(GLLink, 2) <> 'SK' AND LEFT(GLLink, 2) <> 'RK'  OR LEFT(GLLink, 2) = 'NA')) 
         AND TglGL BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "' 
         UNION 
         SELECT 'BIAYA NON OPERASIONAL' AS JenisBiaya, NoAccount, NamaAccount, GLLink, TglGL, KeteranganGL, DebetGL-KreditGL As Nominal, CONCAT(CAST(GLAutoN AS CHAR),NoAccount) AS GroupCOA 
         FROM vglall 
         WHERE LEFT(NoAccount,2) = '80'  
         AND TglGL BETWEEN '" , xTgl1 , "' AND '" , xTgl2 , "' 
         ORDER BY TglGL , JenisBiaya DESC, NoAccount");	
	END IF;
	IF xTipe = "Rugi Laba Margin" THEN
		SELECT IFNULL(SUM(DKHarga - DKHPP - BBN - Jaket - PrgSubsDealer - PotonganHarga - ReturHarga - InsentifSales + Scheme),0) INTO SumLR 
      FROM tvharian WHERE (SKTgl BETWEEN  xTgl1 AND xTgl2 ) AND LeaseKode LIKE CONCAT('%',xLeaseKode);
		SET @MyQuery = CONCAT("SELECT DKNo, MotorType, DKHarga, DKHPP, BBN, Jaket, PrgSubsDealer,SKNo, 
         PotonganHarga, ReturHarga, InsentifSales, CusNama, LeaseKode, Scheme, 
         DKHarga - DKHPP - BBN - Jaket - PrgSubsDealer - PotonganHarga - ReturHarga - InsentifSales + Scheme AS Laba 
         FROM tvharian WHERE 
         (SKTgl BETWEEN  '" , xTgl1 , "' AND '" , xTgl2 , "' )  AND LeaseKode Like '%" , xLeaseKode , "' 
         ORDER BY SKTgl,SKJam, MotorType;");
	END IF;
	IF xTipe = "RLM Unit" THEN
		SET @MyQuery = CONCAT("SELECT tvharian.* FROM 
         (SELECT LeaseKode, COUNT(SKNo) AS SKNo, SUM(PrgSubsDealer) AS PrgSubsDealer, SUM(PotonganHarga) PotonganHarga, SUM(ReturHarga) AS ReturHarga,
         SUM(PrgSubsDealer+PotonganHarga+ReturHarga) AS Potongan,  
         SUM(PrgSubsDealer+PotonganHarga+ReturHarga) / COUNT(SKNo) AS PotonganPerUnit,
         SUM(DKHarga - DKHPP - BBN - Jaket - PrgSubsDealer - PotonganHarga - ReturHarga - InsentifSales + Scheme) AS Laba, 
         SUM(DKHarga - DKHPP - BBN - Jaket - PrgSubsDealer - PotonganHarga - ReturHarga - InsentifSales + Scheme)/ COUNT(SKNo)  AS LabaPerUnit, 
         SUM(DKHarga) AS DKHarga, SUM(DKHPP) AS DKHPP, SUM(BBN) AS BBN, SUM(Jaket) AS Jaket, 
         SUM(InsentifSales) AS InsentifSales, SUM(Scheme) AS Scheme
         FROM tvharian  WHERE (SKTgl BETWEEN  '", xTgl1 , "' AND '", xTgl2 ,"')
         GROUP BY LeaseKode) tvharian INNER JOIN tdleasing ON tdleasing.LeaseKode = tvharian.LeaseKode ORDER BY tdleasing.LeaseNo,tdleasing.LeaseKode");
	END IF;

	PREPARE STMT FROM @MyQuery;
	EXECUTE Stmt;  
END$$

DELIMITER ;

