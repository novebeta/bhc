DELIMITER $$
DROP PROCEDURE IF EXISTS `rMotorRiwayatMotor`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rMotorRiwayatMotor`(IN xTipe VARCHAR(25),IN xTgl1 DATE,IN xTgl2 DATE,IN xLokasi VARCHAR(30),IN xTypeMtr VARCHAR(20),IN xWarna VARCHAR(20),IN xKondisi VARCHAR(20),IN xDealer VARCHAR(20),IN xNoMesin VARCHAR(20))
BEGIN
/*
       xTipe    : "tvmotorlokasi" -> Riwayat
                  "tmotor" -> Dealer Asal
                  

       Cara Akses :
       
       *** RIWAYAT ***
       CALL rMotorRiwayatMotor("tvmotorlokasi","2018-01-01","2018-12-31","","","","","","");

       *** DETAIL ***
       CALL rMutasiPDealer('tmotor','2001-01-01','2019-12-31','','','TtPD.PDTgl','ASC');

*/

       SET xLokasi = CONCAT(xLokasi,"%");
       SET xTypeMtr = CONCAT(xTypeMtr,"%");
       SET xWarna = CONCAT(xWarna,"%");
       SET xDealer = CONCAT(xDealer,"%");
       SET xNoMesin = CONCAT(xNoMesin,"%");
       SET xKondisi = CONCAT(xKondisi,"%");
       
       /* RIWAYAT */
       IF xTipe = "tvmotorlokasi" THEN 
          SET @MyTgl1 = DATE_FORMAT(xTgl1,"%Y-%m-%d 00:00:00");
          SET @MyTgl2 = DATE_FORMAT(xTgl2,"%Y-%m-%d 23:59:59");
          
          SET @MyQuery = "SELECT tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType, tmotor.MotorWarna, 
                          tmotor.MotorTahun, tmotor.DealerKode, LokasiMotor.LokasiKode, LokasiMotor.NoTrans, LokasiMotor.Jam, 
                          LokasiMotor.Kondisi, tdmotortype.MotorNama 
                          FROM tmotor 
                          INNER Join 
                            (SELECT vmotorlokasi.MotorAutoN, vmotorlokasi.MotorNoMesin, vmotorlokasi.LokasiKode, vmotorlokasi.NoTrans, vmotorlokasi.Jam, vmotorlokasi.Kondisi 
                            FROM vmotorlokasi  WHERE ";
                            
          SET @MyQuery = CONCAT(@MyQuery," vmotorlokasi.LokasiKode LIKE '",xLokasi,"' ) LokasiMotor 
                         ON LokasiMotor.MotorNoMesin = tmotor.MotorNoMesin AND LokasiMotor.MotorAutoN = tmotor.MotorAutoN 
                         INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
                         WHERE (tmotor.MotorType LIKE '",xTypeMtr,"' AND tmotor.MotorWarna LIKE '",xWarna,"') AND
                         (LokasiMotor.Jam BETWEEN '",@MyTgl1,"' AND '",@MyTgl2,"') AND
                         (tmotor.MotorNoMesin LIKE '",xNoMesin,"') AND
                         (LokasiMotor.Kondisi LIKE '",xKondisi,"')
                         ORDER BY  tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, 
                         tmotor.MotorNoMesin, LokasiMotor.Jam");
       END IF;


       /* Dealer ASal */
       IF xTipe = "tmotor" THEN 
          SET @MyTgl1 = DATE_FORMAT(xTgl1,"%Y-%m-%d 00:00:00");
          SET @MyTgl2 = DATE_FORMAT(xTgl2,"%Y-%m-%d 23:59:59");
          SET @MyQuery = "SELECT tmotor.MotorAutoN, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorType,tdmotortype.MotorNama, tmotor.MotorWarna, tmotor.MotorTahun, ttpd.DealerKode, ttpd.PDNo, ttpd.PDTgl, tmotor.SDNo, tmotor.DKNo, tmotor.SKNo
                          FROM tmotor 
                          INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
                          INNER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo
                          WHERE tmotor.PDNO <> '--'";
                          
          SET @MyQuery = CONCAT(@MyQuery," AND (tmotor.MotorType LIKE '",xTypeMtr,"' AND tmotor.MotorWarna LIKE '",xWarna,"') AND 
                         (ttpd.PDTgl BETWEEN '",@MyTgl1,"' AND '",@MyTgl2,"') AND 
                         (tmotor.MotorNoMesin LIKE '",xNoMesin,"') ORDER BY ttpd.DealerKode"); 
       END IF;

       PREPARE STMT FROM @MyQuery;
       EXECUTE Stmt;
    
    END$$
DELIMITER ;

