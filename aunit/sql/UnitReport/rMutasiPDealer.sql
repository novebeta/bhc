DELIMITER $$
DROP PROCEDURE IF EXISTS `rMutasiPDealer`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rMutasiPDealer`(IN xTipe VARCHAR(25),IN xTgl1 DATE,IN xTgl2 DATE,IN xLokasi VARCHAR(30),IN xDealer VARCHAR(20),IN xSort VARCHAR(25),IN xOrder VARCHAR(10))
BEGIN
/*
       xTipe    : "ttpd" -> Header
                  "tmotor" -> Detail
                  
       xSort    : "TtPD.PDTgl"
                  "TtPD.PDNo"
                  "TtPD.SDNo"
                  "TtPD.LokasiKode"
                  "tdlokasi.LokasiNama"
                  "TtPD.DealerKode"
                  "tdDealer.DealerNama"
                  "TtPD.PDMemo"
                  "TtPD.PDTotal"
                  "TtPD.UserID"
                  "IFNULL(ttgeneralledgerhd.GLLink,'--')"

       Cara Akses :
       
       *** HEADER ***
       CALL rMutasiPDealer('ttpd','2001-01-01','2019-12-31','','','TtPD.PDTgl','ASC');

       *** DETAIL ***
       CALL rMutasiPDealer('tmotor','2001-01-01','2019-12-31','','','TtPD.PDTgl','ASC');

*/

       SET xLokasi = CONCAT(xLokasi,"%");
       SET xDealer = CONCAT(xDealer,"%");
       
       /* HEADER */
       IF xTipe = "ttpd" THEN 
          SET @MyQuery = "SELECT ttpd.PDNo, ttpd.PDTgl, ttpd.SDNo, ttpd.DealerKode, ttpd.LokasiKode, ttpd.PDMemo, ttpd.PDTotal, ttpd.UserID, 
                          tddealer.DealerNama, tdlokasi.LokasiNama, IFNULL(ttsd.SDTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SDTgl, IFNULL(ttgeneralledgerhd.GLLink,'--') AS GLLink  
                          FROM ttpd 
                          LEFT OUTER JOIN tddealer ON ttpd.DealerKode = tddealer.DealerKode 
                          LEFT OUTER JOIN tdlokasi ON ttpd.LokasiKode = tdlokasi.LokasiKode 
                          LEFT OUTER JOIN ttsd ON ttpd.SDNo = ttsd.SDNo 
                          LEFT OUTER JOIN (SELECT ttgeneralledgerhd.GLLink FROM ttPD INNER JOIN ttgeneralledgerhd ON ttPD.PDNo = ttgeneralledgerhd.GLLink) AS ttgeneralledgerhd ON ttpd.PDNo = ttgeneralledgerhd.GLLink 
                          WHERE ";
                          
          SET @MyQuery = CONCAT(@MyQuery,"(ttpd.PDTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttpd.DealerKode LIKE '",xLokasi,"') AND 
                         (ttpd.LokasiKode LIKE '",xDealer,"') ORDER BY ",xSort," ",xOrder); 
       END IF;


       /* DETAIL */
       IF xTipe = "tmotor" THEN 
          SET @MyQuery = "SELECT BPKBNo, BPKBTglAmbil, BPKBTglJadi, DKNo, FBHarga, FBNo, FakturAHMNo, 
                          FakturAHMTgl, FakturAHMTglAmbil, MotorAutoN, MotorMemo, MotorNoMesin, MotorNoRangka, MotorTahun, 
                          MotorType, MotorWarna, PlatNo, PlatTgl, PlatTglAmbil, SKNo, SSNo, STNKAlamat, STNKNama, STNKNo, 
                          STNKTglAmbil, STNKTglBayar, STNKTglJadi, STNKTglMasuk, ttpd.PDNo, ttpd.PDTgl 
                          FROM ttpd 
                          INNER JOIN tmotor ON ttpd.PDNo = tmotor.PDNo   
                          WHERE ";
                          
          SET @MyQuery = CONCAT(@MyQuery,"(ttpd.PDTgl BETWEEN '",xTgl1,"' AND '",xTgl2,"') AND 
                         (ttpd.DealerKode LIKE '",xLokasi,"') AND 
                         (ttpd.LokasiKode LIKE '",xDealer,"') ORDER BY ",xSort," ",xOrder); 
       END IF;

       PREPARE STMT FROM @MyQuery;
       EXECUTE Stmt;
    
    END$$
DELIMITER ;

