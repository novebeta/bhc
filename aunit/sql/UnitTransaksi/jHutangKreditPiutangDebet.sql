DELIMITER $$

DROP PROCEDURE IF EXISTS `jHutangKreditPiutangDebet`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jHutangKreditPiutangDebet`(IN xNoGL VARCHAR(10))
BEGIN
	DELETE FROM tvhutangkredit WHERE NoGL = xNoGL ;
	DELETE FROM tvpiutangdebet WHERE NoGL = xNoGL ;
	INSERT INTO tvhutangkredit
	SELECT GLLink, ttgeneralledgerhd.NoGL, ttgeneralledgerhd.TglGL, ttgeneralledgerhd.KodeTrans, ttgeneralledgerit.GLAutoN, ttgeneralledgerit.NoAccount, traccount.NamaAccount,
	MemoGL,KeteranganGL, SUM(KreditGL) AS KreditGL, HPLink
	FROM ttgeneralledgerit 
	INNER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.NoGL = ttgeneralledgerit.NoGL
	INNER JOIN traccount ON traccount.NoAccount = ttgeneralledgerit.NoAccount
	WHERE ((KodeTrans <> 'NA') AND ((LEFT(ttgeneralledgerit.NoAccount,2)BETWEEN '24' AND '24') OR (ttgeneralledgerit.NoAccount  = '70000000')))
	AND ttgeneralledgerit.NoGL = xNoGL
	GROUP BY GLLink,ttgeneralledgerit.NoAccount
	HAVING (SUM(KreditGL) > 0);
	INSERT INTO tvpiutangdebet
	SELECT GLLink, ttgeneralledgerhd.NoGL, ttgeneralledgerhd.TglGL, ttgeneralledgerhd.KodeTrans, ttgeneralledgerit.GLAutoN, ttgeneralledgerit.NoAccount, traccount.NamaAccount,
	MemoGL,KeteranganGL, SUM(DebetGL) AS DebetGL, HPLink
	FROM ttgeneralledgerit 
	INNER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.NoGL = ttgeneralledgerit.NoGL
	INNER JOIN traccount ON traccount.NoAccount = ttgeneralledgerit.NoAccount
	WHERE ((KodeTrans <> 'NA') AND ttgeneralledgerit.NoAccount BETWEEN '11060000' AND '11150000')
	AND ttgeneralledgerit.NoGL = xNoGL
	GROUP BY GLLink,ttgeneralledgerit.NoAccount
	HAVING (SUM(DebetGL) > 0);
END$$

DELIMITER ;

