DELIMITER $$

DROP PROCEDURE IF EXISTS `fbmitcoa`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fbmitcoa`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xBMNo VARCHAR(12),
IN xBMAutoN SMALLINT(6),
IN xNoAccount VARCHAR(10),
IN xBMBayar DECIMAL(14,2),
IN xBMKeterangan VARCHAR(150),
IN xBMAutoNOLD SMALLINT(6),
IN xNoAccountOLD VARCHAR(10)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN	
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert
			IF oStatus = 0 THEN				
				SELECT IFNULL(MAX(BMAutoN)+1,0) INTO xBMAutoN FROM ttBMitcoa WHERE BMNo = xBMNo; #AutoN
				INSERT INTO ttBMitcoa (BMNo, BMAutoN, NoAccount, BMBayar, BMKeterangan) VALUES (xBMNo, xBMAutoN, xNoAccount, xBMBayar, xBMKeterangan);
				SET oKeterangan = CONCAT('Berhasil menambahkan data Bank Keluar ', xBMNo,' - ', xNoAccount, ' - ', FORMAT(xBMBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert
			IF oStatus = 0 THEN				
				SELECT (MAX(BMAutoN)+1) INTO xBMAutoN FROM ttBMitcoa WHERE BMNo = xBMNo; #AutoN
				INSERT INTO ttBMitcoa (BMNo, BMAutoN, NoAccount, BMBayar, BMKeterangan) VALUES (xBMNo, xBMAutoN, xNoAccount, xBMBayar, xBMKeterangan);
				SET oKeterangan = CONCAT('Berhasil menambahkan data Bank Keluar ', xBMNo,' - ', xNoAccount, ' - ', FORMAT(xBMBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;	
		END;
		WHEN "Update" THEN
		BEGIN
			 SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Update
			IF oStatus = 0 THEN
				UPDATE ttBMitcoa SET BMNo = xBMNo, BMAutoN = xBMAutoN, NoAccount = xNoAccount, BMBayar = xBMBayar, BMKeterangan = xBMKeterangan
				WHERE BMNo = xBMNoOLD AND BMAutoN = xBMAutoNOLD AND NoAccount = xNoAccountOLD;
				SET oKeterangan = CONCAT("Berhasil mengubah data bank keluar : ", xBMNo , ' - ' ,xNoAccount);
			ELSE
				SET oKeterangan = CONCAT("Gagal mengubah data bank keluar : ", xBMNo , ' - ' ,xNoAccount);
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Delete
			IF oStatus = 0 THEN
				DELETE FROM ttBMitcoa WHERE BMAutoN = xBMAutoN AND NoAccount = xNoAccount AND BMNo = xBMNo;
				SET oKeterangan = CONCAT("Berhasil menghapus data bank keluar : ", xBMNo , ' - ' ,xNoAccount);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
	END CASE;
END$$

DELIMITER ;

