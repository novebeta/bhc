DELIMITER $$

DROP PROCEDURE IF EXISTS `jbmLinkPiutang`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jbmLinkPiutang`(IN xBMNo VARCHAR(18), IN xBMTgl DATE, IN xNoGL VARCHAR(10), IN xNoAccount VARCHAR(10), IN MyKeteranganGL VARCHAR(100))
BEGIN
   DECLARE MyKeteranganGLJA1 VARCHAR(100) DEFAULT MyKeteranganGL;
   DECLARE MyKeteranganGLJA2 VARCHAR(100) DEFAULT MyKeteranganGL;

	CASE MyKeteranganGL
		#WHEN "UM Diterima" THEN SET MyKeteranganGLJA1 = "Kredit";
		WHEN "UM Diterima" THEN SET MyKeteranganGLJA1 = "UM Diterima";
		WHEN "Harga OTR" THEN  SET MyKeteranganGLJA1 = "Kredit";
		WHEN "Sisa Piutang Kons" THEN SET MyKeteranganGLJA1 = "Piutang Sisa";
		WHEN "Piutang Kons UM" THEN SET MyKeteranganGLJA1 = "Piutang Kons UM";
		WHEN "UM Piutang" THEN SET MyKeteranganGLJA1 = "Piutang Kons UM";
		WHEN "Piutang Leasing" THEN SET MyKeteranganGLJA1 = "Piutang Leasing";
		WHEN "Scheme" THEN SET MyKeteranganGLJA1 = "Kredit";
		ELSE BEGIN END;  
	END CASE;
   
	CASE MyKeteranganGL
		WHEN "UM Diterima" THEN SET MyKeteranganGLJA2 = "Uang Muka";
		WHEN "Harga OTR" THEN SET MyKeteranganGLJA2 = "Leasing";
		WHEN "Sisa Piutang Kons" THEN SET MyKeteranganGLJA2 = "Harga OTR";
		WHEN "Piutang Kons UM" THEN SET MyKeteranganGLJA2 = "Piutang UM";
		WHEN "UM Piutang" THEN SET MyKeteranganGLJA2 = "Piutang UM";
		WHEN "Piutang Leasing" THEN SET MyKeteranganGLJA2 = "Harga OTR";
		WHEN "Scheme" THEN SET MyKeteranganGLJA2 = "Kredit";
		ELSE BEGIN END;  
	END CASE;
           
	#Link Hutang Piutang Normal
	DELETE FROM ttglhpit WHERE NoGL = xNoGL;

	DROP TEMPORARY TABLE IF EXISTS AsalRaw; CREATE TEMPORARY TABLE AsalRaw AS
	SELECT NoAccount, tmotor.DKNo, ttSK.SKNo,  (DebetGL-KreditGL) AS DebetGL, BMBayar, KeteranganGL FROM ttbmit
	INNER JOIN tmotor ON ttbmit.DKNo = tmotor.DKNo
	INNER JOIN ttSK ON tmotor.SKNo = ttSK.SKNo
	INNER JOIN ttgeneralledgerhd ON ttSK.NoGL = ttgeneralledgerhd.NoGL
	INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL
	WHERE BMNo = xBMNo AND NoAccount = xNoAccount
	ORDER BY ttsk.skno;

	DROP TEMPORARY TABLE IF EXISTS Asal; CREATE TEMPORARY TABLE Asal AS
	SELECT DKNo, SKNo, IFNULL(SUM(DebetGL),0) AS DebetGL, BMBayar, KeteranganGL FROM AsalRaw
	WHERE (KeteranganGL LIKE CONCAT('%',MyKeteranganGL,'%') OR KeteranganGL LIKE CONCAT('%',MyKeteranganGLJA1,'%'))
	GROUP BY NoAccount , SKNo;

	DROP TEMPORARY TABLE IF EXISTS Terlunasi; CREATE TEMPORARY TABLE Terlunasi AS
	SELECT GLLink, SUM(IFNULL(HPNIlai,0)) AS Terbayar FROM Asal
	LEFT OUTER JOIN ttglhpit ON Asal.SKNo = ttglhpit.GLLink
	WHERE NoAccount = xNoAccount AND HPJenis = 'Piutang' AND ttglhpit.NoGL <> xNoGL
	AND (KeteranganGL LIKE CONCAT('%',MyKeteranganGL,'%') OR  KeteranganGL LIKE CONCAT('%',MyKeteranganGLJA1,'%'))
	GROUP BY GLLink ORDER BY GLLink;

	DROP TEMPORARY TABLE IF EXISTS Terbayar; CREATE TEMPORARY TABLE Terbayar AS
	SELECT Asal.DKNo, Asal.SKNo, Asal.DebetGL, IFNULL(Terbayar,0) AS Terbayar FROM Asal
	LEFT OUTER JOIN
	(SELECT GLLink, SUM(Terbayar) AS Terbayar FROM Terlunasi GROUP BY GLLink) A
	ON Asal.SKNo = A.GLLink;

	DELETE FROM ttglhpit WHERE NoGL = xNoGL;

   INSERT INTO ttglhpit SELECT xNoGL, xBMTgl, SKNo, xNoAccount,'Piutang', (DebetGL - Terbayar) AS DebetGL FROM Terbayar WHERE DebetGL > 0 ;
      
	#Link Hutang Piutang JA		
	DROP TEMPORARY TABLE IF EXISTS JAAsalRaw; CREATE TEMPORARY TABLE JAAsalRaw AS
	SELECT NoAccount,ttbmit.DKNo, ttgeneralledgerhd.GLLink , DebetGL-KreditGL AS DebetGL, BMBayar, KeteranganGL, MemoGL
	FROM  ttbmit
	INNER JOIN ttgeneralledgerhd ON ttbmit.DKNo = ttgeneralledgerhd.GLLink
	INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL
	WHERE BMNo = xBMNo AND NoAccount = xNoAccount
	ORDER BY ttgeneralledgerhd.GLLink;

	DROP TEMPORARY TABLE IF EXISTS JAAsal; CREATE TEMPORARY TABLE JAAsal AS
	SELECT DKNo, GLLink, IFNULL(SUM(DebetGL),0) AS DebetGL, BMBayar, KeteranganGL, MemoGL FROM JAAsalRaw
	WHERE (KeteranganGL LIKE CONCAT('%',MyKeteranganGLJA1,'%') OR KeteranganGL LIKE CONCAT('%',MyKeteranganGLJA2,'%') OR MemoGL LIKE CONCAT('%',MyKeteranganGL,'%') )
	GROUP BY NoAccount , GLLink;

	DROP TEMPORARY TABLE IF EXISTS JATerlunasi; CREATE TEMPORARY TABLE JATerlunasi AS
	SELECT JAAsal.GLLink, SUM(IFNULL(HPNIlai,0)) AS Terbayar, NoGL FROM JAAsal
	LEFT OUTER JOIN ttglhpit ON JAAsal.DKNo = ttglhpit.GLLink
	WHERE NoAccount = xNoAccount AND HPJenis = 'Piutang'  AND ttglhpit.NoGL <> xNoGL
  AND (KeteranganGL LIKE CONCAT('%',MyKeteranganGLJA1,'%') OR KeteranganGL LIKE CONCAT('%',MyKeteranganGLJA2,'%') OR MemoGL LIKE CONCAT('%',MyKeteranganGL,'%') )
	GROUP BY GLLink ORDER BY GLLink;

	DROP TEMPORARY TABLE IF EXISTS JATerbayar; CREATE TEMPORARY TABLE JATerbayar AS
	SELECT JAAsal.DKNo, JAAsal.GLLink, JAAsal.BMBayar, JAAsal.DebetGL, IFNULL(Terbayar,0) AS Terbayar FROM JAAsal
	LEFT OUTER JOIN
	(SELECT GLLink, SUM(Terbayar) AS Terbayar FROM Terlunasi GROUP BY GLLink) A
	ON JAAsal.GLLink = A.GLLink;

	INSERT INTO ttglhpit SELECT xNoGL, xBMTgl, GLLink, xNoAccount,'Piutang', (DebetGL - Terbayar) AS DebetGL FROM JATerbayar WHERE DebetGL > 0 ;
	
END$$

DELIMITER ;

