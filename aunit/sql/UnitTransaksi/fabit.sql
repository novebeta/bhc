DELIMITER $$
DROP PROCEDURE IF EXISTS `fabit`$$
CREATE DEFINER=`root`@`%` PROCEDURE `fabit`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xABNo VARCHAR(10),
IN xDKNo VARCHAR(10))
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = CONCAT('Berhasil menambahkan data Data Konsumen ', xABNo,' - ', xDKNo);
			INSERT INTO ttabit (ABNo, DKNo) VALUES (xABNo, xDKNo);
			UPDATE ttdk SET DKPengajuanBBN = 'Sudah' WHERE DKNo = xDKNo;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			IF oStatus = 0 THEN
				DELETE FROM ttabit WHERE DKNo = xDKNo AND ABNo = xABNo;
				UPDATE ttdk SET DKPengajuanBBN = 'Belum' WHERE DKNo = xDKNo;
				SET oKeterangan = CONCAT("Berhasil menghapus data Data Konsumen : ", xABNo , ' - ' ,xDKNo);
			ELSE
				SET oKeterangan = CONCAT("Gagal menghapus data Data Konsumen : ", xABNo , ' - ' ,xDKNo);
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Batal menyimpan data Data Konsumen: ", xDKNo );
		END;
		WHEN "Loop" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = CONCAT('Berhasil menambahkan data Data Konsumen ', xABNo,' - ', xDKNo);
			INSERT INTO ttabit (ABNo, DKNo) VALUES (xABNo, xDKNo);
		END;
	END CASE;
END$$

DELIMITER ;

