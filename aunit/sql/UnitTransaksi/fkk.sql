DELIMITER $$

DROP PROCEDURE IF EXISTS `fkk`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fkk`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xKKNo VARCHAR(18),
IN xKKNoBaru VARCHAR(18),
IN xKKTgl DATE,
IN xKKMemo VARCHAR(100),
IN xKKNominal DECIMAL(14,2),
IN xKKJenis VARCHAR(20),
IN xKKPerson VARCHAR(50),
IN xKKLink VARCHAR(12),
IN xKKJam DATETIME,
IN xNoGL VARCHAR(10))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN	
			DECLARE rKKNo VARCHAR(100) DEFAULT '';
			cek01: BEGIN
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT KKNo FROM ttkk
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttkk.KKNo 
				WHERE ttgeneralledgerhd.NoGL IS NULL AND KKTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rKKNo;
					IF rKKNo IS NOT NULL AND rKKNo <> '' THEN
						CALL jkk(rKKNo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Daftar Kas Keluar ");
		END;
		WHEN "Load" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Load data Kas Keluar No: ", xKKNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SELECT LokasiNomor INTO @LokasiNomor FROM tdlokasi WHERE LokasiKode = xLokasiKode;
			SET @AutoNo = CONCAT('KK',@LokasiNomor, RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(KKNo,5)+1),5,0)) INTO @AutoNo FROM ttKK WHERE ((KKNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(KKNo) = 12) ORDER BY KKNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 12 THEN SET @AutoNo = CONCAT('KK',@LokasiNomor,RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xKKNo = @AutoNo;
			SET oStatus = 0;SET oKeterangan = CONCAT("Tambah data baru Kas Keluar No: ",xKKNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @KKNoOLD = xKKNo;

			IF LOCATE('=',xKKNo) <> 0 THEN
				SELECT LokasiNomor INTO @LokasiNomor FROM tdlokasi WHERE LokasiKode = xLokasiKode;
				SET @AutoNo = CONCAT('KK',@LokasiNomor, RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(KKNo,5)+1),5,0)) INTO @AutoNo FROM ttKK WHERE ((KKNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(KKNo) = 12) ORDER BY KKNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 12 THEN SET @AutoNo = CONCAT('KK',@LokasiNomor,RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xKKNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xKKNoBaru = xKKNo;				
			END IF;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Update
			IF oStatus = 0 THEN 
				UPDATE ttkk
				SET KKNo = xKKNoBaru, KKTgl = xKKTgl, KKJenis = xKKJenis, KKPerson = xKKPerson, KKLink = xKKLink, KKNominal = xKKNominal, KKMemo = xKKMemo, NoGL = xNoGL, KKJam = xKKJam, UserID = xUserID, LokasiKode = xLokasiKode
				WHERE KKNo = xKKNo;
				SET xKKNo = xKKNoBaru;

				#KKAutoN
				IF xKKJenis = 'Umum' THEN
					UPDATE ttKKitcoa SET KKAutoN = -(KKAutoN) WHERE KKNo = xKKNo;
					blockCOA: BEGIN
					DECLARE myKKNo VARCHAR(12);
					DECLARE myKKAutoN SMALLINT;
					DECLARE myNoAccount VARCHAR(10);
					DECLARE doneCOA INT DEFAULT 0;  	
					DECLARE curCOA CURSOR FOR SELECT KKNo, KKAutoN, NoAccount FROM ttKKitcoa WHERE KKNo = xKKNo ORDER BY ABS(KKAutoN);	
					DECLARE CONTINUE HANDLER FOR NOT FOUND SET doneCOA = 1;
					SET @row_number = 0; 
					OPEN curCOA;
					REPEAT FETCH curCOA INTO myKKNo,myKKAutoN,myNoAccount ;
						IF NOT doneCOA THEN	
							SET @row_number = @row_number + 1; 
							UPDATE ttKKitcoa SET KKAutoN = @row_number
							WHERE KKNo = myKKNo AND KKAutoN = myKKAutoN AND NoAccount = myNoAccount;
						END IF;
					UNTIL doneCOA END REPEAT;
					CLOSE curCOA;
					END blockCOA;		
				END IF;
		  
				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @KKNoOLD; #Hapus Jurnal No Transaksi Lama
				CALL jkk(xKKNo,xUserID); #Posting Jurnal
								
				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Kas Keluar','ttkk', xKKNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Kas Keluar No: ", xKKNo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Kas Keluar','ttkk', xKKNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Kas Keluar No: ", xKKNo);
				END IF;
			ELSEIF oStatus = 1 THEN
			  SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;
			
			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKKNo;
			IF @GLValid = 'Sudah' THEN	
				SET oStatus = 1; SET oKeterangan = CONCAT("No KK:", xKKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM ttkk WHERE KKNo = xKKNo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xKKNo;
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Kas Keluar','ttkk', xKKNo); 
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Kas Keluar No: ",xKKNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
			
		END;
		WHEN "Cancel" THEN
		BEGIN
			SET oStatus = 1;
			IF LOCATE('=',xKKNo) <> 0 THEN
				DELETE FROM ttkk WHERE KKNo = xKKNo;
				SET oKeterangan = CONCAT("Batal menyimpan data Kas Keluar No: ", xKKNoBaru);
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Kas Keluar No: ", xKKNo);
			END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
			UPDATE ttkk SET KKLink = xKKLink WHERE KKNo = xKKNo;
		END;
		WHEN "Jurnal" THEN
		BEGIN
			 IF LOCATE('=',xKKNo) <> 0 THEN /* Data Baru, belum tersimpan tidak boleh di jurnal */
				  SET oStatus = 1;
				  SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			 ELSE
				  CALL jkk(xKKNo,xUserID);
				  SET oStatus = 0;
				  SET oKeterangan = CONCAT("Berhasil memproses Jurnal Kas Keluar No: ", xKKNo);
			 END IF;
		END;
		WHEN "Print" THEN
		BEGIN
			IF xCetak = "1" THEN
				SELECT KKNo, KKTgl, KKMemo, KKNominal, KKJenis, KKPerson, KKLink, UserID, ttkk.LokasiKode, KKJam, NoGL 
				FROM ttkk INNER JOIN tdlokasi ON ttkk.LokasiKode = tdlokasi.LokasiKode
				WHERE (KKNo = xKKNo);
			END IF;
			IF xCetak = "2" THEN
				IF xKKJenis = 'Bayar Unit' THEN
					SELECT KKNo, ttkkit.FBNo, FBTgl, SSNo, SupKode , FBTotal, SupNama, Paid - KKBayar AS Terbayar, KKBayar, FBTotal - KKPaid.Paid AS Sisa  FROM (
					SELECT  ttkkit.KKNo, ttkkit.FBNo, ttkkit.KKBayar, ttfb.FBTgl, ttfb.SSNo, ttfb.SupKode , ttfb.FBTotal,tdsupplier.SupNama
					FROM ttkkit 
					INNER JOIN ttfb ON ttkkit.FBNo = ttfb.FBNo 
					INNER JOIN tdsupplier ON ttfb.SupKode = tdsupplier.SupKode WHERE (ttkkit.KKNo = xKKNo) ORDER BY ttkkit.FBNo) ttkkit
					INNER JOIN (SELECT KKLINK AS FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'Bayar Unit') GROUP BY KKLINK) KKPaid 
					ON ttkkit.FBNo = KKPaid.FBNo 
					WHERE KKNo = xKKNo;
				END IF;
				IF xKKJenis = 'Bayar BBN' THEN
					SELECT KKNo, ttkkit.FBNo, KKBayar, DKTgl, LeaseKode, CusNama, MotorType, BBN, MotorTahun, MotorNoMesin , CusKecamatan, CusKabupaten, 
					KKPaid.Paid - KKBayar AS Terbayar, KKBayar, BBN - KKPaid.Paid AS Sisa FROM (
						SELECT ttkkit.KKNo, ttkkit.FBNo, ttkkit.KKBayar, ttdk.DKTgl, ttdk.LeaseKode, tdcustomer.CusNama, tmotor.MotorType, 
						tmotor.MotorTahun, tmotor.MotorNoMesin , tdcustomer.CusKecamatan, tdcustomer.CusKabupaten,
						(ttdk.BBN+ttdk.JABBN) AS BBN
						FROM ttkkit 
						INNER JOIN ttdk ON ttkkit.FBNo = ttdk.DKNo 
						INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
						INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo WHERE (ttkkit.KKNo = xKKNo ) ORDER BY ttkkit.FBNo) ttkkit
					INNER JOIN (SELECT KKLINK AS FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'Bayar BBN') GROUP BY KKLINK) KKPaid 
					ON ttkkit.FBNo = KKPaid.FBNo
					WHERE (KKNo = xKKNo);					                         
				END IF;
				IF xKKJenis = 'BBN Plus' THEN
					SELECT KKNo, ttkkit.FBNo, KKBayar, DKTgl, LeaseKode, CusNama, MotorType, BBN, MotorTahun, MotorNoMesin , CusKecamatan, CusKabupaten, 
					KKPaid.Paid - KKBayar AS Terbayar, KKBayar, BBN - KKPaid.Paid AS Sisa FROM (
						SELECT ttkkit.KKNo, ttkkit.FBNo, ttkkit.KKBayar, ttdk.DKTgl, ttdk.LeaseKode, tdcustomer.CusNama, tmotor.MotorType, 
						tmotor.MotorTahun, tmotor.MotorNoMesin , tdcustomer.CusKecamatan, tdcustomer.CusKabupaten,
						(ttdk.BBNPlus) AS BBN
						FROM ttkkit 
						INNER JOIN ttdk ON ttkkit.FBNo = ttdk.DKNo 
						INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
						INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo WHERE (ttkkit.KKNo = xKKNo ) ORDER BY ttkkit.FBNo) ttkkit
						INNER JOIN (SELECT KKLINK AS FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'BBN Plus') GROUP BY KKLINK) KKPaid 
					ON ttkkit.FBNo = KKPaid.FBNo
					WHERE KKNo = xKKNo;
				END IF;
				IF xKKJenis = 'BBN Acc' THEN
					SELECT KKNo, ttkkit.FBNo, KKBayar, DKTgl, LeaseKode, CusNama, MotorType, BBN, MotorTahun, MotorNoMesin , CusKecamatan, CusKabupaten, 
					KKPaid.Paid - KKBayar AS Terbayar, KKBayar, BBN - KKPaid.Paid AS Sisa FROM (
						SELECT ttkkit.KKNo, ttkkit.FBNo, ttkkit.KKBayar, ttdk.DKTgl, ttdk.LeaseKode, tdcustomer.CusNama, tmotor.MotorType, 
						tmotor.MotorTahun, tmotor.MotorNoMesin , tdcustomer.CusKecamatan, tdcustomer.CusKabupaten,
						(ttdk.DKSCP) AS BBN
						FROM ttkkit 
						INNER JOIN ttdk ON ttkkit.FBNo = ttdk.DKNo 
						INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
						INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo WHERE (ttkkit.KKNo = xKKNo ) ORDER BY ttkkit.FBNo) ttkkit
						INNER JOIN (SELECT KKLINK AS FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'BBN Acc') GROUP BY KKLINK) KKPaid 
					ON ttkkit.FBNo = KKPaid.FBNo 
					WHERE KKNo = xKKNo;
				END IF;
				IF xKKJenis = 'Umum' THEN				
					SELECT ttkkitcoa.KKNo, ttkkitcoa.KKAutoN, ttkkitcoa.NoAccount, ttkkitcoa.KKBayar, ttkkitcoa.KKKeterangan, traccount.NamaAccount
					FROM  ttkkitcoa 
					INNER JOIN traccount ON ttkkitcoa.NoAccount = traccount.NoAccount
					WHERE (ttkkitcoa.KKNo = xKKNo)
					ORDER BY ttkkitcoa.KKAutoN;				
				END IF;
			END IF;
			SET oStatus = 0; SET oKeterangan = CONCAT("Cetak data Kas Keluar No: ", xKKNo);
			INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE  
			(NOW(), xUserID , 'Cetak' , CONCAT('Kas Keluar ',xKKJenis),'ttkk', xKKNo); 
		END;
	END CASE;
END$$

DELIMITER ;

