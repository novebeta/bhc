DELIMITER $$

DROP PROCEDURE IF EXISTS `famhd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `famhd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan VARCHAR(150),
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xAMNo VARCHAR(10),
IN xAMNoBaru VARCHAR(10),
IN xAMTgl DATE,
IN xAMJam DATETIME,
IN xAMMemo VARCHAR(300),
IN xAMTotal DECIMAL(14,2),
IN xNoGL VARCHAR(10),
IN xMotorTahun DECIMAL(4,0),
IN xFBNo VARCHAR(18))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			#ReJurnal Transaksi Tidak Memiliki Jurnal
			DECLARE rAMNo VARCHAR(100) DEFAULT '';
			cek01: BEGIN
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT AMNo FROM ttamhd
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttamhd.AMNo 
				WHERE ttgeneralledgerhd.NoGL IS NULL AND AMTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rAMNo;
					IF rAMNo IS NOT NULL AND rAMNo <> '' THEN
						CALL jAM(rAMNo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;	
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Daftar Penyesuaian Nilai Persediaan Motor [AM] ");
		END;
		WHEN "Load" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Penyesuaian Nilai Persediaan Motor [AM] No : ",xAMNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SET @AutoNo = CONCAT('AM',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(AMNo,5)+1),5,0)) INTO @AutoNo FROM ttamhd WHERE ((AMNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(AMNo) = 10) ORDER BY AMNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('AM',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xAMNo = @AutoNo;
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Tambah data baru Penyesuaian Nilai Persediaan Motor [AM] No : ", @AutoNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @AMNoOLD = xAMNo;
			
			#Auto Number [ Insert vs Edit ]
			IF LOCATE('=',xAMNo) <> 0  THEN
				SET @AutoNo = CONCAT('AM',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(AMNo,5)+1),5,0)) INTO @AutoNo FROM ttamhd WHERE ((AMNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(AMNo) = 10) ORDER BY AMNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('AM',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xAMNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xAMNoBaru = xAMNo;
			END IF;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xFBNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xAMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");			
			END IF;

			#Update Proses 
			IF oStatus = 0 THEN				
				UPDATE ttamhd
				SET AMNo = xAMNoBaru, AMTgl = xAMTgl, AMJam = xAMJam, AMMemo = xAMMemo, AMTotal = xAMTotal, NoGL = xNoGL, UserID = xUserID, MotorTahun = xMotorTahun, FBNo = xFBNo
				WHERE AMNo = @AMNoOLD;		
				SET xAMNo = xAMNoBaru;

				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @AMNoOLD; #Hapus Jurnal No Transaksi Lama
				#CALL jam(xAMNo,xUserID); #Posting Jurnal
			
				IF @AutoNo = 'Edit' THEN #Edit
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Penyesuaian Nilai Persediaan Motor','ttamhd', xAMNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Penyesuaian Nilai Persediaan Motor [AM] No: ", xAMNo);
				ELSE #Add
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Penyesuaian Nilai Persediaan Motor','ttamhd', xAMNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Penyesuaian Nilai Persediaan Motor [AM] No: ", xAMNo);
				END IF;
			ELSEIF oStatus = 1 THEN
				SET oKeterangan = oKeterangan;
			END IF;					
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;
			
			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xAMNo;
			IF @GLValid = 'Sudah' THEN	
			  SET oStatus = 1; SET oKeterangan = CONCAT("No AM:", xAMNo, " sudah memiliki No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Delete Proses
			IF oStatus = 0 THEN							
				DELETE FROM ttamhd WHERE AMNo = xAMNo; #MainCourse
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xAMNo; #Delete Jurnal
				
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Penyesuaian Nilai Persediaan Motor','ttamhd', xAMNo); 
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Penyesuaian Nilai Persediaan Motor [AM] No: ",xAMNo);
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xAMNo) <> 0 THEN #Tambah Baru -> Batal dan Hapus
				  DELETE FROM ttamhd WHERE AMNo = xAMNo;
				  SET oKeterangan = CONCAT("Batal menyimpan data Penyesuaian Nilai Persediaan Motor [AM] No: ", xAMNoBaru);
			 ELSE #Edit -> Batal dan Hapus
				  SET oKeterangan = CONCAT("Batal menyimpan data Penyesuaian Nilai Persediaan Motor [AM] No: ", xAMNo);
			 END IF;
		END;
		WHEN "Jurnal" THEN
		BEGIN
			 IF LOCATE('=',xAMNo) <> 0 THEN /* Data Baru, belum tersimpan tidak boleh di jurnal */
				  SET oStatus = 1;
				  SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			 ELSE
				  CALL jam(xAMNo,xUserID);
				  SET oStatus = 0;
				  SET oKeterangan = CONCAT("Berhasil memproses Penyesuaian Nilai Persediaan Motor [AM] No: ", xAMNo);
			 END IF;
		END;
		WHEN "Print" THEN
		BEGIN	
			IF xCetak = "1" THEN
				SELECT AMNo, AMTgl, AMJam, AMMemo, AMTotal, NoGL, UserID, MotorTahun, FBNo FROM ttamhd WHERE (AMNo = xAMNo);
			END IF;
			IF xCetak = "2" THEN
				SELECT  ttamit.AMNo, ttamit.MotorNoMesin, ttamit.MotorAutoN, tmotor.MotorType, tmotor.MotorTahun, tmotor.MotorWarna, 
				tmotor.MotorNoRangka, tdmotortype.MotorNama, tmotor.MMHarga
				FROM  ttamit 
				INNER JOIN tmotor ON ttamit.MotorNoMesin = tmotor.MotorNoMesin AND ttamit.MotorAutoN = tmotor.MotorAutoN 
				INNER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
				WHERE (ttamit.AMNo = xAMNo);
			END IF;
			
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Cetak data Penyesuaian Nilai Persediaan Motor No: ", xAMNo);
			INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
			(NOW(), xUserID , 'Cetak' , 'Penyesuaian Nilai Persediaan Motor','ttamhd', xAMNo); 
		END;
	END CASE;
END$$

DELIMITER ;

