DELIMITER $$

DROP PROCEDURE IF EXISTS `fsdmotor`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fsdmotor`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xSDNo VARCHAR(18),
IN xMotorType VARCHAR(50),
IN xMotorTahun DECIMAL(4,0),
IN xMotorWarna VARCHAR(35),
IN xMotorNoMesin VARCHAR(25),
IN xMotorNoRangka VARCHAR(25),
IN xSDHarga DECIMAL(12,2),
IN xMotorAutoN SMALLINT(6),
IN xMotorAutoNOLD SMALLINT(6),
IN xMotorNoMesinOLD VARCHAR(25)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;
			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSDNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSDNo, " sudah memiliki No JT: ",@NoGL," yang tervalidasi");
			END IF;
			
			#Cek apakah Motor sudah pernah terjadi perpindahan
			IF oStatus = 0 THEN
				SET oKeterangan = "";
				SELECT SDJam INTO @SDJam FROM ttSD WHERE SDNo = xSDNo;
				SELECT GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN AND JAM > @SDJam	AND NoTrans <> xSDNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(@SDJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
					SET oStatus = 1;
				ELSE
					SET oStatus = 0;				
					SET oKeterangan = '';
				END IF;		
			END IF;
			
			#Cek Jangan sampai dobel dalam 1 no transaksi
			IF oStatus = 0 THEN
				#Update sdNo sesuai xsdNo					
					SET @MotorNoMesin = '';
					SELECT MotorNoMesin INTO @MotorNoMesin FROM tmotor 
					WHERE (MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN) AND SDNo = xSDNo ;
					IF @MotorNoMesin = '' THEN SET oStatus = 0; ELSE SET oStatus = 1; SET oKeterangan = CONCAT("Gagal menambah data Motor : ", xMotorAutoN , ' - ' ,xMotorNoMesin); END IF;
			END IF;
			
			#Insert Proses
			IF oStatus = 0 THEN
				#UpdateSDNoInTMotor
				SELECT IFNULL(MotorHrgBeli,0) INTO @HrgBeli FROM	tdmotortype WHERE MotorType = xMotorType LIMIT 1;
				
				UPDATE tmotor SET SDNo = xSDNo, SDHarga = @HrgBeli WHERE  MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN; 
				
				#InsertTvMotorLokasi	
				DELETE FROM tvmotorlokasi 	WHERE NoTrans = xSDNo AND MotorAutoN = xMotorAutoNOLD AND MotorNoMesin = xMotorNoMesinOLD;					
				DELETE FROM tvmotorlokasi 	WHERE NoTrans = xSDNo AND MotorAutoN = xMotorAutoN AND MotorNoMesin = xMotorNoMesin;					
				INSERT INTO tvmotorlokasi 
				SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttsd.LokasiKode AS LokasiKode, ttsd.sdNo AS NoTrans, ttsd.sdJam AS Jam, 'OUT' AS Kondisi 
				FROM tmotor  INNER JOIN ttsd ON tmotor.sdNo = ttsd.sdNo WHERE ttsd.sdNo = xsdNo AND tmotor.MotorAutoN = xMotorAutoN AND tmotor.MotorNoMesin = xMotorNoMesin;		
				SET oKeterangan = CONCAT('Berhasil menambahkan data Motor ', xMotorType,' - ', xMotorWarna, ' - ',xMotorAutoN , ' - ' ,xMotorNoMesin);												

				#Hitung SubTotal 
				SET @SDTotal = 0;
				SELECT IFNULL(SUM(SDHarga),0) INTO @SDTotal FROM tmotor WHERE SDNo = xSDNo;
				UPDATE ttSD	SET SDTotal = @SDTotal	WHERE SDNo = xSDNo;			
			END IF;
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSDNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSDNo, " sudah memiliki No JT: ",@NoGL," yang tervalidasi");
			END IF;

			#Cek Jangan sampai dobel dalam 1 no transaksi
			IF oStatus = 0 THEN
				#Update sdNo sesuai xsdNo					
				IF xMotorNoMesin <> xMotorNoMesinOLD THEN
					SET @MotorNoMesin = '';
					SELECT MotorNoMesin INTO @MotorNoMesin FROM tmotor 
					WHERE (MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN) AND SDNo = xSDNo ;
					IF @MotorNoMesin = '' THEN SET oStatus = 0; ELSE SET oStatus = 1; SET oKeterangan = CONCAT("Gagal mengubah data Motor : ", xMotorAutoN , ' - ' ,xMotorNoMesin); END IF;
				END IF;
			END IF;
					
			#Cek apakah Motor Lama sudah pernah terjadi perpindahan
			IF oStatus = 0 THEN
				SET oKeterangan = "";
				SELECT SDJam INTO @SDJam FROM ttSD WHERE SDNo = xSDNo;
				SELECT GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin = xMotorNoMesinOLD AND MotorAutoN = xMotorAutoNOLD AND JAM > @SDJam	AND NoTrans <> xSDNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(@SDJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
				ELSE
					SET oStatus = 0;	
					SET oKeterangan = '';			
				END IF;		
			END IF;
			
			#Update Proses
			IF oStatus = 0 THEN
				UPDATE tmotor SET SDNo = '--' WHERE MotorNoMesin = xMotorNoMesinOLD AND MotorAutoN = xMotorAutoNOLD;
				UPDATE tmotor SET SDNo = xSDNo, SDHarga = xSDHarga WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN;

				#TvMotorLokasi	
				DELETE FROM tvmotorlokasi WHERE MotorNoMesin = xMotorNoMesinOLD AND MotorAutoN = xMotorAutoNOLD AND NoTrans = xSDNo;
				DELETE FROM tvmotorlokasi WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN AND NoTrans = xSDNo;
				INSERT IGNORE INTO tvmotorlokasi
				SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttsd.LokasiKode AS LokasiKode, ttsd.SDNo AS NoTrans, ttsd.SDJam AS Jam, 'OUT' AS Kondisi 
				FROM tmotor  INNER JOIN ttsd ON tmotor.SDNo = ttsd.SDNo WHERE ttsd.SDNo = xSDNo AND tmotor.MotorAutoN = MotorAutoN AND tmotor.MotorNoMesin = xMotorNoMesin;		
				
				#Hitung SubTotal 
				SET @SDTotal = 0;
				SELECT IFNULL(SUM(SDHarga),0) INTO @SDTotal FROM tmotor WHERE SDNo = xSDNo;
				UPDATE ttSD	SET SDTotal = @SDTotal	WHERE SDNo = xSDNo;
				
				SET oKeterangan = CONCAT("Berhasil mengubah data Motor : ", xMotorType,' - ', xMotorWarna, xMotorAutoN , ' - ' ,xMotorNoMesin);			
			END IF;
		
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";

			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSDNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSDNo, " sudah memiliki No JT: ",@NoGL," yang tervalidasi");
			END IF;
				
			#Cek Mutasi Motor
			IF oStatus = 0 THEN
				SELECT SDJam INTO @SDJam FROM ttSD WHERE SDNo = xSDNo;
				SELECT GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin = xMotorNoMesin AND  MotorAutoN = xMotorAutoN AND JAM > @SDJam AND NoTrans <> xSDNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN	
					SET oStatus = 1; 
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(@SDJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
				ELSE 
					SET oStatus = 0;	
					SET oKeterangan = '';
				END IF;		
			END IF;
			
			#Delete Proses
			IF oStatus = 0 THEN
				UPDATE tmotor SET SDNo = '--' WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN ;
				DELETE FROM tvmotorlokasi WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN AND NoTrans = xSDNo;

				#Hitung SubTotal 
				SET @SDTotal = 0;
				SELECT IFNULL(SUM(SDHarga),0) INTO @SDTotal FROM tmotor WHERE SDNo = xSDNo;
				UPDATE ttSD	SET SDTotal = @SDTotal	WHERE SDNo = xSDNo;

				SET oKeterangan = CONCAT("Berhasil menghapus data Motor : ", xMotorAutoN , ' - ' ,xMotorNoMesin);			
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Batal menyimpan data Motor: ", xMotorNoMesin );
		END;
	END CASE;
END$$

DELIMITER ;

