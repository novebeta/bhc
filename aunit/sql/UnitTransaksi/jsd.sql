DELIMITER $$

DROP PROCEDURE IF EXISTS `jsd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jsd`(IN xSDNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE MyAutoN INTEGER DEFAULT 1;
	DECLARE TotSDHarga DOUBLE DEFAULT 0;

	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xSDNo ORDER BY NOGL DESC LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;

	UPDATE tmotor SET SDHarga = FBHarga WHERE SDHarga = 0 AND SDNo = xSDNo;
	SELECT IFNULL(SUM(SDHarga),0) INTO TotSDHarga FROM tmotor WHERE SDNo = xSDNo;
	UPDATE ttsd SET SDTotal = TotSDHarga WHERE SDNo = xSDNo;

	SELECT SDNo, SDTgl, LokasiKode, DealerKode, SDMemo, SDTotal, UserID, SDJam, NoGL INTO
	@SDNo, @SDTgl, @LokasiKode, @DealerKode, @SDMemo, @SDTotal, @UserID, @SDJam, @NoGL
	FROM ttsd WHERE SDNo = xSDNo;
	
	SELECT DealerNama INTO @DealerNama FROM tddealer WHERE DealerKode = @DealerKode LIMIT 1;

	#Numbering JT
	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
    SET xNoGL =  @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;

	#Delete
	DELETE FROM ttgeneralledgerhd WHERE GLLink = xSDNo;
	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL;  
	DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;  
	DELETE FROM ttglhpit WHERE NoGL = xNoGL;

	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, @SDTgl, 'IE', CONCAT('Post Invoice Mutasi Eksternal ke ',@DealerNama), TotSDHarga, TotSDHarga, xSDNo, xUserID, '--', xGLValid);

	SET @PiutAfiliasi = '';
	SELECT IFNULL(NoAccount,'') INTO @PiutAfiliasi FROM traccount WHERE SUBSTRING_INDEX(TRIM(NamaAccount), ' ', -1) LIKE CONCAT('%',SUBSTRING_INDEX(TRIM(@DealerNama), ' ', -1)) AND JenisAccount ='Detail' AND NoParent = '11070300' LIMIT 1; 
	IF @PiutAfiliasi = '' THEN
		IF @DealerKode = "INV" THEN
			SET @PiutAfiliasi = "12030000";
		ELSE
			SET @PiutAfiliasi = "11070301";
		END IF;
	END IF;
	
	#Item
	INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
	(xNoGL, @SDTgl, 1, @PiutAfiliasi, CONCAT('Post Invoice Mutasi Eksternal ke ',@DealerNama), @SDTotal, 0);
	INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
	(xNoGL, @SDTgl, 2, '11150100', CONCAT('Post Invoice Mutasi Eksternal ke ',@DealerNama), 0, @SDTotal);
	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	CALL jHutangKreditPiutangDebet(xNoGL);

	UPDATE ttSD SET NoGL = xNoGL  WHERE SDNo = xSDNo;

END$$

DELIMITER ;

