DELIMITER $$
DROP FUNCTION IF EXISTS `fcapitalize`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `fcapitalize`(s VARCHAR(255)) RETURNS VARCHAR(255) CHARSET latin1
BEGIN
  DECLARE c INT;
  DECLARE X VARCHAR(255);
  DECLARE Y VARCHAR(255);
  DECLARE z VARCHAR(255);
  SET X = UPPER(SUBSTRING( s, 1, 1));
  SET Y = (SUBSTR( s, 2));
  SET c = INSTR( Y, ' ');
  WHILE c > 0
    DO
      SET z = SUBSTR( Y, 1, c);
      SET X = CONCAT( X, z);
      SET z = UPPER( SUBSTR( Y, c+1, 1));
      SET X = CONCAT( X, z);
      SET Y = SUBSTR( Y, c+2);
      SET c = INSTR( Y, ' ');
  END WHILE;
  SET X = CONCAT(X, Y);
  RETURN X;
END$$
DELIMITER ;
