DELIMITER $$

DROP PROCEDURE IF EXISTS `jbm`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jbm`(IN xBMNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE oSumBMBayarIT DOUBLE DEFAULT 0;
	DECLARE oSumBMBayarCOA DOUBLE DEFAULT 0;
   DECLARE MyPesanKu VARCHAR(300) DEFAULT '';
   DECLARE MyPesankuItem VARCHAR(150) DEFAULT '';
	DECLARE oNoUrutKu  DECIMAL(5) DEFAULT 1;
	DECLARE My24050200TitipanBBN DOUBLE DEFAULT 0;
   DECLARE My11060200PiutangBBN DOUBLE DEFAULT 0;
	
	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xBMNo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;
	
	SELECT BMNo, BMTgl, BMMemo, BMNominal, BMJenis, LeaseKode, BMCekNo, BMCekTempo, BMPerson, BMAC, NoAccount, UserID, BMJam, NoGL
	INTO  @BMNo,@BMTgl,@BMMemo,@BMNominal,@BMJenis,@LeaseKode,@BMCekNo,@BMCekTempo,@BMPerson,@BMAC,@NoAccount,@UserID,@BMJam,@NoGL
	FROM ttbmhd	WHERE BMNo = xBMNo;
	SELECT IFNULL(SUM(BMBayar),0) INTO oSumBMBayarIT FROM ttbmit WHERE BMNo = xBMNo;
	SELECT IFNULL(SUM(BMBayar),0) INTO oSumBMBayarCOA FROM ttbmitcoa WHERE BMNo = xBMNo;
	
	IF @BMJenis = 'Kredit' OR @BMJenis = 'Tunai' THEN
		SET MyPesanKu = CONCAT("Post  BM - ","Uang Muka ",@BMJenis);
	ELSE
		SET MyPesanKu = CONCAT("Post  BM - ",@BMJenis);
	END IF;
   IF @BMPerson <> '--' THEN SET MyPesanKu = CONCAT(MyPesanKu,' - ' , @BMPerson); END IF;
   IF @BMMemo <> "--" THEN SET MyPesanKu = CONCAT(MyPesanKu,' - ' , @BMMemo); END IF; 
   SET MyPesanKu = TRIM(MyPesanKu); 
   IF LENGTH(MyPesanKu) > 300 THEN SET MyPesanKu = SUBSTRING(MyPesanKu,0, 299); END IF;
   SET MyPesankuItem = MyPesanKu ;
   IF LENGTH(MyPesankuItem) > 150 THEN SET MyPesankuItem = SUBSTRING(MyPesankuItem,0, 149); END IF;
   
	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;

	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL; 
	DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;
	DELETE FROM ttglhpit WHERE NoGL = xNoGL;
   DELETE FROM ttgeneralledgerhd WHERE GLLink = xBMNo;
   
	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, @BMTgl, 'BM', MyPesanKu, @BMNominal, @BMNominal, xBMNo, xUserID, '--', xGLValid);

	#Item
	CASE @BMJenis
		WHEN "Umum" THEN
		BEGIN
			#ScriptTtglhpit
			DROP TEMPORARY TABLE IF EXISTS ScriptTtglhpit; CREATE TEMPORARY TABLE IF NOT EXISTS ScriptTtglhpit AS
			SELECT NoGL, TglGL, GLLink, NoAccount, HPJenis, HPNilai FROM ttglhpit WHERE NoGL = xNoGL;
	
			SET @row_number = 0; 		
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, (@row_number:=@row_number + 1),@NoAccount , MyPesankuItem , oSumBMBayarCOA, 0 ;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, (@row_number:=@row_number + 1), NoAccount,  BMKeterangan, 0, BMBayar FROM ttBMitcoa WHERE BMNo = xBMNo AND BMBayar >= 0;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, (@row_number:=@row_number + 1), NoAccount,  BMKeterangan, BMBayar, 0 FROM ttBMitcoa WHERE BMNo = xBMNo AND BMBayar < 0;
			
			SELECT COUNT(NoGL) INTO @CountNoGL FROM ScriptTtglhpit;
			IF @CountNoGL > 0 THEN
				INSERT INTO ttglhpit SELECT * FROM ScriptTtglhpit;
				DELETE FROM ttglhpit WHERE NoAccount NOT IN (SELECT NoAccount FROM ttgeneralledgerit WHERE NoGL  = xNoGL) AND NoGL  = xNoGL; 
				UPDATE ttgeneralledgerhd SET HPLink = 'Many'  WHERE NoGL = xNoGL; 
			END IF;		
			
			UPDATE ttBMhd SET BMNominal = oSumBMBayarCOA WHERE BMNo = xBMNo;
		END;
		WHEN "Inden" THEN
		BEGIN
			DROP TEMPORARY TABLE IF EXISTS tvBMsk; CREATE TEMPORARY TABLE tvBMsk AS
			SELECT ttBMit.BMNo, BMJam, IFNULL(tmotor.SKNo,'--') AS SKNo, BMBayar, IFNULL(SKJam,DATE('3000-01-01')) AS SKJam
			FROM ttBMit
			INNER JOIN ttBMhd ON ttBMhd.BMNo = ttBMit.BMNo
			LEFT OUTER JOIN ttdk ON ttdk.INNo = ttBMit.DKNo
			LEFT OUTER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
			LEFT OUTER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo
			WHERE ttBMit.BMNo = xBMNo;

			SET @row_number = 0; 		
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, (@row_number:=@row_number + 1),@NoAccount , MyPesankuItem , @BMNominal, 0;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL AS NoGL, @BMTgl AS TglGL, (@row_number:=@row_number + 1) AS GLAutoN,'11060100' , MyPesankuItem  AS KeteranganGL, 0 AS DebetGL, BMBayar AS KreditGL FROM tvBMsk WHERE SKJam <= BMJam;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL AS NoGL, @BMTgl AS TglGL, (@row_number:=@row_number + 1) AS GLAutoN,'24050100' , MyPesankuItem  AS KeteranganGL, 0 AS DebetGL, BMBayar AS KreditGL FROM tvBMsk WHERE SKJam > BMJam;		
		END;
		WHEN "Tunai" THEN
		BEGIN
			DROP TEMPORARY TABLE IF EXISTS tvBMsk; CREATE TEMPORARY TABLE tvBMsk AS
			SELECT ttBMit.BMNo, BMJam, IFNULL(tmotor.SKNo,'--') AS SKNo, BMBayar, IFNULL(SKJam,DATE('3000-01-01')) AS SKJam
			FROM ttBMit
			INNER JOIN ttBMhd ON ttBMhd.BMNo = ttBMit.BMNo
			LEFT OUTER JOIN ttdk ON ttdk.DKNo = ttBMit.DKNo
			LEFT OUTER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
			LEFT OUTER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo
			WHERE ttBMit.BMNo = xBMNo;

			SET @row_number = 0; 		
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, (@row_number:=@row_number + 1),@NoAccount , MyPesankuItem , @BMNominal, 0;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, (@row_number:=@row_number + 1),'11060100' , MyPesankuItem  AS KeteranganGL,  0 AS DebetGL,BMBayar AS KreditGL FROM tvBMsk WHERE SKJam <= BMJam;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, (@row_number:=@row_number + 1),'24050100' , MyPesankuItem  AS KeteranganGL, 0 AS DebetGL,BMBayar AS KreditGL FROM tvBMsk WHERE SKJam > BMJam;		
			IF @BMNominal - oSumBMBayarIT > 0 THEN
				INSERT INTO ttgeneralledgerit	SELECT xNoGL AS NoGL, @BMTgl AS TglGL, (@row_number:=@row_number + 1) AS GLAutoN,'24050100' , MyPesankuItem  AS KeteranganGL, 0 AS DebetGL, @BMNominal - oSumBMBayarIT  AS KreditGL;		
			END IF;
			CALL jbmLinkPiutang(xBMNo,@BMTgl, xNoGL,'11060100','UM Diterima');
		END;
		WHEN "Kredit" THEN
		BEGIN
			DROP TEMPORARY TABLE IF EXISTS tvBMsk; CREATE TEMPORARY TABLE tvBMsk AS
			SELECT ttBMit.BMNo, BMJam, IFNULL(tmotor.SKNo,'--') AS SKNo, BMBayar, IFNULL(SKJam,DATE('3000-01-01')) AS SKJam
			FROM ttBMit
			INNER JOIN ttBMhd ON ttBMhd.BMNo = ttBMit.BMNo
			LEFT OUTER JOIN ttdk ON ttdk.DKNo = ttBMit.DKNo
			LEFT OUTER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
			LEFT OUTER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo
			WHERE ttBMit.BMNo = xBMNo;

			SET @row_number = 0; 		
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, (@row_number:=@row_number + 1),@NoAccount , MyPesankuItem , @BMNominal, 0;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, (@row_number:=@row_number + 1),'11060100' , MyPesankuItem  AS KeteranganGL,  0 AS DebetGL,BMBayar AS KreditGL FROM tvBMsk WHERE SKJam <= BMJam;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, (@row_number:=@row_number + 1),'24050100' , MyPesankuItem  AS KeteranganGL, 0 AS DebetGL,BMBayar AS KreditGL FROM tvBMsk WHERE SKJam > BMJam;		
			IF @BMNominal - oSumBMBayarIT > 0 THEN
				INSERT INTO ttgeneralledgerit	SELECT xNoGL AS NoGL, @BMTgl AS TglGL, (@row_number:=@row_number + 1) AS GLAutoN,'24050100' , MyPesankuItem  AS KeteranganGL, 0 AS DebetGL, @BMNominal - oSumBMBayarIT  AS KreditGL;		
			END IF;
			CALL jbmLinkPiutang(xBMNo,@BMTgl, xNoGL,'11060100','UM Diterima');
		END;
		WHEN "Piutang UM" THEN
		BEGIN
			DROP TEMPORARY TABLE IF EXISTS tvBMsk; CREATE TEMPORARY TABLE tvBMsk AS
			SELECT ttBMit.BMNo, BMJam, IFNULL(tmotor.SKNo,'--') AS SKNo, BMBayar, IFNULL(SKJam,DATE('3000-01-01')) AS SKJam
			FROM ttBMit
			INNER JOIN ttBMhd ON ttBMhd.BMNo = ttBMit.BMNo
			LEFT OUTER JOIN ttdk ON ttdk.DKNo = ttBMit.DKNo
			LEFT OUTER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
			LEFT OUTER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo
			WHERE ttBMit.BMNo = xBMNo;

			SET @row_number = 0; 		
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, (@row_number:=@row_number + 1),@NoAccount , MyPesankuItem , @BMNominal, 0;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, (@row_number:=@row_number + 1),'11060100' , MyPesankuItem  AS KeteranganGL,  0 AS DebetGL,BMBayar AS KreditGL FROM tvBMsk WHERE SKJam <= BMJam;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, (@row_number:=@row_number + 1),'24050100' , MyPesankuItem  AS KeteranganGL, 0 AS DebetGL,BMBayar AS KreditGL FROM tvBMsk WHERE SKJam > BMJam;		
			IF @BMNominal - oSumBMBayarIT > 0 THEN
				INSERT INTO ttgeneralledgerit	SELECT xNoGL AS NoGL, @BMTgl AS TglGL, (@row_number:=@row_number + 1) AS GLAutoN,'24050100' , MyPesankuItem  AS KeteranganGL, 0 AS DebetGL, @BMNominal - oSumBMBayarIT  AS KreditGL;		
			END IF;
			CALL jbmLinkPiutang(xBMNo,@BMTgl, xNoGL,'11060100','UM Piutang');
		END;		
		WHEN "Piutang Sisa" THEN
		BEGIN
			DROP TEMPORARY TABLE IF EXISTS tvBMsk; CREATE TEMPORARY TABLE tvBMsk AS
			SELECT ttBMit.BMNo, BMJam, IFNULL(tmotor.SKNo,'--') AS SKNo, BMBayar, IFNULL(SKJam,DATE('3000-01-01')) AS SKJam
			FROM ttBMit
			INNER JOIN ttBMhd ON ttBMhd.BMNo = ttBMit.BMNo
			LEFT OUTER JOIN ttdk ON ttdk.DKNo = ttBMit.DKNo
			LEFT OUTER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
			LEFT OUTER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo
			WHERE ttBMit.BMNo = xBMNo;

			SET @row_number = 0; 		
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, (@row_number:=@row_number + 1),@NoAccount , MyPesankuItem , @BMNominal, 0;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, (@row_number:=@row_number + 1),'11060100' , MyPesankuItem  AS KeteranganGL,  0 AS DebetGL,BMBayar AS KreditGL FROM tvBMsk WHERE SKJam <= BMJam;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, (@row_number:=@row_number + 1),'24050100' , MyPesankuItem  AS KeteranganGL, 0 AS DebetGL,BMBayar AS KreditGL FROM tvBMsk WHERE SKJam > BMJam;		
			IF @BMNominal - oSumBMBayarIT > 0 THEN
				INSERT INTO ttgeneralledgerit	SELECT xNoGL AS NoGL, @BMTgl AS TglGL, (@row_number:=@row_number + 1) AS GLAutoN,'24050100' , MyPesankuItem  AS KeteranganGL, 0 AS DebetGL, @BMNominal - oSumBMBayarIT  AS KreditGL;		
			END IF;
			CALL jbmLinkPiutang(xBMNo,@BMTgl, xNoGL,'11060100','Sisa Piutang Kons');
		END;	
		WHEN "Leasing" THEN
		BEGIN	
			SET @MyNoAccountPiutang = '';
			SET @MyNoAccountTitipan = '';
			SELECT IFNULL(NoAccount,'11080100') INTO @MyNoAccountPiutang FROM traccount
			WHERE NamaAccount LIKE CONCAT('%',SUBSTR(@LeaseKode,1,LENGTH(@LeaseKode)-1),'%') AND JenisAccount ='Detail' AND NoParent = '11080000' LIMIT 1;
			SELECT IFNULL(NoAccount,'24060100') INTO @MyNoAccountTitipan FROM traccount
			WHERE NamaAccount LIKE CONCAT('%',SUBSTR(@LeaseKode,1,LENGTH(@LeaseKode)-1),'%') AND JenisAccount ='Detail' AND NoParent = '24060000' LIMIT 1;
			IF @MyNoAccountPiutang IS NULL OR @MyNoAccountPiutang = '' THEN SET @MyNoAccountPiutang = '11080100'; END IF;
			IF @MyNoAccountTitipan IS NULL OR @MyNoAccountTitipan = '' THEN SET @MyNoAccountTitipan = '24060100'; END IF;
			SET @row_number = 0; 		
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, (@row_number:=@row_number + 1),@NoAccount , MyPesankuItem , @BMNominal, 0;
			IF @BMNominal - oSumBMBayarIT = 0 THEN
				INSERT INTO ttgeneralledgerit	SELECT xNoGL , @BMTgl , (@row_number:=@row_number + 1), @MyNoAccountPiutang, MyPesankuItem, 0, @BMNominal;		
			ELSE
				IF @BMNominal > 0 THEN
					INSERT INTO ttgeneralledgerit	SELECT xNoGL , @BMTgl , (@row_number:=@row_number + 1),@MyNoAccountPiutang , MyPesankuItem  , 0 , oSumBMBayarIT  ;		
					INSERT INTO ttgeneralledgerit	SELECT xNoGL , @BMTgl , (@row_number:=@row_number + 1),@MyNoAccountTitipan , MyPesankuItem  , 0 , @BMNominal - oSumBMBayarIT ;		
				ELSE
					INSERT INTO ttgeneralledgerit	SELECT xNoGL , @BMTgl , (@row_number:=@row_number + 1),@MyNoAccountTitipan , MyPesankuItem  , 0 , @BMNominal - oSumBMBayarIT ;		
				END IF;
			END IF;
			CALL jbmLinkPiutang(xBMNo,@BMTgl, xNoGL,@MyNoAccountPiutang,'Piutang Leasing');	
			
			#jbmLinkPiutangJATambahan
			SELECT IFNULL(SUM(KreditGL),0) INTO @JumlahKreditPLunasi FROM ttgeneralledgerit WHERE NOGL = xNoGL AND NoAccount = @MyNoAccountPiutang;
			SELECT IFNULL(SUM(HPNilai),0) INTO @JumlahHPNilai FROM ttglhpit WHERE NOGL = xNoGL AND NoAccount = @MyNoAccountPiutang;
	      IF @JumlahKreditPLunasi > @JumlahHPNilai THEN
				SELECT IFNULL(SUM(DebetGL),0) FROM ttgeneralledgerit 
				INNER JOIN ttgeneralledgerhd ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL 
				WHERE LEFT(ttgeneralledgerhd.NOGL,2) = 'JA' AND NoAccount = @MyNoAccountPiutang AND GLLINK = ('--') 
				GROUP BY GLLink;
				#INSERT INTO ttglhpit SELECT xNoGL, xBMTgl, GLLink, xNoAccount,'Piutang', (DebetGL - Terbayar) AS DebetGL FROM JATerbayar WHERE DebetGL > 0 ;
			END IF;     
			#CALL jbmLinkPiutangJATambahan(xBMNo,@BMTgl, xNoGL, MyNoAccount)	
		END;	
		WHEN "Scheme" THEN
		BEGIN
			SET @MyNoAccountPiutang = '';
			SET @MyNoAccountTitipan = '';
			SELECT IFNULL(NoAccount,'11090100') INTO @MyNoAccountPiutang FROM traccount
			WHERE NamaAccount LIKE CONCAT('%',SUBSTR(@LeaseKode,1,LENGTH(@LeaseKode)-1),'%') AND JenisAccount ='Detail' AND NoParent = '11090000' LIMIT 1;
			SELECT IFNULL(NoAccount,'24060100') INTO @MyNoAccountTitipan FROM traccount
			WHERE NamaAccount LIKE CONCAT('%',SUBSTR(@LeaseKode,1,LENGTH(@LeaseKode)-1),'%') AND JenisAccount ='Detail' AND NoParent = '24060000' LIMIT 1;
			IF @MyNoAccountPiutang IS NULL OR @MyNoAccountPiutang = '' THEN SET @MyNoAccountPiutang = '11090100'; END IF;
			IF @MyNoAccountTitipan IS NULL OR @MyNoAccountTitipan = '' THEN SET @MyNoAccountTitipan = '24060100'; END IF;
			SET @row_number = 0; 		
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, (@row_number:=@row_number + 1),@NoAccount , MyPesankuItem , @BMNominal, 0;
			IF @BMNominal - oSumBMBayarIT = 0 THEN
				INSERT INTO ttgeneralledgerit	SELECT xNoGL , @BMTgl , (@row_number:=@row_number + 1), @MyNoAccountPiutang, MyPesankuItem, 0, @BMNominal;		
			ELSE
				IF @BMNominal > 0 THEN
					INSERT INTO ttgeneralledgerit	SELECT xNoGL , @BMTgl , (@row_number:=@row_number + 1),@MyNoAccountPiutang , MyPesankuItem  , 0 , oSumBMBayarIT  ;		
					INSERT INTO ttgeneralledgerit	SELECT xNoGL , @BMTgl , (@row_number:=@row_number + 1),@MyNoAccountTitipan , MyPesankuItem  , 0 , @BMNominal - oSumBMBayarIT ;		
				ELSE
					INSERT INTO ttgeneralledgerit	SELECT xNoGL , @BMTgl , (@row_number:=@row_number + 1),@MyNoAccountTitipan , MyPesankuItem  , 0 , @BMNominal - oSumBMBayarIT ;		
				END IF;
			END IF;
			CALL jbmLinkPiutang(xBMNo,@BMTgl, xNoGL,@MyNoAccountPiutang,'Scheme');		
		END;	
		WHEN "BBN Plus" THEN
		BEGIN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, oNoUrutKu,@NoAccount , MyPesankuItem , @BMNominal, 0; SET oNoUrutKu = oNoUrutKu + 1;

			DROP TEMPORARY TABLE IF EXISTS TSearch; CREATE TEMPORARY TABLE TSearch AS
			SELECT BMNo, DKNo, BMBayar, BMJam, IFNULL(KKNo,'--') AS KKNo, IFNULL(KKLink,'--') AS KKLink, IFNULL(KKNominal,0) AS KKNominal, IFNULL(KKJam,DATE('2500-01-01')) AS KKJam  FROM 
				(SELECT ttbmit.BMNo, DKNo, BMBayar, BMJam FROM ttbmit INNER JOIN ttbmhd ON ttbmhd.bmno = ttbmit.BMNo WHERE ttbmit.BMNO = xBMNo) ttbmit
			LEFT OUTER JOIN
				(SELECT KKNo, KKTgl, SUM(KKNominal) AS KKNominal, KKJenis, KKLink, KKJam, NoGL FROM vtKK WHERE KKJenis = 'BBN Plus' AND KKLINk IN (SELECT  DKNo FROM ttbmit WHERE ttbmit.BMNO = xBMNo) GROUP BY KKLink) vtkk
			ON vtkk.KKLink = ttbmit.DKNo;
			SELECT IFNULL(SUM(KKNominal),0), IFNULL(SUM(BMBayar - KKNominal),0) INTO My11060200PiutangBBN, My24050200TitipanBBN FROM TSearch WHERE BMJam > KKJam  AND BMBayar > KKNominal;
			IF My11060200PiutangBBN = 0 THEN SELECT IFNULL(SUM(KKNominal),0) INTO My11060200PiutangBBN FROM TSearch WHERE BMJam > KKJam AND BMBayar <= KKNominal; END IF;
			IF My24050200TitipanBBN = 0 THEN SELECT IFNULL(SUM(BMBayar),0) INTO My24050200TitipanBBN FROM TSearch WHERE BMJam <= KKJam; END IF;

			IF @BMNominal - oSumBMBayarIT > 0 THEN SET My24050200TitipanBBN = My24050200TitipanBBN + @BMNominal - oSumBMBayarIT;  END IF;
			IF My24050200TitipanBBN > 0 THEN
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, oNoUrutKu , '24050200', MyPesankuItem , 0, My24050200TitipanBBN; SET oNoUrutKu = oNoUrutKu + 1;
			END IF;
			IF My11060200PiutangBBN > 0 THEN
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, oNoUrutKu , '11060200', MyPesankuItem , 0, My11060200PiutangBBN; SET oNoUrutKu = oNoUrutKu + 1;			
			END IF;
		
			DELETE FROM ttglhpit WHERE NoGL = xNoGL;
			DROP TEMPORARY TABLE IF EXISTS TSearchLink; CREATE TEMPORARY TABLE TSearchLink AS
			SELECT BMNo, DKNo, SUM(BMBayar) AS BMBayar, BMJam, KKno, KKLink, KKNominal, KKJam, KreditGL, NoGL FROM
			(SELECT ttBMit.BMNo, DKNo, BMBayar, BMJam FROM ttBMit 
			INNER JOIN ttBMhd ON ttBMhd.BMno = ttBMit.BMNo 
			INNER JOIN ttgeneralledgerhd ON ttBMhd.NoGL = ttgeneralledgerhd.NoGL 
			INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL 
			WHERE ttBMit.BMNo = xBMNo AND ttgeneralledgerit.NoAccount = '11060200') ttbmit
			INNER JOIN
			(SELECT KKNo, KKTgl, KKNominal, KKJenis, KKLink, KKJam, vtKK.NoGL, (KreditGL+DebetGL) AS KreditGL FROM vtKK 
				INNER JOIN ttgeneralledgerhd ON vtKK.NoGL = ttgeneralledgerhd.NoGL 
				INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL 
				WHERE KKJenis = 'BBN Plus' AND NoAccount = '11060200' AND KKLINk IN (SELECT DKNo FROM ttBMit WHERE BMNO = xBMNo)) vtkk
			ON ttbmit.DKNo = vtkk.KKLINK
			GROUP BY KKNo;			
			
			INSERT INTO ttglhpit SELECT xNoGL, @BKTgl, KKNo, '11060200','Piutang', KreditGL FROM TSearchLink WHERE BMBayar > KreditGL AND KreditGL > 0;
			INSERT INTO ttglhpit SELECT xNoGL, @BKTgl, KKNo, '11060200','Piutang', BMBayar FROM TSearchLink WHERE BMBayar <= KreditGL AND BMBayar > 0;
		END;	
		WHEN "BM Dari Kas" THEN
		BEGIN
			SET @row_number = 0; 		
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, (@row_number:=@row_number + 1),@NoAccount , MyPesankuItem , @BMNominal, 0;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BMTgl, (@row_number:=@row_number + 1),'11040000' , MyPesankuItem  AS KeteranganGL,  0 AS DebetGL,@BMNominal AS KreditGL;
		END;	
		WHEN "Penjualan Multi" THEN
		BEGIN
		END;	
	END CASE;

	#Memberi Nilai HPLink
	SET @CountGL = 0;
	SELECT MAX(GLLink), IFNULL(COUNT(NoGL),0) INTO @GLLink, @CountGL FROM ttglhpit WHERE NoGL = xNoGL GROUP BY NoGL;
	IF @CountGL = 0 THEN
		UPDATE ttgeneralledgerhd SET HPLink = '--' WHERE NoGL = xNoGL;
	ELSEIF @CountGL = 1 THEN
		UPDATE ttgeneralledgerhd SET HPLink = @GLLink WHERE NoGL = xNoGL;
	ELSE
		UPDATE ttgeneralledgerhd SET HPLink = 'Many' WHERE NoGL = xNoGL;						
	END IF;	
	
	#Menghitung Total Debet & Kredit
	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttbmhd SET NoGL = xNoGL  WHERE BMNo = xBMNo;
END$$

DELIMITER ;

