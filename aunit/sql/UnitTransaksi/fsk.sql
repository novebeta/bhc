DELIMITER $$
DROP PROCEDURE IF EXISTS `fsk`$$
CREATE DEFINER=`root`@`%` PROCEDURE `fsk`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xSKNo VARCHAR(100),
IN xSKNoBaru VARCHAR(10),
IN xDKNo VARCHAR(10),
IN xDKTgl DATE,
IN xDKJenis VARCHAR(6),
IN xSPKNo VARCHAR(15),
IN xSKTgl DATE,
IN xSKJam DATETIME,
IN xSKCetak VARCHAR(5),
IN xSalesNama VARCHAR(50),
IN xTeamKode VARCHAR(15),
IN xLeaseKode VARCHAR(10),
IN xSKMemo VARCHAR(100),
IN xNoBukuServis VARCHAR(100),
IN xCusNama VARCHAR(50),
IN xCusKTP VARCHAR(18),
IN xCusKabupaten VARCHAR(50),
IN xCusAlamat VARCHAR(75),
IN xCusRT VARCHAR(3),
IN xCusRW VARCHAR(3),
IN xCusKecamatan VARCHAR(50),
IN xCusTelepon VARCHAR(30),
IN xCusKodePos VARCHAR(35),
IN xCusKelurahan VARCHAR(50),
IN xCusPembeliNama VARCHAR(50),
IN xCusPembeliKTP VARCHAR(18),
IN xCusPembeliAlamat VARCHAR(75),
IN xMotorType VARCHAR(50),
IN xMotorTahun DECIMAL(4,0),
IN xMotorWarna VARCHAR(35),
IN xSKPengirim VARCHAR(50),
IN xMotorNoMesin VARCHAR(25),
IN xMotorNoRangka VARCHAR(25),
IN xDKHarga DECIMAL(11,0),
IN xMotorNama VARCHAR(100),
IN xBBN DECIMAL(11,0),
IN xMotorKategori VARCHAR(15),
IN xRKNo VARCHAR(10),
IN xNoGL VARCHAR(10),
IN xLokasiKodeSK VARCHAR(15))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			#CekPiutangVSTitipanSK liat di module, Setelah Jurnal KK KM, BK , BM dibuat prgor
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Daftar Surat Jalan Konsumen [SK] ");
		END;
		WHEN "Load" THEN
		BEGIN
			SET @JumMotor = 0; SELECT COUNT(MotorNoMesin) INTO @JumMotor FROM tmotor WHERE SKNo = xSKNo;
			CALL ctvmotorLokasi(xSKNo,@JumMotor);

			#PerbaikiSalahJam()	
			UPDATE tvmotorlokasi INNER JOIN 
			(SELECT MotorNoMesin, NoKeluar, JamKeluar, JamMasuk, IDKeluar FROM vmotorsalahjam  WHERE JamMasuk BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND NOW()) SalahJam
			ON tvmotorlokasi.MotorNoMesin = SalahJam.MotorNoMesin AND tvmotorlokasi.NoTrans = SalahJam.NoKeluar
			SET  Jam = DATE_ADD(JamMasuk,INTERVAL 1 SECOND) WHERE LEFT(tvmotorlokasi.NoTrans,2) = 'SK';
			UPDATE ttsk INNER JOIN 
			(SELECT MotorNoMesin, NoKeluar, JamKeluar, JamMasuk, IDKeluar FROM vmotorsalahjam  WHERE JamMasuk BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND NOW()) SalahJam
			ON ttsk.SKNo = SalahJam.NoKeluar
			SET  SKTgl = DATE(JamMasuk), SKJam = DATE_ADD(JamMasuk,INTERVAL 1 SECOND);
			
			SET oStatus = 0; SET oKeterangan = CONCAT("Load data Surat Jalan Konsumen [SK] No: ",xSKNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SET @AutoNo = CONCAT('SK',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(SKNo,5)+1),5,0)) INTO @AutoNo FROM ttsk WHERE ((SKNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(SKNo) = 10) ORDER BY SKNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('SK',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xSKNo = @AutoNo;
			SET oStatus = 0;SET oKeterangan = CONCAT("Tambah data baru Surat Jalan Konsumen [SK] No: ",xSKNo);
		END;
		WHEN "LoopDK" THEN
		BEGIN
			UPDATE ttsk SET DKNo = xDKNo WHERE SKNo = xSKNo;
			UPDATE tmotor SET SKNo = '--' WHERE SKNo = xSKNo;		
			UPDATE tmotor SET SKNo = xSKNo WHERE DKNo = xDKNo;
			SET oStatus = 0;SET oKeterangan = CONCAT("Data Konsumen");	
		END;
		WHEN "LoopMO" THEN
		BEGIN
			SET oStatus = 0;SET oKeterangan = CONCAT("Mutasi Otomatis");		
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @SKNoOLD = xSKNo;

			IF LOCATE('=',xSKNo) <> 0  THEN /* Generate new auto number */
				SET @AutoNo = CONCAT('SK',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(SKNo,5)+1),5,0)) INTO @AutoNo FROM ttsk WHERE ((SKNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(SKNo) = 10) ORDER BY SKNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('SK',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xSKNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xSKNoBaru = xSKNo;
			END IF;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSDNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Cek Jam
			IF oStatus = 0 THEN
				SELECT SKJam INTO @SKJamOLD FROM ttsk WHERE SKNo = @SKNoOLD;
				IF xSKJam < DATE_ADD(@SKJamOLD, INTERVAL -5 DAY) THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT(oKeterangan,"Gagal menyimpan data, Tgl-Jam ", DATE_FORMAT(xSKJam,'%d-%m-%Y %k:%i:%s'), ' tidak boleh lebih kecil dari Tgl-Jam semula ',DATE_FORMAT(@SKJamOLD ,'%d-%m-%Y %k:%i:%s'));
				ELSE
					SET oStatus = 0;
				END IF;
			END IF;
	
			#Cek Motor Movement
			IF oStatus = 0 THEN
				SELECT SKJam INTO xSKJam FROM ttSK WHERE SKNo = xSKNo;
				SELECT IFNULL(GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>'),'')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin IN (SELECT MotorNoMesin FROM tmotor WHERE SKNo = xSKNo) AND JAM > xSKJam	AND NoTrans <> xSKNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(xSKJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
					SET oStatus = 1;
				ELSE
					SET oStatus = 0;				
				END IF;
			END IF;	
			
			#Proses Update
			IF oStatus = 0 THEN 
				#xLokasiKodeSK, dikasih program
				SELECT LokasiKode INTO xLokasiKodeSK FROM vmotorLastlokasi WHERE Kondisi = 'IN' AND LokasiKode <> 'Repair' AND MotorNoMesin = xMotorNoMesin AND Jam < xSKJam;
				
				#Update Utama
				UPDATE ttsk
				SET SKNo = xSKNoBaru, `SKTgl` = xSKTgl, `LokasiKode` = xLokasiKodeSK, `SKMemo` = xSKMemo, `UserID` = xUserID, `SKJam` = xSKJam, `NoGL` = xNoGL,
				`SKCetak` = xSKCetak, `SKPengirim` = xSKPengirim, `DKNo` = xDKNo
				WHERE SKNo = xSKNo;
				SET xSKNo = xSKNoBaru;
				
				UPDATE tmotor SET SKNo = xSKNo WHERE DKNo = xDKNo;
				UPDATE ttdk SET NoBukuServis = xNoBukuServis WHERE DKNo = xDKNo;

				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @SKNoOLD; #Hapus Jurnal No Transaksi Lama
				CALL jsk(xSKNo,xUserID); #Posting Jurnal
							                       
				IF @AutoNo = 'Edit' THEN #Edit
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Surat Jalan Konsumen','ttsk', xSKNo); 

					#EditDelTvMotorLokasi()
					DELETE FROM tvmotorlokasi WHERE NoTrans = @SKNoOLD;

					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Surat Jalan Konsumen [SK] No: ", xSKNo);
				ELSE #Add
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Surat Jalan Konsumen','ttsk', xSKNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Surat Jalan Konsumen [SS] No: ", xSKNo);
				END IF;
				
				#InsertTvMotorLokasi()
				SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttSK.LokasiKode AS LokasiKode, ttSK.SKNo AS NoTrans, ttSK.SKJam AS Jam, 'OUT' AS Kondisi 
				INTO @MotorAutoN,  @MotorNoMesin , @LokasiKode, @NoTrans, @Jam, @Kondisi
				FROM tmotor INNER JOIN ttSK ON tmotor.SKNo = ttSK.SKNo WHERE ttSK.SKNo = xSKNo;
				INSERT INTO tvmotorlokasi (MotorAutoN, MotorNoMesin, LokasiKode, NoTrans, Jam, Kondisi)
				VALUES (@MotorAutoN, @MotorNoMesin, @LokasiKode, @NoTrans,@Jam, 'OUT');

				#CekJamMO()
				
				#IF @BBN = 0 THEN
				#SetPemutihanByOffTheRoad
				#END IF;
			ELSEIF oStatus = 1 THEN
			  SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;
			
			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSKNo;
			IF @GLValid = 'Sudah' THEN	/* Jurnal telah tervalidasi data tidak boleh dihapus */	
				SET oStatus = 1; SET oKeterangan = CONCAT("No SK:", xSKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Cek Motor Movement
			IF oStatus = 0 THEN
				SELECT SKJam INTO xSKJam FROM ttSK WHERE SKNo = xSKNo;
				SELECT GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin IN (SELECT MotorNoMesin FROM tmotor WHERE SKNo = xSKNo) AND JAM > xSKJam	AND NoTrans <> xSKNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(xSKJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
					SET oStatus = 1;
				ELSE
					SET oStatus = 0;				
				END IF;
			END IF;
			
			#Update Proses
			IF oStatus = 0 THEN
				DELETE FROM ttsk WHERE SKNo = xSKNo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xSKNo;
				UPDATE tmotor SET SKNo = '--' WHERE SKNo = xSKNo;
				DELETE FROM tvmotorlokasi WHERE NoTrans = xSKNo;
				
				DELETE FROM ttpbhd WHERE PBNo = REPLACE(xSKNo,"SK", "MO");
				DELETE FROM ttsmhd WHERE SMNo = REPLACE(xSKNo,"SK", "MO");
      
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Surat Jalan Konsumen','ttsk', xSKNo); 
				SET oKeterangan = CONCAT("Berhasil menghapus data Surat Jalan Konsumen [SK] No: ",xSKNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 IF LOCATE('=',xSKNo) <> 0 THEN
				  DELETE FROM ttsk WHERE SKNo = xSKNo;
				  SET oKeterangan = CONCAT("Batal menyimpan data Surat Jalan Konsumen [SK] No: ", xSKNoBaru);
			 ELSE
				  SET oKeterangan = CONCAT("Batal menyimpan data Surat Jalan Konsumen [SK] No: ", xSKNo);
			 END IF;
		END;
		WHEN "Jurnal" THEN
		BEGIN
			 IF LOCATE('=',xSKNo) <> 0 THEN /* Data Baru, belum tersimpan tidak boleh di jurnal */
				  SET oStatus = 1;
				  SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			 ELSE
				  CALL jSK(xSKNo,xUserID);
				  SET oStatus = 0;
				  SET oKeterangan = CONCAT("Berhasil memproses Jurnal Surat Jalan Konsumen [SK] No: ", xSKNo);
			 END IF;
		END;
		WHEN "Print" THEN
		BEGIN
			IF xCetak = "1" THEN
				SELECT   ttsk.SKNo, ttsk.SKTgl, ttsk.LokasiKode, ttsk.SKMemo, ttsk.UserID, ttdk.CusKode, tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, 
				tdcustomer.CusKabupaten, tdcustomer.CusTelepon, tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoMesin, tmotor.MotorNoRangka, ttdk.DKNo, ttdk.DKHarga, tdlokasi.LokasiNama, ttsk.SKJam, 
				tmotor.FBHarga, ttdk.DKTgl, ttdk.DKJenis, tdcustomer.CusKTP, tdcustomer.CusKodePos, tmotor.RKNo, tmotor.PlatNo, ttsk.NoGL, ttdk.LeaseKode, tdmotortype.MotorNama, tdmotortype.MotorKategori, 
				tdcustomer.CusPembeliNama, tdcustomer.CusPembeliKTP, tdcustomer.CusPembeliAlamat, tdsales.SalesNama, ttdk.TeamKode, ttdk.SPKNo, IFNULL(tmotor.MotorAutoN, 0) AS MotorAutoN, ttdk.BBN, ttsk.SKCetak, 
				ttsk.SKPengirim, IFNULL(ttdk.NoBukuServis, '--') AS NoBukuServis, tmotor.PlatNo AS EXPR1, tdcustomer.CusTelepon2
				FROM  tdsales 
				INNER JOIN ttdk ON tdsales.SalesKode = ttdk.SalesKode 
				RIGHT OUTER JOIN ttsk 
				LEFT OUTER JOIN tdlokasi ON ttsk.LokasiKode = tdlokasi.LokasiKode 
				LEFT OUTER JOIN tmotor ON ttsk.SKNo = tmotor.SKNo 
				LEFT OUTER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType ON ttdk.DKNo = tmotor.DKNo 
				LEFT OUTER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
				WHERE (ttsk.SKNo = xSKNo);
			END IF;		 
			SET oStatus = 0;SET oKeterangan = CONCAT("Cetak Surat Jalan Konsumen No: ", xSKNo);
			UPDATE ttsk SET SKCetak = 'Sudah' WHERE SKNo = xSKNo; 
			INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
			(NOW(), xUserID , 'Cetak' , 'Surat Jalan Konsumen','ttsk', xSKNo); 
		END;
	END CASE;
END$$

DELIMITER ;

