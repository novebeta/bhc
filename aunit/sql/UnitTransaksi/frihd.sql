DELIMITER $$

DROP PROCEDURE IF EXISTS `frihd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `frihd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xSMNo VARCHAR(100),
IN xSMNoBaru VARCHAR(100),
IN xSMTgl DATE,
IN xLokasiAsal VARCHAR(15),
IN xLokasiTujuan VARCHAR(15),
IN xSMMemo VARCHAR(100),
IN xSMTotal DECIMAL(14,2),
IN xSMJam DATETIME
)
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			CALL ctvmotorLokasi('oo',0); #Cek kondisi tvmotorlokasi		

			#Hapus tvmotorlokasi yg belum ada link dengan transaksi SM
			DELETE tvmotorlokasi FROM tvmotorlokasi LEFT OUTER JOIN ttsmhd ON ttsmhd.SMNO = tvmotorlokasi.NoTrans	
			WHERE NoTrans LIKE 'RI%' AND ttsmhd.SMNO IS NULL;

			#Hapus tvmotorlokasi yg belum ada link dengan transaksi PB
			DELETE tvmotorlokasi FROM tvmotorlokasi LEFT OUTER JOIN ttpbhd ON ttpbhd.PBNO = tvmotorlokasi.NoTrans	
			WHERE NoTrans LIKE 'RI%' AND ttpbhd.PBNO IS NULL;
			
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Daftar Mutasi Repair IN");
		END;
		WHEN "Load" THEN
		BEGIN
			SET oStatus = 0;
			SET @JumMotor = 0; SELECT COUNT(MotorNoMesin) INTO @JumMotor FROM ttsmit WHERE SMNo = xSMNo;
			CALL ctvmotorLokasi(xSMNo,@JumMotor);
			SET oKeterangan = CONCAT("Load Mutasi Repair IN No: ",xSMNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			 SET @AutoNo = CONCAT('RI',RIGHT(YEAR(NOW()),2),'-');
			 SELECT CONCAT(@AutoNo,LPAD((RIGHT(SMNo,5)+1),5,0)) INTO @AutoNo FROM ttsmhd WHERE ((SMNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(SMNo) = 10) ORDER BY SMNo DESC LIMIT 1;
			 IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('RI',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			 SET xSMNo = @AutoNo;
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Tambah data baru Mutasi Repair IN No: ",xSMNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = CONCAT('Lokasi Asal :',xLokasiAsal, '  Lokasi Tujuan :', xLokasiTujuan);
			SET @AutoNo = 'Edit';
			SET @SMNoOLD = xSMNo;

			IF LOCATE('=',xSMNo) <> 0 THEN 
				SET @AutoNo = CONCAT('RI',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(SMNo,5)+1),5,0)) INTO @AutoNo FROM ttsmhd WHERE ((SMNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(SMNo) = 10) ORDER BY SMNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('RI',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xSMNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit'; 
				SET xSMNoBaru = xSMNo;
			END IF;
	
			# Cek Jam Transaksi
			IF oStatus = 0 THEN 		
				IF @AutoNo = 'Edit' THEN
					SELECT SMJam INTO @SMJamOLD FROM ttsmhd WHERE SMNo = @SMNoOLD;
					IF xSMJam < @SMsJamOLD THEN
						SET oStatus = 1;
						SET oKeterangan = CONCAT(oKeterangan,"Gagal menyimpan data, Tgl-Jam ", DATE_FORMAT(xSMJam,'%d-%m-%Y %k:%i:%s'), ' tidak boleh lebih kecil dari Tgl-Jam semula ',DATE_FORMAT(@SMJamOLD ,'%d-%m-%Y %k:%i:%s'));
					ELSE
						SET oStatus = 0;
					END IF;
				END IF;
			END IF;
			
			#Cek Motor Movement (motor sudah berpindah [jam trans > jam sm])
			IF oStatus = 0 THEN
				SELECT SMJam INTO xSMJam FROM ttsmhd WHERE SMNo = xSMNo;
				SELECT IFNULL(GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>'),'')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin IN (SELECT MotorNoMesin FROM ttsmit WHERE SMNo = xSMNo) AND JAM > xSMJam	AND NoTrans <> xSMNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(xSMJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
				ELSE
					SET oStatus = 0;	
					SET oKeterangan = '';			
				END IF;
			END IF;
			#Update Proses
			IF oStatus = 0 THEN
				#Update Utama
				UPDATE ttsmhd
				SET SMNo = xSMNoBaru, SMTgl = xSMTgl, LokasiAsal = xLokasiAsal, LokasiTujuan = xLokasiTujuan, SMMemo = xSMMemo, UserID = xUserID, SMJam = xSMJam, SMTotal = xSMTotal
				WHERE SMNo = @SMNoOLD;			
				SET xSMNo = xSMNoBaru;

				UPDATE ttpbhd
				SET PBNo = xSMNoBaru, PBTgl = xSMTgl, LokasiAsal = xLokasiAsal, LokasiTujuan = xLokasiTujuan, PBMemo = xSMMemo, UserID = xUserID, PBJam = DATE_ADD(xSMJam,INTERVAL 1 SECOND) , PBTotal = xSMTotal
				WHERE PBNo = @SMNoOLD;			
				SET xSMNo = xSMNoBaru;
				

				#Hitung SubTotal SM
				SET @SMTotal = 0;
				SELECT IFNULL(SUM(FBHarga),0) INTO @SMTotal FROM ttsmit 
				INNER JOIN tmotor  ON  tmotor.MotorAutoN = ttsmit.MotorAutoN AND tmotor.MotorNoMesin = ttsmit.MotorNoMesin WHERE SMNo = xSMNo;
				UPDATE ttsmhd	SET SMTotal = @SMTotal	WHERE SMNo = xSMNo;

				#Hitung SubTotal PB
				SET @PDTotal = 0;
				SELECT IFNULL(SUM(FBHarga),0) INTO @PBTotal FROM ttpbit 
				INNER JOIN tmotor  ON  tmotor.MotorAutoN = ttpbit.MotorAutoN AND tmotor.MotorNoMesin = ttpbit.MotorNoMesin WHERE PBNo = xSMNo;
				UPDATE ttpbhd	SET PBTotal = @PBTotal	WHERE PBNo = xSMNo;
			
				#tvmotorlokasi SM
				DELETE FROM tvmotorlokasi WHERE NoTrans = @SMNoOLD AND Kondisi = 'OUT Pos';
				DELETE FROM tvmotorlokasi WHERE NoTrans = xSMNo AND Kondisi = 'OUT Pos';
				INSERT INTO tvmotorlokasi
				SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttSMhd.LokasiAsal AS LokasiKode, ttSMit.SMNo AS NoTrans, ttSMhd.SMJam AS Jam, 'OUT Pos' AS Kondisi 
				FROM tmotor 
				INNER JOIN ttSMit ON tmotor.MotorNoMesin = ttSMit.MotorNoMesin AND tmotor.MotorAutoN = ttSMit.MotorAutoN 
				INNER JOIN ttSMhd ON ttSMit.SMNo = ttSMhd.SMNo WHERE ttSMhd.SMNo = xSMNo;

				#tvmotorlokasi SM
				DELETE FROM tvmotorlokasi WHERE NoTrans = @SMNoOLD  AND Kondisi = 'IN';
				DELETE FROM tvmotorlokasi WHERE NoTrans = xSMNo  AND Kondisi = 'IN';
				INSERT INTO tvmotorlokasi
				SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttPBhd.LokasiAsal AS LokasiKode, ttPBit.PBNo AS NoTrans, ttPBhd.PBJam AS Jam, 'IN' AS Kondisi 
				FROM tmotor 
				INNER JOIN ttPBit ON tmotor.MotorNoMesin = ttPBit.MotorNoMesin AND tmotor.MotorAutoN = ttPBit.MotorAutoN 
				INNER JOIN ttPBhd ON ttPBit.PBNo = ttPBhd.PBNo WHERE ttPBhd.PBNo = xSMNo;

				
				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID ,	 'Edit' , 'Mutasi Repair IN','ttsmhd', xSMNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data Mutasi Internal Ke Pos No : ", xSMNo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Mutasi Repair IN','ttsmhd', xSMNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menambah data Mutasi Internal Ke Pos No : ", xSMNo);
				END IF;			

			ELSEIF oStatus = 1 THEN
				SET oKeterangan = oKeterangan;
			END IF;

		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = '';
			SET oStatus = 0;

			#Cek Motor Movement
			IF oStatus = 0 THEN
				SELECT SMJam INTO xSMJam FROM ttsmhd WHERE SMNo = xSMNo;
				SELECT IFNULL(GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>'),'')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin IN (SELECT MotorNoMesin FROM ttsmit WHERE SMNo = xSMNo) AND JAM > xSMJam	AND NoTrans <> xSMNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(xSMJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
					SET oStatus = 1;
				ELSE
					SET oStatus = 0;				
				END IF;
			END IF;	

			#Update Proses
			IF oStatus = 0 THEN
				DELETE FROM ttsmhd WHERE SMNo = xSMNo;
				DELETE FROM ttsmit WHERE SMNo = xSMNo;
				DELETE FROM tvmotorlokasi WHERE NoTrans = xSMNo;

				DELETE FROM ttpbhd WHERE PBNo = xSMNo;
				DELETE FROM ttpbit WHERE PBNo = xSMNo;
				DELETE FROM tvmotorlokasi WHERE NoTrans = xSMNo;
				
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Mutasi Repair IN','ttsmhd', xSMNo); 
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Mutasi Internal Ke Pos No: ",xSMNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;					
		END;
		WHEN "Cancel" THEN
		BEGIN
			 IF LOCATE('=',xSMNo) <> 0 THEN
				  DELETE FROM ttsmhd WHERE SMNo = xSMNo;
				  SET oKeterangan = CONCAT("Batal menyimpan data Mutasi Repair IN No: ", xSMNoBaru);
			 ELSE
				  SET oKeterangan = CONCAT("Batal menyimpan data Mutasi Repair IN No: ", xSMNo);
			 END IF;
		END;
		WHEN "Print" THEN
		BEGIN
			IF xCetak = "1" THEN
				SELECT ttsmhd.LokasiAsal, ttsmhd.LokasiTujuan, ttsmhd.SMMemo, ttsmhd.SMNo, ttsmhd.SMTgl, ttsmhd.SMTotal, ttsmhd.UserID, 
				IFNULL(tdlokasi.LokasiNama, '--') AS LokasiNamaAsal, IFNULL(tdlokasi_1.LokasiNama, '--') AS LokasiNamaTujuan, ttsmhd.SMJam, IFNULL(ttpbhd.PBNo, '--') AS PBNo
				FROM ttsmhd 
				LEFT OUTER JOIN tdlokasi ON ttsmhd.LokasiAsal = tdlokasi.LokasiKode 
				LEFT OUTER JOIN tdlokasi tdlokasi_1 ON ttsmhd.LokasiTujuan = tdlokasi_1.LokasiKode 
				LEFT OUTER JOIN ttpbhd ON ttsmhd.SMNo = ttpbhd.SMNo
				WHERE (ttsmhd.SMNo = xSMNo);
			END IF;
			IF xCetak = "2" THEN
				SELECT ttsmit.SMNo, ttsmit.MotorNoMesin, ttsmit.MotorAutoN, tmotor.MotorType, tmotor.MotorTahun, tmotor.MotorWarna, tmotor.MotorNoRangka, tmotor.FBHarga, tdmotortype.MotorNama
				FROM ttsmit 
				INNER JOIN tmotor ON ttsmit.MotorNoMesin = tmotor.MotorNoMesin AND ttsmit.MotorAutoN = tmotor.MotorAutoN 
				LEFT OUTER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
				WHERE  (ttsmit.SMNo = xSMNo);
			END IF;
			SET oStatus = 0; SET oKeterangan = CONCAT("Cetak Repair IN No: ", xSMNo);
			INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
			(NOW(), xUserID , 'Cetak' , 'Repair IN','ttsmhd', xSMNo);
		END;
	END CASE;
END$$

DELIMITER ;

