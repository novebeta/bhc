DELIMITER $$
DROP PROCEDURE IF EXISTS `jpd`$$
CREATE DEFINER=`root`@`%` PROCEDURE `jpd`(IN xPDNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE MyAutoN INTEGER DEFAULT 1;
	DECLARE TotFBHarga DOUBLE DEFAULT 0;
	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xPDNo ORDER BY NOGL DESC LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;
	
	SELECT IFNULL(SUM(FBHarga),0) INTO TotFBHarga FROM tmotor WHERE PDNo = xPDNo;
	UPDATE ttPD SET PDTotal = TotFBHarga WHERE PDNo = xPDNo;
	
	SELECT PDNo, PDTgl, SDNo, LokasiKode, DealerKode, PDMemo, PDTotal, UserID, PDJam, NoGL INTO
	@PDNo, @PDTgl, @SDNo, @LokasiKode, @DealerKode, @PDMemo, @PDTotal, @UserID, @PDJam, @NoGL
	FROM ttPD WHERE PDNo = xPDNo;
	
	SELECT DealerNama INTO @DealerNama FROM tddealer WHERE DealerKode = @DealerKode LIMIT 1;

	#Numbering JT
	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
    SET xNoGL =  @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;
	
	#Delete
	DELETE FROM ttgeneralledgerhd WHERE GLLink = xPDNo;
	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL;  
	DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;  
	DELETE FROM ttglhpit WHERE NoGL = xNoGL;

	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, @PDTgl, 'IP', CONCAT('Post Invoice Penerimaan dari ',@DealerNama), TotFBHarga, TotFBHarga, xPDNo, xUserID, '--', xGLValid);

	#Item
	INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
	(xNoGL, @PDTgl, 1, '11150100', CONCAT('Post Invoice Penerimaan ke ',@DealerNama), @PDTotal, 0 );

	SET @PiutAfiliasi = '24020301';
	SELECT IFNULL(NoAccount,'24020301') INTO @PiutAfiliasi FROM traccount WHERE SUBSTRING_INDEX(TRIM(NamaAccount), ' ', -1) LIKE CONCAT('%',SUBSTRING_INDEX(TRIM(@DealerNama), ' ', -1)) AND JenisAccount ='Detail' AND NoParent = '24020300' LIMIT 1; 

	INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
	(xNoGL, @PDTgl, 2, @PiutAfiliasi, CONCAT('Post Invoice Penerimaan ke ',@DealerNama), 0, @PDTotal);

	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;
	CALL jHutangKreditPiutangDebet(xNoGL);
	UPDATE ttPD SET NoGL = xNoGL  WHERE PDNo = xPDNo;
END$$

DELIMITER ;