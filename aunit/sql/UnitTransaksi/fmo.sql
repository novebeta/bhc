DELIMITER $$
DROP PROCEDURE IF EXISTS `fmo`$$
CREATE DEFINER=`root`@`%` PROCEDURE `fmo`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xMONo VARCHAR(10),
IN xMONoBaru VARCHAR(10),
IN xMOTgl DATE,
IN xMOJam DATETIME,
IN xLokasiAsal VARCHAR(15),
IN xLokasiTujuan VARCHAR(15),
IN xSKNo VARCHAR(10),
IN xMotorAutoN VARCHAR(6),
IN xMotorNoMesin VARCHAR(25))
BEGIN
	CASE xAction
		WHEN "Load" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan =CONCAT('Load Mutasi Otomatis',XMONo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SET xMONo = REPLACE(xSKNo,'SK','MO');
			SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru MO No: ",xMONo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = '';
			IF LOCATE('SK',xSKNo) = 0 THEN
				SET xSKNo = REPLACE(xMONo,'MO','SK');
			END IF;
			IF LOCATE('MO',xMONo) = 0 THEN
				SET xMONo = REPLACE(xSKNo,'SK','MO');
			END IF;
			
			IF xLokasiAsal = xLokasiTujuan THEN
				SET oStatus = 1; SET oKeterangan = CONCAT('Lokasi Asal:', xLokasiAsal, ' = ', ' Lokasi Tujuan :', xLokasiTujuan);			
			END IF;
			SELECT SMNo INTO @SMNo FROM ttsmhd WHERE SMNo = xMONo OR SMNo = REPLACE(xSKNo,'SK','MO');
			IF @SMNo IS NULL THEN
				SET oStatus = 0;
			ELSE
				SET oStatus = 1;SET oKeterangan = CONCAT('Surat Jalan Konsumen [SK} No:', xSKNo, ' telah memiliki Mutasi Otomatis ', @SMNo);			
			END IF;
			
			IF oStatus = 0 THEN
				SELECT SUM(FBHarga) INTO @MySMTotal FROM tmotor WHERE SKNo = xSKNo;
				SELECT MotorAutoN, MotorNoMesin INTO @MotorAutoN, @MotorNoMesin FROM tmotor WHERE SKNo = xSKNo;

				INSERT INTO ttsmhd (SMNo, SMTgl, LokasiAsal, LokasiTujuan, SMMemo, SMTotal, UserID, SMJam) VALUES (
				xMONo, xMOTgl, xLokasiAsal, xLokasiTujuan, CONCAT('Mutasi Otomatis Dari ', xSKNo),  @MySMTotal , xUserID , xMOJam);
				UPDATE ttsmhd SET SMJam =  xMOJam WHERE SMNo = xMONo;			
				INSERT INTO ttsmit (SMNo, MotorNoMesin, MotorAutoN) VALUES (xMONo, @MotorNoMesin , @MotorAutoN);
				INSERT INTO tvmotorlokasi (MotorNoMesin, MotorAutoN, LokasiKode, NoTrans, Jam, Kondisi) VALUES ( 
				@MotorNoMesin , @MotorAutoN, xLokasiAsal, xMOno, xMOJam, 'OUT Pos'); 

				INSERT INTO ttpbhd (PBNo, PBTgl, SMNo, LokasiAsal, LokasiTujuan, PBMemo, PBTotal, UserID, PBJam) VALUES (
				xMONo, xMOTgl, xMONo, xLokasiAsal, xLokasiTujuan,CONCAT('Mutasi Otomatis Dari ', xMONo), @MySMTotal, xUserID , DATE_ADD(xMOJam, INTERVAL 1 SECOND));
				UPDATE ttpbhd SET PBJam = DATE_ADD(xMOJam, INTERVAL 1 SECOND) WHERE PBNo = xMONo; 
				INSERT INTO ttpbit (PBNo, MotorNoMesin, MotorAutoN) VALUES (xMONo, @MotorNoMesin , @MotorAutoN);
				INSERT INTO tvmotorlokasi (MotorNoMesin, MotorAutoN, LokasiKode, NoTrans, Jam, Kondisi) VALUES ( 
				@MotorNoMesin , @MotorAutoN, xLokasiTujuan, xMOno, DATE_ADD(xMOJam, INTERVAL 1 SECOND) , 'IN'); 

				UPDATE ttsk SET LokasiKode = xLokasiTujuan WHERE SKNo = xSKNo;
				UPDATE ttsk SET SKJam = DATE_ADD(xMOJam, INTERVAL 5 SECOND) WHERE SKNo = xSKNo;
				UPDATE tvmotorlokasi SET Jam = DATE_ADD(xMOJam, INTERVAL 5 SECOND), LokasiKode = xLokasiTujuan
				WHERE MotorAutoN = @MotorAutoN AND MotorNoMesin = @MotorNoMesin  AND NoTrans = XSKNo;
			END IF;		
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = '';
			SET oStatus = 0;
			IF LOCATE('SK',xSKNo) = 0 THEN
				SET xSKNo = REPLACE(xMONo,'MO','SK');
			END IF;
			IF LOCATE('MO',xMONo) = 0 THEN
				SET xMONo = REPLACE(xSKNo,'SK','MO');
			END IF;
			
			#Update Proses
			IF oStatus = 0 THEN
				DELETE FROM ttsmhd WHERE SMNo = xMONo;
				DELETE FROM ttsmit WHERE SMNo = xMONo;
				DELETE FROM tvmotorlokasi WHERE NoTrans = xMONo;

				DELETE FROM ttpbhd WHERE PBNo = xMONo;
				DELETE FROM ttpbit WHERE PBNo = xMONo;
				DELETE FROM tvmotorlokasi WHERE NoTrans = xMONo;

				SELECT LokasiAsal INTO @LokasiAsal FROM ttsmhd WHERE SMNo = xMONo;		
				UPDATE ttsk SET LokasiKode = @LokasiAsal, SKJam = xMOJam WHERE SKNo = xSKNo;
				UPDATE tvmotorlokasi SET  LokasiKode = @LokasiAsal , Jam = xMOJam WHERE NoTrans = xSKNo;
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;			
		END;
	END CASE;
END$$

DELIMITER ;

