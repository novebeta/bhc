DELIMITER $$

DROP PROCEDURE IF EXISTS `jkm`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jkm`(IN xKMNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE oKategori VARCHAR(100) DEFAULT '';

	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xKMNo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;

	SELECT KMNo, KMTgl, KMMemo, KMNominal, KMJenis, KMPerson, KMLink, UserID, LokasiKode, KMJam, NoGL
	INTO @KMNo, @KMTgl, @KMMemo, @KMNominal, @KMJenis, @KMPerson, @KMLink, @UserID, @LokasiKode, @KMJam, @NoGL
	FROM ttKM	WHERE KMNo = xKMNo;

	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;

	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL; DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;DELETE FROM ttglhpit WHERE NoGL = xNoGL;
   DELETE FROM ttgeneralledgerhd WHERE GLLink = xKMNo;

	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, @KMTgl, 'KM', CONCAT('Post Kas Masuk ',@KMJenis), @KMNominal, @KMNominal, xKMNo, xUserID, '--', xGLValid);

	#Item
	CASE @KMJenis
		WHEN 'Inden' THEN
			SET oKategori = '';
		WHEN 'Tunai' THEN
			SET oKategori = '';
		WHEN 'Kredit' THEN
			SET oKategori = '';
		WHEN 'Piutang UM' THEN
			SET oKategori = '';
		WHEN 'Leasing' THEN
			SET oKategori = '';
		WHEN 'Piutang Sisa' THEN
			SET oKategori = '';			
		WHEN 'Angsuran Kredit' THEN
			SET oKategori = '';			
		WHEN 'BBN Plus' THEN
			SET oKategori = '';
		WHEN 'BBN Acc' THEN
			SET oKategori = '';
		WHEN 'Umum' THEN
			SET oKategori = '';			
		WHEN 'KM Dari Bank' THEN
			SET oKategori = '';
		WHEN 'Angsuran Karyawan' THEN
			SET oKategori = '';
		WHEN 'KM Pos Dari Dealer' THEN
			SET oKategori = '';
		WHEN 'KM Dealer Dari Pos' THEN
			SET oKategori = '';
		WHEN 'KM Dari Bengkel' THEN
			SET oKategori = '';
	END CASE;
	
	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttKM SET NoGL = xNoGL  WHERE KMNo = xKMNo;
END$$

DELIMITER ;

