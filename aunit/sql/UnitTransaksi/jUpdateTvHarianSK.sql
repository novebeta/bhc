DELIMITER $$

DROP PROCEDURE IF EXISTS `jUpdateTvHarianSK`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jUpdateTvHarianSK`(IN MyGLLink VARCHAR(18), IN MyNoGL VARCHAR(10), IN MyTglGL DATE)
BEGIN
		DECLARE MyDKDP1130100PK DOUBLE DEFAULT 0;
	DECLARE MyDKDPLLeasing1130100, MyDKDPInden1130100, MyDKDPTerima1130100 DOUBLE DEFAULT 0;
	DECLARE MyDKDP2140500HTK DOUBLE DEFAULT 0;
   DECLARE MyDKDPLLeasing2140500, MyDKDPInden2140500, MyDKDPTerima2140500 DOUBLE DEFAULT 0;
   DECLARE MyDKNettoPK, MyDKNettoHTK DOUBLE DEFAULT 0;
   
	/*Variabel yang dihitung Setelah ada KM yang sama dengan tanggal SK*/
	DECLARE MyDKDPPKTerbayarTotal, MyDKDPPKTerbayarLeasing, MyDKDPPKTerbayarInden, MyDKDPPKTerbayarTerima DOUBLE DEFAULT 0;
	DECLARE MyDKNettoKMPKons, MyDKNettoKMPLease DOUBLE DEFAULT 0;

	DECLARE MyDKNetto,MyDKDPLLeasingAcct, MyDKDPIndenAcct, MyDKDPTerimaAcct DOUBLE DEFAULT 0;	
	DECLARE MyDKHarga, MyDKHPP, MyBBN, MyReturHarga, MyInsentifSales, MyJaket, MyScheme, MyProgramSubsidi, MyPotonganHarga DOUBLE DEFAULT 0;
	DECLARE MyDKDPLLeasing, MyDKDPInden, MyDKDPTerima, MyDKDPTotal, MyPrgSubsSupplier, MyPrgSubsDealer, MyPrgSubsFincoy DOUBLE DEFAULT 0;
	DECLARE MyKKNo VARCHAR(18) DEFAULT '--';
   DECLARE MyKKTgl, MyKKJam DATE DEFAULT '1900-01-01';
         
	DECLARE oSKNo, oRKNo VARCHAR(10) DEFAULT '--';
	DELETE FROM tvharian WHERE SKNo = MyGLLink;
	SELECT RKNo INTO oRKNo FROM ttrk WHERE SKNo = MyGLLink;
	IF oRKNo <> '--' THEN
		SELECT ttsk.SKNo, ttsk.SKTgl, ttsk.SKJam, ttsk.LokasiKode, tmotor.MotorAutoN, tmotor.MotorNoMesin, 
      tmotor.MotorNoRangka, tmotor.MotorType, ttdk.DKNo, ttdk.DKJenis, ttdk.LeaseKode, ttdk.CusKode, tdcustomer.CusNama,  
      ttdk.DKDPLLeasing, ttdk.DKDPInden, ttdk.DKDPTerima, ttdk.DKDPTotal, PrgSubsSupplier, PrgSubsDealer, PrgSubsFincoy  
      INTO
      @SKNo, @SKTgl, @SKJam, @LokasiKode, @MotorAutoN, @MotorNoMesin, 
      @MotorNoRangka, @MotorType, @DKNo, @DKJenis, @LeaseKode, @CusKode, @CusNama,  
      MyDKDPLLeasing, MyDKDPInden, MyDKDPTerima, MyDKDPTotal, MyPrgSubsSupplier, MyPrgSubsDealer, MyPrgSubsFincoy
      FROM ttsk 
      INNER JOIN ttrk ON ttsk.SKNo = ttrk.SKNo  
      INNER JOIN ttdk ON ttrk.DKNo = ttdk.DKNo 
      INNER JOIN tmotor ON ttrk.RKNo = tmotor.RKNo 
      INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
      WHERE ttsk.SKNo = MyGLLink;
	ELSE
		SELECT ttsk.SKNo, ttsk.SKTgl, ttsk.SKJam, ttsk.LokasiKode, tmotor.MotorAutoN, tmotor.MotorNoMesin, 
      tmotor.MotorNoRangka, tmotor.MotorType, ttdk.DKNo, ttdk.DKJenis, ttdk.LeaseKode, ttdk.CusKode, tdcustomer.CusNama,  
      ttdk.DKDPLLeasing, ttdk.DKDPInden, ttdk.DKDPTerima, ttdk.DKDPTotal, PrgSubsSupplier, PrgSubsDealer, PrgSubsFincoy  
      INTO
      @SKNo, @SKTgl, @SKJam, @LokasiKode, @MotorAutoN, @MotorNoMesin,
      @MotorNoRangka, @MotorType, @DKNo, @DKJenis, @LeaseKode, @CusKode, @CusNama,  
      MyDKDPLLeasing, MyDKDPInden, MyDKDPTerima, MyDKDPTotal, MyPrgSubsSupplier, MyPrgSubsDealer, MyPrgSubsFincoy
      FROM ttsk 
      INNER JOIN tmotor ON ttsk.SKNo = tmotor.SKNo 
      INNER JOIN ttdk ON tmotor.DKNo = ttdk.DKNo 
      INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
      WHERE ttsk.SKNo = MyGLLink;
	END IF;
	
	/* DebetGL (4), KreditGL (5) */
	
	SELECT SUM(KreditGL - DebetGL) INTO MyDKHarga FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND NoAccount = '30010000';
	SELECT SUM(DebetGL - KreditGL) INTO MyDKHPP FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND NoAccount = '50000000';
	SELECT SUM(DebetGL - KreditGL) INTO MyBBN FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND (NoAccount = '60050000' OR NoAccount = '60380000');

	SELECT SUM(DebetGL - KreditGL) INTO MyReturHarga FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND NoAccount = '60060000';
	SELECT SUM(DebetGL - KreditGL) INTO MyInsentifSales FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND NoAccount = '60020000';
	SELECT SUM(DebetGL - KreditGL) INTO MyJaket FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND NoAccount = '60070000';
	
	SELECT SUM(DebetGL - KreditGL) INTO MyPrgSubsSupplier FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND NoAccount = '11110200';
	SELECT SUM(DebetGL - KreditGL) INTO MyPrgSubsDealer FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND NoAccount = '30030000';
	SELECT SUM(DebetGL - KreditGL) INTO MyPrgSubsFincoy FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND NoAccount LIKE '1109%';
	SELECT SUM(KreditGL - DebetGL) INTO MyScheme FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND (NoAccount = '40990000' OR NoAccount LIKE '4001%' OR NoAccount LIKE '4002%');

	SET MyProgramSubsidi = MyPrgSubsSupplier + MyPrgSubsDealer + MyPrgSubsFincoy;
	
	SELECT KKNo, KKTgl, KKJam INTO MyKKNo, MyKKTgl, MyKKJam FROM ttkk WHERE KKLink = @DKNo  AND KKJenis = 'SubsidiDealer2';
	SELECT SUM(DebetGL - KreditGL) INTO MyPotonganHarga FROM ttgeneralledgerhd 
	INNER JOIN ttgeneralledgerit ON (ttgeneralledgerhd.NoGL = ttgeneralledgerit.NoGL) AND (ttgeneralledgerhd.TglGL = ttgeneralledgerit.TglGL) 
	WHERE (NoAccount = '30040000') AND GLLink = MyKKNo AND KodeTrans = 'KK';
	SELECT SUM(DebetGL - KreditGL) INTO @PotonganHarga FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND NoAccount = '30040000';
	SET MyPotonganHarga = MyPotonganHarga + @PotonganHarga;

	SELECT SUM(DebetGL-KreditGL) INTO MyDKDPLLeasingAcct FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND (NoAccount = '11060100' OR NoAccount LIKE '24050100') AND KeteranganGL LIKE '%Piutang Kons UM%';
	SELECT SUM(DebetGL-KreditGL) INTO MyDKDPIndenAcct FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND (NoAccount = '11060100' OR NoAccount LIKE '24050100') AND KeteranganGL LIKE '%Inden%';
	SELECT SUM(DebetGL-KreditGL) INTO MyDKDPTerimaAcct FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND (NoAccount = '11060100' OR NoAccount LIKE '24050100') AND (KeteranganGL LIKE '%Uang Muka%' OR KeteranganGL LIKE '%UM Diterima%');

	IF MyDKDPLLeasing <> MyDKDPLLeasingAcct THEN
		IF MyDKDPLLeasingAcct <> 0 THEN SET MyDKDPLLeasing = MyDKDPLLeasingAcct; END IF;
	END IF;
	IF MyDKDPInden = MyDKDPIndenAcct THEN
		IF MyDKDPIndenAcct <> 0 THEN SET MyDKDPInden = MyDKDPIndenAcct; END IF;
	END IF;
	IF MyDKDPTerima = MyDKDPTerimaAcct THEN
		IF MyDKDPTerimaAcct <> 0 THEN SET MyDKDPTerima = MyDKDPTerimaAcct; END IF;
	END IF;
	SET MyDKDPTotal = MyDKDPLLeasing + MyDKDPInden + MyDKDPTerima;
	
	
	IF @DKJenis = 'Kredit' THEN
		SELECT SUM(DebetGL-KreditGL) INTO MyDKNetto FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND (NoAccount LIKE '1108%') ;
		IF MyDKNetto > 0 THEN SET MyDKNetto = MyDKNetto; END IF; /*Ada dua Piut Lease yaitu Real DKNetto dan Subs Fincoy, keduanya masuk ke coa 11303*/
   ELSE
		SELECT SUM(DebetGL-KreditGL) INTO MyDKNetto FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND (NoAccount = '11060100' OR NoAccount = '24050100') ;
      IF MyDKNetto > 0 THEN SET MyDKNetto = MyDKNetto - MyDKDPLLeasing - MyDKDPInden - MyDKDPTerima; END IF;
   END IF;

	SELECT SUM(DebetGL-KreditGL) INTO MyDKDPLLeasing1130100 FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND (NoAccount = '11060100' AND KeteranganGL LIKE '%Piutang Kons UM%') ;
	SELECT SUM(DebetGL-KreditGL) INTO MyDKDPInden1130100 FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND (NoAccount = '11060100' AND KeteranganGL LIKE '%Inden%') ;
	SELECT SUM(DebetGL-KreditGL) INTO MyDKDPTerima1130100 FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND (NoAccount = '11060100' AND (KeteranganGL LIKE '%Uang Muka%' OR KeteranganGL LIKE '%UM Diterima%')) ;

	SELECT SUM(DebetGL-KreditGL) INTO MyDKDPLLeasing2140500 FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND (NoAccount = '24050100' AND KeteranganGL LIKE '%Piutang Kons UM%') ;
	SELECT SUM(DebetGL-KreditGL) INTO MyDKDPInden2140500 FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND (NoAccount = '24050100' AND KeteranganGL LIKE '%Inden%') ;
	SELECT SUM(DebetGL-KreditGL) INTO MyDKDPTerima2140500 FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND (NoAccount = '24050100' AND (KeteranganGL LIKE '%Uang Muka%' OR KeteranganGL LIKE '%UM Diterima%')) ;

   SET MyDKDP2140500HTK = MyDKDPLLeasing2140500 + MyDKDPInden2140500 + MyDKDPTerima2140500; /* Hutang Titipan Konsumen  'Jadi DP Net */
   SET MyDKDP1130100PK = MyDKDPLLeasing1130100 + MyDKDPInden1130100 + MyDKDPTerima1130100; /* Piutang Konsumen          'Jadi Piutang Konsumen */

	IF @DKJenis = 'Kredit' THEN
		SET MyDKNettoPK = 0;
		SET MyDKNettoHTK = 0;
	ELSE
		SELECT SUM(DebetGL-KreditGL) INTO MyDKNettoPK FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND (NoAccount = '11060100') ;
		SELECT SUM(DebetGL-KreditGL) INTO MyDKNettoHTK FROM ttgeneralledgerit WHERE NoGL = MyNoGL AND (NoAccount = '24050100') ;
		IF MyDKNettoPK > 0 THEN SET MyDKNettoPK = MyDKNettoPK - MyDKDP1130100PK; END IF;
		IF MyDKNettoHTK > 0 THEN SET MyDKNettoHTK = MyDKNettoHTK - MyDKDP2140500HTK; END IF;
	END IF;

	INSERT INTO tvharian
	(SKNo, SKTgl, SKJam, LokasiKode, NoGL, TglGL, DKNo, DKJenis, LeaseKode, 
	DKHarga, DKHPP, BBN, ReturHarga, InsentifSales, Jaket, PotonganHarga, PrgSubsDealer, PrgSubsSupplier, PrgSubsFincoy, ProgramSubsidi,
	DKDPLLeasing, DKDPInden, DKDPTerima, DKDPTotal, DKNetto,
	DKDP2140500HTK, 
	DKDPLLeasing2140500, DKDPInden2140500, DKDPTerima2140500,
	DKDP1130100PK, 
	DKDPLLeasing1130100, DKDPInden1130100, DKDPTerima1130100, 
	DKDPPKTerbayarTotal, 
	DKDPPKTerbayarLeasing, DKDPPKTerbayarInden, DKDPPKTerbayarTerima,
	DKNettoHTK, DKNettoPK, DKNettoKMPKons, DKNettoKMPLease, 
	CusKode, CusNama, KKNo, KKTgl, KKJam, MotorAutoN, MotorNoMesin, MotorNoRangka, MotorType, Scheme) VALUES (
	@SKNo, @SKTgl, @SKJam, @LokasiKode, MyNoGL, MyTglGL, @DKNo, @DKJenis, @LeaseKode,
	MyDKHarga, MyDKHPP, MyBBN, MyReturHarga, MyInsentifSales, MyJaket, MyPotonganHarga, MyPrgSubsDealer, MyPrgSubsSupplier, MyPrgSubsFincoy, MyProgramSubsidi, 
	MyDKDPLLeasing, MyDKDPInden, MyDKDPTerima, MyDKDPTotal, MyDKNetto,
	MyDKDP2140500HTK, 
	MyDKDPLLeasing2140500, MyDKDPInden2140500, MyDKDPTerima2140500, 
	MyDKDP1130100PK,
	MyDKDPLLeasing1130100, MyDKDPInden1130100, MyDKDPTerima1130100, 
	MyDKDPPKTerbayarTotal, 
	MyDKDPPKTerbayarLeasing, MyDKDPPKTerbayarInden, MyDKDPPKTerbayarTerima,
	MyDKNettoHTK, MyDKNettoPK, MyDKNettoKMPKons, MyDKNettoKMPLease,
	@CusKode, @CusNama, MyKKNo, MyKKTgl, MyKKJam, @MotorAutoN, @MotorNoMesin, @MotorNoRangka, @MotorType, MyScheme);
	
END$$

DELIMITER ;

