DELIMITER $$

DROP PROCEDURE IF EXISTS `ffb`$$

CREATE DEFINER=`root`@`%` PROCEDURE `ffb`(
    IN xAction VARCHAR(10),
    OUT oStatus INT,
    OUT oKeterangan TEXT,
    IN xUserID VARCHAR(15),
    IN xKodeAkses VARCHAR(15),
    IN xLokasiKode VARCHAR(15),
    INOUT xFBNo VARCHAR(18),
    IN xFBNoBaru VARCHAR(18),
    IN xFBTgl DATE,
    IN xFBJam DATETIME,
    IN xSupKode VARCHAR(10),
    IN xSSNo VARCHAR(18),
    IN xSSTgl DATE,
    IN xFBTermin DECIMAL(2,0),
    IN xFBTglTempo DATE,
    IN xDONo VARCHAR(18),
    IN xDOTgl DATE,
    IN xFPNo VARCHAR(30),
    IN xFPTgl DATE,
    IN xFBPtgMD DECIMAL(14,2),
    IN xFBSubTotal DECIMAL(14,2),
    IN xFBLunas VARCHAR(5),
    IN xFBPtgLain DECIMAL(14,2),
    IN xFBPtgTotal DECIMAL(14,2),
    IN xFBPosting VARCHAR(15),
    IN xFBExtraDisc DECIMAL(12,2),
    IN xFBTotal DECIMAL(14,2),
    IN xFBMemo VARCHAR(100),
    IN xNoGL VARCHAR(10),
    IN xCetak VARCHAR(5),
    IN xFBPSS DECIMAL(14,2))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			#ReJurnal Transaksi Tidak Memiliki Jurnal
			DECLARE rFBNo VARCHAR(100) DEFAULT '';
			cek01: BEGIN
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT FBNo FROM ttfb
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttfb.FBNo 
				WHERE ttgeneralledgerhd.NoGL IS NULL AND FBTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rFBNo;
					IF rFBNo IS NOT NULL AND rFBNo <> '' THEN
						CALL jfb(rFBNo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;

			cek02: BEGIN
				DECLARE done2 INT DEFAULT FALSE;
				DECLARE cur2 CURSOR FOR 
				SELECT FBNo FROM ttfb
				INNER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttfb.FBNo 
				WHERE (((TotalDebetGL = 0 OR TotalKreditGL = 0) AND FBTotal <> 0) OR ((TotalDebetGL <> 0 OR TotalKreditGL <> 0) AND FBTotal = 0))
				AND FBTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done2 = 1;		
				OPEN cur2;
				REPEAT FETCH cur2 INTO rFBNo;
					IF rFBNo IS NOT NULL AND rFBNo <> '' THEN
						CALL jfb(rFBNo, 'System');
					END IF;
				UNTIL done2 END REPEAT;
				CLOSE cur2;
			END cek02;
						
			CALL ctvmotorLokasi('oo',0);
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Daftar Faktur Beli [FB] ", rFBNo);
		END;
		WHEN "Load" THEN
		BEGIN		
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Load data Faktur Beli [FB] No : ",xFBNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SET @AutoNo = CONCAT('FB',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(FBNo,5)+1),5,0)) INTO @AutoNo FROM ttfb WHERE ((FBNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(FBNo) = 10) ORDER BY FBNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('FB',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xFBNo = @AutoNo;
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Tambah data baru Faktur Beli [FB] No : ", @AutoNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @FBNoOLD = xFBNo;
			
			#SET oKeterangan= CONCAT_WS(' - ',xFBNo, xFBNoBaru, xFBTgl, xSSNo, xSupKode, xFBTermin, xFBTglTempo, xFBPSS, xFBPtgLain, xFBTotal, xFBMemo, 
			#xFBLunas, xUserID, xNoGL, xFPNo, xFPTgl, xDOTgl, xFBExtraDisc, xFBPosting, xFBPtgMD, xFBJam);
			#xFPHargaUnit, xFPDiscount,  xFPDPP
	
			#Auto Number [ Insert vs Edit ]
			IF LOCATE('=',xFBNo) <> 0 THEN
				SET @AutoNo = CONCAT('FB',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(FBNo,5)+1),5,0)) INTO @AutoNo FROM ttfb WHERE ((FBNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(FBNo) = 10) ORDER BY FBNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('FB',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xFBNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xFBNoBaru = xFBNo;				
			END IF;
								
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xFBNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xFBNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Cek Jam
			IF oStatus = 0 THEN				
				SELECT FBJam INTO @FBJamOLD FROM ttfb WHERE FBNo = @FBNoOLD;
				IF xFBJam < @FBJamOLD THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT(oKeterangan,"Gagal menyimpan data, Tgl-Jam ", DATE_FORMAT(xFBJam,'%d-%m-%Y %k:%i:%s'), ' tidak boleh lebih kecil dari Tgl-Jam semula ',DATE_FORMAT(@FBJamOLD ,'%d-%m-%Y %k:%i:%s'));
				ELSE
					SET oStatus = 0;
				END IF;		
			END IF;
			
			#Update Proses 
			IF oStatus = 0 THEN				
				SET xFBPosting = 'Selesai Posting';					
				UPDATE ttfb
				SET FBNo = xFBNoBaru, FBTgl = xFBTgl, FBJam = xFBJam, SSNo = xSSNo, SupKode = xSupKode, FBTermin = xFBTermin, FBTglTempo = xFBTglTempo, 
					FBPSS = xFBPSS, FBPtgLain = xFBPtgLain,FBTotal = xFBTotal, FBExtraDisc = xFBExtraDisc, FBPtgMD = xFBPtgMD, 
					FBMemo = xFBMemo, UserID = xUserID, NoGL = xNoGL, FPNo = xFPNo, FPTgl = xFPTgl, DOTgl = xDOTgl,
					FBPosting = xFBPosting, FBLunas = xFBLunas
				WHERE FBNo = @FBNoOLD;		
				SET xFBNo = xFBNoBaru;

				#Update Motor
				UPDATE tmotor SET FBNo = xFBNoBaru WHERE FBNo = @FBNoOLD;		
				UPDATE tmotor SET FBHarga = FBHarga WHERE FBHarga = 0 AND FBNo = '--' AND FBNo = xFBNo;		
				UPDATE tmotor SET SKHarga = FBHarga WHERE SKHarga = 0 AND SKNo = '--' AND FBNo = xFBNo;		
				UPDATE tmotor SET SDHarga = FBHarga WHERE SDHarga = 0 AND SDNo = '--' AND FBNo = xFBNo;
				UPDATE tmotor SET RKHarga = FBHarga WHERE RKHarga = 0 AND RKNo = '--' AND FBNo = xFBNo;			
				UPDATE tmotor INNER JOIN ttss ON ttss.SSNo = tmotor.SSNo SET SSHarga = FBHarga WHERE ttss.SSNo <> '--' AND ttss.SSJam > xFBJam AND tmotor.FBNo = xFBNo;
			
				#Update nomor SS di FB
				SELECT IFNULL(MAX(SSNo),'--') INTO xSSNo FROM tmotor WHERE FBNo = xFBNo;
				IF xSSNo IS NULL OR xSSNo = '' THEN SET xSSNo = '--'; END IF;			
				UPDATE ttfb SET SSNo = xSSNo WHERE FBNo = xFBNo;
				
				#Hitung SubTotal & Bawah
				SET @FBSubtotal = 0;
				SELECT IFNULL(SUM(FBHarga),0) INTO @FBSubtotal FROM tmotor WHERE FBNo = xFBNo;
				SET xFBTotal = @FBSubtotal - xFBPSS - xFBPtgLain - xFBExtraDisc - xFBPtgMD;
				UPDATE ttfb	SET FBTotal = xFBTotal	WHERE FBNo = xFBNo;		
				#	xFBPSS = Potongan Program
				#	xFBPtgLain = Pendapatan Unit
				#	xFBExtraDisc = Extra Discount
				#	xFBPtgMD = Titipan Potongan MD				
				
				#Cek FBLunas memperhitungkan MM
				
				#Update ttss
				UPDATE ttSS SET SupKode = xSupKode WHERE FBNo = xFBNo;
				SET @SSNo = 'XX';
				SELECT SSNo INTO @SSNo FROM tmotor WHERE FBNo = xFBNo AND SSNo <> '--' ORDER BY SSNo DESC LIMIT 1 ;
				SET @FBNo = '--';
				SELECT MAX(FBNo) INTO @FBNo FROM tmotor WHERE SSNo = @SSNo ;
				UPDATE ttss SET FBNo	= @FBNo WHERE SSNo = @SSNo;
							
				#Keterangan perbedaan Harga
				SELECT GROUP_CONCAT((CONCAT('Motor dengan No Mesin ',MotorAutoN, ' - ', MotorNoMesin, '  Harga SK:', SKHarga, ' <> Harga FB:',FBHarga) ) SEPARATOR '<br>')
				INTO @Keterangan	FROM tmotor
				WHERE SKHarga <> FBharga AND SKNo <> '--' AND FBNo = xFBNo;				
				SELECT GROUP_CONCAT((CONCAT('Motor dengan No Mesin ',MotorAutoN, ' - ', MotorNoMesin, '  Harga SD:', SDHarga, ' <> Harga FB:',FBHarga) ) SEPARATOR '<br>')
				INTO @Keterangan	FROM tmotor
				WHERE SDHarga <> FBharga AND SDNo <> '--' AND FBNo = xFBNo;				
				SELECT GROUP_CONCAT((CONCAT('Motor dengan No Mesin ',MotorAutoN, ' - ', MotorNoMesin, '  Harga RK:', RKHarga, ' <> Harga FB:',FBHarga) ) SEPARATOR '<br>')
				INTO @Keterangan	FROM tmotor
				WHERE RKHarga <> FBharga AND RKNo <> '--' AND FBNo = xFBNo;				
				SELECT GROUP_CONCAT((CONCAT('Motor dengan No Mesin ',MotorAutoN, ' - ', MotorNoMesin, '  Harga SS:', SSHarga, ' <> Harga FB:',FBHarga) ) SEPARATOR '<br>')
				INTO @Keterangan	FROM tmotor
				WHERE SSHarga <> FBharga AND SSNo <> '--' AND FBNo = xFBNo;							
				SET oKeterangan = CONCAT(oKeterangan,IFNULL(@Keterangan,''));
						
				#Memberi nilai AutoN [cek nomor motor]
				CALL cMotorAutoN('FB',xFBNo,'');
							
				#Update Harga Beli
				UPDATE tdmotortype JOIN (SELECT MotorType, MAX(FBHarga) AS FBHarga FROM tmotor WHERE FBNo = xFBNo GROUP BY MotorType) FB 
				ON tdmotortype.MotorType = FB.MotorType 
				SET tdmotortype.MotorHrgBeli = FB.FBHarga, tdmotortype.TglUpdate = xFBTgl 
				WHERE tdmotortype.TglUpdate <= xFBTgl;
				
				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @FBNoOLD; #Hapus Jurnal No Transaksi Lama
				CALL jfb(xFBNo,xUserID); #Posting Jurnal
			
				IF @AutoNo = 'Edit' THEN #Edit
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Faktur Beli','ttfb', xFBNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Faktur Beli [FB] No: ", xFBNo);
				ELSE #Add
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Faktur Beli','ttfb', xFBNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Faktur Beli [FB] No: ", xFBNo);
				END IF;
			ELSEIF oStatus = 1 THEN
				SET oKeterangan = oKeterangan;
			END IF;				
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;
			
			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xFBNo;
			IF @GLValid = 'Sudah' THEN	
			  SET oStatus = 1; SET oKeterangan = CONCAT("No FB:", xFBNo, " sudah memiliki No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Cek SS
			IF oStatus = 0 THEN
				SELECT COUNT(FBNo), GROUP_CONCAT(MotorNoMesin) INTO @JumMotor, @ListMotorNoMesin FROM tmotor WHERE FBNo = xFBNo AND SSNo <> '--'; 
				IF @JumMotor > 0 THEN
				  SET oStatus = 1; SET oKeterangan = CONCAT("Gagal menghapus, No FB : ", xFBNo,' - No Mesin : ',@ListMotorNoMesin," telah memiliki Penerimaan Barang");	
				END IF;
			END IF;
			
			#Delete Proses
			IF oStatus = 0 THEN
				#DeleteAdjustHPP
				UPDATE tmotor 
				INNER JOIN ttamit ON ttamit.MotorAutoN = tmotor.MotorAutoN AND ttamit.MotorNoMesin = tmotor.MotorNoMesin
				INNER JOIN ttamhd ON ttamit.AMNo = ttamhd.AMNo 
				SET MMHarga = 0 
				WHERE tmotor.FBNo = xFBNo;
      
				#Motor
				UPDATE tmotor SET FBNo = '--' WHERE FBNo = xFBNo; 
				DELETE FROM tmotor WHERE FBNo = xFBNo;

				#Update nomor FB di SS
				SET @SSNo = 'XX';
				SELECT SSNo INTO @SSNo FROM tmotor WHERE FBNo = xFBNo AND SSNo <> '--' ORDER BY SSNo DESC LIMIT 1 ;
				SET @FBNo = '--';
				SELECT MAX(FBNo) INTO @FBNo FROM tmotor WHERE SSNo = @SSNo ;
				UPDATE ttss SET FBNo	= @FBNo WHERE SSNo = @SSNo;
								
				DELETE FROM ttfb WHERE FBNo = xFBNo; /*MainCourse*/
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xFBNo; /*Delete Jurnal*/
				
				CALL cMotorAutoN('FB',xFBNo,'');
				
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Faktur Beli','ttfb', xFBNo); 
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Faktur Beli [FB] No: ",xFBNo);
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 IF LOCATE('=',xFBNo) <> 0 THEN #Tambah Baru -> Batal dan Hapus
				  DELETE FROM ttfb WHERE FBNo = xFBNo;
				  SET oKeterangan = CONCAT("Batal menyimpan data Faktur Beli [FB] No: ", xFBNoBaru);
			 ELSE #Edit -> Batal dan Hapus
				  SET oKeterangan = CONCAT("Batal menyimpan data Faktur Beli [FB] No: ", xFBNo);
			 END IF;
		END;
		WHEN "Jurnal" THEN
		BEGIN
			 IF LOCATE('=',xFBNo) <> 0 THEN /* Data Baru, belum tersimpan tidak boleh di jurnal */
				  SET oStatus = 1;
				  SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			 ELSE
				  CALL jfb(xFBNo,xUserID);
				  SET oStatus = 0;
				  SET oKeterangan = CONCAT("Berhasil memproses Jurnal Faktur Beli [FB] No: ", xFBNo);
			 END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
		  SET oStatus = 0;
		  SET oKeterangan = CONCAT("Update Penerimaan ", xSSNo);
		END;
		WHEN "Print" THEN
		BEGIN
			 IF xCetak = "1" THEN
				SELECT ttfb.FBNo, ttfb.FBTgl, ttfb.SSNo, ttfb.SupKode, ttfb.FBTermin, ttfb.FBTglTempo, ttfb.FBPSS, ttfb.FBPtgLain, 
				ttfb.FBTotal, ttfb.FBMemo, ttfb.FBLunas, ttfb.UserID, IFNULL(tdsupplier.SupNama, '--') AS SupNama, IFNULL(ttss.SSTgl, 
            STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SSTgl, IFNULL(tmotor.FBJum, 0) AS FBJum, IFNULL(tmotor.FBSubTotal, 0) AS FBSubTotal, 
            ttfb.FBPSS + ttfb.FBPtgLain + ttfb.FBExtraDisc AS FBPtgTotal, ttfb.NoGL, ttfb.FPNo, 
            ttfb.FPTgl, ttfb.DOTgl, ttfb.FBExtraDisc, ttfb.FBPosting, ttfb.FBPtgMD, ttfb.FBJam
				FROM ttfb 
				LEFT OUTER JOIN ttss ON ttfb.SSNo = ttss.SSNo 
				LEFT OUTER JOIN tdsupplier ON ttfb.SupKode = tdsupplier.SupKode 
				LEFT OUTER JOIN (SELECT COUNT(MotorType) AS FBJum, SUM(FBHarga) AS FBSubTotal, FBNo FROM tmotor tmotor_1 GROUP BY FBNo) tmotor ON ttfb.FBNo = tmotor.FBNo
				WHERE (ttfb.FBNo = xFBNo);
			 END IF;
			 IF xCetak = "2" THEN
				SELECT BPKBNo, BPKBTglAmbil, BPKBTglJadi, DKNo, DealerKode, FBHarga, FBNo, FakturAHMNilai, FakturAHMNo, FakturAHMTgl, FakturAHMTglAmbil, 
				MMHarga, MotorAutoN, MotorMemo, MotorNoMesin, MotorNoRangka, MotorTahun, MotorType, MotorWarna, PDNo, PlatNo, PlatTgl, PlatTglAmbil, 
				RKHarga, RKNo, SDHarga, SDNo, SKHarga, SKNo, SSNo, STNKAlamat, STNKNama, STNKNo, STNKTglAmbil, STNKTglBayar, STNKTglJadi, STNKTglMasuk,SSHarga
				FROM tmotor
				WHERE (FBNo = xFBNo)
				ORDER BY SSNo DESC, MotorType, MotorTahun, MotorWarna;
			 END IF;

			INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
			(NOW(), xUserID , 'Cetak' , 'Faktur Beli','ttfb', xFBNo); 
		END;
	END CASE;
END$$

DELIMITER ;

