DELIMITER $$

DROP PROCEDURE IF EXISTS `fkp`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fkp`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xKKNo VARCHAR(18),
IN xKKNoBaru VARCHAR(18),
IN xKKTgl DATE,
IN xKKMemo VARCHAR(100),
IN xKKNominal DECIMAL(14,2),
IN xKKJenis VARCHAR(20),
IN xKKPerson VARCHAR(50),
IN xKKLink VARCHAR(12),
IN xKKJam DATETIME,
IN xNoGL VARCHAR(10))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Daftar Kas Keluar Pengajuan");
		END;
		WHEN "Load" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Load data Kas Keluar Pengajuan No: ", xKKNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SET @AutoNo = CONCAT('KP',RIGHT(YEAR(NOW()),2),LPAD(MONTH(NOW()),2,'0'),LPAD(DAY(NOW()),2,'0'),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(KKNo,3)+1),3,0)) INTO @AutoNo FROM ttkk WHERE ((KKNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(KKNo) = 12) ORDER BY KKNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 12 THEN SET @AutoNo = CONCAT('KP',RIGHT(YEAR(NOW()),2),LPAD(MONTH(NOW()),2,'0'),LPAD(DAY(NOW()),2,'0'),'-001');  END IF;
			SET xKKNo = @AutoNo;
			SET oStatus = 0;SET oKeterangan = CONCAT("Tambah data baru Kas Keluar Pengajuan No: ",xKKNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = '';
			SET @AutoNo = 'Edit';
			SET @KKNoOLD = xKKNo;
			
			IF LOCATE('=',xKKNo) <> 0  THEN 
				SET @AutoNo = CONCAT('KP',RIGHT(YEAR(NOW()),2),LPAD(MONTH(NOW()),2,'0'),LPAD(DAY(NOW()),2,'0'),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(KKNo,3)+1),3,0)) INTO @AutoNo FROM ttkk WHERE ((KKNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(KKNo) = 12) ORDER BY KKNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 12 THEN SET @AutoNo = CONCAT('KP',RIGHT(YEAR(NOW()),2),LPAD(MONTH(NOW()),2,'0'),LPAD(DAY(NOW()),2,'0'),'-001');  END IF;
				SET xKKNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit'; 
				SET xKKNoBaru = xKKNo;
			END IF;

			IF oStatus = 0 THEN 
			  UPDATE ttkk
			  SET KKNo = xKKNoBaru, KKTgl = xKKTgl, KKJenis = xKKJenis, KKPerson = xKKPerson, KKLink = xKKLink, KKNominal = xKKNominal, 
			  KKMemo = xKKMemo, NoGL = xNoGL, KKJam = xKKJam, UserID = xUserID, LokasiKode = xLokasiKode
			  WHERE KKNo = xKKNo;
			  SET xKKNo = xKKNoBaru;
			  
 				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Kas Keluar Pengajuan','ttkk', xKKNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Kas Keluar Pengajuan No:  ", xKKNo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Kas Keluar Pengajuan','ttkk', xKKNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Kas Keluar Pengajuan No: ", xKKNo);
				END IF;
				
			ELSEIF oStatus = 1 THEN
			  SET oKeterangan = CONCAT("Gagal, silahkan melengkapi data Kas Keluar Pengajuan No: ", xKKNo);
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			DELETE FROM ttkk WHERE KKNo = xKKNo;
			DELETE FROM ttkkit WHERE KKNo = xKKNo;
			SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Kas Keluar Pengajuan No: ", xKKNo);
			INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
			(NOW(), xUserID , 'Hapus' , 'Kas Keluar Pengajuan','ttkk', xKKNo); 
		END;
		WHEN "Cancel" THEN
			BEGIN
				 IF LOCATE('=',xKKNo) <> 0 THEN
					  DELETE FROM ttkk WHERE KKNo = xKKNo;
					  SET oKeterangan = CONCAT("Batal menyimpan data Kas Keluar Pengajuan No: ", xKKNoBaru);
				 ELSE
					  SET oKeterangan = CONCAT("Batal menyimpan data Kas Keluar Pengajuan No: ", xKKNo);
				 END IF;
			END;
		WHEN "Print" THEN
		BEGIN
			IF xCetak = "1" THEN
				SELECT KKNo, KKTgl, KKMemo, KKNominal, KKJenis, KKPerson, KKLink, UserID, ttkk.LokasiKode, KKJam, NoGL 
				FROM ttkk INNER JOIN tdlokasi ON ttkk.LokasiKode = tdlokasi.LokasiKode
				WHERE (KKNo = xKKNo);
			END IF;
			IF xCetak = "2" THEN
				IF xKKJenis = 'Bayar BBN' THEN
					SELECT KKNo, ttkkit.FBNo, KKBayar, DKTgl, LeaseKode, CusNama, MotorType, BBN, MotorTahun, MotorNoMesin , CusKecamatan, CusKabupaten, 
					KKPaid.Paid - KKBayar AS Terbayar, KKBayar, BBN - KKPaid.Paid AS Sisa FROM (
						SELECT ttkkit.KKNo, ttkkit.FBNo, ttkkit.KKBayar, ttdk.DKTgl, ttdk.LeaseKode, tdcustomer.CusNama, tmotor.MotorType, 
						tmotor.MotorTahun, tmotor.MotorNoMesin , tdcustomer.CusKecamatan, tdcustomer.CusKabupaten,
						(ttdk.BBN+ttdk.JABBN) AS BBN
						FROM ttkkit 
						INNER JOIN ttdk ON ttkkit.FBNo = ttdk.DKNo 
						INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
						INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo WHERE (ttkkit.KKNo = xKKNo ) ORDER BY ttkkit.FBNo) ttkkit
					INNER JOIN (SELECT KKLINK AS FBNo, SUM(KKNominal) AS Paid FROM vtkk WHERE (KKJenis = 'Bayar BBN') GROUP BY KKLINK) KKPaid 
					ON ttkkit.FBNo = KKPaid.FBNo
					WHERE (KKNo = xKKNo);					                         
				END IF;
			END IF;
			SET oStatus = 0; SET oKeterangan = CONCAT("Cetak data Kas Keluar Pengajuan No: ", xKKNo);
			INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE  
			(NOW(), xUserID , 'Cetak' , CONCAT('Kas Keluar Pengajuan ',xKKJenis),'ttkk', xKKNo); 
		END;
	END CASE;
END$$

DELIMITER ;

