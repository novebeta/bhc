DELIMITER $$

DROP PROCEDURE IF EXISTS `jsr`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jsr`(IN xSDNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE MyAutoN INTEGER DEFAULT 1;
	DECLARE TotSDHarga DOUBLE DEFAULT 0;

	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xSDNo ORDER BY NOGL DESC LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;

	UPDATE tmotor SET SDHarga = FBHarga WHERE SDHarga = 0 AND SDNo = xSDNo;
	SELECT IFNULL(SUM(SDHarga),0) INTO TotSDHarga FROM tmotor WHERE SDNo = xSDNo;
	UPDATE ttsd SET SDTotal = TotSDHarga WHERE SDNo = xSDNo;

	SELECT SDNo, SDTgl, LokasiKode, DealerKode, SDMemo, SDTotal, UserID, SDJam, NoGL INTO
	@SDNo, @SDTgl, @LokasiKode, @DealerKode, @SDMemo, @SDTotal, @UserID, @SDJam, @NoGL
	FROM ttsd WHERE SDNo = xSDNo;

	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
    SET xNoGL =  @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;

	DELETE FROM ttgeneralledgerhd WHERE GLLink = xSDNo;
	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL;  
	DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;  
	DELETE FROM ttglhpit WHERE NoGL = xNoGL;

	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, @SDTgl, 'SR', CONCAT('Post Retur Beli ke ',@DealerKode), TotSDHarga, TotSDHarga, xSDNo, xUserID, '--', xGLValid);
	INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
	(xNoGL, @SDTgl, 1, '24010101', CONCAT('Post Retur Beli ke ',@DealerKode), @SDTotal, 0);
	INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
	(xNoGL, @SDTgl, 2, '11150100', CONCAT('Post Retur Beli ke ',@DealerKode), 0, @SDTotal);
	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	CALL jHutangKreditPiutangDebet(xNoGL);

	UPDATE ttSD SET NoGL = xNoGL  WHERE SDNo = xSDNo;

	DROP TEMPORARY TABLE IF EXISTS SDFBit;
	CREATE TEMPORARY TABLE IF NOT EXISTS SDFBit AS
	SELECT FBNo, SUM(FBHarga) AS Harga FROM tmotor WHERE SDNo = xSDNo GROUP BY FBNo;
	INSERT INTO ttglhpit SELECT xNoGL, @SDTgl, FBNo, '24010101', 'Hutang', Harga FROM SDFBit;
	UPDATE ttgeneralledgerhd SET HPLink = 'Many'  WHERE NoGL = xNoGL;

END$$

DELIMITER ;

