DELIMITER $$

DROP PROCEDURE IF EXISTS `famit`$$

CREATE DEFINER=`root`@`%` PROCEDURE `famit`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan VARCHAR(150),
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xAMNo VARCHAR(10),
IN xMotorNoMesin VARCHAR(25),
IN xMotorAutoN SMALLINT(6),
IN xMotorHarga DECIMAL(11,2),
IN xAMNoOLD VARCHAR(10),
IN xMotorAutoNOLD SMALLINT(6),
IN xMotorNoMesinOLD VARCHAR(25))
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xAMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xAMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert
			IF oStatus = 0 THEN		
				INSERT INTO ttamit (AMNo, MotorNoMesin, MotorAutoN) VALUES (xAMNo, xMotorNoMesin, xMotorAutoN);
				UPDATE tmotor SET MMHarga = xMotorHarga	WHERE MotorAutoN = xMotorAutoN   AND MotorNoMesin = xMotorNoMesin;
				SET oKeterangan = CONCAT('Berhasil menambahkan data Penyesuaian Nilai Persediaan Motor  ', xAMNo,' - ', xMotorAutoN, ' - ', xMotorNoMesin);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xAMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xAMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Update
			IF oStatus = 0 THEN
				UPDATE tmotor SET MMHarga = 0	WHERE MotorAutoN = xMotorAutoNOLD  AND MotorNoMesin = xMotorNoMesinOLD;
				UPDATE ttamit SET AMNo = xAMNo, MotorNoMesin = xMotorNoMesin, MotorAutoN = xMotorAutoN
				WHERE AMNo = xAMNoOLD AND MotorNoMesin = xMotorNoMesinOLD AND MotorAutoN = xMotorAutoNOLD;
				UPDATE tmotor SET MMHarga = xMotorHarga	WHERE MotorAutoN = xMotorAutoN AND MotorNoMesin = xMotorNoMesin;
				SET oKeterangan = CONCAT('Berhasil mengubah data Penyesuaian Nilai Persediaan Motor : ', xAMNo,' - ', xMotorAutoN, ' - ', xMotorNoMesin);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xAMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xAMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Delete
			IF oStatus = 0 THEN		
				UPDATE tmotor SET MMHarga = 0	WHERE MotorAutoN = xMotorAutoN AND MotorNoMesin = xMotorNoMesin;
				DELETE FROM ttamit WHERE AMNo = xAMNo AND MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN;
				SET oKeterangan = CONCAT('Berhasil menghapus data Penyesuaian Nilai Persediaan Motor  ', xAMNo,' - ', xMotorAutoN, ' - ', xMotorNoMesin);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;		
		END;
	END CASE;
END$$

DELIMITER ;

