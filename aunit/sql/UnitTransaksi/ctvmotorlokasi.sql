DELIMITER $$

DROP PROCEDURE IF EXISTS `ctvmotorlokasi`$$

CREATE DEFINER=`root`@`%` PROCEDURE `ctvmotorlokasi`(IN xNoTrans VARCHAR(20), IN JumMotor INTEGER)
BEGIN
	DECLARE JumlahNull DOUBLE DEFAULT 0;
	DECLARE oNoTrans VARCHAR(100) DEFAULT '';

	SELECT NoTrans INTO oNoTrans FROM tvmotorlokasi WHERE NoTrans = xNoTrans LIMIT 1;
	IF oNoTrans <> '' OR xNoTrans = 'oo' THEN
		SELECT SUM(IFNULL(tvmotorlokasi.MotorNoMesin,1)) INTO JumlahNull FROM tmotor 
		LEFT OUTER JOIN tvmotorlokasi ON tvmotorLokasi.MotorNoMesin = tmotor.MotorNoMesin
		WHERE MotorTahun >= YEAR(NOW())-1 AND tvmotorlokasi.MotorNoMesin IS NULL ; #AND (tmotor.SSNo <> '--' OR tmotor.SDNo <> '--');
	ELSE
		SET JumlahNull = JumMotor;	
	END IF;
		
	IF JumlahNull > 0 THEN
		TRUNCATE tvmotorlokasi;
		INSERT INTO tvmotorlokasi
		SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttss.LokasiKode AS LokasiKode, ttss.SSNo AS NoTrans, ttss.SSJam AS Jam, 'IN' AS Kondisi
		FROM  tmotor INNER JOIN ttss ON tmotor.SSNo = ttss.SSNo;
		INSERT INTO tvmotorlokasi
		SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttpd.LokasiKode AS LokasiKode, ttpd.PDNo AS NoTrans, ttpd.PDJam AS Jam, 'IN' AS Kondisi
		FROM  tmotor INNER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo;
		INSERT INTO tvmotorlokasi
		SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttpbhd.LokasiTujuan AS LokasiKode, ttpbit.pbNo AS NoTrans, ttpbhd.pbJam AS Jam, 'IN' AS Kondisi
		FROM tmotor
		INNER JOIN ttpbit ON tmotor.MotorNoMesin = ttpbit.MotorNoMesin AND tmotor.MotorAutoN = ttpbit.MotorAutoN
		INNER JOIN ttpbhd ON ttpbit.pbNo = ttpbhd.pbNo;
		INSERT INTO tvmotorlokasi
		SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttRK.LokasiKode  AS LokasiKode, ttRK.RKNo AS NoTrans, ttRK.RKJam AS Jam, 'IN' AS Kondisi
		FROM  tmotor INNER JOIN ttRK ON tmotor.RKNo = ttRK.RKNo;
		INSERT INTO tvmotorlokasi
		SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttSD.LokasiKode  AS LokasiKode, ttSD.SDNo AS NoTrans, ttSD.SDJam AS Jam, 'OUT' AS Kondisi
		FROM  tmotor INNER JOIN ttSD ON tmotor.SDNo = ttSD.SDNo;
		INSERT INTO tvmotorlokasi
		SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttSK.LokasiKode AS LokasiKode, ttSK.SKNo AS NoTrans, ttSK.SKJam AS Jam, 'OUT' AS Kondisi
		FROM  tmotor INNER JOIN ttSK ON tmotor.SKNo = ttSK.SKNo;
		INSERT INTO tvmotorlokasi
		SELECT tmotor.MotorAutoN AS MotorAutoN, tmotor.MotorNoMesin AS MotorNoMesin, ttsk.LokasiKode AS LokasiKode, ttsk.SKNo AS NoTrans, ttsk.SKJam AS Jam, 'OUT' AS Kondisi
		FROM ttsk
		INNER JOIN ttrk ON ttrk.SKNo = ttsk.SKNo
		INNER JOIN tmotor ON tmotor.RKNo = ttrk.RKNo
		WHERE (tmotor.SSNo <> '--' OR tmotor.FBNo <> '--' OR tmotor.PDNo <>'--' OR tmotor.SSNo <> '' OR tmotor.FBNo <> '' OR tmotor.PDNo <>'');
		INSERT INTO tvmotorlokasi
		SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttSMhd.LokasiAsal AS LokasiKode, ttSMit.SMNo AS NoTrans, ttSMhd.SMJam AS Jam, 'OUT Pos' AS Kondisi
		FROM tmotor
		INNER JOIN ttSMit ON tmotor.MotorNoMesin = ttSMit.MotorNoMesin AND tmotor.MotorAutoN = ttSMit.MotorAutoN
		INNER JOIN ttSMhd ON ttSMit.SMNo = ttSMhd.SMNo;
	END IF;
END$$
DELIMITER ;

