DELIMITER $$

DROP PROCEDURE IF EXISTS `frk`$$

CREATE DEFINER=`root`@`%` PROCEDURE `frk`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan VARCHAR(150),
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xRKNo VARCHAR(10),
IN xRKNoBaru VARCHAR(10),
IN xCusKode VARCHAR(10),
IN xSKNo VARCHAR(10),
IN xSKTgl DATE,
IN xDKNo VARCHAR(10),
IN xDKTgl DATE,
IN xRKTgl DATE,
IN xRKJam DATETIME,
IN xRKMemo VARCHAR(100),
IN xCusNama VARCHAR(50),
IN xCusKTP VARCHAR(18),
IN xCusKabupaten VARCHAR(50),
IN xCusAlamat VARCHAR(75),
IN xCusRT VARCHAR(3),
IN xCusRW VARCHAR(3),
IN xCusKecamatan VARCHAR(50),
IN xCusTelepon VARCHAR(30),
IN xCusKodePos VARCHAR(35),
IN xCusKelurahan VARCHAR(50),
IN xMotorType VARCHAR(50),
IN xMotorTahun DECIMAL(4,0),
IN xMotorWarna VARCHAR(35),
IN xDKHarga DECIMAL(11,0),
IN xMotorNoMesin VARCHAR(25),
IN xMotorNoRangka VARCHAR(25),
IN xNoGL VARCHAR(10),
IN xLokasiKodeRK VARCHAR(15))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			SET oStatus = 0; SET oKeterangan = CONCAT("Daftar Retur Konsumen [RK]");
		END;
		WHEN "Load" THEN
		BEGIN
			SET oStatus = 0; SET oKeterangan = CONCAT("Load Retur Konsumen [RK] No : ",xRKNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SET @AutoNo = CONCAT('RK',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(RKNo,5)+1),5,0)) INTO @AutoNo FROM ttrk WHERE ((RKNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(RKNo) = 10) ORDER BY RKNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('RK',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xRKNo = @AutoNo;
			SET oStatus = 0;SET oKeterangan = CONCAT("Tambah data baru Retur Konsumen No: ",xRKNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @RKNoOLD = xRKNo;

			#SET oStatus = 0;
			#SET oKeterangan = CONCAT_WS(' - ',xRKNo, xRKTgl, xRKJam, xRKMemo, xUserID, xSKNo, xNoGL, xLokasiKodeRK, xDKNo, xMotorNoMesin);
			
			#Auto Number [ Insert vs Edit ]
			IF LOCATE('=',xRKNo) <> 0 THEN
				SET @AutoNo = CONCAT('RK',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(RKNo,5)+1),5,0)) INTO @AutoNo FROM ttRK WHERE ((RKNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(RKNo) = 10) ORDER BY RKNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('RK',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xRKNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xRKNoBaru = xRKNo;
			END IF;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xRKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xRKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Cek Jam
			IF oStatus = 0 THEN				
				SELECT RKJam INTO @RKJamOLD FROM ttRK WHERE RKNo = @RKNoOLD;
				IF xRKJam < @RKJamOLD THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT(oKeterangan,"Gagal menyimpan data, Tgl-Jam ", DATE_FORMAT(xRKJam,'%d-%m-%Y %k:%i:%s'), ' tidak boleh lebih kecil dari Tgl-Jam semula ',DATE_FORMAT(@RKJamOLD ,'%d-%m-%Y %k:%i:%s'));
				ELSE
					SET oStatus = 0;
				END IF;		
			END IF;
	
			#Update Proses 
			IF oStatus = 0 THEN
				#SET oKeterangan = CONCAT_WS(" - ",xMotorNoMesin,xNoGL,xSKNo,xRKNoBaru,@RKNoOLD );									
				SELECT LokasiKode, MotorNoMesin INTO xLokasiKode, xMotorNoMesin FROM ttrk WHERE RKNo = xRKNo;
				
				UPDATE ttrk
				SET RKNo = xRKNoBaru, RKTgl = xRKTgl, LokasiKode = xLokasiKode, RKMemo = xRKMemo, UserID = xUserID, RKJam = xRKJam, DKNo = xDKNo, SKNo = xSKNo, MotorNoMesin = xMotorNoMesin, NoGL = xNoGL
				WHERE RKNo = @RKNoOLD;		
				SET xRKNo = xRKNoBaru;
				UPDATE Tmotor SET RKNo = xRKNoBaru WHERE RKNo = @RKNoOLD;
				UPDATE tvmotorlokasi SET NoTrans = xRKNoBaru WHERE NoTrans = @RKNoOLD;
				UPDATE tmotor SET FakturAHMNo = xRKNo WHERE SKNo = xSKNo;

				#SetPemutihanByRK(xSKNo)
				UPDATE tmotor SET BPKBTglJadi = xRKJam WHERE BPKBTglJadi = '1900-01-01' AND SKNo = xSKNo;
				UPDATE tmotor SET BPKBTglAmbil = xRKJam, BPKBPenerima = 'Retur'  WHERE BPKBTglAmbil = '1900-01-01' AND SKNo =  xSKNo;
				UPDATE tmotor SET STNKTglMasuk = xRKJam WHERE STNKTglMasuk = '1900-01-01' AND SKNo = xSKNo;
				UPDATE tmotor SET STNKTglBayar = xRKJam WHERE STNKTglBayar = '1900-01-01' AND SKNo = xSKNo;
				UPDATE tmotor SET STNKTglJadi = xRKJam WHERE STNKTglJadi = '1900-01-01' AND SKNo = xSKNo;
				UPDATE tmotor SET STNKTglAmbil = xRKJam, STNKPenerima = 'Retur' WHERE STNKTglAmbil = '1900-01-01' AND SKNo = xSKNo;
				UPDATE tmotor SET PlatTgl = xRKJam WHERE PlatTgl = '1900-01-01' AND SKNo = xSKNo;
				UPDATE tmotor SET PlatTglAmbil = xRKJam, PlatPenerima = 'Retur' WHERE PlatTglAmbil = '1900-01-01' AND SKNo = xSKNo;
				UPDATE tmotor SET FakturAHMTgl = xRKJam WHERE FakturAHMTgl = '1900-01-01' AND SKNo = xSKNo;
				UPDATE tmotor SET FakturAHMTglAmbil = xRKJam WHERE FakturAHMTglAmbil = '1900-01-01' AND SKNo = xSKNo;
				UPDATE tmotor SET NoticeTglJadi = xRKJam WHERE NoticeTglJadi = '1900-01-01' AND SKNo = xSKNo;
				UPDATE tmotor SET NoticeTglAmbil = xRKJam, NoticePenerima = 'Retur' WHERE NoticeTglAmbil = '1900-01-01' AND SKNo = xSKNo;     
      
				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @RKNoOLD; #Hapus Jurnal No Transaksi Lama
				CALL jrk(xRKNo,xUserID); #Posting Jurnal
			
				IF @AutoNo = 'Edit' THEN #Edit
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Retur Konsumen','ttrk', xRKNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Retur Konsumen [RK] No: ", xRKNo);
				ELSE #Add
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Retur Konsumen','ttrk', xRKNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Retur Konsumen [RK] No: ", xRKNo);
				END IF;
			ELSEIF oStatus = 1 THEN
				SET oKeterangan = oKeterangan;
			END IF;		

		END;
		WHEN "Loop" THEN
		BEGIN
			SET oStatus = 0;SET oKeterangan = CONCAT("Loop SK: ",xSKNo, xRKNo);	
			#DK dan SK Asal sebelum pindah SK
			SELECT SKNo, DKNo, RKJam INTO @SKNoAsal, @DKNoAsal, @RKJamAsal FROM ttrk WHERE RKNo = xRKNo;
			SET oKeterangan = CONCAT_WS(" - ",xRKNo, @SKNoAsal, @DKNoAsal, @RKJamAsal);	


			IF @SKNoAsal IS NOT NULL AND @SKNoAsal <> '--' THEN
				UPDATE tmotor SET FakturAHMNo = '--' WHERE SKNo = @SKNoAsal OR DKNo = @DKNoAsal ;
				DELETE FROM tmotor WHERE RKNo = xRKNo;
				DELETE FROM tvmotorlokasi WHERE NoTrans = xRKNo;
				
				#DelPemutihanByRK(@SKNoAsal)
				UPDATE tmotor SET BPKBTglJadi = '1900-01-01' WHERE BPKBTglJadi = @RKJamAsal AND SKNo = @SKNoAsal ; 
				UPDATE tmotor SET BPKBTglAmbil = '1900-01-01', BPKBPenerima = '--'  WHERE BPKBTglAmbil = @RKJamAsal AND SKNo = @SKNoAsal;
				UPDATE tmotor SET STNKTglMasuk = '1900-01-01' WHERE STNKTglMasuk = @RKJamAsal AND SKNo = @SKNoAsal;
				UPDATE tmotor SET STNKTglBayar = '1900-01-01' WHERE  STNKTglBayar = @RKJamAsal AND SKNo = @SKNoAsal;
				UPDATE tmotor SET STNKTglJadi = '1900-01-01' WHERE STNKTglJadi = @RKJamAsal AND SKNo = @SKNoAsal;
				UPDATE tmotor SET STNKTglAmbil = '1900-01-01', STNKPenerima = '--' WHERE  STNKTglAmbil = @RKJamAsal AND SKNo = @SKNoAsal;
				UPDATE tmotor SET PlatTgl = '1900-01-01' WHERE PlatTgl = @RKJamAsal AND SKNo = @SKNoAsal;
				UPDATE tmotor SET PlatTglAmbil = '1900-01-01', PlatPenerima = '--' WHERE PlatTglAmbil = @RKJamAsal AND SKNo = @SKNoAsal;
				UPDATE tmotor SET FakturAHMTgl = '1900-01-01' WHERE FakturAHMTgl = @RKJamAsal AND SKNo = @SKNoAsal;
				UPDATE tmotor SET FakturAHMTglAmbil = '1900-01-01' WHERE FakturAHMTglAmbil = @RKJamAsal AND SKNo = @SKNoAsal;
				UPDATE tmotor SET NoticeTglJadi = '1900-01-01' WHERE NoticeTglJadi = @RKJamAsal AND SKNo =  @SKNoAsal;
				UPDATE tmotor SET NoticeTglAmbil = '1900-01-01', NoticePenerima = '--'  WHERE NoticeTglAmbil = @RKJamAsal AND SKNo = @SKNoAsal;
				UPDATE tmotor SET FakturAHMNo = '--' WHERE SKNo =  @SKNoAsal;
			END IF;
		
     
			SELECT MotorNoMesin, MotorNoRangka, MotorType, MotorWarna, MotorTahun, DealerKode, FBHarga, tmotor.DKNo, tmotor.SKNo, ttsk.LokasiKode
			INTO @MotorNoMesin, @MotorNoRangka, @MotorType, @MotorWarna, @MotorTahun, @DealerKode, @FBHarga, @DKNo, @SKNo, @LokasiKode
			FROM ttsk INNER JOIN tmotor ON ttsk.SKNo = tmotor.SKNo 
			WHERE ttsk.SKNo = xSKNo;						
				
			SELECT IFNULL(MAX(MotorAutoN) + 1,0) INTO @MotorAutoN FROM tmotor WHERE  MotorNoMesin =  @MotorNoMesin AND RKNo <> xRKNo;
			IF @MotorAutoN IS NULL THEN SET @MotorAutoN = 1; END IF;
			UPDATE ttrk SET SKNo = @SKNo, DKNo = @DKNo, LokasiKode = @LokasiKode, MotorNoMesin = @MotorNoMesin WHERE RKNo = xRKNo;						
			INSERT INTO tmotor (MotorAutoN, MotorNoMesin, MotorNoRangka, MotorType, MotorWarna, MotorTahun, DealerKode, FBHarga, SKHarga, SDHarga, MMHarga, RKHarga, SSHarga, RKNo) VALUES (	
			@MotorAutoN,@MotorNoMesin, @MotorNoRangka, @MotorType, @MotorWarna, @MotorTahun, @DealerKode, @FBHarga, @FBHarga, @FBHarga, 0, @FBHarga, @FBHarga, xRKNo);			

			#SET oKeterangan = CONCAT_WS(" - ",xRKNo,xSKJam,xRKJam);	

			UPDATE tmotor SET FakturAHMNo = xRKNo WHERE SKNo = xSKNo;
			INSERT INTO tvmotorlokasi (MotorAutoN, MotorNoMesin, LokasiKode, NoTrans, Jam, Kondisi)
			VALUES (@MotorAutoN, @MotorNoMesin, @LokasiKode, xRKNo,@RKJamAsal, 'IN');
		
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = '';
			SET oStatus = 0;

			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xRKNo;
			IF @GLValid = 'Sudah' THEN	
			  SET oStatus = 1; SET oKeterangan = CONCAT("No RK:", xRKNo, " sudah memiliki No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Update Proses
			IF oStatus = 0 THEN			
				DELETE FROM ttrk WHERE RKNo = xRKNo;
				UPDATE tmotor SET FakturAHMNo = '--' WHERE FakturAHMNo = xRKNo;
				DELETE FROM tmotor WHERE RKNo = xRKNo;
				DELETE FROM tvmotorlokasi WHERE NoTrans = xRKNo;
				
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xRKNo; 
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Retur Konsumen','ttrk', xRKNo); 
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Retur Konsumen No: ",xRKNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;		
		END;
		WHEN "Cancel" THEN
		BEGIN
			SET oStatus = 1;
			IF LOCATE('=',xRKNo) <> 0 THEN
				DELETE FROM ttrk WHERE RKNo = xRKNo;			
				UPDATE tmotor SET FakturAHMNo = '--' WHERE FakturAHMNo = xRKNo;
				DELETE FROM tmotor WHERE RKNo = xRKNo;			
				SET oKeterangan = CONCAT("Batal menyimpan data Retur Konsumen No: ", xRKNoBaru);
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Retur Konsumen No: ", xRKNo);
			END IF;		
		END;
		WHEN "Jurnal" THEN
		BEGIN
			IF LOCATE('=',xRKNo) <> 0 THEN 
				  SET oStatus = 1;
				  SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			 ELSE
				  CALL jrk(xRKNo,xUserID);
				  SET oStatus = 0;
				  SET oKeterangan = CONCAT("Berhasil memproses Jurnal Retur Konsumen [RK] No: ", xRKNo);
			 END IF;
		END;
		WHEN "Print" THEN
		BEGIN
			SELECT  ttsk.SKTgl, ttdk.CusKode, tdcustomer.CusNama, tdcustomer.CusAlamat, tdcustomer.CusRT, tdcustomer.CusRW, tdcustomer.CusKelurahan, tdcustomer.CusKecamatan, tdcustomer.CusKabupaten, tdcustomer.CusTelepon, 
         tmotor.MotorType, tmotor.MotorWarna, tmotor.MotorTahun, tmotor.MotorNoRangka, ttdk.DKHarga, tdlokasi.LokasiNama, tmotor.FBHarga, ttdk.DKTgl, ttrk.RKNo, ttrk.RKTgl, ttrk.LokasiKode, ttrk.RKMemo, ttrk.UserID, ttrk.RKJam, 
         ttrk.DKNo, ttrk.MotorNoMesin, ttrk.SKNo, tdcustomer.CusKTP, tdcustomer.CusKodePos, ttrk.NoGL, tmotor.MotorAutoN, tmotor.SKHarga, tmotor.SDHarga, tmotor.MMHarga, tmotor.RKHarga, tmotor.SSHarga
			FROM ttrk 
			LEFT OUTER JOIN tmotor ON ttrk.RKNo = tmotor.RKNo 
			LEFT OUTER JOIN ttsk ON ttrk.SKNo = ttsk.SKNo 
			LEFT OUTER JOIN tdlokasi ON ttrk.LokasiKode = tdlokasi.LokasiKode 
			LEFT OUTER JOIN ttdk ON ttrk.DKNo = ttdk.DKNo 
			LEFT OUTER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
			WHERE (ttrk.RKNo = xRKNo);
			
			SET oStatus = 0; SET oKeterangan = CONCAT("Cetak Retur Konsumen No: ", xRKNo);
			INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE  
			(NOW(), xUserID , 'Cetak' , CONCAT('Retur Konsumen'),'ttrk', xRKNo); 
		END;
	END CASE;
END$$

DELIMITER ;

