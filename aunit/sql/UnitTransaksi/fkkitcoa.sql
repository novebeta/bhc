DELIMITER $$
DROP PROCEDURE IF EXISTS `fkkitcoa`$$
CREATE DEFINER=`root`@`%` PROCEDURE `fkkitcoa`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan VARCHAR(150),
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xKKNo VARCHAR(12),
IN xKKAutoN SMALLINT,
IN xNoAccount VARCHAR(100),
IN xKKKeterangan VARCHAR(100),
IN xKKBayar VARCHAR(100),
IN xKKAutoNOLD SMALLINT,
IN xNoAccountOLD VARCHAR(10))
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN	
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert
			IF oStatus = 0 THEN				
				SELECT IFNULL((MAX(KKAutoN)+1),0) INTO xKKAutoN FROM ttKKitcoa WHERE KKNo = xKKNo; #AutoN
				INSERT INTO ttKKitcoa (KKNo, KKAutoN, NoAccount, KKBayar, KKKeterangan) VALUES (xKKNo, xKKAutoN, xNoAccount, xKKBayar, xKKKeterangan);
				SET oKeterangan = CONCAT('Berhasil menambahkan data K Keluar ', xKKNo,' - ', xNoAccount, ' - ', FORMAT(xKKBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert
			IF oStatus = 0 THEN				
				SELECT (MAX(KKAutoN)+1) INTO xKKAutoN FROM ttKKitcoa WHERE KKNo = xKKNo; #AutoN
				INSERT INTO ttKKitcoa (KKNo, KKAutoN, NoAccount, KKBayar, KKKeterangan) VALUES (xKKNo, xKKAutoN, xNoAccount, xKKBayar, xKKKeterangan);
				SET oKeterangan = CONCAT('Berhasil menambahkan data Kas Keluar ', xKKNo,' - ', xNoAccount, ' - ', FORMAT(xKKBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;	
		END;
		WHEN "Update" THEN
		BEGIN
			 SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Update
			IF oStatus = 0 THEN
				UPDATE ttKKitcoa SET KKNo = xKKNo, KKAutoN = xKKAutoN, NoAccount = xNoAccount, KKBayar = xKKBayar, KKKeterangan = xKKKeterangan
				WHERE KKNo = xKKNo AND KKAutoN = xKKAutoNOLD AND NoAccount = xNoAccountOLD;
				SET oKeterangan = CONCAT("Berhasil mengubah data kas keluar : ", xKKNo , ' - ' ,xNoAccount);
			ELSE
				SET oKeterangan = CONCAT("Gagal mengubah data kas keluar : ", xKKNo , ' - ' ,xNoAccount);
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Delete
			IF oStatus = 0 THEN
				DELETE FROM ttKKitcoa WHERE KKAutoN = xKKAutoN AND NoAccount = xNoAccount AND KKNo = xKKNo;
				SET oKeterangan = CONCAT("Berhasil menghapus data kas keluar : ", xKKNo , ' - ' ,xNoAccount);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
	END CASE;
END$$

DELIMITER ;

