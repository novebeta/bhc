DELIMITER $$

DROP PROCEDURE IF EXISTS `fpdmotor`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fpdmotor`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xPDNo VARCHAR(18),
IN xMotorType VARCHAR(50),
IN xMotorTahun DECIMAL(4,0),
IN xMotorWarna VARCHAR(35),
IN xMotorNoMesin VARCHAR(25),
IN xMotorNoRangka VARCHAR(25),
IN xSSHarga DECIMAL(12,2),
IN xMotorAutoN SMALLINT(6),
IN xMotorAutoNOLD SMALLINT(6),
IN xMotorNoMesinOLD VARCHAR(25)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;

			#Cek Validasi Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xPDNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xPDNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Cek apakah motor ini sudah pernah ada, jika pernah ada xMotorAutoN diset baru
			IF oStatus = 0 THEN			
				SET @MotorNoMesin = '';
				SELECT MotorNoMesin INTO @MotorNoMesin FROM tmotor WHERE MotorNoMesin = xMotorNoMesin;
				IF @MotorNoMesin <> '' THEN	
					SELECT COUNT(MotorNoMesin) INTO xMotorAutoN FROM tmotor WHERE MotorNoMesin = xMotorNoMesin;
				END IF;			
			END IF;	
			
			#Insert Proses 
			IF oStatus = 0 THEN	

				#Get Harga Beli
				SELECT MotorHrgBeli INTO xSSHarga FROM	tdmotortype WHERE MotorType = xMotorType LIMIT 1;						
				SELECT DealerKode INTO @DealerKode FROM tddealer WHERE DealerStatus = '1' LIMIT 1;
				
				#Insert tmotor			
				INSERT INTO tmotor
				(MotorAutoN, MotorNoMesin, MotorNoRangka, MotorType, MotorWarna, MotorTahun, MotorMemo, DealerKode, PDNo, FBHarga,  SSHarga,  SKHarga,  SDHarga,  RKHarga) VALUES
				(xMotorAutoN, xMotorNoMesin, xMotorNoRangka, xMotorType, xMotorWarna, xMotorTahun, '--', @DealerKode, xPDNo, xSSHarga, xSSHarga, xSSHarga, xSSHarga, xSSHarga);
				
				#InsertTvMotorLokasi
				INSERT IGNORE INTO tvmotorlokasi
				SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttpd.LokasiKode AS LokasiKode, ttpd.PDNo AS NoTrans, ttpd.PDJam AS Jam, 'IN' AS Kondisi 
				FROM tmotor  INNER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo WHERE ttpd.PDNo = xPDNo AND tmotor.MotorAutoN = MotorAutoN AND tmotor.MotorNoMesin = xMotorNoMesin;		

				#Hitung SubTotal 
				SET @PDTotal = 0;
				SELECT IFNULL(SUM(SSHarga),0) INTO @PDTotal FROM tmotor WHERE PDNo = xPDNo;
				UPDATE ttpd	SET PDTotal = @PDTotal	WHERE PDNo = xPDNo;		
				
				SET oKeterangan = CONCAT('Berhasil menambahkan data Motor ', xMotorType,' ', xMotorWarna);
			END IF;
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = '';

			#Cek Validasi Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xPDNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xPDNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Jika Beda cek apakah sudah pernah ada motor dengan nosin yg sama	
			IF xMotorNoMesin <> xMotorNoMesinOLD THEN
				SET @MotorNoMesin = '';
				SELECT MotorNoMesin INTO @MotorNoMesin FROM tmotor 
				WHERE (MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN);
				IF @MotorNoMesin = '' THEN SET oStatus = 0; ELSE SET oStatus = 1; SET oKeterangan = CONCAT("Gagal mengubah data Motor : ", xMotorAutoN , ' - ' ,xMotorNoMesin); END IF;
			END IF;

			#Cek Ukuran NoSin NoKa
			IF oStatus = 0 THEN
				IF LENGTH(xMotorNoMesin) < 12 THEN
					SET oKeterangan = CONCAT(oKeterangan ,"Nomor Mesin kurang lengkap [12 char]");
					SET oStatus = 1;
				END IF;
				IF LENGTH(xMotorNoRangka) < 17 THEN
					SET oKeterangan = CONCAT(oKeterangan ,"Nomor Rangka kurang lengkap [17 char]");                     
					SET oStatus = 1;
				END IF;
			END IF;
			
			#Cek apakah Motor Lama sudah pernah terjadi perpindahan
			IF oStatus = 0 THEN
				SET oKeterangan = "";
				SELECT PDJam INTO @PDJam FROM ttPD WHERE PDNo = xPDNo;
				SELECT GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin = xMotorNoMesinOLD AND MotorAutoN = xMotorAutoNOLD AND JAM > @PDJam	AND NoTrans <> xPDNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(@PDJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
				ELSE
					SET oStatus = 0;	
					SET oKeterangan = '';			
				END IF;		
			END IF;
			
			#Update Proses 
			IF oStatus = 0 THEN
				UPDATE tmotor
				SET MotorAutoN = xMotorAutoN, MotorNoMesin = xMotorNoMesin, MotorNoRangka = xMotorNoRangka, MotorType = xMotorType, MotorWarna = xMotorWarna, MotorTahun = xMotorTahun, PDNo = xPDNo, SSHarga = xSSHarga
				WHERE MotorAutoN = xMotorAutoNOLD AND MotorNoMesin = xMotorNoMesinOLD;
				
				#Hitung SubTotal 
				SET @PDTotal = 0;
				SELECT IFNULL(SUM(SSHarga),0) INTO @PDTotal FROM tmotor WHERE PDNo = xPDNo;
				UPDATE ttpd	SET PDTotal = @PDTotal	WHERE PDNo = xPDNo;	
				
				#tvmotorlokasi
				DELETE FROM tvmotorlokasi WHERE MotorNoMesin = xMotorNoMesinOLD AND MotorAutoN = xMotorAutoNOLD AND NoTrans = xPDNo;
				DELETE FROM tvmotorlokasi WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN AND NoTrans = xPDNo;
				INSERT IGNORE INTO tvmotorlokasi
				SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttpd.LokasiKode AS LokasiKode, ttpd.PDNo AS NoTrans, ttpd.PDJam AS Jam, 'IN' AS Kondisi 
				FROM tmotor  INNER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo WHERE ttpd.PDNo = xPDNo AND tmotor.MotorAutoN = MotorAutoN AND tmotor.MotorNoMesin = xMotorNoMesin;		
				
				#Cek dan beri nilai MotorAutoN
				CALL cMotorAutoN('PD',xPDNo,xMotorNoMesin);
				
				SET oKeterangan = CONCAT("Berhasil mengubah data Motor : ", xMotorAutoN , ' - ' ,xMotorNoMesin);
			ELSE
				SET oKeterangan = CONCAT("Gagal mengubah data Motor : ", xMotorAutoN , ' - ' ,xMotorNoMesin);
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			
			#Cek Validasi Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSSNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
			SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSDNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Cek Mutasi Motor
			IF oStatus = 0 THEN
				SELECT PDJam INTO @PDJam FROM ttPD WHERE PDNo = xPDNo;
				SELECT GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin = xMotorNoMesin AND  MotorAutoN = xMotorAutoN AND JAM > @PDJam AND NoTrans <> xPDNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN	
					SET oStatus = 1; 
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(@PDJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
				ELSE 
					SET oStatus = 0;	
					SET oKeterangan = '';
				END IF;		
			END IF;

			#Delete Proses					
			IF oStatus = 0 THEN
				DELETE FROM tmotor WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN;
				DELETE FROM tvmotorlokasi WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN; #dihapus secara keseluruhan di tvmotorlokasi
				SET oKeterangan = CONCAT("Berhasil menghapus data Motor : ",  xMotorType,' - ', xMotorWarna,' - ', xMotorAutoN , ' - ' ,xMotorNoMesin);
			ELSE
				 SET oKeterangan = oKeterangan; 
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Batal menyimpan data Motor: ", xMotorNoMesin );
		END;
		WHEN "Loop" THEN
		BEGIN
			SET oStatus = 0;
			SELECT DealerKode INTO @DealerKode FROM tddealer WHERE DealerStatus = '1' LIMIT 1;
			INSERT INTO tmotor
			(MotorAutoN, MotorNoMesin, MotorNoRangka, MotorType, MotorWarna, MotorTahun, MotorMemo, DealerKode, SSNo, FBNo, 	  FBHarga,  SSHarga,  SKHarga,  SDHarga,  RKHarga) VALUES 
			(xMotorAutoN, xMotorNoMesin, xMotorNoRangka, xMotorType, xMotorWarna, xMotorTahun, '--', @DealerKode, xSSNo, xFBNo, xSSHarga, xSSHarga, xSSHarga, xSSHarga, xSSHarga);		


			DELETE FROM tvmotorlokasi WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN AND NoTrans = xPDNo;
			INSERT IGNORE INTO tvmotorlokasi
			SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttpd.LokasiKode AS LokasiKode, ttpd.PDNo AS NoTrans, ttpd.PDJam AS Jam, 'IN' AS Kondisi 
			FROM tmotor  INNER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo WHERE ttpd.PDNo = xPDNo AND tmotor.MotorAutoN = MotorAutoN AND tmotor.MotorNoMesin = xMotorNoMesin;		
					
			SET oKeterangan = CONCAT("Berhasil ditambahkan dengan data motor : ",  xMotorType,' - ', xMotorWarna,' - ',xMotorAutoN , ' - ' ,xMotorNoMesin);
		END;
	END CASE;
END$$

DELIMITER ;

