DELIMITER $$
DROP PROCEDURE IF EXISTS `fkkit`$$
CREATE DEFINER=`root`@`%` PROCEDURE `fkkit`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xKKNo VARCHAR(12),
IN xFBNo VARCHAR(18),
IN xKKBayar DECIMAL(14,2),
IN xKKNoOLD VARCHAR(12),
IN xFBNoOLD VARCHAR(18)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert
			IF oStatus = 0 THEN		
				INSERT INTO ttkkit(KKNo, FBNo, KKBayar) VALUES	(xKKNo, xFBNo, xKKBayar);
				SET oKeterangan = CONCAT('Berhasil menambahkan data Kas Keluar ', xBKNo,' - ', xFBNo, ' - ', FORMAT(xKKBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert
			IF oStatus = 0 THEN		
				INSERT INTO ttkkit(KKNo, FBNo, KKBayar) VALUES	(xKKNo, xFBNo, xKKBayar);
				SET oKeterangan = CONCAT('Berhasil menambahkan data Kas Keluar ', xBKNo,' - ', xFBNo, ' - ', FORMAT(xKKBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Update" THEN
		BEGIN		
			SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Update
			IF oStatus = 0 THEN
				UPDATE ttkkit SET KKNo = xKKNo, FBNo = xFBNo, KKBayar = xKKBayar  WHERE KKNo = xKKNoOLD AND FBNo = xFBNoOLD;
				SET oKeterangan = CONCAT("Berhasil mengubah data Kas Keluar : ", xBKNo , ' - ' ,xFBNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Delete
			IF oStatus = 0 THEN
				DELETE FROM ttkkit WHERE FBNo = xFBNo AND KKNo = xKKNo;
				SET oKeterangan = CONCAT("Berhasil menghapus data Kas Keluar : ", xKKNo , ' - ' ,xFBNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
	END CASE;
END$$

DELIMITER ;

