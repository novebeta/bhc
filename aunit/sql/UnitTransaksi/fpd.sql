DELIMITER $$
DROP PROCEDURE IF EXISTS `fpd`$$
CREATE DEFINER=`root`@`%` PROCEDURE `fpd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xPDNo VARCHAR(10),
IN xPDNoBaru VARCHAR(10),
IN xPDTgl DATE,
IN xPDJam DATETIME,
IN xDealerKode VARCHAR(10),
IN xSDNo VARCHAR(10),
IN xPDMemo VARCHAR(100),
IN xJmlUnit DECIMAL(4,0),
IN xPDTotal DECIMAL(11,0),
IN xNoGL VARCHAR(10),
IN xLokasiKodePD VARCHAR(15))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			SET oStatus = 0;
			CALL ctvmotorLokasi('oo',0); #Cek kondisi tvmotorlokasi		

			#Hapus tvmotorlokasi yg belum ada link dengan transaksi PD
			DELETE tvmotorlokasi FROM tvmotorlokasi
			LEFT OUTER JOIN ttpd ON ttpd.PDNO = tvmotorlokasi.NoTrans
			WHERE NoTrans LIKE 'PD%' AND ttpd.PDNO IS NULL;

			#ReJurnal ada transaksi tidak ada jurnal				
			cek01: BEGIN 
				DECLARE rPDNo VARCHAR(100) DEFAULT '';
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT PDNo FROM ttPD
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttPD.PDNo 
				WHERE PDNo LIKE 'PD%' AND ttgeneralledgerhd.NoGL IS NULL AND PDTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rPDNo;
					IF rPDNo IS NOT NULL AND rPDNo <> '' THEN
						CALL jpd(rPDNo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;
								
			SET oKeterangan = CONCAT("Daftar Penerimaan Barang Dealer [PD]");
		END;
		WHEN "Load" THEN
		BEGIN
			SET oStatus = 0;
			SET @JumMotor = 0; SELECT COUNT(MotorNoMesin) INTO @JumMotor FROM tmotor WHERE PDNo = xPDNo;
			CALL ctvmotorLokasi(xPDNo,@JumMotor);
			SET oKeterangan = CONCAT("Load Data Penerimaan Barang Dealer [PD] No : ",xPDNo);	
		END;
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;
			SET @AutoNo = CONCAT('PD',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(PDNo,5)+1),5,0)) INTO @AutoNo FROM ttPD WHERE ((PDNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(PDNo) = 10) ORDER BY PDNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('PD',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xPDNo = @AutoNo;
			SET oKeterangan = CONCAT("Tambah data baru Penerimaan Barang Dealer [PD] No: ",@AutoNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = '';
			SET @AutoNo = 'Edit';
			SET @PDNoOLD = xPDNo;
			
			#Auto Number [ Insert vs Edit ]
			IF LOCATE('=',xPDNo) <> 0  AND LENGTH(xPDNoBaru) = 10 THEN #Generate new auto number 
				SET @AutoNo = CONCAT('PD',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(PDNo,5)+1),5,0)) INTO @AutoNo FROM ttPD WHERE ((PDNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(PDNo) = 10) ORDER BY PDNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('PD',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xPDNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit'; 
				SET xPDNoBaru = xPDNo;
			END IF;
						
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xPDNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xPDNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Cek Jam
			IF oStatus = 0 THEN
				SELECT PDJam INTO @PDJamOLD FROM ttPD WHERE PDNo = @PDNoOLD;
				IF xPDJam < @PDJamOLD THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT(oKeterangan,"Gagal menyimpan data, Tgl-Jam ", DATE_FORMAT(xPDJam,'%d-%m-%Y %k:%i:%s'), ' tidak boleh lebih kecil dari Tgl-Jam semula ',DATE_FORMAT(@PDJamOLD ,'%d-%m-%Y %k:%i:%s'));
				ELSE
					SET oStatus = 0;
				END IF;
			END IF;

			#Cek SD
			IF oStatus = 0 THEN
				IF xSDNo = '--' OR xSDNo = '' THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT('Silahkan memasukkan Nomor Surat Jalan');			
				END IF;
			END IF;
			
			#Cek Ukuran Nosin NoKa
			IF oStatus = 0 THEN
				SELECT COUNT(MotorNoMesin),GROUP_CONCAT(MotorNoMesin) INTO @JumNoSin, @GroupNosin FROM tmotor WHERE SDNo = xSDNo AND LENGTH(MotorNoMesin) < 12;
				IF @JumNoSin > 0 THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT(oKeterangan,"Nomor Mesin kurang lengkap : ", @GroupNosin);
				END IF;
				SELECT COUNT(MotorNoRangka),GROUP_CONCAT(MotorNoRangka) INTO @JumNoKa, @GroupNoKa FROM tmotor WHERE SDNo = xSDNo AND LENGTH(MotorNoRangka) < 17;
				IF @JumNoKa > 0 THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT(oKeterangan," Nomor Rangka kurang lengkap : ", @GroupNoKa);
				END IF;
			END IF;
					
			#Cek Motor Movement
			IF oStatus = 0 THEN
				SELECT PDJam INTO xPDJam FROM ttPD WHERE PDNo = xPDNo;
				SELECT IFNULL(GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>'),'')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin IN (SELECT MotorNoMesin FROM tmotor WHERE PDNo = xPDNo) AND JAM > xPDJam	AND NoTrans <> xPDNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(xPDJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
				ELSE
					SET oStatus = 0;	
					SET oKeterangan = '';			
				END IF;
			END IF;	
			
			#Proses Update
			IF oStatus = 0 THEN #Update
			
				#Hitung SubTotal & Bawah
				SET @PDTotal = 0;
				SELECT IFNULL(SUM(SSHarga),0) INTO @PDTotal FROM tmotor WHERE PDNo = xPDNo;
				
				#Update Utama
				UPDATE ttpd
				SET PDNo = xPDNoBaru, PDTgl = xPDTgl, SDNo = xSDNo, DealerKode = xDealerKode, LokasiKode = xLokasiKodePD, 
				PDMemo = xPDMemo, PDTotal = xPDTotal, UserID = xUserID, PDJam = xPDJam, NoGL = xNoGL
				WHERE PDNo = @PDNoOLD;
				SET xPDNo = xPDNoBaru;		
							
				#Update Motor
				UPDATE tmotor SET PDNo = xPDNoBaru WHERE PDNo = @PDNoOLD;
				UPDATE tmotor SET FBHarga = SSHarga WHERE (FBHarga = 0 OR FBNo = '--') AND PDNo = xPDNo;
				UPDATE tmotor SET SDHarga = SSHarga WHERE (SDHarga = 0 OR SDNo = '--') AND PDNo = xPDNo;
				UPDATE tmotor SET SKHarga = SSHarga WHERE (SKHarga = 0 OR SKNo = '--') AND PDNo = xPDNo;		
				UPDATE tmotor SET RKHarga = SSHarga WHERE (RKHarga = 0 OR RKNo = '--') AND PDNo = xPDNo;			
				
				#UpdateHarga
				UPDATE tmotor JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
				SET tmotor.SSHarga = tdmotortype.MotorHrgBeli, SKHarga = IF(SKHarga = 0,tdmotortype.MotorHrgBeli,SKHarga), SDHarga = IF(SDHarga = 0,tdmotortype.MotorHrgBeli,SDHarga), RKHarga = IF(RKHarga = 0,tdmotortype.MotorHrgBeli,RKHarga) 
				WHERE tmotor.SSHarga = 0 AND PDNo = xPDNo;
				UPDATE tmotor JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
				SET tmotor.FBHarga = tdmotortype.MotorHrgBeli, SKHarga = IF(SKHarga = 0,tdmotortype.MotorHrgBeli,SKHarga), SDHarga = IF(SDHarga = 0,tdmotortype.MotorHrgBeli,SDHarga), RKHarga = IF(RKHarga = 0,tdmotortype.MotorHrgBeli,RKHarga) 
				WHERE tmotor.FBHarga = 0 AND PDNo = xPDNo;

				#Memberi nilai AutoN [cek nomor motor]
				CALL cMotorAutoN('PD',xPDNo,'');
				
				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @PDNoOLD; #Hapus Jurnal No Transaksi Lama
				CALL jpd(xPDNo,xUserID); #Posting Jurnal				

				#TvMotorLokasi
				DELETE FROM tvmotorlokasi WHERE NoTrans = @PDNoOLD;
				DELETE FROM tvmotorlokasi WHERE NoTrans = xPDNo;
				INSERT INTO tvmotorlokasi
				SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttpd.LokasiKode AS LokasiKode, ttpd.PDNo AS NoTrans, ttpd.PDJam AS Jam, 'IN' AS Kondisi 
				FROM tmotor  INNER JOIN ttpd ON tmotor.PDNo = ttpd.PDNo WHERE ttpd.PDNo = xPDNo;		
				
				IF @AutoNo = 'Edit' THEN /*Edit*/
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Penerimaan Barang dari Dealer','ttpd', xPDNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Penerimaan Barang Dealer [PD] No: ", xPDNo);
				ELSE /*Add*/
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Penerimaan Barang dari Dealer','ttpd', xPDNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Penerimaan Barang Dealer [PD] No: ", xPDNo);
				END IF;
			ELSEIF oStatus = 1 THEN
				SET oKeterangan = oKeterangan;				 
			END IF;
		END;
		WHEN "Delete" THEN
			SET oKeterangan = "";
			SET oStatus = 0;

			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xPDNo;
			IF @GLValid = 'Sudah' THEN	/* Jurnal telah tervalidasi data tidak boleh dihapus */	
				SET oStatus = 1; SET oKeterangan = CONCAT("No PD:", xPDNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Cek Motor Movement
			IF oStatus = 0 THEN
				SELECT PDJam INTO xPDJam FROM ttPD WHERE PDNo = xPDNo;
				SELECT IFNULL(GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>'),'')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin IN (SELECT MotorNoMesin FROM tmotor WHERE PDNo = xPDNo) AND JAM > xPDJam	AND NoTrans <> xPDNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(xPDJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
				ELSE
					SET oStatus = 0;	
					SET oKeterangan = '';			
				END IF;
			END IF;	
		
			#Update Proses
			IF oStatus = 0 THEN
				CALL cMotorAutoN('PD',xPDNo,'');
				DELETE FROM ttPD WHERE PDNo = xPDNo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xPDNo;
				DELETE FROM tmotor WHERE PDNo = xPDNo;
				DELETE FROM tvmotorlokasi WHERE NoTrans = xPDNo;
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Penerimaan Barang dari Dealer','ttpd', xPDNo); 
				SET oKeterangan = CONCAT("Berhasil menghapus data Penerimaan Barang Dealer [PD] No: ",xPDNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		BEGIN
		END;
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xPDNo) <> 0 THEN
				DELETE FROM ttpd WHERE PDNo = xPDNo;
				SET oKeterangan = CONCAT("Batal menyimpan data Penerimaan [PD] No: ", xPDNoBaru);
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Penerimaan [PD] No: ", xPDNo);
			END IF;
			INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
			(NOW(), xUserID , 'Cetak' , 'Penerimaan Barang dari Dealer','ttpd', xPDNo); 
		END;
		WHEN "Jurnal" THEN
		BEGIN
			 IF LOCATE('=',xPDNo) <> 0 THEN /* Data Baru, belum tersimpan tidak boleh di jurnal */
				  SET oStatus = 1;
				  SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			 ELSE
				  CALL jPD(xPDNo,xUserID);
				  SET oStatus = 0;
				  SET oKeterangan = CONCAT("Berhasil memproses Jurnal Penerimaan [PD] No: ", xPDNo);
			 END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
		END;		
		WHEN "Print" THEN
		BEGIN
			IF xCetak = "1" THEN
				SELECT ttpd.PDNo, ttpd.PDTgl, ttpd.SDNo, ttpd.DealerKode, ttpd.LokasiKode, ttpd.PDMemo, ttpd.PDTotal, ttpd.UserID, 
				tddealer.DealerNama, tdlokasi.LokasiNama, IFNULL(ttsd.SDTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SDTgl, ttpd.PDJam, ttpd.NoGL
				FROM ttpd 
				LEFT OUTER JOIN tddealer ON ttpd.DealerKode = tddealer.DealerKode 
				LEFT OUTER JOIN tdlokasi ON ttpd.LokasiKode = tdlokasi.LokasiKode 
				LEFT OUTER JOIN ttsd ON ttpd.SDNo = ttsd.SDNo
				WHERE (ttpd.PDNo = xPDNo);
			END IF;
			IF xCetak = "2" THEN
				SELECT tmotor.BPKBNo, tmotor.BPKBTglAmbil, tmotor.BPKBTglJadi, tmotor.DKNo, tmotor.DealerKode, tmotor.FBHarga, tmotor.FBNo, tmotor.FakturAHMNo, tmotor.FakturAHMTgl, tmotor.FakturAHMTglAmbil, tmotor.MotorAutoN, 
            tmotor.MotorMemo, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorTahun, tmotor.MotorType, tmotor.MotorWarna, tmotor.PDNo, tmotor.PlatNo, tmotor.PlatTgl, tmotor.PlatTglAmbil, tmotor.SDNo, tmotor.SKNo, 
            tmotor.SSNo, tmotor.STNKAlamat, tmotor.STNKNama, tmotor.STNKNo, tmotor.STNKTglAmbil, tmotor.STNKTglBayar, tmotor.STNKTglJadi, tmotor.STNKTglMasuk, tdmotortype.MotorNama, tmotor.FakturAHMNilai, tmotor.RKNo, 
            tmotor.RKHarga, tmotor.SKHarga, tmotor.SDHarga, tmotor.MMHarga, tmotor.SSHarga
				FROM  tmotor 
				LEFT OUTER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
				WHERE (tmotor.PDNo = xPDNo)
				ORDER BY tmotor.MotorType, tmotor.MotorTahun, tmotor.MotorWarna;
			END IF;
			
			INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
			(NOW(), xUserID , 'Cetak' , 'Penerimaan Barang dari Dealer','ttpd', xPDNo);
		END;
	END CASE;
END$$

DELIMITER ;

