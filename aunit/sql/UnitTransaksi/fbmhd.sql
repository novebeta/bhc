DELIMITER $$

DROP PROCEDURE IF EXISTS `fbmhd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fbmhd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xBMNo VARCHAR(18),
IN xBMNoBaru VARCHAR(18),
IN xBMTgl DATE,
IN xBMMemo VARCHAR(100),
IN xBMNominal DECIMAL(14,2),
IN xBMJenis VARCHAR(20),
IN xLeaseKode VARCHAR(10),
IN xBMCekNo VARCHAR(25),
IN xBMCekTempo DATE,
IN xBMPerson VARCHAR(50),
IN xBMAC VARCHAR(25),
IN xNoAccount VARCHAR(10),
IN xBMJam VARCHAR(100),
IN xNoGL VARCHAR(100))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			#ReJurnal Transaksi Tidak Memiliki Jurnal
			DECLARE rBMNo VARCHAR(100) DEFAULT '';
			cek01: BEGIN
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT BMNo FROM ttbmhd
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttbmhd.BMNo 
				WHERE ttgeneralledgerhd.NoGL IS NULL AND BMTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rBMNo;
					IF rBMNo IS NOT NULL AND rBMNo <> '' THEN
						CALL jbm(rBMNo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;
			
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Daftar Bank Masuk");
		END;
		WHEN "Load" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Load data Bank Masuk No: ", xBMNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SET @AutoNo = CONCAT('BM','01',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(BMNo,5)+1),5,0)) INTO @AutoNo FROM ttbmhd WHERE ((BMNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(BMNo) = 12) ORDER BY BMNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 12 THEN SET @AutoNo = CONCAT('BM','01',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xBMNo = @AutoNo;
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Tambah data baru Bank Masuk No: ",xBMNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @BMNoOLD = xBMNo;
			
			IF LOCATE('=',xBMNo) <> 0 THEN
				SET @AutoNo = CONCAT('BM',RIGHT(xNoAccount,2),RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(BMNo,5)+1),5,0)) INTO @AutoNo FROM ttBMhd WHERE ((BMNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(BMNo) = 12) ORDER BY BMNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 12 THEN SET @AutoNo = CONCAT('BM',RIGHT(xNoAccount,2),RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xBMNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xBMNoBaru = xBMNo;
			END IF;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Update
			IF oStatus = 0 THEN 
				UPDATE ttbmhd
				SET BMNo = xBMNoBaru, BMTgl = xBMTgl, BMJenis = xBMJenis, BMPerson = xBMPerson, LeaseKode = xLeaseKode, BMNominal = xBMNominal, 
				BMMemo = xBMMemo, NoGL = xNoGL, BMJam = xBMJam, UserID = xUserID,  BMCekNo = xBMCekNo, BMCekTempo = xBMCekTempo, BMAC = xBMAC, NoAccount = xNoAccount
				WHERE BMNo = xBMNo;
				SET xBMNo = xBMNoBaru;
			
				#BMAutoN
				IF xBMJenis = 'Umum' THEN
					UPDATE ttBMitcoa SET BMAutoN = -(BMAutoN) WHERE BMNo = xBMNo;
					blockCOA: BEGIN
					DECLARE myBMNo VARCHAR(12);
					DECLARE myBMAutoN SMALLINT;
					DECLARE myNoAccount VARCHAR(10);
					DECLARE doneCOA INT DEFAULT 0;  	
					DECLARE curCOA CURSOR FOR SELECT BMNo, BMAutoN, NoAccount FROM ttBMitcoa WHERE BMNo = xBMNo ORDER BY ABS(BMAutoN);	
					DECLARE CONTINUE HANDLER FOR NOT FOUND SET doneCOA = 1;
					SET @row_number = 0; 
					OPEN curCOA;
					REPEAT FETCH curCOA INTO myBMNo,myBMAutoN,myNoAccount ;
						IF NOT doneCOA THEN	
							SET @row_number = @row_number + 1; 
							UPDATE ttBMitcoa SET BMAutoN = @row_number
							WHERE BMNo = myBMNo AND BMAutoN = myBMAutoN AND NoAccount = myNoAccount;
						END IF;
					UNTIL doneCOA END REPEAT;
					CLOSE curCOA;
					END blockCOA;		
				END IF;
				
				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @BMNoOLD; #Hapus Jurnal No Transaksi Lama
				CALL jbm(xBMNo,xUserID); #Posting Jurnal
				
				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Bank Masuk','ttbmhd', xBMNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Bank Masuk No: ", xBMNo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Bank Masuk','ttbmhd', xBMNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Bank Masuk No: ", xBMNo);
				END IF;				
			ELSEIF oStatus = 1 THEN
			  SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;
			
			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBMNo;
			IF @GLValid = 'Sudah' THEN	
				SET oStatus = 1; SET oKeterangan = CONCAT("No BM:", xBMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM ttbmhd WHERE BMNo = xBMNo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xBMNo;
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Bank Masuk','ttbmhd', xBMNo); 
				SET oKeterangan = CONCAT("Berhasil menghapus data Bank Masuk No: ",xBMNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 IF LOCATE('=',xBMNo) <> 0 THEN
				  DELETE FROM ttbmhd WHERE BMNo = xBMNo;
				  SET oKeterangan = CONCAT("Batal menyimpan data Bank Masuk No: ", xBMNoBaru);
			 ELSE
				  SET oKeterangan = CONCAT("Batal menyimpan data Bank Masuk No: ", xBMNo);
			 END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
		END;
		WHEN "Jurnal" THEN
		BEGIN
			 IF LOCATE('=',xBMNo) <> 0 THEN 
				  SET oStatus = 1;
				  SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			 ELSE
				  CALL jbm(xBMNo,xUserID);
				  SET oStatus = 0;
				  SET oKeterangan = CONCAT("Berhasil memproses Jurnal Bank Masuk No: ", xBMNo);
			 END IF;
		END;		
		WHEN "Print" THEN
		BEGIN
			IF xCetak = "1" THEN
				SELECT ttbmhd.BMAC, ttbmhd.BMCekNo, ttbmhd.BMCekTempo, ttbmhd.BMJenis, ttbmhd.BMMemo, ttbmhd.BMNo, ttbmhd.BMNominal, 
				ttbmhd.BMPerson, ttbmhd.BMTgl, ttbmhd.LeaseKode, ttbmhd.NoAccount, ttbmhd.UserID, 
				IFNULL(tdleasing.LeaseNama, '--') AS LeaseNama, IFNULL(traccount.NamaAccount, '--') AS NamaAccount, ttbmhd.BMJam, ttbmhd.NoGL
				FROM ttbmhd 
				LEFT OUTER JOIN tdleasing ON ttbmhd.LeaseKode = tdleasing.LeaseKode 
				LEFT OUTER JOIN traccount ON ttbmhd.NoAccount = traccount.NoAccount
				WHERE  (ttbmhd.BMNo = xBMno);
			END IF;
			IF xCetak = "2" THEN
				SELECT ttbmit.BMNo, ttbmit.DKNo, ttbmit.BMBayar, IFNULL(ttdk.LeaseKode, '--') AS LeaseKode
				FROM ttbmit 
				LEFT OUTER JOIN ttdk ON ttbmit.DKNo = ttdk.DKNo
				WHERE (ttbmit.BMNo = xBMNo);
			END IF;
			
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Cetak data Bank Masuk No: ", xBMNo);
			INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE  
			(NOW(), xUserID , 'Cetak' , CONCAT('Bank Masuk ',xBMJenis),'ttbmhd', xBMNo); 
		END;
	END CASE;
END$$

DELIMITER ;

