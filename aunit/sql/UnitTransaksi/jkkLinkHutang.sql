DELIMITER $$

DROP PROCEDURE IF EXISTS `jkkLinkHutang`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jkkLinkHutang`(IN xKKNo VARCHAR(18), IN xKKTgl DATE,IN xNoGL VARCHAR(10), IN xNoAccount VARCHAR(10))
BEGIN
	#Link Hutang Piutang Normal
	DELETE FROM ttglhpit WHERE NoGL = xNoGL;

	DROP TEMPORARY TABLE IF EXISTS Asal; CREATE TEMPORARY TABLE Asal AS
	SELECT KKLink, ttsk.SKNo, KKNominal, IFNULL(SUM(KreditGL),0) AS KreditGL FROM ttkk 
	INNER JOIN tmotor ON ttkk.KKLink = tmotor.DKNo 
	INNER JOIN ttSK ON tmotor.SKNo = ttSK.SKNo 
	INNER JOIN ttgeneralledgerhd ON ttSK.NoGL = ttgeneralledgerhd.NoGL  
	INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL 
	WHERE KKNo = xKKNo AND NoAccount = xNoAccount
	ORDER BY ttsk.skno;

	DROP TEMPORARY TABLE IF EXISTS Terlunasi; CREATE TEMPORARY TABLE Terlunasi AS
	SELECT GLLink, SUM(IFNULL(HPNIlai,0)) AS Terbayar, NoGL FROM Asal
	LEFT OUTER JOIN ttglhpit ON Asal.SKNo = ttglhpit.GLLink
	WHERE NoAccount = xNoAccount AND HPJenis = 'Hutang'
	GROUP BY GLLink ORDER BY GLLink;

	DROP TEMPORARY TABLE IF EXISTS Terbayar; CREATE TEMPORARY TABLE Terbayar AS
	SELECT Asal.KKLink, Asal.SKNo,Asal.KKNominal,Asal.KreditGL, IFNULL(Terbayar,0) AS Terbayar FROM Asal
	LEFT OUTER JOIN
	(SELECT GLLink, SUM(Terbayar) AS Terbayar FROM Terlunasi WHERE NoGL <> xNoGL GROUP BY GLLink) A
	ON Asal.SKNo = A.GLLink;

	INSERT INTO ttglhpit SELECT xNoGL, xKKTgl, SKNo, xNoAccount,'Hutang', (KreditGL - Terbayar) AS KreditGL FROM Terbayar WHERE KreditGL > 0 ;

	#Link Hutang Piutang JA		
	DROP TEMPORARY TABLE IF EXISTS JAAsal; CREATE TEMPORARY TABLE JAAsal AS
	SELECT ttkk.KKlink, ttgeneralledgerhd.GLLink , KKNominal,IFNULL(SUM(KreditGL), 0) AS KreditGL
	FROM  ttkk
	INNER JOIN ttgeneralledgerhd ON ttkk.KKLink = ttgeneralledgerhd.GLLink 
	INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL 
	WHERE KKNo = xKKNo AND NoAccount = xNoAccount
	GROUP BY NoAccount, ttgeneralledgerhd.GLLink
	ORDER BY ttgeneralledgerhd.GLLink;

              
	DROP TEMPORARY TABLE IF EXISTS JATerlunasi; CREATE TEMPORARY TABLE JATerlunasi AS
	SELECT JAAsal.GLLink, SUM(IFNULL(HPNIlai,0)) AS Terbayar, NoGL FROM JAAsal
	LEFT OUTER JOIN ttglhpit ON JAAsal.KKlink = ttglhpit.GLLink
	WHERE NoAccount = xNoAccount AND HPJenis = 'Hutang'
	GROUP BY GLLink ORDER BY GLLink;

	DROP TEMPORARY TABLE IF EXISTS JATerbayar; CREATE TEMPORARY TABLE JATerbayar AS
	SELECT JAAsal.KKLink, JAAsal.GLLink, JAAsal.KKNominal, JAAsal.KreditGL, IFNULL(Terbayar,0) AS Terbayar FROM JAAsal
	LEFT OUTER JOIN
	(SELECT GLLink, SUM(Terbayar) AS Terbayar FROM Terlunasi WHERE NoGL <> xNoGL GROUP BY GLLink) A
	ON JAAsal.GLLink = A.GLLink;

	INSERT INTO ttglhpit SELECT xNoGL, xKKTgl, GLLink, xNoAccount,'Hutang', (KreditGL - Terbayar) AS KreditGL FROM JATerbayar WHERE KreditGL > 0 ;
END$$

DELIMITER ;

