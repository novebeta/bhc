DELIMITER $$
DROP PROCEDURE IF EXISTS `friit`$$
CREATE DEFINER=`root`@`%` PROCEDURE `friit`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xSMNo VARCHAR(10),
IN xMotorType VARCHAR(50),
IN xMotorNama VARCHAR(100),
IN xMotorTahun  DECIMAL(4,0),
IN xMotorWarna VARCHAR(35),
IN xMotorNoRangka VARCHAR(25),
IN xMotorAutoN SMALLINT(6),
IN xMotorNoMesin VARCHAR(25),
IN xMotorAutoNOLD SMALLINT(6),
IN xMotorNoMesinOLD VARCHAR(25)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan ='';
			
			#Cek apakah Motor sudah pernah terjadi perpindahan
			SELECT SMJam INTO @SMJam FROM ttsmhd WHERE SMNo = xSMNo;
			IF oStatus = 0 THEN
				SELECT GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN AND JAM > @SMJam	AND NoTrans <> xSMNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(@SMJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
				ELSE
					SET oStatus = 0;				
					SET oKeterangan = '';
				END IF;		
			END IF;			
			
			#Insert Proses 
			IF oStatus = 0 THEN
				#Insert Utama SM
				INSERT INTO ttsmit (SMNo, MotorNoMesin, MotorAutoN) VALUES (xSMNo, xMotorNoMesin, xMotorAutoN);
				
				#InsertTvMotorLokasi
				DELETE FROM tvmotorlokasi 	WHERE NoTrans = xSMNo AND MotorAutoN = xMotorAutoNOLD AND MotorNoMesin = xMotorNoMesinOLD AND Kondisi = 'Out Pos';			
				DELETE FROM tvmotorlokasi 	WHERE NoTrans = xSMNo AND MotorAutoN = xMotorAutoN AND MotorNoMesin = xMotorNoMesin AND Kondisi = 'Out Pos';			
				INSERT INTO tvmotorlokasi 			
				SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttSMhd.LokasiAsal AS LokasiKode, ttSMit.SMNo AS NoTrans, @SMJam AS Jam, 'OUT Pos' AS Kondisi 
				FROM tmotor 
				INNER JOIN ttSMit ON tmotor.MotorNoMesin = ttSMit.MotorNoMesin AND tmotor.MotorAutoN = ttSMit.MotorAutoN 
				INNER JOIN ttSMhd ON ttSMit.SMNo = ttSMhd.SMNo WHERE ttSMhd.SMNo = xSMNo AND tmotor.MotorAutoN = xMotorAutoN AND tmotor.MotorNoMesin = xMotorNoMesin;
      
				#Hitung SubTotal SM 
				SET @SMTotal = 0;
				SELECT IFNULL(SUM(FBHarga),0) INTO @SMTotal FROM ttsmit 
				INNER JOIN tmotor  ON  tmotor.MotorAutoN = ttsmit.MotorAutoN AND tmotor.MotorNoMesin = ttsmit.MotorNoMesin WHERE SMNo = xSMNo;
				UPDATE ttsmhd	SET SMTotal = @SMTotal	WHERE SMNo = xSMNo;


				#Insert Utama PB
				INSERT INTO ttpbit (PBNo, MotorNoMesin, MotorAutoN) VALUES (xSMNo, xMotorNoMesin, xMotorAutoN);
				
				#InsertTvMotorLokasi
				DELETE FROM tvmotorlokasi 	WHERE NoTrans = xPBNo AND MotorAutoN = xMotorAutoNOLD AND MotorNoMesin = xMotorNoMesinOLD AND Kondisi = 'IN';			
				DELETE FROM tvmotorlokasi 	WHERE NoTrans = xPBNo AND MotorAutoN = xMotorAutoN AND MotorNoMesin = xMotorNoMesin  AND Kondisi = 'IN';			
				INSERT INTO tvmotorlokasi 			
				SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttPBhd.LokasiAsal AS LokasiKode, ttPBit.PBNo AS NoTrans, DATE_ADD(@SMJam,INTERVAL 1 SECOND)AS Jam, 'IN' AS Kondisi 
				FROM tmotor 
				INNER JOIN ttPBit ON tmotor.MotorNoMesin = ttPBit.MotorNoMesin AND tmotor.MotorAutoN = ttPBit.MotorAutoN 
				INNER JOIN ttPBhd ON ttPBit.SMNo = ttPBhd.SMNo WHERE ttPBhd.PBNo = xSMNo AND tmotor.MotorAutoN = xMotorAutoN AND tmotor.MotorNoMesin = xMotorNoMesin;
      
				#Hitung SubTotal PB 
				SET @PBTotal = 0;
				SELECT IFNULL(SUM(FBHarga),0) INTO @PBTotal FROM ttpbit 
				INNER JOIN tmotor  ON  tmotor.MotorAutoN = ttpbit.MotorAutoN AND tmotor.MotorNoMesin = ttpbit.MotorNoMesin WHERE PBNo = xSMNo;
				UPDATE ttpbhd	SET PBTotal = @PBTotal	WHERE PBNo = xSMNo;
      
      		SET oKeterangan = CONCAT('Berhasil menambahkan data Motor ', xMotorType,' - ', xMotorWarna, ' - ',xMotorAutoN , ' - ' ,xMotorNoMesin);												
			END IF;
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = '';

			#Cek Jangan sampai dobel dalam 1 no transaksi
			IF oStatus = 0 THEN
				#Update sdNo sesuai xsdNo					
				IF xMotorNoMesin <> xMotorNoMesinOLD THEN
					SET @MotorNoMesin = '';
					SELECT MotorNoMesin INTO @MotorNoMesin FROM ttsmit 
					WHERE (MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN) AND SMNo = xSMNo ;
					IF @MotorNoMesin = '' THEN SET oStatus = 0; ELSE SET oStatus = 1; SET oKeterangan = CONCAT("Gagal mengubah data Motor : ", xMotorAutoN , ' - ' ,xMotorNoMesin); END IF;
				END IF;
			END IF;

			#Cek apakah Motor Lama sudah pernah terjadi perpindahan
			SELECT SMJam INTO @SMJam FROM ttsmhd WHERE SMNo = xSMNo;
			IF oStatus = 0 THEN
				SELECT GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin = xMotorNoMesinOLD AND MotorAutoN = xMotorAutoNOLD AND JAM > @SMJam	AND NoTrans <> xSMNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(@SMJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
				ELSE
					SET oStatus = 0;	
					SET oKeterangan = '';			
				END IF;		
			END IF;
	
			#Update Proses
			IF oStatus = 0 THEN
				#Update Utama
				UPDATE ttsmit SET MotorNoMesin = xMotorNoMesin , MotorAutoN = xMotorAutoN WHERE SMNo = xSMNo AND MotorAutoN = xMotorAutoNOLD AND MotorNoMesin = xMotorNoMesinOLD; 

				#TvMotorLokasi	
				DELETE FROM tvmotorlokasi WHERE MotorNoMesin = xMotorNoMesinOLD AND MotorAutoN = xMotorAutoNOLD AND NoTrans = xSMNo AND Kondisi = 'Out Pos';
				DELETE FROM tvmotorlokasi WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN AND NoTrans = xSMNo  AND Kondisi = 'Out Pos';
				SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttSMhd.LokasiAsal AS LokasiKode, ttSMit.SMNo AS NoTrans, ttSMhd.SMJam AS Jam, 'OUT Pos' AS Kondisi 
				FROM tmotor 
				INNER JOIN ttSMit ON tmotor.MotorNoMesin = ttSMit.MotorNoMesin AND tmotor.MotorAutoN = ttSMit.MotorAutoN 
				INNER JOIN ttSMhd ON ttSMit.SMNo = ttSMhd.SMNo WHERE ttSMhd.SMNo = xSMNo AND tmotor.MotorAutoN = xMotorAutoN AND tmotor.MotorNoMesin = xMotorNoMesin;

				#Hitung SubTotal 
				SET @SMTotal = 0;
				SELECT IFNULL(SUM(FBHarga),0) INTO @SMTotal FROM ttsmit 
				INNER JOIN tmotor  ON  tmotor.MotorAutoN = ttsmit.MotorAutoN AND tmotor.MotorNoMesin = ttsmit.MotorNoMesin WHERE SMNo = xSMNo;
				UPDATE ttsmhd	SET SMTotal = @SMTotal	WHERE SMNo = xSMNo;
				
				#Update Utama
				UPDATE ttpbit SET MotorNoMesin = xMotorNoMesin , MotorAutoN = xMotorAutoN WHERE PBNo = xSMNo AND MotorAutoN = xMotorAutoNOLD AND MotorNoMesin = xMotorNoMesinOLD; 

				#InsertTvMotorLokasi
				DELETE FROM tvmotorlokasi 	WHERE NoTrans = xPBNo AND MotorAutoN = xMotorAutoNOLD AND MotorNoMesin = xMotorNoMesinOLD AND Kondisi = 'IN';			
				DELETE FROM tvmotorlokasi 	WHERE NoTrans = xPBNo AND MotorAutoN = xMotorAutoN AND MotorNoMesin = xMotorNoMesin  AND Kondisi = 'IN';			
				INSERT INTO tvmotorlokasi 			
				SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttPBhd.LokasiAsal AS LokasiKode, ttPBit.PBNo AS NoTrans, DATE_ADD(@SMJam,INTERVAL 1 SECOND)AS Jam, 'IN' AS Kondisi 
				FROM tmotor 
				INNER JOIN ttPBit ON tmotor.MotorNoMesin = ttPBit.MotorNoMesin AND tmotor.MotorAutoN = ttPBit.MotorAutoN 
				INNER JOIN ttPBhd ON ttPBit.SMNo = ttPBhd.SMNo WHERE ttPBhd.PBNo = xSMNo AND tmotor.MotorAutoN = xMotorAutoN AND tmotor.MotorNoMesin = xMotorNoMesin;
      
				#Hitung SubTotal PB 
				SET @PBTotal = 0;
				SELECT IFNULL(SUM(FBHarga),0) INTO @PBTotal FROM ttpbit 
				INNER JOIN tmotor  ON  tmotor.MotorAutoN = ttpbit.MotorAutoN AND tmotor.MotorNoMesin = ttpbit.MotorNoMesin WHERE PBNo = xSMNo;
				UPDATE ttpbhd	SET PBTotal = @PBTotal	WHERE PBNo = xSMNo;
	
				SET oKeterangan = CONCAT("Berhasil mengubah data Motor : ", xMotorType,' - ', xMotorWarna, xMotorAutoN , ' - ' ,xMotorNoMesin);						
			END IF;	
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = '';
			SET oKeterangan = CONCAT('No Auto :', xMotorAutoN,' No Mesin ', xMotorNoMesin,' ' );
			
			#Cek apakah Motor Lama sudah pernah terjadi perpindahan
			IF oStatus = 0 THEN
				SELECT SMJam INTO @SMJam FROM ttsmhd WHERE SMNo = xSMNo;
				SELECT GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN AND JAM > @SMJam	AND NoTrans <> xSMNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(@SMJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
				ELSE
					SET oStatus = 0;	
					SET oKeterangan = '';			
				END IF;		
			END IF;
			
			#Delete Proses
			IF oStatus = 0 THEN
				#Delete Utama
				DELETE FROM ttsmit WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN AND SMNo = xSMNo;
				DELETE FROM tvmotorlokasi WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN AND NoTrans = xSMNo AND Kondisi = 'Out Pos';

				#Hitung SubTotal 
				SET @SMTotal = 0;
				SELECT IFNULL(SUM(FBHarga),0) INTO @SMTotal FROM ttsmit 
				INNER JOIN tmotor  ON  tmotor.MotorAutoN = ttsmit.MotorAutoN AND tmotor.MotorNoMesin = ttsmit.MotorNoMesin WHERE SMNo = xSMNo;
				UPDATE ttsmhd	SET SMTotal = @SMTotal	WHERE SMNo = xSMNo;

				#Delete Utama
				DELETE FROM ttpbit WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN AND PBNo = xSMNo;
				DELETE FROM tvmotorlokasi WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN AND NoTrans = xSMNo AND Kondisi = 'IN';

				#Hitung SubTotal PB 
				SET @PBTotal = 0;
				SELECT IFNULL(SUM(FBHarga),0) INTO @PBTotal FROM ttpbit 
				INNER JOIN tmotor  ON  tmotor.MotorAutoN = ttpbit.MotorAutoN AND tmotor.MotorNoMesin = ttpbit.MotorNoMesin WHERE PBNo = xSMNo;
				UPDATE ttpbhd	SET PBTotal = @PBTotal	WHERE PBNo = xSMNo;

				SET oKeterangan = CONCAT("Berhasil menghapus data Motor : ",  xMotorType,' - ', xMotorWarna, xMotorAutoN , ' - ' ,xMotorNoMesin);	
			END IF;		
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Batal menyimpan data Motor: ",  xMotorType,' - ', xMotorWarna, xMotorAutoN , ' - ' ,xMotorNoMesin);
		END;
	END CASE;
END$$

DELIMITER ;

