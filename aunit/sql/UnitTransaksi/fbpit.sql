DELIMITER $$

DROP PROCEDURE IF EXISTS `fbpit`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fbpit`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xBKNo VARCHAR(12),
IN xFBNo VARCHAR(18),
IN xBKBayar DECIMAL(14,2),
IN xBKNoOLD VARCHAR(12),
IN xFBNoOLD VARCHAR(18)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;
			INSERT INTO ttbkit (BKNo, FBNo, BKBayar) VALUES	(xBKNo, xFBNo, xBKBayar);
			SET oKeterangan = CONCAT('Berhasil menambahkan item Bank Keluar ', xFBNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			IF oStatus = 0 THEN
				UPDATE ttbkit
				SET BKNo = xBKNo, FBNo = xFBNo, BKBayar = xBKBayar
				WHERE BKNo = xBKNoOLD AND FBNo = xFBNoOLD;
				SET oKeterangan = CONCAT("Berhasil mengubah data bank keluar : ", xBKNo , ' - ' ,xFBNo);
			ELSE
				SET oKeterangan = CONCAT("Gagal mengubah data bank keluar : ", xBKNo , ' - ' ,xFBNo);
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			IF oStatus = 0 THEN
				DELETE FROM ttbkit WHERE FBNo = xFBNo AND BKNo = xBKNo;
				SET oKeterangan = CONCAT("Berhasil menghapus data bank keluar : ", xBKNo , ' - ' ,xFBNo);
			ELSE
				SET oKeterangan = CONCAT("Gagal menghapus data bank keluar : ", xBKNo , ' - ' ,xFBNo);
			END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
			SET oStatus = 0;
			INSERT INTO ttbkit (BKNo, FBNo, BKBayar) VALUES	(xBKNo, xFBNo, xBKBayar);
			SET oKeterangan = CONCAT('Berhasil menambahkan item bank keluar', xFBNo);
		END;
		WHEN "Cancel" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Batal menyimpan data bank keluar: ", xFBNo );
		END;
	END CASE;
END$$

DELIMITER ;

