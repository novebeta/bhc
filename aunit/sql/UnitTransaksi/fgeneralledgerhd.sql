DELIMITER $$

DROP PROCEDURE IF EXISTS `fgeneralledgerhd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fgeneralledgerhd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan VARCHAR(150),
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xNoGL VARCHAR(10),
IN xNoGLBaru VARCHAR(18),
IN xTglGL DATE,
IN xKodeTrans VARCHAR(2),
IN xMemoGL VARCHAR(300),
IN xTotalDebetGL DECIMAL(18,2),
IN xTotalKreditGL DECIMAL(18,2),
IN xGLLink VARCHAR(18),
IN xHPLink VARCHAR(18),
IN xGLValid VARCHAR(5))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			SET oStatus = 0; SET oKeterangan = CONCAT("Daftar Jurnal");
		END;
		WHEN "Load" THEN
		BEGIN
			SET oStatus = 0; SET oKeterangan = CONCAT("Load data Jurnal No: ",xNoGL);
		END;
		WHEN "Insert" THEN
		BEGIN
			IF	xKodeTrans = 'JA' THEN
				SET @AutoNo = CONCAT('JA',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNo FROM ttgeneralledgerhd WHERE ((NoGL LIKE CONCAT(@AutoNo,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('JA',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xNoGL = @AutoNo;		
			ELSEIF xKodeTrans = 'JP' THEN
				SET @AutoNo = CONCAT('JP',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNo FROM ttgeneralledgerhd WHERE ((NoGL LIKE CONCAT(@AutoNo,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('JP',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xNoGL = @AutoNo;					
			ELSEIF xKodeTrans = 'GL' THEN
				SET @AutoNo = CONCAT('MM',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNo FROM ttgeneralledgerhd WHERE ((NoGL LIKE CONCAT(@AutoNo,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('MM',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xNoGL = @AutoNo;							
			ELSE
				SET @AutoNo = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNo FROM ttgeneralledgerhd WHERE ((NoGL LIKE CONCAT(@AutoNo,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xNoGL = @AutoNo;							
			END IF;
				 
			SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Jurnal No: ",xNoGL);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @NoGLOLD = xNoGL;
					
			IF LOCATE('=',xNoGL) <> 0  THEN
				IF	xKodeTrans = 'JA' THEN
					SET @AutoNo = CONCAT('JA',RIGHT(YEAR(NOW()),2),'-');
					SELECT CONCAT(@AutoNo,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNo FROM ttgeneralledgerhd WHERE ((NoGL LIKE CONCAT(@AutoNo,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
					IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('JA',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				ELSEIF xKodeTrans = 'JP' THEN
					SET @AutoNo = CONCAT('JP',RIGHT(YEAR(NOW()),2),'-');
					SELECT CONCAT(@AutoNo,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNo FROM ttgeneralledgerhd WHERE ((NoGL LIKE CONCAT(@AutoNo,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
					IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('JP',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				ELSEIF xKodeTrans = 'GL' THEN
					SET @AutoNo = CONCAT('MM',RIGHT(YEAR(NOW()),2),'-');
					SELECT CONCAT(@AutoNo,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNo FROM ttgeneralledgerhd WHERE ((NoGL LIKE CONCAT(@AutoNo,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
					IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('MM',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				ELSE
					SET @AutoNo = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
					SELECT CONCAT(@AutoNo,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNo FROM ttgeneralledgerhd WHERE ((NoGL LIKE CONCAT(@AutoNo,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
					IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				END IF;
				SET xNoGLBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xNoGLBaru = xNoGL;
			END IF;

			IF oStatus = 0 THEN 
				IF xKodeTrans = 'GL' THEN
					SET xGLLink = xNoGLBaru;
				END IF;
				
				UPDATE ttgeneralledgerhd
				SET NoGL = xNoGLBaru, `KodeTrans` = xKodeTrans, `MemoGL` = xMemoGL, `TotalDebetGL` = xTotalDebetGL, `TotalKreditGL` = xTotalKreditGL,
				`GLLink` = xGLLink, `UserID` = xUserID, `HPLink` = xHPLink, `GLValid` = xGLValid
				WHERE `NoGL` = xNoGL;
				SET xNoGL = xNoGLBaru;

				IF @AutoNo = 'Edit' THEN #Edit
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , CONCAT('Jurnal ',xKodeTrans,' - ',xGLLink),'ttgeneralledgerhd', xNoGL); 
					SET oKeterangan = CONCAT("Berhasil menyimpan data Jurnal No: ", xNoGL);
				ELSE #Add
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , CONCAT('Jurnal ',xKodeTrans,' - ',xGLLink),'ttgeneralledgerhd', xNoGL); 
					SET oKeterangan = CONCAT("Berhasil menyimpan data baru Jurnal No: ", xNoGL);
				END IF;
			ELSEIF oStatus = 1 THEN
			  SET oKeterangan = CONCAT("Gagal, silahkan melengkapi data Jurnal No: ", xNoGL);
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;
			
			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE NoGL = xNoGL;
			IF @GLValid = 'Sudah' THEN	
			  SET oStatus = 1; SET oKeterangan = CONCAT("No Jurnal : ",@NoGL," sudah tervalidasi");
			END IF;
					
			IF oStatus = 0 THEN
				DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL;
				
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , CONCAT('Jurnal ',xKodeTrans,' - ',xGLLink),'ttgeneralledgerhd', xNoGL); 
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Jurnal No: ",xNoGL);
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 IF LOCATE('=',xNoGL) <> 0 THEN
				  DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL;
				  SET oKeterangan = CONCAT("Batal menyimpan data Jurnal No: ", xNoGLBaru);
			 ELSE
				  SET oKeterangan = CONCAT("Batal menyimpan data Jurnal No: ", xNoGL);
			 END IF;
		END;
		WHEN "Print" THEN
		BEGIN
			IF xCetak = "1" THEN
				SELECT NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid
				FROM  ttgeneralledgerhd
				WHERE (NoGL = xNoGL);
			END IF;
			IF xCetak = "2" THEN
				SELECT ttgeneralledgerit.NoGL, ttgeneralledgerit.GLAutoN, ttgeneralledgerit.NoAccount, ttgeneralledgerit.KeteranganGL, 
				ttgeneralledgerit.DebetGL, ttgeneralledgerit.KreditGL, traccount.NamaAccount, ttgeneralledgerit.TglGL
				FROM ttgeneralledgerit 
				INNER JOIN traccount ON ttgeneralledgerit.NoAccount = traccount.NoAccount
				WHERE (ttgeneralledgerit.NoGL = xNoGL)
				ORDER BY ttgeneralledgerit.GLAutoN;
			END IF;
		END;
	END CASE;
END$$

DELIMITER ;

