DELIMITER $$

DROP PROCEDURE IF EXISTS `jfb`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jfb`(IN xFBNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE MyAutoN INTEGER DEFAULT 1;
	DECLARE TotSSHarga DOUBLE DEFAULT 0;
	DECLARE TotFBHarga DOUBLE DEFAULT 0;
	DECLARE TotSDHarga DOUBLE DEFAULT 0;
	DECLARE TotSKHarga DOUBLE DEFAULT 0;

	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xFBNo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;

	SELECT FBNo, FBTgl, SSNo, SupKode, FBTermin, FBTglTempo, FBPSS, FBPtgLain, FBTotal, FBMemo, FBLunas, UserID, NoGL, FPNo, FPTgl, DOTgl, FBExtraDisc, FBPosting, FBPtgMD, FBJam, FPHargaUnit, FPDiscount, FPDPP INTO
	@FBNo, @FBTgl, @SSNo, @SupKode, @FBTermin, @FBTglTempo, @FBPSS, @FBPtgLain, @FBTotal, @FBMemo, @FBLunas, @UserID, @NoGL, @FPNo, @FPTgl, @DOTgl, @FBExtraDisc, @FBPosting, @FBPtgMD, @FBJam, @FPHargaUnit, @FPDiscount, @FPDPP
	FROM ttfb WHERE FBNo = xFBNo;
	SELECT IFNULL(SUM(SSHarga),0), IFNULL(SUM(FBHarga),0), IFNULL(SUM(SDHarga),0), IFNULL(SUM(SKHarga),0) INTO TotSSHarga,TotFBHarga,TotSDHarga,TotSKHarga FROM tmotor WHERE FBNo = xFBNo ;
	SELECT SupStatus INTO @SupStatus FROM tdsupplier WHERE SupKode = @SupKode LIMIT 1;
	SELECT PerusahaanNama INTO @PerusahaanNama FROM tzcompany LIMIT 1;

	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;
	
	DELETE FROM ttgeneralledgerhd WHERE GLLink = xFBNo;
	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL;  
	DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;  
	DELETE FROM ttglhpit WHERE NoGL = xNoGL;

	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, @FBTgl, 'FB', CONCAT('Post Faktur Beli - ',@SupKode), 0, 0, xFBNo, xUserID, '--', xGLValid);

	INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
	(xNoGL, @FBTgl, MyAutoN, '11150200', CONCAT('Post Faktur Beli - ',@SupKode), TotSSHarga, 0); SET MyAutoN = MyAutoN + 1;

	IF TotFBHarga > TotSSHarga THEN
		INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		(xNoGL, @FBTgl, MyAutoN, '11150100', CONCAT('Post Faktur Beli - ',@SupKode),  TotFBHarga - TotSSHarga, 0); SET MyAutoN = MyAutoN + 1;
	ELSEIF TotFBHarga < TotSSHarga THEN
		INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		(xNoGL, @FBTgl, MyAutoN, '11150100', CONCAT('Post Faktur Beli - ',@SupKode),  TotSSHarga - TotFBHarga , 0); SET MyAutoN = MyAutoN + 1;
	END IF;

	IF @SupStatus = '1' THEN /*24010101 Htg Dag Jika Supplier Utama*/
		INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		(xNoGL, @FBTgl, MyAutoN, '24010101', CONCAT('Post Faktur Beli - ',@SupKode),  0, @FBTotal); SET MyAutoN = MyAutoN + 1;
	ELSE
		INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		(xNoGL, @FBTgl, MyAutoN, '24010200', CONCAT('Post Faktur Beli - ',@SupKode),  0, @FBTotal); SET MyAutoN = MyAutoN + 1;
	END IF; /*24010200 Htg Dag Jika Supplier Bukan Utama*/

	IF LEFT(@PerusahaanNama,20) = 'CV. ANUGERAH PERDANA' THEN
		IF @FBPPS > 0 THEN /*11110901 Potongan Program*/
			INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			(xNoGL, @FBTgl, MyAutoN, '11110901', CONCAT('Post Faktur Beli - ',@SupKode),  0, @FBPPS); SET MyAutoN = MyAutoN + 1;
		END IF;
		IF @FBPtgLain > 0 THEN /*11110911 Pendapatan Unit*/
			INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			(xNoGL, @FBTgl, MyAutoN, '11110911', CONCAT('Post Faktur Beli - ',@SupKode),  0, @FBPtgLain); SET MyAutoN = MyAutoN + 1;
		END IF;
		IF @FBExtraDisc > 0 THEN /*11110902 ExtraDiscount*/
			INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			(xNoGL, @FBTgl, MyAutoN, '11110902', CONCAT('Post Faktur Beli - ',@SupKode),  0, @FBExtraDisc); SET MyAutoN = MyAutoN + 1;
		END IF;
		IF @FBPtgMD > 0 THEN /*24050300 Pendapatan Unit*/
			INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			(xNoGL, @FBTgl, MyAutoN, '24050300', CONCAT('Post Faktur Beli - ',@SupKode),  0, @FBPtgMD); SET MyAutoN = MyAutoN + 1;
		END IF;
	ELSE
		IF @FBPPS > 0 THEN /*24050300 Potongan Program*/
			INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			(xNoGL, @FBTgl, MyAutoN, '24050300', CONCAT('Post Faktur Beli - ',@SupKode),  0, @FBPPS); SET MyAutoN = MyAutoN + 1;
		END IF;
		IF @PerusahaanNama = 'CV. CENDANA MEGAH SANTOSA LUWUK' OR @PerusahaanNama = 'CV. MEGAH UTAMA PALU' THEN
			IF @FBExtraDisc > 0 THEN /*11110500  ExtraDiscount*/
			  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			  (xNoGL, @FBTgl, MyAutoN, '60100200', CONCAT('Post Faktur Beli - ',@SupKode),  0, @FBExtraDisc); SET MyAutoN = MyAutoN + 1;
			END IF;
		ELSE
			IF @FBExtraDisc > 0 THEN /*24050700 ExtraDiscount*/
			  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			  (xNoGL, @FBTgl, MyAutoN, '24050700', CONCAT('Post Faktur Beli - ',@SupKode),  0, @FBExtraDisc); SET MyAutoN = MyAutoN + 1;
			END IF;
		END IF;
		IF @FBPtgLain > 0 THEN /*40040100 Pendapatan Unit*/
			INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			(xNoGL, @FBTgl, MyAutoN, '40040100', CONCAT('Post Faktur Beli - ',@SupKode),  0, @FBPtgLain); SET MyAutoN = MyAutoN + 1;
		END IF;
	END IF;

	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttFB SET NoGL = xNoGL, FBPosting = 'Selesai Posting'  WHERE FBNo = xFBNo;
  
	CALL jHutangKreditPiutangDebet(xNoGL);
	CALL jAdjustHPP(xFBNo);
END$$

DELIMITER ;

