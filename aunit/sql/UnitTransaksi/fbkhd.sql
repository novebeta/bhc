DELIMITER $$

DROP PROCEDURE IF EXISTS `fbkhd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fbkhd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xBKNo VARCHAR(18),
IN xBKNoBaru VARCHAR(18),
IN xBKTgl DATE,
IN xBKMemo VARCHAR(100),
IN xBKNominal DECIMAL(11,0),
IN xBKJenis VARCHAR(20),
IN xSupKode VARCHAR(10),
IN xBKCekNo VARCHAR(25),
IN xBKCekTempo DATE,
IN xBKPerson VARCHAR(50),
IN xBKAC VARCHAR(25),
IN xNoAccount VARCHAR(10),
IN xBKJam DATETIME,
IN xNoGL VARCHAR(10))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			#ReJurnal Transaksi Tidak Memiliki Jurnal
			DECLARE rBKNo VARCHAR(100) DEFAULT '';
			cek01: BEGIN
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT BKNo FROM ttBKhd
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttbkhd.BKNo 
				WHERE ttgeneralledgerhd.NoGL IS NULL AND BKTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rBKNo;
					IF rBKNo IS NOT NULL AND rBKNo <> '' THEN
						CALL jBK(rBKNo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Daftar Bank Keluar");
		END;
		WHEN "Load" THEN
		BEGIN
			 SET oStatus = 0; SET oKeterangan = CONCAT("Load data Bank Keluar No: ", xBKNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SET @AutoNo = CONCAT('BK','01',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(BKNo,5)+1),5,0)) INTO @AutoNo FROM ttbkhd WHERE ((BKNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(BKNo) = 12) ORDER BY BKNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 12 THEN SET @AutoNo = CONCAT('BK','01',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xBKNo = @AutoNo;
			SET oStatus = 0;SET oKeterangan = CONCAT("Tambah data baru Bank Keluar No: ",xBKNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @BKNoOLD = xBKNo;
			
			IF LOCATE('=',xBKNo) <> 0 THEN
				SET @AutoNo = CONCAT('BK',RIGHT(xNoAccount,2),RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(BKNo,5)+1),5,0)) INTO @AutoNo FROM ttbkhd WHERE ((BKNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(BKNo) = 12) ORDER BY BKNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 12 THEN SET @AutoNo = CONCAT('BK',RIGHT(xNoAccount,2),RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xBKNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xBKNoBaru = xBKNo;
			END IF;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			# Update 
			IF oStatus = 0 THEN 
				UPDATE ttbkhd
				SET BKNo = xBKNoBaru, BKTgl = xBKTgl, BKMemo = xBKMemo, BKNominal = xBKNominal, BKJenis = xBKJenis, SupKode = xSupKode, BKCekNo = xBKCekNo, 
				BKCekTempo = xBKCekTempo, BKPerson = xBKPerson, BKAC = xBKAC, NoAccount = xNoAccount, BKJam = xBKJam, NoGL = xNoGL
				WHERE BKNo = xBKNo;
				SET xBKNo = xBKNoBaru;
				
				#BKAutoN
				IF xBKJenis = 'Umum' THEN
					UPDATE ttbkitcoa SET BKAutoN = -(BKAutoN) WHERE BKNo = xBKNo;
					blockCOA: BEGIN
					DECLARE myBKNo VARCHAR(12);
					DECLARE myBKAutoN SMALLINT;
					DECLARE myNoAccount VARCHAR(10);
					DECLARE doneCOA INT DEFAULT 0;  	
					DECLARE curCOA CURSOR FOR SELECT BKNo, BKAutoN, NoAccount FROM ttbkitcoa WHERE BKNo = xBKNo ORDER BY ABS(BKAutoN);	
					DECLARE CONTINUE HANDLER FOR NOT FOUND SET doneCOA = 1;
					SET @row_number = 0; 
					OPEN curCOA;
					REPEAT FETCH curCOA INTO myBKNo,myBKAutoN,myNoAccount ;
						IF NOT doneCOA THEN	
							SET @row_number = @row_number + 1; 
							UPDATE ttbkitcoa SET BKAutoN = @row_number
							WHERE BKNo = myBKNo AND BKAutoN = myBKAutoN AND NoAccount = myNoAccount;
						END IF;
					UNTIL doneCOA END REPEAT;
					CLOSE curCOA;
					END blockCOA;		
				END IF;
				
				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @BKNoOLD; #Hapus Jurnal No Transaksi Lama
				CALL jbk(xBKNo,xUserID); #Posting Jurnal
				
				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Bank Keluar','ttbkhd', xBKNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Bank Keluar No: ", xBKNo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Bank Keluar','ttbkhd', xBKNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Bank Keluar No: ", xBKNo);
				END IF;
			ELSEIF oStatus = 1 THEN
			  SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;
			
			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBKNo;
			IF @GLValid = 'Sudah' THEN	
				SET oStatus = 1; SET oKeterangan = CONCAT("No BK:", xBKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM ttbkhd WHERE BKNo = xBKNo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xBKNo;
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Bank Keluar','ttbkhd', xBKNo); 			
				SET oKeterangan = CONCAT("Berhasil menghapus data Bank Keluar No: ",xBKNo);
			ELSE
				SET oKeterangan = oKeterangan;
			 END IF;
		END;
		WHEN "Jurnal" THEN
		BEGIN
			 IF LOCATE('=',xBKNo) <> 0 THEN /* Data Baru, belum tersimpan tidak boleh di jurnal */
				  SET oStatus = 1;
				  SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			 ELSE
				  CALL jbk(xBKNo,xUserID);
				  SET oStatus = 0;
				  SET oKeterangan = CONCAT("Berhasil memproses Jurnal Bank Keluar No: ", xBKNo);
			 END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 IF LOCATE('=',xBKNo) <> 0 THEN
				  DELETE FROM ttbkhd WHERE BKNo = xBKNo;
				  SET oKeterangan = CONCAT("Batal menyimpan data Bank Keluar No: ", xBKNoBaru);
			 ELSE
				  SET oKeterangan = CONCAT("Batal menyimpan data Bank Keluar No: ", xBKNo);
			 END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
		END;	
		WHEN "Print" THEN
		BEGIN
			IF xCetak = "1" THEN
				SELECT ttbkhd.BKAC, ttbkhd.BKCekNo, ttbkhd.BKCekTempo, ttbkhd.BKJam, ttbkhd.BKJenis, ttbkhd.BKMemo, ttbkhd.BKNo, ttbkhd.BKNominal, ttbkhd.BKPerson, 
				ttbkhd.BKTgl, ttbkhd.NoAccount, ttbkhd.NoGL, ttbkhd.SupKode, ttbkhd.UserID, IFNULL(traccount.NamaAccount, '--') AS NamaAccount, IFNULL(tdsupplier.SupNama, '--') AS SupNama
				FROM ttbkhd 
				LEFT OUTER JOIN tdsupplier ON ttbkhd.SupKode = tdsupplier.SupKode 
				LEFT OUTER JOIN traccount ON ttbkhd.NoAccount = traccount.NoAccount
				WHERE (ttbkhd.BKNo = xBKNo);
			END IF;
			IF xCetak = "2" THEN
				SELECT ttbkit.BKNo, ttbkit.FBNo, ttfb.FBTgl, ttfb.SSNo, ttfb.SupKode, tdsupplier.SupNama, ttfb.FBTotal, 
				IFNULL(ttss.SSTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SSTgl, ttbkit.BKBayar, ttfb.FBPSS, ttfb.FBPtgLain, ttfb.FBExtraDisc
				FROM ttbkit 
				INNER JOIN ttbkhd ON ttbkit.BKNo = ttbkhd.BKNo 
				INNER JOIN ttfb ON ttbkit.FBNo = ttfb.FBNo 
				INNER JOIN tdsupplier ON ttfb.SupKode = tdsupplier.SupKode 
				LEFT OUTER JOIN ttss ON ttfb.SSNo = ttss.SSNo
				WHERE (ttbkit.BKNo = xBKNo) 
				ORDER BY ttbkit.FBNo;
			END IF;
			
			SET oStatus = 0; SET oKeterangan = CONCAT("Cetak data Bank Keluar No: ", xBKNo);
			INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE  
			(NOW(), xUserID , 'Cetak' , CONCAT('Bank Keluar ',xBKJenis),'ttbkhd', xBKNo); 
		END;
	END CASE;
END$$

DELIMITER ;

