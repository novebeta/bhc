DELIMITER $$

DROP PROCEDURE IF EXISTS `jbkLinkHutang`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jbkLinkHutang`(IN xBKNo VARCHAR(18), IN xBKTgl DATE, IN xNoGL VARCHAR(10), IN xNoAccount VARCHAR(10))
BEGIN
	#Link Hutang Piutang Normal
	DELETE FROM ttglhpit WHERE NoGL = xNoGL;

	DROP TEMPORARY TABLE IF EXISTS Asal; CREATE TEMPORARY TABLE Asal AS
	SELECT ttBkit.FBno, ttSK.SKNo, BKBayar, IFNULL(SUM(KreditGL),0) AS KreditGL FROM ttBkit
	INNER JOIN tmotor ON ttBkit.FBNo = tmotor.DKNo
	INNER JOIN ttSK ON tmotor.SKNo = ttSK.SKNo
	INNER JOIN ttgeneralledgerhd ON ttSK.NoGL = ttgeneralledgerhd.NoGL
	INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL
	WHERE BKNo = xBKNo AND NoAccount = xNoAccount
	GROUP BY NoAccount , ttsk.skno
	ORDER BY ttsk.skno;

	DROP TEMPORARY TABLE IF EXISTS Terlunasi; CREATE TEMPORARY TABLE Terlunasi AS
	SELECT GLLink, SUM(IFNULL(HPNIlai,0)) AS Terbayar, NoGL FROM Asal
	LEFT OUTER JOIN ttglhpit ON Asal.SKNo = ttglhpit.GLLink
	WHERE NoAccount = xNoAccount AND HPJenis = 'Hutang' AND ttglhpit.NoGL <> xNoGL
	GROUP BY GLLink ORDER BY GLLink;

	DROP TEMPORARY TABLE IF EXISTS Terbayar; CREATE TEMPORARY TABLE Terbayar AS
	SELECT Asal.FBNo, Asal.SKNo,Asal.BKBayar,Asal.KreditGL, IFNULL(Terbayar,0) AS Terbayar FROM Asal
	LEFT OUTER JOIN
	(SELECT GLLink, SUM(Terbayar) AS Terbayar FROM Terlunasi GROUP BY GLLink) A
	ON Asal.SKNo = A.GLLink;

	INSERT INTO ttglhpit SELECT xNoGL, xBKTgl, SKNo, xNoAccount,'Hutang', (KreditGL - Terbayar) AS KreditGL FROM Terbayar WHERE KreditGL > 0 ;

	#Link Hutang Piutang JA		
	DROP TEMPORARY TABLE IF EXISTS JAAsal; CREATE TEMPORARY TABLE JAAsal AS
	SELECT ttBkit.FBno, ttgeneralledgerhd.GLLink , BKBayar,IFNULL(SUM(KreditGL), 0) AS KreditGL
	FROM  ttBkit
	INNER JOIN ttgeneralledgerhd ON ttBkit.FBNo = ttgeneralledgerhd.GLLink
	INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL
	WHERE BkNo = xBKNo AND NoAccount = xNoAccount
	GROUP BY NoAccount, ttgeneralledgerhd.GLLink
	ORDER BY ttgeneralledgerhd.GLLink;

	DROP TEMPORARY TABLE IF EXISTS JATerlunasi; CREATE TEMPORARY TABLE JATerlunasi AS
	SELECT JAAsal.GLLink, SUM(IFNULL(HPNIlai,0)) AS Terbayar, NoGL FROM JAAsal
	LEFT OUTER JOIN ttglhpit ON JAAsal.FBno = ttglhpit.GLLink
	WHERE NoAccount = xNoAccount AND HPJenis = 'Hutang' AND ttglhpit.NoGL <> xNoGL
	GROUP BY GLLink ORDER BY GLLink;

	DROP TEMPORARY TABLE IF EXISTS JATerbayar; CREATE TEMPORARY TABLE JATerbayar AS
	SELECT JAAsal.FBNo, JAAsal.GLLink, JAAsal.BKBayar, JAAsal.KreditGL, IFNULL(Terbayar,0) AS Terbayar FROM JAAsal
	LEFT OUTER JOIN
	(SELECT GLLink, SUM(Terbayar) AS Terbayar FROM Terlunasi GROUP BY GLLink) A
	ON JAAsal.GLLink = A.GLLink;

	INSERT INTO ttglhpit SELECT xNoGL, xBKTgl, GLLink, xNoAccount,'Hutang', (KreditGL - Terbayar) AS KreditGL FROM JATerbayar WHERE KreditGL > 0 ;
END$$

DELIMITER ;

