DELIMITER $$
DROP PROCEDURE IF EXISTS `fkpit`$$
CREATE DEFINER=`root`@`%` PROCEDURE `fkpit`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xKKNo VARCHAR(12),
IN xFBNo VARCHAR(18),
IN xKKBayar DECIMAL(14,2),
IN xKKNoOLD VARCHAR(12),
IN xFBNoOLD VARCHAR(18)
)
BEGIN
    CASE xAction
		WHEN "Insert" THEN
		BEGIN
			 SET oStatus = 0;
			 INSERT INTO ttkkit (KKNo, FBNo, KKBayar) VALUES (xKKNo, xFBNo, xKKBayar);
			 SET oKeterangan = CONCAT('Berhasil menambahkan item Kas Keluar ', xFBNo);
		END;
		WHEN "Loop" THEN
		BEGIN
			 SET oStatus = 0;
			 INSERT INTO ttkkit (KKNo, FBNo, KKBayar) VALUES (xKKNo, xFBNo, xKKBayar);
			 SET oKeterangan = CONCAT('Berhasil menambahkan item Kas Keluar ', xFBNo);
		END;
		WHEN "Update" THEN
		BEGIN
			 SET oStatus = 0;
			 IF oStatus = 0 THEN
				  UPDATE ttkkit
				  SET KKNo = xKKNo, FBNo = xFBNo, KKBayar = xKKMBayar
				  WHERE KKNo = xKKNoOLD AND FBNo = xFBNoOLD;
				  SET oKeterangan = CONCAT("Berhasil mengubah data kas keluar : ", xKKNo , ' - ' ,xFBNo);
			 ELSE
				  SET oKeterangan = CONCAT("Gagal mengubah data kas keluar : ", xKKNo , ' - ' ,xFBNo);
			 END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			 SET oStatus = 0;
			 IF oStatus = 0 THEN
				  DELETE FROM ttkkit WHERE FBNo = xFBNo AND KKNo = xKKNo;
				  SET oKeterangan = CONCAT("Berhasil menghapus data kas keluar : ", xKKNo , ' - ' ,xFBNo);
			 ELSE
				  SET oKeterangan = CONCAT("Gagal menghapus data kas keluar : ", xKKNo , ' - ' ,xFBNo);
			 END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Batal menyimpan data kas keluar: ", xFBNo );
		END;
	END CASE;
END$$

DELIMITER ;

