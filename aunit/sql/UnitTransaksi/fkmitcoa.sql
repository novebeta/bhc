DELIMITER $$
DROP PROCEDURE IF EXISTS `fkmitcoa`$$
CREATE DEFINER=`root`@`%` PROCEDURE `fkmitcoa`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan VARCHAR(150),
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xKMNo VARCHAR(12),
IN xKMAutoN SMALLINT,
IN xNoAccount VARCHAR(100),
IN xKMKeterangan VARCHAR(100),
IN xKMBayar VARCHAR(100),
IN xKMAutoNOLD SMALLINT,
IN xNoAccountOLD VARCHAR(10))
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN	
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert	
			IF oStatus = 0 THEN					
				SELECT IFNULL((MAX(KMAutoN)+1),0) INTO xKMAutoN FROM ttKMitcoa WHERE KMNo = xKMNo; #AutoN
				INSERT INTO ttKMitcoa (KMNo, KMAutoN, NoAccount, KMBayar, KMKeterangan) VALUES (xKMNo, xKMAutoN, xNoAccount, xKMBayar, xKMKeterangan);
				SET oKeterangan = CONCAT('Berhasil menambahkan data Kas Keluar ', xKMNo,' - ', xNoAccount, ' - ', FORMAT(xKMBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert
			IF oStatus = 0 THEN				
				SELECT (MAX(KMAutoN)+1) INTO xKMAutoN FROM ttKMitcoa WHERE KMNo = xKMNo; #AutoN
				INSERT INTO ttKMitcoa (KMNo, KMAutoN, NoAccount, KMBayar, KMKeterangan) VALUES (xKMNo, xKMAutoN, xNoAccount, xKMBayar, xKMKeterangan);
				SET oKeterangan = CONCAT('Berhasil menambahkan data Kas Keluar ', xKMNo,' - ', xNoAccount, ' - ', FORMAT(xKMBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;	
		END;
		WHEN "Update" THEN
		BEGIN
			 SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Update
			IF oStatus = 0 THEN
				UPDATE ttKMitcoa SET KMNo = xKMNo, KMAutoN = xKMAutoN, NoAccount = xNoAccount, KMBayar = xKMBayar, KMKeterangan = xKMKeterangan
				WHERE KMNo = xKMNo AND KMAutoN = xKMAutoNOLD AND NoAccount = xNoAccountOLD;
				SET oKeterangan = CONCAT("Berhasil mengubah data kas keluar : ", xKMNo , ' - ' ,xNoAccount);
			ELSE
				SET oKeterangan = CONCAT("Gagal mengubah data kas keluar : ", xKMNo , ' - ' ,xNoAccount);
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Delete
			IF oStatus = 0 THEN
				DELETE FROM ttKMitcoa WHERE KMAutoN = xKMAutoN AND NoAccount = xNoAccount AND KMNo = xKMNo;
				SET oKeterangan = CONCAT("Berhasil menghapus data kas keluar : ", xKMNo , ' - ' ,xNoAccount);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
	END CASE;
END$$

DELIMITER ;

