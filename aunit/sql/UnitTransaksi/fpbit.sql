DELIMITER $$
DROP PROCEDURE IF EXISTS `fpbit`$$
CREATE DEFINER=`root`@`%` PROCEDURE `fpbit`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xPBNo VARCHAR(10),
IN xMotorType VARCHAR(50),
IN xMotorTahun VARCHAR(100),
IN xMotorWarna VARCHAR(35),
IN xMotorNoMesin VARCHAR(25),
IN xMotorNoRangka VARCHAR(25),
IN xMotorAutoN SMALLINT(6),
IN xMotorAutoNOLD SMALLINT(6),
IN xMotorNoMesinOLD VARCHAR(25))
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = '';

			#Cek apakah Motor sudah pernah terjadi perpindahan
			IF oStatus = 0 THEN
				SELECT PBJam INTO @PBJam FROM ttPBhd WHERE PBNo = xPBNo;
				SELECT GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN AND JAM > @PBJam	AND NoTrans <> xPBNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(@PBJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
				ELSE
					SET oStatus = 0;				
					SET oKeterangan = '';
				END IF;		
			END IF;			
			
			#Insert Proses 
			IF oStatus = 0 THEN			
				INSERT INTO ttPBit (PBNo, MotorNoMesin, MotorAutoN) VALUES (xPBNo, xMotorNoMesin, xMotorAutoN);
				
				#InsertTvMotorLokasi
				DELETE FROM tvmotorlokasi 	WHERE NoTrans = xPBNo AND MotorAutoN = xMotorAutoNOLD AND MotorNoMesin = xMotorNoMesinOLD;			
				DELETE FROM tvmotorlokasi 	WHERE NoTrans = xPBNo AND MotorAutoN = xMotorAutoN AND MotorNoMesin = xMotorNoMesin;			
				INSERT INTO tvmotorlokasi 
				SELECT  tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttpbhd.LokasiTujuan AS LokasiKode, ttpbit.pbNo AS NoTrans, ttpbhd.pbJam AS Jam, 'IN' AS Kondisi 
				FROM tmotor 
				INNER JOIN ttpbit ON tmotor.MotorNoMesin = ttpbit.MotorNoMesin AND tmotor.MotorAutoN = ttpbit.MotorAutoN 
				INNER JOIN ttpbhd ON ttpbit.pbNo = ttpbhd.pbNo WHERE ttPBhd.PBNo = xPBNo AND tmotor.MotorAutoN = xMotorAutoN AND tmotor.MotorNoMesin = xMotorNoMesin;
      			
				#SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttPBhd.LokasiAsal AS LokasiKode, ttPBit.PBNo AS NoTrans, ttPBhd.PBJam AS Jam, 'OUT Pos' AS Kondisi 
				#FROM tmotor 
				#INNER JOIN ttPBit ON tmotor.MotorNoMesin = ttPBit.MotorNoMesin AND tmotor.MotorAutoN = ttPBit.MotorAutoN 
				#INNER JOIN ttPBhd ON ttPBit.PBNo = ttPBhd.PBNo WHERE ttPBhd.PBNo = xPBNo AND tmotor.MotorAutoN = xMotorAutoN AND tmotor.MotorNoMesin = xMotorNoMesin;
      
				#Hitung SubTotal 
				SET @PBTotal = 0;
				SELECT IFNULL(SUM(FBHarga),0) INTO @PBTotal FROM ttPBit 
				INNER JOIN tmotor  ON  tmotor.MotorAutoN = ttPBit.MotorAutoN AND tmotor.MotorNoMesin = ttPBit.MotorNoMesin WHERE PBNo = xPBNo;
				UPDATE ttPBhd	SET PBTotal = @PBTotal	WHERE PBNo = xPBNo;
      
      		SET oKeterangan = CONCAT('Berhasil menambahkan data Motor ', xMotorType,' - ', xMotorWarna, ' - ',xMotorAutoN , ' - ' ,xMotorNoMesin);												
			END IF;			
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = '';
			
			#Cek Jangan sampai dobel dalam 1 no transaksi
			IF oStatus = 0 THEN
				#Update sdNo sesuai xsdNo					
				IF xMotorNoMesin <> xMotorNoMesinOLD THEN
					SET @MotorNoMesin = '';
					SELECT MotorNoMesin INTO @MotorNoMesin FROM ttPBit 
					WHERE (MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN) AND PBNo = xPBNo ;
					IF @MotorNoMesin = '' THEN SET oStatus = 0; ELSE SET oStatus = 1; SET oKeterangan = CONCAT("Gagal mengubah data Motor : ", xMotorAutoN , ' - ' ,xMotorNoMesin); END IF;
				END IF;
			END IF;
			
			#Cek apakah Motor Lama sudah pernah terjadi perpindahan
			IF oStatus = 0 THEN
				SELECT PBJam INTO @PBJam FROM ttPBhd WHERE PBNo = xPBNo;
				SELECT GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin = xMotorNoMesinOLD AND MotorAutoN = xMotorAutoNOLD AND JAM > @PBJam	AND NoTrans <> xPBNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(@PBJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
				ELSE
					SET oStatus = 0;	
					SET oKeterangan = '';			
				END IF;		
			END IF;
			
			#Update Proses
			IF oStatus = 0 THEN
				#Update Utama
				UPDATE ttPBit SET MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN WHERE PBNo = xPBNo AND MotorAutoN = xMotorAutoNOLD AND MotorNoMesin = xMotorNoMesinOLD; 

				#TvMotorLokasi	
				DELETE FROM tvmotorlokasi WHERE MotorNoMesin = xMotorNoMesinOLD AND MotorAutoN = xMotorAutoNOLD AND NoTrans = xPBNo;
				DELETE FROM tvmotorlokasi WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN AND NoTrans = xPBNo;
				#SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttPBhd.LokasiAsal AS LokasiKode, ttPBit.PBNo AS NoTrans, ttPBhd.PBJam AS Jam, 'OUT Pos' AS Kondisi 
				#FROM tmotor 
				#INNER JOIN ttPBit ON tmotor.MotorNoMesin = ttPBit.MotorNoMesin AND tmotor.MotorAutoN = ttPBit.MotorAutoN 
				#INNER JOIN ttPBhd ON ttPBit.PBNo = ttPBhd.PBNo WHERE ttPBhd.PBNo = xPBNo AND tmotor.MotorAutoN = xMotorAutoN AND tmotor.MotorNoMesin = xMotorNoMesin;

				#Hitung SubTotal 
				SET @PBTotal = 0;
				SELECT IFNULL(SUM(FBHarga),0) INTO @PBTotal FROM ttPBit 
				INNER JOIN tmotor  ON  tmotor.MotorAutoN = ttPBit.MotorAutoN AND tmotor.MotorNoMesin = ttPBit.MotorNoMesin WHERE PBNo = xPBNo;
				UPDATE ttPBhd	SET PBTotal = @PBTotal	WHERE PBNo = xPBNo;
				
				SET oKeterangan = CONCAT("Berhasil mengubah data Motor : ", xMotorType,' - ', xMotorWarna, xMotorAutoN , ' - ' ,xMotorNoMesin);			
			END IF;
		END;
		WHEN "Delete" THEN		
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = '';
			
			#Cek apakah Motor Lama sudah pernah terjadi perpindahan
			IF oStatus = 0 THEN
				SELECT PBJam INTO @PBJam FROM ttPBhd WHERE PBNo = xPBNo;
				SELECT GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN AND JAM > @PBJam	AND NoTrans <> xPBNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(@PBJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
				ELSE
					SET oStatus = 0;	
					SET oKeterangan = '';			
				END IF;		
			END IF;
			
			#Delete Proses
			IF oStatus = 0 THEN
				DELETE FROM ttPBit WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN AND PBNo = xPBNo;
				DELETE FROM tvmotorlokasi WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN AND NoTrans = xPBNo;

				#Hitung SubTotal 
				SET @PBTotal = 0;
				SELECT IFNULL(SUM(FBHarga),0) INTO @PBTotal FROM ttPBit 
				INNER JOIN tmotor  ON  tmotor.MotorAutoN = ttPBit.MotorAutoN AND tmotor.MotorNoMesin = ttPBit.MotorNoMesin WHERE PBNo = xPBNo;
				UPDATE ttPBhd	SET PBTotal = @PBTotal	WHERE PBNo = xPBNo;
				
				SET oKeterangan = CONCAT("Berhasil menghapus data Motor : ", xMotorType,' - ', xMotorWarna, xMotorAutoN , ' - ' ,xMotorNoMesin);			
			END IF;	

		END;
		WHEN "Cancel" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Batal menyimpan data Motor: ",  xMotorType,' - ', xMotorWarna, xMotorAutoN , ' - ' ,xMotorNoMesin);
		END;
		WHEN "Loop" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = '';
			SET oKeterangan = CONCAT(xPBNo,xMotorNoMesin, xMotorNoRangka);
/*
			#Cek apakah Motor sudah pernah terjadi perpindahan
			IF oStatus = 0 THEN
				SELECT PBJam INTO @PBJam FROM ttPBhd WHERE PBNo = xPBNo;
				SELECT GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN AND JAM > @PBJam	AND NoTrans <> xPBNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(@PBJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
				ELSE
					SET oStatus = 0;				
					SET oKeterangan = '';
				END IF;		
			END IF;	
*/			
		END;
	END CASE;
END$$

DELIMITER ;

