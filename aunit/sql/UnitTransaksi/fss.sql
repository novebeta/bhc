DELIMITER $$
DROP PROCEDURE IF EXISTS `fss`$$
CREATE DEFINER=`root`@`%` PROCEDURE `fss`(
    IN xAction VARCHAR(10),
    OUT oStatus INT,
    OUT oKeterangan TEXT,
    IN xUserID VARCHAR(15),
    IN xKodeAkses VARCHAR(15),
    IN xLokasiKode VARCHAR(15),
    INOUT xSSNo VARCHAR(18),
    IN xSSNoBaru VARCHAR(18),
    IN xSSTgl DATE,
    IN xSSJam DATETIME,
    IN xSSNoTerima VARCHAR(10),
    IN xSupKode VARCHAR(10),
    IN xFBNo VARCHAR(18),
    IN xFBTgl DATE,
    IN xDONo VARCHAR(18),
    IN xLokasiKodeSS VARCHAR(15),
    IN xSSMemo VARCHAR(100),
    IN xNoGL VARCHAR(10),
    IN xCetak VARCHAR(5))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN		
			DECLARE rSSNo VARCHAR(100) DEFAULT '';
			cek01: BEGIN
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT SSNo FROM ttSS
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttSS.SSNo 
				WHERE ttgeneralledgerhd.NoGL IS NULL AND SSTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rSSNo;
					IF rSSNo IS NOT NULL AND rSSNo <> '' THEN
						CALL jSS(rSSNo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;
			
			CALL ctvmotorLokasi('oo',0);
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Daftar Penerimaan Barang [SS] ");
		END;
		WHEN "Load" THEN
		BEGIN
			SET @JumMotor = 0; SELECT COUNT(MotorNoMesin) INTO @JumMotor FROM tmotor WHERE SSNo = xSSNo;
			CALL ctvmotorLokasi(xSSNo,@JumMotor);
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Load data Penerimaan Barang [SS] No : ",xSSNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SET @AutoNo = CONCAT('SS',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(SSNo,5)+1),5,0)) INTO @AutoNo FROM ttss WHERE ((SSNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(SSNo) = 10) ORDER BY SSNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('SS',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xSSNo = @AutoNo;
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Tambah data baru Penerimaan Barang [SS] No: ", @AutoNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET @AutoNo = 'Edit';
			SET @SSNoOLD = xSSNo;
			SET oKeterangan = '';
						
			#Auto Number [ Insert vs Edit ]
			IF (LOCATE('=',xSSNo) <> 0) THEN 
				SET @AutoNo = CONCAT('SS',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(SSNo,5)+1),5,0)) INTO @AutoNo FROM ttss WHERE ((SSNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(SSNo) = 10) ORDER BY SSNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('SS',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xSSNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xSSNoBaru = xSSNo;
			END IF;
					
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSSNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSDNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Cek Jam
			IF oStatus = 0 THEN
				SELECT SSJam INTO @SSJamOLD FROM ttss WHERE SSNo = @SSNoOLD;
				IF xSSJam < @SSJamOLD THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT(oKeterangan,"Gagal menyimpan data, Tgl-Jam ", DATE_FORMAT(xSSJam,'%d-%m-%Y %k:%i:%s'), ' tidak boleh lebih kecil dari Tgl-Jam semula ',DATE_FORMAT(@SSJamOLD ,'%d-%m-%Y %k:%i:%s'));
				ELSE
					SET oStatus = 0;
				END IF;
			END IF;
			
			#Cek Ukuran Nosin NoKa
			IF oStatus = 0 THEN
				SELECT COUNT(MotorNoMesin),GROUP_CONCAT(MotorNoMesin) INTO @JumNoSin, @GroupNosin FROM tmotor WHERE SSNo = xSSNo AND LENGTH(MotorNoMesin) < 12;
				IF @JumNoSin > 0 THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT(oKeterangan,"Nomor Mesin kurang lengkap : ", @GroupNosin);
				END IF;
				SELECT COUNT(MotorNoRangka),GROUP_CONCAT(MotorNoRangka) INTO @JumNoKa, @GroupNoKa FROM tmotor WHERE SSNo = xSSNo AND LENGTH(MotorNoRangka) < 17;
				IF @JumNoKa > 0 THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT(oKeterangan," Nomor Rangka kurang lengkap : ", @GroupNoKa);
				END IF;
			END IF;
			
			#Cek Motor Movement
			IF oStatus = 0 THEN
				SELECT SSJam INTO xSSJam FROM ttss WHERE SSNo = xSSNo;
				SELECT IFNULL(GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>'),'')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin IN (SELECT MotorNoMesin FROM tmotor WHERE SSNo = xSSNo) AND JAM > xSSJam	AND NoTrans <> xSSNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(xSSJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
					SET oStatus = 1;
				ELSE
					SET oStatus = 0;				
				END IF;
			END IF;	
			
			#Proses Update
			IF oStatus = 0 THEN #Update
				#Update Utama
				UPDATE ttss
				SET SSNo = xSSNoBaru, SSTgl = xSSTgl, LokasiKode = xLokasiKodeSS, SSMemo = xSSMemo, UserID = xUserID, SSJam = xSSJam, SSNoTerima = xSSNoTerima, DONo = xDONo, NoGL = xNoGL
				WHERE SSNo = @SSNoOLD;
				SET xSSNo = xSSNoBaru;			
				
				#Update nomor FB di SS
				SELECT IFNULL(MAX(FBNo),'--') INTO xFBNo FROM tmotor WHERE SSNo = xSSNo;
				IF xFBNo IS NULL OR xFBNo = '' THEN SET xFBNo = '--'; END IF;			
				UPDATE ttss SET FBNo = xFBNo WHERE SSNo = xSSNo;

				#Update Motor
				UPDATE tmotor SET SSNo = xSSNoBaru WHERE SSNo = @SSNoOLD;							
				UPDATE tmotor SET FBHarga = SSHarga WHERE FBHarga = 0 AND SSNo = xSSNo;
				UPDATE tmotor SET SDHarga = SSHarga WHERE SDHarga = 0 AND SSNo = xSSNo;
				UPDATE tmotor SET SKHarga = SSHarga WHERE SKHarga = 0 AND SSNo = xSSNo;		
				UPDATE tmotor SET RKHarga = SSHarga WHERE RKHarga = 0 AND SSNo = xSSNo;			
				CALL cMotorAutoN('SS',xSSNo,'');
			
				#Update Harga
				UPDATE tmotor JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
				SET tmotor.SSHarga = tdmotortype.MotorHrgBeli, SKHarga = IF(SKHarga = 0,tdmotortype.MotorHrgBeli,SKHarga), SDHarga = IF(SDHarga = 0,tdmotortype.MotorHrgBeli,SDHarga), RKHarga = IF(RKHarga = 0,tdmotortype.MotorHrgBeli,RKHarga) 
				WHERE tmotor.SSHarga = 0 AND SSNo = xSSNo;
				UPDATE tmotor JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
				SET tmotor.FBHarga = tdmotortype.MotorHrgBeli, SKHarga = IF(SKHarga = 0,tdmotortype.MotorHrgBeli,SKHarga), SDHarga = IF(SDHarga = 0,tdmotortype.MotorHrgBeli,SDHarga), RKHarga = IF(RKHarga = 0,tdmotortype.MotorHrgBeli,RKHarga) 
				WHERE tmotor.FBHarga = 0 AND SSNo = xSSNo; 
				
				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @SSNoOLD; #Hapus Jurnal No Transaksi Lama
				CALL jss(xSSNo,xUserID); #Posting Jurnal

				#TvMotorLokasi
				DELETE FROM tvmotorlokasi WHERE NoTrans = @SSNoOLD;
				DELETE FROM tvmotorlokasi WHERE NoTrans = xSSNo;
				INSERT INTO tvmotorlokasi
				SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttss.LokasiKode AS LokasiKode, ttss.SSNo AS NoTrans, ttss.SSJam AS Jam, 'IN' AS Kondisi 
				FROM tmotor  INNER JOIN ttss ON tmotor.SSNo = ttss.SSNo WHERE ttss.SSNo = xSSNo;		
				
				IF @AutoNo = 'Edit' THEN #Edit
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Surat Jalan Supplier','ttss', xSSNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Penerimaan Barang [SS] No: ", xSSNo);
				ELSE #Add
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Surat Jalan Supplier','ttss', xSSNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Penerimaan Barang [SS] No: ", xSSNo);
				END IF;

			ELSEIF oStatus = 1 THEN
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;

			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSSNo;
			IF @GLValid = 'Sudah' THEN	/* Jurnal telah tervalidasi data tidak boleh dihapus */	
				SET oStatus = 1; SET oKeterangan = CONCAT("No SS:", xSSNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Cek Motor Movement
			IF oStatus = 0 THEN
				SELECT SSJam INTO xSSJam FROM ttss WHERE SSNo = xSSNo;
				SELECT GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin IN (SELECT MotorNoMesin FROM tmotor WHERE SSNo = xSSNo) AND JAM > xSSJam	AND NoTrans <> xSSNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(xSSJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
					SET oStatus = 1;
				ELSE
					SET oStatus = 0;				
				END IF;
			END IF;	

			#Cek Motor FB
			IF oStatus = 0 THEN
				SET oKeterangan = "";
				SELECT GROUP_CONCAT(FBNo), GROUP_CONCAT(MotorNoMesin) INTO oKeterangan, @ListMotorNoMesin FROM tmotor WHERE SSNo = xSSNo AND FBNo <> '--' GROUP BY SSNo;
				IF LENGTH(oKeterangan) > 1 THEN
					SET oKeterangan = CONCAT('Motor : ',@ListMotorNoMesin,' telah miliki FB : ', oKeterangan);
					SET oStatus = 1;
				ELSE
					SET oStatus = 0;				
				END IF;				
			END IF;
			
			#Update Proses
			IF oStatus = 0 THEN
				CALL cMotorAutoN('SS',xSSNo,'');
				DELETE FROM ttss WHERE SSNo = xSSNo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xSSNo;
				DELETE FROM tmotor WHERE SSNo = xSSNo;
				DELETE FROM tvmotorlokasi WHERE NoTrans = xSSNo;
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Surat Jalan Supplier','ttss', xSSNo); 
				SET oKeterangan = CONCAT("Berhasil menghapus data Penerimaan Barang [SS] No: ",xSSNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xSSNo) <> 0 THEN
				DELETE FROM ttss WHERE SSNo = xSSNo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xSSNo;
				DELETE FROM tvmotorlokasi WHERE NoTrans = xSSNo;
				SET oKeterangan = CONCAT("Batal menyimpan data Penerimaan Barang [SS] No: ", xSSNoBaru);
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Penerimaan Barang [SS] No: ", xSSNo);
			END IF;
			INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
			(NOW(), xUserID , 'Batal' , 'Surat Jalan Supplier','ttss', REPLACE(xSSNo,'=','-')); 
		END;
		WHEN "Jurnal" THEN
		BEGIN
			IF LOCATE('=',xSSNo) <> 0 THEN /* Data Baru, belum tersimpan tidak boleh di jurnal */
				SET oStatus = 1; 
				SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			ELSE
				CALL jss(xSSNo,xUserID);
				SET oStatus = 0; 
				SET oKeterangan = CONCAT("Berhasil memproses Jurnal Penerimaan Barang [SS] No: ", xSSNo);
			END IF;		
		END;
		WHEN "Print" THEN
		BEGIN
			IF xCetak = "1" THEN
				SELECT ttss.SSNo, ttss.SSTgl, ttss.FBNo, ttss.SupKode, ttss.LokasiKode, ttss.SSMemo, ttss.UserID, IFNULL(tdsupplier.SupNama, '--') AS SupNama, 
				IFNULL(tdlokasi.LokasiNama, '--') AS LokasiNama, IFNULL(ttfb.FBTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS FBTgl, IFNULL(tmotor.SSJum, 0) AS SSJum, 
				ttss.SSJam, ttss.SSNoTerima, ttss.DONo, ttss.NoGL
				FROM ttss 
				LEFT OUTER JOIN ttfb ON ttss.FBNo = ttfb.FBNo 
				LEFT OUTER JOIN tdsupplier ON ttss.SupKode = tdsupplier.SupKode 
				LEFT OUTER JOIN tdlokasi ON ttss.LokasiKode = tdlokasi.LokasiKode 
				LEFT OUTER JOIN (SELECT COUNT(MotorType) AS SSJum, SSNo FROM tmotor tmotor_1 GROUP BY SSNo) tmotor ON ttss.SSNo = tmotor.SSNo
				WHERE (ttss.SSNo = xSSNo);
			END IF;
			IF xCetak = "2" THEN
				SELECT BPKBNo, BPKBTglAmbil, BPKBTglJadi, DKNo, DealerKode, FBHarga, FBNo, FakturAHMNilai, FakturAHMNo, FakturAHMTgl, FakturAHMTglAmbil, 
				MMHarga, MotorAutoN, MotorMemo, MotorNoMesin, MotorNoRangka, MotorTahun, MotorType, MotorWarna, PDNo, PlatNo, PlatTgl, PlatTglAmbil, 
				RKHarga, RKNo, SDHarga, SDNo, SKHarga, SKNo, SSNo, STNKAlamat, STNKNama, STNKNo, STNKTglAmbil, STNKTglBayar, STNKTglJadi, STNKTglMasuk, SSHarga
				FROM tmotor
				WHERE (SSNo = xSSNo)
				ORDER BY FBNo DESC, MotorType, MotorTahun, MotorWarna;
			END IF;
			SET oStatus = 0;SET oKeterangan = CONCAT("Cetak Surat Jalan Supplier No: ", xSSNo);
			INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
			(NOW(), xUserID , 'Cetak' , 'Surat Jalan Supplier','ttss', xSSNo); 
		END;
	END CASE;
END$$

DELIMITER ;