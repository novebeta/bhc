DELIMITER $$

DROP PROCEDURE IF EXISTS `fbphd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fbphd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xBKNo VARCHAR(18),
IN xBKNoBaru VARCHAR(18),
IN xBKTgl DATE,
IN xBKMemo VARCHAR(100),
IN xBKNominal DECIMAL(11,0),
IN xBKJenis VARCHAR(20),
IN xSupKode VARCHAR(10),
IN xBKCekNo VARCHAR(25),
IN xBKCekTempo DATE,
IN xBKPerson VARCHAR(50),
IN xBKAC VARCHAR(25),
IN xNoAccount VARCHAR(10),
IN xBKJam DATETIME,
IN xNoGL VARCHAR(10))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN		
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Daftar Bank Keluar Pengajuan");
		END;
		WHEN "Load" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Load data Bank Keluar Pengajuan No: ", xBKNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SET @AutoNo = CONCAT('BP',RIGHT(YEAR(NOW()),2),LPAD(MONTH(NOW()),2,'0'),LPAD(DAY(NOW()),2,'0'),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(BKNo,3)+1),3,0)) INTO @AutoNo FROM ttbkhd WHERE ((BKNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(BKNo) = 12) ORDER BY BKNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 12 THEN SET @AutoNo = CONCAT('BP',RIGHT(YEAR(NOW()),2),LPAD(MONTH(NOW()),2,'0'),LPAD(DAY(NOW()),2,'0'),'-001');  END IF;
      	SET xBKNo = @AutoNo;
			SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Bank Keluar Pengajuan No: ",xBKNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = '';
			SET @AutoNo = 'Edit';
			SET @BKNoOLD = xBKNo;
			
			IF LOCATE('=',xBKNo) <> 0  THEN 
				SET @AutoNo = CONCAT('BP',RIGHT(YEAR(NOW()),2),LPAD(MONTH(NOW()),2,'0'),LPAD(DAY(NOW()),2,'0'),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(BKNo,3)+1),3,0)) INTO @AutoNo FROM ttbkhd WHERE ((BKNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(BKNo) = 12) ORDER BY BKNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 12 THEN SET @AutoNo = CONCAT('BP',RIGHT(YEAR(NOW()),2),LPAD(MONTH(NOW()),2,'0'),LPAD(DAY(NOW()),2,'0'),'-001');  END IF;
				SET xBKNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit'; 
				SET xBKNoBaru = xBKNo; 
			END IF;
			
			IF oStatus = 0 THEN 
				SELECT SupKode INTO xSupKode FROM tdsupplier WHERE SupStatus = 1;			
				#Update Utama	
				UPDATE ttbkhd
				SET BKNo = xBKNoBaru, BKTgl = xBKTgl, BKMemo = xBKMemo, BKNominal = xBKNominal, BKJenis = xBKJenis, SupKode = xSupKode, BKCekNo = xBKCekNo, BKCekTempo = xBKCekTempo, BKPerson = xBKPerson, BKAC = xBKAC, NoAccount = xNoAccount, BKJam = xBKJam, NoGL = xNoGL
				WHERE BKNo = xBKNo;
				SET xBKNo = xBKNoBaru;

				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID ,	 'Edit' , 'Pengajuan BBN','ttbkhd', xBKNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data Bank Keluar Pengajuan No : ", xBKNo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Pengajuan BBN','ttbkhd', xBKNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menambah data Bank Keluar Pengajuan No : ", xBKNo);
				END IF;			
			ELSEIF oStatus = 1 THEN
				SET oKeterangan = CONCAT("Gagal, silahkan melengkapi data Bank Keluar Pengajuan No: ", xBKNo);
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			DELETE FROM ttbkhd WHERE BKNo = xBKNo;
			DELETE FROM ttbkit WHERE BKNo = xBKNo;
			SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Bank Keluar Pengajuan No: ",xBKNo);
		END;
		WHEN "Cancel" THEN
		BEGIN
			 IF LOCATE('=',xBKNo) <> 0 THEN
				  DELETE FROM ttbkhd WHERE BKNo = xBKNo;
				  SET oKeterangan = CONCAT("Batal menyimpan data Bank Keluar Pengajuan No: ", xBKNoBaru);
			 ELSE
				  SET oKeterangan = CONCAT("Batal menyimpan data Bank Keluar Pengajuan No: ", xBKNo);
			 END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
		END;
		WHEN "Print" THEN
		BEGIN
			IF xCetak = "1" THEN
				SELECT ttbkhd.BKAC, ttbkhd.BKCekNo, ttbkhd.BKCekTempo, ttbkhd.BKJam, ttbkhd.BKJenis, ttbkhd.BKMemo, ttbkhd.BKNo, ttbkhd.BKNominal, ttbkhd.BKPerson, 
				ttbkhd.BKTgl, ttbkhd.NoAccount, ttbkhd.NoGL, ttbkhd.SupKode, ttbkhd.UserID, IFNULL(traccount.NamaAccount, '--') AS NamaAccount, IFNULL(tdsupplier.SupNama, '--') AS SupNama
				FROM ttbkhd 
				LEFT OUTER JOIN tdsupplier ON ttbkhd.SupKode = tdsupplier.SupKode 
				LEFT OUTER JOIN traccount ON ttbkhd.NoAccount = traccount.NoAccount
				WHERE (ttbkhd.BKNo = xBKNo);
			END IF;
			IF xCetak = "2" THEN
				SELECT ttbkit.BKNo, ttbkit.FBNo, ttdk.DKTgl, ttdk.CusKode, tdcustomer.CusNama, 
				CONCAT_WS('', tdcustomer.CusAlamat, '  RT/RW : ', tdcustomer.CusRT, '/', tdcustomer.CusRW) AS CusAlamat, tmotor.MotorType, tmotor.MotorNoMesin, 
            ttdk.LeaseKode, ttdk.BBN, bkPaid.Paid - ttbkit.BKBayar AS Terbayar, ttbkit.BKBayar, ttdk.BBN - bkPaid.Paid AS Sisa, tmotor.MotorTahun
				FROM ttbkit 
				INNER JOIN ttdk ON ttbkit.FBNo = ttdk.DKNo 
				INNER JOIN tmotor ON ttbkit.FBNo = tmotor.DKNo 
				INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
				INNER JOIN ttbkhd ON ttbkit.BKNo = ttbkhd.BKNo 
				INNER JOIN (SELECT FBNo, SUM(BKBayar) AS Paid FROM  ttbkit ttbkit_1 WHERE (BKNo = xBKNo) GROUP BY FBNo) bkPaid ON ttbkit.FBNo = bkPaid.FBNo
				WHERE (ttbkit.BKNo = xBKNo) 
				ORDER BY ttbkit.FBNo;
			END IF;		
			SET oStatus = 0; SET oKeterangan = CONCAT("Cetak data Bank Keluar Pengajuan No: ", xBKNo);
			INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE  
			(NOW(), xUserID , 'Cetak' , CONCAT('Bank Keluar Pengajuan ',xBKJenis),'ttbkhd', xBKNo); 		
		END;
	END CASE;
END$$

DELIMITER ;

