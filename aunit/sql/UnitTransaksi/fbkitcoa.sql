DELIMITER $$

DROP PROCEDURE IF EXISTS `fbkitcoa`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fbkitcoa`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xBKNo VARCHAR(12),
IN xBKAutoN SMALLINT,
IN xNoAccount VARCHAR(10),
IN xBKKeterangan VARCHAR(150),
IN xBKBayar DECIMAL(14,2),
IN xBKAutoNOLD SMALLINT,
IN xNoAccountOLD VARCHAR(10)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN	
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert
			IF oStatus = 0 THEN				
				SELECT IFNULL((MAX(BKAutoN)+1),0) INTO xBKAutoN FROM ttbkitcoa WHERE BKNo = xBKNo; #AutoN
				INSERT INTO ttbkitcoa (BKNo, BKAutoN, NoAccount, BKBayar, BKKeterangan) VALUES (xBKNo, xBKAutoN, xNoAccount, xBKBayar, xBKKeterangan);
				SET oKeterangan = CONCAT('Berhasil menambahkan data Bank Keluar ', xBKNo,' - ', xNoAccount, ' - ', FORMAT(xBKBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert
			IF oStatus = 0 THEN				
				SELECT IFNULL(MAX(BKAutoN)+1,0) INTO xBKAutoN FROM ttbkitcoa WHERE BKNo = xBKNo; #AutoN
				INSERT INTO ttbkitcoa (BKNo, BKAutoN, NoAccount, BKBayar, BKKeterangan) VALUES (xBKNo, xBKAutoN, xNoAccount, xBKBayar, xBKKeterangan);
				SET oKeterangan = CONCAT('Berhasil menambahkan data Bank Keluar ', xBKNo,' - ', xNoAccount, ' - ', FORMAT(xBKBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;	
		END;
		WHEN "Update" THEN
		BEGIN
			 SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Update
			IF oStatus = 0 THEN
				UPDATE ttbkitcoa SET BKNo = xBKNo, BKAutoN = xBKAutoN, NoAccount = xNoAccount, BKBayar = xBKBayar, BKKeterangan = xBKKeterangan
				WHERE BKNo = xBKNoOLD AND BKAutoN = xBKAutoNOLD AND NoAccount = xNoAccountOLD;
				SET oKeterangan = CONCAT("Berhasil mengubah data bank keluar : ", xBKNo , ' - ' ,xNoAccount);
			ELSE
				SET oKeterangan = CONCAT("Gagal mengubah data bank keluar : ", xBKNo , ' - ' ,xNoAccount);
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Delete
			IF oStatus = 0 THEN
				DELETE FROM ttbkitcoa WHERE BKAutoN = xBKAutoN AND NoAccount = xNoAccount AND BKNo = xBKNo;
				SET oKeterangan = CONCAT("Berhasil menghapus data bank keluar : ", xBKNo , ' - ' ,xNoAccount);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 SET oStatus = 0; SET oKeterangan = CONCAT("Batal menyimpan data bank keluar: ", xBKAutoN );
		END;
	END CASE;
END$$

DELIMITER ;

