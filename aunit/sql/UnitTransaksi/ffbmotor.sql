DELIMITER $$

DROP PROCEDURE IF EXISTS `ffbmotor`$$

CREATE DEFINER=`root`@`%` PROCEDURE `ffbmotor`(
    IN xAction VARCHAR(10),
    OUT oStatus INT,
    OUT oKeterangan TEXT,
    IN xUserID VARCHAR(15),
    IN xKodeAkses VARCHAR(15),
    IN xLokasiKode VARCHAR(15),
    IN xFBNo VARCHAR(18),
    IN xMotorType VARCHAR(50),
    IN xMotorTahun DECIMAL(4,0),
    IN xMotorWarna VARCHAR(35),
    IN xMotorNoMesin VARCHAR(25),
    IN xMotorNoRangka VARCHAR(25),
    IN xSSNo VARCHAR(18),
    IN xFBHarga DECIMAL(12,2),
    IN xMotorAutoN SMALLINT(6),
    IN xMotorAutoNOLD SMALLINT(6),
    IN xMotorNoMesinOLD VARCHAR(25)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xFBNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xFBNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
				
			#Cek apakah motor ini sudah pernah ada
			SET @MotorNoMesin = '';
			SELECT MotorNoMesin INTO @MotorNoMesin FROM tmotor WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN ;
			IF @MotorNoMesin <> '' AND LENGTH(xMotorNoMesin) <= 5 THEN #Ketemu dan 5 huruf
				SELECT COUNT(MotorNoMesin) INTO xMotorAutoN FROM tmotor WHERE MotorNoMesin = xMotorNoMesin;
				SET oStatus = 0;
			ELSEIF @MotorNoMesin <> '' AND LENGTH(xMotorNoMesin) > 5 THEN	#Ketemu dan lebih dari 5 huruf
				SET oStatus = 1;
				SET oKeterangan = CONCAT('Terjadi duplikasi data Motor ', xMotorType,' - ', xMotorWarna,' - ',xMotorAutoN, ' - ',xMotorNoMesin);	
			ELSE
				SET oStatus = 0;
			END IF;			

			#Get Harga Beli
			SELECT MotorHrgBeli INTO xFBHarga FROM	tdmotortype WHERE MotorType = xMotorType LIMIT 1;
			
			#Insert Proses 
			IF oStatus = 0 THEN
				SELECT DealerKode INTO @DealerKode FROM tddealer WHERE DealerStatus = '1' LIMIT 1;
				INSERT INTO tmotor
				(MotorAutoN, MotorNoMesin, MotorNoRangka, MotorType, MotorWarna, MotorTahun, MotorMemo, DealerKode, SSNo, FBNo, 	  FBHarga,  SSHarga,  SKHarga,  SDHarga,  RKHarga) VALUES
				(xMotorAutoN, xMotorNoMesin, xMotorNoRangka, xMotorType, xMotorWarna, xMotorTahun, '--', @DealerKode, xSSNo, xFBNo, xFBHarga, xFBHarga, xFBHarga, xFBHarga, xFBHarga);
				SET oKeterangan = CONCAT('Berhasil menambahkan data Motor ', xMotorType,' - ', xMotorWarna,' - ',xMotorAutoN, ' - ',xMotorNoMesin);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Update" THEN
		BEGIN
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xFBNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xFBNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Cek apakah motor ini sudah pernah ada
			SET @MotorNoMesin = '';
			SELECT MotorNoMesin INTO @MotorNoMesin FROM tmotor WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN ;	
			IF @MotorNoMesin <> '' AND LENGTH(xMotorNoMesin) <= 5 THEN #Ketemu dan 5 huruf
				IF (xMotorNoMesin <> xMotorNoMesinOLD)THEN #Beda dengan yg lama, dihitungkan nomor urut baru
					SELECT MAX(MotorAutoN) + 1 INTO xMotorAutoN FROM tmotor WHERE MotorNoMesin = xMotorNoMesin;		
				END IF;
				SET oStatus = 0;
			ELSEIF @MotorNoMesin <> '' AND LENGTH(xMotorNoMesin) > 5 THEN	#Ketemu dan lebih dari 5 huruf
				SET oKeterangan = CONCAT("Gagal mengubah data Motor : ", xMotorType,' - ', xMotorWarna,' - ', xMotorAutoN , ' - ' ,xMotorNoMesin);
			ELSE
				SET oStatus = 0;
			END IF;			
		
			IF oStatus = 0 THEN
				UPDATE tmotor
				SET MotorAutoN = xMotorAutoN, MotorNoMesin = xMotorNoMesin, MotorNoRangka = xMotorNoRangka, MotorType = xMotorType, MotorWarna = xMotorWarna, MotorTahun = xMotorTahun, FBNo = xFBNo, FBHarga = xFBHarga
				WHERE MotorAutoN = xMotorAutoNOLD AND MotorNoMesin = xMotorNoMesinOLD;
				UPDATE tmotor SET FBNo = '--' WHERE MotorNoMesin = xMotorNoMesinOLD AND MotorAutoN = xMotorAutoNOLD;
				CALL cMotorAutoN('FB',xFBNo,xMotorNoMesin);
				SET oKeterangan = CONCAT("Berhasil mengubah data Motor : ", xMotorType,' - ', xMotorWarna,' - ', xMotorAutoN , ' - ' ,xMotorNoMesin);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;

		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xFBNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xFBNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			 /* Jika SSNo Jangan dihapus */
			SET @SSNo = '';
			SELECT SSNo INTO @SSNo FROM tmotor WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN ;
			IF @SSNo = '--' OR @SSNo = '' THEN
				SET oStatus = 0;
			ELSE
				SET oStatus = 1;
				SET oKeterangan = CONCAT("Gagal menghapus data Motor : ",  xMotorType,' - ', xMotorWarna,' - ',xMotorAutoN , ' - ' ,xMotorNoMesin, ' telah memiliki SS ', @SSNo);
			END IF;

			IF oStatus = 0 THEN	 
				DELETE FROM tmotor WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN;
				CALL cMotorAutoN('FB',xFBNo,xMotorNoMesin);
				SET oKeterangan = CONCAT("Berhasil menghapus data Motor : ",  xMotorType,' - ', xMotorWarna,' - ',xMotorAutoN , ' - ' ,xMotorNoMesin);
			ELSE
				UPDATE tmotor SET FBNo = '--' WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN ;
				
				#Update ttss, FBNo
				SET @SSNo = 'XX';
				SELECT SSNo INTO @SSNo FROM tmotor WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN ;
				SET @FBNo = '--';
				SELECT MAX(FBNo) INTO @FBNo FROM tmotor WHERE SSNo = @SSNo ;
				UPDATE ttss SET FBNo	= @FBNo WHERE SSNo = @SSNo;
				
				SET oStatus = 0; #biar pesannya ijo
				SET oKeterangan = CONCAT("Menghapus data Motor dari FB : ",  xMotorType,' - ', xMotorWarna,' - ',xMotorAutoN , ' - ' ,xMotorNoMesin);
			END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
			UPDATE tmotor SET FBNo = xFBNo WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN ;
			SET oKeterangan = CONCAT("FB Berhasil ditambahkan dengan data motor : ",  xMotorType,' - ', xMotorWarna,' - ',xMotorAutoN , ' - ' ,xMotorNoMesin);
		END;
	END CASE;
END$$

DELIMITER ;

