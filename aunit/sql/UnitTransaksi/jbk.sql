DELIMITER $$

DROP PROCEDURE IF EXISTS `jbk`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jbk`(IN xBKNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE oSumBKBayarIT DECIMAL(19,2) DEFAULT 0;
	DECLARE oSumBKBayarCOA DECIMAL(19,2) DEFAULT 0;
   DECLARE MyPesanKu VARCHAR(300) DEFAULT '';
   DECLARE MyPesankuItem VARCHAR(150) DEFAULT '';
	DECLARE oNoUrutKu  DECIMAL(5) DEFAULT 1;
   
	DECLARE My24050200TitipanBBN DECIMAL(19,2) DEFAULT 0;
	DECLARE My11060200PiutangBBN DECIMAL(19,2) DEFAULT 0;
   
	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xBKNo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;
	
	IF LEFT(xBKNo,2) = 'BP' THEN LEAVE sp; END IF;	
	
	SELECT BKNo, BKTgl, BKMemo, BKNominal, BKJenis, SupKode, BKCekNo, BKCekTempo, BKPerson, BKAC, NoAccount, UserID, BKJam, NoGL
	INTO @BKNo, @BKTgl, @BKMemo, @BKNominal, @BKJenis, @SupKode, @BKCekNo, @BKCekTempo, @BKPerson, @BKAC, @NoAccount, @UserID, @BKJam, @NoGL
	FROM ttbkhd	WHERE BKNo = xBKNo;
	SELECT IFNULL(SUM(BKBayar),0) INTO oSumBKBayarIT FROM ttbkit WHERE BKNo = xBKNo;
	SELECT IFNULL(SUM(BKBayar),0) INTO oSumBKBayarCOA FROM ttbkitcoa WHERE BKNo = xBKNo;

   SET MyPesanKu = CONCAT("Post  BK - ",@BKJenis);
   IF @BKPerson <> '--' THEN SET MyPesanKu = CONCAT(MyPesanKu,' - ' , @BKPerson); END IF;
   IF @BKMemo <> "--" THEN SET MyPesanKu = CONCAT(MyPesanKu,' - ' , @BKMemo); END IF; 
   SET MyPesanKu = TRIM(MyPesanKu); 
   IF LENGTH(MyPesanKu) > 300 THEN SET MyPesanKu = SUBSTRING(MyPesanKu,0, 299); END IF;
   SET MyPesankuItem = MyPesanKu ;
   IF LENGTH(MyPesankuItem) > 150 THEN SET MyPesankuItem = SUBSTRING(MyPesankuItem,0, 149); END IF;

	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;

	#ScriptTtglhpit
	DROP TEMPORARY TABLE IF EXISTS ScriptTtglhpit;
	CREATE TEMPORARY TABLE IF NOT EXISTS ScriptTtglhpit AS
	SELECT NoGL, TglGL, GLLink, NoAccount, HPJenis, HPNilai FROM ttglhpit WHERE NoGL = xNoGL;

	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL; DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;DELETE FROM ttglhpit WHERE NoGL = xNoGL;
   DELETE FROM ttgeneralledgerhd WHERE GLLink = xBKNo;
           
	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, @BKTgl, 'BK', MyPesanKu, 0, 0, xBKNo, xUserID, '--', xGLValid);
			
	#Item
	CASE @BKJenis
		WHEN "Umum" THEN
		BEGIN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, BKAutoN, NoAccount,  BKKeterangan, BKBayar, 0 FROM ttbkitcoa WHERE BKNo = xBKNo AND BKBayar >= 0;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, BKAutoN, NoAccount,  BKKeterangan, 0, BKBayar FROM ttbkitcoa WHERE BKNo = xBKNo AND BKBayar < 0;
			
			SET @AutoN = 1;
			SELECT COUNT(NoGL)+1 INTO @AutoN FROM ttgeneralledgerit WHERE NoGL = xNoGL;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, @AutoN,@NoAccount , MyPesankuItem , 0, oSumBKBayarCOA;

			SELECT COUNT(NoGL) INTO @CountNoGL FROM ScriptTtglhpit;
			IF @CountNoGL > 0 THEN
				INSERT INTO ttglhpit SELECT * FROM ScriptTtglhpit;
				DELETE FROM ttglhpit WHERE NoAccount NOT IN (SELECT NoAccount FROM ttgeneralledgerit WHERE NoGL  = xNoGL) AND NoGL  = xNoGL; 
				UPDATE ttgeneralledgerhd SET HPLink = 'Many'  WHERE NoGL = xNoGL; 
			END IF;		
			
			UPDATE ttbkhd SET BKNominal = oSumBKBayarCOA WHERE BKNo = xBKNo;
		END;
		WHEN "Bayar Unit" THEN
		BEGIN
         SELECT SupStatus INTO @SupStatus FROM tdsupplier WHERE SupKode = @SupKode;
			IF @SupStatus = 1 THEN #24010101 Htg Dag Jika Supplier Utama
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, 1, '24010101', MyPesankuItem , oSumBKBayarIT, 0;
			ELSE
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, 1, '24010200', MyPesankuItem , oSumBKBayarIT, 0;
			END IF;
			
			SELECT  GROUP_CONCAT(ttbkit.FBNo), SUM(ttfb.FBPSS), SUM(ttfb.FBPtgLain), SUM(ttfb.FBExtraDisc) INTO @KoleksiFB, @FBPSS, @FBPtgLain, @FBExtraDisc
			FROM ttbkit 
			INNER JOIN ttfb ON ttbkit.FBNo = ttfb.FBNo 
			INNER JOIN ttbkhd ON ttbkit.BKNo = ttbkhd.BKNo 
			WHERE (ttbkit.BKNo = xBKNo);
			
			SET oNoUrutKu = 2;
			IF @FBExtraDisc > 0 THEN #24050700 ExtraDiscount				
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, oNoUrutKu , '24050700', MyPesankuItem, @FBExtraDisc, 0; SET oNoUrutKu = oNoUrutKu + 1;
         END IF;

			SET @PendapatanExtraDiskon = @BKNominal - oSumBKBayarIT- @FBExtraDisc;
			IF @PendapatanExtraDiskon < 0 THEN
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, oNoUrutKu , '40040200', MyPesankuItem, 0, ABS(@PendapatanExtraDiskon); SET oNoUrutKu = oNoUrutKu + 1;
			ELSE
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, oNoUrutKu , '40040200', @PesankuItem , ABS(@PendapatanExtraDiskon), 0; SET oNoUrutKu = oNoUrutKu + 1;
			END IF;
			
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, oNoUrutKu,@NoAccount , MyPesankuItem , 0, @BKNominal;
			
			#Link Hutang Piutang
			DELETE FROM ttglhpit WHERE NoGL = xNoGL;
			IF @SupStatus = 1 THEN #24010101 Htg Dag Jika Supplier Utama
				INSERT INTO ttglhpit SELECT xNoGL, @BKTgl, ttBkit.FBNo, NoAccount, 'Hutang', SUM(KreditGL) AS KreditGL   FROM ttBkit 
				INNER JOIN ttfb ON ttBkit.FBNo = ttfb.FBNo 
				INNER JOIN ttgeneralledgerhd ON ttfb.NoGL = ttgeneralledgerhd.NoGL  
				INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL 
				WHERE BkNo = xBKNo AND NoAccount = '24010101' GROUP BY NoAccount, ttfb.FBNo;
			ELSE
				INSERT INTO ttglhpit SELECT xNoGL, @BKTgl, ttBkit.FBNo, NoAccount, 'Hutang', SUM(KreditGL) AS KreditGL   FROM ttBkit 
				INNER JOIN ttfb ON ttBkit.FBNo = ttfb.FBNo 
				INNER JOIN ttgeneralledgerhd ON ttfb.NoGL = ttgeneralledgerhd.NoGL  
				INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL 
				WHERE BkNo = xBKNo AND NoAccount = '24010200' GROUP BY NoAccount, ttfb.FBNo;
			END IF;
			IF @FBExtraDisc > 0 THEN #24050700 ExtraDiscount				
				INSERT INTO ttglhpit SELECT xNoGL, @BKTgl, ttBkit.FBNo, NoAccount, 'Hutang', SUM(KreditGL) AS KreditGL   FROM ttBkit 
				INNER JOIN ttfb ON ttBkit.FBNo = ttfb.FBNo 
				INNER JOIN ttgeneralledgerhd ON ttfb.NoGL = ttgeneralledgerhd.NoGL  
				INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL 
				WHERE BkNo = xBKNo AND NoAccount = '24050700' GROUP BY NoAccount, ttfb.FBNo;
         END IF;
		END;
		WHEN "Bayar BBN" THEN
		BEGIN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, oNoUrutKu , '24030000', MyPesankuItem , oSumBKBayarIT, 0; SET oNoUrutKu = oNoUrutKu + 1;
			IF (@BKNominal-oSumBKBayarIT) > 0 THEN INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, oNoUrutKu , '60050000', @PesankuItem , @BKNominal-oSumBKBayarIT, 0; SET oNoUrutKu = oNoUrutKu + 1; END IF;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, oNoUrutKu,@NoAccount , MyPesankuItem , 0, @BKNominal;
			
			CALL jbkLinkHutang(xBKNo,@BKTgl,xNoGL,'24030000');					
		END;
		WHEN "BBN Plus" THEN
		BEGIN
			DROP TEMPORARY TABLE IF EXISTS TSearch; CREATE TEMPORARY TABLE TSearch AS
			SELECT KKNo, FBNo, KKBayar, KKJam, IFNULL(KMNo,'--') AS KMNo, IFNULL(KMLink,'--') AS KMLink,
			IFNULL(KMNominal,0) AS KMNominal, IFNULL(KMJam,DATE('2500-01-01')) AS KMJam FROM
			(SELECT ttBKit.BKNo AS KKNo, FBNo, BKBayar AS KKBayar, BKJam AS KKJam FROM ttBKit INNER JOIN ttBKhd ON ttBKhd.BKno = ttBKit.BKNo WHERE ttBKit.BKNo = xBKNo) ttkkit
			LEFT OUTER JOIN
			(SELECT KMNo, KMTgl, SUM(KMNominal) AS KMNominal, KMJenis, KMLink, KMJam, NoGL FROM vtkm WHERE KMJenis = 'BBN Plus' AND KMLINk IN (SELECT FBNo FROM ttBKit WHERE BKNO = xBKNo) GROUP BY KMLink) ttkm
			ON ttkm.KMLink = ttkkit.FBNo;

			SELECT IFNULL(SUM(KMNominal),0), IFNULL(SUM(KKBayar - KMNominal),0) INTO My24050200TitipanBBN, My11060200PiutangBBN FROM TSearch WHERE KKJam > KMJam AND KKBayar > KMNominal;

			IF My24050200TitipanBBN = 0 THEN SELECT IFNULL(SUM(KMNominal),0) INTO My24050200TitipanBBN FROM TSearch WHERE KKJam > KMJam AND KKBayar < KMNominal; END IF;

			IF My11060200PiutangBBN = 0 THEN SELECT IFNULL(SUM(KKBayar),0) INTO My11060200PiutangBBN FROM TSearch WHERE KKJam < KMJam; END IF;

			IF @BKNominal - oSumBKBayarIT > 0 THEN SET My11060200PiutangBBN = My11060200PiutangBBN + @BKNominal - oSumBKBayarIT;  END IF;

			IF My24050200TitipanBBN > 0 THEN
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, oNoUrutKu , '24050200', MyPesankuItem , My24050200TitipanBBN, 0; SET oNoUrutKu = oNoUrutKu + 1;
			END IF;

			IF My11060200PiutangBBN > 0 THEN
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, oNoUrutKu , '11060200', MyPesankuItem , My11060200PiutangBBN, 0; SET oNoUrutKu = oNoUrutKu + 1;			
			END IF;

			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, oNoUrutKu,@NoAccount , MyPesankuItem , 0, @BKNominal;
			
			#Link Hutang Piutang Normal
			DELETE FROM ttglhpit WHERE NoGL = xNoGL;
			DROP TEMPORARY TABLE IF EXISTS TSearchLink; CREATE TEMPORARY TABLE TSearchLink AS
			SELECT BKNo, FBNo, SUM(BKBayar) BKBayar, BKJam, KMNo, KMLink, KMNominal, KMJam, KreditGL, NoGL FROM
			(SELECT ttBKit.BKNo, FBNo, BKBayar, BKJam FROM ttBKit 
				INNER JOIN ttbkhd ON ttbkhd.BKno = ttBKit.BKNo 
				INNER JOIN ttgeneralledgerhd ON ttbkhd.NoGL = ttgeneralledgerhd.NoGL 
				INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL 
				WHERE ttBKit.BKNo = xBKNo AND ttgeneralledgerit.NoAccount = '24050200') ttBKit 
			INNER JOIN 
			(SELECT KMNo, KMTgl, KMNominal, KMJenis, KMLink, KMJam, vtkm.NoGL, (KreditGL+DebetGL) AS KreditGL FROM vtkm 
				INNER JOIN ttgeneralledgerhd ON vtkm.NoGL = ttgeneralledgerhd.NoGL 
				INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL 
				WHERE KMJenis = 'BBN Plus' AND NoAccount = '24050200' AND KMLINk IN (SELECT FBNo FROM ttBKit WHERE BKNO = xBKNo)) ttkm 
			ON ttkm.KMLink = ttBKit.FBNo
			GROUP BY KMNo; 			
			
			INSERT INTO ttglhpit SELECT xNoGL, @BKTgl, KMNo, '24050200','Hutang', KreditGL FROM TSearchLink WHERE BKBayar > KreditGL AND KreditGL > 0;
			INSERT INTO ttglhpit SELECT xNoGL, @BKTgl, KMNo, '24050200','Hutang', BKBayar FROM TSearchLink WHERE BKBayar <= KreditGL AND BKBayar > 0;
			
		END;
		WHEN "BK ke Kas" THEN
		BEGIN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, oNoUrutKu,'11040000' , MyPesankuItem , @BKNominal,0;SET oNoUrutKu = oNoUrutKu + 1;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, oNoUrutKu,@NoAccount , MyPesankuItem , 0, @BKNominal;
		END;
		WHEN "SubsidiDealer2" THEN
		BEGIN
			DROP TEMPORARY TABLE IF EXISTS tvbksk; CREATE TEMPORARY TABLE tvbksk AS
			SELECT ttbkit.BKNo, BKJam, tmotor.SKNo, BKBayar, IFNULL(SKJam,DATE('3000-01-01')) AS SKJam
			FROM ttbkit
			INNER JOIN ttbkhd ON ttbkhd.BKNo = ttbkit.BKNo 
			INNER JOIN tmotor ON ttbkit.FBNo = tmotor.DKNo
			LEFT OUTER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo
			WHERE ttbkit.BKNo = xBKNo;

			SET @row_number = 0; 		
			INSERT INTO ttgeneralledgerit	SELECT xNoGL AS NoGL, @BKTgl AS TglGL, (@row_number:=@row_number + 1) AS GLAutoN,'24040600' , MyPesankuItem  AS KeteranganGL, BKBayar AS DebetGL,0 AS KreditGL FROM tvbksk WHERE SKJam <= BKJam;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL AS NoGL, @BKTgl AS TglGL, (@row_number:=@row_number + 1) AS GLAutoN,'11170300' , MyPesankuItem  AS KeteranganGL, BKBayar AS DebetGL,0 AS KreditGL FROM tvbksk WHERE SKJam > BKJam;		
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, (@row_number:=@row_number + 1),@NoAccount , MyPesankuItem , 0, @BKNominal;
			
			CALL jbkLinkHutang(xBKNo,@BKTgl,xNoGL,'24040600');
		END;
		WHEN "Retur Harga" THEN
		BEGIN
			DROP TEMPORARY TABLE IF EXISTS tvbksk; CREATE TEMPORARY TABLE tvbksk AS
			SELECT ttbkit.BKNo, BKJam, tmotor.SKNo, BKBayar, IFNULL(SKJam,DATE('3000-01-01')) AS SKJam
			FROM ttbkit
			INNER JOIN ttbkhd ON ttbkhd.BKNo = ttbkit.BKNo 
			INNER JOIN tmotor ON ttbkit.FBNo = tmotor.DKNo
			LEFT OUTER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo
			WHERE ttbkit.BKNo = xBKNo;

			SET @row_number = 0; 		
			INSERT INTO ttgeneralledgerit	SELECT xNoGL AS NoGL, @BKTgl AS TglGL, (@row_number:=@row_number + 1) AS GLAutoN,'24040700' , MyPesankuItem  AS KeteranganGL, BKBayar AS DebetGL,0 AS KreditGL FROM tvbksk WHERE SKJam <= BKJam;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL AS NoGL, @BKTgl AS TglGL, (@row_number:=@row_number + 1) AS GLAutoN,'11170400' , MyPesankuItem  AS KeteranganGL, BKBayar AS DebetGL,0 AS KreditGL FROM tvbksk WHERE SKJam > BKJam;		
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, (@row_number:=@row_number + 1),@NoAccount , MyPesankuItem , 0, @BKNominal;
			
			CALL jbkLinkHutang(xBKNo,@BKTgl,xNoGL,'24040700');
		END;
		WHEN "Insentif Sales" THEN
		BEGIN
			DROP TEMPORARY TABLE IF EXISTS tvbksk; CREATE TEMPORARY TABLE tvbksk AS
			SELECT ttbkit.BKNo, BKJam, tmotor.SKNo, BKBayar, IFNULL(SKJam,DATE('3000-01-01')) AS SKJam
			FROM ttbkit
			INNER JOIN ttbkhd ON ttbkhd.BKNo = ttbkit.BKNo 
			INNER JOIN tmotor ON ttbkit.FBNo = tmotor.DKNo
			LEFT OUTER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo
			WHERE ttbkit.BKNo = xBKNo;

			SELECT PerusahaanNama INTO @PerusahaanNama FROM tzcompany LIMIT 1;
	      IF @PerusahaanNama = "CV. ANUGRAH SEJAHTERA BOGOR" 
	      OR @PerusahaanNama = "CV. ANUGERAH SEJAHTERA BOGOR" 
			OR @PerusahaanNama = "CV. KARYA PERDANA INDRAMAYU" 
			OR @PerusahaanNama = "CV. ANUGERAH KIRANA BENGKULU" 
			OR @PerusahaanNama = "CV. ANUGERAH WANGI PAMEKASAN"
			OR @PerusahaanNama = "CV. ANUGERAH SEJATI PROBOLINGGO"
			OR @PerusahaanNama = "CV. ANUGERAH MULIA TOMOHON"  THEN
				SET @row_number = 0; 		
				INSERT INTO ttgeneralledgerit	SELECT xNoGL AS NoGL, @BKTgl AS TglGL, (@row_number:=@row_number + 1) AS GLAutoN,'24051100' , MyPesankuItem  AS KeteranganGL, BKBayar AS DebetGL,0 AS KreditGL FROM tvbksk WHERE SKJam <= BKJam;
				INSERT INTO ttgeneralledgerit	SELECT xNoGL AS NoGL, @BKTgl AS TglGL, (@row_number:=@row_number + 1) AS GLAutoN,'11170500' , MyPesankuItem  AS KeteranganGL, BKBayar AS DebetGL,0 AS KreditGL FROM tvbksk WHERE SKJam > BKJam;		
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, (@row_number:=@row_number + 1),@NoAccount , MyPesankuItem , 0, @BKNominal;
				CALL jbkLinkHutang(xBKNo,@BKTgl,xNoGL,'24051100');
			ELSE
				SET @row_number = 0; 		
				INSERT INTO ttgeneralledgerit	SELECT xNoGL AS NoGL, @BKTgl AS TglGL, (@row_number:=@row_number + 1) AS GLAutoN,'24040400' , MyPesankuItem  AS KeteranganGL, BKBayar AS DebetGL,0 AS KreditGL FROM tvbksk WHERE SKJam <= BKJam;
				INSERT INTO ttgeneralledgerit	SELECT xNoGL AS NoGL, @BKTgl AS TglGL, (@row_number:=@row_number + 1) AS GLAutoN,'11170500' , MyPesankuItem  AS KeteranganGL, BKBayar AS DebetGL,0 AS KreditGL FROM tvbksk WHERE SKJam > BKJam;					
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, (@row_number:=@row_number + 1),@NoAccount , MyPesankuItem , 0, @BKNominal;
				CALL jbkLinkHutang(xBKNo,@BKTgl,xNoGL,'24040400');
			END IF;
		END;
		WHEN "Potongan Khusus" THEN
		BEGIN
			DROP TEMPORARY TABLE IF EXISTS tvbksk; CREATE TEMPORARY TABLE tvbksk AS
			SELECT ttbkit.BKNo, BKJam, tmotor.SKNo, BKBayar, IFNULL(SKJam,DATE('3000-01-01')) AS SKJam
			FROM ttbkit
			INNER JOIN ttbkhd ON ttbkhd.BKNo = ttbkit.BKNo 
			INNER JOIN tmotor ON ttbkit.FBNo = tmotor.DKNo
			LEFT OUTER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo
			WHERE ttbkit.BKNo = xBKNo;

			SELECT PerusahaanNama INTO @PerusahaanNama FROM tzcompany LIMIT 1;
	      IF @PerusahaanNama = "CV. ANUGERAH UTAMA GORONTALO" 
	      OR @PerusahaanNama = "CV. CENDANA MEGAH SENTOSA" THEN
				SET @row_number = 0; 		
				INSERT INTO ttgeneralledgerit	SELECT xNoGL AS NoGL, @BKTgl AS TglGL, (@row_number:=@row_number + 1) AS GLAutoN,'24040100' , MyPesankuItem  AS KeteranganGL, BKBayar AS DebetGL,0 AS KreditGL FROM tvbksk WHERE SKJam <= BKJam;
				INSERT INTO ttgeneralledgerit	SELECT xNoGL AS NoGL, @BKTgl AS TglGL, (@row_number:=@row_number + 1) AS GLAutoN,'30040000' , MyPesankuItem  AS KeteranganGL, BKBayar AS DebetGL,0 AS KreditGL FROM tvbksk WHERE SKJam > BKJam;		
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, (@row_number:=@row_number + 1),@NoAccount , MyPesankuItem , 0, @BKNominal;
				CALL jbkLinkHutang(xBKNo,@BKTgl,xNoGL,'24040100');
			ELSE
				SET @row_number = 0; 		
				INSERT INTO ttgeneralledgerit	SELECT xNoGL AS NoGL, @BKTgl AS TglGL, (@row_number:=@row_number + 1) AS GLAutoN,'24040100' , MyPesankuItem  AS KeteranganGL, BKBayar AS DebetGL,0 AS KreditGL FROM tvbksk WHERE SKJam <= BKJam;
				INSERT INTO ttgeneralledgerit	SELECT xNoGL AS NoGL, @BKTgl AS TglGL, (@row_number:=@row_number + 1) AS GLAutoN,'60380000' , MyPesankuItem  AS KeteranganGL, BKBayar AS DebetGL,0 AS KreditGL FROM tvbksk WHERE SKJam > BKJam;					
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @BKTgl, (@row_number:=@row_number + 1),@NoAccount , MyPesankuItem , 0, @BKNominal;
				CALL jbkLinkHutang(xBKNo,@BKTgl,xNoGL,'24040100');
			END IF;
		END;
		WHEN "Bank Transfer" THEN
		BEGIN
			SET xNoGL = @NoGL;
		END;
	END CASE;

	#Memberi Nilai HPLink
	SET @CountGL = 0;
	SELECT MAX(GLLink), IFNULL(COUNT(NoGL),0) INTO @GLLink, @CountGL FROM ttglhpit WHERE NoGL = xNoGL GROUP BY NoGL;
	IF @CountGL = 0 THEN
		UPDATE ttgeneralledgerhd SET HPLink = '--' WHERE NoGL = xNoGL;
	ELSEIF @CountGL = 1 THEN
		UPDATE ttgeneralledgerhd SET HPLink = @GLLink WHERE NoGL = xNoGL;
	ELSE
		UPDATE ttgeneralledgerhd SET HPLink = 'Many' WHERE NoGL = xNoGL;						
	END IF;	
	
	#Menghitung Total Debet & Kredit
	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttbkhd SET NoGL = xNoGL  WHERE BKNo = xBKNo;
END$$

DELIMITER ;

