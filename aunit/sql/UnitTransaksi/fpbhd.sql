DELIMITER $$
DROP PROCEDURE IF EXISTS `fpbhd`$$
CREATE DEFINER=`root`@`%` PROCEDURE `fpbhd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xPBNo VARCHAR(100),
IN xPBNoBaru VARCHAR(100),
IN xPBTgl DATE,
IN xSMNo VARCHAR(10),
IN xLokasiAsal VARCHAR(15),
IN xLokasiTujuan VARCHAR(15),
IN xPBMemo VARCHAR(100),
IN xPBTotal DECIMAL(11,0),
IN xPBJam VARCHAR(100))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			CALL ctvmotorLokasi('oo',0); #Cek kondisi tvmotorlokasi		

			#Hapus tvmotorlokasi yg belum ada link dengan transaksi PB
			DELETE tvmotorlokasi FROM tvmotorlokasi LEFT OUTER JOIN ttpbhd ON ttpbhd.PBNo = tvmotorlokasi.NoTrans	
			WHERE NoTrans LIKE 'PP%' AND ttpbhd.PBNo IS NULL;

			SET oStatus = 0;
			SET oKeterangan = CONCAT("Daftar Penerimaan Dari Pos");
		END;
		WHEN "Load" THEN
		BEGIN
			SET @JumMotor = 0; SELECT COUNT(MotorNoMesin) INTO @JumMotor FROM ttpbit WHERE PBNo = xPBNo;
			CALL ctvmotorLokasi(xPBNo,@JumMotor);
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Load Penerimaan Dari Pos No: ",xPBNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			 SET @AutoNo = CONCAT('PP',RIGHT(YEAR(NOW()),2),'-');
			 SELECT CONCAT(@AutoNo,LPAD((RIGHT(PBNo,5)+1),5,0)) INTO @AutoNo FROM ttpbhd WHERE ((PBNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(PBNo) = 10) ORDER BY PBNo DESC LIMIT 1;
			 IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('PP',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			 SET xPBNo = @AutoNo;
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Tambah data baru Penerimaan Dari Pos No: ",xPBNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = '';
			SET @AutoNo = 'Edit';
			SET @PBNoOLD = xPBNo;
			
			IF LOCATE('=',xPBNo) <> 0  THEN 
				SET @AutoNo = CONCAT('PP',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(PBNo,5)+1),5,0)) INTO @AutoNo FROM ttpbhd WHERE ((PBNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(PBNo) = 10) ORDER BY PBNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('PP',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xPBNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit'; 
				SET xPBNoBaru = xPBNo;
			END IF;

			# Cek Jam Transaksi
			IF oStatus = 0 THEN 		
				IF @AutoNo = 'Edit' THEN
					SELECT PBJam INTO @PBJamOLD FROM ttpbhd WHERE PBNo = @PBNoOLD;
					IF xPBJam < @PBJamOLD THEN
						SET oStatus = 1;
						SET oKeterangan = CONCAT(oKeterangan,"Gagal menyimpan data, Tgl-Jam ", DATE_FORMAT(xPBJam,'%d-%m-%Y %k:%i:%s'), ' tidak boleh lebih kecil dari Tgl-Jam semula ',DATE_FORMAT(@PBNoOLD ,'%d-%m-%Y %k:%i:%s'));
					ELSE
						SET oStatus = 0;
					END IF;
				END IF;
			END IF;
			
			#Cek Motor Movement (motor sudah berpindah [jam trans > jam pb])
			IF oStatus = 0 THEN
				SELECT PBJam INTO xPBJam FROM ttPBhd WHERE PBNo = xPBNo;
				SELECT IFNULL(GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>'),'')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin IN (SELECT MotorNoMesin FROM ttpbit WHERE PBNo = xPBNo) AND JAM > xPBJam	AND NoTrans <> xPBNo	ORDER BY MotorNoMesin, JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(xPBJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
				ELSE
					SET oStatus = 0;				
					SET oKeterangan = '';					
				END IF;
			END IF;
			
			IF oStatus = 0 THEN /* Update */
				#Update Utama				
				UPDATE ttpbhd
				SET PBNo = xPBNoBaru, PBTgl = xPBTgl, SMNo = xSMNo, LokasiAsal = xLokasiAsal, LokasiTujuan = xLokasiTujuan, PBMemo = xPBMemo, UserID = xUserID, PBJam = xPBJam, PBTotal = xPBTotal
				WHERE PBNo = xPBNo;
				SET xPBNo = xPBNoBaru;
				
				#Hitung SubTotal 
				SET @PBTotal = 0;
				SELECT IFNULL(SUM(FBHarga),0) INTO @PBTotal FROM ttpbit 
				INNER JOIN tmotor  ON  tmotor.MotorAutoN = ttpbit.MotorAutoN AND tmotor.MotorNoMesin = ttpbit.MotorNoMesin WHERE PBNo = xPBNo;
				UPDATE ttpbhd	SET PBTotal = @PBTotal	WHERE PBNo = xPBNo;

				#tvmotorlokasi
				DELETE FROM tvmotorlokasi WHERE NoTrans = @PBNoOLD;
				DELETE FROM tvmotorlokasi WHERE NoTrans = xPBNo;
				INSERT INTO tvmotorlokasi
				SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttpbhd.LokasiAsal AS LokasiKode, ttpbit.PBNo AS NoTrans, ttpbhd.PBJam AS Jam, 'IN' AS Kondisi 
				FROM tmotor 
				INNER JOIN ttpbit ON tmotor.MotorNoMesin = ttpbit.MotorNoMesin AND tmotor.MotorAutoN = ttpbit.MotorAutoN 
				INNER JOIN ttpbhd ON ttpbit.PBNo = ttpbhd.PBNo WHERE ttpbhd.PBNo = xPBNo;
				
				IF @AutoNo = 'Edit' THEN /*Edit*/
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID ,	 'Edit' , 'Penerimaan dari POS','ttpbhd', xSMNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data Penerimaan dari POS No : ", xSMNo);
				ELSE /*Add*/
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Penerimaan dari POS','ttpbhd', xSMNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menambah data Penerimaan dari POS No : ", xSMNo);
				END IF;
				
				SET oKeterangan = CONCAT("Berhasil menyimpan data Penerimaan Dari Pos No: ", xPBNo);
			ELSEIF oStatus = 1 THEN
				SET oKeterangan = oKeterangan;
			END IF;
			
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = '';
			SET oStatus = 0;

			#Cek Motor Movement (motor sudah berpindah [jam trans > jam pb])
			IF oStatus = 0 THEN
				SELECT PBJam INTO xPBJam FROM ttPBhd WHERE PBNo = xPBNo;
				SELECT IFNULL(GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>'),'')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin IN (SELECT MotorNoMesin FROM ttpbit WHERE PBNo = xPBNo) AND JAM > xPBJam	AND NoTrans <> xPBNo	ORDER BY MotorNoMesin, JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(xPBJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
					SET oStatus = 1;
				ELSE
					SET oStatus = 0;				
				END IF;
			END IF;
			
			#Update Proses
			IF oStatus = 0 THEN
				DELETE FROM ttpbhd WHERE PBNo = xPBNo;
				DELETE FROM ttpbit WHERE PBNo = xPBNo;
				DELETE FROM tvmotorlokasi WHERE NoTrans = xPBNo;
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Penerimaan dari POS','ttpbhd', xPBNo); 
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Penerimaan Dari Pos No: ",xPBNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;			
		END;
		WHEN "Loop" THEN
		BEGIN
			UPDATE ttpbhd SET SMNo = xSMNo, LokasiAsal = xLokasiAsal, LokasiTujuan = xLokasiTujuan WHERE PBNo = xPBNo;
			DELETE FROM ttpbit WHERE PBNo = xPBNo;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 IF LOCATE('=',xPBNo) <> 0 THEN
				  DELETE FROM ttpbhd WHERE PBNo = xPBNo;
				  SET oKeterangan = CONCAT("Batal menyimpan data Penerimaan Dari Pos No: ", xPBNoBaru);
			 ELSE
				  SET oKeterangan = CONCAT("Batal menyimpan data Penerimaan Dari Pos No: ", xPBNo);
			 END IF;
		END;

		WHEN "Print" THEN
		BEGIN
			IF xCetak = "1" THEN
				SELECT ttpbhd.LokasiAsal, ttpbhd.LokasiTujuan, ttpbhd.PBMemo, ttpbhd.PBNo, ttpbhd.PBTgl, ttpbhd.PBTotal, ttpbhd.SMNo, ttpbhd.UserID, 
				IFNULL(tdlokasi.LokasiNama, '--') AS LokasiNamaAsal, IFNULL(tdlokasi_1.LokasiNama, '--') AS LokasiNamaTujuan, 
				IFNULL(ttsmhd.SMTgl, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS SMTgl, ttpbhd.PBJam
				FROM ttpbhd 
				LEFT OUTER JOIN ttsmhd ON ttpbhd.SMNo = ttsmhd.SMNo 
				LEFT OUTER JOIN tdlokasi ON ttpbhd.LokasiAsal = tdlokasi.LokasiKode 
				LEFT OUTER JOIN tdlokasi tdlokasi_1 ON ttpbhd.LokasiTujuan = tdlokasi_1.LokasiKode
				WHERE (ttpbhd.PBNo = xPBNo);
			END IF;
			IF xCetak = "2" THEN
				SELECT ttpbit.PBNo, ttpbit.MotorNoMesin, ttpbit.MotorAutoN, tmotor.MotorType, tmotor.MotorTahun, tmotor.MotorWarna, tmotor.MotorNoRangka, tmotor.FBHarga, tdmotortype.MotorNama
				FROM ttpbit 
				INNER JOIN tmotor ON ttpbit.MotorNoMesin = tmotor.MotorNoMesin AND ttpbit.MotorAutoN = tmotor.MotorAutoN 
				LEFT OUTER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
				WHERE (ttpbit.PBNo = xPBNo);
			END IF;
			
			SET oStatus = 0; SET oKeterangan = CONCAT("Cetak data Penerimaan Dari Pos No: ", xPBNo);
			INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE  
			(NOW(), xUserID , 'Cetak' , CONCAT('Penerimaan Dari Pos'),'ttpbd', xPBNo); 
		END;
	END CASE;
END$$

DELIMITER ;

