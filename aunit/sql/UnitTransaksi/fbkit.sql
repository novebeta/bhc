DELIMITER $$
DROP PROCEDURE IF EXISTS `fbkit`$$
CREATE DEFINER=`root`@`%` PROCEDURE `fbkit`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xBKNo VARCHAR(12),
IN xFBNo VARCHAR(18),
IN xBKBayar DECIMAL(14,2),
IN xBKNoOLD VARCHAR(12),
IN xFBNoOLD VARCHAR(18)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN	
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert
			IF oStatus = 0 THEN		
			INSERT INTO ttbkit (BKNo, FBNo, BKBayar) VALUES	(xBKNo, xFBNo, xBKBayar);
				SET oKeterangan = CONCAT('Berhasil menambahkan data Bank Keluar ', xBKNo,' - ', xFBNo, ' - ', FORMAT(xBKBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert
			IF oStatus = 0 THEN		
			INSERT INTO ttbkit (BKNo, FBNo, BKBayar) VALUES	(xBKNo, xFBNo, xBKBayar);
				SET oKeterangan = CONCAT('Berhasil menambahkan data Bank Keluar ', xBKNo,' - ', xFBNo, ' - ', FORMAT(xBKBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;		
		END;
		WHEN "Update" THEN
		BEGIN
			 SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Update
			IF oStatus = 0 THEN
				UPDATE ttbkit SET BKNo = xBKNo, FBNo = xFBNo, BKBayar = xBKBayar  WHERE BKNo = xBKNoOLD AND FBNo = xFBNoOLD;
				SET oKeterangan = CONCAT("Berhasil mengubah data bank keluar : ", xBKNo , ' - ' ,xFBNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Delete
			IF oStatus = 0 THEN
				DELETE FROM ttbkit WHERE FBNo = xFBNo AND BKNo = xBKNo;
				SET oKeterangan = CONCAT("Berhasil menghapus data bank keluar : ", xBKNo , ' - ' ,xFBNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
	END CASE;
END$$

DELIMITER ;

