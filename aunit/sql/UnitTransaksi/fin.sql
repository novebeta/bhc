DELIMITER $$
DROP PROCEDURE IF EXISTS `fin`$$
CREATE DEFINER=`root`@`%` PROCEDURE `fin`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan VARCHAR(150),
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xINNo VARCHAR(10),
IN xINNoBaru VARCHAR(10),
IN xSalesKode VARCHAR(10),
IN xDKNo VARCHAR(10),
IN xINTgl DATE,
IN xLeaseKode VARCHAR(10),
IN xCusNama VARCHAR(50),
IN xCusKode VARCHAR(10),
IN xCusKTP VARCHAR(18),
IN xCusKabupaten VARCHAR(50),
IN xCusAlamat VARCHAR(50),
IN xCusRT VARCHAR(3),
IN xCusRW VARCHAR(3),
IN xCusKecamatan VARCHAR(50),
IN xCusTelepon VARCHAR(30),
IN xCusStatusHP VARCHAR(10),
IN xCusKodePos VARCHAR(7),
IN xCusKelurahan VARCHAR(50),
IN xCusEmail VARCHAR(50),
IN xCusStatusRumah VARCHAR(15),
IN xCusKodeKons VARCHAR(35),
IN xCusSex VARCHAR(12),
IN xCusTempatLhr VARCHAR(30),
IN xCusTglLhr DATE,
IN xCusAgama VARCHAR(15),
IN xCusPekerjaan VARCHAR(35),
IN xCusPendidikan VARCHAR(20),
IN xCusPengeluaran VARCHAR(35),
IN xCusKK VARCHAR(16),
IN xCusNPWP VARCHAR(20),
IN xCusTelepon2 VARCHAR(30),
IN xMotorType VARCHAR(50),
IN xMotorWarna VARCHAR(35),
IN xINHarga VARCHAR(11),
IN xINMemo VARCHAR(100),
IN xINDP VARCHAR(11),
IN xINJenis VARCHAR(6))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Daftar Inden [IN]");
		END;
		WHEN "Load" THEN
		BEGIN
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Load data Inden [IN] No: ",xINNo);		 
		END;
		WHEN "Insert" THEN
		BEGIN
			SET @AutoNo = CONCAT('IN',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(INNo,5)+1),5,0)) INTO @AutoNo FROM ttin WHERE ((INNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(INNo) = 10) ORDER BY INNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('IN',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xINNo = @AutoNo;
			SET oStatus = 0; SET oKeterangan = CONCAT("Tambah data baru Inden [IN] No: ",xINNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = '';
			SET @AutoNo = 'Edit';
			SET @INNoOLD = xINNo;
			
			IF LOCATE('=',xINNo) <> 0  THEN 
				SET @AutoNo = CONCAT('IN',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(INNo,5)+1),5,0)) INTO @AutoNo FROM ttin WHERE ((INNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(INNo) = 10) ORDER BY INNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('IN',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xINNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit'; 
				SET xINNoBaru = xINNo;
			END IF;
					
			IF oStatus = 0 THEN		
				#Buat Data Konsumen
				IF xCusKode = '--' THEN
					SET @CusKode = CONCAT('KS',RIGHT(YEAR(NOW()),2),'-');
					SELECT CONCAT(@CusKode,LPAD((RIGHT(CusKode,5)+1),5,0)) INTO @CusKode FROM tdcustomer WHERE ((CusKode LIKE CONCAT(@CusKode,'%')) AND LENGTH(CusKode) = 10) ORDER BY CusKode DESC LIMIT 1;
					IF LENGTH(@CusKode) <> 10 THEN SET @CusKode = CONCAT('IN',RIGHT(YEAR(NOW()),2),'-00001');  END IF;				
					
					SELECT Provinsi INTO @Provinsi FROM tdarea WHERE Kabupaten = xCusKabupaten LIMIT 1;								

					INSERT INTO tdcustomer (CusKode, CusKTP, CusNama, CusAlamat, CusRT, CusRW, 
					CusProvinsi, CusKabupaten, CusKecamatan, CusKelurahan, CusKodePos,
					CusTelepon, CusSex,CusTempatLhr, CusTglLhr, CusAgama, CusPekerjaan, CusPendidikan, CusPengeluaran, 
					CusTglBeli, CusKeterangan, CusEmail, CusKodeKons, CusStatusHP, CusStatusRumah, 
					CusPembeliNama, CusPembeliKTP, CusPembeliAlamat, 
					CusKK, CusTelepon2, CusNPWP)
					VALUES (@CusKode, xCusKTP, fcapitalize(xCusNama), fcapitalize(xCusAlamat), xCusRT, xCusRW, 
					@Provinsi, xCusKabupaten, xCusKecamatan, xCusKelurahan, xCusKodePos, 
					xCusTelepon, xCusSex, xCusTempatLhr, xCusTglLhr, xCusAgama, xCusPekerjaan, xCusPendidikan, xCusPengeluaran, 
					'1900-01-01', '--', xCusEmail, xCusKodeKons, xCusStatusHP, xCusStatusRumah, 
					'--', '--', '--', 
					xCusKK, xCusTelepon2, xCusNPWP);
					
					SET xCusKode = @CusKode;
					UPDATE ttin SET CusKode = xCusKode WHERE INNo = xINNo;
				ELSE #Update Data Konsumen
					SELECT Provinsi INTO @Provinsi FROM tdarea WHERE Kabupaten = xCusKabupaten LIMIT 1;
				   UPDATE tdcustomer SET 
				   CusKTP = xCusKTP, CusNama = fcapitalize(xCusNama), CusAlamat = fcapitalize(xCusAlamat), CusRT = xCusRT, CusRW = xCusRW, 
				   CusProvinsi = @Provinsi, CusKabupaten = xCusKabupaten, CusKecamatan = xCusKecamatan, CusKelurahan = xCusKelurahan, CusKodePos = xCusKodePos, 
				   CusTelepon = xCusTelepon, CusSex = xCusSex, CusTempatLhr = xCusTempatLhr, CusTglLhr = xCusTglLhr, CusAgama = xCusAgama, 
				   CusPekerjaan = xCusPekerjaan, CusPendidikan = xCusPendidikan, CusPengeluaran = xCusPengeluaran,  CusEmail = xCusEmail, 
				   CusKodeKons = xCusKodeKons, CusStatusHP = xCusStatusHP, CusStatusRumah = xCusStatusRumah, 
				   CusKK = xCusKK, CusTelepon2 = xCusTelepon2, CusNPWP = xCusNPWP
					WHERE CusKode = xCusKode;
				END IF;

				UPDATE ttin
				SET INNo = xINNoBaru, INTgl = xINTgl, SalesKode = xSalesKode, CusKode = xCusKode, LeaseKode = xLeaseKode, INJenis = xINJenis, INMemo = xINMemo, 
				INDP = xINDP, UserID = xUserID, INHarga = xINHarga, MotorType = xMotorType, MotorWarna = xMotorWarna, DKNo = xDKNo
				WHERE INNo = @INNoOLD;
				SET xINNo = xINNoBaru;
								
				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID ,	 'Edit' , 'Inden Penjualan','ttin', xINNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data Inden No : ", xINNo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Inden Penjualan','ttin', xINNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menambah data Inden No : ", xINNo);
				END IF;
			ELSEIF oStatus = 1 THEN
				SET oKeterangan = CONCAT("Gagal, silahkan melengkapi data Inden No: ", xINNo);			
			END IF;		

		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = '';
			SET oStatus = 0;

			SELECT DKNo INTO @DKNo FROM ttin WHERE INNo = xINNo;
			IF @DKNo IS NOT NULL AND @DKNo <> '--' THEN
				SET oStatus = 1;SET oKeterangan = CONCAT("Sudah memiliki DK No: ",@DKNo);
			END IF;
			
			IF oStatus = 0 THEN
				DELETE FROM ttIN WHERE INNo = xINNo;
				SET oStatus = 1; SET oKeterangan = CONCAT("Berhasil menghapus data Inden [IN] No: ",xINNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 IF LOCATE('=',xINNo) <> 0 THEN
				  DELETE FROM ttin WHERE INNo = xINNo;
				  SET oKeterangan = CONCAT("Batal menyimpan data Inden [IN] No: ", xINNoBaru);
			 ELSE
				  SET oKeterangan = CONCAT("Batal menyimpan data Inden [IN] No: ", xINNo);
			 END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
			SET oStatus = 0; 
			SET oKeterangan = CONCAT("Update Konsumen");
			UPDATE ttin SET CusKode = xCusKode WHERE INNo = xINNo;
			
		END;
		WHEN "Print" THEN
		BEGIN
			IF xCetak = "1" THEN
				SELECT  ttin.INNo, ttin.INTgl, ttin.SalesKode, ttin.CusKode, ttin.LeaseKode, ttin.INJenis, ttin.INMemo, ttin.INDP, ttin.UserID, IFNULL(tdsales.SalesNama, '--') AS SalesNama, IFNULL(tdleasing.LeaseNama, '--') AS LeaseNama, ttin.INHarga, 
				ttin.MotorType, ttin.MotorWarna, ttin.DKNo, IFNULL(tdcustomer.CusKTP, '--') AS CusKTP, IFNULL(tdcustomer.CusNama, '--') AS CusNama, IFNULL(tdcustomer.CusAlamat, '--') AS CusAlamat, IFNULL(tdcustomer.CusRT, '--') 
				AS CusRT, IFNULL(tdcustomer.CusRW, '--') AS CusRW, IFNULL(tdcustomer.CusProvinsi, '--') AS CusProvinsi, IFNULL(tdcustomer.CusKabupaten, '') AS CusKabupaten, IFNULL(tdcustomer.CusKecamatan, '--') AS CusKecamatan, 
				IFNULL(tdcustomer.CusKelurahan, '--') AS CusKelurahan, IFNULL(tdcustomer.CusKodePos, '--') AS CusKodePos, IFNULL(tdcustomer.CusTelepon, '--') AS CusTelepon, IFNULL(tdcustomer.CusSex, '--') AS CusSex, 
				IFNULL(tdcustomer.CusTempatLhr, '--') AS CusTempatLhr, IFNULL(tdcustomer.CusTglLhr, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS CusTglLhr, IFNULL(tdcustomer.CusAgama, '--') AS CusAgama, 
				IFNULL(tdcustomer.CusPekerjaan, '--') AS CusPekerjaan, IFNULL(tdcustomer.CusPendidikan, '--') AS CusPendidikan, IFNULL(tdcustomer.CusPengeluaran, '--') AS CusPengeluaran, IFNULL(tdcustomer.CusEmail, '--') AS CusEmail, 
				IFNULL(tdcustomer.CusKodeKons, '--') AS CusKodeKons, IFNULL(tdcustomer.CusStatusHP, '--') AS CusStatusHP, IFNULL(tdcustomer.CusStatusRumah, '--') AS CusStatusRumah, tdcustomer.CusTelepon2
				FROM ttin 
				LEFT OUTER JOIN tdleasing ON ttin.LeaseKode = tdleasing.LeaseKode 
				LEFT OUTER JOIN tdsales ON ttin.SalesKode = tdsales.SalesKode 
				LEFT OUTER JOIN tdcustomer ON ttin.CusKode = tdcustomer.CusKode 
				LEFT OUTER JOIN ttdk ON ttin.INNo = ttdk.INNo
				WHERE (ttin.INNo = xINNo);
			END IF;
			 
			SET oStatus = 0;SET oKeterangan = CONCAT("Cetak data Inden [IN] No: ", xABNo);		
			INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
			(NOW(), xUserID , 'Cetak' , 'Inden','ttin', xABNo); 
		END;
	END CASE;
END$$

DELIMITER ;

