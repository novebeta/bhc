DELIMITER $$

DROP PROCEDURE IF EXISTS `fsd`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fsd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan VARCHAR(150),
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xSDNo VARCHAR(10),
IN xSDNoBaru VARCHAR(10),
IN xSDTgl DATE,
IN xSDJam DATETIME,
IN xSupKode VARCHAR(10),
IN xLokasiKodeSD VARCHAR(15),
IN xSDMemo VARCHAR(100),
IN xSDTotal DECIMAL(14,2),
IN xNoGL VARCHAR(10),
IN xCetak VARCHAR(5),
IN xKodeTrans VARCHAR(2))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			SET oStatus = 0;
			CALL ctvmotorLokasi('oo',0); #Cek kondisi tvmotorlokasi		
			IF xKodeTrans = 'SR' THEN
				#Hapus tvmotorlokasi yg belum ada link dengan transaksi SR
				DELETE tvmotorlokasi FROM tvmotorlokasi
				LEFT OUTER JOIN ttsd ON ttsd.SDNO = tvmotorlokasi.NoTrans
				WHERE NoTrans LIKE 'SR%' AND ttsd.SDNO IS NULL;

				#ReJurnal				
				cek01: BEGIN 
					DECLARE rSDNo VARCHAR(100) DEFAULT '';
					DECLARE done INT DEFAULT FALSE;
					DECLARE cur1 CURSOR FOR 
					SELECT SDNo FROM ttsd
					LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttsd.SDNo 
					WHERE SDNo LIKE 'SR%' AND ttgeneralledgerhd.NoGL IS NULL AND SDTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
					DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
					OPEN cur1;
					REPEAT FETCH cur1 INTO rSDNo;
						IF rSDNo IS NOT NULL AND rSDNo <> '' THEN
							CALL jsr(rSDNo, 'System');
						END IF;
					UNTIL done END REPEAT;
					CLOSE cur1;
				END cek01;
				SET oStatus = 0;
				SET oKeterangan = CONCAT("Daftar Retur Beli [SR]");
			ELSEIF xKodeTrans = 'IR' THEN
				SET oKeterangan = CONCAT("Daftar Retur Beli [IR]");	
			ELSEIF xKodeTrans = 'ME' THEN
				#Hapus tvmotorlokasi yg belum ada link dengan transaksi ME
				DELETE tvmotorlokasi FROM tvmotorlokasi
				LEFT OUTER JOIN ttsd ON ttsd.SDNO = tvmotorlokasi.NoTrans
				WHERE NoTrans LIKE 'ME%' AND ttsd.SDNO IS NULL;

				cek01: BEGIN 
					DECLARE rSDNo VARCHAR(100) DEFAULT '';
					DECLARE done INT DEFAULT FALSE;
					DECLARE cur1 CURSOR FOR 
					SELECT SDNo FROM ttsd
					LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttsd.SDNo 
					WHERE SDNo LIKE 'ME%' AND ttgeneralledgerhd.NoGL IS NULL AND SDTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
					DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
					OPEN cur1;
					REPEAT FETCH cur1 INTO rSDNo;
						IF rSDNo IS NOT NULL AND rSDNo <> '' THEN
							CALL jsd(rSDNo, 'System');
						END IF;
					UNTIL done END REPEAT;
					CLOSE cur1;
				END cek01;
				SET oKeterangan = CONCAT("Daftar Mutasi Eksternal [ME]");			
			ELSEIF xKodeTrans = 'IE' THEN
				SET oKeterangan = CONCAT("Daftar Invoice Mutasi Eksternal [IE]");			
			END IF;
		END;
		WHEN "Load" THEN
		BEGIN
			SET oStatus = 0;
			SET @JumMotor = 0; SELECT COUNT(MotorNoMesin) INTO @JumMotor FROM tmotor WHERE SDNo = xSDNo;
			CALL ctvmotorLokasi(xSDNo,@JumMotor);
			IF xKodeTrans = 'SR' THEN
				SET oKeterangan = CONCAT("Load Data Retur Beli [SR] No : ",xSDNo);	
			ELSEIF xKodeTrans = 'IR' THEN
				SET oKeterangan = CONCAT("Load Data Retur Beli [IR] No : ",xSDNo);	
			ELSEIF xKodeTrans = 'ME' THEN
				SET oKeterangan = CONCAT("Load Data Mutasi Eksternal [ME] No : ",xSDNo);			
			ELSEIF xKodeTrans = 'IE' THEN
				SET oKeterangan = CONCAT("Load Data Invoice Mutasi Eksternal [IE] No : ",xSDNo);			
			END IF;
		END;
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;
			IF xKodeTrans = 'SR' THEN
				SET @AutoNo = CONCAT('SR',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(SDNo,5)+1),5,0)) INTO @AutoNo FROM ttsd WHERE ((SDNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(SDNo) = 10) ORDER BY SDNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('SR',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xSDNo = @AutoNo;
				SET oKeterangan = CONCAT("Tambah data baru Retur Beli [SR] No: ",@AutoNo);
			ELSEIF xKodeTrans = 'ME' THEN
				SET @AutoNo = CONCAT('ME',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(SDNo,5)+1),5,0)) INTO @AutoNo FROM ttsd WHERE ((SDNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(SDNo) = 10) ORDER BY SDNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('ME',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xSDNo = @AutoNo;
				SET oKeterangan = CONCAT("Tambah data baru Mutasi Eksternal [ME] No: ",@AutoNo);
			END IF;
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = '';
			SET @AutoNo = 'Edit';
			SET @SDNoOLD = xSDNo;
			
			#Auto Number [ Insert vs Edit ]
			IF xKodeTrans = 'SR' OR xKodeTrans = 'IR'  THEN
				IF LOCATE('=',xSDNo) <> 0  AND LENGTH(xSDNoBaru) = 10 THEN #Generate new auto number 
					SET @AutoNo = CONCAT('SR',RIGHT(YEAR(NOW()),2),'-');
					SELECT CONCAT(@AutoNo,LPAD((RIGHT(SDNo,5)+1),5,0)) INTO @AutoNo FROM ttsd WHERE ((SDNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(SDNo) = 10) ORDER BY SDNo DESC LIMIT 1;
					IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('SR',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
					SET xSDNoBaru = @AutoNo;
				ELSE
					SET @AutoNo = 'Edit'; 
					SET xSDNoBaru = xSDNo;
				END IF;
			ELSEIF xKodeTrans = 'ME' OR xKodeTrans = 'IE' THEN
				IF LOCATE('=',xSDNo) <> 0  AND LENGTH(xSDNoBaru) = 10 THEN #Generate new auto number
				  SET @AutoNo = CONCAT('ME',RIGHT(YEAR(NOW()),2),'-');
				  SELECT CONCAT(@AutoNo,LPAD((RIGHT(SDNo,5)+1),5,0)) INTO @AutoNo FROM ttsd WHERE ((SDNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(SDNo) = 10) ORDER BY SDNo DESC LIMIT 1;
				  IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('ME',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				  SET xSDNoBaru = @AutoNo;
				ELSE
					SET @AutoNo = 'Edit'; 
				END IF;
			END IF;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSDNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSDNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			# Cek Jam Transaksi
			IF oStatus = 0 THEN 		
				IF @AutoNo = 'Edit' THEN
					SELECT SDJam INTO @SDJamOLD FROM ttsd WHERE SDNo = @SDNoOLD;
					IF xSDJam < @SDJamOLD THEN
						SET oStatus = 1;
						SET oKeterangan = CONCAT(oKeterangan,"Gagal menyimpan data, Tgl-Jam ", DATE_FORMAT(xSDJam,'%d-%m-%Y %k:%i:%s'), ' tidak boleh lebih kecil dari Tgl-Jam semula ',DATE_FORMAT(@SDJamOLD ,'%d-%m-%Y %k:%i:%s'));
					ELSE
						SET oStatus = 0;
					END IF;
				END IF;
			END IF;

			#Cek Motor Movement
			IF oStatus = 0 THEN
				SELECT SDJam INTO xSDJam FROM ttsd WHERE SDNo = xSDNo;
				SELECT IFNULL(GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>'),'')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin IN (SELECT MotorNoMesin FROM tmotor WHERE SDNo = xSDNo) AND JAM > xSDJam	AND NoTrans <> xSDNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(xSDJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
				ELSE
					SET oStatus = 0;				
					SET oKeterangan = '';
				END IF;
			END IF;
					
			#Update Proses
			IF oStatus = 0 THEN 
				#Update Utama		
				UPDATE ttsd
				SET SDNo = xSDNoBaru, SDTgl = xSDTgl, LokasiKode = xLokasiKodeSD, DealerKode = xSupKode, SDMemo = xSDMemo, SDTotal = xSDTotal, UserID = xUserID, SDJam = xSDJam, NoGL = xNoGL
				WHERE SDNo = @SDNoOLD;
				SET xSDNo = xSDNoBaru;	#IN OUT send to PHP

				#Update Motor
				UPDATE tmotor SET SDNo = xSDNoBaru WHERE SDNo = @SDNoOLD;							
				
				#Hitung SubTotal 
				SET @SDTotal = 0;
				SELECT IFNULL(SUM(SDHarga),0) INTO @SDTotal FROM tmotor WHERE SDNo = xSDNo;
				UPDATE ttSD	SET SDTotal = @SDTotal	WHERE SDNo = xSDNo;	
						
				#Jurnal
				IF xKodeTrans = 'SR' OR xKodeTrans = 'IR'  THEN
					DELETE FROM ttgeneralledgerhd WHERE GLLink = @SDNoOLD; #Hapus Jurnal No Transaksi Lama
					CALL jsr(xSDNo,xUserID); #Posting Jurnal
				ELSEIF xKodeTrans = 'ME' OR xKodeTrans = 'IE' THEN
					DELETE FROM ttgeneralledgerhd WHERE GLLink = @SDNoOLD; #Hapus Jurnal No Transaksi Lama
					CALL jsd(xSDNo,xUserID); #Posting Jurnal
				END IF;

				#tvmotorlokasi
				DELETE FROM tvmotorlokasi WHERE NoTrans = @SDNoOLD;
				DELETE FROM tvmotorlokasi WHERE NoTrans = xSDNo;
				INSERT INTO tvmotorlokasi
				SELECT tmotor.MotorAutoN AS MotorAutoN,  tmotor.MotorNoMesin AS MotorNoMesin, ttSD.LokasiKode AS LokasiKode, ttSD.SDNo AS NoTrans, ttSD.SDJam AS Jam, 'OUT' AS Kondisi 
				FROM tmotor  INNER JOIN ttSD ON tmotor.SDNo = ttSD.SDNo WHERE ttSD.SDNo = xSDNo;		
				
				IF @AutoNo = 'Edit' THEN /*Edit*/
					IF xKodeTrans = 'SR' THEN
						INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
						(NOW(), xUserID ,	 'Edit' , 'Retur Beli','ttsd', xSDNo); 
						SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Retur Beli [SR] No: ", xSDNo);
					ELSEIF xKodeTrans = 'IR' THEN
						INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
						(NOW(), xUserID , 'Edit' , 'Invoice Retur Beli','ttsd', xSDNo); 
						SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Invoice Retur Beli [IR] No: ", REPLACE(xSDNo,'SR','IR'));
					ELSEIF xKodeTrans = 'ME' THEN
						INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
						(NOW(), xUserID , 'Edit' , 'Mutasi Eksternal','ttsd', xSDNo); 
						SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Mutasi Eksternal [ME] No: ", xSDNo);
					ELSEIF xKodeTrans = 'IE' THEN
						INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
						(NOW(), xUserID , 'Edit' , 'Invoice Mutasi Eksternal','ttsd', xSDNo); 
						SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Invoice Eksternal [IE] No: ", REPLACE(xSDNo,'ME','IE'));
					END IF;
				ELSE /*Add*/
					IF xKodeTrans = 'SR' THEN
						INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
						(NOW(), xUserID , 'Tambah' , 'Retur Beli','ttsd', xSDNo); 
						SET oKeterangan = CONCAT(oKeterangan,"Berhasil menambah data Retur Beli [SR] No: ", xSDNo);
					ELSEIF xKodeTrans = 'ME' THEN
						INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
						(NOW(), xUserID , 'Tambah' , 'Mutasi Eksternal','ttsd', xSDNo); 
						SET oKeterangan = CONCAT(oKeterangan,"Berhasil menambah data Mutasi Eksternal [ME] No: ", xSDNo);
					END IF;
				END IF;			
			ELSEIF oStatus = 1 THEN
				SET oKeterangan = oKeterangan;  
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;

			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xSDNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xSDNo, " sudah memiliki No JT: ",@NoGL," yang tervalidasi");
			END IF;
			
			#Cek Motor Movement
			IF oStatus = 0 THEN
				SELECT SDJam INTO xSDJam FROM ttsd WHERE SDNo = xSDNo;
				SELECT GROUP_CONCAT((CONCAT(RPAD(MotorNoMesin, 15, ' '), RPAD(NoTrans, 18, ' '), RPAD(Jam, 22, ' '), RPAD(Kondisi, 6, ' '), RPAD(LokasiKode, 18, ' ') ) ) SEPARATOR '<br>')
				INTO oKeterangan FROM tvmotorlokasi
				WHERE MotorNoMesin IN (SELECT MotorNoMesin FROM tmotor WHERE SDNo = xSDNo) AND JAM > xSDJam	AND NoTrans <> xSDNo	ORDER BY JAM DESC;				
				IF LENGTH(oKeterangan) > 1 THEN
					SET oStatus = 1;
					SET oKeterangan = CONCAT("Telah terjadi perpindahan Motor sesudah tanggal : ",DATE_FORMAT(xSDJam,'%d-%m-%Y %k:%i:%s'),'<br>', oKeterangan);
				ELSE
					SET oStatus = 0;	
					SET oKeterangan = '';			
				END IF;
				#Cek IR dan IE tidak boleh dihapus
				IF xKodeTrans = 'IR' OR xKodeTrans = 'IE' THEN
					SET oStatus = 1; SET oKeterangan = CONCAT("Invoice tidak dapat dihapus secara langsung");
				END IF;
			END IF;

			#Delete Proses
			IF oStatus = 0 THEN
				DELETE FROM ttsd WHERE SDNo = xSDNo;				
				UPDATE tmotor SET SDNo = '--' WHERE SDNo = xSDNo;					
				DELETE FROM tvmotorlokasi WHERE NoTrans = xSDNo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xSDNo;
				IF xKodeTrans = 'SR' THEN
					SET oKeterangan = CONCAT("Berhasil menghapus data Retur Beli [SR] No: ",xSDNo);
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Hapus' , 'Retur Beli','ttsd', xSDNo); 
				ELSEIF xKodeTrans = 'ME' THEN
					SET oKeterangan = CONCAT("Berhasil menghapus data Mutasi Eksternal [ME] No: ",xSDNo);
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Hapus' , 'Mutasi Eksternal','ttsd', xSDNo); 
				END IF;								
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			IF xKodeTrans = 'SR' THEN
				IF LOCATE('=',xSDNo) <> 0 THEN
					DELETE FROM ttsd WHERE SDNo = xSDNo;
					DELETE FROM ttgeneralledgerhd WHERE GLLink = xSDNo;
					DELETE FROM tvmotorlokasi WHERE NoTrans = xSDNo;
					SET oKeterangan = CONCAT("Batal menyimpan data Retur Beli [SR] No: ", xSDNoBaru);
				ELSE
					SET oKeterangan = CONCAT("Batal menyimpan data Retur Beli [SR] No: ", xSDNo);
				END IF;
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Batal' , 'Retur Beli','ttsd', REPLACE(xSDNo,'=','-')); 
			ELSEIF xKodeTrans = 'IR' THEN
				SET oKeterangan = CONCAT("Batal menyimpan data Invoice Retur Beli [IR] No: ", xSDNo);
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Batal' , 'Invoice Retur Beli','ttsd', REPLACE(xSDNo,'=','-')); 
			ELSEIF xKodeTrans = 'ME' THEN
				IF LOCATE('=',xSDNo) <> 0 THEN
					DELETE FROM ttsd WHERE SDNo = xSDNo;
					DELETE FROM ttgeneralledgerhd WHERE GLLink = xSDNo;
					DELETE FROM tvmotorlokasi WHERE NoTrans = xSDNo;
					SET oKeterangan = CONCAT("Batal menyimpan data Mutasi Eksternal [ME] No: ", xSDNoBaru);
				ELSE
					SET oKeterangan = CONCAT("Batal menyimpan data Mutasi Eksternal [ME] No: ", xSDNo);
				END IF;
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Batal' , 'Mutasi Eksternal','ttsd', REPLACE(xSDNo,'=','-')); 
			ELSEIF xKodeTrans = 'IE' THEN
				SET oKeterangan = CONCAT("Batal menyimpan data Invoice Mutasi Eksternal [IE] No: ", xSDNo);
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Batal' , 'Invoice Mutasi Eksternal','ttsd', REPLACE(xSDNo,'=','-')); 
			END IF;		
		END;
		WHEN "Jurnal" THEN
		BEGIN
			IF LOCATE('=',xSDNo) <> 0 THEN /* Data Baru, belum tersimpan tidak boleh di jurnal */
				SET oStatus = 1;
				SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			ELSE
				CALL jsr(xSDNo,xUserID);
				SET oStatus = 0;
				SET oKeterangan = CONCAT("Berhasil memproses Jurnal Retur Beli [SD] No: ", xSDNo);
			END IF;
		END;
		WHEN "Print" THEN
		BEGIN
			IF xCetak = "1" THEN
				SELECT ttsd.SDNo, ttsd.SDTgl, ttsd.LokasiKode, ttsd.DealerKode, ttsd.SDMemo, ttsd.SDTotal, ttsd.UserID, tdlokasi.LokasiNama, tddealer.DealerNama, ttsd.SDJam, ttsd.NoGL
				FROM ttsd 
				INNER JOIN tdlokasi ON ttsd.LokasiKode = tdlokasi.LokasiKode 
				INNER JOIN tddealer ON ttsd.DealerKode = tddealer.DealerKode
				WHERE (ttsd.SDNo = xSDNo);
			END IF;
			IF xCetak = "2" THEN
				SELECT tmotor.BPKBNo, tmotor.BPKBTglAmbil, tmotor.BPKBTglJadi, tmotor.DKNo, tmotor.DealerKode, tmotor.FBHarga, tmotor.FBNo, tmotor.FakturAHMNo, tmotor.FakturAHMTgl, tmotor.FakturAHMTglAmbil, tmotor.MotorAutoN, 
            tmotor.MotorMemo, tmotor.MotorNoMesin, tmotor.MotorNoRangka, tmotor.MotorTahun, tmotor.MotorType, tmotor.MotorWarna, tmotor.PDNo, tmotor.PlatNo, tmotor.PlatTgl, tmotor.PlatTglAmbil, tmotor.SDNo, tmotor.SKNo, 
            tmotor.SSNo, tmotor.STNKAlamat, tmotor.STNKNama, tmotor.STNKNo, tmotor.STNKTglAmbil, tmotor.STNKTglBayar, tmotor.STNKTglJadi, tmotor.STNKTglMasuk, tdmotortype.MotorNama, tmotor.FakturAHMNilai, tmotor.RKNo, 
            tmotor.RKHarga, tmotor.SKHarga, tmotor.SDHarga, tmotor.MMHarga, tmotor.SSHarga
				FROM tmotor 
				LEFT OUTER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
				WHERE (tmotor.SDNo = xSDNo)
				ORDER BY tmotor.MotorType, tmotor.MotorTahun, tmotor.MotorWarna;			
			END IF;

			IF xKodeTrans = 'SR' THEN
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Cetak' , 'Retur Beli','ttsd', REPLACE(xSDNo,'=','-')); 
			ELSEIF xKodeTrans = 'IR' THEN
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Cetak' , 'Invoice Retur Beli','ttsd', REPLACE(xSDNo,'=','-')); 
			ELSEIF xKodeTrans = 'ME' THEN
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Cetak' , 'Mutasi Eksternal','ttsd', REPLACE(xSDNo,'=','-')); 
			ELSEIF xKodeTrans = 'IE' THEN
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Cetak' , 'Invoice Mutasi Eksternal','ttsd', REPLACE(xSDNo,'=','-')); 
			END IF;			
		END;
	END CASE;
END$$

DELIMITER ;

