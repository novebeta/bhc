DELIMITER $$

DROP PROCEDURE IF EXISTS `fkm`$$

CREATE DEFINER=`root`@`%` PROCEDURE `fkm`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xKMNo VARCHAR(18),
IN xKMNoBaru VARCHAR(18),
IN xKMTgl DATE,
IN xKMMemo VARCHAR(100),
IN xKMNominal DECIMAL(11,0),
IN xKMJenis VARCHAR(20),
IN xKMPerson VARCHAR(50),
IN xKMLink VARCHAR(12),
IN xKMJam DATETIME,
IN xNoGL VARCHAR(10)
)
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			DECLARE rKMNo VARCHAR(100) DEFAULT '';
			cek01: BEGIN
				DECLARE done INT DEFAULT FALSE;
				DECLARE cur1 CURSOR FOR 
				SELECT KMNo FROM ttKM
				LEFT OUTER JOIN ttgeneralledgerhd ON ttgeneralledgerhd.GLLink = ttKM.KMNo 
				WHERE ttgeneralledgerhd.NoGL IS NULL AND KMTgl BETWEEN DATE_ADD(NOW(), INTERVAL -3 DAY) AND DATE_ADD(NOW(), INTERVAL -50 MINUTE);
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;		
				OPEN cur1;
				REPEAT FETCH cur1 INTO rKMNo;
					IF rKMNo IS NOT NULL AND rKMNo <> '' THEN
						CALL jKM(rKMNo, 'System');
					END IF;
				UNTIL done END REPEAT;
				CLOSE cur1;
			END cek01;
					
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Daftar Kas Masuk ");
		END;
		WHEN "Load" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Load data Kas Masuk No: ", xKMNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			SELECT LokasiNomor INTO @LokasiNomor FROM tdlokasi WHERE LokasiKode = xLokasiKode;
			SET @AutoNo = CONCAT('KM',@LokasiNomor, RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(KMNo,5)+1),5,0)) INTO @AutoNo FROM ttkm WHERE ((KMNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(KMNo) = 12) ORDER BY KMNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 12 THEN SET @AutoNo = CONCAT('KM',@LokasiNomor,RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xKMNo = @AutoNo;
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Tambah data baru Kas Masuk No: ",xKMNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @KMNoOLD = xKMNo;
			
			IF LOCATE('=',xKMNo) <> 0  THEN
				SELECT LokasiNomor INTO @LokasiNomor FROM tdlokasi WHERE LokasiKode = xLokasiKode;
				SET @AutoNo = CONCAT('KM',@LokasiNomor, RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(KMNo,5)+1),5,0)) INTO @AutoNo FROM ttkm WHERE ((KMNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(KMNo) = 12) ORDER BY KMNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 12 THEN SET @AutoNo = CONCAT('KM',@LokasiNomor,RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xKMNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xKMNoBaru = xKMNo;
			END IF;

			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xKMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Update
			IF oStatus = 0 THEN 
				UPDATE ttkm
				SET KMNo = xKMNoBaru, KMTgl = xKMTgl, KMJenis = xKMJenis, KMPerson = xKMPerson, KMLink = xKMLink, KMNominal = xKMNominal, KMMemo = xKMMemo, NoGL = xNoGL, KMJam = xKMJam, UserID = xUserID, LokasiKode = xLokasiKode
				WHERE KMNo = xKMNo;
				SET xKMNo = xKMNoBaru;

				#KMAutoN
				IF xKMJenis = 'Umum' THEN
					UPDATE ttKMitcoa SET KMAutoN = -(KMAutoN) WHERE KMNo = xKMNo;
					blockCOA: BEGIN
					DECLARE myKMNo VARCHAR(12);
					DECLARE myKMAutoN SMALLINT;
					DECLARE myNoAccount VARCHAR(10);
					DECLARE doneCOA INT DEFAULT 0;  	
					DECLARE curCOA CURSOR FOR SELECT KMNo, KMAutoN, NoAccount FROM ttKMitcoa WHERE KMNo = xKMNo ORDER BY ABS(KMAutoN);	
					DECLARE CONTINUE HANDLER FOR NOT FOUND SET doneCOA = 1;
					SET @row_number = 0; 
					OPEN curCOA;
					REPEAT FETCH curCOA INTO myKMNo,myKMAutoN,myNoAccount ;
						SET @row_number = @row_number + 1; 
						UPDATE ttKMitcoa SET KMAutoN = @row_number
						WHERE KMNo = myKMNo AND KMAutoN = myKMAutoN AND NoAccount = myNoAccount;
					UNTIL doneCOA END REPEAT;
					CLOSE curCOA;
					END blockCOA;		
				END IF;
				
				#Jurnal
				DELETE FROM ttgeneralledgerhd WHERE GLLink = @KMNoOLD; #Hapus Jurnal No Transaksi Lama
				CALL jkm(xKMNo,xUserID); #Posting Jurnal

				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Edit' , 'Kas Masuk','ttkm', xKMNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil memperbarui data Kas Masuk No: ", xKMNo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Kas Masuk','ttkm', xKMNo);
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data baru Kas Masuk No: ", xKMNo);
				END IF;
			ELSEIF oStatus = 1 THEN
			  SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;
			
			#Cek Jurnal Validasi
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xKMNo;
			IF @GLValid = 'Sudah' THEN	
				SET oStatus = 1; SET oKeterangan = CONCAT("No KM:", xKMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;		
			
			IF oStatus = 0 THEN
				DELETE FROM ttkm WHERE KMNo = xKMNo;
				DELETE FROM ttgeneralledgerhd WHERE GLLink = xKMNo;

				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Kas Masuk','ttkm', xKMNo); 

				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Kas Masuk No: ",xKMNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;
		END;
		WHEN "Cancel" THEN
		BEGIN
			 IF LOCATE('=',xKMNo) <> 0 THEN
				  DELETE FROM ttkm WHERE KMNo = xKMNo;
				  SET oKeterangan = CONCAT("Batal menyimpan data Kas Masuk No: ", xKMNoBaru);
			 ELSE
				  SET oKeterangan = CONCAT("Batal menyimpan data Kas Masuk No: ", xKMNo);
			 END IF;
		END;
		WHEN "Jurnal" THEN
		BEGIN
			 IF LOCATE('=',xKMNo) <> 0 THEN 
				  SET oStatus = 1;
				  SET oKeterangan = CONCAT("Proses Jurnal Gagal, silahkan simpan terlebih dahulu");
			 ELSE
				  CALL jkm(xKMNo,xUserID);
				  SET oStatus = 0;
				  SET oKeterangan = CONCAT("Berhasil memproses Jurnal Kas Masuk No: ", xKMNo);
			 END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
			UPDATE ttkm SET KMLink = xKMLink WHERE KMNo = xKMNo;
		END;
		WHEN "Print" THEN
		BEGIN
			IF xCetak = "1" THEN
				SELECT  KMNo, KMTgl, KMMemo, KMNominal, KMJenis, KMPerson, KMLink, UserID, LokasiKode, KMJam, NoGL
				FROM ttkm
				WHERE  (KMNo = xKMNo);
			END IF;
			IF xCetak = "2" THEN
				IF xKKJenis = 'Umum' THEN				
					SELECT  ttkmitcoa.KMNo, ttkmitcoa.KMAutoN, ttkmitcoa.NoAccount, ttkmitcoa.KMBayar, ttkmitcoa.KMKeterangan, traccount.NamaAccount
					FROM  ttkmitcoa 
					INNER JOIN traccount ON ttkmitcoa.NoAccount = traccount.NoAccount
					WHERE (ttkmitcoa.KMNo = xKMNo)
					ORDER BY ttkmitcoa.KMAutoN;
				END IF;
			END IF;
			SET oStatus = 0;SET oKeterangan = CONCAT("Cetak data Kas Masuk No: ", xKMNo);
			INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE  
			(NOW(), xUserID , 'Cetak' , CONCAT('Kas Masuk ',xKMJenis),'ttkm', xKMNo); 
		END;
	END CASE;
END$$

DELIMITER ;

