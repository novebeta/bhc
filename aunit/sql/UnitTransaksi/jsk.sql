DELIMITER $$

DROP PROCEDURE IF EXISTS `jsk`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jsk`(IN xSKNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE xAutoN INTEGER DEFAULT 1;

	DECLARE oSKHarga, oDKHPP, oDKHarga,
	oPrgSubsSupplier, oPrgSubsDealer, oPrgSubsFincoy,
	oDKDPTotal, oDKDPLLeasing, oDKDPTerima, oDKDPInden, oDKNetto, oBBN, oJaket,
	oReturHarga, oNamaHadiah, oPotonganAHM, oInsentifSales, oPotonganHarga, oPotonganKhusus,
	oProgramSubsidi, oDKSCP, oDKScheme, oDKScheme2 DOUBLE DEFAULT 0;
	DECLARE oDealerKode VARCHAR(10) DEFAULT '--';
	DECLARE oDKDPTerima_Byr, oDKDPInden_Byr, oDKDPLLeasing_Byr, oDKNetto_Byr DOUBLE DEFAULT 0;
	DECLARE oPotonganHarga_Byr, oReturHarga_Byr, oInsentifSales_Byr DOUBLE DEFAULT 0;
	DECLARE oBiayaMaterai DOUBLE DEFAULT 0;

	DECLARE oGLLink VARCHAR(18) DEFAULT '--';

	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xSKNo ORDER BY NOGL DESC LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;

	SELECT PerusahaanNama INTO @PerusahaanNama FROM tzcompany LIMIT 1;
	SELECT LokasiKode INTO @LokasiKode FROM tuser WHERE  UserID = xUserID;
   SELECT IFNULL(RKNo,'') INTO @RKNo FROM ttrk WHERE SKNo =  xSKNo;

	IF @RKNo IS NULL THEN
		SELECT
		ttsk.SKNo, ttsk.NoGL,ttsk.SKTgl,ttsk.SKJam,ttdk.DKNo    , DKTgl,  LeaseKode,DealerKode,
		DKJenis, INNo, ttsk.UserID, SKHarga, DKHPP, DKHarga,
		PrgSubsSupplier, PrgSubsDealer, PrgSubsFincoy,
		DKDPTotal, DKDPLLeasing, DKDPTerima, DKDPInden, DKNetto, BBN, Jaket,
		ReturHarga, NamaHadiah, PotonganAHM,InsentifSales,PotonganHarga, PotonganKhusus,
		ProgramSubsidi,DKSCP,DKScheme,DKScheme2 INTO
		@SKNo, @NoGL, @SKTgl,@SKJam, @DKNo, @DKTgl,  @LeaseKode, @DealerKodeDK,
		@DKJenis, @INNo, @UserID, oSKHarga, oDKHPP, oDKHarga,
		oPrgSubsSupplier, oPrgSubsDealer, oPrgSubsFincoy,
		oDKDPTotal, oDKDPLLeasing, oDKDPTerima, oDKDPInden, oDKNetto, oBBN, oJaket,
		oReturHarga, @NamaHadiah, oPotonganAHM, oInsentifSales, oPotonganHarga, oPotonganKhusus,
		oProgramSubsidi, oDKSCP, oDKScheme, oDKScheme2
		FROM  ttdk
		INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
		INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo WHERE ttsk.SKNo = xSKNo;
		SELECT SalesNama INTO @SalesNama
		FROM  ttdk
		INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
		INNER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode
		INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo WHERE ttsk.SKNo = xSKNo;
		SELECT
		IF(TRIM(tdcustomer.CusNama)<>TRIM(tdcustomer.CusPembeliNama) AND tdcustomer.CusPembeliNama <>'--' AND tdcustomer.CusPembeliNama <>'',CONCAT_WS(' qq ',tdcustomer.CusNama,tdcustomer.CusPembeliNama),tdcustomer.CusNama) AS CusNama,CusKabupaten
		INTO
		@CusNama,@CusKabupaten
		FROM  ttdk
		INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
		INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
		INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo WHERE ttsk.SKNo = xSKNo;
	ELSE
		SELECT
		ttsk.SKNo, ttsk.NoGL,ttsk.SKTgl,ttsk.SKJam,ttdk.DKNo, DKTgl,  LeaseKode,DealerKode,
		DKJenis, INNo, ttsk.UserID, SKHarga, DKHPP, DKHarga,
		PrgSubsSupplier, PrgSubsDealer, PrgSubsFincoy,
		DKDPTotal, DKDPLLeasing, DKDPTerima, DKDPInden, DKNetto, BBN, Jaket,
		ReturHarga, NamaHadiah, PotonganAHM,InsentifSales,PotonganHarga, PotonganKhusus,
		ProgramSubsidi,DKSCP,DKScheme,DKScheme2 INTO
		@SKNo, @NoGL, @SKTgl,@SKJam, @DKNo, @DKTgl,  @LeaseKode, @DealerKodeDK,
		@DKJenis, @INNo, @UserID, oSKHarga, oDKHPP, oDKHarga,
		oPrgSubsSupplier, oPrgSubsDealer, oPrgSubsFincoy,
		oDKDPTotal, oDKDPLLeasing, oDKDPTerima, oDKDPInden, oDKNetto, oBBN, oJaket,
		oReturHarga, @NamaHadiah, oPotonganAHM, oInsentifSales, oPotonganHarga, oPotonganKhusus,
		oProgramSubsidi, oDKSCP, oDKScheme, oDKScheme2
		FROM  ttdk
		INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
		INNER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo
		INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo WHERE ttrk.SKNo = xSKNo;
		SELECT SalesNama INTO @SalesNama
		FROM  ttdk
		INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
		INNER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo
		INNER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode
		INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo WHERE ttrk.SKNo = xSKNo;
		SELECT
		IF(TRIM(tdcustomer.CusNama)<>TRIM(tdcustomer.CusPembeliNama) AND tdcustomer.CusPembeliNama <>'--' AND tdcustomer.CusPembeliNama <>'',CONCAT_WS(' qq ',tdcustomer.CusNama,tdcustomer.CusPembeliNama),tdcustomer.CusNama) AS CusNama,CusKabupaten
		INTO
		@CusNama,@CusKabupaten
		FROM  ttdk
		INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
		INNER JOIN ttrk ON tmotor.RKNo = ttrk.RKNo
		INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
		INNER JOIN ttsk ON tmotor.SKNo = ttsk.SKNo WHERE ttrk.SKNo = xSKNo;
	END IF;
	IF @INNo <> '' AND @INNo <> '--' THEN  UPDATE ttIN SET DKNo = @DKNo, LeaseKode =  @LeaseKode WHERE  INNo = @INNo;  END IF;

	SELECT NoAccount INTO @LeaseCOA FROM traccount WHERE UPPER(NamaAccount) LIKE UPPER(CONCAT('%',LEFT(@LeaseKode,3),'%')) AND JenisAccount ='Detail' AND NoParent = '11080000' LIMIT 1;

	/* Terbayar KM / BM */
	SET @KMBMJenis = @DKJenis;
	SELECT IFNULL(SUM(KMNominal),0) INTO oDKDPTerima_Byr FROM (
	SELECT KMNo, KMLINK , ttkm.KMJenis, 'KM' AS Via , KMNominal, KMTgl, KMJam, NoGL  FROM ttkm
	WHERE KMLink = @DKNo AND KMJam < @SKJam AND KMJenis = @KMBMJenis UNION
	SELECT ttbmit.BMNo AS KMNo , ttbmit.DKNo AS KMLINK, ttbmhd.BMJenis AS KMJenis, 'BM' AS Via, ttbmit.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, NoGL FROM ttbmhd INNER JOIN ttbmit ON ttbmhd.BMNo = ttbmit.BMNo
	WHERE ttbmit.DKNo = @DKNo AND ttbmhd.BMJam < @SKJam AND ttbmhd.BMJenis = @KMBMJenis UNION
	SELECT ttbmitpm.BMNo AS KMNo , ttbmitpm.DKNo AS KMLINK, ttbmitpm.BMJenis AS KMJenis, 'BM' AS Via, ttbmitpm.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, NoGL FROM ttbmhd INNER JOIN ttbmitpm ON ttbmhd.BMNo = ttbmitpm.BMNo
	WHERE ttbmitpm.BMJenis IN ('Piutang UM','Tunai','Kredit','Inden','Piutang Sisa','Leasing','Scheme')
	AND ttbmitpm.DKNo = @DKNo  AND ttbmhd.BMJam < @SKJam AND ttbmitpm.BMJenis = @KMBMJenis UNION
	SELECT KMNo, KMLINK, ttkm.KMJenis, 'KM' AS Via, KMNominal, KMTgl, KMJam, NoGL
	FROM  ttkm
	INNER JOIN ttin ON ttin.INNo = ttkm.KMLink
	INNER JOIN ttdk ON ttdk.DKNo = ttin.DKNo
	WHERE  ttdk.DKNo = @DKNo AND KMJam < @SKJam AND KMJenis = @KMBMJenis
	UNION
	SELECT ttbmit.BMNo AS KMNo , ttbmit.DKNo AS KMLINK, ttbmhd.BMJenis AS KMJenis, 'BM' AS Via, ttbmit.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, NoGL
	FROM ttbmit
	LEFT OUTER JOIN ttbmhd ON (ttbmit.BMNo = ttbmhd.BMNo)
	LEFT OUTER JOIN ttin ON ttbmit.DKNo = ttin.INNo  WHERE ttin.DKNo IS NOT NULL
	AND ttin.DKNo = @DKNo
	AND ttbmhd.BMJam < @SKJam
	AND ttbmhd.BMJenis = @KMBMJenis) A;

	SET @KMBMJenis = 'Inden';
	SELECT IFNULL(SUM(KMNominal),0) INTO oDKDPInden_Byr FROM (
	SELECT KMNo, KMLINK , ttkm.KMJenis, 'KM' AS Via , KMNominal, KMTgl, KMJam, NoGL  FROM ttkm
	WHERE KMLink = @DKNo AND KMJam < @SKJam AND KMJenis = @KMBMJenis UNION
	SELECT ttbmit.BMNo AS KMNo , ttbmit.DKNo AS KMLINK, ttbmhd.BMJenis AS KMJenis, 'BM' AS Via, ttbmit.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, NoGL FROM ttbmhd INNER JOIN ttbmit ON ttbmhd.BMNo = ttbmit.BMNo
	WHERE ttbmit.DKNo = @DKNo AND ttbmhd.BMJam < @SKJam AND ttbmhd.BMJenis = @KMBMJenis UNION
	SELECT ttbmitpm.BMNo AS KMNo , ttbmitpm.DKNo AS KMLINK, ttbmitpm.BMJenis AS KMJenis, 'BM' AS Via, ttbmitpm.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, NoGL FROM ttbmhd INNER JOIN ttbmitpm ON ttbmhd.BMNo = ttbmitpm.BMNo
	WHERE ttbmitpm.BMJenis IN ('Piutang UM','Tunai','Kredit','Inden','Piutang Sisa','Leasing','Scheme')
	AND ttbmitpm.DKNo = @DKNo  AND ttbmhd.BMJam < @SKJam AND ttbmitpm.BMJenis = @KMBMJenis UNION
	SELECT KMNo, KMLINK, ttkm.KMJenis, 'KM' AS Via, KMNominal, KMTgl, KMJam, NoGL
	FROM  ttkm
	INNER JOIN ttin ON ttin.INNo = ttkm.KMLink
	INNER JOIN ttdk ON ttdk.DKNo = ttin.DKNo
	WHERE  ttdk.DKNo = @DKNo AND KMJam < @SKJam AND KMJenis = @KMBMJenis
	UNION
	SELECT ttbmit.BMNo AS KMNo , ttbmit.DKNo AS KMLINK, ttbmhd.BMJenis AS KMJenis, 'BM' AS Via, ttbmit.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, NoGL
	FROM ttbmit
	LEFT OUTER JOIN ttbmhd ON (ttbmit.BMNo = ttbmhd.BMNo)
	LEFT OUTER JOIN ttin ON ttbmit.DKNo = ttin.INNo  WHERE ttin.DKNo IS NOT NULL
	AND ttin.DKNo = @DKNo
	AND ttbmhd.BMJam < @SKJam
	AND ttbmhd.BMJenis = @KMBMJenis) A;

	SET @KMBMJenis = 'Piutang UM';
	SELECT IFNULL(SUM(KMNominal),0) INTO oDKDPLLeasing_Byr FROM (
	SELECT KMNo, KMLINK , ttkm.KMJenis, 'KM' AS Via , KMNominal, KMTgl, KMJam, NoGL  FROM ttkm
	WHERE KMLink = @DKNo AND KMJam < @SKJam AND KMJenis = @KMBMJenis UNION
	SELECT ttbmit.BMNo AS KMNo , ttbmit.DKNo AS KMLINK, ttbmhd.BMJenis AS KMJenis, 'BM' AS Via, ttbmit.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, NoGL FROM ttbmhd INNER JOIN ttbmit ON ttbmhd.BMNo = ttbmit.BMNo
	WHERE ttbmit.DKNo = @DKNo AND ttbmhd.BMJam < @SKJam AND ttbmhd.BMJenis = @KMBMJenis UNION
	SELECT ttbmitpm.BMNo AS KMNo , ttbmitpm.DKNo AS KMLINK, ttbmitpm.BMJenis AS KMJenis, 'BM' AS Via, ttbmitpm.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, NoGL FROM ttbmhd INNER JOIN ttbmitpm ON ttbmhd.BMNo = ttbmitpm.BMNo
	WHERE ttbmitpm.BMJenis IN ('Piutang UM','Tunai','Kredit','Inden','Piutang Sisa','Leasing','Scheme')
	AND ttbmitpm.DKNo = @DKNo  AND ttbmhd.BMJam < @SKJam AND ttbmitpm.BMJenis = @KMBMJenis UNION
	SELECT KMNo, KMLINK, ttkm.KMJenis, 'KM' AS Via, KMNominal, KMTgl, KMJam, NoGL
	FROM  ttkm
	INNER JOIN ttin ON ttin.INNo = ttkm.KMLink
	INNER JOIN ttdk ON ttdk.DKNo = ttin.DKNo
	WHERE  ttdk.DKNo = @DKNo AND KMJam < @SKJam AND KMJenis = @KMBMJenis
	UNION
	SELECT ttbmit.BMNo AS KMNo , ttbmit.DKNo AS KMLINK, ttbmhd.BMJenis AS KMJenis, 'BM' AS Via, ttbmit.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, NoGL
	FROM ttbmit
	LEFT OUTER JOIN ttbmhd ON (ttbmit.BMNo = ttbmhd.BMNo)
	LEFT OUTER JOIN ttin ON ttbmit.DKNo = ttin.INNo  WHERE ttin.DKNo IS NOT NULL
	AND ttin.DKNo = @DKNo
	AND ttbmhd.BMJam < @SKJam
	AND ttbmhd.BMJenis = @KMBMJenis) A;

	IF @DKJenis = 'Tunai' THEN
      SET @KMBMJenis = 'Piutang Sisa';
  		SELECT IFNULL(SUM(KMNominal),0) INTO oDKNetto_Byr FROM (
			SELECT KMNo, KMLINK , ttkm.KMJenis, 'KM' AS Via , KMNominal, KMTgl, KMJam, NoGL  FROM ttkm
			WHERE KMLink = @DKNo AND KMJam < @SKJam AND KMJenis = @KMBMJenis UNION
			SELECT ttbmit.BMNo AS KMNo , ttbmit.DKNo AS KMLINK, ttbmhd.BMJenis AS KMJenis, 'BM' AS Via, ttbmit.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, NoGL FROM ttbmhd INNER JOIN ttbmit ON ttbmhd.BMNo = ttbmit.BMNo
			WHERE ttbmit.DKNo = @DKNo AND ttbmhd.BMJam < @SKJam AND ttbmhd.BMJenis = @KMBMJenis UNION
			SELECT ttbmitpm.BMNo AS KMNo , ttbmitpm.DKNo AS KMLINK, ttbmitpm.BMJenis AS KMJenis, 'BM' AS Via, ttbmitpm.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, NoGL FROM ttbmhd INNER JOIN ttbmitpm ON ttbmhd.BMNo = ttbmitpm.BMNo
			WHERE ttbmitpm.BMJenis IN ('Piutang UM','Tunai','Kredit','Inden','Piutang Sisa','Leasing','Scheme')
			AND ttbmitpm.DKNo = @DKNo  AND ttbmhd.BMJam < @SKJam AND ttbmitpm.BMJenis = @KMBMJenis UNION
			SELECT KMNo, KMLINK, ttkm.KMJenis, 'KM' AS Via, KMNominal, KMTgl, KMJam, NoGL
			FROM  ttkm
			INNER JOIN ttin ON ttin.INNo = ttkm.KMLink
			INNER JOIN ttdk ON ttdk.DKNo = ttin.DKNo
			WHERE  ttdk.DKNo = @DKNo AND KMJam < @SKJam AND KMJenis = @KMBMJenis
			UNION
			SELECT ttbmit.BMNo AS KMNo , ttbmit.DKNo AS KMLINK, ttbmhd.BMJenis AS KMJenis, 'BM' AS Via, ttbmit.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, NoGL
			FROM ttbmit
			LEFT OUTER JOIN ttbmhd ON (ttbmit.BMNo = ttbmhd.BMNo)
			LEFT OUTER JOIN ttin ON ttbmit.DKNo = ttin.INNo  WHERE ttin.DKNo IS NOT NULL
			AND ttin.DKNo = @DKNo
			AND ttbmhd.BMJam < @SKJam
			AND ttbmhd.BMJenis = @KMBMJenis) A;
	ELSE
      SET @KMBMJenis = 'Leasing';
  		SELECT IFNULL(SUM(KMNominal),0) INTO oDKNetto_Byr FROM (
			SELECT KMNo, KMLINK , ttkm.KMJenis, 'KM' AS Via , KMNominal, KMTgl, KMJam, NoGL  FROM ttkm
			WHERE KMLink = @DKNo AND KMJam < @SKJam AND KMJenis = @KMBMJenis UNION
			SELECT ttbmit.BMNo AS KMNo , ttbmit.DKNo AS KMLINK, ttbmhd.BMJenis AS KMJenis, 'BM' AS Via, ttbmit.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, NoGL FROM ttbmhd INNER JOIN ttbmit ON ttbmhd.BMNo = ttbmit.BMNo
			WHERE ttbmit.DKNo = @DKNo AND ttbmhd.BMJam < @SKJam AND ttbmhd.BMJenis = @KMBMJenis UNION
			SELECT ttbmitpm.BMNo AS KMNo , ttbmitpm.DKNo AS KMLINK, ttbmitpm.BMJenis AS KMJenis, 'BM' AS Via, ttbmitpm.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, NoGL FROM ttbmhd INNER JOIN ttbmitpm ON ttbmhd.BMNo = ttbmitpm.BMNo
			WHERE ttbmitpm.BMJenis IN ('Piutang UM','Tunai','Kredit','Inden','Piutang Sisa','Leasing','Scheme')
			AND ttbmitpm.DKNo = @DKNo  AND ttbmhd.BMJam < @SKJam AND ttbmitpm.BMJenis = @KMBMJenis UNION
			SELECT KMNo, KMLINK, ttkm.KMJenis, 'KM' AS Via, KMNominal, KMTgl, KMJam, NoGL
			FROM  ttkm
			INNER JOIN ttin ON ttin.INNo = ttkm.KMLink
			INNER JOIN ttdk ON ttdk.DKNo = ttin.DKNo
			WHERE  ttdk.DKNo = @DKNo AND KMJam < @SKJam AND KMJenis = @KMBMJenis
			UNION
			SELECT ttbmit.BMNo AS KMNo , ttbmit.DKNo AS KMLINK, ttbmhd.BMJenis AS KMJenis, 'BM' AS Via, ttbmit.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, NoGL
			FROM ttbmit
			LEFT OUTER JOIN ttbmhd ON (ttbmit.BMNo = ttbmhd.BMNo)
			LEFT OUTER JOIN ttin ON ttbmit.DKNo = ttin.INNo  WHERE ttin.DKNo IS NOT NULL
			AND ttin.DKNo = @DKNo
			AND ttbmhd.BMJam < @SKJam
			AND ttbmhd.BMJenis = @KMBMJenis) A;
   END IF;
   SELECT DealerKode INTO oDealerKode FROM tddealer WHERE DealerStatus = '1' LIMIT 1;

  /* Terbayar KK / BK */
	SET @KKBKJenis = 'SubsidiDealer2';
	SELECT IFNULL(SUM(KKNominal),0) INTO oPotonganHarga_Byr FROM (
	SELECT ttBKit.BKNo AS KKNo , ttBKit.FBNo AS KKLINK, ttBKhd.BKJenis AS KKJenis, 'BK' AS Via, ttBKit.BKBayar AS KKNominal, ttBKhd.BKTgl AS KKTgl, ttBKhd.BKJam AS KKjam, ttBKhd.NoGL
	FROM ttBKhd
	INNER JOIN ttBKit ON ttBKhd.BKNo = ttBKit.BKNo
	WHERE ttBKit.FBNo = @DKNo AND ttBKhd.BKJam < @SKJam AND ttBKhd.BKJenis = @KKBKJenis
	UNION
	SELECT ttbmitpm.BMNo AS KKNo , ttbmitpm.DKNo AS KKLINK, ttbmitpm.BMJenis AS KKJenis, 'BM' AS Via, ttbmitpm.BMBayar AS KKNominal, ttbmhd.BMTgl AS KKTgl, ttbmhd.BMJam AS KKjam, ttBmhd.NoGL
	FROM ttbmhd
	INNER JOIN ttbmitpm ON ttbmhd.BMNo = ttbmitpm.BMNo WHERE ttbmitpm.BMJenis IN ('Retur Harga','SubsidiDealer2','Insentif Sales')
	AND ttbmitpm.DKNo = @DKNo  AND ttbmhd.BMJam < @SKJam AND ttbmitpm.BMJenis = @KKBKJenis
	UNION
	SELECT KKNo, KKLINK , ttKK.KKJenis, 'KK' AS Via , KKNominal, KKTgl, KKJam, NoGL
	FROM ttKK
	WHERE KKLINK = @DKNo AND KKJam < @SKJam AND ttKK.KKJenis = @KKBKJenis
	UNION
	SELECT ttkk.KKNo, ttkkit.FBNo AS KKLink , ttKK.KKJenis, 'KK' AS Via , ttkkit.KKBayar AS KKNominal, KKTgl, KKJam, NoGL
	FROM ttkk INNER JOIN ttkkit ON (ttkk.KKNo = ttkkit.KKNo)
	WHERE ttkkit.FBNo  = @DKNo AND KKJam < @SKJam AND ttKK.KKJenis = @KKBKJenis) A;

	SET @KKBKJenis = 'Retur Harga';
	SELECT IFNULL(SUM(KKNominal),0) INTO oReturHarga_Byr FROM (
	SELECT ttBKit.BKNo AS KKNo , ttBKit.FBNo AS KKLINK, ttBKhd.BKJenis AS KKJenis, 'BK' AS Via, ttBKit.BKBayar AS KKNominal, ttBKhd.BKTgl AS KKTgl, ttBKhd.BKJam AS KKjam, ttBKhd.NoGL
	FROM ttBKhd
	INNER JOIN ttBKit ON ttBKhd.BKNo = ttBKit.BKNo
	WHERE ttBKit.FBNo = @DKNo AND ttBKhd.BKJam < @SKJam AND ttBKhd.BKJenis = @KKBKJenis
	UNION
	SELECT ttbmitpm.BMNo AS KKNo , ttbmitpm.DKNo AS KKLINK, ttbmitpm.BMJenis AS KKJenis, 'BM' AS Via, ttbmitpm.BMBayar AS KKNominal, ttbmhd.BMTgl AS KKTgl, ttbmhd.BMJam AS KKjam, ttBmhd.NoGL
	FROM ttbmhd
	INNER JOIN ttbmitpm ON ttbmhd.BMNo = ttbmitpm.BMNo WHERE ttbmitpm.BMJenis IN ('Retur Harga','SubsidiDealer2','Insentif Sales')
	AND ttbmitpm.DKNo = @DKNo  AND ttbmhd.BMJam < @SKJam AND ttbmitpm.BMJenis = @KKBKJenis
	UNION
	SELECT KKNo, KKLINK , ttKK.KKJenis, 'KK' AS Via , KKNominal, KKTgl, KKJam, NoGL
	FROM ttKK
	WHERE KKLINK = @DKNo AND KKJam < @SKJam AND ttKK.KKJenis = @KKBKJenis
	UNION
	SELECT ttkk.KKNo, ttkkit.FBNo AS KKLink , ttKK.KKJenis, 'KK' AS Via , ttkkit.KKBayar AS KKNominal, KKTgl, KKJam, NoGL
	FROM ttkk INNER JOIN ttkkit ON (ttkk.KKNo = ttkkit.KKNo)
	WHERE ttkkit.FBNo  = @DKNo AND KKJam < @SKJam AND ttKK.KKJenis = @KKBKJenis) A;

	SET @KKBKJenis = 'Insentif Sales';
	SELECT IFNULL(SUM(KKNominal),0) INTO oInsentifSales_Byr FROM (
	SELECT ttBKit.BKNo AS KKNo , ttBKit.FBNo AS KKLINK, ttBKhd.BKJenis AS KKJenis, 'BK' AS Via, ttBKit.BKBayar AS KKNominal, ttBKhd.BKTgl AS KKTgl, ttBKhd.BKJam AS KKjam, ttBKhd.NoGL
	FROM ttBKhd
	INNER JOIN ttBKit ON ttBKhd.BKNo = ttBKit.BKNo
	WHERE ttBKit.FBNo = @DKNo AND ttBKhd.BKJam < @SKJam AND ttBKhd.BKJenis = @KKBKJenis
	UNION
	SELECT ttbmitpm.BMNo AS KKNo , ttbmitpm.DKNo AS KKLINK, ttbmitpm.BMJenis AS KKJenis, 'BM' AS Via, ttbmitpm.BMBayar AS KKNominal, ttbmhd.BMTgl AS KKTgl, ttbmhd.BMJam AS KKjam, ttBmhd.NoGL
	FROM ttbmhd
	INNER JOIN ttbmitpm ON ttbmhd.BMNo = ttbmitpm.BMNo WHERE ttbmitpm.BMJenis IN ('Retur Harga','SubsidiDealer2','Insentif Sales')
	AND ttbmitpm.DKNo = @DKNo  AND ttbmhd.BMJam < @SKJam AND ttbmitpm.BMJenis = @KKBKJenis
	UNION
	SELECT KKNo, KKLINK , ttKK.KKJenis, 'KK' AS Via , KKNominal, KKTgl, KKJam, NoGL
	FROM ttKK
	WHERE KKLINK = @DKNo AND KKJam < @SKJam AND ttKK.KKJenis = @KKBKJenis
	UNION
	SELECT ttkk.KKNo, ttkkit.FBNo AS KKLink , ttKK.KKJenis, 'KK' AS Via , ttkkit.KKBayar AS KKNominal, KKTgl, KKJam, NoGL
	FROM ttkk INNER JOIN ttkkit ON (ttkk.KKNo = ttkkit.KKNo)
	WHERE ttkkit.FBNo  = @DKNo AND KKJam < @SKJam AND ttKK.KKJenis = @KKBKJenis) A;

	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xNoGL =  @AutoNoGL;
		ELSE
			SET xNoGL = @NoGL;
	END IF;

	DELETE FROM ttgeneralledgerhd WHERE GLLink = xSKNo;
	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL;  
	DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;  
	DELETE FROM ttglhpit WHERE NoGL = xNoGL;

	IF LENGTH(@CusNama) > 80 THEN SET @CusNama = LEFT(@CusNama,80); END IF;

	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, @SKTgl, 'SK', CONCAT('Post SK - ',@SKNo,' - ',@DKNo, ' - ',@CusNama), 0, 0, xSKNo, xUserID, '--', xGLValid);

	INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
	(xNoGL, @SKTgl, xAutoN, '50000000', CONCAT('Post SK - ',@DKJenis,' - ',@CusNama), oDKHPP, 0); SET xAutoN = xAutoN + 1;
	INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
	(xNoGL, @SKTgl, xAutoN, '11150100', CONCAT('Post SK - ',@DKJenis,' - ',@CusNama), 0,oDKHPP); SET xAutoN = xAutoN + 1;

	INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
	(xNoGL, @SKTgl, xAutoN, '30010000', CONCAT('Post SK - ',@DKJenis,' - ',@CusNama), 0,oDKHarga); SET xAutoN = xAutoN + 1;

	IF oPrgSubsSupplier > 0 THEN
		IF LEFT(@PerusahaanNama,20) = 'CV. ANUGERAH PERDANA' THEN
			INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			(xNoGL, @SKTgl, xAutoN, '11110200', CONCAT('Post SK - ',@DKJenis,' - ',@CusNama), oPrgSubsSupplier,0); SET xAutoN = xAutoN + 1;
		ELSE
			IF @DealerKodeDK = oDealerKode THEN
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '11110200', CONCAT('Post SK - ',@DKJenis,' - ',@CusNama), oPrgSubsSupplier,0); SET xAutoN = xAutoN + 1;
			ELSE
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '11070400', CONCAT('Post SK - ',@DKJenis,' - ',@CusNama), oPrgSubsSupplier,0); SET xAutoN = xAutoN + 1;
			END IF;
		END IF;
	END IF;

	IF oPrgSubsDealer > 0 THEN /*Jika ada net Subsidi Dealer 1 - Subsidi Dealer 1*/
		INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		(xNoGL, @SKTgl, xAutoN, '30030000', CONCAT('Post SK - ',@DKJenis,' - ',@CusNama), oPrgSubsDealer,0); SET xAutoN = xAutoN + 1;
	END IF;

	IF oPrgSubsFincoy > 0 THEN /*Jika ada net Subsidi Fincoy - Piutang Leasing Adira */
		SELECT NoAccount INTO @PiutSubsLease FROM traccount WHERE NamaAccount LIKE CONCAT('%' , LEFT(@LeaseKode,3) , '%') AND JenisAccount ='Detail' AND NoParent = '11090000' LIMIT 1 ;
		INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		(xNoGL, @SKTgl, xAutoN, @PiutSubsLease, CONCAT('Post SK - ',@DKJenis,' - ',@CusNama), oPrgSubsFincoy,0); SET xAutoN = xAutoN + 1;
	END IF;

	IF LENGTH(@CusNama) > 66 THEN SET @CusNama = LEFT(@CusNama,65); END IF;
	IF oDKDPLLeasing > 0 THEN /*Piutang Konsumen UM*/
		IF oDKDPLLeasing_Byr > 0 THEN /*Hutang Titipan Konsumen*/
			INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			(xNoGL, @SKTgl, xAutoN, '24050100', CONCAT('Post SK - ',@DKJenis,' - Piutang Kons UM - ',@CusNama), oDKDPLLeasing_Byr,0); SET xAutoN = xAutoN + 1;
		END IF;
		IF (oDKDPLLeasing - oDKDPLLeasing_Byr) > 0 THEN /*Piutang Konsumen*/
			INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			(xNoGL, @SKTgl, xAutoN, '11060100', CONCAT('Post SK - ',@DKJenis,' - Piutang Kons UM - ',@CusNama), oDKDPLLeasing - oDKDPLLeasing_Byr,0); SET xAutoN = xAutoN + 1;
		END IF;
	END IF;

	IF oDKDPInden > 0 THEN /*Inden*/
		IF oDKDPInden_Byr > 0 THEN /*Hutang Titipan Konsumen*/
			INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			(xNoGL, @SKTgl, xAutoN, '24050100', CONCAT('Post SK - ',@DKJenis,' - UM Inden - ',@CusNama), oDKDPInden_Byr,0); SET xAutoN = xAutoN + 1;
		END IF;
		IF (oDKDPInden - oDKDPInden_Byr) > 0 THEN /*Piutang Konsumen*/
			INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			(xNoGL, @SKTgl, xAutoN, '11060100', CONCAT('Post SK - ',@DKJenis,' - UM Inden - ',@CusNama), oDKDPInden - oDKDPInden_Byr,0); SET xAutoN = xAutoN + 1;
		END IF;
	END IF;
	
	IF oDKDPTerima > 0 THEN /*Uang Muka*/
		IF oDKDPTerima_Byr > 0 THEN /*Hutang Titipan Konsumen*/
			INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			(xNoGL, @SKTgl, xAutoN, '24050100', CONCAT('Post SK - ',@DKJenis,' - UM Diterima - ',@CusNama), oDKDPTerima_Byr,0); SET xAutoN = xAutoN + 1;
		END IF;
		IF (oDKDPTerima - oDKDPTerima_Byr) > 0 THEN /*Piutang Konsumen*/
			INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			(xNoGL, @SKTgl, xAutoN, '11060100', CONCAT('Post SK - ',@DKJenis,' - UM Diterima - ',@CusNama), oDKDPTerima - oDKDPTerima_Byr,0); SET xAutoN = xAutoN + 1;
		END IF;
	END IF;

	IF oDKNetto > 0 THEN /*Sisa Piutang Konsumen (Tunai) - Piutang Leasing (Kredit)*/
		IF @DKJenis = 'Kredit' THEN /*Mesti terbayar lunas di BM Pelunasan Leasing*/
			IF LENGTH(@CusNama) > 41 THEN SET @CusNama = LEFT(@CusNama,40); END IF;
			IF oDKNetto_Byr > 0 THEN
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, @LeaseCOA, CONCAT('Post SK - ',@DKJenis,' - Piutang Leasing (Sudah Bayar) - ',@CusNama), oDKNetto_Byr,0); SET xAutoN = xAutoN + 1;
			END IF;
			IF (oDKNetto - oDKNetto_Byr) > 0 THEN /*Piutang Leasing*/
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, @LeaseCOA, CONCAT('Post SK - ',@DKJenis,' - Piutang Leasing (Belum Bayar) - ',@CusNama), oDKNetto - oDKNetto_Byr,0); SET xAutoN = xAutoN + 1;
			END IF;
		ELSEIF @DKJenis = 'Tunai' THEN
			IF LENGTH(@CusNama) > 51 THEN SET @CusNama = LEFT(@CusNama,50); END IF;
			IF oDKNetto_Byr > 0 THEN /*Hutang Titipan Konsumen*/
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '24050100', CONCAT('Post SK - ',@DKJenis,' - Sisa Piutang Kons - ',@CusNama), oDKNetto_Byr,0); SET xAutoN = xAutoN + 1;
			END IF;
			IF (oDKNetto - oDKNetto_Byr) > 0 THEN /*Piutang Konsumen*/
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '11060100', CONCAT('Post SK - ',@DKJenis,' - Sisa Piutang Kons - ',@CusNama), oDKNetto - oDKNetto_Byr,0); SET xAutoN = xAutoN + 1;
			END IF;
		END IF;
	END IF;

	IF oPotonganHarga > 0 THEN /*Subsidi Dealer 2 (SD2)*/
		IF (oPotonganHarga - oPotonganHarga_Byr) = 0 THEN /*Sudah ada KK*/
			  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			  (xNoGL, @SKTgl, xAutoN, '30040000', CONCAT('Post SK - ',@DKJenis,' - Subsidi Dealer 2 - ',@CusNama), oPotonganHarga_Byr,0); SET xAutoN = xAutoN + 1;
			  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			  (xNoGL, @SKTgl, xAutoN, '11170300', CONCAT('Post SK - ',@DKJenis,' - Subsidi Dealer 2 - ',@CusNama), 0,oPotonganHarga_Byr); SET xAutoN = xAutoN + 1;
		ELSEIF oPotonganHarga_Byr = 0 THEN
			  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			  (xNoGL, @SKTgl, xAutoN, '30040000', CONCAT('Post SK - ',@DKJenis,' - Subsidi Dealer 2 - ',@CusNama), oPotonganHarga,0); SET xAutoN = xAutoN + 1;
			  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			  (xNoGL, @SKTgl, xAutoN, '24040600', CONCAT('Post SK - ',@DKJenis,' - Subsidi Dealer 2 - ',@CusNama), 0,oPotonganHarga); SET xAutoN = xAutoN + 1;
		ELSE
			  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			  (xNoGL, @SKTgl, xAutoN, '30040000', CONCAT('Post SK - ',@DKJenis,' - Subsidi Dealer 2 - ',@CusNama), oPotonganHarga,0); SET xAutoN = xAutoN + 1;
			  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			  (xNoGL, @SKTgl, xAutoN, '11170300', CONCAT('Post SK - ',@DKJenis,' - Subsidi Dealer 2 - ',@CusNama), 0,oPotonganHarga_Byr); SET xAutoN = xAutoN + 1;
			  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			  (xNoGL, @SKTgl, xAutoN, '24040600', CONCAT('Post SK - ',@DKJenis,' - Subsidi Dealer 2 - ',@CusNama), 0,oPotonganHarga - oPotonganHarga_Byr); SET xAutoN = xAutoN + 1;
		END IF;
	END IF;

	IF oBBN > 0 THEN
		INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		(xNoGL, @SKTgl, xAutoN, '60050000', CONCAT('Post SK - ',@DKJenis,' - BBN - ',@CusNama), oBBN,0); SET xAutoN = xAutoN + 1;
		INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		(xNoGL, @SKTgl, xAutoN, '24030000', CONCAT('Post SK - ',@DKJenis,' - BBN - ',@CusNama), 0,oBBN); SET xAutoN = xAutoN + 1;
	END IF;


	IF LEFT(@PerusahaanNama,20) = 'CV. ANUGERAH PERDANA' AND @LokasiKode = 'PlatMerah' THEN
		IF oReturHarga > 0 THEN /*Komisi / RH*/
			INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			(xNoGL, @SKTgl, xAutoN, '60060000', CONCAT('Post SK - ',@DKJenis,' - Retur Harga - ',@CusNama), oReturHarga,0); SET xAutoN = xAutoN + 1;
			INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			(xNoGL, @SKTgl, xAutoN, '24040700', CONCAT('Post SK - ',@DKJenis,' - Retur Harga - ',@CusNama), 0,oReturHarga); SET xAutoN = xAutoN + 1;
		END IF;
	ELSE
		IF oReturHarga > 0 THEN /*Komisi / RH*/
			IF (oReturHarga - oReturHarga_Byr) = 0 THEN /*Sudah ada KK Retur Harga*/
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '60060000', CONCAT('Post SK - ',@DKJenis,' - Retur Harga - ',@CusNama), oReturHarga_Byr,0); SET xAutoN = xAutoN + 1;
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '11170400', CONCAT('Post SK - ',@DKJenis,' - Retur Harga - ',@CusNama), 0,oReturHarga_Byr); SET xAutoN = xAutoN + 1;
			ELSEIF oReturHarga_Byr = 0 THEN
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '60060000', CONCAT('Post SK - ',@DKJenis,' - Retur Harga - ',@CusNama), oReturHarga,0); SET xAutoN = xAutoN + 1;
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '24040700', CONCAT('Post SK - ',@DKJenis,' - Retur Harga - ',@CusNama), 0,oReturHarga); SET xAutoN = xAutoN + 1;
			ELSE
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '60060000', CONCAT('Post SK - ',@DKJenis,' - Retur Harga - ',@CusNama), oReturHarga,0); SET xAutoN = xAutoN + 1;
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '11170400', CONCAT('Post SK - ',@DKJenis,' - Retur Harga - ',@CusNama), 0,oReturHarga_Byr); SET xAutoN = xAutoN + 1;
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '24040700', CONCAT('Post SK - ',@DKJenis,' - Retur Harga - ',@CusNama), 0,oReturHarga - oReturHarga_Byr); SET xAutoN = xAutoN + 1;
			END IF;
		END IF;
	END IF;


	IF @PerusahaanNama = "CV. ANUGRAH SEJAHTERA BOGOR" OR @PerusahaanNama = "CV. ANUGERAH SEJAHTERA BOGOR"
							OR @PerusahaanNama = "CV. KARYA PERDANA INDRAMAYU"
							OR @PerusahaanNama = "CV. ANUGERAH KIRANA BENGKULU"
							OR @PerusahaanNama = "CV. ANUGERAH WANGI PAMEKASAN"
							OR @PerusahaanNama = "CV. ANUGERAH SEJATI PROBOLINGGO"
							OR @PerusahaanNama = "CV. ANUGERAH WANGI PAMEKASAN"
							OR @PerusahaanNama = "CV. ANUGERAH MULIA TOMOHON"  THEN
		IF oInsentifSales > 0 THEN /*InsentIF Sales*/
			IF (oInsentifSales - oInsentifSales_Byr) = 0 THEN /*Sudah ada KK*/
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '60060000', CONCAT('Post SK - ',@DKJenis,' - Insentif Sales - ',@CusNama), oInsentifSales_Byr,0); SET xAutoN = xAutoN + 1;
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '11170500', CONCAT('Post SK - ',@DKJenis,' - Insentif Sales - ',@CusNama), 0,oInsentifSales_Byr); SET xAutoN = xAutoN + 1;
			ELSEIF oInsentifSales_Byr = 0 THEN
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '60060000', CONCAT('Post SK - ',@DKJenis,' - Insentif Sales - ',@CusNama), oInsentifSales,0); SET xAutoN = xAutoN + 1;
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '24051100', CONCAT('Post SK - ',@DKJenis,' - Insentif Sales - ',@CusNama), 0,oInsentifSales); SET xAutoN = xAutoN + 1;
			ELSE
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '60060000', CONCAT('Post SK - ',@DKJenis,' - Insentif Sales - ',@CusNama), oInsentifSales,0); SET xAutoN = xAutoN + 1;
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '11170500', CONCAT('Post SK - ',@DKJenis,' - Insentif Sales - ',@CusNama), 0,oInsentifSales_Byr); SET xAutoN = xAutoN + 1;
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '24051100', CONCAT('Post SK - ',@DKJenis,' - Insentif Sales - ',@CusNama), 0,oInsentifSales - oInsentifSales_Byr); SET xAutoN = xAutoN + 1;
			END IF;
		END IF;
	ELSEIF LEFT(@PerusahaanNama,20) = 'CV. ANUGERAH PERDANA' THEN
		INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		(xNoGL, @SKTgl, xAutoN, '30010000', CONCAT('Post SK - ',@DKJenis,' - Insentif Sales - ',@CusNama), oInsentifSales,0); SET xAutoN = xAutoN + 1;
		INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		(xNoGL, @SKTgl, xAutoN, '11150600', CONCAT('Post SK - ',@DKJenis,' - Insentif Sales - ',@CusNama), 0,oInsentifSales); SET xAutoN = xAutoN + 1;
	ELSEIF @PerusahaanNama = 'CV. MEGAH UTAMA PALU'  THEN
		INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		(xNoGL, @SKTgl, xAutoN, '60140000', CONCAT('Post SK - ',@DKJenis,' - Insentif Sales - ',@CusNama), oInsentifSales,0); SET xAutoN = xAutoN + 1;
		INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		(xNoGL, @SKTgl, xAutoN, '11150300', CONCAT('Post SK - ',@DKJenis,' - Insentif Sales - ',@CusNama), 0,oInsentifSales); SET xAutoN = xAutoN + 1;
	ELSE
		IF oInsentifSales > 0 THEN /*InsentIF Sales*/
			IF (oInsentifSales - oInsentifSales_Byr) = 0 THEN /*Sudah ada KK*/
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '60020000', CONCAT('Post SK - ',@DKJenis,' - Insentif Sales - ',@CusNama), oInsentifSales_Byr,0); SET xAutoN = xAutoN + 1;
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '11170500', CONCAT('Post SK - ',@DKJenis,' - Insentif Sales - ',@CusNama), 0,oInsentifSales_Byr); SET xAutoN = xAutoN + 1;
			ELSEIF oInsentifSales_Byr = 0 THEN
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '60020000', CONCAT('Post SK - ',@DKJenis,' - Insentif Sales - ',@CusNama), oInsentifSales,0); SET xAutoN = xAutoN + 1;
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '24040400', CONCAT('Post SK - ',@DKJenis,' - Insentif Sales - ',@CusNama), 0,oInsentifSales); SET xAutoN = xAutoN + 1;
			ELSE
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '60020000', CONCAT('Post SK - ',@DKJenis,' - Insentif Sales - ',@CusNama), oInsentifSales,0); SET xAutoN = xAutoN + 1;
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '11170500', CONCAT('Post SK - ',@DKJenis,' - Insentif Sales - ',@CusNama), 0,oInsentifSales_Byr); SET xAutoN = xAutoN + 1;
				INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
				(xNoGL, @SKTgl, xAutoN, '24040400', CONCAT('Post SK - ',@DKJenis,' - Insentif Sales - ',@CusNama), 0,oInsentifSales - oInsentifSales_Byr); SET xAutoN = xAutoN + 1;
			END IF;
		END IF;
	END IF;

	IF oJaket > 0 THEN
		IF @PerusahaanNama  = "CV. ANUGRAH SEJAHTERA BOGOR" OR @PerusahaanNama  = "CV. ANUGERAH SEJAHTERA BOGOR"  THEN
			 INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			 (xNoGL, @SKTgl, xAutoN, '11120200', CONCAT('Post SK - ',@DKJenis,' - Beban Jaket - ',@SalesNama), oJaket,0); SET xAutoN = xAutoN + 1;
			 INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			 (xNoGL, @SKTgl, xAutoN, '11160300', CONCAT('Post SK - ',@DKJenis,' - Beban Jaket - ',@SalesNama), 0,oJaket); SET xAutoN = xAutoN + 1;
		ELSE
			 INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			 (xNoGL, @SKTgl, xAutoN, '60070000', CONCAT('Post SK - ',@DKJenis,' - Beban Jaket - ',@CusNama), oJaket,0); SET xAutoN = xAutoN + 1;
			 INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			 (xNoGL, @SKTgl, xAutoN, '11160200', CONCAT('Post SK - ',@DKJenis,' - Beban Jaket - ',@CusNama), 0,oJaket); SET xAutoN = xAutoN + 1;
		END IF;
	END IF;

	IF oPotonganAHM > 0 THEN
		INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		(xNoGL, @SKTgl, xAutoN, '11110100', CONCAT('Post SK - ',@DKJenis,' - ',@CusNama), oPotonganAHM,0); SET xAutoN = xAutoN + 1; /*Piut Main Dealer*/
		INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		(xNoGL, @SKTgl, xAutoN, '40040300', CONCAT('Post SK - ',@DKJenis,' - ',@CusNama), 0,oPotonganAHM); SET xAutoN = xAutoN + 1; /*Pend Pot Astra*/
	END IF;

	IF @PerusahaanNama = 'CV. ANUGERAH UTAMA GORONTALO' OR @PerusahaanNama = 'CV. CENDANA MEGAH LESTARI' THEN
		IF oPotonganKhusus > 0 THEN
			 INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			 (xNoGL, @SKTgl, xAutoN, '30040000', CONCAT('Post SK - ',@DKJenis,' - Potongan Khusus - ',@CusNama), oPotonganKhusus,0); SET xAutoN = xAutoN + 1;
			 INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			 (xNoGL, @SKTgl, xAutoN, '24040100', CONCAT('Post SK - ',@DKJenis,' - Potongan Khusus - ',@CusNama), 0,oPotonganKhusus); SET xAutoN = xAutoN + 1; /*Hutang Biaya Potongan Khusus*/
		END IF;
	ELSEIF @PerusahaanNama = 'CV. CENDANA MEGAH SENTOSA' OR @PerusahaanNama = 'CV. CENDANA MEGAH SANTOSA' OR @PerusahaanNama = 'CV. CENDANA HARUM DEMAK'  THEN
		IF oPotonganKhusus > 0 THEN
			 INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			 (xNoGL, @SKTgl, xAutoN, '30040000', CONCAT('Post SK - ',@DKJenis,' - Potongan Khusus - ',@CusNama), oPotonganKhusus,0); SET xAutoN = xAutoN + 1;
			 INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			 (xNoGL, @SKTgl, xAutoN, '24051100', CONCAT('Post SK - ',@DKJenis,' - Potongan Khusus - ',@CusNama), 0,oPotonganKhusus); SET xAutoN = xAutoN + 1; /*Hutang Biaya Potongan Khusus*/
		END IF;
	ELSE
		IF oPotonganKhusus > 0 THEN
			 INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			 (xNoGL, @SKTgl, xAutoN, '60380000', CONCAT('Post SK - ',@DKJenis,' - Potongan Khusus - ',@CusNama), oPotonganKhusus,0); SET xAutoN = xAutoN + 1;
			 INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			 (xNoGL, @SKTgl, xAutoN, '24040100', CONCAT('Post SK - ',@DKJenis,' - Potongan Khusus - ',@CusNama), 0,oPotonganKhusus); SET xAutoN = xAutoN + 1; /*Hutang Biaya Potongan Khusus*/
		END IF;
	END IF;

	IF (LEFT(@PerusahaanNama,20) = 'CV. ANUGERAH PERDANA' AND @LokasiKode = 'PlatMerah') THEN
		IF oDKSCP > 0 THEN
			 INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			 (xNoGL, @SKTgl, xAutoN, '11110200', CONCAT('Post SK - ',@DKJenis,' - ',@CusNama), oDKSCP,0); SET xAutoN = xAutoN + 1; /*Piutang SCP MD*/
			 INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			 (xNoGL, @SKTgl, xAutoN, '40040400', CONCAT('Post SK - ',@DKJenis,' - ',@CusNama), 0,oDKSCP); SET xAutoN = xAutoN + 1; /*Pdp Operasional*/
		END IF;
	ELSE
		IF oDKSCP > 0 THEN /*Dihapus oleh bu wawiek*/
			 SET oDKSCP = oDKSCP;
			/*
			scp = scp + InsertGLit(JMNumber, Tgl, No, "11110200", Pesan + " - " + MyCusNama, MyDKSCP, 0) : No += 1 'Piutang SCP MD
			scp = scp + InsertGLit(JMNumber, Tgl, No, "40990000", Pesan + " - " + MyCusNama, 0, MyDKSCP) : No += 1 'Pdp Operasional
			*/
		END IF;
	END IF;

	IF (LEFT(@PerusahaanNama,20) = 'CV. ANUGERAH PERDANA' AND @LokasiKode = 'PlatMerah') THEN
		IF oDKScheme > 0 THEN
			 INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			 (xNoGL, @SKTgl, xAutoN, '11112200', CONCAT('Post SK - ',@DKJenis,' - ',@CusNama), oDKScheme,0); SET xAutoN = xAutoN + 1; /*Piutang AHM-MD GC*/
			 INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			 (xNoGL, @SKTgl, xAutoN, '40040500', CONCAT('Post SK - ',@DKJenis,' - ',@CusNama), 0,oDKScheme); SET xAutoN = xAutoN + 1; /*Pendapatan GC AHM*/
		END IF;
	ELSE
		/*
		'1109XX00	Piutang Subsidi/Scheme Leasing
		'4001XX00	PENDapatan Subsidi/Scheme Leasing
		'sch 1 = Piut Subs vs Pdpt Oprs
		'sch 2 = Piut Project Saving  vs Pdpt Oprs
		*/
		SELECT NoAccount INTO @COAPiutSubsLease FROM traccount WHERE NamaAccount LIKE UPPER(CONCAT('%',LEFT(@LeaseKode,3),'%')) AND JenisAccount ='Detail' AND NoParent = '11090000' AND NamaAccount LIKE '%Leasing%';
		SELECT NoAccount INTO @COAPiutProjectSaving FROM traccount WHERE NamaAccount LIKE UPPER(CONCAT('%',LEFT(@LeaseKode,3),'%')) AND JenisAccount ='Detail' AND NoParent = '11090000' AND NamaAccount LIKE '%Project Saving%';
		SELECT NoAccount INTO @COAPdpSubsLease FROM traccount WHERE NamaAccount LIKE UPPER(CONCAT('%',LEFT(@LeaseKode,3),'%')) AND JenisAccount ='Detail' AND NoParent = '40010000';

		IF oDKScheme > 0 THEN
			INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			(xNoGL, @SKTgl, xAutoN, @COAPiutSubsLease, CONCAT('Post SK - ',@DKJenis,' - ',@CusNama), oDKScheme,0); SET xAutoN = xAutoN + 1; /*Piut Subs Leasing*/
		END IF;
		IF oDKScheme2 > 0 THEN
			INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			(xNoGL, @SKTgl, xAutoN, @COAPiutProjectSaving, CONCAT('Post SK - ',@DKJenis,' - ',@CusNama), oDKScheme2,0); SET xAutoN = xAutoN + 1; /*Piut Subs Leasing*/
		END IF;
		IF oDKScheme + oDKScheme2 > 0 THEN
			INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			(xNoGL, @SKTgl, xAutoN, @COAPdpSubsLease, CONCAT('Post SK - ',@DKJenis,' - ',@CusNama), 0,oDKScheme + oDKScheme2); SET xAutoN = xAutoN + 1; /*Pdp Oprs*/
		END IF;
	END IF;

	IF @DKJenis = 'Kredit' THEN
		SELECT IFNULL(Nilai,0) INTO oBiayaMaterai FROM tdvariabel WHERE Nama = 'Materai';
		IF oBiayaMaterai > 0 THEN
			 INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			 (xNoGL, @SKTgl, xAutoN, '60130000', CONCAT('Post SK - ',@DKJenis,' - ',@CusNama), oBiayaMaterai,0); SET xAutoN = xAutoN + 1;
			 INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
			 (xNoGL, @SKTgl, xAutoN, '11150500', CONCAT('Post SK - ',@DKJenis,' - ',@CusNama), 0,oBiayaMaterai); SET xAutoN = xAutoN + 1;
		END IF;
	END IF;

	SELECT IFNULL(Biaya,0) , COADebet, COAKredit INTO @BiayaGesek, @COADebet, @COAKredit FROM tdcekfisik WHERE Kabupaten = @CusKabupaten ;
	IF @BiayaGesek > 0 THEN
	  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
	  (xNoGL, @SKTgl, xAutoN, @COADebet, CONCAT('Post SK - ',@DKJenis,' - ',@CusNama), @BiayaGesek,0); SET xAutoN = xAutoN + 1;
	  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
	  (xNoGL, @SKTgl, xAutoN, @COAKredit, CONCAT('Post SK - ',@DKJenis,' - ',@CusNama), 0,@BiayaGesek); SET xAutoN = xAutoN + 1;
	END IF;

	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;
	UPDATE ttSK SET NoGL = xNoGL  WHERE SKNo = xSKNo;

	CALL jHutangKreditPiutangDebet(xNoGL);

	CALL jUpdateTvHarianSK (xSKNo, xNoGL ,@SKTgl);

	DELETE FROM ttglhpit WHERE NoGL = xNoGL;

	/*Link Hutang */
	DROP TEMPORARY TABLE IF EXISTS `HutangLink`;
	CREATE TEMPORARY TABLE IF NOT EXISTS `HutangLink` AS
	SELECT KMNo, KMLINK , KMJenis, Via , KMNominal, KMTgl, KMJam, NoGL, NoAccount, KreditGL FROM (
	SELECT KMNo, KMLINK , ttkm.KMJenis, 'KM' AS Via , KMNominal, KMTgl, KMJam, ttkm.NoGL, ttgeneralledgerit.NoAccount,ttgeneralledgerit.KreditGL  FROM ttkm
	INNER JOIN ttgeneralledgerhd ON ttkm.NoGL = ttgeneralledgerhd.NoGL
	INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL
	INNER JOIN (SELECT NoAccount, DebetGL FROM ttgeneralledgerit WHERE DebetGL > 0 AND NoAccount = '24050100' AND ttgeneralledgerit.NoGL = xNoGL) GLNow  ON GLNow.NoAccount = ttgeneralledgerit.NoAccount
	WHERE (KMLink = @DKNo OR KMLINK IN (SELECT INNo FROM ttin WHERE DKNo = @DKNo)) AND ttgeneralledgerit.KreditGL <> 0 AND ttgeneralledgerit.DebetGL = 0
	UNION
	SELECT ttbmit.BMNo AS KMNo , ttbmit.DKNo AS KMLINK, ttbmhd.BMJenis AS KMJenis, 'BM' AS Via, ttbmit.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, ttbmhd.NoGL, ttgeneralledgerit.NoAccount, ttgeneralledgerit.KreditGL FROM ttbmhd INNER JOIN ttbmit ON ttbmhd.BMNo = ttbmit.BMNo
	INNER JOIN ttgeneralledgerhd ON ttbmhd.NoGL = ttgeneralledgerhd.NoGL
	INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL
	INNER JOIN (SELECT NoAccount, DebetGL FROM ttgeneralledgerit WHERE DebetGL > 0 AND NoAccount = '24050100' AND ttgeneralledgerit.NoGL = xNoGL) GLNow  ON GLNow.NoAccount = ttgeneralledgerit.NoAccount
	WHERE (ttbmit.DKNo = @DKNo OR ttbmit.DKNo IN (SELECT INNo FROM ttin WHERE DKNo = @DKNo)) AND ttgeneralledgerit.KreditGL <> 0 AND ttgeneralledgerit.DebetGL = 0
	UNION
	SELECT ttbmitpm.BMNo AS KMNo , ttbmitpm.DKNo AS KMLINK, ttbmitpm.BMJenis AS KMJenis, 'BM' AS Via, ttbmitpm.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, ttbmhd.NoGL, ttgeneralledgerit.NoAccount, ttgeneralledgerit.KreditGL FROM ttbmhd INNER JOIN ttbmitpm ON ttbmhd.BMNo = ttbmitpm.BMNo
	INNER JOIN ttgeneralledgerhd ON ttbmhd.NoGL = ttgeneralledgerhd.NoGL
	INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL
	INNER JOIN (SELECT NoAccount, DebetGL FROM ttgeneralledgerit WHERE DebetGL > 0 AND NoAccount = '24050100' AND ttgeneralledgerit.NoGL = xNoGL) GLNow  ON GLNow.NoAccount = ttgeneralledgerit.NoAccount
	WHERE ttbmitpm.BMJenis IN ('Piutang UM','Tunai','Kredit','Inden','Piutang Sisa','Leasing','Scheme')
	AND (ttbmitpm.DKNo = @DKNo OR ttbmitpm.DKNo IN (SELECT INNo FROM ttin WHERE DKNo = @DKNo)) AND ttgeneralledgerit.KreditGL <> 0 AND ttgeneralledgerit.DebetGL = 0
	) A GROUP BY KMno, NoAccount, NoGL, KMLink;
	INSERT INTO ttglhpit SELECT xNoGL, @SKTgl, KMno, '24050100', 'Hutang', KreditGL FROM HutangLink;

	/*Link Piutang */
	DROP TEMPORARY TABLE IF EXISTS `PiutangLink`;
	CREATE TEMPORARY TABLE IF NOT EXISTS `PiutangLink` AS
	SELECT KMNo, KMLink, KMJenis, Via, KMNominal, KMTgl, KMJam, NoGL, NoAccount, KreditGL FROM (
	SELECT KMNo, KMLINK , ttkm.KMJenis, 'KM' AS Via , KMNominal, KMTgl, KMJam, ttkm.NoGL, ttgeneralledgerit.NoAccount, ttgeneralledgerit.KreditGL  FROM ttkm
	INNER JOIN ttgeneralledgerhd ON ttkm.NoGL = ttgeneralledgerhd.NoGL
	INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL
	WHERE (ttgeneralledgerit.NoAccount IN (SELECT ttgeneralledgerit.NoAccount FROM ttgeneralledgerit WHERE KreditGL > 0 AND ttgeneralledgerit.NoAccount = '11060100' AND ttgeneralledgerit.NoGL = xNoGL))
	  AND (KMLink = @DKNo OR ttkm.KMLINK IN (SELECT INNo FROM ttin WHERE DKNo = @DKNo))
	  AND ttgeneralledgerit.KreditGL <> 0  AND ttgeneralledgerit.DebetGL = 0
	UNION
	SELECT ttbmit.BMNo AS KMNo , ttbmit.DKNo AS KMLINK, ttbmhd.BMJenis AS KMJenis, 'BM' AS Via, ttbmit.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, ttbmhd.NoGL, ttgeneralledgerit.NoAccount ,ttgeneralledgerit.KreditGL FROM ttbmhd INNER JOIN ttbmit ON ttbmhd.BMNo = ttbmit.BMNo
	INNER JOIN ttgeneralledgerhd ON ttbmhd.NoGL = ttgeneralledgerhd.NoGL
	INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL
	WHERE (ttgeneralledgerit.NoAccount IN (SELECT ttgeneralledgerit.NoAccount FROM ttgeneralledgerit WHERE KreditGL > 0 AND ttgeneralledgerit.NoAccount = '11060100' AND ttgeneralledgerit.NoGL = xNoGL))
	  AND (ttbmit.DKNo = @DKNo OR ttbmit.DKNo IN (SELECT INNo FROM ttin WHERE DKNo = @DKNo))
	  AND ttgeneralledgerit.KreditGL <> 0  AND ttgeneralledgerit.DebetGL = 0
	UNION
	SELECT ttbmitpm.BMNo AS KMNo , ttbmitpm.DKNo AS KMLINK, ttbmitpm.BMJenis AS KMJenis, 'BM' AS Via, ttbmitpm.BMBayar AS KMNominal, ttbmhd.BMTgl AS KMTgl, ttbmhd.BMJam AS KMjam, ttbmhd.NoGL, ttgeneralledgerit.NoAccount ,ttgeneralledgerit.KreditGL FROM ttbmhd INNER JOIN ttbmitpm ON ttbmhd.BMNo = ttbmitpm.BMNo
	INNER JOIN ttgeneralledgerhd ON ttbmhd.NoGL = ttgeneralledgerhd.NoGL
	INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL
	WHERE ttbmitpm.BMJenis IN ('Piutang UM','Tunai','Kredit','Inden','Piutang Sisa','Leasing','Scheme')
	AND (ttgeneralledgerit.NoAccount IN (SELECT ttgeneralledgerit.NoAccount FROM ttgeneralledgerit WHERE KreditGL > 0 AND ttgeneralledgerit.NoAccount = '11060100' AND ttgeneralledgerit.NoGL = xNoGL))
	  AND (ttbmitpm.DKNo = @DKNo OR ttbmitpm.DKNo IN (SELECT INNo FROM ttin WHERE DKNo = @DKNo))
	  AND ttgeneralledgerit.KreditGL <> 0  AND ttgeneralledgerit.DebetGL = 0
	) A GROUP BY KMno, NoAccount, NoGL, KMLink ;
	INSERT INTO ttglhpit SELECT xNoGL, @SKTgl, KMno, '11060100', 'Piutang', KreditGL FROM PiutangLink;

	SELECT GROUP_CONCAT(GLLink) INTO oGLLink FROM ttglhpit WHERE NoGL = xNoGL GROUP BY NoGL;
	IF LENGTH(oGLLink) > 18 THEN
		 UPDATE ttgeneralledgerhd SET HPLink = 'Many'  WHERE NoGL = xNoGL;
	ELSE
		 UPDATE ttgeneralledgerhd SET HPLink = oGLLink  WHERE NoGL = xNoGL;
	END IF;


END$$

DELIMITER ;

