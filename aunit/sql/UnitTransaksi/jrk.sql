DELIMITER $$

DROP PROCEDURE IF EXISTS `jrk`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jrk`(IN xRKNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';

	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xRKNo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;


	SELECT  ttrk.RKNo, ttrk.RKTgl, ttrk.LokasiKode, ttrk.RKMemo, ttrk.UserID, ttrk.RKJam, 
   ttrk.DKNo, ttrk.SKNo, ttrk.NoGL, tmotor.RKHarga, ttsk.SKTgl, tdcustomer.CusNama
   INTO @RKNo, @RKTgl, @LokasiKode, @RKMemo, @UserID, @RKJam, @DKNo, @SKNo, @NoGL, @RKHarga, @SKTgl, @CusNama
	FROM ttrk 
	LEFT OUTER JOIN tmotor ON ttrk.RKNo = tmotor.RKNo 
	LEFT OUTER JOIN ttsk ON ttrk.SKNo = ttsk.SKNo 
	LEFT OUTER JOIN tdlokasi ON ttrk.LokasiKode = tdlokasi.LokasiKode 
	LEFT OUTER JOIN ttdk ON ttrk.DKNo = ttdk.DKNo 
	LEFT OUTER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
	WHERE (ttrk.RKNo = xRKNo);

	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;

	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL; DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;DELETE FROM ttglhpit WHERE NoGL = xNoGL;
   DELETE FROM ttgeneralledgerhd WHERE GLLink = xRKNo;

	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, @RKTgl, 'RK', CONCAT('Post Retur Konsumen',@CusNama), 0, 0, xRKNo, xUserID, '--', xGLValid);

	#Item
	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttRK SET NoGL = xNoGL  WHERE RKNo = xRKNo;
	
	#PerbaikanFBHarga
	#SetHutangKreditPiutangDebet
	#LinkHutangPiutang
	#UpdateTvHarianRK
END$$

DELIMITER ;

