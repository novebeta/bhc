DELIMITER $$
DROP PROCEDURE IF EXISTS `fbmit`$$
CREATE DEFINER=`root`@`%` PROCEDURE `fbmit`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
IN xBMNo VARCHAR(12),
IN xDKNo VARCHAR(12),
IN xBMBayar DECIMAL(14,2),
IN xBMNoOLD VARCHAR(12),
IN xDKNoOLD VARCHAR(12)
)
BEGIN
	CASE xAction
		WHEN "Insert" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBKNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBKNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert
			IF oStatus = 0 THEN		
				INSERT INTO ttbmit (BMNo, DKNo, BMBayar) VALUES (xBMNo, xDKNo, xBMBayar);
				SET oKeterangan = CONCAT('Berhasil menambahkan data Bank Masuk ', xBKNo,' - ', xDKNo, ' - ', FORMAT(xBMBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;		
		END;
		WHEN "Loop" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Insert
			IF oStatus = 0 THEN		
				INSERT INTO ttbmit (BMNo, DKNo, BMBayar) VALUES (xBMNo, xDKNo, xBMBayar);
				SET oKeterangan = CONCAT('Berhasil menambahkan data Bank Masuk ', xBKNo,' - ', xDKNo, ' - ', FORMAT(xBMBayar,2));
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;	
		END;
		WHEN "Update" THEN
		BEGIN
			 SET oStatus = 0;
			 
 			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Update
			IF oStatus = 0 THEN
				UPDATE ttbmit SET BMNo = xBMNo, DKNo = xDKNo, BMBayar = xBMBayar  WHERE BMNo = xBKNoOLD AND DKNo = xDKNoOLD;
				SET oKeterangan = CONCAT("Berhasil mengubah data bank masuk : ", xBMNo , ' - ' ,xDKNo);
			ELSE
				SET oKeterangan = CONCAT("Gagal mengubah data bank masuk : ", xBMNo , ' - ' ,xDKNo);
			END IF;
		END;
		WHEN "Delete" THEN
		BEGIN
			SET oStatus = 0;
			
			#Cek Jurnal
			SELECT NoGL, GLValid INTO @NoGL, @GLValid FROM ttgeneralledgerhd WHERE GLLink = xBMNo;
			IF @GLValid = 'Sudah' THEN	#Jurnal telah tervalidasi data tidak boleh dihapus
				SET oStatus = 1; SET oKeterangan = CONCAT("No :", xBMNo, " dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;

			#Delete
			IF oStatus = 0 THEN
				DELETE FROM ttbmit WHERE DKNo = xDKNo AND BMNo = xBMNo;
				SET oKeterangan = CONCAT("Berhasil menghapus data bank masuk : ", xBMNo , ' - ' ,xDKNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;					 
		END;
	END CASE;
END$$

DELIMITER ;

