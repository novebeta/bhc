DELIMITER $$
DROP PROCEDURE IF EXISTS `fabhd`$$
CREATE DEFINER=`root`@`%` PROCEDURE `fabhd`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan TEXT,
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xABNo VARCHAR(100),
IN xABNoBaru VARCHAR(100),
IN xABTgl DATE,
IN xABMemo VARCHAR(100))
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Daftar Pengajuan Berkas");
		END;
		WHEN "Load" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Load Pengajuan Berkas No : ",xABNo);
		END;
		WHEN "Insert" THEN
		BEGIN
			 SET @AutoNo = CONCAT('AB',RIGHT(YEAR(NOW()),2),'-');
			 SELECT CONCAT(@AutoNo,LPAD((RIGHT(ABNo,5)+1),5,0)) INTO @AutoNo FROM ttabhd WHERE ((ABNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(ABNo) = 10) ORDER BY ABNo DESC LIMIT 1;
			 IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('AB',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			 SET xABNo = @AutoNo;
			 SET oStatus = 0;
			 SET oKeterangan = CONCAT("Tambah data baru Pengajuan Berkas No: ",xABNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = '';
			SET @AutoNo = 'Edit';
			SET @ABNoOLD = xABNo;

	
			IF LOCATE('=',xABNo) <> 0   THEN 
				SET @AutoNo = CONCAT('AB',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(ABNo,5)+1),5,0)) INTO @AutoNo FROM ttabhd WHERE ((ABNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(ABNo) = 10) ORDER BY ABNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('AB',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xABNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit'; 
				SET xABNoBaru = xABNo;
			END IF;
							
			IF oStatus = 0 THEN
				#Update Utama				
				UPDATE ttabhd
				SET ABNo = xABNoBaru, ABTgl = xABTgl, ABMemo = xABMemo, UserID = xUserID
				WHERE ABNo = xABNo;
				SET xABNo = xABNoBaru;
				
				IF @AutoNo = 'Edit' THEN 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID ,	 'Edit' , 'Pengajuan Berkas','ttabhd', xABNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan data Pengajuan Berkas  No : ", xABNo);
				ELSE 
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Pengajuan Berkas','ttabhd', xABNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menambah data Pengajuan Berkas No : ", xABNo);
				END IF;
			ELSEIF oStatus = 1 THEN
				SET oKeterangan = CONCAT("Gagal, silahkan melengkapi data Pengajuan Berkas No: ", xABNo);
			END IF;

		END;
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = '';
			SET oStatus = 0;
			
			#Update Proses
			IF oStatus = 0 THEN
				DELETE FROM ttabhd WHERE ABNo = xABNo;
				DELETE FROM ttabit WHERE ABNo = xABNo;
				INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
				(NOW(), xUserID , 'Hapus' , 'Pengajuan Berkas','ttabhd', xABNo); 
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Pengajuan Berkas No: ",xABNo);
			ELSE
				SET oKeterangan = oKeterangan;
			END IF;			
		END;
		WHEN "Cancel" THEN
		BEGIN
			SET oStatus = 1;
			IF LOCATE('=',xABNo) <> 0 THEN
				DELETE FROM ttabhd WHERE ABNo = xABNo;
				SET oKeterangan = CONCAT("Batal menyimpan data Pengajuan Berkas No: ", xABNoBaru);
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Pengajuan Berkas No: ", xABNo);
			END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
		END;
		WHEN "Print" THEN
		BEGIN
			IF xCetak = "1" THEN
				SELECT ABNo, ABTgl, ABMemo, UserID FROM ttabhd WHERE ttabhd.ABNo = xABNo;
			END IF;
			IF xCetak = "2" THEN
				SELECT ttabit.ABNo, ttabit.DKNo, ttdk.DKTgl, tdcustomer.CusNama, tmotor.MotorType, tmotor.MotorNoMesin, tdmotortype.MotorNama, 
				tdcustomer.CusKelurahan, ttdk.BBN, tdcustomer.CusKabupaten, tmotor.MotorTahun, tmotor.SKNo
				FROM ttabit 
				INNER JOIN ttdk ON ttabit.DKNo = ttdk.DKNo 
				INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 
				INNER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
				INNER JOIN	tdmotortype ON tmotor.MotorType = tdmotortype.MotorType
				WHERE   (ttabit.ABNo = xABNo);
			END IF;
			 
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Cetak data Pengajuan Berkas No: ", xABNo);
			
			INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
			(NOW(), xUserID , 'Cetak' , 'Pengajuan Berkas','ttabhd', xABNo); 
		END;
	END CASE;
END$$

DELIMITER ;

