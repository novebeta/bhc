DELIMITER $$
DROP PROCEDURE IF EXISTS `cMotorAutoN`$$
CREATE DEFINER=`root`@`%` PROCEDURE `cMotorAutoN`(IN xKodeTrans VARCHAR(10), IN xNoTrans VARCHAR(20),  IN xMotorNoMesin VARCHAR(25))
BEGIN
	DECLARE myMotorNoMesin VARCHAR(100);

	IF xKodeTrans = 'FB' THEN
		blockFB: BEGIN
			DECLARE doneFB INT DEFAULT 0;  	
			DECLARE curFB CURSOR FOR SELECT MotorNoMesin FROM tmotor WHERE FBNo = xNoTrans AND MotorNoMesin LIKE CONCAT('%',xMotorNoMesin) GROUP BY MotorNoMesin;	
			DECLARE CONTINUE HANDLER FOR NOT FOUND SET doneFB = 1;
			OPEN curFB;
			REPEAT FETCH curFB INTO myMotorNoMesin;
				SET @row_number = -1; 
				UPDATE tmotor INNER JOIN 
				(SELECT  (@row_number:=@row_number + 1) AS num, MotorAutoN, MotorNoMesin FROM tmotor WHERE MotorNoMesin = myMotorNoMesin ORDER BY MotorAutoN)  A
				ON A.MotorNoMesin = tmotor.MotorNoMesin AND A.MotorAutoN = tmotor.MotorAutoN
				SET tmotor.MotorAutoN = A.num;
			UNTIL doneFB END REPEAT;
			CLOSE curFB;
		END blockFB;
	END IF;

	IF xKodeTrans = 'PD' THEN
		blockPD: BEGIN
			DECLARE donePD INT DEFAULT 0;  	
			DECLARE curPD CURSOR FOR SELECT MotorNoMesin FROM tmotor WHERE PDNo = xNoTrans AND MotorNoMesin LIKE CONCAT('%',xMotorNoMesin) GROUP BY MotorNoMesin;	
			DECLARE CONTINUE HANDLER FOR NOT FOUND SET donePD = 1;
			OPEN curPD;
			REPEAT FETCH curPD INTO myMotorNoMesin;
				SET @row_number = -1; 
				UPDATE tmotor INNER JOIN 
				(SELECT  (@row_number:=@row_number + 1) AS num, MotorAutoN, MotorNoMesin FROM tmotor WHERE MotorNoMesin = myMotorNoMesin ORDER BY MotorAutoN)  A
				ON A.MotorNoMesin = tmotor.MotorNoMesin AND A.MotorAutoN = tmotor.MotorAutoN
				SET tmotor.MotorAutoN = A.num;
			UNTIL donePD END REPEAT;
			CLOSE curPD;
		END blockPD;
	END IF;

	#Untuk motor yg sudah ada SR nya, sementara belum di terapkan
	IF xKodeTrans = 'SS' THEN
		blockSS: BEGIN
			DECLARE doneSS INT DEFAULT 0;  
			DECLARE curSS CURSOR FOR SELECT MotorNoMesin FROM tmotor WHERE SSNo = xNoTrans AND MotorNoMesin LIKE CONCAT('%',xMotorNoMesin) GROUP BY MotorNoMesin;	
			DECLARE CONTINUE HANDLER FOR NOT FOUND SET doneSS = 1;
			OPEN curSS;
			REPEAT FETCH curSS INTO myMotorNoMesin;
				SET @row_number = -1; 
				UPDATE tmotor INNER JOIN 
				(SELECT  (@row_number:=@row_number + 1) AS num, MotorAutoN, MotorNoMesin FROM tmotor WHERE MotorNoMesin = myMotorNoMesin ORDER BY MotorAutoN)  A
				ON A.MotorNoMesin = tmotor.MotorNoMesin AND A.MotorAutoN = tmotor.MotorAutoN
				SET tmotor.MotorAutoN = A.num;
			UNTIL doneSS END REPEAT;
			CLOSE curSS;
		END blockSS;
	END IF;

END$$
DELIMITER ;

