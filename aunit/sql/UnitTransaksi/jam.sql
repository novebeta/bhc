DELIMITER $$
DROP PROCEDURE IF EXISTS `jam`$$
CREATE DEFINER=`root`@`%` PROCEDURE `jam`(IN xAMNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE MyAutoN INTEGER DEFAULT 1;
	DECLARE TotAMHarga DOUBLE DEFAULT 0;
	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xAMNo ORDER BY NOGL DESC LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;
	
	SELECT IFNULL(SUM(MMHarga),0) INTO TotAMHarga FROM tmotor 
	INNER JOIN ttamit ON ttamit.MotorAutoN = tmotor.MotorAutoN AND ttamit.MotorNoMesin = tmotor.MotorNoMesin
	WHERE AMNo = xAMNo;
	UPDATE ttamhd SET AMTotal = TotAMHarga WHERE AMNo = xAMNo;
	
	SELECT AMNo, AMTgl, AMJam, AMMemo, AMTotal, NoGL, UserID, MotorTahun, FBNo 
	INTO	@AMNo, @AMTgl, @AMJam, @AMMemo, @AMTotal, @NoGL, @UserID, @MotorTahun, @FBNo 
	FROM ttamhd WHERE AMNo = xAMNo;
	
	#Numbering JT
	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
    SET xNoGL =  @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;
	
	#Delete
	DELETE FROM ttgeneralledgerhd WHERE GLLink = xAMNo;
	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL;  
	DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;  
	DELETE FROM ttglhpit WHERE NoGL = xNoGL;

	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, @AMTgl, 'AM', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',@FBNo), TotAMHarga, TotAMHarga, xAMNo, xUserID, '--', xGLValid);
	
	#Item
	IF TotAMHarga < 0 THEN
		INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		(xNoGL, @AMTgl, 1, '50000000', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',@FBNo), -TotAMHarga, 0 );
		INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		(xNoGL, @AMTgl, 2, '11150100', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',@FBNo), 0, -TotAMHarga);
	ELSE
		INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		(xNoGL, @AMTgl, 1, '11150100', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',@FBNo), TotAMHarga, 0 );
		INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		(xNoGL, @AMTgl, 2, '50000000', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',@FBNo), 0, TotAMHarga);
	END IF;

	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttamhd SET NoGL = xNoGL  WHERE AMNo = xAMNo;
	
END$$

DELIMITER ;