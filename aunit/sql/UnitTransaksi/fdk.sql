DELIMITER $$
DROP PROCEDURE IF EXISTS `fdk`$$
CREATE DEFINER=`root`@`%` PROCEDURE `fdk`(
IN xAction VARCHAR(10),
OUT oStatus INT,
OUT oKeterangan VARCHAR(150),
IN xUserID VARCHAR(15),
IN xKodeAkses VARCHAR(15),
IN xLokasiKode VARCHAR(15),
INOUT xDKNo VARCHAR(10),
IN xDKNoBaru VARCHAR(10),
IN xSalesKode VARCHAR(10),
IN xTeamKode VARCHAR(15),
IN xDKTgl DATE,
IN xINNo VARCHAR(10),
IN xLeaseKode VARCHAR(10),
IN xSPKNo VARCHAR(15),
IN xDKSurveyor VARCHAR(50),
IN xNoSK VARCHAR(100),
IN xCusNama VARCHAR(50),
IN xCusKode VARCHAR(10),
IN xCusKTP VARCHAR(18),
IN xCusKabupaten VARCHAR(50),
IN xCusAlamat VARCHAR(75),
IN xCusRT VARCHAR(3),
IN xCusRW VARCHAR(3),
IN xCusKecamatan VARCHAR(50),
IN xCusTelepon VARCHAR(30),
IN xCusStatusHP VARCHAR(10),
IN xCusKodePos VARCHAR(35),
IN xCusKelurahan VARCHAR(50),
IN xCusStatusRumah VARCHAR(15),
IN xCusKodeKons VARCHAR(35),
IN xCusSex VARCHAR(12),
IN xCusTempatLhr VARCHAR(30),
IN xCusTglLhr DATE,
IN xCusAgama VARCHAR(15),
IN xCusPekerjaan VARCHAR(35),
IN xCusPendidikan VARCHAR(20),
IN xCusPengeluaran VARCHAR(35),
IN xCusPembeliNama VARCHAR(50),
IN xCusPembeliKTP VARCHAR(18),
IN xCusKK VARCHAR(16),
IN xROCount DECIMAL(4,0),
IN xCusPembeliAlamat VARCHAR(75),
IN xDKMerkSblmnya VARCHAR(25),
IN xDKMotorUntuk VARCHAR(25),
IN xDKJenis VARCHAR(25),
IN xDKMotorOleh VARCHAR(25),
IN xMotorType VARCHAR(50),
IN xMotorTahun DECIMAL(4,0),
IN xMotorWarna VARCHAR(35),
IN xDKSCP DECIMAL(12,2),
IN xMotorNoMesin VARCHAR(25),
IN xNoRangka VARCHAR(25),
IN xDKScheme DECIMAL(12,2),
IN xMotorNama VARCHAR(100),
IN xMotorKategori VARCHAR(15),
IN xDKScheme2 DECIMAL(12,2),
IN xProgramNama VARCHAR(25),
IN xDKPengajuanBBN VARCHAR(5),
IN xNDPPO DECIMAL(12,2),
IN xDKHarga DECIMAL(11,2),
IN xPrgSubsSupplier DECIMAL(11,2),
IN xPrgSubsDealer DECIMAL(11,2),
IN xPrgSubsFincoy DECIMAL(11,2),
IN xProgramSubsidi DECIMAL(11,2),
IN xDKDPLLeasing DECIMAL(11,2),
IN xDKDPInden DECIMAL(11,2),
IN xDKDPTerima DECIMAL(11,2),
IN xDKDPTotal DECIMAL(11,2),
IN xReturHarga DECIMAL(11,2),
IN xDKMediator VARCHAR(50),
IN xPotonganHarga DECIMAL(11,2),
IN xDKNetto DECIMAL(11,2),
IN xInsentifSales DECIMAL(11,2),
IN xBBN DECIMAL(11,2),
IN xBBNPlus DECIMAL(11,2),
IN xDKTenor DECIMAL(2,0),
IN xPotonganKhusus DECIMAL(11,2),
IN xJaket DECIMAL(11,2),
IN xNamaHadiah VARCHAR(100),
IN xDKAngsuran DECIMAL(11,2),
IN xDKMemo VARCHAR(100),
IN xHadiah2 VARCHAR(50),
IN xDKTOP DECIMAL(3,0),
IN xDKJenisSblmnya VARCHAR(25),
IN xCusEmail VARCHAR(50) ,
IN xCusTelepon2 VARCHAR(30) ,
IN xCusNPWP VARCHAR(20) ,
IN xMotorAutoN SMALLINT
)
BEGIN
	CASE xAction
		WHEN "Display" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Daftar Data Konsumen [DK] ");
		END;
		WHEN "Load" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Load data Data Konsumen [DK] No: ",xDKNo);
			#xCusProvinsi, xCusTglBeli, xCusKeterangan, 
			#xCusEmail, xCusTelepon2, xCusNPWP
		END;
		WHEN "Insert" THEN
		BEGIN
			SET @AutoNo = CONCAT('DK',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNo,LPAD((RIGHT(DKNo,5)+1),5,0)) INTO @AutoNo FROM ttDK WHERE ((DKNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(DKNo) = 10) ORDER BY DKNo DESC LIMIT 1;
			IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('DK',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
			SET xDKNo = @AutoNo;
			SET oStatus = 0;
			SET oKeterangan = CONCAT("Tambah data baru Data Konsumen [DK] No: ",@AutoNo);
		END;
		WHEN "Update" THEN
		BEGIN
			SET oStatus = 0;
			SET oKeterangan = "";
			SET @AutoNo = 'Edit';
			SET @DKNoOLD = xDKNo;

			#SET oKeterangan = CONCAT_WS(' - ',xCusKode, xCusKTP, xCusNama,xCusAlamat, xCusRT, xCusRW,  xCusKabupaten, xCusKecamatan, xCusKelurahan, xCusKodePos, xCusTelepon);
			#SET oKeterangan = CONCAT_WS(' - ', xCusSex, xCusTempatLhr, xCusTglLhr, xCusAgama, xCusPekerjaan, xCusPendidikan); 
			#SET oKeterangan = CONCAT_WS(' - ', xCusPengeluaran, xCusKodeKons, xCusStatusHP, xCusStatusRumah, xCusPembeliNama, xCusPembeliKTP, xCusPembeliAlamat, xCusKK);
					
			
			#Generate new auto number
			IF LOCATE('=',xDKNo) <> 0  THEN 
				SET @AutoNo = CONCAT('DK',RIGHT(YEAR(NOW()),2),'-');
				SELECT CONCAT(@AutoNo,LPAD((RIGHT(DKNo,5)+1),5,0)) INTO @AutoNo FROM ttdk WHERE ((DKNo LIKE CONCAT(@AutoNo,'%')) AND LENGTH(DKNo) = 10) ORDER BY DKNo DESC LIMIT 1;
				IF LENGTH(@AutoNo) <> 10 THEN SET @AutoNo = CONCAT('DK',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
				SET xDKNoBaru = @AutoNo;
			ELSE
				SET @AutoNo = 'Edit';
				SET xDKNoBaru = xDKNo;
			END IF;
			
			#Cek Jurnal Validasi
			SELECT NoGL, GLValid, GLLink INTO @NoGL, @GLValid, @GLLink FROM ttgeneralledgerhd INNER JOIN tmotor ON tmotor.SKNo = GLLink WHERE tmotor.DKNo = xDKNo;
			IF @GLValid = 'Sudah' THEN	
			  SET oStatus = 1; SET oKeterangan = CONCAT("No DK: ", xDKNo, " sudah memiliki SK: ",@GLLink," dengan No JT: ",@NoGL," sudah tervalidasi");
			END IF;
			
			#Cek PO
			IF xKodeAkses <> "Admin" AND xKodeAkses <> "Auditor" AND xKodeAkses <> "Akuntansi" AND xKodeAkses <> "ADH" THEN
				SELECT  DKPONo INTO @DKPONo FROM ttdk WHERE DKNo = xDKNo;
				IF @DKPONo = '--' THEN
					SET oStatus = 1;
					SET oKeterangan = 'Nomor PO telah diisi';
				END IF;
			END IF;
					
			SET oStatus = 0;
			IF oStatus = 0 THEN  #Update
				#Buat Data Konsumen
				IF xCusKode = '--' THEN
					SET @CusKode = CONCAT('KS',RIGHT(YEAR(NOW()),2),'-');
					SELECT CONCAT(@CusKode,LPAD((RIGHT(CusKode,5)+1),5,0)) INTO @CusKode FROM tdcustomer WHERE ((CusKode LIKE CONCAT(@CusKode,'%')) AND LENGTH(CusKode) = 10) ORDER BY CusKode DESC LIMIT 1;
					IF LENGTH(@CusKode) <> 10 THEN SET @CusKode = CONCAT('IN',RIGHT(YEAR(NOW()),2),'-00001');  END IF;				
					
					SELECT Provinsi INTO @Provinsi FROM tdarea WHERE Kabupaten = xCusKabupaten LIMIT 1;								

					SELECT Kelurahan INTO @CusKelurahan FROM tdarea WHERE Kabupaten = xCusKabupaten AND Kecamatan = xCusKecamatan AND Kelurahan = xCusKelurahan LIMIT 1;
					IF @CusKelurahan IS NULL THEN
						SELECT Kelurahan INTO xCusKelurahan FROM tdarea WHERE Kabupaten = xCusKabupaten AND Kecamatan = xCusKecamatan LIMIT 1;
					END IF;

					INSERT INTO tdcustomer (CusKode, CusKTP, CusNama, CusAlamat, CusRT, CusRW, 
					CusProvinsi, CusKabupaten, CusKecamatan, CusKelurahan, CusKodePos,
					CusTelepon, CusSex,CusTempatLhr, CusTglLhr, CusAgama, CusPekerjaan, CusPendidikan, CusPengeluaran, 
					CusTglBeli, CusKeterangan, CusEmail, CusKodeKons, CusStatusHP, CusStatusRumah, 
					CusPembeliNama, CusPembeliKTP, CusPembeliAlamat, 
					CusKK, CusTelepon2, CusNPWP)
					VALUES (@CusKode, xCusKTP, fcapitalize(xCusNama), fcapitalize(xCusAlamat), xCusRT, xCusRW, 
					@Provinsi, xCusKabupaten, xCusKecamatan, xCusKelurahan, xCusKodePos, 
					xCusTelepon, xCusSex, xCusTempatLhr, xCusTglLhr, xCusAgama, xCusPekerjaan, xCusPendidikan, xCusPengeluaran, 
					'1900-01-01', '--', xCusEmail, xCusKodeKons, xCusStatusHP, xCusStatusRumah, 
					'--', '--', '--', 
					xCusKK, xCusTelepon2, xCusNPWP);
					
					SET xCusKode = @CusKode;
					UPDATE ttin SET CusKode = xCusKode WHERE INNo = xINNo;
				ELSE #Update Data Konsumen
					SELECT Provinsi INTO @Provinsi FROM tdarea WHERE Kabupaten = xCusKabupaten LIMIT 1;
				   UPDATE tdcustomer SET 
				   CusKTP = xCusKTP, CusNama = fcapitalize(xCusNama), CusAlamat = fcapitalize(xCusAlamat), CusRT = xCusRT, CusRW = xCusRW, 
				   CusProvinsi = @Provinsi, CusKabupaten = xCusKabupaten, CusKecamatan = xCusKecamatan, CusKelurahan = xCusKelurahan, CusKodePos = xCusKodePos, 
				   CusTelepon = xCusTelepon, CusSex = xCusSex, CusTempatLhr = xCusTempatLhr, CusTglLhr = xCusTglLhr, CusAgama = xCusAgama, 
				   CusPekerjaan = xCusPekerjaan, CusPendidikan = xCusPendidikan, CusPengeluaran = xCusPengeluaran,  CusEmail = xCusEmail, 
				   CusKodeKons = xCusKodeKons, CusStatusHP = xCusStatusHP, CusStatusRumah = xCusStatusRumah, 
				   CusKK = xCusKK, CusTelepon2 = xCusTelepon2, CusNPWP = xCusNPWP
					WHERE CusKode = xCusKode;
				END IF;
				
				#xDKLunas, xDKHPP 										#Program	
				#xPotonganAHM, 											#Unkown
				#xDKTglTagih, xDKPONo, xDKPOTgl, xDKPOLeasing, 	#form DK Status
				#xNoBukuServis												#from SK
			
				#SET oKeterangan = CONCAT_WS(' - ', xDKNo, xDKTgl, xSalesKode, xCusKode, xLeaseKode, xDKJenis, xDKMemo, xINNo, xUserID, xDKHarga);		
				#SET oKeterangan = CONCAT_WS(' - ', xDKDPTotal, xDKDPLLeasing, xDKDPTerima, xDKDPInden, xDKNetto, xProgramNama, xProgramSubsidi, xBBN, xJaket);
				#SET oKeterangan = CONCAT_WS(' - ',  xPrgSubsSupplier, xPrgSubsDealer, xPrgSubsFincoy, xPotonganHarga, xReturHarga, xPotonganKhusus, xNamaHadiah);
				#SET oKeterangan = CONCAT_WS(' - ',  xDKTenor, xDKAngsuran, xDKMerkSblmnya, xDKJenisSblmnya, xDKMotorUntuk, xDKMotorOleh)
				#SET oKeterangan = CONCAT_WS(' - ',   xDKMediator,xDKTOP,xSPKNo, xInsentifSales, xTeamKode,  xBBNPlus, xROCount, xDKScheme, xDKSCP, xDKPengajuanBBN,  xDKScheme2, xDKSurveyor);

				IF xDKJenis = 'Tunai' THEN
					SET xDKTOP = 1;
				ELSE
					SET xDKTOP = 7;
				END IF;
				
				IF xTeamKode = '' OR xTeamKode = '--' THEN SELECT TeamKode INTO xTeamKode FROM tdsales WHERE SalesKode = xSalesKode; END IF;
				IF xDKJenis = '--' THEN SET xDKJenis = 'Kredit'; ELSE SET xDKJenis = xDKJenis; END IF;
				
				#GetRO
				SET xROCount = 0;
				IF (xCusKTP <> "      -      -" AND xCusKTP <> "--") AND (xCusPembeliKTP <> "      -      -" AND xCusPembeliKTP <> "--") THEN
					SELECT  IFNULL(COUNT(DKNo),0) INTO xROCount FROM ttdk INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode 	
					WHERE (CusKTP = xCusKTP OR CusPembeliKTP = xCusPembeliKTP) AND  DKNo < xDKNo;
				ELSE
					IF (xCusKTP <> "      -      -" AND xCusKTP <> "--") AND (xCusPembeliKTP = "      -      -" OR xCusPembeliKTP = "--") THEN
						SELECT  IFNULL(COUNT(DKNo),0) INTO xROCount FROM ttdk INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode WHERE (CusKTP = xCusKTP ) AND  DKNo < xDKNo;
					END IF;
					IF (xCusPembeliKTP <> "      -      -"AND xCusKTP <> "--") AND xCusPembeliKTP <> "--" AND xCusKTP = "      -      -" THEN
						SELECT  IFNULL(COUNT(DKNo),0) INTO xROCount FROM ttdk INNER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode WHERE (CusPembeliKTP = xCusPembeliKTP) AND  DKNo < xDKNo;
					END IF;
					IF (xCusKTP = "      -      -" AND xCusKTP <> "--") AND (xCusPembeliKTP = "      -      -" OR xCusPembeliKTP = "--") THEN
						SET xROCount = 0;
					END IF;
				END IF;

				#Update
				IF @AutoNo = 'Edit' THEN 
					UPDATE ttdk
					SET DKNo = xDKNoBaru, DKTgl = xDKTgl, SalesKode = xSalesKode, CusKode = xCusKode, LeaseKode = xLeaseKode, DKJenis = xDKJenis, DKMemo = xDKMemo, INNo = xINNo, UserID = xUserID, 
					DKHarga = xDKHarga, DKDPTotal = xDKDPTotal, DKDPLLeasing = xDKDPLLeasing, DKDPTerima = xDKDPTerima, DKDPInden = xDKDPInden, DKNetto = xDKNetto, 
					ProgramNama = xProgramNama, ProgramSubsidi = xProgramSubsidi, BBN = xBBN, Jaket = xJaket, 
					PrgSubsSupplier = xPrgSubsSupplier, PrgSubsDealer = xPrgSubsDealer, PrgSubsFincoy = xPrgSubsFincoy, PotonganHarga = xPotonganHarga, 
					ReturHarga = xReturHarga, PotonganKhusus = xPotonganKhusus, NamaHadiah = xNamaHadiah, DKTenor = xDKTenor, DKAngsuran = xDKAngsuran, 
					DKMerkSblmnya = xDKMerkSblmnya, DKJenisSblmnya = xDKJenisSblmnya, DKMotorUntuk = xDKMotorUntuk, DKMotorOleh = xDKMotorOleh, DKMediator = xDKMediator, DKTOP = xDKTOP, 
					SPKNo = xSPKNo, InsentifSales = xInsentifSales, TeamKode = xTeamKode, 
					BBNPlus = xBBNPlus, ROCount = xROCount, DKScheme = xDKScheme, DKSCP = xDKSCP, DKScheme2 = xDKScheme2, DKSurveyor = xDKSurveyor,
					DKHPP = 0
					WHERE DKNo = xDKNo;
					SET xDKNo = xDKNoBaru;
				
					UPDATE tmotor SET DKNo = xDKNo WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN;
					
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID ,	 'Edit' , 'Data Konsumen','ttdk', xDKNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menyimpan Data Konsumen No : ", xDKNo);
				ELSE 			
					UPDATE ttdk
					SET DKNo = xDKNoBaru, DKTgl = xDKTgl, SalesKode = xSalesKode, CusKode = xCusKode, LeaseKode = xLeaseKode, DKJenis = xDKJenis, DKMemo = xDKMemo, INNo = xINNo, UserID = xUserID, 
					DKHarga = xDKHarga, DKDPTotal = xDKDPTotal, DKDPLLeasing = xDKDPLLeasing, DKDPTerima = xDKDPTerima, DKDPInden = xDKDPInden, DKNetto = xDKNetto, 
					ProgramNama = xProgramNama, ProgramSubsidi = xProgramSubsidi, BBN = xBBN, Jaket = xJaket, 
					PrgSubsSupplier = xPrgSubsSupplier, PrgSubsDealer = xPrgSubsDealer, PrgSubsFincoy = xPrgSubsFincoy, PotonganHarga = xPotonganHarga, 
					ReturHarga = xReturHarga, PotonganKhusus = xPotonganKhusus, NamaHadiah = xNamaHadiah, DKTenor = xDKTenor, DKAngsuran = xDKAngsuran, 
					DKMerkSblmnya = xDKMerkSblmnya, DKJenisSblmnya = xDKJenisSblmnya, DKMotorUntuk = xDKMotorUntuk, DKMotorOleh = xDKMotorOleh, DKMediator = xDKMediator, DKTOP = xDKTOP, 
					SPKNo = xSPKNo, InsentifSales = xInsentifSales, TeamKode = xTeamKode, 
					BBNPlus = xBBNPlus, ROCount = xROCount, DKScheme = xDKScheme, DKSCP = xDKSCP, DKScheme2 = xDKScheme2, DKSurveyor = xDKSurveyor,
					DKHPP = 0
					WHERE DKNo = xDKNo;				
					SET xDKNo = xDKNoBaru;

					UPDATE tmotor SET DKNo = xDKNo WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN;
					
					INSERT INTO tuserlog (LogTglJam, UserID, LogJenis , LogNama, LogTabel, NoTrans) VALUE 
					(NOW(), xUserID , 'Tambah' , 'Data Konsumen','ttdk', xDKNo); 
					SET oKeterangan = CONCAT(oKeterangan,"Berhasil menambah Data Konsumen No : ", xDKNo);
				END IF;			
			ELSEIF oStatus = 1 THEN
				SET oKeterangan = oKeterangan;
			END IF;
		END;	
		WHEN "Delete" THEN
		BEGIN
			SET oKeterangan = "";
			SET oStatus = 0;		
/*			
			#Cek Apakah ada SK
			IF oStatus = 0 THEN
				SELECT GROUP_CONCAT(SKNo) INTO @ListSK FROM ttsk INNER JOIN tmotor ON tmotor.SKNo = ttsk.SKNo WHERE tmotor.DKNo = xDKNo GROUP BY SKNo;
				IF LENGTH(@ListSK) > 1 THEN
					SET oStatus = 1; SET oKeterangan = CONCAT('Data Konsumen No : ',xDKNo,' telah miliki Surat Jalan Konsumen No : ', @ListSK);				
				END IF;				
			END IF;

			#Cek Apakah ada RK
			IF oStatus = 0 THEN
				SELECT RKNo INTO @RKNo FROM ttrk WHERE DKNo = xDKNo;
				IF @RKNo  IS NOT NULL THEN
					SET oStatus = 1; SET oKeterangan = CONCAT('Data Konsumen No : ',xDKNo,' telah miliki Retur Konsumen No : ', @RKNo);				
				END IF;				
			END IF;
		
			#Cek Apakah Sudah Cetak 
			#Cek juga di HMDS_LOG
			IF oStatus = 0 THEN
				SELECT LogTglJam INTO @LogTglJam FROM tuserlog WHERE LogJenis = 'Print' AND NoTrans = xDKNo;
				IF @LogTglJam IS NOT NULL THEN
					SET oStatus = 1; SET oKeterangan = 'DATA ini karena sudah pernah di cetak';				
				END IF;
			END IF;			              
*/			
			IF oStatus = 0 THEN
				DELETE FROM ttdk WHERE DKNo = xDKNo;			
				SET oStatus = 0; SET oKeterangan = CONCAT("Berhasil menghapus data Data Konsumen [DK] No: ",xDKNo);
			END IF;
		END;
		WHEN "Loop" THEN
		BEGIN
			SET oStatus = 0; 
			SET oKeterangan = CONCAT("Update Konsumen");
			UPDATE ttdk SET CusKode = xCusKode WHERE DKNo = xDKNo;
		END;
		WHEN "LoopIN" THEN
		BEGIN
			SET oStatus = 0; 
			SET oKeterangan = CONCAT("Update Inden ",xINNo);

			SELECT INNo, SalesKode, CusKode, LeaseKode, INDP,  MotorType, MotorWarna INTO 
			@INNo, @SalesKode, @CusKode, @LeaseKode, @INDP,  @MotorType, @MotorWarna
			FROM ttin WHERE INNo = xINNo;

			UPDATE ttdk SET INNo = xINNo,  DKDPInden = @INDP, LeaseKode = @LeaseKode, 
			CusKode = @CusKode, SalesKode = @SalesKode  WHERE DKNo = xDKNo;
			#TeamKode, #Motor
		END;				
		WHEN "Cancel" THEN
		BEGIN
			IF LOCATE('=',xDKNo) <> 0 THEN
				DELETE FROM ttdk WHERE DKNo = xDKNo;
				UPDATE tmotor SET DKNo = '--' WHERE MotorNoMesin = xMotorNoMesin AND MotorAutoN = xMotorAutoN;
				SET oKeterangan = CONCAT("Batal menyimpan data Data Konsumen [DK] No: ", xDKNoBaru);
			ELSE
				SET oKeterangan = CONCAT("Batal menyimpan data Data Konsumen [DK] No: ", xDKNo);
			END IF;
		END;
		WHEN "Print" THEN
		BEGIN
			/* ambil data header */
			IF xCetak = "1" THEN
				SELECT ttdk.DKNo, ttdk.DKTgl, ttdk.SalesKode, ttdk.CusKode, ttdk.LeaseKode, ttdk.DKJenis, ttdk.DKMemo, ttdk.DKLunas, ttdk.UserID, ttdk.DKHPP, ttdk.DKHarga, ttdk.DKDPTotal, ttdk.DKDPLLeasing, ttdk.DKDPTerima, ttdk.DKNetto, 
				ttdk.ProgramNama, ttdk.ProgramSubsidi, ttdk.BBN, ttdk.Jaket, ttdk.PrgSubsSupplier, ttdk.PrgSubsDealer, ttdk.PotonganHarga, ttdk.ReturHarga, ttdk.PotonganKhusus, ttdk.NamaHadiah, IFNULL(tmotor.MotorNoMesin, '--') 
				AS MotorNoMesin, IFNULL(tmotor.MotorAutoN, 0) AS MotorAutoN, IFNULL(tmotor.MotorType, '--') AS MotorType, IFNULL(tmotor.MotorWarna, '--') AS MotorWarna, IFNULL(tmotor.MotorNoRangka, '--') AS MotorNoRangka, 
				tmotor.MotorTahun, IFNULL(tdsales.SalesNama, '--') AS SalesNama, IFNULL(tdleasing.LeaseNama, '--') AS LeaseNama, ttdk.INNo, ttdk.PrgSubsFincoy, IFNULL(tmotor.SKNo, '--') AS SKNo, ttdk.PotonganAHM, ttdk.DKDPInden, 
				ttdk.DKTenor, ttdk.DKAngsuran, IFNULL(tdcustomer.CusKTP, '--') AS CusKTP, IFNULL(tdcustomer.CusNama, '') AS CusNama, IFNULL(tdcustomer.CusAlamat, '') AS CusAlamat, IFNULL(tdcustomer.CusRT, '') AS CusRT, 
				IFNULL(tdcustomer.CusRW, '') AS CusRW, IFNULL(tdcustomer.CusProvinsi, '--') AS CusProvinsi, IFNULL(tdcustomer.CusKabupaten, '') AS CusKabupaten, IFNULL(tdcustomer.CusKecamatan, '--') AS CusKecamatan, 
				IFNULL(tdcustomer.CusKelurahan, '--') AS CusKelurahan, IFNULL(tdcustomer.CusKodePos, '--') AS CusKodePos, IFNULL(tdcustomer.CusTelepon, '') AS CusTelepon, IFNULL(tdcustomer.CusSex, '--') AS CusSex, 
				IFNULL(tdcustomer.CusTempatLhr, '') AS CusTempatLhr, IFNULL(tdcustomer.CusTglLhr, STR_TO_DATE('01/01/1900', '%m/%d/%Y')) AS CusTglLhr, IFNULL(tdcustomer.CusAgama, '--') AS CusAgama, 
				IFNULL(tdcustomer.CusPekerjaan, '--') AS CusPekerjaan, IFNULL(tdcustomer.CusPendidikan, '--') AS CusPendidikan, IFNULL(tdcustomer.CusPengeluaran, '--') AS CusPengeluaran, IFNULL(tdcustomer.CusEmail, '--') AS CusEmail, 
				IFNULL(tdcustomer.CusKodeKons, '--') AS CusKodeKons, ttdk.DKMerkSblmnya, ttdk.DKJenisSblmnya, ttdk.DKMotorUntuk, ttdk.DKMotorOleh, ttdk.DKMediator, ttdk.DKTOP, ttdk.DKPOLeasing, ttdk.SPKNo, 
				IFNULL(tdmotortype.MotorNama, '--') AS MotorNama, IFNULL(tdmotortype.MotorKategori, '--') AS MotorKategori, tdcustomer.CusStatusHP, tdcustomer.CusStatusRumah, tdcustomer.CusPembeliNama, tdcustomer.CusPembeliKTP, 
				tdcustomer.CusPembeliAlamat, ttdk.InsentifSales, ttdk.TeamKode, IFNULL(tmotor.FakturAHMNo, '--') AS RKNo, ttdk.DKTglTagih, ttdk.DKPONo, ttdk.DKPOTgl, ttdk.BBNPlus, ttdk.ROCount, ttdk.DKScheme, ttdk.DKSCP, 
				ttdk.DKPengajuanBBN, ttdk.JAPrgSubsSupplier, ttdk.JAPrgSubsDealer, ttdk.JAPrgSubsFincoy, ttdk.JADKHarga, ttdk.JADKDPTerima, ttdk.JADKDPLLeasing, ttdk.JABBN, ttdk.JAReturHarga, ttdk.JAPotonganHarga, 
				ttdk.JAInsentifSales, ttdk.JAPotonganKhusus, ttdk.DKScheme2, tmotor.FakturAHMNo, ttdk.DKSurveyor, tdcustomer.CusKK, tdcustomer.CusTelepon2, tdcustomer.CusNPWP
				FROM  ttdk 
				LEFT OUTER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo 
				LEFT OUTER JOIN tdmotortype ON tmotor.MotorType = tdmotortype.MotorType 
				LEFT OUTER JOIN tdleasing ON ttdk.LeaseKode = tdleasing.LeaseKode 
				LEFT OUTER JOIN tdsales ON ttdk.SalesKode = tdsales.SalesKode 
				LEFT OUTER JOIN tdcustomer ON ttdk.CusKode = tdcustomer.CusKode
				WHERE (ttdk.DKNo = xDKNo);
			END IF;
		END;
	END CASE;
END$$

DELIMITER ;

