DELIMITER $$

DROP PROCEDURE IF EXISTS `jkk`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jkk`(IN xKKNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE oSumKKBayarIT DECIMAL(19,2) DEFAULT 0;
	DECLARE oSumKKBayarCOA DECIMAL(19,2) DEFAULT 0;
	DECLARE MyPesanKu VARCHAR(300) DEFAULT '';
   DECLARE MyPesankuItem VARCHAR(150) DEFAULT '';
	DECLARE oNoUrutKu  DECIMAL(5) DEFAULT 1;
	DECLARE MyAccountPOS  VARCHAR(10) DEFAULT "11010100";
	DECLARE My24050200TitipanBBN DECIMAL(19,2) DEFAULT 0;
	DECLARE My11060200PiutangBBN DECIMAL(19,2) DEFAULT 0;
	
	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xKKNo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;
	
	IF LEFT(xKKNo,2) = 'KP' THEN LEAVE sp; END IF;	
		
	SELECT KKNo, KKTgl, KKMemo, KKNominal, KKJenis, KKPerson, KKLink, ttkk.UserID, ttkk.LokasiKode, KKJam, ttkk.NoGL, IFNULL(CusNama,''), IFNULL(ttsk.SKNo,'--'), IFNULL(SKJam,STR_TO_DATE('3000/01/01', '%Y/%m/%d')) 
	INTO  @KKNo,@KKTgl,@KKMemo,@KKNominal,@KKJenis,@KKPerson,@KKLink,@UserID,@LokasiKode,@KKJam,@NoGL,@CusNama, @SKNo, @SKJam
	FROM ttKK	
	LEFT OUTER JOIN ttdk ON ttkk.KKLink = ttdk.DKNo
	LEFT OUTER JOIN tmotor ON ttdk.DKNo = tmotor.DKNo	
	LEFT OUTER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo
	LEFT OUTER JOIN tdcustomer ON tdcustomer.CusKode = ttdk.CusKode
	WHERE KKNo = xKKNo;
	SELECT IFNULL(SUM(KKBayar),0) INTO oSumKKBayarIT FROM ttkkit WHERE KKNo = xKKNo;
	SELECT IFNULL(SUM(KKBayar),0) INTO oSumKKBayarCOA FROM ttkkitcoa WHERE KKNo = xKKNo;

   SELECT IFNULL(tdlokasi.NoAccount,'11010100') INTO MyAccountPOS FROM tdlokasi INNER JOIN traccount ON traccount.NoAccount = tdlokasi.NoAccount    
   WHERE LokasiKode = @LokasiKode;
	IF MyAccountPOS = '' OR (LENGTH(TRIM(MyAccountPOS)) < 10) THEN SET MyAccountPOS = '11010100'; END IF;

   SET MyPesanKu = CONCAT("Post  KK - ",@KKJenis);
   IF @CusNama <> '' 	THEN SET MyPesanKu = CONCAT(MyPesanKu,' - ' , @CusNama); 	END IF;
   IF @KKPerson <> '--' AND @KKPerson <> @CusNama THEN SET MyPesanKu = CONCAT(MyPesanKu,' - ' , @KKPerson); 	END IF;
   IF @KKMemo <> "--" AND @KKMemo <> @KKJenis THEN SET MyPesanKu = CONCAT(MyPesanKu,' - ' , @KKMemo); 		END IF; 
   SET MyPesanKu = TRIM(MyPesanKu); 
   IF LENGTH(MyPesanKu) > 300 THEN SET MyPesanKu = SUBSTRING(MyPesanKu,0, 299); END IF;
   SET MyPesankuItem = MyPesanKu ;
   IF LENGTH(MyPesankuItem) > 150 THEN SET MyPesankuItem = SUBSTRING(MyPesankuItem,0, 149); END IF;
    
	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;

	#ScriptTtglhpit
	DROP TEMPORARY TABLE IF EXISTS ScriptTtglhpit;
	CREATE TEMPORARY TABLE IF NOT EXISTS ScriptTtglhpit AS
	SELECT NoGL, TglGL, GLLink, NoAccount, HPJenis, HPNilai FROM ttglhpit WHERE NoGL = xNoGL;
	
	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL; DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;DELETE FROM ttglhpit WHERE NoGL = xNoGL;
   DELETE FROM ttgeneralledgerhd WHERE GLLink = xKKNo;

	#Header
	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, @KKTgl, 'KK', MyPesanKu, @KKNominal, @KKNominal, xKKNo, xUserID, '--', xGLValid);

	#Item
	CASE @KKJenis
		WHEN 'Umum' THEN
		BEGIN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, KKAutoN, NoAccount,  KKKeterangan, KKBayar, 0 FROM ttkkitcoa WHERE KKNo = xKKNo AND KKBayar >= 0;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, KKAutoN, NoAccount,  KKKeterangan, 0, KKBayar FROM ttkkitcoa WHERE KKNo = xKKNo AND KKBayar < 0;
			
			SET @AutoN = 1;
			SELECT COUNT(NoGL)+1 INTO @AutoN FROM ttgeneralledgerit WHERE NoGL = xNoGL;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, @AutoN, MyAccountPOS , MyPesankuItem , 0, oSumKKBayarCOA;

			SELECT COUNT(NoGL) INTO @CountNoGL FROM ScriptTtglhpit;
			IF @CountNoGL > 0 THEN
				INSERT INTO ttglhpit SELECT * FROM ScriptTtglhpit;
				DELETE FROM ttglhpit WHERE NoAccount NOT IN (SELECT NoAccount FROM ttgeneralledgerit WHERE NoGL  = xNoGL) AND NoGL  = xNoGL; 
				UPDATE ttgeneralledgerhd SET HPLink = 'Many'  WHERE NoGL = xNoGL; 
			END IF;		
			
			UPDATE ttKK SET KKNominal = oSumKKBayarCOA WHERE KKNo = xKKNo;		
		END;
		WHEN 'SubsidiDealer2' THEN
		BEGIN
			IF @SKJam > @KKJam THEN
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu,'11170300' , MyPesankuItem , @KKNominal,0;SET oNoUrutKu = oNoUrutKu + 1;
			ELSE
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu,'24040700' , MyPesankuItem , @KKNominal,0;SET oNoUrutKu = oNoUrutKu + 1;
			END IF;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu, MyAccountPOS , MyPesankuItem , 0,@KKNominal;		
			CALL jkkLinkHutang(xKKNo,@KKTgl,xNoGL,'24040600');			
		END;
		WHEN 'Retur Harga'  THEN
		BEGIN		
			IF @SKJam > @KKJam THEN
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu,'11170400' , MyPesankuItem , @KKNominal,0;SET oNoUrutKu = oNoUrutKu + 1;
			ELSE
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu,'24040600' , MyPesankuItem , @KKNominal,0;SET oNoUrutKu = oNoUrutKu + 1;
			END IF;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu, MyAccountPOS , MyPesankuItem , 0,@KKNominal;					
			CALL jkkLinkHutang(xKKNo,@KKTgl,xNoGL,'24040700');		
		END;
		WHEN 'Insentif Sales' THEN
		BEGIN
			SELECT PerusahaanNama INTO @PerusahaanNama FROM tzcompany LIMIT 1;
	      IF @PerusahaanNama = "CV. ANUGRAH SEJAHTERA BOGOR" 
	      OR @PerusahaanNama = "CV. ANUGERAH SEJAHTERA BOGOR" 
			OR @PerusahaanNama = "CV. KARYA PERDANA INDRAMAYU" 
			OR @PerusahaanNama = "CV. ANUGERAH KIRANA BENGKULU" 
			OR @PerusahaanNama = "CV. ANUGERAH WANGI PAMEKASAN"
			OR @PerusahaanNama = "CV. ANUGERAH SEJATI PROBOLINGGO"
			OR @PerusahaanNama = "CV. ANUGERAH MULIA TOMOHON"  THEN
				IF @SKJam > @KKJam THEN
					INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu,'11170500' , MyPesankuItem , @KKNominal,0;SET oNoUrutKu = oNoUrutKu + 1;
				ELSE
					INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu,'24051100' , MyPesankuItem , @KKNominal,0;SET oNoUrutKu = oNoUrutKu + 1;
				END IF;
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu, MyAccountPOS , MyPesankuItem , 0,@KKNominal;
				CALL jkkLinkHutang(xKKNo,@KKTgl,xNoGL,'24051100');		
			ELSE
				IF @SKJam > @KKJam THEN
					INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu,'11170500' , MyPesankuItem , @KKNominal,0;SET oNoUrutKu = oNoUrutKu + 1;
				ELSE
					INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu,'24040400' , MyPesankuItem , @KKNominal,0;SET oNoUrutKu = oNoUrutKu + 1;
				END IF;
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu, MyAccountPOS , MyPesankuItem , 0,@KKNominal;
				CALL jkkLinkHutang(xKKNo,@KKTgl,xNoGL,'24040400');
			END IF;
		END;
		WHEN 'Potongan Khusus' THEN
		BEGIN
			SELECT PerusahaanNama INTO @PerusahaanNama FROM tzcompany LIMIT 1;
	      IF @PerusahaanNama = "CV. ANUGERAH UTAMA GORONTALO" THEN
				IF @SKJam > @KKJam THEN
					INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu,'30040000' , MyPesankuItem , @KKNominal,0;SET oNoUrutKu = oNoUrutKu + 1;
				ELSE
					INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu,'24040100' , MyPesankuItem , @KKNominal,0;SET oNoUrutKu = oNoUrutKu + 1;
				END IF;
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu, MyAccountPOS , MyPesankuItem , 0,@KKNominal;
				CALL jkkLinkHutang(xKKNo,@KKTgl,xNoGL,'24040100');
	      ELSEIF @PerusahaanNama = "CV. CENDANA HARUM DEMAK" OR @PerusahaanNama = "CV. CENDANA MEGAH SENTOSA" THEN
				IF @SKJam > @KKJam THEN
					INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu,'24051100' , MyPesankuItem , @KKNominal,0;SET oNoUrutKu = oNoUrutKu + 1;
				ELSE
					INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu,'24051100' , MyPesankuItem , @KKNominal,0;SET oNoUrutKu = oNoUrutKu + 1;
				END IF;
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu, MyAccountPOS , MyPesankuItem , 0,@KKNominal;
				CALL jkkLinkHutang(xKKNo,@KKTgl,xNoGL,'24051100');
			ELSE
				IF @SKJam > @KKJam THEN
					INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu,'60380000' , MyPesankuItem , @KKNominal,0;SET oNoUrutKu = oNoUrutKu + 1;
				ELSE
					INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu,'24040100' , MyPesankuItem , @KKNominal,0;SET oNoUrutKu = oNoUrutKu + 1;
				END IF;
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu, MyAccountPOS , MyPesankuItem , 0,@KKNominal;
				CALL jkkLinkHutang(xKKNo,@KKTgl,xNoGL,'24040100');
			END IF;	
		END;
		WHEN 'KK Ke Bank' THEN
		BEGIN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu,'11040000' , MyPesankuItem , @KKNominal,0;SET oNoUrutKu = oNoUrutKu + 1;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu, MyAccountPOS , MyPesankuItem , 0,@KKNominal;	
		END;
		WHEN 'KK Ke Bengkel' THEN
		BEGIN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu,'24020250' , MyPesankuItem , @KKNominal,0;SET oNoUrutKu = oNoUrutKu + 1;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu, MyAccountPOS , MyPesankuItem , 0,@KKNominal;	
		END;
		WHEN 'Pinjaman Karyawan' THEN
		BEGIN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu,'11120100' , MyPesankuItem , @KKNominal,0;SET oNoUrutKu = oNoUrutKu + 1;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu, MyAccountPOS , MyPesankuItem , 0,@KKNominal;	
		END;
		WHEN 'KK Dealer Ke Pos' THEN
		BEGIN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu,'11040000' , MyPesankuItem , @KKNominal,0;SET oNoUrutKu = oNoUrutKu + 1;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu, MyAccountPOS , MyPesankuItem , 0,@KKNominal;			
		END;
		WHEN 'KK Pos Ke Dealer' THEN
		BEGIN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu,'11040000' , MyPesankuItem , @KKNominal,0;SET oNoUrutKu = oNoUrutKu + 1;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu, MyAccountPOS , MyPesankuItem , 0,@KKNominal;			
		END;
		WHEN 'Bayar Unit' THEN
		BEGIN
         SELECT SupStatus INTO @SupStatus FROM tdsupplier WHERE SupKode = @SupKode;
			IF @SupStatus = 1 THEN #24010101 Htg Dag Jika Supplier Utama
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, 1, '24010101', MyPesankuItem , oSumKKBayarIT, 0;
			ELSE
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, 1, '24010200', MyPesankuItem , oSumKKBayarIT, 0;
			END IF;
			
			SELECT  GROUP_CONCAT(ttKKit.FBNo), SUM(ttfb.FBPSS), SUM(ttfb.FBPtgLain), SUM(ttfb.FBExtraDisc) INTO @KoleksiFB, @FBPSS, @FBPtgLain, @FBExtraDisc
			FROM ttKKit 
			INNER JOIN ttfb ON ttKKit.FBNo = ttfb.FBNo 
			INNER JOIN ttkk ON ttKKit.KKNo = ttkk.KKNo 
			WHERE (ttKKit.KKNo = xKKNo);
			
			SET oNoUrutKu = 2;
			IF @FBExtraDisc > 0 THEN #24050700 ExtraDiscount				
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu , '24050700', MyPesankuItem, @FBExtraDisc, 0; SET oNoUrutKu = oNoUrutKu + 1;
         END IF;

			SET @PendapatanExtraDiskon = @KKNominal - oSumKKBayarIT- @FBExtraDisc;
			IF @PendapatanExtraDiskon < 0 THEN
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu , '40040200', MyPesankuItem, 0, ABS(@PendapatanExtraDiskon); SET oNoUrutKu = oNoUrutKu + 1;
			ELSE
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu , '40040200', @PesankuItem , ABS(@PendapatanExtraDiskon), 0; SET oNoUrutKu = oNoUrutKu + 1;
			END IF;
			
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu, MyAccountPOS, MyPesankuItem , 0, @KKNominal;
			
			#Link Hutang Piutang
			DELETE FROM ttglhpit WHERE NoGL = xNoGL;
			IF @SupStatus = 1 THEN #24010101 Htg Dag Jika Supplier Utama
				INSERT INTO ttglhpit SELECT xNoGL, @KKTgl, ttKKit.FBNo, NoAccount, 'Hutang', SUM(KreditGL) AS KreditGL   FROM ttKKit 
				INNER JOIN ttfb ON ttKKit.FBNo = ttfb.FBNo 
				INNER JOIN ttgeneralledgerhd ON ttfb.NoGL = ttgeneralledgerhd.NoGL  
				INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL 
				WHERE KKNo = xKKNo AND NoAccount = '24010101' GROUP BY NoAccount, ttfb.FBNo;
			ELSE
				INSERT INTO ttglhpit SELECT xNoGL, @KKTgl, ttKKit.FBNo, NoAccount, 'Hutang', SUM(KreditGL) AS KreditGL   FROM ttKKit 
				INNER JOIN ttfb ON ttKKit.FBNo = ttfb.FBNo 
				INNER JOIN ttgeneralledgerhd ON ttfb.NoGL = ttgeneralledgerhd.NoGL  
				INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL 
				WHERE KKNo = xKKNo AND NoAccount = '24010200' GROUP BY NoAccount, ttfb.FBNo;
			END IF;
			IF @FBExtraDisc > 0 THEN #24050700 ExtraDiscount				
				INSERT INTO ttglhpit SELECT xNoGL, @KKTgl, ttKKit.FBNo, NoAccount, 'Hutang', SUM(KreditGL) AS KreditGL   FROM ttKKit 
				INNER JOIN ttfb ON ttKKit.FBNo = ttfb.FBNo 
				INNER JOIN ttgeneralledgerhd ON ttfb.NoGL = ttgeneralledgerhd.NoGL  
				INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL 
				WHERE KKNo = xKKNo AND NoAccount = '24050700' GROUP BY NoAccount, ttfb.FBNo;
         END IF;	
		END;
		WHEN 'Bayar BBN' THEN
		BEGIN
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu , '24030000', MyPesankuItem , oSumKKBayarIT, 0; SET oNoUrutKu = oNoUrutKu + 1;
			IF (@KKNominal-oSumKKBayarIT) > 0 THEN INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu , '60050000', @PesankuItem , @KKNominal-oSumKKBayarIT, 0; SET oNoUrutKu = oNoUrutKu + 1; END IF;
			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu,MyAccountPOS , MyPesankuItem , 0, @KKNominal;
			
			DELETE FROM ttglhpit WHERE NoGL = xNoGL;

			DROP TEMPORARY TABLE IF EXISTS Asal; CREATE TEMPORARY TABLE Asal AS
			SELECT ttkkit.FBNo, ttsk.SKNo, KKBayar, IFNULL(SUM(KreditGL),0) AS KreditGL  FROM ttkkit
			INNER JOIN tmotor ON ttkkit.FBNo = tmotor.DKNo
			INNER JOIN ttSK ON tmotor.SKNo = ttSK.SKNo
			INNER JOIN ttgeneralledgerhd ON ttSK.NoGL = ttgeneralledgerhd.NoGL
			INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL
			WHERE KKNo = xKKNo AND NoAccount = '24030000'
			GROUP BY NoAccount , ttsk.skno
			ORDER BY ttsk.skno;

			DROP TEMPORARY TABLE IF EXISTS Terlunasi; CREATE TEMPORARY TABLE Terlunasi AS
			SELECT GLLink, SUM(IFNULL(HPNIlai,0)) AS Terbayar, NoGL FROM Asal
			LEFT OUTER JOIN ttglhpit ON Asal.SKNo = ttglhpit.GLLink
			WHERE NoAccount = '24030000' AND HPJenis = 'Hutang'
			GROUP BY GLLink ORDER BY GLLink;

			DROP TEMPORARY TABLE IF EXISTS Terbayar; CREATE TEMPORARY TABLE Terbayar AS
			SELECT Asal.FBNo, Asal.SKNo,Asal.KKBayar,Asal.KreditGL, IFNULL(Terbayar,0) AS Terbayar FROM Asal
			LEFT OUTER JOIN
			(SELECT GLLink, SUM(Terbayar) AS Terbayar FROM Terlunasi WHERE NoGL <> xNoGL GROUP BY GLLink) A
			ON Asal.SKNo = A.GLLink;

			INSERT INTO ttglhpit SELECT xNoGL, @KKTgl, SKNo, '24030000','Hutang', (KreditGL - Terbayar) AS KreditGL FROM Terbayar WHERE KreditGL > 0 ;
	
			#Link Hutang Piutang JA		
			DROP TEMPORARY TABLE IF EXISTS JAAsal; CREATE TEMPORARY TABLE JAAsal AS
			SELECT ttKKit.FBno, ttgeneralledgerhd.GLLink , KKBayar,IFNULL(SUM(KreditGL), 0) AS KreditGL
			FROM  ttKKit
			INNER JOIN ttgeneralledgerhd ON ttKKit.FBNo = ttgeneralledgerhd.GLLink
			INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL
			WHERE KKNo = xKKNo AND NoAccount = '24030000'
			GROUP BY NoAccount, ttgeneralledgerhd.GLLink
			ORDER BY ttgeneralledgerhd.GLLink;

			DROP TEMPORARY TABLE IF EXISTS JATerlunasi; CREATE TEMPORARY TABLE JATerlunasi AS
			SELECT JAAsal.GLLink, SUM(IFNULL(HPNIlai,0)) AS Terbayar, NoGL FROM JAAsal
			LEFT OUTER JOIN ttglhpit ON JAAsal.FBno = ttglhpit.GLLink
			WHERE NoAccount = '24030000' AND HPJenis = 'Hutang'
			GROUP BY GLLink ORDER BY GLLink;

			DROP TEMPORARY TABLE IF EXISTS JATerbayar; CREATE TEMPORARY TABLE JATerbayar AS
			SELECT JAAsal.FBNo, JAAsal.GLLink, JAAsal.KKBayar, JAAsal.KreditGL, IFNULL(Terbayar,0) AS Terbayar FROM JAAsal
			LEFT OUTER JOIN
			(SELECT GLLink, SUM(Terbayar) AS Terbayar FROM Terlunasi WHERE NoGL <> xNoGL GROUP BY GLLink) A
			ON JAAsal.GLLink = A.GLLink;

			INSERT INTO ttglhpit SELECT xNoGL, @KKTgl, GLLink, '24030000','Hutang', (KreditGL - Terbayar) AS KreditGL FROM JATerbayar WHERE KreditGL > 0 ;
		END;
		WHEN 'BBN Plus' THEN
		BEGIN
			DROP TEMPORARY TABLE IF EXISTS TSearch; CREATE TEMPORARY TABLE TSearch AS
			SELECT KKNo, FBNo, KKBayar, KKJam, IFNULL(KMNo,'--') AS KMNo, IFNULL(KMLink,'--') AS KMLink, IFNULL(KMNominal,0) AS KMNominal, IFNULL(KMJam,DATE('2500-01-01')) AS KMJam FROM 
			(SELECT ttkkit.KKNo, FBNo, KKBayar, KKJam FROM ttkkit INNER JOIN ttkk ON ttkk.kkno = ttkkit.KKNo WHERE ttkkit.KKNo = xKKNo) ttkkit
			LEFT OUTER JOIN 
			(SELECT KMNo, KMTgl, SUM(KMNominal) AS KMNominal, KMJenis, KMLink, KMJam, NoGL FROM vtkm WHERE KMJenis = 'BBN Plus' AND KMLINk IN (SELECT FBNo FROM ttkkit WHERE KKNO = xKKNo) GROUP BY KMLink) ttkm 
			ON ttkm.KMLink = ttkkit.FBNo;

			SELECT IFNULL(SUM(KMNominal),0), IFNULL(SUM(KKBayar - KMNominal),0) INTO My24050200TitipanBBN, My11060200PiutangBBN FROM TSearch WHERE KKJam > KMJam AND KKBayar > KMNominal;

			IF My24050200TitipanBBN = 0 THEN SELECT IFNULL(SUM(KMNominal),0) INTO My24050200TitipanBBN FROM TSearch WHERE KKJam > KMJam AND KKBayar < KMNominal; END IF;

			IF My11060200PiutangBBN = 0 THEN SELECT IFNULL(SUM(KKBayar),0) INTO My11060200PiutangBBN FROM TSearch WHERE KKJam < KMJam; END IF;

			IF @KKNominal - oSumKKBayarIT > 0 THEN SET My11060200PiutangBBN = My11060200PiutangBBN + @KKNominal - oSumKKBayarIT;  END IF;

			IF My24050200TitipanBBN > 0 THEN
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu , '24050200', MyPesankuItem , My24050200TitipanBBN, 0; SET oNoUrutKu = oNoUrutKu + 1;
			END IF;

			IF My11060200PiutangBBN > 0 THEN
				INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu , '11060200', MyPesankuItem , My11060200PiutangBBN, 0; SET oNoUrutKu = oNoUrutKu + 1;			
			END IF;

			INSERT INTO ttgeneralledgerit	SELECT xNoGL, @KKTgl, oNoUrutKu,MyAccountPOS , MyPesankuItem , 0, @KKNominal;

			#Link Hutang Piutang Normal
			DELETE FROM ttglhpit WHERE NoGL = xNoGL;
			DROP TEMPORARY TABLE IF EXISTS TSearchLink; CREATE TEMPORARY TABLE TSearchLink AS
         SELECT KKNo, FBNo, SUM(KKBayar) KKBayar, KKJam, KMNo, KMLink, KMNominal, KMJam , KreditGL, NoGL FROM        
         (SELECT ttkkit.KKNo, FBNo, KKBayar, KKJam FROM ttkkit 
            INNER JOIN ttkk ON ttkk.kkno = ttkkit.KKNo 
            INNER JOIN ttgeneralledgerhd ON ttkk.NoGL = ttgeneralledgerhd.NoGL  
            INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL 
            WHERE ttkkit.KKNo = xKKNo AND NoAccount = '24050200') ttkkit       
         INNER JOIN        
         (SELECT KMNo, KMTgl, KMNominal, KMJenis, KMLink, KMJam, vtkm.NoGL, (KreditGL+DebetGL) AS KreditGL  FROM vtkm 
            INNER JOIN ttgeneralledgerhd ON vtkm.NoGL = ttgeneralledgerhd.NoGL  
            INNER JOIN ttgeneralledgerit ON ttgeneralledgerit.NoGL = ttgeneralledgerhd.NoGL 
            WHERE KMJenis = 'BBN Plus' AND NoAccount = '24050200' AND KMLINk IN (SELECT FBNo FROM ttkkit WHERE KKNO = xKKNo)) ttkm        
         ON ttkm.KMLink = ttkkit.FBNo
         GROUP BY KMNo;        			
			INSERT INTO ttglhpit SELECT xNoGL, @BKTgl, KMNo, '24050200','Hutang', KreditGL FROM TSearchLink WHERE BKBayar > KreditGL AND KreditGL > 0;
			INSERT INTO ttglhpit SELECT xNoGL, @BKTgl, KMNo, '24050200','Hutang', BKBayar FROM TSearchLink WHERE BKBayar <= KreditGL AND BKBayar > 0;

		END;
		WHEN 'BBN Acc' THEN
		BEGIN
		END;
	END CASE;
	
	#Memberi Nilai HPLink
	SET @CountGL = 0;
	SELECT MAX(GLLink), IFNULL(COUNT(NoGL),0) INTO @GLLink, @CountGL FROM ttglhpit WHERE NoGL = xNoGL GROUP BY NoGL;
	IF @CountGL = 0 THEN
		UPDATE ttgeneralledgerhd SET HPLink = '--' WHERE NoGL = xNoGL;
	ELSEIF @CountGL = 1 THEN
		UPDATE ttgeneralledgerhd SET HPLink = @GLLink WHERE NoGL = xNoGL;
	ELSE
		UPDATE ttgeneralledgerhd SET HPLink = 'Many' WHERE NoGL = xNoGL;						
	END IF;	
	
	#Menghitung Total Debet & Kredit	
	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttKK SET NoGL = xNoGL  WHERE KKNo = xKKNo;
END$$

DELIMITER ;

