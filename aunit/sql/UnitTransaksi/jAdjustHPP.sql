DELIMITER $$

DROP PROCEDURE IF EXISTS `jAdjustHPP`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jAdjustHPP`(IN xFBNo VARCHAR(18))
BEGIN
	DECLARE oAMNo VARCHAR(18) DEFAULT '--';
	DECLARE oTotSelisih DOUBLE;
	DECLARE oTotSKSD VARCHAR(300);
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';

	SELECT FBNo, FBTgl, UserID, FBJam INTO @FBNo, @FBTgl, @UserID, @FBJam FROM ttfb WHERE FBNo = xFBNo;
	SELECT AMNo INTO oAMNo FROM ttamhd WHERE FBNo =  xFBNo;
	IF oAMNo = '--' THEN
		SET @AutoAMNo = CONCAT('AM',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoAMNo,LPAD(RIGHT(AMNo,5)+1,5,0)) INTO @AutoAMNo FROM TtAMhd WHERE ((AMNo LIKE CONCAT(@AutoAMNo,'%')) AND LENGTH(AMNo) = 10) ORDER BY AMNo DESC LIMIT 1;
		IF LENGTH(@AutoAMNo) <> 10 THEN SET @AutoAMNo = CONCAT('AM',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET oAMNo = @AutoAMNo;
	ELSE
		SELECT NoGL INTO xNoGL FROM ttgeneralledgerhd WHERE GLLink = oAMNo;
		DELETE FROM ttamhd WHERE AMNo = oAMNo;
		DELETE FROM ttamit WHERE AMNo = oAMNo;
	END IF;

	DROP TEMPORARY TABLE IF EXISTS SKSDvsFB;
	CREATE TEMPORARY TABLE IF NOT EXISTS SKSDvsFB AS
	SELECT MotorAutoN, MotorNoMesin,  tmotor.SKNo AS SKNo, (SKHarga-FBHarga) AS Harga FROM tmotor
	LEFT OUTER JOIN ttfb ON ttfb.FBNo = tmotor.FBNo
	LEFT OUTER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo
	WHERE Tmotor.FBNO = xFBNo AND (FBHarga <> SKHarga) AND tmotor.SKNo <> '--'
	UNION
	SELECT MotorAutoN, MotorNoMesin,  tmotor.SDNo AS SKNo, (SDHarga-FBHarga) AS Harga FROM tmotor
	LEFT OUTER JOIN ttfb ON ttfb.FBNo = tmotor.FBNo
	LEFT OUTER JOIN ttsd ON ttsd.SDNo = tmotor.SDNo
	WHERE Tmotor.FBNO = xFBNo AND (FBHarga <> SDHarga) AND tmotor.SDNo <> '--';

	SELECT IFNULL(SUM(Harga),0), LEFT(GROUP_CONCAT(SKNo),300) INTO oTotSelisih, oTotSKSD FROM SKSDvsFB;

	IF oTotSelisih <> 0 THEN
		INSERT INTO ttamit SELECT oAMNo, MotorNoMesin, MotorAutoN FROM SKSDvsFB;
		INSERT INTO ttamhd SELECT oAMNo, @FBTgl, @FBJam, CONCAT('Adjust HPP SK : ',oTotSKSD), oTotSelisih, '--', @UserID, YEAR(@FBTgl), xFBNo;
		UPDATE tmotor INNER JOIN SKSDvsFB ON SKSDvsFB.MotorAutoN = tmotor.MotorAutoN AND SKSDvsFB.MotorNoMesin = tmotor.MotorNoMesin SET tmotor.MMHarga = SKSDvsFB.Harga;

		IF xNoGL = '--' THEN
			SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
			IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		END IF;
		DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL;  DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;  DELETE FROM ttglhpit WHERE NoGL = xNoGL;
		INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
		VALUES (xNoGL, @FBTgl, 'AM', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',oAMNo), 0, 0, oAMNo, @UserID, '--', 'Belum');
		IF oTotSelisih < 0 THEN
		  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		  (xNoGL, @FBTgl, 1, '50000000', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',@oAMNo), ABS(oTotSelisih), 0);
		  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		  (xNoGL, @FBTgl, 2, '11150100', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',@oAMNo), 0, ABS(oTotSelisih));
		ELSE
		  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		  (xNoGL, @FBTgl, 1, '11150100', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',@oAMNo), ABS(oTotSelisih), 0);
		  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		  (xNoGL, @FBTgl, 2, '50000000', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',@oAMNo), 0, ABS(oTotSelisih));
		END IF;
	END IF;

	/* FB vs RK */
	SET @AMNo = oAMNo; SET oAMNo = '--';
	SELECT AMNo INTO oAMNo FROM ttamhd WHERE FBNo = CONCAT(@AMNo,'-RK');
	IF oAMNo = '--' THEN
		SET @AutoAMNo = CONCAT('AM',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoAMNo,LPAD(RIGHT(AMNo,5)+1,5,0)) INTO @AutoAMNo FROM TtAMhd WHERE ((AMNo LIKE CONCAT(@AutoAMNo,'%')) AND LENGTH(AMNo) = 10) ORDER BY AMNo DESC LIMIT 1;
		IF LENGTH(@AutoAMNo) <> 10 THEN SET @AutoAMNo = CONCAT('AM',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET oAMNo = @AutoAMNo;
	ELSE
		SELECT NoGL INTO xNoGL FROM ttgeneralledgerhd WHERE GLLink = oAMNo;
		DELETE FROM ttamhd WHERE AMNo = oAMNo;
		DELETE FROM ttamit WHERE AMNo = oAMNo;
	END IF;
	DROP TEMPORARY TABLE IF EXISTS DafFBMotor;
	CREATE TEMPORARY TABLE IF NOT EXISTS DafFBMotor AS
  SELECT MotorAutoN, MotorNoMesin FROM tmotor WHERE FBNo = xFBNo;

	DROP TEMPORARY TABLE IF EXISTS SKSDvsFB;
	CREATE TEMPORARY TABLE IF NOT EXISTS SKSDvsFB AS
	SELECT tmotor.MotorAutoN, tmotor.MotorNoMesin, RKNo AS NomorTrans, (FBHarga-RKHarga) AS Selisih FROM tmotor
	INNER JOIN DafFBMotor ON DafFBMotor.MotorNoMesin = tmotor.MotorNoMesin  WHERE DafFBMotor.MotorAutoN <> tmotor.MotorAutoN AND (RKHarga <> FBHarga) AND RKNo <> '--' ;
	SELECT IFNULL(SUM(Selisih),0), LEFT(GROUP_CONCAT(NomorTrans),300) INTO oTotSelisih, oTotSKSD FROM SKSDvsFB;

	IF oTotSelisih <> 0 THEN
		INSERT INTO ttamit SELECT oAMNo, MotorNoMesin, MotorAutoN FROM SKSDvsFB;
		INSERT INTO ttamhd SELECT oAMNo, @FBTgl, @FBJam, CONCAT('Adjust HPP RK : ',oTotSKSD), oTotSelisih, '--', @UserID, YEAR(@FBTgl), CONCAT(xFBNo,'-RK');
		UPDATE tmotor INNER JOIN SKSDvsFB ON SKSDvsFB.MotorAutoN = tmotor.MotorAutoN AND SKSDvsFB.MotorNoMesin = tmotor.MotorNoMesin SET tmotor.MMHarga = SKSDvsFB.Harga;
		IF xNoGL = '--' THEN
			SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
			IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		END IF;
		DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL;  DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;  DELETE FROM ttglhpit WHERE NoGL = xNoGL;
		INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
		VALUES (xNoGL, @FBTgl, 'AM', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',oAMNo, ' - RK'), 0, 0, oAMNo, @UserID, '--', 'Belum');
		IF oTotSelisih < 0 THEN
		  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		  (xNoGL, @FBTgl, 1, '50000000', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',@oAMNo, ' - RK'), ABS(oTotSelisih), 0);
		  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		  (xNoGL, @FBTgl, 2, '11150100', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',@oAMNo, ' - RK'), 0, ABS(oTotSelisih));
		ELSE
		  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		  (xNoGL, @FBTgl, 1, '11150100', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',@oAMNo, ' - RK'), ABS(oTotSelisih), 0);
		  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		  (xNoGL, @FBTgl, 2, '50000000', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',@oAMNo, ' - RK'), 0, ABS(oTotSelisih));
		END IF;
	END IF;

  /* FB vs PD */
	SET @AMNo = oAMNo; SET oAMNo = '--';
	SELECT AMNo INTO oAMNo FROM ttamhd WHERE FBNo = CONCAT(@AMNo,'-PD');
	IF oAMNo = '--' THEN
		SET @AutoAMNo = CONCAT('AM',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoAMNo,LPAD(RIGHT(AMNo,5)+1,5,0)) INTO @AutoAMNo FROM TtAMhd WHERE ((AMNo LIKE CONCAT(@AutoAMNo,'%')) AND LENGTH(AMNo) = 10) ORDER BY AMNo DESC LIMIT 1;
		IF LENGTH(@AutoAMNo) <> 10 THEN SET @AutoAMNo = CONCAT('AM',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET oAMNo = @AutoAMNo;
	ELSE
		SELECT NoGL INTO xNoGL FROM ttgeneralledgerhd WHERE GLLink = oAMNo;
		DELETE FROM ttamhd WHERE AMNo = oAMNo;
		DELETE FROM ttamit WHERE AMNo = oAMNo;
	END IF;
	DROP TEMPORARY TABLE IF EXISTS DafFBMotor;
	CREATE TEMPORARY TABLE IF NOT EXISTS DafFBMotor AS
	SELECT MotorAutoN, MotorNoMesin FROM tmotor WHERE FBNo = xFBNo;

	DROP TEMPORARY TABLE IF EXISTS SKSDvsFB;
	CREATE TEMPORARY TABLE IF NOT EXISTS SKSDvsFB AS
	SELECT tmotor.MotorAutoN, tmotor.MotorNoMesin, PDNo AS NomorTrans, (FBHarga-SSHarga) AS Selisih FROM tmotor
	INNER JOIN DafFBMotor ON DafFBMotor.MotorNoMesin = tmotor.MotorNoMesin  WHERE DafFBMotor.MotorAutoN <> tmotor.MotorAutoN AND (SSHarga <> FBHarga) AND PDNo <> '--' ;
	SELECT IFNULL(SUM(Selisih),0), LEFT(GROUP_CONCAT(NomorTrans),300) INTO oTotSelisih, oTotSKSD FROM SKSDvsFB;

	IF oTotSelisih <> 0 THEN
		INSERT INTO ttamit SELECT oAMNo, MotorNoMesin, MotorAutoN FROM SKSDvsFB;
		INSERT INTO ttamhd SELECT oAMNo, @FBTgl, @FBJam, CONCAT('Adjust HPP PD : ',oTotSKSD), oTotSelisih, '--', @UserID, YEAR(@FBTgl), CONCAT(xFBNo,'-PD');
		UPDATE tmotor INNER JOIN SKSDvsFB ON SKSDvsFB.MotorAutoN = tmotor.MotorAutoN AND SKSDvsFB.MotorNoMesin = tmotor.MotorNoMesin SET tmotor.MMHarga = SKSDvsFB.Harga;
		IF xNoGL = '--' THEN
			SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
			IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		END IF;
		DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL;  DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;  DELETE FROM ttglhpit WHERE NoGL = xNoGL;
		INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
		VALUES (xNoGL, @FBTgl, 'AM', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',oAMNo, ' - PD'), 0, 0, oAMNo, @UserID, '--', 'Belum');
		IF oTotSelisih < 0 THEN
		  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		  (xNoGL, @FBTgl, 1, '50000000', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',@oAMNo, ' - PD'), ABS(oTotSelisih), 0);
		  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		  (xNoGL, @FBTgl, 2, '11150100', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',@oAMNo, ' - PD'), 0, ABS(oTotSelisih));
		ELSE
		  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		  (xNoGL, @FBTgl, 1, '11150100', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',@oAMNo, ' - PD'), ABS(oTotSelisih), 0);
		  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		  (xNoGL, @FBTgl, 2, '50000000', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',@oAMNo, ' - PD'), 0, ABS(oTotSelisih));
		END IF;
	END IF;

	/* FB vs SK/SD */
	SET @AMNo = oAMNo; SET oAMNo = '--';
	SELECT AMNo INTO oAMNo FROM ttamhd WHERE FBNo = CONCAT(@AMNo,'-SK/ME');
	IF oAMNo = '--' THEN
		SET @AutoAMNo = CONCAT('AM',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoAMNo,LPAD(RIGHT(AMNo,5)+1,5,0)) INTO @AutoAMNo FROM TtAMhd WHERE ((AMNo LIKE CONCAT(@AutoAMNo,'%')) AND LENGTH(AMNo) = 10) ORDER BY AMNo DESC LIMIT 1;
		IF LENGTH(@AutoAMNo) <> 10 THEN SET @AutoAMNo = CONCAT('AM',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET oAMNo = @AutoAMNo;
	ELSE
		SELECT NoGL INTO xNoGL FROM ttgeneralledgerhd WHERE GLLink = oAMNo;
		DELETE FROM ttamhd WHERE AMNo = oAMNo;
		DELETE FROM ttamit WHERE AMNo = oAMNo;
	END IF;
	DROP TEMPORARY TABLE IF EXISTS DafFBMotor;
	CREATE TEMPORARY TABLE IF NOT EXISTS DafFBMotor AS
	SELECT MotorAutoN, MotorNoMesin FROM tmotor WHERE FBNo = xFBNo;

	DROP TEMPORARY TABLE IF EXISTS SKSDvsFB;
	CREATE TEMPORARY TABLE IF NOT EXISTS SKSDvsFB AS
	SELECT tmotor.MotorAutoN, tmotor.MotorNoMesin, SKNo AS NomorTrans, (FBHarga-SKHarga) AS Selisih FROM tmotor
	INNER JOIN DafFBMotor ON DafFBMotor.MotorNoMesin = tmotor.MotorNoMesin  WHERE DafFBMotor.MotorAutoN <> tmotor.MotorAutoN AND (SKHarga <> FBHarga) AND PDNo <> '--';

	INSERT INTO SKSDvsFB
	SELECT tmotor.MotorAutoN, tmotor.MotorNoMesin, SDNo AS NomorTrans, (FBHarga-SDHarga) AS Selisih FROM tmotor
	INNER JOIN DafFBMotor ON DafFBMotor.MotorNoMesin = tmotor.MotorNoMesin  WHERE DafFBMotor.MotorAutoN <> tmotor.MotorAutoN AND (SDHarga <> FBHarga) AND SDNo <> '--' ;
	SELECT IFNULL(SUM(Selisih),0), LEFT(GROUP_CONCAT(NomorTrans),300) INTO oTotSelisih, oTotSKSD FROM SKSDvsFB;

	IF oTotSelisih <> 0 THEN
		INSERT INTO ttamit SELECT oAMNo, MotorNoMesin, MotorAutoN FROM SKSDvsFB;
		INSERT INTO ttamhd SELECT oAMNo, @FBTgl, @FBJam, CONCAT('Adjust HPP SK/ME : ',oTotSKSD), oTotSelisih, '--', @UserID, YEAR(@FBTgl), CONCAT(xFBNo,'-SK/ME');
		UPDATE tmotor INNER JOIN SKSDvsFB ON SKSDvsFB.MotorAutoN = tmotor.MotorAutoN AND SKSDvsFB.MotorNoMesin = tmotor.MotorNoMesin SET tmotor.MMHarga = SKSDvsFB.Harga;
		IF xNoGL = '--' THEN
			SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
			SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
			IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		END IF;
		DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL;  DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;  DELETE FROM ttglhpit WHERE NoGL = xNoGL;
		INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
		VALUES (xNoGL, @FBTgl, 'AM', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',oAMNo, ' - SK/ME'), 0, 0, oAMNo, @UserID, '--', 'Belum');
		IF oTotSelisih < 0 THEN
		  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		  (xNoGL, @FBTgl, 1, '50000000', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',@oAMNo, ' - SK/ME'), ABS(oTotSelisih), 0);
		  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		  (xNoGL, @FBTgl, 2, '11150100', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',@oAMNo, ' - SK/ME'), 0, ABS(oTotSelisih));
		ELSE
		  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		  (xNoGL, @FBTgl, 1, '11150100', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',@oAMNo, ' - SK/ME'), ABS(oTotSelisih), 0);
		  INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
		  (xNoGL, @FBTgl, 2, '50000000', CONCAT('Post Penyesuaian Nilai Persediaan Motor - ',@oAMNo, ' - SK/ME'), 0, ABS(oTotSelisih));
		END IF;
	END IF;
END$$

DELIMITER ;

