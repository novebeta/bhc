DELIMITER $$

DROP PROCEDURE IF EXISTS `jss`$$

CREATE DEFINER=`root`@`%` PROCEDURE `jss`(IN xSSNo VARCHAR(18), IN xUserID VARCHAR(15))
sp:BEGIN
	DECLARE xNoGL VARCHAR(10) DEFAULT '--';
	DECLARE xGLValid VARCHAR(5) DEFAULT 'Belum';
	DECLARE oSumSSHarga DECIMAL(19,2) DEFAULT 0;

	SELECT NoGL, GLValid INTO xNoGL, xGLValid FROM ttgeneralledgerhd WHERE GLLink = xSSNo LIMIT 1;
	IF xGLValid = 'Sudah' THEN  LEAVE sp;  END IF;
	SELECT SSNo, SSTgl, FBNo, SupKode, LokasiKode, SSMemo, UserID, SSJam, SSNoTerima, DONo, NoGL INTO
	@SSNo, @SSTgl, @FBNo, @SupKode, @LokasiKode, @SSMemo, @UserID, @SSJam, @SSNoTerima, @DONo, @NoGL
	FROM ttss WHERE SSNo = xSSNo;
	SELECT IFNULL(SUM(SSHarga),0) INTO oSumSSHarga FROM tmotor WHERE SSNo = xSSNo;

	IF xNoGL = '--' OR xNoGL <> @NoGL THEN
		SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-');
		SELECT CONCAT(@AutoNoGL,LPAD((RIGHT(NoGL,5)+1),5,0)) INTO @AutoNoGL FROM TTGeneralLedgerHd WHERE ((NoGL LIKE CONCAT(@AutoNoGL,'%')) AND LENGTH(NoGL) = 10) ORDER BY NoGL DESC LIMIT 1;
		IF LENGTH(@AutoNoGL) <> 10 THEN SET @AutoNoGL = CONCAT('JT',RIGHT(YEAR(NOW()),2),'-00001');  END IF;
		SET xNoGL = @AutoNoGL;
	ELSE
		SET xNoGL = @NoGL;
	END IF;

	DELETE FROM ttgeneralledgerhd WHERE NoGL = xNoGL; DELETE FROM ttgeneralledgerit WHERE NoGL = xNoGL;DELETE FROM ttglhpit WHERE NoGL = xNoGL;
   DELETE FROM ttgeneralledgerhd WHERE GLLink = xSSNo;

	INSERT INTO ttgeneralledgerhd (NoGL, TglGL, KodeTrans, MemoGL, TotalDebetGL, TotalKreditGL, GLLink, UserID, HPLink, GLValid)
	VALUES (xNoGL, @SSTgl, 'SS', CONCAT('Post Terima Barang dari ',@SupKode), oSumSSHarga, oSumSSHarga, xSSNo, xUserID, '--', xGLValid);

	INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
	(xNoGL, @SSTgl, 1, '11150100', CONCAT('Post Terima Barang dari ',@SupKode), oSumSSHarga, 0);

	INSERT INTO ttgeneralledgerit (NoGL, TglGL, GLAutoN, NoAccount, KeteranganGL, DebetGL, KreditGL) VALUES
	(xNoGL, @SSTgl, 2, '11150200', CONCAT('Post Terima Barang dari ',@SupKode), 0, oSumSSHarga);

	UPDATE ttgeneralledgerhd INNER JOIN (SELECT NoGL, SUM(DebetGL) AS SumDebet, SUM(KreditGL) AS SumKredit FROM ttgeneralledgerit WHERE NoGL = xNoGL) Total
	ON Total.NoGL = ttgeneralledgerhd.NoGL SET TotalDebetGL = SumDebet, TotalKreditGL = SumKredit;

	UPDATE ttSS SET NoGL = xNoGL  WHERE SSNo = xSSNo;
END$$

DELIMITER ;

