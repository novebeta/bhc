<?php

namespace console\controllers;

use common\components\Api;
use common\components\General;
use DateTime;
use Exception;
use Yii;
use yii\helpers\Json;

class AccController extends \yii\console\Controller
{
    protected function sendWAbyOutlet($outlet){
        try{
            $db_arr   = Yii::$app->components['db'];
            $connection = new \yii\db\Connection([
                'dsn'      => $db_arr['dsn'] . 'dbname=' . $outlet . ';',
                'username' => $db_arr['username'],
                'password' => $db_arr['password'],
            ]);
            $connection->open();
            // echo $outlet;
            echo "\n";
            General::cCmd("use $outlet")->execute();
            $rowData = General::cCmd( "CALL fkkwa ('Display',@Status,@Keterangan,'Foen','System','Muntilan','','','','','','','','2022-01-01','2022-12-31','', ' >= ','0');" )
                    ->queryAll();
            foreach ($rowData as $value) {
                $rowDataPesan = General::cCmd( "CALL fkkwa ('Send',@Status,@Keterangan,'','','".$value['LokasiKode']."','".$value['KKNo']."','".$value['Dealer']."','','','','','','2022-01-01','2022-12-31','', ' >= ','0');" )
                ->queryAll();
                if(!empty($rowDataPesan)){
                    // echo $rowDataPesan[0]['Pesan'];
                    // $rowDataPesan[0]['NoHP'] = '087738074320';
                    $noHps = explode(';',$rowDataPesan[0]['NoHP']);
                    foreach ($noHps as $noHpItem) {
                        $data = json_encode(['CusTelepon'=>$noHpItem]);
                        General::send('cen','',$rowDataPesan[0]['Pesan'],$data);
                        $commandLog = $connection->createCommand()->batchInsert('tuserlog', [
                            'LogTglJam','UserID','LogJenis','LogNama','LogTabel','NoTrans'
                        ],[[
                            'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System',
                            'LogJenis'=>'Aju','LogNama'=>substr($rowDataPesan[0]['NoHP'].' - '.$rowDataPesan[0]['Pesan'],0,300),
                            'LogTabel'=>'ttkk','NoTrans'=>$value['KKNo']
                        ]]);
                        $commandLog->execute();
                    }
                }
            }
        }catch(Exception $e){
            echo "Error : ".$e->getMessage();
            $commandLog = $connection->createCommand()->batchInsert('tuserlog', [
                'LogTglJam','UserID','LogJenis','LogNama','LogTabel','NoTrans'
            ],[[
                'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System',
                'LogJenis'=>'Aju','LogNama'=>substr($e->getMessage(),0,300),
                'LogTabel'=>'ttkk','NoTrans'=>'CLI'
            ]]);
            $commandLog->execute();
        }
    }
    protected function sendWAKasir($outlet){
        try{
            $db_arr   = Yii::$app->components['db'];
            $connection = new \yii\db\Connection([
                'dsn'      => $db_arr['dsn'] . 'dbname=' . $outlet . ';',
                'username' => $db_arr['username'],
                'password' => $db_arr['password'],
            ]);
            $connection->open();
            // echo $outlet;
            echo "\n";
            General::cCmd("use $outlet")->execute();
            $rowData = General::cCmd( "CALL fkkwa ('Display',@Status,@Keterangan,'Foen','SystemKasir','Muntilan','','','','','','','','2022-01-01','2023-12-31','', ' >= ','0');" )
                    ->queryAll();
            foreach ($rowData as $value) {
                $rowDataPesan = General::cCmd( "CALL fkkwa ('SendKasir',@Status,@Keterangan,'','','".$value['LokasiKode']."','".$value['KKNo']."','".$value['Dealer']."','','','','','','2022-01-01','2022-12-31','', ' >= ','0');" )
                ->queryAll();
                if(!empty($rowDataPesan)){
                    // echo $rowDataPesan[0]['Pesan'];
                    // $rowDataPesan[0]['NoHP'] = '087738074320';
                    $noHps = explode(';',$rowDataPesan[0]['NoHP']);
                    foreach ($noHps as $noHpItem) {
                        $data = json_encode(['CusTelepon'=>$noHpItem]);
                        General::send('cen','',$rowDataPesan[0]['Pesan'],$data);
                        $commandLog = $connection->createCommand()->batchInsert('tuserlog', [
                            'LogTglJam','UserID','LogJenis','LogNama','LogTabel','NoTrans'
                        ],[[
                            'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System',
                            'LogJenis'=>'Aju','LogNama'=>substr($rowDataPesan[0]['NoHP'].' - '.$rowDataPesan[0]['Pesan'],0,300),
                            'LogTabel'=>'ttkk','NoTrans'=>$value['KKNo']
                        ]]);
                        $commandLog->execute();
                    }
                }
            }
        }catch(Exception $e){
            echo "Error : ".$e->getMessage();
            $commandLog = $connection->createCommand()->batchInsert('tuserlog', [
                'LogTglJam','UserID','LogJenis','LogNama','LogTabel','NoTrans'
            ],[[
                'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System',
                'LogJenis'=>'Aju','LogNama'=>substr($e->getMessage(),0,300),
                'LogTabel'=>'ttkk','NoTrans'=>'CLI'
            ]]);
            $commandLog->execute();
        }
    }
    public function actionSendWa($outlets = null)
    {
        if($outlets != null){
            $outlets = explode(',',$outlets);
        }
        foreach (Yii::$app->params['H1'] as $key => $outlet) {
            if(!empty($outlets) && !in_array($key,$outlets)){
                continue;
            }
            self::sendWAbyOutlet($key);
            self::sendWAKasir($key);
        }
    }
}
