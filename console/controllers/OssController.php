<?php

namespace console\controllers;

use aunit\models\Attachment;

class OssController extends \yii\console\Controller
{
    public function actionUpdateDoc($outlets = null)
    {
        if($outlets != null){
            $outlets = explode(',',$outlets);
        }
        $attachment = new Attachment();
        $attachment->updateDoc();
    }

    public function actionSingleUpdateDoc($outlet)
    {
        $attachment = new Attachment();
        $attachment->singleUpdateDoc($outlet);
    }
}
