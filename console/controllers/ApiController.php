<?php

namespace console\controllers;

use common\components\Api;
use DateTime;
use Exception;
use Yii;
use yii\helpers\Json;
//php -dxdebug.mode=debug -dxdebug.start_with_request=yes ./yii api/uinb cwb
class ApiController extends \yii\console\Controller
{
    static public function array_except($array, $removeKeys){
        $keys = array_keys($removeKeys);
        foreach($keys as $key){
            unset($array[$key]);
        }
        return $array;
    }
    public function actionBast($outlets = null)
    {
        $fromTime = Yii::$app->formatter->asDate('-1 day', 'yyyy-MM-dd');
        $toTime   = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        $addParam = [];
        if($outlets != null){
            $outlets = explode(',',$outlets);
        }
        foreach (Yii::$app->params['H1'] as $key => $outlet) {
            if(!empty($outlets) && !in_array($key,$outlets)){
                continue;
            }
            try{
                $api        = new Api(base64_encode($key));
                $mainDealer = $api->getMainDealer();
                switch ($mainDealer) {
                    case API::MD_HSO:
                        $arrParams = [
                            'fromTime' => $fromTime . ' 00:00:00',
                            'toTime'   => $toTime . ' 23:59:59',
                            'dealerId' => $api->getDealerId(),
                            "DeliveryDocumentId"=>"",
                            "idSPK"=>"",
                            "idCustomer"=>""
                        ];

                        break;
                    case API::MD_ANPER:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                        break;
                    default:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                }
                $arrParams = array_merge_recursive($arrParams, $addParam);
                $hasil     = $api->call(Api::TYPE_BAST, Json::encode($arrParams));
            }catch(Exception $e){
                echo "Error : ".$e->getMessage();
            }
            echo "\n";
        }
    }
    public function actionDoch($outlets = null)
    {
        $fromTime = Yii::$app->formatter->asDate('-1 day', 'yyyy-MM-dd');
        $toTime   = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        $addParam = [];
        if($outlets != null){
            $outlets = explode(',',$outlets);
        }
        foreach (Yii::$app->params['H1'] as $key => $outlet) {
            if(!empty($outlets) && !in_array($key,$outlets)){
                continue;
            }
            try{
                $api        = new Api(base64_encode($key));
                $mainDealer = $api->getMainDealer();
                switch ($mainDealer) {
                    case API::MD_HSO:
                        $arrParams = [
                            'fromTime' => $fromTime . ' 00:00:00',
                            'toTime'   => $toTime . ' 23:59:59',
                            'dealerId' => $api->getDealerId(),
                        ];

                        break;
                    case API::MD_ANPER:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                        break;
                    default:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                }
                $arrParams = array_merge_recursive($arrParams, $addParam);
                $hasil     = $api->call(Api::TYPE_DOCH, Json::encode($arrParams));
            }catch(Exception $e){
                echo "Error : ".$e->getMessage();
            }
            echo "\n";
        }
    }
    public function actionLsng($outlets = null)
    {
        $fromTime = Yii::$app->formatter->asDate('-1 day', 'yyyy-MM-dd');
        $toTime   = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        $addParam = [];
        if($outlets != null){
            $outlets = explode(',',$outlets);
        }
        foreach (Yii::$app->params['H1'] as $key => $outlet) {
            if(!empty($outlets) && !in_array($key,$outlets)){
                continue;
            }
            try{
                $api        = new Api(base64_encode($key));
                $mainDealer = $api->getMainDealer();
                switch ($mainDealer) {
                    case API::MD_HSO:
                        $arrParams = [
                            'fromTime' => $fromTime . ' 00:00:00',
                            'toTime'   => $toTime . ' 23:59:59',
                            'dealerId' => $api->getDealerId(),
                        ];

                        break;
                    case API::MD_ANPER:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                        break;
                    default:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                }
                $arrParams = array_merge_recursive($arrParams, $addParam);
                $hasil     = $api->call(Api::TYPE_LSNG, Json::encode($arrParams));
            }catch(Exception $e){
                echo "Error : ".$e->getMessage();
            }
            echo "\n";
        }
    }
    public function actionUinb($outlets = null)
    {
        $fromTime = Yii::$app->formatter->asDate('-1 day', 'yyyy-MM-dd');
        $toTime   = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        $addParam = [];
        if($outlets != null){
            $outlets = explode(',',$outlets);
        }
        foreach (Yii::$app->params['H1'] as $key => $outlet) {
            if(!empty($outlets) && !in_array($key,$outlets)){
                continue;
            }
            try{
                $api        = new Api(base64_encode($key));
                $mainDealer = $api->getMainDealer();
                switch ($mainDealer) {
                    case API::MD_ANPER:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                        break;
                    default:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'poId'                   => '',
                            'noShippingList'         => '',
                        ];
                }
                $arrParams = array_merge_recursive($arrParams, $addParam);
                $hasil     = $api->call(Api::TYPE_UINB, Json::encode($arrParams));
            }catch(Exception $e){
                echo "Error : ".$e->getMessage();
            }
            echo "\n";
        }
    }
    public function actionSpk($outlets = null)
    {
        $fromTime = Yii::$app->formatter->asDate('-1 day', 'yyyy-MM-dd');
        $toTime   = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        $addParam = [];
        if($outlets != null){
            $outlets = explode(',',$outlets);
        }
        foreach (Yii::$app->params['H1'] as $key => $outlet) {
            if(!empty($outlets) && !in_array($key,$outlets)){
                continue;
            }
            try{
                $api        = new Api(base64_encode($key));
                $mainDealer = $api->getMainDealer();
                switch ($mainDealer) {
                    case API::MD_HSO:
                        $arrParams = [
                            'fromTime' => $fromTime . ' 00:00:00',
                            'toTime'   => $toTime . ' 23:59:59',
                            'dealerId' => $api->getDealerId(),
                            'idProspect' => '',
                            'idSalesPeople' => '',
                        ];

                        break;
                    case API::MD_ANPER:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                        break;
                    default :
                        $arrParams = [
                            'fromTime' => $fromTime . ' 00:00:00',
                            'toTime'   => $toTime . ' 23:59:59',
                            'dealerId' => $api->getDealerId(),
                        ];
                        break;
                }
                $arrParams = array_merge_recursive($arrParams, $addParam);
                $hasil     = $api->call(Api::TYPE_SPK, Json::encode($arrParams));
            }catch(Exception $e){
                echo "Error : ".$e->getMessage();
            }
            echo "\n";
        }
    }
    public function actionPrsp($outlets = null)
    {
        $fromTime = Yii::$app->formatter->asDate('-1 day', 'yyyy-MM-dd');
        $toTime   = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        $addParam = [];
        if($outlets != null){
            $outlets = explode(',',$outlets);
        }
        foreach (Yii::$app->params['H1'] as $key => $outlet) {
            if(!empty($outlets) && !in_array($key,$outlets)){
                continue;
            }
            try{
                $api        = new Api(base64_encode($key));
                $mainDealer = $api->getMainDealer();
                switch ($mainDealer) {
                    case API::MD_HSO:
                        $arrParams = [
                            'fromTime' => $fromTime . ' 00:00:00',
                            'toTime'   => $toTime . ' 23:59:59',
                            'dealerId' => $api->getDealerId(),
                            'idProspect' => '',
                            'idSalesPeople' => '',
                        ];

                        break;
                    case API::MD_ANPER:
                        $arrParams = [
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                        break;
                    default:
                        $arrParams = [
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                }
                $arrParams = array_merge_recursive($arrParams, $addParam);
                $hasil     = $api->call(Api::TYPE_PRSP, Json::encode($arrParams),[],true);
            }catch(Exception $e){
                echo "Error : ".$e->getMessage();
            }
            echo "\n";
        }
    }
    public function actionInv1($outlets = null)
    {
        $db_arr   = Yii::$app->components['db'];
        $fromTime = Yii::$app->formatter->asDate('-1 day', 'yyyy-MM-dd');
        $toTime   = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        $addParam = [];
        if($outlets != null){
            $outlets = explode(',',$outlets);
        }
        foreach (Yii::$app->params['H1'] as $key => $outlet) {
            if(!empty($outlets) && !in_array($key,$outlets)){
                continue;
            } 
            try{
                $sukses = $gagal = 0;
                echo "Outlet : " . $key;
                $connection = new \yii\db\Connection([
                    'dsn'      => $db_arr['dsn'] . 'dbname=' . $key . ';',
                    'username' => $db_arr['username'],
                    'password' => $db_arr['password'],
                ]);
                $connection->open();
                $command = $connection->createCommand("SELECT CONCAT('TJ-',DealerKodeDAstra,'-',RIGHT(YEAR(SKTgl),2),'-',LPAD(MONTH(SKTgl),2,0),'-',RIGHT(ttsk.SKNo,5))  AS idInvoice,
                REPLACE(zidSPK,'--','') AS idSPK, REPLACE(zidCustomer,'--','') AS idCustomer, CONVERT(DKDPTerima,decimal(15)) AS Amount, 
                'Cash' AS tipePembayaran, 'Cash' AS caraBayar, 'Close' AS STATUS, 
                DKJenis AS Note, 
                DATE_FORMAT(SKJam,'%d/%m/%Y %T') AS createTime
                FROM ttdk 
                INNER JOIN tdcustomer ON tdcustomer.CusKode = ttdk.CusKode
                INNER JOIN tmotor ON tmotor.DKNo = ttdk.DKNo
                INNER JOIN ttsk ON ttsk.SKNo = tmotor.SKNo
                INNER JOIN tddealer ON tddealer.DealerStatus = 1
                WHERE  DATE(SKJam) BETWEEN CURDATE() - INTERVAL 13 DAY AND CURDATE() 
                ORDER BY SKJam DESC");
                $records = $command->queryAll();
                // echo json_encode($records);
                if(sizeof($records) == 0){
                    echo " Tidak ada data.\n";
                    continue;
                }
                foreach ($records as $item) {
                    $api        = new Api(base64_encode($key));
                    $mainDealer = $api->getMainDealer();
                    switch ($mainDealer) {
                        case API::MD_HSO:
                            $arrParams = [
                                // 'fromTime' => $fromTime . ' 00:00:00',
                                // 'toTime'   => $toTime . ' 23:59:59',
                                // 'dealerId' => $api->getDealerId(),
                            ];

                            break;
                        case API::MD_ANPER:
                            $arrParams = [
                                'dealerId'               => $api->getDealerId(),
                                'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                                'requestServertimezone'  => date('T'),
                                'requestServerTimestamp' => $api->getCurrUnixTime(),
                            ];
                            break;
                        default:
                            $arrParams = [
                                'dealerId'               => $api->getDealerId(),
                                'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                                'requestServertimezone'  => date('T'),
                                'requestServerTimestamp' => $api->getCurrUnixTime(),
                            ];
                    }
                    $addParam = [
                        'idInvoice'      => $item['idInvoice'],
                        "idSPK"          => $item['idSPK'],
                        "idCustomer"     => $item['idCustomer'],
                        "amount"         => $item['Amount'],
                        "tipePembayaran" => $item['tipePembayaran'],
                        "caraBayar"      => $item['caraBayar'],
                        "status"         => $item['STATUS'],
                        "note"           => $item['Note'],
                        "createdTime"    => $item['createTime'],
                    ];
                    $arrParams = array_merge_recursive($arrParams, $addParam);
                    $hasil     = $api->call(Api::TYPE_INV1, Json::encode($arrParams,JSON_NUMERIC_CHECK),[],false);
                    $arrHasil  = Json::decode($hasil);
                    // echo "\n".$arrHasil."\n";
                    if ($arrHasil['status'] ?? false) {
                        $sukses++;
                    } else {
                        $gagal++;
                        echo "\n".Json::encode($arrParams,JSON_NUMERIC_CHECK)."\n";
                        echo "\n".Json::encode($arrHasil,JSON_NUMERIC_CHECK)."\n";
                    }
                }
                $hasil = " Berhasil : " . $sukses . ", Gagal : " . $gagal;
                echo $hasil;
                $connection->createCommand()->insert('tuserlog', [
                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>$hasil,'LogTabel'=>'inv1','NoTrans'=>'CLI'
                ])->execute();
            }catch(Exception $e){
                echo "Error : ".$e->getMessage();
            }
            echo "\n";
        }
    }
    public function actionInv1Read($outlets = null)
    {
        $fromTime = Yii::$app->formatter->asDate('-1 day', 'yyyy-MM-dd');
        $toTime   = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        $addParam = [];
        if($outlets != null){
            $outlets = explode(',',$outlets);
        }
        foreach (Yii::$app->params['H1'] as $key => $outlet) {
            if(!empty($outlets) && !in_array($key,$outlets)){
                continue;
            }
            try{
                $api        = new Api(base64_encode($key));
                $mainDealer = $api->getMainDealer();
                if(empty(Yii::$app->params['mainDealer'][$mainDealer][Api::TYPE_INV1_READ])){
                    echo "Outlet : " . $key . ", not implemented yet\n";
                    continue;
                }
                switch ($mainDealer) {
                    case API::MD_HSO:
                        $arrParams = [
                            'fromTime' => $fromTime . ' 00:00:00',
                            'toTime'   => $toTime . ' 23:59:59',
                            'dealerId' => $api->getDealerId(),
                            'idSPK' => '',
                            'idCustomer' => '',
                        ];

                        break;
                    case API::MD_ANPER:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                        break;
                    default:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                }
                $arrParams = array_merge_recursive($arrParams, $addParam);
                $hasil     = $api->call(Api::TYPE_INV1_READ, Json::encode($arrParams),[],true);
            }catch(Exception $e){
                echo "Error : ".$e->getMessage();
            }
            echo "\n";
        }
    }
    public function actionCdb($outlets = null)
    {
        $fromTime = Yii::$app->formatter->asDate('-1 day', 'yyyy-MM-dd');
        $toTime   = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        $addParam = [];
        if($outlets != null){
            $outlets = explode(',',$outlets);
        }
        foreach (Yii::$app->params['H1'] as $key => $outlet) {
            if(!empty($outlets) && !in_array($key,$outlets)){
                continue;
            }
            try{
                $api        = new Api(base64_encode($key));
                $mainDealer = $api->getMainDealer();
                if(empty(Yii::$app->params['mainDealer'][$mainDealer][Api::TYPE_CDB])){
                    echo "Outlet : " . $key . ", not implemented yet\n";
                    continue;
                }
                switch ($mainDealer) {
                    case API::MD_HSO:
                        $arrParams = [
                            'fromTime' => $fromTime . ' 00:00:00',
                            'toTime'   => $toTime . ' 23:59:59',
                            'dealerId' => $api->getDealerId(),
                            'idSPK' => '',
                        ];

                        break;
                    case API::MD_ANPER:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                        break;
                    default:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                }
                $arrParams = array_merge_recursive($arrParams, $addParam);
                $hasil     = $api->call(Api::TYPE_CDB, Json::encode($arrParams),[],true);
            }catch(Exception $e){
                echo "Error : ".$e->getMessage();
            }
            echo "\n";
        }
    }
    public function actionInv2($outlets = null)
    {
        $db_arr   = Yii::$app->components['db'];
        $fromTime = Yii::$app->formatter->asDate('-1 day', 'yyyy-MM-dd');
        $toTime   = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        if($outlets != null){
            $outlets = explode(',',$outlets);
        }
        foreach (Yii::$app->params['H2'] as $key => $outlet) {
            if(!empty($outlets) && !in_array($key,$outlets)){
                continue;
            }
            try{
                $sukses = $gagal = 0;
                echo "Outlet : " . $key;
                $connection = new \yii\db\Connection([
                    'dsn'      => $db_arr['dsn'] . 'dbname=' . $key . ';',
                    'username' => $db_arr['username'],
                    'password' => $db_arr['password'],
                ]);
                $connection->open();
                $headers = $connection->createCommand("SELECT
                    REPLACE(ttsdhd.ZnoWorkOrder,'--','') AS noWorkOrder,
                    REPLACE(REPLACE(ttsdhd.ZnoWorkOrder, 'PKB', 'NJB'),'--','') AS noNJB,
                    DATE_FORMAT(ttsdhd.SDTgl,'%d/%m/%Y %T') AS tanggalNJB,
                    CONVERT(ttsdhd.SDTotalNonGratis,DECIMAL(15)) AS totalHargaNJB,
                    REPLACE(REPLACE(ttsdhd.ZnoWorkOrder, 'PKB', 'NSC'),'--','') AS noNSC,
                    DATE_FORMAT(ttsdhd.SDTgl,'%d/%m/%Y %T') AS tanggalNSC,
                    CONVERT(ttsdhd.SDTotalNonGratis,DECIMAL(15)) AS totalHargaNSC,
                    IF(REPLACE(SA.ZhondaId,'--','')='',hondaIdSA,SA.ZhondaId) AS HondaidSA,
                    IF(REPLACE(Mekanik.ZhondaId,'--','')='',hondaIdMekanik,hondaIdSA) AS HondaidMekanik
                FROM ttsdhd
                LEFT OUTER JOIN tdkaryawan Mekanik ON ttsdhd.KarKode = Mekanik.KarKode
                LEFT OUTER JOIN tdkaryawan SA ON ttsdhd.KarKodeSA = SA.KarKode
                LEFT OUTER JOIN txpkbhd ON txpkbhd.noWorkOrder = ttsdhd.ZnoWorkOrder
                WHERE ttsdhd.SDTgl  BETWEEN CURDATE() - INTERVAL 1 DAY AND CURDATE() AND ttsdhd.ZnoWorkOrder <> '--'
                #WHERE DATE(ttsdhd.SDTgl) = CURDATE()
                #AND ttsdhd.ZnoWorkOrder <> '--'
                GROUP BY ttsdhd.ZnoWorkOrder ORDER BY NoWorkOrder DESC;")->queryAll();
                // echo json_encode($records);
                if(sizeof($headers) == 0){
                    echo " Tidak ada data.\n";
                    continue;
                }
                foreach ($headers as $item) {
                    $addParam = [];
                    $njb = $connection->createCommand("SELECT '' AS idJob, CONVERT(SUM(SDHrgJual),decimal(15)) AS hargaServis, '' AS promoIdJasa, 
                    CONVERT(SUM(SDHrgJual),decimal(15)) AS discServiceAmount, CONVERT(SUM(SDJualDisc)/SUM(SDHrgJual)*100,decimal(15)) AS discServicePercentage, 
                    CONVERT(SUM(SDHrgJual) - SUM(SDJualDisc),decimal(15)) AS totalHargaServis, DATE_FORMAT(ttsdhd.SVTgl,'%d/%m/%Y %T') AS createdTime
                    FROM ttsditjasa
                    INNER JOIN ttsdhd ON ttsdhd.SDNo = ttsditjasa.SDNo
                    WHERE REPLACE(ttsdhd.ZnoWorkOrder, 'PKB', 'NJB') = :idJob
                    GROUP BY ttsditjasa.SDNO;",[':idJob'=>$item['noNJB']])->queryAll(); 

                    $nsc = $connection->createCommand("SELECT '' AS idJob, BrgKode AS partsNumber, CONVERT(SDQty,decimal(15)) AS kuantitas, CONVERT(SDHrgJual,decimal(15)) AS hargaParts, '' AS promoIDParts,
                    CONVERT(SDJualDisc,decimal(15)) AS discPartsAmount, CONVERT((SDJualDisc/(SDQty*SDHrgJual))*100,decimal(15)) AS discPartsPercentage, CONVERT(SDPajak,decimal(15)) AS ppn, 
                    CONVERT(SDQty*SDHrgJual-SDJualDisc,decimal(15)) AS totalHargaParts, 0 AS UangMuka,
                    DATE_FORMAT(ttsdhd.SVTgl,'%d/%m/%Y %T') AS createdTime
                    FROM ttsditbarang
                    INNER JOIN ttsdhd ON ttsdhd.SDNo = ttsditbarang.SDNo 
                    WHERE REPLACE(ttsdhd.ZnoWorkOrder, 'PKB', 'NJB') = :idJob",[':idJob'=>$item['noNJB']])->queryAll();

                    $api        = new Api(base64_encode($key));
                    $item['njb'] = $njb;
                    $item['nsc'] = $nsc;
                    $addParam = $item;
                    // $arrParams = array_merge_recursive($arrParams, $addParam);
                    $arrParams = $addParam;
                    $hasil     = $api->call(Api::TYPE_INV2, Json::encode($arrParams,JSON_NUMERIC_CHECK),[],false);
                    $arrHasil  = Json::decode($hasil);
                    if ($arrHasil['status']) {
                        $sukses++;
                    } else {
                        $gagal++;
                        echo "\n".Json::encode($arrParams,JSON_NUMERIC_CHECK)."\n";
                        echo "\n".$arrHasil['message']."\n";
                    }
                }
                $hasil = " Berhasil : " . $sukses . ", Gagal : " . $gagal;
                echo $hasil;
                $connection->createCommand()->insert('tuserlog', [
                    'LogTglJam'=>date('Y-m-d H:i:s'),'UserID'=>'System','LogJenis'=>'API','LogNama'=>$hasil,'LogTabel'=>'inv2','NoTrans'=>'CLI'
                ])->execute();
            }catch(Exception $e){
                echo "Error : ".$e->getMessage();
            }
            echo "\n";
        }
    }
    public function actionInv2Read($outlets = null)
    {
        $fromTime = Yii::$app->formatter->asDate('-1 day', 'yyyy-MM-dd');
        $toTime   = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        $addParam = [];
        if($outlets != null){
            $outlets = explode(',',$outlets);
        }
        foreach (Yii::$app->params['H2'] as $key => $outlet) {
            if(!empty($outlets) && !in_array($key,$outlets)){
                continue;
            }
            try{
                $api        = new Api(base64_encode($key));
                $mainDealer = $api->getMainDealer();
                if(empty(Yii::$app->params['mainDealer'][$mainDealer][Api::TYPE_INV2_READ])){
                    echo "Outlet : " . $key . ", not implemented yet\n";
                    continue;
                }
                switch ($mainDealer) {
                    case API::MD_HSO:
                        $arrParams = [
                            'fromTime' => $fromTime . ' 00:00:00',
                            'toTime'   => $toTime . ' 23:59:59',
                            'dealerId' => $api->getDealerId(),
                        ];

                        break;
                    case API::MD_ANPER:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                        break;
                    default:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                }
                $arrParams = array_merge_recursive($arrParams, $addParam);
                $hasil     = $api->call(Api::TYPE_INV2_READ, Json::encode($arrParams),[],true);
            }catch(Exception $e){
                echo "Error : ".$e->getMessage();
            }
            echo "\n";
        }
    }
    public function actionDphlo($outlets = null)
    {
        $fromTime = Yii::$app->formatter->asDate('-1 day', 'yyyy-MM-dd');
        $toTime   = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        $addParam = [];
        if($outlets != null){
            $outlets = explode(',',$outlets);
        }
        foreach (Yii::$app->params['H2'] as $key => $outlet) {
            if(!empty($outlets) && !in_array($key,$outlets)){
                continue;
            }
            try{
                $api        = new Api(base64_encode($key));
                $mainDealer = $api->getMainDealer();
                if(empty(Yii::$app->params['mainDealer'][$mainDealer][Api::TYPE_DPHLO])){
                    echo "Outlet : " . $key . ", not implemented yet\n";
                    continue;
                }
                switch ($mainDealer) {
                    case API::MD_HSO:
                        $arrParams = [
                            'fromTime' => $fromTime . ' 00:00:00',
                            'toTime'   => $toTime . ' 23:59:59',
                            'dealerId' => $api->getDealerId(),
                        ];

                        break;
                    case API::MD_ANPER:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                        break;
                    default:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                }
                $arrParams = array_merge_recursive($arrParams, $addParam);
                $hasil     = $api->call(Api::TYPE_DPHLO, Json::encode($arrParams),[],true);
            }catch(Exception $e){
                echo "Error : ".$e->getMessage();
            }
            echo "\n";
        }
    }
    public function actionUnpaidhlo($outlets = null)
    {
        $fromTime = Yii::$app->formatter->asDate('-1 day', 'yyyy-MM-dd');
        $toTime   = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        $addParam = [];
        if($outlets != null){
            $outlets = explode(',',$outlets);
        }
        foreach (Yii::$app->params['H2'] as $key => $outlet) {
            if(!empty($outlets) && !in_array($key,$outlets)){
                continue;
            }
            try{
                $api        = new Api(base64_encode($key));
                $mainDealer = $api->getMainDealer();
                if(empty(Yii::$app->params['mainDealer'][$mainDealer][Api::TYPE_UNPAIDHLO])){
                    echo "Outlet : " . $key . ", not implemented yet\n";
                    continue;
                }
                switch ($mainDealer) {
                    case API::MD_HSO:
                        $arrParams = [
                            'fromTime' => $fromTime . ' 00:00:00',
                            'toTime'   => $toTime . ' 23:59:59',
                            'dealerId' => $api->getDealerId(),
                        ];

                        break;
                    // case API::MD_ANPER:
                    //     $arrParams = [
                    //         'fromTime'               => $fromTime . ' 00:00:00',
                    //         'toTime'                 => $toTime . ' 23:59:59',
                    //         'dealerId'               => $api->getDealerId(),
                    //         'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                    //         'requestServertimezone'  => date('T'),
                    //         'requestServerTimestamp' => $api->getCurrUnixTime(),
                    //     ];
                    //     break;
                    // default:
                    //     $arrParams = [
                    //         'fromTime'               => $fromTime . ' 00:00:00',
                    //         'toTime'                 => $toTime . ' 23:59:59',
                    //         'dealerId'               => $api->getDealerId(),
                    //         'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                    //         'requestServertimezone'  => date('T'),
                    //         'requestServerTimestamp' => $api->getCurrUnixTime(),
                    //     ];
                }
                $arrParams = array_merge_recursive($arrParams, $addParam);
                $hasil     = $api->call(Api::TYPE_UNPAIDHLO, Json::encode($arrParams),[],true);
            }catch(Exception $e){
                echo "Error : ".$e->getMessage();
            }
            echo "\n";
        }
    }
    public function actionPinb($outlets = null)
    {
        $fromTime = Yii::$app->formatter->asDate('-1 day', 'yyyy-MM-dd');
        $toTime   = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        $addParam = ['noPO'=>''];
        if($outlets != null){
            $outlets = explode(',',$outlets);
        }
        foreach (Yii::$app->params['H2'] as $key => $outlet) {
            if(!empty($outlets) && !in_array($key,$outlets)){
                continue;
            }
            try{
                $api        = new Api(base64_encode($key));
                $mainDealer = $api->getMainDealer();
                switch ($mainDealer) {
                    case API::MD_HSO:
                        $arrParams = [
                            'fromTime' => $fromTime . ' 00:00:00',
                            'toTime'   => $toTime . ' 23:59:59',
                            'dealerId' => $api->getDealerId(),
                        ];

                        break;
                    case API::MD_ANPER:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                        break;
                    default:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                }
                $arrParams = array_merge_recursive($arrParams, $addParam);
                $hasil     = $api->call(Api::TYPE_PINB, Json::encode($arrParams));
            }catch(Exception $e){
                echo "Error : ".$e->getMessage();
            }
            echo "\n";
        }
    }
    public function actionPkb($outlets = null)
    {
        $fromTime = Yii::$app->formatter->asDate('-1 day', 'yyyy-MM-dd');
        $toTime   = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        $addParam = [];
        if($outlets != null){
            $outlets = explode(',',$outlets);
        }
        foreach (Yii::$app->params['H2'] as $key => $outlet) {
            if(!empty($outlets) && !in_array($key,$outlets)){
                continue;
            }
            try{
                $api        = new Api(base64_encode($key));
                $mainDealer = $api->getMainDealer();
                switch ($mainDealer) {
                    case API::MD_HSO:
                        $arrParams = [
                            'fromTime' => $fromTime . ' 00:00:00',
                            'toTime'   => $toTime . ' 23:59:59',
                            'dealerId' => $api->getDealerId(),
                            'noWorkOrder' => '',
                        ];

                        break;
                    case API::MD_ANPER:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                        break;
                    default:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                }
                $arrParams = array_merge_recursive($arrParams, $addParam);
                $hasil     = $api->call(Api::TYPE_PKB, Json::encode($arrParams));
                //echo $hasil;
                $arrHasil  = Json::decode($hasil);
                if (empty($arrHasil['data'])) {
                    echo "Outlet : " . $key . ", Data : 0";
                } else if (is_array($arrHasil['data'])) {
                    echo "Outlet : " . $key . ", Data : " . sizeof($arrHasil['data'])."\n";
                    $db_arr   = Yii::$app->components['db'];
                    $connection = new \yii\db\Connection([
                        'dsn'      => $db_arr['dsn'] . 'dbname=' . $key . ';',
                        'username' => $db_arr['username'],
                        'password' => $db_arr['password'],
                    ]);
                    $connection->open();
                    $dataFields = [
                        'noWorkOrder',  'noSAForm', 'tanggalServis', 'waktuPKB', 'noPolisi','noRangka', 'noMesin',
                        'kodeTipeUnit', 'tahunMotor', 'informasiBensin', 'kmTerakhir', 'tipeComingCustomer', 'namaPemilik',
                        'alamatPemilik', 'kodePropinsiPemilik', 'kodeKotaPemilik', 'kodeKecamatanPemilik','kodeKelurahanPemilik',
                        'kodePosPemilik', 'alamatPembawa', 'kodePropinsiPembawa', 'kodeKotaPembawa','kodeKecamatanPembawa',
                        'kodeKelurahanPembawa', 'kodePosPembawa', 'namaPembawa', 'noTelpPembawa','hubunganDenganPemilik',
                        'keluhanKonsumen', 'rekomendasiSA', 'hondaIdSA', 'hondaIdMekanik','saranMekanik','asalUnitEntry',
                        'idPIT', 'jenisPIT', 'waktuPendaftaran', 'waktuSelesai','totalFRT','setUpPembayaran','catatanTambahan',
                        'konfirmasiPekerjaanTambahan', 'noBukuClaimC2', 'noWorkOrderJobReturn', 'totalBiayaService','waktuPekerjaan',
                        'statusWorkOrder','createdTime',
                    ];
                    $dataTemplate = array_fill_keys($dataFields, null);
                    $dataTemplate = (array_change_key_case($dataTemplate, CASE_LOWER));

                    $dataFieldParts = [
                        'noWorkOrder',  'idJob', 'partsNumber', 'kuantitas', 'hargaParts','createdTime'
                    ];
                    $dataTemplateParts = array_fill_keys($dataFieldParts, null);
                    $dataTemplateParts = (array_change_key_case($dataTemplateParts, CASE_LOWER));


                    $dataFieldServices = [
                        'noWorkOrder',  'idJob', 'namaPekerjaan', 'jenisPekerjaan', 'biayaService','createdTime'
                    ];
                    $dataTemplateServices = array_fill_keys($dataFieldServices, null);
                    $dataTemplateServices = (array_change_key_case($dataTemplateServices, CASE_LOWER));

                    $record = $recordParts = $recordServices = [];
                    foreach($arrHasil['data'] as $data){
                        $data = (array_change_key_case($data, CASE_LOWER));
                        $parts = $data['parts'];
                        unset($data['parts']);
                        $services = $data['services'];
                        unset($data['services']);
                        $DateTime = DateTime::createFromFormat('d/m/Y', $data['tanggalservis']);
                        $data['tanggalservis'] = ($DateTime === false)? null:$DateTime->format('Y-m-d');
                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['waktupkb']);
                        $data['waktupkb'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['waktupendaftaran']);
                        $data['waktupendaftaran'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['waktuselesai']);
                        $data['waktuselesai'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                        $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $data['createdtime']);
                        $data['createdtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                        $realData = array_merge($dataTemplate,$data);
                        $diffKeys = array_diff_key($realData, $dataTemplate);
                        // var_dump($realData);die();return;
                        $finalArrays = self::array_except($realData, $diffKeys);
                        $record[] = array_values($finalArrays);
                        foreach($parts as $detil){
                            $detil = (array_change_key_case($detil, CASE_LOWER));
                            $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $detil['createdtime']);
                            $detil['createdtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                            $detil = ['noworkorder'=> $data['noworkorder']] + $detil;
                            $realData = array_merge($dataTemplateParts,$detil);
                            $diffKeys = array_diff_key($realData, $dataTemplateParts);
                            // var_dump($detil);die();return;
                            $finalArrays = self::array_except($realData, $diffKeys);
                            $recordParts[] = array_values($finalArrays);
                        }
                        foreach($services as $detil){
                            $detil = (array_change_key_case($detil, CASE_LOWER));
                            $DateTime = DateTime::createFromFormat('d/m/Y H:i:s', $detil['createdtime']);
                            $detil['createdtime'] = ($DateTime === false)? null:$DateTime->format('Y-m-d H:i:s');
                            $detil = ['noworkorder'=> $data['noworkorder']] + $detil;
                            $realData = array_merge($dataTemplateServices, $detil);
                            $diffKeys = array_diff_key($realData, $dataTemplateServices);
                            // var_dump($realData);die();return;
                            $finalArrays = self::array_except($realData, $diffKeys);
                            $recordServices[] = array_values($finalArrays);
                        }
                    }
                    $command = $connection->createCommand()->batchInsert(
                        'txpkbhd',$dataFields,$record
                    );
                    $sql = $command->getRawSql();
                    $sql .= ' ON DUPLICATE KEY UPDATE noSAForm=VALUES(noSAForm),tanggalServis=VALUES(tanggalServis),waktuPKB=VALUES(waktuPKB)'
                    .",noPolisi=VALUES(noPolisi),noRangka=VALUES(noRangka),noMesin=VALUES(noMesin),kodeTipeUnit=VALUES(kodeTipeUnit)"
                    .",tahunMotor=VALUES(tahunMotor),informasiBensin=VALUES(informasiBensin),kmTerakhir=VALUES(kmTerakhir),tipeComingCustomer=VALUES(tipeComingCustomer)"
                    .",namaPemilik=VALUES(namaPemilik),alamatPemilik=VALUES(alamatPemilik),kodePropinsiPemilik=VALUES(kodePropinsiPemilik),kodeKotaPemilik=VALUES(kodeKotaPemilik)"
                    .",kodeKecamatanPemilik=VALUES(kodeKecamatanPemilik),kodeKelurahanPemilik=VALUES(kodeKelurahanPemilik),kodePosPemilik=VALUES(kodePosPemilik),alamatPembawa=VALUES(alamatPembawa)"
                    .",kodePropinsiPembawa=VALUES(kodePropinsiPembawa),kodeKotaPembawa=VALUES(kodeKotaPembawa),kodeKecamatanPembawa=VALUES(kodeKecamatanPembawa)"
                    .",kodeKelurahanPembawa=VALUES(kodeKelurahanPembawa),kodePosPembawa=VALUES(kodePosPembawa),namaPembawa=VALUES(namaPembawa),noTelpPembawa=VALUES(noTelpPembawa)"
                    .",hubunganDenganPemilik=VALUES(hubunganDenganPemilik),keluhanKonsumen=VALUES(keluhanKonsumen),rekomendasiSA=VALUES(rekomendasiSA),hondaIdSA=VALUES(hondaIdSA)"
                    .",hondaIdMekanik=VALUES(hondaIdMekanik),saranMekanik=VALUES(saranMekanik),asalUnitEntry=VALUES(asalUnitEntry),idPIT=VALUES(idPIT),jenisPIT=VALUES(jenisPIT)"
                    .",waktuPendaftaran=VALUES(waktuPendaftaran),waktuSelesai=VALUES(waktuSelesai),totalFRT=VALUES(totalFRT),setUpPembayaran=VALUES(setUpPembayaran)"
                    .",catatanTambahan=VALUES(catatanTambahan),konfirmasiPekerjaanTambahan=VALUES(konfirmasiPekerjaanTambahan),noBukuClaimC2=VALUES(noBukuClaimC2),noWorkOrderJobReturn=VALUES(noWorkOrderJobReturn)"
                    .",totalBiayaService=VALUES(totalBiayaService),waktuPekerjaan=VALUES(waktuPekerjaan),statusWorkOrder=VALUES(statusWorkOrder),createdTime=VALUES(createdTime)";
                    $command->setRawSql($sql);
                    // echo $sql;
                    $aff = $command->execute();
                    if(!empty($recordParts) && is_array($recordParts)){
                        $command = $connection->createCommand()->batchInsert(
                            'txpkbitbarang',$dataFieldParts, $recordParts
                        );
                        $sql = $command->getRawSql();
                        $sql .= ' ON DUPLICATE KEY UPDATE kuantitas=VALUES(kuantitas),hargaParts=VALUES(hargaParts),createdTime=VALUES(createdTime)';
                        $command->setRawSql($sql);
                        // echo $sql;
                        $command->execute();
                    }
                    if(!empty($recordServices) && is_array($recordServices)){
                        $command = $connection->createCommand()->batchInsert(
                            'txpkbitjasa',$dataFieldServices,$recordServices
                        );
                        $sql = $command->getRawSql();
                        $sql .= ' ON DUPLICATE KEY UPDATE jenisPekerjaan=VALUES(jenisPekerjaan),biayaService=VALUES(biayaService),createdTime=VALUES(createdTime)';
                        $command->setRawSql($sql);
                        // echo $sql;
                        $command->execute();
                    }
                    echo "Row Affected : ".$aff;
                } else {
                    echo "Outlet : " . $key . ", Data : undefined";
                }
            }catch(Exception $e){
                echo "Error : ".$e->getMessage();
            }
            echo "\n";
        }
    }
    public function actionPrsl($outlets = null)
    {
        $fromTime = Yii::$app->formatter->asDate('-1 day', 'yyyy-MM-dd');
        $toTime   = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        $addParam = [];
        if($outlets != null){
            $outlets = explode(',',$outlets);
        }
        foreach (Yii::$app->params['H2'] as $key => $outlet) {
            if(!empty($outlets) && !in_array($key,$outlets)){
                continue;
            }
            try{
                $api        = new Api(base64_encode($key));
                $mainDealer = $api->getMainDealer();
                switch ($mainDealer) {
                    case API::MD_HSO:
                        $arrParams = [
                            'fromTime' => $fromTime . ' 00:00:00',
                            'toTime'   => $toTime . ' 23:59:59',
                            'dealerId' => $api->getDealerId(),
                        ];

                        break;
                    case API::MD_ANPER:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                        break;
                    default:
                        $arrParams = [
                            'fromTime'               => $fromTime . ' 00:00:00',
                            'toTime'                 => $toTime . ' 23:59:59',
                            'dealerId'               => $api->getDealerId(),
                            'requestServerTime'      => date('Y-m-d H:i:s', $api->getCurrUnixTime()),
                            'requestServertimezone'  => date('T'),
                            'requestServerTimestamp' => $api->getCurrUnixTime(),
                        ];
                }
                $arrParams = array_merge_recursive($arrParams, $addParam);
                $hasil     = $api->call(Api::TYPE_PRSL, Json::encode($arrParams));
            }catch(Exception $e){
                echo "Error : ".$e->getMessage();
            }
            echo "\n";
        }
    }

}
